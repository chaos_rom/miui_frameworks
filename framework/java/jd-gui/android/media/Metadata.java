package android.media;

import android.os.Parcel;
import android.util.Log;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.TimeZone;

public class Metadata
{
    public static final int ALBUM = 8;
    public static final int ALBUM_ART = 18;
    public static final int ANY = 0;
    public static final int ARTIST = 9;
    public static final int AUDIO_BIT_RATE = 21;
    public static final int AUDIO_CODEC = 26;
    public static final int AUDIO_SAMPLE_RATE = 23;
    public static final int AUTHOR = 10;
    public static final int BIT_RATE = 20;
    public static final int BOOLEAN_VAL = 3;
    public static final int BYTE_ARRAY_VAL = 7;
    public static final int CD_TRACK_MAX = 16;
    public static final int CD_TRACK_NUM = 15;
    public static final int COMMENT = 6;
    public static final int COMPOSER = 11;
    public static final int COPYRIGHT = 7;
    public static final int DATE = 13;
    public static final int DATE_VAL = 6;
    public static final int DOUBLE_VAL = 5;
    public static final int DRM_CRIPPLED = 31;
    public static final int DURATION = 14;
    private static final int FIRST_CUSTOM = 8192;
    public static final int GENRE = 12;
    public static final int INTEGER_VAL = 2;
    private static final int LAST_SYSTEM = 31;
    private static final int LAST_TYPE = 7;
    public static final int LONG_VAL = 4;
    public static final Set<Integer> MATCH_ALL = Collections.singleton(Integer.valueOf(0));
    public static final Set<Integer> MATCH_NONE = Collections.EMPTY_SET;
    public static final int MIME_TYPE = 25;
    public static final int NUM_TRACKS = 30;
    public static final int PAUSE_AVAILABLE = 1;
    public static final int RATING = 17;
    public static final int SEEK_AVAILABLE = 4;
    public static final int SEEK_BACKWARD_AVAILABLE = 2;
    public static final int SEEK_FORWARD_AVAILABLE = 3;
    public static final int STRING_VAL = 1;
    private static final String TAG = "media.Metadata";
    public static final int TITLE = 5;
    public static final int VIDEO_BIT_RATE = 22;
    public static final int VIDEO_CODEC = 27;
    public static final int VIDEO_FRAME = 19;
    public static final int VIDEO_FRAME_RATE = 24;
    public static final int VIDEO_HEIGHT = 28;
    public static final int VIDEO_WIDTH = 29;
    private static final int kInt32Size = 4;
    private static final int kMetaHeaderSize = 8;
    private static final int kMetaMarker = 1296389185;
    private static final int kRecordHeaderSize = 12;
    private final HashMap<Integer, Integer> mKeyToPosMap = new HashMap();
    private Parcel mParcel;

    private boolean checkMetadataId(int paramInt)
    {
        if ((paramInt <= 0) || ((31 < paramInt) && (paramInt < 8192)))
            Log.e("media.Metadata", "Invalid metadata ID " + paramInt);
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private void checkType(int paramInt1, int paramInt2)
    {
        int i = ((Integer)this.mKeyToPosMap.get(Integer.valueOf(paramInt1))).intValue();
        this.mParcel.setDataPosition(i);
        int j = this.mParcel.readInt();
        if (j != paramInt2)
            throw new IllegalStateException("Wrong type " + paramInt2 + " but got " + j);
    }

    public static int firstCustomId()
    {
        return 8192;
    }

    public static int lastSytemId()
    {
        return 31;
    }

    public static int lastType()
    {
        return 7;
    }

    private boolean scanAllRecords(Parcel paramParcel, int paramInt)
    {
        int i = 0;
        int j = 0;
        this.mKeyToPosMap.clear();
        int k;
        int m;
        if (paramInt > 12)
        {
            k = paramParcel.dataPosition();
            m = paramParcel.readInt();
            if (m <= 12)
            {
                Log.e("media.Metadata", "Record is too short");
                j = 1;
            }
        }
        else
        {
            label48: if ((paramInt == 0) && (j == 0))
                break label235;
            Log.e("media.Metadata", "Ran out of data or error on record " + i);
            this.mKeyToPosMap.clear();
        }
        label235: for (boolean bool = false; ; bool = true)
        {
            return bool;
            int n = paramParcel.readInt();
            if (!checkMetadataId(n))
            {
                j = 1;
                break label48;
            }
            if (this.mKeyToPosMap.containsKey(Integer.valueOf(n)))
            {
                Log.e("media.Metadata", "Duplicate metadata ID found");
                j = 1;
                break label48;
            }
            this.mKeyToPosMap.put(Integer.valueOf(n), Integer.valueOf(paramParcel.dataPosition()));
            int i1 = paramParcel.readInt();
            if ((i1 <= 0) || (i1 > 7))
            {
                Log.e("media.Metadata", "Invalid metadata type " + i1);
                j = 1;
                break label48;
            }
            paramParcel.setDataPosition(k + m);
            paramInt -= m;
            i++;
            break;
        }
    }

    public boolean getBoolean(int paramInt)
    {
        int i = 1;
        checkType(paramInt, 3);
        if (this.mParcel.readInt() == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public byte[] getByteArray(int paramInt)
    {
        checkType(paramInt, 7);
        return this.mParcel.createByteArray();
    }

    public Date getDate(int paramInt)
    {
        checkType(paramInt, 6);
        long l = this.mParcel.readLong();
        String str = this.mParcel.readString();
        if (str.length() == 0);
        Calendar localCalendar;
        for (Date localDate = new Date(l); ; localDate = localCalendar.getTime())
        {
            return localDate;
            localCalendar = Calendar.getInstance(TimeZone.getTimeZone(str));
            localCalendar.setTimeInMillis(l);
        }
    }

    public double getDouble(int paramInt)
    {
        checkType(paramInt, 5);
        return this.mParcel.readDouble();
    }

    public int getInt(int paramInt)
    {
        checkType(paramInt, 2);
        return this.mParcel.readInt();
    }

    public long getLong(int paramInt)
    {
        checkType(paramInt, 4);
        return this.mParcel.readLong();
    }

    public String getString(int paramInt)
    {
        checkType(paramInt, 1);
        return this.mParcel.readString();
    }

    public boolean has(int paramInt)
    {
        if (!checkMetadataId(paramInt))
            throw new IllegalArgumentException("Invalid key: " + paramInt);
        return this.mKeyToPosMap.containsKey(Integer.valueOf(paramInt));
    }

    public Set<Integer> keySet()
    {
        return this.mKeyToPosMap.keySet();
    }

    public boolean parse(Parcel paramParcel)
    {
        boolean bool = false;
        if (paramParcel.dataAvail() < 8)
            Log.e("media.Metadata", "Not enough data " + paramParcel.dataAvail());
        while (true)
        {
            return bool;
            int i = paramParcel.dataPosition();
            int j = paramParcel.readInt();
            if ((4 + paramParcel.dataAvail() < j) || (j < 8))
            {
                Log.e("media.Metadata", "Bad size " + j + " avail " + paramParcel.dataAvail() + " position " + i);
                paramParcel.setDataPosition(i);
            }
            else
            {
                int k = paramParcel.readInt();
                if (k != 1296389185)
                {
                    Log.e("media.Metadata", "Marker missing " + Integer.toHexString(k));
                    paramParcel.setDataPosition(i);
                }
                else if (!scanAllRecords(paramParcel, j - 8))
                {
                    paramParcel.setDataPosition(i);
                }
                else
                {
                    this.mParcel = paramParcel;
                    bool = true;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.Metadata
 * JD-Core Version:        0.6.2
 */