package android.media;

import java.io.IOException;
import java.io.InputStream;

public final class ResampleInputStream extends InputStream
{
    private static final String TAG = "ResampleInputStream";
    private static final int mFirLength = 29;
    private byte[] mBuf;
    private int mBufCount;
    private InputStream mInputStream;
    private final byte[] mOneByte = new byte[1];
    private final int mRateIn;
    private final int mRateOut;

    static
    {
        System.loadLibrary("media_jni");
    }

    public ResampleInputStream(InputStream paramInputStream, int paramInt1, int paramInt2)
    {
        if (paramInt1 != paramInt2 * 2)
            throw new IllegalArgumentException("only support 2:1 at the moment");
        this.mInputStream = paramInputStream;
        this.mRateIn = 2;
        this.mRateOut = 1;
    }

    private static native void fir21(byte[] paramArrayOfByte1, int paramInt1, byte[] paramArrayOfByte2, int paramInt2, int paramInt3);

    public void close()
        throws IOException
    {
        try
        {
            if (this.mInputStream != null)
                this.mInputStream.close();
            return;
        }
        finally
        {
            this.mInputStream = null;
        }
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mInputStream != null)
        {
            close();
            throw new IllegalStateException("someone forgot to close ResampleInputStream");
        }
    }

    public int read()
        throws IOException
    {
        if (read(this.mOneByte, 0, 1) == 1);
        for (int i = 0xFF & this.mOneByte[0]; ; i = -1)
            return i;
    }

    public int read(byte[] paramArrayOfByte)
        throws IOException
    {
        return read(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        int i = -1;
        if (this.mInputStream == null)
            throw new IllegalStateException("not open");
        int j = 2 * (29 + paramInt2 / 2 * this.mRateIn / this.mRateOut);
        if (this.mBuf == null)
            this.mBuf = new byte[j];
        while (true)
        {
            int k = 2 * ((-29 + this.mBufCount / 2) * this.mRateOut / this.mRateIn);
            int n;
            if (k > 0)
                if (k < paramInt2)
                {
                    n = k;
                    label94: fir21(this.mBuf, 0, paramArrayOfByte, paramInt1, n / 2);
                    int i1 = n * this.mRateIn / this.mRateOut;
                    this.mBufCount -= i1;
                    if (this.mBufCount > 0)
                        System.arraycopy(this.mBuf, i1, this.mBuf, 0, this.mBufCount);
                    i = n;
                }
            int m;
            do
            {
                return i;
                if (j <= this.mBuf.length)
                    break;
                byte[] arrayOfByte = new byte[j];
                System.arraycopy(this.mBuf, 0, arrayOfByte, 0, this.mBufCount);
                this.mBuf = arrayOfByte;
                break;
                n = 2 * (paramInt2 / 2);
                break label94;
                m = this.mInputStream.read(this.mBuf, this.mBufCount, this.mBuf.length - this.mBufCount);
            }
            while (m == i);
            this.mBufCount = (m + this.mBufCount);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.ResampleInputStream
 * JD-Core Version:        0.6.2
 */