package android.media;

import android.content.ContentValues;
import android.content.IContentProvider;
import android.net.Uri;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MediaInserter
{
    private int mBufferSizePerUri;
    private final HashMap<Uri, List<ContentValues>> mPriorityRowMap = new HashMap();
    private IContentProvider mProvider;
    private final HashMap<Uri, List<ContentValues>> mRowMap = new HashMap();

    public MediaInserter(IContentProvider paramIContentProvider, int paramInt)
    {
        this.mProvider = paramIContentProvider;
        this.mBufferSizePerUri = paramInt;
    }

    private void flush(Uri paramUri, List<ContentValues> paramList)
        throws RemoteException
    {
        if (!paramList.isEmpty())
        {
            ContentValues[] arrayOfContentValues = (ContentValues[])paramList.toArray(new ContentValues[paramList.size()]);
            this.mProvider.bulkInsert(paramUri, arrayOfContentValues);
            paramList.clear();
        }
    }

    private void flushAllPriority()
        throws RemoteException
    {
        Iterator localIterator = this.mPriorityRowMap.keySet().iterator();
        while (localIterator.hasNext())
        {
            Uri localUri = (Uri)localIterator.next();
            flush(localUri, (List)this.mPriorityRowMap.get(localUri));
        }
        this.mPriorityRowMap.clear();
    }

    private void insert(Uri paramUri, ContentValues paramContentValues, boolean paramBoolean)
        throws RemoteException
    {
        if (paramBoolean);
        for (HashMap localHashMap = this.mPriorityRowMap; ; localHashMap = this.mRowMap)
        {
            Object localObject = (List)localHashMap.get(paramUri);
            if (localObject == null)
            {
                localObject = new ArrayList();
                localHashMap.put(paramUri, localObject);
            }
            ((List)localObject).add(new ContentValues(paramContentValues));
            if (((List)localObject).size() >= this.mBufferSizePerUri)
            {
                flushAllPriority();
                flush(paramUri, (List)localObject);
            }
            return;
        }
    }

    public void flushAll()
        throws RemoteException
    {
        flushAllPriority();
        Iterator localIterator = this.mRowMap.keySet().iterator();
        while (localIterator.hasNext())
        {
            Uri localUri = (Uri)localIterator.next();
            flush(localUri, (List)this.mRowMap.get(localUri));
        }
        this.mRowMap.clear();
    }

    public void insert(Uri paramUri, ContentValues paramContentValues)
        throws RemoteException
    {
        insert(paramUri, paramContentValues, false);
    }

    public void insertwithPriority(Uri paramUri, ContentValues paramContentValues)
        throws RemoteException
    {
        insert(paramUri, paramContentValues, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaInserter
 * JD-Core Version:        0.6.2
 */