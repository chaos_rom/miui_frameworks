package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IRemoteVolumeObserver extends IInterface
{
    public abstract void dispatchRemoteVolumeUpdate(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRemoteVolumeObserver
    {
        private static final String DESCRIPTOR = "android.media.IRemoteVolumeObserver";
        static final int TRANSACTION_dispatchRemoteVolumeUpdate = 1;

        public Stub()
        {
            attachInterface(this, "android.media.IRemoteVolumeObserver");
        }

        public static IRemoteVolumeObserver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.media.IRemoteVolumeObserver");
                if ((localIInterface != null) && ((localIInterface instanceof IRemoteVolumeObserver)))
                    localObject = (IRemoteVolumeObserver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.media.IRemoteVolumeObserver");
                continue;
                paramParcel1.enforceInterface("android.media.IRemoteVolumeObserver");
                dispatchRemoteVolumeUpdate(paramParcel1.readInt(), paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements IRemoteVolumeObserver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void dispatchRemoteVolumeUpdate(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.media.IRemoteVolumeObserver");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.media.IRemoteVolumeObserver";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.IRemoteVolumeObserver
 * JD-Core Version:        0.6.2
 */