package android.media;

public class AudioSystem
{
    public static final int AUDIO_STATUS_ERROR = 1;
    public static final int AUDIO_STATUS_OK = 0;
    public static final int AUDIO_STATUS_SERVER_DIED = 100;
    public static final int DEVICE_IN_AMBIENT = 131072;
    public static final int DEVICE_IN_AUX_DIGITAL = 8388608;
    public static final int DEVICE_IN_BLUETOOTH_SCO_HEADSET = 2097152;
    public static final int DEVICE_IN_BUILTIN_MIC1 = 262144;
    public static final int DEVICE_IN_BUILTIN_MIC2 = 524288;
    public static final int DEVICE_IN_COMMUNICATION = 65536;
    public static final int DEVICE_IN_DEFAULT = -2147483648;
    public static final int DEVICE_IN_MIC_ARRAY = 1048576;
    public static final int DEVICE_IN_WIRED_HEADSET = 4194304;
    public static final int DEVICE_OUT_ALL = 65535;
    public static final int DEVICE_OUT_ALL_A2DP = 896;
    public static final int DEVICE_OUT_ALL_SCO = 112;
    public static final int DEVICE_OUT_ALL_USB = 24576;
    public static final int DEVICE_OUT_ANLG_DOCK_HEADSET = 2048;
    public static final String DEVICE_OUT_ANLG_DOCK_HEADSET_NAME = "analog_dock";
    public static final int DEVICE_OUT_AUX_DIGITAL = 1024;
    public static final String DEVICE_OUT_AUX_DIGITAL_NAME = "aux_digital";
    public static final int DEVICE_OUT_BLUETOOTH_A2DP = 128;
    public static final int DEVICE_OUT_BLUETOOTH_A2DP_HEADPHONES = 256;
    public static final String DEVICE_OUT_BLUETOOTH_A2DP_HEADPHONES_NAME = "bt_a2dp_hp";
    public static final String DEVICE_OUT_BLUETOOTH_A2DP_NAME = "bt_a2dp";
    public static final int DEVICE_OUT_BLUETOOTH_A2DP_SPEAKER = 512;
    public static final String DEVICE_OUT_BLUETOOTH_A2DP_SPEAKER_NAME = "bt_a2dp_spk";
    public static final int DEVICE_OUT_BLUETOOTH_SCO = 16;
    public static final int DEVICE_OUT_BLUETOOTH_SCO_CARKIT = 64;
    public static final String DEVICE_OUT_BLUETOOTH_SCO_CARKIT_NAME = "bt_sco_carkit";
    public static final int DEVICE_OUT_BLUETOOTH_SCO_HEADSET = 32;
    public static final String DEVICE_OUT_BLUETOOTH_SCO_HEADSET_NAME = "bt_sco_hs";
    public static final String DEVICE_OUT_BLUETOOTH_SCO_NAME = "bt_sco";
    public static final int DEVICE_OUT_DEFAULT = 32768;
    public static final int DEVICE_OUT_DGTL_DOCK_HEADSET = 4096;
    public static final String DEVICE_OUT_DGTL_DOCK_HEADSET_NAME = "digital_dock";
    public static final int DEVICE_OUT_EARPIECE = 1;
    public static final String DEVICE_OUT_EARPIECE_NAME = "earpiece";
    public static final int DEVICE_OUT_SPEAKER = 2;
    public static final String DEVICE_OUT_SPEAKER_NAME = "speaker";
    public static final int DEVICE_OUT_USB_ACCESSORY = 8192;
    public static final String DEVICE_OUT_USB_ACCESSORY_NAME = "usb_accessory";
    public static final int DEVICE_OUT_USB_DEVICE = 16384;
    public static final String DEVICE_OUT_USB_DEVICE_NAME = "usb_device";
    public static final int DEVICE_OUT_WIRED_HEADPHONE = 8;
    public static final String DEVICE_OUT_WIRED_HEADPHONE_NAME = "headphone";
    public static final int DEVICE_OUT_WIRED_HEADSET = 4;
    public static final String DEVICE_OUT_WIRED_HEADSET_NAME = "headset";
    public static final int DEVICE_STATE_AVAILABLE = 1;
    public static final int DEVICE_STATE_UNAVAILABLE = 0;
    public static final int FORCE_ANALOG_DOCK = 8;
    public static final int FORCE_BT_A2DP = 4;
    public static final int FORCE_BT_CAR_DOCK = 6;
    public static final int FORCE_BT_DESK_DOCK = 7;
    public static final int FORCE_BT_SCO = 3;
    public static final int FORCE_DEFAULT = 0;
    public static final int FORCE_DIGITAL_DOCK = 9;
    public static final int FORCE_HEADPHONES = 2;
    public static final int FORCE_NONE = 0;
    public static final int FORCE_NO_BT_A2DP = 10;
    public static final int FORCE_SPEAKER = 1;
    public static final int FORCE_WIRED_ACCESSORY = 5;
    public static final int FOR_COMMUNICATION = 0;
    public static final int FOR_DOCK = 3;
    public static final int FOR_MEDIA = 1;
    public static final int FOR_RECORD = 2;
    public static final int MODE_CURRENT = -1;
    public static final int MODE_INVALID = -2;
    public static final int MODE_IN_CALL = 2;
    public static final int MODE_IN_COMMUNICATION = 3;
    public static final int MODE_NORMAL = 0;
    public static final int MODE_RINGTONE = 1;
    private static final int NUM_DEVICE_STATES = 1;
    private static final int NUM_FORCE_CONFIG = 11;
    private static final int NUM_FORCE_USE = 4;
    public static final int NUM_MODES = 4;
    public static final int NUM_STREAMS = 5;
    private static final int NUM_STREAM_TYPES = 10;
    public static final int PHONE_STATE_INCALL = 2;
    public static final int PHONE_STATE_OFFCALL = 0;
    public static final int PHONE_STATE_RINGING = 1;

    @Deprecated
    public static final int ROUTE_ALL = -1;

    @Deprecated
    public static final int ROUTE_BLUETOOTH = 4;

    @Deprecated
    public static final int ROUTE_BLUETOOTH_A2DP = 16;

    @Deprecated
    public static final int ROUTE_BLUETOOTH_SCO = 4;

    @Deprecated
    public static final int ROUTE_EARPIECE = 1;

    @Deprecated
    public static final int ROUTE_HEADSET = 8;

    @Deprecated
    public static final int ROUTE_SPEAKER = 2;
    public static final int STREAM_ALARM = 4;
    public static final int STREAM_BLUETOOTH_SCO = 6;
    public static final int STREAM_DTMF = 8;
    public static final int STREAM_MUSIC = 3;
    public static final int STREAM_NOTIFICATION = 5;
    public static final int STREAM_RING = 2;
    public static final int STREAM_SYSTEM = 1;
    public static final int STREAM_SYSTEM_ENFORCED = 7;
    public static final int STREAM_TTS = 9;
    public static final int STREAM_VOICE_CALL = 0;
    public static final int SYNC_EVENT_NONE = 0;
    public static final int SYNC_EVENT_PRESENTATION_COMPLETE = 1;
    private static ErrorCallback mErrorCallback;

    private static void errorCallbackFromNative(int paramInt)
    {
        ErrorCallback localErrorCallback = null;
        try
        {
            if (mErrorCallback != null)
                localErrorCallback = mErrorCallback;
            if (localErrorCallback != null)
                localErrorCallback.onError(paramInt);
            return;
        }
        finally
        {
        }
    }

    public static native int getDeviceConnectionState(int paramInt, String paramString);

    public static String getDeviceName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "";
        case 1:
        case 2:
        case 4:
        case 8:
        case 16:
        case 32:
        case 64:
        case 128:
        case 256:
        case 512:
        case 1024:
        case 2048:
        case 4096:
        case 8192:
        case 16384:
        }
        while (true)
        {
            return str;
            str = "earpiece";
            continue;
            str = "speaker";
            continue;
            str = "headset";
            continue;
            str = "headphone";
            continue;
            str = "bt_sco";
            continue;
            str = "bt_sco_hs";
            continue;
            str = "bt_sco_carkit";
            continue;
            str = "bt_a2dp";
            continue;
            str = "bt_a2dp_hp";
            continue;
            str = "bt_a2dp_spk";
            continue;
            str = "aux_digital";
            continue;
            str = "analog_dock";
            continue;
            str = "digital_dock";
            continue;
            str = "usb_accessory";
            continue;
            str = "usb_device";
        }
    }

    public static native int getDevicesForStream(int paramInt);

    public static native int getForceUse(int paramInt);

    public static native boolean getMasterMute();

    public static native float getMasterVolume();

    public static final int getNumStreamTypes()
    {
        return 10;
    }

    public static native String getParameters(String paramString);

    public static native int getStreamVolumeIndex(int paramInt1, int paramInt2);

    public static native int initStreamVolume(int paramInt1, int paramInt2, int paramInt3);

    public static native boolean isMicrophoneMuted();

    public static native boolean isStreamActive(int paramInt1, int paramInt2);

    public static native int muteMicrophone(boolean paramBoolean);

    public static native int setDeviceConnectionState(int paramInt1, int paramInt2, String paramString);

    public static void setErrorCallback(ErrorCallback paramErrorCallback)
    {
        try
        {
            mErrorCallback = paramErrorCallback;
            isMicrophoneMuted();
            return;
        }
        finally
        {
        }
    }

    public static native int setForceUse(int paramInt1, int paramInt2);

    public static native int setMasterMute(boolean paramBoolean);

    public static native int setMasterVolume(float paramFloat);

    public static native int setParameters(String paramString);

    public static native int setPhoneState(int paramInt);

    public static native int setStreamVolumeIndex(int paramInt1, int paramInt2, int paramInt3);

    public static abstract interface ErrorCallback
    {
        public abstract void onError(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.AudioSystem
 * JD-Core Version:        0.6.2
 */