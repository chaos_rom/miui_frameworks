package android.media;

import android.graphics.Rect;
import android.os.Parcel;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public final class TimedText
{
    private static final int FIRST_PRIVATE_KEY = 101;
    private static final int FIRST_PUBLIC_KEY = 1;
    private static final int KEY_BACKGROUND_COLOR_RGBA = 3;
    private static final int KEY_DISPLAY_FLAGS = 1;
    private static final int KEY_END_CHAR = 104;
    private static final int KEY_FONT_ID = 105;
    private static final int KEY_FONT_SIZE = 106;
    private static final int KEY_GLOBAL_SETTING = 101;
    private static final int KEY_HIGHLIGHT_COLOR_RGBA = 4;
    private static final int KEY_LOCAL_SETTING = 102;
    private static final int KEY_SCROLL_DELAY = 5;
    private static final int KEY_START_CHAR = 103;
    private static final int KEY_START_TIME = 7;
    private static final int KEY_STRUCT_BLINKING_TEXT_LIST = 8;
    private static final int KEY_STRUCT_FONT_LIST = 9;
    private static final int KEY_STRUCT_HIGHLIGHT_LIST = 10;
    private static final int KEY_STRUCT_HYPER_TEXT_LIST = 11;
    private static final int KEY_STRUCT_JUSTIFICATION = 15;
    private static final int KEY_STRUCT_KARAOKE_LIST = 12;
    private static final int KEY_STRUCT_STYLE_LIST = 13;
    private static final int KEY_STRUCT_TEXT = 16;
    private static final int KEY_STRUCT_TEXT_POS = 14;
    private static final int KEY_STYLE_FLAGS = 2;
    private static final int KEY_TEXT_COLOR_RGBA = 107;
    private static final int KEY_WRAP_TEXT = 6;
    private static final int LAST_PRIVATE_KEY = 107;
    private static final int LAST_PUBLIC_KEY = 16;
    private static final String TAG = "TimedText";
    private int mBackgroundColorRGBA = -1;
    private List<CharPos> mBlinkingPosList = null;
    private int mDisplayFlags = -1;
    private List<Font> mFontList = null;
    private int mHighlightColorRGBA = -1;
    private List<CharPos> mHighlightPosList = null;
    private List<HyperText> mHyperTextList = null;
    private Justification mJustification;
    private List<Karaoke> mKaraokeList = null;
    private final HashMap<Integer, Object> mKeyObjectMap = new HashMap();
    private int mScrollDelay = -1;
    private List<Style> mStyleList = null;
    private Rect mTextBounds = null;
    private String mTextChars = null;
    private int mWrapText = -1;

    public TimedText(Parcel paramParcel)
    {
        if (!parseParcel(paramParcel))
        {
            this.mKeyObjectMap.clear();
            throw new IllegalArgumentException("parseParcel() fails");
        }
    }

    private boolean containsKey(int paramInt)
    {
        if ((isValidKey(paramInt)) && (this.mKeyObjectMap.containsKey(Integer.valueOf(paramInt))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private Object getObject(int paramInt)
    {
        if (containsKey(paramInt))
            return this.mKeyObjectMap.get(Integer.valueOf(paramInt));
        throw new IllegalArgumentException("Invalid key: " + paramInt);
    }

    private boolean isValidKey(int paramInt)
    {
        int i = 1;
        if (((paramInt < i) || (paramInt > 16)) && ((paramInt < 101) || (paramInt > 107)))
            i = 0;
        return i;
    }

    private Set keySet()
    {
        return this.mKeyObjectMap.keySet();
    }

    private boolean parseParcel(Parcel paramParcel)
    {
        paramParcel.setDataPosition(0);
        boolean bool;
        if (paramParcel.dataAvail() == 0)
            bool = false;
        while (true)
        {
            return bool;
            int i = paramParcel.readInt();
            byte[] arrayOfByte;
            if (i == 102)
            {
                int i1 = paramParcel.readInt();
                if (i1 != 7)
                {
                    bool = false;
                }
                else
                {
                    int i2 = paramParcel.readInt();
                    this.mKeyObjectMap.put(Integer.valueOf(i1), Integer.valueOf(i2));
                    if (paramParcel.readInt() != 16)
                    {
                        bool = false;
                    }
                    else
                    {
                        paramParcel.readInt();
                        arrayOfByte = paramParcel.createByteArray();
                        if ((arrayOfByte == null) || (arrayOfByte.length == 0))
                            this.mTextChars = null;
                    }
                }
            }
            else
            {
                label603: 
                while (true)
                {
                    if (paramParcel.dataAvail() <= 0)
                        break label605;
                    int j = paramParcel.readInt();
                    if (!isValidKey(j))
                    {
                        Log.w("TimedText", "Invalid timed text key found: " + j);
                        bool = false;
                        break;
                        this.mTextChars = new String(arrayOfByte);
                        continue;
                        if (i == 101)
                            continue;
                        Log.w("TimedText", "Invalid timed text key found: " + i);
                        bool = false;
                        break;
                    }
                    Object localObject = null;
                    switch (j)
                    {
                    case 2:
                    case 7:
                    default:
                    case 13:
                    case 9:
                    case 10:
                    case 12:
                    case 11:
                    case 8:
                    case 6:
                    case 4:
                    case 1:
                    case 15:
                    case 3:
                    case 14:
                    case 5:
                    }
                    while (true)
                    {
                        if (localObject == null)
                            break label603;
                        if (this.mKeyObjectMap.containsKey(Integer.valueOf(j)))
                            this.mKeyObjectMap.remove(Integer.valueOf(j));
                        this.mKeyObjectMap.put(Integer.valueOf(j), localObject);
                        break;
                        readStyle(paramParcel);
                        localObject = this.mStyleList;
                        continue;
                        readFont(paramParcel);
                        localObject = this.mFontList;
                        continue;
                        readHighlight(paramParcel);
                        localObject = this.mHighlightPosList;
                        continue;
                        readKaraoke(paramParcel);
                        localObject = this.mKaraokeList;
                        continue;
                        readHyperText(paramParcel);
                        localObject = this.mHyperTextList;
                        continue;
                        readBlinkingText(paramParcel);
                        localObject = this.mBlinkingPosList;
                        continue;
                        this.mWrapText = paramParcel.readInt();
                        localObject = Integer.valueOf(this.mWrapText);
                        continue;
                        this.mHighlightColorRGBA = paramParcel.readInt();
                        localObject = Integer.valueOf(this.mHighlightColorRGBA);
                        continue;
                        this.mDisplayFlags = paramParcel.readInt();
                        localObject = Integer.valueOf(this.mDisplayFlags);
                        continue;
                        this.mJustification = new Justification(paramParcel.readInt(), paramParcel.readInt());
                        localObject = this.mJustification;
                        continue;
                        this.mBackgroundColorRGBA = paramParcel.readInt();
                        localObject = Integer.valueOf(this.mBackgroundColorRGBA);
                        continue;
                        int k = paramParcel.readInt();
                        int m = paramParcel.readInt();
                        int n = paramParcel.readInt();
                        this.mTextBounds = new Rect(m, k, paramParcel.readInt(), n);
                        continue;
                        this.mScrollDelay = paramParcel.readInt();
                        localObject = Integer.valueOf(this.mScrollDelay);
                    }
                }
                label605: bool = true;
            }
        }
    }

    private void readBlinkingText(Parcel paramParcel)
    {
        CharPos localCharPos = new CharPos(paramParcel.readInt(), paramParcel.readInt());
        if (this.mBlinkingPosList == null)
            this.mBlinkingPosList = new ArrayList();
        this.mBlinkingPosList.add(localCharPos);
    }

    private void readFont(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        for (int j = 0; j < i; j++)
        {
            int k = paramParcel.readInt();
            int m = paramParcel.readInt();
            Font localFont = new Font(k, new String(paramParcel.createByteArray(), 0, m));
            if (this.mFontList == null)
                this.mFontList = new ArrayList();
            this.mFontList.add(localFont);
        }
    }

    private void readHighlight(Parcel paramParcel)
    {
        CharPos localCharPos = new CharPos(paramParcel.readInt(), paramParcel.readInt());
        if (this.mHighlightPosList == null)
            this.mHighlightPosList = new ArrayList();
        this.mHighlightPosList.add(localCharPos);
    }

    private void readHyperText(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        int j = paramParcel.readInt();
        int k = paramParcel.readInt();
        String str = new String(paramParcel.createByteArray(), 0, k);
        int m = paramParcel.readInt();
        HyperText localHyperText = new HyperText(i, j, str, new String(paramParcel.createByteArray(), 0, m));
        if (this.mHyperTextList == null)
            this.mHyperTextList = new ArrayList();
        this.mHyperTextList.add(localHyperText);
    }

    private void readKaraoke(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        for (int j = 0; j < i; j++)
        {
            Karaoke localKaraoke = new Karaoke(paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt());
            if (this.mKaraokeList == null)
                this.mKaraokeList = new ArrayList();
            this.mKaraokeList.add(localKaraoke);
        }
    }

    private void readStyle(Parcel paramParcel)
    {
        int i = 0;
        int j = -1;
        int k = -1;
        int m = -1;
        boolean bool1 = false;
        boolean bool2 = false;
        boolean bool3 = false;
        int n = -1;
        int i1 = -1;
        while ((i == 0) && (paramParcel.dataAvail() > 0))
            switch (paramParcel.readInt())
            {
            default:
                paramParcel.setDataPosition(-4 + paramParcel.dataPosition());
                i = 1;
                break;
            case 103:
                j = paramParcel.readInt();
                break;
            case 104:
                k = paramParcel.readInt();
                break;
            case 105:
                m = paramParcel.readInt();
                break;
            case 2:
                int i2 = paramParcel.readInt();
                if (i2 % 2 == 1)
                {
                    bool1 = true;
                    if (i2 % 4 < 2)
                        break label194;
                    bool2 = true;
                    if (i2 / 4 != 1)
                        break label200;
                }
                for (bool3 = true; ; bool3 = false)
                {
                    break;
                    bool1 = false;
                    break label163;
                    bool2 = false;
                    break label174;
                }
            case 106:
                n = paramParcel.readInt();
                break;
            case 107:
                label163: label174: label194: label200: i1 = paramParcel.readInt();
            }
        Style localStyle = new Style(j, k, m, bool1, bool2, bool3, n, i1);
        if (this.mStyleList == null)
            this.mStyleList = new ArrayList();
        this.mStyleList.add(localStyle);
    }

    public Rect getBounds()
    {
        return this.mTextBounds;
    }

    public String getText()
    {
        return this.mTextChars;
    }

    public static final class HyperText
    {
        public final String URL;
        public final String altString;
        public final int endChar;
        public final int startChar;

        public HyperText(int paramInt1, int paramInt2, String paramString1, String paramString2)
        {
            this.startChar = paramInt1;
            this.endChar = paramInt2;
            this.URL = paramString1;
            this.altString = paramString2;
        }
    }

    public static final class Karaoke
    {
        public final int endChar;
        public final int endTimeMs;
        public final int startChar;
        public final int startTimeMs;

        public Karaoke(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.startTimeMs = paramInt1;
            this.endTimeMs = paramInt2;
            this.startChar = paramInt3;
            this.endChar = paramInt4;
        }
    }

    public static final class Font
    {
        public final int ID;
        public final String name;

        public Font(int paramInt, String paramString)
        {
            this.ID = paramInt;
            this.name = paramString;
        }
    }

    public static final class Style
    {
        public final int colorRGBA;
        public final int endChar;
        public final int fontID;
        public final int fontSize;
        public final boolean isBold;
        public final boolean isItalic;
        public final boolean isUnderlined;
        public final int startChar;

        public Style(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, int paramInt4, int paramInt5)
        {
            this.startChar = paramInt1;
            this.endChar = paramInt2;
            this.fontID = paramInt3;
            this.isBold = paramBoolean1;
            this.isItalic = paramBoolean2;
            this.isUnderlined = paramBoolean3;
            this.fontSize = paramInt4;
            this.colorRGBA = paramInt5;
        }
    }

    public static final class Justification
    {
        public final int horizontalJustification;
        public final int verticalJustification;

        public Justification(int paramInt1, int paramInt2)
        {
            this.horizontalJustification = paramInt1;
            this.verticalJustification = paramInt2;
        }
    }

    public static final class CharPos
    {
        public final int endChar;
        public final int startChar;

        public CharPos(int paramInt1, int paramInt2)
        {
            this.startChar = paramInt1;
            this.endChar = paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.TimedText
 * JD-Core Version:        0.6.2
 */