package android.media;

import android.content.Context;
import android.net.Uri;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.Log;
import java.util.LinkedList;

public class AsyncPlayer
{
    private static final int PLAY = 1;
    private static final int STOP = 2;
    private static final boolean mDebug;
    private final LinkedList<Command> mCmdQueue = new LinkedList();
    private MediaPlayer mPlayer;
    private int mState = 2;
    private String mTag;
    private Thread mThread;
    private PowerManager.WakeLock mWakeLock;

    public AsyncPlayer(String paramString)
    {
        if (paramString != null);
        for (this.mTag = paramString; ; this.mTag = "AsyncPlayer")
            return;
    }

    private void acquireWakeLock()
    {
        if (this.mWakeLock != null)
            this.mWakeLock.acquire();
    }

    private void enqueueLocked(Command paramCommand)
    {
        this.mCmdQueue.add(paramCommand);
        if (this.mThread == null)
        {
            acquireWakeLock();
            this.mThread = new Thread();
            this.mThread.start();
        }
    }

    private void releaseWakeLock()
    {
        if (this.mWakeLock != null)
            this.mWakeLock.release();
    }

    private void startSound(Command paramCommand)
    {
        try
        {
            MediaPlayer localMediaPlayer = new MediaPlayer();
            localMediaPlayer.setAudioStreamType(paramCommand.stream);
            localMediaPlayer.setDataSource(paramCommand.context, paramCommand.uri);
            localMediaPlayer.setLooping(paramCommand.looping);
            localMediaPlayer.prepare();
            localMediaPlayer.start();
            if (this.mPlayer != null)
                this.mPlayer.release();
            this.mPlayer = localMediaPlayer;
            long l = SystemClock.uptimeMillis() - paramCommand.requestTime;
            if (l > 1000L)
                Log.w(this.mTag, "Notification sound delayed by " + l + "msecs");
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Log.w(this.mTag, "error loading sound for " + paramCommand.uri, localException);
        }
    }

    public void play(Context paramContext, Uri paramUri, boolean paramBoolean, int paramInt)
    {
        Command localCommand = new Command(null);
        localCommand.requestTime = SystemClock.uptimeMillis();
        localCommand.code = 1;
        localCommand.context = paramContext;
        localCommand.uri = paramUri;
        localCommand.looping = paramBoolean;
        localCommand.stream = paramInt;
        synchronized (this.mCmdQueue)
        {
            enqueueLocked(localCommand);
            this.mState = 1;
            return;
        }
    }

    public void setUsesWakeLock(Context paramContext)
    {
        if ((this.mWakeLock != null) || (this.mThread != null))
            throw new RuntimeException("assertion failed mWakeLock=" + this.mWakeLock + " mThread=" + this.mThread);
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, this.mTag);
    }

    public void stop()
    {
        synchronized (this.mCmdQueue)
        {
            if (this.mState != 2)
            {
                Command localCommand = new Command(null);
                localCommand.requestTime = SystemClock.uptimeMillis();
                localCommand.code = 2;
                enqueueLocked(localCommand);
                this.mState = 2;
            }
            return;
        }
    }

    private final class Thread extends Thread
    {
        Thread()
        {
            super();
        }

        public void run()
        {
            while (true)
            {
                AsyncPlayer.Command localCommand;
                synchronized (AsyncPlayer.this.mCmdQueue)
                {
                    localCommand = (AsyncPlayer.Command)AsyncPlayer.this.mCmdQueue.removeFirst();
                    switch (localCommand.code)
                    {
                    default:
                    case 1:
                    case 2:
                    }
                }
                synchronized (AsyncPlayer.this.mCmdQueue)
                {
                    while (AsyncPlayer.this.mCmdQueue.size() == 0)
                    {
                        AsyncPlayer.access$402(AsyncPlayer.this, null);
                        AsyncPlayer.this.releaseWakeLock();
                        return;
                        localObject1 = finally;
                        throw localObject1;
                        AsyncPlayer.this.startSound(localCommand);
                        continue;
                        if (AsyncPlayer.this.mPlayer != null)
                        {
                            long l = SystemClock.uptimeMillis() - localCommand.requestTime;
                            if (l > 1000L)
                                Log.w(AsyncPlayer.this.mTag, "Notification stop delayed by " + l + "msecs");
                            AsyncPlayer.this.mPlayer.stop();
                            AsyncPlayer.this.mPlayer.release();
                            AsyncPlayer.access$302(AsyncPlayer.this, null);
                        }
                        else
                        {
                            Log.w(AsyncPlayer.this.mTag, "STOP command without a player");
                        }
                    }
                }
            }
        }
    }

    private static final class Command
    {
        int code;
        Context context;
        boolean looping;
        long requestTime;
        int stream;
        Uri uri;

        public String toString()
        {
            return "{ code=" + this.code + " looping=" + this.looping + " stream=" + this.stream + " uri=" + this.uri + " }";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.AsyncPlayer
 * JD-Core Version:        0.6.2
 */