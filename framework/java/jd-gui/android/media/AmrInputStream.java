package android.media;

import java.io.IOException;
import java.io.InputStream;

public final class AmrInputStream extends InputStream
{
    private static final int SAMPLES_PER_FRAME = 160;
    private static final String TAG = "AmrInputStream";
    private final byte[] mBuf = new byte[320];
    private int mBufIn = 0;
    private int mBufOut = 0;
    private int mGae;
    private InputStream mInputStream;
    private byte[] mOneByte = new byte[1];

    static
    {
        System.loadLibrary("media_jni");
    }

    public AmrInputStream(InputStream paramInputStream)
    {
        this.mInputStream = paramInputStream;
        this.mGae = GsmAmrEncoderNew();
        GsmAmrEncoderInitialize(this.mGae);
    }

    private static native void GsmAmrEncoderCleanup(int paramInt);

    private static native void GsmAmrEncoderDelete(int paramInt);

    private static native int GsmAmrEncoderEncode(int paramInt1, byte[] paramArrayOfByte1, int paramInt2, byte[] paramArrayOfByte2, int paramInt3)
        throws IOException;

    private static native void GsmAmrEncoderInitialize(int paramInt);

    private static native int GsmAmrEncoderNew();

    // ERROR //
    public void close()
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 43	android/media/AmrInputStream:mInputStream	Ljava/io/InputStream;
        //     4: ifnull +10 -> 14
        //     7: aload_0
        //     8: getfield 43	android/media/AmrInputStream:mInputStream	Ljava/io/InputStream;
        //     11: invokevirtual 62	java/io/InputStream:close	()V
        //     14: aload_0
        //     15: aconst_null
        //     16: putfield 43	android/media/AmrInputStream:mInputStream	Ljava/io/InputStream;
        //     19: aload_0
        //     20: getfield 49	android/media/AmrInputStream:mGae	I
        //     23: ifeq +10 -> 33
        //     26: aload_0
        //     27: getfield 49	android/media/AmrInputStream:mGae	I
        //     30: invokestatic 64	android/media/AmrInputStream:GsmAmrEncoderCleanup	(I)V
        //     33: aload_0
        //     34: getfield 49	android/media/AmrInputStream:mGae	I
        //     37: ifeq +10 -> 47
        //     40: aload_0
        //     41: getfield 49	android/media/AmrInputStream:mGae	I
        //     44: invokestatic 66	android/media/AmrInputStream:GsmAmrEncoderDelete	(I)V
        //     47: aload_0
        //     48: iconst_0
        //     49: putfield 49	android/media/AmrInputStream:mGae	I
        //     52: return
        //     53: astore_1
        //     54: aload_0
        //     55: aconst_null
        //     56: putfield 43	android/media/AmrInputStream:mInputStream	Ljava/io/InputStream;
        //     59: aload_0
        //     60: getfield 49	android/media/AmrInputStream:mGae	I
        //     63: ifeq +10 -> 73
        //     66: aload_0
        //     67: getfield 49	android/media/AmrInputStream:mGae	I
        //     70: invokestatic 64	android/media/AmrInputStream:GsmAmrEncoderCleanup	(I)V
        //     73: aload_0
        //     74: getfield 49	android/media/AmrInputStream:mGae	I
        //     77: ifeq +10 -> 87
        //     80: aload_0
        //     81: getfield 49	android/media/AmrInputStream:mGae	I
        //     84: invokestatic 66	android/media/AmrInputStream:GsmAmrEncoderDelete	(I)V
        //     87: aload_0
        //     88: iconst_0
        //     89: putfield 49	android/media/AmrInputStream:mGae	I
        //     92: aload_1
        //     93: athrow
        //     94: astore_2
        //     95: aload_0
        //     96: getfield 49	android/media/AmrInputStream:mGae	I
        //     99: ifeq +10 -> 109
        //     102: aload_0
        //     103: getfield 49	android/media/AmrInputStream:mGae	I
        //     106: invokestatic 66	android/media/AmrInputStream:GsmAmrEncoderDelete	(I)V
        //     109: aload_0
        //     110: iconst_0
        //     111: putfield 49	android/media/AmrInputStream:mGae	I
        //     114: aload_2
        //     115: athrow
        //     116: astore_3
        //     117: aload_0
        //     118: iconst_0
        //     119: putfield 49	android/media/AmrInputStream:mGae	I
        //     122: aload_3
        //     123: athrow
        //     124: astore 4
        //     126: aload_0
        //     127: iconst_0
        //     128: putfield 49	android/media/AmrInputStream:mGae	I
        //     131: aload 4
        //     133: athrow
        //     134: astore 5
        //     136: aload_0
        //     137: getfield 49	android/media/AmrInputStream:mGae	I
        //     140: ifeq +10 -> 150
        //     143: aload_0
        //     144: getfield 49	android/media/AmrInputStream:mGae	I
        //     147: invokestatic 66	android/media/AmrInputStream:GsmAmrEncoderDelete	(I)V
        //     150: aload_0
        //     151: iconst_0
        //     152: putfield 49	android/media/AmrInputStream:mGae	I
        //     155: aload 5
        //     157: athrow
        //     158: astore 6
        //     160: aload_0
        //     161: iconst_0
        //     162: putfield 49	android/media/AmrInputStream:mGae	I
        //     165: aload 6
        //     167: athrow
        //     168: astore 7
        //     170: aload_0
        //     171: iconst_0
        //     172: putfield 49	android/media/AmrInputStream:mGae	I
        //     175: aload 7
        //     177: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     0	14	53	finally
        //     59	73	94	finally
        //     95	109	116	finally
        //     73	87	124	finally
        //     19	33	134	finally
        //     136	150	158	finally
        //     33	47	168	finally
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mGae != 0)
        {
            close();
            throw new IllegalStateException("someone forgot to close AmrInputStream");
        }
    }

    public int read()
        throws IOException
    {
        if (read(this.mOneByte, 0, 1) == 1);
        for (int i = 0xFF & this.mOneByte[0]; ; i = -1)
            return i;
    }

    public int read(byte[] paramArrayOfByte)
        throws IOException
    {
        return read(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        int i = -1;
        if (this.mGae == 0)
            throw new IllegalStateException("not open");
        int j;
        int k;
        if (this.mBufOut >= this.mBufIn)
        {
            this.mBufOut = 0;
            this.mBufIn = 0;
            j = 0;
            if (j < 320)
            {
                k = this.mInputStream.read(this.mBuf, j, 320 - j);
                if (k != i);
            }
        }
        while (true)
        {
            return i;
            j += k;
            break;
            this.mBufIn = GsmAmrEncoderEncode(this.mGae, this.mBuf, 0, this.mBuf, 0);
            if (paramInt2 > this.mBufIn - this.mBufOut)
                paramInt2 = this.mBufIn - this.mBufOut;
            System.arraycopy(this.mBuf, this.mBufOut, paramArrayOfByte, paramInt1, paramInt2);
            this.mBufOut = (paramInt2 + this.mBufOut);
            i = paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.AmrInputStream
 * JD-Core Version:        0.6.2
 */