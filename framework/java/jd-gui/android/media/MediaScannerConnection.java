package android.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;

public class MediaScannerConnection
    implements ServiceConnection
{
    private static final String TAG = "MediaScannerConnection";
    private MediaScannerConnectionClient mClient;
    private boolean mConnected;
    private Context mContext;
    private final IMediaScannerListener.Stub mListener = new IMediaScannerListener.Stub()
    {
        public void scanCompleted(String paramAnonymousString, Uri paramAnonymousUri)
        {
            MediaScannerConnection.MediaScannerConnectionClient localMediaScannerConnectionClient = MediaScannerConnection.this.mClient;
            if (localMediaScannerConnectionClient != null)
                localMediaScannerConnectionClient.onScanCompleted(paramAnonymousString, paramAnonymousUri);
        }
    };
    private IMediaScannerService mService;

    public MediaScannerConnection(Context paramContext, MediaScannerConnectionClient paramMediaScannerConnectionClient)
    {
        this.mContext = paramContext;
        this.mClient = paramMediaScannerConnectionClient;
    }

    public static void scanFile(Context paramContext, String[] paramArrayOfString1, String[] paramArrayOfString2, OnScanCompletedListener paramOnScanCompletedListener)
    {
        ClientProxy localClientProxy = new ClientProxy(paramArrayOfString1, paramArrayOfString2, paramOnScanCompletedListener);
        MediaScannerConnection localMediaScannerConnection = new MediaScannerConnection(paramContext, localClientProxy);
        localClientProxy.mConnection = localMediaScannerConnection;
        localMediaScannerConnection.connect();
    }

    public void connect()
    {
        try
        {
            if (!this.mConnected)
            {
                Intent localIntent = new Intent(IMediaScannerService.class.getName());
                this.mContext.bindService(localIntent, this, 1);
                this.mConnected = true;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public void disconnect()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 63	android/media/MediaScannerConnection:mConnected	Z
        //     6: istore_2
        //     7: iload_2
        //     8: ifeq +16 -> 24
        //     11: aload_0
        //     12: getfield 43	android/media/MediaScannerConnection:mContext	Landroid/content/Context;
        //     15: aload_0
        //     16: invokevirtual 89	android/content/Context:unbindService	(Landroid/content/ServiceConnection;)V
        //     19: aload_0
        //     20: iconst_0
        //     21: putfield 63	android/media/MediaScannerConnection:mConnected	Z
        //     24: aload_0
        //     25: monitorexit
        //     26: return
        //     27: astore_1
        //     28: aload_0
        //     29: monitorexit
        //     30: aload_1
        //     31: athrow
        //     32: astore_3
        //     33: goto -14 -> 19
        //
        // Exception table:
        //     from	to	target	type
        //     2	7	27	finally
        //     11	19	27	finally
        //     19	30	27	finally
        //     11	19	32	java/lang/IllegalArgumentException
    }

    /** @deprecated */
    public boolean isConnected()
    {
        try
        {
            if (this.mService != null)
            {
                boolean bool2 = this.mConnected;
                if (bool2)
                {
                    bool1 = true;
                    return bool1;
                }
            }
            boolean bool1 = false;
        }
        finally
        {
        }
    }

    public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
    {
        try
        {
            this.mService = IMediaScannerService.Stub.asInterface(paramIBinder);
            if ((this.mService != null) && (this.mClient != null))
                this.mClient.onMediaScannerConnected();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void onServiceDisconnected(ComponentName paramComponentName)
    {
        try
        {
            this.mService = null;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public void scanFile(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 93	android/media/MediaScannerConnection:mService	Landroid/media/IMediaScannerService;
        //     6: ifnull +10 -> 16
        //     9: aload_0
        //     10: getfield 63	android/media/MediaScannerConnection:mConnected	Z
        //     13: ifne +18 -> 31
        //     16: new 111	java/lang/IllegalStateException
        //     19: dup
        //     20: ldc 113
        //     22: invokespecial 114	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     25: athrow
        //     26: astore_3
        //     27: aload_0
        //     28: monitorexit
        //     29: aload_3
        //     30: athrow
        //     31: aload_0
        //     32: getfield 93	android/media/MediaScannerConnection:mService	Landroid/media/IMediaScannerService;
        //     35: aload_1
        //     36: aload_2
        //     37: aload_0
        //     38: getfield 41	android/media/MediaScannerConnection:mListener	Landroid/media/IMediaScannerListener$Stub;
        //     41: invokeinterface 118 4 0
        //     46: aload_0
        //     47: monitorexit
        //     48: return
        //     49: astore 4
        //     51: goto -5 -> 46
        //
        // Exception table:
        //     from	to	target	type
        //     2	29	26	finally
        //     31	46	26	finally
        //     46	48	26	finally
        //     31	46	49	android/os/RemoteException
    }

    static class ClientProxy
        implements MediaScannerConnection.MediaScannerConnectionClient
    {
        final MediaScannerConnection.OnScanCompletedListener mClient;
        MediaScannerConnection mConnection;
        final String[] mMimeTypes;
        int mNextPath;
        final String[] mPaths;

        ClientProxy(String[] paramArrayOfString1, String[] paramArrayOfString2, MediaScannerConnection.OnScanCompletedListener paramOnScanCompletedListener)
        {
            this.mPaths = paramArrayOfString1;
            this.mMimeTypes = paramArrayOfString2;
            this.mClient = paramOnScanCompletedListener;
        }

        public void onMediaScannerConnected()
        {
            scanNextPath();
        }

        public void onScanCompleted(String paramString, Uri paramUri)
        {
            if (this.mClient != null)
                this.mClient.onScanCompleted(paramString, paramUri);
            scanNextPath();
        }

        void scanNextPath()
        {
            if (this.mNextPath >= this.mPaths.length)
            {
                this.mConnection.disconnect();
                return;
            }
            if (this.mMimeTypes != null);
            for (String str = this.mMimeTypes[this.mNextPath]; ; str = null)
            {
                this.mConnection.scanFile(this.mPaths[this.mNextPath], str);
                this.mNextPath = (1 + this.mNextPath);
                break;
            }
        }
    }

    public static abstract interface MediaScannerConnectionClient extends MediaScannerConnection.OnScanCompletedListener
    {
        public abstract void onMediaScannerConnected();

        public abstract void onScanCompleted(String paramString, Uri paramUri);
    }

    public static abstract interface OnScanCompletedListener
    {
        public abstract void onScanCompleted(String paramString, Uri paramUri);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.media.MediaScannerConnection
 * JD-Core Version:        0.6.2
 */