package android.renderscript;

public class RSIllegalArgumentException extends RSRuntimeException
{
    public RSIllegalArgumentException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.RSIllegalArgumentException
 * JD-Core Version:        0.6.2
 */