package android.renderscript;

public class Short3
{
    public short x;
    public short y;
    public short z;

    public Short3()
    {
    }

    public Short3(short paramShort1, short paramShort2, short paramShort3)
    {
        this.x = paramShort1;
        this.y = paramShort2;
        this.z = paramShort3;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Short3
 * JD-Core Version:        0.6.2
 */