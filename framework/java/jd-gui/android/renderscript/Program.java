package android.renderscript;

public class Program extends BaseObj
{
    static final int MAX_CONSTANT = 8;
    static final int MAX_INPUT = 8;
    static final int MAX_OUTPUT = 8;
    static final int MAX_TEXTURE = 8;
    Type[] mConstants;
    Element[] mInputs;
    Element[] mOutputs;
    String mShader;
    int mTextureCount;
    String[] mTextureNames;
    TextureType[] mTextures;

    Program(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    public void bindConstants(Allocation paramAllocation, int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mConstants.length))
            throw new IllegalArgumentException("Slot ID out of range.");
        if ((paramAllocation != null) && (paramAllocation.getType().getID(this.mRS) != this.mConstants[paramInt].getID(this.mRS)))
            throw new IllegalArgumentException("Allocation type does not match slot type.");
        if (paramAllocation != null);
        for (int i = paramAllocation.getID(this.mRS); ; i = 0)
        {
            this.mRS.nProgramBindConstants(getID(this.mRS), paramInt, i);
            return;
        }
    }

    public void bindSampler(Sampler paramSampler, int paramInt)
        throws IllegalArgumentException
    {
        this.mRS.validate();
        if ((paramInt < 0) || (paramInt >= this.mTextureCount))
            throw new IllegalArgumentException("Slot ID out of range.");
        if (paramSampler != null);
        for (int i = paramSampler.getID(this.mRS); ; i = 0)
        {
            this.mRS.nProgramBindSampler(getID(this.mRS), paramInt, i);
            return;
        }
    }

    public void bindTexture(Allocation paramAllocation, int paramInt)
        throws IllegalArgumentException
    {
        this.mRS.validate();
        if ((paramInt < 0) || (paramInt >= this.mTextureCount))
            throw new IllegalArgumentException("Slot ID out of range.");
        if ((paramAllocation != null) && (paramAllocation.getType().hasFaces()) && (this.mTextures[paramInt] != TextureType.TEXTURE_CUBE))
            throw new IllegalArgumentException("Cannot bind cubemap to 2d texture slot");
        if (paramAllocation != null);
        for (int i = paramAllocation.getID(this.mRS); ; i = 0)
        {
            this.mRS.nProgramBindTexture(getID(this.mRS), paramInt, i);
            return;
        }
    }

    public Type getConstant(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mConstants.length))
            throw new IllegalArgumentException("Slot ID out of range.");
        return this.mConstants[paramInt];
    }

    public int getConstantCount()
    {
        if (this.mConstants != null);
        for (int i = this.mConstants.length; ; i = 0)
            return i;
    }

    public int getTextureCount()
    {
        return this.mTextureCount;
    }

    public String getTextureName(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mTextureCount))
            throw new IllegalArgumentException("Slot ID out of range.");
        return this.mTextureNames[paramInt];
    }

    public TextureType getTextureType(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mTextureCount))
            throw new IllegalArgumentException("Slot ID out of range.");
        return this.mTextures[paramInt];
    }

    public static class BaseProgramBuilder
    {
        int mConstantCount;
        Type[] mConstants;
        int mInputCount;
        Element[] mInputs;
        int mOutputCount;
        Element[] mOutputs;
        RenderScript mRS;
        String mShader;
        int mTextureCount;
        String[] mTextureNames;
        Program.TextureType[] mTextureTypes;
        Type[] mTextures;

        protected BaseProgramBuilder(RenderScript paramRenderScript)
        {
            this.mRS = paramRenderScript;
            this.mInputs = new Element[8];
            this.mOutputs = new Element[8];
            this.mConstants = new Type[8];
            this.mInputCount = 0;
            this.mOutputCount = 0;
            this.mConstantCount = 0;
            this.mTextureCount = 0;
            this.mTextureTypes = new Program.TextureType[8];
            this.mTextureNames = new String[8];
        }

        public BaseProgramBuilder addConstant(Type paramType)
            throws IllegalStateException
        {
            if (this.mConstantCount >= 8)
                throw new RSIllegalArgumentException("Max input count exceeded.");
            if (paramType.getElement().isComplex())
                throw new RSIllegalArgumentException("Complex elements not allowed.");
            this.mConstants[this.mConstantCount] = paramType;
            this.mConstantCount = (1 + this.mConstantCount);
            return this;
        }

        public BaseProgramBuilder addTexture(Program.TextureType paramTextureType)
            throws IllegalArgumentException
        {
            addTexture(paramTextureType, "Tex" + this.mTextureCount);
            return this;
        }

        public BaseProgramBuilder addTexture(Program.TextureType paramTextureType, String paramString)
            throws IllegalArgumentException
        {
            if (this.mTextureCount >= 8)
                throw new IllegalArgumentException("Max texture count exceeded.");
            this.mTextureTypes[this.mTextureCount] = paramTextureType;
            this.mTextureNames[this.mTextureCount] = paramString;
            this.mTextureCount = (1 + this.mTextureCount);
            return this;
        }

        public int getCurrentConstantIndex()
        {
            return -1 + this.mConstantCount;
        }

        public int getCurrentTextureIndex()
        {
            return -1 + this.mTextureCount;
        }

        protected void initProgram(Program paramProgram)
        {
            paramProgram.mInputs = new Element[this.mInputCount];
            System.arraycopy(this.mInputs, 0, paramProgram.mInputs, 0, this.mInputCount);
            paramProgram.mOutputs = new Element[this.mOutputCount];
            System.arraycopy(this.mOutputs, 0, paramProgram.mOutputs, 0, this.mOutputCount);
            paramProgram.mConstants = new Type[this.mConstantCount];
            System.arraycopy(this.mConstants, 0, paramProgram.mConstants, 0, this.mConstantCount);
            paramProgram.mTextureCount = this.mTextureCount;
            paramProgram.mTextures = new Program.TextureType[this.mTextureCount];
            System.arraycopy(this.mTextureTypes, 0, paramProgram.mTextures, 0, this.mTextureCount);
            paramProgram.mTextureNames = new String[this.mTextureCount];
            System.arraycopy(this.mTextureNames, 0, paramProgram.mTextureNames, 0, this.mTextureCount);
        }

        // ERROR //
        public BaseProgramBuilder setShader(android.content.res.Resources paramResources, int paramInt)
        {
            // Byte code:
            //     0: aload_1
            //     1: iload_2
            //     2: invokevirtual 136	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
            //     5: astore_3
            //     6: sipush 1024
            //     9: newarray byte
            //     11: astore 6
            //     13: iconst_0
            //     14: istore 7
            //     16: aload 6
            //     18: arraylength
            //     19: iload 7
            //     21: isub
            //     22: istore 8
            //     24: iload 8
            //     26: ifne +36 -> 62
            //     29: iconst_2
            //     30: aload 6
            //     32: arraylength
            //     33: imul
            //     34: newarray byte
            //     36: astore 12
            //     38: aload 6
            //     40: iconst_0
            //     41: aload 12
            //     43: iconst_0
            //     44: aload 6
            //     46: arraylength
            //     47: invokestatic 118	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
            //     50: aload 12
            //     52: astore 6
            //     54: aload 6
            //     56: arraylength
            //     57: iload 7
            //     59: isub
            //     60: istore 8
            //     62: aload_3
            //     63: aload 6
            //     65: iload 7
            //     67: iload 8
            //     69: invokevirtual 142	java/io/InputStream:read	([BII)I
            //     72: istore 9
            //     74: iload 9
            //     76: ifgt +27 -> 103
            //     79: aload_3
            //     80: invokevirtual 145	java/io/InputStream:close	()V
            //     83: aload_0
            //     84: new 57	java/lang/String
            //     87: dup
            //     88: aload 6
            //     90: iconst_0
            //     91: iload 7
            //     93: ldc 147
            //     95: invokespecial 150	java/lang/String:<init>	([BIILjava/lang/String;)V
            //     98: putfield 152	android/renderscript/Program$BaseProgramBuilder:mShader	Ljava/lang/String;
            //     101: aload_0
            //     102: areturn
            //     103: iload 7
            //     105: iload 9
            //     107: iadd
            //     108: istore 7
            //     110: goto -94 -> 16
            //     113: astore 4
            //     115: aload_3
            //     116: invokevirtual 145	java/io/InputStream:close	()V
            //     119: aload 4
            //     121: athrow
            //     122: astore 5
            //     124: new 154	android/content/res/Resources$NotFoundException
            //     127: dup
            //     128: invokespecial 155	android/content/res/Resources$NotFoundException:<init>	()V
            //     131: athrow
            //     132: astore 10
            //     134: ldc 157
            //     136: ldc 159
            //     138: invokestatic 165	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     141: pop
            //     142: goto -41 -> 101
            //
            // Exception table:
            //     from	to	target	type
            //     6	74	113	finally
            //     79	83	122	java/io/IOException
            //     115	122	122	java/io/IOException
            //     83	101	132	java/io/UnsupportedEncodingException
        }

        public BaseProgramBuilder setShader(String paramString)
        {
            this.mShader = paramString;
            return this;
        }
    }

    static enum ProgramParam
    {
        int mID;

        static
        {
            CONSTANT = new ProgramParam("CONSTANT", 2, 2);
            TEXTURE_TYPE = new ProgramParam("TEXTURE_TYPE", 3, 3);
            ProgramParam[] arrayOfProgramParam = new ProgramParam[4];
            arrayOfProgramParam[0] = INPUT;
            arrayOfProgramParam[1] = OUTPUT;
            arrayOfProgramParam[2] = CONSTANT;
            arrayOfProgramParam[3] = TEXTURE_TYPE;
        }

        private ProgramParam(int paramInt)
        {
            this.mID = paramInt;
        }
    }

    public static enum TextureType
    {
        int mID;

        static
        {
            TextureType[] arrayOfTextureType = new TextureType[2];
            arrayOfTextureType[0] = TEXTURE_2D;
            arrayOfTextureType[1] = TEXTURE_CUBE;
        }

        private TextureType(int paramInt)
        {
            this.mID = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Program
 * JD-Core Version:        0.6.2
 */