package android.renderscript;

public class Long3
{
    public long x;
    public long y;
    public long z;

    public Long3()
    {
    }

    public Long3(long paramLong1, long paramLong2, long paramLong3)
    {
        this.x = paramLong1;
        this.y = paramLong2;
        this.z = paramLong3;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Long3
 * JD-Core Version:        0.6.2
 */