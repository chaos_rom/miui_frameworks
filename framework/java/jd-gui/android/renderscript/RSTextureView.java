package android.renderscript;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.util.AttributeSet;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;

public class RSTextureView extends TextureView
    implements TextureView.SurfaceTextureListener
{
    private RenderScriptGL mRS;
    private SurfaceTexture mSurfaceTexture;

    public RSTextureView(Context paramContext)
    {
        super(paramContext);
        init();
    }

    public RSTextureView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        init();
    }

    private void init()
    {
        setSurfaceTextureListener(this);
    }

    public RenderScriptGL createRenderScriptGL(RenderScriptGL.SurfaceConfig paramSurfaceConfig)
    {
        RenderScriptGL localRenderScriptGL = new RenderScriptGL(getContext(), paramSurfaceConfig);
        setRenderScriptGL(localRenderScriptGL);
        if (this.mSurfaceTexture != null)
            this.mRS.setSurfaceTexture(this.mSurfaceTexture, getWidth(), getHeight());
        return localRenderScriptGL;
    }

    public void destroyRenderScriptGL()
    {
        this.mRS.destroy();
        this.mRS = null;
    }

    public RenderScriptGL getRenderScriptGL()
    {
        return this.mRS;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
    {
        this.mSurfaceTexture = paramSurfaceTexture;
        if (this.mRS != null)
            this.mRS.setSurfaceTexture(this.mSurfaceTexture, paramInt1, paramInt2);
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture)
    {
        this.mSurfaceTexture = paramSurfaceTexture;
        if (this.mRS != null)
            this.mRS.setSurfaceTexture(null, 0, 0);
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
    {
        this.mSurfaceTexture = paramSurfaceTexture;
        if (this.mRS != null)
            this.mRS.setSurfaceTexture(this.mSurfaceTexture, paramInt1, paramInt2);
    }

    public void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture)
    {
        this.mSurfaceTexture = paramSurfaceTexture;
    }

    public void pause()
    {
        if (this.mRS != null)
            this.mRS.pause();
    }

    public void resume()
    {
        if (this.mRS != null)
            this.mRS.resume();
    }

    public void setRenderScriptGL(RenderScriptGL paramRenderScriptGL)
    {
        this.mRS = paramRenderScriptGL;
        if (this.mSurfaceTexture != null)
            this.mRS.setSurfaceTexture(this.mSurfaceTexture, getWidth(), getHeight());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.RSTextureView
 * JD-Core Version:        0.6.2
 */