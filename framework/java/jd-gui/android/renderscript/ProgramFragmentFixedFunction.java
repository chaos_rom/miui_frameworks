package android.renderscript;

public class ProgramFragmentFixedFunction extends ProgramFragment
{
    ProgramFragmentFixedFunction(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    public static class Builder
    {
        public static final int MAX_TEXTURE = 2;
        int mNumTextures;
        boolean mPointSpriteEnable;
        RenderScript mRS;
        String mShader;
        Slot[] mSlots;
        boolean mVaryingColorEnable;

        public Builder(RenderScript paramRenderScript)
        {
            this.mRS = paramRenderScript;
            this.mSlots = new Slot[2];
            this.mPointSpriteEnable = false;
        }

        private void buildShaderString()
        {
            this.mShader = "//rs_shader_internal\n";
            this.mShader += "varying lowp vec4 varColor;\n";
            this.mShader += "varying vec2 varTex0;\n";
            this.mShader += "void main() {\n";
            label157: int i;
            if (this.mVaryingColorEnable)
            {
                this.mShader += "    lowp vec4 col = varColor;\n";
                if (this.mNumTextures != 0)
                {
                    if (!this.mPointSpriteEnable)
                        break label243;
                    this.mShader += "    vec2 t0 = gl_PointCoord;\n";
                }
                i = 0;
                label159: if (i >= this.mNumTextures)
                    break label632;
                switch (ProgramFragmentFixedFunction.1.$SwitchMap$android$renderscript$ProgramFragmentFixedFunction$Builder$EnvMode[this.mSlots[i].env.ordinal()])
                {
                default:
                case 1:
                case 2:
                case 3:
                }
            }
            while (true)
            {
                i++;
                break label159;
                this.mShader += "    lowp vec4 col = UNI_Color;\n";
                break;
                label243: this.mShader += "    vec2 t0 = varTex0.xy;\n";
                break label157;
                switch (ProgramFragmentFixedFunction.1.$SwitchMap$android$renderscript$ProgramFragmentFixedFunction$Builder$Format[this.mSlots[i].format.ordinal()])
                {
                default:
                    break;
                case 1:
                    this.mShader += "    col.a = texture2D(UNI_Tex0, t0).a;\n";
                    break;
                case 2:
                    this.mShader += "    col.rgba = texture2D(UNI_Tex0, t0).rgba;\n";
                    break;
                case 3:
                    this.mShader += "    col.rgb = texture2D(UNI_Tex0, t0).rgb;\n";
                    break;
                case 4:
                    this.mShader += "    col.rgba = texture2D(UNI_Tex0, t0).rgba;\n";
                    continue;
                    switch (ProgramFragmentFixedFunction.1.$SwitchMap$android$renderscript$ProgramFragmentFixedFunction$Builder$Format[this.mSlots[i].format.ordinal()])
                    {
                    default:
                        break;
                    case 1:
                        this.mShader += "    col.a *= texture2D(UNI_Tex0, t0).a;\n";
                        break;
                    case 2:
                        this.mShader += "    col.rgba *= texture2D(UNI_Tex0, t0).rgba;\n";
                        break;
                    case 3:
                        this.mShader += "    col.rgb *= texture2D(UNI_Tex0, t0).rgb;\n";
                        break;
                    case 4:
                        this.mShader += "    col.rgba *= texture2D(UNI_Tex0, t0).rgba;\n";
                        continue;
                        this.mShader += "    col = texture2D(UNI_Tex0, t0);\n";
                    }
                    break;
                }
            }
            label632: this.mShader += "    gl_FragColor = col;\n";
            this.mShader += "}\n";
        }

        public ProgramFragmentFixedFunction create()
        {
            ProgramFragmentFixedFunction.InternalBuilder localInternalBuilder = new ProgramFragmentFixedFunction.InternalBuilder(this.mRS);
            this.mNumTextures = 0;
            for (int i = 0; i < 2; i++)
                if (this.mSlots[i] != null)
                    this.mNumTextures = (1 + this.mNumTextures);
            buildShaderString();
            localInternalBuilder.setShader(this.mShader);
            Type localType = null;
            if (!this.mVaryingColorEnable)
            {
                Element.Builder localBuilder = new Element.Builder(this.mRS);
                localBuilder.add(Element.F32_4(this.mRS), "Color");
                Type.Builder localBuilder1 = new Type.Builder(this.mRS, localBuilder.create());
                localBuilder1.setX(1);
                localType = localBuilder1.create();
                localInternalBuilder.addConstant(localType);
            }
            for (int j = 0; j < this.mNumTextures; j++)
                localInternalBuilder.addTexture(Program.TextureType.TEXTURE_2D);
            ProgramFragmentFixedFunction localProgramFragmentFixedFunction = localInternalBuilder.create();
            localProgramFragmentFixedFunction.mTextureCount = 2;
            if (!this.mVaryingColorEnable)
            {
                Allocation localAllocation = Allocation.createTyped(this.mRS, localType);
                FieldPacker localFieldPacker = new FieldPacker(16);
                localFieldPacker.addF32(new Float4(1.0F, 1.0F, 1.0F, 1.0F));
                localAllocation.setFromFieldPacker(0, localFieldPacker);
                localProgramFragmentFixedFunction.bindConstants(localAllocation, 0);
            }
            return localProgramFragmentFixedFunction;
        }

        public Builder setPointSpriteTexCoordinateReplacement(boolean paramBoolean)
        {
            this.mPointSpriteEnable = paramBoolean;
            return this;
        }

        public Builder setTexture(EnvMode paramEnvMode, Format paramFormat, int paramInt)
            throws IllegalArgumentException
        {
            if ((paramInt < 0) || (paramInt >= 2))
                throw new IllegalArgumentException("MAX_TEXTURE exceeded.");
            this.mSlots[paramInt] = new Slot(paramEnvMode, paramFormat);
            return this;
        }

        public Builder setVaryingColor(boolean paramBoolean)
        {
            this.mVaryingColorEnable = paramBoolean;
            return this;
        }

        private class Slot
        {
            ProgramFragmentFixedFunction.Builder.EnvMode env;
            ProgramFragmentFixedFunction.Builder.Format format;

            Slot(ProgramFragmentFixedFunction.Builder.EnvMode paramFormat, ProgramFragmentFixedFunction.Builder.Format arg3)
            {
                this.env = paramFormat;
                Object localObject;
                this.format = localObject;
            }
        }

        public static enum Format
        {
            int mID;

            static
            {
                Format[] arrayOfFormat = new Format[4];
                arrayOfFormat[0] = ALPHA;
                arrayOfFormat[1] = LUMINANCE_ALPHA;
                arrayOfFormat[2] = RGB;
                arrayOfFormat[3] = RGBA;
            }

            private Format(int paramInt)
            {
                this.mID = paramInt;
            }
        }

        public static enum EnvMode
        {
            int mID;

            static
            {
                MODULATE = new EnvMode("MODULATE", 1, 2);
                DECAL = new EnvMode("DECAL", 2, 3);
                EnvMode[] arrayOfEnvMode = new EnvMode[3];
                arrayOfEnvMode[0] = REPLACE;
                arrayOfEnvMode[1] = MODULATE;
                arrayOfEnvMode[2] = DECAL;
            }

            private EnvMode(int paramInt)
            {
                this.mID = paramInt;
            }
        }
    }

    static class InternalBuilder extends Program.BaseProgramBuilder
    {
        public InternalBuilder(RenderScript paramRenderScript)
        {
            super();
        }

        public ProgramFragmentFixedFunction create()
        {
            this.mRS.validate();
            int[] arrayOfInt = new int[2 * (this.mInputCount + this.mOutputCount + this.mConstantCount + this.mTextureCount)];
            String[] arrayOfString = new String[this.mTextureCount];
            int i = 0;
            for (int j = 0; j < this.mInputCount; j++)
            {
                int i4 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.INPUT.mID;
                i = i4 + 1;
                arrayOfInt[i4] = this.mInputs[j].getID(this.mRS);
            }
            for (int k = 0; k < this.mOutputCount; k++)
            {
                int i3 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.OUTPUT.mID;
                i = i3 + 1;
                arrayOfInt[i3] = this.mOutputs[k].getID(this.mRS);
            }
            for (int m = 0; m < this.mConstantCount; m++)
            {
                int i2 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.CONSTANT.mID;
                i = i2 + 1;
                arrayOfInt[i2] = this.mConstants[m].getID(this.mRS);
            }
            for (int n = 0; n < this.mTextureCount; n++)
            {
                int i1 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.TEXTURE_TYPE.mID;
                i = i1 + 1;
                arrayOfInt[i1] = this.mTextureTypes[n].mID;
                arrayOfString[n] = this.mTextureNames[n];
            }
            ProgramFragmentFixedFunction localProgramFragmentFixedFunction = new ProgramFragmentFixedFunction(this.mRS.nProgramFragmentCreate(this.mShader, arrayOfString, arrayOfInt), this.mRS);
            initProgram(localProgramFragmentFixedFunction);
            return localProgramFragmentFixedFunction;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.ProgramFragmentFixedFunction
 * JD-Core Version:        0.6.2
 */