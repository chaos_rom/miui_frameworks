package android.renderscript;

public class Double4
{
    public double w;
    public double x;
    public double y;
    public double z;

    public Double4()
    {
    }

    public Double4(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
    {
        this.x = paramDouble1;
        this.y = paramDouble2;
        this.z = paramDouble3;
        this.w = paramDouble4;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Double4
 * JD-Core Version:        0.6.2
 */