package android.renderscript;

public class Double3
{
    public double x;
    public double y;
    public double z;

    public Double3()
    {
    }

    public Double3(double paramDouble1, double paramDouble2, double paramDouble3)
    {
        this.x = paramDouble1;
        this.y = paramDouble2;
        this.z = paramDouble3;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Double3
 * JD-Core Version:        0.6.2
 */