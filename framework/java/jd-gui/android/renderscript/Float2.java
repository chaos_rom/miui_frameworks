package android.renderscript;

public class Float2
{
    public float x;
    public float y;

    public Float2()
    {
    }

    public Float2(float paramFloat1, float paramFloat2)
    {
        this.x = paramFloat1;
        this.y = paramFloat2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Float2
 * JD-Core Version:        0.6.2
 */