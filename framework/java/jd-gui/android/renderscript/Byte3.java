package android.renderscript;

public class Byte3
{
    public byte x;
    public byte y;
    public byte z;

    public Byte3()
    {
    }

    public Byte3(byte paramByte1, byte paramByte2, byte paramByte3)
    {
        this.x = paramByte1;
        this.y = paramByte2;
        this.z = paramByte3;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Byte3
 * JD-Core Version:        0.6.2
 */