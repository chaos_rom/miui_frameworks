package android.renderscript;

public class ProgramStore extends BaseObj
{
    BlendDstFunc mBlendDst;
    BlendSrcFunc mBlendSrc;
    boolean mColorMaskA;
    boolean mColorMaskB;
    boolean mColorMaskG;
    boolean mColorMaskR;
    DepthFunc mDepthFunc;
    boolean mDepthMask;
    boolean mDither;

    ProgramStore(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    public static ProgramStore BLEND_ALPHA_DEPTH_NONE(RenderScript paramRenderScript)
    {
        if (paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_NO_DEPTH == null)
        {
            Builder localBuilder = new Builder(paramRenderScript);
            localBuilder.setDepthFunc(DepthFunc.ALWAYS);
            localBuilder.setBlendFunc(BlendSrcFunc.SRC_ALPHA, BlendDstFunc.ONE_MINUS_SRC_ALPHA);
            localBuilder.setDitherEnabled(false);
            localBuilder.setDepthMaskEnabled(false);
            paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_NO_DEPTH = localBuilder.create();
        }
        return paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_NO_DEPTH;
    }

    public static ProgramStore BLEND_ALPHA_DEPTH_TEST(RenderScript paramRenderScript)
    {
        if (paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_TEST == null)
        {
            Builder localBuilder = new Builder(paramRenderScript);
            localBuilder.setDepthFunc(DepthFunc.LESS);
            localBuilder.setBlendFunc(BlendSrcFunc.SRC_ALPHA, BlendDstFunc.ONE_MINUS_SRC_ALPHA);
            localBuilder.setDitherEnabled(false);
            localBuilder.setDepthMaskEnabled(true);
            paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_TEST = localBuilder.create();
        }
        return paramRenderScript.mProgramStore_BLEND_ALPHA_DEPTH_TEST;
    }

    public static ProgramStore BLEND_NONE_DEPTH_NONE(RenderScript paramRenderScript)
    {
        if (paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_NO_DEPTH == null)
        {
            Builder localBuilder = new Builder(paramRenderScript);
            localBuilder.setDepthFunc(DepthFunc.ALWAYS);
            localBuilder.setBlendFunc(BlendSrcFunc.ONE, BlendDstFunc.ZERO);
            localBuilder.setDitherEnabled(false);
            localBuilder.setDepthMaskEnabled(false);
            paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_NO_DEPTH = localBuilder.create();
        }
        return paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_NO_DEPTH;
    }

    public static ProgramStore BLEND_NONE_DEPTH_TEST(RenderScript paramRenderScript)
    {
        if (paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_TEST == null)
        {
            Builder localBuilder = new Builder(paramRenderScript);
            localBuilder.setDepthFunc(DepthFunc.LESS);
            localBuilder.setBlendFunc(BlendSrcFunc.ONE, BlendDstFunc.ZERO);
            localBuilder.setDitherEnabled(false);
            localBuilder.setDepthMaskEnabled(true);
            paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_TEST = localBuilder.create();
        }
        return paramRenderScript.mProgramStore_BLEND_NONE_DEPTH_TEST;
    }

    public BlendDstFunc getBlendDstFunc()
    {
        return this.mBlendDst;
    }

    public BlendSrcFunc getBlendSrcFunc()
    {
        return this.mBlendSrc;
    }

    public DepthFunc getDepthFunc()
    {
        return this.mDepthFunc;
    }

    public boolean isColorMaskAlphaEnabled()
    {
        return this.mColorMaskA;
    }

    public boolean isColorMaskBlueEnabled()
    {
        return this.mColorMaskB;
    }

    public boolean isColorMaskGreenEnabled()
    {
        return this.mColorMaskG;
    }

    public boolean isColorMaskRedEnabled()
    {
        return this.mColorMaskR;
    }

    public boolean isDepthMaskEnabled()
    {
        return this.mDepthMask;
    }

    public boolean isDitherEnabled()
    {
        return this.mDither;
    }

    public static class Builder
    {
        ProgramStore.BlendDstFunc mBlendDst;
        ProgramStore.BlendSrcFunc mBlendSrc;
        boolean mColorMaskA;
        boolean mColorMaskB;
        boolean mColorMaskG;
        boolean mColorMaskR;
        ProgramStore.DepthFunc mDepthFunc;
        boolean mDepthMask;
        boolean mDither;
        RenderScript mRS;

        public Builder(RenderScript paramRenderScript)
        {
            this.mRS = paramRenderScript;
            this.mDepthFunc = ProgramStore.DepthFunc.ALWAYS;
            this.mDepthMask = false;
            this.mColorMaskR = true;
            this.mColorMaskG = true;
            this.mColorMaskB = true;
            this.mColorMaskA = true;
            this.mBlendSrc = ProgramStore.BlendSrcFunc.ONE;
            this.mBlendDst = ProgramStore.BlendDstFunc.ZERO;
        }

        public ProgramStore create()
        {
            this.mRS.validate();
            ProgramStore localProgramStore = new ProgramStore(this.mRS.nProgramStoreCreate(this.mColorMaskR, this.mColorMaskG, this.mColorMaskB, this.mColorMaskA, this.mDepthMask, this.mDither, this.mBlendSrc.mID, this.mBlendDst.mID, this.mDepthFunc.mID), this.mRS);
            localProgramStore.mDepthFunc = this.mDepthFunc;
            localProgramStore.mDepthMask = this.mDepthMask;
            localProgramStore.mColorMaskR = this.mColorMaskR;
            localProgramStore.mColorMaskG = this.mColorMaskG;
            localProgramStore.mColorMaskB = this.mColorMaskB;
            localProgramStore.mColorMaskA = this.mColorMaskA;
            localProgramStore.mBlendSrc = this.mBlendSrc;
            localProgramStore.mBlendDst = this.mBlendDst;
            localProgramStore.mDither = this.mDither;
            return localProgramStore;
        }

        public Builder setBlendFunc(ProgramStore.BlendSrcFunc paramBlendSrcFunc, ProgramStore.BlendDstFunc paramBlendDstFunc)
        {
            this.mBlendSrc = paramBlendSrcFunc;
            this.mBlendDst = paramBlendDstFunc;
            return this;
        }

        public Builder setColorMaskEnabled(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
        {
            this.mColorMaskR = paramBoolean1;
            this.mColorMaskG = paramBoolean2;
            this.mColorMaskB = paramBoolean3;
            this.mColorMaskA = paramBoolean4;
            return this;
        }

        public Builder setDepthFunc(ProgramStore.DepthFunc paramDepthFunc)
        {
            this.mDepthFunc = paramDepthFunc;
            return this;
        }

        public Builder setDepthMaskEnabled(boolean paramBoolean)
        {
            this.mDepthMask = paramBoolean;
            return this;
        }

        public Builder setDitherEnabled(boolean paramBoolean)
        {
            this.mDither = paramBoolean;
            return this;
        }
    }

    public static enum BlendDstFunc
    {
        int mID;

        static
        {
            ONE = new BlendDstFunc("ONE", 1, 1);
            SRC_COLOR = new BlendDstFunc("SRC_COLOR", 2, 2);
            ONE_MINUS_SRC_COLOR = new BlendDstFunc("ONE_MINUS_SRC_COLOR", 3, 3);
            SRC_ALPHA = new BlendDstFunc("SRC_ALPHA", 4, 4);
            ONE_MINUS_SRC_ALPHA = new BlendDstFunc("ONE_MINUS_SRC_ALPHA", 5, 5);
            DST_ALPHA = new BlendDstFunc("DST_ALPHA", 6, 6);
            ONE_MINUS_DST_ALPHA = new BlendDstFunc("ONE_MINUS_DST_ALPHA", 7, 7);
            BlendDstFunc[] arrayOfBlendDstFunc = new BlendDstFunc[8];
            arrayOfBlendDstFunc[0] = ZERO;
            arrayOfBlendDstFunc[1] = ONE;
            arrayOfBlendDstFunc[2] = SRC_COLOR;
            arrayOfBlendDstFunc[3] = ONE_MINUS_SRC_COLOR;
            arrayOfBlendDstFunc[4] = SRC_ALPHA;
            arrayOfBlendDstFunc[5] = ONE_MINUS_SRC_ALPHA;
            arrayOfBlendDstFunc[6] = DST_ALPHA;
            arrayOfBlendDstFunc[7] = ONE_MINUS_DST_ALPHA;
        }

        private BlendDstFunc(int paramInt)
        {
            this.mID = paramInt;
        }
    }

    public static enum BlendSrcFunc
    {
        int mID;

        static
        {
            ONE = new BlendSrcFunc("ONE", 1, 1);
            DST_COLOR = new BlendSrcFunc("DST_COLOR", 2, 2);
            ONE_MINUS_DST_COLOR = new BlendSrcFunc("ONE_MINUS_DST_COLOR", 3, 3);
            SRC_ALPHA = new BlendSrcFunc("SRC_ALPHA", 4, 4);
            ONE_MINUS_SRC_ALPHA = new BlendSrcFunc("ONE_MINUS_SRC_ALPHA", 5, 5);
            DST_ALPHA = new BlendSrcFunc("DST_ALPHA", 6, 6);
            ONE_MINUS_DST_ALPHA = new BlendSrcFunc("ONE_MINUS_DST_ALPHA", 7, 7);
            SRC_ALPHA_SATURATE = new BlendSrcFunc("SRC_ALPHA_SATURATE", 8, 8);
            BlendSrcFunc[] arrayOfBlendSrcFunc = new BlendSrcFunc[9];
            arrayOfBlendSrcFunc[0] = ZERO;
            arrayOfBlendSrcFunc[1] = ONE;
            arrayOfBlendSrcFunc[2] = DST_COLOR;
            arrayOfBlendSrcFunc[3] = ONE_MINUS_DST_COLOR;
            arrayOfBlendSrcFunc[4] = SRC_ALPHA;
            arrayOfBlendSrcFunc[5] = ONE_MINUS_SRC_ALPHA;
            arrayOfBlendSrcFunc[6] = DST_ALPHA;
            arrayOfBlendSrcFunc[7] = ONE_MINUS_DST_ALPHA;
            arrayOfBlendSrcFunc[8] = SRC_ALPHA_SATURATE;
        }

        private BlendSrcFunc(int paramInt)
        {
            this.mID = paramInt;
        }
    }

    public static enum DepthFunc
    {
        int mID;

        static
        {
            GREATER = new DepthFunc("GREATER", 3, 3);
            GREATER_OR_EQUAL = new DepthFunc("GREATER_OR_EQUAL", 4, 4);
            EQUAL = new DepthFunc("EQUAL", 5, 5);
            NOT_EQUAL = new DepthFunc("NOT_EQUAL", 6, 6);
            DepthFunc[] arrayOfDepthFunc = new DepthFunc[7];
            arrayOfDepthFunc[0] = ALWAYS;
            arrayOfDepthFunc[1] = LESS;
            arrayOfDepthFunc[2] = LESS_OR_EQUAL;
            arrayOfDepthFunc[3] = GREATER;
            arrayOfDepthFunc[4] = GREATER_OR_EQUAL;
            arrayOfDepthFunc[5] = EQUAL;
            arrayOfDepthFunc[6] = NOT_EQUAL;
        }

        private DepthFunc(int paramInt)
        {
            this.mID = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.ProgramStore
 * JD-Core Version:        0.6.2
 */