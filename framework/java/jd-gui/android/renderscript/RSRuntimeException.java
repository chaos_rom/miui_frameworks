package android.renderscript;

public class RSRuntimeException extends RuntimeException
{
    public RSRuntimeException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.RSRuntimeException
 * JD-Core Version:        0.6.2
 */