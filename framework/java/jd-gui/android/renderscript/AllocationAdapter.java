package android.renderscript;

import android.util.Log;

public class AllocationAdapter extends Allocation
{
    AllocationAdapter(int paramInt, RenderScript paramRenderScript, Allocation paramAllocation)
    {
        super(paramInt, paramRenderScript, paramAllocation.mType, paramAllocation.mUsage);
        this.mAdaptedAllocation = paramAllocation;
    }

    public static AllocationAdapter create1D(RenderScript paramRenderScript, Allocation paramAllocation)
    {
        paramRenderScript.validate();
        AllocationAdapter localAllocationAdapter = new AllocationAdapter(0, paramRenderScript, paramAllocation);
        localAllocationAdapter.mConstrainedLOD = true;
        localAllocationAdapter.mConstrainedFace = true;
        localAllocationAdapter.mConstrainedY = true;
        localAllocationAdapter.mConstrainedZ = true;
        localAllocationAdapter.initLOD(0);
        return localAllocationAdapter;
    }

    public static AllocationAdapter create2D(RenderScript paramRenderScript, Allocation paramAllocation)
    {
        Log.e("rs", "create2d " + paramAllocation);
        paramRenderScript.validate();
        AllocationAdapter localAllocationAdapter = new AllocationAdapter(0, paramRenderScript, paramAllocation);
        localAllocationAdapter.mConstrainedLOD = true;
        localAllocationAdapter.mConstrainedFace = true;
        localAllocationAdapter.mConstrainedY = false;
        localAllocationAdapter.mConstrainedZ = true;
        localAllocationAdapter.initLOD(0);
        return localAllocationAdapter;
    }

    int getID(RenderScript paramRenderScript)
    {
        throw new RSInvalidStateException("This operation is not supported with adapters at this time.");
    }

    void initLOD(int paramInt)
    {
        if (paramInt < 0)
            throw new RSIllegalArgumentException("Attempting to set negative lod (" + paramInt + ").");
        int i = this.mAdaptedAllocation.mType.getX();
        int j = this.mAdaptedAllocation.mType.getY();
        int k = this.mAdaptedAllocation.mType.getZ();
        for (int m = 0; m < paramInt; m++)
        {
            if ((i == 1) && (j == 1) && (k == 1))
                throw new RSIllegalArgumentException("Attempting to set lod (" + paramInt + ") out of range.");
            if (i > 1)
                i >>= 1;
            if (j > 1)
                j >>= 1;
            if (k > 1)
                k >>= 1;
        }
        this.mCurrentDimX = i;
        this.mCurrentDimY = j;
        this.mCurrentDimZ = k;
        this.mCurrentCount = this.mCurrentDimX;
        if (this.mCurrentDimY > 1)
            this.mCurrentCount *= this.mCurrentDimY;
        if (this.mCurrentDimZ > 1)
            this.mCurrentCount *= this.mCurrentDimZ;
        this.mSelectedY = 0;
        this.mSelectedZ = 0;
    }

    public void readData(float[] paramArrayOfFloat)
    {
        super.copyTo(paramArrayOfFloat);
    }

    public void readData(int[] paramArrayOfInt)
    {
        super.copyTo(paramArrayOfInt);
    }

    /** @deprecated */
    public void resize(int paramInt)
    {
        try
        {
            throw new RSInvalidStateException("Resize not allowed for Adapters.");
        }
        finally
        {
        }
    }

    public void setFace(Type.CubemapFace paramCubemapFace)
    {
        if (!this.mAdaptedAllocation.getType().hasFaces())
            throw new RSInvalidStateException("Cannot set Face when the allocation type does not include faces.");
        if (!this.mConstrainedFace)
            throw new RSInvalidStateException("Cannot set LOD when the adapter includes mipmaps.");
        if (paramCubemapFace == null)
            throw new RSIllegalArgumentException("Cannot set null face.");
        this.mSelectedFace = paramCubemapFace;
    }

    public void setLOD(int paramInt)
    {
        if (!this.mAdaptedAllocation.getType().hasMipmaps())
            throw new RSInvalidStateException("Cannot set LOD when the allocation type does not include mipmaps.");
        if (!this.mConstrainedLOD)
            throw new RSInvalidStateException("Cannot set LOD when the adapter includes mipmaps.");
        initLOD(paramInt);
    }

    public void setY(int paramInt)
    {
        if (this.mAdaptedAllocation.getType().getY() == 0)
            throw new RSInvalidStateException("Cannot set Y when the allocation type does not include Y dim.");
        if (this.mAdaptedAllocation.getType().getY() <= paramInt)
            throw new RSInvalidStateException("Cannot set Y greater than dimension of allocation.");
        if (!this.mConstrainedY)
            throw new RSInvalidStateException("Cannot set Y when the adapter includes Y.");
        this.mSelectedY = paramInt;
    }

    public void setZ(int paramInt)
    {
        if (this.mAdaptedAllocation.getType().getZ() == 0)
            throw new RSInvalidStateException("Cannot set Z when the allocation type does not include Z dim.");
        if (this.mAdaptedAllocation.getType().getZ() <= paramInt)
            throw new RSInvalidStateException("Cannot set Z greater than dimension of allocation.");
        if (!this.mConstrainedZ)
            throw new RSInvalidStateException("Cannot set Z when the adapter includes Z.");
        this.mSelectedZ = paramInt;
    }

    public void subData(int paramInt, FieldPacker paramFieldPacker)
    {
        super.setFromFieldPacker(paramInt, paramFieldPacker);
    }

    public void subData1D(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        super.copy1DRangeFrom(paramInt1, paramInt2, paramArrayOfByte);
    }

    public void subData1D(int paramInt1, int paramInt2, float[] paramArrayOfFloat)
    {
        super.copy1DRangeFrom(paramInt1, paramInt2, paramArrayOfFloat);
    }

    public void subData1D(int paramInt1, int paramInt2, int[] paramArrayOfInt)
    {
        super.copy1DRangeFrom(paramInt1, paramInt2, paramArrayOfInt);
    }

    public void subData1D(int paramInt1, int paramInt2, short[] paramArrayOfShort)
    {
        super.copy1DRangeFrom(paramInt1, paramInt2, paramArrayOfShort);
    }

    public void subData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float[] paramArrayOfFloat)
    {
        super.copy2DRangeFrom(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfFloat);
    }

    public void subData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
    {
        super.copy2DRangeFrom(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfInt);
    }

    public void subElementData(int paramInt1, int paramInt2, FieldPacker paramFieldPacker)
    {
        super.setFromFieldPacker(paramInt1, paramInt2, paramFieldPacker);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.AllocationAdapter
 * JD-Core Version:        0.6.2
 */