package android.renderscript;

public class Path extends BaseObj
{
    boolean mCoverageToAlpha;
    Allocation mLoopBuffer;
    Primitive mPrimitive;
    float mQuality;
    Allocation mVertexBuffer;

    Path(int paramInt, RenderScript paramRenderScript, Primitive paramPrimitive, Allocation paramAllocation1, Allocation paramAllocation2, float paramFloat)
    {
        super(paramInt, paramRenderScript);
        this.mVertexBuffer = paramAllocation1;
        this.mLoopBuffer = paramAllocation2;
        this.mPrimitive = paramPrimitive;
        this.mQuality = paramFloat;
    }

    public static Path createDynamicPath(RenderScript paramRenderScript, Primitive paramPrimitive, float paramFloat, Allocation paramAllocation)
    {
        return null;
    }

    public static Path createDynamicPath(RenderScript paramRenderScript, Primitive paramPrimitive, float paramFloat, Allocation paramAllocation1, Allocation paramAllocation2)
    {
        return null;
    }

    public static Path createStaticPath(RenderScript paramRenderScript, Primitive paramPrimitive, float paramFloat, Allocation paramAllocation)
    {
        return new Path(paramRenderScript.nPathCreate(paramPrimitive.mID, false, paramAllocation.getID(paramRenderScript), 0, paramFloat), paramRenderScript, paramPrimitive, null, null, paramFloat);
    }

    public static Path createStaticPath(RenderScript paramRenderScript, Primitive paramPrimitive, float paramFloat, Allocation paramAllocation1, Allocation paramAllocation2)
    {
        return null;
    }

    public Allocation getLoopAllocation()
    {
        return this.mLoopBuffer;
    }

    public Primitive getPrimitive()
    {
        return this.mPrimitive;
    }

    public Allocation getVertexAllocation()
    {
        return this.mVertexBuffer;
    }

    void updateFromNative()
    {
    }

    public static enum Primitive
    {
        int mID;

        static
        {
            CUBIC_BEZIER = new Primitive("CUBIC_BEZIER", 1, 1);
            Primitive[] arrayOfPrimitive = new Primitive[2];
            arrayOfPrimitive[0] = QUADRATIC_BEZIER;
            arrayOfPrimitive[1] = CUBIC_BEZIER;
        }

        private Primitive(int paramInt)
        {
            this.mID = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Path
 * JD-Core Version:        0.6.2
 */