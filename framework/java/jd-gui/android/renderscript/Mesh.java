package android.renderscript;

import java.util.Vector;

public class Mesh extends BaseObj
{
    Allocation[] mIndexBuffers;
    Primitive[] mPrimitives;
    Allocation[] mVertexBuffers;

    Mesh(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    public Allocation getIndexSetAllocation(int paramInt)
    {
        return this.mIndexBuffers[paramInt];
    }

    public Primitive getPrimitive(int paramInt)
    {
        return this.mPrimitives[paramInt];
    }

    public int getPrimitiveCount()
    {
        if (this.mIndexBuffers == null);
        for (int i = 0; ; i = this.mIndexBuffers.length)
            return i;
    }

    public Allocation getVertexAllocation(int paramInt)
    {
        return this.mVertexBuffers[paramInt];
    }

    public int getVertexAllocationCount()
    {
        if (this.mVertexBuffers == null);
        for (int i = 0; ; i = this.mVertexBuffers.length)
            return i;
    }

    void updateFromNative()
    {
        super.updateFromNative();
        int i = this.mRS.nMeshGetVertexBufferCount(getID(this.mRS));
        int j = this.mRS.nMeshGetIndexCount(getID(this.mRS));
        int[] arrayOfInt1 = new int[i];
        int[] arrayOfInt2 = new int[j];
        int[] arrayOfInt3 = new int[j];
        this.mRS.nMeshGetVertices(getID(this.mRS), arrayOfInt1, i);
        this.mRS.nMeshGetIndices(getID(this.mRS), arrayOfInt2, arrayOfInt3, j);
        this.mVertexBuffers = new Allocation[i];
        this.mIndexBuffers = new Allocation[j];
        this.mPrimitives = new Primitive[j];
        for (int k = 0; k < i; k++)
            if (arrayOfInt1[k] != 0)
            {
                this.mVertexBuffers[k] = new Allocation(arrayOfInt1[k], this.mRS, null, 1);
                this.mVertexBuffers[k].updateFromNative();
            }
        for (int m = 0; m < j; m++)
        {
            if (arrayOfInt2[m] != 0)
            {
                this.mIndexBuffers[m] = new Allocation(arrayOfInt2[m], this.mRS, null, 1);
                this.mIndexBuffers[m].updateFromNative();
            }
            this.mPrimitives[m] = Primitive.values()[arrayOfInt3[m]];
        }
    }

    public static class TriangleMeshBuilder
    {
        public static final int COLOR = 1;
        public static final int NORMAL = 2;
        public static final int TEXTURE_0 = 256;
        float mA = 1.0F;
        float mB = 1.0F;
        Element mElement;
        int mFlags;
        float mG = 1.0F;
        int mIndexCount;
        short[] mIndexData;
        int mMaxIndex;
        float mNX = 0.0F;
        float mNY = 0.0F;
        float mNZ = -1.0F;
        float mR = 1.0F;
        RenderScript mRS;
        float mS0 = 0.0F;
        float mT0 = 0.0F;
        int mVtxCount;
        float[] mVtxData;
        int mVtxSize;

        public TriangleMeshBuilder(RenderScript paramRenderScript, int paramInt1, int paramInt2)
        {
            this.mRS = paramRenderScript;
            this.mVtxCount = 0;
            this.mMaxIndex = 0;
            this.mIndexCount = 0;
            this.mVtxData = new float[''];
            this.mIndexData = new short[''];
            this.mVtxSize = paramInt1;
            this.mFlags = paramInt2;
            if ((paramInt1 < 2) || (paramInt1 > 3))
                throw new IllegalArgumentException("Vertex size out of range.");
        }

        private void latch()
        {
            if ((0x1 & this.mFlags) != 0)
            {
                makeSpace(4);
                float[] arrayOfFloat7 = this.mVtxData;
                int i2 = this.mVtxCount;
                this.mVtxCount = (i2 + 1);
                arrayOfFloat7[i2] = this.mR;
                float[] arrayOfFloat8 = this.mVtxData;
                int i3 = this.mVtxCount;
                this.mVtxCount = (i3 + 1);
                arrayOfFloat8[i3] = this.mG;
                float[] arrayOfFloat9 = this.mVtxData;
                int i4 = this.mVtxCount;
                this.mVtxCount = (i4 + 1);
                arrayOfFloat9[i4] = this.mB;
                float[] arrayOfFloat10 = this.mVtxData;
                int i5 = this.mVtxCount;
                this.mVtxCount = (i5 + 1);
                arrayOfFloat10[i5] = this.mA;
            }
            if ((0x100 & this.mFlags) != 0)
            {
                makeSpace(2);
                float[] arrayOfFloat5 = this.mVtxData;
                int n = this.mVtxCount;
                this.mVtxCount = (n + 1);
                arrayOfFloat5[n] = this.mS0;
                float[] arrayOfFloat6 = this.mVtxData;
                int i1 = this.mVtxCount;
                this.mVtxCount = (i1 + 1);
                arrayOfFloat6[i1] = this.mT0;
            }
            if ((0x2 & this.mFlags) != 0)
            {
                makeSpace(4);
                float[] arrayOfFloat1 = this.mVtxData;
                int i = this.mVtxCount;
                this.mVtxCount = (i + 1);
                arrayOfFloat1[i] = this.mNX;
                float[] arrayOfFloat2 = this.mVtxData;
                int j = this.mVtxCount;
                this.mVtxCount = (j + 1);
                arrayOfFloat2[j] = this.mNY;
                float[] arrayOfFloat3 = this.mVtxData;
                int k = this.mVtxCount;
                this.mVtxCount = (k + 1);
                arrayOfFloat3[k] = this.mNZ;
                float[] arrayOfFloat4 = this.mVtxData;
                int m = this.mVtxCount;
                this.mVtxCount = (m + 1);
                arrayOfFloat4[m] = 0.0F;
            }
            this.mMaxIndex = (1 + this.mMaxIndex);
        }

        private void makeSpace(int paramInt)
        {
            if (paramInt + this.mVtxCount >= this.mVtxData.length)
            {
                float[] arrayOfFloat = new float[2 * this.mVtxData.length];
                System.arraycopy(this.mVtxData, 0, arrayOfFloat, 0, this.mVtxData.length);
                this.mVtxData = arrayOfFloat;
            }
        }

        public TriangleMeshBuilder addTriangle(int paramInt1, int paramInt2, int paramInt3)
        {
            if ((paramInt1 >= this.mMaxIndex) || (paramInt1 < 0) || (paramInt2 >= this.mMaxIndex) || (paramInt2 < 0) || (paramInt3 >= this.mMaxIndex) || (paramInt3 < 0))
                throw new IllegalStateException("Index provided greater than vertex count.");
            if (3 + this.mIndexCount >= this.mIndexData.length)
            {
                short[] arrayOfShort4 = new short[2 * this.mIndexData.length];
                System.arraycopy(this.mIndexData, 0, arrayOfShort4, 0, this.mIndexData.length);
                this.mIndexData = arrayOfShort4;
            }
            short[] arrayOfShort1 = this.mIndexData;
            int i = this.mIndexCount;
            this.mIndexCount = (i + 1);
            arrayOfShort1[i] = ((short)paramInt1);
            short[] arrayOfShort2 = this.mIndexData;
            int j = this.mIndexCount;
            this.mIndexCount = (j + 1);
            arrayOfShort2[j] = ((short)paramInt2);
            short[] arrayOfShort3 = this.mIndexData;
            int k = this.mIndexCount;
            this.mIndexCount = (k + 1);
            arrayOfShort3[k] = ((short)paramInt3);
            return this;
        }

        public TriangleMeshBuilder addVertex(float paramFloat1, float paramFloat2)
        {
            if (this.mVtxSize != 2)
                throw new IllegalStateException("add mistmatch with declared components.");
            makeSpace(2);
            float[] arrayOfFloat1 = this.mVtxData;
            int i = this.mVtxCount;
            this.mVtxCount = (i + 1);
            arrayOfFloat1[i] = paramFloat1;
            float[] arrayOfFloat2 = this.mVtxData;
            int j = this.mVtxCount;
            this.mVtxCount = (j + 1);
            arrayOfFloat2[j] = paramFloat2;
            latch();
            return this;
        }

        public TriangleMeshBuilder addVertex(float paramFloat1, float paramFloat2, float paramFloat3)
        {
            if (this.mVtxSize != 3)
                throw new IllegalStateException("add mistmatch with declared components.");
            makeSpace(4);
            float[] arrayOfFloat1 = this.mVtxData;
            int i = this.mVtxCount;
            this.mVtxCount = (i + 1);
            arrayOfFloat1[i] = paramFloat1;
            float[] arrayOfFloat2 = this.mVtxData;
            int j = this.mVtxCount;
            this.mVtxCount = (j + 1);
            arrayOfFloat2[j] = paramFloat2;
            float[] arrayOfFloat3 = this.mVtxData;
            int k = this.mVtxCount;
            this.mVtxCount = (k + 1);
            arrayOfFloat3[k] = paramFloat3;
            float[] arrayOfFloat4 = this.mVtxData;
            int m = this.mVtxCount;
            this.mVtxCount = (m + 1);
            arrayOfFloat4[m] = 1.0F;
            latch();
            return this;
        }

        public Mesh create(boolean paramBoolean)
        {
            Element.Builder localBuilder = new Element.Builder(this.mRS);
            localBuilder.add(Element.createVector(this.mRS, Element.DataType.FLOAT_32, this.mVtxSize), "position");
            if ((0x1 & this.mFlags) != 0)
                localBuilder.add(Element.F32_4(this.mRS), "color");
            if ((0x100 & this.mFlags) != 0)
                localBuilder.add(Element.F32_2(this.mRS), "texture0");
            if ((0x2 & this.mFlags) != 0)
                localBuilder.add(Element.F32_3(this.mRS), "normal");
            this.mElement = localBuilder.create();
            int i = 1;
            if (paramBoolean)
                i |= 4;
            Mesh.Builder localBuilder1 = new Mesh.Builder(this.mRS, i);
            localBuilder1.addVertexType(this.mElement, this.mMaxIndex);
            localBuilder1.addIndexSetType(Element.U16(this.mRS), this.mIndexCount, Mesh.Primitive.TRIANGLE);
            Mesh localMesh = localBuilder1.create();
            localMesh.getVertexAllocation(0).copy1DRangeFromUnchecked(0, this.mMaxIndex, this.mVtxData);
            if ((paramBoolean) && (paramBoolean))
                localMesh.getVertexAllocation(0).syncAll(1);
            localMesh.getIndexSetAllocation(0).copy1DRangeFromUnchecked(0, this.mIndexCount, this.mIndexData);
            if (paramBoolean)
                localMesh.getIndexSetAllocation(0).syncAll(1);
            return localMesh;
        }

        public TriangleMeshBuilder setColor(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
        {
            if ((0x1 & this.mFlags) == 0)
                throw new IllegalStateException("add mistmatch with declared components.");
            this.mR = paramFloat1;
            this.mG = paramFloat2;
            this.mB = paramFloat3;
            this.mA = paramFloat4;
            return this;
        }

        public TriangleMeshBuilder setNormal(float paramFloat1, float paramFloat2, float paramFloat3)
        {
            if ((0x2 & this.mFlags) == 0)
                throw new IllegalStateException("add mistmatch with declared components.");
            this.mNX = paramFloat1;
            this.mNY = paramFloat2;
            this.mNZ = paramFloat3;
            return this;
        }

        public TriangleMeshBuilder setTexture(float paramFloat1, float paramFloat2)
        {
            if ((0x100 & this.mFlags) == 0)
                throw new IllegalStateException("add mistmatch with declared components.");
            this.mS0 = paramFloat1;
            this.mT0 = paramFloat2;
            return this;
        }
    }

    public static class AllocationBuilder
    {
        Vector mIndexTypes;
        RenderScript mRS;
        int mVertexTypeCount;
        Entry[] mVertexTypes;

        public AllocationBuilder(RenderScript paramRenderScript)
        {
            this.mRS = paramRenderScript;
            this.mVertexTypeCount = 0;
            this.mVertexTypes = new Entry[16];
            this.mIndexTypes = new Vector();
        }

        public AllocationBuilder addIndexSetAllocation(Allocation paramAllocation, Mesh.Primitive paramPrimitive)
        {
            Entry localEntry = new Entry();
            localEntry.a = paramAllocation;
            localEntry.prim = paramPrimitive;
            this.mIndexTypes.addElement(localEntry);
            return this;
        }

        public AllocationBuilder addIndexSetType(Mesh.Primitive paramPrimitive)
        {
            Entry localEntry = new Entry();
            localEntry.a = null;
            localEntry.prim = paramPrimitive;
            this.mIndexTypes.addElement(localEntry);
            return this;
        }

        public AllocationBuilder addVertexAllocation(Allocation paramAllocation)
            throws IllegalStateException
        {
            if (this.mVertexTypeCount >= this.mVertexTypes.length)
                throw new IllegalStateException("Max vertex types exceeded.");
            this.mVertexTypes[this.mVertexTypeCount] = new Entry();
            this.mVertexTypes[this.mVertexTypeCount].a = paramAllocation;
            this.mVertexTypeCount = (1 + this.mVertexTypeCount);
            return this;
        }

        public Mesh create()
        {
            this.mRS.validate();
            int[] arrayOfInt1 = new int[this.mVertexTypeCount];
            int[] arrayOfInt2 = new int[this.mIndexTypes.size()];
            int[] arrayOfInt3 = new int[this.mIndexTypes.size()];
            Allocation[] arrayOfAllocation1 = new Allocation[this.mIndexTypes.size()];
            Mesh.Primitive[] arrayOfPrimitive = new Mesh.Primitive[this.mIndexTypes.size()];
            Allocation[] arrayOfAllocation2 = new Allocation[this.mVertexTypeCount];
            for (int i = 0; i < this.mVertexTypeCount; i++)
            {
                Entry localEntry2 = this.mVertexTypes[i];
                arrayOfAllocation2[i] = localEntry2.a;
                arrayOfInt1[i] = localEntry2.a.getID(this.mRS);
            }
            int j = 0;
            if (j < this.mIndexTypes.size())
            {
                Entry localEntry1 = (Entry)this.mIndexTypes.elementAt(j);
                if (localEntry1.a == null);
                for (int k = 0; ; k = localEntry1.a.getID(this.mRS))
                {
                    arrayOfAllocation1[j] = localEntry1.a;
                    arrayOfPrimitive[j] = localEntry1.prim;
                    arrayOfInt2[j] = k;
                    arrayOfInt3[j] = localEntry1.prim.mID;
                    j++;
                    break;
                }
            }
            Mesh localMesh = new Mesh(this.mRS.nMeshCreate(arrayOfInt1, arrayOfInt2, arrayOfInt3), this.mRS);
            localMesh.mVertexBuffers = arrayOfAllocation2;
            localMesh.mIndexBuffers = arrayOfAllocation1;
            localMesh.mPrimitives = arrayOfPrimitive;
            return localMesh;
        }

        public int getCurrentIndexSetIndex()
        {
            return -1 + this.mIndexTypes.size();
        }

        public int getCurrentVertexTypeIndex()
        {
            return -1 + this.mVertexTypeCount;
        }

        class Entry
        {
            Allocation a;
            Mesh.Primitive prim;

            Entry()
            {
            }
        }
    }

    public static class Builder
    {
        Vector mIndexTypes;
        RenderScript mRS;
        int mUsage;
        int mVertexTypeCount;
        Entry[] mVertexTypes;

        public Builder(RenderScript paramRenderScript, int paramInt)
        {
            this.mRS = paramRenderScript;
            this.mUsage = paramInt;
            this.mVertexTypeCount = 0;
            this.mVertexTypes = new Entry[16];
            this.mIndexTypes = new Vector();
        }

        public Builder addIndexSetType(Element paramElement, int paramInt, Mesh.Primitive paramPrimitive)
        {
            Entry localEntry = new Entry();
            localEntry.t = null;
            localEntry.e = paramElement;
            localEntry.size = paramInt;
            localEntry.prim = paramPrimitive;
            this.mIndexTypes.addElement(localEntry);
            return this;
        }

        public Builder addIndexSetType(Mesh.Primitive paramPrimitive)
        {
            Entry localEntry = new Entry();
            localEntry.t = null;
            localEntry.e = null;
            localEntry.size = 0;
            localEntry.prim = paramPrimitive;
            this.mIndexTypes.addElement(localEntry);
            return this;
        }

        public Builder addIndexSetType(Type paramType, Mesh.Primitive paramPrimitive)
        {
            Entry localEntry = new Entry();
            localEntry.t = paramType;
            localEntry.e = null;
            localEntry.size = 0;
            localEntry.prim = paramPrimitive;
            this.mIndexTypes.addElement(localEntry);
            return this;
        }

        public Builder addVertexType(Element paramElement, int paramInt)
            throws IllegalStateException
        {
            if (this.mVertexTypeCount >= this.mVertexTypes.length)
                throw new IllegalStateException("Max vertex types exceeded.");
            this.mVertexTypes[this.mVertexTypeCount] = new Entry();
            this.mVertexTypes[this.mVertexTypeCount].t = null;
            this.mVertexTypes[this.mVertexTypeCount].e = paramElement;
            this.mVertexTypes[this.mVertexTypeCount].size = paramInt;
            this.mVertexTypeCount = (1 + this.mVertexTypeCount);
            return this;
        }

        public Builder addVertexType(Type paramType)
            throws IllegalStateException
        {
            if (this.mVertexTypeCount >= this.mVertexTypes.length)
                throw new IllegalStateException("Max vertex types exceeded.");
            this.mVertexTypes[this.mVertexTypeCount] = new Entry();
            this.mVertexTypes[this.mVertexTypeCount].t = paramType;
            this.mVertexTypes[this.mVertexTypeCount].e = null;
            this.mVertexTypeCount = (1 + this.mVertexTypeCount);
            return this;
        }

        public Mesh create()
        {
            this.mRS.validate();
            int[] arrayOfInt1 = new int[this.mVertexTypeCount];
            int[] arrayOfInt2 = new int[this.mIndexTypes.size()];
            int[] arrayOfInt3 = new int[this.mIndexTypes.size()];
            Allocation[] arrayOfAllocation1 = new Allocation[this.mVertexTypeCount];
            Allocation[] arrayOfAllocation2 = new Allocation[this.mIndexTypes.size()];
            Mesh.Primitive[] arrayOfPrimitive = new Mesh.Primitive[this.mIndexTypes.size()];
            int i = 0;
            if (i < this.mVertexTypeCount)
            {
                Allocation localAllocation2 = null;
                Entry localEntry2 = this.mVertexTypes[i];
                if (localEntry2.t != null)
                    localAllocation2 = Allocation.createTyped(this.mRS, localEntry2.t, this.mUsage);
                while (true)
                {
                    arrayOfAllocation1[i] = localAllocation2;
                    arrayOfInt1[i] = localAllocation2.getID(this.mRS);
                    i++;
                    break;
                    if (localEntry2.e != null)
                        localAllocation2 = Allocation.createSized(this.mRS, localEntry2.e, localEntry2.size, this.mUsage);
                }
            }
            int j = 0;
            if (j < this.mIndexTypes.size())
            {
                Allocation localAllocation1 = null;
                Entry localEntry1 = (Entry)this.mIndexTypes.elementAt(j);
                if (localEntry1.t != null)
                {
                    localAllocation1 = Allocation.createTyped(this.mRS, localEntry1.t, this.mUsage);
                    label235: if (localAllocation1 != null)
                        break label318;
                }
                label318: for (int k = 0; ; k = localAllocation1.getID(this.mRS))
                {
                    arrayOfAllocation2[j] = localAllocation1;
                    arrayOfPrimitive[j] = localEntry1.prim;
                    arrayOfInt2[j] = k;
                    arrayOfInt3[j] = localEntry1.prim.mID;
                    j++;
                    break;
                    if (localEntry1.e == null)
                        break label235;
                    localAllocation1 = Allocation.createSized(this.mRS, localEntry1.e, localEntry1.size, this.mUsage);
                    break label235;
                }
            }
            Mesh localMesh = new Mesh(this.mRS.nMeshCreate(arrayOfInt1, arrayOfInt2, arrayOfInt3), this.mRS);
            localMesh.mVertexBuffers = arrayOfAllocation1;
            localMesh.mIndexBuffers = arrayOfAllocation2;
            localMesh.mPrimitives = arrayOfPrimitive;
            return localMesh;
        }

        public int getCurrentIndexSetIndex()
        {
            return -1 + this.mIndexTypes.size();
        }

        public int getCurrentVertexTypeIndex()
        {
            return -1 + this.mVertexTypeCount;
        }

        Type newType(Element paramElement, int paramInt)
        {
            Type.Builder localBuilder = new Type.Builder(this.mRS, paramElement);
            localBuilder.setX(paramInt);
            return localBuilder.create();
        }

        class Entry
        {
            Element e;
            Mesh.Primitive prim;
            int size;
            Type t;
            int usage;

            Entry()
            {
            }
        }
    }

    public static enum Primitive
    {
        int mID;

        static
        {
            LINE = new Primitive("LINE", 1, 1);
            LINE_STRIP = new Primitive("LINE_STRIP", 2, 2);
            TRIANGLE = new Primitive("TRIANGLE", 3, 3);
            TRIANGLE_STRIP = new Primitive("TRIANGLE_STRIP", 4, 4);
            TRIANGLE_FAN = new Primitive("TRIANGLE_FAN", 5, 5);
            Primitive[] arrayOfPrimitive = new Primitive[6];
            arrayOfPrimitive[0] = POINT;
            arrayOfPrimitive[1] = LINE;
            arrayOfPrimitive[2] = LINE_STRIP;
            arrayOfPrimitive[3] = TRIANGLE;
            arrayOfPrimitive[4] = TRIANGLE_STRIP;
            arrayOfPrimitive[5] = TRIANGLE_FAN;
        }

        private Primitive(int paramInt)
        {
            this.mID = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Mesh
 * JD-Core Version:        0.6.2
 */