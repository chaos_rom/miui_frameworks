package android.renderscript;

public class ProgramVertex extends Program
{
    ProgramVertex(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    public Element getInput(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mInputs.length))
            throw new IllegalArgumentException("Slot ID out of range.");
        return this.mInputs[paramInt];
    }

    public int getInputCount()
    {
        if (this.mInputs != null);
        for (int i = this.mInputs.length; ; i = 0)
            return i;
    }

    public static class Builder extends Program.BaseProgramBuilder
    {
        public Builder(RenderScript paramRenderScript)
        {
            super();
        }

        public Builder addInput(Element paramElement)
            throws IllegalStateException
        {
            if (this.mInputCount >= 8)
                throw new RSIllegalArgumentException("Max input count exceeded.");
            if (paramElement.isComplex())
                throw new RSIllegalArgumentException("Complex elements not allowed.");
            Element[] arrayOfElement = this.mInputs;
            int i = this.mInputCount;
            this.mInputCount = (i + 1);
            arrayOfElement[i] = paramElement;
            return this;
        }

        public ProgramVertex create()
        {
            this.mRS.validate();
            int[] arrayOfInt = new int[2 * (this.mInputCount + this.mOutputCount + this.mConstantCount + this.mTextureCount)];
            String[] arrayOfString = new String[this.mTextureCount];
            int i = 0;
            for (int j = 0; j < this.mInputCount; j++)
            {
                int i4 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.INPUT.mID;
                i = i4 + 1;
                arrayOfInt[i4] = this.mInputs[j].getID(this.mRS);
            }
            for (int k = 0; k < this.mOutputCount; k++)
            {
                int i3 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.OUTPUT.mID;
                i = i3 + 1;
                arrayOfInt[i3] = this.mOutputs[k].getID(this.mRS);
            }
            for (int m = 0; m < this.mConstantCount; m++)
            {
                int i2 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.CONSTANT.mID;
                i = i2 + 1;
                arrayOfInt[i2] = this.mConstants[m].getID(this.mRS);
            }
            for (int n = 0; n < this.mTextureCount; n++)
            {
                int i1 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.TEXTURE_TYPE.mID;
                i = i1 + 1;
                arrayOfInt[i1] = this.mTextureTypes[n].mID;
                arrayOfString[n] = this.mTextureNames[n];
            }
            ProgramVertex localProgramVertex = new ProgramVertex(this.mRS.nProgramVertexCreate(this.mShader, arrayOfString, arrayOfInt), this.mRS);
            initProgram(localProgramVertex);
            return localProgramVertex;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.ProgramVertex
 * JD-Core Version:        0.6.2
 */