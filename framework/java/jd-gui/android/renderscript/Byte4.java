package android.renderscript;

public class Byte4
{
    public byte w;
    public byte x;
    public byte y;
    public byte z;

    public Byte4()
    {
    }

    public Byte4(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4)
    {
        this.x = paramByte1;
        this.y = paramByte2;
        this.z = paramByte3;
        this.w = paramByte4;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Byte4
 * JD-Core Version:        0.6.2
 */