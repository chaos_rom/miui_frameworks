package android.renderscript;

public class Int2
{
    public int x;
    public int y;

    public Int2()
    {
    }

    public Int2(int paramInt1, int paramInt2)
    {
        this.x = paramInt1;
        this.y = paramInt2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Int2
 * JD-Core Version:        0.6.2
 */