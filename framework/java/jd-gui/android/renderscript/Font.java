package android.renderscript;

import android.content.res.AssetManager.AssetInputStream;
import android.content.res.Resources;
import android.os.Environment;
import android.util.DisplayMetrics;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Font extends BaseObj
{
    private static Map<String, FontFamily> sFontFamilyMap;
    private static final String[] sMonoNames;
    private static final String[] sSansNames;
    private static final String[] sSerifNames;

    static
    {
        String[] arrayOfString1 = new String[5];
        arrayOfString1[0] = "sans-serif";
        arrayOfString1[1] = "arial";
        arrayOfString1[2] = "helvetica";
        arrayOfString1[3] = "tahoma";
        arrayOfString1[4] = "verdana";
        sSansNames = arrayOfString1;
        String[] arrayOfString2 = new String[10];
        arrayOfString2[0] = "serif";
        arrayOfString2[1] = "times";
        arrayOfString2[2] = "times new roman";
        arrayOfString2[3] = "palatino";
        arrayOfString2[4] = "georgia";
        arrayOfString2[5] = "baskerville";
        arrayOfString2[6] = "goudy";
        arrayOfString2[7] = "fantasy";
        arrayOfString2[8] = "cursive";
        arrayOfString2[9] = "ITC Stone Serif";
        sSerifNames = arrayOfString2;
        String[] arrayOfString3 = new String[4];
        arrayOfString3[0] = "monospace";
        arrayOfString3[1] = "courier";
        arrayOfString3[2] = "courier new";
        arrayOfString3[3] = "monaco";
        sMonoNames = arrayOfString3;
        initFontFamilyMap();
    }

    Font(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    private static void addFamilyToMap(FontFamily paramFontFamily)
    {
        for (int i = 0; i < paramFontFamily.mNames.length; i++)
            sFontFamilyMap.put(paramFontFamily.mNames[i], paramFontFamily);
    }

    public static Font create(RenderScript paramRenderScript, Resources paramResources, String paramString, Style paramStyle, float paramFloat)
    {
        String str1 = getFontFileName(paramString, paramStyle);
        String str2 = Environment.getRootDirectory().getAbsolutePath();
        return createFromFile(paramRenderScript, paramResources, str2 + "/fonts/" + str1, paramFloat);
    }

    public static Font createFromAsset(RenderScript paramRenderScript, Resources paramResources, String paramString, float paramFloat)
    {
        paramRenderScript.validate();
        int i = paramRenderScript.nFontCreateFromAsset(paramResources.getAssets(), paramString, paramFloat, paramResources.getDisplayMetrics().densityDpi);
        if (i == 0)
            throw new RSRuntimeException("Unable to create font from asset " + paramString);
        return new Font(i, paramRenderScript);
    }

    public static Font createFromFile(RenderScript paramRenderScript, Resources paramResources, File paramFile, float paramFloat)
    {
        return createFromFile(paramRenderScript, paramResources, paramFile.getAbsolutePath(), paramFloat);
    }

    public static Font createFromFile(RenderScript paramRenderScript, Resources paramResources, String paramString, float paramFloat)
    {
        paramRenderScript.validate();
        int i = paramRenderScript.nFontCreateFromFile(paramString, paramFloat, paramResources.getDisplayMetrics().densityDpi);
        if (i == 0)
            throw new RSRuntimeException("Unable to create font from file " + paramString);
        return new Font(i, paramRenderScript);
    }

    public static Font createFromResource(RenderScript paramRenderScript, Resources paramResources, int paramInt, float paramFloat)
    {
        String str = "R." + Integer.toString(paramInt);
        paramRenderScript.validate();
        int j;
        try
        {
            InputStream localInputStream = paramResources.openRawResource(paramInt);
            int i = paramResources.getDisplayMetrics().densityDpi;
            if ((localInputStream instanceof AssetManager.AssetInputStream))
            {
                j = paramRenderScript.nFontCreateFromAssetStream(str, paramFloat, i, ((AssetManager.AssetInputStream)localInputStream).getAssetInt());
                if (j != 0)
                    break label142;
                throw new RSRuntimeException("Unable to create font from resource " + paramInt);
            }
        }
        catch (Exception localException)
        {
            throw new RSRuntimeException("Unable to open resource " + paramInt);
        }
        throw new RSRuntimeException("Unsupported asset stream created");
        label142: return new Font(j, paramRenderScript);
    }

    static String getFontFileName(String paramString, Style paramStyle)
    {
        FontFamily localFontFamily = (FontFamily)sFontFamilyMap.get(paramString);
        if (localFontFamily != null);
        String str;
        switch (1.$SwitchMap$android$renderscript$Font$Style[paramStyle.ordinal()])
        {
        default:
            str = "DroidSans.ttf";
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return str;
            str = localFontFamily.mNormalFileName;
            continue;
            str = localFontFamily.mBoldFileName;
            continue;
            str = localFontFamily.mItalicFileName;
            continue;
            str = localFontFamily.mBoldItalicFileName;
        }
    }

    private static void initFontFamilyMap()
    {
        sFontFamilyMap = new HashMap();
        FontFamily localFontFamily1 = new FontFamily(null);
        localFontFamily1.mNames = sSansNames;
        localFontFamily1.mNormalFileName = "Roboto-Regular.ttf";
        localFontFamily1.mBoldFileName = "Roboto-Bold.ttf";
        localFontFamily1.mItalicFileName = "Roboto-Italic.ttf";
        localFontFamily1.mBoldItalicFileName = "Roboto-BoldItalic.ttf";
        addFamilyToMap(localFontFamily1);
        FontFamily localFontFamily2 = new FontFamily(null);
        localFontFamily2.mNames = sSerifNames;
        localFontFamily2.mNormalFileName = "DroidSerif-Regular.ttf";
        localFontFamily2.mBoldFileName = "DroidSerif-Bold.ttf";
        localFontFamily2.mItalicFileName = "DroidSerif-Italic.ttf";
        localFontFamily2.mBoldItalicFileName = "DroidSerif-BoldItalic.ttf";
        addFamilyToMap(localFontFamily2);
        FontFamily localFontFamily3 = new FontFamily(null);
        localFontFamily3.mNames = sMonoNames;
        localFontFamily3.mNormalFileName = "DroidSansMono.ttf";
        localFontFamily3.mBoldFileName = "DroidSansMono.ttf";
        localFontFamily3.mItalicFileName = "DroidSansMono.ttf";
        localFontFamily3.mBoldItalicFileName = "DroidSansMono.ttf";
        addFamilyToMap(localFontFamily3);
    }

    public static enum Style
    {
        static
        {
            BOLD = new Style("BOLD", 1);
            ITALIC = new Style("ITALIC", 2);
            BOLD_ITALIC = new Style("BOLD_ITALIC", 3);
            Style[] arrayOfStyle = new Style[4];
            arrayOfStyle[0] = NORMAL;
            arrayOfStyle[1] = BOLD;
            arrayOfStyle[2] = ITALIC;
            arrayOfStyle[3] = BOLD_ITALIC;
        }
    }

    private static class FontFamily
    {
        String mBoldFileName;
        String mBoldItalicFileName;
        String mItalicFileName;
        String[] mNames;
        String mNormalFileName;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Font
 * JD-Core Version:        0.6.2
 */