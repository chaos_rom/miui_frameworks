package android.renderscript;

import android.content.res.Resources;

public class ScriptC extends Script
{
    private static final String TAG = "ScriptC";

    protected ScriptC(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    protected ScriptC(RenderScript paramRenderScript, Resources paramResources, int paramInt)
    {
        super(0, paramRenderScript);
        int i = internalCreate(paramRenderScript, paramResources, paramInt);
        if (i == 0)
            throw new RSRuntimeException("Loading of ScriptC script failed.");
        setID(i);
    }

    /** @deprecated */
    // ERROR //
    private static int internalCreate(RenderScript paramRenderScript, Resources paramResources, int paramInt)
    {
        // Byte code:
        //     0: ldc 2
        //     2: monitorenter
        //     3: aload_1
        //     4: iload_2
        //     5: invokevirtual 36	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
        //     8: astore 4
        //     10: sipush 1024
        //     13: newarray byte
        //     15: astore 7
        //     17: iconst_0
        //     18: istore 8
        //     20: aload 7
        //     22: arraylength
        //     23: iload 8
        //     25: isub
        //     26: istore 9
        //     28: iload 9
        //     30: ifne +36 -> 66
        //     33: iconst_2
        //     34: aload 7
        //     36: arraylength
        //     37: imul
        //     38: newarray byte
        //     40: astore 14
        //     42: aload 7
        //     44: iconst_0
        //     45: aload 14
        //     47: iconst_0
        //     48: aload 7
        //     50: arraylength
        //     51: invokestatic 42	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     54: aload 14
        //     56: astore 7
        //     58: aload 7
        //     60: arraylength
        //     61: iload 8
        //     63: isub
        //     64: istore 9
        //     66: aload 4
        //     68: aload 7
        //     70: iload 8
        //     72: iload 9
        //     74: invokevirtual 48	java/io/InputStream:read	([BII)I
        //     77: istore 10
        //     79: iload 10
        //     81: ifgt +62 -> 143
        //     84: aload 4
        //     86: invokevirtual 52	java/io/InputStream:close	()V
        //     89: aload_1
        //     90: iload_2
        //     91: invokevirtual 56	android/content/res/Resources:getResourceEntryName	(I)Ljava/lang/String;
        //     94: astore 11
        //     96: ldc 8
        //     98: new 58	java/lang/StringBuilder
        //     101: dup
        //     102: invokespecial 60	java/lang/StringBuilder:<init>	()V
        //     105: ldc 62
        //     107: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     110: aload 11
        //     112: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     115: invokevirtual 70	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     118: invokestatic 76	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     121: pop
        //     122: aload_0
        //     123: aload 11
        //     125: getstatic 81	android/renderscript/RenderScript:mCachePath	Ljava/lang/String;
        //     128: aload 7
        //     130: iload 8
        //     132: invokevirtual 85	android/renderscript/RenderScript:nScriptCCreate	(Ljava/lang/String;Ljava/lang/String;[BI)I
        //     135: istore 13
        //     137: ldc 2
        //     139: monitorexit
        //     140: iload 13
        //     142: ireturn
        //     143: iload 8
        //     145: iload 10
        //     147: iadd
        //     148: istore 8
        //     150: goto -130 -> 20
        //     153: astore 5
        //     155: aload 4
        //     157: invokevirtual 52	java/io/InputStream:close	()V
        //     160: aload 5
        //     162: athrow
        //     163: astore 6
        //     165: new 87	android/content/res/Resources$NotFoundException
        //     168: dup
        //     169: invokespecial 88	android/content/res/Resources$NotFoundException:<init>	()V
        //     172: athrow
        //     173: astore_3
        //     174: ldc 2
        //     176: monitorexit
        //     177: aload_3
        //     178: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     10	79	153	finally
        //     84	89	163	java/io/IOException
        //     155	163	163	java/io/IOException
        //     3	10	173	finally
        //     84	89	173	finally
        //     89	137	173	finally
        //     155	163	173	finally
        //     165	173	173	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.ScriptC
 * JD-Core Version:        0.6.2
 */