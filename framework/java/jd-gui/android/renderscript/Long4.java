package android.renderscript;

public class Long4
{
    public long w;
    public long x;
    public long y;
    public long z;

    public Long4()
    {
    }

    public Long4(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
    {
        this.x = paramLong1;
        this.y = paramLong2;
        this.z = paramLong3;
        this.w = paramLong4;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Long4
 * JD-Core Version:        0.6.2
 */