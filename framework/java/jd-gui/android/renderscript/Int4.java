package android.renderscript;

public class Int4
{
    public int w;
    public int x;
    public int y;
    public int z;

    public Int4()
    {
    }

    public Int4(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.x = paramInt1;
        this.y = paramInt2;
        this.z = paramInt3;
        this.w = paramInt4;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Int4
 * JD-Core Version:        0.6.2
 */