package android.renderscript;

public class Short4
{
    public short w;
    public short x;
    public short y;
    public short z;

    public Short4()
    {
    }

    public Short4(short paramShort1, short paramShort2, short paramShort3, short paramShort4)
    {
        this.x = paramShort1;
        this.y = paramShort2;
        this.z = paramShort3;
        this.w = paramShort4;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Short4
 * JD-Core Version:        0.6.2
 */