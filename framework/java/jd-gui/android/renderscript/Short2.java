package android.renderscript;

public class Short2
{
    public short x;
    public short y;

    public Short2()
    {
    }

    public Short2(short paramShort1, short paramShort2)
    {
        this.x = paramShort1;
        this.y = paramShort2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Short2
 * JD-Core Version:        0.6.2
 */