package android.renderscript;

import java.io.UnsupportedEncodingException;

public class BaseObj
{
    private boolean mDestroyed;
    private int mID;
    private String mName;
    RenderScript mRS;

    BaseObj(int paramInt, RenderScript paramRenderScript)
    {
        paramRenderScript.validate();
        this.mRS = paramRenderScript;
        this.mID = paramInt;
        this.mDestroyed = false;
    }

    void checkValid()
    {
        if (this.mID == 0)
            throw new RSIllegalArgumentException("Invalid object.");
    }

    /** @deprecated */
    public void destroy()
    {
        try
        {
            if (this.mDestroyed)
                throw new RSInvalidStateException("Object already destroyed.");
        }
        finally
        {
        }
        this.mDestroyed = true;
        this.mRS.nObjDestroy(this.mID);
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if (getClass() != paramObject.getClass())
            {
                bool = false;
            }
            else
            {
                BaseObj localBaseObj = (BaseObj)paramObject;
                if (this.mID != localBaseObj.mID)
                    bool = false;
            }
        }
    }

    protected void finalize()
        throws Throwable
    {
        if (!this.mDestroyed)
        {
            if ((this.mID != 0) && (this.mRS.isAlive()))
                this.mRS.nObjDestroy(this.mID);
            this.mRS = null;
            this.mID = 0;
            this.mDestroyed = true;
        }
        super.finalize();
    }

    int getID(RenderScript paramRenderScript)
    {
        this.mRS.validate();
        if (this.mDestroyed)
            throw new RSInvalidStateException("using a destroyed object.");
        if (this.mID == 0)
            throw new RSRuntimeException("Internal error: Object id 0.");
        if ((paramRenderScript != null) && (paramRenderScript != this.mRS))
            throw new RSInvalidStateException("using object with mismatched context.");
        return this.mID;
    }

    public String getName()
    {
        return this.mName;
    }

    public int hashCode()
    {
        return this.mID;
    }

    void setID(int paramInt)
    {
        if (this.mID != 0)
            throw new RSRuntimeException("Internal Error, reset of object ID.");
        this.mID = paramInt;
    }

    public void setName(String paramString)
    {
        if (paramString == null)
            throw new RSIllegalArgumentException("setName requires a string of non-zero length.");
        if (paramString.length() < 1)
            throw new RSIllegalArgumentException("setName does not accept a zero length string.");
        if (this.mName != null)
            throw new RSIllegalArgumentException("setName object already has a name.");
        try
        {
            byte[] arrayOfByte = paramString.getBytes("UTF-8");
            this.mRS.nAssignName(this.mID, arrayOfByte);
            this.mName = paramString;
            return;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
            throw new RuntimeException(localUnsupportedEncodingException);
        }
    }

    void updateFromNative()
    {
        this.mRS.validate();
        this.mName = this.mRS.nGetName(getID(this.mRS));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.BaseObj
 * JD-Core Version:        0.6.2
 */