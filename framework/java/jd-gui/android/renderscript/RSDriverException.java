package android.renderscript;

public class RSDriverException extends RSRuntimeException
{
    public RSDriverException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.RSDriverException
 * JD-Core Version:        0.6.2
 */