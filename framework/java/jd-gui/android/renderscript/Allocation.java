package android.renderscript;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.SurfaceTexture;
import android.view.Surface;

public class Allocation extends BaseObj
{
    public static final int USAGE_GRAPHICS_CONSTANTS = 8;
    public static final int USAGE_GRAPHICS_RENDER_TARGET = 16;
    public static final int USAGE_GRAPHICS_TEXTURE = 2;
    public static final int USAGE_GRAPHICS_VERTEX = 4;
    public static final int USAGE_IO_INPUT = 32;
    public static final int USAGE_IO_OUTPUT = 64;
    public static final int USAGE_SCRIPT = 1;
    static BitmapFactory.Options mBitmapOptions = new BitmapFactory.Options();
    Allocation mAdaptedAllocation;
    Bitmap mBitmap;
    boolean mConstrainedFace;
    boolean mConstrainedLOD;
    boolean mConstrainedY;
    boolean mConstrainedZ;
    int mCurrentCount;
    int mCurrentDimX;
    int mCurrentDimY;
    int mCurrentDimZ;
    boolean mReadAllowed = true;
    Type.CubemapFace mSelectedFace = Type.CubemapFace.POSITIVE_X;
    int mSelectedLOD;
    int mSelectedY;
    int mSelectedZ;
    Type mType;
    int mUsage;
    boolean mWriteAllowed = true;

    static
    {
        mBitmapOptions.inScaled = false;
    }

    Allocation(int paramInt1, RenderScript paramRenderScript, Type paramType, int paramInt2)
    {
        super(paramInt1, paramRenderScript);
        if ((paramInt2 & 0xFFFFFF80) != 0)
            throw new RSIllegalArgumentException("Unknown usage specified.");
        if ((paramInt2 & 0x20) != 0)
        {
            this.mWriteAllowed = false;
            if ((paramInt2 & 0xFFFFFFDC) != 0)
                throw new RSIllegalArgumentException("Invalid usage combination.");
        }
        this.mType = paramType;
        this.mUsage = paramInt2;
        if (paramType != null)
            updateCacheInfo(paramType);
    }

    public static Allocation createCubemapFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap)
    {
        return createCubemapFromBitmap(paramRenderScript, paramBitmap, MipmapControl.MIPMAP_NONE, 2);
    }

    public static Allocation createCubemapFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap, MipmapControl paramMipmapControl, int paramInt)
    {
        boolean bool1 = true;
        paramRenderScript.validate();
        int i = paramBitmap.getHeight();
        int j = paramBitmap.getWidth();
        if (j % 6 != 0)
            throw new RSIllegalArgumentException("Cubemap height must be multiple of 6");
        if (j / 6 != i)
            throw new RSIllegalArgumentException("Only square cube map faces supported");
        if ((i & i - 1) == 0);
        for (boolean bool2 = bool1; !bool2; bool2 = false)
            throw new RSIllegalArgumentException("Only power of 2 cube faces supported");
        Element localElement = elementFromBitmap(paramRenderScript, paramBitmap);
        Type.Builder localBuilder = new Type.Builder(paramRenderScript, localElement);
        localBuilder.setX(i);
        localBuilder.setY(i);
        localBuilder.setFaces(bool1);
        if (paramMipmapControl == MipmapControl.MIPMAP_FULL);
        Type localType;
        int k;
        while (true)
        {
            localBuilder.setMipmaps(bool1);
            localType = localBuilder.create();
            k = paramRenderScript.nAllocationCubeCreateFromBitmap(localType.getID(paramRenderScript), paramMipmapControl.mID, paramBitmap, paramInt);
            if (k != 0)
                break;
            throw new RSRuntimeException("Load failed for bitmap " + paramBitmap + " element " + localElement);
            bool1 = false;
        }
        return new Allocation(k, paramRenderScript, localType, paramInt);
    }

    public static Allocation createCubemapFromCubeFaces(RenderScript paramRenderScript, Bitmap paramBitmap1, Bitmap paramBitmap2, Bitmap paramBitmap3, Bitmap paramBitmap4, Bitmap paramBitmap5, Bitmap paramBitmap6)
    {
        return createCubemapFromCubeFaces(paramRenderScript, paramBitmap1, paramBitmap2, paramBitmap3, paramBitmap4, paramBitmap5, paramBitmap6, MipmapControl.MIPMAP_NONE, 2);
    }

    public static Allocation createCubemapFromCubeFaces(RenderScript paramRenderScript, Bitmap paramBitmap1, Bitmap paramBitmap2, Bitmap paramBitmap3, Bitmap paramBitmap4, Bitmap paramBitmap5, Bitmap paramBitmap6, MipmapControl paramMipmapControl, int paramInt)
    {
        int i = paramBitmap1.getHeight();
        if ((paramBitmap1.getWidth() != i) || (paramBitmap2.getWidth() != i) || (paramBitmap2.getHeight() != i) || (paramBitmap3.getWidth() != i) || (paramBitmap3.getHeight() != i) || (paramBitmap4.getWidth() != i) || (paramBitmap4.getHeight() != i) || (paramBitmap5.getWidth() != i) || (paramBitmap5.getHeight() != i) || (paramBitmap6.getWidth() != i) || (paramBitmap6.getHeight() != i))
            throw new RSIllegalArgumentException("Only square cube map faces supported");
        if ((i & i - 1) == 0);
        for (int j = 1; j == 0; j = 0)
            throw new RSIllegalArgumentException("Only power of 2 cube faces supported");
        Type.Builder localBuilder = new Type.Builder(paramRenderScript, elementFromBitmap(paramRenderScript, paramBitmap1));
        localBuilder.setX(i);
        localBuilder.setY(i);
        localBuilder.setFaces(true);
        if (paramMipmapControl == MipmapControl.MIPMAP_FULL);
        for (boolean bool = true; ; bool = false)
        {
            localBuilder.setMipmaps(bool);
            Allocation localAllocation = createTyped(paramRenderScript, localBuilder.create(), paramMipmapControl, paramInt);
            AllocationAdapter localAllocationAdapter = AllocationAdapter.create2D(paramRenderScript, localAllocation);
            localAllocationAdapter.setFace(Type.CubemapFace.POSITIVE_X);
            localAllocationAdapter.copyFrom(paramBitmap1);
            localAllocationAdapter.setFace(Type.CubemapFace.NEGATIVE_X);
            localAllocationAdapter.copyFrom(paramBitmap2);
            localAllocationAdapter.setFace(Type.CubemapFace.POSITIVE_Y);
            localAllocationAdapter.copyFrom(paramBitmap3);
            localAllocationAdapter.setFace(Type.CubemapFace.NEGATIVE_Y);
            localAllocationAdapter.copyFrom(paramBitmap4);
            localAllocationAdapter.setFace(Type.CubemapFace.POSITIVE_Z);
            localAllocationAdapter.copyFrom(paramBitmap5);
            localAllocationAdapter.setFace(Type.CubemapFace.NEGATIVE_Z);
            localAllocationAdapter.copyFrom(paramBitmap6);
            return localAllocation;
        }
    }

    public static Allocation createFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap)
    {
        return createFromBitmap(paramRenderScript, paramBitmap, MipmapControl.MIPMAP_NONE, 2);
    }

    public static Allocation createFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap, MipmapControl paramMipmapControl, int paramInt)
    {
        paramRenderScript.validate();
        Type localType = typeFromBitmap(paramRenderScript, paramBitmap, paramMipmapControl);
        int i = paramRenderScript.nAllocationCreateFromBitmap(localType.getID(paramRenderScript), paramMipmapControl.mID, paramBitmap, paramInt);
        if (i == 0)
            throw new RSRuntimeException("Load failed.");
        return new Allocation(i, paramRenderScript, localType, paramInt);
    }

    public static Allocation createFromBitmapResource(RenderScript paramRenderScript, Resources paramResources, int paramInt)
    {
        return createFromBitmapResource(paramRenderScript, paramResources, paramInt, MipmapControl.MIPMAP_NONE, 2);
    }

    public static Allocation createFromBitmapResource(RenderScript paramRenderScript, Resources paramResources, int paramInt1, MipmapControl paramMipmapControl, int paramInt2)
    {
        paramRenderScript.validate();
        Bitmap localBitmap = BitmapFactory.decodeResource(paramResources, paramInt1);
        Allocation localAllocation = createFromBitmap(paramRenderScript, localBitmap, paramMipmapControl, paramInt2);
        localBitmap.recycle();
        return localAllocation;
    }

    public static Allocation createFromString(RenderScript paramRenderScript, String paramString, int paramInt)
    {
        paramRenderScript.validate();
        try
        {
            byte[] arrayOfByte = paramString.getBytes("UTF-8");
            Allocation localAllocation = createSized(paramRenderScript, Element.U8(paramRenderScript), arrayOfByte.length, paramInt);
            localAllocation.copyFrom(arrayOfByte);
            return localAllocation;
        }
        catch (Exception localException)
        {
        }
        throw new RSRuntimeException("Could not convert string to utf-8.");
    }

    public static Allocation createSized(RenderScript paramRenderScript, Element paramElement, int paramInt)
    {
        return createSized(paramRenderScript, paramElement, paramInt, 1);
    }

    public static Allocation createSized(RenderScript paramRenderScript, Element paramElement, int paramInt1, int paramInt2)
    {
        paramRenderScript.validate();
        Type.Builder localBuilder = new Type.Builder(paramRenderScript, paramElement);
        localBuilder.setX(paramInt1);
        Type localType = localBuilder.create();
        int i = paramRenderScript.nAllocationCreateTyped(localType.getID(paramRenderScript), MipmapControl.MIPMAP_NONE.mID, paramInt2, 0);
        if (i == 0)
            throw new RSRuntimeException("Allocation creation failed.");
        return new Allocation(i, paramRenderScript, localType, paramInt2);
    }

    public static Allocation createTyped(RenderScript paramRenderScript, Type paramType)
    {
        return createTyped(paramRenderScript, paramType, MipmapControl.MIPMAP_NONE, 1);
    }

    public static Allocation createTyped(RenderScript paramRenderScript, Type paramType, int paramInt)
    {
        return createTyped(paramRenderScript, paramType, MipmapControl.MIPMAP_NONE, paramInt);
    }

    public static Allocation createTyped(RenderScript paramRenderScript, Type paramType, MipmapControl paramMipmapControl, int paramInt)
    {
        paramRenderScript.validate();
        if (paramType.getID(paramRenderScript) == 0)
            throw new RSInvalidStateException("Bad Type");
        int i = paramRenderScript.nAllocationCreateTyped(paramType.getID(paramRenderScript), paramMipmapControl.mID, paramInt, 0);
        if (i == 0)
            throw new RSRuntimeException("Allocation creation failed.");
        return new Allocation(i, paramRenderScript, paramType, paramInt);
    }

    private void data1DChecks(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mRS.validate();
        if (paramInt1 < 0)
            throw new RSIllegalArgumentException("Offset must be >= 0.");
        if (paramInt2 < 1)
            throw new RSIllegalArgumentException("Count must be >= 1.");
        if (paramInt1 + paramInt2 > this.mCurrentCount)
            throw new RSIllegalArgumentException("Overflow, Available count " + this.mCurrentCount + ", got " + paramInt2 + " at offset " + paramInt1 + ".");
        if (paramInt3 < paramInt4)
            throw new RSIllegalArgumentException("Array too small for allocation type.");
    }

    static Element elementFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap)
    {
        Bitmap.Config localConfig = paramBitmap.getConfig();
        Element localElement;
        if (localConfig == Bitmap.Config.ALPHA_8)
            localElement = Element.A_8(paramRenderScript);
        while (true)
        {
            return localElement;
            if (localConfig == Bitmap.Config.ARGB_4444)
            {
                localElement = Element.RGBA_4444(paramRenderScript);
            }
            else if (localConfig == Bitmap.Config.ARGB_8888)
            {
                localElement = Element.RGBA_8888(paramRenderScript);
            }
            else
            {
                if (localConfig != Bitmap.Config.RGB_565)
                    break;
                localElement = Element.RGB_565(paramRenderScript);
            }
        }
        throw new RSInvalidStateException("Bad bitmap type: " + localConfig);
    }

    private int getIDSafe()
    {
        if (this.mAdaptedAllocation != null);
        for (int i = this.mAdaptedAllocation.getID(this.mRS); ; i = getID(this.mRS))
            return i;
    }

    static Type typeFromBitmap(RenderScript paramRenderScript, Bitmap paramBitmap, MipmapControl paramMipmapControl)
    {
        Type.Builder localBuilder = new Type.Builder(paramRenderScript, elementFromBitmap(paramRenderScript, paramBitmap));
        localBuilder.setX(paramBitmap.getWidth());
        localBuilder.setY(paramBitmap.getHeight());
        if (paramMipmapControl == MipmapControl.MIPMAP_FULL);
        for (boolean bool = true; ; bool = false)
        {
            localBuilder.setMipmaps(bool);
            return localBuilder.create();
        }
    }

    private void updateCacheInfo(Type paramType)
    {
        this.mCurrentDimX = paramType.getX();
        this.mCurrentDimY = paramType.getY();
        this.mCurrentDimZ = paramType.getZ();
        this.mCurrentCount = this.mCurrentDimX;
        if (this.mCurrentDimY > 1)
            this.mCurrentCount *= this.mCurrentDimY;
        if (this.mCurrentDimZ > 1)
            this.mCurrentCount *= this.mCurrentDimZ;
    }

    private void validate2DRange(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (this.mAdaptedAllocation != null);
        do
        {
            return;
            if ((paramInt1 < 0) || (paramInt2 < 0))
                throw new RSIllegalArgumentException("Offset cannot be negative.");
            if ((paramInt4 < 0) || (paramInt3 < 0))
                throw new RSIllegalArgumentException("Height or width cannot be negative.");
        }
        while ((paramInt1 + paramInt3 <= this.mCurrentDimX) && (paramInt2 + paramInt4 <= this.mCurrentDimY));
        throw new RSIllegalArgumentException("Updated region larger than allocation.");
    }

    private void validateBitmapFormat(Bitmap paramBitmap)
    {
        Bitmap.Config localConfig = paramBitmap.getConfig();
        switch (1.$SwitchMap$android$graphics$Bitmap$Config[localConfig.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        do
        {
            do
            {
                do
                {
                    do
                        return;
                    while (this.mType.getElement().mKind == Element.DataKind.PIXEL_A);
                    throw new RSIllegalArgumentException("Allocation kind is " + this.mType.getElement().mKind + ", type " + this.mType.getElement().mType + " of " + this.mType.getElement().getBytesSize() + " bytes, passed bitmap was " + localConfig);
                }
                while ((this.mType.getElement().mKind == Element.DataKind.PIXEL_RGBA) && (this.mType.getElement().getBytesSize() == 4));
                throw new RSIllegalArgumentException("Allocation kind is " + this.mType.getElement().mKind + ", type " + this.mType.getElement().mType + " of " + this.mType.getElement().getBytesSize() + " bytes, passed bitmap was " + localConfig);
            }
            while ((this.mType.getElement().mKind == Element.DataKind.PIXEL_RGB) && (this.mType.getElement().getBytesSize() == 2));
            throw new RSIllegalArgumentException("Allocation kind is " + this.mType.getElement().mKind + ", type " + this.mType.getElement().mType + " of " + this.mType.getElement().getBytesSize() + " bytes, passed bitmap was " + localConfig);
        }
        while ((this.mType.getElement().mKind == Element.DataKind.PIXEL_RGBA) && (this.mType.getElement().getBytesSize() == 2));
        throw new RSIllegalArgumentException("Allocation kind is " + this.mType.getElement().mKind + ", type " + this.mType.getElement().mType + " of " + this.mType.getElement().getBytesSize() + " bytes, passed bitmap was " + localConfig);
    }

    private void validateBitmapSize(Bitmap paramBitmap)
    {
        if ((this.mCurrentDimX != paramBitmap.getWidth()) || (this.mCurrentDimY != paramBitmap.getHeight()))
            throw new RSIllegalArgumentException("Cannot update allocation from bitmap, sizes mismatch");
    }

    private void validateIsFloat32()
    {
        if (this.mType.mElement.mType == Element.DataType.FLOAT_32)
            return;
        throw new RSIllegalArgumentException("32 bit float source does not match allocation type " + this.mType.mElement.mType);
    }

    private void validateIsInt16()
    {
        if ((this.mType.mElement.mType == Element.DataType.SIGNED_16) || (this.mType.mElement.mType == Element.DataType.UNSIGNED_16))
            return;
        throw new RSIllegalArgumentException("16 bit integer source does not match allocation type " + this.mType.mElement.mType);
    }

    private void validateIsInt32()
    {
        if ((this.mType.mElement.mType == Element.DataType.SIGNED_32) || (this.mType.mElement.mType == Element.DataType.UNSIGNED_32))
            return;
        throw new RSIllegalArgumentException("32 bit integer source does not match allocation type " + this.mType.mElement.mType);
    }

    private void validateIsInt8()
    {
        if ((this.mType.mElement.mType == Element.DataType.SIGNED_8) || (this.mType.mElement.mType == Element.DataType.UNSIGNED_8))
            return;
        throw new RSIllegalArgumentException("8 bit integer source does not match allocation type " + this.mType.mElement.mType);
    }

    private void validateIsObject()
    {
        if ((this.mType.mElement.mType == Element.DataType.RS_ELEMENT) || (this.mType.mElement.mType == Element.DataType.RS_TYPE) || (this.mType.mElement.mType == Element.DataType.RS_ALLOCATION) || (this.mType.mElement.mType == Element.DataType.RS_SAMPLER) || (this.mType.mElement.mType == Element.DataType.RS_SCRIPT) || (this.mType.mElement.mType == Element.DataType.RS_MESH) || (this.mType.mElement.mType == Element.DataType.RS_PROGRAM_FRAGMENT) || (this.mType.mElement.mType == Element.DataType.RS_PROGRAM_VERTEX) || (this.mType.mElement.mType == Element.DataType.RS_PROGRAM_RASTER) || (this.mType.mElement.mType == Element.DataType.RS_PROGRAM_STORE))
            return;
        throw new RSIllegalArgumentException("Object source does not match allocation type " + this.mType.mElement.mType);
    }

    public void copy1DRangeFrom(int paramInt1, int paramInt2, Allocation paramAllocation, int paramInt3)
    {
        this.mRS.nAllocationData2D(getIDSafe(), paramInt1, 0, this.mSelectedLOD, this.mSelectedFace.mID, paramInt2, 1, paramAllocation.getID(this.mRS), paramInt3, 0, paramAllocation.mSelectedLOD, paramAllocation.mSelectedFace.mID);
    }

    public void copy1DRangeFrom(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        validateIsInt8();
        copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfByte);
    }

    public void copy1DRangeFrom(int paramInt1, int paramInt2, float[] paramArrayOfFloat)
    {
        validateIsFloat32();
        copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfFloat);
    }

    public void copy1DRangeFrom(int paramInt1, int paramInt2, int[] paramArrayOfInt)
    {
        validateIsInt32();
        copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfInt);
    }

    public void copy1DRangeFrom(int paramInt1, int paramInt2, short[] paramArrayOfShort)
    {
        validateIsInt16();
        copy1DRangeFromUnchecked(paramInt1, paramInt2, paramArrayOfShort);
    }

    public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        int i = paramInt2 * this.mType.mElement.getBytesSize();
        data1DChecks(paramInt1, paramInt2, paramArrayOfByte.length, i);
        this.mRS.nAllocationData1D(getIDSafe(), paramInt1, this.mSelectedLOD, paramInt2, paramArrayOfByte, i);
    }

    public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, float[] paramArrayOfFloat)
    {
        int i = paramInt2 * this.mType.mElement.getBytesSize();
        data1DChecks(paramInt1, paramInt2, 4 * paramArrayOfFloat.length, i);
        this.mRS.nAllocationData1D(getIDSafe(), paramInt1, this.mSelectedLOD, paramInt2, paramArrayOfFloat, i);
    }

    public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, int[] paramArrayOfInt)
    {
        int i = paramInt2 * this.mType.mElement.getBytesSize();
        data1DChecks(paramInt1, paramInt2, 4 * paramArrayOfInt.length, i);
        this.mRS.nAllocationData1D(getIDSafe(), paramInt1, this.mSelectedLOD, paramInt2, paramArrayOfInt, i);
    }

    public void copy1DRangeFromUnchecked(int paramInt1, int paramInt2, short[] paramArrayOfShort)
    {
        int i = paramInt2 * this.mType.mElement.getBytesSize();
        data1DChecks(paramInt1, paramInt2, 2 * paramArrayOfShort.length, i);
        this.mRS.nAllocationData1D(getIDSafe(), paramInt1, this.mSelectedLOD, paramInt2, paramArrayOfShort, i);
    }

    public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Allocation paramAllocation, int paramInt5, int paramInt6)
    {
        this.mRS.validate();
        validate2DRange(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mRS.nAllocationData2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramInt3, paramInt4, paramAllocation.getID(this.mRS), paramInt5, paramInt6, paramAllocation.mSelectedLOD, paramAllocation.mSelectedFace.mID);
    }

    public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte)
    {
        this.mRS.validate();
        validate2DRange(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mRS.nAllocationData2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramInt3, paramInt4, paramArrayOfByte, paramArrayOfByte.length);
    }

    public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float[] paramArrayOfFloat)
    {
        this.mRS.validate();
        validate2DRange(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mRS.nAllocationData2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramInt3, paramInt4, paramArrayOfFloat, 4 * paramArrayOfFloat.length);
    }

    public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
    {
        this.mRS.validate();
        validate2DRange(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mRS.nAllocationData2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramInt3, paramInt4, paramArrayOfInt, 4 * paramArrayOfInt.length);
    }

    public void copy2DRangeFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4, short[] paramArrayOfShort)
    {
        this.mRS.validate();
        validate2DRange(paramInt1, paramInt2, paramInt3, paramInt4);
        this.mRS.nAllocationData2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramInt3, paramInt4, paramArrayOfShort, 2 * paramArrayOfShort.length);
    }

    public void copy2DRangeFrom(int paramInt1, int paramInt2, Bitmap paramBitmap)
    {
        this.mRS.validate();
        validateBitmapFormat(paramBitmap);
        validate2DRange(paramInt1, paramInt2, paramBitmap.getWidth(), paramBitmap.getHeight());
        this.mRS.nAllocationData2D(getIDSafe(), paramInt1, paramInt2, this.mSelectedLOD, this.mSelectedFace.mID, paramBitmap);
    }

    public void copyFrom(Bitmap paramBitmap)
    {
        this.mRS.validate();
        validateBitmapSize(paramBitmap);
        validateBitmapFormat(paramBitmap);
        this.mRS.nAllocationCopyFromBitmap(getID(this.mRS), paramBitmap);
    }

    public void copyFrom(byte[] paramArrayOfByte)
    {
        this.mRS.validate();
        copy1DRangeFrom(0, this.mCurrentCount, paramArrayOfByte);
    }

    public void copyFrom(float[] paramArrayOfFloat)
    {
        this.mRS.validate();
        copy1DRangeFrom(0, this.mCurrentCount, paramArrayOfFloat);
    }

    public void copyFrom(int[] paramArrayOfInt)
    {
        this.mRS.validate();
        copy1DRangeFrom(0, this.mCurrentCount, paramArrayOfInt);
    }

    public void copyFrom(BaseObj[] paramArrayOfBaseObj)
    {
        this.mRS.validate();
        validateIsObject();
        if (paramArrayOfBaseObj.length != this.mCurrentCount)
            throw new RSIllegalArgumentException("Array size mismatch, allocation sizeX = " + this.mCurrentCount + ", array length = " + paramArrayOfBaseObj.length);
        int[] arrayOfInt = new int[paramArrayOfBaseObj.length];
        for (int i = 0; i < paramArrayOfBaseObj.length; i++)
            arrayOfInt[i] = paramArrayOfBaseObj[i].getID(this.mRS);
        copy1DRangeFromUnchecked(0, this.mCurrentCount, arrayOfInt);
    }

    public void copyFrom(short[] paramArrayOfShort)
    {
        this.mRS.validate();
        copy1DRangeFrom(0, this.mCurrentCount, paramArrayOfShort);
    }

    public void copyFromUnchecked(byte[] paramArrayOfByte)
    {
        this.mRS.validate();
        copy1DRangeFromUnchecked(0, this.mCurrentCount, paramArrayOfByte);
    }

    public void copyFromUnchecked(float[] paramArrayOfFloat)
    {
        this.mRS.validate();
        copy1DRangeFromUnchecked(0, this.mCurrentCount, paramArrayOfFloat);
    }

    public void copyFromUnchecked(int[] paramArrayOfInt)
    {
        this.mRS.validate();
        copy1DRangeFromUnchecked(0, this.mCurrentCount, paramArrayOfInt);
    }

    public void copyFromUnchecked(short[] paramArrayOfShort)
    {
        this.mRS.validate();
        copy1DRangeFromUnchecked(0, this.mCurrentCount, paramArrayOfShort);
    }

    public void copyTo(Bitmap paramBitmap)
    {
        this.mRS.validate();
        validateBitmapFormat(paramBitmap);
        validateBitmapSize(paramBitmap);
        this.mRS.nAllocationCopyToBitmap(getID(this.mRS), paramBitmap);
    }

    public void copyTo(byte[] paramArrayOfByte)
    {
        validateIsInt8();
        this.mRS.validate();
        this.mRS.nAllocationRead(getID(this.mRS), paramArrayOfByte);
    }

    public void copyTo(float[] paramArrayOfFloat)
    {
        validateIsFloat32();
        this.mRS.validate();
        this.mRS.nAllocationRead(getID(this.mRS), paramArrayOfFloat);
    }

    public void copyTo(int[] paramArrayOfInt)
    {
        validateIsInt32();
        this.mRS.validate();
        this.mRS.nAllocationRead(getID(this.mRS), paramArrayOfInt);
    }

    public void copyTo(short[] paramArrayOfShort)
    {
        validateIsInt16();
        this.mRS.validate();
        this.mRS.nAllocationRead(getID(this.mRS), paramArrayOfShort);
    }

    public void generateMipmaps()
    {
        this.mRS.nAllocationGenerateMipmaps(getID(this.mRS));
    }

    public int getBytesSize()
    {
        return this.mType.getCount() * this.mType.getElement().getBytesSize();
    }

    public Element getElement()
    {
        return this.mType.getElement();
    }

    public Surface getSurface()
    {
        return new Surface(getSurfaceTexture());
    }

    public SurfaceTexture getSurfaceTexture()
    {
        if ((0x20 & this.mUsage) == 0)
            throw new RSInvalidStateException("Allocation is not a surface texture.");
        SurfaceTexture localSurfaceTexture = new SurfaceTexture(this.mRS.nAllocationGetSurfaceTextureID(getID(this.mRS)));
        this.mRS.nAllocationGetSurfaceTextureID2(getID(this.mRS), localSurfaceTexture);
        return localSurfaceTexture;
    }

    public Type getType()
    {
        return this.mType;
    }

    public int getUsage()
    {
        return this.mUsage;
    }

    public void ioReceive()
    {
        if ((0x20 & this.mUsage) == 0)
            throw new RSIllegalArgumentException("Can only receive if IO_INPUT usage specified.");
        this.mRS.validate();
        this.mRS.nAllocationIoReceive(getID(this.mRS));
    }

    public void ioSend()
    {
        if ((0x40 & this.mUsage) == 0)
            throw new RSIllegalArgumentException("Can only send buffer if IO_OUTPUT usage specified.");
        this.mRS.validate();
        this.mRS.nAllocationIoSend(getID(this.mRS));
    }

    public void ioSendOutput()
    {
        ioSend();
    }

    /** @deprecated */
    public void resize(int paramInt)
    {
        try
        {
            if ((this.mType.getY() > 0) || (this.mType.getZ() > 0) || (this.mType.hasFaces()) || (this.mType.hasMipmaps()))
                throw new RSInvalidStateException("Resize only support for 1D allocations at this time.");
        }
        finally
        {
        }
        this.mRS.nAllocationResize1D(getID(this.mRS), paramInt);
        this.mRS.finish();
        this.mType = new Type(this.mRS.nAllocationGetType(getID(this.mRS)), this.mRS);
        this.mType.updateFromNative();
        updateCacheInfo(this.mType);
    }

    public void resize(int paramInt1, int paramInt2)
    {
        if ((this.mType.getZ() > 0) || (this.mType.hasFaces()) || (this.mType.hasMipmaps()))
            throw new RSInvalidStateException("Resize only support for 2D allocations at this time.");
        if (this.mType.getY() == 0)
            throw new RSInvalidStateException("Resize only support for 2D allocations at this time.");
        this.mRS.nAllocationResize2D(getID(this.mRS), paramInt1, paramInt2);
        this.mRS.finish();
        this.mType = new Type(this.mRS.nAllocationGetType(getID(this.mRS)), this.mRS);
        this.mType.updateFromNative();
        updateCacheInfo(this.mType);
    }

    public void setFromFieldPacker(int paramInt1, int paramInt2, FieldPacker paramFieldPacker)
    {
        this.mRS.validate();
        if (paramInt2 >= this.mType.mElement.mElements.length)
            throw new RSIllegalArgumentException("Component_number " + paramInt2 + " out of range.");
        if (paramInt1 < 0)
            throw new RSIllegalArgumentException("Offset must be >= 0.");
        byte[] arrayOfByte = paramFieldPacker.getData();
        int i = this.mType.mElement.mElements[paramInt2].getBytesSize() * this.mType.mElement.mArraySizes[paramInt2];
        if (arrayOfByte.length != i)
            throw new RSIllegalArgumentException("Field packer sizelength " + arrayOfByte.length + " does not match component size " + i + ".");
        this.mRS.nAllocationElementData1D(getIDSafe(), paramInt1, this.mSelectedLOD, paramInt2, arrayOfByte, arrayOfByte.length);
    }

    public void setFromFieldPacker(int paramInt, FieldPacker paramFieldPacker)
    {
        this.mRS.validate();
        int i = this.mType.mElement.getBytesSize();
        byte[] arrayOfByte = paramFieldPacker.getData();
        int j = arrayOfByte.length / i;
        if (i * j != arrayOfByte.length)
            throw new RSIllegalArgumentException("Field packer length " + arrayOfByte.length + " not divisible by element size " + i + ".");
        copy1DRangeFromUnchecked(paramInt, j, arrayOfByte);
    }

    public void setSurface(Surface paramSurface)
    {
        this.mRS.validate();
        if ((0x40 & this.mUsage) == 0)
            throw new RSInvalidStateException("Allocation is not USAGE_IO_OUTPUT.");
        this.mRS.nAllocationSetSurface(getID(this.mRS), paramSurface);
    }

    public void setSurfaceTexture(SurfaceTexture paramSurfaceTexture)
    {
        this.mRS.validate();
        if ((0x40 & this.mUsage) == 0)
            throw new RSInvalidStateException("Allocation is not USAGE_IO_OUTPUT.");
        Surface localSurface = new Surface(paramSurfaceTexture);
        this.mRS.nAllocationSetSurface(getID(this.mRS), localSurface);
    }

    public void syncAll(int paramInt)
    {
        switch (paramInt)
        {
        case 3:
        case 5:
        case 6:
        case 7:
        default:
            throw new RSIllegalArgumentException("Source must be exactly one usage type.");
        case 1:
        case 2:
        case 4:
        case 8:
        }
        this.mRS.validate();
        this.mRS.nAllocationSyncAll(getIDSafe(), paramInt);
    }

    void updateFromNative()
    {
        super.updateFromNative();
        int i = this.mRS.nAllocationGetType(getID(this.mRS));
        if (i != 0)
        {
            this.mType = new Type(i, this.mRS);
            this.mType.updateFromNative();
            updateCacheInfo(this.mType);
        }
    }

    public static enum MipmapControl
    {
        int mID;

        static
        {
            MIPMAP_FULL = new MipmapControl("MIPMAP_FULL", 1, 1);
            MIPMAP_ON_SYNC_TO_TEXTURE = new MipmapControl("MIPMAP_ON_SYNC_TO_TEXTURE", 2, 2);
            MipmapControl[] arrayOfMipmapControl = new MipmapControl[3];
            arrayOfMipmapControl[0] = MIPMAP_NONE;
            arrayOfMipmapControl[1] = MIPMAP_FULL;
            arrayOfMipmapControl[2] = MIPMAP_ON_SYNC_TO_TEXTURE;
        }

        private MipmapControl(int paramInt)
        {
            this.mID = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Allocation
 * JD-Core Version:        0.6.2
 */