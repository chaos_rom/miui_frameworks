package android.renderscript;

public class ProgramRaster extends BaseObj
{
    CullMode mCullMode = CullMode.BACK;
    boolean mPointSprite = false;

    ProgramRaster(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    public static ProgramRaster CULL_BACK(RenderScript paramRenderScript)
    {
        if (paramRenderScript.mProgramRaster_CULL_BACK == null)
        {
            Builder localBuilder = new Builder(paramRenderScript);
            localBuilder.setCullMode(CullMode.BACK);
            paramRenderScript.mProgramRaster_CULL_BACK = localBuilder.create();
        }
        return paramRenderScript.mProgramRaster_CULL_BACK;
    }

    public static ProgramRaster CULL_FRONT(RenderScript paramRenderScript)
    {
        if (paramRenderScript.mProgramRaster_CULL_FRONT == null)
        {
            Builder localBuilder = new Builder(paramRenderScript);
            localBuilder.setCullMode(CullMode.FRONT);
            paramRenderScript.mProgramRaster_CULL_FRONT = localBuilder.create();
        }
        return paramRenderScript.mProgramRaster_CULL_FRONT;
    }

    public static ProgramRaster CULL_NONE(RenderScript paramRenderScript)
    {
        if (paramRenderScript.mProgramRaster_CULL_NONE == null)
        {
            Builder localBuilder = new Builder(paramRenderScript);
            localBuilder.setCullMode(CullMode.NONE);
            paramRenderScript.mProgramRaster_CULL_NONE = localBuilder.create();
        }
        return paramRenderScript.mProgramRaster_CULL_NONE;
    }

    public CullMode getCullMode()
    {
        return this.mCullMode;
    }

    public boolean isPointSpriteEnabled()
    {
        return this.mPointSprite;
    }

    public static class Builder
    {
        ProgramRaster.CullMode mCullMode;
        boolean mPointSprite;
        RenderScript mRS;

        public Builder(RenderScript paramRenderScript)
        {
            this.mRS = paramRenderScript;
            this.mPointSprite = false;
            this.mCullMode = ProgramRaster.CullMode.BACK;
        }

        public ProgramRaster create()
        {
            this.mRS.validate();
            ProgramRaster localProgramRaster = new ProgramRaster(this.mRS.nProgramRasterCreate(this.mPointSprite, this.mCullMode.mID), this.mRS);
            localProgramRaster.mPointSprite = this.mPointSprite;
            localProgramRaster.mCullMode = this.mCullMode;
            return localProgramRaster;
        }

        public Builder setCullMode(ProgramRaster.CullMode paramCullMode)
        {
            this.mCullMode = paramCullMode;
            return this;
        }

        public Builder setPointSpriteEnabled(boolean paramBoolean)
        {
            this.mPointSprite = paramBoolean;
            return this;
        }
    }

    public static enum CullMode
    {
        int mID;

        static
        {
            CullMode[] arrayOfCullMode = new CullMode[3];
            arrayOfCullMode[0] = BACK;
            arrayOfCullMode[1] = FRONT;
            arrayOfCullMode[2] = NONE;
        }

        private CullMode(int paramInt)
        {
            this.mID = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.ProgramRaster
 * JD-Core Version:        0.6.2
 */