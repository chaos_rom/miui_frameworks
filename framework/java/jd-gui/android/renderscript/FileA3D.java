package android.renderscript;

import android.content.res.AssetManager;
import android.content.res.AssetManager.AssetInputStream;
import android.content.res.Resources;
import java.io.File;
import java.io.InputStream;

public class FileA3D extends BaseObj
{
    IndexEntry[] mFileEntries;
    InputStream mInputStream;

    FileA3D(int paramInt, RenderScript paramRenderScript, InputStream paramInputStream)
    {
        super(paramInt, paramRenderScript);
        this.mInputStream = paramInputStream;
    }

    public static FileA3D createFromAsset(RenderScript paramRenderScript, AssetManager paramAssetManager, String paramString)
    {
        paramRenderScript.validate();
        int i = paramRenderScript.nFileA3DCreateFromAsset(paramAssetManager, paramString);
        if (i == 0)
            throw new RSRuntimeException("Unable to create a3d file from asset " + paramString);
        FileA3D localFileA3D = new FileA3D(i, paramRenderScript, null);
        localFileA3D.initEntries();
        return localFileA3D;
    }

    public static FileA3D createFromFile(RenderScript paramRenderScript, File paramFile)
    {
        return createFromFile(paramRenderScript, paramFile.getAbsolutePath());
    }

    public static FileA3D createFromFile(RenderScript paramRenderScript, String paramString)
    {
        int i = paramRenderScript.nFileA3DCreateFromFile(paramString);
        if (i == 0)
            throw new RSRuntimeException("Unable to create a3d file from " + paramString);
        FileA3D localFileA3D = new FileA3D(i, paramRenderScript, null);
        localFileA3D.initEntries();
        return localFileA3D;
    }

    public static FileA3D createFromResource(RenderScript paramRenderScript, Resources paramResources, int paramInt)
    {
        paramRenderScript.validate();
        InputStream localInputStream;
        int i;
        try
        {
            localInputStream = paramResources.openRawResource(paramInt);
            if ((localInputStream instanceof AssetManager.AssetInputStream))
            {
                i = paramRenderScript.nFileA3DCreateFromAssetStream(((AssetManager.AssetInputStream)localInputStream).getAssetInt());
                if (i != 0)
                    break label103;
                throw new RSRuntimeException("Unable to create a3d file from resource " + paramInt);
            }
        }
        catch (Exception localException)
        {
            throw new RSRuntimeException("Unable to open resource " + paramInt);
        }
        throw new RSRuntimeException("Unsupported asset stream");
        label103: FileA3D localFileA3D = new FileA3D(i, paramRenderScript, localInputStream);
        localFileA3D.initEntries();
        return localFileA3D;
    }

    private void initEntries()
    {
        int i = this.mRS.nFileA3DGetNumIndexEntries(getID(this.mRS));
        if (i <= 0);
        while (true)
        {
            return;
            this.mFileEntries = new IndexEntry[i];
            int[] arrayOfInt = new int[i];
            String[] arrayOfString = new String[i];
            this.mRS.nFileA3DGetIndexEntries(getID(this.mRS), i, arrayOfInt, arrayOfString);
            for (int j = 0; j < i; j++)
                this.mFileEntries[j] = new IndexEntry(this.mRS, j, getID(this.mRS), arrayOfString[j], EntryType.toEntryType(arrayOfInt[j]));
        }
    }

    public IndexEntry getIndexEntry(int paramInt)
    {
        if ((getIndexEntryCount() == 0) || (paramInt < 0) || (paramInt >= this.mFileEntries.length));
        for (IndexEntry localIndexEntry = null; ; localIndexEntry = this.mFileEntries[paramInt])
            return localIndexEntry;
    }

    public int getIndexEntryCount()
    {
        if (this.mFileEntries == null);
        for (int i = 0; ; i = this.mFileEntries.length)
            return i;
    }

    public static class IndexEntry
    {
        FileA3D.EntryType mEntryType;
        int mID;
        int mIndex;
        BaseObj mLoadedObj;
        String mName;
        RenderScript mRS;

        IndexEntry(RenderScript paramRenderScript, int paramInt1, int paramInt2, String paramString, FileA3D.EntryType paramEntryType)
        {
            this.mRS = paramRenderScript;
            this.mIndex = paramInt1;
            this.mID = paramInt2;
            this.mName = paramString;
            this.mEntryType = paramEntryType;
            this.mLoadedObj = null;
        }

        /** @deprecated */
        static BaseObj internalCreate(RenderScript paramRenderScript, IndexEntry paramIndexEntry)
        {
            BaseObj localBaseObj = null;
            try
            {
                if (paramIndexEntry.mLoadedObj != null)
                    localBaseObj = paramIndexEntry.mLoadedObj;
                int i;
                do
                {
                    do
                        return localBaseObj;
                    while (paramIndexEntry.mEntryType == FileA3D.EntryType.UNKNOWN);
                    i = paramRenderScript.nFileA3DGetEntryByIndex(paramIndexEntry.mID, paramIndexEntry.mIndex);
                }
                while (i == 0);
                switch (FileA3D.1.$SwitchMap$android$renderscript$FileA3D$EntryType[paramIndexEntry.mEntryType.ordinal()])
                {
                default:
                case 1:
                }
                while (true)
                {
                    paramIndexEntry.mLoadedObj.updateFromNative();
                    localBaseObj = paramIndexEntry.mLoadedObj;
                    break;
                    paramIndexEntry.mLoadedObj = new Mesh(i, paramRenderScript);
                }
            }
            finally
            {
            }
        }

        public FileA3D.EntryType getEntryType()
        {
            return this.mEntryType;
        }

        public Mesh getMesh()
        {
            return (Mesh)getObject();
        }

        public String getName()
        {
            return this.mName;
        }

        public BaseObj getObject()
        {
            this.mRS.validate();
            return internalCreate(this.mRS, this);
        }
    }

    public static enum EntryType
    {
        int mID;

        static
        {
            MESH = new EntryType("MESH", 1, 1);
            EntryType[] arrayOfEntryType = new EntryType[2];
            arrayOfEntryType[0] = UNKNOWN;
            arrayOfEntryType[1] = MESH;
        }

        private EntryType(int paramInt)
        {
            this.mID = paramInt;
        }

        static EntryType toEntryType(int paramInt)
        {
            return values()[paramInt];
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.FileA3D
 * JD-Core Version:        0.6.2
 */