package android.renderscript;

public class Int3
{
    public int x;
    public int y;
    public int z;

    public Int3()
    {
    }

    public Int3(int paramInt1, int paramInt2, int paramInt3)
    {
        this.x = paramInt1;
        this.y = paramInt2;
        this.z = paramInt3;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Int3
 * JD-Core Version:        0.6.2
 */