package android.renderscript;

public class Float4
{
    public float w;
    public float x;
    public float y;
    public float z;

    public Float4()
    {
    }

    public Float4(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.x = paramFloat1;
        this.y = paramFloat2;
        this.z = paramFloat3;
        this.w = paramFloat4;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Float4
 * JD-Core Version:        0.6.2
 */