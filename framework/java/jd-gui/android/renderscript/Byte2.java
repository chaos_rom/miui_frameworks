package android.renderscript;

public class Byte2
{
    public byte x;
    public byte y;

    public Byte2()
    {
    }

    public Byte2(byte paramByte1, byte paramByte2)
    {
        this.x = paramByte1;
        this.y = paramByte2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Byte2
 * JD-Core Version:        0.6.2
 */