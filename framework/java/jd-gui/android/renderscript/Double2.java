package android.renderscript;

public class Double2
{
    public double x;
    public double y;

    public Double2()
    {
    }

    public Double2(double paramDouble1, double paramDouble2)
    {
        this.x = paramDouble1;
        this.y = paramDouble2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Double2
 * JD-Core Version:        0.6.2
 */