package android.renderscript;

public class ProgramVertexFixedFunction extends ProgramVertex
{
    ProgramVertexFixedFunction(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    public void bindConstants(Constants paramConstants)
    {
        this.mRS.validate();
        bindConstants(paramConstants.getAllocation(), 0);
    }

    public static class Constants
    {
        static final int MODELVIEW_OFFSET = 0;
        static final int PROJECTION_OFFSET = 16;
        static final int TEXTURE_OFFSET = 32;
        Allocation mAlloc;
        private FieldPacker mIOBuffer;
        Matrix4f mModel;
        Matrix4f mProjection;
        Matrix4f mTexture;

        public Constants(RenderScript paramRenderScript)
        {
            Type localType = ProgramVertexFixedFunction.Builder.getConstantInputType(paramRenderScript);
            this.mAlloc = Allocation.createTyped(paramRenderScript, localType);
            this.mIOBuffer = new FieldPacker(localType.getElement().getBytesSize() * localType.getCount());
            this.mModel = new Matrix4f();
            this.mProjection = new Matrix4f();
            this.mTexture = new Matrix4f();
            setModelview(new Matrix4f());
            setProjection(new Matrix4f());
            setTexture(new Matrix4f());
        }

        private void addToBuffer(int paramInt, Matrix4f paramMatrix4f)
        {
            this.mIOBuffer.reset(paramInt);
            for (int i = 0; i < 16; i++)
                this.mIOBuffer.addF32(paramMatrix4f.mMat[i]);
            this.mAlloc.setFromFieldPacker(0, this.mIOBuffer);
        }

        public void destroy()
        {
            this.mAlloc.destroy();
            this.mAlloc = null;
        }

        Allocation getAllocation()
        {
            return this.mAlloc;
        }

        public void setModelview(Matrix4f paramMatrix4f)
        {
            this.mModel.load(paramMatrix4f);
            addToBuffer(0, paramMatrix4f);
        }

        public void setProjection(Matrix4f paramMatrix4f)
        {
            this.mProjection.load(paramMatrix4f);
            addToBuffer(64, paramMatrix4f);
        }

        public void setTexture(Matrix4f paramMatrix4f)
        {
            this.mTexture.load(paramMatrix4f);
            addToBuffer(128, paramMatrix4f);
        }
    }

    public static class Builder
    {
        RenderScript mRS;
        String mShader;
        boolean mTextureMatrixEnable;

        public Builder(RenderScript paramRenderScript)
        {
            this.mRS = paramRenderScript;
        }

        private void buildShaderString()
        {
            this.mShader = "//rs_shader_internal\n";
            this.mShader += "varying vec4 varColor;\n";
            this.mShader += "varying vec2 varTex0;\n";
            this.mShader += "void main() {\n";
            this.mShader += "    gl_Position = UNI_MVP * ATTRIB_position;\n";
            this.mShader += "    gl_PointSize = 1.0;\n";
            this.mShader += "    varColor = ATTRIB_color;\n";
            if (this.mTextureMatrixEnable);
            for (this.mShader += "    varTex0 = (UNI_TexMatrix * vec4(ATTRIB_texture0, 0.0, 1.0)).xy;\n"; ; this.mShader += "    varTex0 = ATTRIB_texture0;\n")
            {
                this.mShader += "}\n";
                return;
            }
        }

        static Type getConstantInputType(RenderScript paramRenderScript)
        {
            Element.Builder localBuilder = new Element.Builder(paramRenderScript);
            localBuilder.add(Element.MATRIX4X4(paramRenderScript), "MV");
            localBuilder.add(Element.MATRIX4X4(paramRenderScript), "P");
            localBuilder.add(Element.MATRIX4X4(paramRenderScript), "TexMatrix");
            localBuilder.add(Element.MATRIX4X4(paramRenderScript), "MVP");
            Type.Builder localBuilder1 = new Type.Builder(paramRenderScript, localBuilder.create());
            localBuilder1.setX(1);
            return localBuilder1.create();
        }

        public ProgramVertexFixedFunction create()
        {
            buildShaderString();
            ProgramVertexFixedFunction.InternalBuilder localInternalBuilder = new ProgramVertexFixedFunction.InternalBuilder(this.mRS);
            localInternalBuilder.setShader(this.mShader);
            localInternalBuilder.addConstant(getConstantInputType(this.mRS));
            Element.Builder localBuilder = new Element.Builder(this.mRS);
            localBuilder.add(Element.F32_4(this.mRS), "position");
            localBuilder.add(Element.F32_4(this.mRS), "color");
            localBuilder.add(Element.F32_3(this.mRS), "normal");
            localBuilder.add(Element.F32_2(this.mRS), "texture0");
            localInternalBuilder.addInput(localBuilder.create());
            return localInternalBuilder.create();
        }

        public Builder setTextureMatrixEnable(boolean paramBoolean)
        {
            this.mTextureMatrixEnable = paramBoolean;
            return this;
        }
    }

    static class InternalBuilder extends Program.BaseProgramBuilder
    {
        public InternalBuilder(RenderScript paramRenderScript)
        {
            super();
        }

        public InternalBuilder addInput(Element paramElement)
            throws IllegalStateException
        {
            if (this.mInputCount >= 8)
                throw new RSIllegalArgumentException("Max input count exceeded.");
            if (paramElement.isComplex())
                throw new RSIllegalArgumentException("Complex elements not allowed.");
            Element[] arrayOfElement = this.mInputs;
            int i = this.mInputCount;
            this.mInputCount = (i + 1);
            arrayOfElement[i] = paramElement;
            return this;
        }

        public ProgramVertexFixedFunction create()
        {
            this.mRS.validate();
            int[] arrayOfInt = new int[2 * (this.mInputCount + this.mOutputCount + this.mConstantCount + this.mTextureCount)];
            String[] arrayOfString = new String[this.mTextureCount];
            int i = 0;
            for (int j = 0; j < this.mInputCount; j++)
            {
                int i4 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.INPUT.mID;
                i = i4 + 1;
                arrayOfInt[i4] = this.mInputs[j].getID(this.mRS);
            }
            for (int k = 0; k < this.mOutputCount; k++)
            {
                int i3 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.OUTPUT.mID;
                i = i3 + 1;
                arrayOfInt[i3] = this.mOutputs[k].getID(this.mRS);
            }
            for (int m = 0; m < this.mConstantCount; m++)
            {
                int i2 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.CONSTANT.mID;
                i = i2 + 1;
                arrayOfInt[i2] = this.mConstants[m].getID(this.mRS);
            }
            for (int n = 0; n < this.mTextureCount; n++)
            {
                int i1 = i + 1;
                arrayOfInt[i] = Program.ProgramParam.TEXTURE_TYPE.mID;
                i = i1 + 1;
                arrayOfInt[i1] = this.mTextureTypes[n].mID;
                arrayOfString[n] = this.mTextureNames[n];
            }
            ProgramVertexFixedFunction localProgramVertexFixedFunction = new ProgramVertexFixedFunction(this.mRS.nProgramVertexCreate(this.mShader, arrayOfString, arrayOfInt), this.mRS);
            initProgram(localProgramVertexFixedFunction);
            return localProgramVertexFixedFunction;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.ProgramVertexFixedFunction
 * JD-Core Version:        0.6.2
 */