package android.renderscript;

import java.io.UnsupportedEncodingException;

public class Script extends BaseObj
{
    Script(int paramInt, RenderScript paramRenderScript)
    {
        super(paramInt, paramRenderScript);
    }

    public void bindAllocation(Allocation paramAllocation, int paramInt)
    {
        this.mRS.validate();
        if (paramAllocation != null)
            this.mRS.nScriptBindAllocation(getID(this.mRS), paramAllocation.getID(this.mRS), paramInt);
        while (true)
        {
            return;
            this.mRS.nScriptBindAllocation(getID(this.mRS), 0, paramInt);
        }
    }

    protected void forEach(int paramInt, Allocation paramAllocation1, Allocation paramAllocation2, FieldPacker paramFieldPacker)
    {
        if ((paramAllocation1 == null) && (paramAllocation2 == null))
            throw new RSIllegalArgumentException("At least one of ain or aout is required to be non-null.");
        int i = 0;
        if (paramAllocation1 != null)
            i = paramAllocation1.getID(this.mRS);
        int j = 0;
        if (paramAllocation2 != null)
            j = paramAllocation2.getID(this.mRS);
        byte[] arrayOfByte = null;
        if (paramFieldPacker != null)
            arrayOfByte = paramFieldPacker.getData();
        this.mRS.nScriptForEach(getID(this.mRS), paramInt, i, j, arrayOfByte);
    }

    protected void invoke(int paramInt)
    {
        this.mRS.nScriptInvoke(getID(this.mRS), paramInt);
    }

    protected void invoke(int paramInt, FieldPacker paramFieldPacker)
    {
        if (paramFieldPacker != null)
            this.mRS.nScriptInvokeV(getID(this.mRS), paramInt, paramFieldPacker.getData());
        while (true)
        {
            return;
            this.mRS.nScriptInvoke(getID(this.mRS), paramInt);
        }
    }

    public void setTimeZone(String paramString)
    {
        this.mRS.validate();
        try
        {
            this.mRS.nScriptSetTimeZone(getID(this.mRS), paramString.getBytes("UTF-8"));
            return;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
            throw new RuntimeException(localUnsupportedEncodingException);
        }
    }

    public void setVar(int paramInt, double paramDouble)
    {
        this.mRS.nScriptSetVarD(getID(this.mRS), paramInt, paramDouble);
    }

    public void setVar(int paramInt, float paramFloat)
    {
        this.mRS.nScriptSetVarF(getID(this.mRS), paramInt, paramFloat);
    }

    public void setVar(int paramInt1, int paramInt2)
    {
        this.mRS.nScriptSetVarI(getID(this.mRS), paramInt1, paramInt2);
    }

    public void setVar(int paramInt, long paramLong)
    {
        this.mRS.nScriptSetVarJ(getID(this.mRS), paramInt, paramLong);
    }

    public void setVar(int paramInt, BaseObj paramBaseObj)
    {
        RenderScript localRenderScript = this.mRS;
        int i = getID(this.mRS);
        if (paramBaseObj == null);
        for (int j = 0; ; j = paramBaseObj.getID(this.mRS))
        {
            localRenderScript.nScriptSetVarObj(i, paramInt, j);
            return;
        }
    }

    public void setVar(int paramInt, FieldPacker paramFieldPacker)
    {
        this.mRS.nScriptSetVarV(getID(this.mRS), paramInt, paramFieldPacker.getData());
    }

    public void setVar(int paramInt, FieldPacker paramFieldPacker, Element paramElement, int[] paramArrayOfInt)
    {
        this.mRS.nScriptSetVarVE(getID(this.mRS), paramInt, paramFieldPacker.getData(), paramElement.getID(this.mRS), paramArrayOfInt);
    }

    public void setVar(int paramInt, boolean paramBoolean)
    {
        RenderScript localRenderScript = this.mRS;
        int i = getID(this.mRS);
        if (paramBoolean);
        for (int j = 1; ; j = 0)
        {
            localRenderScript.nScriptSetVarI(i, paramInt, j);
            return;
        }
    }

    public static class FieldBase
    {
        protected Allocation mAllocation;
        protected Element mElement;

        public Allocation getAllocation()
        {
            return this.mAllocation;
        }

        public Element getElement()
        {
            return this.mElement;
        }

        public Type getType()
        {
            return this.mAllocation.getType();
        }

        protected void init(RenderScript paramRenderScript, int paramInt)
        {
            this.mAllocation = Allocation.createSized(paramRenderScript, this.mElement, paramInt, 1);
        }

        protected void init(RenderScript paramRenderScript, int paramInt1, int paramInt2)
        {
            this.mAllocation = Allocation.createSized(paramRenderScript, this.mElement, paramInt1, paramInt2 | 0x1);
        }

        public void updateAllocation()
        {
        }
    }

    public static class Builder
    {
        RenderScript mRS;

        Builder(RenderScript paramRenderScript)
        {
            this.mRS = paramRenderScript;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Script
 * JD-Core Version:        0.6.2
 */