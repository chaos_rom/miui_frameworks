package android.renderscript;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.util.Log;
import android.view.Surface;
import java.io.File;

public class RenderScript
{
    private static final String CACHE_PATH = "com.android.renderscript.cache";
    static final boolean DEBUG = false;
    static final boolean LOG_ENABLED = false;
    static final String LOG_TAG = "RenderScript_jni";
    static String mCachePath;
    static boolean sInitialized = false;
    private Context mApplicationContext;
    int mContext;
    int mDev;
    Element mElement_ALLOCATION;
    Element mElement_A_8;
    Element mElement_BOOLEAN;
    Element mElement_CHAR_2;
    Element mElement_CHAR_3;
    Element mElement_CHAR_4;
    Element mElement_DOUBLE_2;
    Element mElement_DOUBLE_3;
    Element mElement_DOUBLE_4;
    Element mElement_ELEMENT;
    Element mElement_F32;
    Element mElement_F64;
    Element mElement_FLOAT_2;
    Element mElement_FLOAT_3;
    Element mElement_FLOAT_4;
    Element mElement_FONT;
    Element mElement_I16;
    Element mElement_I32;
    Element mElement_I64;
    Element mElement_I8;
    Element mElement_INT_2;
    Element mElement_INT_3;
    Element mElement_INT_4;
    Element mElement_LONG_2;
    Element mElement_LONG_3;
    Element mElement_LONG_4;
    Element mElement_MATRIX_2X2;
    Element mElement_MATRIX_3X3;
    Element mElement_MATRIX_4X4;
    Element mElement_MESH;
    Element mElement_PROGRAM_FRAGMENT;
    Element mElement_PROGRAM_RASTER;
    Element mElement_PROGRAM_STORE;
    Element mElement_PROGRAM_VERTEX;
    Element mElement_RGBA_4444;
    Element mElement_RGBA_5551;
    Element mElement_RGBA_8888;
    Element mElement_RGB_565;
    Element mElement_RGB_888;
    Element mElement_SAMPLER;
    Element mElement_SCRIPT;
    Element mElement_SHORT_2;
    Element mElement_SHORT_3;
    Element mElement_SHORT_4;
    Element mElement_TYPE;
    Element mElement_U16;
    Element mElement_U32;
    Element mElement_U64;
    Element mElement_U8;
    Element mElement_UCHAR_2;
    Element mElement_UCHAR_3;
    Element mElement_UCHAR_4;
    Element mElement_UINT_2;
    Element mElement_UINT_3;
    Element mElement_UINT_4;
    Element mElement_ULONG_2;
    Element mElement_ULONG_3;
    Element mElement_ULONG_4;
    Element mElement_USHORT_2;
    Element mElement_USHORT_3;
    Element mElement_USHORT_4;
    RSErrorHandler mErrorCallback = null;
    RSMessageHandler mMessageCallback = null;
    MessageThread mMessageThread;
    ProgramRaster mProgramRaster_CULL_BACK;
    ProgramRaster mProgramRaster_CULL_FRONT;
    ProgramRaster mProgramRaster_CULL_NONE;
    ProgramStore mProgramStore_BLEND_ALPHA_DEPTH_NO_DEPTH;
    ProgramStore mProgramStore_BLEND_ALPHA_DEPTH_TEST;
    ProgramStore mProgramStore_BLEND_NONE_DEPTH_NO_DEPTH;
    ProgramStore mProgramStore_BLEND_NONE_DEPTH_TEST;
    Sampler mSampler_CLAMP_LINEAR;
    Sampler mSampler_CLAMP_LINEAR_MIP_LINEAR;
    Sampler mSampler_CLAMP_NEAREST;
    Sampler mSampler_WRAP_LINEAR;
    Sampler mSampler_WRAP_LINEAR_MIP_LINEAR;
    Sampler mSampler_WRAP_NEAREST;

    static
    {
        try
        {
            System.loadLibrary("rs_jni");
            _nInit();
            sInitialized = true;
            return;
        }
        catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
        {
            Log.e("RenderScript_jni", "Error loading RS jni library: " + localUnsatisfiedLinkError);
            throw new RSRuntimeException("Error loading RS jni library: " + localUnsatisfiedLinkError);
        }
    }

    RenderScript(Context paramContext)
    {
        if (paramContext != null)
            this.mApplicationContext = paramContext.getApplicationContext();
    }

    static native void _nInit();

    public static RenderScript create(Context paramContext)
    {
        return create(paramContext, paramContext.getApplicationInfo().targetSdkVersion);
    }

    public static RenderScript create(Context paramContext, int paramInt)
    {
        RenderScript localRenderScript = new RenderScript(paramContext);
        localRenderScript.mDev = localRenderScript.nDeviceCreate();
        localRenderScript.mContext = localRenderScript.nContextCreate(localRenderScript.mDev, 0, paramInt);
        if (localRenderScript.mContext == 0)
            throw new RSDriverException("Failed to create RS context.");
        localRenderScript.mMessageThread = new MessageThread(localRenderScript);
        localRenderScript.mMessageThread.start();
        return localRenderScript;
    }

    public static void setupDiskCache(File paramFile)
    {
        File localFile = new File(paramFile, "com.android.renderscript.cache");
        mCachePath = localFile.getAbsolutePath();
        localFile.mkdirs();
    }

    public void contextDump()
    {
        validate();
        nContextDump(0);
    }

    public void destroy()
    {
        validate();
        nContextDeinitToClient(this.mContext);
        this.mMessageThread.mRun = false;
        try
        {
            this.mMessageThread.join();
            label27: nContextDestroy();
            this.mContext = 0;
            nDeviceDestroy(this.mDev);
            this.mDev = 0;
            return;
        }
        catch (InterruptedException localInterruptedException)
        {
            break label27;
        }
    }

    public void finish()
    {
        nContextFinish();
    }

    public final Context getApplicationContext()
    {
        return this.mApplicationContext;
    }

    public RSErrorHandler getErrorHandler()
    {
        return this.mErrorCallback;
    }

    public RSMessageHandler getMessageHandler()
    {
        return this.mMessageCallback;
    }

    boolean isAlive()
    {
        if (this.mContext != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    /** @deprecated */
    void nAllocationCopyFromBitmap(int paramInt, Bitmap paramBitmap)
    {
        try
        {
            validate();
            rsnAllocationCopyFromBitmap(this.mContext, paramInt, paramBitmap);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationCopyToBitmap(int paramInt, Bitmap paramBitmap)
    {
        try
        {
            validate();
            rsnAllocationCopyToBitmap(this.mContext, paramInt, paramBitmap);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nAllocationCreateBitmapRef(int paramInt, Bitmap paramBitmap)
    {
        try
        {
            validate();
            int i = rsnAllocationCreateBitmapRef(this.mContext, paramInt, paramBitmap);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nAllocationCreateFromAssetStream(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            int i = rsnAllocationCreateFromAssetStream(this.mContext, paramInt1, paramInt2, paramInt3);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nAllocationCreateFromBitmap(int paramInt1, int paramInt2, Bitmap paramBitmap, int paramInt3)
    {
        try
        {
            validate();
            int i = rsnAllocationCreateFromBitmap(this.mContext, paramInt1, paramInt2, paramBitmap, paramInt3);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nAllocationCreateTyped(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        try
        {
            validate();
            int i = rsnAllocationCreateTyped(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nAllocationCubeCreateFromBitmap(int paramInt1, int paramInt2, Bitmap paramBitmap, int paramInt3)
    {
        try
        {
            validate();
            int i = rsnAllocationCubeCreateFromBitmap(this.mContext, paramInt1, paramInt2, paramBitmap, paramInt3);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte, int paramInt5)
    {
        try
        {
            validate();
            rsnAllocationData1D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfByte, paramInt5);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float[] paramArrayOfFloat, int paramInt5)
    {
        try
        {
            validate();
            rsnAllocationData1D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfFloat, paramInt5);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt, int paramInt5)
    {
        try
        {
            validate();
            rsnAllocationData1D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfInt, paramInt5);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, short[] paramArrayOfShort, int paramInt5)
    {
        try
        {
            validate();
            rsnAllocationData1D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfShort, paramInt5);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12)
    {
        try
        {
            validate();
            rsnAllocationData2D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9, paramInt10, paramInt11, paramInt12);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, byte[] paramArrayOfByte, int paramInt8)
    {
        try
        {
            validate();
            rsnAllocationData2D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramArrayOfByte, paramInt8);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, float[] paramArrayOfFloat, int paramInt8)
    {
        try
        {
            validate();
            rsnAllocationData2D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramArrayOfFloat, paramInt8);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int[] paramArrayOfInt, int paramInt8)
    {
        try
        {
            validate();
            rsnAllocationData2D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramArrayOfInt, paramInt8);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, short[] paramArrayOfShort, int paramInt8)
    {
        try
        {
            validate();
            rsnAllocationData2D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramArrayOfShort, paramInt8);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Bitmap paramBitmap)
    {
        try
        {
            validate();
            rsnAllocationData2D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramBitmap);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationElementData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte, int paramInt5)
    {
        try
        {
            validate();
            rsnAllocationElementData1D(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfByte, paramInt5);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationGenerateMipmaps(int paramInt)
    {
        try
        {
            validate();
            rsnAllocationGenerateMipmaps(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nAllocationGetSurfaceTextureID(int paramInt)
    {
        try
        {
            validate();
            int i = rsnAllocationGetSurfaceTextureID(this.mContext, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationGetSurfaceTextureID2(int paramInt, SurfaceTexture paramSurfaceTexture)
    {
        try
        {
            validate();
            rsnAllocationGetSurfaceTextureID2(this.mContext, paramInt, paramSurfaceTexture);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nAllocationGetType(int paramInt)
    {
        try
        {
            validate();
            int i = rsnAllocationGetType(this.mContext, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationIoReceive(int paramInt)
    {
        try
        {
            validate();
            rsnAllocationIoReceive(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationIoSend(int paramInt)
    {
        try
        {
            validate();
            rsnAllocationIoSend(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationRead(int paramInt, byte[] paramArrayOfByte)
    {
        try
        {
            validate();
            rsnAllocationRead(this.mContext, paramInt, paramArrayOfByte);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationRead(int paramInt, float[] paramArrayOfFloat)
    {
        try
        {
            validate();
            rsnAllocationRead(this.mContext, paramInt, paramArrayOfFloat);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationRead(int paramInt, int[] paramArrayOfInt)
    {
        try
        {
            validate();
            rsnAllocationRead(this.mContext, paramInt, paramArrayOfInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationRead(int paramInt, short[] paramArrayOfShort)
    {
        try
        {
            validate();
            rsnAllocationRead(this.mContext, paramInt, paramArrayOfShort);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationResize1D(int paramInt1, int paramInt2)
    {
        try
        {
            validate();
            rsnAllocationResize1D(this.mContext, paramInt1, paramInt2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationResize2D(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            rsnAllocationResize2D(this.mContext, paramInt1, paramInt2, paramInt3);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationSetSurface(int paramInt, Surface paramSurface)
    {
        try
        {
            validate();
            rsnAllocationSetSurface(this.mContext, paramInt, paramSurface);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAllocationSyncAll(int paramInt1, int paramInt2)
    {
        try
        {
            validate();
            rsnAllocationSyncAll(this.mContext, paramInt1, paramInt2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nAssignName(int paramInt, byte[] paramArrayOfByte)
    {
        try
        {
            validate();
            rsnAssignName(this.mContext, paramInt, paramArrayOfByte);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextBindProgramFragment(int paramInt)
    {
        try
        {
            validate();
            rsnContextBindProgramFragment(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextBindProgramRaster(int paramInt)
    {
        try
        {
            validate();
            rsnContextBindProgramRaster(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextBindProgramStore(int paramInt)
    {
        try
        {
            validate();
            rsnContextBindProgramStore(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextBindProgramVertex(int paramInt)
    {
        try
        {
            validate();
            rsnContextBindProgramVertex(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextBindRootScript(int paramInt)
    {
        try
        {
            validate();
            rsnContextBindRootScript(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextBindSampler(int paramInt1, int paramInt2)
    {
        try
        {
            validate();
            rsnContextBindSampler(this.mContext, paramInt1, paramInt2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nContextCreate(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            int i = rsnContextCreate(paramInt1, paramInt2, paramInt3);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nContextCreateGL(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, int paramInt13, float paramFloat, int paramInt14)
    {
        try
        {
            int i = rsnContextCreateGL(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9, paramInt10, paramInt11, paramInt12, paramInt13, paramFloat, paramInt14);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    native void nContextDeinitToClient(int paramInt);

    /** @deprecated */
    void nContextDestroy()
    {
        try
        {
            validate();
            rsnContextDestroy(this.mContext);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextDump(int paramInt)
    {
        try
        {
            validate();
            rsnContextDump(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextFinish()
    {
        try
        {
            validate();
            rsnContextFinish(this.mContext);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    native String nContextGetErrorMessage(int paramInt);

    native int nContextGetUserMessage(int paramInt, int[] paramArrayOfInt);

    native void nContextInitToClient(int paramInt);

    /** @deprecated */
    void nContextPause()
    {
        try
        {
            validate();
            rsnContextPause(this.mContext);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    native int nContextPeekMessage(int paramInt, int[] paramArrayOfInt);

    /** @deprecated */
    void nContextResume()
    {
        try
        {
            validate();
            rsnContextResume(this.mContext);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextSetPriority(int paramInt)
    {
        try
        {
            validate();
            rsnContextSetPriority(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextSetSurface(int paramInt1, int paramInt2, Surface paramSurface)
    {
        try
        {
            validate();
            rsnContextSetSurface(this.mContext, paramInt1, paramInt2, paramSurface);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nContextSetSurfaceTexture(int paramInt1, int paramInt2, SurfaceTexture paramSurfaceTexture)
    {
        try
        {
            validate();
            rsnContextSetSurfaceTexture(this.mContext, paramInt1, paramInt2, paramSurfaceTexture);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    native int nDeviceCreate();

    native void nDeviceDestroy(int paramInt);

    native void nDeviceSetConfig(int paramInt1, int paramInt2, int paramInt3);

    /** @deprecated */
    int nElementCreate(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3)
    {
        try
        {
            validate();
            int i = rsnElementCreate(this.mContext, paramInt1, paramInt2, paramBoolean, paramInt3);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nElementCreate2(int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2)
    {
        try
        {
            validate();
            int i = rsnElementCreate2(this.mContext, paramArrayOfInt1, paramArrayOfString, paramArrayOfInt2);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nElementGetNativeData(int paramInt, int[] paramArrayOfInt)
    {
        try
        {
            validate();
            rsnElementGetNativeData(this.mContext, paramInt, paramArrayOfInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nElementGetSubElements(int paramInt, int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2)
    {
        try
        {
            validate();
            rsnElementGetSubElements(this.mContext, paramInt, paramArrayOfInt1, paramArrayOfString, paramArrayOfInt2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nFileA3DCreateFromAsset(AssetManager paramAssetManager, String paramString)
    {
        try
        {
            validate();
            int i = rsnFileA3DCreateFromAsset(this.mContext, paramAssetManager, paramString);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nFileA3DCreateFromAssetStream(int paramInt)
    {
        try
        {
            validate();
            int i = rsnFileA3DCreateFromAssetStream(this.mContext, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nFileA3DCreateFromFile(String paramString)
    {
        try
        {
            validate();
            int i = rsnFileA3DCreateFromFile(this.mContext, paramString);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nFileA3DGetEntryByIndex(int paramInt1, int paramInt2)
    {
        try
        {
            validate();
            int i = rsnFileA3DGetEntryByIndex(this.mContext, paramInt1, paramInt2);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nFileA3DGetIndexEntries(int paramInt1, int paramInt2, int[] paramArrayOfInt, String[] paramArrayOfString)
    {
        try
        {
            validate();
            rsnFileA3DGetIndexEntries(this.mContext, paramInt1, paramInt2, paramArrayOfInt, paramArrayOfString);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nFileA3DGetNumIndexEntries(int paramInt)
    {
        try
        {
            validate();
            int i = rsnFileA3DGetNumIndexEntries(this.mContext, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nFontCreateFromAsset(AssetManager paramAssetManager, String paramString, float paramFloat, int paramInt)
    {
        try
        {
            validate();
            int i = rsnFontCreateFromAsset(this.mContext, paramAssetManager, paramString, paramFloat, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nFontCreateFromAssetStream(String paramString, float paramFloat, int paramInt1, int paramInt2)
    {
        try
        {
            validate();
            int i = rsnFontCreateFromAssetStream(this.mContext, paramString, paramFloat, paramInt1, paramInt2);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nFontCreateFromFile(String paramString, float paramFloat, int paramInt)
    {
        try
        {
            validate();
            int i = rsnFontCreateFromFile(this.mContext, paramString, paramFloat, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    String nGetName(int paramInt)
    {
        try
        {
            validate();
            String str = rsnGetName(this.mContext, paramInt);
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nMeshCreate(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3)
    {
        try
        {
            validate();
            int i = rsnMeshCreate(this.mContext, paramArrayOfInt1, paramArrayOfInt2, paramArrayOfInt3);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nMeshGetIndexCount(int paramInt)
    {
        try
        {
            validate();
            int i = rsnMeshGetIndexCount(this.mContext, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nMeshGetIndices(int paramInt1, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt2)
    {
        try
        {
            validate();
            rsnMeshGetIndices(this.mContext, paramInt1, paramArrayOfInt1, paramArrayOfInt2, paramInt2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nMeshGetVertexBufferCount(int paramInt)
    {
        try
        {
            validate();
            int i = rsnMeshGetVertexBufferCount(this.mContext, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nMeshGetVertices(int paramInt1, int[] paramArrayOfInt, int paramInt2)
    {
        try
        {
            validate();
            rsnMeshGetVertices(this.mContext, paramInt1, paramArrayOfInt, paramInt2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nObjDestroy(int paramInt)
    {
        try
        {
            if (this.mContext != 0)
                rsnObjDestroy(this.mContext, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nPathCreate(int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, float paramFloat)
    {
        try
        {
            validate();
            int i = rsnPathCreate(this.mContext, paramInt1, paramBoolean, paramInt2, paramInt3, paramFloat);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nProgramBindConstants(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            rsnProgramBindConstants(this.mContext, paramInt1, paramInt2, paramInt3);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nProgramBindSampler(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            rsnProgramBindSampler(this.mContext, paramInt1, paramInt2, paramInt3);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nProgramBindTexture(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            rsnProgramBindTexture(this.mContext, paramInt1, paramInt2, paramInt3);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nProgramFragmentCreate(String paramString, String[] paramArrayOfString, int[] paramArrayOfInt)
    {
        try
        {
            validate();
            int i = rsnProgramFragmentCreate(this.mContext, paramString, paramArrayOfString, paramArrayOfInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nProgramRasterCreate(boolean paramBoolean, int paramInt)
    {
        try
        {
            validate();
            int i = rsnProgramRasterCreate(this.mContext, paramBoolean, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nProgramStoreCreate(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            int i = rsnProgramStoreCreate(this.mContext, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramInt1, paramInt2, paramInt3);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nProgramVertexCreate(String paramString, String[] paramArrayOfString, int[] paramArrayOfInt)
    {
        try
        {
            validate();
            int i = rsnProgramVertexCreate(this.mContext, paramString, paramArrayOfString, paramArrayOfInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nSamplerCreate(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float paramFloat)
    {
        try
        {
            validate();
            int i = rsnSamplerCreate(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramFloat);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptBindAllocation(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            rsnScriptBindAllocation(this.mContext, paramInt1, paramInt2, paramInt3);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nScriptCCreate(String paramString1, String paramString2, byte[] paramArrayOfByte, int paramInt)
    {
        try
        {
            validate();
            int i = rsnScriptCCreate(this.mContext, paramString1, paramString2, paramArrayOfByte, paramInt);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptForEach(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte)
    {
        try
        {
            validate();
            if (paramArrayOfByte == null)
                rsnScriptForEach(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4);
            while (true)
            {
                return;
                rsnScriptForEach(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfByte);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    void nScriptInvoke(int paramInt1, int paramInt2)
    {
        try
        {
            validate();
            rsnScriptInvoke(this.mContext, paramInt1, paramInt2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptInvokeV(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        try
        {
            validate();
            rsnScriptInvokeV(this.mContext, paramInt1, paramInt2, paramArrayOfByte);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptSetTimeZone(int paramInt, byte[] paramArrayOfByte)
    {
        try
        {
            validate();
            rsnScriptSetTimeZone(this.mContext, paramInt, paramArrayOfByte);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptSetVarD(int paramInt1, int paramInt2, double paramDouble)
    {
        try
        {
            validate();
            rsnScriptSetVarD(this.mContext, paramInt1, paramInt2, paramDouble);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptSetVarF(int paramInt1, int paramInt2, float paramFloat)
    {
        try
        {
            validate();
            rsnScriptSetVarF(this.mContext, paramInt1, paramInt2, paramFloat);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptSetVarI(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            rsnScriptSetVarI(this.mContext, paramInt1, paramInt2, paramInt3);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptSetVarJ(int paramInt1, int paramInt2, long paramLong)
    {
        try
        {
            validate();
            rsnScriptSetVarJ(this.mContext, paramInt1, paramInt2, paramLong);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptSetVarObj(int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            validate();
            rsnScriptSetVarObj(this.mContext, paramInt1, paramInt2, paramInt3);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptSetVarV(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        try
        {
            validate();
            rsnScriptSetVarV(this.mContext, paramInt1, paramInt2, paramArrayOfByte);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nScriptSetVarVE(int paramInt1, int paramInt2, byte[] paramArrayOfByte, int paramInt3, int[] paramArrayOfInt)
    {
        try
        {
            validate();
            rsnScriptSetVarVE(this.mContext, paramInt1, paramInt2, paramArrayOfByte, paramInt3, paramArrayOfInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    int nTypeCreate(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2)
    {
        try
        {
            validate();
            int i = rsnTypeCreate(this.mContext, paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1, paramBoolean2);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void nTypeGetNativeData(int paramInt, int[] paramArrayOfInt)
    {
        try
        {
            validate();
            rsnTypeGetNativeData(this.mContext, paramInt, paramArrayOfInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    native void rsnAllocationCopyFromBitmap(int paramInt1, int paramInt2, Bitmap paramBitmap);

    native void rsnAllocationCopyToBitmap(int paramInt1, int paramInt2, Bitmap paramBitmap);

    native int rsnAllocationCreateBitmapRef(int paramInt1, int paramInt2, Bitmap paramBitmap);

    native int rsnAllocationCreateFromAssetStream(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    native int rsnAllocationCreateFromBitmap(int paramInt1, int paramInt2, int paramInt3, Bitmap paramBitmap, int paramInt4);

    native int rsnAllocationCreateTyped(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    native int rsnAllocationCubeCreateFromBitmap(int paramInt1, int paramInt2, int paramInt3, Bitmap paramBitmap, int paramInt4);

    native void rsnAllocationData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte[] paramArrayOfByte, int paramInt6);

    native void rsnAllocationData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6);

    native void rsnAllocationData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int[] paramArrayOfInt, int paramInt6);

    native void rsnAllocationData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, short[] paramArrayOfShort, int paramInt6);

    native void rsnAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, int paramInt13);

    native void rsnAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, byte[] paramArrayOfByte, int paramInt9);

    native void rsnAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, float[] paramArrayOfFloat, int paramInt9);

    native void rsnAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int[] paramArrayOfInt, int paramInt9);

    native void rsnAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, short[] paramArrayOfShort, int paramInt9);

    native void rsnAllocationData2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Bitmap paramBitmap);

    native void rsnAllocationElementData1D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte[] paramArrayOfByte, int paramInt6);

    native void rsnAllocationGenerateMipmaps(int paramInt1, int paramInt2);

    native int rsnAllocationGetSurfaceTextureID(int paramInt1, int paramInt2);

    native void rsnAllocationGetSurfaceTextureID2(int paramInt1, int paramInt2, SurfaceTexture paramSurfaceTexture);

    native int rsnAllocationGetType(int paramInt1, int paramInt2);

    native void rsnAllocationIoReceive(int paramInt1, int paramInt2);

    native void rsnAllocationIoSend(int paramInt1, int paramInt2);

    native void rsnAllocationRead(int paramInt1, int paramInt2, byte[] paramArrayOfByte);

    native void rsnAllocationRead(int paramInt1, int paramInt2, float[] paramArrayOfFloat);

    native void rsnAllocationRead(int paramInt1, int paramInt2, int[] paramArrayOfInt);

    native void rsnAllocationRead(int paramInt1, int paramInt2, short[] paramArrayOfShort);

    native void rsnAllocationResize1D(int paramInt1, int paramInt2, int paramInt3);

    native void rsnAllocationResize2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    native void rsnAllocationSetSurface(int paramInt1, int paramInt2, Surface paramSurface);

    native void rsnAllocationSyncAll(int paramInt1, int paramInt2, int paramInt3);

    native void rsnAssignName(int paramInt1, int paramInt2, byte[] paramArrayOfByte);

    native void rsnContextBindProgramFragment(int paramInt1, int paramInt2);

    native void rsnContextBindProgramRaster(int paramInt1, int paramInt2);

    native void rsnContextBindProgramStore(int paramInt1, int paramInt2);

    native void rsnContextBindProgramVertex(int paramInt1, int paramInt2);

    native void rsnContextBindRootScript(int paramInt1, int paramInt2);

    native void rsnContextBindSampler(int paramInt1, int paramInt2, int paramInt3);

    native int rsnContextCreate(int paramInt1, int paramInt2, int paramInt3);

    native int rsnContextCreateGL(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, int paramInt13, float paramFloat, int paramInt14);

    native void rsnContextDestroy(int paramInt);

    native void rsnContextDump(int paramInt1, int paramInt2);

    native void rsnContextFinish(int paramInt);

    native void rsnContextPause(int paramInt);

    native void rsnContextResume(int paramInt);

    native void rsnContextSetPriority(int paramInt1, int paramInt2);

    native void rsnContextSetSurface(int paramInt1, int paramInt2, int paramInt3, Surface paramSurface);

    native void rsnContextSetSurfaceTexture(int paramInt1, int paramInt2, int paramInt3, SurfaceTexture paramSurfaceTexture);

    native int rsnElementCreate(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, int paramInt4);

    native int rsnElementCreate2(int paramInt, int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2);

    native void rsnElementGetNativeData(int paramInt1, int paramInt2, int[] paramArrayOfInt);

    native void rsnElementGetSubElements(int paramInt1, int paramInt2, int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2);

    native int rsnFileA3DCreateFromAsset(int paramInt, AssetManager paramAssetManager, String paramString);

    native int rsnFileA3DCreateFromAssetStream(int paramInt1, int paramInt2);

    native int rsnFileA3DCreateFromFile(int paramInt, String paramString);

    native int rsnFileA3DGetEntryByIndex(int paramInt1, int paramInt2, int paramInt3);

    native void rsnFileA3DGetIndexEntries(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt, String[] paramArrayOfString);

    native int rsnFileA3DGetNumIndexEntries(int paramInt1, int paramInt2);

    native int rsnFontCreateFromAsset(int paramInt1, AssetManager paramAssetManager, String paramString, float paramFloat, int paramInt2);

    native int rsnFontCreateFromAssetStream(int paramInt1, String paramString, float paramFloat, int paramInt2, int paramInt3);

    native int rsnFontCreateFromFile(int paramInt1, String paramString, float paramFloat, int paramInt2);

    native String rsnGetName(int paramInt1, int paramInt2);

    native int rsnMeshCreate(int paramInt, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3);

    native int rsnMeshGetIndexCount(int paramInt1, int paramInt2);

    native void rsnMeshGetIndices(int paramInt1, int paramInt2, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt3);

    native int rsnMeshGetVertexBufferCount(int paramInt1, int paramInt2);

    native void rsnMeshGetVertices(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3);

    native void rsnObjDestroy(int paramInt1, int paramInt2);

    native int rsnPathCreate(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, int paramInt4, float paramFloat);

    native void rsnProgramBindConstants(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    native void rsnProgramBindSampler(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    native void rsnProgramBindTexture(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    native int rsnProgramFragmentCreate(int paramInt, String paramString, String[] paramArrayOfString, int[] paramArrayOfInt);

    native int rsnProgramRasterCreate(int paramInt1, boolean paramBoolean, int paramInt2);

    native int rsnProgramStoreCreate(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, int paramInt2, int paramInt3, int paramInt4);

    native int rsnProgramVertexCreate(int paramInt, String paramString, String[] paramArrayOfString, int[] paramArrayOfInt);

    native int rsnSamplerCreate(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, float paramFloat);

    native void rsnScriptBindAllocation(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    native int rsnScriptCCreate(int paramInt1, String paramString1, String paramString2, byte[] paramArrayOfByte, int paramInt2);

    native void rsnScriptForEach(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    native void rsnScriptForEach(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, byte[] paramArrayOfByte);

    native void rsnScriptInvoke(int paramInt1, int paramInt2, int paramInt3);

    native void rsnScriptInvokeV(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfByte);

    native void rsnScriptSetTimeZone(int paramInt1, int paramInt2, byte[] paramArrayOfByte);

    native void rsnScriptSetVarD(int paramInt1, int paramInt2, int paramInt3, double paramDouble);

    native void rsnScriptSetVarF(int paramInt1, int paramInt2, int paramInt3, float paramFloat);

    native void rsnScriptSetVarI(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    native void rsnScriptSetVarJ(int paramInt1, int paramInt2, int paramInt3, long paramLong);

    native void rsnScriptSetVarObj(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    native void rsnScriptSetVarV(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfByte);

    native void rsnScriptSetVarVE(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfByte, int paramInt4, int[] paramArrayOfInt);

    native int rsnTypeCreate(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean1, boolean paramBoolean2);

    native void rsnTypeGetNativeData(int paramInt1, int paramInt2, int[] paramArrayOfInt);

    int safeID(BaseObj paramBaseObj)
    {
        if (paramBaseObj != null);
        for (int i = paramBaseObj.getID(this); ; i = 0)
            return i;
    }

    public void setErrorHandler(RSErrorHandler paramRSErrorHandler)
    {
        this.mErrorCallback = paramRSErrorHandler;
    }

    public void setMessageHandler(RSMessageHandler paramRSMessageHandler)
    {
        this.mMessageCallback = paramRSMessageHandler;
    }

    public void setPriority(Priority paramPriority)
    {
        validate();
        nContextSetPriority(paramPriority.mID);
    }

    void validate()
    {
        if (this.mContext == 0)
            throw new RSInvalidStateException("Calling RS with no Context active.");
    }

    static class MessageThread extends Thread
    {
        static final int RS_ERROR_FATAL_UNKNOWN = 4096;
        static final int RS_MESSAGE_TO_CLIENT_ERROR = 3;
        static final int RS_MESSAGE_TO_CLIENT_EXCEPTION = 1;
        static final int RS_MESSAGE_TO_CLIENT_NONE = 0;
        static final int RS_MESSAGE_TO_CLIENT_RESIZE = 2;
        static final int RS_MESSAGE_TO_CLIENT_USER = 4;
        int[] mAuxData = new int[2];
        RenderScript mRS;
        boolean mRun = true;

        MessageThread(RenderScript paramRenderScript)
        {
            super();
            this.mRS = paramRenderScript;
        }

        public void run()
        {
            int[] arrayOfInt = new int[16];
            this.mRS.nContextInitToClient(this.mRS.mContext);
            while (this.mRun)
            {
                arrayOfInt[0] = 0;
                int i = this.mRS.nContextPeekMessage(this.mRS.mContext, this.mAuxData);
                int j = this.mAuxData[1];
                int k = this.mAuxData[0];
                if (i == 4)
                {
                    if (j >> 2 >= arrayOfInt.length)
                        arrayOfInt = new int[j + 3 >> 2];
                    if (this.mRS.nContextGetUserMessage(this.mRS.mContext, arrayOfInt) != 4)
                        throw new RSDriverException("Error processing message from Renderscript.");
                    if (this.mRS.mMessageCallback != null)
                    {
                        this.mRS.mMessageCallback.mData = arrayOfInt;
                        this.mRS.mMessageCallback.mID = k;
                        this.mRS.mMessageCallback.mLength = j;
                        this.mRS.mMessageCallback.run();
                    }
                    else
                    {
                        throw new RSInvalidStateException("Received a message from the script with no message handler installed.");
                    }
                }
                else if (i == 3)
                {
                    String str = this.mRS.nContextGetErrorMessage(this.mRS.mContext);
                    if (k >= 4096)
                        throw new RSRuntimeException("Fatal error " + k + ", details: " + str);
                    if (this.mRS.mErrorCallback != null)
                    {
                        this.mRS.mErrorCallback.mErrorMessage = str;
                        this.mRS.mErrorCallback.mErrorNum = k;
                        this.mRS.mErrorCallback.run();
                    }
                }
                else
                {
                    try
                    {
                        sleep(1L, 0);
                    }
                    catch (InterruptedException localInterruptedException)
                    {
                    }
                }
            }
            Log.d("RenderScript_jni", "MessageThread exiting.");
        }
    }

    public static enum Priority
    {
        int mID;

        static
        {
            Priority[] arrayOfPriority = new Priority[2];
            arrayOfPriority[0] = LOW;
            arrayOfPriority[1] = NORMAL;
        }

        private Priority(int paramInt)
        {
            this.mID = paramInt;
        }
    }

    public static class RSErrorHandler
        implements Runnable
    {
        protected String mErrorMessage;
        protected int mErrorNum;

        public void run()
        {
        }
    }

    public static class RSMessageHandler
        implements Runnable
    {
        protected int[] mData;
        protected int mID;
        protected int mLength;

        public void run()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.RenderScript
 * JD-Core Version:        0.6.2
 */