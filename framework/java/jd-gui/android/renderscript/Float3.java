package android.renderscript;

public class Float3
{
    public float x;
    public float y;
    public float z;

    public Float3()
    {
    }

    public Float3(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        this.x = paramFloat1;
        this.y = paramFloat2;
        this.z = paramFloat3;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.renderscript.Float3
 * JD-Core Version:        0.6.2
 */