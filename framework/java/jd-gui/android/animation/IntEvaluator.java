package android.animation;

public class IntEvaluator
    implements TypeEvaluator<Integer>
{
    public Integer evaluate(float paramFloat, Integer paramInteger1, Integer paramInteger2)
    {
        int i = paramInteger1.intValue();
        return Integer.valueOf((int)(i + paramFloat * (paramInteger2.intValue() - i)));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.animation.IntEvaluator
 * JD-Core Version:        0.6.2
 */