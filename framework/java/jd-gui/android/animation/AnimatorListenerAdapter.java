package android.animation;

public abstract class AnimatorListenerAdapter
    implements Animator.AnimatorListener
{
    public void onAnimationCancel(Animator paramAnimator)
    {
    }

    public void onAnimationEnd(Animator paramAnimator)
    {
    }

    public void onAnimationRepeat(Animator paramAnimator)
    {
    }

    public void onAnimationStart(Animator paramAnimator)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.animation.AnimatorListenerAdapter
 * JD-Core Version:        0.6.2
 */