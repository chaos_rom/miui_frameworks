package android.animation;

public class TimeAnimator extends ValueAnimator
{
    private TimeListener mListener;
    private long mPreviousTime = -1L;

    void animateValue(float paramFloat)
    {
    }

    boolean animationFrame(long paramLong)
    {
        long l1 = 0L;
        long l2;
        if (this.mListener != null)
        {
            l2 = paramLong - this.mStartTime;
            if (this.mPreviousTime >= l1)
                break label46;
        }
        while (true)
        {
            this.mPreviousTime = paramLong;
            this.mListener.onTimeUpdate(this, l2, l1);
            return false;
            label46: l1 = paramLong - this.mPreviousTime;
        }
    }

    void initAnimation()
    {
    }

    public void setTimeListener(TimeListener paramTimeListener)
    {
        this.mListener = paramTimeListener;
    }

    public static abstract interface TimeListener
    {
        public abstract void onTimeUpdate(TimeAnimator paramTimeAnimator, long paramLong1, long paramLong2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.animation.TimeAnimator
 * JD-Core Version:        0.6.2
 */