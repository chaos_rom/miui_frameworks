package android.animation;

public abstract interface TypeEvaluator<T>
{
    public abstract T evaluate(float paramFloat, T paramT1, T paramT2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.animation.TypeEvaluator
 * JD-Core Version:        0.6.2
 */