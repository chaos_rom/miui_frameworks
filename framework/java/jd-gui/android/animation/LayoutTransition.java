package android.animation;

import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

public class LayoutTransition
{
    public static final int APPEARING = 2;
    public static final int CHANGE_APPEARING = 0;
    public static final int CHANGE_DISAPPEARING = 1;
    public static final int CHANGING = 4;
    private static long DEFAULT_DURATION = 0L;
    public static final int DISAPPEARING = 3;
    private static final int FLAG_APPEARING = 1;
    private static final int FLAG_CHANGE_APPEARING = 4;
    private static final int FLAG_CHANGE_DISAPPEARING = 8;
    private static final int FLAG_CHANGING = 16;
    private static final int FLAG_DISAPPEARING = 2;
    private static ObjectAnimator defaultChange;
    private static ObjectAnimator defaultChangeIn;
    private static ObjectAnimator defaultChangeOut;
    private static ObjectAnimator defaultFadeIn;
    private static ObjectAnimator defaultFadeOut;
    private final LinkedHashMap<View, Animator> currentAppearingAnimations = new LinkedHashMap();
    private final LinkedHashMap<View, Animator> currentChangingAnimations = new LinkedHashMap();
    private final LinkedHashMap<View, Animator> currentDisappearingAnimations = new LinkedHashMap();
    private final HashMap<View, View.OnLayoutChangeListener> layoutChangeListenerMap = new HashMap();
    private boolean mAnimateParentHierarchy = true;
    private Animator mAppearingAnim = null;
    private long mAppearingDelay = DEFAULT_DURATION;
    private long mAppearingDuration = DEFAULT_DURATION;
    private TimeInterpolator mAppearingInterpolator = new AccelerateDecelerateInterpolator();
    private Animator mChangingAnim = null;
    private Animator mChangingAppearingAnim = null;
    private long mChangingAppearingDelay = 0L;
    private long mChangingAppearingDuration = DEFAULT_DURATION;
    private TimeInterpolator mChangingAppearingInterpolator = new DecelerateInterpolator();
    private long mChangingAppearingStagger = 0L;
    private long mChangingDelay = 0L;
    private Animator mChangingDisappearingAnim = null;
    private long mChangingDisappearingDelay = DEFAULT_DURATION;
    private long mChangingDisappearingDuration = DEFAULT_DURATION;
    private TimeInterpolator mChangingDisappearingInterpolator = new DecelerateInterpolator();
    private long mChangingDisappearingStagger = 0L;
    private long mChangingDuration = DEFAULT_DURATION;
    private TimeInterpolator mChangingInterpolator = new DecelerateInterpolator();
    private long mChangingStagger = 0L;
    private Animator mDisappearingAnim = null;
    private long mDisappearingDelay = 0L;
    private long mDisappearingDuration = DEFAULT_DURATION;
    private TimeInterpolator mDisappearingInterpolator = new AccelerateDecelerateInterpolator();
    private ArrayList<TransitionListener> mListeners;
    private int mTransitionTypes = 15;
    private final HashMap<View, Animator> pendingAnimations = new HashMap();
    private long staggerDelay;

    public LayoutTransition()
    {
        if (defaultChangeIn == null)
        {
            int[] arrayOfInt1 = new int[2];
            arrayOfInt1[0] = 0;
            arrayOfInt1[1] = 1;
            PropertyValuesHolder localPropertyValuesHolder1 = PropertyValuesHolder.ofInt("left", arrayOfInt1);
            int[] arrayOfInt2 = new int[2];
            arrayOfInt2[0] = 0;
            arrayOfInt2[1] = 1;
            PropertyValuesHolder localPropertyValuesHolder2 = PropertyValuesHolder.ofInt("top", arrayOfInt2);
            int[] arrayOfInt3 = new int[2];
            arrayOfInt3[0] = 0;
            arrayOfInt3[1] = 1;
            PropertyValuesHolder localPropertyValuesHolder3 = PropertyValuesHolder.ofInt("right", arrayOfInt3);
            int[] arrayOfInt4 = new int[2];
            arrayOfInt4[0] = 0;
            arrayOfInt4[1] = 1;
            PropertyValuesHolder localPropertyValuesHolder4 = PropertyValuesHolder.ofInt("bottom", arrayOfInt4);
            int[] arrayOfInt5 = new int[2];
            arrayOfInt5[0] = 0;
            arrayOfInt5[1] = 1;
            PropertyValuesHolder localPropertyValuesHolder5 = PropertyValuesHolder.ofInt("scrollX", arrayOfInt5);
            int[] arrayOfInt6 = new int[2];
            arrayOfInt6[0] = 0;
            arrayOfInt6[1] = 1;
            PropertyValuesHolder localPropertyValuesHolder6 = PropertyValuesHolder.ofInt("scrollY", arrayOfInt6);
            Object localObject = (Object)null;
            PropertyValuesHolder[] arrayOfPropertyValuesHolder = new PropertyValuesHolder[6];
            arrayOfPropertyValuesHolder[0] = localPropertyValuesHolder1;
            arrayOfPropertyValuesHolder[1] = localPropertyValuesHolder2;
            arrayOfPropertyValuesHolder[2] = localPropertyValuesHolder3;
            arrayOfPropertyValuesHolder[3] = localPropertyValuesHolder4;
            arrayOfPropertyValuesHolder[4] = localPropertyValuesHolder5;
            arrayOfPropertyValuesHolder[5] = localPropertyValuesHolder6;
            defaultChangeIn = ObjectAnimator.ofPropertyValuesHolder(localObject, arrayOfPropertyValuesHolder);
            defaultChangeIn.setDuration(DEFAULT_DURATION);
            defaultChangeIn.setStartDelay(this.mChangingAppearingDelay);
            defaultChangeIn.setInterpolator(this.mChangingAppearingInterpolator);
            defaultChangeOut = defaultChangeIn.clone();
            defaultChangeOut.setStartDelay(this.mChangingDisappearingDelay);
            defaultChangeOut.setInterpolator(this.mChangingDisappearingInterpolator);
            defaultChange = defaultChangeIn.clone();
            defaultChange.setStartDelay(this.mChangingDelay);
            defaultChange.setInterpolator(this.mChangingInterpolator);
            float[] arrayOfFloat1 = new float[2];
            arrayOfFloat1[0] = 0.0F;
            arrayOfFloat1[1] = 1.0F;
            defaultFadeIn = ObjectAnimator.ofFloat(null, "alpha", arrayOfFloat1);
            defaultFadeIn.setDuration(DEFAULT_DURATION);
            defaultFadeIn.setStartDelay(this.mAppearingDelay);
            defaultFadeIn.setInterpolator(this.mAppearingInterpolator);
            float[] arrayOfFloat2 = new float[2];
            arrayOfFloat2[0] = 1.0F;
            arrayOfFloat2[1] = 0.0F;
            defaultFadeOut = ObjectAnimator.ofFloat(null, "alpha", arrayOfFloat2);
            defaultFadeOut.setDuration(DEFAULT_DURATION);
            defaultFadeOut.setStartDelay(this.mDisappearingDelay);
            defaultFadeOut.setInterpolator(this.mDisappearingInterpolator);
        }
        this.mChangingAppearingAnim = defaultChangeIn;
        this.mChangingDisappearingAnim = defaultChangeOut;
        this.mChangingAnim = defaultChange;
        this.mAppearingAnim = defaultFadeIn;
        this.mDisappearingAnim = defaultFadeOut;
    }

    private void addChild(ViewGroup paramViewGroup, View paramView, boolean paramBoolean)
    {
        if (paramViewGroup.getWindowVisibility() != 0);
        while (true)
        {
            return;
            if ((0x1 & this.mTransitionTypes) == 1)
                cancel(3);
            if ((paramBoolean) && ((0x4 & this.mTransitionTypes) == 4))
            {
                cancel(0);
                cancel(4);
            }
            if ((hasListeners()) && ((0x1 & this.mTransitionTypes) == 1))
            {
                Iterator localIterator = ((ArrayList)this.mListeners.clone()).iterator();
                while (localIterator.hasNext())
                    ((TransitionListener)localIterator.next()).startTransition(this, paramViewGroup, paramView, 2);
            }
            if ((paramBoolean) && ((0x4 & this.mTransitionTypes) == 4))
                runChangeTransition(paramViewGroup, paramView, 2);
            if ((0x1 & this.mTransitionTypes) == 1)
                runAppearingTransition(paramViewGroup, paramView);
        }
    }

    private boolean hasListeners()
    {
        if ((this.mListeners != null) && (this.mListeners.size() > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void removeChild(ViewGroup paramViewGroup, View paramView, boolean paramBoolean)
    {
        if (paramViewGroup.getWindowVisibility() != 0);
        while (true)
        {
            return;
            if ((0x2 & this.mTransitionTypes) == 2)
                cancel(2);
            if ((paramBoolean) && ((0x8 & this.mTransitionTypes) == 8))
            {
                cancel(1);
                cancel(4);
            }
            if ((hasListeners()) && ((0x2 & this.mTransitionTypes) == 2))
            {
                Iterator localIterator = ((ArrayList)this.mListeners.clone()).iterator();
                while (localIterator.hasNext())
                    ((TransitionListener)localIterator.next()).startTransition(this, paramViewGroup, paramView, 3);
            }
            if ((paramBoolean) && ((0x8 & this.mTransitionTypes) == 8))
                runChangeTransition(paramViewGroup, paramView, 3);
            if ((0x2 & this.mTransitionTypes) == 2)
                runDisappearingTransition(paramViewGroup, paramView);
        }
    }

    private void runAppearingTransition(final ViewGroup paramViewGroup, final View paramView)
    {
        Animator localAnimator1 = (Animator)this.currentDisappearingAnimations.get(paramView);
        if (localAnimator1 != null)
            localAnimator1.cancel();
        if (this.mAppearingAnim == null)
        {
            if (hasListeners())
            {
                Iterator localIterator = ((ArrayList)this.mListeners.clone()).iterator();
                while (localIterator.hasNext())
                    ((TransitionListener)localIterator.next()).endTransition(this, paramViewGroup, paramView, 2);
            }
        }
        else
        {
            Animator localAnimator2 = this.mAppearingAnim.clone();
            localAnimator2.setTarget(paramView);
            localAnimator2.setStartDelay(this.mAppearingDelay);
            localAnimator2.setDuration(this.mAppearingDuration);
            if ((localAnimator2 instanceof ObjectAnimator))
                ((ObjectAnimator)localAnimator2).setCurrentPlayTime(0L);
            localAnimator2.addListener(new AnimatorListenerAdapter()
            {
                public void onAnimationEnd(Animator paramAnonymousAnimator)
                {
                    LayoutTransition.this.currentAppearingAnimations.remove(paramView);
                    if (LayoutTransition.this.hasListeners())
                    {
                        Iterator localIterator = ((ArrayList)LayoutTransition.this.mListeners.clone()).iterator();
                        while (localIterator.hasNext())
                            ((LayoutTransition.TransitionListener)localIterator.next()).endTransition(LayoutTransition.this, paramViewGroup, paramView, 2);
                    }
                }
            });
            this.currentAppearingAnimations.put(paramView, localAnimator2);
            localAnimator2.start();
        }
    }

    private void runChangeTransition(final ViewGroup paramViewGroup, View paramView, int paramInt)
    {
        Animator localAnimator = null;
        ObjectAnimator localObjectAnimator = null;
        long l;
        switch (paramInt)
        {
        default:
            l = 0L;
            if (localAnimator != null)
                break;
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return;
            localAnimator = this.mChangingAppearingAnim;
            l = this.mChangingAppearingDuration;
            localObjectAnimator = defaultChangeIn;
            break;
            localAnimator = this.mChangingDisappearingAnim;
            l = this.mChangingDisappearingDuration;
            localObjectAnimator = defaultChangeOut;
            break;
            localAnimator = this.mChangingAnim;
            l = this.mChangingDuration;
            localObjectAnimator = defaultChange;
            break;
            this.staggerDelay = 0L;
            ViewTreeObserver localViewTreeObserver = paramViewGroup.getViewTreeObserver();
            if (localViewTreeObserver.isAlive())
            {
                int i = paramViewGroup.getChildCount();
                for (int j = 0; j < i; j++)
                {
                    View localView = paramViewGroup.getChildAt(j);
                    if (localView != paramView)
                        setupChangeAnimation(paramViewGroup, paramInt, localAnimator, l, localView);
                }
                if (this.mAnimateParentHierarchy)
                {
                    ViewGroup localViewGroup = paramViewGroup;
                    while (localViewGroup != null)
                    {
                        ViewParent localViewParent = localViewGroup.getParent();
                        if ((localViewParent instanceof ViewGroup))
                        {
                            setupChangeAnimation((ViewGroup)localViewParent, paramInt, localObjectAnimator, l, localViewGroup);
                            localViewGroup = (ViewGroup)localViewParent;
                        }
                        else
                        {
                            localViewGroup = null;
                        }
                    }
                }
                localViewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener()
                {
                    public boolean onPreDraw()
                    {
                        paramViewGroup.getViewTreeObserver().removeOnPreDrawListener(this);
                        if (LayoutTransition.this.layoutChangeListenerMap.size() > 0)
                        {
                            Iterator localIterator = LayoutTransition.this.layoutChangeListenerMap.keySet().iterator();
                            while (localIterator.hasNext())
                            {
                                View localView = (View)localIterator.next();
                                localView.removeOnLayoutChangeListener((View.OnLayoutChangeListener)LayoutTransition.this.layoutChangeListenerMap.get(localView));
                            }
                        }
                        LayoutTransition.this.layoutChangeListenerMap.clear();
                        return true;
                    }
                });
            }
        }
    }

    private void runDisappearingTransition(final ViewGroup paramViewGroup, final View paramView)
    {
        Animator localAnimator1 = (Animator)this.currentAppearingAnimations.get(paramView);
        if (localAnimator1 != null)
            localAnimator1.cancel();
        if (this.mDisappearingAnim == null)
        {
            if (hasListeners())
            {
                Iterator localIterator = ((ArrayList)this.mListeners.clone()).iterator();
                while (localIterator.hasNext())
                    ((TransitionListener)localIterator.next()).endTransition(this, paramViewGroup, paramView, 3);
            }
        }
        else
        {
            Animator localAnimator2 = this.mDisappearingAnim.clone();
            localAnimator2.setStartDelay(this.mDisappearingDelay);
            localAnimator2.setDuration(this.mDisappearingDuration);
            localAnimator2.setTarget(paramView);
            localAnimator2.addListener(new AnimatorListenerAdapter()
            {
                public void onAnimationEnd(Animator paramAnonymousAnimator)
                {
                    LayoutTransition.this.currentDisappearingAnimations.remove(paramView);
                    paramView.setAlpha(this.val$preAnimAlpha);
                    if (LayoutTransition.this.hasListeners())
                    {
                        Iterator localIterator = ((ArrayList)LayoutTransition.this.mListeners.clone()).iterator();
                        while (localIterator.hasNext())
                            ((LayoutTransition.TransitionListener)localIterator.next()).endTransition(LayoutTransition.this, paramViewGroup, paramView, 3);
                    }
                }
            });
            if ((localAnimator2 instanceof ObjectAnimator))
                ((ObjectAnimator)localAnimator2).setCurrentPlayTime(0L);
            this.currentDisappearingAnimations.put(paramView, localAnimator2);
            localAnimator2.start();
        }
    }

    private void setupChangeAnimation(final ViewGroup paramViewGroup, final int paramInt, Animator paramAnimator, final long paramLong, final View paramView)
    {
        if (this.layoutChangeListenerMap.get(paramView) != null);
        while (true)
        {
            return;
            if ((paramView.getWidth() != 0) || (paramView.getHeight() != 0))
            {
                final Animator localAnimator1 = paramAnimator.clone();
                localAnimator1.setTarget(paramView);
                localAnimator1.setupStartValues();
                Animator localAnimator2 = (Animator)this.pendingAnimations.get(paramView);
                if (localAnimator2 != null)
                {
                    localAnimator2.cancel();
                    this.pendingAnimations.remove(paramView);
                }
                this.pendingAnimations.put(paramView, localAnimator1);
                float[] arrayOfFloat = new float[2];
                arrayOfFloat[0] = 0.0F;
                arrayOfFloat[1] = 1.0F;
                ValueAnimator localValueAnimator = ValueAnimator.ofFloat(arrayOfFloat).setDuration(100L + paramLong);
                localValueAnimator.addListener(new AnimatorListenerAdapter()
                {
                    public void onAnimationEnd(Animator paramAnonymousAnimator)
                    {
                        LayoutTransition.this.pendingAnimations.remove(paramView);
                    }
                });
                localValueAnimator.start();
                final View.OnLayoutChangeListener local3 = new View.OnLayoutChangeListener()
                {
                    public void onLayoutChange(View paramAnonymousView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4, int paramAnonymousInt5, int paramAnonymousInt6, int paramAnonymousInt7, int paramAnonymousInt8)
                    {
                        localAnimator1.setupEndValues();
                        if ((localAnimator1 instanceof ValueAnimator))
                        {
                            int i = 0;
                            PropertyValuesHolder[] arrayOfPropertyValuesHolder = ((ValueAnimator)localAnimator1).getValues();
                            for (int j = 0; j < arrayOfPropertyValuesHolder.length; j++)
                            {
                                KeyframeSet localKeyframeSet = arrayOfPropertyValuesHolder[j].mKeyframeSet;
                                if ((localKeyframeSet.mFirstKeyframe == null) || (localKeyframeSet.mLastKeyframe == null) || (!localKeyframeSet.mFirstKeyframe.getValue().equals(localKeyframeSet.mLastKeyframe.getValue())))
                                    i = 1;
                            }
                            if (i == 0)
                                return;
                        }
                        long l = 0L;
                        switch (paramInt)
                        {
                        default:
                        case 2:
                        case 3:
                        case 4:
                        }
                        while (true)
                        {
                            localAnimator1.setStartDelay(l);
                            localAnimator1.setDuration(paramLong);
                            Animator localAnimator = (Animator)LayoutTransition.this.currentChangingAnimations.get(paramViewGroup);
                            if (localAnimator != null)
                                localAnimator.cancel();
                            if ((Animator)LayoutTransition.this.pendingAnimations.get(paramViewGroup) != null)
                                LayoutTransition.this.pendingAnimations.remove(paramViewGroup);
                            LayoutTransition.this.currentChangingAnimations.put(paramViewGroup, localAnimator1);
                            this.val$parent.requestTransitionStart(LayoutTransition.this);
                            paramViewGroup.removeOnLayoutChangeListener(this);
                            LayoutTransition.this.layoutChangeListenerMap.remove(paramViewGroup);
                            break;
                            l = LayoutTransition.this.mChangingAppearingDelay + LayoutTransition.this.staggerDelay;
                            LayoutTransition.access$314(LayoutTransition.this, LayoutTransition.this.mChangingAppearingStagger);
                            continue;
                            l = LayoutTransition.this.mChangingDisappearingDelay + LayoutTransition.this.staggerDelay;
                            LayoutTransition.access$314(LayoutTransition.this, LayoutTransition.this.mChangingDisappearingStagger);
                            continue;
                            l = LayoutTransition.this.mChangingDelay + LayoutTransition.this.staggerDelay;
                            LayoutTransition.access$314(LayoutTransition.this, LayoutTransition.this.mChangingStagger);
                        }
                    }
                };
                localAnimator1.addListener(new AnimatorListenerAdapter()
                {
                    public void onAnimationCancel(Animator paramAnonymousAnimator)
                    {
                        paramView.removeOnLayoutChangeListener(local3);
                        LayoutTransition.this.layoutChangeListenerMap.remove(paramView);
                    }

                    public void onAnimationEnd(Animator paramAnonymousAnimator)
                    {
                        LayoutTransition.this.currentChangingAnimations.remove(paramView);
                        if (LayoutTransition.this.hasListeners())
                        {
                            Iterator localIterator = ((ArrayList)LayoutTransition.this.mListeners.clone()).iterator();
                            if (localIterator.hasNext())
                            {
                                LayoutTransition.TransitionListener localTransitionListener = (LayoutTransition.TransitionListener)localIterator.next();
                                LayoutTransition localLayoutTransition = LayoutTransition.this;
                                ViewGroup localViewGroup = paramViewGroup;
                                View localView = paramView;
                                int i;
                                if (paramInt == 2)
                                    i = 0;
                                while (true)
                                {
                                    localTransitionListener.endTransition(localLayoutTransition, localViewGroup, localView, i);
                                    break;
                                    if (paramInt == 3)
                                        i = 1;
                                    else
                                        i = 4;
                                }
                            }
                        }
                    }

                    public void onAnimationStart(Animator paramAnonymousAnimator)
                    {
                        if (LayoutTransition.this.hasListeners())
                        {
                            Iterator localIterator = ((ArrayList)LayoutTransition.this.mListeners.clone()).iterator();
                            if (localIterator.hasNext())
                            {
                                LayoutTransition.TransitionListener localTransitionListener = (LayoutTransition.TransitionListener)localIterator.next();
                                LayoutTransition localLayoutTransition = LayoutTransition.this;
                                ViewGroup localViewGroup = paramViewGroup;
                                View localView = paramView;
                                int i;
                                if (paramInt == 2)
                                    i = 0;
                                while (true)
                                {
                                    localTransitionListener.startTransition(localLayoutTransition, localViewGroup, localView, i);
                                    break;
                                    if (paramInt == 3)
                                        i = 1;
                                    else
                                        i = 4;
                                }
                            }
                        }
                    }
                });
                paramView.addOnLayoutChangeListener(local3);
                this.layoutChangeListenerMap.put(paramView, local3);
            }
        }
    }

    public void addChild(ViewGroup paramViewGroup, View paramView)
    {
        addChild(paramViewGroup, paramView, true);
    }

    public void addTransitionListener(TransitionListener paramTransitionListener)
    {
        if (this.mListeners == null)
            this.mListeners = new ArrayList();
        this.mListeners.add(paramTransitionListener);
    }

    public void cancel()
    {
        if (this.currentChangingAnimations.size() > 0)
        {
            Iterator localIterator3 = ((LinkedHashMap)this.currentChangingAnimations.clone()).values().iterator();
            while (localIterator3.hasNext())
                ((Animator)localIterator3.next()).cancel();
            this.currentChangingAnimations.clear();
        }
        if (this.currentAppearingAnimations.size() > 0)
        {
            Iterator localIterator2 = ((LinkedHashMap)this.currentAppearingAnimations.clone()).values().iterator();
            while (localIterator2.hasNext())
                ((Animator)localIterator2.next()).end();
            this.currentAppearingAnimations.clear();
        }
        if (this.currentDisappearingAnimations.size() > 0)
        {
            Iterator localIterator1 = ((LinkedHashMap)this.currentDisappearingAnimations.clone()).values().iterator();
            while (localIterator1.hasNext())
                ((Animator)localIterator1.next()).end();
            this.currentDisappearingAnimations.clear();
        }
    }

    public void cancel(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            if (this.currentChangingAnimations.size() > 0)
            {
                Iterator localIterator3 = ((LinkedHashMap)this.currentChangingAnimations.clone()).values().iterator();
                while (localIterator3.hasNext())
                    ((Animator)localIterator3.next()).cancel();
                this.currentChangingAnimations.clear();
                continue;
                if (this.currentAppearingAnimations.size() > 0)
                {
                    Iterator localIterator2 = ((LinkedHashMap)this.currentAppearingAnimations.clone()).values().iterator();
                    while (localIterator2.hasNext())
                        ((Animator)localIterator2.next()).end();
                    this.currentAppearingAnimations.clear();
                    continue;
                    if (this.currentDisappearingAnimations.size() > 0)
                    {
                        Iterator localIterator1 = ((LinkedHashMap)this.currentDisappearingAnimations.clone()).values().iterator();
                        while (localIterator1.hasNext())
                            ((Animator)localIterator1.next()).end();
                        this.currentDisappearingAnimations.clear();
                    }
                }
            }
        }
    }

    public void disableTransitionType(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 2:
        case 3:
        case 0:
        case 1:
        case 4:
        }
        while (true)
        {
            return;
            this.mTransitionTypes = (0xFFFFFFFE & this.mTransitionTypes);
            continue;
            this.mTransitionTypes = (0xFFFFFFFD & this.mTransitionTypes);
            continue;
            this.mTransitionTypes = (0xFFFFFFFB & this.mTransitionTypes);
            continue;
            this.mTransitionTypes = (0xFFFFFFF7 & this.mTransitionTypes);
            continue;
            this.mTransitionTypes = (0xFFFFFFEF & this.mTransitionTypes);
        }
    }

    public void enableTransitionType(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 2:
        case 3:
        case 0:
        case 1:
        case 4:
        }
        while (true)
        {
            return;
            this.mTransitionTypes = (0x1 | this.mTransitionTypes);
            continue;
            this.mTransitionTypes = (0x2 | this.mTransitionTypes);
            continue;
            this.mTransitionTypes = (0x4 | this.mTransitionTypes);
            continue;
            this.mTransitionTypes = (0x8 | this.mTransitionTypes);
            continue;
            this.mTransitionTypes = (0x10 | this.mTransitionTypes);
        }
    }

    public void endChangingAnimations()
    {
        Iterator localIterator = ((LinkedHashMap)this.currentChangingAnimations.clone()).values().iterator();
        while (localIterator.hasNext())
        {
            Animator localAnimator = (Animator)localIterator.next();
            localAnimator.start();
            localAnimator.end();
        }
        this.currentChangingAnimations.clear();
    }

    public Animator getAnimator(int paramInt)
    {
        Animator localAnimator;
        switch (paramInt)
        {
        default:
            localAnimator = null;
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return localAnimator;
            localAnimator = this.mChangingAppearingAnim;
            continue;
            localAnimator = this.mChangingDisappearingAnim;
            continue;
            localAnimator = this.mChangingAnim;
            continue;
            localAnimator = this.mAppearingAnim;
            continue;
            localAnimator = this.mDisappearingAnim;
        }
    }

    public long getDuration(int paramInt)
    {
        long l;
        switch (paramInt)
        {
        default:
            l = 0L;
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return l;
            l = this.mChangingAppearingDuration;
            continue;
            l = this.mChangingDisappearingDuration;
            continue;
            l = this.mChangingDuration;
            continue;
            l = this.mAppearingDuration;
            continue;
            l = this.mDisappearingDuration;
        }
    }

    public TimeInterpolator getInterpolator(int paramInt)
    {
        TimeInterpolator localTimeInterpolator;
        switch (paramInt)
        {
        default:
            localTimeInterpolator = null;
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return localTimeInterpolator;
            localTimeInterpolator = this.mChangingAppearingInterpolator;
            continue;
            localTimeInterpolator = this.mChangingDisappearingInterpolator;
            continue;
            localTimeInterpolator = this.mChangingInterpolator;
            continue;
            localTimeInterpolator = this.mAppearingInterpolator;
            continue;
            localTimeInterpolator = this.mDisappearingInterpolator;
        }
    }

    public long getStagger(int paramInt)
    {
        long l;
        switch (paramInt)
        {
        case 2:
        case 3:
        default:
            l = 0L;
        case 0:
        case 1:
        case 4:
        }
        while (true)
        {
            return l;
            l = this.mChangingAppearingStagger;
            continue;
            l = this.mChangingDisappearingStagger;
            continue;
            l = this.mChangingStagger;
        }
    }

    public long getStartDelay(int paramInt)
    {
        long l;
        switch (paramInt)
        {
        default:
            l = 0L;
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return l;
            l = this.mChangingAppearingDelay;
            continue;
            l = this.mChangingDisappearingDelay;
            continue;
            l = this.mChangingDelay;
            continue;
            l = this.mAppearingDelay;
            continue;
            l = this.mDisappearingDelay;
        }
    }

    public List<TransitionListener> getTransitionListeners()
    {
        return this.mListeners;
    }

    @Deprecated
    public void hideChild(ViewGroup paramViewGroup, View paramView)
    {
        removeChild(paramViewGroup, paramView, true);
    }

    public void hideChild(ViewGroup paramViewGroup, View paramView, int paramInt)
    {
        if (paramInt == 8);
        for (boolean bool = true; ; bool = false)
        {
            removeChild(paramViewGroup, paramView, bool);
            return;
        }
    }

    public boolean isChangingLayout()
    {
        if (this.currentChangingAnimations.size() > 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isRunning()
    {
        if ((this.currentChangingAnimations.size() > 0) || (this.currentAppearingAnimations.size() > 0) || (this.currentDisappearingAnimations.size() > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isTransitionTypeEnabled(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        default:
            i = 0;
        case 2:
        case 3:
        case 0:
        case 1:
        case 4:
        }
        while (true)
        {
            return i;
            if ((0x1 & this.mTransitionTypes) != i)
            {
                int j = 0;
                continue;
                if ((0x2 & this.mTransitionTypes) != 2)
                {
                    j = 0;
                    continue;
                    if ((0x4 & this.mTransitionTypes) != 4)
                    {
                        j = 0;
                        continue;
                        if ((0x8 & this.mTransitionTypes) != 8)
                        {
                            j = 0;
                            continue;
                            if ((0x10 & this.mTransitionTypes) != 16)
                                j = 0;
                        }
                    }
                }
            }
        }
    }

    public void layoutChange(ViewGroup paramViewGroup)
    {
        if (paramViewGroup.getWindowVisibility() != 0);
        while (true)
        {
            return;
            if (((0x10 & this.mTransitionTypes) == 16) && (!isRunning()))
                runChangeTransition(paramViewGroup, null, 4);
        }
    }

    public void removeChild(ViewGroup paramViewGroup, View paramView)
    {
        removeChild(paramViewGroup, paramView, true);
    }

    public void removeTransitionListener(TransitionListener paramTransitionListener)
    {
        if (this.mListeners == null);
        while (true)
        {
            return;
            this.mListeners.remove(paramTransitionListener);
        }
    }

    public void setAnimateParentHierarchy(boolean paramBoolean)
    {
        this.mAnimateParentHierarchy = paramBoolean;
    }

    public void setAnimator(int paramInt, Animator paramAnimator)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            this.mChangingAppearingAnim = paramAnimator;
            continue;
            this.mChangingDisappearingAnim = paramAnimator;
            continue;
            this.mChangingAnim = paramAnimator;
            continue;
            this.mAppearingAnim = paramAnimator;
            continue;
            this.mDisappearingAnim = paramAnimator;
        }
    }

    public void setDuration(int paramInt, long paramLong)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            this.mChangingAppearingDuration = paramLong;
            continue;
            this.mChangingDisappearingDuration = paramLong;
            continue;
            this.mChangingDuration = paramLong;
            continue;
            this.mAppearingDuration = paramLong;
            continue;
            this.mDisappearingDuration = paramLong;
        }
    }

    public void setDuration(long paramLong)
    {
        this.mChangingAppearingDuration = paramLong;
        this.mChangingDisappearingDuration = paramLong;
        this.mChangingDuration = paramLong;
        this.mAppearingDuration = paramLong;
        this.mDisappearingDuration = paramLong;
    }

    public void setInterpolator(int paramInt, TimeInterpolator paramTimeInterpolator)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            this.mChangingAppearingInterpolator = paramTimeInterpolator;
            continue;
            this.mChangingDisappearingInterpolator = paramTimeInterpolator;
            continue;
            this.mChangingInterpolator = paramTimeInterpolator;
            continue;
            this.mAppearingInterpolator = paramTimeInterpolator;
            continue;
            this.mDisappearingInterpolator = paramTimeInterpolator;
        }
    }

    public void setStagger(int paramInt, long paramLong)
    {
        switch (paramInt)
        {
        case 2:
        case 3:
        default:
        case 0:
        case 1:
        case 4:
        }
        while (true)
        {
            return;
            this.mChangingAppearingStagger = paramLong;
            continue;
            this.mChangingDisappearingStagger = paramLong;
            continue;
            this.mChangingStagger = paramLong;
        }
    }

    public void setStartDelay(int paramInt, long paramLong)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        case 4:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            this.mChangingAppearingDelay = paramLong;
            continue;
            this.mChangingDisappearingDelay = paramLong;
            continue;
            this.mChangingDelay = paramLong;
            continue;
            this.mAppearingDelay = paramLong;
            continue;
            this.mDisappearingDelay = paramLong;
        }
    }

    @Deprecated
    public void showChild(ViewGroup paramViewGroup, View paramView)
    {
        addChild(paramViewGroup, paramView, true);
    }

    public void showChild(ViewGroup paramViewGroup, View paramView, int paramInt)
    {
        if (paramInt == 8);
        for (boolean bool = true; ; bool = false)
        {
            addChild(paramViewGroup, paramView, bool);
            return;
        }
    }

    public void startChangingAnimations()
    {
        Iterator localIterator = ((LinkedHashMap)this.currentChangingAnimations.clone()).values().iterator();
        while (localIterator.hasNext())
        {
            Animator localAnimator = (Animator)localIterator.next();
            if ((localAnimator instanceof ObjectAnimator))
                ((ObjectAnimator)localAnimator).setCurrentPlayTime(0L);
            localAnimator.start();
        }
    }

    public static abstract interface TransitionListener
    {
        public abstract void endTransition(LayoutTransition paramLayoutTransition, ViewGroup paramViewGroup, View paramView, int paramInt);

        public abstract void startTransition(LayoutTransition paramLayoutTransition, ViewGroup paramViewGroup, View paramView, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.animation.LayoutTransition
 * JD-Core Version:        0.6.2
 */