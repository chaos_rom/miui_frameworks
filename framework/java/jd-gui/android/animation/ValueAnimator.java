package android.animation;

import android.os.Looper;
import android.util.AndroidRuntimeException;
import android.view.Choreographer;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ValueAnimator extends Animator
{
    public static final int INFINITE = -1;
    public static final int RESTART = 1;
    public static final int REVERSE = 2;
    static final int RUNNING = 1;
    static final int SEEKED = 2;
    static final int STOPPED;
    private static ThreadLocal<AnimationHandler> sAnimationHandler = new ThreadLocal();
    private static final TimeInterpolator sDefaultInterpolator = new AccelerateDecelerateInterpolator();
    private static float sDurationScale = 1.0F;
    private float mCurrentFraction = 0.0F;
    private int mCurrentIteration = 0;
    private long mDelayStartTime;
    private long mDuration = ()(300.0F * sDurationScale);
    boolean mInitialized = false;
    private TimeInterpolator mInterpolator = sDefaultInterpolator;
    private boolean mPlayingBackwards = false;
    int mPlayingState = 0;
    private int mRepeatCount = 0;
    private int mRepeatMode = 1;
    private boolean mRunning = false;
    long mSeekTime = -1L;
    private long mStartDelay = 0L;
    private boolean mStartListenersCalled = false;
    long mStartTime;
    private boolean mStarted = false;
    private boolean mStartedDelay = false;
    private long mUnscaledDuration = 300L;
    private long mUnscaledStartDelay = 0L;
    private ArrayList<AnimatorUpdateListener> mUpdateListeners = null;
    PropertyValuesHolder[] mValues;
    HashMap<String, PropertyValuesHolder> mValuesMap;

    public static void clearAllAnimations()
    {
        AnimationHandler localAnimationHandler = (AnimationHandler)sAnimationHandler.get();
        if (localAnimationHandler != null)
        {
            localAnimationHandler.mAnimations.clear();
            localAnimationHandler.mPendingAnimations.clear();
            localAnimationHandler.mDelayedAnims.clear();
        }
    }

    private boolean delayedAnimationFrame(long paramLong)
    {
        int i = 1;
        if (!this.mStartedDelay)
        {
            this.mStartedDelay = i;
            this.mDelayStartTime = paramLong;
            i = 0;
        }
        while (true)
        {
            return i;
            long l = paramLong - this.mDelayStartTime;
            if (l <= this.mStartDelay)
                break;
            this.mStartTime = (paramLong - (l - this.mStartDelay));
            this.mPlayingState = i;
        }
    }

    private void endAnimation(AnimationHandler paramAnimationHandler)
    {
        paramAnimationHandler.mAnimations.remove(this);
        paramAnimationHandler.mPendingAnimations.remove(this);
        paramAnimationHandler.mDelayedAnims.remove(this);
        this.mPlayingState = 0;
        if (((this.mStarted) || (this.mRunning)) && (this.mListeners != null))
        {
            if (!this.mRunning)
                notifyStartListeners();
            ArrayList localArrayList = (ArrayList)this.mListeners.clone();
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((Animator.AnimatorListener)localArrayList.get(j)).onAnimationEnd(this);
        }
        this.mRunning = false;
        this.mStarted = false;
        this.mStartListenersCalled = false;
    }

    public static int getCurrentAnimationsCount()
    {
        AnimationHandler localAnimationHandler = (AnimationHandler)sAnimationHandler.get();
        if (localAnimationHandler != null);
        for (int i = localAnimationHandler.mAnimations.size(); ; i = 0)
            return i;
    }

    public static long getFrameDelay()
    {
        return Choreographer.getFrameDelay();
    }

    private AnimationHandler getOrCreateAnimationHandler()
    {
        AnimationHandler localAnimationHandler = (AnimationHandler)sAnimationHandler.get();
        if (localAnimationHandler == null)
        {
            localAnimationHandler = new AnimationHandler(null);
            sAnimationHandler.set(localAnimationHandler);
        }
        return localAnimationHandler;
    }

    private void notifyStartListeners()
    {
        if ((this.mListeners != null) && (!this.mStartListenersCalled))
        {
            ArrayList localArrayList = (ArrayList)this.mListeners.clone();
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                ((Animator.AnimatorListener)localArrayList.get(j)).onAnimationStart(this);
        }
        this.mStartListenersCalled = true;
    }

    public static ValueAnimator ofFloat(float[] paramArrayOfFloat)
    {
        ValueAnimator localValueAnimator = new ValueAnimator();
        localValueAnimator.setFloatValues(paramArrayOfFloat);
        return localValueAnimator;
    }

    public static ValueAnimator ofInt(int[] paramArrayOfInt)
    {
        ValueAnimator localValueAnimator = new ValueAnimator();
        localValueAnimator.setIntValues(paramArrayOfInt);
        return localValueAnimator;
    }

    public static ValueAnimator ofObject(TypeEvaluator paramTypeEvaluator, Object[] paramArrayOfObject)
    {
        ValueAnimator localValueAnimator = new ValueAnimator();
        localValueAnimator.setObjectValues(paramArrayOfObject);
        localValueAnimator.setEvaluator(paramTypeEvaluator);
        return localValueAnimator;
    }

    public static ValueAnimator ofPropertyValuesHolder(PropertyValuesHolder[] paramArrayOfPropertyValuesHolder)
    {
        ValueAnimator localValueAnimator = new ValueAnimator();
        localValueAnimator.setValues(paramArrayOfPropertyValuesHolder);
        return localValueAnimator;
    }

    public static void setDurationScale(float paramFloat)
    {
        sDurationScale = paramFloat;
    }

    public static void setFrameDelay(long paramLong)
    {
        Choreographer.setFrameDelay(paramLong);
    }

    private void start(boolean paramBoolean)
    {
        if (Looper.myLooper() == null)
            throw new AndroidRuntimeException("Animators may only be run on Looper threads");
        this.mPlayingBackwards = paramBoolean;
        this.mCurrentIteration = 0;
        this.mPlayingState = 0;
        this.mStarted = true;
        this.mStartedDelay = false;
        AnimationHandler localAnimationHandler = getOrCreateAnimationHandler();
        localAnimationHandler.mPendingAnimations.add(this);
        if (this.mStartDelay == 0L)
        {
            setCurrentPlayTime(0L);
            this.mPlayingState = 0;
            this.mRunning = true;
            notifyStartListeners();
        }
        localAnimationHandler.start();
    }

    private void startAnimation(AnimationHandler paramAnimationHandler)
    {
        initAnimation();
        paramAnimationHandler.mAnimations.add(this);
        if ((this.mStartDelay > 0L) && (this.mListeners != null))
            notifyStartListeners();
    }

    public void addUpdateListener(AnimatorUpdateListener paramAnimatorUpdateListener)
    {
        if (this.mUpdateListeners == null)
            this.mUpdateListeners = new ArrayList();
        this.mUpdateListeners.add(paramAnimatorUpdateListener);
    }

    void animateValue(float paramFloat)
    {
        float f = this.mInterpolator.getInterpolation(paramFloat);
        this.mCurrentFraction = f;
        int i = this.mValues.length;
        for (int j = 0; j < i; j++)
            this.mValues[j].calculateValue(f);
        if (this.mUpdateListeners != null)
        {
            int k = this.mUpdateListeners.size();
            for (int m = 0; m < k; m++)
                ((AnimatorUpdateListener)this.mUpdateListeners.get(m)).onAnimationUpdate(this);
        }
    }

    boolean animationFrame(long paramLong)
    {
        boolean bool1 = false;
        switch (this.mPlayingState)
        {
        default:
            return bool1;
        case 1:
        case 2:
        }
        float f;
        if (this.mDuration > 0L)
            f = (float)(paramLong - this.mStartTime) / (float)this.mDuration;
        boolean bool2;
        while (f >= 1.0F)
        {
            if ((this.mCurrentIteration >= this.mRepeatCount) && (this.mRepeatCount != -1))
                break label220;
            if (this.mListeners != null)
            {
                int i = this.mListeners.size();
                int j = 0;
                while (true)
                    if (j < i)
                    {
                        ((Animator.AnimatorListener)this.mListeners.get(j)).onAnimationRepeat(this);
                        j++;
                        continue;
                        f = 1.0F;
                        break;
                    }
            }
            if (this.mRepeatMode == 2)
            {
                if (!this.mPlayingBackwards)
                    break label214;
                bool2 = false;
                label155: this.mPlayingBackwards = bool2;
            }
            this.mCurrentIteration += (int)f;
            f %= 1.0F;
            this.mStartTime += this.mDuration;
        }
        while (true)
        {
            if (this.mPlayingBackwards)
                f = 1.0F - f;
            animateValue(f);
            break;
            label214: bool2 = true;
            break label155;
            label220: bool1 = true;
            f = Math.min(f, 1.0F);
        }
    }

    public void cancel()
    {
        AnimationHandler localAnimationHandler = getOrCreateAnimationHandler();
        if ((this.mPlayingState != 0) || (localAnimationHandler.mPendingAnimations.contains(this)) || (localAnimationHandler.mDelayedAnims.contains(this)))
        {
            if (((this.mStarted) || (this.mRunning)) && (this.mListeners != null))
            {
                if (!this.mRunning)
                    notifyStartListeners();
                Iterator localIterator = ((ArrayList)this.mListeners.clone()).iterator();
                while (localIterator.hasNext())
                    ((Animator.AnimatorListener)localIterator.next()).onAnimationCancel(this);
            }
            endAnimation(localAnimationHandler);
        }
    }

    public ValueAnimator clone()
    {
        ValueAnimator localValueAnimator = (ValueAnimator)super.clone();
        if (this.mUpdateListeners != null)
        {
            ArrayList localArrayList = this.mUpdateListeners;
            localValueAnimator.mUpdateListeners = new ArrayList();
            int k = localArrayList.size();
            for (int m = 0; m < k; m++)
                localValueAnimator.mUpdateListeners.add(localArrayList.get(m));
        }
        localValueAnimator.mSeekTime = -1L;
        localValueAnimator.mPlayingBackwards = false;
        localValueAnimator.mCurrentIteration = 0;
        localValueAnimator.mInitialized = false;
        localValueAnimator.mPlayingState = 0;
        localValueAnimator.mStartedDelay = false;
        PropertyValuesHolder[] arrayOfPropertyValuesHolder = this.mValues;
        if (arrayOfPropertyValuesHolder != null)
        {
            int i = arrayOfPropertyValuesHolder.length;
            localValueAnimator.mValues = new PropertyValuesHolder[i];
            localValueAnimator.mValuesMap = new HashMap(i);
            for (int j = 0; j < i; j++)
            {
                PropertyValuesHolder localPropertyValuesHolder = arrayOfPropertyValuesHolder[j].clone();
                localValueAnimator.mValues[j] = localPropertyValuesHolder;
                localValueAnimator.mValuesMap.put(localPropertyValuesHolder.getPropertyName(), localPropertyValuesHolder);
            }
        }
        return localValueAnimator;
    }

    final boolean doAnimationFrame(long paramLong)
    {
        if (this.mPlayingState == 0)
        {
            this.mPlayingState = 1;
            if (this.mSeekTime >= 0L)
                break label39;
            this.mStartTime = paramLong;
        }
        while (true)
        {
            return animationFrame(Math.max(paramLong, this.mStartTime));
            label39: this.mStartTime = (paramLong - this.mSeekTime);
            this.mSeekTime = -1L;
        }
    }

    public void end()
    {
        AnimationHandler localAnimationHandler = getOrCreateAnimationHandler();
        if ((!localAnimationHandler.mAnimations.contains(this)) && (!localAnimationHandler.mPendingAnimations.contains(this)))
        {
            this.mStartedDelay = false;
            startAnimation(localAnimationHandler);
            this.mStarted = true;
            if ((this.mRepeatCount <= 0) || ((0x1 & this.mRepeatCount) != 1))
                break label84;
            animateValue(0.0F);
        }
        while (true)
        {
            endAnimation(localAnimationHandler);
            return;
            if (this.mInitialized)
                break;
            initAnimation();
            break;
            label84: animateValue(1.0F);
        }
    }

    public float getAnimatedFraction()
    {
        return this.mCurrentFraction;
    }

    public Object getAnimatedValue()
    {
        if ((this.mValues != null) && (this.mValues.length > 0));
        for (Object localObject = this.mValues[0].getAnimatedValue(); ; localObject = null)
            return localObject;
    }

    public Object getAnimatedValue(String paramString)
    {
        PropertyValuesHolder localPropertyValuesHolder = (PropertyValuesHolder)this.mValuesMap.get(paramString);
        if (localPropertyValuesHolder != null);
        for (Object localObject = localPropertyValuesHolder.getAnimatedValue(); ; localObject = null)
            return localObject;
    }

    public long getCurrentPlayTime()
    {
        if ((!this.mInitialized) || (this.mPlayingState == 0));
        for (long l = 0L; ; l = AnimationUtils.currentAnimationTimeMillis() - this.mStartTime)
            return l;
    }

    public long getDuration()
    {
        return this.mUnscaledDuration;
    }

    public TimeInterpolator getInterpolator()
    {
        return this.mInterpolator;
    }

    public int getRepeatCount()
    {
        return this.mRepeatCount;
    }

    public int getRepeatMode()
    {
        return this.mRepeatMode;
    }

    public long getStartDelay()
    {
        return this.mUnscaledStartDelay;
    }

    public PropertyValuesHolder[] getValues()
    {
        return this.mValues;
    }

    void initAnimation()
    {
        if (!this.mInitialized)
        {
            int i = this.mValues.length;
            for (int j = 0; j < i; j++)
                this.mValues[j].init();
            this.mInitialized = true;
        }
    }

    public boolean isRunning()
    {
        int i = 1;
        if ((this.mPlayingState == i) || (this.mRunning));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean isStarted()
    {
        return this.mStarted;
    }

    public void removeAllUpdateListeners()
    {
        if (this.mUpdateListeners == null);
        while (true)
        {
            return;
            this.mUpdateListeners.clear();
            this.mUpdateListeners = null;
        }
    }

    public void removeUpdateListener(AnimatorUpdateListener paramAnimatorUpdateListener)
    {
        if (this.mUpdateListeners == null);
        while (true)
        {
            return;
            this.mUpdateListeners.remove(paramAnimatorUpdateListener);
            if (this.mUpdateListeners.size() == 0)
                this.mUpdateListeners = null;
        }
    }

    public void reverse()
    {
        boolean bool;
        if (!this.mPlayingBackwards)
        {
            bool = true;
            this.mPlayingBackwards = bool;
            if (this.mPlayingState != 1)
                break label53;
            long l1 = AnimationUtils.currentAnimationTimeMillis();
            long l2 = l1 - this.mStartTime;
            this.mStartTime = (l1 - (this.mDuration - l2));
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label53: start(true);
        }
    }

    public void setCurrentPlayTime(long paramLong)
    {
        initAnimation();
        long l = AnimationUtils.currentAnimationTimeMillis();
        if (this.mPlayingState != 1)
        {
            this.mSeekTime = paramLong;
            this.mPlayingState = 2;
        }
        this.mStartTime = (l - paramLong);
        doAnimationFrame(l);
    }

    public ValueAnimator setDuration(long paramLong)
    {
        if (paramLong < 0L)
            throw new IllegalArgumentException("Animators cannot have negative duration: " + paramLong);
        this.mUnscaledDuration = paramLong;
        this.mDuration = (()((float)paramLong * sDurationScale));
        return this;
    }

    public void setEvaluator(TypeEvaluator paramTypeEvaluator)
    {
        if ((paramTypeEvaluator != null) && (this.mValues != null) && (this.mValues.length > 0))
            this.mValues[0].setEvaluator(paramTypeEvaluator);
    }

    public void setFloatValues(float[] paramArrayOfFloat)
    {
        if ((paramArrayOfFloat == null) || (paramArrayOfFloat.length == 0))
            return;
        if ((this.mValues == null) || (this.mValues.length == 0))
        {
            PropertyValuesHolder[] arrayOfPropertyValuesHolder = new PropertyValuesHolder[1];
            arrayOfPropertyValuesHolder[0] = PropertyValuesHolder.ofFloat("", paramArrayOfFloat);
            setValues(arrayOfPropertyValuesHolder);
        }
        while (true)
        {
            this.mInitialized = false;
            break;
            this.mValues[0].setFloatValues(paramArrayOfFloat);
        }
    }

    public void setIntValues(int[] paramArrayOfInt)
    {
        if ((paramArrayOfInt == null) || (paramArrayOfInt.length == 0))
            return;
        if ((this.mValues == null) || (this.mValues.length == 0))
        {
            PropertyValuesHolder[] arrayOfPropertyValuesHolder = new PropertyValuesHolder[1];
            arrayOfPropertyValuesHolder[0] = PropertyValuesHolder.ofInt("", paramArrayOfInt);
            setValues(arrayOfPropertyValuesHolder);
        }
        while (true)
        {
            this.mInitialized = false;
            break;
            this.mValues[0].setIntValues(paramArrayOfInt);
        }
    }

    public void setInterpolator(TimeInterpolator paramTimeInterpolator)
    {
        if (paramTimeInterpolator != null);
        for (this.mInterpolator = paramTimeInterpolator; ; this.mInterpolator = new LinearInterpolator())
            return;
    }

    public void setObjectValues(Object[] paramArrayOfObject)
    {
        if ((paramArrayOfObject == null) || (paramArrayOfObject.length == 0))
            return;
        if ((this.mValues == null) || (this.mValues.length == 0))
        {
            PropertyValuesHolder[] arrayOfPropertyValuesHolder = new PropertyValuesHolder[1];
            arrayOfPropertyValuesHolder[0] = PropertyValuesHolder.ofObject("", (TypeEvaluator)null, paramArrayOfObject);
            setValues(arrayOfPropertyValuesHolder);
        }
        while (true)
        {
            this.mInitialized = false;
            break;
            this.mValues[0].setObjectValues(paramArrayOfObject);
        }
    }

    public void setRepeatCount(int paramInt)
    {
        this.mRepeatCount = paramInt;
    }

    public void setRepeatMode(int paramInt)
    {
        this.mRepeatMode = paramInt;
    }

    public void setStartDelay(long paramLong)
    {
        this.mStartDelay = (()((float)paramLong * sDurationScale));
        this.mUnscaledStartDelay = paramLong;
    }

    public void setValues(PropertyValuesHolder[] paramArrayOfPropertyValuesHolder)
    {
        int i = paramArrayOfPropertyValuesHolder.length;
        this.mValues = paramArrayOfPropertyValuesHolder;
        this.mValuesMap = new HashMap(i);
        for (int j = 0; j < i; j++)
        {
            PropertyValuesHolder localPropertyValuesHolder = paramArrayOfPropertyValuesHolder[j];
            this.mValuesMap.put(localPropertyValuesHolder.getPropertyName(), localPropertyValuesHolder);
        }
        this.mInitialized = false;
    }

    public void start()
    {
        start(false);
    }

    public String toString()
    {
        String str = "ValueAnimator@" + Integer.toHexString(hashCode());
        if (this.mValues != null)
            for (int i = 0; i < this.mValues.length; i++)
                str = str + "\n        " + this.mValues[i].toString();
        return str;
    }

    public static abstract interface AnimatorUpdateListener
    {
        public abstract void onAnimationUpdate(ValueAnimator paramValueAnimator);
    }

    private static class AnimationHandler
        implements Runnable
    {
        private boolean mAnimationScheduled;
        private final ArrayList<ValueAnimator> mAnimations = new ArrayList();
        private final Choreographer mChoreographer = Choreographer.getInstance();
        private final ArrayList<ValueAnimator> mDelayedAnims = new ArrayList();
        private final ArrayList<ValueAnimator> mEndingAnims = new ArrayList();
        private final ArrayList<ValueAnimator> mPendingAnimations = new ArrayList();
        private final ArrayList<ValueAnimator> mReadyAnims = new ArrayList();

        private void doAnimationFrame(long paramLong)
        {
            while (this.mPendingAnimations.size() > 0)
            {
                ArrayList localArrayList = (ArrayList)this.mPendingAnimations.clone();
                this.mPendingAnimations.clear();
                int i3 = localArrayList.size();
                int i4 = 0;
                if (i4 < i3)
                {
                    ValueAnimator localValueAnimator4 = (ValueAnimator)localArrayList.get(i4);
                    if (localValueAnimator4.mStartDelay == 0L)
                        localValueAnimator4.startAnimation(this);
                    while (true)
                    {
                        i4++;
                        break;
                        this.mDelayedAnims.add(localValueAnimator4);
                    }
                }
            }
            int i = this.mDelayedAnims.size();
            for (int j = 0; j < i; j++)
            {
                ValueAnimator localValueAnimator3 = (ValueAnimator)this.mDelayedAnims.get(j);
                if (localValueAnimator3.delayedAnimationFrame(paramLong))
                    this.mReadyAnims.add(localValueAnimator3);
            }
            int k = this.mReadyAnims.size();
            if (k > 0)
            {
                for (int i2 = 0; i2 < k; i2++)
                {
                    ValueAnimator localValueAnimator2 = (ValueAnimator)this.mReadyAnims.get(i2);
                    localValueAnimator2.startAnimation(this);
                    ValueAnimator.access$302(localValueAnimator2, true);
                    this.mDelayedAnims.remove(localValueAnimator2);
                }
                this.mReadyAnims.clear();
            }
            int m = this.mAnimations.size();
            int n = 0;
            while (n < m)
            {
                ValueAnimator localValueAnimator1 = (ValueAnimator)this.mAnimations.get(n);
                if (localValueAnimator1.doAnimationFrame(paramLong))
                    this.mEndingAnims.add(localValueAnimator1);
                if (this.mAnimations.size() == m)
                {
                    n++;
                }
                else
                {
                    m--;
                    this.mEndingAnims.remove(localValueAnimator1);
                }
            }
            if (this.mEndingAnims.size() > 0)
            {
                for (int i1 = 0; i1 < this.mEndingAnims.size(); i1++)
                    ((ValueAnimator)this.mEndingAnims.get(i1)).endAnimation(this);
                this.mEndingAnims.clear();
            }
            if ((!this.mAnimations.isEmpty()) || (!this.mDelayedAnims.isEmpty()))
                scheduleAnimation();
        }

        private void scheduleAnimation()
        {
            if (!this.mAnimationScheduled)
            {
                this.mChoreographer.postCallback(1, this, null);
                this.mAnimationScheduled = true;
            }
        }

        public void run()
        {
            this.mAnimationScheduled = false;
            doAnimationFrame(this.mChoreographer.getFrameTime());
        }

        public void start()
        {
            scheduleAnimation();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.animation.ValueAnimator
 * JD-Core Version:        0.6.2
 */