package android.animation;

public abstract interface TimeInterpolator
{
    public abstract float getInterpolation(float paramFloat);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.animation.TimeInterpolator
 * JD-Core Version:        0.6.2
 */