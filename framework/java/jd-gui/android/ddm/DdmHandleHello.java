package android.ddm;

import android.os.Debug;
import android.os.Process;
import java.nio.ByteBuffer;
import org.apache.harmony.dalvik.ddmc.Chunk;
import org.apache.harmony.dalvik.ddmc.ChunkHandler;
import org.apache.harmony.dalvik.ddmc.DdmServer;

public class DdmHandleHello extends ChunkHandler
{
    public static final int CHUNK_FEAT = type("FEAT");
    public static final int CHUNK_HELO = type("HELO");
    public static final int CHUNK_WAIT = type("WAIT");
    private static DdmHandleHello mInstance = new DdmHandleHello();

    private Chunk handleFEAT(Chunk paramChunk)
    {
        String[] arrayOfString = Debug.getVmFeatureList();
        int i = 4 + 4 * arrayOfString.length;
        for (int j = -1 + arrayOfString.length; j >= 0; j--)
            i += 2 * arrayOfString[j].length();
        ByteBuffer localByteBuffer = ByteBuffer.allocate(i);
        localByteBuffer.order(ChunkHandler.CHUNK_ORDER);
        localByteBuffer.putInt(arrayOfString.length);
        for (int k = -1 + arrayOfString.length; k >= 0; k--)
        {
            localByteBuffer.putInt(arrayOfString[k].length());
            putString(localByteBuffer, arrayOfString[k]);
        }
        return new Chunk(CHUNK_FEAT, localByteBuffer);
    }

    private Chunk handleHELO(Chunk paramChunk)
    {
        wrapChunk(paramChunk).getInt();
        String str1 = System.getProperty("java.vm.name", "?");
        String str2 = System.getProperty("java.vm.version", "?");
        String str3 = str1 + " v" + str2;
        String str4 = DdmHandleAppName.getAppName();
        ByteBuffer localByteBuffer = ByteBuffer.allocate(16 + 2 * str3.length() + 2 * str4.length());
        localByteBuffer.order(ChunkHandler.CHUNK_ORDER);
        localByteBuffer.putInt(1);
        localByteBuffer.putInt(Process.myPid());
        localByteBuffer.putInt(str3.length());
        localByteBuffer.putInt(str4.length());
        putString(localByteBuffer, str3);
        putString(localByteBuffer, str4);
        Chunk localChunk = new Chunk(CHUNK_HELO, localByteBuffer);
        if (Debug.waitingForDebugger())
            sendWAIT(0);
        return localChunk;
    }

    public static void register()
    {
        DdmServer.registerHandler(CHUNK_HELO, mInstance);
        DdmServer.registerHandler(CHUNK_FEAT, mInstance);
    }

    public static void sendWAIT(int paramInt)
    {
        byte[] arrayOfByte = new byte[1];
        arrayOfByte[0] = ((byte)paramInt);
        DdmServer.sendChunk(new Chunk(CHUNK_WAIT, arrayOfByte, 0, 1));
    }

    public void connected()
    {
    }

    public void disconnected()
    {
    }

    public Chunk handleChunk(Chunk paramChunk)
    {
        int i = paramChunk.type;
        if (i == CHUNK_HELO);
        for (Chunk localChunk = handleHELO(paramChunk); ; localChunk = handleFEAT(paramChunk))
        {
            return localChunk;
            if (i != CHUNK_FEAT)
                break;
        }
        throw new RuntimeException("Unknown packet " + ChunkHandler.name(i));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.ddm.DdmHandleHello
 * JD-Core Version:        0.6.2
 */