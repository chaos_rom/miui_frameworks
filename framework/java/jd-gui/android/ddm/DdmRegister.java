package android.ddm;

import org.apache.harmony.dalvik.ddmc.DdmServer;

public class DdmRegister
{
    public static void registerHandlers()
    {
        DdmHandleHello.register();
        DdmHandleThread.register();
        DdmHandleHeap.register();
        DdmHandleNativeHeap.register();
        DdmHandleProfiling.register();
        DdmHandleExit.register();
        DdmServer.registrationComplete();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.ddm.DdmRegister
 * JD-Core Version:        0.6.2
 */