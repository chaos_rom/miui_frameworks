package android.ddm;

import java.nio.ByteBuffer;
import org.apache.harmony.dalvik.ddmc.Chunk;
import org.apache.harmony.dalvik.ddmc.ChunkHandler;
import org.apache.harmony.dalvik.ddmc.DdmServer;

public class DdmHandleAppName extends ChunkHandler
{
    public static final int CHUNK_APNM = type("APNM");
    private static volatile String mAppName = "";
    private static DdmHandleAppName mInstance = new DdmHandleAppName();

    public static String getAppName()
    {
        return mAppName;
    }

    public static void register()
    {
    }

    private static void sendAPNM(String paramString)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(4 + 2 * paramString.length());
        localByteBuffer.order(ChunkHandler.CHUNK_ORDER);
        localByteBuffer.putInt(paramString.length());
        putString(localByteBuffer, paramString);
        DdmServer.sendChunk(new Chunk(CHUNK_APNM, localByteBuffer));
    }

    public static void setAppName(String paramString)
    {
        if ((paramString == null) || (paramString.length() == 0));
        while (true)
        {
            return;
            mAppName = paramString;
            sendAPNM(paramString);
        }
    }

    public void connected()
    {
    }

    public void disconnected()
    {
    }

    public Chunk handleChunk(Chunk paramChunk)
    {
        return null;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.ddm.DdmHandleAppName
 * JD-Core Version:        0.6.2
 */