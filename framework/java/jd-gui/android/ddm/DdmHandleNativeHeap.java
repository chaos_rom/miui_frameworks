package android.ddm;

import android.util.Log;
import org.apache.harmony.dalvik.ddmc.Chunk;
import org.apache.harmony.dalvik.ddmc.ChunkHandler;
import org.apache.harmony.dalvik.ddmc.DdmServer;

public class DdmHandleNativeHeap extends ChunkHandler
{
    public static final int CHUNK_NHGT = type("NHGT");
    private static DdmHandleNativeHeap mInstance = new DdmHandleNativeHeap();

    private native byte[] getLeakInfo();

    private Chunk handleNHGT(Chunk paramChunk)
    {
        byte[] arrayOfByte = getLeakInfo();
        if (arrayOfByte != null)
            Log.i("ddm-nativeheap", "Sending " + arrayOfByte.length + " bytes");
        for (Chunk localChunk = new Chunk(ChunkHandler.type("NHGT"), arrayOfByte, 0, arrayOfByte.length); ; localChunk = createFailChunk(1, "Something went wrong"))
            return localChunk;
    }

    public static void register()
    {
        DdmServer.registerHandler(CHUNK_NHGT, mInstance);
    }

    public void connected()
    {
    }

    public void disconnected()
    {
    }

    public Chunk handleChunk(Chunk paramChunk)
    {
        Log.i("ddm-nativeheap", "Handling " + name(paramChunk.type) + " chunk");
        int i = paramChunk.type;
        if (i == CHUNK_NHGT)
            return handleNHGT(paramChunk);
        throw new RuntimeException("Unknown packet " + ChunkHandler.name(i));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.ddm.DdmHandleNativeHeap
 * JD-Core Version:        0.6.2
 */