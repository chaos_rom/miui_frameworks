package android.mtp;

public class MtpServer
    implements Runnable
{
    private int mNativeContext;

    static
    {
        System.loadLibrary("media_jni");
    }

    public MtpServer(MtpDatabase paramMtpDatabase, boolean paramBoolean)
    {
        native_setup(paramMtpDatabase, paramBoolean);
    }

    private final native void native_add_storage(MtpStorage paramMtpStorage);

    private final native void native_cleanup();

    private final native void native_remove_storage(int paramInt);

    private final native void native_run();

    private final native void native_send_object_added(int paramInt);

    private final native void native_send_object_removed(int paramInt);

    private final native void native_setup(MtpDatabase paramMtpDatabase, boolean paramBoolean);

    public void addStorage(MtpStorage paramMtpStorage)
    {
        native_add_storage(paramMtpStorage);
    }

    public void removeStorage(MtpStorage paramMtpStorage)
    {
        native_remove_storage(paramMtpStorage.getStorageId());
    }

    public void run()
    {
        native_run();
        native_cleanup();
    }

    public void sendObjectAdded(int paramInt)
    {
        native_send_object_added(paramInt);
    }

    public void sendObjectRemoved(int paramInt)
    {
        native_send_object_removed(paramInt);
    }

    public void start()
    {
        new Thread(this, "MtpServer").start();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.mtp.MtpServer
 * JD-Core Version:        0.6.2
 */