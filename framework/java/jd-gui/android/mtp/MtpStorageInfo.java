package android.mtp;

public final class MtpStorageInfo
{
    private String mDescription;
    private long mFreeSpace;
    private long mMaxCapacity;
    private int mStorageId;
    private String mVolumeIdentifier;

    public final String getDescription()
    {
        return this.mDescription;
    }

    public final long getFreeSpace()
    {
        return this.mFreeSpace;
    }

    public final long getMaxCapacity()
    {
        return this.mMaxCapacity;
    }

    public final int getStorageId()
    {
        return this.mStorageId;
    }

    public final String getVolumeIdentifier()
    {
        return this.mVolumeIdentifier;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.mtp.MtpStorageInfo
 * JD-Core Version:        0.6.2
 */