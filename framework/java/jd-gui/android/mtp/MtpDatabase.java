package android.mtp;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.IContentProvider;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaScanner;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.MediaStore.Audio.Playlists;
import android.provider.MediaStore.Files;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.io.File;
import java.util.HashMap;
import java.util.Locale;

public class MtpDatabase
{
    static final int[] ALL_PROPERTIES = arrayOfInt5;
    static final int[] AUDIO_PROPERTIES;
    private static final int DEVICE_PROPERTIES_DATABASE_VERSION = 1;
    static final int[] FILE_PROPERTIES;
    private static final String FORMAT_PARENT_WHERE = "parent=? AND parent=?";
    private static final String FORMAT_WHERE = "parent=?";
    private static final String[] ID_PROJECTION;
    private static final String ID_WHERE = "_id=?";
    static final int[] IMAGE_PROPERTIES;
    private static final String[] OBJECT_INFO_PROJECTION;
    private static final String PARENT_WHERE = "format=?";
    private static final String[] PATH_PROJECTION;
    private static final String[] PATH_SIZE_FORMAT_PROJECTION;
    private static final String PATH_WHERE = "_data=?";
    private static final String STORAGE_FORMAT_PARENT_WHERE = "storage_id=? AND format=? AND parent=?";
    private static final String STORAGE_FORMAT_WHERE = "storage_id=? AND format=?";
    private static final String STORAGE_PARENT_WHERE = "storage_id=? AND parent=?";
    private static final String STORAGE_WHERE = "storage_id=?";
    private static final String TAG = "MtpDatabase";
    static final int[] VIDEO_PROPERTIES;
    private final Context mContext;
    private boolean mDatabaseModified;
    private SharedPreferences mDeviceProperties;
    private final IContentProvider mMediaProvider;
    private final MediaScanner mMediaScanner;
    private final String mMediaStoragePath;
    private int mNativeContext;
    private final Uri mObjectsUri;
    private final HashMap<Integer, MtpPropertyGroup> mPropertyGroupsByFormat = new HashMap();
    private final HashMap<Integer, MtpPropertyGroup> mPropertyGroupsByProperty = new HashMap();
    private final HashMap<String, MtpStorage> mStorageMap = new HashMap();
    private final String[] mSubDirectories;
    private String mSubDirectoriesWhere;
    private String[] mSubDirectoriesWhereArgs;
    private final String mVolumeName;

    static
    {
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "_id";
        ID_PROJECTION = arrayOfString1;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "_id";
        arrayOfString2[1] = "_data";
        PATH_PROJECTION = arrayOfString2;
        String[] arrayOfString3 = new String[4];
        arrayOfString3[0] = "_id";
        arrayOfString3[1] = "_data";
        arrayOfString3[2] = "_size";
        arrayOfString3[3] = "format";
        PATH_SIZE_FORMAT_PROJECTION = arrayOfString3;
        String[] arrayOfString4 = new String[7];
        arrayOfString4[0] = "_id";
        arrayOfString4[1] = "storage_id";
        arrayOfString4[2] = "format";
        arrayOfString4[3] = "parent";
        arrayOfString4[4] = "_data";
        arrayOfString4[5] = "_size";
        arrayOfString4[6] = "date_modified";
        OBJECT_INFO_PROJECTION = arrayOfString4;
        System.loadLibrary("media_jni");
        int[] arrayOfInt1 = new int[10];
        arrayOfInt1[0] = 56321;
        arrayOfInt1[1] = 56322;
        arrayOfInt1[2] = 56323;
        arrayOfInt1[3] = 56324;
        arrayOfInt1[4] = 56327;
        arrayOfInt1[5] = 56329;
        arrayOfInt1[6] = 56331;
        arrayOfInt1[7] = 56385;
        arrayOfInt1[8] = 56388;
        arrayOfInt1[9] = 56398;
        FILE_PROPERTIES = arrayOfInt1;
        int[] arrayOfInt2 = new int[19];
        arrayOfInt2[0] = 56321;
        arrayOfInt2[1] = 56322;
        arrayOfInt2[2] = 56323;
        arrayOfInt2[3] = 56324;
        arrayOfInt2[4] = 56327;
        arrayOfInt2[5] = 56329;
        arrayOfInt2[6] = 56331;
        arrayOfInt2[7] = 56385;
        arrayOfInt2[8] = 56388;
        arrayOfInt2[9] = 56544;
        arrayOfInt2[10] = 56398;
        arrayOfInt2[11] = 56390;
        arrayOfInt2[12] = 56474;
        arrayOfInt2[13] = 56475;
        arrayOfInt2[14] = 56459;
        arrayOfInt2[15] = 56473;
        arrayOfInt2[16] = 56457;
        arrayOfInt2[17] = 56460;
        arrayOfInt2[18] = 56470;
        AUDIO_PROPERTIES = arrayOfInt2;
        int[] arrayOfInt3 = new int[15];
        arrayOfInt3[0] = 56321;
        arrayOfInt3[1] = 56322;
        arrayOfInt3[2] = 56323;
        arrayOfInt3[3] = 56324;
        arrayOfInt3[4] = 56327;
        arrayOfInt3[5] = 56329;
        arrayOfInt3[6] = 56331;
        arrayOfInt3[7] = 56385;
        arrayOfInt3[8] = 56388;
        arrayOfInt3[9] = 56544;
        arrayOfInt3[10] = 56398;
        arrayOfInt3[11] = 56390;
        arrayOfInt3[12] = 56474;
        arrayOfInt3[13] = 56457;
        arrayOfInt3[14] = 56392;
        VIDEO_PROPERTIES = arrayOfInt3;
        int[] arrayOfInt4 = new int[12];
        arrayOfInt4[0] = 56321;
        arrayOfInt4[1] = 56322;
        arrayOfInt4[2] = 56323;
        arrayOfInt4[3] = 56324;
        arrayOfInt4[4] = 56327;
        arrayOfInt4[5] = 56329;
        arrayOfInt4[6] = 56331;
        arrayOfInt4[7] = 56385;
        arrayOfInt4[8] = 56388;
        arrayOfInt4[9] = 56544;
        arrayOfInt4[10] = 56398;
        arrayOfInt4[11] = 56392;
        IMAGE_PROPERTIES = arrayOfInt4;
        int[] arrayOfInt5 = new int[25];
        arrayOfInt5[0] = 56321;
        arrayOfInt5[1] = 56322;
        arrayOfInt5[2] = 56323;
        arrayOfInt5[3] = 56324;
        arrayOfInt5[4] = 56327;
        arrayOfInt5[5] = 56329;
        arrayOfInt5[6] = 56331;
        arrayOfInt5[7] = 56385;
        arrayOfInt5[8] = 56388;
        arrayOfInt5[9] = 56544;
        arrayOfInt5[10] = 56398;
        arrayOfInt5[11] = 56392;
        arrayOfInt5[12] = 56390;
        arrayOfInt5[13] = 56474;
        arrayOfInt5[14] = 56475;
        arrayOfInt5[15] = 56459;
        arrayOfInt5[16] = 56473;
        arrayOfInt5[17] = 56457;
        arrayOfInt5[18] = 56460;
        arrayOfInt5[19] = 56470;
        arrayOfInt5[20] = 56390;
        arrayOfInt5[21] = 56474;
        arrayOfInt5[22] = 56457;
        arrayOfInt5[23] = 56392;
        arrayOfInt5[24] = 56392;
    }

    public MtpDatabase(Context paramContext, String paramString1, String paramString2, String[] paramArrayOfString)
    {
        native_setup();
        this.mContext = paramContext;
        this.mMediaProvider = paramContext.getContentResolver().acquireProvider("media");
        this.mVolumeName = paramString1;
        this.mMediaStoragePath = paramString2;
        this.mObjectsUri = MediaStore.Files.getMtpObjectsUri(paramString1);
        this.mMediaScanner = new MediaScanner(paramContext);
        this.mSubDirectories = paramArrayOfString;
        if (paramArrayOfString != null)
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("(");
            int i = paramArrayOfString.length;
            for (int j = 0; j < i; j++)
            {
                localStringBuilder.append("_data=? OR _data LIKE ?");
                if (j != i - 1)
                    localStringBuilder.append(" OR ");
            }
            localStringBuilder.append(")");
            this.mSubDirectoriesWhere = localStringBuilder.toString();
            this.mSubDirectoriesWhereArgs = new String[i * 2];
            int k = 0;
            int m = 0;
            while (k < i)
            {
                String str3 = paramArrayOfString[k];
                String[] arrayOfString1 = this.mSubDirectoriesWhereArgs;
                int n = m + 1;
                arrayOfString1[m] = str3;
                String[] arrayOfString2 = this.mSubDirectoriesWhereArgs;
                m = n + 1;
                arrayOfString2[n] = (str3 + "/%");
                k++;
            }
        }
        Locale localLocale = paramContext.getResources().getConfiguration().locale;
        String str1;
        if (localLocale != null)
        {
            str1 = localLocale.getLanguage();
            String str2 = localLocale.getCountry();
            if (str1 != null)
            {
                if (str2 == null)
                    break label352;
                this.mMediaScanner.setLocale(str1 + "_" + str2);
            }
        }
        while (true)
        {
            initDeviceProperties(paramContext);
            return;
            label352: this.mMediaScanner.setLocale(str1);
        }
    }

    // ERROR //
    private int beginSendObject(String paramString, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokespecial 257	android/mtp/MtpDatabase:inStorageSubDirectory	(Ljava/lang/String;)Z
        //     5: ifne +10 -> 15
        //     8: bipush 255
        //     10: istore 12
        //     12: iload 12
        //     14: ireturn
        //     15: aload_1
        //     16: ifnull +138 -> 154
        //     19: aconst_null
        //     20: astore 15
        //     22: aload_0
        //     23: getfield 171	android/mtp/MtpDatabase:mMediaProvider	Landroid/content/IContentProvider;
        //     26: astore 19
        //     28: aload_0
        //     29: getfield 183	android/mtp/MtpDatabase:mObjectsUri	Landroid/net/Uri;
        //     32: astore 20
        //     34: getstatic 81	android/mtp/MtpDatabase:ID_PROJECTION	[Ljava/lang/String;
        //     37: astore 21
        //     39: iconst_1
        //     40: anewarray 77	java/lang/String
        //     43: astore 22
        //     45: aload 22
        //     47: iconst_0
        //     48: aload_1
        //     49: aastore
        //     50: aload 19
        //     52: aload 20
        //     54: aload 21
        //     56: ldc 33
        //     58: aload 22
        //     60: aconst_null
        //     61: aconst_null
        //     62: invokeinterface 263 7 0
        //     67: astore 15
        //     69: aload 15
        //     71: ifnull +255 -> 326
        //     74: aload 15
        //     76: invokeinterface 269 1 0
        //     81: ifle +245 -> 326
        //     84: ldc 48
        //     86: new 194	java/lang/StringBuilder
        //     89: dup
        //     90: invokespecial 195	java/lang/StringBuilder:<init>	()V
        //     93: ldc_w 271
        //     96: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     99: aload_1
        //     100: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     103: invokevirtual 211	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     106: invokestatic 277	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     109: pop
        //     110: bipush 255
        //     112: istore 12
        //     114: aload 15
        //     116: ifnull -104 -> 12
        //     119: aload 15
        //     121: invokeinterface 280 1 0
        //     126: goto -114 -> 12
        //     129: astore 17
        //     131: ldc 48
        //     133: ldc_w 282
        //     136: aload 17
        //     138: invokestatic 286	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     141: pop
        //     142: aload 15
        //     144: ifnull +10 -> 154
        //     147: aload 15
        //     149: invokeinterface 280 1 0
        //     154: aload_0
        //     155: iconst_1
        //     156: putfield 288	android/mtp/MtpDatabase:mDatabaseModified	Z
        //     159: new 290	android/content/ContentValues
        //     162: dup
        //     163: invokespecial 291	android/content/ContentValues:<init>	()V
        //     166: astore 9
        //     168: aload 9
        //     170: ldc 83
        //     172: aload_1
        //     173: invokevirtual 295	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
        //     176: aload 9
        //     178: ldc 89
        //     180: iload_2
        //     181: invokestatic 301	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     184: invokevirtual 304	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
        //     187: aload 9
        //     189: ldc 95
        //     191: iload_3
        //     192: invokestatic 301	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     195: invokevirtual 304	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
        //     198: aload 9
        //     200: ldc 93
        //     202: iload 4
        //     204: invokestatic 301	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     207: invokevirtual 304	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
        //     210: aload 9
        //     212: ldc 87
        //     214: lload 5
        //     216: invokestatic 309	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     219: invokevirtual 312	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
        //     222: aload 9
        //     224: ldc 97
        //     226: lload 7
        //     228: invokestatic 309	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     231: invokevirtual 312	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
        //     234: aload_0
        //     235: getfield 171	android/mtp/MtpDatabase:mMediaProvider	Landroid/content/IContentProvider;
        //     238: aload_0
        //     239: getfield 183	android/mtp/MtpDatabase:mObjectsUri	Landroid/net/Uri;
        //     242: aload 9
        //     244: invokeinterface 316 3 0
        //     249: astore 13
        //     251: aload 13
        //     253: ifnull +46 -> 299
        //     256: aload 13
        //     258: invokevirtual 322	android/net/Uri:getPathSegments	()Ljava/util/List;
        //     261: iconst_2
        //     262: invokeinterface 328 2 0
        //     267: checkcast 77	java/lang/String
        //     270: invokestatic 332	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     273: istore 14
        //     275: iload 14
        //     277: istore 12
        //     279: goto -267 -> 12
        //     282: astore 16
        //     284: aload 15
        //     286: ifnull +10 -> 296
        //     289: aload 15
        //     291: invokeinterface 280 1 0
        //     296: aload 16
        //     298: athrow
        //     299: bipush 255
        //     301: istore 12
        //     303: goto -291 -> 12
        //     306: astore 10
        //     308: ldc 48
        //     310: ldc_w 282
        //     313: aload 10
        //     315: invokestatic 286	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     318: pop
        //     319: bipush 255
        //     321: istore 12
        //     323: goto -311 -> 12
        //     326: aload 15
        //     328: ifnull -174 -> 154
        //     331: goto -184 -> 147
        //
        // Exception table:
        //     from	to	target	type
        //     22	110	129	android/os/RemoteException
        //     22	110	282	finally
        //     131	142	282	finally
        //     234	275	306	android/os/RemoteException
    }

    private Cursor createObjectQuery(int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException
    {
        String str;
        if (paramInt1 == -1)
            if (paramInt2 == 0)
                if (paramInt3 == 0)
                {
                    str = null;
                    localObject = null;
                    if (this.mSubDirectoriesWhere != null)
                    {
                        if (str != null)
                            break label302;
                        str = this.mSubDirectoriesWhere;
                    }
                }
        label302: String[] arrayOfString;
        for (Object localObject = this.mSubDirectoriesWhereArgs; ; localObject = arrayOfString)
        {
            return this.mMediaProvider.query(this.mObjectsUri, ID_PROJECTION, str, (String[])localObject, null, null);
            if (paramInt3 == -1)
                paramInt3 = 0;
            str = "format=?";
            localObject = new String[1];
            localObject[0] = Integer.toString(paramInt3);
            break;
            if (paramInt3 == 0)
            {
                str = "parent=?";
                localObject = new String[1];
                localObject[0] = Integer.toString(paramInt2);
                break;
            }
            if (paramInt3 == -1)
                paramInt3 = 0;
            str = "parent=? AND parent=?";
            localObject = new String[2];
            localObject[0] = Integer.toString(paramInt2);
            localObject[1] = Integer.toString(paramInt3);
            break;
            if (paramInt2 == 0)
            {
                if (paramInt3 == 0)
                {
                    str = "storage_id=?";
                    localObject = new String[1];
                    localObject[0] = Integer.toString(paramInt1);
                    break;
                }
                if (paramInt3 == -1)
                    paramInt3 = 0;
                str = "storage_id=? AND parent=?";
                localObject = new String[2];
                localObject[0] = Integer.toString(paramInt1);
                localObject[1] = Integer.toString(paramInt3);
                break;
            }
            if (paramInt3 == 0)
            {
                str = "storage_id=? AND format=?";
                localObject = new String[2];
                localObject[0] = Integer.toString(paramInt1);
                localObject[1] = Integer.toString(paramInt2);
                break;
            }
            if (paramInt3 == -1)
                paramInt3 = 0;
            str = "storage_id=? AND format=? AND parent=?";
            localObject = new String[3];
            localObject[0] = Integer.toString(paramInt1);
            localObject[1] = Integer.toString(paramInt2);
            localObject[2] = Integer.toString(paramInt3);
            break;
            str = str + " AND " + this.mSubDirectoriesWhere;
            arrayOfString = new String[localObject.length + this.mSubDirectoriesWhereArgs.length];
            for (int i = 0; i < localObject.length; i++)
                arrayOfString[i] = localObject[i];
            for (int j = 0; j < this.mSubDirectoriesWhereArgs.length; j++)
            {
                arrayOfString[i] = this.mSubDirectoriesWhereArgs[j];
                i++;
            }
        }
    }

    private int deleteFile(int paramInt)
    {
        this.mDatabaseModified = true;
        Cursor localCursor = null;
        try
        {
            IContentProvider localIContentProvider1 = this.mMediaProvider;
            Uri localUri1 = this.mObjectsUri;
            String[] arrayOfString1 = PATH_SIZE_FORMAT_PROJECTION;
            String[] arrayOfString2 = new String[1];
            arrayOfString2[0] = Integer.toString(paramInt);
            localCursor = localIContentProvider1.query(localUri1, arrayOfString1, "_id=?", arrayOfString2, null, null);
            String str1;
            int j;
            if ((localCursor != null) && (localCursor.moveToNext()))
            {
                str1 = localCursor.getString(1);
                j = localCursor.getInt(3);
                if ((str1 != null) && (j != 0))
                    break label133;
                if (localCursor != null)
                    localCursor.close();
                i = 8194;
            }
            while (true)
            {
                return i;
                i = 8201;
                if (localCursor == null)
                    continue;
                while (true)
                {
                    localCursor.close();
                    break;
                    label133: if (!isStorageSubDirectory(str1))
                        break label154;
                    i = 8205;
                    if (localCursor == null)
                        break;
                }
                label154: if (j == 12289)
                {
                    Uri localUri3 = MediaStore.Files.getMtpObjectsUri(this.mVolumeName);
                    IContentProvider localIContentProvider2 = this.mMediaProvider;
                    String[] arrayOfString3 = new String[3];
                    arrayOfString3[0] = (str1 + "/%");
                    arrayOfString3[1] = Integer.toString(1 + str1.length());
                    arrayOfString3[2] = (str1 + "/");
                    localIContentProvider2.delete(localUri3, "_data LIKE ?1 AND lower(substr(_data,1,?2))=lower(?3)", arrayOfString3);
                }
                Uri localUri2 = MediaStore.Files.getMtpObjectsUri(this.mVolumeName, paramInt);
                if (this.mMediaProvider.delete(localUri2, null, null) > 0)
                {
                    if (j != 12289)
                    {
                        boolean bool = str1.toLowerCase(Locale.US).endsWith("/.nomedia");
                        if (!bool);
                    }
                    try
                    {
                        String str2 = str1.substring(0, str1.lastIndexOf("/"));
                        this.mMediaProvider.call("unhide", str2, null);
                        i = 8193;
                        if (localCursor == null)
                            continue;
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        while (true)
                            Log.e("MtpDatabase", "failed to unhide/rescan for " + str1);
                    }
                }
            }
        }
        catch (RemoteException localRemoteException1)
        {
            while (true)
            {
                Log.e("MtpDatabase", "RemoteException in deleteFile", localRemoteException1);
                if (localCursor != null)
                    localCursor.close();
                int i = 8194;
                continue;
                i = 8201;
                if (localCursor == null);
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    private void endSendObject(String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        ContentValues localContentValues;
        if (paramBoolean)
            if (paramInt2 == 47621)
            {
                String str = paramString;
                int i = str.lastIndexOf('/');
                if (i >= 0)
                    str = str.substring(i + 1);
                if (str.endsWith(".pla"))
                    str = str.substring(0, -4 + str.length());
                localContentValues = new ContentValues(1);
                localContentValues.put("_data", paramString);
                localContentValues.put("name", str);
                localContentValues.put("format", Integer.valueOf(paramInt2));
                localContentValues.put("date_modified", Long.valueOf(System.currentTimeMillis() / 1000L));
                localContentValues.put("media_scanner_new_object_id", Integer.valueOf(paramInt1));
            }
        while (true)
        {
            try
            {
                this.mMediaProvider.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, localContentValues);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("MtpDatabase", "RemoteException in endSendObject", localRemoteException);
                continue;
            }
            this.mMediaScanner.scanMtpFile(paramString, this.mVolumeName, paramInt1, paramInt2);
            continue;
            deleteFile(paramInt1);
        }
    }

    private int getDeviceProperty(int paramInt, long[] paramArrayOfLong, char[] paramArrayOfChar)
    {
        int k;
        switch (paramInt)
        {
        default:
            k = 8202;
        case 54273:
        case 54274:
        case 20483:
        }
        while (true)
        {
            return k;
            String str2 = this.mDeviceProperties.getString(Integer.toString(paramInt), "");
            int m = str2.length();
            if (m > 255)
                m = 255;
            str2.getChars(0, m, paramArrayOfChar, 0);
            paramArrayOfChar[m] = '\000';
            k = 8193;
            continue;
            Display localDisplay = ((WindowManager)this.mContext.getSystemService("window")).getDefaultDisplay();
            int i = localDisplay.getMaximumSizeDimension();
            int j = localDisplay.getMaximumSizeDimension();
            String str1 = Integer.toString(i) + "x" + Integer.toString(j);
            str1.getChars(0, str1.length(), paramArrayOfChar, 0);
            paramArrayOfChar[str1.length()] = '\000';
            k = 8193;
        }
    }

    private int getNumObjects(int paramInt1, int paramInt2, int paramInt3)
    {
        Cursor localCursor = null;
        while (true)
        {
            try
            {
                localCursor = createObjectQuery(paramInt1, paramInt2, paramInt3);
                if (localCursor != null)
                {
                    int j = localCursor.getCount();
                    i = j;
                    return i;
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("MtpDatabase", "RemoteException in getNumObjects", localRemoteException);
                if (localCursor != null)
                    localCursor.close();
                int i = -1;
                continue;
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            if (localCursor == null);
        }
    }

    private int getObjectFilePath(int paramInt, char[] paramArrayOfChar, long[] paramArrayOfLong)
    {
        int i;
        if (paramInt == 0)
        {
            this.mMediaStoragePath.getChars(0, this.mMediaStoragePath.length(), paramArrayOfChar, 0);
            paramArrayOfChar[this.mMediaStoragePath.length()] = '\000';
            paramArrayOfLong[0] = 0L;
            paramArrayOfLong[1] = 12289L;
            i = 8193;
        }
        while (true)
        {
            return i;
            Cursor localCursor = null;
            try
            {
                IContentProvider localIContentProvider = this.mMediaProvider;
                Uri localUri = this.mObjectsUri;
                String[] arrayOfString1 = PATH_SIZE_FORMAT_PROJECTION;
                String[] arrayOfString2 = new String[1];
                arrayOfString2[0] = Integer.toString(paramInt);
                localCursor = localIContentProvider.query(localUri, arrayOfString1, "_id=?", arrayOfString2, null, null);
                if ((localCursor != null) && (localCursor.moveToNext()))
                {
                    String str = localCursor.getString(1);
                    str.getChars(0, str.length(), paramArrayOfChar, 0);
                    paramArrayOfChar[str.length()] = '\000';
                    paramArrayOfLong[0] = localCursor.getLong(2);
                    paramArrayOfLong[1] = localCursor.getLong(3);
                    if (localCursor != null)
                        localCursor.close();
                    i = 8193;
                    continue;
                }
                i = 8201;
                if (localCursor == null)
                    continue;
                localCursor.close();
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Log.e("MtpDatabase", "RemoteException in getObjectFilePath", localRemoteException);
                    i = 8194;
                    if (localCursor == null)
                        break;
                }
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
        }
    }

    private boolean getObjectInfo(int paramInt, int[] paramArrayOfInt, char[] paramArrayOfChar, long[] paramArrayOfLong)
    {
        Cursor localCursor = null;
        while (true)
        {
            try
            {
                IContentProvider localIContentProvider = this.mMediaProvider;
                Uri localUri = this.mObjectsUri;
                String[] arrayOfString1 = OBJECT_INFO_PROJECTION;
                String[] arrayOfString2 = new String[1];
                arrayOfString2[0] = Integer.toString(paramInt);
                localCursor = localIContentProvider.query(localUri, arrayOfString1, "_id=?", arrayOfString2, null, null);
                if ((localCursor != null) && (localCursor.moveToNext()))
                {
                    paramArrayOfInt[0] = localCursor.getInt(1);
                    paramArrayOfInt[1] = localCursor.getInt(2);
                    paramArrayOfInt[2] = localCursor.getInt(3);
                    String str = localCursor.getString(4);
                    int i = str.lastIndexOf('/');
                    if (i >= 0)
                    {
                        j = i + 1;
                        int k = str.length();
                        if (k - j > 255)
                            k = j + 255;
                        str.getChars(j, k, paramArrayOfChar, 0);
                        paramArrayOfChar[(k - j)] = '\000';
                        paramArrayOfLong[0] = localCursor.getLong(5);
                        paramArrayOfLong[1] = localCursor.getLong(6);
                        bool = true;
                        return bool;
                    }
                    int j = 0;
                    continue;
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("MtpDatabase", "RemoteException in getObjectInfo", localRemoteException);
                if (localCursor != null)
                    localCursor.close();
                boolean bool = false;
                continue;
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            if (localCursor == null);
        }
    }

    private int[] getObjectList(int paramInt1, int paramInt2, int paramInt3)
    {
        Object localObject1 = null;
        while (true)
        {
            try
            {
                Cursor localCursor = createObjectQuery(paramInt1, paramInt2, paramInt3);
                localObject1 = localCursor;
                if (localObject1 == null)
                {
                    if (localObject1 != null)
                        localObject1.close();
                    arrayOfInt = null;
                    return arrayOfInt;
                }
                int i = localObject1.getCount();
                if (i <= 0)
                    break label159;
                arrayOfInt = new int[i];
                int j = 0;
                if (j < i)
                {
                    localObject1.moveToNext();
                    arrayOfInt[j] = localObject1.getInt(0);
                    j++;
                    continue;
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("MtpDatabase", "RemoteException in getObjectList", localRemoteException);
                if (localObject1 != null)
                    localObject1.close();
                int[] arrayOfInt = null;
                continue;
            }
            finally
            {
                if (localObject1 != null)
                    localObject1.close();
            }
            if (localObject1 != null)
            {
                localObject1.close();
                continue;
                label159: if (localObject1 == null);
            }
        }
    }

    private MtpPropertyList getObjectPropertyList(long paramLong1, int paramInt1, long paramLong2, int paramInt2, int paramInt3)
    {
        MtpPropertyList localMtpPropertyList;
        if (paramInt2 != 0)
        {
            localMtpPropertyList = new MtpPropertyList(0, 43015);
            return localMtpPropertyList;
        }
        MtpPropertyGroup localMtpPropertyGroup;
        if (paramLong2 == 4294967295L)
        {
            localMtpPropertyGroup = (MtpPropertyGroup)this.mPropertyGroupsByFormat.get(Integer.valueOf(paramInt1));
            if (localMtpPropertyGroup == null)
            {
                int[] arrayOfInt2 = getSupportedObjectProperties(paramInt1);
                localMtpPropertyGroup = new MtpPropertyGroup(this, this.mMediaProvider, this.mVolumeName, arrayOfInt2);
                this.mPropertyGroupsByFormat.put(new Integer(paramInt1), localMtpPropertyGroup);
            }
        }
        while (true)
        {
            localMtpPropertyList = localMtpPropertyGroup.getPropertyList((int)paramLong1, paramInt1, paramInt3);
            break;
            localMtpPropertyGroup = (MtpPropertyGroup)this.mPropertyGroupsByProperty.get(Long.valueOf(paramLong2));
            if (localMtpPropertyGroup == null)
            {
                int[] arrayOfInt1 = new int[1];
                arrayOfInt1[0] = ((int)paramLong2);
                localMtpPropertyGroup = new MtpPropertyGroup(this, this.mMediaProvider, this.mVolumeName, arrayOfInt1);
                this.mPropertyGroupsByProperty.put(new Integer((int)paramLong2), localMtpPropertyGroup);
            }
        }
    }

    private int[] getObjectReferences(int paramInt)
    {
        Uri localUri = MediaStore.Files.getMtpReferencesUri(this.mVolumeName, paramInt);
        Object localObject1 = null;
        while (true)
        {
            try
            {
                Cursor localCursor = this.mMediaProvider.query(localUri, ID_PROJECTION, null, null, null, null);
                localObject1 = localCursor;
                if (localObject1 == null)
                {
                    if (localObject1 != null)
                        localObject1.close();
                    arrayOfInt = null;
                    return arrayOfInt;
                }
                int i = localObject1.getCount();
                if (i <= 0)
                    break label165;
                arrayOfInt = new int[i];
                int j = 0;
                if (j < i)
                {
                    localObject1.moveToNext();
                    arrayOfInt[j] = localObject1.getInt(0);
                    j++;
                    continue;
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("MtpDatabase", "RemoteException in getObjectList", localRemoteException);
                if (localObject1 != null)
                    localObject1.close();
                int[] arrayOfInt = null;
                continue;
            }
            finally
            {
                if (localObject1 != null)
                    localObject1.close();
            }
            if (localObject1 != null)
            {
                localObject1.close();
                continue;
                label165: if (localObject1 == null);
            }
        }
    }

    private int[] getSupportedCaptureFormats()
    {
        return null;
    }

    private int[] getSupportedDeviceProperties()
    {
        int[] arrayOfInt = new int[3];
        arrayOfInt[0] = 54273;
        arrayOfInt[1] = 54274;
        arrayOfInt[2] = 20483;
        return arrayOfInt;
    }

    private int[] getSupportedObjectProperties(int paramInt)
    {
        int[] arrayOfInt;
        switch (paramInt)
        {
        default:
            arrayOfInt = FILE_PROPERTIES;
        case 12296:
        case 12297:
        case 47361:
        case 47362:
        case 47363:
        case 12299:
        case 47489:
        case 47492:
        case 14337:
        case 14340:
        case 14343:
        case 14347:
        case 0:
        }
        while (true)
        {
            return arrayOfInt;
            arrayOfInt = AUDIO_PROPERTIES;
            continue;
            arrayOfInt = VIDEO_PROPERTIES;
            continue;
            arrayOfInt = IMAGE_PROPERTIES;
            continue;
            arrayOfInt = ALL_PROPERTIES;
        }
    }

    private int[] getSupportedPlaybackFormats()
    {
        int[] arrayOfInt = new int[25];
        arrayOfInt[0] = 12288;
        arrayOfInt[1] = 12289;
        arrayOfInt[2] = 12292;
        arrayOfInt[3] = 12293;
        arrayOfInt[4] = 12296;
        arrayOfInt[5] = 12297;
        arrayOfInt[6] = 12299;
        arrayOfInt[7] = 14337;
        arrayOfInt[8] = 14338;
        arrayOfInt[9] = 14343;
        arrayOfInt[10] = 14344;
        arrayOfInt[11] = 14347;
        arrayOfInt[12] = 14349;
        arrayOfInt[13] = 47361;
        arrayOfInt[14] = 47362;
        arrayOfInt[15] = 47363;
        arrayOfInt[16] = 47490;
        arrayOfInt[17] = 47491;
        arrayOfInt[18] = 47492;
        arrayOfInt[19] = 47621;
        arrayOfInt[20] = 47632;
        arrayOfInt[21] = 47633;
        arrayOfInt[22] = 47636;
        arrayOfInt[23] = 47746;
        arrayOfInt[24] = 47366;
        return arrayOfInt;
    }

    private boolean inStorageSubDirectory(String paramString)
    {
        boolean bool;
        if (this.mSubDirectories == null)
            bool = true;
        while (true)
        {
            return bool;
            if (paramString == null)
            {
                bool = false;
            }
            else
            {
                bool = false;
                int i = paramString.length();
                for (int j = 0; (j < this.mSubDirectories.length) && (!bool); j++)
                {
                    String str = this.mSubDirectories[j];
                    int k = str.length();
                    if ((k < i) && (paramString.charAt(k) == '/') && (paramString.startsWith(str)))
                        bool = true;
                }
            }
        }
    }

    private void initDeviceProperties(Context paramContext)
    {
        this.mDeviceProperties = paramContext.getSharedPreferences("device-properties", 0);
        File localFile = paramContext.getDatabasePath("device-properties");
        SQLiteDatabase localSQLiteDatabase;
        Cursor localCursor;
        if (localFile.exists())
        {
            localSQLiteDatabase = null;
            localCursor = null;
        }
        try
        {
            localSQLiteDatabase = paramContext.openOrCreateDatabase("device-properties", 0, null);
            if (localSQLiteDatabase != null)
            {
                String[] arrayOfString = new String[3];
                arrayOfString[0] = "_id";
                arrayOfString[1] = "code";
                arrayOfString[2] = "value";
                localCursor = localSQLiteDatabase.query("properties", arrayOfString, null, null, null, null, null);
                if (localCursor != null)
                {
                    localEditor = this.mDeviceProperties.edit();
                    while (localCursor.moveToNext())
                        localEditor.putString(localCursor.getString(1), localCursor.getString(2));
                }
            }
        }
        catch (Exception localException)
        {
            SharedPreferences.Editor localEditor;
            Log.e("MtpDatabase", "failed to migrate device properties", localException);
            if (localCursor != null)
                localCursor.close();
            if (localSQLiteDatabase != null);
            while (true)
            {
                localSQLiteDatabase.close();
                do
                {
                    localFile.delete();
                    return;
                    localEditor.commit();
                    if (localCursor != null)
                        localCursor.close();
                }
                while (localSQLiteDatabase == null);
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
            if (localSQLiteDatabase != null)
                localSQLiteDatabase.close();
        }
    }

    private boolean isStorageSubDirectory(String paramString)
    {
        boolean bool = false;
        if (this.mSubDirectories == null);
        label44: 
        while (true)
        {
            return bool;
            for (int i = 0; ; i++)
            {
                if (i >= this.mSubDirectories.length)
                    break label44;
                if (paramString.equals(this.mSubDirectories[i]))
                {
                    bool = true;
                    break;
                }
            }
        }
    }

    private final native void native_finalize();

    private final native void native_setup();

    private int renameFile(int paramInt, String paramString)
    {
        Cursor localCursor = null;
        Object localObject1 = null;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = Integer.toString(paramInt);
        int i;
        File localFile1;
        String str1;
        File localFile2;
        while (true)
        {
            try
            {
                localCursor = this.mMediaProvider.query(this.mObjectsUri, PATH_PROJECTION, "_id=?", arrayOfString, null, null);
                if ((localCursor != null) && (localCursor.moveToNext()))
                {
                    String str2 = localCursor.getString(1);
                    localObject1 = str2;
                }
                if (localCursor != null)
                    localCursor.close();
                if (localObject1 == null)
                {
                    i = 8201;
                    return i;
                }
            }
            catch (RemoteException localRemoteException1)
            {
                Log.e("MtpDatabase", "RemoteException in getObjectFilePath", localRemoteException1);
                i = 8194;
                if (localCursor == null)
                    continue;
                localCursor.close();
                continue;
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            if (isStorageSubDirectory(localObject1))
            {
                i = 8205;
            }
            else
            {
                localFile1 = new File(localObject1);
                int j = localObject1.lastIndexOf('/');
                if (j <= 1)
                {
                    i = 8194;
                }
                else
                {
                    str1 = localObject1.substring(0, j + 1) + paramString;
                    localFile2 = new File(str1);
                    if (!localFile1.renameTo(localFile2))
                    {
                        Log.w("MtpDatabase", "renaming " + localObject1 + " to " + str1 + " failed");
                        i = 8194;
                    }
                    else
                    {
                        ContentValues localContentValues = new ContentValues();
                        localContentValues.put("_data", str1);
                        int k = 0;
                        try
                        {
                            int m = this.mMediaProvider.update(this.mObjectsUri, localContentValues, "_id=?", arrayOfString);
                            k = m;
                            if (k == 0)
                            {
                                Log.e("MtpDatabase", "Unable to update path for " + localObject1 + " to " + str1);
                                localFile2.renameTo(localFile1);
                                i = 8194;
                            }
                        }
                        catch (RemoteException localRemoteException2)
                        {
                            while (true)
                                Log.e("MtpDatabase", "RemoteException in mMediaProvider.update", localRemoteException2);
                        }
                    }
                }
            }
        }
        if (localFile2.isDirectory())
            if ((!localFile1.getName().startsWith(".")) || (str1.startsWith(".")));
        while (true)
        {
            try
            {
                this.mMediaProvider.call("unhide", str1, null);
                i = 8193;
            }
            catch (RemoteException localRemoteException4)
            {
                Log.e("MtpDatabase", "failed to unhide/rescan for " + str1);
                continue;
            }
            if ((localFile1.getName().toLowerCase(Locale.US).equals(".nomedia")) && (!str1.toLowerCase(Locale.US).equals(".nomedia")))
                try
                {
                    this.mMediaProvider.call("unhide", localFile1.getParent(), null);
                }
                catch (RemoteException localRemoteException3)
                {
                    Log.e("MtpDatabase", "failed to unhide/rescan for " + str1);
                }
        }
    }

    private void sessionEnded()
    {
        if (this.mDatabaseModified)
        {
            this.mContext.sendBroadcast(new Intent("android.provider.action.MTP_SESSION_END"));
            this.mDatabaseModified = false;
        }
    }

    private void sessionStarted()
    {
        this.mDatabaseModified = false;
    }

    private int setDeviceProperty(int paramInt, long paramLong, String paramString)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = 8202;
        case 54273:
        case 54274:
        }
        while (true)
        {
            return i;
            SharedPreferences.Editor localEditor = this.mDeviceProperties.edit();
            localEditor.putString(Integer.toString(paramInt), paramString);
            if (localEditor.commit())
                i = 8193;
            else
                i = 8194;
        }
    }

    private int setObjectProperty(int paramInt1, int paramInt2, long paramLong, String paramString)
    {
        switch (paramInt2)
        {
        default:
        case 56327:
        }
        for (int i = 43018; ; i = renameFile(paramInt1, paramString))
            return i;
    }

    private int setObjectReferences(int paramInt, int[] paramArrayOfInt)
    {
        this.mDatabaseModified = true;
        Uri localUri = MediaStore.Files.getMtpReferencesUri(this.mVolumeName, paramInt);
        int i = paramArrayOfInt.length;
        ContentValues[] arrayOfContentValues = new ContentValues[i];
        for (int j = 0; j < i; j++)
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("_id", Integer.valueOf(paramArrayOfInt[j]));
            arrayOfContentValues[j] = localContentValues;
        }
        try
        {
            int m = this.mMediaProvider.bulkInsert(localUri, arrayOfContentValues);
            if (m > 0)
            {
                k = 8193;
                return k;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("MtpDatabase", "RemoteException in setObjectReferences", localRemoteException);
                int k = 8194;
            }
        }
    }

    public void addStorage(MtpStorage paramMtpStorage)
    {
        this.mStorageMap.put(paramMtpStorage.getPath(), paramMtpStorage);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            native_finalize();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public void removeStorage(MtpStorage paramMtpStorage)
    {
        this.mStorageMap.remove(paramMtpStorage.getPath());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.mtp.MtpDatabase
 * JD-Core Version:        0.6.2
 */