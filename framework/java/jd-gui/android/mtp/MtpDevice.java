package android.mtp;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;

public final class MtpDevice
{
    private static final String TAG = "MtpDevice";
    private final UsbDevice mDevice;
    private int mNativeContext;

    static
    {
        System.loadLibrary("media_jni");
    }

    public MtpDevice(UsbDevice paramUsbDevice)
    {
        this.mDevice = paramUsbDevice;
    }

    private native void native_close();

    private native boolean native_delete_object(int paramInt);

    private native MtpDeviceInfo native_get_device_info();

    private native byte[] native_get_object(int paramInt1, int paramInt2);

    private native int[] native_get_object_handles(int paramInt1, int paramInt2, int paramInt3);

    private native MtpObjectInfo native_get_object_info(int paramInt);

    private native long native_get_parent(int paramInt);

    private native long native_get_storage_id(int paramInt);

    private native int[] native_get_storage_ids();

    private native MtpStorageInfo native_get_storage_info(int paramInt);

    private native byte[] native_get_thumbnail(int paramInt);

    private native boolean native_import_file(int paramInt, String paramString);

    private native boolean native_open(String paramString, int paramInt);

    public void close()
    {
        native_close();
    }

    public boolean deleteObject(int paramInt)
    {
        return native_delete_object(paramInt);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            native_close();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getDeviceId()
    {
        return this.mDevice.getDeviceId();
    }

    public MtpDeviceInfo getDeviceInfo()
    {
        return native_get_device_info();
    }

    public String getDeviceName()
    {
        return this.mDevice.getDeviceName();
    }

    public byte[] getObject(int paramInt1, int paramInt2)
    {
        return native_get_object(paramInt1, paramInt2);
    }

    public int[] getObjectHandles(int paramInt1, int paramInt2, int paramInt3)
    {
        return native_get_object_handles(paramInt1, paramInt2, paramInt3);
    }

    public MtpObjectInfo getObjectInfo(int paramInt)
    {
        return native_get_object_info(paramInt);
    }

    public long getParent(int paramInt)
    {
        return native_get_parent(paramInt);
    }

    public long getStorageId(int paramInt)
    {
        return native_get_storage_id(paramInt);
    }

    public int[] getStorageIds()
    {
        return native_get_storage_ids();
    }

    public MtpStorageInfo getStorageInfo(int paramInt)
    {
        return native_get_storage_info(paramInt);
    }

    public byte[] getThumbnail(int paramInt)
    {
        return native_get_thumbnail(paramInt);
    }

    public boolean importFile(int paramInt, String paramString)
    {
        return native_import_file(paramInt, paramString);
    }

    public boolean open(UsbDeviceConnection paramUsbDeviceConnection)
    {
        boolean bool = native_open(this.mDevice.getDeviceName(), paramUsbDeviceConnection.getFileDescriptor());
        if (!bool)
            paramUsbDeviceConnection.close();
        return bool;
    }

    public String toString()
    {
        return this.mDevice.getDeviceName();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.mtp.MtpDevice
 * JD-Core Version:        0.6.2
 */