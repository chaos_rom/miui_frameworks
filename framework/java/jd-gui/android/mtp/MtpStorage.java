package android.mtp;

import android.content.Context;
import android.content.res.Resources;
import android.os.storage.StorageVolume;

public class MtpStorage
{
    private final String mDescription;
    private final long mMaxFileSize;
    private final String mPath;
    private final boolean mRemovable;
    private final long mReserveSpace;
    private final int mStorageId;

    public MtpStorage(StorageVolume paramStorageVolume, Context paramContext)
    {
        this.mStorageId = paramStorageVolume.getStorageId();
        this.mPath = paramStorageVolume.getPath();
        this.mDescription = paramContext.getResources().getString(paramStorageVolume.getDescriptionId());
        this.mReserveSpace = paramStorageVolume.getMtpReserveSpace();
        this.mRemovable = paramStorageVolume.isRemovable();
        this.mMaxFileSize = paramStorageVolume.getMaxFileSize();
    }

    public static int getStorageId(int paramInt)
    {
        return 1 + (paramInt + 1 << 16);
    }

    public final String getDescription()
    {
        return this.mDescription;
    }

    public long getMaxFileSize()
    {
        return this.mMaxFileSize;
    }

    public final String getPath()
    {
        return this.mPath;
    }

    public final long getReserveSpace()
    {
        return this.mReserveSpace;
    }

    public final int getStorageId()
    {
        return this.mStorageId;
    }

    public final boolean isRemovable()
    {
        return this.mRemovable;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.mtp.MtpStorage
 * JD-Core Version:        0.6.2
 */