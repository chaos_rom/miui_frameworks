package android.mtp;

public class MtpDeviceInfo
{
    private String mManufacturer;
    private String mModel;
    private String mSerialNumber;
    private String mVersion;

    public final String getManufacturer()
    {
        return this.mManufacturer;
    }

    public final String getModel()
    {
        return this.mModel;
    }

    public final String getSerialNumber()
    {
        return this.mSerialNumber;
    }

    public final String getVersion()
    {
        return this.mVersion;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.mtp.MtpDeviceInfo
 * JD-Core Version:        0.6.2
 */