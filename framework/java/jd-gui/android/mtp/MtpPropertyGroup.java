package android.mtp;

import android.content.IContentProvider;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.MediaStore.Audio.Genres;
import android.provider.MediaStore.Audio.Media;
import android.provider.MediaStore.Files;
import android.util.Log;
import java.util.ArrayList;

class MtpPropertyGroup
{
    private static final String FORMAT_WHERE = "format=?";
    private static final String ID_FORMAT_WHERE = "_id=? AND format=?";
    private static final String ID_WHERE = "_id=?";
    private static final String PARENT_FORMAT_WHERE = "parent=? AND format=?";
    private static final String PARENT_WHERE = "parent=?";
    private static final String TAG = "MtpPropertyGroup";
    private String[] mColumns;
    private final MtpDatabase mDatabase;
    private final Property[] mProperties;
    private final IContentProvider mProvider;
    private final Uri mUri;
    private final String mVolumeName;

    public MtpPropertyGroup(MtpDatabase paramMtpDatabase, IContentProvider paramIContentProvider, String paramString, int[] paramArrayOfInt)
    {
        this.mDatabase = paramMtpDatabase;
        this.mProvider = paramIContentProvider;
        this.mVolumeName = paramString;
        this.mUri = MediaStore.Files.getMtpObjectsUri(paramString);
        int i = paramArrayOfInt.length;
        ArrayList localArrayList = new ArrayList(i);
        localArrayList.add("_id");
        this.mProperties = new Property[i];
        for (int j = 0; j < i; j++)
            this.mProperties[j] = createProperty(paramArrayOfInt[j], localArrayList);
        int k = localArrayList.size();
        this.mColumns = new String[k];
        for (int m = 0; m < k; m++)
            this.mColumns[m] = ((String)localArrayList.get(m));
    }

    private Property createProperty(int paramInt, ArrayList<String> paramArrayList)
    {
        Object localObject = null;
        int i;
        switch (paramInt)
        {
        default:
            i = 0;
            Log.e("MtpPropertyGroup", "unsupported property " + paramInt);
            if (localObject != null)
                paramArrayList.add(localObject);
            break;
        case 56321:
        case 56322:
        case 56323:
        case 56324:
        case 56327:
        case 56388:
        case 56329:
        case 56398:
        case 56473:
        case 56331:
        case 56385:
        case 56457:
        case 56459:
        case 56544:
        case 56390:
        case 56474:
        case 56475:
        case 56460:
        case 56470:
        case 56392:
        }
        for (Property localProperty = new Property(paramInt, i, -1 + paramArrayList.size()); ; localProperty = new Property(paramInt, i, -1))
        {
            return localProperty;
            localObject = "storage_id";
            i = 6;
            break;
            localObject = "format";
            i = 4;
            break;
            i = 4;
            break;
            localObject = "_size";
            i = 8;
            break;
            localObject = "_data";
            i = 65535;
            break;
            localObject = "title";
            i = 65535;
            break;
            localObject = "date_modified";
            i = 65535;
            break;
            localObject = "date_added";
            i = 65535;
            break;
            localObject = "year";
            i = 65535;
            break;
            localObject = "parent";
            i = 6;
            break;
            localObject = "storage_id";
            i = 10;
            break;
            localObject = "duration";
            i = 6;
            break;
            localObject = "track";
            i = 4;
            break;
            localObject = "_display_name";
            i = 65535;
            break;
            i = 65535;
            break;
            i = 65535;
            break;
            localObject = "album_artist";
            i = 65535;
            break;
            i = 65535;
            break;
            localObject = "composer";
            i = 65535;
            break;
            localObject = "description";
            i = 65535;
            break;
        }
    }

    private native String format_date_time(long paramLong);

    private static String nameFromPath(String paramString)
    {
        int i = 0;
        int j = paramString.lastIndexOf('/');
        if (j >= 0)
            i = j + 1;
        int k = paramString.length();
        if (k - i > 255)
            k = i + 255;
        return paramString.substring(i, k);
    }

    private String queryAudio(int paramInt, String paramString)
    {
        Cursor localCursor = null;
        try
        {
            IContentProvider localIContentProvider = this.mProvider;
            Uri localUri = MediaStore.Audio.Media.getContentUri(this.mVolumeName);
            String[] arrayOfString1 = new String[2];
            arrayOfString1[0] = "_id";
            arrayOfString1[1] = paramString;
            String[] arrayOfString2 = new String[1];
            arrayOfString2[0] = Integer.toString(paramInt);
            localCursor = localIContentProvider.query(localUri, arrayOfString1, "_id=?", arrayOfString2, null, null);
            if ((localCursor != null) && (localCursor.moveToNext()))
            {
                String str2 = localCursor.getString(1);
                str1 = str2;
            }
            while (true)
            {
                return str1;
                str1 = "";
                if (localCursor == null);
            }
        }
        catch (Exception localException)
        {
            while (true)
            {
                if (localCursor != null)
                    localCursor.close();
                String str1 = null;
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    private String queryGenre(int paramInt)
    {
        Cursor localCursor = null;
        try
        {
            Uri localUri = MediaStore.Audio.Genres.getContentUriForAudioId(this.mVolumeName, paramInt);
            IContentProvider localIContentProvider = this.mProvider;
            String[] arrayOfString = new String[2];
            arrayOfString[0] = "_id";
            arrayOfString[1] = "name";
            localCursor = localIContentProvider.query(localUri, arrayOfString, null, null, null, null);
            if ((localCursor != null) && (localCursor.moveToNext()))
            {
                String str2 = localCursor.getString(1);
                str1 = str2;
            }
            while (true)
            {
                return str1;
                str1 = "";
                if (localCursor == null);
            }
        }
        catch (Exception localException)
        {
            while (true)
            {
                Log.e("MtpPropertyGroup", "queryGenre exception", localException);
                if (localCursor != null)
                    localCursor.close();
                String str1 = null;
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    // ERROR //
    private java.lang.Long queryLong(int paramInt, String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_0
        //     3: getfield 46	android/mtp/MtpPropertyGroup:mProvider	Landroid/content/IContentProvider;
        //     6: astore 7
        //     8: aload_0
        //     9: getfield 56	android/mtp/MtpPropertyGroup:mUri	Landroid/net/Uri;
        //     12: astore 8
        //     14: iconst_2
        //     15: anewarray 79	java/lang/String
        //     18: astore 9
        //     20: aload 9
        //     22: iconst_0
        //     23: ldc 63
        //     25: aastore
        //     26: aload 9
        //     28: iconst_1
        //     29: aload_2
        //     30: aastore
        //     31: iconst_1
        //     32: anewarray 79	java/lang/String
        //     35: astore 10
        //     37: aload 10
        //     39: iconst_0
        //     40: iload_1
        //     41: invokestatic 170	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     44: aastore
        //     45: aload 7
        //     47: aload 8
        //     49: aload 9
        //     51: ldc 17
        //     53: aload 10
        //     55: aconst_null
        //     56: aconst_null
        //     57: invokeinterface 176 7 0
        //     62: astore_3
        //     63: aload_3
        //     64: ifnull +74 -> 138
        //     67: aload_3
        //     68: invokeinterface 182 1 0
        //     73: ifeq +65 -> 138
        //     76: new 208	java/lang/Long
        //     79: dup
        //     80: aload_3
        //     81: iconst_1
        //     82: invokeinterface 212 2 0
        //     87: invokespecial 215	java/lang/Long:<init>	(J)V
        //     90: astore 5
        //     92: aload_3
        //     93: ifnull +9 -> 102
        //     96: aload_3
        //     97: invokeinterface 188 1 0
        //     102: aload 5
        //     104: areturn
        //     105: astore 6
        //     107: aload_3
        //     108: ifnull +9 -> 117
        //     111: aload_3
        //     112: invokeinterface 188 1 0
        //     117: aload 6
        //     119: athrow
        //     120: astore 4
        //     122: aload_3
        //     123: ifnull +9 -> 132
        //     126: aload_3
        //     127: invokeinterface 188 1 0
        //     132: aconst_null
        //     133: astore 5
        //     135: goto -33 -> 102
        //     138: aload_3
        //     139: ifnull -7 -> 132
        //     142: goto -16 -> 126
        //
        // Exception table:
        //     from	to	target	type
        //     2	92	105	finally
        //     2	92	120	java/lang/Exception
    }

    private String queryString(int paramInt, String paramString)
    {
        Cursor localCursor = null;
        try
        {
            IContentProvider localIContentProvider = this.mProvider;
            Uri localUri = this.mUri;
            String[] arrayOfString1 = new String[2];
            arrayOfString1[0] = "_id";
            arrayOfString1[1] = paramString;
            String[] arrayOfString2 = new String[1];
            arrayOfString2[0] = Integer.toString(paramInt);
            localCursor = localIContentProvider.query(localUri, arrayOfString1, "_id=?", arrayOfString2, null, null);
            if ((localCursor != null) && (localCursor.moveToNext()))
            {
                String str2 = localCursor.getString(1);
                str1 = str2;
            }
            while (true)
            {
                return str1;
                str1 = "";
                if (localCursor == null);
            }
        }
        catch (Exception localException)
        {
            while (true)
            {
                if (localCursor != null)
                    localCursor.close();
                String str1 = null;
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    MtpPropertyList getPropertyList(int paramInt1, int paramInt2, int paramInt3)
    {
        MtpPropertyList localMtpPropertyList;
        if (paramInt3 > 1)
        {
            localMtpPropertyList = new MtpPropertyList(0, 43016);
            return localMtpPropertyList;
        }
        String str1;
        String[] arrayOfString;
        label36: Cursor localCursor;
        if (paramInt2 == 0)
            if (paramInt1 == -1)
            {
                str1 = null;
                arrayOfString = null;
                localCursor = null;
                if ((paramInt3 > 0) || (paramInt1 == -1));
            }
        while (true)
        {
            int j;
            int k;
            Property localProperty;
            int n;
            int i1;
            try
            {
                if (this.mColumns.length > 1)
                {
                    localCursor = this.mProvider.query(this.mUri, this.mColumns, str1, arrayOfString, null, null);
                    if (localCursor == null)
                    {
                        localMtpPropertyList = new MtpPropertyList(0, 8201);
                        if (localCursor == null)
                            break;
                        localCursor.close();
                        break;
                        arrayOfString = new String[1];
                        arrayOfString[0] = Integer.toString(paramInt1);
                        if (paramInt3 == 1)
                        {
                            str1 = "parent=?";
                            break label36;
                        }
                        str1 = "_id=?";
                        break label36;
                        if (paramInt1 == -1)
                        {
                            str1 = "format=?";
                            arrayOfString = new String[1];
                            arrayOfString[0] = Integer.toString(paramInt2);
                            break label36;
                        }
                        arrayOfString = new String[2];
                        arrayOfString[0] = Integer.toString(paramInt1);
                        arrayOfString[1] = Integer.toString(paramInt2);
                        if (paramInt3 == 1)
                        {
                            str1 = "parent=? AND format=?";
                            break label36;
                        }
                        str1 = "_id=? AND format=?";
                        break label36;
                    }
                }
                int i;
                if (localCursor == null)
                {
                    i = 1;
                    localMtpPropertyList = new MtpPropertyList(i * this.mProperties.length, 8193);
                    j = 0;
                    if (j >= i)
                        break label937;
                    if (localCursor == null)
                        break label945;
                    localCursor.moveToNext();
                    paramInt1 = (int)localCursor.getLong(0);
                    break label945;
                    int m = this.mProperties.length;
                    if (k >= m)
                        break label931;
                    localProperty = this.mProperties[k];
                    n = localProperty.code;
                    i1 = localProperty.column;
                }
                switch (n)
                {
                default:
                    if (localProperty.type != 65535)
                        break label869;
                    String str10 = localCursor.getString(i1);
                    localMtpPropertyList.append(paramInt1, n, str10);
                    break label951;
                    i = localCursor.getCount();
                    break;
                case 56323:
                    localMtpPropertyList.append(paramInt1, n, 4, 0L);
                case 56327:
                case 56388:
                case 56329:
                case 56398:
                case 56473:
                case 56385:
                case 56459:
                case 56390:
                case 56474:
                case 56460:
                }
            }
            catch (RemoteException localRemoteException)
            {
                localMtpPropertyList = new MtpPropertyList(0, 8194);
                if (localCursor == null)
                    break;
                continue;
                String str8 = localCursor.getString(i1);
                if (str8 != null)
                {
                    String str9 = nameFromPath(str8);
                    localMtpPropertyList.append(paramInt1, n, str9);
                }
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            localMtpPropertyList.setResult(8201);
            break label951;
            String str7 = localCursor.getString(i1);
            if (str7 == null)
                str7 = queryString(paramInt1, "name");
            if (str7 == null)
            {
                str7 = queryString(paramInt1, "_data");
                if (str7 != null)
                    str7 = nameFromPath(str7);
            }
            if (str7 != null)
            {
                localMtpPropertyList.append(paramInt1, n, str7);
            }
            else
            {
                localMtpPropertyList.setResult(8201);
                break label951;
                String str6 = format_date_time(localCursor.getInt(i1));
                localMtpPropertyList.append(paramInt1, n, str6);
                break label951;
                int i2 = localCursor.getInt(i1);
                String str5 = Integer.toString(i2) + "0101T000000";
                localMtpPropertyList.append(paramInt1, n, str5);
                break label951;
                long l2 = (localCursor.getLong(i1) << 32) + paramInt1;
                localMtpPropertyList.append(paramInt1, n, 10, l2);
                break label951;
                long l1 = localCursor.getInt(i1) % 1000;
                localMtpPropertyList.append(paramInt1, n, 4, l1);
                break label951;
                String str4 = queryAudio(paramInt1, "artist");
                localMtpPropertyList.append(paramInt1, n, str4);
                break label951;
                String str3 = queryAudio(paramInt1, "album");
                localMtpPropertyList.append(paramInt1, n, str3);
                break label951;
                String str2 = queryGenre(paramInt1);
                label931: label937: label945: if (str2 != null)
                {
                    localMtpPropertyList.append(paramInt1, n, str2);
                }
                else
                {
                    localMtpPropertyList.setResult(8201);
                    break label951;
                    label869: if (localProperty.type == 0)
                    {
                        int i4 = localProperty.type;
                        localMtpPropertyList.append(paramInt1, n, i4, 0L);
                    }
                    else
                    {
                        int i3 = localProperty.type;
                        long l3 = localCursor.getLong(i1);
                        localMtpPropertyList.append(paramInt1, n, i3, l3);
                        break label951;
                        j++;
                        continue;
                        if (localCursor == null)
                            break;
                        continue;
                        k = 0;
                        continue;
                    }
                }
            }
            label951: k++;
        }
    }

    private class Property
    {
        int code;
        int column;
        int type;

        Property(int paramInt1, int paramInt2, int arg4)
        {
            this.code = paramInt1;
            this.type = paramInt2;
            int i;
            this.column = i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.mtp.MtpPropertyGroup
 * JD-Core Version:        0.6.2
 */