package android.accounts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.HashMap;

public class ChooseAccountActivity extends Activity
{
    private static final String TAG = "AccountManager";
    private AccountManagerResponse mAccountManagerResponse = null;
    private Parcelable[] mAccounts = null;
    private Bundle mResult;
    private HashMap<String, AuthenticatorDescription> mTypeToAuthDescription = new HashMap();

    private void getAuthDescriptions()
    {
        for (AuthenticatorDescription localAuthenticatorDescription : AccountManager.get(this).getAuthenticatorTypes())
            this.mTypeToAuthDescription.put(localAuthenticatorDescription.type, localAuthenticatorDescription);
    }

    private Drawable getDrawableForType(String paramString)
    {
        Object localObject = null;
        if (this.mTypeToAuthDescription.containsKey(paramString));
        try
        {
            AuthenticatorDescription localAuthenticatorDescription = (AuthenticatorDescription)this.mTypeToAuthDescription.get(paramString);
            Drawable localDrawable = createPackageContext(localAuthenticatorDescription.packageName, 0).getResources().getDrawable(localAuthenticatorDescription.iconId);
            localObject = localDrawable;
            return localObject;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
                if (Log.isLoggable("AccountManager", 5))
                    Log.w("AccountManager", "No icon name for account type " + paramString);
        }
        catch (Resources.NotFoundException localNotFoundException)
        {
            while (true)
                if (Log.isLoggable("AccountManager", 5))
                    Log.w("AccountManager", "No icon resource for account type " + paramString);
        }
    }

    public void finish()
    {
        if (this.mAccountManagerResponse != null)
        {
            if (this.mResult == null)
                break label30;
            this.mAccountManagerResponse.onResult(this.mResult);
        }
        while (true)
        {
            super.finish();
            return;
            label30: this.mAccountManagerResponse.onError(4, "canceled");
        }
    }

    public void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        this.mAccounts = getIntent().getParcelableArrayExtra("accounts");
        this.mAccountManagerResponse = ((AccountManagerResponse)getIntent().getParcelableExtra("accountManagerResponse"));
        if (this.mAccounts == null)
        {
            setResult(0);
            finish();
        }
        while (true)
        {
            return;
            getAuthDescriptions();
            AccountInfo[] arrayOfAccountInfo = new AccountInfo[this.mAccounts.length];
            for (int i = 0; i < this.mAccounts.length; i++)
                arrayOfAccountInfo[i] = new AccountInfo(((Account)this.mAccounts[i]).name, getDrawableForType(((Account)this.mAccounts[i]).type));
            setContentView(17367089);
            ListView localListView = (ListView)findViewById(16908298);
            localListView.setAdapter(new AccountArrayAdapter(this, 17367043, arrayOfAccountInfo));
            localListView.setChoiceMode(1);
            localListView.setTextFilterEnabled(true);
            localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
                {
                    ChooseAccountActivity.this.onListItemClick((ListView)paramAnonymousAdapterView, paramAnonymousView, paramAnonymousInt, paramAnonymousLong);
                }
            });
        }
    }

    protected void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
    {
        Account localAccount = (Account)this.mAccounts[paramInt];
        Log.d("AccountManager", "selected account " + localAccount);
        Bundle localBundle = new Bundle();
        localBundle.putString("authAccount", localAccount.name);
        localBundle.putString("accountType", localAccount.type);
        this.mResult = localBundle;
        finish();
    }

    private static class AccountArrayAdapter extends ArrayAdapter<ChooseAccountActivity.AccountInfo>
    {
        private ChooseAccountActivity.AccountInfo[] mInfos;
        private LayoutInflater mLayoutInflater;

        public AccountArrayAdapter(Context paramContext, int paramInt, ChooseAccountActivity.AccountInfo[] paramArrayOfAccountInfo)
        {
            super(paramInt, paramArrayOfAccountInfo);
            this.mInfos = paramArrayOfAccountInfo;
            this.mLayoutInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            ChooseAccountActivity.ViewHolder localViewHolder;
            if (paramView == null)
            {
                paramView = this.mLayoutInflater.inflate(17367090, null);
                localViewHolder = new ChooseAccountActivity.ViewHolder(null);
                localViewHolder.text = ((TextView)paramView.findViewById(16908907));
                localViewHolder.icon = ((ImageView)paramView.findViewById(16908906));
                paramView.setTag(localViewHolder);
            }
            while (true)
            {
                localViewHolder.text.setText(this.mInfos[paramInt].name);
                localViewHolder.icon.setImageDrawable(this.mInfos[paramInt].drawable);
                return paramView;
                localViewHolder = (ChooseAccountActivity.ViewHolder)paramView.getTag();
            }
        }
    }

    private static class ViewHolder
    {
        ImageView icon;
        TextView text;
    }

    private static class AccountInfo
    {
        final Drawable drawable;
        final String name;

        AccountInfo(String paramString, Drawable paramDrawable)
        {
            this.name = paramString;
            this.drawable = paramDrawable;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.ChooseAccountActivity
 * JD-Core Version:        0.6.2
 */