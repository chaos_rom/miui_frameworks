package android.accounts;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManager;
import android.app.AppGlobals;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.RegisteredServicesCache.ServiceInfo;
import android.content.pm.RegisteredServicesCacheListener;
import android.content.pm.UserInfo;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UserId;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import com.android.internal.util.IndentingPrintWriter;
import java.io.File;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import miui.content.pm.ExtraPackageManager;

public class AccountManagerService extends IAccountManager.Stub
    implements RegisteredServicesCacheListener<AuthenticatorDescription>
{
    private static final Intent ACCOUNTS_CHANGED_INTENT;
    private static final String ACCOUNTS_ID = "_id";
    private static final String ACCOUNTS_NAME = "name";
    private static final String ACCOUNTS_PASSWORD = "password";
    private static final String ACCOUNTS_TYPE = "type";
    private static final String ACCOUNTS_TYPE_COUNT = "count(type)";
    private static final String[] ACCOUNT_TYPE_COUNT_PROJECTION;
    private static final String AUTHTOKENS_ACCOUNTS_ID = "accounts_id";
    private static final String AUTHTOKENS_AUTHTOKEN = "authtoken";
    private static final String AUTHTOKENS_ID = "_id";
    private static final String AUTHTOKENS_TYPE = "type";
    private static final String[] COLUMNS_AUTHTOKENS_TYPE_AND_AUTHTOKEN;
    private static final String[] COLUMNS_EXTRAS_KEY_AND_VALUE;
    private static final String COUNT_OF_MATCHING_GRANTS = "SELECT COUNT(*) FROM grants, accounts WHERE accounts_id=_id AND uid=? AND auth_token_type=? AND name=? AND type=?";
    private static final String DATABASE_NAME = "accounts.db";
    private static final int DATABASE_VERSION = 4;
    private static final Account[] EMPTY_ACCOUNT_ARRAY;
    private static final String EXTRAS_ACCOUNTS_ID = "accounts_id";
    private static final String EXTRAS_ID = "_id";
    private static final String EXTRAS_KEY = "key";
    private static final String EXTRAS_VALUE = "value";
    private static final String GRANTS_ACCOUNTS_ID = "accounts_id";
    private static final String GRANTS_AUTH_TOKEN_TYPE = "auth_token_type";
    private static final String GRANTS_GRANTEE_UID = "uid";
    private static final int MESSAGE_TIMED_OUT = 3;
    private static final String META_KEY = "key";
    private static final String META_VALUE = "value";
    private static final String SELECTION_AUTHTOKENS_BY_ACCOUNT = "accounts_id=(select _id FROM accounts WHERE name=? AND type=?)";
    private static final String SELECTION_USERDATA_BY_ACCOUNT = "accounts_id=(select _id FROM accounts WHERE name=? AND type=?)";
    private static final String TABLE_ACCOUNTS = "accounts";
    private static final String TABLE_AUTHTOKENS = "authtokens";
    private static final String TABLE_EXTRAS = "extras";
    private static final String TABLE_GRANTS = "grants";
    private static final String TABLE_META = "meta";
    private static final String TAG = "AccountManagerService";
    private static final int TIMEOUT_DELAY_MS = 60000;
    private static AtomicReference<AccountManagerService> sThis;
    private final IAccountAuthenticatorCache mAuthenticatorCache;
    private final Context mContext;
    private final MessageHandler mMessageHandler;
    private HandlerThread mMessageThread;
    private final AtomicInteger mNotificationIds = new AtomicInteger(1);
    private final PackageManager mPackageManager;
    private final LinkedHashMap<String, Session> mSessions = new LinkedHashMap();
    private final SparseArray<UserAccounts> mUsers = new SparseArray();

    static
    {
        String[] arrayOfString1 = new String[2];
        arrayOfString1[0] = "type";
        arrayOfString1[1] = "count(type)";
        ACCOUNT_TYPE_COUNT_PROJECTION = arrayOfString1;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "type";
        arrayOfString2[1] = "authtoken";
        COLUMNS_AUTHTOKENS_TYPE_AND_AUTHTOKEN = arrayOfString2;
        String[] arrayOfString3 = new String[2];
        arrayOfString3[0] = "key";
        arrayOfString3[1] = "value";
        COLUMNS_EXTRAS_KEY_AND_VALUE = arrayOfString3;
        sThis = new AtomicReference();
        EMPTY_ACCOUNT_ARRAY = new Account[0];
        ACCOUNTS_CHANGED_INTENT = new Intent("android.accounts.LOGIN_ACCOUNTS_CHANGED");
        ACCOUNTS_CHANGED_INTENT.setFlags(134217728);
    }

    public AccountManagerService(Context paramContext)
    {
        this(paramContext, paramContext.getPackageManager(), new AccountAuthenticatorCache(paramContext));
    }

    public AccountManagerService(Context paramContext, PackageManager paramPackageManager, IAccountAuthenticatorCache paramIAccountAuthenticatorCache)
    {
        this.mContext = paramContext;
        this.mPackageManager = paramPackageManager;
        this.mMessageThread = new HandlerThread("AccountManagerService");
        this.mMessageThread.start();
        this.mMessageHandler = new MessageHandler(this.mMessageThread.getLooper());
        this.mAuthenticatorCache = paramIAccountAuthenticatorCache;
        this.mAuthenticatorCache.setListener(this, null);
        sThis.set(this);
        IntentFilter localIntentFilter1 = new IntentFilter();
        localIntentFilter1.addAction("android.intent.action.PACKAGE_REMOVED");
        localIntentFilter1.addDataScheme("package");
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                AccountManagerService.this.purgeOldGrantsAll();
            }
        }
        , localIntentFilter1);
        IntentFilter localIntentFilter2 = new IntentFilter();
        localIntentFilter2.addAction("android.intent.action.USER_REMOVED");
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                AccountManagerService.this.onUserRemoved(paramAnonymousIntent);
            }
        }
        , localIntentFilter2);
    }

    private boolean addAccountInternal(UserAccounts paramUserAccounts, Account paramAccount, String paramString, Bundle paramBundle)
    {
        boolean bool;
        if (paramAccount == null)
            bool = false;
        while (true)
        {
            return bool;
            SQLiteDatabase localSQLiteDatabase;
            synchronized (paramUserAccounts.cacheLock)
            {
                localSQLiteDatabase = paramUserAccounts.openHelper.getWritableDatabase();
                localSQLiteDatabase.beginTransaction();
            }
            try
            {
                String[] arrayOfString = new String[2];
                arrayOfString[0] = paramAccount.name;
                arrayOfString[1] = paramAccount.type;
                if (DatabaseUtils.longForQuery(localSQLiteDatabase, "select count(*) from accounts WHERE name=? AND type=?", arrayOfString) > 0L)
                {
                    Log.w("AccountManagerService", "insertAccountIntoDatabase: " + paramAccount + ", skipping since the account already exists");
                    bool = false;
                    localSQLiteDatabase.endTransaction();
                    continue;
                    localObject2 = finally;
                    throw localObject2;
                }
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("name", paramAccount.name);
                localContentValues.put("type", paramAccount.type);
                localContentValues.put("password", paramString);
                long l = localSQLiteDatabase.insert("accounts", "name", localContentValues);
                if (l < 0L)
                {
                    Log.w("AccountManagerService", "insertAccountIntoDatabase: " + paramAccount + ", skipping the DB insert failed");
                    bool = false;
                    localSQLiteDatabase.endTransaction();
                    continue;
                }
                if (paramBundle != null)
                {
                    Iterator localIterator = paramBundle.keySet().iterator();
                    while (true)
                        if (localIterator.hasNext())
                        {
                            String str = (String)localIterator.next();
                            if (insertExtraLocked(localSQLiteDatabase, l, str, paramBundle.getString(str)) < 0L)
                            {
                                Log.w("AccountManagerService", "insertAccountIntoDatabase: " + paramAccount + ", skipping since insertExtra failed for key " + str);
                                bool = false;
                                localSQLiteDatabase.endTransaction();
                                break;
                            }
                        }
                }
                localSQLiteDatabase.setTransactionSuccessful();
                insertAccountIntoCacheLocked(paramUserAccounts, paramAccount);
                localSQLiteDatabase.endTransaction();
                sendAccountsChangedBroadcast(paramUserAccounts.userId);
                bool = true;
            }
            finally
            {
                localSQLiteDatabase.endTransaction();
            }
        }
    }

    private void checkAuthenticateAccountsPermission(Account paramAccount)
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "android.permission.AUTHENTICATE_ACCOUNTS";
        checkBinderPermission(arrayOfString);
        checkCallingUidAgainstAuthenticator(paramAccount);
    }

    private void checkBinderPermission(String[] paramArrayOfString)
    {
        int i = Binder.getCallingUid();
        int j = paramArrayOfString.length;
        for (int k = 0; k < j; k++)
        {
            String str2 = paramArrayOfString[k];
            if (this.mContext.checkCallingOrSelfPermission(str2) == 0)
            {
                if (Log.isLoggable("AccountManagerService", 2))
                    Log.v("AccountManagerService", "    caller uid " + i + " has " + str2);
                return;
            }
        }
        String str1 = "caller uid " + i + " lacks any of " + TextUtils.join(",", paramArrayOfString);
        Log.w("AccountManagerService", "    " + str1);
        throw new SecurityException(str1);
    }

    private void checkCallingUidAgainstAuthenticator(Account paramAccount)
    {
        int i = Binder.getCallingUid();
        if ((paramAccount == null) || (!hasAuthenticatorUid(paramAccount.type, i)))
        {
            String str = "caller uid " + i + " is different than the authenticator's uid";
            Log.w("AccountManagerService", str);
            throw new SecurityException(str);
        }
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "caller uid " + i + " is the same as the authenticator's uid");
    }

    private void checkManageAccountsOrUseCredentialsPermissions()
    {
        String[] arrayOfString = new String[2];
        arrayOfString[0] = "android.permission.MANAGE_ACCOUNTS";
        arrayOfString[1] = "android.permission.USE_CREDENTIALS";
        checkBinderPermission(arrayOfString);
    }

    private void checkManageAccountsPermission()
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "android.permission.MANAGE_ACCOUNTS";
        checkBinderPermission(arrayOfString);
    }

    private void checkReadAccountsPermission()
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "android.permission.GET_ACCOUNTS";
        checkBinderPermission(arrayOfString);
    }

    private void createNoCredentialsPermissionNotification(Account paramAccount, Intent paramIntent)
    {
        int i = paramIntent.getIntExtra("uid", -1);
        String str1 = paramIntent.getStringExtra("authTokenType");
        paramIntent.getStringExtra("authTokenLabel");
        Notification localNotification = new Notification(17301642, null, 0L);
        Context localContext = this.mContext;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = paramAccount.name;
        String str2 = localContext.getString(17040500, arrayOfObject);
        int j = str2.indexOf('\n');
        String str3 = str2;
        String str4 = "";
        if (j > 0)
        {
            str3 = str2.substring(0, j);
            str4 = str2.substring(j + 1);
        }
        localNotification.setLatestEventInfo(this.mContext, str3, str4, PendingIntent.getActivity(this.mContext, 0, paramIntent, 268435456));
        installNotification(getCredentialPermissionNotificationId(paramAccount, str1, i).intValue(), localNotification);
    }

    private void doNotification(UserAccounts paramUserAccounts, Account paramAccount, CharSequence paramCharSequence, Intent paramIntent)
    {
        long l = clearCallingIdentity();
        try
        {
            if (Log.isLoggable("AccountManagerService", 2))
                Log.v("AccountManagerService", "doNotification: " + paramCharSequence + " intent:" + paramIntent);
            if ((paramIntent.getComponent() != null) && (GrantCredentialsPermissionActivity.class.getName().equals(paramIntent.getComponent().getClassName())))
                createNoCredentialsPermissionNotification(paramAccount, paramIntent);
            while (true)
            {
                return;
                Integer localInteger = getSigninRequiredNotificationId(paramUserAccounts, paramAccount);
                paramIntent.addCategory(String.valueOf(localInteger));
                Notification localNotification = new Notification(17301642, null, 0L);
                String str = this.mContext.getText(17039647).toString();
                Context localContext = this.mContext;
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = paramAccount.name;
                localNotification.setLatestEventInfo(localContext, String.format(str, arrayOfObject), paramCharSequence, PendingIntent.getActivity(this.mContext, 0, paramIntent, 268435456));
                installNotification(localInteger.intValue(), localNotification);
            }
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    // ERROR //
    private void dumpUser(UserAccounts paramUserAccounts, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_1
        //     1: invokestatic 334	android/accounts/AccountManagerService$UserAccounts:access$200	(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;
        //     4: astore 6
        //     6: aload 6
        //     8: monitorenter
        //     9: aload_1
        //     10: invokestatic 338	android/accounts/AccountManagerService$UserAccounts:access$300	(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;
        //     13: invokevirtual 630	android/accounts/AccountManagerService$DatabaseHelper:getReadableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
        //     16: astore 8
        //     18: iload 5
        //     20: ifeq +101 -> 121
        //     23: aload 8
        //     25: ldc 114
        //     27: getstatic 158	android/accounts/AccountManagerService:ACCOUNT_TYPE_COUNT_PROJECTION	[Ljava/lang/String;
        //     30: aconst_null
        //     31: aconst_null
        //     32: ldc 62
        //     34: aconst_null
        //     35: aconst_null
        //     36: invokevirtual 634	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //     39: astore 19
        //     41: aload 19
        //     43: invokeinterface 639 1 0
        //     48: ifeq +309 -> 357
        //     51: aload_3
        //     52: new 361	java/lang/StringBuilder
        //     55: dup
        //     56: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     59: aload 19
        //     61: iconst_0
        //     62: invokeinterface 641 2 0
        //     67: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     70: ldc_w 485
        //     73: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     76: aload 19
        //     78: iconst_1
        //     79: invokeinterface 641 2 0
        //     84: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     87: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     90: invokevirtual 646	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     93: goto -52 -> 41
        //     96: astore 20
        //     98: aload 19
        //     100: ifnull +10 -> 110
        //     103: aload 19
        //     105: invokeinterface 649 1 0
        //     110: aload 20
        //     112: athrow
        //     113: astore 7
        //     115: aload 6
        //     117: monitorexit
        //     118: aload 7
        //     120: athrow
        //     121: aload_0
        //     122: aload_1
        //     123: aconst_null
        //     124: invokevirtual 653	android/accounts/AccountManagerService:getAccountsFromCacheLocked	(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/lang/String;)[Landroid/accounts/Account;
        //     127: astore 9
        //     129: aload_3
        //     130: new 361	java/lang/StringBuilder
        //     133: dup
        //     134: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     137: ldc_w 655
        //     140: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     143: aload 9
        //     145: arraylength
        //     146: invokevirtual 474	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     149: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     152: invokevirtual 646	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     155: aload 9
        //     157: arraylength
        //     158: istore 10
        //     160: iconst_0
        //     161: istore 11
        //     163: iload 11
        //     165: iload 10
        //     167: if_icmpge +41 -> 208
        //     170: aload 9
        //     172: iload 11
        //     174: aaload
        //     175: astore 18
        //     177: aload_3
        //     178: new 361	java/lang/StringBuilder
        //     181: dup
        //     182: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     185: ldc_w 493
        //     188: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     191: aload 18
        //     193: invokevirtual 371	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     196: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     199: invokevirtual 646	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     202: iinc 11 1
        //     205: goto -42 -> 163
        //     208: aload_3
        //     209: invokevirtual 657	java/io/PrintWriter:println	()V
        //     212: aload_0
        //     213: getfield 207	android/accounts/AccountManagerService:mSessions	Ljava/util/LinkedHashMap;
        //     216: astore 12
        //     218: aload 12
        //     220: monitorenter
        //     221: invokestatic 662	android/os/SystemClock:elapsedRealtime	()J
        //     224: lstore 14
        //     226: aload_3
        //     227: new 361	java/lang/StringBuilder
        //     230: dup
        //     231: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     234: ldc_w 664
        //     237: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     240: aload_0
        //     241: getfield 207	android/accounts/AccountManagerService:mSessions	Ljava/util/LinkedHashMap;
        //     244: invokevirtual 667	java/util/LinkedHashMap:size	()I
        //     247: invokevirtual 474	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     250: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     253: invokevirtual 646	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     256: aload_0
        //     257: getfield 207	android/accounts/AccountManagerService:mSessions	Ljava/util/LinkedHashMap;
        //     260: invokevirtual 671	java/util/LinkedHashMap:values	()Ljava/util/Collection;
        //     263: invokeinterface 674 1 0
        //     268: astore 16
        //     270: aload 16
        //     272: invokeinterface 417 1 0
        //     277: ifeq +56 -> 333
        //     280: aload 16
        //     282: invokeinterface 421 1 0
        //     287: checkcast 31	android/accounts/AccountManagerService$Session
        //     290: astore 17
        //     292: aload_3
        //     293: new 361	java/lang/StringBuilder
        //     296: dup
        //     297: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     300: ldc_w 493
        //     303: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     306: aload 17
        //     308: lload 14
        //     310: invokevirtual 678	android/accounts/AccountManagerService$Session:toDebugString	(J)Ljava/lang/String;
        //     313: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     316: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     319: invokevirtual 646	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     322: goto -52 -> 270
        //     325: astore 13
        //     327: aload 12
        //     329: monitorexit
        //     330: aload 13
        //     332: athrow
        //     333: aload 12
        //     335: monitorexit
        //     336: aload_3
        //     337: invokevirtual 657	java/io/PrintWriter:println	()V
        //     340: aload_0
        //     341: getfield 242	android/accounts/AccountManagerService:mAuthenticatorCache	Landroid/accounts/IAccountAuthenticatorCache;
        //     344: aload_2
        //     345: aload_3
        //     346: aload 4
        //     348: invokeinterface 682 4 0
        //     353: aload 6
        //     355: monitorexit
        //     356: return
        //     357: aload 19
        //     359: ifnull -6 -> 353
        //     362: aload 19
        //     364: invokeinterface 649 1 0
        //     369: goto -16 -> 353
        //
        // Exception table:
        //     from	to	target	type
        //     41	93	96	finally
        //     9	41	113	finally
        //     103	118	113	finally
        //     121	221	113	finally
        //     330	333	113	finally
        //     336	369	113	finally
        //     221	330	325	finally
        //     333	336	325	finally
    }

    private long getAccountIdLocked(SQLiteDatabase paramSQLiteDatabase, Account paramAccount)
    {
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "_id";
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = paramAccount.name;
        arrayOfString2[1] = paramAccount.type;
        Cursor localCursor = paramSQLiteDatabase.query("accounts", arrayOfString1, "name=? AND type=?", arrayOfString2, null, null, null);
        try
        {
            if (localCursor.moveToNext())
            {
                long l2 = localCursor.getLong(0);
                l1 = l2;
                return l1;
            }
            long l1 = -1L;
        }
        finally
        {
            localCursor.close();
        }
    }

    private List<UserInfo> getAllUsers()
    {
        try
        {
            List localList2 = AppGlobals.getPackageManager().getUsers();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    private Integer getCredentialPermissionNotificationId(Account paramAccount, String paramString, int paramInt)
    {
        UserAccounts localUserAccounts = getUserAccounts(UserId.getUserId(paramInt));
        synchronized (localUserAccounts.credentialsPermissionNotificationIds)
        {
            Pair localPair = new Pair(new Pair(paramAccount, paramString), Integer.valueOf(paramInt));
            Integer localInteger = (Integer)localUserAccounts.credentialsPermissionNotificationIds.get(localPair);
            if (localInteger == null)
            {
                localInteger = Integer.valueOf(this.mNotificationIds.incrementAndGet());
                localUserAccounts.credentialsPermissionNotificationIds.put(localPair, localInteger);
            }
            return localInteger;
        }
    }

    private static String getDatabaseName(int paramInt)
    {
        File localFile1 = Environment.getSystemSecureDirectory();
        File localFile2 = new File(localFile1, "users/" + paramInt + "/" + "accounts.db");
        if (paramInt == 0)
        {
            File localFile3 = new File(localFile1, "accounts.db");
            if ((localFile3.exists()) && (!localFile2.exists()))
            {
                File localFile4 = new File(localFile1, "users/" + paramInt);
                if ((!localFile4.exists()) && (!localFile4.mkdirs()))
                    throw new IllegalStateException("User dir cannot be created: " + localFile4);
                if (!localFile3.renameTo(localFile2))
                    throw new IllegalStateException("User dir cannot be migrated: " + localFile2);
            }
        }
        return localFile2.getPath();
    }

    private long getExtrasIdLocked(SQLiteDatabase paramSQLiteDatabase, long paramLong, String paramString)
    {
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "_id";
        String str = "accounts_id=" + paramLong + " AND " + "key" + "=?";
        String[] arrayOfString2 = new String[1];
        arrayOfString2[0] = paramString;
        Cursor localCursor = paramSQLiteDatabase.query("extras", arrayOfString1, str, arrayOfString2, null, null, null);
        try
        {
            if (localCursor.moveToNext())
            {
                long l2 = localCursor.getLong(0);
                l1 = l2;
                return l1;
            }
            long l1 = -1L;
        }
        finally
        {
            localCursor.close();
        }
    }

    private Integer getSigninRequiredNotificationId(UserAccounts paramUserAccounts, Account paramAccount)
    {
        synchronized (paramUserAccounts.signinRequiredNotificationIds)
        {
            Integer localInteger = (Integer)paramUserAccounts.signinRequiredNotificationIds.get(paramAccount);
            if (localInteger == null)
            {
                localInteger = Integer.valueOf(this.mNotificationIds.incrementAndGet());
                paramUserAccounts.signinRequiredNotificationIds.put(paramAccount, localInteger);
            }
            return localInteger;
        }
    }

    public static AccountManagerService getSingleton()
    {
        return (AccountManagerService)sThis.get();
    }

    private UserAccounts getUserAccountsForCaller()
    {
        return getUserAccounts(UserId.getCallingUserId());
    }

    private void grantAppPermission(Account paramAccount, String paramString, int paramInt)
    {
        if ((paramAccount == null) || (paramString == null))
            Log.e("AccountManagerService", "grantAppPermission: called with invalid arguments", new Exception());
        while (true)
        {
            return;
            UserAccounts localUserAccounts = getUserAccounts(UserId.getUserId(paramInt));
            SQLiteDatabase localSQLiteDatabase;
            synchronized (localUserAccounts.cacheLock)
            {
                localSQLiteDatabase = localUserAccounts.openHelper.getWritableDatabase();
                localSQLiteDatabase.beginTransaction();
            }
            try
            {
                long l = getAccountIdLocked(localSQLiteDatabase, paramAccount);
                if (l >= 0L)
                {
                    ContentValues localContentValues = new ContentValues();
                    localContentValues.put("accounts_id", Long.valueOf(l));
                    localContentValues.put("auth_token_type", paramString);
                    localContentValues.put("uid", Integer.valueOf(paramInt));
                    localSQLiteDatabase.insert("grants", "accounts_id", localContentValues);
                    localSQLiteDatabase.setTransactionSuccessful();
                }
                localSQLiteDatabase.endTransaction();
                cancelNotification(getCredentialPermissionNotificationId(paramAccount, paramString, paramInt).intValue());
                continue;
                localObject2 = finally;
                throw localObject2;
            }
            finally
            {
                localSQLiteDatabase.endTransaction();
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private boolean hasAuthenticatorUid(String paramString, int paramInt)
    {
        Iterator localIterator = this.mAuthenticatorCache.getAllServices().iterator();
        boolean bool;
        while (true)
            if (localIterator.hasNext())
            {
                RegisteredServicesCache.ServiceInfo localServiceInfo = (RegisteredServicesCache.ServiceInfo)localIterator.next();
                if (((AuthenticatorDescription)localServiceInfo.type).type.equals(paramString))
                    if ((localServiceInfo.uid == paramInt) || (Injector.checkSignatures(this.mPackageManager, localServiceInfo.uid, paramInt, paramString) == 0))
                        bool = true;
            }
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = false;
        }
    }

    private boolean hasExplicitlyGrantedPermission(Account paramAccount, String paramString, int paramInt)
    {
        boolean bool = false;
        if (paramInt == 1000)
            bool = true;
        while (true)
        {
            return bool;
            UserAccounts localUserAccounts = getUserAccountsForCaller();
            synchronized (localUserAccounts.cacheLock)
            {
                SQLiteDatabase localSQLiteDatabase = localUserAccounts.openHelper.getReadableDatabase();
                String[] arrayOfString = new String[4];
                arrayOfString[0] = String.valueOf(paramInt);
                arrayOfString[1] = paramString;
                arrayOfString[2] = paramAccount.name;
                arrayOfString[3] = paramAccount.type;
                if (DatabaseUtils.longForQuery(localSQLiteDatabase, "SELECT COUNT(*) FROM grants, accounts WHERE accounts_id=_id AND uid=? AND auth_token_type=? AND name=? AND type=?", arrayOfString) != 0L)
                    bool = true;
                if ((!bool) && (ActivityManager.isRunningInTestHarness()))
                {
                    Log.d("AccountManagerService", "no credentials permission for usage of " + paramAccount + ", " + paramString + " by uid " + paramInt + " but ignoring since device is in test harness.");
                    bool = true;
                    continue;
                }
            }
        }
    }

    private boolean inSystemImage(int paramInt)
    {
        boolean bool = false;
        String[] arrayOfString = this.mPackageManager.getPackagesForUid(paramInt);
        int i = arrayOfString.length;
        int j = 0;
        while (true)
        {
            String str;
            if (j < i)
                str = arrayOfString[j];
            try
            {
                PackageInfo localPackageInfo = this.mPackageManager.getPackageInfo(str, 0);
                if (localPackageInfo != null)
                {
                    int k = localPackageInfo.applicationInfo.flags;
                    if ((k & 0x1) != 0)
                    {
                        bool = true;
                        label67: return bool;
                    }
                }
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                break label67;
                j++;
            }
        }
    }

    private UserAccounts initUser(int paramInt)
    {
        synchronized (this.mUsers)
        {
            UserAccounts localUserAccounts = (UserAccounts)this.mUsers.get(paramInt);
            if (localUserAccounts == null)
            {
                localUserAccounts = new UserAccounts(this.mContext, paramInt);
                this.mUsers.append(paramInt, localUserAccounts);
                purgeOldGrants(localUserAccounts);
                validateAccountsAndPopulateCache(localUserAccounts);
            }
            return localUserAccounts;
        }
    }

    private void insertAccountIntoCacheLocked(UserAccounts paramUserAccounts, Account paramAccount)
    {
        Account[] arrayOfAccount1 = (Account[])paramUserAccounts.accountCache.get(paramAccount.type);
        if (arrayOfAccount1 != null);
        for (int i = arrayOfAccount1.length; ; i = 0)
        {
            Account[] arrayOfAccount2 = new Account[i + 1];
            if (arrayOfAccount1 != null)
                System.arraycopy(arrayOfAccount1, 0, arrayOfAccount2, 0, i);
            arrayOfAccount2[i] = paramAccount;
            paramUserAccounts.accountCache.put(paramAccount.type, arrayOfAccount2);
            return;
        }
    }

    private long insertExtraLocked(SQLiteDatabase paramSQLiteDatabase, long paramLong, String paramString1, String paramString2)
    {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("key", paramString1);
        localContentValues.put("accounts_id", Long.valueOf(paramLong));
        localContentValues.put("value", paramString2);
        return paramSQLiteDatabase.insert("extras", "key", localContentValues);
    }

    private void invalidateAuthTokenLocked(UserAccounts paramUserAccounts, SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2)
    {
        if ((paramString2 == null) || (paramString1 == null));
        while (true)
        {
            return;
            String[] arrayOfString = new String[2];
            arrayOfString[0] = paramString2;
            arrayOfString[1] = paramString1;
            Cursor localCursor = paramSQLiteDatabase.rawQuery("SELECT authtokens._id, accounts.name, authtokens.type FROM accounts JOIN authtokens ON accounts._id = accounts_id WHERE authtoken = ? AND accounts.type = ?", arrayOfString);
            try
            {
                if (localCursor.moveToNext())
                {
                    long l = localCursor.getLong(0);
                    String str1 = localCursor.getString(1);
                    String str2 = localCursor.getString(2);
                    paramSQLiteDatabase.delete("authtokens", "_id=" + l, null);
                    writeAuthTokenIntoCacheLocked(paramUserAccounts, paramSQLiteDatabase, new Account(str1, paramString1), str2, null);
                }
            }
            finally
            {
                localCursor.close();
            }
        }
    }

    private Intent newGrantCredentialsPermissionIntent(Account paramAccount, int paramInt, AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString1, String paramString2)
    {
        Intent localIntent = new Intent(this.mContext, GrantCredentialsPermissionActivity.class);
        localIntent.setFlags(268435456);
        localIntent.addCategory(String.valueOf(getCredentialPermissionNotificationId(paramAccount, paramString1, paramInt)));
        localIntent.putExtra("account", paramAccount);
        localIntent.putExtra("authTokenType", paramString1);
        localIntent.putExtra("response", paramAccountAuthenticatorResponse);
        localIntent.putExtra("uid", paramInt);
        return localIntent;
    }

    private void onResult(IAccountManagerResponse paramIAccountManagerResponse, Bundle paramBundle)
    {
        if (paramBundle == null)
            Log.e("AccountManagerService", "the result is unexpectedly null", new Exception());
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", getClass().getSimpleName() + " calling onResult() on response " + paramIAccountManagerResponse);
        try
        {
            paramIAccountManagerResponse.onResult(paramBundle);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                if (Log.isLoggable("AccountManagerService", 2))
                    Log.v("AccountManagerService", "failure while notifying response", localRemoteException);
        }
    }

    private void onUserRemoved(Intent paramIntent)
    {
        int i = paramIntent.getIntExtra("android.intent.extra.user_id", -1);
        if (i < 1);
        while (true)
        {
            return;
            UserAccounts localUserAccounts;
            synchronized (this.mUsers)
            {
                localUserAccounts = (UserAccounts)this.mUsers.get(i);
                this.mUsers.remove(i);
                if (localUserAccounts == null)
                    new File(getDatabaseName(i)).delete();
            }
            synchronized (localUserAccounts.cacheLock)
            {
                localUserAccounts.openHelper.close();
                new File(getDatabaseName(i)).delete();
            }
        }
    }

    private boolean permissionIsGranted(Account paramAccount, String paramString, int paramInt)
    {
        boolean bool1 = false;
        boolean bool2 = inSystemImage(paramInt);
        boolean bool3;
        if ((paramAccount != null) && (hasAuthenticatorUid(paramAccount.type, paramInt)))
        {
            bool3 = true;
            if ((paramAccount == null) || (!hasExplicitlyGrantedPermission(paramAccount, paramString, paramInt)))
                break label140;
        }
        label140: for (boolean bool4 = true; ; bool4 = false)
        {
            if (Log.isLoggable("AccountManagerService", 2))
                Log.v("AccountManagerService", "checkGrantsOrCallingUidAgainstAuthenticator: caller uid " + paramInt + ", " + paramAccount + ": is authenticator? " + bool3 + ", has explicit permission? " + bool4);
            if ((bool3) || (bool4) || (bool2))
                bool1 = true;
            return bool1;
            bool3 = false;
            break;
        }
    }

    private void purgeOldGrants(UserAccounts paramUserAccounts)
    {
        Cursor localCursor;
        while (true)
        {
            SQLiteDatabase localSQLiteDatabase;
            int i;
            String[] arrayOfString2;
            synchronized (paramUserAccounts.cacheLock)
            {
                localSQLiteDatabase = paramUserAccounts.openHelper.getWritableDatabase();
                String[] arrayOfString1 = new String[1];
                arrayOfString1[0] = "uid";
                localCursor = localSQLiteDatabase.query("grants", arrayOfString1, null, null, "uid", null, null);
            }
            int j = 0;
        }
        localCursor.close();
    }

    private void purgeOldGrantsAll()
    {
        SparseArray localSparseArray = this.mUsers;
        int i = 0;
        try
        {
            while (i < this.mUsers.size())
            {
                purgeOldGrants((UserAccounts)this.mUsers.valueAt(i));
                i++;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private String readPasswordInternal(UserAccounts paramUserAccounts, Account paramAccount)
    {
        Object localObject4;
        if (paramAccount == null)
            localObject4 = null;
        while (true)
        {
            return localObject4;
            Cursor localCursor;
            synchronized (paramUserAccounts.cacheLock)
            {
                SQLiteDatabase localSQLiteDatabase = paramUserAccounts.openHelper.getReadableDatabase();
                String[] arrayOfString1 = new String[1];
                arrayOfString1[0] = "password";
                String[] arrayOfString2 = new String[2];
                arrayOfString2[0] = paramAccount.name;
                arrayOfString2[1] = paramAccount.type;
                localCursor = localSQLiteDatabase.query("accounts", arrayOfString1, "name=? AND type=?", arrayOfString2, null, null, null);
            }
            try
            {
                if (localCursor.moveToNext())
                {
                    String str = localCursor.getString(0);
                    localObject4 = str;
                    localCursor.close();
                    continue;
                    localObject2 = finally;
                    throw localObject2;
                }
                localCursor.close();
                localObject4 = null;
            }
            finally
            {
                localCursor.close();
            }
        }
    }

    private void removeAccountFromCacheLocked(UserAccounts paramUserAccounts, Account paramAccount)
    {
        Account[] arrayOfAccount1 = (Account[])paramUserAccounts.accountCache.get(paramAccount.type);
        ArrayList localArrayList;
        if (arrayOfAccount1 != null)
        {
            localArrayList = new ArrayList();
            int i = arrayOfAccount1.length;
            for (int j = 0; j < i; j++)
            {
                Account localAccount = arrayOfAccount1[j];
                if (!localAccount.equals(paramAccount))
                    localArrayList.add(localAccount);
            }
            if (!localArrayList.isEmpty())
                break label110;
            paramUserAccounts.accountCache.remove(paramAccount.type);
        }
        while (true)
        {
            paramUserAccounts.userDataCache.remove(paramAccount);
            paramUserAccounts.authTokenCache.remove(paramAccount);
            return;
            label110: Account[] arrayOfAccount2 = (Account[])localArrayList.toArray(new Account[localArrayList.size()]);
            paramUserAccounts.accountCache.put(paramAccount.type, arrayOfAccount2);
        }
    }

    private void removeAccountInternal(UserAccounts paramUserAccounts, Account paramAccount)
    {
        synchronized (paramUserAccounts.cacheLock)
        {
            SQLiteDatabase localSQLiteDatabase = paramUserAccounts.openHelper.getWritableDatabase();
            String[] arrayOfString = new String[2];
            arrayOfString[0] = paramAccount.name;
            arrayOfString[1] = paramAccount.type;
            localSQLiteDatabase.delete("accounts", "name=? AND type=?", arrayOfString);
            removeAccountFromCacheLocked(paramUserAccounts, paramAccount);
            sendAccountsChangedBroadcast(paramUserAccounts.userId);
            return;
        }
    }

    private void revokeAppPermission(Account paramAccount, String paramString, int paramInt)
    {
        if ((paramAccount == null) || (paramString == null))
            Log.e("AccountManagerService", "revokeAppPermission: called with invalid arguments", new Exception());
        while (true)
        {
            return;
            UserAccounts localUserAccounts = getUserAccounts(UserId.getUserId(paramInt));
            SQLiteDatabase localSQLiteDatabase;
            synchronized (localUserAccounts.cacheLock)
            {
                localSQLiteDatabase = localUserAccounts.openHelper.getWritableDatabase();
                localSQLiteDatabase.beginTransaction();
            }
            try
            {
                long l = getAccountIdLocked(localSQLiteDatabase, paramAccount);
                if (l >= 0L)
                {
                    String[] arrayOfString = new String[3];
                    arrayOfString[0] = String.valueOf(l);
                    arrayOfString[1] = paramString;
                    arrayOfString[2] = String.valueOf(paramInt);
                    localSQLiteDatabase.delete("grants", "accounts_id=? AND auth_token_type=? AND uid=?", arrayOfString);
                    localSQLiteDatabase.setTransactionSuccessful();
                }
                localSQLiteDatabase.endTransaction();
                cancelNotification(getCredentialPermissionNotificationId(paramAccount, paramString, paramInt).intValue());
                continue;
                localObject2 = finally;
                throw localObject2;
            }
            finally
            {
                localSQLiteDatabase.endTransaction();
            }
        }
    }

    private boolean saveAuthTokenToDatabase(UserAccounts paramUserAccounts, Account paramAccount, String paramString1, String paramString2)
    {
        boolean bool;
        if ((paramAccount == null) || (paramString1 == null))
            bool = false;
        while (true)
        {
            return bool;
            cancelNotification(getSigninRequiredNotificationId(paramUserAccounts, paramAccount).intValue());
            SQLiteDatabase localSQLiteDatabase;
            synchronized (paramUserAccounts.cacheLock)
            {
                localSQLiteDatabase = paramUserAccounts.openHelper.getWritableDatabase();
                localSQLiteDatabase.beginTransaction();
            }
            try
            {
                long l = getAccountIdLocked(localSQLiteDatabase, paramAccount);
                if (l < 0L)
                {
                    bool = false;
                    localSQLiteDatabase.endTransaction();
                    continue;
                    localObject2 = finally;
                    throw localObject2;
                }
                String str = "accounts_id=" + l + " AND " + "type" + "=?";
                String[] arrayOfString = new String[1];
                arrayOfString[0] = paramString1;
                localSQLiteDatabase.delete("authtokens", str, arrayOfString);
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("accounts_id", Long.valueOf(l));
                localContentValues.put("type", paramString1);
                localContentValues.put("authtoken", paramString2);
                if (localSQLiteDatabase.insert("authtokens", "authtoken", localContentValues) >= 0L)
                {
                    localSQLiteDatabase.setTransactionSuccessful();
                    writeAuthTokenIntoCacheLocked(paramUserAccounts, localSQLiteDatabase, paramAccount, paramString1, paramString2);
                    bool = true;
                    localSQLiteDatabase.endTransaction();
                    continue;
                }
                bool = false;
                localSQLiteDatabase.endTransaction();
            }
            finally
            {
                localSQLiteDatabase.endTransaction();
            }
        }
    }

    private static boolean scanArgs(String[] paramArrayOfString, String paramString)
    {
        int j;
        if (paramArrayOfString != null)
        {
            int i = paramArrayOfString.length;
            j = 0;
            if (j < i)
                if (!paramString.equals(paramArrayOfString[j]));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    private void sendAccountsChangedBroadcast(int paramInt)
    {
        Log.i("AccountManagerService", "the accounts changed, sending broadcast of " + ACCOUNTS_CHANGED_INTENT.getAction());
        this.mContext.sendBroadcast(ACCOUNTS_CHANGED_INTENT, paramInt);
    }

    private void setPasswordInternal(UserAccounts paramUserAccounts, Account paramAccount, String paramString)
    {
        if (paramAccount == null);
        while (true)
        {
            return;
            SQLiteDatabase localSQLiteDatabase;
            synchronized (paramUserAccounts.cacheLock)
            {
                localSQLiteDatabase = paramUserAccounts.openHelper.getWritableDatabase();
                localSQLiteDatabase.beginTransaction();
            }
            try
            {
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("password", paramString);
                long l = getAccountIdLocked(localSQLiteDatabase, paramAccount);
                if (l >= 0L)
                {
                    String[] arrayOfString = new String[1];
                    arrayOfString[0] = String.valueOf(l);
                    localSQLiteDatabase.update("accounts", localContentValues, "_id=?", arrayOfString);
                    localSQLiteDatabase.delete("authtokens", "accounts_id=?", arrayOfString);
                    paramUserAccounts.authTokenCache.remove(paramAccount);
                    localSQLiteDatabase.setTransactionSuccessful();
                }
                localSQLiteDatabase.endTransaction();
                sendAccountsChangedBroadcast(paramUserAccounts.userId);
                continue;
                localObject2 = finally;
                throw localObject2;
            }
            finally
            {
                localSQLiteDatabase.endTransaction();
            }
        }
    }

    private void setUserdataInternal(UserAccounts paramUserAccounts, Account paramAccount, String paramString1, String paramString2)
    {
        if ((paramAccount == null) || (paramString1 == null));
        while (true)
        {
            return;
            SQLiteDatabase localSQLiteDatabase;
            synchronized (paramUserAccounts.cacheLock)
            {
                localSQLiteDatabase = paramUserAccounts.openHelper.getWritableDatabase();
                localSQLiteDatabase.beginTransaction();
            }
            try
            {
                long l1 = getAccountIdLocked(localSQLiteDatabase, paramAccount);
                if (l1 < 0L)
                {
                    localSQLiteDatabase.endTransaction();
                    continue;
                    localObject2 = finally;
                    throw localObject2;
                }
                long l2 = getExtrasIdLocked(localSQLiteDatabase, l1, paramString1);
                if (l2 < 0L)
                {
                    long l3 = insertExtraLocked(localSQLiteDatabase, l1, paramString1, paramString2);
                    if (l3 >= 0L)
                        break label184;
                    localSQLiteDatabase.endTransaction();
                    continue;
                }
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("value", paramString2);
                int i = localSQLiteDatabase.update("extras", localContentValues, "_id=" + l2, null);
                if (1 != i)
                {
                    localSQLiteDatabase.endTransaction();
                    continue;
                }
                label184: writeUserDataIntoCacheLocked(paramUserAccounts, localSQLiteDatabase, paramAccount, paramString1, paramString2);
                localSQLiteDatabase.setTransactionSuccessful();
                localSQLiteDatabase.endTransaction();
            }
            finally
            {
                localSQLiteDatabase.endTransaction();
            }
        }
    }

    private static final String stringArrayToString(String[] paramArrayOfString)
    {
        if (paramArrayOfString != null);
        for (String str = "[" + TextUtils.join(",", paramArrayOfString) + "]"; ; str = null)
            return str;
    }

    private void validateAccountsAndPopulateCache(UserAccounts paramUserAccounts)
    {
        int i;
        Cursor localCursor;
        LinkedHashMap localLinkedHashMap;
        while (true)
        {
            String str2;
            String str3;
            synchronized (paramUserAccounts.cacheLock)
            {
                SQLiteDatabase localSQLiteDatabase = paramUserAccounts.openHelper.getWritableDatabase();
                i = 0;
                String[] arrayOfString = new String[3];
                arrayOfString[0] = "_id";
                arrayOfString[1] = "type";
                arrayOfString[2] = "name";
                localCursor = localSQLiteDatabase.query("accounts", arrayOfString, null, null, null, null, null);
                try
                {
                    paramUserAccounts.accountCache.clear();
                    localLinkedHashMap = new LinkedHashMap();
                    if (!localCursor.moveToNext())
                        break;
                    long l = localCursor.getLong(0);
                    str2 = localCursor.getString(1);
                    str3 = localCursor.getString(2);
                    if (this.mAuthenticatorCache.getServiceInfo(AuthenticatorDescription.newKey(str2)) == null)
                    {
                        Log.d("AccountManagerService", "deleting account " + str3 + " because type " + str2 + " no longer has a registered authenticator");
                        localSQLiteDatabase.delete("accounts", "_id=" + l, null);
                        i = 1;
                        Account localAccount = new Account(str3, str2);
                        paramUserAccounts.userDataCache.remove(localAccount);
                        paramUserAccounts.authTokenCache.remove(localAccount);
                        continue;
                    }
                }
                finally
                {
                    localCursor.close();
                    if (i != 0)
                        sendAccountsChangedBroadcast(paramUserAccounts.userId);
                }
            }
            ArrayList localArrayList2 = (ArrayList)localLinkedHashMap.get(str2);
            if (localArrayList2 == null)
            {
                localArrayList2 = new ArrayList();
                localLinkedHashMap.put(str2, localArrayList2);
            }
            localArrayList2.add(str3);
        }
        Iterator localIterator1 = localLinkedHashMap.entrySet().iterator();
        while (localIterator1.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator1.next();
            String str1 = (String)localEntry.getKey();
            ArrayList localArrayList1 = (ArrayList)localEntry.getValue();
            Account[] arrayOfAccount = new Account[localArrayList1.size()];
            int j = 0;
            Iterator localIterator2 = localArrayList1.iterator();
            while (localIterator2.hasNext())
            {
                arrayOfAccount[j] = new Account((String)localIterator2.next(), str1);
                j++;
            }
            paramUserAccounts.accountCache.put(str1, arrayOfAccount);
        }
        localCursor.close();
        if (i != 0)
            sendAccountsChangedBroadcast(paramUserAccounts.userId);
    }

    public boolean addAccount(Account paramAccount, String paramString, Bundle paramBundle)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "addAccount: " + paramAccount + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        checkAuthenticateAccountsPermission(paramAccount);
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            boolean bool = addAccountInternal(localUserAccounts, paramAccount, paramString, paramBundle);
            return bool;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void addAcount(IAccountManagerResponse paramIAccountManagerResponse, final String paramString1, final String paramString2, final String[] paramArrayOfString, boolean paramBoolean, Bundle paramBundle)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "addAccount: accountType " + paramString1 + ", response " + paramIAccountManagerResponse + ", authTokenType " + paramString2 + ", requiredFeatures " + stringArrayToString(paramArrayOfString) + ", expectActivityLaunch " + paramBoolean + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramIAccountManagerResponse == null)
            throw new IllegalArgumentException("response is null");
        if (paramString1 == null)
            throw new IllegalArgumentException("accountType is null");
        checkManageAccountsPermission();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        int i = Binder.getCallingPid();
        int j = Binder.getCallingUid();
        final Bundle localBundle;
        if (paramBundle == null)
            localBundle = new Bundle();
        while (true)
        {
            localBundle.putInt("callerUid", j);
            localBundle.putInt("callerPid", i);
            long l = clearCallingIdentity();
            try
            {
                new Session(localUserAccounts, paramIAccountManagerResponse, paramString1, paramBoolean, true, paramString2)
                {
                    public void run()
                        throws RemoteException
                    {
                        this.mAuthenticator.addAccount(this, this.mAccountType, paramString2, paramArrayOfString, localBundle);
                    }

                    protected String toDebugString(long paramAnonymousLong)
                    {
                        StringBuilder localStringBuilder = new StringBuilder().append(super.toDebugString(paramAnonymousLong)).append(", addAccount").append(", accountType ").append(paramString1).append(", requiredFeatures ");
                        if (paramArrayOfString != null);
                        for (String str = TextUtils.join(",", paramArrayOfString); ; str = null)
                            return str;
                    }
                }
                .bind();
                return;
                localBundle = paramBundle;
            }
            finally
            {
                restoreCallingIdentity(l);
            }
        }
    }

    protected void cancelNotification(int paramInt)
    {
        long l = clearCallingIdentity();
        try
        {
            ((NotificationManager)this.mContext.getSystemService("notification")).cancel(paramInt);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void clearPassword(Account paramAccount)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "clearPassword: " + paramAccount + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        checkManageAccountsPermission();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            setPasswordInternal(localUserAccounts, paramAccount, null);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void confirmCredentials(IAccountManagerResponse paramIAccountManagerResponse, final Account paramAccount, final Bundle paramBundle, boolean paramBoolean)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "confirmCredentials: " + paramAccount + ", response " + paramIAccountManagerResponse + ", expectActivityLaunch " + paramBoolean + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramIAccountManagerResponse == null)
            throw new IllegalArgumentException("response is null");
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        checkManageAccountsPermission();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            new Session(localUserAccounts, paramIAccountManagerResponse, paramAccount.type, paramBoolean, true, paramAccount)
            {
                public void run()
                    throws RemoteException
                {
                    this.mAuthenticator.confirmCredentials(this, paramAccount, paramBundle);
                }

                protected String toDebugString(long paramAnonymousLong)
                {
                    return super.toDebugString(paramAnonymousLong) + ", confirmCredentials" + ", " + paramAccount;
                }
            }
            .bind();
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump AccountsManager from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " without permission " + "android.permission.DUMP");
        while (true)
        {
            return;
            if ((scanArgs(paramArrayOfString, "--checkin")) || (scanArgs(paramArrayOfString, "-c")));
            for (boolean bool = true; ; bool = false)
            {
                IndentingPrintWriter localIndentingPrintWriter = new IndentingPrintWriter(paramPrintWriter, "    ");
                int i = this.mUsers.size();
                for (int j = 0; j < i; j++)
                {
                    localIndentingPrintWriter.println("User " + this.mUsers.keyAt(j) + ":");
                    ((IndentingPrintWriter)localIndentingPrintWriter).increaseIndent();
                    dumpUser((UserAccounts)this.mUsers.valueAt(j), paramFileDescriptor, localIndentingPrintWriter, paramArrayOfString, bool);
                    ((IndentingPrintWriter)localIndentingPrintWriter).decreaseIndent();
                    if (j < i - 1)
                        localIndentingPrintWriter.println();
                }
            }
        }
    }

    public void editProperties(IAccountManagerResponse paramIAccountManagerResponse, final String paramString, boolean paramBoolean)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "editProperties: accountType " + paramString + ", response " + paramIAccountManagerResponse + ", expectActivityLaunch " + paramBoolean + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramIAccountManagerResponse == null)
            throw new IllegalArgumentException("response is null");
        if (paramString == null)
            throw new IllegalArgumentException("accountType is null");
        checkManageAccountsPermission();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            new Session(localUserAccounts, paramIAccountManagerResponse, paramString, paramBoolean, true, paramString)
            {
                public void run()
                    throws RemoteException
                {
                    this.mAuthenticator.editProperties(this, this.mAccountType);
                }

                protected String toDebugString(long paramAnonymousLong)
                {
                    return super.toDebugString(paramAnonymousLong) + ", editProperties" + ", accountType " + paramString;
                }
            }
            .bind();
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    // ERROR //
    String getAccountLabel(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 242	android/accounts/AccountManagerService:mAuthenticatorCache	Landroid/accounts/IAccountAuthenticatorCache;
        //     4: aload_1
        //     5: invokestatic 1094	android/accounts/AuthenticatorDescription:newKey	(Ljava/lang/String;)Landroid/accounts/AuthenticatorDescription;
        //     8: invokeinterface 1098 2 0
        //     13: astore_2
        //     14: aload_2
        //     15: ifnonnull +31 -> 46
        //     18: new 1131	java/lang/IllegalArgumentException
        //     21: dup
        //     22: new 361	java/lang/StringBuilder
        //     25: dup
        //     26: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     29: ldc_w 1246
        //     32: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     35: aload_1
        //     36: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     42: invokespecial 1134	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     45: athrow
        //     46: aload_0
        //     47: getfield 221	android/accounts/AccountManagerService:mContext	Landroid/content/Context;
        //     50: aload_2
        //     51: getfield 835	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     54: checkcast 837	android/accounts/AuthenticatorDescription
        //     57: getfield 1249	android/accounts/AuthenticatorDescription:packageName	Ljava/lang/String;
        //     60: iconst_0
        //     61: invokevirtual 1253	android/content/Context:createPackageContext	(Ljava/lang/String;I)Landroid/content/Context;
        //     64: astore 4
        //     66: aload 4
        //     68: aload_2
        //     69: getfield 835	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     72: checkcast 837	android/accounts/AuthenticatorDescription
        //     75: getfield 1256	android/accounts/AuthenticatorDescription:labelId	I
        //     78: invokevirtual 1257	android/content/Context:getString	(I)Ljava/lang/String;
        //     81: astore 6
        //     83: aload 6
        //     85: areturn
        //     86: astore_3
        //     87: new 1131	java/lang/IllegalArgumentException
        //     90: dup
        //     91: new 361	java/lang/StringBuilder
        //     94: dup
        //     95: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     98: ldc_w 1246
        //     101: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     104: aload_1
        //     105: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     108: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     111: invokespecial 1134	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     114: athrow
        //     115: astore 5
        //     117: new 1131	java/lang/IllegalArgumentException
        //     120: dup
        //     121: new 361	java/lang/StringBuilder
        //     124: dup
        //     125: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     128: ldc_w 1246
        //     131: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     134: aload_1
        //     135: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     141: invokespecial 1134	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     144: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     46	66	86	android/content/pm/PackageManager$NameNotFoundException
        //     66	83	115	android/content/res/Resources$NotFoundException
    }

    public Account[] getAccounts(int paramInt)
    {
        checkReadAccountsPermission();
        UserAccounts localUserAccounts = getUserAccounts(paramInt);
        long l = clearCallingIdentity();
        try
        {
            synchronized (localUserAccounts.cacheLock)
            {
                Account[] arrayOfAccount = getAccountsFromCacheLocked(localUserAccounts, null);
                return arrayOfAccount;
            }
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public Account[] getAccounts(String paramString)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "getAccounts: accountType " + paramString + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        checkReadAccountsPermission();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            synchronized (localUserAccounts.cacheLock)
            {
                Account[] arrayOfAccount = getAccountsFromCacheLocked(localUserAccounts, paramString);
                return arrayOfAccount;
            }
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void getAccountsByFeatures(IAccountManagerResponse paramIAccountManagerResponse, String paramString, String[] paramArrayOfString)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "getAccounts: accountType " + paramString + ", response " + paramIAccountManagerResponse + ", features " + stringArrayToString(paramArrayOfString) + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramIAccountManagerResponse == null)
            throw new IllegalArgumentException("response is null");
        if (paramString == null)
            throw new IllegalArgumentException("accountType is null");
        checkReadAccountsPermission();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        if (paramArrayOfString != null);
        while (true)
        {
            try
            {
                if (paramArrayOfString.length == 0)
                    synchronized (localUserAccounts.cacheLock)
                    {
                        Account[] arrayOfAccount = getAccountsFromCacheLocked(localUserAccounts, paramString);
                        Bundle localBundle = new Bundle();
                        localBundle.putParcelableArray("accounts", arrayOfAccount);
                        onResult(paramIAccountManagerResponse, localBundle);
                        return;
                    }
            }
            finally
            {
                restoreCallingIdentity(l);
            }
            new GetAccountsByTypeAndFeatureSession(localUserAccounts, paramIAccountManagerResponse, paramString, paramArrayOfString).bind();
        }
    }

    protected Account[] getAccountsFromCacheLocked(UserAccounts paramUserAccounts, String paramString)
    {
        Account[] arrayOfAccount3;
        Object localObject;
        if (paramString != null)
        {
            arrayOfAccount3 = (Account[])paramUserAccounts.accountCache.get(paramString);
            if (arrayOfAccount3 == null)
                localObject = EMPTY_ACCOUNT_ARRAY;
        }
        while (true)
        {
            return localObject;
            localObject = (Account[])Arrays.copyOf(arrayOfAccount3, arrayOfAccount3.length);
            continue;
            int i = 0;
            Iterator localIterator1 = paramUserAccounts.accountCache.values().iterator();
            while (localIterator1.hasNext())
                i += ((Account[])localIterator1.next()).length;
            if (i == 0)
            {
                localObject = EMPTY_ACCOUNT_ARRAY;
            }
            else
            {
                Account[] arrayOfAccount1 = new Account[i];
                int j = 0;
                Iterator localIterator2 = paramUserAccounts.accountCache.values().iterator();
                while (localIterator2.hasNext())
                {
                    Account[] arrayOfAccount2 = (Account[])localIterator2.next();
                    System.arraycopy(arrayOfAccount2, 0, arrayOfAccount1, j, arrayOfAccount2.length);
                    j += arrayOfAccount2.length;
                }
                localObject = arrayOfAccount1;
            }
        }
    }

    public AccountAndUser[] getAllAccounts()
    {
        ArrayList localArrayList = new ArrayList();
        List localList = getAllUsers();
        if (localList == null);
        for (AccountAndUser[] arrayOfAccountAndUser = new AccountAndUser[0]; ; arrayOfAccountAndUser = (AccountAndUser[])localArrayList.toArray(new AccountAndUser[localArrayList.size()]))
        {
            return arrayOfAccountAndUser;
            synchronized (this.mUsers)
            {
                Iterator localIterator = localList.iterator();
                while (localIterator.hasNext())
                {
                    UserInfo localUserInfo = (UserInfo)localIterator.next();
                    UserAccounts localUserAccounts = getUserAccounts(localUserInfo.id);
                    if (localUserAccounts != null)
                        synchronized (localUserAccounts.cacheLock)
                        {
                            Account[] arrayOfAccount = getAccountsFromCacheLocked(localUserAccounts, null);
                            for (int i = 0; i < arrayOfAccount.length; i++)
                                localArrayList.add(new AccountAndUser(arrayOfAccount[i], localUserInfo.id));
                        }
                }
            }
        }
    }

    public void getAuthToken(IAccountManagerResponse paramIAccountManagerResponse, final Account paramAccount, final String paramString, final boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "getAuthToken: " + paramAccount + ", response " + paramIAccountManagerResponse + ", authTokenType " + paramString + ", notifyOnAuthFailure " + paramBoolean1 + ", expectActivityLaunch " + paramBoolean2 + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramIAccountManagerResponse == null)
            throw new IllegalArgumentException("response is null");
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("authTokenType is null");
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "android.permission.USE_CREDENTIALS";
        checkBinderPermission(arrayOfString);
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        RegisteredServicesCache.ServiceInfo localServiceInfo = this.mAuthenticatorCache.getServiceInfo(AuthenticatorDescription.newKey(paramAccount.type));
        final boolean bool1;
        if ((localServiceInfo != null) && (((AuthenticatorDescription)localServiceInfo.type).customTokens))
            bool1 = true;
        while (true)
        {
            final int i = Binder.getCallingUid();
            final boolean bool2;
            label235: final Bundle localBundle1;
            label249: long l;
            if ((bool1) || (permissionIsGranted(paramAccount, paramString, i)))
            {
                bool2 = true;
                if (paramBundle != null)
                    break label381;
                localBundle1 = new Bundle();
                localBundle1.putInt("callerUid", i);
                localBundle1.putInt("callerPid", Binder.getCallingPid());
                if (paramBoolean1)
                    localBundle1.putBoolean("notifyOnAuthFailure", true);
                l = clearCallingIdentity();
                if ((bool1) || (!bool2))
                    break label388;
            }
            try
            {
                String str = readAuthTokenInternal(localUserAccounts, paramAccount, paramString);
                if (str != null)
                {
                    Bundle localBundle2 = new Bundle();
                    localBundle2.putString("authtoken", str);
                    localBundle2.putString("authAccount", paramAccount.name);
                    localBundle2.putString("accountType", paramAccount.type);
                    onResult(paramIAccountManagerResponse, localBundle2);
                }
                while (true)
                {
                    return;
                    bool1 = false;
                    break;
                    bool2 = false;
                    break label235;
                    label381: localBundle1 = paramBundle;
                    break label249;
                    label388: new Session(localUserAccounts, paramIAccountManagerResponse, paramAccount.type, paramBoolean2, false, localBundle1)
                    {
                        public void onResult(Bundle paramAnonymousBundle)
                        {
                            if (paramAnonymousBundle != null)
                                if (paramAnonymousBundle.containsKey("authTokenLabelKey"))
                                {
                                    Intent localIntent2 = AccountManagerService.this.newGrantCredentialsPermissionIntent(paramAccount, i, new AccountAuthenticatorResponse(this), paramString, paramAnonymousBundle.getString("authTokenLabelKey"));
                                    Bundle localBundle = new Bundle();
                                    localBundle.putParcelable("intent", localIntent2);
                                    onResult(localBundle);
                                }
                            while (true)
                            {
                                return;
                                String str1 = paramAnonymousBundle.getString("authtoken");
                                if (str1 != null)
                                {
                                    String str2 = paramAnonymousBundle.getString("authAccount");
                                    String str3 = paramAnonymousBundle.getString("accountType");
                                    if ((TextUtils.isEmpty(str3)) || (TextUtils.isEmpty(str2)))
                                        onError(5, "the type and name should not be empty");
                                    else if (!bool1)
                                        AccountManagerService.this.saveAuthTokenToDatabase(this.mAccounts, new Account(str2, str3), paramString, str1);
                                }
                                else
                                {
                                    Intent localIntent1 = (Intent)paramAnonymousBundle.getParcelable("intent");
                                    if ((localIntent1 != null) && (paramBoolean1) && (!bool1))
                                        AccountManagerService.this.doNotification(this.mAccounts, paramAccount, paramAnonymousBundle.getString("authFailedMessage"), localIntent1);
                                    super.onResult(paramAnonymousBundle);
                                }
                            }
                        }

                        public void run()
                            throws RemoteException
                        {
                            if (!bool2)
                                this.mAuthenticator.getAuthTokenLabel(this, paramString);
                            while (true)
                            {
                                return;
                                this.mAuthenticator.getAuthToken(this, paramAccount, paramString, localBundle1);
                            }
                        }

                        protected String toDebugString(long paramAnonymousLong)
                        {
                            if (localBundle1 != null)
                                localBundle1.keySet();
                            return super.toDebugString(paramAnonymousLong) + ", getAuthToken" + ", " + paramAccount + ", authTokenType " + paramString + ", loginOptions " + localBundle1 + ", notifyOnAuthFailure " + paramBoolean1;
                        }
                    }
                    .bind();
                }
            }
            finally
            {
                restoreCallingIdentity(l);
            }
        }
    }

    public void getAuthTokenLabel(IAccountManagerResponse paramIAccountManagerResponse, final String paramString1, final String paramString2)
        throws RemoteException
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("accountType is null");
        if (paramString2 == null)
            throw new IllegalArgumentException("authTokenType is null");
        int i = getCallingUid();
        clearCallingIdentity();
        if (i != 1000)
            throw new SecurityException("can only call from system");
        UserAccounts localUserAccounts = getUserAccounts(UserId.getUserId(i));
        long l = clearCallingIdentity();
        try
        {
            new Session(localUserAccounts, paramIAccountManagerResponse, paramString1, false, false, paramString1)
            {
                public void onResult(Bundle paramAnonymousBundle)
                {
                    if (paramAnonymousBundle != null)
                    {
                        String str = paramAnonymousBundle.getString("authTokenLabelKey");
                        Bundle localBundle = new Bundle();
                        localBundle.putString("authTokenLabelKey", str);
                        super.onResult(localBundle);
                    }
                    while (true)
                    {
                        return;
                        super.onResult(paramAnonymousBundle);
                    }
                }

                public void run()
                    throws RemoteException
                {
                    this.mAuthenticator.getAuthTokenLabel(this, paramString2);
                }

                protected String toDebugString(long paramAnonymousLong)
                {
                    return super.toDebugString(paramAnonymousLong) + ", getAuthTokenLabel" + ", " + paramString1 + ", authTokenType " + paramString2;
                }
            }
            .bind();
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public AuthenticatorDescription[] getAuthenticatorTypes()
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "getAuthenticatorTypes: caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        long l = clearCallingIdentity();
        AuthenticatorDescription[] arrayOfAuthenticatorDescription;
        try
        {
            Collection localCollection = this.mAuthenticatorCache.getAllServices();
            arrayOfAuthenticatorDescription = new AuthenticatorDescription[localCollection.size()];
            int i = 0;
            Iterator localIterator = localCollection.iterator();
            if (localIterator.hasNext())
            {
                arrayOfAuthenticatorDescription[i] = ((AuthenticatorDescription)((RegisteredServicesCache.ServiceInfo)localIterator.next()).type);
                i++;
            }
        }
        finally
        {
            restoreCallingIdentity(l);
        }
        return arrayOfAuthenticatorDescription;
    }

    public String getPassword(Account paramAccount)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "getPassword: " + paramAccount + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        checkAuthenticateAccountsPermission(paramAccount);
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            String str = readPasswordInternal(localUserAccounts, paramAccount);
            return str;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    protected UserAccounts getUserAccounts(int paramInt)
    {
        synchronized (this.mUsers)
        {
            UserAccounts localUserAccounts = (UserAccounts)this.mUsers.get(paramInt);
            if (localUserAccounts == null)
            {
                localUserAccounts = initUser(paramInt);
                this.mUsers.append(paramInt, localUserAccounts);
            }
            return localUserAccounts;
        }
    }

    public String getUserData(Account paramAccount, String paramString)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "getUserData: " + paramAccount + ", key " + paramString + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("key is null");
        checkAuthenticateAccountsPermission(paramAccount);
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            String str = readUserDataInternal(localUserAccounts, paramAccount, paramString);
            return str;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void hasFeatures(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, String[] paramArrayOfString)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "hasFeatures: " + paramAccount + ", response " + paramIAccountManagerResponse + ", features " + stringArrayToString(paramArrayOfString) + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramIAccountManagerResponse == null)
            throw new IllegalArgumentException("response is null");
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramArrayOfString == null)
            throw new IllegalArgumentException("features is null");
        checkReadAccountsPermission();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            new TestFeaturesSession(localUserAccounts, paramIAccountManagerResponse, paramAccount, paramArrayOfString).bind();
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    protected void installNotification(int paramInt, Notification paramNotification)
    {
        ((NotificationManager)this.mContext.getSystemService("notification")).notify(paramInt, paramNotification);
    }

    public void invalidateAuthToken(String paramString1, String paramString2)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "invalidateAuthToken: accountType " + paramString1 + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramString1 == null)
            throw new IllegalArgumentException("accountType is null");
        if (paramString2 == null)
            throw new IllegalArgumentException("authToken is null");
        checkManageAccountsOrUseCredentialsPermissions();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            synchronized (localUserAccounts.cacheLock)
            {
                SQLiteDatabase localSQLiteDatabase = localUserAccounts.openHelper.getWritableDatabase();
                localSQLiteDatabase.beginTransaction();
                try
                {
                    invalidateAuthTokenLocked(localUserAccounts, localSQLiteDatabase, paramString1, paramString2);
                    localSQLiteDatabase.setTransactionSuccessful();
                    localSQLiteDatabase.endTransaction();
                    return;
                }
                finally
                {
                    localSQLiteDatabase.endTransaction();
                }
            }
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public IBinder onBind(Intent paramIntent)
    {
        return asBinder();
    }

    public void onServiceChanged(AuthenticatorDescription paramAuthenticatorDescription, boolean paramBoolean)
    {
        List localList = getAllUsers();
        if (localList == null)
            validateAccountsAndPopulateCache(getUserAccountsForCaller());
        while (true)
        {
            return;
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
                validateAccountsAndPopulateCache(getUserAccounts(((UserInfo)localIterator.next()).id));
        }
    }

    public String peekAuthToken(Account paramAccount, String paramString)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "peekAuthToken: " + paramAccount + ", authTokenType " + paramString + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("authTokenType is null");
        checkAuthenticateAccountsPermission(paramAccount);
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            String str = readAuthTokenInternal(localUserAccounts, paramAccount, paramString);
            return str;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    protected String readAuthTokenInternal(UserAccounts paramUserAccounts, Account paramAccount, String paramString)
    {
        synchronized (paramUserAccounts.cacheLock)
        {
            HashMap localHashMap = (HashMap)paramUserAccounts.authTokenCache.get(paramAccount);
            if (localHashMap == null)
            {
                localHashMap = readAuthTokensForAccountFromDatabaseLocked(paramUserAccounts.openHelper.getReadableDatabase(), paramAccount);
                paramUserAccounts.authTokenCache.put(paramAccount, localHashMap);
            }
            String str = (String)localHashMap.get(paramString);
            return str;
        }
    }

    protected HashMap<String, String> readAuthTokensForAccountFromDatabaseLocked(SQLiteDatabase paramSQLiteDatabase, Account paramAccount)
    {
        HashMap localHashMap = new HashMap();
        String[] arrayOfString1 = COLUMNS_AUTHTOKENS_TYPE_AND_AUTHTOKEN;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = paramAccount.name;
        arrayOfString2[1] = paramAccount.type;
        Cursor localCursor = paramSQLiteDatabase.query("authtokens", arrayOfString1, "accounts_id=(select _id FROM accounts WHERE name=? AND type=?)", arrayOfString2, null, null, null);
        try
        {
            if (localCursor.moveToNext())
                localHashMap.put(localCursor.getString(0), localCursor.getString(1));
        }
        finally
        {
            localCursor.close();
        }
        return localHashMap;
    }

    protected HashMap<String, String> readUserDataForAccountFromDatabaseLocked(SQLiteDatabase paramSQLiteDatabase, Account paramAccount)
    {
        HashMap localHashMap = new HashMap();
        String[] arrayOfString1 = COLUMNS_EXTRAS_KEY_AND_VALUE;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = paramAccount.name;
        arrayOfString2[1] = paramAccount.type;
        Cursor localCursor = paramSQLiteDatabase.query("extras", arrayOfString1, "accounts_id=(select _id FROM accounts WHERE name=? AND type=?)", arrayOfString2, null, null, null);
        try
        {
            if (localCursor.moveToNext())
                localHashMap.put(localCursor.getString(0), localCursor.getString(1));
        }
        finally
        {
            localCursor.close();
        }
        return localHashMap;
    }

    protected String readUserDataInternal(UserAccounts paramUserAccounts, Account paramAccount, String paramString)
    {
        synchronized (paramUserAccounts.cacheLock)
        {
            HashMap localHashMap = (HashMap)paramUserAccounts.userDataCache.get(paramAccount);
            if (localHashMap == null)
            {
                localHashMap = readUserDataForAccountFromDatabaseLocked(paramUserAccounts.openHelper.getReadableDatabase(), paramAccount);
                paramUserAccounts.userDataCache.put(paramAccount, localHashMap);
            }
            String str = (String)localHashMap.get(paramString);
            return str;
        }
    }

    // ERROR //
    public void removeAccount(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount)
    {
        // Byte code:
        //     0: ldc 129
        //     2: iconst_2
        //     3: invokestatic 469	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     6: ifeq +63 -> 69
        //     9: ldc 129
        //     11: new 361	java/lang/StringBuilder
        //     14: dup
        //     15: invokespecial 362	java/lang/StringBuilder:<init>	()V
        //     18: ldc_w 1420
        //     21: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     24: aload_2
        //     25: invokevirtual 371	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     28: ldc_w 1144
        //     31: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     34: aload_1
        //     35: invokevirtual 371	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     38: ldc_w 1124
        //     41: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     44: invokestatic 461	android/os/Binder:getCallingUid	()I
        //     47: invokevirtual 474	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     50: ldc_w 1126
        //     53: invokevirtual 368	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokestatic 1129	android/os/Binder:getCallingPid	()I
        //     59: invokevirtual 474	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     62: invokevirtual 377	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     65: invokestatic 479	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     68: pop
        //     69: aload_1
        //     70: ifnonnull +14 -> 84
        //     73: new 1131	java/lang/IllegalArgumentException
        //     76: dup
        //     77: ldc_w 1154
        //     80: invokespecial 1134	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     83: athrow
        //     84: aload_2
        //     85: ifnonnull +14 -> 99
        //     88: new 1131	java/lang/IllegalArgumentException
        //     91: dup
        //     92: ldc_w 1133
        //     95: invokespecial 1134	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     98: athrow
        //     99: aload_0
        //     100: invokespecial 1158	android/accounts/AccountManagerService:checkManageAccountsPermission	()V
        //     103: aload_0
        //     104: invokespecial 848	android/accounts/AccountManagerService:getUserAccountsForCaller	()Landroid/accounts/AccountManagerService$UserAccounts;
        //     107: astore_3
        //     108: invokestatic 577	android/accounts/AccountManagerService:clearCallingIdentity	()J
        //     111: lstore 4
        //     113: aload_0
        //     114: aload_0
        //     115: aload_3
        //     116: aload_2
        //     117: invokespecial 315	android/accounts/AccountManagerService:getSigninRequiredNotificationId	(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/Integer;
        //     120: invokevirtual 569	java/lang/Integer:intValue	()I
        //     123: invokevirtual 824	android/accounts/AccountManagerService:cancelNotification	(I)V
        //     126: aload_3
        //     127: invokestatic 719	android/accounts/AccountManagerService$UserAccounts:access$800	(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;
        //     130: astore 6
        //     132: aload 6
        //     134: monitorenter
        //     135: aload_3
        //     136: invokestatic 719	android/accounts/AccountManagerService$UserAccounts:access$800	(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;
        //     139: invokevirtual 1421	java/util/HashMap:keySet	()Ljava/util/Set;
        //     142: invokeinterface 411 1 0
        //     147: astore 8
        //     149: aload 8
        //     151: invokeinterface 417 1 0
        //     156: ifeq +63 -> 219
        //     159: aload 8
        //     161: invokeinterface 421 1 0
        //     166: checkcast 721	android/util/Pair
        //     169: astore 10
        //     171: aload_2
        //     172: aload 10
        //     174: getfield 1424	android/util/Pair:first	Ljava/lang/Object;
        //     177: checkcast 721	android/util/Pair
        //     180: getfield 1424	android/util/Pair:first	Ljava/lang/Object;
        //     183: invokevirtual 1022	android/accounts/Account:equals	(Ljava/lang/Object;)Z
        //     186: ifeq -37 -> 149
        //     189: aload_0
        //     190: aload_3
        //     191: invokestatic 719	android/accounts/AccountManagerService$UserAccounts:access$800	(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;
        //     194: aload 10
        //     196: invokevirtual 733	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     199: checkcast 566	java/lang/Integer
        //     202: invokevirtual 569	java/lang/Integer:intValue	()I
        //     205: invokevirtual 824	android/accounts/AccountManagerService:cancelNotification	(I)V
        //     208: goto -59 -> 149
        //     211: astore 7
        //     213: aload 6
        //     215: monitorexit
        //     216: aload 7
        //     218: athrow
        //     219: aload 6
        //     221: monitorexit
        //     222: new 37	android/accounts/AccountManagerService$RemoveAccountSession
        //     225: dup
        //     226: aload_0
        //     227: aload_3
        //     228: aload_1
        //     229: aload_2
        //     230: invokespecial 1427	android/accounts/AccountManagerService$RemoveAccountSession:<init>	(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;)V
        //     233: invokevirtual 1428	android/accounts/AccountManagerService$RemoveAccountSession:bind	()V
        //     236: lload 4
        //     238: invokestatic 607	android/accounts/AccountManagerService:restoreCallingIdentity	(J)V
        //     241: return
        //     242: astore 9
        //     244: lload 4
        //     246: invokestatic 607	android/accounts/AccountManagerService:restoreCallingIdentity	(J)V
        //     249: aload 9
        //     251: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     135	216	211	finally
        //     219	222	211	finally
        //     222	236	242	finally
    }

    protected void removeAccountInternal(Account paramAccount)
    {
        removeAccountInternal(getUserAccountsForCaller(), paramAccount);
    }

    public void setAuthToken(Account paramAccount, String paramString1, String paramString2)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "setAuthToken: " + paramAccount + ", authTokenType " + paramString1 + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString1 == null)
            throw new IllegalArgumentException("authTokenType is null");
        checkAuthenticateAccountsPermission(paramAccount);
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            saveAuthTokenToDatabase(localUserAccounts, paramAccount, paramString1, paramString2);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void setPassword(Account paramAccount, String paramString)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "setAuthToken: " + paramAccount + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        checkAuthenticateAccountsPermission(paramAccount);
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            setPasswordInternal(localUserAccounts, paramAccount, paramString);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void setUserData(Account paramAccount, String paramString1, String paramString2)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "setUserData: " + paramAccount + ", key " + paramString1 + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramString1 == null)
            throw new IllegalArgumentException("key is null");
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        checkAuthenticateAccountsPermission(paramAccount);
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            setUserdataInternal(localUserAccounts, paramAccount, paramString1, paramString2);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void systemReady()
    {
        this.mAuthenticatorCache.generateServicesMap();
        initUser(0);
    }

    public void updateAppPermission(Account paramAccount, String paramString, int paramInt, boolean paramBoolean)
        throws RemoteException
    {
        if (getCallingUid() != 1000)
            throw new SecurityException();
        if (paramBoolean)
            grantAppPermission(paramAccount, paramString, paramInt);
        while (true)
        {
            return;
            revokeAppPermission(paramAccount, paramString, paramInt);
        }
    }

    public void updateCredentials(IAccountManagerResponse paramIAccountManagerResponse, final Account paramAccount, final String paramString, boolean paramBoolean, final Bundle paramBundle)
    {
        if (Log.isLoggable("AccountManagerService", 2))
            Log.v("AccountManagerService", "updateCredentials: " + paramAccount + ", response " + paramIAccountManagerResponse + ", authTokenType " + paramString + ", expectActivityLaunch " + paramBoolean + ", caller's uid " + Binder.getCallingUid() + ", pid " + Binder.getCallingPid());
        if (paramIAccountManagerResponse == null)
            throw new IllegalArgumentException("response is null");
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("authTokenType is null");
        checkManageAccountsPermission();
        UserAccounts localUserAccounts = getUserAccountsForCaller();
        long l = clearCallingIdentity();
        try
        {
            new Session(localUserAccounts, paramIAccountManagerResponse, paramAccount.type, paramBoolean, true, paramAccount)
            {
                public void run()
                    throws RemoteException
                {
                    this.mAuthenticator.updateCredentials(this, paramAccount, paramString, paramBundle);
                }

                protected String toDebugString(long paramAnonymousLong)
                {
                    if (paramBundle != null)
                        paramBundle.keySet();
                    return super.toDebugString(paramAnonymousLong) + ", updateCredentials" + ", " + paramAccount + ", authTokenType " + paramString + ", loginOptions " + paramBundle;
                }
            }
            .bind();
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    protected void writeAuthTokenIntoCacheLocked(UserAccounts paramUserAccounts, SQLiteDatabase paramSQLiteDatabase, Account paramAccount, String paramString1, String paramString2)
    {
        HashMap localHashMap = (HashMap)paramUserAccounts.authTokenCache.get(paramAccount);
        if (localHashMap == null)
        {
            localHashMap = readAuthTokensForAccountFromDatabaseLocked(paramSQLiteDatabase, paramAccount);
            paramUserAccounts.authTokenCache.put(paramAccount, localHashMap);
        }
        if (paramString2 == null)
            localHashMap.remove(paramString1);
        while (true)
        {
            return;
            localHashMap.put(paramString1, paramString2);
        }
    }

    protected void writeUserDataIntoCacheLocked(UserAccounts paramUserAccounts, SQLiteDatabase paramSQLiteDatabase, Account paramAccount, String paramString1, String paramString2)
    {
        HashMap localHashMap = (HashMap)paramUserAccounts.userDataCache.get(paramAccount);
        if (localHashMap == null)
        {
            localHashMap = readUserDataForAccountFromDatabaseLocked(paramSQLiteDatabase, paramAccount);
            paramUserAccounts.userDataCache.put(paramAccount, localHashMap);
        }
        if (paramString2 == null)
            localHashMap.remove(paramString1);
        while (true)
        {
            return;
            localHashMap.put(paramString1, paramString2);
        }
    }

    static class DatabaseHelper extends SQLiteOpenHelper
    {
        public DatabaseHelper(Context paramContext, int paramInt)
        {
            super(AccountManagerService.getDatabaseName(paramInt), null, 4);
        }

        private void createAccountsDeletionTrigger(SQLiteDatabase paramSQLiteDatabase)
        {
            paramSQLiteDatabase.execSQL(" CREATE TRIGGER accountsDelete DELETE ON accounts BEGIN     DELETE FROM authtokens         WHERE accounts_id=OLD._id ;     DELETE FROM extras         WHERE accounts_id=OLD._id ;     DELETE FROM grants         WHERE accounts_id=OLD._id ; END");
        }

        private void createGrantsTable(SQLiteDatabase paramSQLiteDatabase)
        {
            paramSQLiteDatabase.execSQL("CREATE TABLE grants (    accounts_id INTEGER NOT NULL, auth_token_type STRING NOT NULL,    uid INTEGER NOT NULL,    UNIQUE (accounts_id,auth_token_type,uid))");
        }

        public void onCreate(SQLiteDatabase paramSQLiteDatabase)
        {
            paramSQLiteDatabase.execSQL("CREATE TABLE accounts ( _id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, type TEXT NOT NULL, password TEXT, UNIQUE(name,type))");
            paramSQLiteDatabase.execSQL("CREATE TABLE authtokens (    _id INTEGER PRIMARY KEY AUTOINCREMENT,    accounts_id INTEGER NOT NULL, type TEXT NOT NULL,    authtoken TEXT,    UNIQUE (accounts_id,type))");
            createGrantsTable(paramSQLiteDatabase);
            paramSQLiteDatabase.execSQL("CREATE TABLE extras ( _id INTEGER PRIMARY KEY AUTOINCREMENT, accounts_id INTEGER, key TEXT NOT NULL, value TEXT, UNIQUE(accounts_id,key))");
            paramSQLiteDatabase.execSQL("CREATE TABLE meta ( key TEXT PRIMARY KEY NOT NULL, value TEXT)");
            createAccountsDeletionTrigger(paramSQLiteDatabase);
        }

        public void onOpen(SQLiteDatabase paramSQLiteDatabase)
        {
            if (Log.isLoggable("AccountManagerService", 2))
                Log.v("AccountManagerService", "opened database accounts.db");
        }

        public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
        {
            Log.e("AccountManagerService", "upgrade from version " + paramInt1 + " to version " + paramInt2);
            if (paramInt1 == 1)
                paramInt1++;
            if (paramInt1 == 2)
            {
                createGrantsTable(paramSQLiteDatabase);
                paramSQLiteDatabase.execSQL("DROP TRIGGER accountsDelete");
                createAccountsDeletionTrigger(paramSQLiteDatabase);
                paramInt1++;
            }
            if (paramInt1 == 3)
            {
                paramSQLiteDatabase.execSQL("UPDATE accounts SET type = 'com.google' WHERE type == 'com.google.GAIA'");
                (paramInt1 + 1);
            }
        }
    }

    private class MessageHandler extends Handler
    {
        MessageHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                throw new IllegalStateException("unhandled message: " + paramMessage.what);
            case 3:
            }
            ((AccountManagerService.Session)paramMessage.obj).onTimedOut();
        }
    }

    private abstract class Session extends IAccountAuthenticatorResponse.Stub
        implements IBinder.DeathRecipient, ServiceConnection
    {
        final String mAccountType;
        protected final AccountManagerService.UserAccounts mAccounts;
        IAccountAuthenticator mAuthenticator = null;
        final long mCreationTime;
        final boolean mExpectActivityLaunch;
        private int mNumErrors = 0;
        private int mNumRequestContinued = 0;
        public int mNumResults = 0;
        IAccountManagerResponse mResponse;
        private final boolean mStripAuthTokenFromResult;

        public Session(AccountManagerService.UserAccounts paramIAccountManagerResponse, IAccountManagerResponse paramString, String paramBoolean1, boolean paramBoolean2, boolean arg6)
        {
            if (paramString == null)
                throw new IllegalArgumentException("response is null");
            if (paramBoolean1 == null)
                throw new IllegalArgumentException("accountType is null");
            this.mAccounts = paramIAccountManagerResponse;
            boolean bool;
            this.mStripAuthTokenFromResult = bool;
            this.mResponse = paramString;
            this.mAccountType = paramBoolean1;
            this.mExpectActivityLaunch = paramBoolean2;
            this.mCreationTime = SystemClock.elapsedRealtime();
            synchronized (AccountManagerService.this.mSessions)
            {
                AccountManagerService.this.mSessions.put(toString(), this);
            }
            try
            {
                paramString.asBinder().linkToDeath(this, 0);
                return;
                localObject = finally;
                throw localObject;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    this.mResponse = null;
                    binderDied();
                }
            }
        }

        private boolean bindToAuthenticator(String paramString)
        {
            boolean bool = false;
            RegisteredServicesCache.ServiceInfo localServiceInfo = AccountManagerService.this.mAuthenticatorCache.getServiceInfo(AuthenticatorDescription.newKey(paramString));
            if (localServiceInfo == null)
                if (Log.isLoggable("AccountManagerService", 2))
                    Log.v("AccountManagerService", "there is no authenticator for " + paramString + ", bailing out");
            while (true)
            {
                return bool;
                Intent localIntent = new Intent();
                localIntent.setAction("android.accounts.AccountAuthenticator");
                localIntent.setComponent(localServiceInfo.componentName);
                if (Log.isLoggable("AccountManagerService", 2))
                    Log.v("AccountManagerService", "performing bindService to " + localServiceInfo.componentName);
                if (!AccountManagerService.this.mContext.bindService(localIntent, this, 1, AccountManagerService.UserAccounts.access$700(this.mAccounts)))
                {
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", "bindService to " + localServiceInfo.componentName + " failed");
                }
                else
                    bool = true;
            }
        }

        private void close()
        {
            synchronized (AccountManagerService.this.mSessions)
            {
                if (AccountManagerService.this.mSessions.remove(toString()) != null)
                {
                    if (this.mResponse != null)
                    {
                        this.mResponse.asBinder().unlinkToDeath(this, 0);
                        this.mResponse = null;
                    }
                    cancelTimeout();
                    unbind();
                }
            }
        }

        private void unbind()
        {
            if (this.mAuthenticator != null)
            {
                this.mAuthenticator = null;
                AccountManagerService.this.mContext.unbindService(this);
            }
        }

        void bind()
        {
            if (Log.isLoggable("AccountManagerService", 2))
                Log.v("AccountManagerService", "initiating bind to authenticator type " + this.mAccountType);
            if (!bindToAuthenticator(this.mAccountType))
            {
                Log.d("AccountManagerService", "bind attempt failed for " + toDebugString());
                onError(1, "bind failure");
            }
        }

        public void binderDied()
        {
            this.mResponse = null;
            close();
        }

        public void cancelTimeout()
        {
            AccountManagerService.this.mMessageHandler.removeMessages(3, this);
        }

        IAccountManagerResponse getResponseAndClose()
        {
            IAccountManagerResponse localIAccountManagerResponse;
            if (this.mResponse == null)
                localIAccountManagerResponse = null;
            while (true)
            {
                return localIAccountManagerResponse;
                localIAccountManagerResponse = this.mResponse;
                close();
            }
        }

        public void onError(int paramInt, String paramString)
        {
            this.mNumErrors = (1 + this.mNumErrors);
            IAccountManagerResponse localIAccountManagerResponse = getResponseAndClose();
            if (localIAccountManagerResponse != null)
                if (Log.isLoggable("AccountManagerService", 2))
                    Log.v("AccountManagerService", getClass().getSimpleName() + " calling onError() on response " + localIAccountManagerResponse);
            while (true)
            {
                try
                {
                    localIAccountManagerResponse.onError(paramInt, paramString);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    if (!Log.isLoggable("AccountManagerService", 2))
                        continue;
                    Log.v("AccountManagerService", "Session.onError: caught RemoteException while responding", localRemoteException);
                    continue;
                }
                if (Log.isLoggable("AccountManagerService", 2))
                    Log.v("AccountManagerService", "Session.onError: already closed");
            }
        }

        public void onRequestContinued()
        {
            this.mNumRequestContinued = (1 + this.mNumRequestContinued);
        }

        public void onResult(Bundle paramBundle)
        {
            this.mNumResults = (1 + this.mNumResults);
            if ((paramBundle != null) && (!TextUtils.isEmpty(paramBundle.getString("authtoken"))))
            {
                String str1 = paramBundle.getString("authAccount");
                String str2 = paramBundle.getString("accountType");
                if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2)))
                {
                    Account localAccount = new Account(str1, str2);
                    AccountManagerService.this.cancelNotification(AccountManagerService.this.getSigninRequiredNotificationId(this.mAccounts, localAccount).intValue());
                }
            }
            IAccountManagerResponse localIAccountManagerResponse;
            if ((this.mExpectActivityLaunch) && (paramBundle != null) && (paramBundle.containsKey("intent")))
                localIAccountManagerResponse = this.mResponse;
            while (true)
            {
                if ((localIAccountManagerResponse == null) || (paramBundle == null));
                try
                {
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", getClass().getSimpleName() + " calling onError() on response " + localIAccountManagerResponse);
                    localIAccountManagerResponse.onError(5, "null bundle returned");
                    while (true)
                    {
                        return;
                        localIAccountManagerResponse = getResponseAndClose();
                        break;
                        if (this.mStripAuthTokenFromResult)
                            paramBundle.remove("authtoken");
                        if (Log.isLoggable("AccountManagerService", 2))
                            Log.v("AccountManagerService", getClass().getSimpleName() + " calling onResult() on response " + localIAccountManagerResponse);
                        localIAccountManagerResponse.onResult(paramBundle);
                    }
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        if (Log.isLoggable("AccountManagerService", 2))
                            Log.v("AccountManagerService", "failure while notifying response", localRemoteException);
                }
            }
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            this.mAuthenticator = IAccountAuthenticator.Stub.asInterface(paramIBinder);
            try
            {
                run();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    onError(1, "remote exception");
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            this.mAuthenticator = null;
            IAccountManagerResponse localIAccountManagerResponse = getResponseAndClose();
            if (localIAccountManagerResponse != null);
            try
            {
                localIAccountManagerResponse.onError(1, "disconnected");
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", "Session.onServiceDisconnected: caught RemoteException while responding", localRemoteException);
            }
        }

        public void onTimedOut()
        {
            IAccountManagerResponse localIAccountManagerResponse = getResponseAndClose();
            if (localIAccountManagerResponse != null);
            try
            {
                localIAccountManagerResponse.onError(1, "timeout");
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", "Session.onTimedOut: caught RemoteException while responding", localRemoteException);
            }
        }

        public abstract void run()
            throws RemoteException;

        public void scheduleTimeout()
        {
            AccountManagerService.this.mMessageHandler.sendMessageDelayed(AccountManagerService.this.mMessageHandler.obtainMessage(3, this), 60000L);
        }

        protected String toDebugString()
        {
            return toDebugString(SystemClock.elapsedRealtime());
        }

        protected String toDebugString(long paramLong)
        {
            StringBuilder localStringBuilder = new StringBuilder().append("Session: expectLaunch ").append(this.mExpectActivityLaunch).append(", connected ");
            if (this.mAuthenticator != null);
            for (boolean bool = true; ; bool = false)
                return bool + ", stats (" + this.mNumResults + "/" + this.mNumRequestContinued + "/" + this.mNumErrors + ")" + ", lifetime " + (paramLong - this.mCreationTime) / 1000.0D;
        }
    }

    private class GetAccountsByTypeAndFeatureSession extends AccountManagerService.Session
    {
        private volatile Account[] mAccountsOfType = null;
        private volatile ArrayList<Account> mAccountsWithFeatures = null;
        private volatile int mCurrentAccount = 0;
        private final String[] mFeatures;

        public GetAccountsByTypeAndFeatureSession(AccountManagerService.UserAccounts paramIAccountManagerResponse, IAccountManagerResponse paramString, String paramArrayOfString, String[] arg5)
        {
            super(paramIAccountManagerResponse, paramString, paramArrayOfString, false, true);
            Object localObject;
            this.mFeatures = localObject;
        }

        public void checkAccount()
        {
            if (this.mCurrentAccount >= this.mAccountsOfType.length)
                sendResult();
            while (true)
            {
                return;
                IAccountAuthenticator localIAccountAuthenticator = this.mAuthenticator;
                if (localIAccountAuthenticator == null)
                {
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", "checkAccount: aborting session since we are no longer connected to the authenticator, " + toDebugString());
                }
                else
                    try
                    {
                        localIAccountAuthenticator.hasFeatures(this, this.mAccountsOfType[this.mCurrentAccount], this.mFeatures);
                    }
                    catch (RemoteException localRemoteException)
                    {
                        onError(1, "remote exception");
                    }
            }
        }

        public void onResult(Bundle paramBundle)
        {
            this.mNumResults = (1 + this.mNumResults);
            if (paramBundle == null)
                onError(5, "null bundle");
            while (true)
            {
                return;
                if (paramBundle.getBoolean("booleanResult", false))
                    this.mAccountsWithFeatures.add(this.mAccountsOfType[this.mCurrentAccount]);
                this.mCurrentAccount = (1 + this.mCurrentAccount);
                checkAccount();
            }
        }

        public void run()
            throws RemoteException
        {
            synchronized (AccountManagerService.UserAccounts.access$200(this.mAccounts))
            {
                this.mAccountsOfType = AccountManagerService.this.getAccountsFromCacheLocked(this.mAccounts, this.mAccountType);
                this.mAccountsWithFeatures = new ArrayList(this.mAccountsOfType.length);
                this.mCurrentAccount = 0;
                checkAccount();
                return;
            }
        }

        public void sendResult()
        {
            IAccountManagerResponse localIAccountManagerResponse = getResponseAndClose();
            if (localIAccountManagerResponse != null);
            try
            {
                Account[] arrayOfAccount = new Account[this.mAccountsWithFeatures.size()];
                for (int i = 0; i < arrayOfAccount.length; i++)
                    arrayOfAccount[i] = ((Account)this.mAccountsWithFeatures.get(i));
                if (Log.isLoggable("AccountManagerService", 2))
                    Log.v("AccountManagerService", getClass().getSimpleName() + " calling onResult() on response " + localIAccountManagerResponse);
                Bundle localBundle = new Bundle();
                localBundle.putParcelableArray("accounts", arrayOfAccount);
                localIAccountManagerResponse.onResult(localBundle);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", "failure while notifying response", localRemoteException);
            }
        }

        protected String toDebugString(long paramLong)
        {
            StringBuilder localStringBuilder = new StringBuilder().append(super.toDebugString(paramLong)).append(", getAccountsByTypeAndFeatures").append(", ");
            if (this.mFeatures != null);
            for (String str = TextUtils.join(",", this.mFeatures); ; str = null)
                return str;
        }
    }

    private class RemoveAccountSession extends AccountManagerService.Session
    {
        final Account mAccount;

        public RemoveAccountSession(AccountManagerService.UserAccounts paramIAccountManagerResponse, IAccountManagerResponse paramAccount, Account arg4)
        {
            super(paramIAccountManagerResponse, paramAccount, localObject.type, false, true);
            this.mAccount = localObject;
        }

        public void onResult(Bundle paramBundle)
        {
            IAccountManagerResponse localIAccountManagerResponse;
            Bundle localBundle;
            if ((paramBundle != null) && (paramBundle.containsKey("booleanResult")) && (!paramBundle.containsKey("intent")))
            {
                boolean bool = paramBundle.getBoolean("booleanResult");
                if (bool)
                    AccountManagerService.this.removeAccountInternal(this.mAccounts, this.mAccount);
                localIAccountManagerResponse = getResponseAndClose();
                if (localIAccountManagerResponse != null)
                {
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", getClass().getSimpleName() + " calling onResult() on response " + localIAccountManagerResponse);
                    localBundle = new Bundle();
                    localBundle.putBoolean("booleanResult", bool);
                }
            }
            try
            {
                localIAccountManagerResponse.onResult(localBundle);
                label126: super.onResult(paramBundle);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label126;
            }
        }

        public void run()
            throws RemoteException
        {
            this.mAuthenticator.getAccountRemovalAllowed(this, this.mAccount);
        }

        protected String toDebugString(long paramLong)
        {
            return super.toDebugString(paramLong) + ", removeAccount" + ", account " + this.mAccount;
        }
    }

    private class TestFeaturesSession extends AccountManagerService.Session
    {
        private final Account mAccount;
        private final String[] mFeatures;

        public TestFeaturesSession(AccountManagerService.UserAccounts paramIAccountManagerResponse, IAccountManagerResponse paramAccount, Account paramArrayOfString, String[] arg5)
        {
            super(paramIAccountManagerResponse, paramAccount, paramArrayOfString.type, false, true);
            Object localObject;
            this.mFeatures = localObject;
            this.mAccount = paramArrayOfString;
        }

        public void onResult(Bundle paramBundle)
        {
            IAccountManagerResponse localIAccountManagerResponse = getResponseAndClose();
            if (localIAccountManagerResponse != null)
            {
                if (paramBundle == null);
                try
                {
                    localIAccountManagerResponse.onError(5, "null bundle");
                    return;
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", getClass().getSimpleName() + " calling onResult() on response " + localIAccountManagerResponse);
                    Bundle localBundle = new Bundle();
                    localBundle.putBoolean("booleanResult", paramBundle.getBoolean("booleanResult", false));
                    localIAccountManagerResponse.onResult(localBundle);
                }
                catch (RemoteException localRemoteException)
                {
                    if (Log.isLoggable("AccountManagerService", 2))
                        Log.v("AccountManagerService", "failure while notifying response", localRemoteException);
                }
            }
        }

        public void run()
            throws RemoteException
        {
            try
            {
                this.mAuthenticator.hasFeatures(this, this.mAccount, this.mFeatures);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    onError(1, "remote exception");
            }
        }

        protected String toDebugString(long paramLong)
        {
            StringBuilder localStringBuilder = new StringBuilder().append(super.toDebugString(paramLong)).append(", hasFeatures").append(", ").append(this.mAccount).append(", ");
            if (this.mFeatures != null);
            for (String str = TextUtils.join(",", this.mFeatures); ; str = null)
                return str;
        }
    }

    static class UserAccounts
    {
        private final HashMap<String, Account[]> accountCache = new LinkedHashMap();
        private HashMap<Account, HashMap<String, String>> authTokenCache = new HashMap();
        private final Object cacheLock = new Object();
        private final HashMap<Pair<Pair<Account, String>, Integer>, Integer> credentialsPermissionNotificationIds = new HashMap();
        private final AccountManagerService.DatabaseHelper openHelper;
        private final HashMap<Account, Integer> signinRequiredNotificationIds = new HashMap();
        private HashMap<Account, HashMap<String, String>> userDataCache = new HashMap();
        private final int userId;

        UserAccounts(Context paramContext, int paramInt)
        {
            this.userId = paramInt;
            synchronized (this.cacheLock)
            {
                this.openHelper = new AccountManagerService.DatabaseHelper(paramContext, paramInt);
                return;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static int checkSignatures(PackageManager paramPackageManager, int paramInt1, int paramInt2, String paramString)
        {
            if (ExtraPackageManager.isTrustedAccountSignature(paramPackageManager, paramString, paramInt1, paramInt2));
            for (int i = 0; ; i = -3)
                return i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AccountManagerService
 * JD-Core Version:        0.6.2
 */