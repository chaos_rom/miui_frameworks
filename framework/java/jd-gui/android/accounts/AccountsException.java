package android.accounts;

public class AccountsException extends Exception
{
    public AccountsException()
    {
    }

    public AccountsException(String paramString)
    {
        super(paramString);
    }

    public AccountsException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public AccountsException(Throwable paramThrowable)
    {
        super(paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AccountsException
 * JD-Core Version:        0.6.2
 */