package android.accounts;

import android.content.pm.RegisteredServicesCache.ServiceInfo;
import android.content.pm.RegisteredServicesCacheListener;
import android.os.Handler;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collection;

public abstract interface IAccountAuthenticatorCache
{
    public abstract void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString);

    public abstract void generateServicesMap();

    public abstract Collection<RegisteredServicesCache.ServiceInfo<AuthenticatorDescription>> getAllServices();

    public abstract RegisteredServicesCache.ServiceInfo<AuthenticatorDescription> getServiceInfo(AuthenticatorDescription paramAuthenticatorDescription);

    public abstract void setListener(RegisteredServicesCacheListener<AuthenticatorDescription> paramRegisteredServicesCacheListener, Handler paramHandler);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.IAccountAuthenticatorCache
 * JD-Core Version:        0.6.2
 */