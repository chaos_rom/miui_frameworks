package android.accounts;

public abstract interface AccountManagerCallback<V>
{
    public abstract void run(AccountManagerFuture<V> paramAccountManagerFuture);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AccountManagerCallback
 * JD-Core Version:        0.6.2
 */