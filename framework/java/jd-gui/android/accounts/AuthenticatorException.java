package android.accounts;

public class AuthenticatorException extends AccountsException
{
    public AuthenticatorException()
    {
    }

    public AuthenticatorException(String paramString)
    {
        super(paramString);
    }

    public AuthenticatorException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public AuthenticatorException(Throwable paramThrowable)
    {
        super(paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AuthenticatorException
 * JD-Core Version:        0.6.2
 */