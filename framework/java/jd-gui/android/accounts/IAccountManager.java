package android.accounts;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IAccountManager extends IInterface
{
    public abstract boolean addAccount(Account paramAccount, String paramString, Bundle paramBundle)
        throws RemoteException;

    public abstract void addAcount(IAccountManagerResponse paramIAccountManagerResponse, String paramString1, String paramString2, String[] paramArrayOfString, boolean paramBoolean, Bundle paramBundle)
        throws RemoteException;

    public abstract void clearPassword(Account paramAccount)
        throws RemoteException;

    public abstract void confirmCredentials(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, Bundle paramBundle, boolean paramBoolean)
        throws RemoteException;

    public abstract void editProperties(IAccountManagerResponse paramIAccountManagerResponse, String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract Account[] getAccounts(String paramString)
        throws RemoteException;

    public abstract void getAccountsByFeatures(IAccountManagerResponse paramIAccountManagerResponse, String paramString, String[] paramArrayOfString)
        throws RemoteException;

    public abstract void getAuthToken(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, String paramString, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle)
        throws RemoteException;

    public abstract void getAuthTokenLabel(IAccountManagerResponse paramIAccountManagerResponse, String paramString1, String paramString2)
        throws RemoteException;

    public abstract AuthenticatorDescription[] getAuthenticatorTypes()
        throws RemoteException;

    public abstract String getPassword(Account paramAccount)
        throws RemoteException;

    public abstract String getUserData(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract void hasFeatures(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, String[] paramArrayOfString)
        throws RemoteException;

    public abstract void invalidateAuthToken(String paramString1, String paramString2)
        throws RemoteException;

    public abstract String peekAuthToken(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract void removeAccount(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount)
        throws RemoteException;

    public abstract void setAuthToken(Account paramAccount, String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setPassword(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract void setUserData(Account paramAccount, String paramString1, String paramString2)
        throws RemoteException;

    public abstract void updateAppPermission(Account paramAccount, String paramString, int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract void updateCredentials(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, String paramString, boolean paramBoolean, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccountManager
    {
        private static final String DESCRIPTOR = "android.accounts.IAccountManager";
        static final int TRANSACTION_addAccount = 7;
        static final int TRANSACTION_addAcount = 17;
        static final int TRANSACTION_clearPassword = 13;
        static final int TRANSACTION_confirmCredentials = 20;
        static final int TRANSACTION_editProperties = 19;
        static final int TRANSACTION_getAccounts = 4;
        static final int TRANSACTION_getAccountsByFeatures = 6;
        static final int TRANSACTION_getAuthToken = 16;
        static final int TRANSACTION_getAuthTokenLabel = 21;
        static final int TRANSACTION_getAuthenticatorTypes = 3;
        static final int TRANSACTION_getPassword = 1;
        static final int TRANSACTION_getUserData = 2;
        static final int TRANSACTION_hasFeatures = 5;
        static final int TRANSACTION_invalidateAuthToken = 9;
        static final int TRANSACTION_peekAuthToken = 10;
        static final int TRANSACTION_removeAccount = 8;
        static final int TRANSACTION_setAuthToken = 11;
        static final int TRANSACTION_setPassword = 12;
        static final int TRANSACTION_setUserData = 14;
        static final int TRANSACTION_updateAppPermission = 15;
        static final int TRANSACTION_updateCredentials = 18;

        public Stub()
        {
            attachInterface(this, "android.accounts.IAccountManager");
        }

        public static IAccountManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.accounts.IAccountManager");
                if ((localIInterface != null) && ((localIInterface instanceof IAccountManager)))
                    localObject = (IAccountManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.accounts.IAccountManager");
                continue;
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount14 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount14 = null)
                {
                    String str10 = getPassword(localAccount14);
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str10);
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount13 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount13 = null)
                {
                    String str9 = getUserData(localAccount13, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str9);
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                AuthenticatorDescription[] arrayOfAuthenticatorDescription = getAuthenticatorTypes();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedArray(arrayOfAuthenticatorDescription, j);
                continue;
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                Account[] arrayOfAccount = getAccounts(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeTypedArray(arrayOfAccount, j);
                continue;
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                IAccountManagerResponse localIAccountManagerResponse7 = IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                if (paramParcel1.readInt() != 0);
                for (Account localAccount12 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount12 = null)
                {
                    hasFeatures(localIAccountManagerResponse7, localAccount12, paramParcel1.createStringArray());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                getAccountsByFeatures(IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString(), paramParcel1.createStringArray());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                Account localAccount11;
                label505: String str8;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount11 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    str8 = paramParcel1.readString();
                    if (paramParcel1.readInt() == 0)
                        break label572;
                }
                label572: for (Bundle localBundle5 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle5 = null)
                {
                    boolean bool = addAccount(localAccount11, str8, localBundle5);
                    paramParcel2.writeNoException();
                    if (bool)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                    localAccount11 = null;
                    break label505;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                IAccountManagerResponse localIAccountManagerResponse6 = IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                if (paramParcel1.readInt() != 0);
                for (Account localAccount10 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount10 = null)
                {
                    removeAccount(localIAccountManagerResponse6, localAccount10);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                invalidateAuthToken(paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount9 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount9 = null)
                {
                    String str7 = peekAuthToken(localAccount9, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str7);
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount8 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount8 = null)
                {
                    setAuthToken(localAccount8, paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount7 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount7 = null)
                {
                    setPassword(localAccount7, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount6 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount6 = null)
                {
                    clearPassword(localAccount6);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                if (paramParcel1.readInt() != 0);
                for (Account localAccount5 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount5 = null)
                {
                    setUserData(localAccount5, paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                Account localAccount4;
                label949: String str6;
                int i10;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount4 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    str6 = paramParcel1.readString();
                    i10 = paramParcel1.readInt();
                    if (paramParcel1.readInt() == 0)
                        break label997;
                }
                label997: int i12;
                for (int i11 = j; ; i12 = 0)
                {
                    updateAppPermission(localAccount4, str6, i10, i11);
                    paramParcel2.writeNoException();
                    break;
                    localAccount4 = null;
                    break label949;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                IAccountManagerResponse localIAccountManagerResponse5 = IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                Account localAccount3;
                label1039: String str5;
                int i6;
                label1056: int i8;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount3 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    str5 = paramParcel1.readString();
                    if (paramParcel1.readInt() == 0)
                        break label1117;
                    i6 = j;
                    if (paramParcel1.readInt() == 0)
                        break label1123;
                    i8 = j;
                    label1067: if (paramParcel1.readInt() == 0)
                        break label1129;
                }
                label1117: label1123: label1129: for (Bundle localBundle4 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle4 = null)
                {
                    getAuthToken(localIAccountManagerResponse5, localAccount3, str5, i6, i8, localBundle4);
                    paramParcel2.writeNoException();
                    break;
                    localAccount3 = null;
                    break label1039;
                    int i7 = 0;
                    break label1056;
                    int i9 = 0;
                    break label1067;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                IAccountManagerResponse localIAccountManagerResponse4 = IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                String str3 = paramParcel1.readString();
                String str4 = paramParcel1.readString();
                String[] arrayOfString = paramParcel1.createStringArray();
                int i4;
                if (paramParcel1.readInt() != 0)
                {
                    i4 = j;
                    label1179: if (paramParcel1.readInt() == 0)
                        break label1229;
                }
                label1229: for (Bundle localBundle3 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle3 = null)
                {
                    addAcount(localIAccountManagerResponse4, str3, str4, arrayOfString, i4, localBundle3);
                    paramParcel2.writeNoException();
                    break;
                    int i5 = 0;
                    break label1179;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                IAccountManagerResponse localIAccountManagerResponse3 = IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                Account localAccount2;
                label1271: String str2;
                int i2;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount2 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    str2 = paramParcel1.readString();
                    if (paramParcel1.readInt() == 0)
                        break label1336;
                    i2 = j;
                    label1288: if (paramParcel1.readInt() == 0)
                        break label1342;
                }
                label1336: label1342: for (Bundle localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle2 = null)
                {
                    updateCredentials(localIAccountManagerResponse3, localAccount2, str2, i2, localBundle2);
                    paramParcel2.writeNoException();
                    break;
                    localAccount2 = null;
                    break label1271;
                    int i3 = 0;
                    break label1288;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                IAccountManagerResponse localIAccountManagerResponse2 = IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                String str1 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                int i1;
                for (int n = j; ; i1 = 0)
                {
                    editProperties(localIAccountManagerResponse2, str1, n);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                IAccountManagerResponse localIAccountManagerResponse1 = IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                Account localAccount1;
                label1439: Bundle localBundle1;
                if (paramParcel1.readInt() != 0)
                {
                    localAccount1 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label1496;
                    localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                    label1460: if (paramParcel1.readInt() == 0)
                        break label1502;
                }
                label1496: label1502: int m;
                for (int k = j; ; m = 0)
                {
                    confirmCredentials(localIAccountManagerResponse1, localAccount1, localBundle1, k);
                    paramParcel2.writeNoException();
                    break;
                    localAccount1 = null;
                    break label1439;
                    localBundle1 = null;
                    break label1460;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountManager");
                getAuthTokenLabel(IAccountManagerResponse.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IAccountManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public boolean addAccount(Account paramAccount, String paramString, Bundle paramBundle)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            if (paramBundle != null)
                            {
                                localParcel1.writeInt(1);
                                paramBundle.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(7, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label140;
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label140: bool = false;
                }
            }

            public void addAcount(IAccountManagerResponse paramIAccountManagerResponse, String paramString1, String paramString2, String[] paramArrayOfString, boolean paramBoolean, Bundle paramBundle)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    IBinder localIBinder;
                    if (paramIAccountManagerResponse != null)
                    {
                        localIBinder = paramIAccountManagerResponse.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString1);
                        localParcel1.writeString(paramString2);
                        localParcel1.writeStringArray(paramArrayOfString);
                        if (!paramBoolean)
                            break label128;
                        label63: localParcel1.writeInt(i);
                        if (paramBundle == null)
                            break label134;
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(17, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localIBinder = null;
                        break;
                        label128: i = 0;
                        break label63;
                        label134: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void clearPassword(Account paramAccount)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void confirmCredentials(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, Bundle paramBundle, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                        IBinder localIBinder;
                        if (paramIAccountManagerResponse != null)
                        {
                            localIBinder = paramIAccountManagerResponse.asBinder();
                            localParcel1.writeStrongBinder(localIBinder);
                            if (paramAccount != null)
                            {
                                localParcel1.writeInt(1);
                                paramAccount.writeToParcel(localParcel1, 0);
                                if (paramBundle == null)
                                    break label146;
                                localParcel1.writeInt(1);
                                paramBundle.writeToParcel(localParcel1, 0);
                                break label161;
                                localParcel1.writeInt(i);
                                this.mRemote.transact(20, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localIBinder = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label146: localParcel1.writeInt(0);
                    label161: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public void editProperties(IAccountManagerResponse paramIAccountManagerResponse, String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramIAccountManagerResponse != null)
                    {
                        localIBinder = paramIAccountManagerResponse.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString);
                        if (paramBoolean)
                            i = 1;
                        localParcel1.writeInt(i);
                        this.mRemote.transact(19, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Account[] getAccounts(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    Account[] arrayOfAccount = (Account[])localParcel2.createTypedArray(Account.CREATOR);
                    return arrayOfAccount;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getAccountsByFeatures(IAccountManagerResponse paramIAccountManagerResponse, String paramString, String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramIAccountManagerResponse != null)
                    {
                        localIBinder = paramIAccountManagerResponse.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString);
                        localParcel1.writeStringArray(paramArrayOfString);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getAuthToken(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, String paramString, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                        IBinder localIBinder;
                        if (paramIAccountManagerResponse != null)
                        {
                            localIBinder = paramIAccountManagerResponse.asBinder();
                            localParcel1.writeStrongBinder(localIBinder);
                            if (paramAccount != null)
                            {
                                localParcel1.writeInt(1);
                                paramAccount.writeToParcel(localParcel1, 0);
                                localParcel1.writeString(paramString);
                                if (!paramBoolean1)
                                    break label172;
                                j = i;
                                localParcel1.writeInt(j);
                                if (!paramBoolean2)
                                    break label178;
                                localParcel1.writeInt(i);
                                if (paramBundle == null)
                                    break label184;
                                localParcel1.writeInt(1);
                                paramBundle.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(16, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localIBinder = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label172: int j = 0;
                    continue;
                    label178: i = 0;
                    continue;
                    label184: localParcel1.writeInt(0);
                }
            }

            public void getAuthTokenLabel(IAccountManagerResponse paramIAccountManagerResponse, String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramIAccountManagerResponse != null)
                    {
                        localIBinder = paramIAccountManagerResponse.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString1);
                        localParcel1.writeString(paramString2);
                        this.mRemote.transact(21, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public AuthenticatorDescription[] getAuthenticatorTypes()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    AuthenticatorDescription[] arrayOfAuthenticatorDescription = (AuthenticatorDescription[])localParcel2.createTypedArray(AuthenticatorDescription.CREATOR);
                    return arrayOfAuthenticatorDescription;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.accounts.IAccountManager";
            }

            public String getPassword(Account paramAccount)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        String str = localParcel2.readString();
                        return str;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getUserData(Account paramAccount, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        String str = localParcel2.readString();
                        return str;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void hasFeatures(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    IBinder localIBinder;
                    if (paramIAccountManagerResponse != null)
                    {
                        localIBinder = paramIAccountManagerResponse.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        if (paramAccount == null)
                            break label97;
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeStringArray(paramArrayOfString);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localIBinder = null;
                        break;
                        label97: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void invalidateAuthToken(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String peekAuthToken(Account paramAccount, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(10, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        String str = localParcel2.readString();
                        return str;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeAccount(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    IBinder localIBinder;
                    if (paramIAccountManagerResponse != null)
                    {
                        localIBinder = paramIAccountManagerResponse.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        if (paramAccount == null)
                            break label85;
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(8, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localIBinder = null;
                        break;
                        label85: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAuthToken(Account paramAccount, String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString1);
                        localParcel1.writeString(paramString2);
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPassword(Account paramAccount, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(12, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setUserData(Account paramAccount, String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                    if (paramAccount != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccount.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString1);
                        localParcel1.writeString(paramString2);
                        this.mRemote.transact(14, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateAppPermission(Account paramAccount, String paramString, int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                        if (paramAccount != null)
                        {
                            localParcel1.writeInt(1);
                            paramAccount.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            localParcel1.writeInt(paramInt);
                            if (paramBoolean)
                            {
                                localParcel1.writeInt(i);
                                this.mRemote.transact(15, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    i = 0;
                }
            }

            public void updateCredentials(IAccountManagerResponse paramIAccountManagerResponse, Account paramAccount, String paramString, boolean paramBoolean, Bundle paramBundle)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.accounts.IAccountManager");
                        IBinder localIBinder;
                        if (paramIAccountManagerResponse != null)
                        {
                            localIBinder = paramIAccountManagerResponse.asBinder();
                            localParcel1.writeStrongBinder(localIBinder);
                            if (paramAccount != null)
                            {
                                localParcel1.writeInt(1);
                                paramAccount.writeToParcel(localParcel1, 0);
                                localParcel1.writeString(paramString);
                                if (!paramBoolean)
                                    break label156;
                                localParcel1.writeInt(i);
                                if (paramBundle == null)
                                    break label162;
                                localParcel1.writeInt(1);
                                paramBundle.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(18, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localIBinder = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label156: i = 0;
                    continue;
                    label162: localParcel1.writeInt(0);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.IAccountManager
 * JD-Core Version:        0.6.2
 */