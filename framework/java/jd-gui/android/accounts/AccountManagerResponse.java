package android.accounts;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public class AccountManagerResponse
    implements Parcelable
{
    public static final Parcelable.Creator<AccountManagerResponse> CREATOR = new Parcelable.Creator()
    {
        public AccountManagerResponse createFromParcel(Parcel paramAnonymousParcel)
        {
            return new AccountManagerResponse(paramAnonymousParcel);
        }

        public AccountManagerResponse[] newArray(int paramAnonymousInt)
        {
            return new AccountManagerResponse[paramAnonymousInt];
        }
    };
    private IAccountManagerResponse mResponse;

    public AccountManagerResponse(IAccountManagerResponse paramIAccountManagerResponse)
    {
        this.mResponse = paramIAccountManagerResponse;
    }

    public AccountManagerResponse(Parcel paramParcel)
    {
        this.mResponse = IAccountManagerResponse.Stub.asInterface(paramParcel.readStrongBinder());
    }

    public int describeContents()
    {
        return 0;
    }

    public void onError(int paramInt, String paramString)
    {
        try
        {
            this.mResponse.onError(paramInt, paramString);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void onResult(Bundle paramBundle)
    {
        try
        {
            this.mResponse.onResult(paramBundle);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStrongBinder(this.mResponse.asBinder());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AccountManagerResponse
 * JD-Core Version:        0.6.2
 */