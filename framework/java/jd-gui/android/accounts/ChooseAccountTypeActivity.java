package android.accounts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class ChooseAccountTypeActivity extends Activity
{
    private static final String TAG = "AccountChooser";
    private ArrayList<AuthInfo> mAuthenticatorInfosToDisplay;
    private HashMap<String, AuthInfo> mTypeToAuthenticatorInfo = new HashMap();

    private void buildTypeToAuthDescriptionMap()
    {
        AuthenticatorDescription[] arrayOfAuthenticatorDescription = AccountManager.get(this).getAuthenticatorTypes();
        int i = arrayOfAuthenticatorDescription.length;
        int j = 0;
        while (true)
            if (j < i)
            {
                AuthenticatorDescription localAuthenticatorDescription = arrayOfAuthenticatorDescription[j];
                Object localObject = null;
                Drawable localDrawable = null;
                try
                {
                    Context localContext = createPackageContext(localAuthenticatorDescription.packageName, 0);
                    localDrawable = localContext.getResources().getDrawable(localAuthenticatorDescription.iconId);
                    CharSequence localCharSequence = localContext.getResources().getText(localAuthenticatorDescription.labelId);
                    if (localCharSequence != null)
                        localObject = localCharSequence.toString();
                    String str = localCharSequence.toString();
                    localObject = str;
                    AuthInfo localAuthInfo = new AuthInfo(localAuthenticatorDescription, (String)localObject, localDrawable);
                    this.mTypeToAuthenticatorInfo.put(localAuthenticatorDescription.type, localAuthInfo);
                    j++;
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    while (true)
                        if (Log.isLoggable("AccountChooser", 5))
                            Log.w("AccountChooser", "No icon name for account type " + localAuthenticatorDescription.type);
                }
                catch (Resources.NotFoundException localNotFoundException)
                {
                    while (true)
                        if (Log.isLoggable("AccountChooser", 5))
                            Log.w("AccountChooser", "No icon resource for account type " + localAuthenticatorDescription.type);
                }
            }
    }

    private void setResultAndFinish(String paramString)
    {
        Bundle localBundle = new Bundle();
        localBundle.putString("accountType", paramString);
        setResult(-1, new Intent().putExtras(localBundle));
        if (Log.isLoggable("AccountChooser", 2))
            Log.v("AccountChooser", "ChooseAccountTypeActivity.setResultAndFinish: selected account type " + paramString);
        finish();
    }

    public void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        if (Log.isLoggable("AccountChooser", 2))
            Log.v("AccountChooser", "ChooseAccountTypeActivity.onCreate(savedInstanceState=" + paramBundle + ")");
        HashSet localHashSet = null;
        String[] arrayOfString = getIntent().getStringArrayExtra("allowableAccountTypes");
        if (arrayOfString != null)
        {
            localHashSet = new HashSet(arrayOfString.length);
            int i = arrayOfString.length;
            for (int j = 0; j < i; j++)
                localHashSet.add(arrayOfString[j]);
        }
        buildTypeToAuthDescriptionMap();
        this.mAuthenticatorInfosToDisplay = new ArrayList(this.mTypeToAuthenticatorInfo.size());
        Iterator localIterator = this.mTypeToAuthenticatorInfo.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            String str = (String)localEntry.getKey();
            AuthInfo localAuthInfo = (AuthInfo)localEntry.getValue();
            if ((localHashSet == null) || (localHashSet.contains(str)))
                this.mAuthenticatorInfosToDisplay.add(localAuthInfo);
        }
        if (this.mAuthenticatorInfosToDisplay.isEmpty())
        {
            Bundle localBundle = new Bundle();
            localBundle.putString("errorMessage", "no allowable account types");
            setResult(-1, new Intent().putExtras(localBundle));
            finish();
        }
        while (true)
        {
            return;
            if (this.mAuthenticatorInfosToDisplay.size() == 1)
            {
                setResultAndFinish(((AuthInfo)this.mAuthenticatorInfosToDisplay.get(0)).desc.type);
            }
            else
            {
                setContentView(17367091);
                ListView localListView = (ListView)findViewById(16908298);
                localListView.setAdapter(new AccountArrayAdapter(this, 17367043, this.mAuthenticatorInfosToDisplay));
                localListView.setChoiceMode(0);
                localListView.setTextFilterEnabled(false);
                localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
                    {
                        ChooseAccountTypeActivity.this.setResultAndFinish(((ChooseAccountTypeActivity.AuthInfo)ChooseAccountTypeActivity.this.mAuthenticatorInfosToDisplay.get(paramAnonymousInt)).desc.type);
                    }
                });
            }
        }
    }

    private static class AccountArrayAdapter extends ArrayAdapter<ChooseAccountTypeActivity.AuthInfo>
    {
        private ArrayList<ChooseAccountTypeActivity.AuthInfo> mInfos;
        private LayoutInflater mLayoutInflater;

        public AccountArrayAdapter(Context paramContext, int paramInt, ArrayList<ChooseAccountTypeActivity.AuthInfo> paramArrayList)
        {
            super(paramInt, paramArrayList);
            this.mInfos = paramArrayList;
            this.mLayoutInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            ChooseAccountTypeActivity.ViewHolder localViewHolder;
            if (paramView == null)
            {
                paramView = this.mLayoutInflater.inflate(17367090, null);
                localViewHolder = new ChooseAccountTypeActivity.ViewHolder(null);
                localViewHolder.text = ((TextView)paramView.findViewById(16908907));
                localViewHolder.icon = ((ImageView)paramView.findViewById(16908906));
                paramView.setTag(localViewHolder);
            }
            while (true)
            {
                localViewHolder.text.setText(((ChooseAccountTypeActivity.AuthInfo)this.mInfos.get(paramInt)).name);
                localViewHolder.icon.setImageDrawable(((ChooseAccountTypeActivity.AuthInfo)this.mInfos.get(paramInt)).drawable);
                return paramView;
                localViewHolder = (ChooseAccountTypeActivity.ViewHolder)paramView.getTag();
            }
        }
    }

    private static class ViewHolder
    {
        ImageView icon;
        TextView text;
    }

    private static class AuthInfo
    {
        final AuthenticatorDescription desc;
        final Drawable drawable;
        final String name;

        AuthInfo(AuthenticatorDescription paramAuthenticatorDescription, String paramString, Drawable paramDrawable)
        {
            this.desc = paramAuthenticatorDescription;
            this.name = paramString;
            this.drawable = paramDrawable;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.ChooseAccountTypeActivity
 * JD-Core Version:        0.6.2
 */