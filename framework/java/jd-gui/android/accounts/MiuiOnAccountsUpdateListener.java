package android.accounts;

import android.os.Bundle;

public abstract interface MiuiOnAccountsUpdateListener extends OnAccountsUpdateListener
{
    public abstract void onPostAccountUpdated(Account paramAccount, int paramInt, Bundle paramBundle);

    public abstract void onPreAccountUpdated(Account paramAccount, int paramInt, Bundle paramBundle);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.MiuiOnAccountsUpdateListener
 * JD-Core Version:        0.6.2
 */