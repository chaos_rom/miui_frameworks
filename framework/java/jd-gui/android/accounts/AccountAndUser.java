package android.accounts;

public class AccountAndUser
{
    public Account account;
    public int userId;

    public AccountAndUser(Account paramAccount, int paramInt)
    {
        this.account = paramAccount;
        this.userId = paramInt;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof AccountAndUser))
            {
                bool = false;
            }
            else
            {
                AccountAndUser localAccountAndUser = (AccountAndUser)paramObject;
                if ((!this.account.equals(localAccountAndUser.account)) || (this.userId != localAccountAndUser.userId))
                    bool = false;
            }
        }
    }

    public int hashCode()
    {
        return this.account.hashCode() + this.userId;
    }

    public String toString()
    {
        return this.account.toString() + " u" + this.userId;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AccountAndUser
 * JD-Core Version:        0.6.2
 */