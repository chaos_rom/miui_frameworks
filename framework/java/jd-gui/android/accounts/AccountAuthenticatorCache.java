package android.accounts;

import android.content.Context;
import android.content.pm.RegisteredServicesCache;
import android.content.pm.XmlSerializerAndParser;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class AccountAuthenticatorCache extends RegisteredServicesCache<AuthenticatorDescription>
    implements IAccountAuthenticatorCache
{
    private static final String TAG = "Account";
    private static final MySerializer sSerializer = new MySerializer(null);

    public AccountAuthenticatorCache(Context paramContext)
    {
        super(paramContext, "android.accounts.AccountAuthenticator", "android.accounts.AccountAuthenticator", "account-authenticator", sSerializer);
    }

    public AuthenticatorDescription parseServiceAttributes(Resources paramResources, String paramString, AttributeSet paramAttributeSet)
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AccountAuthenticator);
        try
        {
            String str = localTypedArray.getString(2);
            int i = localTypedArray.getResourceId(0, 0);
            int j = localTypedArray.getResourceId(1, 0);
            int k = localTypedArray.getResourceId(3, 0);
            int m = localTypedArray.getResourceId(4, 0);
            boolean bool1 = localTypedArray.getBoolean(5, false);
            boolean bool2 = TextUtils.isEmpty(str);
            if (bool2);
            for (AuthenticatorDescription localAuthenticatorDescription = null; ; localAuthenticatorDescription = new AuthenticatorDescription(str, paramString, i, j, k, m, bool1))
                return localAuthenticatorDescription;
        }
        finally
        {
            localTypedArray.recycle();
        }
    }

    private static class MySerializer
        implements XmlSerializerAndParser<AuthenticatorDescription>
    {
        public AuthenticatorDescription createFromXml(XmlPullParser paramXmlPullParser)
            throws IOException, XmlPullParserException
        {
            return AuthenticatorDescription.newKey(paramXmlPullParser.getAttributeValue(null, "type"));
        }

        public void writeAsXml(AuthenticatorDescription paramAuthenticatorDescription, XmlSerializer paramXmlSerializer)
            throws IOException
        {
            paramXmlSerializer.attribute(null, "type", paramAuthenticatorDescription.type);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AccountAuthenticatorCache
 * JD-Core Version:        0.6.2
 */