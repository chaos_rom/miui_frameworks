package android.accounts;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IAccountAuthenticator extends IInterface
{
    public abstract void addAccount(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString1, String paramString2, String[] paramArrayOfString, Bundle paramBundle)
        throws RemoteException;

    public abstract void confirmCredentials(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, Bundle paramBundle)
        throws RemoteException;

    public abstract void editProperties(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString)
        throws RemoteException;

    public abstract void getAccountRemovalAllowed(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount)
        throws RemoteException;

    public abstract void getAuthToken(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
        throws RemoteException;

    public abstract void getAuthTokenLabel(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString)
        throws RemoteException;

    public abstract void hasFeatures(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String[] paramArrayOfString)
        throws RemoteException;

    public abstract void updateCredentials(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccountAuthenticator
    {
        private static final String DESCRIPTOR = "android.accounts.IAccountAuthenticator";
        static final int TRANSACTION_addAccount = 1;
        static final int TRANSACTION_confirmCredentials = 2;
        static final int TRANSACTION_editProperties = 6;
        static final int TRANSACTION_getAccountRemovalAllowed = 8;
        static final int TRANSACTION_getAuthToken = 3;
        static final int TRANSACTION_getAuthTokenLabel = 4;
        static final int TRANSACTION_hasFeatures = 7;
        static final int TRANSACTION_updateCredentials = 5;

        public Stub()
        {
            attachInterface(this, "android.accounts.IAccountAuthenticator");
        }

        public static IAccountAuthenticator asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.accounts.IAccountAuthenticator");
                if ((localIInterface != null) && ((localIInterface instanceof IAccountAuthenticator)))
                    localObject = (IAccountAuthenticator)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.accounts.IAccountAuthenticator");
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.accounts.IAccountAuthenticator");
                    IAccountAuthenticatorResponse localIAccountAuthenticatorResponse6 = IAccountAuthenticatorResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                    String str3 = paramParcel1.readString();
                    String str4 = paramParcel1.readString();
                    String[] arrayOfString = paramParcel1.createStringArray();
                    if (paramParcel1.readInt() != 0);
                    for (Bundle localBundle4 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle4 = null)
                    {
                        addAccount(localIAccountAuthenticatorResponse6, str3, str4, arrayOfString, localBundle4);
                        bool = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.accounts.IAccountAuthenticator");
                    IAccountAuthenticatorResponse localIAccountAuthenticatorResponse5 = IAccountAuthenticatorResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                    Account localAccount5;
                    if (paramParcel1.readInt() != 0)
                    {
                        localAccount5 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label269;
                    }
                    for (Bundle localBundle3 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle3 = null)
                    {
                        confirmCredentials(localIAccountAuthenticatorResponse5, localAccount5, localBundle3);
                        bool = true;
                        break;
                        localAccount5 = null;
                        break label226;
                    }
                    paramParcel1.enforceInterface("android.accounts.IAccountAuthenticator");
                    IAccountAuthenticatorResponse localIAccountAuthenticatorResponse4 = IAccountAuthenticatorResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                    Account localAccount4;
                    String str2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localAccount4 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                        str2 = paramParcel1.readString();
                        if (paramParcel1.readInt() == 0)
                            break label362;
                    }
                    for (Bundle localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle2 = null)
                    {
                        getAuthToken(localIAccountAuthenticatorResponse4, localAccount4, str2, localBundle2);
                        bool = true;
                        break;
                        localAccount4 = null;
                        break label311;
                    }
                    paramParcel1.enforceInterface("android.accounts.IAccountAuthenticator");
                    getAuthTokenLabel(IAccountAuthenticatorResponse.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                    bool = true;
                    continue;
                    paramParcel1.enforceInterface("android.accounts.IAccountAuthenticator");
                    IAccountAuthenticatorResponse localIAccountAuthenticatorResponse3 = IAccountAuthenticatorResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                    Account localAccount3;
                    String str1;
                    if (paramParcel1.readInt() != 0)
                    {
                        localAccount3 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                        str1 = paramParcel1.readString();
                        if (paramParcel1.readInt() == 0)
                            break label482;
                    }
                    for (Bundle localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle1 = null)
                    {
                        updateCredentials(localIAccountAuthenticatorResponse3, localAccount3, str1, localBundle1);
                        bool = true;
                        break;
                        localAccount3 = null;
                        break label431;
                    }
                    paramParcel1.enforceInterface("android.accounts.IAccountAuthenticator");
                    editProperties(IAccountAuthenticatorResponse.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                    bool = true;
                }
            case 7:
                label226: label362: paramParcel1.enforceInterface("android.accounts.IAccountAuthenticator");
                label269: label311: label482: IAccountAuthenticatorResponse localIAccountAuthenticatorResponse2 = IAccountAuthenticatorResponse.Stub.asInterface(paramParcel1.readStrongBinder());
                label431: if (paramParcel1.readInt() != 0);
                for (Account localAccount2 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount2 = null)
                {
                    hasFeatures(localIAccountAuthenticatorResponse2, localAccount2, paramParcel1.createStringArray());
                    bool = true;
                    break;
                }
            case 8:
            }
            paramParcel1.enforceInterface("android.accounts.IAccountAuthenticator");
            IAccountAuthenticatorResponse localIAccountAuthenticatorResponse1 = IAccountAuthenticatorResponse.Stub.asInterface(paramParcel1.readStrongBinder());
            if (paramParcel1.readInt() != 0);
            for (Account localAccount1 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount1 = null)
            {
                getAccountRemovalAllowed(localIAccountAuthenticatorResponse1, localAccount1);
                bool = true;
                break;
            }
        }

        private static class Proxy
            implements IAccountAuthenticator
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addAccount(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString1, String paramString2, String[] paramArrayOfString, Bundle paramBundle)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    if (paramIAccountAuthenticatorResponse != null)
                        localIBinder = paramIAccountAuthenticatorResponse.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    localParcel.writeString(paramString1);
                    localParcel.writeString(paramString2);
                    localParcel.writeStringArray(paramArrayOfString);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void confirmCredentials(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, Bundle paramBundle)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                        if (paramIAccountAuthenticatorResponse != null)
                            localIBinder = paramIAccountAuthenticatorResponse.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        if (paramAccount != null)
                        {
                            localParcel.writeInt(1);
                            paramAccount.writeToParcel(localParcel, 0);
                            if (paramBundle != null)
                            {
                                localParcel.writeInt(1);
                                paramBundle.writeToParcel(localParcel, 0);
                                this.mRemote.transact(2, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }

            public void editProperties(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    if (paramIAccountAuthenticatorResponse != null)
                        localIBinder = paramIAccountAuthenticatorResponse.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    localParcel.writeString(paramString);
                    this.mRemote.transact(6, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void getAccountRemovalAllowed(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    if (paramIAccountAuthenticatorResponse != null)
                        localIBinder = paramIAccountAuthenticatorResponse.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    if (paramAccount != null)
                    {
                        localParcel.writeInt(1);
                        paramAccount.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(8, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void getAuthToken(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                        if (paramIAccountAuthenticatorResponse != null)
                            localIBinder = paramIAccountAuthenticatorResponse.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        if (paramAccount != null)
                        {
                            localParcel.writeInt(1);
                            paramAccount.writeToParcel(localParcel, 0);
                            localParcel.writeString(paramString);
                            if (paramBundle != null)
                            {
                                localParcel.writeInt(1);
                                paramBundle.writeToParcel(localParcel, 0);
                                this.mRemote.transact(3, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }

            public void getAuthTokenLabel(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    if (paramIAccountAuthenticatorResponse != null)
                        localIBinder = paramIAccountAuthenticatorResponse.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    localParcel.writeString(paramString);
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.accounts.IAccountAuthenticator";
            }

            public void hasFeatures(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String[] paramArrayOfString)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    if (paramIAccountAuthenticatorResponse != null)
                        localIBinder = paramIAccountAuthenticatorResponse.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    if (paramAccount != null)
                    {
                        localParcel.writeInt(1);
                        paramAccount.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeStringArray(paramArrayOfString);
                        this.mRemote.transact(7, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void updateCredentials(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                        if (paramIAccountAuthenticatorResponse != null)
                            localIBinder = paramIAccountAuthenticatorResponse.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        if (paramAccount != null)
                        {
                            localParcel.writeInt(1);
                            paramAccount.writeToParcel(localParcel, 0);
                            localParcel.writeString(paramString);
                            if (paramBundle != null)
                            {
                                localParcel.writeInt(1);
                                paramBundle.writeToParcel(localParcel, 0);
                                this.mRemote.transact(5, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.IAccountAuthenticator
 * JD-Core Version:        0.6.2
 */