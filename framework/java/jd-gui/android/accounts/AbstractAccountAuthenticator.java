package android.accounts;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.Arrays;

public abstract class AbstractAccountAuthenticator
{
    private static final String TAG = "AccountAuthenticator";
    private final Context mContext;
    private Transport mTransport = new Transport(null);

    public AbstractAccountAuthenticator(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private void checkBinderPermission()
    {
        int i = Binder.getCallingUid();
        if (this.mContext.checkCallingOrSelfPermission("android.permission.ACCOUNT_MANAGER") != 0)
            throw new SecurityException("caller uid " + i + " lacks " + "android.permission.ACCOUNT_MANAGER");
    }

    private void handleException(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString1, String paramString2, Exception paramException)
        throws RemoteException
    {
        if ((paramException instanceof NetworkErrorException))
        {
            if (Log.isLoggable("AccountAuthenticator", 2))
                Log.v("AccountAuthenticator", paramString1 + "(" + paramString2 + ")", paramException);
            paramIAccountAuthenticatorResponse.onError(3, paramException.getMessage());
        }
        while (true)
        {
            return;
            if ((paramException instanceof UnsupportedOperationException))
            {
                if (Log.isLoggable("AccountAuthenticator", 2))
                    Log.v("AccountAuthenticator", paramString1 + "(" + paramString2 + ")", paramException);
                paramIAccountAuthenticatorResponse.onError(6, paramString1 + " not supported");
            }
            else if ((paramException instanceof IllegalArgumentException))
            {
                if (Log.isLoggable("AccountAuthenticator", 2))
                    Log.v("AccountAuthenticator", paramString1 + "(" + paramString2 + ")", paramException);
                paramIAccountAuthenticatorResponse.onError(7, paramString1 + " not supported");
            }
            else
            {
                Log.w("AccountAuthenticator", paramString1 + "(" + paramString2 + ")", paramException);
                paramIAccountAuthenticatorResponse.onError(1, paramString1 + " failed");
            }
        }
    }

    public abstract Bundle addAccount(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString1, String paramString2, String[] paramArrayOfString, Bundle paramBundle)
        throws NetworkErrorException;

    public abstract Bundle confirmCredentials(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, Bundle paramBundle)
        throws NetworkErrorException;

    public abstract Bundle editProperties(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString);

    public Bundle getAccountRemovalAllowed(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount)
        throws NetworkErrorException
    {
        Bundle localBundle = new Bundle();
        localBundle.putBoolean("booleanResult", true);
        return localBundle;
    }

    public abstract Bundle getAuthToken(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
        throws NetworkErrorException;

    public abstract String getAuthTokenLabel(String paramString);

    public final IBinder getIBinder()
    {
        return this.mTransport.asBinder();
    }

    public abstract Bundle hasFeatures(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String[] paramArrayOfString)
        throws NetworkErrorException;

    public abstract Bundle updateCredentials(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
        throws NetworkErrorException;

    private class Transport extends IAccountAuthenticator.Stub
    {
        private Transport()
        {
        }

        public void addAccount(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString1, String paramString2, String[] paramArrayOfString, Bundle paramBundle)
            throws RemoteException
        {
            StringBuilder localStringBuilder;
            String str;
            if (Log.isLoggable("AccountAuthenticator", 2))
            {
                localStringBuilder = new StringBuilder().append("addAccount: accountType ").append(paramString1).append(", authTokenType ").append(paramString2).append(", features ");
                if (paramArrayOfString != null)
                    break label154;
                str = "[]";
            }
            while (true)
            {
                Log.v("AccountAuthenticator", str);
                AbstractAccountAuthenticator.this.checkBinderPermission();
                try
                {
                    Bundle localBundle = AbstractAccountAuthenticator.this.addAccount(new AccountAuthenticatorResponse(paramIAccountAuthenticatorResponse), paramString1, paramString2, paramArrayOfString, paramBundle);
                    if (Log.isLoggable("AccountAuthenticator", 2))
                    {
                        localBundle.keySet();
                        Log.v("AccountAuthenticator", "addAccount: result " + AccountManager.sanitizeResult(localBundle));
                    }
                    if (localBundle != null)
                        paramIAccountAuthenticatorResponse.onResult(localBundle);
                    return;
                    label154: str = Arrays.toString(paramArrayOfString);
                }
                catch (Exception localException)
                {
                    while (true)
                        AbstractAccountAuthenticator.this.handleException(paramIAccountAuthenticatorResponse, "addAccount", paramString1, localException);
                }
            }
        }

        public void confirmCredentials(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, Bundle paramBundle)
            throws RemoteException
        {
            if (Log.isLoggable("AccountAuthenticator", 2))
                Log.v("AccountAuthenticator", "confirmCredentials: " + paramAccount);
            AbstractAccountAuthenticator.this.checkBinderPermission();
            try
            {
                Bundle localBundle = AbstractAccountAuthenticator.this.confirmCredentials(new AccountAuthenticatorResponse(paramIAccountAuthenticatorResponse), paramAccount, paramBundle);
                if (Log.isLoggable("AccountAuthenticator", 2))
                {
                    localBundle.keySet();
                    Log.v("AccountAuthenticator", "confirmCredentials: result " + AccountManager.sanitizeResult(localBundle));
                }
                if (localBundle != null)
                    paramIAccountAuthenticatorResponse.onResult(localBundle);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    AbstractAccountAuthenticator.this.handleException(paramIAccountAuthenticatorResponse, "confirmCredentials", paramAccount.toString(), localException);
            }
        }

        public void editProperties(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString)
            throws RemoteException
        {
            AbstractAccountAuthenticator.this.checkBinderPermission();
            try
            {
                Bundle localBundle = AbstractAccountAuthenticator.this.editProperties(new AccountAuthenticatorResponse(paramIAccountAuthenticatorResponse), paramString);
                if (localBundle != null)
                    paramIAccountAuthenticatorResponse.onResult(localBundle);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    AbstractAccountAuthenticator.this.handleException(paramIAccountAuthenticatorResponse, "editProperties", paramString, localException);
            }
        }

        public void getAccountRemovalAllowed(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount)
            throws RemoteException
        {
            AbstractAccountAuthenticator.this.checkBinderPermission();
            try
            {
                Bundle localBundle = AbstractAccountAuthenticator.this.getAccountRemovalAllowed(new AccountAuthenticatorResponse(paramIAccountAuthenticatorResponse), paramAccount);
                if (localBundle != null)
                    paramIAccountAuthenticatorResponse.onResult(localBundle);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    AbstractAccountAuthenticator.this.handleException(paramIAccountAuthenticatorResponse, "getAccountRemovalAllowed", paramAccount.toString(), localException);
            }
        }

        public void getAuthToken(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
            throws RemoteException
        {
            if (Log.isLoggable("AccountAuthenticator", 2))
                Log.v("AccountAuthenticator", "getAuthToken: " + paramAccount + ", authTokenType " + paramString);
            AbstractAccountAuthenticator.this.checkBinderPermission();
            try
            {
                Bundle localBundle = AbstractAccountAuthenticator.this.getAuthToken(new AccountAuthenticatorResponse(paramIAccountAuthenticatorResponse), paramAccount, paramString, paramBundle);
                if (Log.isLoggable("AccountAuthenticator", 2))
                {
                    localBundle.keySet();
                    Log.v("AccountAuthenticator", "getAuthToken: result " + AccountManager.sanitizeResult(localBundle));
                }
                if (localBundle != null)
                    paramIAccountAuthenticatorResponse.onResult(localBundle);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    AbstractAccountAuthenticator.this.handleException(paramIAccountAuthenticatorResponse, "getAuthToken", paramAccount.toString() + "," + paramString, localException);
            }
        }

        public void getAuthTokenLabel(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, String paramString)
            throws RemoteException
        {
            if (Log.isLoggable("AccountAuthenticator", 2))
                Log.v("AccountAuthenticator", "getAuthTokenLabel: authTokenType " + paramString);
            AbstractAccountAuthenticator.this.checkBinderPermission();
            try
            {
                Bundle localBundle = new Bundle();
                localBundle.putString("authTokenLabelKey", AbstractAccountAuthenticator.this.getAuthTokenLabel(paramString));
                if (Log.isLoggable("AccountAuthenticator", 2))
                {
                    localBundle.keySet();
                    Log.v("AccountAuthenticator", "getAuthTokenLabel: result " + AccountManager.sanitizeResult(localBundle));
                }
                paramIAccountAuthenticatorResponse.onResult(localBundle);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    AbstractAccountAuthenticator.this.handleException(paramIAccountAuthenticatorResponse, "getAuthTokenLabel", paramString, localException);
            }
        }

        public void hasFeatures(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String[] paramArrayOfString)
            throws RemoteException
        {
            AbstractAccountAuthenticator.this.checkBinderPermission();
            try
            {
                Bundle localBundle = AbstractAccountAuthenticator.this.hasFeatures(new AccountAuthenticatorResponse(paramIAccountAuthenticatorResponse), paramAccount, paramArrayOfString);
                if (localBundle != null)
                    paramIAccountAuthenticatorResponse.onResult(localBundle);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    AbstractAccountAuthenticator.this.handleException(paramIAccountAuthenticatorResponse, "hasFeatures", paramAccount.toString(), localException);
            }
        }

        public void updateCredentials(IAccountAuthenticatorResponse paramIAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
            throws RemoteException
        {
            if (Log.isLoggable("AccountAuthenticator", 2))
                Log.v("AccountAuthenticator", "updateCredentials: " + paramAccount + ", authTokenType " + paramString);
            AbstractAccountAuthenticator.this.checkBinderPermission();
            try
            {
                Bundle localBundle = AbstractAccountAuthenticator.this.updateCredentials(new AccountAuthenticatorResponse(paramIAccountAuthenticatorResponse), paramAccount, paramString, paramBundle);
                if (Log.isLoggable("AccountAuthenticator", 2))
                {
                    localBundle.keySet();
                    Log.v("AccountAuthenticator", "updateCredentials: result " + AccountManager.sanitizeResult(localBundle));
                }
                if (localBundle != null)
                    paramIAccountAuthenticatorResponse.onResult(localBundle);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    AbstractAccountAuthenticator.this.handleException(paramIAccountAuthenticatorResponse, "updateCredentials", paramAccount.toString() + "," + paramString, localException);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AbstractAccountAuthenticator
 * JD-Core Version:        0.6.2
 */