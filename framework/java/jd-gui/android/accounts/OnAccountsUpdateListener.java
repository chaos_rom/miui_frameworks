package android.accounts;

public abstract interface OnAccountsUpdateListener
{
    public abstract void onAccountsUpdated(Account[] paramArrayOfAccount);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.OnAccountsUpdateListener
 * JD-Core Version:        0.6.2
 */