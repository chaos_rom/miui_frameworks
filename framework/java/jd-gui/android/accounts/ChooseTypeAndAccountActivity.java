package android.accounts;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ChooseTypeAndAccountActivity extends Activity
    implements AccountManagerCallback<Bundle>
{
    public static final String EXTRA_ADD_ACCOUNT_AUTH_TOKEN_TYPE_STRING = "authTokenType";
    public static final String EXTRA_ADD_ACCOUNT_OPTIONS_BUNDLE = "addAccountOptions";
    public static final String EXTRA_ADD_ACCOUNT_REQUIRED_FEATURES_STRING_ARRAY = "addAccountRequiredFeatures";
    public static final String EXTRA_ALLOWABLE_ACCOUNTS_ARRAYLIST = "allowableAccounts";
    public static final String EXTRA_ALLOWABLE_ACCOUNT_TYPES_STRING_ARRAY = "allowableAccountTypes";
    public static final String EXTRA_ALWAYS_PROMPT_FOR_ACCOUNT = "alwaysPromptForAccount";
    public static final String EXTRA_DESCRIPTION_TEXT_OVERRIDE = "descriptionTextOverride";
    public static final String EXTRA_SELECTED_ACCOUNT = "selectedAccount";
    private static final String KEY_INSTANCE_STATE_EXISTING_ACCOUNTS = "existingAccounts";
    private static final String KEY_INSTANCE_STATE_PENDING_REQUEST = "pendingRequest";
    private static final String KEY_INSTANCE_STATE_SELECTED_ACCOUNT_NAME = "selectedAccountName";
    private static final String KEY_INSTANCE_STATE_SELECTED_ADD_ACCOUNT = "selectedAddAccount";
    public static final int REQUEST_ADD_ACCOUNT = 2;
    public static final int REQUEST_CHOOSE_TYPE = 1;
    public static final int REQUEST_NULL = 0;
    private static final int SELECTED_ITEM_NONE = -1;
    private static final String TAG = "AccountChooser";
    private ArrayList<Account> mAccounts;
    private Parcelable[] mExistingAccounts = null;
    private Button mOkButton;
    private int mPendingRequest = 0;
    private int mSelectedItemIndex;

    private void onAccountSelected(Account paramAccount)
    {
        Log.d("AccountChooser", "selected account " + paramAccount);
        setResultAndFinish(paramAccount.name, paramAccount.type);
    }

    private void setResultAndFinish(String paramString1, String paramString2)
    {
        Bundle localBundle = new Bundle();
        localBundle.putString("authAccount", paramString1);
        localBundle.putString("accountType", paramString2);
        setResult(-1, new Intent().putExtras(localBundle));
        if (Log.isLoggable("AccountChooser", 2))
            Log.v("AccountChooser", "ChooseTypeAndAccountActivity.setResultAndFinish: selected account " + paramString1 + ", " + paramString2);
        finish();
    }

    private void startChooseAccountTypeActivity()
    {
        if (Log.isLoggable("AccountChooser", 2))
            Log.v("AccountChooser", "ChooseAccountTypeActivity.startChooseAccountTypeActivity()");
        Intent localIntent = new Intent(this, ChooseAccountTypeActivity.class);
        localIntent.setFlags(524288);
        localIntent.putExtra("allowableAccountTypes", getIntent().getStringArrayExtra("allowableAccountTypes"));
        localIntent.putExtra("addAccountOptions", getIntent().getBundleExtra("addAccountOptions"));
        localIntent.putExtra("addAccountRequiredFeatures", getIntent().getStringArrayExtra("addAccountRequiredFeatures"));
        localIntent.putExtra("authTokenType", getIntent().getStringExtra("authTokenType"));
        startActivityForResult(localIntent, 1);
        this.mPendingRequest = 1;
    }

    protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
    {
        Bundle localBundle;
        if (Log.isLoggable("AccountChooser", 2))
        {
            if ((paramIntent != null) && (paramIntent.getExtras() != null))
                paramIntent.getExtras().keySet();
            if (paramIntent != null)
            {
                localBundle = paramIntent.getExtras();
                Log.v("AccountChooser", "ChooseTypeAndAccountActivity.onActivityResult(reqCode=" + paramInt1 + ", resCode=" + paramInt2 + ", extras=" + localBundle + ")");
            }
        }
        else
        {
            this.mPendingRequest = 0;
            if (paramInt2 != 0)
                break label122;
            if (this.mAccounts.isEmpty())
            {
                setResult(0);
                finish();
            }
        }
        while (true)
        {
            return;
            localBundle = null;
            break;
            label122: if (paramInt2 != -1)
                break label175;
            if (paramInt1 != 1)
                break label204;
            if (paramIntent == null)
                break label159;
            str3 = paramIntent.getStringExtra("accountType");
            if (str3 == null)
                break label159;
            runAddAccountForAuthenticator(str3);
        }
        label159: Log.d("AccountChooser", "ChooseTypeAndAccountActivity.onActivityResult: unable to find account type, pretending the request was canceled");
        label167: label175: label204: 
        while (paramInt1 != 2)
        {
            String str3;
            Log.d("AccountChooser", "ChooseTypeAndAccountActivity.onActivityResult: unable to find added account, pretending the request was canceled");
            if (Log.isLoggable("AccountChooser", 2))
                Log.v("AccountChooser", "ChooseTypeAndAccountActivity.onActivityResult: canceled");
            setResult(0);
            finish();
            break;
        }
        String str1 = null;
        String str2 = null;
        if (paramIntent != null)
        {
            str1 = paramIntent.getStringExtra("authAccount");
            str2 = paramIntent.getStringExtra("accountType");
        }
        Account[] arrayOfAccount;
        HashSet localHashSet;
        int k;
        if ((str1 == null) || (str2 == null))
        {
            arrayOfAccount = AccountManager.get(this).getAccounts();
            localHashSet = new HashSet();
            Parcelable[] arrayOfParcelable = this.mExistingAccounts;
            int i = arrayOfParcelable.length;
            for (int j = 0; j < i; j++)
                localHashSet.add((Account)arrayOfParcelable[j]);
            k = arrayOfAccount.length;
        }
        for (int m = 0; ; m++)
            if (m < k)
            {
                Account localAccount = arrayOfAccount[m];
                if (!localHashSet.contains(localAccount))
                {
                    str1 = localAccount.name;
                    str2 = localAccount.type;
                }
            }
            else
            {
                if ((str1 == null) && (str2 == null))
                    break label167;
                setResultAndFinish(str1, str2);
                break;
            }
    }

    public void onCancelButtonClicked(View paramView)
    {
        onBackPressed();
    }

    public void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        if (Log.isLoggable("AccountChooser", 2))
            Log.v("AccountChooser", "ChooseTypeAndAccountActivity.onCreate(savedInstanceState=" + paramBundle + ")");
        AccountManager localAccountManager = AccountManager.get(this);
        Intent localIntent = getIntent();
        String str1 = null;
        boolean bool1 = false;
        if (paramBundle != null)
        {
            this.mPendingRequest = paramBundle.getInt("pendingRequest");
            this.mExistingAccounts = paramBundle.getParcelableArray("existingAccounts");
            str1 = paramBundle.getString("selectedAccountName");
            bool1 = paramBundle.getBoolean("selectedAddAccount", false);
        }
        while (true)
        {
            if (Log.isLoggable("AccountChooser", 2))
                Log.v("AccountChooser", "selected account name is " + str1);
            HashMap localHashMap = new HashMap();
            for (AuthenticatorDescription localAuthenticatorDescription : localAccountManager.getAuthenticatorTypes())
                localHashMap.put(localAuthenticatorDescription.type, localAuthenticatorDescription);
            this.mPendingRequest = 0;
            this.mExistingAccounts = null;
            Account localAccount1 = (Account)localIntent.getParcelableExtra("selectedAccount");
            if (localAccount1 != null)
                str1 = localAccount1.name;
        }
        HashSet localHashSet1 = null;
        ArrayList localArrayList = localIntent.getParcelableArrayListExtra("allowableAccounts");
        if (localArrayList != null)
        {
            int i6 = localArrayList.size();
            localHashSet1 = new HashSet(i6);
            Iterator localIterator2 = localArrayList.iterator();
            while (localIterator2.hasNext())
            {
                Account localAccount4 = (Account)localIterator2.next();
                localHashSet1.add(localAccount4);
            }
        }
        HashSet localHashSet2 = null;
        String[] arrayOfString1 = localIntent.getStringArrayExtra("allowableAccountTypes");
        if (arrayOfString1 != null)
        {
            int i1 = arrayOfString1.length;
            localHashSet2 = new HashSet(i1);
            HashSet localHashSet3 = new HashSet(arrayOfString1.length);
            int i2 = arrayOfString1.length;
            for (int i3 = 0; i3 < i2; i3++)
                localHashSet3.add(arrayOfString1[i3]);
            AuthenticatorDescription[] arrayOfAuthenticatorDescription2 = AccountManager.get(this).getAuthenticatorTypes();
            HashSet localHashSet4 = new HashSet(arrayOfAuthenticatorDescription2.length);
            int i4 = arrayOfAuthenticatorDescription2.length;
            for (int i5 = 0; i5 < i4; i5++)
                localHashSet4.add(arrayOfAuthenticatorDescription2[i5].type);
            Iterator localIterator1 = localHashSet3.iterator();
            while (localIterator1.hasNext())
            {
                String str4 = (String)localIterator1.next();
                if (localHashSet4.contains(str4))
                    localHashSet2.add(str4);
            }
        }
        Account[] arrayOfAccount = localAccountManager.getAccounts();
        this.mAccounts = new ArrayList(arrayOfAccount.length);
        this.mSelectedItemIndex = -1;
        int k = arrayOfAccount.length;
        int m = 0;
        if (m < k)
        {
            Account localAccount3 = arrayOfAccount[m];
            if ((localHashSet1 != null) && (!localHashSet1.contains(localAccount3)));
            while (true)
            {
                m++;
                break;
                if (localHashSet2 != null)
                {
                    String str3 = localAccount3.type;
                    if (!localHashSet2.contains(str3));
                }
                else
                {
                    if (localAccount3.name.equals(str1))
                        this.mSelectedItemIndex = this.mAccounts.size();
                    this.mAccounts.add(localAccount3);
                }
            }
        }
        if (this.mPendingRequest == 0)
        {
            if (this.mAccounts.isEmpty())
                if (localHashSet2.size() == 1)
                    runAddAccountForAuthenticator((String)localHashSet2.iterator().next());
            while (true)
            {
                return;
                startChooseAccountTypeActivity();
                continue;
                if ((localIntent.getBooleanExtra("alwaysPromptForAccount", false)) || (this.mAccounts.size() != 1))
                    break;
                Account localAccount2 = (Account)this.mAccounts.get(0);
                setResultAndFinish(localAccount2.name, localAccount2.type);
            }
        }
        setContentView(17367092);
        String str2 = localIntent.getStringExtra("descriptionTextOverride");
        TextView localTextView = (TextView)findViewById(16908908);
        if (!TextUtils.isEmpty(str2))
            localTextView.setText(str2);
        String[] arrayOfString2;
        while (true)
        {
            arrayOfString2 = new String[1 + this.mAccounts.size()];
            for (int n = 0; n < this.mAccounts.size(); n++)
                arrayOfString2[n] = ((Account)this.mAccounts.get(n)).name;
            localTextView.setVisibility(8);
        }
        arrayOfString2[this.mAccounts.size()] = getResources().getString(17040552);
        ListView localListView = (ListView)findViewById(16908298);
        ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 17367055, arrayOfString2);
        localListView.setAdapter(localArrayAdapter);
        localListView.setChoiceMode(1);
        localListView.setItemsCanFocus(false);
        AdapterView.OnItemClickListener local1 = new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
            {
                ChooseTypeAndAccountActivity.access$002(ChooseTypeAndAccountActivity.this, paramAnonymousInt);
                ChooseTypeAndAccountActivity.this.mOkButton.setEnabled(true);
            }
        };
        localListView.setOnItemClickListener(local1);
        if (bool1)
            this.mSelectedItemIndex = this.mAccounts.size();
        if (this.mSelectedItemIndex != -1)
        {
            localListView.setItemChecked(this.mSelectedItemIndex, true);
            if (Log.isLoggable("AccountChooser", 2))
                Log.v("AccountChooser", "List item " + this.mSelectedItemIndex + " should be selected");
        }
        this.mOkButton = ((Button)findViewById(16908314));
        Button localButton = this.mOkButton;
        if (this.mSelectedItemIndex != -1);
        for (boolean bool2 = true; ; bool2 = false)
        {
            localButton.setEnabled(bool2);
            break;
        }
    }

    protected void onDestroy()
    {
        if (Log.isLoggable("AccountChooser", 2))
            Log.v("AccountChooser", "ChooseTypeAndAccountActivity.onDestroy()");
        super.onDestroy();
    }

    public void onOkButtonClicked(View paramView)
    {
        if (this.mSelectedItemIndex == this.mAccounts.size())
            startChooseAccountTypeActivity();
        while (true)
        {
            return;
            if (this.mSelectedItemIndex != -1)
                onAccountSelected((Account)this.mAccounts.get(this.mSelectedItemIndex));
        }
    }

    protected void onSaveInstanceState(Bundle paramBundle)
    {
        super.onSaveInstanceState(paramBundle);
        paramBundle.putInt("pendingRequest", this.mPendingRequest);
        if (this.mPendingRequest == 2)
            paramBundle.putParcelableArray("existingAccounts", this.mExistingAccounts);
        if (this.mSelectedItemIndex != -1)
        {
            if (this.mSelectedItemIndex != this.mAccounts.size())
                break label64;
            paramBundle.putBoolean("selectedAddAccount", true);
        }
        while (true)
        {
            return;
            label64: paramBundle.putBoolean("selectedAddAccount", false);
            paramBundle.putString("selectedAccountName", ((Account)this.mAccounts.get(this.mSelectedItemIndex)).name);
        }
    }

    public void run(AccountManagerFuture<Bundle> paramAccountManagerFuture)
    {
        try
        {
            Intent localIntent = (Intent)((Bundle)paramAccountManagerFuture.getResult()).getParcelable("intent");
            if (localIntent != null)
            {
                this.mPendingRequest = 2;
                this.mExistingAccounts = AccountManager.get(this).getAccounts();
                localIntent.setFlags(0xEFFFFFFF & localIntent.getFlags());
                startActivityForResult(localIntent, 2);
                return;
            }
        }
        catch (OperationCanceledException localOperationCanceledException)
        {
            while (true)
            {
                setResult(0);
                finish();
            }
        }
        catch (AuthenticatorException localAuthenticatorException)
        {
            while (true)
            {
                Bundle localBundle = new Bundle();
                localBundle.putString("errorMessage", "error communicating with server");
                setResult(-1, new Intent().putExtras(localBundle));
                finish();
            }
        }
        catch (IOException localIOException)
        {
            label80: break label80;
        }
    }

    protected void runAddAccountForAuthenticator(String paramString)
    {
        if (Log.isLoggable("AccountChooser", 2))
            Log.v("AccountChooser", "runAddAccountForAuthenticator: " + paramString);
        Bundle localBundle = getIntent().getBundleExtra("addAccountOptions");
        String[] arrayOfString = getIntent().getStringArrayExtra("addAccountRequiredFeatures");
        String str = getIntent().getStringExtra("authTokenType");
        AccountManager.get(this).addAccount(paramString, str, arrayOfString, localBundle, null, this, null);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.ChooseTypeAndAccountActivity
 * JD-Core Version:        0.6.2
 */