package android.accounts;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.collect.Maps;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class AccountManager
{
    public static final String ACTION_AUTHENTICATOR_INTENT = "android.accounts.AccountAuthenticator";
    public static final String AUTHENTICATOR_ATTRIBUTES_NAME = "account-authenticator";
    public static final String AUTHENTICATOR_META_DATA_NAME = "android.accounts.AccountAuthenticator";
    public static final int ERROR_CODE_BAD_ARGUMENTS = 7;
    public static final int ERROR_CODE_BAD_REQUEST = 8;
    public static final int ERROR_CODE_CANCELED = 4;
    public static final int ERROR_CODE_INVALID_RESPONSE = 5;
    public static final int ERROR_CODE_NETWORK_ERROR = 3;
    public static final int ERROR_CODE_REMOTE_EXCEPTION = 1;
    public static final int ERROR_CODE_UNSUPPORTED_OPERATION = 6;
    public static final String KEY_ACCOUNTS = "accounts";
    public static final String KEY_ACCOUNT_AUTHENTICATOR_RESPONSE = "accountAuthenticatorResponse";
    public static final String KEY_ACCOUNT_MANAGER_RESPONSE = "accountManagerResponse";
    public static final String KEY_ACCOUNT_NAME = "authAccount";
    public static final String KEY_ACCOUNT_TYPE = "accountType";
    public static final String KEY_ANDROID_PACKAGE_NAME = "androidPackageName";
    public static final String KEY_AUTHENTICATOR_TYPES = "authenticator_types";
    public static final String KEY_AUTHTOKEN = "authtoken";
    public static final String KEY_AUTH_FAILED_MESSAGE = "authFailedMessage";
    public static final String KEY_AUTH_TOKEN_LABEL = "authTokenLabelKey";
    public static final String KEY_BOOLEAN_RESULT = "booleanResult";
    public static final String KEY_CALLER_PID = "callerPid";
    public static final String KEY_CALLER_UID = "callerUid";
    public static final String KEY_ERROR_CODE = "errorCode";
    public static final String KEY_ERROR_MESSAGE = "errorMessage";
    public static final String KEY_INTENT = "intent";
    public static final String KEY_NOTIFY_ON_FAILURE = "notifyOnAuthFailure";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_USERDATA = "userdata";
    public static final String LOGIN_ACCOUNTS_CHANGED_ACTION = "android.accounts.LOGIN_ACCOUNTS_CHANGED";
    private static final String TAG = "AccountManager";
    private final BroadcastReceiver mAccountsChangedBroadcastReceiver = new BroadcastReceiver()
    {
        // ERROR //
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 12	android/accounts/AccountManager$13:this$0	Landroid/accounts/AccountManager;
            //     4: invokevirtual 21	android/accounts/AccountManager:getAccounts	()[Landroid/accounts/Account;
            //     7: astore_3
            //     8: aload_0
            //     9: getfield 12	android/accounts/AccountManager$13:this$0	Landroid/accounts/AccountManager;
            //     12: invokestatic 25	android/accounts/AccountManager:access$1300	(Landroid/accounts/AccountManager;)Ljava/util/HashMap;
            //     15: astore 4
            //     17: aload 4
            //     19: monitorenter
            //     20: aload_0
            //     21: getfield 12	android/accounts/AccountManager$13:this$0	Landroid/accounts/AccountManager;
            //     24: invokestatic 25	android/accounts/AccountManager:access$1300	(Landroid/accounts/AccountManager;)Ljava/util/HashMap;
            //     27: invokevirtual 31	java/util/HashMap:entrySet	()Ljava/util/Set;
            //     30: invokeinterface 37 1 0
            //     35: astore 6
            //     37: aload 6
            //     39: invokeinterface 43 1 0
            //     44: ifeq +54 -> 98
            //     47: aload 6
            //     49: invokeinterface 47 1 0
            //     54: checkcast 49	java/util/Map$Entry
            //     57: astore 7
            //     59: aload_0
            //     60: getfield 12	android/accounts/AccountManager$13:this$0	Landroid/accounts/AccountManager;
            //     63: aload 7
            //     65: invokeinterface 52 1 0
            //     70: checkcast 54	android/os/Handler
            //     73: aload 7
            //     75: invokeinterface 57 1 0
            //     80: checkcast 59	android/accounts/OnAccountsUpdateListener
            //     83: aload_3
            //     84: invokestatic 63	android/accounts/AccountManager:access$1400	(Landroid/accounts/AccountManager;Landroid/os/Handler;Landroid/accounts/OnAccountsUpdateListener;[Landroid/accounts/Account;)V
            //     87: goto -50 -> 37
            //     90: astore 5
            //     92: aload 4
            //     94: monitorexit
            //     95: aload 5
            //     97: athrow
            //     98: aload 4
            //     100: monitorexit
            //     101: return
            //
            // Exception table:
            //     from	to	target	type
            //     20	95	90	finally
            //     98	101	90	finally
        }
    };
    private final HashMap<OnAccountsUpdateListener, Handler> mAccountsUpdatedListeners = Maps.newHashMap();
    private final Context mContext;
    private final Handler mMainHandler;
    private final IAccountManager mService;

    public AccountManager(Context paramContext, IAccountManager paramIAccountManager)
    {
        this.mContext = paramContext;
        this.mService = paramIAccountManager;
        this.mMainHandler = new Handler(this.mContext.getMainLooper());
    }

    public AccountManager(Context paramContext, IAccountManager paramIAccountManager, Handler paramHandler)
    {
        this.mContext = paramContext;
        this.mService = paramIAccountManager;
        this.mMainHandler = paramHandler;
    }

    private Exception convertErrorToException(int paramInt, String paramString)
    {
        Object localObject;
        if (paramInt == 3)
            localObject = new IOException(paramString);
        while (true)
        {
            return localObject;
            if (paramInt == 6)
                localObject = new UnsupportedOperationException(paramString);
            else if (paramInt == 5)
                localObject = new AuthenticatorException(paramString);
            else if (paramInt == 7)
                localObject = new IllegalArgumentException(paramString);
            else
                localObject = new AuthenticatorException(paramString);
        }
    }

    private void ensureNotOnMainThread()
    {
        Looper localLooper = Looper.myLooper();
        if ((localLooper != null) && (localLooper == this.mContext.getMainLooper()))
        {
            IllegalStateException localIllegalStateException = new IllegalStateException("calling this from your main thread can lead to deadlock");
            Log.e("AccountManager", "calling this from your main thread can lead to deadlock and/or ANRs", localIllegalStateException);
            if (this.mContext.getApplicationInfo().targetSdkVersion >= 8)
                throw localIllegalStateException;
        }
    }

    public static AccountManager get(Context paramContext)
    {
        if (paramContext == null)
            throw new IllegalArgumentException("context is null");
        return (AccountManager)paramContext.getSystemService("account");
    }

    public static Intent newChooseAccountIntent(Account paramAccount, ArrayList<Account> paramArrayList, String[] paramArrayOfString1, boolean paramBoolean, String paramString1, String paramString2, String[] paramArrayOfString2, Bundle paramBundle)
    {
        Intent localIntent = new Intent();
        localIntent.setClassName("android", "android.accounts.ChooseTypeAndAccountActivity");
        localIntent.putExtra("allowableAccounts", paramArrayList);
        localIntent.putExtra("allowableAccountTypes", paramArrayOfString1);
        localIntent.putExtra("addAccountOptions", paramBundle);
        localIntent.putExtra("selectedAccount", paramAccount);
        localIntent.putExtra("alwaysPromptForAccount", paramBoolean);
        localIntent.putExtra("descriptionTextOverride", paramString1);
        localIntent.putExtra("authTokenType", paramString2);
        localIntent.putExtra("addAccountRequiredFeatures", paramArrayOfString2);
        return localIntent;
    }

    private void postToHandler(Handler paramHandler, final AccountManagerCallback<Bundle> paramAccountManagerCallback, final AccountManagerFuture<Bundle> paramAccountManagerFuture)
    {
        if (paramHandler == null)
            paramHandler = this.mMainHandler;
        paramHandler.post(new Runnable()
        {
            public void run()
            {
                paramAccountManagerCallback.run(paramAccountManagerFuture);
            }
        });
    }

    private void postToHandler(Handler paramHandler, final OnAccountsUpdateListener paramOnAccountsUpdateListener, Account[] paramArrayOfAccount)
    {
        final Account[] arrayOfAccount = new Account[paramArrayOfAccount.length];
        System.arraycopy(paramArrayOfAccount, 0, arrayOfAccount, 0, arrayOfAccount.length);
        if (paramHandler == null)
            paramHandler = this.mMainHandler;
        paramHandler.post(new Runnable()
        {
            public void run()
            {
                try
                {
                    paramOnAccountsUpdateListener.onAccountsUpdated(arrayOfAccount);
                    return;
                }
                catch (SQLException localSQLException)
                {
                    while (true)
                        Log.e("AccountManager", "Can't update accounts", localSQLException);
                }
            }
        });
    }

    public static Bundle sanitizeResult(Bundle paramBundle)
    {
        Bundle localBundle;
        if ((paramBundle != null) && (paramBundle.containsKey("authtoken")) && (!TextUtils.isEmpty(paramBundle.getString("authtoken"))))
        {
            localBundle = new Bundle(paramBundle);
            localBundle.putString("authtoken", "<omitted for logging purposes>");
        }
        while (true)
        {
            return localBundle;
            localBundle = paramBundle;
        }
    }

    public AccountManagerFuture<Bundle> addAccount(final String paramString1, final String paramString2, final String[] paramArrayOfString, Bundle paramBundle, final Activity paramActivity, AccountManagerCallback<Bundle> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("accountType is null");
        final Bundle localBundle = new Bundle();
        if (paramBundle != null)
            localBundle.putAll(paramBundle);
        localBundle.putString("androidPackageName", this.mContext.getPackageName());
        return new AmsTask(paramActivity, paramHandler, paramAccountManagerCallback, paramString1)
        {
            public void doWork()
                throws RemoteException
            {
                IAccountManager localIAccountManager = AccountManager.this.mService;
                IAccountManagerResponse localIAccountManagerResponse = this.mResponse;
                String str1 = paramString1;
                String str2 = paramString2;
                String[] arrayOfString = paramArrayOfString;
                if (paramActivity != null);
                for (boolean bool = true; ; bool = false)
                {
                    localIAccountManager.addAcount(localIAccountManagerResponse, str1, str2, arrayOfString, bool, localBundle);
                    return;
                }
            }
        }
        .start();
    }

    public boolean addAccountExplicitly(Account paramAccount, String paramString, Bundle paramBundle)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        try
        {
            boolean bool = this.mService.addAccount(paramAccount, paramString, paramBundle);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    // ERROR //
    public void addOnAccountsUpdatedListener(OnAccountsUpdateListener paramOnAccountsUpdateListener, Handler paramHandler, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +14 -> 15
        //     4: new 215	java/lang/IllegalArgumentException
        //     7: dup
        //     8: ldc_w 383
        //     11: invokespecial 216	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     14: athrow
        //     15: aload_0
        //     16: getfield 152	android/accounts/AccountManager:mAccountsUpdatedListeners	Ljava/util/HashMap;
        //     19: astore 4
        //     21: aload 4
        //     23: monitorenter
        //     24: aload_0
        //     25: getfield 152	android/accounts/AccountManager:mAccountsUpdatedListeners	Ljava/util/HashMap;
        //     28: aload_1
        //     29: invokevirtual 388	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     32: ifeq +22 -> 54
        //     35: new 223	java/lang/IllegalStateException
        //     38: dup
        //     39: ldc_w 390
        //     42: invokespecial 226	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     45: athrow
        //     46: astore 5
        //     48: aload 4
        //     50: monitorexit
        //     51: aload 5
        //     53: athrow
        //     54: aload_0
        //     55: getfield 152	android/accounts/AccountManager:mAccountsUpdatedListeners	Ljava/util/HashMap;
        //     58: invokevirtual 393	java/util/HashMap:isEmpty	()Z
        //     61: istore 6
        //     63: aload_0
        //     64: getfield 152	android/accounts/AccountManager:mAccountsUpdatedListeners	Ljava/util/HashMap;
        //     67: aload_1
        //     68: aload_2
        //     69: invokevirtual 397	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     72: pop
        //     73: iload 6
        //     75: ifeq +41 -> 116
        //     78: new 399	android/content/IntentFilter
        //     81: dup
        //     82: invokespecial 400	android/content/IntentFilter:<init>	()V
        //     85: astore 8
        //     87: aload 8
        //     89: ldc 125
        //     91: invokevirtual 403	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     94: aload 8
        //     96: ldc_w 405
        //     99: invokevirtual 403	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     102: aload_0
        //     103: getfield 159	android/accounts/AccountManager:mContext	Landroid/content/Context;
        //     106: aload_0
        //     107: getfield 157	android/accounts/AccountManager:mAccountsChangedBroadcastReceiver	Landroid/content/BroadcastReceiver;
        //     110: aload 8
        //     112: invokevirtual 409	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     115: pop
        //     116: aload 4
        //     118: monitorexit
        //     119: iload_3
        //     120: ifeq +13 -> 133
        //     123: aload_0
        //     124: aload_2
        //     125: aload_1
        //     126: aload_0
        //     127: invokevirtual 413	android/accounts/AccountManager:getAccounts	()[Landroid/accounts/Account;
        //     130: invokespecial 185	android/accounts/AccountManager:postToHandler	(Landroid/os/Handler;Landroid/accounts/OnAccountsUpdateListener;[Landroid/accounts/Account;)V
        //     133: return
        //
        // Exception table:
        //     from	to	target	type
        //     24	51	46	finally
        //     54	119	46	finally
    }

    public String blockingGetAuthToken(Account paramAccount, String paramString, boolean paramBoolean)
        throws OperationCanceledException, IOException, AuthenticatorException
    {
        String str = null;
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("authTokenType is null");
        Bundle localBundle = (Bundle)getAuthToken(paramAccount, paramString, paramBoolean, null, null).getResult();
        if (localBundle == null)
            Log.e("AccountManager", "blockingGetAuthToken: null was returned from getResult() for " + paramAccount + ", authTokenType " + paramString);
        while (true)
        {
            return str;
            str = localBundle.getString("authtoken");
        }
    }

    public void clearPassword(Account paramAccount)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        try
        {
            this.mService.clearPassword(paramAccount);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public AccountManagerFuture<Bundle> confirmCredentials(final Account paramAccount, final Bundle paramBundle, final Activity paramActivity, AccountManagerCallback<Bundle> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        return new AmsTask(paramActivity, paramHandler, paramAccountManagerCallback, paramAccount)
        {
            public void doWork()
                throws RemoteException
            {
                IAccountManager localIAccountManager = AccountManager.this.mService;
                IAccountManagerResponse localIAccountManagerResponse = this.mResponse;
                Account localAccount = paramAccount;
                Bundle localBundle = paramBundle;
                if (paramActivity != null);
                for (boolean bool = true; ; bool = false)
                {
                    localIAccountManager.confirmCredentials(localIAccountManagerResponse, localAccount, localBundle, bool);
                    return;
                }
            }
        }
        .start();
    }

    public AccountManagerFuture<Bundle> editProperties(final String paramString, final Activity paramActivity, AccountManagerCallback<Bundle> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramString == null)
            throw new IllegalArgumentException("accountType is null");
        return new AmsTask(paramActivity, paramHandler, paramAccountManagerCallback, paramString)
        {
            public void doWork()
                throws RemoteException
            {
                IAccountManager localIAccountManager = AccountManager.this.mService;
                IAccountManagerResponse localIAccountManagerResponse = this.mResponse;
                String str = paramString;
                if (paramActivity != null);
                for (boolean bool = true; ; bool = false)
                {
                    localIAccountManager.editProperties(localIAccountManagerResponse, str, bool);
                    return;
                }
            }
        }
        .start();
    }

    public Account[] getAccounts()
    {
        try
        {
            Account[] arrayOfAccount = this.mService.getAccounts(null);
            return arrayOfAccount;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public Account[] getAccountsByType(String paramString)
    {
        try
        {
            Account[] arrayOfAccount = this.mService.getAccounts(paramString);
            return arrayOfAccount;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public AccountManagerFuture<Account[]> getAccountsByTypeAndFeatures(final String paramString, final String[] paramArrayOfString, AccountManagerCallback<Account[]> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramString == null)
            throw new IllegalArgumentException("type is null");
        return new Future2Task(paramHandler, paramAccountManagerCallback, paramString)
        {
            public Account[] bundleToResult(Bundle paramAnonymousBundle)
                throws AuthenticatorException
            {
                if (!paramAnonymousBundle.containsKey("accounts"))
                    throw new AuthenticatorException("no result in response");
                Parcelable[] arrayOfParcelable = paramAnonymousBundle.getParcelableArray("accounts");
                Account[] arrayOfAccount = new Account[arrayOfParcelable.length];
                for (int i = 0; i < arrayOfParcelable.length; i++)
                    arrayOfAccount[i] = ((Account)arrayOfParcelable[i]);
                return arrayOfAccount;
            }

            public void doWork()
                throws RemoteException
            {
                AccountManager.this.mService.getAccountsByFeatures(this.mResponse, paramString, paramArrayOfString);
            }
        }
        .start();
    }

    public AccountManagerFuture<Bundle> getAuthToken(final Account paramAccount, final String paramString, Bundle paramBundle, Activity paramActivity, AccountManagerCallback<Bundle> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("authTokenType is null");
        final Bundle localBundle = new Bundle();
        if (paramBundle != null)
            localBundle.putAll(paramBundle);
        localBundle.putString("androidPackageName", this.mContext.getPackageName());
        return new AmsTask(paramActivity, paramHandler, paramAccountManagerCallback, paramAccount)
        {
            public void doWork()
                throws RemoteException
            {
                AccountManager.this.mService.getAuthToken(this.mResponse, paramAccount, paramString, false, true, localBundle);
            }
        }
        .start();
    }

    public AccountManagerFuture<Bundle> getAuthToken(final Account paramAccount, final String paramString, Bundle paramBundle, final boolean paramBoolean, AccountManagerCallback<Bundle> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("authTokenType is null");
        final Bundle localBundle = new Bundle();
        if (paramBundle != null)
            localBundle.putAll(paramBundle);
        localBundle.putString("androidPackageName", this.mContext.getPackageName());
        return new AmsTask(null, paramHandler, paramAccountManagerCallback, paramAccount)
        {
            public void doWork()
                throws RemoteException
            {
                AccountManager.this.mService.getAuthToken(this.mResponse, paramAccount, paramString, paramBoolean, false, localBundle);
            }
        }
        .start();
    }

    @Deprecated
    public AccountManagerFuture<Bundle> getAuthToken(Account paramAccount, String paramString, boolean paramBoolean, AccountManagerCallback<Bundle> paramAccountManagerCallback, Handler paramHandler)
    {
        return getAuthToken(paramAccount, paramString, null, paramBoolean, paramAccountManagerCallback, paramHandler);
    }

    public AccountManagerFuture<Bundle> getAuthTokenByFeatures(String paramString1, String paramString2, String[] paramArrayOfString, Activity paramActivity, Bundle paramBundle1, Bundle paramBundle2, AccountManagerCallback<Bundle> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("account type is null");
        if (paramString2 == null)
            throw new IllegalArgumentException("authTokenType is null");
        GetAuthTokenByTypeAndFeaturesTask localGetAuthTokenByTypeAndFeaturesTask = new GetAuthTokenByTypeAndFeaturesTask(paramString1, paramString2, paramArrayOfString, paramActivity, paramBundle1, paramBundle2, paramAccountManagerCallback, paramHandler);
        localGetAuthTokenByTypeAndFeaturesTask.start();
        return localGetAuthTokenByTypeAndFeaturesTask;
    }

    public AccountManagerFuture<String> getAuthTokenLabel(final String paramString1, final String paramString2, AccountManagerCallback<String> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("accountType is null");
        if (paramString2 == null)
            throw new IllegalArgumentException("authTokenType is null");
        return new Future2Task(paramHandler, paramAccountManagerCallback, paramString1)
        {
            public String bundleToResult(Bundle paramAnonymousBundle)
                throws AuthenticatorException
            {
                if (!paramAnonymousBundle.containsKey("authTokenLabelKey"))
                    throw new AuthenticatorException("no result in response");
                return paramAnonymousBundle.getString("authTokenLabelKey");
            }

            public void doWork()
                throws RemoteException
            {
                AccountManager.this.mService.getAuthTokenLabel(this.mResponse, paramString1, paramString2);
            }
        }
        .start();
    }

    public AuthenticatorDescription[] getAuthenticatorTypes()
    {
        try
        {
            AuthenticatorDescription[] arrayOfAuthenticatorDescription = this.mService.getAuthenticatorTypes();
            return arrayOfAuthenticatorDescription;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public String getPassword(Account paramAccount)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        try
        {
            String str = this.mService.getPassword(paramAccount);
            return str;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public String getUserData(Account paramAccount, String paramString)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("key is null");
        try
        {
            String str = this.mService.getUserData(paramAccount, paramString);
            return str;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public AccountManagerFuture<Boolean> hasFeatures(final Account paramAccount, final String[] paramArrayOfString, AccountManagerCallback<Boolean> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramArrayOfString == null)
            throw new IllegalArgumentException("features is null");
        return new Future2Task(paramHandler, paramAccountManagerCallback, paramAccount)
        {
            public Boolean bundleToResult(Bundle paramAnonymousBundle)
                throws AuthenticatorException
            {
                if (!paramAnonymousBundle.containsKey("booleanResult"))
                    throw new AuthenticatorException("no result in response");
                return Boolean.valueOf(paramAnonymousBundle.getBoolean("booleanResult"));
            }

            public void doWork()
                throws RemoteException
            {
                AccountManager.this.mService.hasFeatures(this.mResponse, paramAccount, paramArrayOfString);
            }
        }
        .start();
    }

    public void invalidateAuthToken(String paramString1, String paramString2)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("accountType is null");
        if (paramString2 != null);
        try
        {
            this.mService.invalidateAuthToken(paramString1, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public String peekAuthToken(Account paramAccount, String paramString)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString == null)
            throw new IllegalArgumentException("authTokenType is null");
        try
        {
            String str = this.mService.peekAuthToken(paramAccount, paramString);
            return str;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public AccountManagerFuture<Boolean> removeAccount(final Account paramAccount, AccountManagerCallback<Boolean> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        return new Future2Task(paramHandler, paramAccountManagerCallback, paramAccount)
        {
            public Boolean bundleToResult(Bundle paramAnonymousBundle)
                throws AuthenticatorException
            {
                if (!paramAnonymousBundle.containsKey("booleanResult"))
                    throw new AuthenticatorException("no result in response");
                return Boolean.valueOf(paramAnonymousBundle.getBoolean("booleanResult"));
            }

            public void doWork()
                throws RemoteException
            {
                AccountManager.this.mService.removeAccount(this.mResponse, paramAccount);
            }
        }
        .start();
    }

    public void removeOnAccountsUpdatedListener(OnAccountsUpdateListener paramOnAccountsUpdateListener)
    {
        if (paramOnAccountsUpdateListener == null)
            throw new IllegalArgumentException("listener is null");
        synchronized (this.mAccountsUpdatedListeners)
        {
            if (!this.mAccountsUpdatedListeners.containsKey(paramOnAccountsUpdateListener))
            {
                Log.e("AccountManager", "Listener was not previously added");
            }
            else
            {
                this.mAccountsUpdatedListeners.remove(paramOnAccountsUpdateListener);
                if (this.mAccountsUpdatedListeners.isEmpty())
                    this.mContext.unregisterReceiver(this.mAccountsChangedBroadcastReceiver);
            }
        }
    }

    public void setAuthToken(Account paramAccount, String paramString1, String paramString2)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString1 == null)
            throw new IllegalArgumentException("authTokenType is null");
        try
        {
            this.mService.setAuthToken(paramAccount, paramString1, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void setPassword(Account paramAccount, String paramString)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        try
        {
            this.mService.setPassword(paramAccount, paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void setUserData(Account paramAccount, String paramString1, String paramString2)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        if (paramString1 == null)
            throw new IllegalArgumentException("key is null");
        try
        {
            this.mService.setUserData(paramAccount, paramString1, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public void updateAppPermission(Account paramAccount, String paramString, int paramInt, boolean paramBoolean)
    {
        try
        {
            this.mService.updateAppPermission(paramAccount, paramString, paramInt, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public AccountManagerFuture<Bundle> updateCredentials(final Account paramAccount, final String paramString, final Bundle paramBundle, final Activity paramActivity, AccountManagerCallback<Bundle> paramAccountManagerCallback, Handler paramHandler)
    {
        if (paramAccount == null)
            throw new IllegalArgumentException("account is null");
        return new AmsTask(paramActivity, paramHandler, paramAccountManagerCallback, paramAccount)
        {
            public void doWork()
                throws RemoteException
            {
                IAccountManager localIAccountManager = AccountManager.this.mService;
                IAccountManagerResponse localIAccountManagerResponse = this.mResponse;
                Account localAccount = paramAccount;
                String str = paramString;
                if (paramActivity != null);
                for (boolean bool = true; ; bool = false)
                {
                    localIAccountManager.updateCredentials(localIAccountManagerResponse, localAccount, str, bool, paramBundle);
                    return;
                }
            }
        }
        .start();
    }

    private class GetAuthTokenByTypeAndFeaturesTask extends AccountManager.AmsTask
        implements AccountManagerCallback<Bundle>
    {
        final String mAccountType;
        final Bundle mAddAccountOptions;
        final String mAuthTokenType;
        final String[] mFeatures;
        volatile AccountManagerFuture<Bundle> mFuture = null;
        final Bundle mLoginOptions;
        final AccountManagerCallback<Bundle> mMyCallback;
        private volatile int mNumAccounts = 0;

        GetAuthTokenByTypeAndFeaturesTask(String paramArrayOfString, String[] paramActivity, Activity paramBundle1, Bundle paramBundle2, Bundle paramAccountManagerCallback, AccountManagerCallback<Bundle> paramHandler, Handler arg8)
        {
            super(paramBundle2, localHandler, localAccountManagerCallback);
            if (paramArrayOfString == null)
                throw new IllegalArgumentException("account type is null");
            this.mAccountType = paramArrayOfString;
            this.mAuthTokenType = paramActivity;
            this.mFeatures = paramBundle1;
            this.mAddAccountOptions = paramAccountManagerCallback;
            this.mLoginOptions = paramHandler;
            this.mMyCallback = this;
        }

        public void doWork()
            throws RemoteException
        {
            AccountManager.this.getAccountsByTypeAndFeatures(this.mAccountType, this.mFeatures, new AccountManagerCallback()
            {
                public void run(AccountManagerFuture<Account[]> paramAnonymousAccountManagerFuture)
                {
                    try
                    {
                        arrayOfAccount = (Account[])paramAnonymousAccountManagerFuture.getResult();
                        AccountManager.GetAuthTokenByTypeAndFeaturesTask.access$1202(AccountManager.GetAuthTokenByTypeAndFeaturesTask.this, arrayOfAccount.length);
                        if (arrayOfAccount.length == 0)
                            if (AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mActivity != null)
                            {
                                AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mFuture = AccountManager.this.addAccount(AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mAccountType, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mAuthTokenType, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mFeatures, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mAddAccountOptions, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mActivity, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mMyCallback, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mHandler);
                                return;
                            }
                    }
                    catch (OperationCanceledException localOperationCanceledException)
                    {
                        while (true)
                            AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.setException(localOperationCanceledException);
                    }
                    catch (IOException localIOException)
                    {
                        while (true)
                            AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.setException(localIOException);
                    }
                    catch (AuthenticatorException localAuthenticatorException)
                    {
                        while (true)
                        {
                            Account[] arrayOfAccount;
                            AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.setException(localAuthenticatorException);
                            continue;
                            Bundle localBundle2 = new Bundle();
                            localBundle2.putString("authAccount", null);
                            localBundle2.putString("accountType", null);
                            localBundle2.putString("authtoken", null);
                            try
                            {
                                AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mResponse.onResult(localBundle2);
                            }
                            catch (RemoteException localRemoteException2)
                            {
                            }
                            continue;
                            if (arrayOfAccount.length == 1)
                            {
                                if (AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mActivity == null)
                                    AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mFuture = AccountManager.this.getAuthToken(arrayOfAccount[0], AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mAuthTokenType, false, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mMyCallback, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mHandler);
                                else
                                    AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mFuture = AccountManager.this.getAuthToken(arrayOfAccount[0], AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mAuthTokenType, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mLoginOptions, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mActivity, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mMyCallback, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mHandler);
                            }
                            else if (AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mActivity != null)
                            {
                                IAccountManagerResponse.Stub local1 = new IAccountManagerResponse.Stub()
                                {
                                    public void onError(int paramAnonymous2Int, String paramAnonymous2String)
                                        throws RemoteException
                                    {
                                        AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mResponse.onError(paramAnonymous2Int, paramAnonymous2String);
                                    }

                                    public void onResult(Bundle paramAnonymous2Bundle)
                                        throws RemoteException
                                    {
                                        Account localAccount = new Account(paramAnonymous2Bundle.getString("authAccount"), paramAnonymous2Bundle.getString("accountType"));
                                        AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mFuture = AccountManager.this.getAuthToken(localAccount, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mAuthTokenType, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mLoginOptions, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mActivity, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mMyCallback, AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mHandler);
                                    }
                                };
                                Intent localIntent = new Intent();
                                localIntent.setClassName("android", "android.accounts.ChooseAccountActivity");
                                localIntent.putExtra("accounts", arrayOfAccount);
                                localIntent.putExtra("accountManagerResponse", new AccountManagerResponse(local1));
                                AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mActivity.startActivity(localIntent);
                            }
                            else
                            {
                                Bundle localBundle1 = new Bundle();
                                localBundle1.putString("accounts", null);
                                try
                                {
                                    AccountManager.GetAuthTokenByTypeAndFeaturesTask.this.mResponse.onResult(localBundle1);
                                }
                                catch (RemoteException localRemoteException1)
                                {
                                }
                            }
                        }
                    }
                }
            }
            , this.mHandler);
        }

        public void run(AccountManagerFuture<Bundle> paramAccountManagerFuture)
        {
            try
            {
                localBundle = (Bundle)paramAccountManagerFuture.getResult();
                if (this.mNumAccounts == 0)
                {
                    String str1 = localBundle.getString("authAccount");
                    String str2 = localBundle.getString("accountType");
                    if ((TextUtils.isEmpty(str1)) || (TextUtils.isEmpty(str2)))
                    {
                        setException(new AuthenticatorException("account not in result"));
                    }
                    else
                    {
                        Account localAccount = new Account(str1, str2);
                        this.mNumAccounts = 1;
                        AccountManager.this.getAuthToken(localAccount, this.mAuthTokenType, null, this.mActivity, this.mMyCallback, this.mHandler);
                    }
                }
            }
            catch (OperationCanceledException localOperationCanceledException)
            {
                Bundle localBundle;
                cancel(true);
                return;
                set(localBundle);
            }
            catch (IOException localIOException)
            {
                setException(localIOException);
            }
            catch (AuthenticatorException localAuthenticatorException)
            {
                setException(localAuthenticatorException);
            }
        }
    }

    private abstract class Future2Task<T> extends AccountManager.BaseFutureTask<T>
        implements AccountManagerFuture<T>
    {
        final AccountManagerCallback<T> mCallback;

        public Future2Task(AccountManagerCallback<T> arg2)
        {
            super(localHandler);
            Object localObject;
            this.mCallback = localObject;
        }

        // ERROR //
        private T internalGetResult(Long paramLong, TimeUnit paramTimeUnit)
            throws OperationCanceledException, IOException, AuthenticatorException
        {
            // Byte code:
            //     0: aload_0
            //     1: invokevirtual 46	android/accounts/AccountManager$Future2Task:isDone	()Z
            //     4: ifne +10 -> 14
            //     7: aload_0
            //     8: getfield 21	android/accounts/AccountManager$Future2Task:this$0	Landroid/accounts/AccountManager;
            //     11: invokestatic 50	android/accounts/AccountManager:access$200	(Landroid/accounts/AccountManager;)V
            //     14: aload_1
            //     15: ifnonnull +22 -> 37
            //     18: aload_0
            //     19: invokevirtual 54	android/accounts/AccountManager$Future2Task:get	()Ljava/lang/Object;
            //     22: astore 20
            //     24: aload 20
            //     26: astore 16
            //     28: aload_0
            //     29: iconst_1
            //     30: invokevirtual 58	android/accounts/AccountManager$Future2Task:cancel	(Z)Z
            //     33: pop
            //     34: aload 16
            //     36: areturn
            //     37: aload_1
            //     38: invokevirtual 64	java/lang/Long:longValue	()J
            //     41: lstore 11
            //     43: aload_0
            //     44: lload 11
            //     46: aload_2
            //     47: invokevirtual 67	android/accounts/AccountManager$Future2Task:get	(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
            //     50: astore 15
            //     52: aload 15
            //     54: astore 16
            //     56: goto -28 -> 28
            //     59: astore 9
            //     61: aload 9
            //     63: invokevirtual 71	java/util/concurrent/ExecutionException:getCause	()Ljava/lang/Throwable;
            //     66: astore 10
            //     68: aload 10
            //     70: instanceof 32
            //     73: ifeq +20 -> 93
            //     76: aload 10
            //     78: checkcast 32	java/io/IOException
            //     81: athrow
            //     82: astore 7
            //     84: aload_0
            //     85: iconst_1
            //     86: invokevirtual 58	android/accounts/AccountManager$Future2Task:cancel	(Z)Z
            //     89: pop
            //     90: aload 7
            //     92: athrow
            //     93: aload 10
            //     95: instanceof 73
            //     98: ifeq +13 -> 111
            //     101: new 34	android/accounts/AuthenticatorException
            //     104: dup
            //     105: aload 10
            //     107: invokespecial 76	android/accounts/AuthenticatorException:<init>	(Ljava/lang/Throwable;)V
            //     110: athrow
            //     111: aload 10
            //     113: instanceof 34
            //     116: ifeq +9 -> 125
            //     119: aload 10
            //     121: checkcast 34	android/accounts/AuthenticatorException
            //     124: athrow
            //     125: aload 10
            //     127: instanceof 78
            //     130: ifeq +9 -> 139
            //     133: aload 10
            //     135: checkcast 78	java/lang/RuntimeException
            //     138: athrow
            //     139: aload 10
            //     141: instanceof 80
            //     144: ifeq +9 -> 153
            //     147: aload 10
            //     149: checkcast 80	java/lang/Error
            //     152: athrow
            //     153: new 82	java/lang/IllegalStateException
            //     156: dup
            //     157: aload 10
            //     159: invokespecial 83	java/lang/IllegalStateException:<init>	(Ljava/lang/Throwable;)V
            //     162: athrow
            //     163: astore 19
            //     165: aload_0
            //     166: iconst_1
            //     167: invokevirtual 58	android/accounts/AccountManager$Future2Task:cancel	(Z)Z
            //     170: pop
            //     171: new 30	android/accounts/OperationCanceledException
            //     174: dup
            //     175: invokespecial 86	android/accounts/OperationCanceledException:<init>	()V
            //     178: athrow
            //     179: astore 6
            //     181: goto -16 -> 165
            //     184: astore 14
            //     186: goto -21 -> 165
            //     189: astore 18
            //     191: goto -26 -> 165
            //     194: astore 5
            //     196: goto -31 -> 165
            //     199: astore 13
            //     201: goto -36 -> 165
            //     204: astore_3
            //     205: goto -40 -> 165
            //
            // Exception table:
            //     from	to	target	type
            //     18	24	59	java/util/concurrent/ExecutionException
            //     37	43	59	java/util/concurrent/ExecutionException
            //     43	52	59	java/util/concurrent/ExecutionException
            //     18	24	82	finally
            //     37	43	82	finally
            //     43	52	82	finally
            //     61	82	82	finally
            //     93	163	82	finally
            //     18	24	163	java/util/concurrent/CancellationException
            //     37	43	179	java/util/concurrent/CancellationException
            //     43	52	184	java/util/concurrent/CancellationException
            //     18	24	189	java/util/concurrent/TimeoutException
            //     37	43	194	java/util/concurrent/TimeoutException
            //     43	52	199	java/util/concurrent/TimeoutException
            //     18	24	204	java/lang/InterruptedException
            //     37	43	204	java/lang/InterruptedException
            //     43	52	204	java/lang/InterruptedException
        }

        protected void done()
        {
            if (this.mCallback != null)
                postRunnableToHandler(new Runnable()
                {
                    public void run()
                    {
                        AccountManager.Future2Task.this.mCallback.run(AccountManager.Future2Task.this);
                    }
                });
        }

        public T getResult()
            throws OperationCanceledException, IOException, AuthenticatorException
        {
            return internalGetResult(null, null);
        }

        public T getResult(long paramLong, TimeUnit paramTimeUnit)
            throws OperationCanceledException, IOException, AuthenticatorException
        {
            return internalGetResult(Long.valueOf(paramLong), paramTimeUnit);
        }

        public Future2Task<T> start()
        {
            startTask();
            return this;
        }
    }

    private abstract class BaseFutureTask<T> extends FutureTask<T>
    {
        final Handler mHandler;
        public final IAccountManagerResponse mResponse;

        public BaseFutureTask(Handler arg2)
        {
            super();
            Object localObject;
            this.mHandler = localObject;
            this.mResponse = new Response();
        }

        public abstract T bundleToResult(Bundle paramBundle)
            throws AuthenticatorException;

        public abstract void doWork()
            throws RemoteException;

        protected void postRunnableToHandler(Runnable paramRunnable)
        {
            if (this.mHandler == null);
            for (Handler localHandler = AccountManager.this.mMainHandler; ; localHandler = this.mHandler)
            {
                localHandler.post(paramRunnable);
                return;
            }
        }

        protected void startTask()
        {
            try
            {
                doWork();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    setException(localRemoteException);
            }
        }

        protected class Response extends IAccountManagerResponse.Stub
        {
            protected Response()
            {
            }

            public void onError(int paramInt, String paramString)
            {
                if (paramInt == 4)
                    AccountManager.BaseFutureTask.this.cancel(true);
                while (true)
                {
                    return;
                    AccountManager.BaseFutureTask.this.setException(AccountManager.access$400(AccountManager.this, paramInt, paramString));
                }
            }

            public void onResult(Bundle paramBundle)
            {
                try
                {
                    Object localObject = AccountManager.BaseFutureTask.this.bundleToResult(paramBundle);
                    if (localObject != null)
                        AccountManager.BaseFutureTask.this.set(localObject);
                }
                catch (ClassCastException localClassCastException)
                {
                    onError(5, "no result in response");
                }
                catch (AuthenticatorException localAuthenticatorException)
                {
                    label31: break label31;
                }
            }
        }
    }

    private abstract class AmsTask extends FutureTask<Bundle>
        implements AccountManagerFuture<Bundle>
    {
        final Activity mActivity;
        final AccountManagerCallback<Bundle> mCallback;
        final Handler mHandler;
        final IAccountManagerResponse mResponse;

        public AmsTask(Handler paramAccountManagerCallback, AccountManagerCallback<Bundle> arg3)
        {
            super();
            Object localObject1;
            this.mHandler = localObject1;
            Object localObject2;
            this.mCallback = localObject2;
            this.mActivity = paramAccountManagerCallback;
            this.mResponse = new Response(null);
        }

        // ERROR //
        private Bundle internalGetResult(Long paramLong, TimeUnit paramTimeUnit)
            throws OperationCanceledException, IOException, AuthenticatorException
        {
            // Byte code:
            //     0: aload_0
            //     1: invokevirtual 73	android/accounts/AccountManager$AmsTask:isDone	()Z
            //     4: ifne +10 -> 14
            //     7: aload_0
            //     8: getfield 30	android/accounts/AccountManager$AmsTask:this$0	Landroid/accounts/AccountManager;
            //     11: invokestatic 76	android/accounts/AccountManager:access$200	(Landroid/accounts/AccountManager;)V
            //     14: aload_1
            //     15: ifnonnull +25 -> 40
            //     18: aload_0
            //     19: invokevirtual 80	android/accounts/AccountManager$AmsTask:get	()Ljava/lang/Object;
            //     22: astore 19
            //     24: aload 19
            //     26: checkcast 82	android/os/Bundle
            //     29: astore 16
            //     31: aload_0
            //     32: iconst_1
            //     33: invokevirtual 86	android/accounts/AccountManager$AmsTask:cancel	(Z)Z
            //     36: pop
            //     37: aload 16
            //     39: areturn
            //     40: aload_1
            //     41: invokevirtual 92	java/lang/Long:longValue	()J
            //     44: lstore 11
            //     46: aload_0
            //     47: lload 11
            //     49: aload_2
            //     50: invokevirtual 95	android/accounts/AccountManager$AmsTask:get	(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
            //     53: astore 14
            //     55: aload 14
            //     57: checkcast 82	android/os/Bundle
            //     60: astore 16
            //     62: goto -31 -> 31
            //     65: astore 10
            //     67: new 57	android/accounts/OperationCanceledException
            //     70: dup
            //     71: invokespecial 98	android/accounts/OperationCanceledException:<init>	()V
            //     74: athrow
            //     75: astore 8
            //     77: aload_0
            //     78: iconst_1
            //     79: invokevirtual 86	android/accounts/AccountManager$AmsTask:cancel	(Z)Z
            //     82: pop
            //     83: aload 8
            //     85: athrow
            //     86: astore 6
            //     88: aload 6
            //     90: invokevirtual 102	java/util/concurrent/ExecutionException:getCause	()Ljava/lang/Throwable;
            //     93: astore 7
            //     95: aload 7
            //     97: instanceof 59
            //     100: ifeq +9 -> 109
            //     103: aload 7
            //     105: checkcast 59	java/io/IOException
            //     108: athrow
            //     109: aload 7
            //     111: instanceof 104
            //     114: ifeq +13 -> 127
            //     117: new 61	android/accounts/AuthenticatorException
            //     120: dup
            //     121: aload 7
            //     123: invokespecial 106	android/accounts/AuthenticatorException:<init>	(Ljava/lang/Throwable;)V
            //     126: athrow
            //     127: aload 7
            //     129: instanceof 61
            //     132: ifeq +9 -> 141
            //     135: aload 7
            //     137: checkcast 61	android/accounts/AuthenticatorException
            //     140: athrow
            //     141: aload 7
            //     143: instanceof 108
            //     146: ifeq +9 -> 155
            //     149: aload 7
            //     151: checkcast 108	java/lang/RuntimeException
            //     154: athrow
            //     155: aload 7
            //     157: instanceof 110
            //     160: ifeq +9 -> 169
            //     163: aload 7
            //     165: checkcast 110	java/lang/Error
            //     168: athrow
            //     169: new 112	java/lang/IllegalStateException
            //     172: dup
            //     173: aload 7
            //     175: invokespecial 113	java/lang/IllegalStateException:<init>	(Ljava/lang/Throwable;)V
            //     178: athrow
            //     179: astore 18
            //     181: aload_0
            //     182: iconst_1
            //     183: invokevirtual 86	android/accounts/AccountManager$AmsTask:cancel	(Z)Z
            //     186: pop
            //     187: new 57	android/accounts/OperationCanceledException
            //     190: dup
            //     191: invokespecial 98	android/accounts/OperationCanceledException:<init>	()V
            //     194: athrow
            //     195: astore 20
            //     197: goto -16 -> 181
            //     200: astore 5
            //     202: goto -21 -> 181
            //     205: astore 13
            //     207: goto -26 -> 181
            //     210: astore 15
            //     212: goto -31 -> 181
            //     215: astore_3
            //     216: goto -35 -> 181
            //
            // Exception table:
            //     from	to	target	type
            //     18	24	65	java/util/concurrent/CancellationException
            //     24	31	65	java/util/concurrent/CancellationException
            //     40	46	65	java/util/concurrent/CancellationException
            //     46	55	65	java/util/concurrent/CancellationException
            //     55	62	65	java/util/concurrent/CancellationException
            //     18	24	75	finally
            //     24	31	75	finally
            //     40	46	75	finally
            //     46	55	75	finally
            //     55	62	75	finally
            //     67	75	75	finally
            //     88	179	75	finally
            //     18	24	86	java/util/concurrent/ExecutionException
            //     24	31	86	java/util/concurrent/ExecutionException
            //     40	46	86	java/util/concurrent/ExecutionException
            //     46	55	86	java/util/concurrent/ExecutionException
            //     55	62	86	java/util/concurrent/ExecutionException
            //     18	24	179	java/lang/InterruptedException
            //     24	31	195	java/lang/InterruptedException
            //     40	46	200	java/lang/InterruptedException
            //     46	55	205	java/lang/InterruptedException
            //     55	62	210	java/lang/InterruptedException
            //     18	24	215	java/util/concurrent/TimeoutException
            //     24	31	215	java/util/concurrent/TimeoutException
            //     40	46	215	java/util/concurrent/TimeoutException
            //     46	55	215	java/util/concurrent/TimeoutException
            //     55	62	215	java/util/concurrent/TimeoutException
        }

        public abstract void doWork()
            throws RemoteException;

        protected void done()
        {
            if (this.mCallback != null)
                AccountManager.this.postToHandler(this.mHandler, this.mCallback, this);
        }

        public Bundle getResult()
            throws OperationCanceledException, IOException, AuthenticatorException
        {
            return internalGetResult(null, null);
        }

        public Bundle getResult(long paramLong, TimeUnit paramTimeUnit)
            throws OperationCanceledException, IOException, AuthenticatorException
        {
            return internalGetResult(Long.valueOf(paramLong), paramTimeUnit);
        }

        protected void set(Bundle paramBundle)
        {
            if (paramBundle == null)
                Log.e("AccountManager", "the bundle must not be null", new Exception());
            super.set(paramBundle);
        }

        public final AccountManagerFuture<Bundle> start()
        {
            try
            {
                doWork();
                return this;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    setException(localRemoteException);
            }
        }

        private class Response extends IAccountManagerResponse.Stub
        {
            private Response()
            {
            }

            public void onError(int paramInt, String paramString)
            {
                if (paramInt == 4)
                    AccountManager.AmsTask.this.cancel(true);
                while (true)
                {
                    return;
                    AccountManager.AmsTask.this.setException(AccountManager.access$400(AccountManager.this, paramInt, paramString));
                }
            }

            public void onResult(Bundle paramBundle)
            {
                Intent localIntent = (Intent)paramBundle.getParcelable("intent");
                if ((localIntent != null) && (AccountManager.AmsTask.this.mActivity != null))
                    AccountManager.AmsTask.this.mActivity.startActivity(localIntent);
                while (true)
                {
                    return;
                    if (paramBundle.getBoolean("retry"))
                        try
                        {
                            AccountManager.AmsTask.this.doWork();
                        }
                        catch (RemoteException localRemoteException)
                        {
                        }
                    else
                        AccountManager.AmsTask.this.set(paramBundle);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AccountManager
 * JD-Core Version:        0.6.2
 */