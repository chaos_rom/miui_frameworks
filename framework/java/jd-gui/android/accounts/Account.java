package android.accounts;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public class Account
    implements Parcelable
{
    public static final Parcelable.Creator<Account> CREATOR = new Parcelable.Creator()
    {
        public Account createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Account(paramAnonymousParcel);
        }

        public Account[] newArray(int paramAnonymousInt)
        {
            return new Account[paramAnonymousInt];
        }
    };
    public final String name;
    public final String type;

    public Account(Parcel paramParcel)
    {
        this.name = paramParcel.readString();
        this.type = paramParcel.readString();
    }

    public Account(String paramString1, String paramString2)
    {
        if (TextUtils.isEmpty(paramString1))
            throw new IllegalArgumentException("the name must not be empty: " + paramString1);
        if (TextUtils.isEmpty(paramString2))
            throw new IllegalArgumentException("the type must not be empty: " + paramString2);
        this.name = paramString1;
        this.type = paramString2;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof Account))
            {
                bool = false;
            }
            else
            {
                Account localAccount = (Account)paramObject;
                if ((!this.name.equals(localAccount.name)) || (!this.type.equals(localAccount.type)))
                    bool = false;
            }
        }
    }

    public int hashCode()
    {
        return 31 * (527 + this.name.hashCode()) + this.type.hashCode();
    }

    public String toString()
    {
        return "Account {name=" + this.name + ", type=" + this.type + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.name);
        paramParcel.writeString(this.type);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.Account
 * JD-Core Version:        0.6.2
 */