package android.accounts;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IAccountAuthenticatorResponse extends IInterface
{
    public abstract void onError(int paramInt, String paramString)
        throws RemoteException;

    public abstract void onRequestContinued()
        throws RemoteException;

    public abstract void onResult(Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccountAuthenticatorResponse
    {
        private static final String DESCRIPTOR = "android.accounts.IAccountAuthenticatorResponse";
        static final int TRANSACTION_onError = 3;
        static final int TRANSACTION_onRequestContinued = 2;
        static final int TRANSACTION_onResult = 1;

        public Stub()
        {
            attachInterface(this, "android.accounts.IAccountAuthenticatorResponse");
        }

        public static IAccountAuthenticatorResponse asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.accounts.IAccountAuthenticatorResponse");
                if ((localIInterface != null) && ((localIInterface instanceof IAccountAuthenticatorResponse)))
                    localObject = (IAccountAuthenticatorResponse)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.accounts.IAccountAuthenticatorResponse");
                continue;
                paramParcel1.enforceInterface("android.accounts.IAccountAuthenticatorResponse");
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                {
                    onResult(localBundle);
                    break;
                }
                paramParcel1.enforceInterface("android.accounts.IAccountAuthenticatorResponse");
                onRequestContinued();
                continue;
                paramParcel1.enforceInterface("android.accounts.IAccountAuthenticatorResponse");
                onError(paramParcel1.readInt(), paramParcel1.readString());
            }
        }

        private static class Proxy
            implements IAccountAuthenticatorResponse
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.accounts.IAccountAuthenticatorResponse";
            }

            public void onError(int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticatorResponse");
                    localParcel.writeInt(paramInt);
                    localParcel.writeString(paramString);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onRequestContinued()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticatorResponse");
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onResult(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accounts.IAccountAuthenticatorResponse");
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.IAccountAuthenticatorResponse
 * JD-Core Version:        0.6.2
 */