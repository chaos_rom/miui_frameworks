package android.accounts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class AccountAuthenticatorActivity extends Activity
{
    private AccountAuthenticatorResponse mAccountAuthenticatorResponse = null;
    private Bundle mResultBundle = null;

    public void finish()
    {
        if (this.mAccountAuthenticatorResponse != null)
        {
            if (this.mResultBundle == null)
                break label35;
            this.mAccountAuthenticatorResponse.onResult(this.mResultBundle);
        }
        while (true)
        {
            this.mAccountAuthenticatorResponse = null;
            super.finish();
            return;
            label35: this.mAccountAuthenticatorResponse.onError(4, "canceled");
        }
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        this.mAccountAuthenticatorResponse = ((AccountAuthenticatorResponse)getIntent().getParcelableExtra("accountAuthenticatorResponse"));
        if (this.mAccountAuthenticatorResponse != null)
            this.mAccountAuthenticatorResponse.onRequestContinued();
    }

    public final void setAccountAuthenticatorResult(Bundle paramBundle)
    {
        this.mResultBundle = paramBundle;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.AccountAuthenticatorActivity
 * JD-Core Version:        0.6.2
 */