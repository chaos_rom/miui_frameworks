package android.accounts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import java.io.IOException;

public class GrantCredentialsPermissionActivity extends Activity
    implements View.OnClickListener
{
    public static final String EXTRAS_ACCOUNT = "account";
    public static final String EXTRAS_ACCOUNT_TYPE_LABEL = "accountTypeLabel";
    public static final String EXTRAS_AUTH_TOKEN_LABEL = "authTokenLabel";
    public static final String EXTRAS_AUTH_TOKEN_TYPE = "authTokenType";
    public static final String EXTRAS_PACKAGES = "application";
    public static final String EXTRAS_REQUESTING_UID = "uid";
    public static final String EXTRAS_RESPONSE = "response";
    private Account mAccount;
    private String mAuthTokenType;
    protected LayoutInflater mInflater;
    private Bundle mResultBundle = null;
    private int mUid;

    private String getAccountLabel(Account paramAccount)
    {
        AuthenticatorDescription[] arrayOfAuthenticatorDescription = AccountManager.get(this).getAuthenticatorTypes();
        int i = 0;
        int j = arrayOfAuthenticatorDescription.length;
        AuthenticatorDescription localAuthenticatorDescription;
        if (i < j)
        {
            localAuthenticatorDescription = arrayOfAuthenticatorDescription[i];
            if (!localAuthenticatorDescription.type.equals(paramAccount.type));
        }
        while (true)
        {
            try
            {
                String str2 = createPackageContext(localAuthenticatorDescription.packageName, 0).getString(localAuthenticatorDescription.labelId);
                str1 = str2;
                return str1;
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                str1 = paramAccount.type;
                continue;
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                str1 = paramAccount.type;
                continue;
            }
            i++;
            break;
            String str1 = paramAccount.type;
        }
    }

    private View newPackageView(String paramString)
    {
        View localView = this.mInflater.inflate(17367163, null);
        ((TextView)localView.findViewById(16909055)).setText(paramString);
        return localView;
    }

    public void finish()
    {
        AccountAuthenticatorResponse localAccountAuthenticatorResponse = (AccountAuthenticatorResponse)getIntent().getParcelableExtra("response");
        if (localAccountAuthenticatorResponse != null)
        {
            if (this.mResultBundle == null)
                break label37;
            localAccountAuthenticatorResponse.onResult(this.mResultBundle);
        }
        while (true)
        {
            super.finish();
            return;
            label37: localAccountAuthenticatorResponse.onError(4, "canceled");
        }
    }

    public void onClick(View paramView)
    {
        switch (paramView.getId())
        {
        default:
        case 16908930:
        case 16908929:
        }
        while (true)
        {
            finish();
            return;
            AccountManager.get(this).updateAppPermission(this.mAccount, this.mAuthTokenType, this.mUid, true);
            Intent localIntent = new Intent();
            localIntent.putExtra("retry", true);
            setResult(-1, localIntent);
            setAccountAuthenticatorResult(localIntent.getExtras());
            continue;
            AccountManager.get(this).updateAppPermission(this.mAccount, this.mAuthTokenType, this.mUid, false);
            setResult(0);
        }
    }

    // ERROR //
    protected void onCreate(Bundle paramBundle)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokespecial 183	android/app/Activity:onCreate	(Landroid/os/Bundle;)V
        //     5: aload_0
        //     6: ldc 184
        //     8: invokevirtual 187	android/accounts/GrantCredentialsPermissionActivity:setContentView	(I)V
        //     11: aload_0
        //     12: ldc 188
        //     14: invokevirtual 191	android/accounts/GrantCredentialsPermissionActivity:setTitle	(I)V
        //     17: aload_0
        //     18: aload_0
        //     19: ldc 193
        //     21: invokevirtual 197	android/accounts/GrantCredentialsPermissionActivity:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     24: checkcast 98	android/view/LayoutInflater
        //     27: putfield 95	android/accounts/GrantCredentialsPermissionActivity:mInflater	Landroid/view/LayoutInflater;
        //     30: aload_0
        //     31: invokevirtual 120	android/accounts/GrantCredentialsPermissionActivity:getIntent	()Landroid/content/Intent;
        //     34: invokevirtual 172	android/content/Intent:getExtras	()Landroid/os/Bundle;
        //     37: astore_2
        //     38: aload_2
        //     39: ifnonnull +13 -> 52
        //     42: aload_0
        //     43: iconst_0
        //     44: invokevirtual 178	android/accounts/GrantCredentialsPermissionActivity:setResult	(I)V
        //     47: aload_0
        //     48: invokevirtual 147	android/accounts/GrantCredentialsPermissionActivity:finish	()V
        //     51: return
        //     52: aload_0
        //     53: aload_2
        //     54: ldc 12
        //     56: invokevirtual 202	android/os/Bundle:getParcelable	(Ljava/lang/String;)Landroid/os/Parcelable;
        //     59: checkcast 68	android/accounts/Account
        //     62: putfield 149	android/accounts/GrantCredentialsPermissionActivity:mAccount	Landroid/accounts/Account;
        //     65: aload_0
        //     66: aload_2
        //     67: ldc 21
        //     69: invokevirtual 205	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
        //     72: putfield 151	android/accounts/GrantCredentialsPermissionActivity:mAuthTokenType	Ljava/lang/String;
        //     75: aload_0
        //     76: aload_2
        //     77: ldc 27
        //     79: invokevirtual 209	android/os/Bundle:getInt	(Ljava/lang/String;)I
        //     82: putfield 153	android/accounts/GrantCredentialsPermissionActivity:mUid	I
        //     85: aload_0
        //     86: invokevirtual 213	android/accounts/GrantCredentialsPermissionActivity:getPackageManager	()Landroid/content/pm/PackageManager;
        //     89: astore_3
        //     90: aload_3
        //     91: aload_0
        //     92: getfield 153	android/accounts/GrantCredentialsPermissionActivity:mUid	I
        //     95: invokevirtual 219	android/content/pm/PackageManager:getPackagesForUid	(I)[Ljava/lang/String;
        //     98: astore 4
        //     100: aload_0
        //     101: getfield 149	android/accounts/GrantCredentialsPermissionActivity:mAccount	Landroid/accounts/Account;
        //     104: ifnull +15 -> 119
        //     107: aload_0
        //     108: getfield 151	android/accounts/GrantCredentialsPermissionActivity:mAuthTokenType	Ljava/lang/String;
        //     111: ifnull +8 -> 119
        //     114: aload 4
        //     116: ifnonnull +15 -> 131
        //     119: aload_0
        //     120: iconst_0
        //     121: invokevirtual 178	android/accounts/GrantCredentialsPermissionActivity:setResult	(I)V
        //     124: aload_0
        //     125: invokevirtual 147	android/accounts/GrantCredentialsPermissionActivity:finish	()V
        //     128: goto -77 -> 51
        //     131: aload_0
        //     132: aload_0
        //     133: getfield 149	android/accounts/GrantCredentialsPermissionActivity:mAccount	Landroid/accounts/Account;
        //     136: invokespecial 221	android/accounts/GrantCredentialsPermissionActivity:getAccountLabel	(Landroid/accounts/Account;)Ljava/lang/String;
        //     139: astore 6
        //     141: aload_0
        //     142: ldc 222
        //     144: invokevirtual 223	android/accounts/GrantCredentialsPermissionActivity:findViewById	(I)Landroid/view/View;
        //     147: checkcast 111	android/widget/TextView
        //     150: astore 7
        //     152: aload 7
        //     154: bipush 8
        //     156: invokevirtual 226	android/widget/TextView:setVisibility	(I)V
        //     159: new 8	android/accounts/GrantCredentialsPermissionActivity$1
        //     162: dup
        //     163: aload_0
        //     164: aload 7
        //     166: invokespecial 229	android/accounts/GrantCredentialsPermissionActivity$1:<init>	(Landroid/accounts/GrantCredentialsPermissionActivity;Landroid/widget/TextView;)V
        //     169: astore 8
        //     171: aload_0
        //     172: invokestatic 57	android/accounts/AccountManager:get	(Landroid/content/Context;)Landroid/accounts/AccountManager;
        //     175: aload_0
        //     176: getfield 149	android/accounts/GrantCredentialsPermissionActivity:mAccount	Landroid/accounts/Account;
        //     179: getfield 69	android/accounts/Account:type	Ljava/lang/String;
        //     182: aload_0
        //     183: getfield 151	android/accounts/GrantCredentialsPermissionActivity:mAuthTokenType	Ljava/lang/String;
        //     186: aload 8
        //     188: aconst_null
        //     189: invokevirtual 233	android/accounts/AccountManager:getAuthTokenLabel	(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
        //     192: pop
        //     193: aload_0
        //     194: ldc 234
        //     196: invokevirtual 223	android/accounts/GrantCredentialsPermissionActivity:findViewById	(I)Landroid/view/View;
        //     199: aload_0
        //     200: invokevirtual 238	android/view/View:setOnClickListener	(Landroid/view/View$OnClickListener;)V
        //     203: aload_0
        //     204: ldc 239
        //     206: invokevirtual 223	android/accounts/GrantCredentialsPermissionActivity:findViewById	(I)Landroid/view/View;
        //     209: aload_0
        //     210: invokevirtual 238	android/view/View:setOnClickListener	(Landroid/view/View$OnClickListener;)V
        //     213: aload_0
        //     214: ldc 240
        //     216: invokevirtual 223	android/accounts/GrantCredentialsPermissionActivity:findViewById	(I)Landroid/view/View;
        //     219: checkcast 242	android/widget/LinearLayout
        //     222: astore 10
        //     224: aload 4
        //     226: arraylength
        //     227: istore 11
        //     229: iconst_0
        //     230: istore 12
        //     232: iload 12
        //     234: iload 11
        //     236: if_icmpge +70 -> 306
        //     239: aload 4
        //     241: iload 12
        //     243: aaload
        //     244: astore 13
        //     246: aload_3
        //     247: aload_3
        //     248: aload 13
        //     250: iconst_0
        //     251: invokevirtual 246	android/content/pm/PackageManager:getApplicationInfo	(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
        //     254: invokevirtual 250	android/content/pm/PackageManager:getApplicationLabel	(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
        //     257: invokevirtual 256	java/lang/Object:toString	()Ljava/lang/String;
        //     260: astore 16
        //     262: aload 16
        //     264: astore 15
        //     266: aload 10
        //     268: aload_0
        //     269: aload 15
        //     271: invokespecial 258	android/accounts/GrantCredentialsPermissionActivity:newPackageView	(Ljava/lang/String;)Landroid/view/View;
        //     274: invokevirtual 261	android/widget/LinearLayout:addView	(Landroid/view/View;)V
        //     277: iinc 12 1
        //     280: goto -48 -> 232
        //     283: astore 5
        //     285: aload_0
        //     286: iconst_0
        //     287: invokevirtual 178	android/accounts/GrantCredentialsPermissionActivity:setResult	(I)V
        //     290: aload_0
        //     291: invokevirtual 147	android/accounts/GrantCredentialsPermissionActivity:finish	()V
        //     294: goto -243 -> 51
        //     297: astore 14
        //     299: aload 13
        //     301: astore 15
        //     303: goto -37 -> 266
        //     306: aload_0
        //     307: ldc_w 262
        //     310: invokevirtual 223	android/accounts/GrantCredentialsPermissionActivity:findViewById	(I)Landroid/view/View;
        //     313: checkcast 111	android/widget/TextView
        //     316: aload_0
        //     317: getfield 149	android/accounts/GrantCredentialsPermissionActivity:mAccount	Landroid/accounts/Account;
        //     320: getfield 265	android/accounts/Account:name	Ljava/lang/String;
        //     323: invokevirtual 115	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
        //     326: aload_0
        //     327: ldc_w 266
        //     330: invokevirtual 223	android/accounts/GrantCredentialsPermissionActivity:findViewById	(I)Landroid/view/View;
        //     333: checkcast 111	android/widget/TextView
        //     336: aload 6
        //     338: invokevirtual 115	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
        //     341: goto -290 -> 51
        //
        // Exception table:
        //     from	to	target	type
        //     131	141	283	java/lang/IllegalArgumentException
        //     246	262	297	android/content/pm/PackageManager$NameNotFoundException
    }

    public final void setAccountAuthenticatorResult(Bundle paramBundle)
    {
        this.mResultBundle = paramBundle;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.GrantCredentialsPermissionActivity
 * JD-Core Version:        0.6.2
 */