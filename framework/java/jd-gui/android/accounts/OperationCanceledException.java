package android.accounts;

public class OperationCanceledException extends AccountsException
{
    public OperationCanceledException()
    {
    }

    public OperationCanceledException(String paramString)
    {
        super(paramString);
    }

    public OperationCanceledException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public OperationCanceledException(Throwable paramThrowable)
    {
        super(paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accounts.OperationCanceledException
 * JD-Core Version:        0.6.2
 */