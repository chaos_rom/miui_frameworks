package android.server;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.IBluetoothA2dp.Stub;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.Settings.Secure;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class BluetoothA2dpService extends IBluetoothA2dp.Stub
{
    public static final String BLUETOOTH_A2DP_SERVICE = "bluetooth_a2dp";
    private static final String BLUETOOTH_ADMIN_PERM = "android.permission.BLUETOOTH_ADMIN";
    private static final String BLUETOOTH_ENABLED = "bluetooth_enabled";
    private static final String BLUETOOTH_PERM = "android.permission.BLUETOOTH";
    private static final boolean DBG = true;
    private static final int MSG_CONNECTION_STATE_CHANGED = 0;
    private static final String PROPERTY_STATE = "State";
    private static final String TAG = "BluetoothA2dpService";
    private final BluetoothAdapter mAdapter;
    private HashMap<BluetoothDevice, Integer> mAudioDevices;
    private final AudioManager mAudioManager;
    private final BluetoothService mBluetoothService;
    private final Context mContext;
    private IntentBroadcastHandler mIntentBroadcastHandler;
    private final IntentFilter mIntentFilter;
    private BluetoothDevice mPlayingA2dpDevice;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str1 = paramAnonymousIntent.getAction();
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)paramAnonymousIntent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if (str1.equals("android.bluetooth.adapter.action.STATE_CHANGED"))
                switch (paramAnonymousIntent.getIntExtra("android.bluetooth.adapter.extra.STATE", -2147483648))
                {
                default:
                case 12:
                case 13:
                }
            while (true)
            {
                return;
                BluetoothA2dpService.this.onBluetoothEnable();
                continue;
                BluetoothA2dpService.this.onBluetoothDisable();
                continue;
                if (str1.equals("android.bluetooth.device.action.ACL_DISCONNECTED"))
                {
                    try
                    {
                        if (BluetoothA2dpService.this.mAudioDevices.containsKey(localBluetoothDevice))
                        {
                            int k = ((Integer)BluetoothA2dpService.this.mAudioDevices.get(localBluetoothDevice)).intValue();
                            BluetoothA2dpService.this.handleSinkStateChange(localBluetoothDevice, k, 0);
                        }
                        continue;
                    }
                    finally
                    {
                        localObject = finally;
                        throw localObject;
                    }
                }
                else if ((str1.equals("android.media.VOLUME_CHANGED_ACTION")) && (paramAnonymousIntent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1) == 3))
                {
                    List localList = BluetoothA2dpService.this.getConnectedDevices();
                    if ((localList.size() != 0) && (BluetoothA2dpService.this.isPhoneDocked((BluetoothDevice)localList.get(0))))
                    {
                        String str2 = ((BluetoothDevice)localList.get(0)).getAddress();
                        int i = paramAnonymousIntent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", 0);
                        int j = paramAnonymousIntent.getIntExtra("android.media.EXTRA_PREV_VOLUME_STREAM_VALUE", 0);
                        String str3 = BluetoothA2dpService.this.mBluetoothService.getObjectPathFromAddress(str2);
                        if (i > j)
                            BluetoothA2dpService.this.avrcpVolumeUpNative(str3);
                        else if (i < j)
                            BluetoothA2dpService.this.avrcpVolumeDownNative(str3);
                    }
                }
            }
        }
    };
    private int mTargetA2dpState;
    private final PowerManager.WakeLock mWakeLock;

    public BluetoothA2dpService(Context paramContext, BluetoothService paramBluetoothService)
    {
        this.mContext = paramContext;
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "BluetoothA2dpService");
        this.mIntentBroadcastHandler = new IntentBroadcastHandler(null);
        this.mAudioManager = ((AudioManager)paramContext.getSystemService("audio"));
        this.mBluetoothService = paramBluetoothService;
        if (this.mBluetoothService == null)
            throw new RuntimeException("Platform does not support Bluetooth");
        if (!initNative())
            throw new RuntimeException("Could not init BluetoothA2dpService");
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mIntentFilter = new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED");
        this.mIntentFilter.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        this.mIntentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        this.mIntentFilter.addAction("android.media.VOLUME_CHANGED_ACTION");
        this.mContext.registerReceiver(this.mReceiver, this.mIntentFilter);
        this.mAudioDevices = new HashMap();
        if (this.mBluetoothService.isEnabled())
            onBluetoothEnable();
        this.mTargetA2dpState = -1;
        this.mBluetoothService.setA2dpService(this);
    }

    /** @deprecated */
    private void addAudioSink(BluetoothDevice paramBluetoothDevice)
    {
        try
        {
            if (this.mAudioDevices.get(paramBluetoothDevice) == null)
                this.mAudioDevices.put(paramBluetoothDevice, Integer.valueOf(0));
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void adjustOtherSinkPriorities(BluetoothDevice paramBluetoothDevice)
    {
        Iterator localIterator = this.mAdapter.getBondedDevices().iterator();
        while (localIterator.hasNext())
        {
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
            if ((getPriority(localBluetoothDevice) >= 1000) && (!localBluetoothDevice.equals(paramBluetoothDevice)))
                setPriority(localBluetoothDevice, 100);
        }
    }

    /** @deprecated */
    private synchronized native boolean avrcpVolumeDownNative(String paramString);

    /** @deprecated */
    private synchronized native boolean avrcpVolumeUpNative(String paramString);

    private boolean checkSinkSuspendState(int paramInt)
    {
        boolean bool = true;
        if (paramInt != this.mTargetA2dpState)
        {
            if ((paramInt != 10) || (this.mTargetA2dpState != 2))
                break label35;
            this.mAudioManager.setParameters("A2dpSuspended=true");
        }
        while (true)
        {
            return bool;
            label35: if ((paramInt == 2) && (this.mTargetA2dpState == 10))
                this.mAudioManager.setParameters("A2dpSuspended=false");
            else
                bool = false;
        }
    }

    private native void cleanupNative();

    /** @deprecated */
    private synchronized native boolean connectSinkNative(String paramString);

    private int convertBluezSinkStringToState(String paramString)
    {
        int i;
        if (paramString.equalsIgnoreCase("disconnected"))
            i = 0;
        while (true)
        {
            return i;
            if (paramString.equalsIgnoreCase("connecting"))
                i = 1;
            else if (paramString.equalsIgnoreCase("connected"))
                i = 2;
            else if (paramString.equalsIgnoreCase("playing"))
                i = 10;
            else
                i = -1;
        }
    }

    /** @deprecated */
    private synchronized native boolean disconnectSinkNative(String paramString);

    /** @deprecated */
    private synchronized native Object[] getSinkPropertiesNative(String paramString);

    private void handleSinkPlayingStateChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2)
    {
        Intent localIntent = new Intent("android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED");
        localIntent.putExtra("android.bluetooth.device.extra.DEVICE", paramBluetoothDevice);
        localIntent.putExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", paramInt2);
        localIntent.putExtra("android.bluetooth.profile.extra.STATE", paramInt1);
        localIntent.addFlags(134217728);
        this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
        log("A2DP Playing state : device: " + paramBluetoothDevice + " State:" + paramInt2 + "->" + paramInt1);
    }

    private void handleSinkStateChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2)
    {
        if (paramInt2 != paramInt1)
        {
            this.mAudioDevices.put(paramBluetoothDevice, Integer.valueOf(paramInt2));
            checkSinkSuspendState(paramInt2);
            this.mTargetA2dpState = -1;
            if ((getPriority(paramBluetoothDevice) > 0) && (paramInt2 == 2))
            {
                setPriority(paramBluetoothDevice, 1000);
                adjustOtherSinkPriorities(paramBluetoothDevice);
            }
            int i = this.mAudioManager.setBluetoothA2dpDeviceConnectionState(paramBluetoothDevice, paramInt2);
            this.mWakeLock.acquire();
            this.mIntentBroadcastHandler.sendMessageDelayed(this.mIntentBroadcastHandler.obtainMessage(0, paramInt1, paramInt2, paramBluetoothDevice), i);
        }
    }

    private native boolean initNative();

    /** @deprecated */
    private boolean isConnectSinkFeasible(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        try
        {
            if ((this.mBluetoothService.isEnabled()) && (isSinkDevice(paramBluetoothDevice)))
            {
                int i = getPriority(paramBluetoothDevice);
                if (i != 0)
                    break label38;
            }
            while (true)
            {
                return bool;
                label38: addAudioSink(paramBluetoothDevice);
                String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
                if (str != null)
                    bool = true;
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    private boolean isDisconnectSinkFeasible(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        try
        {
            String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
            if (str == null);
            while (true)
            {
                return bool;
                int i = getConnectionState(paramBluetoothDevice);
                switch (i)
                {
                case 0:
                case 3:
                case 1:
                case 2:
                }
                bool = true;
            }
        }
        finally
        {
        }
    }

    private boolean isPhoneDocked(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        Intent localIntent = this.mContext.registerReceiver(null, new IntentFilter("android.intent.action.DOCK_EVENT"));
        if ((localIntent != null) && (localIntent.getIntExtra("android.intent.extra.DOCK_STATE", 0) != 0))
        {
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIntent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if ((localBluetoothDevice != null) && (paramBluetoothDevice.equals(localBluetoothDevice)))
                bool = true;
        }
        return bool;
    }

    private boolean isSinkDevice(BluetoothDevice paramBluetoothDevice)
    {
        ParcelUuid[] arrayOfParcelUuid = this.mBluetoothService.getRemoteUuids(paramBluetoothDevice.getAddress());
        if ((arrayOfParcelUuid != null) && (BluetoothUuid.isUuidPresent(arrayOfParcelUuid, BluetoothUuid.AudioSink)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothA2dpService", paramString);
    }

    /** @deprecated */
    private void onBluetoothDisable()
    {
        while (true)
        {
            int j;
            BluetoothDevice localBluetoothDevice;
            try
            {
                if (this.mAudioDevices.isEmpty())
                    break label164;
                BluetoothDevice[] arrayOfBluetoothDevice1 = new BluetoothDevice[this.mAudioDevices.size()];
                BluetoothDevice[] arrayOfBluetoothDevice2 = (BluetoothDevice[])this.mAudioDevices.keySet().toArray(arrayOfBluetoothDevice1);
                int i = arrayOfBluetoothDevice2.length;
                j = 0;
                if (j >= i)
                    break label157;
                localBluetoothDevice = arrayOfBluetoothDevice2[j];
                int k = getConnectionState(localBluetoothDevice);
                switch (k)
                {
                case 1:
                case 2:
                case 10:
                    disconnectSinkNative(this.mBluetoothService.getObjectPathFromAddress(localBluetoothDevice.getAddress()));
                    handleSinkStateChange(localBluetoothDevice, k, 0);
                case 3:
                }
            }
            finally
            {
            }
            handleSinkStateChange(localBluetoothDevice, 3, 0);
            break label177;
            label157: this.mAudioDevices.clear();
            label164: this.mAudioManager.setParameters("bluetooth_enabled=false");
            return;
            label177: j++;
        }
    }

    /** @deprecated */
    private void onBluetoothEnable()
    {
        while (true)
        {
            int j;
            try
            {
                String str1 = this.mBluetoothService.getProperty("Devices", true);
                if (str1 != null)
                {
                    String[] arrayOfString = str1.split(",");
                    int i = arrayOfString.length;
                    j = 0;
                    if (j < i)
                    {
                        String str2 = arrayOfString[j];
                        String str3 = this.mBluetoothService.getAddressFromObjectPath(str2);
                        BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(str3);
                        ParcelUuid[] arrayOfParcelUuid1 = this.mBluetoothService.getRemoteUuids(str3);
                        if (arrayOfParcelUuid1 == null)
                            break label150;
                        ParcelUuid[] arrayOfParcelUuid2 = new ParcelUuid[2];
                        arrayOfParcelUuid2[0] = BluetoothUuid.AudioSink;
                        arrayOfParcelUuid2[1] = BluetoothUuid.AdvAudioDist;
                        if (!BluetoothUuid.containsAnyUuid(arrayOfParcelUuid1, arrayOfParcelUuid2))
                            break label150;
                        addAudioSink(localBluetoothDevice);
                        break label150;
                    }
                }
                this.mAudioManager.setParameters("bluetooth_enabled=true");
                this.mAudioManager.setParameters("A2dpSuspended=false");
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            label150: j++;
        }
    }

    private void onConnectSinkResult(String paramString, boolean paramBoolean)
    {
        String str;
        if ((!paramBoolean) && (paramString != null))
        {
            str = this.mBluetoothService.getAddressFromObjectPath(paramString);
            if (str != null)
                break label22;
        }
        while (true)
        {
            return;
            label22: BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(str);
            handleSinkStateChange(localBluetoothDevice, getConnectionState(localBluetoothDevice), 0);
        }
    }

    /** @deprecated */
    private void onSinkPropertyChanged(String paramString, String[] paramArrayOfString)
    {
        while (true)
        {
            String str1;
            String str2;
            try
            {
                boolean bool = this.mBluetoothService.isEnabled();
                if (!bool)
                    return;
                str1 = paramArrayOfString[0];
                str2 = this.mBluetoothService.getAddressFromObjectPath(paramString);
                if (str2 == null)
                {
                    Log.e("BluetoothA2dpService", "onSinkPropertyChanged: Address of the remote device in null");
                    continue;
                }
            }
            finally
            {
            }
            BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(str2);
            if (str1.equals("State"))
            {
                int i = convertBluezSinkStringToState(paramArrayOfString[1]);
                log("A2DP: onSinkPropertyChanged newState is: " + i + "mPlayingA2dpDevice: " + this.mPlayingA2dpDevice);
                if (this.mAudioDevices.get(localBluetoothDevice) == null)
                {
                    addAudioSink(localBluetoothDevice);
                    handleSinkStateChange(localBluetoothDevice, 0, i);
                }
                else if ((i == 10) && (this.mPlayingA2dpDevice == null))
                {
                    this.mPlayingA2dpDevice = localBluetoothDevice;
                    handleSinkPlayingStateChange(localBluetoothDevice, i, 11);
                }
                else if ((i == 2) && (this.mPlayingA2dpDevice != null))
                {
                    this.mPlayingA2dpDevice = null;
                    handleSinkPlayingStateChange(localBluetoothDevice, 11, 10);
                }
                else
                {
                    this.mPlayingA2dpDevice = null;
                    handleSinkStateChange(localBluetoothDevice, ((Integer)this.mAudioDevices.get(localBluetoothDevice)).intValue(), i);
                }
            }
        }
    }

    /** @deprecated */
    private synchronized native boolean resumeSinkNative(String paramString);

    /** @deprecated */
    private synchronized native boolean suspendSinkNative(String paramString);

    /** @deprecated */
    public boolean allowIncomingConnect(BluetoothDevice paramBluetoothDevice, boolean paramBoolean)
    {
        boolean bool1 = false;
        while (true)
        {
            String str;
            Integer localInteger;
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
                str = paramBluetoothDevice.getAddress();
                boolean bool2 = BluetoothAdapter.checkBluetoothAddress(str);
                if (!bool2)
                    return bool1;
                localInteger = this.mBluetoothService.getAuthorizationAgentRequestData(str);
                if (localInteger == null)
                {
                    Log.w("BluetoothA2dpService", "allowIncomingConnect(" + paramBluetoothDevice + ") called but no native data available");
                    continue;
                }
            }
            finally
            {
            }
            log("allowIncomingConnect: A2DP: " + paramBluetoothDevice + ":" + paramBoolean);
            boolean bool3 = this.mBluetoothService.setAuthorizationNative(str, paramBoolean, localInteger.intValue());
            bool1 = bool3;
        }
    }

    /** @deprecated */
    public boolean connect(BluetoothDevice paramBluetoothDevice)
    {
        while (true)
        {
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
                log("connectSink(" + paramBluetoothDevice + ")");
                boolean bool1 = isConnectSinkFeasible(paramBluetoothDevice);
                if (!bool1)
                {
                    bool3 = false;
                    return bool3;
                }
                Iterator localIterator = this.mAudioDevices.keySet().iterator();
                if (localIterator.hasNext())
                {
                    BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
                    if (getConnectionState(localBluetoothDevice) == 0)
                        continue;
                    disconnect(localBluetoothDevice);
                    continue;
                }
            }
            finally
            {
            }
            boolean bool2 = this.mBluetoothService.connectSink(paramBluetoothDevice.getAddress());
            boolean bool3 = bool2;
        }
    }

    /** @deprecated */
    public boolean connectSinkInternal(BluetoothDevice paramBluetoothDevice)
    {
        int i;
        try
        {
            boolean bool1 = this.mBluetoothService.isEnabled();
            boolean bool2;
            if (!bool1)
                bool2 = false;
            while (true)
            {
                return bool2;
                i = ((Integer)this.mAudioDevices.get(paramBluetoothDevice)).intValue();
                int[] arrayOfInt = new int[3];
                arrayOfInt[0] = 1;
                arrayOfInt[1] = 2;
                arrayOfInt[2] = 3;
                if (getDevicesMatchingConnectionStates(arrayOfInt).size() == 0)
                    break;
                bool2 = false;
                continue;
                String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
                if (getPriority(paramBluetoothDevice) < 1000)
                    setPriority(paramBluetoothDevice, 1000);
                handleSinkStateChange(paramBluetoothDevice, i, 1);
                if (!connectSinkNative(str))
                {
                    handleSinkStateChange(paramBluetoothDevice, ((Integer)this.mAudioDevices.get(paramBluetoothDevice)).intValue(), i);
                    bool2 = false;
                    continue;
                    bool2 = false;
                    continue;
                    bool2 = true;
                }
                else
                {
                    bool2 = true;
                }
            }
        }
        finally
        {
        }
        switch (i)
        {
        default:
        case 2:
        case 3:
        case 1:
        }
    }

    /** @deprecated */
    public boolean disconnect(BluetoothDevice paramBluetoothDevice)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            log("disconnectSink(" + paramBluetoothDevice + ")");
            boolean bool1 = isDisconnectSinkFeasible(paramBluetoothDevice);
            if (!bool1);
            boolean bool2;
            for (boolean bool3 = false; ; bool3 = bool2)
            {
                return bool3;
                bool2 = this.mBluetoothService.disconnectSink(paramBluetoothDevice.getAddress());
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean disconnectSinkInternal(BluetoothDevice paramBluetoothDevice)
    {
        try
        {
            int i = getConnectionState(paramBluetoothDevice);
            String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
            switch (i)
            {
            case 1:
            case 2:
            default:
                handleSinkStateChange(paramBluetoothDevice, i, 3);
                if (!disconnectSinkNative(str))
                    handleSinkStateChange(paramBluetoothDevice, ((Integer)this.mAudioDevices.get(paramBluetoothDevice)).intValue(), i);
                break;
            case 0:
            case 3:
                for (bool = false; ; bool = false)
                    return bool;
            }
            boolean bool = true;
        }
        finally
        {
        }
    }

    /** @deprecated */
    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "BluetoothA2dpService");
            boolean bool = this.mAudioDevices.isEmpty();
            if (bool);
            while (true)
            {
                return;
                paramPrintWriter.println("Cached audio devices:");
                Iterator localIterator = this.mAudioDevices.keySet().iterator();
                while (localIterator.hasNext())
                {
                    BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
                    int i = ((Integer)this.mAudioDevices.get(localBluetoothDevice)).intValue();
                    paramPrintWriter.println(localBluetoothDevice + " " + BluetoothA2dp.stateToString(i));
                }
            }
        }
        finally
        {
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            cleanupNative();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    /** @deprecated */
    public List<BluetoothDevice> getConnectedDevices()
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            int[] arrayOfInt = new int[1];
            arrayOfInt[0] = 2;
            List localList = getDevicesMatchingConnectionStates(arrayOfInt);
            return localList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int getConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            Integer localInteger = (Integer)this.mAudioDevices.get(paramBluetoothDevice);
            if (localInteger == null);
            int i;
            for (int j = 0; ; j = i)
            {
                return j;
                i = localInteger.intValue();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        ArrayList localArrayList;
        while (true)
        {
            int k;
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
                localArrayList = new ArrayList();
                Iterator localIterator = this.mAudioDevices.keySet().iterator();
                if (!localIterator.hasNext())
                    break;
                BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
                int i = getConnectionState(localBluetoothDevice);
                int j = paramArrayOfInt.length;
                k = 0;
                if (k < j)
                    if (paramArrayOfInt[k] == i)
                        localArrayList.add(localBluetoothDevice);
            }
            finally
            {
            }
        }
        return localArrayList;
    }

    /** @deprecated */
    public int getPriority(BluetoothDevice paramBluetoothDevice)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            int i = Settings.Secure.getInt(this.mContext.getContentResolver(), Settings.Secure.getBluetoothA2dpSinkPriorityKey(paramBluetoothDevice.getAddress()), -1);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean isA2dpPlaying(BluetoothDevice paramBluetoothDevice)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            log("isA2dpPlaying(" + paramBluetoothDevice + ")");
            boolean bool1 = paramBluetoothDevice.equals(this.mPlayingA2dpDevice);
            if (bool1)
            {
                bool2 = true;
                return bool2;
            }
            boolean bool2 = false;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean resumeSink(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            log("resumeSink(" + paramBluetoothDevice + "), mTargetA2dpState: " + this.mTargetA2dpState);
            if (paramBluetoothDevice != null)
            {
                HashMap localHashMap = this.mAudioDevices;
                if (localHashMap != null)
                    break label71;
            }
            while (true)
            {
                return bool1;
                label71: String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
                Integer localInteger = (Integer)this.mAudioDevices.get(paramBluetoothDevice);
                if ((str != null) && (localInteger != null))
                {
                    this.mTargetA2dpState = 10;
                    boolean bool2 = checkSinkSuspendState(localInteger.intValue());
                    bool1 = bool2;
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean setPriority(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            boolean bool = Settings.Secure.putInt(this.mContext.getContentResolver(), Settings.Secure.getBluetoothA2dpSinkPriorityKey(paramBluetoothDevice.getAddress()), paramInt);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean suspendSink(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            log("suspendSink(" + paramBluetoothDevice + "), mTargetA2dpState: " + this.mTargetA2dpState);
            if (paramBluetoothDevice != null)
            {
                HashMap localHashMap = this.mAudioDevices;
                if (localHashMap != null)
                    break label71;
            }
            while (true)
            {
                return bool1;
                label71: String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
                Integer localInteger = (Integer)this.mAudioDevices.get(paramBluetoothDevice);
                if ((str != null) && (localInteger != null))
                {
                    this.mTargetA2dpState = 2;
                    boolean bool2 = checkSinkSuspendState(localInteger.intValue());
                    bool1 = bool2;
                }
            }
        }
        finally
        {
        }
    }

    private class IntentBroadcastHandler extends Handler
    {
        private IntentBroadcastHandler()
        {
        }

        private void onConnectionStateChanged(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2)
        {
            Intent localIntent = new Intent("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", paramBluetoothDevice);
            localIntent.putExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", paramInt1);
            localIntent.putExtra("android.bluetooth.profile.extra.STATE", paramInt2);
            localIntent.addFlags(134217728);
            BluetoothA2dpService.this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
            BluetoothA2dpService.log("A2DP state : device: " + paramBluetoothDevice + " State:" + paramInt1 + "->" + paramInt2);
            BluetoothA2dpService.this.mBluetoothService.sendConnectionStateChange(paramBluetoothDevice, 2, paramInt2, paramInt1);
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 0:
            }
            while (true)
            {
                return;
                onConnectionStateChanged((BluetoothDevice)paramMessage.obj, paramMessage.arg1, paramMessage.arg2);
                BluetoothA2dpService.this.mWakeLock.release();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothA2dpService
 * JD-Core Version:        0.6.2
 */