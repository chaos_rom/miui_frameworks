package android.server;

import android.content.Context;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

class BluetoothAdapterProperties
{
    private static final String TAG = "BluetoothAdapterProperties";
    private final Context mContext;
    private final Map<String, String> mPropertiesMap = new HashMap();
    private final BluetoothService mService;

    BluetoothAdapterProperties(Context paramContext, BluetoothService paramBluetoothService)
    {
        this.mContext = paramContext;
        this.mService = paramBluetoothService;
    }

    /** @deprecated */
    void clear()
    {
        try
        {
            this.mPropertiesMap.clear();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void getAllProperties()
    {
        while (true)
        {
            String[] arrayOfString;
            int i;
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
                this.mPropertiesMap.clear();
                arrayOfString = (String[])this.mService.getAdapterPropertiesNative();
                if (arrayOfString == null)
                {
                    Log.e("BluetoothAdapterProperties", "*Error*: GetAdapterProperties returned NULL");
                    return;
                }
                i = 0;
                if (i >= arrayOfString.length)
                    break label229;
                String str2 = arrayOfString[i];
                str3 = null;
                if (str2 == null)
                {
                    Log.e("BluetoothAdapterProperties", "Error:Adapter Property at index " + i + " is null");
                }
                else if ((str2.equals("Devices")) || (str2.equals("UUIDs")))
                {
                    StringBuilder localStringBuilder = new StringBuilder();
                    i++;
                    int j = Integer.valueOf(arrayOfString[i]).intValue();
                    int k = 0;
                    if (k < j)
                    {
                        i++;
                        localStringBuilder.append(arrayOfString[i]);
                        localStringBuilder.append(",");
                        k++;
                        continue;
                    }
                    if (j > 0)
                        str3 = localStringBuilder.toString();
                    this.mPropertiesMap.put(str2, str3);
                }
            }
            finally
            {
            }
            i++;
            String str3 = arrayOfString[i];
            continue;
            label229: String str1 = this.mService.getAdapterPathNative();
            if (str1 != null)
            {
                this.mPropertiesMap.put("ObjectPath", str1 + "/dev_");
                continue;
                i++;
            }
        }
    }

    String getObjectPath()
    {
        return getProperty("ObjectPath");
    }

    /** @deprecated */
    String getProperty(String paramString)
    {
        try
        {
            if (this.mPropertiesMap.isEmpty())
                getAllProperties();
            String str = (String)this.mPropertiesMap.get(paramString);
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    boolean isEmpty()
    {
        try
        {
            boolean bool = this.mPropertiesMap.isEmpty();
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void setProperty(String paramString1, String paramString2)
    {
        try
        {
            this.mPropertiesMap.put(paramString1, paramString2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothAdapterProperties
 * JD-Core Version:        0.6.2
 */