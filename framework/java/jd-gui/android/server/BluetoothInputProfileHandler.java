package android.server;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothDeviceProfileState;
import android.bluetooth.BluetoothProfileState;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class BluetoothInputProfileHandler
{
    private static final boolean DBG = true;
    private static final String TAG = "BluetoothInputProfileHandler";
    public static BluetoothInputProfileHandler sInstance;
    private BluetoothService mBluetoothService;
    private Context mContext;
    private final BluetoothProfileState mHidProfileState;
    private final HashMap<BluetoothDevice, Integer> mInputDevices;

    private BluetoothInputProfileHandler(Context paramContext, BluetoothService paramBluetoothService)
    {
        this.mContext = paramContext;
        this.mBluetoothService = paramBluetoothService;
        this.mInputDevices = new HashMap();
        this.mHidProfileState = new BluetoothProfileState(this.mContext, 2);
        this.mHidProfileState.start();
    }

    private static void debugLog(String paramString)
    {
        Log.d("BluetoothInputProfileHandler", paramString);
    }

    private static void errorLog(String paramString)
    {
        Log.e("BluetoothInputProfileHandler", paramString);
    }

    /** @deprecated */
    static BluetoothInputProfileHandler getInstance(Context paramContext, BluetoothService paramBluetoothService)
    {
        try
        {
            if (sInstance == null)
                sInstance = new BluetoothInputProfileHandler(paramContext, paramBluetoothService);
            BluetoothInputProfileHandler localBluetoothInputProfileHandler = sInstance;
            return localBluetoothInputProfileHandler;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void handleInputDeviceStateChange(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        int i;
        if (this.mInputDevices.get(paramBluetoothDevice) == null)
        {
            i = 0;
            if (i != paramInt)
                break label37;
        }
        while (true)
        {
            return;
            i = ((Integer)this.mInputDevices.get(paramBluetoothDevice)).intValue();
            break;
            label37: this.mInputDevices.put(paramBluetoothDevice, Integer.valueOf(paramInt));
            if (((getInputDevicePriority(paramBluetoothDevice) > 0) && (paramInt == 1)) || (paramInt == 2))
                setInputDevicePriority(paramBluetoothDevice, 1000);
            Intent localIntent = new Intent("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", paramBluetoothDevice);
            localIntent.putExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", i);
            localIntent.putExtra("android.bluetooth.profile.extra.STATE", paramInt);
            localIntent.addFlags(134217728);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
            debugLog("InputDevice state : device: " + paramBluetoothDevice + " State:" + i + "->" + paramInt);
            this.mBluetoothService.sendConnectionStateChange(paramBluetoothDevice, 4, paramInt, i);
        }
    }

    boolean connectInputDevice(BluetoothDevice paramBluetoothDevice, BluetoothDeviceProfileState paramBluetoothDeviceProfileState)
    {
        boolean bool = false;
        if ((this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress()) == null) || (getInputDeviceConnectionState(paramBluetoothDevice) != 0) || (getInputDevicePriority(paramBluetoothDevice) == 0));
        while (true)
        {
            return bool;
            if (paramBluetoothDeviceProfileState != null)
            {
                Message localMessage = new Message();
                localMessage.arg1 = 5;
                localMessage.obj = paramBluetoothDeviceProfileState;
                this.mHidProfileState.sendMessage(localMessage);
                bool = true;
            }
        }
    }

    boolean connectInputDeviceInternal(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
        handleInputDeviceStateChange(paramBluetoothDevice, 1);
        if (!this.mBluetoothService.connectInputDeviceNative(str))
            handleInputDeviceStateChange(paramBluetoothDevice, 0);
        while (true)
        {
            return bool;
            bool = true;
        }
    }

    boolean disconnectInputDevice(BluetoothDevice paramBluetoothDevice, BluetoothDeviceProfileState paramBluetoothDeviceProfileState)
    {
        boolean bool = false;
        if ((this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress()) == null) || (getInputDeviceConnectionState(paramBluetoothDevice) == 0));
        while (true)
        {
            return bool;
            if (paramBluetoothDeviceProfileState != null)
            {
                Message localMessage = new Message();
                localMessage.arg1 = 54;
                localMessage.obj = paramBluetoothDeviceProfileState;
                this.mHidProfileState.sendMessage(localMessage);
                bool = true;
            }
        }
    }

    boolean disconnectInputDeviceInternal(BluetoothDevice paramBluetoothDevice)
    {
        String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
        handleInputDeviceStateChange(paramBluetoothDevice, 3);
        if (!this.mBluetoothService.disconnectInputDeviceNative(str))
            handleInputDeviceStateChange(paramBluetoothDevice, 2);
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    List<BluetoothDevice> getConnectedInputDevices()
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 2;
        return lookupInputDevicesMatchingStates(arrayOfInt);
    }

    int getInputDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        if (this.mInputDevices.get(paramBluetoothDevice) == null);
        for (int i = 0; ; i = ((Integer)this.mInputDevices.get(paramBluetoothDevice)).intValue())
            return i;
    }

    int getInputDevicePriority(BluetoothDevice paramBluetoothDevice)
    {
        return Settings.Secure.getInt(this.mContext.getContentResolver(), Settings.Secure.getBluetoothInputDevicePriorityKey(paramBluetoothDevice.getAddress()), -1);
    }

    List<BluetoothDevice> getInputDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        return lookupInputDevicesMatchingStates(paramArrayOfInt);
    }

    void handleInputDevicePropertyChange(String paramString, boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 2; ; i = 0)
        {
            handleInputDeviceStateChange(BluetoothAdapter.getDefaultAdapter().getRemoteDevice(paramString), i);
            return;
        }
    }

    List<BluetoothDevice> lookupInputDevicesMatchingStates(int[] paramArrayOfInt)
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mInputDevices.keySet().iterator();
        label88: 
        while (localIterator.hasNext())
        {
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
            int i = getInputDeviceConnectionState(localBluetoothDevice);
            int j = paramArrayOfInt.length;
            for (int k = 0; ; k++)
            {
                if (k >= j)
                    break label88;
                if (paramArrayOfInt[k] == i)
                {
                    localArrayList.add(localBluetoothDevice);
                    break;
                }
            }
        }
        return localArrayList;
    }

    void setInitialInputDevicePriority(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        switch (paramInt)
        {
        case 11:
        default:
        case 12:
        case 10:
        }
        while (true)
        {
            return;
            if (getInputDevicePriority(paramBluetoothDevice) == -1)
            {
                setInputDevicePriority(paramBluetoothDevice, 100);
                continue;
                setInputDevicePriority(paramBluetoothDevice, -1);
            }
        }
    }

    boolean setInputDevicePriority(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        if (!BluetoothAdapter.checkBluetoothAddress(paramBluetoothDevice.getAddress()));
        for (boolean bool = false; ; bool = Settings.Secure.putInt(this.mContext.getContentResolver(), Settings.Secure.getBluetoothInputDevicePriorityKey(paramBluetoothDevice.getAddress()), paramInt))
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothInputProfileHandler
 * JD-Core Version:        0.6.2
 */