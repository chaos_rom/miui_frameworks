package android.server;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHealthAppConfiguration;
import android.bluetooth.IBluetoothHealthCallback;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

final class BluetoothHealthProfileHandler
{
    private static final boolean DBG = false;
    private static final int MESSAGE_CONNECT_CHANNEL = 2;
    private static final int MESSAGE_REGISTER_APPLICATION = 0;
    private static final int MESSAGE_UNREGISTER_APPLICATION = 1;
    private static final String TAG = "BluetoothHealthProfileHandler";
    private static final AtomicInteger sChannelId = new AtomicInteger();
    private static BluetoothHealthProfileHandler sInstance;
    private BluetoothService mBluetoothService;
    private HashMap<BluetoothHealthAppConfiguration, IBluetoothHealthCallback> mCallbacks;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 0:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration2 = (BluetoothHealthAppConfiguration)paramAnonymousMessage.obj;
                int j = localBluetoothHealthAppConfiguration2.getRole();
                if (j == 2);
                for (String str4 = BluetoothHealthProfileHandler.this.mBluetoothService.registerHealthApplicationNative(localBluetoothHealthAppConfiguration2.getDataType(), BluetoothHealthProfileHandler.this.getStringRole(j), localBluetoothHealthAppConfiguration2.getName()); ; str4 = BluetoothHealthProfileHandler.this.mBluetoothService.registerHealthApplicationNative(localBluetoothHealthAppConfiguration2.getDataType(), BluetoothHealthProfileHandler.this.getStringRole(j), localBluetoothHealthAppConfiguration2.getName(), BluetoothHealthProfileHandler.this.getStringChannelType(localBluetoothHealthAppConfiguration2.getChannelType())))
                {
                    if (str4 != null)
                        break label163;
                    BluetoothHealthProfileHandler.this.callHealthApplicationStatusCallback(localBluetoothHealthAppConfiguration2, 1);
                    BluetoothHealthProfileHandler.this.mCallbacks.remove(localBluetoothHealthAppConfiguration2);
                    break;
                }
                label163: BluetoothHealthProfileHandler.this.mHealthAppConfigs.put(localBluetoothHealthAppConfiguration2, str4);
                BluetoothHealthProfileHandler.this.callHealthApplicationStatusCallback(localBluetoothHealthAppConfiguration2, 0);
                continue;
                BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration1 = (BluetoothHealthAppConfiguration)paramAnonymousMessage.obj;
                Iterator localIterator = BluetoothHealthProfileHandler.this.mHealthChannels.iterator();
                while (localIterator.hasNext())
                {
                    BluetoothHealthProfileHandler.HealthChannel localHealthChannel2 = (BluetoothHealthProfileHandler.HealthChannel)localIterator.next();
                    if ((BluetoothHealthProfileHandler.HealthChannel.access$800(localHealthChannel2).equals(localBluetoothHealthAppConfiguration1)) && (BluetoothHealthProfileHandler.HealthChannel.access$900(localHealthChannel2) != 0))
                        BluetoothHealthProfileHandler.this.disconnectChannel(BluetoothHealthProfileHandler.HealthChannel.access$1000(localHealthChannel2), localBluetoothHealthAppConfiguration1, BluetoothHealthProfileHandler.HealthChannel.access$1100(localHealthChannel2));
                }
                if (BluetoothHealthProfileHandler.this.mBluetoothService.unregisterHealthApplicationNative((String)BluetoothHealthProfileHandler.this.mHealthAppConfigs.get(localBluetoothHealthAppConfiguration1)))
                {
                    BluetoothHealthProfileHandler.this.callHealthApplicationStatusCallback(localBluetoothHealthAppConfiguration1, 2);
                    BluetoothHealthProfileHandler.this.mCallbacks.remove(localBluetoothHealthAppConfiguration1);
                    BluetoothHealthProfileHandler.this.mHealthAppConfigs.remove(localBluetoothHealthAppConfiguration1);
                }
                else
                {
                    BluetoothHealthProfileHandler.this.callHealthApplicationStatusCallback(localBluetoothHealthAppConfiguration1, 3);
                    continue;
                    BluetoothHealthProfileHandler.HealthChannel localHealthChannel1 = (BluetoothHealthProfileHandler.HealthChannel)paramAnonymousMessage.obj;
                    String str1 = BluetoothHealthProfileHandler.this.mBluetoothService.getObjectPathFromAddress(BluetoothHealthProfileHandler.HealthChannel.access$1000(localHealthChannel1).getAddress());
                    String str2 = (String)BluetoothHealthProfileHandler.this.mHealthAppConfigs.get(BluetoothHealthProfileHandler.HealthChannel.access$800(localHealthChannel1));
                    String str3 = BluetoothHealthProfileHandler.this.getStringChannelType(BluetoothHealthProfileHandler.HealthChannel.access$1200(localHealthChannel1));
                    if (!BluetoothHealthProfileHandler.this.mBluetoothService.createChannelNative(str1, str2, str3, BluetoothHealthProfileHandler.HealthChannel.access$1100(localHealthChannel1)))
                    {
                        int i = BluetoothHealthProfileHandler.HealthChannel.access$900(localHealthChannel1);
                        BluetoothHealthProfileHandler.this.callHealthChannelCallback(BluetoothHealthProfileHandler.HealthChannel.access$800(localHealthChannel1), BluetoothHealthProfileHandler.HealthChannel.access$1000(localHealthChannel1), i, 0, null, BluetoothHealthProfileHandler.HealthChannel.access$1100(localHealthChannel1));
                        BluetoothHealthProfileHandler.this.mHealthChannels.remove(localHealthChannel1);
                    }
                }
            }
        }
    };
    private HashMap<BluetoothHealthAppConfiguration, String> mHealthAppConfigs;
    private ArrayList<HealthChannel> mHealthChannels;
    private HashMap<BluetoothDevice, Integer> mHealthDevices;

    private BluetoothHealthProfileHandler(Context paramContext, BluetoothService paramBluetoothService)
    {
        this.mBluetoothService = paramBluetoothService;
        this.mHealthAppConfigs = new HashMap();
        this.mHealthChannels = new ArrayList();
        this.mHealthDevices = new HashMap();
        this.mCallbacks = new HashMap();
    }

    private void broadcastHealthDeviceStateChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2)
    {
        if (this.mHealthDevices.get(paramBluetoothDevice) == null)
            this.mHealthDevices.put(paramBluetoothDevice, Integer.valueOf(0));
        int i = ((Integer)this.mHealthDevices.get(paramBluetoothDevice)).intValue();
        int j = convertState(paramInt2);
        if (i != j)
            switch (i)
            {
            default:
            case 0:
            case 1:
            case 2:
            case 3:
            }
        while (true)
        {
            return;
            updateAndSendIntent(paramBluetoothDevice, i, j);
            continue;
            if (j == 2)
            {
                updateAndSendIntent(paramBluetoothDevice, i, j);
            }
            else
            {
                int[] arrayOfInt3 = new int[2];
                arrayOfInt3[0] = 1;
                arrayOfInt3[1] = 3;
                if (findChannelByStates(paramBluetoothDevice, arrayOfInt3).isEmpty())
                {
                    updateAndSendIntent(paramBluetoothDevice, i, j);
                    continue;
                    int[] arrayOfInt2 = new int[2];
                    arrayOfInt2[0] = 1;
                    arrayOfInt2[1] = 2;
                    if (findChannelByStates(paramBluetoothDevice, arrayOfInt2).isEmpty())
                    {
                        updateAndSendIntent(paramBluetoothDevice, i, j);
                        continue;
                        int[] arrayOfInt1 = new int[2];
                        arrayOfInt1[0] = 1;
                        arrayOfInt1[1] = 3;
                        if (findChannelByStates(paramBluetoothDevice, arrayOfInt1).isEmpty())
                            updateAndSendIntent(paramBluetoothDevice, i, j);
                    }
                }
            }
        }
    }

    private void callHealthApplicationStatusCallback(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
    {
        debugLog("Health Device Application: " + paramBluetoothHealthAppConfiguration + " State Change: status:" + paramInt);
        IBluetoothHealthCallback localIBluetoothHealthCallback = (IBluetoothHealthCallback)this.mCallbacks.get(paramBluetoothHealthAppConfiguration);
        if (localIBluetoothHealthCallback != null);
        try
        {
            localIBluetoothHealthCallback.onHealthAppConfigurationStatusChange(paramBluetoothHealthAppConfiguration, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                errorLog("Remote Exception:" + localRemoteException);
        }
    }

    private void callHealthChannelCallback(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt3)
    {
        broadcastHealthDeviceStateChange(paramBluetoothDevice, paramInt1, paramInt2);
        debugLog("Health Device Callback: " + paramBluetoothDevice + " State Change: " + paramInt1 + "->" + paramInt2);
        Object localObject = null;
        if (paramParcelFileDescriptor != null);
        try
        {
            ParcelFileDescriptor localParcelFileDescriptor = paramParcelFileDescriptor.dup();
            localObject = localParcelFileDescriptor;
            localIBluetoothHealthCallback = (IBluetoothHealthCallback)this.mCallbacks.get(paramBluetoothHealthAppConfiguration);
            if (localIBluetoothHealthCallback == null);
        }
        catch (IOException localIOException)
        {
            try
            {
                IBluetoothHealthCallback localIBluetoothHealthCallback;
                localIBluetoothHealthCallback.onHealthChannelStateChange(paramBluetoothHealthAppConfiguration, paramBluetoothDevice, paramInt1, paramInt2, localObject, paramInt3);
                return;
                localIOException = localIOException;
                localObject = null;
                errorLog("Exception while duping: " + localIOException);
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    errorLog("Remote Exception:" + localRemoteException);
            }
        }
    }

    private int convertState(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            errorLog("Mismatch in Channel and Health Device State");
            i = -1;
        case 2:
        case 1:
        case 3:
        case 0:
        }
        while (true)
        {
            return i;
            i = 2;
            continue;
            i = 1;
            continue;
            i = 3;
            continue;
            i = 0;
        }
    }

    private static void debugLog(String paramString)
    {
    }

    private static void errorLog(String paramString)
    {
        Log.e("BluetoothHealthProfileHandler", paramString);
    }

    private HealthChannel findChannelById(int paramInt)
    {
        Iterator localIterator = this.mHealthChannels.iterator();
        HealthChannel localHealthChannel;
        do
        {
            if (!localIterator.hasNext())
                break;
            localHealthChannel = (HealthChannel)localIterator.next();
        }
        while (localHealthChannel.mId != paramInt);
        while (true)
        {
            return localHealthChannel;
            localHealthChannel = null;
        }
    }

    private HealthChannel findChannelByPath(BluetoothDevice paramBluetoothDevice, String paramString)
    {
        Iterator localIterator = this.mHealthChannels.iterator();
        HealthChannel localHealthChannel;
        do
        {
            if (!localIterator.hasNext())
                break;
            localHealthChannel = (HealthChannel)localIterator.next();
        }
        while ((!paramString.equals(localHealthChannel.mChannelPath)) || (!paramBluetoothDevice.equals(localHealthChannel.mDevice)));
        while (true)
        {
            return localHealthChannel;
            localHealthChannel = null;
        }
    }

    private List<HealthChannel> findChannelByStates(BluetoothDevice paramBluetoothDevice, int[] paramArrayOfInt)
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mHealthChannels.iterator();
        while (localIterator.hasNext())
        {
            HealthChannel localHealthChannel = (HealthChannel)localIterator.next();
            if (localHealthChannel.mDevice.equals(paramBluetoothDevice))
            {
                int i = paramArrayOfInt.length;
                for (int j = 0; j < i; j++)
                {
                    int k = paramArrayOfInt[j];
                    if (localHealthChannel.mState == k)
                        localArrayList.add(localHealthChannel);
                }
            }
        }
        return localArrayList;
    }

    private HealthChannel findConnectingChannel(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        Iterator localIterator = this.mHealthChannels.iterator();
        HealthChannel localHealthChannel;
        do
        {
            if (!localIterator.hasNext())
                break;
            localHealthChannel = (HealthChannel)localIterator.next();
        }
        while ((!localHealthChannel.mDevice.equals(paramBluetoothDevice)) || (!localHealthChannel.mConfig.equals(paramBluetoothHealthAppConfiguration)) || (localHealthChannel.mState != 1));
        while (true)
        {
            return localHealthChannel;
            localHealthChannel = null;
        }
    }

    private BluetoothHealthAppConfiguration findHealthApplication(BluetoothDevice paramBluetoothDevice, String paramString)
    {
        BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration = null;
        HealthChannel localHealthChannel = findChannelByPath(paramBluetoothDevice, paramString);
        if (localHealthChannel != null)
            localBluetoothHealthAppConfiguration = localHealthChannel.mConfig;
        while (true)
        {
            return localBluetoothHealthAppConfiguration;
            String str = this.mBluetoothService.getChannelApplicationNative(paramString);
            if (str == null)
            {
                errorLog("Config path is null for application");
            }
            else
            {
                Iterator localIterator = this.mHealthAppConfigs.entrySet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    if (((String)localEntry.getValue()).equals(str))
                        localBluetoothHealthAppConfiguration = (BluetoothHealthAppConfiguration)localEntry.getKey();
                }
                if (localBluetoothHealthAppConfiguration == null)
                    errorLog("No associated application for path:" + str);
            }
        }
    }

    private int getChannelId()
    {
        int i;
        int j;
        do
        {
            i = sChannelId.incrementAndGet();
            j = 0;
            Iterator localIterator = this.mHealthChannels.iterator();
            while (localIterator.hasNext())
                if (((HealthChannel)localIterator.next()).mId == i)
                    j = 1;
        }
        while (j != 0);
        return i;
    }

    /** @deprecated */
    static BluetoothHealthProfileHandler getInstance(Context paramContext, BluetoothService paramBluetoothService)
    {
        try
        {
            if (sInstance == null)
                sInstance = new BluetoothHealthProfileHandler(paramContext, paramBluetoothService);
            BluetoothHealthProfileHandler localBluetoothHealthProfileHandler = sInstance;
            return localBluetoothHealthProfileHandler;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private HealthChannel getMainChannel(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        Iterator localIterator = this.mHealthChannels.iterator();
        HealthChannel localHealthChannel;
        do
        {
            if (!localIterator.hasNext())
                break;
            localHealthChannel = (HealthChannel)localIterator.next();
        }
        while ((!localHealthChannel.mDevice.equals(paramBluetoothDevice)) || (!localHealthChannel.mConfig.equals(paramBluetoothHealthAppConfiguration)) || (!localHealthChannel.mMainChannel));
        while (true)
        {
            return localHealthChannel;
            localHealthChannel = null;
        }
    }

    private String getStringChannelType(int paramInt)
    {
        String str;
        if (paramInt == 10)
            str = "Reliable";
        while (true)
        {
            return str;
            if (paramInt == 11)
                str = "Streaming";
            else
                str = "Any";
        }
    }

    private String getStringRole(int paramInt)
    {
        String str;
        if (paramInt == 2)
            str = "Sink";
        while (true)
        {
            return str;
            if (paramInt == 1)
                str = "Streaming";
            else
                str = null;
        }
    }

    private void updateAndSendIntent(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2)
    {
        this.mHealthDevices.put(paramBluetoothDevice, Integer.valueOf(paramInt2));
        this.mBluetoothService.sendConnectionStateChange(paramBluetoothDevice, 3, paramInt2, paramInt1);
    }

    boolean connectChannel(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
    {
        boolean bool;
        if (this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress()) == null)
            bool = false;
        while (true)
        {
            return bool;
            if ((String)this.mHealthAppConfigs.get(paramBluetoothHealthAppConfiguration) == null)
            {
                bool = false;
            }
            else
            {
                HealthChannel localHealthChannel = new HealthChannel(paramBluetoothDevice, paramBluetoothHealthAppConfiguration, null, false, null);
                HealthChannel.access$902(localHealthChannel, 1);
                HealthChannel.access$1202(localHealthChannel, paramInt);
                this.mHealthChannels.add(localHealthChannel);
                callHealthChannelCallback(paramBluetoothHealthAppConfiguration, paramBluetoothDevice, 0, 1, null, localHealthChannel.mId);
                Message localMessage = this.mHandler.obtainMessage(2);
                localMessage.obj = localHealthChannel;
                this.mHandler.sendMessage(localMessage);
                bool = true;
            }
        }
    }

    boolean connectChannelToSource(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        return connectChannel(paramBluetoothDevice, paramBluetoothHealthAppConfiguration, 12);
    }

    boolean disconnectChannel(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
    {
        HealthChannel localHealthChannel = findChannelById(paramInt);
        boolean bool;
        if (localHealthChannel == null)
            bool = false;
        while (true)
        {
            return bool;
            String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
            this.mBluetoothService.releaseChannelFdNative(localHealthChannel.mChannelPath);
            int i = localHealthChannel.mState;
            HealthChannel.access$902(localHealthChannel, 3);
            callHealthChannelCallback(paramBluetoothHealthAppConfiguration, paramBluetoothDevice, i, localHealthChannel.mState, null, localHealthChannel.mId);
            if (!this.mBluetoothService.destroyChannelNative(str, localHealthChannel.mChannelPath, localHealthChannel.mId))
            {
                int j = localHealthChannel.mState;
                HealthChannel.access$902(localHealthChannel, 2);
                callHealthChannelCallback(paramBluetoothHealthAppConfiguration, paramBluetoothDevice, j, localHealthChannel.mState, localHealthChannel.mChannelFd, localHealthChannel.mId);
                bool = false;
            }
            else
            {
                bool = true;
            }
        }
    }

    List<BluetoothDevice> getConnectedHealthDevices()
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 2;
        return lookupHealthDevicesMatchingStates(arrayOfInt);
    }

    int getHealthDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        if (this.mHealthDevices.get(paramBluetoothDevice) == null);
        for (int i = 0; ; i = ((Integer)this.mHealthDevices.get(paramBluetoothDevice)).intValue())
            return i;
    }

    List<BluetoothDevice> getHealthDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        return lookupHealthDevicesMatchingStates(paramArrayOfInt);
    }

    ParcelFileDescriptor getMainChannelFd(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        HealthChannel localHealthChannel1 = getMainChannel(paramBluetoothDevice, paramBluetoothHealthAppConfiguration);
        if (localHealthChannel1 != null);
        while (true)
        {
            Object localObject;
            try
            {
                ParcelFileDescriptor localParcelFileDescriptor2 = localHealthChannel1.mChannelFd.dup();
                localObject = localParcelFileDescriptor2;
                return localObject;
            }
            catch (IOException localIOException2)
            {
                localObject = null;
                continue;
            }
            String str1 = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
            if (str1 == null)
            {
                localObject = null;
            }
            else
            {
                String str2 = this.mBluetoothService.getMainChannelNative(str1);
                if (str2 == null)
                {
                    localObject = null;
                }
                else
                {
                    HealthChannel localHealthChannel2 = findChannelByPath(paramBluetoothDevice, str2);
                    if (localHealthChannel2 == null)
                    {
                        errorLog("Main Channel present but we don't have any account of it:" + paramBluetoothDevice + ":" + paramBluetoothHealthAppConfiguration);
                        localObject = null;
                    }
                    else
                    {
                        HealthChannel.access$1402(localHealthChannel2, true);
                        try
                        {
                            ParcelFileDescriptor localParcelFileDescriptor1 = localHealthChannel2.mChannelFd.dup();
                            localObject = localParcelFileDescriptor1;
                        }
                        catch (IOException localIOException1)
                        {
                            localObject = null;
                        }
                    }
                }
            }
        }
    }

    List<BluetoothDevice> lookupHealthDevicesMatchingStates(int[] paramArrayOfInt)
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mHealthDevices.keySet().iterator();
        label88: 
        while (localIterator.hasNext())
        {
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
            int i = getHealthDeviceConnectionState(localBluetoothDevice);
            int j = paramArrayOfInt.length;
            for (int k = 0; ; k++)
            {
                if (k >= j)
                    break label88;
                if (paramArrayOfInt[k] == i)
                {
                    localArrayList.add(localBluetoothDevice);
                    break;
                }
            }
        }
        return localArrayList;
    }

    void onHealthDeviceChannelChanged(String paramString1, String paramString2, boolean paramBoolean)
    {
        debugLog("onHealthDeviceChannelChanged: devicePath: " + paramString1 + "ChannelPath: " + paramString2 + "Exists: " + paramBoolean);
        BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        String str1 = this.mBluetoothService.getAddressFromObjectPath(paramString1);
        if (str1 == null);
        BluetoothDevice localBluetoothDevice;
        BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration;
        HealthChannel localHealthChannel;
        ParcelFileDescriptor localParcelFileDescriptor;
        while (true)
        {
            return;
            localBluetoothDevice = localBluetoothAdapter.getRemoteDevice(str1);
            localBluetoothHealthAppConfiguration = findHealthApplication(localBluetoothDevice, paramString2);
            if (!paramBoolean)
                break label334;
            localHealthChannel = findConnectingChannel(localBluetoothDevice, localBluetoothHealthAppConfiguration);
            if (localHealthChannel == null)
            {
                localHealthChannel = new HealthChannel(localBluetoothDevice, localBluetoothHealthAppConfiguration, null, false, paramString2);
                HealthChannel.access$902(localHealthChannel, 0);
                this.mHealthChannels.add(localHealthChannel);
            }
            HealthChannel.access$1502(localHealthChannel, paramString2);
            localParcelFileDescriptor = this.mBluetoothService.getChannelFdNative(paramString2);
            if (localParcelFileDescriptor != null)
                break;
            errorLog("Error obtaining fd for channel:" + paramString2);
            disconnectChannel(localBluetoothDevice, localBluetoothHealthAppConfiguration, localHealthChannel.mId);
        }
        boolean bool;
        if (getMainChannel(localBluetoothDevice, localBluetoothHealthAppConfiguration) == null)
            bool = false;
        while (true)
            if (!bool)
            {
                String str2 = this.mBluetoothService.getMainChannelNative(paramString1);
                if (str2 == null)
                {
                    errorLog("Main Channel Path is null for devicePath:" + paramString1);
                    break;
                    bool = true;
                    continue;
                }
                if (str2.equals(paramString2))
                    bool = true;
            }
        HealthChannel.access$1602(localHealthChannel, localParcelFileDescriptor);
        HealthChannel.access$1402(localHealthChannel, bool);
        int i = localHealthChannel.mState;
        for (int j = 2; ; j = 0)
        {
            HealthChannel.access$902(localHealthChannel, j);
            callHealthChannelCallback(localBluetoothHealthAppConfiguration, localBluetoothDevice, i, j, localHealthChannel.mChannelFd, localHealthChannel.mId);
            break;
            label334: localHealthChannel = findChannelByPath(localBluetoothDevice, paramString2);
            if (localHealthChannel == null)
            {
                errorLog("Channel not found:" + localBluetoothHealthAppConfiguration + ":" + paramString2);
                break;
            }
            this.mHealthChannels.remove(localHealthChannel);
            HealthChannel.access$1602(localHealthChannel, null);
            i = localHealthChannel.mState;
        }
    }

    void onHealthDeviceChannelConnectionError(int paramInt1, int paramInt2)
    {
        HealthChannel localHealthChannel = findChannelById(paramInt1);
        if (localHealthChannel == null)
            errorLog("No record of this channel:" + paramInt1);
        callHealthChannelCallback(localHealthChannel.mConfig, localHealthChannel.mDevice, localHealthChannel.mState, paramInt2, null, paramInt1);
    }

    void onHealthDevicePropertyChanged(String paramString1, String paramString2)
    {
        BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        String str = this.mBluetoothService.getAddressFromObjectPath(paramString1);
        if (str == null);
        while (true)
        {
            return;
            if (!paramString2.equals("/"))
            {
                BluetoothDevice localBluetoothDevice = localBluetoothAdapter.getRemoteDevice(str);
                if (findHealthApplication(localBluetoothDevice, paramString2) != null)
                {
                    HealthChannel localHealthChannel = findChannelByPath(localBluetoothDevice, paramString2);
                    if (localHealthChannel == null)
                        errorLog("Health Channel is not present:" + paramString2);
                    else
                        HealthChannel.access$1402(localHealthChannel, true);
                }
            }
        }
    }

    boolean registerAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, IBluetoothHealthCallback paramIBluetoothHealthCallback)
    {
        Message localMessage = this.mHandler.obtainMessage(0);
        localMessage.obj = paramBluetoothHealthAppConfiguration;
        this.mHandler.sendMessage(localMessage);
        this.mCallbacks.put(paramBluetoothHealthAppConfiguration, paramIBluetoothHealthCallback);
        return true;
    }

    boolean unregisterAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        int i = 1;
        if ((String)this.mHealthAppConfigs.get(paramBluetoothHealthAppConfiguration) == null)
            i = 0;
        while (true)
        {
            return i;
            Message localMessage = this.mHandler.obtainMessage(i);
            localMessage.obj = paramBluetoothHealthAppConfiguration;
            this.mHandler.sendMessage(localMessage);
        }
    }

    class HealthChannel
    {
        private ParcelFileDescriptor mChannelFd;
        private String mChannelPath;
        private int mChannelType;
        private BluetoothHealthAppConfiguration mConfig;
        private BluetoothDevice mDevice;
        private int mId;
        private boolean mMainChannel;
        private int mState;

        HealthChannel(BluetoothDevice paramBluetoothHealthAppConfiguration, BluetoothHealthAppConfiguration paramParcelFileDescriptor, ParcelFileDescriptor paramBoolean, boolean paramString, String arg6)
        {
            this.mChannelFd = paramBoolean;
            this.mMainChannel = paramString;
            Object localObject;
            this.mChannelPath = localObject;
            this.mDevice = paramBluetoothHealthAppConfiguration;
            this.mConfig = paramParcelFileDescriptor;
            this.mState = 0;
            this.mId = BluetoothHealthProfileHandler.this.getChannelId();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothHealthProfileHandler
 * JD-Core Version:        0.6.2
 */