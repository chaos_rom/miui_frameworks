package android.server;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.bluetooth.BluetoothUuid;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

class BluetoothEventLoop
{
    private static final String BLUETOOTH_ADMIN_PERM = "android.permission.BLUETOOTH_ADMIN";
    private static final String BLUETOOTH_PERM = "android.permission.BLUETOOTH";
    private static final int CREATE_DEVICE_ALREADY_EXISTS = 1;
    private static final int CREATE_DEVICE_FAILED = -1;
    private static final int CREATE_DEVICE_SUCCESS = 0;
    private static final boolean DBG = false;
    private static final int EVENT_AGENT_CANCEL = 2;
    private static final int EVENT_PAIRING_CONSENT_DELAYED_ACCEPT = 1;
    private static final String TAG = "BluetoothEventLoop";
    private BluetoothA2dp mA2dp;
    private final BluetoothAdapter mAdapter;
    private final HashMap<String, Integer> mAuthorizationAgentRequestData;
    private final BluetoothService mBluetoothService;
    private final BluetoothAdapterStateMachine mBluetoothState;
    private final Context mContext;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                String str2 = (String)paramAnonymousMessage.obj;
                if (str2 != null)
                {
                    BluetoothEventLoop.this.mBluetoothService.setPairingConfirmation(str2, true);
                    continue;
                    String[] arrayOfString = BluetoothEventLoop.this.mBluetoothService.listInState(11);
                    if (arrayOfString.length != 0)
                        if (arrayOfString.length > 1)
                        {
                            Log.e("BluetoothEventLoop", " There is more than one device in the Bonding State");
                        }
                        else
                        {
                            String str1 = arrayOfString[0];
                            BluetoothEventLoop.this.mBluetoothService.setBondState(str1, 10, 8);
                        }
                }
            }
        }
    };
    private boolean mInterrupted;
    private int mNativeData;
    private final HashMap<String, Integer> mPasskeyAgentRequestData;
    private BluetoothProfile.ServiceListener mProfileServiceListener = new BluetoothProfile.ServiceListener()
    {
        public void onServiceConnected(int paramAnonymousInt, BluetoothProfile paramAnonymousBluetoothProfile)
        {
            if (paramAnonymousInt == 2)
                BluetoothEventLoop.access$102(BluetoothEventLoop.this, (BluetoothA2dp)paramAnonymousBluetoothProfile);
        }

        public void onServiceDisconnected(int paramAnonymousInt)
        {
            if (paramAnonymousInt == 2)
                BluetoothEventLoop.access$102(BluetoothEventLoop.this, null);
        }
    };
    private boolean mStarted;
    private Thread mThread;
    private PowerManager.WakeLock mWakeLock;

    static
    {
        classInitNative();
    }

    BluetoothEventLoop(Context paramContext, BluetoothAdapter paramBluetoothAdapter, BluetoothService paramBluetoothService, BluetoothAdapterStateMachine paramBluetoothAdapterStateMachine)
    {
        this.mBluetoothService = paramBluetoothService;
        this.mContext = paramContext;
        this.mBluetoothState = paramBluetoothAdapterStateMachine;
        this.mPasskeyAgentRequestData = new HashMap();
        this.mAuthorizationAgentRequestData = new HashMap();
        this.mAdapter = paramBluetoothAdapter;
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(805306394, "BluetoothEventLoop");
        this.mWakeLock.setReferenceCounted(false);
        initializeNativeDataNative();
    }

    private void addDevice(String paramString, String[] paramArrayOfString)
    {
        BluetoothDeviceProperties localBluetoothDeviceProperties = this.mBluetoothService.getDeviceProperties();
        localBluetoothDeviceProperties.addProperties(paramString, paramArrayOfString);
        String str1 = localBluetoothDeviceProperties.getProperty(paramString, "RSSI");
        String str2 = localBluetoothDeviceProperties.getProperty(paramString, "Class");
        String str3 = localBluetoothDeviceProperties.getProperty(paramString, "Name");
        short s;
        if (str1 != null)
        {
            s = (short)Integer.valueOf(str1).intValue();
            if (str2 == null)
                break label153;
            Intent localIntent = new Intent("android.bluetooth.device.action.FOUND");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(paramString));
            localIntent.putExtra("android.bluetooth.device.extra.CLASS", new BluetoothClass(Integer.valueOf(str2).intValue()));
            localIntent.putExtra("android.bluetooth.device.extra.RSSI", s);
            localIntent.putExtra("android.bluetooth.device.extra.NAME", str3);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
        }
        while (true)
        {
            return;
            s = -32768;
            break;
            label153: log("ClassValue: " + str2 + " for remote device: " + paramString + " is null");
        }
    }

    private String checkPairingRequestAndGetAddress(String paramString, int paramInt)
    {
        Object localObject = null;
        String str1 = this.mBluetoothService.getAddressFromObjectPath(paramString);
        if (str1 == null)
            Log.e("BluetoothEventLoop", "Unable to get device address in checkPairingRequestAndGetAddress, returning null");
        while (true)
        {
            return localObject;
            String str2 = str1.toUpperCase();
            this.mPasskeyAgentRequestData.put(str2, new Integer(paramInt));
            if (this.mBluetoothService.getBluetoothState() == 13)
            {
                this.mBluetoothService.cancelPairingUserInput(str2);
            }
            else
            {
                if (this.mBluetoothService.getBondState(str2) != 12)
                    this.mBluetoothService.setBondState(str2, 11);
                localObject = str2;
            }
        }
    }

    private static native void classInitNative();

    private native void cleanupNativeDataNative();

    private native void initializeNativeDataNative();

    private native boolean isEventLoopRunningNative();

    private boolean isOtherSinkInNonDisconnectedState(String paramString)
    {
        boolean bool = false;
        BluetoothA2dp localBluetoothA2dp = this.mA2dp;
        int[] arrayOfInt = new int[3];
        arrayOfInt[0] = 2;
        arrayOfInt[1] = 1;
        arrayOfInt[2] = 3;
        List localList = localBluetoothA2dp.getDevicesMatchingConnectionStates(arrayOfInt);
        if (localList.size() == 0)
            break label56;
        while (true)
        {
            return bool;
            Iterator localIterator = localList.iterator();
            label56: if (localIterator.hasNext())
            {
                if (((BluetoothDevice)localIterator.next()).getAddress().equals(paramString))
                    break;
                bool = true;
            }
        }
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothEventLoop", paramString);
    }

    private void onAgentAuthorize(String paramString1, String paramString2, int paramInt)
    {
        if (!this.mBluetoothService.isEnabled());
        String str;
        while (true)
        {
            return;
            str = this.mBluetoothService.getAddressFromObjectPath(paramString1);
            if (str != null)
                break;
            Log.e("BluetoothEventLoop", "Unable to get device address in onAuthAgentAuthorize");
        }
        boolean bool = false;
        ParcelUuid localParcelUuid = ParcelUuid.fromString(paramString2);
        BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(str);
        this.mAuthorizationAgentRequestData.put(str, new Integer(paramInt));
        if ((this.mA2dp != null) && ((BluetoothUuid.isAudioSource(localParcelUuid)) || (BluetoothUuid.isAvrcpTarget(localParcelUuid)) || (BluetoothUuid.isAdvAudioDist(localParcelUuid))) && (!isOtherSinkInNonDisconnectedState(str)))
            if (this.mA2dp.getPriority(localBluetoothDevice) > 0)
            {
                bool = true;
                label131: if ((!bool) || (BluetoothUuid.isAvrcpTarget(localParcelUuid)))
                    break label235;
                Log.i("BluetoothEventLoop", "First check pass for incoming A2DP / AVRCP connection from " + str);
                this.mBluetoothService.notifyIncomingA2dpConnection(str, false);
            }
        while (true)
        {
            log("onAgentAuthorize(" + paramString1 + ", " + paramString2 + ") = " + bool);
            break;
            bool = false;
            break label131;
            label235: Log.i("BluetoothEventLoop", "" + bool + "Incoming A2DP / AVRCP connection from " + str);
            this.mA2dp.allowIncomingConnect(localBluetoothDevice, bool);
            this.mBluetoothService.notifyIncomingA2dpConnection(str, true);
            continue;
            if (BluetoothUuid.isInputDevice(localParcelUuid))
            {
                if (this.mBluetoothService.getInputDevicePriority(localBluetoothDevice) > 0);
                for (bool = true; ; bool = false)
                {
                    if (!bool)
                        break label373;
                    Log.i("BluetoothEventLoop", "First check pass for incoming HID connection from " + str);
                    this.mBluetoothService.notifyIncomingHidConnection(str);
                    break;
                }
                label373: Log.i("BluetoothEventLoop", "Rejecting incoming HID connection from " + str);
                this.mBluetoothService.allowIncomingProfileConnect(localBluetoothDevice, bool);
            }
            else if (BluetoothUuid.isBnep(localParcelUuid))
            {
                bool = this.mBluetoothService.allowIncomingTethering();
                this.mBluetoothService.allowIncomingProfileConnect(localBluetoothDevice, bool);
            }
            else
            {
                Log.i("BluetoothEventLoop", "Rejecting incoming " + paramString2 + " connection from " + str);
                this.mBluetoothService.allowIncomingProfileConnect(localBluetoothDevice, false);
            }
        }
    }

    private void onAgentCancel()
    {
        Intent localIntent = new Intent("android.bluetooth.device.action.PAIRING_CANCEL");
        this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
        this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(2), 1500L);
    }

    private boolean onAgentOutOfBandDataAvailable(String paramString)
    {
        boolean bool = false;
        if (!this.mBluetoothService.isEnabled());
        while (true)
        {
            return bool;
            String str = this.mBluetoothService.getAddressFromObjectPath(paramString);
            if ((str != null) && (this.mBluetoothService.getDeviceOutOfBandData(this.mAdapter.getRemoteDevice(str)) != null))
                bool = true;
        }
    }

    private void onCreateDeviceResult(String paramString, int paramInt)
    {
        switch (paramInt)
        {
        case 0:
        default:
        case 1:
        case -1:
        }
        while (true)
        {
            return;
            String str = this.mBluetoothService.getObjectPathFromAddress(paramString);
            if (str != null)
            {
                this.mBluetoothService.discoverServicesNative(str, "");
            }
            else
            {
                Log.w("BluetoothEventLoop", "Device exists, but we don't have the bluez path, failing");
                this.mBluetoothService.sendUuidIntent(paramString);
                this.mBluetoothService.makeServiceChannelCallbacks(paramString);
            }
        }
    }

    private void onCreatePairedDeviceResult(String paramString, int paramInt)
    {
        String str = paramString.toUpperCase();
        this.mBluetoothService.onCreatePairedDeviceResult(str, paramInt);
    }

    private void onDeviceCreated(String paramString)
    {
        String str = this.mBluetoothService.getAddressFromObjectPath(paramString);
        if (str == null)
            Log.e("BluetoothEventLoop", "onDeviceCreated: device address null! deviceObjectPath: " + paramString);
        while (true)
        {
            return;
            if (!this.mBluetoothService.isRemoteDeviceInCache(str))
            {
                String[] arrayOfString = this.mBluetoothService.getRemoteDeviceProperties(str);
                if (arrayOfString != null)
                    addDevice(str, arrayOfString);
            }
        }
    }

    private void onDeviceDisappeared(String paramString)
    {
        Intent localIntent = new Intent("android.bluetooth.device.action.DISAPPEARED");
        localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(paramString));
        this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
    }

    private void onDeviceDisconnectRequested(String paramString)
    {
        String str = this.mBluetoothService.getAddressFromObjectPath(paramString);
        if (str == null)
            Log.e("BluetoothEventLoop", "onDeviceDisconnectRequested: Address of the remote device in null");
        while (true)
        {
            return;
            Intent localIntent = new Intent("android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(str));
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
        }
    }

    private void onDeviceFound(String paramString, String[] paramArrayOfString)
    {
        if (paramArrayOfString == null)
            Log.e("BluetoothEventLoop", "ERROR: Remote device properties are null");
        while (true)
        {
            return;
            addDevice(paramString, paramArrayOfString);
        }
    }

    private void onDevicePropertyChanged(String paramString, String[] paramArrayOfString)
    {
        String str1 = paramArrayOfString[0];
        String str2 = this.mBluetoothService.getAddressFromObjectPath(paramString);
        if (str2 == null)
            Log.e("BluetoothEventLoop", "onDevicePropertyChanged: Address of the remote device in null");
        while (true)
        {
            return;
            log("Device property changed: " + str2 + " property: " + str1 + " value: " + paramArrayOfString[1]);
            BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(str2);
            if (str1.equals("Name"))
            {
                this.mBluetoothService.setRemoteDeviceProperty(str2, str1, paramArrayOfString[1]);
                Intent localIntent4 = new Intent("android.bluetooth.device.action.NAME_CHANGED");
                localIntent4.putExtra("android.bluetooth.device.extra.DEVICE", localBluetoothDevice);
                localIntent4.putExtra("android.bluetooth.device.extra.NAME", paramArrayOfString[1]);
                localIntent4.addFlags(134217728);
                this.mContext.sendBroadcast(localIntent4, "android.permission.BLUETOOTH");
            }
            else if (str1.equals("Alias"))
            {
                this.mBluetoothService.setRemoteDeviceProperty(str2, str1, paramArrayOfString[1]);
                Intent localIntent3 = new Intent("android.bluetooth.device.action.ALIAS_CHANGED");
                localIntent3.putExtra("android.bluetooth.device.extra.DEVICE", localBluetoothDevice);
                localIntent3.addFlags(134217728);
                this.mContext.sendBroadcast(localIntent3, "android.permission.BLUETOOTH");
            }
            else if (str1.equals("Class"))
            {
                this.mBluetoothService.setRemoteDeviceProperty(str2, str1, paramArrayOfString[1]);
                Intent localIntent2 = new Intent("android.bluetooth.device.action.CLASS_CHANGED");
                localIntent2.putExtra("android.bluetooth.device.extra.DEVICE", localBluetoothDevice);
                localIntent2.putExtra("android.bluetooth.device.extra.CLASS", new BluetoothClass(Integer.valueOf(paramArrayOfString[1]).intValue()));
                localIntent2.addFlags(134217728);
                this.mContext.sendBroadcast(localIntent2, "android.permission.BLUETOOTH");
            }
            else
            {
                if (str1.equals("Connected"))
                {
                    this.mBluetoothService.setRemoteDeviceProperty(str2, str1, paramArrayOfString[1]);
                    Intent localIntent1;
                    if (paramArrayOfString[1].equals("true"))
                    {
                        localIntent1 = new Intent("android.bluetooth.device.action.ACL_CONNECTED");
                        if (this.mBluetoothService.isBluetoothDock(str2))
                            this.mBluetoothService.setLinkTimeout(str2, 8000);
                    }
                    while (true)
                    {
                        localIntent1.putExtra("android.bluetooth.device.extra.DEVICE", localBluetoothDevice);
                        localIntent1.addFlags(134217728);
                        this.mContext.sendBroadcast(localIntent1, "android.permission.BLUETOOTH");
                        break;
                        localIntent1 = new Intent("android.bluetooth.device.action.ACL_DISCONNECTED");
                    }
                }
                if (str1.equals("UUIDs"))
                {
                    String str3 = null;
                    if (Integer.valueOf(paramArrayOfString[1]).intValue() > 0)
                    {
                        StringBuilder localStringBuilder = new StringBuilder();
                        for (int i = 2; i < paramArrayOfString.length; i++)
                        {
                            localStringBuilder.append(paramArrayOfString[i]);
                            localStringBuilder.append(",");
                        }
                        str3 = localStringBuilder.toString();
                    }
                    this.mBluetoothService.setRemoteDeviceProperty(str2, str1, str3);
                    this.mBluetoothService.updateDeviceServiceChannelCache(str2);
                    this.mBluetoothService.sendUuidIntent(str2);
                }
                else if (str1.equals("Paired"))
                {
                    if (paramArrayOfString[1].equals("true"))
                    {
                        if (this.mBluetoothService.getPendingOutgoingBonding() == null)
                            this.mBluetoothService.setBondState(str2, 12);
                    }
                    else
                    {
                        this.mBluetoothService.setBondState(str2, 10);
                        this.mBluetoothService.setRemoteDeviceProperty(str2, "Trusted", "false");
                    }
                }
                else if (str1.equals("Trusted"))
                {
                    this.mBluetoothService.setRemoteDeviceProperty(str2, str1, paramArrayOfString[1]);
                }
            }
        }
    }

    private void onDeviceRemoved(String paramString)
    {
        String str = this.mBluetoothService.getAddressFromObjectPath(paramString);
        if (str != null)
        {
            this.mBluetoothService.setBondState(str.toUpperCase(), 10, 9);
            this.mBluetoothService.setRemoteDeviceProperty(str, "UUIDs", null);
        }
    }

    private void onDiscoverServicesResult(String paramString, boolean paramBoolean)
    {
        String str = this.mBluetoothService.getAddressFromObjectPath(paramString);
        if (str == null);
        while (true)
        {
            return;
            if (paramBoolean)
                this.mBluetoothService.updateRemoteDevicePropertiesCache(str);
            this.mBluetoothService.sendUuidIntent(str);
            this.mBluetoothService.makeServiceChannelCallbacks(str);
        }
    }

    private void onDisplayPasskey(String paramString, int paramInt1, int paramInt2)
    {
        String str = checkPairingRequestAndGetAddress(paramString, paramInt2);
        if (str == null);
        while (true)
        {
            return;
            this.mWakeLock.acquire();
            Intent localIntent = new Intent("android.bluetooth.device.action.PAIRING_REQUEST");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(str));
            localIntent.putExtra("android.bluetooth.device.extra.PAIRING_KEY", paramInt1);
            localIntent.putExtra("android.bluetooth.device.extra.PAIRING_VARIANT", 4);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
            this.mWakeLock.release();
        }
    }

    private void onHealthDeviceChannelChanged(String paramString1, String paramString2, boolean paramBoolean)
    {
        log("Health Device : devicePath: " + paramString1 + ":channelPath:" + paramString2 + ":exists" + paramBoolean);
        this.mBluetoothService.onHealthDeviceChannelChanged(paramString1, paramString2, paramBoolean);
    }

    private void onHealthDeviceConnectionResult(int paramInt1, int paramInt2)
    {
        log("onHealthDeviceConnectionResult " + paramInt1 + " " + paramInt2);
        if (paramInt2 != 6000)
            this.mBluetoothService.onHealthDeviceChannelConnectionError(paramInt1, 0);
    }

    private void onHealthDevicePropertyChanged(String paramString, String[] paramArrayOfString)
    {
        log("Health Device : Name of Property is: " + paramArrayOfString[0] + " Value:" + paramArrayOfString[1]);
        this.mBluetoothService.onHealthDevicePropertyChanged(paramString, paramArrayOfString[1]);
    }

    private void onInputDeviceConnectionResult(String paramString, int paramInt)
    {
        String str;
        if (paramInt != 5004)
        {
            str = this.mBluetoothService.getAddressFromObjectPath(paramString);
            if (str != null);
        }
        else
        {
            return;
        }
        boolean bool = false;
        BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(str);
        int i = this.mBluetoothService.getInputDeviceConnectionState(localBluetoothDevice);
        if (i == 1)
            if (paramInt == 5001)
                bool = true;
        while (true)
        {
            this.mBluetoothService.handleInputDevicePropertyChange(str, bool);
            break;
            bool = false;
            continue;
            if (i == 3)
            {
                if (paramInt == 5000)
                    bool = false;
                else
                    bool = true;
            }
            else
                Log.e("BluetoothEventLoop", "Error onInputDeviceConnectionResult. State is:" + i);
        }
    }

    private void onInputDevicePropertyChanged(String paramString, String[] paramArrayOfString)
    {
        String str = this.mBluetoothService.getAddressFromObjectPath(paramString);
        if (str == null)
            Log.e("BluetoothEventLoop", "onInputDevicePropertyChanged: Address of the remote device is null");
        while (true)
        {
            return;
            log("Input Device : Name of Property is: " + paramArrayOfString[0]);
            boolean bool = false;
            if (paramArrayOfString[1].equals("true"))
                bool = true;
            this.mBluetoothService.handleInputDevicePropertyChange(str, bool);
        }
    }

    private void onNetworkDeviceConnected(String paramString1, String paramString2, int paramInt)
    {
        BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(paramString1);
        this.mBluetoothService.handlePanDeviceStateChange(localBluetoothDevice, paramString2, 2, 1);
    }

    private void onNetworkDeviceDisconnected(String paramString)
    {
        BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(paramString);
        this.mBluetoothService.handlePanDeviceStateChange(localBluetoothDevice, 0, 1);
    }

    private void onPanDeviceConnectionResult(String paramString, int paramInt)
    {
        log("onPanDeviceConnectionResult " + paramString + " " + paramInt);
        String str;
        if (paramInt != 1004)
        {
            str = this.mBluetoothService.getAddressFromObjectPath(paramString);
            if (str != null);
        }
        else
        {
            return;
        }
        int i = 0;
        BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(str);
        int j = this.mBluetoothService.getPanDeviceConnectionState(localBluetoothDevice);
        if (j == 1)
            if (paramInt == 1001)
            {
                i = 1;
                label94: if (i == 0)
                    break label188;
            }
        label188: for (int k = 2; ; k = 0)
        {
            this.mBluetoothService.handlePanDeviceStateChange(localBluetoothDevice, k, 2);
            break;
            i = 0;
            break label94;
            if (j == 3)
            {
                if (paramInt == 1000)
                {
                    i = 0;
                    break label94;
                }
                i = 1;
                break label94;
            }
            Log.e("BluetoothEventLoop", "Error onPanDeviceConnectionResult. State is: " + j + " result: " + paramInt);
            break label94;
        }
    }

    private void onPanDevicePropertyChanged(String paramString, String[] paramArrayOfString)
    {
        String str1 = paramArrayOfString[0];
        String str2 = this.mBluetoothService.getAddressFromObjectPath(paramString);
        if (str2 == null)
            Log.e("BluetoothEventLoop", "onPanDevicePropertyChanged: Address of the remote device in null");
        while (true)
        {
            return;
            BluetoothDevice localBluetoothDevice = this.mAdapter.getRemoteDevice(str2);
            if (str1.equals("Connected"))
            {
                if (paramArrayOfString[1].equals("false"))
                    this.mBluetoothService.handlePanDeviceStateChange(localBluetoothDevice, 0, 2);
            }
            else if (str1.equals("Interface"))
            {
                String str3 = paramArrayOfString[1];
                if (!str3.equals(""))
                    this.mBluetoothService.handlePanDeviceStateChange(localBluetoothDevice, str3, 2, 2);
            }
        }
    }

    private void onRequestOobData(String paramString, int paramInt)
    {
        String str = checkPairingRequestAndGetAddress(paramString, paramInt);
        if (str == null);
        while (true)
        {
            return;
            Intent localIntent = new Intent("android.bluetooth.device.action.PAIRING_REQUEST");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(str));
            localIntent.putExtra("android.bluetooth.device.extra.PAIRING_VARIANT", 6);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
        }
    }

    private void onRequestPairingConsent(String paramString, int paramInt)
    {
        String str = checkPairingRequestAndGetAddress(paramString, paramInt);
        if (str == null);
        while (true)
        {
            return;
            if (this.mBluetoothService.getBondState(str) == 12)
            {
                Message localMessage = this.mHandler.obtainMessage(1);
                localMessage.obj = str;
                this.mHandler.sendMessageDelayed(localMessage, 1500L);
            }
            else
            {
                this.mWakeLock.acquire();
                Intent localIntent = new Intent("android.bluetooth.device.action.PAIRING_REQUEST");
                localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(str));
                localIntent.putExtra("android.bluetooth.device.extra.PAIRING_VARIANT", 3);
                this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
                this.mWakeLock.release();
            }
        }
    }

    private void onRequestPasskey(String paramString, int paramInt)
    {
        String str = checkPairingRequestAndGetAddress(paramString, paramInt);
        if (str == null);
        while (true)
        {
            return;
            this.mWakeLock.acquire();
            Intent localIntent = new Intent("android.bluetooth.device.action.PAIRING_REQUEST");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(str));
            localIntent.putExtra("android.bluetooth.device.extra.PAIRING_VARIANT", 1);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
            this.mWakeLock.release();
        }
    }

    private void onRequestPasskeyConfirmation(String paramString, int paramInt1, int paramInt2)
    {
        String str = checkPairingRequestAndGetAddress(paramString, paramInt2);
        if (str == null);
        while (true)
        {
            return;
            this.mWakeLock.acquire();
            Intent localIntent = new Intent("android.bluetooth.device.action.PAIRING_REQUEST");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(str));
            localIntent.putExtra("android.bluetooth.device.extra.PAIRING_KEY", paramInt1);
            localIntent.putExtra("android.bluetooth.device.extra.PAIRING_VARIANT", 2);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
            this.mWakeLock.release();
        }
    }

    private void onRequestPinCode(String paramString, int paramInt)
    {
        String str1 = checkPairingRequestAndGetAddress(paramString, paramInt);
        if (str1 == null);
        while (true)
        {
            return;
            String str2 = this.mBluetoothService.getPendingOutgoingBonding();
            int i = new BluetoothClass(this.mBluetoothService.getRemoteClass(str1)).getDeviceClass();
            if (str1.equals(str2))
            {
                if (this.mBluetoothService.isBluetoothDock(str1))
                {
                    String str3 = this.mBluetoothService.getDockPin();
                    this.mBluetoothService.setPin(str1, BluetoothDevice.convertPinToBytes(str3));
                }
                else
                {
                    switch (i)
                    {
                    default:
                    case 1028:
                    case 1032:
                    case 1048:
                    case 1052:
                    case 1064:
                    }
                }
            }
            else
            {
                while (true)
                    if ((i == 1344) || (i == 1472))
                    {
                        if (this.mBluetoothService.isFixedPinZerosAutoPairKeyboard(str1))
                        {
                            this.mBluetoothService.setPin(str1, BluetoothDevice.convertPinToBytes("0000"));
                            break;
                            if (!this.mBluetoothService.attemptAutoPair(str1))
                                continue;
                            break;
                        }
                        sendDisplayPinIntent(str1, (int)Math.floor(10000.0D * Math.random()));
                        break;
                    }
                this.mWakeLock.acquire();
                Intent localIntent = new Intent("android.bluetooth.device.action.PAIRING_REQUEST");
                localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(str1));
                localIntent.putExtra("android.bluetooth.device.extra.PAIRING_VARIANT", 0);
                this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
                this.mWakeLock.release();
            }
        }
    }

    private void sendDisplayPinIntent(String paramString, int paramInt)
    {
        this.mWakeLock.acquire();
        Intent localIntent = new Intent("android.bluetooth.device.action.PAIRING_REQUEST");
        localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(paramString));
        localIntent.putExtra("android.bluetooth.device.extra.PAIRING_KEY", paramInt);
        localIntent.putExtra("android.bluetooth.device.extra.PAIRING_VARIANT", 5);
        this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
        this.mWakeLock.release();
    }

    private native void startEventLoopNative();

    private native void stopEventLoopNative();

    protected void finalize()
        throws Throwable
    {
        try
        {
            cleanupNativeDataNative();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    HashMap<String, Integer> getAuthorizationAgentRequestData()
    {
        return this.mAuthorizationAgentRequestData;
    }

    HashMap<String, Integer> getPasskeyAgentRequestData()
    {
        return this.mPasskeyAgentRequestData;
    }

    void getProfileProxy()
    {
        this.mAdapter.getProfileProxy(this.mContext, this.mProfileServiceListener, 2);
        this.mAdapter.getProfileProxy(this.mContext, this.mProfileServiceListener, 4);
    }

    public boolean isEventLoopRunning()
    {
        return isEventLoopRunningNative();
    }

    void onPropertyChanged(String[] paramArrayOfString)
    {
        BluetoothAdapterProperties localBluetoothAdapterProperties = this.mBluetoothService.getAdapterProperties();
        if (localBluetoothAdapterProperties.isEmpty())
            localBluetoothAdapterProperties.getAllProperties();
        log("Property Changed: " + paramArrayOfString[0] + " : " + paramArrayOfString[1]);
        String str1 = paramArrayOfString[0];
        if (str1.equals("Name"))
        {
            localBluetoothAdapterProperties.setProperty(str1, paramArrayOfString[1]);
            Intent localIntent3 = new Intent("android.bluetooth.adapter.action.LOCAL_NAME_CHANGED");
            localIntent3.putExtra("android.bluetooth.adapter.extra.LOCAL_NAME", paramArrayOfString[1]);
            localIntent3.addFlags(134217728);
            this.mContext.sendBroadcast(localIntent3, "android.permission.BLUETOOTH");
        }
        while (true)
        {
            return;
            if ((str1.equals("Pairable")) || (str1.equals("Discoverable")))
            {
                localBluetoothAdapterProperties.setProperty(str1, paramArrayOfString[1]);
                if (str1.equals("Discoverable"))
                    this.mBluetoothState.sendMessage(53);
                String str2;
                if (str1.equals("Pairable"))
                {
                    str2 = paramArrayOfString[1];
                    label184: if (!str1.equals("Discoverable"))
                        break label293;
                }
                label293: for (String str3 = paramArrayOfString[1]; ; str3 = localBluetoothAdapterProperties.getProperty("Discoverable"))
                {
                    if ((str2 == null) || (str3 == null))
                        break label303;
                    int i = BluetoothService.bluezStringToScanMode(str2.equals("true"), str3.equals("true"));
                    if (i < 0)
                        break;
                    Intent localIntent1 = new Intent("android.bluetooth.adapter.action.SCAN_MODE_CHANGED");
                    localIntent1.putExtra("android.bluetooth.adapter.extra.SCAN_MODE", i);
                    localIntent1.addFlags(134217728);
                    this.mContext.sendBroadcast(localIntent1, "android.permission.BLUETOOTH");
                    break;
                    str2 = localBluetoothAdapterProperties.getProperty("Pairable");
                    break label184;
                }
            }
            else
            {
                label303: if (str1.equals("Discovering"))
                {
                    localBluetoothAdapterProperties.setProperty(str1, paramArrayOfString[1]);
                    if (paramArrayOfString[1].equals("true"));
                    for (Intent localIntent2 = new Intent("android.bluetooth.adapter.action.DISCOVERY_STARTED"); ; localIntent2 = new Intent("android.bluetooth.adapter.action.DISCOVERY_FINISHED"))
                    {
                        this.mContext.sendBroadcast(localIntent2, "android.permission.BLUETOOTH");
                        break;
                        this.mBluetoothService.cancelDiscovery();
                    }
                }
                if ((str1.equals("Devices")) || (str1.equals("UUIDs")))
                {
                    String str4 = null;
                    if (Integer.valueOf(paramArrayOfString[1]).intValue() > 0)
                    {
                        StringBuilder localStringBuilder = new StringBuilder();
                        for (int j = 2; j < paramArrayOfString.length; j++)
                        {
                            localStringBuilder.append(paramArrayOfString[j]);
                            localStringBuilder.append(",");
                        }
                        str4 = localStringBuilder.toString();
                    }
                    localBluetoothAdapterProperties.setProperty(str1, str4);
                    if (str1.equals("UUIDs"))
                        this.mBluetoothService.updateBluetoothState(str4);
                }
                else
                {
                    if (str1.equals("Powered"))
                    {
                        BluetoothAdapterStateMachine localBluetoothAdapterStateMachine = this.mBluetoothState;
                        if (paramArrayOfString[1].equals("true"));
                        for (Boolean localBoolean = new Boolean(true); ; localBoolean = new Boolean(false))
                        {
                            localBluetoothAdapterStateMachine.sendMessage(54, localBoolean);
                            break;
                        }
                    }
                    if (str1.equals("DiscoverableTimeout"))
                        localBluetoothAdapterProperties.setProperty(str1, paramArrayOfString[1]);
                }
            }
        }
    }

    void start()
    {
        if (!isEventLoopRunningNative())
            startEventLoopNative();
    }

    public void stop()
    {
        if (isEventLoopRunningNative())
            stopEventLoopNative();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothEventLoop
 * JD-Core Version:        0.6.2
 */