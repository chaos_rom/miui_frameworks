package android.server.search;

import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class Searchables
{
    public static String ENHANCED_GOOGLE_SEARCH_COMPONENT_NAME = "com.google.android.providers.enhancedgooglesearch/.Launcher";
    private static final Comparator<ResolveInfo> GLOBAL_SEARCH_RANKER = new Comparator()
    {
        public int compare(ResolveInfo paramAnonymousResolveInfo1, ResolveInfo paramAnonymousResolveInfo2)
        {
            int i;
            if (paramAnonymousResolveInfo1 == paramAnonymousResolveInfo2)
                i = 0;
            while (true)
            {
                return i;
                boolean bool1 = Searchables.isSystemApp(paramAnonymousResolveInfo1);
                boolean bool2 = Searchables.isSystemApp(paramAnonymousResolveInfo2);
                if ((bool1) && (!bool2))
                    i = -1;
                else if ((bool2) && (!bool1))
                    i = 1;
                else
                    i = paramAnonymousResolveInfo2.priority - paramAnonymousResolveInfo1.priority;
            }
        }
    };
    public static String GOOGLE_SEARCH_COMPONENT_NAME = "com.android.googlesearch/.GoogleSearch";
    private static final String LOG_TAG = "Searchables";
    private static final String MD_LABEL_DEFAULT_SEARCHABLE = "android.app.default_searchable";
    private static final String MD_SEARCHABLE_SYSTEM_SEARCH = "*";
    private Context mContext;
    private ComponentName mCurrentGlobalSearchActivity = null;
    private List<ResolveInfo> mGlobalSearchActivities;
    private ArrayList<SearchableInfo> mSearchablesInGlobalSearchList = null;
    private ArrayList<SearchableInfo> mSearchablesList = null;
    private HashMap<ComponentName, SearchableInfo> mSearchablesMap = null;
    private ComponentName mWebSearchActivity = null;

    public Searchables(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private List<ResolveInfo> findGlobalSearchActivities()
    {
        Intent localIntent = new Intent("android.search.action.GLOBAL_SEARCH");
        List localList = this.mContext.getPackageManager().queryIntentActivities(localIntent, 65536);
        if ((localList != null) && (!localList.isEmpty()))
            Collections.sort(localList, GLOBAL_SEARCH_RANKER);
        return localList;
    }

    private ComponentName findGlobalSearchActivity(List<ResolveInfo> paramList)
    {
        String str = getGlobalSearchProviderSetting();
        ComponentName localComponentName;
        if (!TextUtils.isEmpty(str))
        {
            localComponentName = ComponentName.unflattenFromString(str);
            if ((localComponentName == null) || (!isInstalled(localComponentName)));
        }
        while (true)
        {
            return localComponentName;
            localComponentName = getDefaultGlobalSearchProvider(paramList);
        }
    }

    private ComponentName findWebSearchActivity(ComponentName paramComponentName)
    {
        ComponentName localComponentName = null;
        if (paramComponentName == null);
        while (true)
        {
            return localComponentName;
            Intent localIntent = new Intent("android.intent.action.WEB_SEARCH");
            localIntent.setPackage(paramComponentName.getPackageName());
            List localList = this.mContext.getPackageManager().queryIntentActivities(localIntent, 65536);
            if ((localList != null) && (!localList.isEmpty()))
            {
                ActivityInfo localActivityInfo = ((ResolveInfo)localList.get(0)).activityInfo;
                localComponentName = new ComponentName(localActivityInfo.packageName, localActivityInfo.name);
            }
            else
            {
                Log.w("Searchables", "No web search activity found");
            }
        }
    }

    private ComponentName getDefaultGlobalSearchProvider(List<ResolveInfo> paramList)
    {
        ActivityInfo localActivityInfo;
        if ((paramList != null) && (!paramList.isEmpty()))
            localActivityInfo = ((ResolveInfo)paramList.get(0)).activityInfo;
        for (ComponentName localComponentName = new ComponentName(localActivityInfo.packageName, localActivityInfo.name); ; localComponentName = null)
        {
            return localComponentName;
            Log.w("Searchables", "No global search activity found");
        }
    }

    private String getGlobalSearchProviderSetting()
    {
        return Settings.Secure.getString(this.mContext.getContentResolver(), "search_global_search_activity");
    }

    private boolean isInstalled(ComponentName paramComponentName)
    {
        Intent localIntent = new Intent("android.search.action.GLOBAL_SEARCH");
        localIntent.setComponent(paramComponentName);
        List localList = this.mContext.getPackageManager().queryIntentActivities(localIntent, 65536);
        if ((localList != null) && (!localList.isEmpty()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static final boolean isSystemApp(ResolveInfo paramResolveInfo)
    {
        if ((0x1 & paramResolveInfo.activityInfo.applicationInfo.flags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void buildSearchableList()
    {
        HashMap localHashMap = new HashMap();
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        PackageManager localPackageManager = this.mContext.getPackageManager();
        List localList1 = localPackageManager.queryIntentActivities(new Intent("android.intent.action.SEARCH"), 128);
        Intent localIntent = new Intent("android.intent.action.WEB_SEARCH");
        List localList2 = localPackageManager.queryIntentActivities(localIntent, 128);
        if ((localList1 != null) || (localList2 != null))
        {
            int i;
            int j;
            label101: int m;
            if (localList1 == null)
            {
                i = 0;
                if (localList2 != null)
                    break label238;
                j = 0;
                int k = i + j;
                m = 0;
                label111: if (m >= k)
                    break label270;
                if (m >= i)
                    break label250;
            }
            label238: label250: for (ResolveInfo localResolveInfo = (ResolveInfo)localList1.get(m); ; localResolveInfo = (ResolveInfo)localList2.get(m - i))
            {
                ActivityInfo localActivityInfo = localResolveInfo.activityInfo;
                if (localHashMap.get(new ComponentName(localActivityInfo.packageName, localActivityInfo.name)) == null)
                {
                    SearchableInfo localSearchableInfo = SearchableInfo.getActivityMetaData(this.mContext, localActivityInfo);
                    if (localSearchableInfo != null)
                    {
                        localArrayList1.add(localSearchableInfo);
                        localHashMap.put(localSearchableInfo.getSearchActivity(), localSearchableInfo);
                        if (localSearchableInfo.shouldIncludeInGlobalSearch())
                            localArrayList2.add(localSearchableInfo);
                    }
                }
                m++;
                break label111;
                i = localList1.size();
                break;
                j = localList2.size();
                break label101;
            }
        }
        label270: List localList3 = findGlobalSearchActivities();
        ComponentName localComponentName1 = findGlobalSearchActivity(localList3);
        ComponentName localComponentName2 = findWebSearchActivity(localComponentName1);
        try
        {
            this.mSearchablesMap = localHashMap;
            this.mSearchablesList = localArrayList1;
            this.mSearchablesInGlobalSearchList = localArrayList2;
            this.mGlobalSearchActivities = localList3;
            this.mCurrentGlobalSearchActivity = localComponentName1;
            this.mWebSearchActivity = localComponentName2;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public ArrayList<ResolveInfo> getGlobalSearchActivities()
    {
        try
        {
            ArrayList localArrayList = new ArrayList(this.mGlobalSearchActivities);
            return localArrayList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public ComponentName getGlobalSearchActivity()
    {
        try
        {
            ComponentName localComponentName = this.mCurrentGlobalSearchActivity;
            return localComponentName;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public SearchableInfo getSearchableInfo(ComponentName paramComponentName)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 55	android/server/search/Searchables:mSearchablesMap	Ljava/util/HashMap;
        //     6: aload_1
        //     7: invokevirtual 210	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     10: checkcast 212	android/app/SearchableInfo
        //     13: astore_3
        //     14: aload_3
        //     15: ifnull +11 -> 26
        //     18: aload_0
        //     19: monitorexit
        //     20: aload_3
        //     21: astore 5
        //     23: goto +226 -> 249
        //     26: aload_0
        //     27: monitorexit
        //     28: aload_0
        //     29: getfield 65	android/server/search/Searchables:mContext	Landroid/content/Context;
        //     32: invokevirtual 85	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     35: aload_1
        //     36: sipush 128
        //     39: invokevirtual 257	android/content/pm/PackageManager:getActivityInfo	(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
        //     42: astore 6
        //     44: aconst_null
        //     45: astore 7
        //     47: aload 6
        //     49: getfield 261	android/content/pm/PackageItemInfo:metaData	Landroid/os/Bundle;
        //     52: astore 8
        //     54: aload 8
        //     56: ifnull +12 -> 68
        //     59: aload 8
        //     61: ldc 18
        //     63: invokevirtual 266	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
        //     66: astore 7
        //     68: aload 7
        //     70: ifnonnull +27 -> 97
        //     73: aload 6
        //     75: getfield 192	android/content/pm/ComponentInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     78: getfield 261	android/content/pm/PackageItemInfo:metaData	Landroid/os/Bundle;
        //     81: astore 15
        //     83: aload 15
        //     85: ifnull +12 -> 97
        //     88: aload 15
        //     90: ldc 18
        //     92: invokevirtual 266	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
        //     95: astore 7
        //     97: aload 7
        //     99: ifnull +153 -> 252
        //     102: aload 7
        //     104: ldc 21
        //     106: invokevirtual 271	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     109: istore 9
        //     111: iload 9
        //     113: ifeq +14 -> 127
        //     116: aconst_null
        //     117: astore 5
        //     119: goto +130 -> 249
        //     122: astore_2
        //     123: aload_0
        //     124: monitorexit
        //     125: aload_2
        //     126: athrow
        //     127: aload_1
        //     128: invokevirtual 135	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     131: astore 10
        //     133: aload 7
        //     135: iconst_0
        //     136: invokevirtual 275	java/lang/String:charAt	(I)C
        //     139: bipush 46
        //     141: if_icmpne +75 -> 216
        //     144: new 117	android/content/ComponentName
        //     147: dup
        //     148: aload 10
        //     150: new 277	java/lang/StringBuilder
        //     153: dup
        //     154: invokespecial 278	java/lang/StringBuilder:<init>	()V
        //     157: aload 10
        //     159: invokevirtual 282	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     162: aload 7
        //     164: invokevirtual 282	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     167: invokevirtual 285	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     170: invokespecial 160	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     173: astore 11
        //     175: aload_0
        //     176: monitorenter
        //     177: aload_0
        //     178: getfield 55	android/server/search/Searchables:mSearchablesMap	Ljava/util/HashMap;
        //     181: aload 11
        //     183: invokevirtual 210	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     186: checkcast 212	android/app/SearchableInfo
        //     189: astore 13
        //     191: aload 13
        //     193: ifnull +39 -> 232
        //     196: aload_0
        //     197: getfield 55	android/server/search/Searchables:mSearchablesMap	Ljava/util/HashMap;
        //     200: aload_1
        //     201: aload 13
        //     203: invokevirtual 228	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     206: pop
        //     207: aload_0
        //     208: monitorexit
        //     209: aload 13
        //     211: astore 5
        //     213: goto +36 -> 249
        //     216: new 117	android/content/ComponentName
        //     219: dup
        //     220: aload 10
        //     222: aload 7
        //     224: invokespecial 160	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     227: astore 11
        //     229: goto -54 -> 175
        //     232: aload_0
        //     233: monitorexit
        //     234: goto +18 -> 252
        //     237: astore 12
        //     239: aload_0
        //     240: monitorexit
        //     241: aload 12
        //     243: athrow
        //     244: astore 4
        //     246: goto +6 -> 252
        //     249: aload 5
        //     251: areturn
        //     252: aconst_null
        //     253: astore 5
        //     255: goto -6 -> 249
        //
        // Exception table:
        //     from	to	target	type
        //     2	28	122	finally
        //     123	125	122	finally
        //     177	209	237	finally
        //     232	241	237	finally
        //     28	111	244	android/content/pm/PackageManager$NameNotFoundException
        //     127	177	244	android/content/pm/PackageManager$NameNotFoundException
        //     216	229	244	android/content/pm/PackageManager$NameNotFoundException
        //     241	244	244	android/content/pm/PackageManager$NameNotFoundException
    }

    /** @deprecated */
    public ArrayList<SearchableInfo> getSearchablesInGlobalSearchList()
    {
        try
        {
            ArrayList localArrayList = new ArrayList(this.mSearchablesInGlobalSearchList);
            return localArrayList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public ArrayList<SearchableInfo> getSearchablesList()
    {
        try
        {
            ArrayList localArrayList = new ArrayList(this.mSearchablesList);
            return localArrayList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public ComponentName getWebSearchActivity()
    {
        try
        {
            ComponentName localComponentName = this.mWebSearchActivity;
            return localComponentName;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.search.Searchables
 * JD-Core Version:        0.6.2
 */