package android.server.search;

import android.app.ISearchManager.Stub;
import android.app.SearchableInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.database.ContentObserver;
import android.os.Process;
import android.provider.Settings.Secure;
import android.util.Log;
import com.android.internal.content.PackageMonitor;
import java.util.List;

public class SearchManagerService extends ISearchManager.Stub
{
    private static final String TAG = "SearchManagerService";
    private final Context mContext;
    private ContentObserver mGlobalSearchObserver;
    private Searchables mSearchables;

    public SearchManagerService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mContext.registerReceiver(new BootCompletedReceiver(null), new IntentFilter("android.intent.action.BOOT_COMPLETED"));
        this.mGlobalSearchObserver = new GlobalSearchProviderObserver(this.mContext.getContentResolver());
    }

    /** @deprecated */
    private Searchables getSearchables()
    {
        try
        {
            if (this.mSearchables == null)
            {
                Log.i("SearchManagerService", "Building list of searchable activities");
                new MyPackageMonitor().register(this.mContext, null, true);
                this.mSearchables = new Searchables(this.mContext);
                this.mSearchables.buildSearchableList();
            }
            Searchables localSearchables = this.mSearchables;
            return localSearchables;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public List<ResolveInfo> getGlobalSearchActivities()
    {
        return getSearchables().getGlobalSearchActivities();
    }

    public ComponentName getGlobalSearchActivity()
    {
        return getSearchables().getGlobalSearchActivity();
    }

    public SearchableInfo getSearchableInfo(ComponentName paramComponentName)
    {
        if (paramComponentName == null)
            Log.e("SearchManagerService", "getSearchableInfo(), activity == null");
        for (SearchableInfo localSearchableInfo = null; ; localSearchableInfo = getSearchables().getSearchableInfo(paramComponentName))
            return localSearchableInfo;
    }

    public List<SearchableInfo> getSearchablesInGlobalSearch()
    {
        return getSearchables().getSearchablesInGlobalSearchList();
    }

    public ComponentName getWebSearchActivity()
    {
        return getSearchables().getWebSearchActivity();
    }

    class GlobalSearchProviderObserver extends ContentObserver
    {
        private final ContentResolver mResolver;

        public GlobalSearchProviderObserver(ContentResolver arg2)
        {
            super();
            Object localObject;
            this.mResolver = localObject;
            this.mResolver.registerContentObserver(Settings.Secure.getUriFor("search_global_search_activity"), false, this);
        }

        public void onChange(boolean paramBoolean)
        {
            SearchManagerService.this.getSearchables().buildSearchableList();
            Intent localIntent = new Intent("android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED");
            localIntent.addFlags(536870912);
            SearchManagerService.this.mContext.sendBroadcast(localIntent);
        }
    }

    class MyPackageMonitor extends PackageMonitor
    {
        MyPackageMonitor()
        {
        }

        private void updateSearchables()
        {
            SearchManagerService.this.getSearchables().buildSearchableList();
            Intent localIntent = new Intent("android.search.action.SEARCHABLES_CHANGED");
            localIntent.addFlags(536870912);
            SearchManagerService.this.mContext.sendBroadcast(localIntent);
        }

        public void onPackageModified(String paramString)
        {
            updateSearchables();
        }

        public void onSomePackagesChanged()
        {
            updateSearchables();
        }
    }

    private final class BootCompletedReceiver extends BroadcastReceiver
    {
        private BootCompletedReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            new Thread()
            {
                public void run()
                {
                    Process.setThreadPriority(10);
                    SearchManagerService.this.mContext.unregisterReceiver(SearchManagerService.BootCompletedReceiver.this);
                    SearchManagerService.this.getSearchables();
                }
            }
            .start();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.search.SearchManagerService
 * JD-Core Version:        0.6.2
 */