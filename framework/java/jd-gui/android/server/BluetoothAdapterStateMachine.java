package android.server;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.IBluetoothStateChangeCallback;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings.Secure;
import android.util.Log;
import com.android.internal.util.IState;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;

final class BluetoothAdapterStateMachine extends StateMachine
{
    static final int AIRPLANE_MODE_OFF = 56;
    static final int AIRPLANE_MODE_ON = 55;
    static final int ALL_DEVICES_DISCONNECTED = 52;
    private static final boolean DBG = false;
    private static final int DEVICES_DISCONNECT_TIMEOUT = 103;
    private static final int DEVICES_DISCONNECT_TIMEOUT_TIME = 3000;
    static final int PER_PROCESS_TURN_OFF = 4;
    static final int PER_PROCESS_TURN_ON = 3;
    private static final int POWER_DOWN_TIMEOUT = 106;
    private static final int POWER_DOWN_TIMEOUT_TIME = 20;
    static final int POWER_STATE_CHANGED = 54;
    private static final int PREPARE_BLUETOOTH_TIMEOUT = 104;
    private static final int PREPARE_BLUETOOTH_TIMEOUT_TIME = 10000;
    static final int SCAN_MODE_CHANGED = 53;
    static final int SERVICE_RECORD_LOADED = 51;
    private static final String TAG = "BluetoothAdapterStateMachine";
    private static final int TURN_COLD = 102;
    static final int TURN_HOT = 5;
    private static final int TURN_OFF_TIMEOUT = 105;
    private static final int TURN_OFF_TIMEOUT_TIME = 5000;
    private static final int TURN_ON_CONTINUE = 101;
    static final int USER_TURN_OFF = 2;
    static final int USER_TURN_ON = 1;
    private BluetoothOn mBluetoothOn;
    private BluetoothService mBluetoothService;
    private Context mContext;
    private boolean mDelayBroadcastStateOff;
    private BluetoothEventLoop mEventLoop;
    private HotOff mHotOff;
    private PerProcessState mPerProcessState;
    private PowerOff mPowerOff;
    private int mPublicState;
    private Switching mSwitching;
    private WarmUp mWarmUp;

    BluetoothAdapterStateMachine(Context paramContext, BluetoothService paramBluetoothService, BluetoothAdapter paramBluetoothAdapter)
    {
        super("BluetoothAdapterStateMachine");
        this.mContext = paramContext;
        this.mBluetoothService = paramBluetoothService;
        this.mEventLoop = new BluetoothEventLoop(paramContext, paramBluetoothAdapter, paramBluetoothService, this);
        this.mBluetoothOn = new BluetoothOn(null);
        this.mSwitching = new Switching(null);
        this.mHotOff = new HotOff(null);
        this.mWarmUp = new WarmUp(null);
        this.mPowerOff = new PowerOff(null);
        this.mPerProcessState = new PerProcessState(null);
        addState(this.mBluetoothOn);
        addState(this.mSwitching);
        addState(this.mHotOff);
        addState(this.mWarmUp);
        addState(this.mPowerOff);
        addState(this.mPerProcessState);
        setInitialState(this.mPowerOff);
        this.mPublicState = 10;
        this.mDelayBroadcastStateOff = false;
    }

    private void allProcessesCallback(boolean paramBoolean)
    {
        Iterator localIterator = this.mBluetoothService.getApplicationStateChangeCallbacks().iterator();
        while (localIterator.hasNext())
            perProcessCallback(paramBoolean, (IBluetoothStateChangeCallback)localIterator.next());
        if (!paramBoolean)
            this.mBluetoothService.clearApplicationStateChangeTracker();
    }

    private void broadcastState(int paramInt)
    {
        log("Bluetooth state " + this.mPublicState + " -> " + paramInt);
        if (this.mPublicState == paramInt);
        while (true)
        {
            return;
            Intent localIntent = new Intent("android.bluetooth.adapter.action.STATE_CHANGED");
            localIntent.putExtra("android.bluetooth.adapter.extra.PREVIOUS_STATE", this.mPublicState);
            localIntent.putExtra("android.bluetooth.adapter.extra.STATE", paramInt);
            localIntent.addFlags(134217728);
            this.mPublicState = paramInt;
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
        }
    }

    private void dump(PrintWriter paramPrintWriter)
    {
        IState localIState = getCurrentState();
        if (localIState == this.mPowerOff)
            paramPrintWriter.println("Bluetooth OFF - power down\n");
        while (true)
        {
            return;
            if (localIState == this.mWarmUp)
                paramPrintWriter.println("Bluetooth OFF - warm up\n");
            else if (localIState == this.mHotOff)
                paramPrintWriter.println("Bluetooth OFF - hot but off\n");
            else if (localIState == this.mSwitching)
                paramPrintWriter.println("Bluetooth Switching\n");
            else if (localIState == this.mBluetoothOn)
                paramPrintWriter.println("Bluetooth ON\n");
            else
                paramPrintWriter.println("ERROR: Bluetooth UNKNOWN STATE ");
        }
    }

    private void finishSwitchingOff()
    {
        this.mBluetoothService.finishDisable();
        broadcastState(10);
        this.mBluetoothService.cleanupAfterFinishDisable();
    }

    private boolean getBluetoothPersistedSetting()
    {
        boolean bool = false;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "bluetooth_on", 0) > 0)
            bool = true;
        return bool;
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothAdapterStateMachine", paramString);
    }

    private void perProcessCallback(boolean paramBoolean, IBluetoothStateChangeCallback paramIBluetoothStateChangeCallback)
    {
        if (paramIBluetoothStateChangeCallback == null);
        while (true)
        {
            return;
            try
            {
                paramIBluetoothStateChangeCallback.onBluetoothStateChange(paramBoolean);
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    private void persistSwitchSetting(boolean paramBoolean)
    {
        long l = Binder.clearCallingIdentity();
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Settings.Secure.putInt(localContentResolver, "bluetooth_on", i);
            Binder.restoreCallingIdentity(l);
            return;
        }
    }

    private void recoverStateMachine(int paramInt, Object paramObject)
    {
        Log.e("BluetoothAdapterStateMachine", "Get unexpected power on event, reset with: " + paramInt);
        transitionTo(this.mHotOff);
        deferMessage(obtainMessage(102));
        deferMessage(obtainMessage(paramInt, paramObject));
    }

    private void shutoffBluetooth()
    {
        this.mBluetoothService.shutoffBluetooth();
        this.mEventLoop.stop();
        this.mBluetoothService.cleanNativeAfterShutoffBluetooth();
    }

    int getBluetoothAdapterState()
    {
        return this.mPublicState;
    }

    BluetoothEventLoop getBluetoothEventLoop()
    {
        return this.mEventLoop;
    }

    private class PerProcessState extends State
    {
        boolean isTurningOn = false;
        IBluetoothStateChangeCallback mCallback = null;

        private PerProcessState()
        {
        }

        public void enter()
        {
            int i = BluetoothAdapterStateMachine.this.getCurrentMessage().what;
            if (i == 3)
                this.isTurningOn = true;
            while (true)
            {
                return;
                if (i == 2)
                    this.isTurningOn = false;
                else
                    Log.e("BluetoothAdapterStateMachine", "enter PerProcessState: wrong msg: " + i);
            }
        }

        public boolean processMessage(Message paramMessage)
        {
            BluetoothAdapterStateMachine.log("PerProcessState process message: " + paramMessage.what);
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 3:
            case 53:
            case 54:
            case 105:
            case 1:
            case 5:
            case 52:
            case 103:
            case 4:
            case 55:
            case 2:
            }
            while (true)
            {
                return bool;
                this.mCallback = ((IBluetoothStateChangeCallback)BluetoothAdapterStateMachine.this.getCurrentMessage().obj);
                if (BluetoothAdapterStateMachine.this.mBluetoothService.getNumberOfApplicationStateChangeTrackers() > 1)
                {
                    BluetoothAdapterStateMachine.this.perProcessCallback(true, this.mCallback);
                    continue;
                    if (this.isTurningOn)
                    {
                        BluetoothAdapterStateMachine.this.perProcessCallback(true, this.mCallback);
                        this.isTurningOn = false;
                        continue;
                        BluetoothAdapterStateMachine.this.removeMessages(105);
                        if (!((Boolean)paramMessage.obj).booleanValue())
                        {
                            BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mHotOff);
                            if (!BluetoothAdapterStateMachine.this.mContext.getResources().getBoolean(17891367))
                                BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(102));
                        }
                        else if (!this.isTurningOn)
                        {
                            BluetoothAdapterStateMachine.this.recoverStateMachine(102, null);
                            Iterator localIterator2 = BluetoothAdapterStateMachine.this.mBluetoothService.getApplicationStateChangeCallbacks().iterator();
                            while (localIterator2.hasNext())
                            {
                                IBluetoothStateChangeCallback localIBluetoothStateChangeCallback2 = (IBluetoothStateChangeCallback)localIterator2.next();
                                BluetoothAdapterStateMachine.this.perProcessCallback(false, localIBluetoothStateChangeCallback2);
                                BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(3, localIBluetoothStateChangeCallback2));
                            }
                            BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mHotOff);
                            Log.e("BluetoothAdapterStateMachine", "Power-down timed out, resetting...");
                            BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(102));
                            if (BluetoothAdapterStateMachine.this.mContext.getResources().getBoolean(17891367))
                            {
                                BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(5));
                                continue;
                                BluetoothAdapterStateMachine.this.broadcastState(11);
                                BluetoothAdapterStateMachine.this.persistSwitchSetting(true);
                                BluetoothAdapterStateMachine.this.mBluetoothService.initBluetoothAfterTurningOn();
                                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mBluetoothOn);
                                BluetoothAdapterStateMachine.this.broadcastState(12);
                                BluetoothAdapterStateMachine.this.mBluetoothService.runBluetooth();
                                continue;
                                BluetoothAdapterStateMachine.this.broadcastState(13);
                                if (BluetoothAdapterStateMachine.this.mBluetoothService.getAdapterConnectionState() != 0)
                                {
                                    BluetoothAdapterStateMachine.this.mBluetoothService.disconnectDevices();
                                    BluetoothAdapterStateMachine.this.sendMessageDelayed(103, 3000L);
                                }
                                else
                                {
                                    BluetoothAdapterStateMachine.this.removeMessages(103);
                                    BluetoothAdapterStateMachine.this.finishSwitchingOff();
                                    continue;
                                    BluetoothAdapterStateMachine.this.finishSwitchingOff();
                                    Log.e("BluetoothAdapterStateMachine", "Devices fail to disconnect, reseting...");
                                    BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mHotOff);
                                    BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(102));
                                    Iterator localIterator1 = BluetoothAdapterStateMachine.this.mBluetoothService.getApplicationStateChangeCallbacks().iterator();
                                    while (localIterator1.hasNext())
                                    {
                                        IBluetoothStateChangeCallback localIBluetoothStateChangeCallback1 = (IBluetoothStateChangeCallback)localIterator1.next();
                                        BluetoothAdapterStateMachine.this.perProcessCallback(false, localIBluetoothStateChangeCallback1);
                                        BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(3, localIBluetoothStateChangeCallback1));
                                    }
                                    BluetoothAdapterStateMachine.this.perProcessCallback(false, (IBluetoothStateChangeCallback)paramMessage.obj);
                                    if (BluetoothAdapterStateMachine.this.mBluetoothService.isApplicationStateChangeTrackerEmpty())
                                    {
                                        BluetoothAdapterStateMachine.this.mBluetoothService.switchConnectable(false);
                                        BluetoothAdapterStateMachine.this.sendMessageDelayed(105, 5000L);
                                        continue;
                                        BluetoothAdapterStateMachine.this.mBluetoothService.switchConnectable(false);
                                        BluetoothAdapterStateMachine.this.sendMessageDelayed(105, 5000L);
                                        BluetoothAdapterStateMachine.this.allProcessesCallback(false);
                                        continue;
                                        Log.w("BluetoothAdapterStateMachine", "PerProcessState received: " + paramMessage.what);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private class BluetoothOn extends State
    {
        private BluetoothOn()
        {
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            BluetoothAdapterStateMachine.log("BluetoothOn process message: " + paramMessage.what);
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 2:
            case 55:
            case 1:
            case 56:
            case 3:
            case 4:
            case 54:
            }
            while (true)
            {
                return bool;
                if (((Boolean)paramMessage.obj).booleanValue())
                    BluetoothAdapterStateMachine.this.persistSwitchSetting(false);
                if (BluetoothAdapterStateMachine.this.mBluetoothService.isDiscovering())
                    BluetoothAdapterStateMachine.this.mBluetoothService.cancelDiscovery();
                if (!BluetoothAdapterStateMachine.this.mBluetoothService.isApplicationStateChangeTrackerEmpty())
                {
                    BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mPerProcessState);
                    BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(5));
                }
                else
                {
                    BluetoothAdapterStateMachine.this.broadcastState(13);
                    BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mSwitching);
                    if (BluetoothAdapterStateMachine.this.mBluetoothService.getAdapterConnectionState() != 0)
                    {
                        BluetoothAdapterStateMachine.this.mBluetoothService.disconnectDevices();
                        BluetoothAdapterStateMachine.this.sendMessageDelayed(103, 3000L);
                    }
                    while ((paramMessage.what == 55) || (BluetoothAdapterStateMachine.this.mBluetoothService.isAirplaneModeOn()))
                    {
                        BluetoothAdapterStateMachine.this.allProcessesCallback(false);
                        break;
                        BluetoothAdapterStateMachine.this.mBluetoothService.switchConnectable(false);
                        BluetoothAdapterStateMachine.this.sendMessageDelayed(105, 5000L);
                    }
                    Log.w("BluetoothAdapterStateMachine", "BluetoothOn received: " + paramMessage.what);
                    continue;
                    BluetoothAdapterStateMachine.this.perProcessCallback(true, (IBluetoothStateChangeCallback)paramMessage.obj);
                    continue;
                    BluetoothAdapterStateMachine.this.perProcessCallback(false, (IBluetoothStateChangeCallback)paramMessage.obj);
                    continue;
                    if (((Boolean)paramMessage.obj).booleanValue())
                        BluetoothAdapterStateMachine.this.recoverStateMachine(1, Boolean.valueOf(false));
                }
            }
        }
    }

    private class Switching extends State
    {
        private Switching()
        {
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            BluetoothAdapterStateMachine.log("Switching process message: " + paramMessage.what);
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 53:
            case 54:
            case 52:
            case 103:
            case 105:
            case 1:
            case 2:
            case 3:
            case 4:
            case 55:
            case 56:
            }
            while (true)
            {
                return bool;
                if (BluetoothAdapterStateMachine.this.mPublicState == 11)
                {
                    BluetoothAdapterStateMachine.this.mBluetoothService.setPairable();
                    BluetoothAdapterStateMachine.this.mBluetoothService.initBluetoothAfterTurningOn();
                    BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mBluetoothOn);
                    BluetoothAdapterStateMachine.this.broadcastState(12);
                    BluetoothAdapterStateMachine.this.mBluetoothService.runBluetooth();
                    continue;
                    BluetoothAdapterStateMachine.this.removeMessages(105);
                    if (!((Boolean)paramMessage.obj).booleanValue())
                    {
                        if (BluetoothAdapterStateMachine.this.mPublicState == 13)
                        {
                            BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mHotOff);
                            BluetoothAdapterStateMachine.this.mBluetoothService.finishDisable();
                            BluetoothAdapterStateMachine.this.mBluetoothService.cleanupAfterFinishDisable();
                            BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(102));
                            if ((BluetoothAdapterStateMachine.this.mContext.getResources().getBoolean(17891367)) && (!BluetoothAdapterStateMachine.this.mBluetoothService.isAirplaneModeOn()))
                            {
                                BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(5));
                                BluetoothAdapterStateMachine.access$2902(BluetoothAdapterStateMachine.this, true);
                            }
                        }
                    }
                    else if (BluetoothAdapterStateMachine.this.mPublicState != 11)
                        if (BluetoothAdapterStateMachine.this.mContext.getResources().getBoolean(17891367))
                        {
                            BluetoothAdapterStateMachine.this.recoverStateMachine(5, null);
                        }
                        else
                        {
                            BluetoothAdapterStateMachine.this.recoverStateMachine(102, null);
                            continue;
                            BluetoothAdapterStateMachine.this.removeMessages(103);
                            BluetoothAdapterStateMachine.this.mBluetoothService.switchConnectable(false);
                            BluetoothAdapterStateMachine.this.sendMessageDelayed(105, 5000L);
                            continue;
                            BluetoothAdapterStateMachine.this.sendMessage(52);
                            Log.e("BluetoothAdapterStateMachine", "Devices failed to disconnect, reseting...");
                            BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(102));
                            if (BluetoothAdapterStateMachine.this.mContext.getResources().getBoolean(17891367))
                            {
                                BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(5));
                                continue;
                                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mHotOff);
                                BluetoothAdapterStateMachine.this.finishSwitchingOff();
                                Log.e("BluetoothAdapterStateMachine", "Devices failed to power down, reseting...");
                                BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(102));
                                if (BluetoothAdapterStateMachine.this.mContext.getResources().getBoolean(17891367))
                                {
                                    BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(5));
                                    continue;
                                    BluetoothAdapterStateMachine.this.deferMessage(paramMessage);
                                }
                            }
                        }
                }
            }
        }
    }

    private class HotOff extends State
    {
        private HotOff()
        {
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            BluetoothAdapterStateMachine.log("HotOff process message: " + paramMessage.what);
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 2:
            case 1:
            case 101:
            case 55:
            case 102:
            case 106:
            case 56:
            case 3:
            case 4:
            case 54:
            case 5:
            }
            while (true)
            {
                return bool;
                BluetoothAdapterStateMachine.this.broadcastState(11);
                if (((Boolean)paramMessage.obj).booleanValue())
                    BluetoothAdapterStateMachine.this.persistSwitchSetting(true);
                BluetoothAdapterStateMachine.this.mBluetoothService.switchConnectable(true);
                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mSwitching);
                continue;
                BluetoothAdapterStateMachine.this.shutoffBluetooth();
                BluetoothAdapterStateMachine.this.sendMessageDelayed(106, 20L);
                continue;
                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mPowerOff);
                if (!BluetoothAdapterStateMachine.this.mDelayBroadcastStateOff)
                {
                    BluetoothAdapterStateMachine.this.broadcastState(10);
                    continue;
                    if (BluetoothAdapterStateMachine.this.getBluetoothPersistedSetting())
                    {
                        BluetoothAdapterStateMachine.this.broadcastState(11);
                        BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mSwitching);
                        BluetoothAdapterStateMachine.this.mBluetoothService.switchConnectable(true);
                        continue;
                        BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mPerProcessState);
                        BluetoothAdapterStateMachine.this.deferMessage(paramMessage);
                        BluetoothAdapterStateMachine.this.mBluetoothService.switchConnectable(true);
                        continue;
                        BluetoothAdapterStateMachine.this.perProcessCallback(false, (IBluetoothStateChangeCallback)paramMessage.obj);
                        continue;
                        if (((Boolean)paramMessage.obj).booleanValue())
                        {
                            BluetoothAdapterStateMachine.this.recoverStateMachine(5, null);
                            continue;
                            BluetoothAdapterStateMachine.this.deferMessage(paramMessage);
                        }
                    }
                }
            }
        }
    }

    private class WarmUp extends State
    {
        private WarmUp()
        {
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            BluetoothAdapterStateMachine.log("WarmUp process message: " + paramMessage.what);
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 51:
            case 104:
            case 1:
            case 3:
            case 4:
            case 55:
            case 56:
            case 101:
            case 2:
            }
            while (true)
            {
                return bool;
                BluetoothAdapterStateMachine.this.removeMessages(104);
                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mHotOff);
                if (BluetoothAdapterStateMachine.this.mDelayBroadcastStateOff)
                {
                    BluetoothAdapterStateMachine.this.broadcastState(10);
                    BluetoothAdapterStateMachine.access$2902(BluetoothAdapterStateMachine.this, false);
                    continue;
                    Log.e("BluetoothAdapterStateMachine", "Bluetooth adapter SDP failed to load");
                    BluetoothAdapterStateMachine.this.shutoffBluetooth();
                    BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mPowerOff);
                    BluetoothAdapterStateMachine.this.broadcastState(10);
                    continue;
                    BluetoothAdapterStateMachine.this.deferMessage(paramMessage);
                    continue;
                    Log.w("BluetoothAdapterStateMachine", "WarmUp received: " + paramMessage.what);
                }
            }
        }
    }

    private class PowerOff extends State
    {
        private PowerOff()
        {
        }

        private boolean prepareBluetooth()
        {
            boolean bool = false;
            if (BluetoothAdapterStateMachine.this.mBluetoothService.enableNative() != 0);
            while (true)
            {
                return bool;
                int i = 0;
                int j = 2;
                int k = j - 1;
                if ((j > 0) && (i == 0))
                {
                    BluetoothAdapterStateMachine.this.mEventLoop.start();
                    int m = 5;
                    int n = m - 1;
                    if ((m > 0) && (i == 0))
                    {
                        if (!BluetoothAdapterStateMachine.this.mEventLoop.isEventLoopRunning())
                            break label83;
                        i = 1;
                    }
                    while (true)
                        while (true)
                        {
                            j = k;
                            break;
                            try
                            {
                                label83: Thread.sleep(100L);
                                m = n;
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                                BluetoothAdapterStateMachine.log("prepareBluetooth sleep interrupted: " + n);
                            }
                        }
                }
                if (i == 0)
                {
                    BluetoothAdapterStateMachine.this.mBluetoothService.disableNative();
                }
                else if (!BluetoothAdapterStateMachine.this.mBluetoothService.prepareBluetooth())
                {
                    BluetoothAdapterStateMachine.this.mEventLoop.stop();
                    BluetoothAdapterStateMachine.this.mBluetoothService.disableNative();
                }
                else
                {
                    BluetoothAdapterStateMachine.this.sendMessageDelayed(104, 10000L);
                    bool = true;
                }
            }
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            BluetoothAdapterStateMachine.log("PowerOff process message: " + paramMessage.what);
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 55:
            case 1:
            case 5:
            case 56:
            case 3:
            case 4:
            case 2:
            }
            while (true)
            {
                return bool;
                BluetoothAdapterStateMachine.this.broadcastState(11);
                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mWarmUp);
                if (prepareBluetooth())
                {
                    if (((Boolean)paramMessage.obj).booleanValue())
                        BluetoothAdapterStateMachine.this.persistSwitchSetting(true);
                    BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(101));
                }
                else
                {
                    Log.e("BluetoothAdapterStateMachine", "failed to prepare bluetooth, abort turning on");
                    BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mPowerOff);
                    BluetoothAdapterStateMachine.this.broadcastState(10);
                    continue;
                    if (prepareBluetooth())
                    {
                        BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mWarmUp);
                        continue;
                        if (BluetoothAdapterStateMachine.this.getBluetoothPersistedSetting())
                        {
                            BluetoothAdapterStateMachine.this.broadcastState(11);
                            BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mWarmUp);
                            if (prepareBluetooth())
                            {
                                BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(101));
                                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mWarmUp);
                            }
                            else
                            {
                                Log.e("BluetoothAdapterStateMachine", "failed to prepare bluetooth, abort turning on");
                                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mPowerOff);
                                BluetoothAdapterStateMachine.this.broadcastState(10);
                            }
                        }
                        else if (BluetoothAdapterStateMachine.this.mContext.getResources().getBoolean(17891367))
                        {
                            BluetoothAdapterStateMachine.this.sendMessage(5);
                            continue;
                            if (prepareBluetooth())
                                BluetoothAdapterStateMachine.this.transitionTo(BluetoothAdapterStateMachine.this.mWarmUp);
                            BluetoothAdapterStateMachine.this.deferMessage(BluetoothAdapterStateMachine.this.obtainMessage(3));
                            continue;
                            BluetoothAdapterStateMachine.this.perProcessCallback(false, (IBluetoothStateChangeCallback)paramMessage.obj);
                            continue;
                            Log.w("BluetoothAdapterStateMachine", "PowerOff received: " + paramMessage.what);
                        }
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothAdapterStateMachine
 * JD-Core Version:        0.6.2
 */