package android.server;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

class BluetoothBondState
{
    private static final String AUTO_PAIRING_BLACKLIST = "/etc/bluetooth/auto_pairing.conf";
    private static final boolean DBG = true;
    private static final String DYNAMIC_AUTO_PAIRING_BLACKLIST = "/data/misc/bluetooth/dynamic_auto_pairing.conf";
    private static final String TAG = "BluetoothBondState";
    private BluetoothA2dp mA2dpProxy;
    private ArrayList<String> mAutoPairingAddressBlacklist;
    private ArrayList<String> mAutoPairingDynamicAddressBlacklist;
    private ArrayList<String> mAutoPairingExactNameBlacklist;
    private ArrayList<String> mAutoPairingFixedPinZerosKeyboardList;
    private ArrayList<String> mAutoPairingPartialNameBlacklist;
    private final BluetoothInputProfileHandler mBluetoothInputProfileHandler;
    private final Context mContext;
    private BluetoothHeadset mHeadsetProxy;
    private ArrayList<String> mPairingRequestRcvd = new ArrayList();
    private String mPendingOutgoingBonding;
    private final HashMap<String, Integer> mPinAttempt = new HashMap();
    private BluetoothProfile.ServiceListener mProfileServiceListener = new BluetoothProfile.ServiceListener()
    {
        public void onServiceConnected(int paramAnonymousInt, BluetoothProfile paramAnonymousBluetoothProfile)
        {
            if (paramAnonymousInt == 2)
                BluetoothBondState.access$002(BluetoothBondState.this, (BluetoothA2dp)paramAnonymousBluetoothProfile);
            while (true)
            {
                return;
                if (paramAnonymousInt == 1)
                    BluetoothBondState.access$102(BluetoothBondState.this, (BluetoothHeadset)paramAnonymousBluetoothProfile);
            }
        }

        public void onServiceDisconnected(int paramAnonymousInt)
        {
            if (paramAnonymousInt == 2)
                BluetoothBondState.access$002(BluetoothBondState.this, null);
            while (true)
            {
                return;
                if (paramAnonymousInt == 1)
                    BluetoothBondState.access$102(BluetoothBondState.this, null);
            }
        }
    };
    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (paramAnonymousIntent == null);
            while (true)
            {
                return;
                if (paramAnonymousIntent.getAction().equals("android.bluetooth.device.action.PAIRING_REQUEST"))
                {
                    String str = ((BluetoothDevice)paramAnonymousIntent.getParcelableExtra("android.bluetooth.device.extra.DEVICE")).getAddress();
                    BluetoothBondState.this.mPairingRequestRcvd.add(str);
                }
            }
        }
    };
    private final BluetoothService mService;
    private final HashMap<String, Integer> mState = new HashMap();

    BluetoothBondState(Context paramContext, BluetoothService paramBluetoothService)
    {
        this.mContext = paramContext;
        this.mService = paramBluetoothService;
        this.mBluetoothInputProfileHandler = BluetoothInputProfileHandler.getInstance(this.mContext, this.mService);
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        this.mContext.registerReceiver(this.mReceiver, localIntentFilter);
        readAutoPairingData();
    }

    private void closeProfileProxy()
    {
        BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.mA2dpProxy != null)
            localBluetoothAdapter.closeProfileProxy(2, this.mA2dpProxy);
        if (this.mHeadsetProxy != null)
            localBluetoothAdapter.closeProfileProxy(1, this.mHeadsetProxy);
    }

    // ERROR //
    private void copyAutoPairingData()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: aconst_null
        //     3: astore_2
        //     4: new 130	java/io/File
        //     7: dup
        //     8: ldc 18
        //     10: invokespecial 132	java/io/File:<init>	(Ljava/lang/String;)V
        //     13: invokevirtual 136	java/io/File:exists	()Z
        //     16: istore 11
        //     18: iload 11
        //     20: ifeq +16 -> 36
        //     23: iconst_0
        //     24: ifeq +5 -> 29
        //     27: aconst_null
        //     28: athrow
        //     29: iconst_0
        //     30: ifeq +5 -> 35
        //     33: aconst_null
        //     34: athrow
        //     35: return
        //     36: new 138	java/io/FileInputStream
        //     39: dup
        //     40: ldc 12
        //     42: invokespecial 139	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     45: astore 12
        //     47: new 141	java/io/FileOutputStream
        //     50: dup
        //     51: ldc 18
        //     53: invokespecial 142	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
        //     56: astore 13
        //     58: sipush 1024
        //     61: newarray byte
        //     63: astore 14
        //     65: aload 12
        //     67: aload 14
        //     69: invokevirtual 146	java/io/FileInputStream:read	([B)I
        //     72: istore 15
        //     74: iload 15
        //     76: ifle +72 -> 148
        //     79: aload 13
        //     81: aload 14
        //     83: iconst_0
        //     84: iload 15
        //     86: invokevirtual 150	java/io/FileOutputStream:write	([BII)V
        //     89: goto -24 -> 65
        //     92: astore_3
        //     93: aload 13
        //     95: astore_2
        //     96: aload 12
        //     98: astore_1
        //     99: ldc 21
        //     101: new 152	java/lang/StringBuilder
        //     104: dup
        //     105: invokespecial 153	java/lang/StringBuilder:<init>	()V
        //     108: ldc 155
        //     110: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     113: aload_3
        //     114: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     117: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     120: invokestatic 172	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     123: pop
        //     124: aload_1
        //     125: ifnull +7 -> 132
        //     128: aload_1
        //     129: invokevirtual 175	java/io/FileInputStream:close	()V
        //     132: aload_2
        //     133: ifnull -98 -> 35
        //     136: aload_2
        //     137: invokevirtual 176	java/io/FileOutputStream:close	()V
        //     140: goto -105 -> 35
        //     143: astore 7
        //     145: goto -110 -> 35
        //     148: aload 12
        //     150: ifnull +8 -> 158
        //     153: aload 12
        //     155: invokevirtual 175	java/io/FileInputStream:close	()V
        //     158: aload 13
        //     160: ifnull +8 -> 168
        //     163: aload 13
        //     165: invokevirtual 176	java/io/FileOutputStream:close	()V
        //     168: goto -133 -> 35
        //     171: astore 16
        //     173: goto -138 -> 35
        //     176: astore 8
        //     178: ldc 21
        //     180: new 152	java/lang/StringBuilder
        //     183: dup
        //     184: invokespecial 153	java/lang/StringBuilder:<init>	()V
        //     187: ldc 178
        //     189: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     192: aload 8
        //     194: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     197: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     200: invokestatic 172	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     203: pop
        //     204: aload_1
        //     205: ifnull +7 -> 212
        //     208: aload_1
        //     209: invokevirtual 175	java/io/FileInputStream:close	()V
        //     212: aload_2
        //     213: ifnull -178 -> 35
        //     216: aload_2
        //     217: invokevirtual 176	java/io/FileOutputStream:close	()V
        //     220: goto -185 -> 35
        //     223: astore 10
        //     225: goto -190 -> 35
        //     228: astore 4
        //     230: aload_1
        //     231: ifnull +7 -> 238
        //     234: aload_1
        //     235: invokevirtual 175	java/io/FileInputStream:close	()V
        //     238: aload_2
        //     239: ifnull +7 -> 246
        //     242: aload_2
        //     243: invokevirtual 176	java/io/FileOutputStream:close	()V
        //     246: aload 4
        //     248: athrow
        //     249: astore 5
        //     251: goto -5 -> 246
        //     254: astore 4
        //     256: aload 12
        //     258: astore_1
        //     259: goto -29 -> 230
        //     262: astore 4
        //     264: aload 13
        //     266: astore_2
        //     267: aload 12
        //     269: astore_1
        //     270: goto -40 -> 230
        //     273: astore 8
        //     275: aload 12
        //     277: astore_1
        //     278: goto -100 -> 178
        //     281: astore 8
        //     283: aload 13
        //     285: astore_2
        //     286: aload 12
        //     288: astore_1
        //     289: goto -111 -> 178
        //     292: astore_3
        //     293: goto -194 -> 99
        //     296: astore_3
        //     297: aload 12
        //     299: astore_1
        //     300: goto -201 -> 99
        //     303: astore 17
        //     305: goto -270 -> 35
        //
        // Exception table:
        //     from	to	target	type
        //     58	89	92	java/io/FileNotFoundException
        //     128	140	143	java/io/IOException
        //     153	168	171	java/io/IOException
        //     4	18	176	java/io/IOException
        //     36	47	176	java/io/IOException
        //     208	220	223	java/io/IOException
        //     4	18	228	finally
        //     36	47	228	finally
        //     99	124	228	finally
        //     178	204	228	finally
        //     234	246	249	java/io/IOException
        //     47	58	254	finally
        //     58	89	262	finally
        //     47	58	273	java/io/IOException
        //     58	89	281	java/io/IOException
        //     4	18	292	java/io/FileNotFoundException
        //     36	47	292	java/io/FileNotFoundException
        //     47	58	296	java/io/FileNotFoundException
        //     27	35	303	java/io/IOException
    }

    private void getProfileProxy()
    {
        BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.mA2dpProxy == null)
            localBluetoothAdapter.getProfileProxy(this.mContext, this.mProfileServiceListener, 2);
        if (this.mHeadsetProxy == null)
            localBluetoothAdapter.getProfileProxy(this.mContext, this.mProfileServiceListener, 1);
    }

    private void loadBondState()
    {
        if (this.mService.getBluetoothStateInternal() != 11);
        while (true)
        {
            return;
            String str1 = this.mService.getAdapterProperties().getProperty("Devices");
            if (str1 != null)
            {
                String[] arrayOfString = str1.split(",");
                if (arrayOfString != null)
                {
                    this.mState.clear();
                    Log.d("BluetoothBondState", "found " + arrayOfString.length + " bonded devices");
                    int i = arrayOfString.length;
                    for (int j = 0; j < i; j++)
                    {
                        String str2 = arrayOfString[j];
                        this.mState.put(this.mService.getAddressFromObjectPath(str2).toUpperCase(), Integer.valueOf(12));
                    }
                }
            }
        }
    }

    private void setProfilePriorities(String paramString, int paramInt)
    {
        BluetoothDevice localBluetoothDevice = this.mService.getRemoteDevice(paramString);
        this.mBluetoothInputProfileHandler.setInitialInputDevicePriority(localBluetoothDevice, paramInt);
        if (paramInt == 12)
        {
            if ((this.mA2dpProxy != null) && (this.mA2dpProxy.getPriority(localBluetoothDevice) == -1))
                this.mA2dpProxy.setPriority(localBluetoothDevice, 100);
            if ((this.mHeadsetProxy != null) && (this.mHeadsetProxy.getPriority(localBluetoothDevice) == -1))
                this.mHeadsetProxy.setPriority(localBluetoothDevice, 100);
        }
        while (true)
        {
            if ((this.mA2dpProxy == null) || (this.mHeadsetProxy == null))
                Log.e("BluetoothBondState", "Proxy is null:" + this.mA2dpProxy + ":" + this.mHeadsetProxy);
            return;
            if (paramInt == 10)
            {
                if (this.mA2dpProxy != null)
                    this.mA2dpProxy.setPriority(localBluetoothDevice, -1);
                if (this.mHeadsetProxy != null)
                    this.mHeadsetProxy.setPriority(localBluetoothDevice, -1);
            }
        }
    }

    // ERROR //
    private void updateAutoPairingData(String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 269	java/io/BufferedWriter
        //     5: dup
        //     6: new 271	java/io/FileWriter
        //     9: dup
        //     10: ldc 18
        //     12: iconst_1
        //     13: invokespecial 274	java/io/FileWriter:<init>	(Ljava/lang/String;Z)V
        //     16: invokespecial 277	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
        //     19: astore_3
        //     20: new 152	java/lang/StringBuilder
        //     23: dup
        //     24: invokespecial 153	java/lang/StringBuilder:<init>	()V
        //     27: astore 4
        //     29: aload_0
        //     30: getfield 279	android/server/BluetoothBondState:mAutoPairingDynamicAddressBlacklist	Ljava/util/ArrayList;
        //     33: invokevirtual 282	java/util/ArrayList:size	()I
        //     36: ifne +12 -> 48
        //     39: aload 4
        //     41: ldc_w 284
        //     44: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     47: pop
        //     48: aload 4
        //     50: aload_1
        //     51: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     54: pop
        //     55: aload 4
        //     57: ldc 203
        //     59: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     62: pop
        //     63: aload_3
        //     64: aload 4
        //     66: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     69: invokevirtual 286	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     72: aload_3
        //     73: ifnull +142 -> 215
        //     76: aload_3
        //     77: invokevirtual 287	java/io/BufferedWriter:close	()V
        //     80: return
        //     81: astore 15
        //     83: goto -3 -> 80
        //     86: astore 5
        //     88: ldc 21
        //     90: new 152	java/lang/StringBuilder
        //     93: dup
        //     94: invokespecial 153	java/lang/StringBuilder:<init>	()V
        //     97: ldc_w 289
        //     100: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     103: aload 5
        //     105: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     108: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     111: invokestatic 172	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     114: pop
        //     115: aload_2
        //     116: ifnull -36 -> 80
        //     119: aload_2
        //     120: invokevirtual 287	java/io/BufferedWriter:close	()V
        //     123: goto -43 -> 80
        //     126: astore 9
        //     128: goto -48 -> 80
        //     131: astore 10
        //     133: ldc 21
        //     135: new 152	java/lang/StringBuilder
        //     138: dup
        //     139: invokespecial 153	java/lang/StringBuilder:<init>	()V
        //     142: ldc_w 291
        //     145: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     148: aload 10
        //     150: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     153: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     156: invokestatic 172	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     159: pop
        //     160: aload_2
        //     161: ifnull -81 -> 80
        //     164: aload_2
        //     165: invokevirtual 287	java/io/BufferedWriter:close	()V
        //     168: goto -88 -> 80
        //     171: astore 12
        //     173: goto -93 -> 80
        //     176: astore 6
        //     178: aload_2
        //     179: ifnull +7 -> 186
        //     182: aload_2
        //     183: invokevirtual 287	java/io/BufferedWriter:close	()V
        //     186: aload 6
        //     188: athrow
        //     189: astore 7
        //     191: goto -5 -> 186
        //     194: astore 6
        //     196: aload_3
        //     197: astore_2
        //     198: goto -20 -> 178
        //     201: astore 10
        //     203: aload_3
        //     204: astore_2
        //     205: goto -72 -> 133
        //     208: astore 5
        //     210: aload_3
        //     211: astore_2
        //     212: goto -124 -> 88
        //     215: goto -135 -> 80
        //
        // Exception table:
        //     from	to	target	type
        //     76	80	81	java/io/IOException
        //     2	20	86	java/io/FileNotFoundException
        //     119	123	126	java/io/IOException
        //     2	20	131	java/io/IOException
        //     164	168	171	java/io/IOException
        //     2	20	176	finally
        //     88	115	176	finally
        //     133	160	176	finally
        //     182	186	189	java/io/IOException
        //     20	72	194	finally
        //     20	72	201	java/io/IOException
        //     20	72	208	java/io/FileNotFoundException
    }

    /** @deprecated */
    public void addAutoPairingFailure(String paramString)
    {
        try
        {
            if (this.mAutoPairingDynamicAddressBlacklist == null)
                this.mAutoPairingDynamicAddressBlacklist = new ArrayList();
            updateAutoPairingData(paramString);
            this.mAutoPairingDynamicAddressBlacklist.add(paramString);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void attempt(String paramString)
    {
        try
        {
            Integer localInteger = (Integer)this.mPinAttempt.get(paramString);
            if (localInteger == null);
            int i;
            for (int j = 1; ; j = i + 1)
            {
                Log.d("BluetoothBondState", "attemp newAttempt: " + j);
                this.mPinAttempt.put(paramString, new Integer(j));
                return;
                i = localInteger.intValue();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void clearPinAttempts(String paramString)
    {
        try
        {
            Log.d("BluetoothBondState", "clearPinAttempts: " + paramString);
            this.mPinAttempt.remove(paramString);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int getAttempt(String paramString)
    {
        try
        {
            Integer localInteger = (Integer)this.mPinAttempt.get(paramString);
            if (localInteger == null);
            int i;
            for (int j = 0; ; j = i)
            {
                return j;
                i = localInteger.intValue();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getBondState(String paramString)
    {
        try
        {
            Integer localInteger = (Integer)this.mState.get(paramString);
            if (localInteger == null);
            int i;
            for (int j = 10; ; j = i)
            {
                return j;
                i = localInteger.intValue();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public String getPendingOutgoingBonding()
    {
        try
        {
            String str = this.mPendingOutgoingBonding;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean hasAutoPairingFailed(String paramString)
    {
        try
        {
            ArrayList localArrayList = this.mAutoPairingDynamicAddressBlacklist;
            if (localArrayList == null);
            boolean bool1;
            for (boolean bool2 = false; ; bool2 = bool1)
            {
                return bool2;
                bool1 = this.mAutoPairingDynamicAddressBlacklist.contains(paramString);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void initBondState()
    {
        try
        {
            getProfileProxy();
            loadBondState();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean isAutoPairingAttemptsInProgress(String paramString)
    {
        try
        {
            int i = getAttempt(paramString);
            if (i != 0)
            {
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    public boolean isAutoPairingBlacklisted(String paramString)
    {
        boolean bool = true;
        if (this.mAutoPairingAddressBlacklist != null)
        {
            Iterator localIterator4 = this.mAutoPairingAddressBlacklist.iterator();
            do
                if (!localIterator4.hasNext())
                    break;
            while (!paramString.startsWith((String)localIterator4.next()));
        }
        while (true)
        {
            return bool;
            if (this.mAutoPairingDynamicAddressBlacklist != null)
            {
                Iterator localIterator3 = this.mAutoPairingDynamicAddressBlacklist.iterator();
                while (true)
                    if (localIterator3.hasNext())
                        if (paramString.equals((String)localIterator3.next()))
                            break;
            }
            String str = this.mService.getRemoteName(paramString);
            if (str != null)
            {
                if (this.mAutoPairingExactNameBlacklist != null)
                {
                    Iterator localIterator2 = this.mAutoPairingExactNameBlacklist.iterator();
                    while (true)
                        if (localIterator2.hasNext())
                            if (str.equals((String)localIterator2.next()))
                                break;
                }
                if (this.mAutoPairingPartialNameBlacklist != null)
                {
                    Iterator localIterator1 = this.mAutoPairingPartialNameBlacklist.iterator();
                    while (true)
                        if (localIterator1.hasNext())
                            if (str.startsWith((String)localIterator1.next()))
                                break;
                }
            }
            bool = false;
        }
    }

    public boolean isFixedPinZerosAutoPairKeyboard(String paramString)
    {
        if (this.mAutoPairingFixedPinZerosKeyboardList != null)
        {
            Iterator localIterator = this.mAutoPairingFixedPinZerosKeyboardList.iterator();
            do
                if (!localIterator.hasNext())
                    break;
            while (!paramString.startsWith((String)localIterator.next()));
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    /** @deprecated */
    String[] listInState(int paramInt)
    {
        ArrayList localArrayList;
        try
        {
            localArrayList = new ArrayList(this.mState.size());
            Iterator localIterator = this.mState.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                if (((Integer)localEntry.getValue()).intValue() == paramInt)
                    localArrayList.add(localEntry.getKey());
            }
        }
        finally
        {
        }
        String[] arrayOfString = (String[])localArrayList.toArray(new String[localArrayList.size()]);
        return arrayOfString;
    }

    /** @deprecated */
    // ERROR //
    public void readAutoPairingData()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 339	android/server/BluetoothBondState:mAutoPairingAddressBlacklist	Ljava/util/ArrayList;
        //     6: astore_2
        //     7: aload_2
        //     8: ifnull +6 -> 14
        //     11: aload_0
        //     12: monitorexit
        //     13: return
        //     14: aload_0
        //     15: invokespecial 395	android/server/BluetoothBondState:copyAutoPairingData	()V
        //     18: aconst_null
        //     19: astore_3
        //     20: new 138	java/io/FileInputStream
        //     23: dup
        //     24: ldc 18
        //     26: invokespecial 139	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     29: astore 4
        //     31: new 397	java/io/BufferedReader
        //     34: dup
        //     35: new 399	java/io/InputStreamReader
        //     38: dup
        //     39: new 401	java/io/DataInputStream
        //     42: dup
        //     43: aload 4
        //     45: invokespecial 404	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     48: invokespecial 405	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
        //     51: invokespecial 408	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
        //     54: astore 5
        //     56: aload 5
        //     58: invokevirtual 411	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     61: astore 14
        //     63: aload 14
        //     65: ifnull +351 -> 416
        //     68: aload 14
        //     70: invokevirtual 414	java/lang/String:trim	()Ljava/lang/String;
        //     73: astore 16
        //     75: aload 16
        //     77: invokevirtual 417	java/lang/String:length	()I
        //     80: ifeq -24 -> 56
        //     83: aload 16
        //     85: ldc_w 419
        //     88: invokevirtual 355	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     91: ifne -35 -> 56
        //     94: aload 16
        //     96: ldc_w 421
        //     99: invokevirtual 209	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     102: astore 17
        //     104: aload 17
        //     106: ifnull -50 -> 56
        //     109: aload 17
        //     111: arraylength
        //     112: iconst_2
        //     113: if_icmpne -57 -> 56
        //     116: aload 17
        //     118: iconst_1
        //     119: aaload
        //     120: ldc 203
        //     122: invokevirtual 209	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     125: astore 18
        //     127: aload 17
        //     129: iconst_0
        //     130: aaload
        //     131: ldc_w 423
        //     134: invokevirtual 426	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     137: ifeq +70 -> 207
        //     140: aload_0
        //     141: new 62	java/util/ArrayList
        //     144: dup
        //     145: aload 18
        //     147: invokestatic 432	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
        //     150: invokespecial 435	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     153: putfield 339	android/server/BluetoothBondState:mAutoPairingAddressBlacklist	Ljava/util/ArrayList;
        //     156: goto -100 -> 56
        //     159: astore 11
        //     161: aload 4
        //     163: astore_3
        //     164: ldc 21
        //     166: new 152	java/lang/StringBuilder
        //     169: dup
        //     170: invokespecial 153	java/lang/StringBuilder:<init>	()V
        //     173: ldc_w 437
        //     176: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     179: aload 11
        //     181: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     184: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     187: invokestatic 172	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     190: pop
        //     191: aload_3
        //     192: ifnull -181 -> 11
        //     195: aload_3
        //     196: invokevirtual 175	java/io/FileInputStream:close	()V
        //     199: goto -188 -> 11
        //     202: astore 13
        //     204: goto -193 -> 11
        //     207: aload 17
        //     209: iconst_0
        //     210: aaload
        //     211: ldc_w 439
        //     214: invokevirtual 426	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     217: ifeq +70 -> 287
        //     220: aload_0
        //     221: new 62	java/util/ArrayList
        //     224: dup
        //     225: aload 18
        //     227: invokestatic 432	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
        //     230: invokespecial 435	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     233: putfield 363	android/server/BluetoothBondState:mAutoPairingExactNameBlacklist	Ljava/util/ArrayList;
        //     236: goto -180 -> 56
        //     239: astore 8
        //     241: aload 4
        //     243: astore_3
        //     244: ldc 21
        //     246: new 152	java/lang/StringBuilder
        //     249: dup
        //     250: invokespecial 153	java/lang/StringBuilder:<init>	()V
        //     253: ldc_w 441
        //     256: invokevirtual 159	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     259: aload 8
        //     261: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     264: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     267: invokestatic 172	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     270: pop
        //     271: aload_3
        //     272: ifnull -261 -> 11
        //     275: aload_3
        //     276: invokevirtual 175	java/io/FileInputStream:close	()V
        //     279: goto -268 -> 11
        //     282: astore 10
        //     284: goto -273 -> 11
        //     287: aload 17
        //     289: iconst_0
        //     290: aaload
        //     291: ldc_w 443
        //     294: invokevirtual 426	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     297: ifeq +43 -> 340
        //     300: aload_0
        //     301: new 62	java/util/ArrayList
        //     304: dup
        //     305: aload 18
        //     307: invokestatic 432	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
        //     310: invokespecial 435	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     313: putfield 365	android/server/BluetoothBondState:mAutoPairingPartialNameBlacklist	Ljava/util/ArrayList;
        //     316: goto -260 -> 56
        //     319: astore 6
        //     321: aload 4
        //     323: astore_3
        //     324: aload_3
        //     325: ifnull +7 -> 332
        //     328: aload_3
        //     329: invokevirtual 175	java/io/FileInputStream:close	()V
        //     332: aload 6
        //     334: athrow
        //     335: astore_1
        //     336: aload_0
        //     337: monitorexit
        //     338: aload_1
        //     339: athrow
        //     340: aload 17
        //     342: iconst_0
        //     343: aaload
        //     344: ldc_w 445
        //     347: invokevirtual 426	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     350: ifeq +22 -> 372
        //     353: aload_0
        //     354: new 62	java/util/ArrayList
        //     357: dup
        //     358: aload 18
        //     360: invokestatic 432	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
        //     363: invokespecial 435	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     366: putfield 368	android/server/BluetoothBondState:mAutoPairingFixedPinZerosKeyboardList	Ljava/util/ArrayList;
        //     369: goto -313 -> 56
        //     372: aload 17
        //     374: iconst_0
        //     375: aaload
        //     376: ldc_w 447
        //     379: invokevirtual 426	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     382: ifeq +22 -> 404
        //     385: aload_0
        //     386: new 62	java/util/ArrayList
        //     389: dup
        //     390: aload 18
        //     392: invokestatic 432	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
        //     395: invokespecial 435	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     398: putfield 279	android/server/BluetoothBondState:mAutoPairingDynamicAddressBlacklist	Ljava/util/ArrayList;
        //     401: goto -345 -> 56
        //     404: ldc 21
        //     406: ldc_w 449
        //     409: invokestatic 172	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     412: pop
        //     413: goto -357 -> 56
        //     416: aload 4
        //     418: ifnull +36 -> 454
        //     421: aload 4
        //     423: invokevirtual 175	java/io/FileInputStream:close	()V
        //     426: goto -415 -> 11
        //     429: astore 15
        //     431: goto -420 -> 11
        //     434: astore 7
        //     436: goto -104 -> 332
        //     439: astore 6
        //     441: goto -117 -> 324
        //     444: astore 8
        //     446: goto -202 -> 244
        //     449: astore 11
        //     451: goto -287 -> 164
        //     454: goto -443 -> 11
        //
        // Exception table:
        //     from	to	target	type
        //     31	156	159	java/io/FileNotFoundException
        //     207	236	159	java/io/FileNotFoundException
        //     287	316	159	java/io/FileNotFoundException
        //     340	413	159	java/io/FileNotFoundException
        //     195	199	202	java/io/IOException
        //     31	156	239	java/io/IOException
        //     207	236	239	java/io/IOException
        //     287	316	239	java/io/IOException
        //     340	413	239	java/io/IOException
        //     275	279	282	java/io/IOException
        //     31	156	319	finally
        //     207	236	319	finally
        //     287	316	319	finally
        //     340	413	319	finally
        //     2	7	335	finally
        //     14	18	335	finally
        //     195	199	335	finally
        //     275	279	335	finally
        //     328	332	335	finally
        //     332	335	335	finally
        //     421	426	335	finally
        //     421	426	429	java/io/IOException
        //     328	332	434	java/io/IOException
        //     20	31	439	finally
        //     164	191	439	finally
        //     244	271	439	finally
        //     20	31	444	java/io/IOException
        //     20	31	449	java/io/FileNotFoundException
    }

    /** @deprecated */
    public void setBondState(String paramString, int paramInt)
    {
        try
        {
            setBondState(paramString, paramInt, 0);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setBondState(String paramString, int paramInt1, int paramInt2)
    {
        while (true)
        {
            try
            {
                Log.d("BluetoothBondState", "setBondState address " + paramInt1 + "reason: " + paramInt2);
                int i = getBondState(paramString);
                if (i == paramInt1)
                    return;
                if ((i == 11) && (paramString.equals(this.mPendingOutgoingBonding)))
                    this.mPendingOutgoingBonding = null;
                if (paramInt1 == 12)
                {
                    boolean bool = false;
                    if (this.mPairingRequestRcvd.contains(paramString))
                        bool = true;
                    this.mService.addProfileState(paramString, bool);
                    this.mPairingRequestRcvd.remove(paramString);
                    setProfilePriorities(paramString, paramInt1);
                    Log.d("BluetoothBondState", paramString + " bond state " + i + " -> " + paramInt1 + " (" + paramInt2 + ")");
                    Intent localIntent = new Intent("android.bluetooth.device.action.BOND_STATE_CHANGED");
                    localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mService.getRemoteDevice(paramString));
                    localIntent.putExtra("android.bluetooth.device.extra.BOND_STATE", paramInt1);
                    localIntent.putExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", i);
                    if (paramInt1 != 10)
                        break label341;
                    if (paramInt2 <= 0)
                    {
                        Log.w("BluetoothBondState", "setBondState() called to unbond device, but reason code is invalid. Overriding reason code with BOND_RESULT_REMOVED");
                        paramInt2 = 9;
                    }
                    localIntent.putExtra("android.bluetooth.device.extra.REASON", paramInt2);
                    this.mState.remove(paramString);
                    this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
                    continue;
                }
            }
            finally
            {
            }
            if (paramInt1 == 11)
            {
                if ((this.mA2dpProxy == null) || (this.mHeadsetProxy == null))
                    getProfileProxy();
            }
            else if (paramInt1 == 10)
            {
                this.mPairingRequestRcvd.remove(paramString);
                continue;
                label341: this.mState.put(paramString, Integer.valueOf(paramInt1));
            }
        }
    }

    /** @deprecated */
    void setPendingOutgoingBonding(String paramString)
    {
        try
        {
            this.mPendingOutgoingBonding = paramString;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothBondState
 * JD-Core Version:        0.6.2
 */