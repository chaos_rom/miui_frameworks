package android.server;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothTetheringDataTracker;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.net.ConnectivityManager;
import android.net.InterfaceConfiguration;
import android.net.LinkAddress;
import android.net.NetworkUtils;
import android.os.INetworkManagementService;
import android.os.INetworkManagementService.Stub;
import android.os.ServiceManager;
import android.util.Log;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class BluetoothPanProfileHandler
{
    private static final String BLUETOOTH_IFACE_ADDR_START = "192.168.44.1";
    private static final int BLUETOOTH_MAX_PAN_CONNECTIONS = 5;
    private static final int BLUETOOTH_PREFIX_LENGTH = 24;
    private static final boolean DBG = true;
    static final String NAP_BRIDGE = "pan1";
    static final String NAP_ROLE = "nap";
    private static final String TAG = "BluetoothPanProfileHandler";
    public static BluetoothPanProfileHandler sInstance;
    private ArrayList<String> mBluetoothIfaceAddresses;
    private BluetoothService mBluetoothService;
    private Context mContext;
    private int mMaxPanDevices;
    private final HashMap<BluetoothDevice, BluetoothPanDevice> mPanDevices;
    private boolean mTetheringOn;
    private BroadcastReceiver mTetheringReceiver = null;

    private BluetoothPanProfileHandler(Context paramContext, BluetoothService paramBluetoothService)
    {
        this.mContext = paramContext;
        this.mPanDevices = new HashMap();
        this.mBluetoothService = paramBluetoothService;
        this.mTetheringOn = false;
        this.mBluetoothIfaceAddresses = new ArrayList();
        try
        {
            this.mMaxPanDevices = paramContext.getResources().getInteger(17694730);
            return;
        }
        catch (Resources.NotFoundException localNotFoundException)
        {
            while (true)
                this.mMaxPanDevices = 5;
        }
    }

    private String createNewTetheringAddressLocked()
    {
        String str;
        if (getConnectedPanDevices().size() == this.mMaxPanDevices)
        {
            debugLog("Max PAN device connections reached");
            str = null;
        }
        while (true)
        {
            return str;
            String[] arrayOfString;
            Integer localInteger;
            for (str = "192.168.44.1"; this.mBluetoothIfaceAddresses.contains(str); str = str.replace(arrayOfString[2], localInteger.toString()))
            {
                arrayOfString = str.split("\\.");
                localInteger = Integer.valueOf(1 + Integer.parseInt(arrayOfString[2]));
            }
            this.mBluetoothIfaceAddresses.add(str);
        }
    }

    private static void debugLog(String paramString)
    {
        Log.d("BluetoothPanProfileHandler", paramString);
    }

    private boolean disconnectPanServerDevices()
    {
        int i = 1;
        debugLog("disconnect all PAN devices");
        Iterator localIterator = this.mPanDevices.keySet().iterator();
        while (localIterator.hasNext())
        {
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
            BluetoothPanDevice localBluetoothPanDevice = (BluetoothPanDevice)this.mPanDevices.get(localBluetoothDevice);
            int j = localBluetoothPanDevice.mState;
            if ((j == 2) && (localBluetoothPanDevice.mLocalRole == i))
            {
                String str = this.mBluetoothService.getObjectPathFromAddress(localBluetoothDevice.getAddress());
                handlePanDeviceStateChange(localBluetoothDevice, localBluetoothPanDevice.mIface, 3, localBluetoothPanDevice.mLocalRole);
                if (!this.mBluetoothService.disconnectPanServerDeviceNative(str, localBluetoothDevice.getAddress(), localBluetoothPanDevice.mIface))
                {
                    errorLog("could not disconnect Pan Server Device " + localBluetoothDevice.getAddress());
                    handlePanDeviceStateChange(localBluetoothDevice, localBluetoothPanDevice.mIface, j, localBluetoothPanDevice.mLocalRole);
                    i = 0;
                }
            }
        }
        return i;
    }

    private String enableTethering(String paramString)
    {
        debugLog("updateTetherState:" + paramString);
        INetworkManagementService localINetworkManagementService = INetworkManagementService.Stub.asInterface(ServiceManager.getService("network_management"));
        ConnectivityManager localConnectivityManager = (ConnectivityManager)this.mContext.getSystemService("connectivity");
        localConnectivityManager.getTetherableBluetoothRegexs();
        new String[0];
        try
        {
            String[] arrayOfString = localINetworkManagementService.listInterfaces();
            int i = 0;
            int j = arrayOfString.length;
            k = 0;
            if (k < j)
            {
                if (arrayOfString[k].equals(paramString))
                    i = 1;
            }
            else
            {
                if (i != 0)
                    break label147;
                str = null;
                return str;
            }
        }
        catch (Exception localException1)
        {
            while (true)
            {
                int k;
                Log.e("BluetoothPanProfileHandler", "Error listing Interfaces :" + localException1);
                String str = null;
                continue;
                k++;
                continue;
                label147: str = createNewTetheringAddressLocked();
                if (str == null)
                    str = null;
                else
                    try
                    {
                        InterfaceConfiguration localInterfaceConfiguration = localINetworkManagementService.getInterfaceConfig(paramString);
                        if (localInterfaceConfiguration != null)
                        {
                            LinkAddress localLinkAddress1 = localInterfaceConfiguration.getLinkAddress();
                            InetAddress localInetAddress;
                            if (localLinkAddress1 != null)
                            {
                                localInetAddress = localLinkAddress1.getAddress();
                                if ((localInetAddress != null) && (!localInetAddress.equals(NetworkUtils.numericToInetAddress("0.0.0.0"))) && (!localInetAddress.equals(NetworkUtils.numericToInetAddress("::0"))));
                            }
                            else
                            {
                                localInetAddress = NetworkUtils.numericToInetAddress(str);
                            }
                            localInterfaceConfiguration.setInterfaceUp();
                            localInterfaceConfiguration.clearFlag("running");
                            LinkAddress localLinkAddress2 = new LinkAddress(localInetAddress, 24);
                            localInterfaceConfiguration.setLinkAddress(localLinkAddress2);
                            localINetworkManagementService.setInterfaceConfig(paramString, localInterfaceConfiguration);
                            if (localConnectivityManager.tether(paramString) != 0)
                                Log.e("BluetoothPanProfileHandler", "Error tethering " + paramString);
                        }
                    }
                    catch (Exception localException2)
                    {
                        Log.e("BluetoothPanProfileHandler", "Error configuring interface " + paramString + ", :" + localException2);
                        str = null;
                    }
            }
        }
    }

    private static void errorLog(String paramString)
    {
        Log.e("BluetoothPanProfileHandler", paramString);
    }

    static BluetoothPanProfileHandler getInstance(Context paramContext, BluetoothService paramBluetoothService)
    {
        if (sInstance == null)
            sInstance = new BluetoothPanProfileHandler(paramContext, paramBluetoothService);
        return sInstance;
    }

    boolean allowIncomingTethering()
    {
        if ((isTetheringOn()) && (getConnectedPanDevices().size() < this.mMaxPanDevices));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean connectPanDevice(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
        Log.d("BluetoothPanProfileHandler", "connect PAN(" + str + ")");
        if (getPanDeviceConnectionState(paramBluetoothDevice) != 0)
            errorLog(paramBluetoothDevice + " already connected to PAN");
        int i = 0;
        Iterator localIterator = this.mPanDevices.keySet().iterator();
        while (localIterator.hasNext())
            if (getPanDeviceConnectionState((BluetoothDevice)localIterator.next()) == 2)
                i++;
        if (i > 8)
            debugLog(paramBluetoothDevice + " could not connect to PAN because 8 other devices are" + "already connected");
        while (true)
        {
            return bool;
            handlePanDeviceStateChange(paramBluetoothDevice, null, 1, 2);
            if (this.mBluetoothService.connectPanDeviceNative(str, "nap"))
            {
                debugLog("connecting to PAN");
                bool = true;
            }
            else
            {
                handlePanDeviceStateChange(paramBluetoothDevice, null, 0, 2);
                errorLog("could not connect to PAN");
            }
        }
    }

    boolean disconnectPanDevice(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        String str = this.mBluetoothService.getObjectPathFromAddress(paramBluetoothDevice.getAddress());
        debugLog("disconnect PAN(" + str + ")");
        int i = getPanDeviceConnectionState(paramBluetoothDevice);
        if (i != 2)
            debugLog(paramBluetoothDevice + " already disconnected from PAN");
        while (true)
        {
            return bool;
            BluetoothPanDevice localBluetoothPanDevice = (BluetoothPanDevice)this.mPanDevices.get(paramBluetoothDevice);
            if (localBluetoothPanDevice == null)
            {
                errorLog("No record for this Pan device:" + paramBluetoothDevice);
            }
            else
            {
                handlePanDeviceStateChange(paramBluetoothDevice, localBluetoothPanDevice.mIface, 3, localBluetoothPanDevice.mLocalRole);
                if (localBluetoothPanDevice.mLocalRole == 1)
                {
                    if (!this.mBluetoothService.disconnectPanServerDeviceNative(str, paramBluetoothDevice.getAddress(), localBluetoothPanDevice.mIface))
                        handlePanDeviceStateChange(paramBluetoothDevice, localBluetoothPanDevice.mIface, i, localBluetoothPanDevice.mLocalRole);
                }
                else if (!this.mBluetoothService.disconnectPanDeviceNative(str))
                    handlePanDeviceStateChange(paramBluetoothDevice, localBluetoothPanDevice.mIface, i, localBluetoothPanDevice.mLocalRole);
                else
                    bool = true;
            }
        }
    }

    List<BluetoothDevice> getConnectedPanDevices()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mPanDevices.keySet().iterator();
        while (localIterator.hasNext())
        {
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
            if (getPanDeviceConnectionState(localBluetoothDevice) == 2)
                localArrayList.add(localBluetoothDevice);
        }
        return localArrayList;
    }

    int getPanDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        BluetoothPanDevice localBluetoothPanDevice = (BluetoothPanDevice)this.mPanDevices.get(paramBluetoothDevice);
        if (localBluetoothPanDevice == null);
        for (int i = 0; ; i = localBluetoothPanDevice.mState)
            return i;
    }

    List<BluetoothDevice> getPanDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mPanDevices.keySet().iterator();
        label88: 
        while (localIterator.hasNext())
        {
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
            int i = getPanDeviceConnectionState(localBluetoothDevice);
            int j = paramArrayOfInt.length;
            for (int k = 0; ; k++)
            {
                if (k >= j)
                    break label88;
                if (paramArrayOfInt[k] == i)
                {
                    localArrayList.add(localBluetoothDevice);
                    break;
                }
            }
        }
        return localArrayList;
    }

    void handlePanDeviceStateChange(BluetoothDevice paramBluetoothDevice, String paramString, int paramInt1, int paramInt2)
    {
        String str = null;
        BluetoothPanDevice localBluetoothPanDevice1 = (BluetoothPanDevice)this.mPanDevices.get(paramBluetoothDevice);
        int i;
        if (localBluetoothPanDevice1 == null)
            i = 0;
        while (i == paramInt1)
        {
            return;
            i = localBluetoothPanDevice1.mState;
            str = localBluetoothPanDevice1.mIfaceAddr;
        }
        if (paramInt2 == 1)
            if (paramInt1 == 2)
            {
                str = enableTethering(paramString);
                if (str == null)
                    Log.e("BluetoothPanProfileHandler", "Error seting up tether interface");
                label80: if (localBluetoothPanDevice1 != null)
                    break label308;
                BluetoothPanDevice localBluetoothPanDevice2 = new BluetoothPanDevice(paramInt1, str, paramString, paramInt2);
                this.mPanDevices.put(paramBluetoothDevice, localBluetoothPanDevice2);
            }
        while (true)
        {
            Intent localIntent = new Intent("android.bluetooth.pan.profile.action.CONNECTION_STATE_CHANGED");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", paramBluetoothDevice);
            localIntent.putExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", i);
            localIntent.putExtra("android.bluetooth.profile.extra.STATE", paramInt1);
            localIntent.putExtra("android.bluetooth.pan.extra.LOCAL_ROLE", paramInt2);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
            debugLog("Pan Device state : device: " + paramBluetoothDevice + " State:" + i + "->" + paramInt1);
            this.mBluetoothService.sendConnectionStateChange(paramBluetoothDevice, 5, paramInt1, i);
            break;
            if ((paramInt1 != 0) || (str == null))
                break label80;
            this.mBluetoothIfaceAddresses.remove(str);
            str = null;
            break label80;
            if (paramInt1 == 2)
            {
                BluetoothTetheringDataTracker.getInstance().startReverseTether(paramString, paramBluetoothDevice);
                break label80;
            }
            if ((paramInt1 != 0) || ((i != 2) && (i != 3)))
                break label80;
            BluetoothTetheringDataTracker.getInstance().stopReverseTether(localBluetoothPanDevice1.mIface);
            break label80;
            label308: BluetoothPanDevice.access$302(localBluetoothPanDevice1, paramInt1);
            BluetoothPanDevice.access$602(localBluetoothPanDevice1, str);
            BluetoothPanDevice.access$402(localBluetoothPanDevice1, paramInt2);
            BluetoothPanDevice.access$502(localBluetoothPanDevice1, paramString);
        }
    }

    boolean isTetheringOn()
    {
        return this.mTetheringOn;
    }

    void setBluetoothTethering(boolean paramBoolean)
    {
        if (!paramBoolean)
            disconnectPanServerDevices();
        if ((this.mBluetoothService.getBluetoothState() != 12) && (paramBoolean))
        {
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
            this.mTetheringReceiver = new BroadcastReceiver()
            {
                public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
                {
                    if (paramAnonymousIntent.getIntExtra("android.bluetooth.adapter.extra.STATE", 10) == 12)
                    {
                        BluetoothPanProfileHandler.access$002(BluetoothPanProfileHandler.this, true);
                        BluetoothPanProfileHandler.this.mContext.unregisterReceiver(BluetoothPanProfileHandler.this.mTetheringReceiver);
                    }
                }
            };
            this.mContext.registerReceiver(this.mTetheringReceiver, localIntentFilter);
        }
        while (true)
        {
            return;
            this.mTetheringOn = paramBoolean;
        }
    }

    private class BluetoothPanDevice
    {
        private String mIface;
        private String mIfaceAddr;
        private int mLocalRole;
        private int mState;

        BluetoothPanDevice(int paramString1, String paramString2, String paramInt1, int arg5)
        {
            this.mState = paramString1;
            this.mIfaceAddr = paramString2;
            this.mIface = paramInt1;
            int i;
            this.mLocalRole = i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothPanProfileHandler
 * JD-Core Version:        0.6.2
 */