package android.server;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class BluetoothDeviceProperties
{
    private static final String TAG = "BluetoothDeviceProperties";
    private final HashMap<String, Map<String, String>> mPropertiesMap = new HashMap();
    private final BluetoothService mService;

    BluetoothDeviceProperties(BluetoothService paramBluetoothService)
    {
        this.mService = paramBluetoothService;
    }

    Map<String, String> addProperties(String paramString, String[] paramArrayOfString)
    {
        while (true)
        {
            Object localObject2;
            int i;
            String str2;
            synchronized (this.mPropertiesMap)
            {
                localObject2 = (Map)this.mPropertiesMap.get(paramString);
                if (localObject2 == null)
                {
                    localObject2 = new HashMap();
                    continue;
                    if (i < paramArrayOfString.length)
                    {
                        String str1 = paramArrayOfString[i];
                        str2 = null;
                        if (str1 == null)
                        {
                            Log.e("BluetoothDeviceProperties", "Error: Remote Device Property at index " + i + " is null");
                        }
                        else if ((str1.equals("UUIDs")) || (str1.equals("Nodes")))
                        {
                            StringBuilder localStringBuilder = new StringBuilder();
                            i++;
                            int j = Integer.valueOf(paramArrayOfString[i]).intValue();
                            int k = 0;
                            if (k < j)
                            {
                                i++;
                                localStringBuilder.append(paramArrayOfString[i]);
                                localStringBuilder.append(",");
                                k++;
                                continue;
                            }
                            if (j > 0)
                                str2 = localStringBuilder.toString();
                            ((Map)localObject2).put(str1, str2);
                        }
                    }
                }
            }
        }
    }

    // ERROR //
    String getProperty(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 23	android/server/BluetoothDeviceProperties:mPropertiesMap	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 23	android/server/BluetoothDeviceProperties:mPropertiesMap	Ljava/util/HashMap;
        //     11: aload_1
        //     12: invokevirtual 31	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     15: checkcast 33	java/util/Map
        //     18: astore 5
        //     20: aload 5
        //     22: ifnull +21 -> 43
        //     25: aload 5
        //     27: aload_2
        //     28: invokeinterface 93 2 0
        //     33: checkcast 61	java/lang/String
        //     36: astore 8
        //     38: aload_3
        //     39: monitorexit
        //     40: goto +79 -> 119
        //     43: aload_0
        //     44: aload_1
        //     45: invokevirtual 97	android/server/BluetoothDeviceProperties:updateCache	(Ljava/lang/String;)Ljava/util/Map;
        //     48: astore 6
        //     50: aload 6
        //     52: ifnull +28 -> 80
        //     55: aload 6
        //     57: aload_2
        //     58: invokeinterface 93 2 0
        //     63: checkcast 61	java/lang/String
        //     66: astore 8
        //     68: aload_3
        //     69: monitorexit
        //     70: goto +49 -> 119
        //     73: astore 4
        //     75: aload_3
        //     76: monitorexit
        //     77: aload 4
        //     79: athrow
        //     80: aload_3
        //     81: monitorexit
        //     82: ldc 8
        //     84: new 35	java/lang/StringBuilder
        //     87: dup
        //     88: invokespecial 36	java/lang/StringBuilder:<init>	()V
        //     91: ldc 99
        //     93: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     96: aload_2
        //     97: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     100: ldc 101
        //     102: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     105: aload_1
        //     106: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     109: invokevirtual 51	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     112: invokestatic 57	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     115: pop
        //     116: aconst_null
        //     117: astore 8
        //     119: aload 8
        //     121: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	77	73	finally
        //     80	82	73	finally
    }

    boolean isEmpty()
    {
        synchronized (this.mPropertiesMap)
        {
            boolean bool = this.mPropertiesMap.isEmpty();
            return bool;
        }
    }

    boolean isInCache(String paramString)
    {
        while (true)
        {
            synchronized (this.mPropertiesMap)
            {
                if (this.mPropertiesMap.get(paramString) != null)
                {
                    bool = true;
                    return bool;
                }
            }
            boolean bool = false;
        }
    }

    Set<String> keySet()
    {
        synchronized (this.mPropertiesMap)
        {
            Set localSet = this.mPropertiesMap.keySet();
            return localSet;
        }
    }

    void setProperty(String paramString1, String paramString2, String paramString3)
    {
        synchronized (this.mPropertiesMap)
        {
            Map localMap = (Map)this.mPropertiesMap.get(paramString1);
            if (localMap != null)
            {
                localMap.put(paramString2, paramString3);
                this.mPropertiesMap.put(paramString1, localMap);
                return;
            }
            Log.e("BluetoothDeviceProperties", "setRemoteDeviceProperty for a device not in cache:" + paramString1);
        }
    }

    Map<String, String> updateCache(String paramString)
    {
        String[] arrayOfString = this.mService.getRemoteDeviceProperties(paramString);
        if (arrayOfString != null);
        for (Map localMap = addProperties(paramString, arrayOfString); ; localMap = null)
            return localMap;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothDeviceProperties
 * JD-Core Version:        0.6.2
 */