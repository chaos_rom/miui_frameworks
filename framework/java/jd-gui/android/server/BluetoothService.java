package android.server;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothDeviceProfileState;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothHealthAppConfiguration;
import android.bluetooth.BluetoothInputDevice;
import android.bluetooth.BluetoothPan;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.bluetooth.BluetoothProfileState;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.IBluetooth.Stub;
import android.bluetooth.IBluetoothCallback;
import android.bluetooth.IBluetoothHealthCallback;
import android.bluetooth.IBluetoothStateChangeCallback;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings.System;
import android.util.Log;
import android.util.Pair;
import com.android.internal.app.IBatteryStats;
import com.android.internal.app.IBatteryStats.Stub;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class BluetoothService extends IBluetooth.Stub
{
    private static final String BLUETOOTH_ADMIN_PERM = "android.permission.BLUETOOTH_ADMIN";
    static final String BLUETOOTH_PERM = "android.permission.BLUETOOTH";
    private static final boolean DBG = true;
    private static final String DOCK_ADDRESS_PATH = "/sys/class/switch/dock/bt_addr";
    private static final String DOCK_PIN_PATH = "/sys/class/switch/dock/bt_pin";
    private static final String INCOMING_CONNECTION_FILE = "/data/misc/bluetooth/incoming_connection.conf";
    private static final long INIT_AUTO_PAIRING_FAILURE_ATTEMPT_DELAY = 3000L;
    private static final long MAX_AUTO_PAIRING_FAILURE_ATTEMPT_DELAY = 12000L;
    private static final int MESSAGE_AUTO_PAIRING_FAILURE_ATTEMPT_DELAY = 2;
    private static final int MESSAGE_REMOVE_SERVICE_RECORD = 3;
    private static final int MESSAGE_UUID_INTENT = 1;
    private static final int RFCOMM_RECORD_REAPER = 10;
    private static final ParcelUuid[] RFCOMM_UUIDS;
    private static final String SHARED_PREFERENCES_NAME = "bluetooth_service_settings";
    private static final String SHARED_PREFERENCE_DOCK_ADDRESS = "dock_bluetooth_address";
    private static final int STATE_CHANGE_REAPER = 11;
    private static final String TAG = "BluetoothService";
    private static final int UUID_INTENT_DELAY = 6000;
    private static String mDockAddress;
    private final BluetoothProfileState mA2dpProfileState;
    private BluetoothA2dpService mA2dpService;
    private BluetoothAdapter mAdapter;
    private int mAdapterConnectionState = 0;
    private final BluetoothAdapterProperties mAdapterProperties;
    private int[] mAdapterSdpHandles;
    private ParcelUuid[] mAdapterUuids;
    private boolean mAllowConnect = true;
    private final IBatteryStats mBatteryStats;
    private BluetoothHealthProfileHandler mBluetoothHealthProfileHandler;
    private BluetoothInputProfileHandler mBluetoothInputProfileHandler;
    private BluetoothPanProfileHandler mBluetoothPanProfileHandler;
    private BluetoothProfile.ServiceListener mBluetoothProfileServiceListener = new BluetoothProfile.ServiceListener()
    {
        public void onServiceConnected(int paramAnonymousInt, BluetoothProfile paramAnonymousBluetoothProfile)
        {
            if (paramAnonymousInt == 1)
                BluetoothService.access$802(BluetoothService.this, (BluetoothHeadset)paramAnonymousBluetoothProfile);
            while (true)
            {
                return;
                if (paramAnonymousInt == 4)
                    BluetoothService.access$902(BluetoothService.this, (BluetoothInputDevice)paramAnonymousBluetoothProfile);
                else if (paramAnonymousInt == 5)
                    BluetoothService.access$1002(BluetoothService.this, (BluetoothPan)paramAnonymousBluetoothProfile);
            }
        }

        public void onServiceDisconnected(int paramAnonymousInt)
        {
            if (paramAnonymousInt == 1)
                BluetoothService.access$802(BluetoothService.this, null);
            while (true)
            {
                return;
                if (paramAnonymousInt == 4)
                    BluetoothService.access$902(BluetoothService.this, null);
                else if (paramAnonymousInt == 5)
                    BluetoothService.access$1002(BluetoothService.this, null);
            }
        }
    };
    private BluetoothAdapterStateMachine mBluetoothState;
    private final BluetoothBondState mBondState;
    private final Context mContext;
    private final HashMap<String, Pair<byte[], byte[]>> mDeviceOobData;
    private final HashMap<String, BluetoothDeviceProfileState> mDeviceProfileState;
    private final BluetoothDeviceProperties mDeviceProperties;
    private final HashMap<String, Map<ParcelUuid, Integer>> mDeviceServiceChannelCache;
    private String mDockPin;
    private BluetoothEventLoop mEventLoop;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                String str2 = (String)paramAnonymousMessage.obj;
                if (str2 != null)
                {
                    BluetoothService.this.sendUuidIntent(str2);
                    BluetoothService.this.makeServiceChannelCallbacks(str2);
                    continue;
                    String str1 = (String)paramAnonymousMessage.obj;
                    if (str1 != null)
                    {
                        int i = BluetoothService.this.mBondState.getAttempt(str1);
                        if ((i > 0) && (i <= 2))
                        {
                            BluetoothService.this.mBondState.attempt(str1);
                            BluetoothService.this.createBond(str1);
                        }
                        else if (i > 0)
                        {
                            BluetoothService.this.mBondState.clearPinAttempts(str1);
                            continue;
                            Pair localPair = (Pair)paramAnonymousMessage.obj;
                            BluetoothService.this.checkAndRemoveRecord(((Integer)localPair.first).intValue(), ((Integer)localPair.second).intValue());
                        }
                    }
                }
            }
        }
    };
    private BluetoothHeadset mHeadsetProxy;
    private final BluetoothProfileState mHfpProfileState;
    private HashMap<String, Pair<Integer, String>> mIncomingConnections;
    private BluetoothInputDevice mInputDevice;
    private boolean mIsAirplaneSensitive;
    private boolean mIsAirplaneToggleable;
    private int mNativeData;
    private BluetoothPan mPan;
    private HashMap<Integer, Pair<Integer, Integer>> mProfileConnectionState;
    private int mProfilesConnected = 0;
    private int mProfilesConnecting = 0;
    private int mProfilesDisconnecting = 0;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (paramAnonymousIntent == null);
            while (true)
            {
                return;
                String str = paramAnonymousIntent.getAction();
                if (str.equals("android.intent.action.AIRPLANE_MODE"))
                {
                    paramAnonymousContext.getContentResolver();
                    if (BluetoothService.this.isAirplaneModeOn())
                        BluetoothService.this.mBluetoothState.sendMessage(55);
                    else
                        BluetoothService.this.mBluetoothState.sendMessage(56);
                }
                else if ("android.intent.action.DOCK_EVENT".equals(str))
                {
                    int i = paramAnonymousIntent.getIntExtra("android.intent.extra.DOCK_STATE", 0);
                    Log.v("BluetoothService", "Received ACTION_DOCK_EVENT with State:" + i);
                    if (i == 0)
                    {
                        BluetoothService.access$502(null);
                        BluetoothService.access$602(BluetoothService.this, null);
                    }
                    else
                    {
                        Context localContext = BluetoothService.this.mContext;
                        SharedPreferences.Editor localEditor = localContext.getSharedPreferences("bluetooth_service_settings", 0).edit();
                        localEditor.putBoolean("dock_bluetooth_address" + BluetoothService.mDockAddress, true);
                        localEditor.apply();
                    }
                }
            }
        }
    };
    private final HashMap<Integer, ServiceRecordClient> mServiceRecordToPid;
    private Map<Integer, IBluetoothStateChangeCallback> mStateChangeTracker = Collections.synchronizedMap(new HashMap());
    private final HashMap<RemoteService, IBluetoothCallback> mUuidCallbackTracker;
    private final ArrayList<String> mUuidIntentTracker;

    static
    {
        ParcelUuid[] arrayOfParcelUuid = new ParcelUuid[3];
        arrayOfParcelUuid[0] = BluetoothUuid.Handsfree;
        arrayOfParcelUuid[1] = BluetoothUuid.HSP;
        arrayOfParcelUuid[2] = BluetoothUuid.ObexObjectPush;
        RFCOMM_UUIDS = arrayOfParcelUuid;
        classInitNative();
    }

    public BluetoothService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mBatteryStats = IBatteryStats.Stub.asInterface(ServiceManager.getService("batteryinfo"));
        initializeNativeDataNative();
        if (isEnabledNative() == 1)
        {
            Log.w("BluetoothService", "Bluetooth daemons already running - runtime restart? ");
            disableNative();
        }
        this.mBondState = new BluetoothBondState(paramContext, this);
        this.mAdapterProperties = new BluetoothAdapterProperties(paramContext, this);
        this.mDeviceProperties = new BluetoothDeviceProperties(this);
        this.mDeviceServiceChannelCache = new HashMap();
        this.mDeviceOobData = new HashMap();
        this.mUuidIntentTracker = new ArrayList();
        this.mUuidCallbackTracker = new HashMap();
        this.mServiceRecordToPid = new HashMap();
        this.mDeviceProfileState = new HashMap();
        this.mA2dpProfileState = new BluetoothProfileState(this.mContext, 1);
        this.mHfpProfileState = new BluetoothProfileState(this.mContext, 0);
        this.mHfpProfileState.start();
        this.mA2dpProfileState.start();
        IntentFilter localIntentFilter = new IntentFilter();
        registerForAirplaneMode(localIntentFilter);
        localIntentFilter.addAction("android.intent.action.DOCK_EVENT");
        this.mContext.registerReceiver(this.mReceiver, localIntentFilter);
        this.mBluetoothInputProfileHandler = BluetoothInputProfileHandler.getInstance(this.mContext, this);
        this.mBluetoothPanProfileHandler = BluetoothPanProfileHandler.getInstance(this.mContext, this);
        this.mBluetoothHealthProfileHandler = BluetoothHealthProfileHandler.getInstance(this.mContext, this);
        this.mIncomingConnections = new HashMap();
        this.mProfileConnectionState = new HashMap();
    }

    /** @deprecated */
    private void addReservedSdpRecords(ArrayList<ParcelUuid> paramArrayList)
    {
        try
        {
            int[] arrayOfInt = new int[paramArrayList.size()];
            for (int i = 0; i < paramArrayList.size(); i++)
                arrayOfInt[i] = BluetoothUuid.getServiceIdentifierFromParcelUuid((ParcelUuid)paramArrayList.get(i));
            this.mAdapterSdpHandles = addReservedServiceRecordsNative(arrayOfInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private native int[] addReservedServiceRecordsNative(int[] paramArrayOfInt);

    private native int addRfcommServiceRecordNative(String paramString, long paramLong1, long paramLong2, short paramShort);

    private void autoConnect()
    {
        try
        {
            if (!this.mAllowConnect)
            {
                Log.d("BluetoothService", "Not auto-connecting devices because of temporary BT on state.");
            }
            else
            {
                String[] arrayOfString = getKnownDevices();
                if (arrayOfString != null)
                {
                    int i = arrayOfString.length;
                    for (int j = 0; j < i; j++)
                    {
                        String str = getAddressFromObjectPath(arrayOfString[j]);
                        BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(str);
                        if (localBluetoothDeviceProfileState != null)
                        {
                            Message localMessage = new Message();
                            localMessage.what = 101;
                            localBluetoothDeviceProfileState.sendMessage(localMessage);
                        }
                    }
                }
            }
        }
        finally
        {
        }
    }

    static int bluezStringToScanMode(boolean paramBoolean1, boolean paramBoolean2)
    {
        int i;
        if ((paramBoolean1) && (paramBoolean2))
            i = 23;
        while (true)
        {
            return i;
            if ((paramBoolean1) && (!paramBoolean2))
                i = 21;
            else
                i = 20;
        }
    }

    private native boolean cancelDeviceCreationNative(String paramString);

    private native boolean cancelPairingUserInputNative(String paramString, int paramInt);

    /** @deprecated */
    private void checkAndRemoveRecord(int paramInt1, int paramInt2)
    {
        try
        {
            ServiceRecordClient localServiceRecordClient = (ServiceRecordClient)this.mServiceRecordToPid.get(Integer.valueOf(paramInt1));
            if ((localServiceRecordClient != null) && (paramInt2 == localServiceRecordClient.pid))
            {
                Log.d("BluetoothService", "Removing service record " + Integer.toHexString(paramInt1) + " for pid " + paramInt2);
                if (localServiceRecordClient.death != null)
                    localServiceRecordClient.binder.unlinkToDeath(localServiceRecordClient.death, 0);
                this.mServiceRecordToPid.remove(Integer.valueOf(paramInt1));
                removeServiceRecordNative(paramInt1);
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private static native void classInitNative();

    private native void cleanupNativeDataNative();

    private ParcelUuid[] convertStringToParcelUuid(String paramString)
    {
        String[] arrayOfString = paramString.split(",");
        ParcelUuid[] arrayOfParcelUuid = new ParcelUuid[arrayOfString.length];
        for (int i = 0; i < arrayOfString.length; i++)
            arrayOfParcelUuid[i] = ParcelUuid.fromString(arrayOfString[i]);
        return arrayOfParcelUuid;
    }

    private int convertToAdapterState(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            Log.e("BluetoothService", "Error in convertToAdapterState");
            i = -1;
        case 0:
        case 3:
        case 2:
        case 1:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 3;
            continue;
            i = 2;
            continue;
            i = 1;
        }
    }

    private native boolean createDeviceNative(String paramString);

    private void createIncomingConnectionStateFile()
    {
        File localFile = new File("/data/misc/bluetooth/incoming_connection.conf");
        if (!localFile.exists());
        try
        {
            localFile.createNewFile();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.e("BluetoothService", "IOException: cannot create file");
        }
    }

    private native boolean createPairedDeviceNative(String paramString, int paramInt);

    private native boolean createPairedDeviceOutOfBandNative(String paramString, int paramInt);

    private void dumpAclConnectedDevices(PrintWriter paramPrintWriter)
    {
        String[] arrayOfString = getKnownDevices();
        paramPrintWriter.println("\n--ACL connected devices--");
        if (arrayOfString != null)
        {
            int i = arrayOfString.length;
            for (int j = 0; j < i; j++)
                paramPrintWriter.println(getAddressFromObjectPath(arrayOfString[j]));
        }
    }

    private void dumpApplicationServiceRecords(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("\n--Application Service Records--");
        Iterator localIterator = this.mServiceRecordToPid.keySet().iterator();
        while (localIterator.hasNext())
        {
            Integer localInteger1 = (Integer)localIterator.next();
            Integer localInteger2 = Integer.valueOf(((ServiceRecordClient)this.mServiceRecordToPid.get(localInteger1)).pid);
            paramPrintWriter.println("\tpid " + localInteger2 + " handle " + Integer.toHexString(localInteger1.intValue()));
        }
    }

    private void dumpHeadsetConnectionState(PrintWriter paramPrintWriter, BluetoothDevice paramBluetoothDevice)
    {
        switch (this.mHeadsetProxy.getConnectionState(paramBluetoothDevice))
        {
        default:
        case 1:
        case 2:
        case 3:
        case 12:
        }
        while (true)
        {
            return;
            paramPrintWriter.println("getConnectionState() = STATE_CONNECTING");
            continue;
            paramPrintWriter.println("getConnectionState() = STATE_CONNECTED");
            continue;
            paramPrintWriter.println("getConnectionState() = STATE_DISCONNECTING");
            continue;
            paramPrintWriter.println("getConnectionState() = STATE_AUDIO_CONNECTED");
        }
    }

    private void dumpHeadsetService(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("\n--Headset Service--");
        if (this.mHeadsetProxy != null)
        {
            List localList1 = this.mHeadsetProxy.getConnectedDevices();
            if (localList1.size() == 0)
                paramPrintWriter.println("No headsets connected");
            while (true)
            {
                localList1.clear();
                BluetoothHeadset localBluetoothHeadset = this.mHeadsetProxy;
                int[] arrayOfInt = new int[2];
                arrayOfInt[0] = 2;
                arrayOfInt[1] = 0;
                List localList2 = localBluetoothHeadset.getDevicesMatchingConnectionStates(arrayOfInt);
                paramPrintWriter.println("--Connected and Disconnected Headsets");
                Iterator localIterator = localList2.iterator();
                while (localIterator.hasNext())
                {
                    BluetoothDevice localBluetoothDevice2 = (BluetoothDevice)localIterator.next();
                    paramPrintWriter.println(localBluetoothDevice2);
                    if (this.mHeadsetProxy.isAudioConnected(localBluetoothDevice2))
                        paramPrintWriter.println("SCO audio connected to device:" + localBluetoothDevice2);
                }
                BluetoothDevice localBluetoothDevice1 = (BluetoothDevice)localList1.get(0);
                paramPrintWriter.println("\ngetConnectedDevices[0] = " + localBluetoothDevice1);
                dumpHeadsetConnectionState(paramPrintWriter, localBluetoothDevice1);
                paramPrintWriter.println("getBatteryUsageHint() = " + this.mHeadsetProxy.getBatteryUsageHint(localBluetoothDevice1));
            }
        }
    }

    private void dumpInputDeviceProfile(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("\n--Bluetooth Service- Input Device Profile");
        if (this.mInputDevice != null)
        {
            List localList1 = this.mInputDevice.getConnectedDevices();
            if (localList1.size() == 0)
                paramPrintWriter.println("No input devices connected");
            while (true)
            {
                localList1.clear();
                BluetoothInputDevice localBluetoothInputDevice = this.mInputDevice;
                int[] arrayOfInt = new int[2];
                arrayOfInt[0] = 2;
                arrayOfInt[1] = 0;
                List localList2 = localBluetoothInputDevice.getDevicesMatchingConnectionStates(arrayOfInt);
                paramPrintWriter.println("--Connected and Disconnected input devices");
                Iterator localIterator = localList2.iterator();
                while (localIterator.hasNext())
                    paramPrintWriter.println((BluetoothDevice)localIterator.next());
                paramPrintWriter.println("Number of connected devices:" + localList1.size());
                BluetoothDevice localBluetoothDevice = (BluetoothDevice)localList1.get(0);
                paramPrintWriter.println("getConnectedDevices[0] = " + localBluetoothDevice);
                paramPrintWriter.println("Priority of Connected device = " + this.mInputDevice.getPriority(localBluetoothDevice));
                switch (this.mInputDevice.getConnectionState(localBluetoothDevice))
                {
                default:
                    break;
                case 1:
                    paramPrintWriter.println("getConnectionState() = STATE_CONNECTING");
                    break;
                case 2:
                    paramPrintWriter.println("getConnectionState() = STATE_CONNECTED");
                    break;
                case 3:
                    paramPrintWriter.println("getConnectionState() = STATE_DISCONNECTING");
                }
            }
        }
    }

    private void dumpKnownDevices(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("\n--Known devices--");
        Iterator localIterator1 = this.mDeviceProperties.keySet().iterator();
        if (localIterator1.hasNext())
        {
            String str = (String)localIterator1.next();
            int i = this.mBondState.getBondState(str);
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = str;
            arrayOfObject[1] = toBondStateString(i);
            arrayOfObject[2] = Integer.valueOf(this.mBondState.getAttempt(str));
            arrayOfObject[3] = getRemoteName(str);
            paramPrintWriter.printf("%s %10s (%d) %s\n", arrayOfObject);
            Map localMap = (Map)this.mDeviceServiceChannelCache.get(str);
            if (localMap == null)
                paramPrintWriter.println("\tuuids = null");
            while (true)
            {
                Iterator localIterator3 = this.mUuidCallbackTracker.keySet().iterator();
                while (localIterator3.hasNext())
                {
                    RemoteService localRemoteService = (RemoteService)localIterator3.next();
                    if (localRemoteService.address.equals(str))
                        paramPrintWriter.println("\tPENDING CALLBACK: " + localRemoteService.uuid);
                }
                break;
                Iterator localIterator2 = localMap.keySet().iterator();
                while (localIterator2.hasNext())
                {
                    ParcelUuid localParcelUuid = (ParcelUuid)localIterator2.next();
                    Integer localInteger = (Integer)localMap.get(localParcelUuid);
                    if (localInteger == null)
                        paramPrintWriter.println("\t" + localParcelUuid);
                    else
                        paramPrintWriter.println("\t" + localParcelUuid + " RFCOMM channel = " + localInteger);
                }
            }
        }
    }

    private void dumpPanProfile(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("\n--Bluetooth Service- Pan Profile");
        if (this.mPan != null)
        {
            List localList1 = this.mPan.getConnectedDevices();
            if (localList1.size() == 0)
                paramPrintWriter.println("No Pan devices connected");
            while (true)
            {
                localList1.clear();
                BluetoothPan localBluetoothPan = this.mPan;
                int[] arrayOfInt = new int[2];
                arrayOfInt[0] = 2;
                arrayOfInt[1] = 0;
                List localList2 = localBluetoothPan.getDevicesMatchingConnectionStates(arrayOfInt);
                paramPrintWriter.println("--Connected and Disconnected Pan devices");
                Iterator localIterator = localList2.iterator();
                while (localIterator.hasNext())
                    paramPrintWriter.println((BluetoothDevice)localIterator.next());
                paramPrintWriter.println("Number of connected devices:" + localList1.size());
                BluetoothDevice localBluetoothDevice = (BluetoothDevice)localList1.get(0);
                paramPrintWriter.println("getConnectedDevices[0] = " + localBluetoothDevice);
                switch (this.mPan.getConnectionState(localBluetoothDevice))
                {
                default:
                    break;
                case 1:
                    paramPrintWriter.println("getConnectionState() = STATE_CONNECTING");
                    break;
                case 2:
                    paramPrintWriter.println("getConnectionState() = STATE_CONNECTED");
                    break;
                case 3:
                    paramPrintWriter.println("getConnectionState() = STATE_DISCONNECTING");
                }
            }
        }
    }

    private void dumpProfileState(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("\n--Profile State dump--");
        paramPrintWriter.println("\n Headset profile state:" + this.mAdapter.getProfileConnectionState(1));
        paramPrintWriter.println("\n A2dp profile state:" + this.mAdapter.getProfileConnectionState(2));
        paramPrintWriter.println("\n HID profile state:" + this.mAdapter.getProfileConnectionState(4));
        paramPrintWriter.println("\n PAN profile state:" + this.mAdapter.getProfileConnectionState(5));
    }

    private native Object[] getDevicePropertiesNative(String paramString);

    private int getDeviceServiceChannelForUuid(String paramString, ParcelUuid paramParcelUuid)
    {
        return getDeviceServiceChannelNative(getObjectPathFromAddress(paramString), paramParcelUuid.toString(), 4);
    }

    private native int getDeviceServiceChannelNative(String paramString1, String paramString2, int paramInt);

    private void getProfileProxy()
    {
        this.mAdapter.getProfileProxy(this.mContext, this.mBluetoothProfileServiceListener, 1);
    }

    private void initProfileState()
    {
        String[] arrayOfString1 = null;
        String str = getProperty("Devices", false);
        if (str != null)
            arrayOfString1 = str.split(",");
        if (arrayOfString1 == null);
        while (true)
        {
            return;
            String[] arrayOfString2 = arrayOfString1;
            int i = arrayOfString2.length;
            for (int j = 0; j < i; j++)
                addProfileState(getAddressFromObjectPath(arrayOfString2[j]), false);
        }
    }

    private native void initializeNativeDataNative();

    private boolean isBondingFeasible(String paramString)
    {
        boolean bool = false;
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
        if (!isEnabledInternal());
        while (true)
        {
            return bool;
            if (BluetoothAdapter.checkBluetoothAddress(paramString))
            {
                String str = paramString.toUpperCase();
                if (this.mBondState.getPendingOutgoingBonding() != null)
                    Log.d("BluetoothService", "Ignoring createBond(): another device is bonding");
                else if ((!this.mBondState.isAutoPairingAttemptsInProgress(str)) && (this.mBondState.getBondState(str) != 10))
                    Log.d("BluetoothService", "Ignoring createBond(): this device is already bonding or bonded");
                else if ((str.equals(mDockAddress)) && (!writeDockPin()))
                    Log.e("BluetoothService", "Error while writing Pin for the dock");
                else
                    bool = true;
            }
        }
    }

    private boolean isEnabledInternal()
    {
        if (getBluetoothStateInternal() == 12);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private native int isEnabledNative();

    private static void log(String paramString)
    {
        Log.d("BluetoothService", paramString);
    }

    private void pairingAttempt(String paramString, int paramInt)
    {
        int i = this.mBondState.getAttempt(paramString);
        if (3000L * i > 12000L)
        {
            this.mBondState.clearPinAttempts(paramString);
            setBondState(paramString, 10, paramInt);
        }
        while (true)
        {
            return;
            Message localMessage = this.mHandler.obtainMessage(2);
            localMessage.obj = paramString;
            if (!this.mHandler.sendMessageDelayed(localMessage, 3000L * i))
            {
                this.mBondState.clearPinAttempts(paramString);
                setBondState(paramString, 10, paramInt);
            }
        }
    }

    private native byte[] readAdapterOutOfBandDataNative();

    /** @deprecated */
    // ERROR //
    public static String readDockBluetoothAddress()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_0
        //     2: ldc 2
        //     4: monitorenter
        //     5: getstatic 345	android/server/BluetoothService:mDockAddress	Ljava/lang/String;
        //     8: ifnull +12 -> 20
        //     11: getstatic 345	android/server/BluetoothService:mDockAddress	Ljava/lang/String;
        //     14: astore_0
        //     15: ldc 2
        //     17: monitorexit
        //     18: aload_0
        //     19: areturn
        //     20: aconst_null
        //     21: astore_2
        //     22: new 798	java/io/BufferedInputStream
        //     25: dup
        //     26: new 800	java/io/FileInputStream
        //     29: dup
        //     30: ldc 32
        //     32: invokespecial 801	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     35: invokespecial 804	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
        //     38: astore_3
        //     39: bipush 17
        //     41: newarray byte
        //     43: astore 11
        //     45: aload_3
        //     46: aload 11
        //     48: invokevirtual 808	java/io/BufferedInputStream:read	([B)I
        //     51: pop
        //     52: new 474	java/lang/String
        //     55: dup
        //     56: aload 11
        //     58: invokespecial 811	java/lang/String:<init>	([B)V
        //     61: invokevirtual 749	java/lang/String:toUpperCase	()Ljava/lang/String;
        //     64: astore 13
        //     66: aload 13
        //     68: invokestatic 746	android/bluetooth/BluetoothAdapter:checkBluetoothAddress	(Ljava/lang/String;)Z
        //     71: ifeq +28 -> 99
        //     74: aload 13
        //     76: putstatic 345	android/server/BluetoothService:mDockAddress	Ljava/lang/String;
        //     79: getstatic 345	android/server/BluetoothService:mDockAddress	Ljava/lang/String;
        //     82: astore_0
        //     83: aload_3
        //     84: ifnull -69 -> 15
        //     87: aload_3
        //     88: invokevirtual 814	java/io/BufferedInputStream:close	()V
        //     91: goto -76 -> 15
        //     94: astore 16
        //     96: goto -81 -> 15
        //     99: ldc 66
        //     101: new 427	java/lang/StringBuilder
        //     104: dup
        //     105: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     108: ldc_w 816
        //     111: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     114: aload 13
        //     116: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     119: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     122: invokestatic 489	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     125: pop
        //     126: aload_3
        //     127: ifnull +7 -> 134
        //     130: aload_3
        //     131: invokevirtual 814	java/io/BufferedInputStream:close	()V
        //     134: aconst_null
        //     135: putstatic 345	android/server/BluetoothService:mDockAddress	Ljava/lang/String;
        //     138: goto -123 -> 15
        //     141: astore_1
        //     142: ldc 2
        //     144: monitorexit
        //     145: aload_1
        //     146: athrow
        //     147: astore 18
        //     149: ldc 66
        //     151: ldc_w 818
        //     154: invokestatic 489	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     157: pop
        //     158: aload_2
        //     159: ifnull -25 -> 134
        //     162: aload_2
        //     163: invokevirtual 814	java/io/BufferedInputStream:close	()V
        //     166: goto -32 -> 134
        //     169: astore 8
        //     171: goto -37 -> 134
        //     174: astore 17
        //     176: ldc 66
        //     178: ldc_w 820
        //     181: invokestatic 489	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     184: pop
        //     185: aload_2
        //     186: ifnull -52 -> 134
        //     189: aload_2
        //     190: invokevirtual 814	java/io/BufferedInputStream:close	()V
        //     193: goto -59 -> 134
        //     196: astore 5
        //     198: aload_2
        //     199: ifnull +7 -> 206
        //     202: aload_2
        //     203: invokevirtual 814	java/io/BufferedInputStream:close	()V
        //     206: aload 5
        //     208: athrow
        //     209: astore 6
        //     211: goto -5 -> 206
        //     214: astore 15
        //     216: goto -82 -> 134
        //     219: astore 5
        //     221: aload_3
        //     222: astore_2
        //     223: goto -25 -> 198
        //     226: astore 9
        //     228: aload_3
        //     229: astore_2
        //     230: goto -54 -> 176
        //     233: astore 4
        //     235: aload_3
        //     236: astore_2
        //     237: goto -88 -> 149
        //
        // Exception table:
        //     from	to	target	type
        //     87	91	94	java/io/IOException
        //     5	15	141	finally
        //     87	91	141	finally
        //     130	134	141	finally
        //     134	138	141	finally
        //     162	166	141	finally
        //     189	193	141	finally
        //     202	206	141	finally
        //     206	209	141	finally
        //     22	39	147	java/io/FileNotFoundException
        //     162	166	169	java/io/IOException
        //     189	193	169	java/io/IOException
        //     22	39	174	java/io/IOException
        //     22	39	196	finally
        //     149	158	196	finally
        //     176	185	196	finally
        //     202	206	209	java/io/IOException
        //     130	134	214	java/io/IOException
        //     39	83	219	finally
        //     99	126	219	finally
        //     39	83	226	java/io/IOException
        //     99	126	226	java/io/IOException
        //     39	83	233	java/io/FileNotFoundException
        //     99	126	233	java/io/FileNotFoundException
    }

    // ERROR //
    private void readIncomingConnectionState()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 321	android/server/BluetoothService:mIncomingConnections	Ljava/util/HashMap;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aconst_null
        //     8: astore_2
        //     9: new 800	java/io/FileInputStream
        //     12: dup
        //     13: ldc 38
        //     15: invokespecial 801	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     18: astore_3
        //     19: new 823	java/io/BufferedReader
        //     22: dup
        //     23: new 825	java/io/InputStreamReader
        //     26: dup
        //     27: new 827	java/io/DataInputStream
        //     30: dup
        //     31: aload_3
        //     32: invokespecial 828	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     35: invokespecial 829	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
        //     38: invokespecial 832	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
        //     41: astore 4
        //     43: aload 4
        //     45: invokevirtual 835	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     48: astore 11
        //     50: aload 11
        //     52: ifnull +122 -> 174
        //     55: aload 11
        //     57: invokevirtual 838	java/lang/String:trim	()Ljava/lang/String;
        //     60: astore 13
        //     62: aload 13
        //     64: invokevirtual 841	java/lang/String:length	()I
        //     67: ifeq -24 -> 43
        //     70: aload 13
        //     72: ldc_w 472
        //     75: invokevirtual 478	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     78: astore 14
        //     80: aload 14
        //     82: ifnull -39 -> 43
        //     85: aload 14
        //     87: arraylength
        //     88: iconst_3
        //     89: if_icmpne -46 -> 43
        //     92: new 843	android/util/Pair
        //     95: dup
        //     96: aload 14
        //     98: iconst_1
        //     99: aaload
        //     100: invokestatic 846	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     103: invokestatic 422	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     106: aload 14
        //     108: iconst_2
        //     109: aaload
        //     110: invokespecial 849	android/util/Pair:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
        //     113: astore 15
        //     115: aload_0
        //     116: getfield 321	android/server/BluetoothService:mIncomingConnections	Ljava/util/HashMap;
        //     119: aload 14
        //     121: iconst_0
        //     122: aaload
        //     123: aload 15
        //     125: invokevirtual 853	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     128: pop
        //     129: goto -86 -> 43
        //     132: astore 10
        //     134: aload_3
        //     135: astore_2
        //     136: new 427	java/lang/StringBuilder
        //     139: dup
        //     140: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     143: ldc_w 855
        //     146: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     149: aload 10
        //     151: invokevirtual 856	java/io/FileNotFoundException:toString	()Ljava/lang/String;
        //     154: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     157: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     160: invokestatic 858	android/server/BluetoothService:log	(Ljava/lang/String;)V
        //     163: aload_2
        //     164: ifnull +7 -> 171
        //     167: aload_2
        //     168: invokevirtual 859	java/io/FileInputStream:close	()V
        //     171: aload_1
        //     172: monitorexit
        //     173: return
        //     174: aload_3
        //     175: ifnull +7 -> 182
        //     178: aload_3
        //     179: invokevirtual 859	java/io/FileInputStream:close	()V
        //     182: goto -11 -> 171
        //     185: astore 5
        //     187: new 427	java/lang/StringBuilder
        //     190: dup
        //     191: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     194: ldc_w 861
        //     197: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     200: aload 5
        //     202: invokevirtual 862	java/io/IOException:toString	()Ljava/lang/String;
        //     205: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     208: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     211: invokestatic 858	android/server/BluetoothService:log	(Ljava/lang/String;)V
        //     214: aload_2
        //     215: ifnull -44 -> 171
        //     218: aload_2
        //     219: invokevirtual 859	java/io/FileInputStream:close	()V
        //     222: goto -51 -> 171
        //     225: astore 9
        //     227: goto -56 -> 171
        //     230: astore 6
        //     232: aload_2
        //     233: ifnull +7 -> 240
        //     236: aload_2
        //     237: invokevirtual 859	java/io/FileInputStream:close	()V
        //     240: aload 6
        //     242: athrow
        //     243: aload_1
        //     244: monitorexit
        //     245: aload 7
        //     247: athrow
        //     248: astore 8
        //     250: goto -10 -> 240
        //     253: astore 12
        //     255: goto -73 -> 182
        //     258: astore 7
        //     260: goto -17 -> 243
        //     263: astore 6
        //     265: aload_3
        //     266: astore_2
        //     267: goto -35 -> 232
        //     270: astore 5
        //     272: aload_3
        //     273: astore_2
        //     274: goto -87 -> 187
        //     277: astore 10
        //     279: goto -143 -> 136
        //     282: astore 7
        //     284: goto -41 -> 243
        //
        // Exception table:
        //     from	to	target	type
        //     19	129	132	java/io/FileNotFoundException
        //     9	19	185	java/io/IOException
        //     167	171	225	java/io/IOException
        //     218	222	225	java/io/IOException
        //     9	19	230	finally
        //     136	163	230	finally
        //     187	214	230	finally
        //     236	240	248	java/io/IOException
        //     178	182	253	java/io/IOException
        //     178	182	258	finally
        //     19	129	263	finally
        //     19	129	270	java/io/IOException
        //     9	19	277	java/io/FileNotFoundException
        //     167	171	282	finally
        //     171	173	282	finally
        //     218	222	282	finally
        //     236	240	282	finally
        //     240	245	282	finally
    }

    private void registerForAirplaneMode(IntentFilter paramIntentFilter)
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        String str1 = Settings.System.getString(localContentResolver, "airplane_mode_radios");
        String str2 = Settings.System.getString(localContentResolver, "airplane_mode_toggleable_radios");
        boolean bool1;
        if (str1 == null)
        {
            bool1 = true;
            this.mIsAirplaneSensitive = bool1;
            if (str2 != null)
                break label79;
        }
        label79: for (boolean bool2 = false; ; bool2 = str2.contains("bluetooth"))
        {
            this.mIsAirplaneToggleable = bool2;
            if (this.mIsAirplaneSensitive)
                paramIntentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            return;
            bool1 = str1.contains("bluetooth");
            break;
        }
    }

    private native boolean removeDeviceNative(String paramString);

    private native boolean removeReservedServiceRecordsNative(int[] paramArrayOfInt);

    private native boolean removeServiceRecordNative(int paramInt);

    static String scanModeToBluezString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        case 22:
        default:
            str = null;
        case 20:
        case 21:
        case 23:
        }
        while (true)
        {
            return str;
            str = "off";
            continue;
            str = "connectable";
            continue;
            str = "discoverable";
        }
    }

    private native boolean setAdapterPropertyBooleanNative(String paramString, int paramInt);

    private native boolean setAdapterPropertyIntegerNative(String paramString, int paramInt);

    private native boolean setAdapterPropertyStringNative(String paramString1, String paramString2);

    private native boolean setDevicePropertyBooleanNative(String paramString1, String paramString2, int paramInt);

    private native boolean setDevicePropertyStringNative(String paramString1, String paramString2, String paramString3);

    private native boolean setLinkTimeoutNative(String paramString, int paramInt);

    private native boolean setPairingConfirmationNative(String paramString, boolean paramBoolean, int paramInt);

    private native boolean setPasskeyNative(String paramString, int paramInt1, int paramInt2);

    private native boolean setPinNative(String paramString1, String paramString2, int paramInt);

    private boolean setPropertyBoolean(String paramString, boolean paramBoolean)
    {
        int i = 0;
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        if (!isEnabledInternal());
        while (true)
        {
            return i;
            if (paramBoolean)
                i = 1;
            boolean bool = setAdapterPropertyBooleanNative(paramString, i);
        }
    }

    private boolean setPropertyInteger(String paramString, int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        if (!isEnabledInternal());
        for (boolean bool = false; ; bool = setAdapterPropertyIntegerNative(paramString, paramInt))
            return bool;
    }

    private boolean setPropertyString(String paramString1, String paramString2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        if (!isEnabledInternal());
        for (boolean bool = false; ; bool = setAdapterPropertyStringNative(paramString1, paramString2))
            return bool;
    }

    private native boolean setRemoteOutOfBandDataNative(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt);

    private native boolean setupNativeDataNative();

    private native boolean startDiscoveryNative();

    private native boolean stopDiscoveryNative();

    private native boolean tearDownNativeDataNative();

    private static String toBondStateString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "??????";
        case 10:
        case 11:
        case 12:
        }
        while (true)
        {
            return str;
            str = "not bonded";
            continue;
            str = "bonding";
            continue;
            str = "bonded";
        }
    }

    // ERROR //
    private void truncateIncomingConnectionFile()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: new 940	java/io/RandomAccessFile
        //     5: dup
        //     6: ldc 38
        //     8: ldc_w 942
        //     11: invokespecial 944	java/io/RandomAccessFile:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     14: astore_2
        //     15: aload_2
        //     16: lconst_0
        //     17: invokevirtual 948	java/io/RandomAccessFile:setLength	(J)V
        //     20: aload_2
        //     21: ifnull +7 -> 28
        //     24: aload_2
        //     25: invokevirtual 949	java/io/RandomAccessFile:close	()V
        //     28: return
        //     29: astore_3
        //     30: new 427	java/lang/StringBuilder
        //     33: dup
        //     34: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     37: ldc_w 951
        //     40: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     43: aload_3
        //     44: invokevirtual 856	java/io/FileNotFoundException:toString	()Ljava/lang/String;
        //     47: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     53: invokestatic 858	android/server/BluetoothService:log	(Ljava/lang/String;)V
        //     56: aload_1
        //     57: ifnull -29 -> 28
        //     60: aload_1
        //     61: invokevirtual 949	java/io/RandomAccessFile:close	()V
        //     64: goto -36 -> 28
        //     67: astore 6
        //     69: goto -41 -> 28
        //     72: astore 7
        //     74: new 427	java/lang/StringBuilder
        //     77: dup
        //     78: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     81: ldc_w 953
        //     84: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     87: aload 7
        //     89: invokevirtual 862	java/io/IOException:toString	()Ljava/lang/String;
        //     92: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     95: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     98: invokestatic 858	android/server/BluetoothService:log	(Ljava/lang/String;)V
        //     101: aload_1
        //     102: ifnull -74 -> 28
        //     105: aload_1
        //     106: invokevirtual 949	java/io/RandomAccessFile:close	()V
        //     109: goto -81 -> 28
        //     112: astore 4
        //     114: aload_1
        //     115: ifnull +7 -> 122
        //     118: aload_1
        //     119: invokevirtual 949	java/io/RandomAccessFile:close	()V
        //     122: aload 4
        //     124: athrow
        //     125: astore 5
        //     127: goto -5 -> 122
        //     130: astore 8
        //     132: goto -104 -> 28
        //     135: astore 4
        //     137: aload_2
        //     138: astore_1
        //     139: goto -25 -> 114
        //     142: astore 7
        //     144: aload_2
        //     145: astore_1
        //     146: goto -72 -> 74
        //     149: astore_3
        //     150: aload_2
        //     151: astore_1
        //     152: goto -122 -> 30
        //
        // Exception table:
        //     from	to	target	type
        //     2	15	29	java/io/FileNotFoundException
        //     60	64	67	java/io/IOException
        //     105	109	67	java/io/IOException
        //     2	15	72	java/io/IOException
        //     2	15	112	finally
        //     30	56	112	finally
        //     74	101	112	finally
        //     118	122	125	java/io/IOException
        //     24	28	130	java/io/IOException
        //     15	20	135	finally
        //     15	20	142	java/io/IOException
        //     15	20	149	java/io/FileNotFoundException
    }

    private boolean updateCountersAndCheckForConnectionStateChange(int paramInt1, int paramInt2)
    {
        int i = 1;
        switch (paramInt2)
        {
        default:
            switch (paramInt1)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 0:
            }
            break;
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return i;
            this.mProfilesConnecting = (-1 + this.mProfilesConnecting);
            break;
            this.mProfilesConnected = (-1 + this.mProfilesConnected);
            break;
            this.mProfilesDisconnecting = (-1 + this.mProfilesDisconnecting);
            break;
            this.mProfilesConnecting = (1 + this.mProfilesConnecting);
            if ((this.mProfilesConnected != 0) || (this.mProfilesConnecting != i))
            {
                int j = 0;
                continue;
                this.mProfilesConnected = (1 + this.mProfilesConnected);
                if (this.mProfilesConnected != j)
                {
                    j = 0;
                    continue;
                    this.mProfilesDisconnecting = (1 + this.mProfilesDisconnecting);
                    if ((this.mProfilesConnected != 0) || (this.mProfilesDisconnecting != j))
                    {
                        j = 0;
                        continue;
                        if ((this.mProfilesConnected != 0) || (this.mProfilesConnecting != 0))
                            j = 0;
                    }
                }
            }
        }
    }

    private void updateProfileConnectionState(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 1;
        int j = paramInt2;
        int k = 1;
        Pair localPair = (Pair)this.mProfileConnectionState.get(Integer.valueOf(paramInt1));
        int m;
        if (localPair != null)
        {
            m = ((Integer)localPair.first).intValue();
            i = ((Integer)localPair.second).intValue();
            if (paramInt2 != m)
                break label100;
            i++;
        }
        while (true)
        {
            if (k != 0)
                this.mProfileConnectionState.put(Integer.valueOf(paramInt1), new Pair(Integer.valueOf(j), Integer.valueOf(i)));
            return;
            label100: if ((paramInt2 == 2) || ((paramInt2 == 1) && (m != 2)))
            {
                i = 1;
            }
            else if ((i == 1) && (paramInt3 == m))
            {
                k = 1;
            }
            else if ((i > 1) && (paramInt3 == m))
            {
                i--;
                if ((m == 2) || (m == 1))
                    j = m;
            }
            else
            {
                k = 0;
            }
        }
    }

    /** @deprecated */
    private void updateSdpRecords()
    {
        try
        {
            ArrayList localArrayList = new ArrayList();
            Resources localResources = this.mContext.getResources();
            if (localResources.getBoolean(17891370))
            {
                localArrayList.add(BluetoothUuid.HSP_AG);
                localArrayList.add(BluetoothUuid.ObexObjectPush);
            }
            if (localResources.getBoolean(17891368))
            {
                localArrayList.add(BluetoothUuid.Handsfree_AG);
                localArrayList.add(BluetoothUuid.PBAP_PSE);
            }
            addReservedSdpRecords(localArrayList);
            try
            {
                Thread.sleep(50L);
                label81: if (localResources.getBoolean(17891370))
                {
                    setBluetoothTetheringNative(true, "nap", "pan1");
                    localArrayList.add(BluetoothUuid.AudioSource);
                    localArrayList.add(BluetoothUuid.AvrcpTarget);
                    localArrayList.add(BluetoothUuid.NAP);
                }
                this.mAdapterUuids = new ParcelUuid[localArrayList.size()];
                for (int i = 0; i < localArrayList.size(); i++)
                    this.mAdapterUuids[i] = ((ParcelUuid)localArrayList.get(i));
            }
            catch (InterruptedException localInterruptedException)
            {
                break label81;
                return;
            }
        }
        finally
        {
        }
    }

    private boolean validateProfileConnectionState(int paramInt)
    {
        int i = 1;
        if ((paramInt == 0) || (paramInt == i) || (paramInt == 2) || (paramInt == 3));
        while (true)
        {
            return i;
            i = 0;
        }
    }

    /** @deprecated */
    // ERROR //
    private boolean writeDockPin()
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_1
        //     2: aload_0
        //     3: monitorenter
        //     4: aconst_null
        //     5: astore_2
        //     6: new 1020	java/io/BufferedWriter
        //     9: dup
        //     10: new 1022	java/io/FileWriter
        //     13: dup
        //     14: ldc 35
        //     16: invokespecial 1023	java/io/FileWriter:<init>	(Ljava/lang/String;)V
        //     19: invokespecial 1026	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
        //     22: astore_3
        //     23: ldc2_w 1027
        //     26: invokestatic 1034	java/lang/Math:random	()D
        //     29: dmul
        //     30: invokestatic 1038	java/lang/Math:floor	(D)D
        //     33: d2i
        //     34: istore 12
        //     36: iconst_1
        //     37: anewarray 639	java/lang/Object
        //     40: astore 13
        //     42: aload 13
        //     44: iconst_0
        //     45: iload 12
        //     47: invokestatic 422	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     50: aastore
        //     51: aload_0
        //     52: ldc_w 1040
        //     55: aload 13
        //     57: invokestatic 1044	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     60: putfield 351	android/server/BluetoothService:mDockPin	Ljava/lang/String;
        //     63: aload_3
        //     64: aload_0
        //     65: getfield 351	android/server/BluetoothService:mDockPin	Ljava/lang/String;
        //     68: invokevirtual 1047	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     71: aload_3
        //     72: ifnull +7 -> 79
        //     75: aload_3
        //     76: invokevirtual 1048	java/io/BufferedWriter:close	()V
        //     79: aload_0
        //     80: monitorexit
        //     81: iload_1
        //     82: ireturn
        //     83: astore 16
        //     85: ldc 66
        //     87: ldc_w 1050
        //     90: invokestatic 489	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     93: pop
        //     94: aload_2
        //     95: ifnull +7 -> 102
        //     98: aload_2
        //     99: invokevirtual 1048	java/io/BufferedWriter:close	()V
        //     102: aload_0
        //     103: aconst_null
        //     104: putfield 351	android/server/BluetoothService:mDockPin	Ljava/lang/String;
        //     107: iconst_0
        //     108: istore_1
        //     109: goto -30 -> 79
        //     112: astore 15
        //     114: ldc 66
        //     116: ldc_w 1052
        //     119: invokestatic 489	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     122: pop
        //     123: aload_2
        //     124: ifnull -22 -> 102
        //     127: aload_2
        //     128: invokevirtual 1048	java/io/BufferedWriter:close	()V
        //     131: goto -29 -> 102
        //     134: astore 9
        //     136: goto -34 -> 102
        //     139: astore 5
        //     141: aload_2
        //     142: ifnull +7 -> 149
        //     145: aload_2
        //     146: invokevirtual 1048	java/io/BufferedWriter:close	()V
        //     149: aload 5
        //     151: athrow
        //     152: astore 6
        //     154: aload_0
        //     155: monitorexit
        //     156: aload 6
        //     158: athrow
        //     159: astore 7
        //     161: goto -12 -> 149
        //     164: astore 14
        //     166: goto -87 -> 79
        //     169: astore 5
        //     171: aload_3
        //     172: astore_2
        //     173: goto -32 -> 141
        //     176: astore 10
        //     178: aload_3
        //     179: astore_2
        //     180: goto -66 -> 114
        //     183: astore 4
        //     185: aload_3
        //     186: astore_2
        //     187: goto -102 -> 85
        //     190: astore 6
        //     192: goto -38 -> 154
        //
        // Exception table:
        //     from	to	target	type
        //     6	23	83	java/io/FileNotFoundException
        //     6	23	112	java/io/IOException
        //     98	102	134	java/io/IOException
        //     127	131	134	java/io/IOException
        //     6	23	139	finally
        //     85	94	139	finally
        //     114	123	139	finally
        //     98	102	152	finally
        //     102	107	152	finally
        //     127	131	152	finally
        //     145	149	152	finally
        //     149	152	152	finally
        //     145	149	159	java/io/IOException
        //     75	79	164	java/io/IOException
        //     23	71	169	finally
        //     23	71	176	java/io/IOException
        //     23	71	183	java/io/FileNotFoundException
        //     75	79	190	finally
    }

    BluetoothDeviceProfileState addProfileState(String paramString, boolean paramBoolean)
    {
        BluetoothDeviceProfileState localBluetoothDeviceProfileState = new BluetoothDeviceProfileState(this.mContext, paramString, this, this.mA2dpService, paramBoolean);
        this.mDeviceProfileState.put(paramString, localBluetoothDeviceProfileState);
        localBluetoothDeviceProfileState.start();
        return localBluetoothDeviceProfileState;
    }

    /** @deprecated */
    public int addRfcommServiceRecord(String paramString, ParcelUuid paramParcelUuid, int paramInt, IBinder paramIBinder)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            boolean bool = isEnabledInternal();
            int i;
            if (!bool)
                i = -1;
            while (true)
            {
                return i;
                if ((paramString == null) || (paramParcelUuid == null) || (paramInt < 1) || (paramInt > 30))
                {
                    i = -1;
                }
                else if (BluetoothUuid.isUuidPresent(BluetoothUuid.RESERVED_UUIDS, paramParcelUuid))
                {
                    Log.w("BluetoothService", "Attempted to register a reserved UUID: " + paramParcelUuid);
                    i = -1;
                }
                else
                {
                    i = addRfcommServiceRecordNative(paramString, paramParcelUuid.getUuid().getMostSignificantBits(), paramParcelUuid.getUuid().getLeastSignificantBits(), (short)paramInt);
                    Log.d("BluetoothService", "new handle " + Integer.toHexString(i));
                    if (i == -1)
                    {
                        i = -1;
                    }
                    else
                    {
                        ServiceRecordClient localServiceRecordClient = new ServiceRecordClient(null);
                        localServiceRecordClient.pid = Binder.getCallingPid();
                        localServiceRecordClient.binder = paramIBinder;
                        localServiceRecordClient.death = new Reaper(i, localServiceRecordClient.pid, 10);
                        this.mServiceRecordToPid.put(new Integer(i), localServiceRecordClient);
                        try
                        {
                            paramIBinder.linkToDeath(localServiceRecordClient.death, 0);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            Log.e("BluetoothService", "", localRemoteException);
                            localServiceRecordClient.death = null;
                        }
                    }
                }
            }
        }
        finally
        {
        }
    }

    public boolean allowIncomingProfileConnect(BluetoothDevice paramBluetoothDevice, boolean paramBoolean)
    {
        boolean bool = false;
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
        String str = paramBluetoothDevice.getAddress();
        if (!BluetoothAdapter.checkBluetoothAddress(str));
        while (true)
        {
            return bool;
            Integer localInteger = getAuthorizationAgentRequestData(str);
            if (localInteger == null)
            {
                Log.w("BluetoothService", "allowIncomingProfileConnect(" + paramBluetoothDevice + ") called but no native data available");
            }
            else
            {
                log("allowIncomingProfileConnect: " + paramBluetoothDevice + " : " + paramBoolean + " : " + localInteger);
                bool = setAuthorizationNative(str, paramBoolean, localInteger.intValue());
            }
        }
    }

    boolean allowIncomingTethering()
    {
        synchronized (this.mBluetoothPanProfileHandler)
        {
            boolean bool = this.mBluetoothPanProfileHandler.allowIncomingTethering();
            return bool;
        }
    }

    /** @deprecated */
    boolean attemptAutoPair(String paramString)
    {
        try
        {
            if ((!this.mBondState.hasAutoPairingFailed(paramString)) && (!this.mBondState.isAutoPairingBlacklisted(paramString)))
            {
                this.mBondState.attempt(paramString);
                setPin(paramString, BluetoothDevice.convertPinToBytes("0000"));
                bool = true;
                return bool;
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean cancelBondProcess(String paramString)
    {
        boolean bool1 = false;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            boolean bool2 = isEnabledInternal();
            if (!bool2);
            while (true)
            {
                return bool1;
                if (BluetoothAdapter.checkBluetoothAddress(paramString))
                {
                    String str = paramString.toUpperCase();
                    if (this.mBondState.getBondState(str) == 11)
                    {
                        this.mBondState.setBondState(str, 10, 3);
                        cancelDeviceCreationNative(str);
                        bool1 = true;
                    }
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean cancelDiscovery()
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            boolean bool1 = isEnabledInternal();
            if (!bool1);
            boolean bool2;
            for (boolean bool3 = false; ; bool3 = bool2)
            {
                return bool3;
                bool2 = stopDiscoveryNative();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean cancelPairingUserInput(String paramString)
    {
        boolean bool1 = false;
        while (true)
        {
            String str;
            Integer localInteger;
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
                boolean bool2 = isEnabledInternal();
                if (!bool2)
                    return bool1;
                if (!BluetoothAdapter.checkBluetoothAddress(paramString))
                    continue;
                this.mBondState.setBondState(paramString, 10, 3);
                str = paramString.toUpperCase();
                localInteger = (Integer)this.mEventLoop.getPasskeyAgentRequestData().remove(str);
                if (localInteger == null)
                {
                    Log.w("BluetoothService", "cancelUserInputNative(" + str + ") called but no native data " + "available, ignoring. Maybe the PasskeyAgent Request was already cancelled " + "by the remote or by bluez.\n");
                    continue;
                }
            }
            finally
            {
            }
            boolean bool3 = cancelPairingUserInputNative(str, localInteger.intValue());
            bool1 = bool3;
        }
    }

    public boolean changeApplicationBluetoothState(boolean paramBoolean, IBluetoothStateChangeCallback paramIBluetoothStateChangeCallback, IBinder paramIBinder)
    {
        boolean bool = false;
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        int i = Binder.getCallingPid();
        if (!this.mStateChangeTracker.containsKey(Integer.valueOf(i)))
            if (paramBoolean)
                this.mStateChangeTracker.put(Integer.valueOf(i), paramIBluetoothStateChangeCallback);
        while (true)
        {
            if (paramIBinder != null);
            try
            {
                paramIBinder.linkToDeath(new Reaper(i, 11), 0);
                if (paramBoolean)
                {
                    j = 3;
                    this.mBluetoothState.sendMessage(j, paramIBluetoothStateChangeCallback);
                    bool = true;
                    return bool;
                    if (paramBoolean)
                        continue;
                    this.mStateChangeTracker.remove(Integer.valueOf(i));
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Log.e("BluetoothService", "", localRemoteException);
                    continue;
                    int j = 4;
                }
            }
        }
    }

    /** @deprecated */
    void cleanNativeAfterShutoffBluetooth()
    {
        try
        {
            this.mAdapterProperties.clear();
            disableNative();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    // ERROR //
    void cleanupAfterFinishDisable()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 246	android/server/BluetoothService:mAdapterProperties	Landroid/server/BluetoothAdapterProperties;
        //     6: invokevirtual 1201	android/server/BluetoothAdapterProperties:clear	()V
        //     9: aload_0
        //     10: getfield 264	android/server/BluetoothService:mServiceRecordToPid	Ljava/util/HashMap;
        //     13: invokevirtual 524	java/util/HashMap:keySet	()Ljava/util/Set;
        //     16: invokeinterface 530 1 0
        //     21: astore_2
        //     22: aload_2
        //     23: invokeinterface 535 1 0
        //     28: ifeq +28 -> 56
        //     31: aload_0
        //     32: aload_2
        //     33: invokeinterface 539 1 0
        //     38: checkcast 418	java/lang/Integer
        //     41: invokevirtual 549	java/lang/Integer:intValue	()I
        //     44: invokespecial 467	android/server/BluetoothService:removeServiceRecordNative	(I)Z
        //     47: pop
        //     48: goto -26 -> 22
        //     51: astore_1
        //     52: aload_0
        //     53: monitorexit
        //     54: aload_1
        //     55: athrow
        //     56: aload_0
        //     57: getfield 264	android/server/BluetoothService:mServiceRecordToPid	Ljava/util/HashMap;
        //     60: invokevirtual 1203	java/util/HashMap:clear	()V
        //     63: aload_0
        //     64: iconst_0
        //     65: putfield 179	android/server/BluetoothService:mProfilesConnected	I
        //     68: aload_0
        //     69: iconst_0
        //     70: putfield 181	android/server/BluetoothService:mProfilesConnecting	I
        //     73: aload_0
        //     74: iconst_0
        //     75: putfield 183	android/server/BluetoothService:mProfilesDisconnecting	I
        //     78: aload_0
        //     79: iconst_0
        //     80: putfield 187	android/server/BluetoothService:mAdapterConnectionState	I
        //     83: aload_0
        //     84: aconst_null
        //     85: putfield 1017	android/server/BluetoothService:mAdapterUuids	[Landroid/os/ParcelUuid;
        //     88: aload_0
        //     89: aconst_null
        //     90: putfield 380	android/server/BluetoothService:mAdapterSdpHandles	[I
        //     93: invokestatic 1206	android/os/Binder:clearCallingIdentity	()J
        //     96: lstore_3
        //     97: aload_0
        //     98: getfield 216	android/server/BluetoothService:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     101: invokeinterface 1211 1 0
        //     106: lload_3
        //     107: invokestatic 1214	android/os/Binder:restoreCallingIdentity	(J)V
        //     110: aload_0
        //     111: monitorexit
        //     112: return
        //     113: astore 6
        //     115: lload_3
        //     116: invokestatic 1214	android/os/Binder:restoreCallingIdentity	(J)V
        //     119: aload 6
        //     121: athrow
        //     122: astore 5
        //     124: lload_3
        //     125: invokestatic 1214	android/os/Binder:restoreCallingIdentity	(J)V
        //     128: goto -18 -> 110
        //
        // Exception table:
        //     from	to	target	type
        //     2	48	51	finally
        //     56	97	51	finally
        //     106	110	51	finally
        //     115	128	51	finally
        //     97	106	113	finally
        //     97	106	122	android/os/RemoteException
    }

    void clearApplicationStateChangeTracker()
    {
        this.mStateChangeTracker.clear();
    }

    public boolean connectChannelToSink(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            boolean bool = this.mBluetoothHealthProfileHandler.connectChannel(paramBluetoothDevice, paramBluetoothHealthAppConfiguration, paramInt);
            return bool;
        }
    }

    public boolean connectChannelToSource(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            boolean bool = this.mBluetoothHealthProfileHandler.connectChannelToSource(paramBluetoothDevice, paramBluetoothHealthAppConfiguration);
            return bool;
        }
    }

    public boolean connectHeadset(String paramString)
    {
        boolean bool = false;
        if (getBondState(paramString) != 12);
        while (true)
        {
            return bool;
            BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
            if (localBluetoothDeviceProfileState != null)
            {
                Message localMessage = new Message();
                localMessage.arg1 = 1;
                localMessage.obj = localBluetoothDeviceProfileState;
                this.mHfpProfileState.sendMessage(localMessage);
                bool = true;
            }
        }
    }

    public boolean connectInputDevice(BluetoothDevice paramBluetoothDevice)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
        BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramBluetoothDevice.getAddress());
        synchronized (this.mBluetoothInputProfileHandler)
        {
            boolean bool = this.mBluetoothInputProfileHandler.connectInputDevice(paramBluetoothDevice, localBluetoothDeviceProfileState);
            return bool;
        }
    }

    public boolean connectInputDeviceInternal(BluetoothDevice paramBluetoothDevice)
    {
        synchronized (this.mBluetoothInputProfileHandler)
        {
            boolean bool = this.mBluetoothInputProfileHandler.connectInputDeviceInternal(paramBluetoothDevice);
            return bool;
        }
    }

    native boolean connectInputDeviceNative(String paramString);

    public boolean connectPanDevice(BluetoothDevice paramBluetoothDevice)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
        synchronized (this.mBluetoothPanProfileHandler)
        {
            boolean bool = this.mBluetoothPanProfileHandler.connectPanDevice(paramBluetoothDevice);
            return bool;
        }
    }

    native boolean connectPanDeviceNative(String paramString1, String paramString2);

    public boolean connectSink(String paramString)
    {
        boolean bool = false;
        if (getBondState(paramString) != 12);
        while (true)
        {
            return bool;
            BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
            if (localBluetoothDeviceProfileState != null)
            {
                Message localMessage = new Message();
                localMessage.arg1 = 3;
                localMessage.obj = localBluetoothDeviceProfileState;
                this.mA2dpProfileState.sendMessage(localMessage);
                bool = true;
            }
        }
    }

    /** @deprecated */
    public boolean createBond(String paramString)
    {
        boolean bool1 = false;
        try
        {
            boolean bool2 = isBondingFeasible(paramString);
            if (!bool2);
            while (true)
            {
                return bool1;
                if (createPairedDeviceNative(paramString, 60000))
                {
                    this.mBondState.setPendingOutgoingBonding(paramString);
                    this.mBondState.setBondState(paramString, 11);
                    bool1 = true;
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean createBondOutOfBand(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        boolean bool1 = false;
        try
        {
            boolean bool2 = isBondingFeasible(paramString);
            if (!bool2);
            while (true)
            {
                return bool1;
                if (createPairedDeviceOutOfBandNative(paramString, 60000))
                {
                    setDeviceOutOfBandData(paramString, paramArrayOfByte1, paramArrayOfByte2);
                    this.mBondState.setPendingOutgoingBonding(paramString);
                    this.mBondState.setBondState(paramString, 11);
                    bool1 = true;
                }
            }
        }
        finally
        {
        }
    }

    native boolean createChannelNative(String paramString1, String paramString2, String paramString3, int paramInt);

    native boolean destroyChannelNative(String paramString1, String paramString2, int paramInt);

    public boolean disable()
    {
        return disable(true);
    }

    /** @deprecated */
    public boolean disable(boolean paramBoolean)
    {
        boolean bool = true;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            int i = getBluetoothStateInternal();
            switch (i)
            {
            case 11:
            default:
                bool = false;
            case 10:
            case 12:
            }
            while (true)
            {
                return bool;
                this.mBluetoothState.sendMessage(2, Boolean.valueOf(paramBoolean));
            }
        }
        finally
        {
        }
    }

    native int disableNative();

    public boolean disconnectChannel(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            boolean bool = this.mBluetoothHealthProfileHandler.disconnectChannel(paramBluetoothDevice, paramBluetoothHealthAppConfiguration, paramInt);
            return bool;
        }
    }

    /** @deprecated */
    void disconnectDevices()
    {
        try
        {
            Iterator localIterator1 = getConnectedInputDevices().iterator();
            while (localIterator1.hasNext())
                disconnectInputDevice((BluetoothDevice)localIterator1.next());
        }
        finally
        {
        }
        Iterator localIterator2 = getConnectedPanDevices().iterator();
        while (localIterator2.hasNext())
            disconnectPanDevice((BluetoothDevice)localIterator2.next());
    }

    public boolean disconnectHeadset(String paramString)
    {
        boolean bool = false;
        if (getBondState(paramString) != 12);
        while (true)
        {
            return bool;
            BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
            if (localBluetoothDeviceProfileState != null)
            {
                Message localMessage = new Message();
                localMessage.arg1 = 50;
                localMessage.obj = localBluetoothDeviceProfileState;
                this.mHfpProfileState.sendMessage(localMessage);
                bool = true;
            }
        }
    }

    public boolean disconnectInputDevice(BluetoothDevice paramBluetoothDevice)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
        BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramBluetoothDevice.getAddress());
        synchronized (this.mBluetoothInputProfileHandler)
        {
            boolean bool = this.mBluetoothInputProfileHandler.disconnectInputDevice(paramBluetoothDevice, localBluetoothDeviceProfileState);
            return bool;
        }
    }

    public boolean disconnectInputDeviceInternal(BluetoothDevice paramBluetoothDevice)
    {
        synchronized (this.mBluetoothInputProfileHandler)
        {
            boolean bool = this.mBluetoothInputProfileHandler.disconnectInputDeviceInternal(paramBluetoothDevice);
            return bool;
        }
    }

    native boolean disconnectInputDeviceNative(String paramString);

    public boolean disconnectPanDevice(BluetoothDevice paramBluetoothDevice)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
        synchronized (this.mBluetoothPanProfileHandler)
        {
            boolean bool = this.mBluetoothPanProfileHandler.disconnectPanDevice(paramBluetoothDevice);
            return bool;
        }
    }

    native boolean disconnectPanDeviceNative(String paramString);

    native boolean disconnectPanServerDeviceNative(String paramString1, String paramString2, String paramString3);

    public boolean disconnectSink(String paramString)
    {
        boolean bool = false;
        if (getBondState(paramString) != 12);
        while (true)
        {
            return bool;
            BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
            if (localBluetoothDeviceProfileState != null)
            {
                Message localMessage = new Message();
                localMessage.arg1 = 52;
                localMessage.obj = localBluetoothDeviceProfileState;
                this.mA2dpProfileState.sendMessage(localMessage);
                bool = true;
            }
        }
    }

    native boolean discoverServicesNative(String paramString1, String paramString2);

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "BluetoothService");
        if (getBluetoothStateInternal() != 12);
        while (true)
        {
            return;
            paramPrintWriter.println("mIsAirplaneSensitive = " + this.mIsAirplaneSensitive);
            paramPrintWriter.println("mIsAirplaneToggleable = " + this.mIsAirplaneToggleable);
            paramPrintWriter.println("Local address = " + getAddress());
            paramPrintWriter.println("Local name = " + getName());
            paramPrintWriter.println("isDiscovering() = " + isDiscovering());
            this.mAdapter.getProfileProxy(this.mContext, this.mBluetoothProfileServiceListener, 1);
            this.mAdapter.getProfileProxy(this.mContext, this.mBluetoothProfileServiceListener, 4);
            this.mAdapter.getProfileProxy(this.mContext, this.mBluetoothProfileServiceListener, 5);
            dumpKnownDevices(paramPrintWriter);
            dumpAclConnectedDevices(paramPrintWriter);
            dumpHeadsetService(paramPrintWriter);
            dumpInputDeviceProfile(paramPrintWriter);
            dumpPanProfile(paramPrintWriter);
            dumpApplicationServiceRecords(paramPrintWriter);
            dumpProfileState(paramPrintWriter);
        }
    }

    public boolean enable()
    {
        return enable(true, true);
    }

    /** @deprecated */
    public boolean enable(boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool1 = true;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            if ((this.mIsAirplaneSensitive) && (isAirplaneModeOn()))
            {
                boolean bool2 = this.mIsAirplaneToggleable;
                if (!bool2)
                    bool1 = false;
            }
            while (true)
            {
                return bool1;
                this.mAllowConnect = paramBoolean2;
                this.mBluetoothState.sendMessage(1, Boolean.valueOf(paramBoolean1));
            }
        }
        finally
        {
        }
    }

    native int enableNative();

    public boolean enableNoAutoConnect()
    {
        return enable(false, false);
    }

    /** @deprecated */
    public boolean fetchRemoteUuids(String paramString, ParcelUuid paramParcelUuid, IBluetoothCallback paramIBluetoothCallback)
    {
        boolean bool1 = false;
        while (true)
        {
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
                boolean bool2 = isEnabledInternal();
                if (!bool2)
                    return bool1;
                if (!BluetoothAdapter.checkBluetoothAddress(paramString))
                    continue;
                RemoteService localRemoteService = new RemoteService(paramString, paramParcelUuid);
                if ((paramParcelUuid != null) && (this.mUuidCallbackTracker.get(localRemoteService) != null))
                    continue;
                if (this.mUuidIntentTracker.contains(paramString))
                {
                    if (paramParcelUuid != null)
                        this.mUuidCallbackTracker.put(new RemoteService(paramString, paramParcelUuid), paramIBluetoothCallback);
                }
                else
                {
                    bool1 = createDeviceNative(paramString);
                    this.mUuidIntentTracker.add(paramString);
                    if (paramParcelUuid != null)
                        this.mUuidCallbackTracker.put(new RemoteService(paramString, paramParcelUuid), paramIBluetoothCallback);
                    Message localMessage = this.mHandler.obtainMessage(1);
                    localMessage.obj = paramString;
                    this.mHandler.sendMessageDelayed(localMessage, 6000L);
                    continue;
                }
            }
            finally
            {
            }
            bool1 = true;
        }
    }

    protected void finalize()
        throws Throwable
    {
        this.mContext.unregisterReceiver(this.mReceiver);
        try
        {
            cleanupNativeDataNative();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    /** @deprecated */
    void finishDisable()
    {
        try
        {
            for (String str : this.mBondState.listInState(11))
                this.mBondState.setBondState(str, 10, 3);
            String[] arrayOfString2 = this.mBondState.listInState(12);
            int k = arrayOfString2.length;
            for (int m = 0; m < k; m++)
                removeProfileState(arrayOfString2[m]);
            Intent localIntent = new Intent("android.bluetooth.adapter.action.SCAN_MODE_CHANGED");
            localIntent.putExtra("android.bluetooth.adapter.extra.SCAN_MODE", 20);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getAdapterConnectionState()
    {
        return this.mAdapterConnectionState;
    }

    native String getAdapterPathNative();

    BluetoothAdapterProperties getAdapterProperties()
    {
        return this.mAdapterProperties;
    }

    native Object[] getAdapterPropertiesNative();

    /** @deprecated */
    public String getAddress()
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            String str = getProperty("Address", false);
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    String getAddressFromObjectPath(String paramString)
    {
        String str1 = null;
        String str2 = this.mAdapterProperties.getObjectPath();
        if ((str2 == null) || (paramString == null))
            Log.e("BluetoothService", "getAddressFromObjectPath: AdapterObjectPath:" + str2 + "    or deviceObjectPath:" + paramString + " is null");
        while (true)
        {
            return str1;
            if (!paramString.startsWith(str2))
            {
                Log.e("BluetoothService", "getAddressFromObjectPath: AdapterObjectPath:" + str2 + "    is not a prefix of deviceObjectPath:" + paramString + "bluetoothd crashed ?");
            }
            else
            {
                String str3 = paramString.substring(str2.length());
                if (str3 != null)
                    str1 = str3.replace('_', ':');
                else
                    Log.e("BluetoothService", "getAddressFromObjectPath: Address being returned is null");
            }
        }
    }

    Collection<IBluetoothStateChangeCallback> getApplicationStateChangeCallbacks()
    {
        return this.mStateChangeTracker.values();
    }

    Integer getAuthorizationAgentRequestData(String paramString)
    {
        return (Integer)this.mEventLoop.getAuthorizationAgentRequestData().remove(paramString);
    }

    public int getBluetoothState()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        return getBluetoothStateInternal();
    }

    int getBluetoothStateInternal()
    {
        return this.mBluetoothState.getBluetoothAdapterState();
    }

    /** @deprecated */
    public int getBondState(String paramString)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            boolean bool = BluetoothAdapter.checkBluetoothAddress(paramString);
            if (!bool);
            int i;
            for (int j = -2147483648; ; j = i)
            {
                return j;
                i = this.mBondState.getBondState(paramString.toUpperCase());
            }
        }
        finally
        {
        }
    }

    native String getChannelApplicationNative(String paramString);

    native ParcelFileDescriptor getChannelFdNative(String paramString);

    public List<BluetoothDevice> getConnectedHealthDevices()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            List localList = this.mBluetoothHealthProfileHandler.getConnectedHealthDevices();
            return localList;
        }
    }

    public List<BluetoothDevice> getConnectedInputDevices()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothInputProfileHandler)
        {
            List localList = this.mBluetoothInputProfileHandler.getConnectedInputDevices();
            return localList;
        }
    }

    public List<BluetoothDevice> getConnectedPanDevices()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothPanProfileHandler)
        {
            List localList = this.mBluetoothPanProfileHandler.getConnectedPanDevices();
            return localList;
        }
    }

    Pair<byte[], byte[]> getDeviceOutOfBandData(BluetoothDevice paramBluetoothDevice)
    {
        return (Pair)this.mDeviceOobData.get(paramBluetoothDevice.getAddress());
    }

    BluetoothDeviceProperties getDeviceProperties()
    {
        return this.mDeviceProperties;
    }

    public int getDiscoverableTimeout()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        String str = getProperty("DiscoverableTimeout", true);
        if (str != null);
        for (int i = Integer.valueOf(str).intValue(); ; i = -1)
            return i;
    }

    /** @deprecated */
    String getDockPin()
    {
        try
        {
            String str = this.mDockPin;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getHealthDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            int i = this.mBluetoothHealthProfileHandler.getHealthDeviceConnectionState(paramBluetoothDevice);
            return i;
        }
    }

    public List<BluetoothDevice> getHealthDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            List localList = this.mBluetoothHealthProfileHandler.getHealthDevicesMatchingConnectionStates(paramArrayOfInt);
            return localList;
        }
    }

    public Pair<Integer, String> getIncomingState(String paramString)
    {
        if (this.mIncomingConnections.isEmpty())
        {
            createIncomingConnectionStateFile();
            readIncomingConnectionState();
        }
        return (Pair)this.mIncomingConnections.get(paramString);
    }

    public int getInputDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothInputProfileHandler)
        {
            int i = this.mBluetoothInputProfileHandler.getInputDeviceConnectionState(paramBluetoothDevice);
            return i;
        }
    }

    public int getInputDevicePriority(BluetoothDevice paramBluetoothDevice)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothInputProfileHandler)
        {
            int i = this.mBluetoothInputProfileHandler.getInputDevicePriority(paramBluetoothDevice);
            return i;
        }
    }

    public List<BluetoothDevice> getInputDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothInputProfileHandler)
        {
            List localList = this.mBluetoothInputProfileHandler.getInputDevicesMatchingConnectionStates(paramArrayOfInt);
            return localList;
        }
    }

    String[] getKnownDevices()
    {
        String[] arrayOfString = null;
        String str = getProperty("Devices", true);
        if (str != null)
            arrayOfString = str.split(",");
        return arrayOfString;
    }

    public ParcelFileDescriptor getMainChannelFd(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            ParcelFileDescriptor localParcelFileDescriptor = this.mBluetoothHealthProfileHandler.getMainChannelFd(paramBluetoothDevice, paramBluetoothHealthAppConfiguration);
            return localParcelFileDescriptor;
        }
    }

    native String getMainChannelNative(String paramString);

    /** @deprecated */
    public String getName()
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            String str = getProperty("Name", false);
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    int getNumberOfApplicationStateChangeTrackers()
    {
        return this.mStateChangeTracker.size();
    }

    String getObjectPathFromAddress(String paramString)
    {
        String str1 = this.mAdapterProperties.getObjectPath();
        if (str1 == null)
            Log.e("BluetoothService", "Error: Object Path is null");
        for (String str2 = null; ; str2 = str1 + paramString.replace(":", "_"))
            return str2;
    }

    public int getPanDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothPanProfileHandler)
        {
            int i = this.mBluetoothPanProfileHandler.getPanDeviceConnectionState(paramBluetoothDevice);
            return i;
        }
    }

    public List<BluetoothDevice> getPanDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothPanProfileHandler)
        {
            List localList = this.mBluetoothPanProfileHandler.getPanDevicesMatchingConnectionStates(paramArrayOfInt);
            return localList;
        }
    }

    /** @deprecated */
    String getPendingOutgoingBonding()
    {
        try
        {
            String str = this.mBondState.getPendingOutgoingBonding();
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getProfileConnectionState(int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        Pair localPair = (Pair)this.mProfileConnectionState.get(Integer.valueOf(paramInt));
        if (localPair == null);
        for (int i = 0; ; i = ((Integer)localPair.first).intValue())
            return i;
    }

    String getProperty(String paramString, boolean paramBoolean)
    {
        String str = null;
        if (paramBoolean)
            if (isEnabledInternal())
                break label25;
        while (true)
        {
            return str;
            if (this.mEventLoop.isEventLoopRunning())
                label25: str = this.mAdapterProperties.getProperty(paramString);
        }
    }

    /** @deprecated */
    public String getRemoteAlias(String paramString)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            boolean bool = BluetoothAdapter.checkBluetoothAddress(paramString);
            if (!bool);
            String str;
            for (Object localObject2 = null; ; localObject2 = str)
            {
                return localObject2;
                str = this.mDeviceProperties.getProperty(paramString, "Alias");
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public int getRemoteClass(String paramString)
    {
        int i = -16777216;
        try
        {
            if (!BluetoothAdapter.checkBluetoothAddress(paramString))
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            while (true)
            {
                return i;
                String str = this.mDeviceProperties.getProperty(paramString, "Class");
                if (str != null)
                {
                    int j = Integer.valueOf(str).intValue();
                    i = j;
                }
            }
        }
        finally
        {
        }
    }

    BluetoothDevice getRemoteDevice(String paramString)
    {
        return this.mAdapter.getRemoteDevice(paramString);
    }

    String[] getRemoteDeviceProperties(String paramString)
    {
        if (!isEnabledInternal());
        for (String[] arrayOfString = null; ; arrayOfString = (String[])getDevicePropertiesNative(getObjectPathFromAddress(paramString)))
            return arrayOfString;
    }

    /** @deprecated */
    public String getRemoteName(String paramString)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            boolean bool = BluetoothAdapter.checkBluetoothAddress(paramString);
            if (!bool);
            String str;
            for (Object localObject2 = null; ; localObject2 = str)
            {
                return localObject2;
                str = this.mDeviceProperties.getProperty(paramString, "Name");
            }
        }
        finally
        {
        }
    }

    public int getRemoteServiceChannel(String paramString, ParcelUuid paramParcelUuid)
    {
        int i = -1;
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        if (!isEnabledInternal());
        while (true)
        {
            return i;
            if (!BluetoothAdapter.checkBluetoothAddress(paramString))
            {
                i = -2147483648;
            }
            else if ((!this.mDeviceProperties.isEmpty()) || (this.mDeviceProperties.updateCache(paramString) != null))
            {
                Map localMap = (Map)this.mDeviceServiceChannelCache.get(paramString);
                if ((localMap != null) && (localMap.containsKey(paramParcelUuid)))
                    i = ((Integer)localMap.get(paramParcelUuid)).intValue();
            }
        }
    }

    /** @deprecated */
    public ParcelUuid[] getRemoteUuids(String paramString)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            boolean bool = BluetoothAdapter.checkBluetoothAddress(paramString);
            if (!bool);
            ParcelUuid[] arrayOfParcelUuid;
            for (Object localObject2 = null; ; localObject2 = arrayOfParcelUuid)
            {
                return localObject2;
                arrayOfParcelUuid = getUuidFromCache(paramString);
            }
        }
        finally
        {
        }
    }

    public int getScanMode()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        if (!isEnabledInternal());
        for (int i = 20; ; i = bluezStringToScanMode(getProperty("Pairable", true).equals("true"), getProperty("Discoverable", true).equals("true")))
            return i;
    }

    /** @deprecated */
    public boolean getTrustState(String paramString)
    {
        boolean bool1 = false;
        try
        {
            if (!BluetoothAdapter.checkBluetoothAddress(paramString))
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            while (true)
            {
                return bool1;
                String str = this.mDeviceProperties.getProperty(paramString, "Trusted");
                if (str != null)
                {
                    boolean bool2 = str.equals("true");
                    bool1 = bool2;
                }
            }
        }
        finally
        {
        }
    }

    ParcelUuid[] getUuidFromCache(String paramString)
    {
        String str = this.mDeviceProperties.getProperty(paramString, "UUIDs");
        ParcelUuid[] arrayOfParcelUuid;
        if (str == null)
            arrayOfParcelUuid = null;
        while (true)
        {
            return arrayOfParcelUuid;
            String[] arrayOfString = str.split(",");
            arrayOfParcelUuid = new ParcelUuid[arrayOfString.length];
            for (int i = 0; i < arrayOfString.length; i++)
                arrayOfParcelUuid[i] = ParcelUuid.fromString(arrayOfString[i]);
        }
    }

    public ParcelUuid[] getUuids()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        String str = getProperty("UUIDs", true);
        if (str == null);
        for (ParcelUuid[] arrayOfParcelUuid = null; ; arrayOfParcelUuid = convertStringToParcelUuid(str))
            return arrayOfParcelUuid;
    }

    void handleInputDevicePropertyChange(String paramString, boolean paramBoolean)
    {
        synchronized (this.mBluetoothInputProfileHandler)
        {
            this.mBluetoothInputProfileHandler.handleInputDevicePropertyChange(paramString, paramBoolean);
            return;
        }
    }

    void handlePanDeviceStateChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2)
    {
        synchronized (this.mBluetoothPanProfileHandler)
        {
            this.mBluetoothPanProfileHandler.handlePanDeviceStateChange(paramBluetoothDevice, null, paramInt1, paramInt2);
            return;
        }
    }

    void handlePanDeviceStateChange(BluetoothDevice paramBluetoothDevice, String paramString, int paramInt1, int paramInt2)
    {
        synchronized (this.mBluetoothPanProfileHandler)
        {
            this.mBluetoothPanProfileHandler.handlePanDeviceStateChange(paramBluetoothDevice, paramString, paramInt1, paramInt2);
            return;
        }
    }

    /** @deprecated */
    public void initAfterA2dpRegistration()
    {
        try
        {
            this.mEventLoop.getProfileProxy();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void initAfterRegistration()
    {
        try
        {
            this.mAdapter = BluetoothAdapter.getDefaultAdapter();
            this.mBluetoothState = new BluetoothAdapterStateMachine(this.mContext, this, this.mAdapter);
            this.mBluetoothState.start();
            if (this.mContext.getResources().getBoolean(17891367))
                this.mBluetoothState.sendMessage(5);
            this.mEventLoop = this.mBluetoothState.getBluetoothEventLoop();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void initBluetoothAfterTurningOn()
    {
        String str1 = getProperty("Discoverable", false);
        String str2 = getProperty("DiscoverableTimeout", false);
        if (str2 == null)
        {
            Log.w("BluetoothService", "Null DiscoverableTimeout property");
            str2 = "1";
        }
        if ((str1.equals("true")) && (Integer.valueOf(str2).intValue() != 0))
            setAdapterPropertyBooleanNative("Discoverable", 0);
        this.mBondState.initBondState();
        initProfileState();
        getProfileProxy();
    }

    final boolean isAirplaneModeOn()
    {
        int i = 1;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "airplane_mode_on", 0) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    boolean isApplicationStateChangeTrackerEmpty()
    {
        return this.mStateChangeTracker.isEmpty();
    }

    /** @deprecated */
    public boolean isBluetoothDock(String paramString)
    {
        try
        {
            boolean bool = this.mContext.getSharedPreferences("bluetooth_service_settings", 0).contains("dock_bluetooth_address" + paramString);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isDiscovering()
    {
        boolean bool = false;
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        String str = getProperty("Discovering", false);
        if (str == null);
        while (true)
        {
            return bool;
            bool = str.equals("true");
        }
    }

    public boolean isEnabled()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        return isEnabledInternal();
    }

    /** @deprecated */
    boolean isFixedPinZerosAutoPairKeyboard(String paramString)
    {
        try
        {
            boolean bool = this.mBondState.isFixedPinZerosAutoPairKeyboard(paramString);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    boolean isRemoteDeviceInCache(String paramString)
    {
        return this.mDeviceProperties.isInCache(paramString);
    }

    public boolean isTetheringOn()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothPanProfileHandler)
        {
            boolean bool = this.mBluetoothPanProfileHandler.isTetheringOn();
            return bool;
        }
    }

    /** @deprecated */
    public String[] listBonds()
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            String[] arrayOfString = this.mBondState.listInState(12);
            return arrayOfString;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    String[] listInState(int paramInt)
    {
        try
        {
            String[] arrayOfString = this.mBondState.listInState(paramInt);
            return arrayOfString;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    List<BluetoothDevice> lookupInputDevicesMatchingStates(int[] paramArrayOfInt)
    {
        synchronized (this.mBluetoothInputProfileHandler)
        {
            List localList = this.mBluetoothInputProfileHandler.lookupInputDevicesMatchingStates(paramArrayOfInt);
            return localList;
        }
    }

    /** @deprecated */
    // ERROR //
    void makeServiceChannelCallbacks(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 262	android/server/BluetoothService:mUuidCallbackTracker	Ljava/util/HashMap;
        //     6: invokevirtual 524	java/util/HashMap:keySet	()Ljava/util/Set;
        //     9: invokeinterface 530 1 0
        //     14: astore_3
        //     15: aload_3
        //     16: invokeinterface 535 1 0
        //     21: ifeq +128 -> 149
        //     24: aload_3
        //     25: invokeinterface 539 1 0
        //     30: checkcast 15	android/server/BluetoothService$RemoteService
        //     33: astore 4
        //     35: aload 4
        //     37: getfield 661	android/server/BluetoothService$RemoteService:address	Ljava/lang/String;
        //     40: aload_1
        //     41: invokevirtual 665	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     44: ifeq -29 -> 15
        //     47: ldc 66
        //     49: new 427	java/lang/StringBuilder
        //     52: dup
        //     53: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     56: ldc_w 1630
        //     59: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     62: aload 4
        //     64: getfield 661	android/server/BluetoothService$RemoteService:address	Ljava/lang/String;
        //     67: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     70: ldc_w 1632
        //     73: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     76: aload 4
        //     78: getfield 670	android/server/BluetoothService$RemoteService:uuid	Landroid/os/ParcelUuid;
        //     81: invokevirtual 544	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     84: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     87: invokestatic 388	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     90: pop
        //     91: aload_0
        //     92: getfield 262	android/server/BluetoothService:mUuidCallbackTracker	Ljava/util/HashMap;
        //     95: aload 4
        //     97: invokevirtual 398	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     100: checkcast 1634	android/bluetooth/IBluetoothCallback
        //     103: astore 6
        //     105: aload 6
        //     107: ifnull +12 -> 119
        //     110: aload 6
        //     112: bipush 255
        //     114: invokeinterface 1637 2 0
        //     119: aload_3
        //     120: invokeinterface 1639 1 0
        //     125: goto -110 -> 15
        //     128: astore_2
        //     129: aload_0
        //     130: monitorexit
        //     131: aload_2
        //     132: athrow
        //     133: astore 7
        //     135: ldc 66
        //     137: ldc_w 1108
        //     140: aload 7
        //     142: invokestatic 1111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     145: pop
        //     146: goto -27 -> 119
        //     149: aload_0
        //     150: monitorexit
        //     151: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	105	128	finally
        //     110	119	128	finally
        //     119	125	128	finally
        //     135	146	128	finally
        //     110	119	133	android/os/RemoteException
    }

    boolean notifyIncomingA2dpConnection(String paramString, boolean paramBoolean)
    {
        boolean bool = false;
        while (true)
        {
            BluetoothDeviceProfileState localBluetoothDeviceProfileState;
            Message localMessage;
            try
            {
                if (!this.mAllowConnect)
                {
                    Log.d("BluetoothService", "Not allowing a2dp connection because of temporary BT on state.");
                }
                else
                {
                    localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
                    if (localBluetoothDeviceProfileState != null)
                    {
                        localMessage = new Message();
                        if (paramBoolean)
                        {
                            if (this.mHeadsetProxy.getPriority(getRemoteDevice(paramString)) >= 100)
                            {
                                localMessage.what = 103;
                                localMessage.arg1 = 1;
                                localBluetoothDeviceProfileState.sendMessageDelayed(localMessage, 4000L);
                            }
                            bool = true;
                        }
                    }
                }
            }
            finally
            {
            }
            localMessage.what = 4;
            localBluetoothDeviceProfileState.sendMessage(localMessage);
        }
        return bool;
    }

    public boolean notifyIncomingConnection(String paramString, boolean paramBoolean)
    {
        boolean bool = false;
        while (true)
        {
            BluetoothDeviceProfileState localBluetoothDeviceProfileState;
            Message localMessage;
            try
            {
                if (!this.mAllowConnect)
                {
                    Log.d("BluetoothService", "Not allowing incoming connection because of temporary BT on state.");
                }
                else
                {
                    localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
                    if (localBluetoothDeviceProfileState != null)
                    {
                        localMessage = new Message();
                        if (paramBoolean)
                        {
                            if (this.mA2dpService.getPriority(getRemoteDevice(paramString)) >= 100)
                            {
                                localMessage.what = 103;
                                localMessage.arg1 = 3;
                                localBluetoothDeviceProfileState.sendMessageDelayed(localMessage, 4000L);
                            }
                            bool = true;
                        }
                    }
                }
            }
            finally
            {
            }
            localMessage.what = 2;
            localBluetoothDeviceProfileState.sendMessage(localMessage);
        }
        return bool;
    }

    boolean notifyIncomingHidConnection(String paramString)
    {
        BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
        if (localBluetoothDeviceProfileState == null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            Message localMessage = new Message();
            localMessage.what = 6;
            localBluetoothDeviceProfileState.sendMessage(localMessage);
        }
    }

    /** @deprecated */
    void onCreatePairedDeviceResult(String paramString, int paramInt)
    {
        if (paramInt == 0);
        while (true)
        {
            try
            {
                setBondState(paramString, 12);
                if (this.mBondState.isAutoPairingAttemptsInProgress(paramString))
                    this.mBondState.clearPinAttempts(paramString);
                return;
                if ((paramInt == 1) && (this.mBondState.getAttempt(paramString) == 1))
                {
                    this.mBondState.addAutoPairingFailure(paramString);
                    pairingAttempt(paramString, paramInt);
                    continue;
                }
            }
            finally
            {
            }
            if ((paramInt == 4) && (this.mBondState.isAutoPairingAttemptsInProgress(paramString)))
            {
                pairingAttempt(paramString, paramInt);
            }
            else
            {
                setBondState(paramString, 10, paramInt);
                if (this.mBondState.isAutoPairingAttemptsInProgress(paramString))
                    this.mBondState.clearPinAttempts(paramString);
            }
        }
    }

    void onHealthDeviceChannelChanged(String paramString1, String paramString2, boolean paramBoolean)
    {
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            this.mBluetoothHealthProfileHandler.onHealthDeviceChannelChanged(paramString1, paramString2, paramBoolean);
            return;
        }
    }

    void onHealthDeviceChannelConnectionError(int paramInt1, int paramInt2)
    {
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            this.mBluetoothHealthProfileHandler.onHealthDeviceChannelConnectionError(paramInt1, paramInt2);
            return;
        }
    }

    void onHealthDevicePropertyChanged(String paramString1, String paramString2)
    {
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            this.mBluetoothHealthProfileHandler.onHealthDevicePropertyChanged(paramString1, paramString2);
            return;
        }
    }

    /** @deprecated */
    boolean prepareBluetooth()
    {
        boolean bool1 = false;
        try
        {
            boolean bool2 = setupNativeDataNative();
            if (!bool2);
            while (true)
            {
                return bool1;
                switchConnectable(false);
                try
                {
                    Thread.sleep(50L);
                    label28: updateSdpRecords();
                    bool1 = true;
                }
                catch (InterruptedException localInterruptedException)
                {
                    break label28;
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public byte[] readOutOfBandData()
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            boolean bool = isEnabledInternal();
            if (!bool);
            byte[] arrayOfByte;
            for (Object localObject2 = null; ; localObject2 = arrayOfByte)
            {
                return localObject2;
                arrayOfByte = readAdapterOutOfBandDataNative();
            }
        }
        finally
        {
        }
    }

    public boolean registerAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, IBluetoothHealthCallback paramIBluetoothHealthCallback)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            boolean bool = this.mBluetoothHealthProfileHandler.registerAppConfiguration(paramBluetoothHealthAppConfiguration, paramIBluetoothHealthCallback);
            return bool;
        }
    }

    native String registerHealthApplicationNative(int paramInt, String paramString1, String paramString2);

    native String registerHealthApplicationNative(int paramInt, String paramString1, String paramString2, String paramString3);

    native boolean releaseChannelFdNative(String paramString);

    /** @deprecated */
    public boolean removeBond(String paramString)
    {
        boolean bool1 = false;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            boolean bool2 = isEnabledInternal();
            if (!bool2);
            while (true)
            {
                return bool1;
                if (BluetoothAdapter.checkBluetoothAddress(paramString))
                {
                    BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
                    if (localBluetoothDeviceProfileState != null)
                    {
                        localBluetoothDeviceProfileState.sendMessage(100);
                        bool1 = true;
                    }
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean removeBondInternal(String paramString)
    {
        try
        {
            setTrust(paramString, false);
            boolean bool = removeDeviceNative(getObjectPathFromAddress(paramString));
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void removeProfileState(String paramString)
    {
        BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)this.mDeviceProfileState.get(paramString);
        if (localBluetoothDeviceProfileState == null);
        while (true)
        {
            return;
            localBluetoothDeviceProfileState.quit();
            this.mDeviceProfileState.remove(paramString);
        }
    }

    public void removeServiceRecord(int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        if (getBluetoothStateInternal() == 10);
        while (true)
        {
            return;
            Message localMessage = this.mHandler.obtainMessage(3);
            localMessage.obj = new Pair(Integer.valueOf(paramInt), Integer.valueOf(Binder.getCallingPid()));
            this.mHandler.sendMessage(localMessage);
        }
    }

    void runBluetooth()
    {
        autoConnect();
        long l = Binder.clearCallingIdentity();
        try
        {
            this.mBatteryStats.noteBluetoothOn();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("BluetoothService", "", localRemoteException);
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    /** @deprecated */
    public void sendConnectionStateChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, int paramInt3)
    {
        while (true)
        {
            try
            {
                int i = getBluetoothStateInternal();
                if (i == 10)
                    return;
                if ((!validateProfileConnectionState(paramInt2)) || (!validateProfileConnectionState(paramInt3)))
                {
                    Log.e("BluetoothService", "Error in sendConnectionStateChange: prevState " + paramInt3 + " state " + paramInt2);
                    continue;
                }
            }
            finally
            {
            }
            updateProfileConnectionState(paramInt1, paramInt2, paramInt3);
            if (updateCountersAndCheckForConnectionStateChange(paramInt2, paramInt3))
            {
                this.mAdapterConnectionState = paramInt2;
                if (paramInt2 == 0)
                    this.mBluetoothState.sendMessage(52);
                Intent localIntent = new Intent("android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED");
                localIntent.putExtra("android.bluetooth.device.extra.DEVICE", paramBluetoothDevice);
                localIntent.putExtra("android.bluetooth.adapter.extra.CONNECTION_STATE", convertToAdapterState(paramInt2));
                localIntent.putExtra("android.bluetooth.adapter.extra.PREVIOUS_CONNECTION_STATE", convertToAdapterState(paramInt3));
                localIntent.addFlags(134217728);
                this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH");
                Log.d("BluetoothService", "CONNECTION_STATE_CHANGE: " + paramBluetoothDevice + ": " + paramInt3 + " -> " + paramInt2);
            }
        }
    }

    public void sendProfileStateMessage(int paramInt1, int paramInt2)
    {
        Message localMessage = new Message();
        localMessage.what = paramInt2;
        if (paramInt1 == 0)
            this.mHfpProfileState.sendMessage(localMessage);
        while (true)
        {
            return;
            if (paramInt1 == 1)
                this.mA2dpProfileState.sendMessage(localMessage);
        }
    }

    /** @deprecated */
    void sendUuidIntent(String paramString)
    {
        try
        {
            ParcelUuid[] arrayOfParcelUuid = getUuidFromCache(paramString);
            Intent localIntent = new Intent("android.bluetooth.device.action.UUID");
            localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mAdapter.getRemoteDevice(paramString));
            localIntent.putExtra("android.bluetooth.device.extra.UUID", arrayOfParcelUuid);
            this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
            this.mUuidIntentTracker.remove(paramString);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void setA2dpService(BluetoothA2dpService paramBluetoothA2dpService)
    {
        this.mA2dpService = paramBluetoothA2dpService;
    }

    native boolean setAuthorizationNative(String paramString, boolean paramBoolean, int paramInt);

    public void setBluetoothTethering(boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothPanProfileHandler)
        {
            this.mBluetoothPanProfileHandler.setBluetoothTethering(paramBoolean);
            return;
        }
    }

    native boolean setBluetoothTetheringNative(boolean paramBoolean, String paramString1, String paramString2);

    /** @deprecated */
    boolean setBondState(String paramString, int paramInt)
    {
        try
        {
            boolean bool = setBondState(paramString, paramInt, 0);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    boolean setBondState(String paramString, int paramInt1, int paramInt2)
    {
        try
        {
            this.mBondState.setBondState(paramString.toUpperCase(), paramInt1, paramInt2);
            return true;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean setDeviceOutOfBandData(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            boolean bool1 = isEnabledInternal();
            if (!bool1);
            for (boolean bool2 = false; ; bool2 = true)
            {
                return bool2;
                Pair localPair = new Pair(paramArrayOfByte1, paramArrayOfByte2);
                Log.d("BluetoothService", "Setting out of band data for: " + paramString + ":" + Arrays.toString(paramArrayOfByte1) + ":" + Arrays.toString(paramArrayOfByte2));
                this.mDeviceOobData.put(paramString, localPair);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean setDiscoverableTimeout(int paramInt)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            boolean bool = setPropertyInteger("DiscoverableTimeout", paramInt);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean setInputDevicePriority(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
        synchronized (this.mBluetoothInputProfileHandler)
        {
            boolean bool = this.mBluetoothInputProfileHandler.setInputDevicePriority(paramBluetoothDevice, paramInt);
            return bool;
        }
    }

    void setLinkTimeout(String paramString, int paramInt)
    {
        if (!setLinkTimeoutNative(getObjectPathFromAddress(paramString), paramInt))
            Log.d("BluetoothService", "Set Link Timeout to " + paramInt + " slots failed");
    }

    /** @deprecated */
    public boolean setName(String paramString)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            if (paramString == null);
            boolean bool1;
            for (boolean bool2 = false; ; bool2 = bool1)
            {
                return bool2;
                bool1 = setPropertyString("Name", paramString);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    void setPairable()
    {
        try
        {
            String str = getProperty("Pairable", false);
            if (str == null)
                Log.e("BluetoothService", "null pairableString");
            while (true)
            {
                return;
                if (str.equals("false"))
                    setAdapterPropertyBooleanNative("Pairable", 1);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean setPairingConfirmation(String paramString, boolean paramBoolean)
    {
        boolean bool1 = false;
        while (true)
        {
            String str;
            Integer localInteger;
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
                boolean bool2 = isEnabledInternal();
                if (!bool2)
                    return bool1;
                str = paramString.toUpperCase();
                localInteger = (Integer)this.mEventLoop.getPasskeyAgentRequestData().remove(str);
                if (localInteger == null)
                {
                    Log.w("BluetoothService", "setPasskey(" + str + ") called but no native data available, " + "ignoring. Maybe the PasskeyAgent Request was cancelled by the remote device" + " or by bluez.\n");
                    continue;
                }
            }
            finally
            {
            }
            boolean bool3 = setPairingConfirmationNative(str, paramBoolean, localInteger.intValue());
            bool1 = bool3;
        }
    }

    /** @deprecated */
    public boolean setPasskey(String paramString, int paramInt)
    {
        boolean bool1 = false;
        while (true)
        {
            String str;
            Integer localInteger;
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
                boolean bool2 = isEnabledInternal();
                if (!bool2)
                    return bool1;
                if ((paramInt < 0) || (paramInt > 999999) || (!BluetoothAdapter.checkBluetoothAddress(paramString)))
                    continue;
                str = paramString.toUpperCase();
                localInteger = (Integer)this.mEventLoop.getPasskeyAgentRequestData().remove(str);
                if (localInteger == null)
                {
                    Log.w("BluetoothService", "setPasskey(" + str + ") called but no native data available, " + "ignoring. Maybe the PasskeyAgent Request was cancelled by the remote device" + " or by bluez.\n");
                    continue;
                }
            }
            finally
            {
            }
            boolean bool3 = setPasskeyNative(str, paramInt, localInteger.intValue());
            bool1 = bool3;
        }
    }

    /** @deprecated */
    public boolean setPin(String paramString, byte[] paramArrayOfByte)
    {
        boolean bool1 = false;
        while (true)
        {
            String str1;
            Integer localInteger;
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
                boolean bool2 = isEnabledInternal();
                if (!bool2)
                    return bool1;
                if ((paramArrayOfByte == null) || (paramArrayOfByte.length <= 0) || (paramArrayOfByte.length > 16) || (!BluetoothAdapter.checkBluetoothAddress(paramString)))
                    continue;
                str1 = paramString.toUpperCase();
                localInteger = (Integer)this.mEventLoop.getPasskeyAgentRequestData().remove(str1);
                if (localInteger == null)
                {
                    Log.w("BluetoothService", "setPin(" + str1 + ") called but no native data available, " + "ignoring. Maybe the PasskeyAgent Request was cancelled by the remote device" + " or by bluez.\n");
                    continue;
                }
            }
            finally
            {
            }
            try
            {
                String str2 = new String(paramArrayOfByte, "UTF8");
                bool1 = setPinNative(str1, str2, localInteger.intValue());
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                Log.e("BluetoothService", "UTF8 not supported?!?");
            }
        }
    }

    /** @deprecated */
    public boolean setRemoteAlias(String paramString1, String paramString2)
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
            boolean bool1 = BluetoothAdapter.checkBluetoothAddress(paramString1);
            if (!bool1);
            boolean bool2;
            for (boolean bool3 = false; ; bool3 = bool2)
            {
                return bool3;
                bool2 = setDevicePropertyStringNative(getObjectPathFromAddress(paramString1), "Alias", paramString2);
            }
        }
        finally
        {
        }
    }

    void setRemoteDeviceProperty(String paramString1, String paramString2, String paramString3)
    {
        this.mDeviceProperties.setProperty(paramString1, paramString2, paramString3);
    }

    /** @deprecated */
    public boolean setRemoteOutOfBandData(String paramString)
    {
        boolean bool1 = false;
        String str;
        Integer localInteger;
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            boolean bool2 = isEnabledInternal();
            if (!bool2);
            while (true)
            {
                return bool1;
                str = paramString.toUpperCase();
                localInteger = (Integer)this.mEventLoop.getPasskeyAgentRequestData().remove(str);
                if (localInteger != null)
                    break;
                Log.w("BluetoothService", "setRemoteOobData(" + str + ") called but no native data available, " + "ignoring. Maybe the PasskeyAgent Request was cancelled by the remote device" + " or by bluez.\n");
            }
        }
        finally
        {
        }
        Pair localPair = (Pair)this.mDeviceOobData.get(str);
        byte[] arrayOfByte1;
        if (localPair == null)
            arrayOfByte1 = new byte[16];
        for (byte[] arrayOfByte2 = new byte[16]; ; arrayOfByte2 = (byte[])localPair.second)
        {
            bool1 = setRemoteOutOfBandDataNative(str, arrayOfByte1, arrayOfByte2, localInteger.intValue());
            break;
            arrayOfByte1 = (byte[])localPair.first;
        }
    }

    /** @deprecated */
    public boolean setScanMode(int paramInt1, int paramInt2)
    {
        while (true)
        {
            try
            {
                this.mContext.enforceCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS", "Need WRITE_SECURE_SETTINGS permission");
                boolean bool3;
                switch (paramInt1)
                {
                case 22:
                default:
                    Log.w("BluetoothService", "Requested invalid scan mode " + paramInt1);
                    bool3 = false;
                    return bool3;
                case 20:
                    bool1 = false;
                    bool2 = false;
                    setPropertyBoolean("Discoverable", bool2);
                    setPropertyBoolean("Pairable", bool1);
                    bool3 = true;
                    break;
                case 23:
                    bool1 = true;
                    bool2 = true;
                    Log.d("BluetoothService", "BT Discoverable for " + paramInt2 + " seconds");
                    continue;
                case 21:
                }
            }
            finally
            {
            }
            boolean bool1 = true;
            boolean bool2 = false;
        }
    }

    /** @deprecated */
    public boolean setTrust(String paramString, boolean paramBoolean)
    {
        int i = 0;
        try
        {
            if (!BluetoothAdapter.checkBluetoothAddress(paramString))
                this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            while (true)
            {
                return i;
                if (isEnabledInternal())
                {
                    String str = getObjectPathFromAddress(paramString);
                    if (paramBoolean)
                        i = 1;
                    int j = setDevicePropertyBooleanNative(str, "Trusted", i);
                    i = j;
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    void shutoffBluetooth()
    {
        try
        {
            if (this.mAdapterSdpHandles != null)
                removeReservedServiceRecordsNative(this.mAdapterSdpHandles);
            setBluetoothTetheringNative(false, "nap", "pan1");
            tearDownNativeDataNative();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean startDiscovery()
    {
        try
        {
            this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH_ADMIN", "Need BLUETOOTH_ADMIN permission");
            boolean bool1 = isEnabledInternal();
            if (!bool1);
            boolean bool2;
            for (boolean bool3 = false; ; bool3 = bool2)
            {
                return bool3;
                bool2 = startDiscoveryNative();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    void switchConnectable(boolean paramBoolean)
    {
        try
        {
            if (paramBoolean)
            {
                i = 1;
                setAdapterPropertyBooleanNative("Powered", i);
                return;
            }
            int i = 0;
        }
        finally
        {
        }
    }

    public boolean unregisterAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BLUETOOTH", "Need BLUETOOTH permission");
        synchronized (this.mBluetoothHealthProfileHandler)
        {
            boolean bool = this.mBluetoothHealthProfileHandler.unregisterAppConfiguration(paramBluetoothHealthAppConfiguration);
            return bool;
        }
    }

    native boolean unregisterHealthApplicationNative(String paramString);

    /** @deprecated */
    void updateBluetoothState(String paramString)
    {
        try
        {
            ParcelUuid[] arrayOfParcelUuid = convertStringToParcelUuid(paramString);
            if ((this.mAdapterUuids != null) && (BluetoothUuid.containsAllUuids(arrayOfParcelUuid, this.mAdapterUuids)))
                this.mBluetoothState.sendMessage(51);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    void updateDeviceServiceChannelCache(String paramString)
    {
        // Byte code:
        //     0: ldc 66
        //     2: new 427	java/lang/StringBuilder
        //     5: dup
        //     6: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     9: ldc_w 1877
        //     12: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     15: aload_1
        //     16: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     19: ldc_w 1879
        //     22: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     25: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     28: invokestatic 388	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     31: pop
        //     32: aload_0
        //     33: aload_1
        //     34: invokevirtual 1881	android/server/BluetoothService:getRemoteUuids	(Ljava/lang/String;)[Landroid/os/ParcelUuid;
        //     37: astore_3
        //     38: new 257	java/util/ArrayList
        //     41: dup
        //     42: invokespecial 258	java/util/ArrayList:<init>	()V
        //     45: astore 4
        //     47: aload_0
        //     48: monitorenter
        //     49: aload_0
        //     50: getfield 262	android/server/BluetoothService:mUuidCallbackTracker	Ljava/util/HashMap;
        //     53: invokevirtual 524	java/util/HashMap:keySet	()Ljava/util/Set;
        //     56: invokeinterface 530 1 0
        //     61: astore 6
        //     63: aload 6
        //     65: invokeinterface 535 1 0
        //     70: ifeq +48 -> 118
        //     73: aload 6
        //     75: invokeinterface 539 1 0
        //     80: checkcast 15	android/server/BluetoothService$RemoteService
        //     83: astore 29
        //     85: aload 29
        //     87: getfield 661	android/server/BluetoothService$RemoteService:address	Ljava/lang/String;
        //     90: aload_1
        //     91: invokevirtual 665	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     94: ifeq -31 -> 63
        //     97: aload 4
        //     99: aload 29
        //     101: getfield 670	android/server/BluetoothService$RemoteService:uuid	Landroid/os/ParcelUuid;
        //     104: invokevirtual 982	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     107: pop
        //     108: goto -45 -> 63
        //     111: astore 5
        //     113: aload_0
        //     114: monitorexit
        //     115: aload 5
        //     117: athrow
        //     118: aload_0
        //     119: monitorexit
        //     120: new 168	java/util/HashMap
        //     123: dup
        //     124: invokespecial 169	java/util/HashMap:<init>	()V
        //     127: astore 7
        //     129: getstatic 159	android/server/BluetoothService:RFCOMM_UUIDS	[Landroid/os/ParcelUuid;
        //     132: astore 8
        //     134: aload 8
        //     136: arraylength
        //     137: istore 9
        //     139: iconst_0
        //     140: istore 10
        //     142: iload 10
        //     144: iload 9
        //     146: if_icmpge +87 -> 233
        //     149: aload 8
        //     151: iload 10
        //     153: aaload
        //     154: astore 25
        //     156: aload_3
        //     157: aload 25
        //     159: invokestatic 1069	android/bluetooth/BluetoothUuid:isUuidPresent	([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z
        //     162: ifeq +65 -> 227
        //     165: aload_0
        //     166: aload_1
        //     167: aload 25
        //     169: invokespecial 1883	android/server/BluetoothService:getDeviceServiceChannelForUuid	(Ljava/lang/String;Landroid/os/ParcelUuid;)I
        //     172: istore 26
        //     174: aload 7
        //     176: aload 25
        //     178: iload 26
        //     180: invokestatic 422	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     183: invokeinterface 1191 3 0
        //     188: pop
        //     189: ldc 66
        //     191: new 427	java/lang/StringBuilder
        //     194: dup
        //     195: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     198: ldc_w 1885
        //     201: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     204: aload 25
        //     206: invokevirtual 544	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     209: ldc_w 1632
        //     212: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     215: iload 26
        //     217: invokevirtual 443	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     220: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     223: invokestatic 388	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     226: pop
        //     227: iinc 10 1
        //     230: goto -88 -> 142
        //     233: aload 4
        //     235: invokevirtual 1886	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     238: astore 11
        //     240: aload 11
        //     242: invokeinterface 535 1 0
        //     247: ifeq +89 -> 336
        //     250: aload 11
        //     252: invokeinterface 539 1 0
        //     257: checkcast 145	android/os/ParcelUuid
        //     260: astore 21
        //     262: aload_3
        //     263: aload 21
        //     265: invokestatic 1069	android/bluetooth/BluetoothUuid:isUuidPresent	([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z
        //     268: ifeq -28 -> 240
        //     271: aload_0
        //     272: aload_1
        //     273: aload 21
        //     275: invokespecial 1883	android/server/BluetoothService:getDeviceServiceChannelForUuid	(Ljava/lang/String;Landroid/os/ParcelUuid;)I
        //     278: istore 22
        //     280: aload 7
        //     282: aload 21
        //     284: iload 22
        //     286: invokestatic 422	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     289: invokeinterface 1191 3 0
        //     294: pop
        //     295: ldc 66
        //     297: new 427	java/lang/StringBuilder
        //     300: dup
        //     301: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     304: ldc_w 1888
        //     307: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     310: aload 21
        //     312: invokevirtual 544	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     315: ldc_w 1632
        //     318: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     321: iload 22
        //     323: invokevirtual 443	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     326: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     329: invokestatic 388	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     332: pop
        //     333: goto -93 -> 240
        //     336: aload_0
        //     337: monitorenter
        //     338: aload_0
        //     339: getfield 262	android/server/BluetoothService:mUuidCallbackTracker	Ljava/util/HashMap;
        //     342: invokevirtual 524	java/util/HashMap:keySet	()Ljava/util/Set;
        //     345: invokeinterface 530 1 0
        //     350: astore 13
        //     352: aload 13
        //     354: invokeinterface 535 1 0
        //     359: ifeq +164 -> 523
        //     362: aload 13
        //     364: invokeinterface 539 1 0
        //     369: checkcast 15	android/server/BluetoothService$RemoteService
        //     372: astore 15
        //     374: aload 15
        //     376: getfield 661	android/server/BluetoothService$RemoteService:address	Ljava/lang/String;
        //     379: aload_1
        //     380: invokevirtual 665	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     383: ifeq -31 -> 352
        //     386: aload 7
        //     388: aload 15
        //     390: getfield 670	android/server/BluetoothService$RemoteService:uuid	Landroid/os/ParcelUuid;
        //     393: invokeinterface 1190 2 0
        //     398: ifeq -46 -> 352
        //     401: aload 7
        //     403: aload 15
        //     405: getfield 670	android/server/BluetoothService$RemoteService:uuid	Landroid/os/ParcelUuid;
        //     408: invokeinterface 672 2 0
        //     413: checkcast 418	java/lang/Integer
        //     416: invokevirtual 549	java/lang/Integer:intValue	()I
        //     419: istore 16
        //     421: ldc 66
        //     423: new 427	java/lang/StringBuilder
        //     426: dup
        //     427: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     430: ldc_w 1890
        //     433: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     436: aload 15
        //     438: getfield 670	android/server/BluetoothService$RemoteService:uuid	Landroid/os/ParcelUuid;
        //     441: invokevirtual 544	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     444: ldc_w 1892
        //     447: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     450: iload 16
        //     452: invokevirtual 443	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     455: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     458: invokestatic 388	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     461: pop
        //     462: aload_0
        //     463: getfield 262	android/server/BluetoothService:mUuidCallbackTracker	Ljava/util/HashMap;
        //     466: aload 15
        //     468: invokevirtual 398	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     471: checkcast 1634	android/bluetooth/IBluetoothCallback
        //     474: astore 18
        //     476: aload 18
        //     478: ifnull +12 -> 490
        //     481: aload 18
        //     483: iload 16
        //     485: invokeinterface 1637 2 0
        //     490: aload 13
        //     492: invokeinterface 1639 1 0
        //     497: goto -145 -> 352
        //     500: astore 12
        //     502: aload_0
        //     503: monitorexit
        //     504: aload 12
        //     506: athrow
        //     507: astore 19
        //     509: ldc 66
        //     511: ldc_w 1108
        //     514: aload 19
        //     516: invokestatic 1111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     519: pop
        //     520: goto -30 -> 490
        //     523: aload_0
        //     524: getfield 253	android/server/BluetoothService:mDeviceServiceChannelCache	Ljava/util/HashMap;
        //     527: aload_1
        //     528: aload 7
        //     530: invokevirtual 853	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     533: pop
        //     534: aload_0
        //     535: monitorexit
        //     536: return
        //
        // Exception table:
        //     from	to	target	type
        //     49	115	111	finally
        //     118	120	111	finally
        //     338	476	500	finally
        //     481	490	500	finally
        //     490	504	500	finally
        //     509	536	500	finally
        //     481	490	507	android/os/RemoteException
    }

    void updateRemoteDevicePropertiesCache(String paramString)
    {
        this.mDeviceProperties.updateCache(paramString);
    }

    // ERROR //
    public void writeIncomingConnectionState(String paramString, Pair<Integer, String> paramPair)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 321	android/server/BluetoothService:mIncomingConnections	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 321	android/server/BluetoothService:mIncomingConnections	Ljava/util/HashMap;
        //     11: aload_1
        //     12: aload_2
        //     13: invokevirtual 853	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     16: pop
        //     17: aload_0
        //     18: invokespecial 1897	android/server/BluetoothService:truncateIncomingConnectionFile	()V
        //     21: aconst_null
        //     22: astore 6
        //     24: new 427	java/lang/StringBuilder
        //     27: dup
        //     28: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     31: astore 7
        //     33: new 1020	java/io/BufferedWriter
        //     36: dup
        //     37: new 1022	java/io/FileWriter
        //     40: dup
        //     41: ldc 38
        //     43: iconst_1
        //     44: invokespecial 1899	java/io/FileWriter:<init>	(Ljava/lang/String;Z)V
        //     47: invokespecial 1026	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
        //     50: astore 8
        //     52: aload_0
        //     53: getfield 321	android/server/BluetoothService:mIncomingConnections	Ljava/util/HashMap;
        //     56: invokevirtual 524	java/util/HashMap:keySet	()Ljava/util/Set;
        //     59: invokeinterface 530 1 0
        //     64: astore 14
        //     66: aload 14
        //     68: invokeinterface 535 1 0
        //     73: ifeq +144 -> 217
        //     76: aload 14
        //     78: invokeinterface 539 1 0
        //     83: checkcast 474	java/lang/String
        //     86: astore 16
        //     88: aload_0
        //     89: getfield 321	android/server/BluetoothService:mIncomingConnections	Ljava/util/HashMap;
        //     92: aload 16
        //     94: invokevirtual 398	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     97: checkcast 843	android/util/Pair
        //     100: astore 17
        //     102: aload 7
        //     104: aload 16
        //     106: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     109: pop
        //     110: aload 7
        //     112: ldc_w 472
        //     115: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     118: pop
        //     119: aload 7
        //     121: aload 17
        //     123: getfield 960	android/util/Pair:first	Ljava/lang/Object;
        //     126: checkcast 418	java/lang/Integer
        //     129: invokevirtual 1900	java/lang/Integer:toString	()Ljava/lang/String;
        //     132: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     135: pop
        //     136: aload 7
        //     138: ldc_w 472
        //     141: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     144: pop
        //     145: aload 7
        //     147: aload 17
        //     149: getfield 963	android/util/Pair:second	Ljava/lang/Object;
        //     152: checkcast 474	java/lang/String
        //     155: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     158: pop
        //     159: aload 7
        //     161: ldc_w 1902
        //     164: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     167: pop
        //     168: goto -102 -> 66
        //     171: astore 13
        //     173: aload 8
        //     175: astore 6
        //     177: new 427	java/lang/StringBuilder
        //     180: dup
        //     181: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     184: ldc_w 1904
        //     187: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     190: aload 13
        //     192: invokevirtual 856	java/io/FileNotFoundException:toString	()Ljava/lang/String;
        //     195: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     198: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     201: invokestatic 858	android/server/BluetoothService:log	(Ljava/lang/String;)V
        //     204: aload 6
        //     206: ifnull +8 -> 214
        //     209: aload 6
        //     211: invokevirtual 1048	java/io/BufferedWriter:close	()V
        //     214: aload_3
        //     215: monitorexit
        //     216: return
        //     217: aload 8
        //     219: aload 7
        //     221: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     224: invokevirtual 1047	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     227: aload 8
        //     229: ifnull +8 -> 237
        //     232: aload 8
        //     234: invokevirtual 1048	java/io/BufferedWriter:close	()V
        //     237: goto -23 -> 214
        //     240: astore 9
        //     242: new 427	java/lang/StringBuilder
        //     245: dup
        //     246: invokespecial 428	java/lang/StringBuilder:<init>	()V
        //     249: ldc_w 1906
        //     252: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     255: aload 9
        //     257: invokevirtual 862	java/io/IOException:toString	()Ljava/lang/String;
        //     260: invokevirtual 434	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     263: invokevirtual 446	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     266: invokestatic 858	android/server/BluetoothService:log	(Ljava/lang/String;)V
        //     269: aload 6
        //     271: ifnull -57 -> 214
        //     274: aload 6
        //     276: invokevirtual 1048	java/io/BufferedWriter:close	()V
        //     279: goto -65 -> 214
        //     282: astore 12
        //     284: goto -70 -> 214
        //     287: astore 10
        //     289: aload 6
        //     291: ifnull +8 -> 299
        //     294: aload 6
        //     296: invokevirtual 1048	java/io/BufferedWriter:close	()V
        //     299: aload 10
        //     301: athrow
        //     302: astore 4
        //     304: aload_3
        //     305: monitorexit
        //     306: aload 4
        //     308: athrow
        //     309: astore 11
        //     311: goto -12 -> 299
        //     314: astore 15
        //     316: goto -79 -> 237
        //     319: astore 10
        //     321: aload 8
        //     323: astore 6
        //     325: goto -36 -> 289
        //     328: astore 9
        //     330: aload 8
        //     332: astore 6
        //     334: goto -92 -> 242
        //     337: astore 13
        //     339: goto -162 -> 177
        //
        // Exception table:
        //     from	to	target	type
        //     52	168	171	java/io/FileNotFoundException
        //     217	227	171	java/io/FileNotFoundException
        //     33	52	240	java/io/IOException
        //     209	214	282	java/io/IOException
        //     274	279	282	java/io/IOException
        //     33	52	287	finally
        //     177	204	287	finally
        //     242	269	287	finally
        //     7	33	302	finally
        //     209	214	302	finally
        //     214	216	302	finally
        //     232	237	302	finally
        //     274	279	302	finally
        //     294	299	302	finally
        //     299	306	302	finally
        //     294	299	309	java/io/IOException
        //     232	237	314	java/io/IOException
        //     52	168	319	finally
        //     217	227	319	finally
        //     52	168	328	java/io/IOException
        //     217	227	328	java/io/IOException
        //     33	52	337	java/io/FileNotFoundException
    }

    private class Reaper
        implements IBinder.DeathRecipient
    {
        int mHandle;
        int mPid;
        int mType;

        Reaper(int paramInt1, int arg3)
        {
            this.mPid = paramInt1;
            int i;
            this.mType = i;
        }

        Reaper(int paramInt1, int paramInt2, int arg4)
        {
            this.mPid = paramInt2;
            this.mHandle = paramInt1;
            int i;
            this.mType = i;
        }

        public void binderDied()
        {
            synchronized (BluetoothService.this)
            {
                Log.d("BluetoothService", "Tracked app " + this.mPid + " died" + "Type:" + this.mType);
                if (this.mType == 10)
                    BluetoothService.this.checkAndRemoveRecord(this.mHandle, this.mPid);
                while (this.mType != 11)
                    return;
                BluetoothService.this.mStateChangeTracker.remove(Integer.valueOf(this.mPid));
            }
        }
    }

    private static class RemoteService
    {
        public String address;
        public ParcelUuid uuid;

        public RemoteService(String paramString, ParcelUuid paramParcelUuid)
        {
            this.address = paramString;
            this.uuid = paramParcelUuid;
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = false;
            if ((paramObject instanceof RemoteService))
            {
                RemoteService localRemoteService = (RemoteService)paramObject;
                if ((this.address.equals(localRemoteService.address)) && (this.uuid.equals(localRemoteService.uuid)))
                    bool = true;
            }
            return bool;
        }

        public int hashCode()
        {
            int i = 0;
            int j;
            int k;
            if (this.address == null)
            {
                j = 0;
                k = 31 * (j + 31);
                if (this.uuid != null)
                    break label41;
            }
            while (true)
            {
                return k + i;
                j = this.address.hashCode();
                break;
                label41: i = this.uuid.hashCode();
            }
        }
    }

    private static class ServiceRecordClient
    {
        IBinder binder;
        IBinder.DeathRecipient death;
        int pid;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.server.BluetoothService
 * JD-Core Version:        0.6.2
 */