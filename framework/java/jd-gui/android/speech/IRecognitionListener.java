package android.speech;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IRecognitionListener extends IInterface
{
    public abstract void onBeginningOfSpeech()
        throws RemoteException;

    public abstract void onBufferReceived(byte[] paramArrayOfByte)
        throws RemoteException;

    public abstract void onEndOfSpeech()
        throws RemoteException;

    public abstract void onError(int paramInt)
        throws RemoteException;

    public abstract void onEvent(int paramInt, Bundle paramBundle)
        throws RemoteException;

    public abstract void onPartialResults(Bundle paramBundle)
        throws RemoteException;

    public abstract void onReadyForSpeech(Bundle paramBundle)
        throws RemoteException;

    public abstract void onResults(Bundle paramBundle)
        throws RemoteException;

    public abstract void onRmsChanged(float paramFloat)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRecognitionListener
    {
        private static final String DESCRIPTOR = "android.speech.IRecognitionListener";
        static final int TRANSACTION_onBeginningOfSpeech = 2;
        static final int TRANSACTION_onBufferReceived = 4;
        static final int TRANSACTION_onEndOfSpeech = 5;
        static final int TRANSACTION_onError = 6;
        static final int TRANSACTION_onEvent = 9;
        static final int TRANSACTION_onPartialResults = 8;
        static final int TRANSACTION_onReadyForSpeech = 1;
        static final int TRANSACTION_onResults = 7;
        static final int TRANSACTION_onRmsChanged = 3;

        public Stub()
        {
            attachInterface(this, "android.speech.IRecognitionListener");
        }

        public static IRecognitionListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.speech.IRecognitionListener");
                if ((localIInterface != null) && ((localIInterface instanceof IRecognitionListener)))
                    localObject = (IRecognitionListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.speech.IRecognitionListener");
                    continue;
                    paramParcel1.enforceInterface("android.speech.IRecognitionListener");
                    if (paramParcel1.readInt() != 0);
                    for (Bundle localBundle4 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle4 = null)
                    {
                        onReadyForSpeech(localBundle4);
                        break;
                    }
                    paramParcel1.enforceInterface("android.speech.IRecognitionListener");
                    onBeginningOfSpeech();
                    continue;
                    paramParcel1.enforceInterface("android.speech.IRecognitionListener");
                    onRmsChanged(paramParcel1.readFloat());
                    continue;
                    paramParcel1.enforceInterface("android.speech.IRecognitionListener");
                    onBufferReceived(paramParcel1.createByteArray());
                    continue;
                    paramParcel1.enforceInterface("android.speech.IRecognitionListener");
                    onEndOfSpeech();
                    continue;
                    paramParcel1.enforceInterface("android.speech.IRecognitionListener");
                    onError(paramParcel1.readInt());
                }
            case 7:
                paramParcel1.enforceInterface("android.speech.IRecognitionListener");
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle3 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle3 = null)
                {
                    onResults(localBundle3);
                    break;
                }
            case 8:
                paramParcel1.enforceInterface("android.speech.IRecognitionListener");
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle2 = null)
                {
                    onPartialResults(localBundle2);
                    break;
                }
            case 9:
            }
            paramParcel1.enforceInterface("android.speech.IRecognitionListener");
            int i = paramParcel1.readInt();
            if (paramParcel1.readInt() != 0);
            for (Bundle localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle1 = null)
            {
                onEvent(i, localBundle1);
                break;
            }
        }

        private static class Proxy
            implements IRecognitionListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.speech.IRecognitionListener";
            }

            public void onBeginningOfSpeech()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onBufferReceived(byte[] paramArrayOfByte)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    localParcel.writeByteArray(paramArrayOfByte);
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onEndOfSpeech()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    this.mRemote.transact(5, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onError(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(6, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onEvent(int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    localParcel.writeInt(paramInt);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onPartialResults(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(8, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onReadyForSpeech(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onResults(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(7, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onRmsChanged(float paramFloat)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionListener");
                    localParcel.writeFloat(paramFloat);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.IRecognitionListener
 * JD-Core Version:        0.6.2
 */