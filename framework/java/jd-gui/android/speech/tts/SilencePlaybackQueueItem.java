package android.speech.tts;

import android.os.ConditionVariable;

class SilencePlaybackQueueItem extends PlaybackQueueItem
{
    private final ConditionVariable mCondVar = new ConditionVariable();
    private final long mSilenceDurationMs;

    SilencePlaybackQueueItem(TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject, long paramLong)
    {
        super(paramUtteranceProgressDispatcher, paramObject);
        this.mSilenceDurationMs = paramLong;
    }

    public void run()
    {
        getDispatcher().dispatchOnStart();
        if (this.mSilenceDurationMs > 0L)
            this.mCondVar.block(this.mSilenceDurationMs);
        getDispatcher().dispatchOnDone();
    }

    void stop(boolean paramBoolean)
    {
        this.mCondVar.open();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.SilencePlaybackQueueItem
 * JD-Core Version:        0.6.2
 */