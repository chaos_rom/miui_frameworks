package android.speech.tts;

import android.util.EventLog;

public class EventLogTags
{
    public static final int TTS_SPEAK_FAILURE = 76002;
    public static final int TTS_SPEAK_SUCCESS = 76001;

    public static void writeTtsSpeakFailure(String paramString1, int paramInt1, int paramInt2, int paramInt3, String paramString2, int paramInt4, int paramInt5)
    {
        Object[] arrayOfObject = new Object[7];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = Integer.valueOf(paramInt3);
        arrayOfObject[4] = paramString2;
        arrayOfObject[5] = Integer.valueOf(paramInt4);
        arrayOfObject[6] = Integer.valueOf(paramInt5);
        EventLog.writeEvent(76002, arrayOfObject);
    }

    public static void writeTtsSpeakSuccess(String paramString1, int paramInt1, int paramInt2, int paramInt3, String paramString2, int paramInt4, int paramInt5, long paramLong1, long paramLong2, long paramLong3)
    {
        Object[] arrayOfObject = new Object[10];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = Integer.valueOf(paramInt3);
        arrayOfObject[4] = paramString2;
        arrayOfObject[5] = Integer.valueOf(paramInt4);
        arrayOfObject[6] = Integer.valueOf(paramInt5);
        arrayOfObject[7] = Long.valueOf(paramLong1);
        arrayOfObject[8] = Long.valueOf(paramLong2);
        arrayOfObject[9] = Long.valueOf(paramLong3);
        EventLog.writeEvent(76001, arrayOfObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.EventLogTags
 * JD-Core Version:        0.6.2
 */