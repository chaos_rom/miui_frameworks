package android.speech.tts;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import com.android.internal.R.styleable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.xmlpull.v1.XmlPullParserException;

public class TtsEngines
{
    private static final boolean DBG = false;
    private static final String LOCALE_DELIMITER = "-";
    private static final String TAG = "TtsEngines";
    private static final String XML_TAG_NAME = "tts-engine";
    private final Context mContext;

    public TtsEngines(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private String getDefaultLocale()
    {
        Locale localLocale = Locale.getDefault();
        String str1 = localLocale.getISO3Language();
        Object localObject;
        if (TextUtils.isEmpty(str1))
        {
            Log.w("TtsEngines", "Default locale is empty.");
            localObject = "";
        }
        while (true)
        {
            return localObject;
            if (!TextUtils.isEmpty(localLocale.getISO3Country()))
            {
                String str2 = str1 + "-" + localLocale.getISO3Country();
                if (!TextUtils.isEmpty(localLocale.getVariant()))
                    str2 = str2 + "-" + localLocale.getVariant();
                localObject = str2;
            }
            else
            {
                localObject = str1;
            }
        }
    }

    private TextToSpeech.EngineInfo getEngineInfo(ResolveInfo paramResolveInfo, PackageManager paramPackageManager)
    {
        ServiceInfo localServiceInfo = paramResolveInfo.serviceInfo;
        TextToSpeech.EngineInfo localEngineInfo;
        CharSequence localCharSequence;
        String str;
        if (localServiceInfo != null)
        {
            localEngineInfo = new TextToSpeech.EngineInfo();
            localEngineInfo.name = localServiceInfo.packageName;
            localCharSequence = localServiceInfo.loadLabel(paramPackageManager);
            if (TextUtils.isEmpty(localCharSequence))
            {
                str = localEngineInfo.name;
                localEngineInfo.label = str;
                localEngineInfo.icon = localServiceInfo.getIconResource();
                localEngineInfo.priority = paramResolveInfo.priority;
                localEngineInfo.system = isSystemEngine(localServiceInfo);
            }
        }
        while (true)
        {
            return localEngineInfo;
            str = localCharSequence.toString();
            break;
            localEngineInfo = null;
        }
    }

    private String getV1Locale()
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        String str1 = Settings.Secure.getString(localContentResolver, "tts_default_lang");
        String str2 = Settings.Secure.getString(localContentResolver, "tts_default_country");
        String str3 = Settings.Secure.getString(localContentResolver, "tts_default_variant");
        String str4;
        if (TextUtils.isEmpty(str1))
            str4 = getDefaultLocale();
        while (true)
        {
            return str4;
            str4 = str1;
            if (!TextUtils.isEmpty(str2))
            {
                str4 = str4 + "-" + str2;
                if (!TextUtils.isEmpty(str3))
                    str4 = str4 + "-" + str3;
            }
        }
    }

    private boolean isSystemEngine(ServiceInfo paramServiceInfo)
    {
        ApplicationInfo localApplicationInfo = paramServiceInfo.applicationInfo;
        if ((localApplicationInfo != null) && ((0x1 & localApplicationInfo.flags) != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static String parseEnginePrefFromList(String paramString1, String paramString2)
    {
        String str1 = null;
        if (TextUtils.isEmpty(paramString1));
        label84: 
        while (true)
        {
            return str1;
            String[] arrayOfString = paramString1.split(",");
            int i = arrayOfString.length;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label84;
                String str2 = arrayOfString[j];
                int k = str2.indexOf(':');
                if ((k > 0) && (paramString2.equals(str2.substring(0, k))))
                {
                    str1 = str2.substring(k + 1);
                    break;
                }
            }
        }
    }

    public static String[] parseLocalePref(String paramString)
    {
        String[] arrayOfString1 = new String[3];
        arrayOfString1[0] = "";
        arrayOfString1[1] = "";
        arrayOfString1[2] = "";
        if (!TextUtils.isEmpty(paramString))
        {
            String[] arrayOfString2 = paramString.split("-");
            System.arraycopy(arrayOfString2, 0, arrayOfString1, 0, arrayOfString2.length);
        }
        return arrayOfString1;
    }

    private String settingsActivityFromServiceInfo(ServiceInfo paramServiceInfo, PackageManager paramPackageManager)
    {
        XmlResourceParser localXmlResourceParser = null;
        try
        {
            localXmlResourceParser = paramServiceInfo.loadXmlMetaData(paramPackageManager, "android.speech.tts");
            if (localXmlResourceParser == null)
            {
                Log.w("TtsEngines", "No meta-data found for :" + paramServiceInfo);
                if (localXmlResourceParser != null)
                    localXmlResourceParser.close();
                str = null;
            }
            while (true)
            {
                return str;
                Resources localResources = paramPackageManager.getResourcesForApplication(paramServiceInfo.applicationInfo);
                int i;
                do
                {
                    i = localXmlResourceParser.next();
                    if (i == 1)
                        break;
                }
                while (i != 2);
                if (!"tts-engine".equals(localXmlResourceParser.getName()))
                {
                    Log.w("TtsEngines", "Package " + paramServiceInfo + " uses unknown tag :" + localXmlResourceParser.getName());
                    if (localXmlResourceParser != null)
                        localXmlResourceParser.close();
                    str = null;
                }
                else
                {
                    TypedArray localTypedArray = localResources.obtainAttributes(Xml.asAttributeSet(localXmlResourceParser), R.styleable.TextToSpeechEngine);
                    str = localTypedArray.getString(0);
                    localTypedArray.recycle();
                    if (localXmlResourceParser != null)
                    {
                        localXmlResourceParser.close();
                        continue;
                        if (localXmlResourceParser != null)
                            localXmlResourceParser.close();
                        str = null;
                    }
                }
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
            {
                Log.w("TtsEngines", "Could not load resources for : " + paramServiceInfo);
                if (localXmlResourceParser != null)
                    localXmlResourceParser.close();
                str = null;
            }
        }
        catch (XmlPullParserException localXmlPullParserException)
        {
            while (true)
            {
                Log.w("TtsEngines", "Error parsing metadata for " + paramServiceInfo + ":" + localXmlPullParserException);
                if (localXmlResourceParser != null)
                    localXmlResourceParser.close();
                str = null;
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Log.w("TtsEngines", "Error parsing metadata for " + paramServiceInfo + ":" + localIOException);
                if (localXmlResourceParser != null)
                    localXmlResourceParser.close();
                String str = null;
            }
        }
        finally
        {
            if (localXmlResourceParser != null)
                localXmlResourceParser.close();
        }
    }

    private String updateValueInCommaSeparatedList(String paramString1, String paramString2, String paramString3)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if (TextUtils.isEmpty(paramString1))
            localStringBuilder.append(paramString2).append(':').append(paramString3);
        while (true)
        {
            return localStringBuilder.toString();
            String[] arrayOfString = paramString1.split(",");
            int i = 1;
            int j = 0;
            int k = arrayOfString.length;
            int m = 0;
            if (m < k)
            {
                String str = arrayOfString[m];
                int n = str.indexOf(':');
                if (n > 0)
                {
                    if (!paramString2.equals(str.substring(0, n)))
                        break label147;
                    if (i == 0)
                        break label136;
                    i = 0;
                }
                while (true)
                {
                    j = 1;
                    localStringBuilder.append(paramString2).append(':').append(paramString3);
                    m++;
                    break;
                    label136: localStringBuilder.append(',');
                }
                label147: if (i != 0)
                    i = 0;
                while (true)
                {
                    localStringBuilder.append(str);
                    break;
                    localStringBuilder.append(',');
                }
            }
            if (j == 0)
            {
                localStringBuilder.append(',');
                localStringBuilder.append(paramString2).append(':').append(paramString3);
            }
        }
    }

    public String getDefaultEngine()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "tts_default_synth");
        if (isEngineInstalled(str));
        while (true)
        {
            return str;
            str = getHighestRankedEngineName();
        }
    }

    public TextToSpeech.EngineInfo getEngineInfo(String paramString)
    {
        PackageManager localPackageManager = this.mContext.getPackageManager();
        Intent localIntent = new Intent("android.intent.action.TTS_SERVICE");
        localIntent.setPackage(paramString);
        List localList = localPackageManager.queryIntentServices(localIntent, 65536);
        if ((localList != null) && (localList.size() == 1));
        for (TextToSpeech.EngineInfo localEngineInfo = getEngineInfo((ResolveInfo)localList.get(0), localPackageManager); ; localEngineInfo = null)
            return localEngineInfo;
    }

    public List<TextToSpeech.EngineInfo> getEngines()
    {
        PackageManager localPackageManager = this.mContext.getPackageManager();
        List localList = localPackageManager.queryIntentServices(new Intent("android.intent.action.TTS_SERVICE"), 65536);
        Object localObject;
        if (localList == null)
            localObject = Collections.emptyList();
        while (true)
        {
            return localObject;
            localObject = new ArrayList(localList.size());
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
            {
                TextToSpeech.EngineInfo localEngineInfo = getEngineInfo((ResolveInfo)localIterator.next(), localPackageManager);
                if (localEngineInfo != null)
                    ((List)localObject).add(localEngineInfo);
            }
            Collections.sort((List)localObject, EngineInfoComparator.INSTANCE);
        }
    }

    public String getHighestRankedEngineName()
    {
        List localList = getEngines();
        if ((localList.size() > 0) && (((TextToSpeech.EngineInfo)localList.get(0)).system));
        for (String str = ((TextToSpeech.EngineInfo)localList.get(0)).name; ; str = null)
            return str;
    }

    public String getLocalePrefForEngine(String paramString)
    {
        String str = parseEnginePrefFromList(Settings.Secure.getString(this.mContext.getContentResolver(), "tts_default_locale"), paramString);
        if (TextUtils.isEmpty(str))
            str = getV1Locale();
        return str;
    }

    public Intent getSettingsIntent(String paramString)
    {
        PackageManager localPackageManager = this.mContext.getPackageManager();
        Intent localIntent1 = new Intent("android.intent.action.TTS_SERVICE");
        localIntent1.setPackage(paramString);
        List localList = localPackageManager.queryIntentServices(localIntent1, 65664);
        Intent localIntent2;
        if ((localList != null) && (localList.size() == 1))
        {
            ServiceInfo localServiceInfo = ((ResolveInfo)localList.get(0)).serviceInfo;
            if (localServiceInfo != null)
            {
                String str = settingsActivityFromServiceInfo(localServiceInfo, localPackageManager);
                if (str != null)
                {
                    localIntent2 = new Intent();
                    localIntent2.setClassName(paramString, str);
                }
            }
        }
        while (true)
        {
            return localIntent2;
            localIntent2 = null;
        }
    }

    public boolean isEngineInstalled(String paramString)
    {
        boolean bool = false;
        if (paramString == null);
        while (true)
        {
            return bool;
            if (getEngineInfo(paramString) != null)
                bool = true;
        }
    }

    /** @deprecated */
    public void updateLocalePrefForEngine(String paramString1, String paramString2)
    {
        try
        {
            String str = updateValueInCommaSeparatedList(Settings.Secure.getString(this.mContext.getContentResolver(), "tts_default_locale"), paramString1, paramString2);
            Settings.Secure.putString(this.mContext.getContentResolver(), "tts_default_locale", str.toString());
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private static class EngineInfoComparator
        implements Comparator<TextToSpeech.EngineInfo>
    {
        static EngineInfoComparator INSTANCE = new EngineInfoComparator();

        public int compare(TextToSpeech.EngineInfo paramEngineInfo1, TextToSpeech.EngineInfo paramEngineInfo2)
        {
            int i;
            if ((paramEngineInfo1.system) && (!paramEngineInfo2.system))
                i = -1;
            while (true)
            {
                return i;
                if ((paramEngineInfo2.system) && (!paramEngineInfo1.system))
                    i = 1;
                else
                    i = paramEngineInfo2.priority - paramEngineInfo1.priority;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.TtsEngines
 * JD-Core Version:        0.6.2
 */