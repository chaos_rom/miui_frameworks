package android.speech.tts;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ITextToSpeechService extends IInterface
{
    public abstract String[] getFeaturesForLanguage(String paramString1, String paramString2, String paramString3)
        throws RemoteException;

    public abstract String[] getLanguage()
        throws RemoteException;

    public abstract int isLanguageAvailable(String paramString1, String paramString2, String paramString3)
        throws RemoteException;

    public abstract boolean isSpeaking()
        throws RemoteException;

    public abstract int loadLanguage(String paramString1, String paramString2, String paramString3)
        throws RemoteException;

    public abstract int playAudio(IBinder paramIBinder, Uri paramUri, int paramInt, Bundle paramBundle)
        throws RemoteException;

    public abstract int playSilence(IBinder paramIBinder, long paramLong, int paramInt, Bundle paramBundle)
        throws RemoteException;

    public abstract void setCallback(IBinder paramIBinder, ITextToSpeechCallback paramITextToSpeechCallback)
        throws RemoteException;

    public abstract int speak(IBinder paramIBinder, String paramString, int paramInt, Bundle paramBundle)
        throws RemoteException;

    public abstract int stop(IBinder paramIBinder)
        throws RemoteException;

    public abstract int synthesizeToFile(IBinder paramIBinder, String paramString1, String paramString2, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ITextToSpeechService
    {
        private static final String DESCRIPTOR = "android.speech.tts.ITextToSpeechService";
        static final int TRANSACTION_getFeaturesForLanguage = 9;
        static final int TRANSACTION_getLanguage = 7;
        static final int TRANSACTION_isLanguageAvailable = 8;
        static final int TRANSACTION_isSpeaking = 5;
        static final int TRANSACTION_loadLanguage = 10;
        static final int TRANSACTION_playAudio = 3;
        static final int TRANSACTION_playSilence = 4;
        static final int TRANSACTION_setCallback = 11;
        static final int TRANSACTION_speak = 1;
        static final int TRANSACTION_stop = 6;
        static final int TRANSACTION_synthesizeToFile = 2;

        public Stub()
        {
            attachInterface(this, "android.speech.tts.ITextToSpeechService");
        }

        public static ITextToSpeechService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.speech.tts.ITextToSpeechService");
                if ((localIInterface != null) && ((localIInterface instanceof ITextToSpeechService)))
                    localObject = (ITextToSpeechService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.speech.tts.ITextToSpeechService");
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                IBinder localIBinder4 = paramParcel1.readStrongBinder();
                String str3 = paramParcel1.readString();
                int i7 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle4 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle4 = null)
                {
                    int i8 = speak(localIBinder4, str3, i7, localBundle4);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i8);
                    break;
                }
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                IBinder localIBinder3 = paramParcel1.readStrongBinder();
                String str1 = paramParcel1.readString();
                String str2 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle3 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle3 = null)
                {
                    int i6 = synthesizeToFile(localIBinder3, str1, str2, localBundle3);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i6);
                    break;
                }
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                IBinder localIBinder2 = paramParcel1.readStrongBinder();
                Uri localUri;
                label324: int i4;
                if (paramParcel1.readInt() != 0)
                {
                    localUri = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                    i4 = paramParcel1.readInt();
                    if (paramParcel1.readInt() == 0)
                        break label384;
                }
                label384: for (Bundle localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle2 = null)
                {
                    int i5 = playAudio(localIBinder2, localUri, i4, localBundle2);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i5);
                    break;
                    localUri = null;
                    break label324;
                }
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                IBinder localIBinder1 = paramParcel1.readStrongBinder();
                long l = paramParcel1.readLong();
                int i2 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle1 = null)
                {
                    int i3 = playSilence(localIBinder1, l, i2, localBundle1);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i3);
                    break;
                }
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                boolean bool = isSpeaking();
                paramParcel2.writeNoException();
                if (bool);
                int i1;
                for (int n = i; ; i1 = 0)
                {
                    paramParcel2.writeInt(n);
                    break;
                }
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                int m = stop(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(m);
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                String[] arrayOfString2 = getLanguage();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString2);
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                int k = isLanguageAvailable(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(k);
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                String[] arrayOfString1 = getFeaturesForLanguage(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString1);
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                int j = loadLanguage(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(j);
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechService");
                setCallback(paramParcel1.readStrongBinder(), ITextToSpeechCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements ITextToSpeechService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String[] getFeaturesForLanguage(String paramString1, String paramString2, String paramString3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.speech.tts.ITextToSpeechService";
            }

            public String[] getLanguage()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int isLanguageAvailable(String paramString1, String paramString2, String paramString3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isSpeaking()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int loadLanguage(String paramString1, String paramString2, String paramString3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int playAudio(IBinder paramIBinder, Uri paramUri, int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                        localParcel1.writeStrongBinder(paramIBinder);
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            if (paramBundle != null)
                            {
                                localParcel1.writeInt(1);
                                paramBundle.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(3, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                return i;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public int playSilence(IBinder paramIBinder, long paramLong, int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeLong(paramLong);
                    localParcel1.writeInt(paramInt);
                    if (paramBundle != null)
                    {
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setCallback(IBinder paramIBinder, ITextToSpeechCallback paramITextToSpeechCallback)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramITextToSpeechCallback != null)
                    {
                        localIBinder = paramITextToSpeechCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int speak(IBinder paramIBinder, String paramString, int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    if (paramBundle != null)
                    {
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int stop(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int synthesizeToFile(IBinder paramIBinder, String paramString1, String paramString2, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.speech.tts.ITextToSpeechService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    if (paramBundle != null)
                    {
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.ITextToSpeechService
 * JD-Core Version:        0.6.2
 */