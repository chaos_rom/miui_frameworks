package android.speech.tts;

import android.media.AudioTrack;
import android.util.Log;

class BlockingAudioTrack
{
    private static final boolean DBG = false;
    private static final long MAX_PROGRESS_WAIT_MS = 2500L;
    private static final long MAX_SLEEP_TIME_MS = 2500L;
    private static final int MIN_AUDIO_BUFFER_SIZE = 8192;
    private static final long MIN_SLEEP_TIME_MS = 20L;
    private static final String TAG = "TTS.BlockingAudioTrack";
    private int mAudioBufferSize;
    private final int mAudioFormat;
    private AudioTrack mAudioTrack;
    private final Object mAudioTrackLock = new Object();
    private final int mBytesPerFrame;
    private int mBytesWritten = 0;
    private final int mChannelCount;
    private boolean mIsShortUtterance;
    private final float mPan;
    private final int mSampleRateInHz;
    private volatile boolean mStopped;
    private final int mStreamType;
    private final float mVolume;

    BlockingAudioTrack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2)
    {
        this.mStreamType = paramInt1;
        this.mSampleRateInHz = paramInt2;
        this.mAudioFormat = paramInt3;
        this.mChannelCount = paramInt4;
        this.mVolume = paramFloat1;
        this.mPan = paramFloat2;
        this.mBytesPerFrame = (getBytesPerFrame(this.mAudioFormat) * this.mChannelCount);
        this.mIsShortUtterance = false;
        this.mAudioBufferSize = 0;
        this.mBytesWritten = 0;
        this.mAudioTrack = null;
        this.mStopped = false;
    }

    private void blockUntilCompletion(AudioTrack paramAudioTrack)
    {
        int i = this.mBytesWritten / this.mBytesPerFrame;
        int j = -1;
        long l1 = 0L;
        int k = paramAudioTrack.getPlaybackHeadPosition();
        long l2;
        if ((k < i) && (paramAudioTrack.getPlayState() == 3) && (!this.mStopped))
        {
            l2 = clip(1000 * (i - k) / paramAudioTrack.getSampleRate(), 20L, 2500L);
            if (k != j)
                break label99;
            l1 += l2;
            if (l1 <= 2500L)
                break label102;
            Log.w("TTS.BlockingAudioTrack", "Waited unsuccessfully for 2500ms for AudioTrack to make progress, Aborting");
        }
        while (true)
            while (true)
            {
                return;
                label99: l1 = 0L;
                label102: j = k;
                try
                {
                    Thread.sleep(l2);
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
    }

    private void blockUntilDone(AudioTrack paramAudioTrack)
    {
        if (this.mBytesWritten <= 0);
        while (true)
        {
            return;
            if (this.mIsShortUtterance)
                blockUntilEstimatedCompletion();
            else
                blockUntilCompletion(paramAudioTrack);
        }
    }

    private void blockUntilEstimatedCompletion()
    {
        long l = 1000 * (this.mBytesWritten / this.mBytesPerFrame) / this.mSampleRateInHz;
        try
        {
            Thread.sleep(l);
            label24: return;
        }
        catch (InterruptedException localInterruptedException)
        {
            break label24;
        }
    }

    private static float clip(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        if (paramFloat1 > paramFloat3);
        while (true)
        {
            return paramFloat3;
            if (paramFloat1 < paramFloat2)
                paramFloat3 = paramFloat2;
            else
                paramFloat3 = paramFloat1;
        }
    }

    private static final long clip(long paramLong1, long paramLong2, long paramLong3)
    {
        if (paramLong1 < paramLong2);
        while (true)
        {
            return paramLong2;
            if (paramLong1 > paramLong3)
                paramLong2 = paramLong3;
            else
                paramLong2 = paramLong1;
        }
    }

    private AudioTrack createStreamingAudioTrack()
    {
        int i = getChannelConfig(this.mChannelCount);
        int j = Math.max(8192, AudioTrack.getMinBufferSize(this.mSampleRateInHz, i, this.mAudioFormat));
        AudioTrack localAudioTrack = new AudioTrack(this.mStreamType, this.mSampleRateInHz, i, this.mAudioFormat, j, 1);
        if (localAudioTrack.getState() != 1)
        {
            Log.w("TTS.BlockingAudioTrack", "Unable to create audio track.");
            localAudioTrack.release();
            localAudioTrack = null;
        }
        while (true)
        {
            return localAudioTrack;
            this.mAudioBufferSize = j;
            setupVolume(localAudioTrack, this.mVolume, this.mPan);
        }
    }

    private static int getBytesPerFrame(int paramInt)
    {
        int i = 2;
        if (paramInt == 3)
            i = 1;
        while (true)
        {
            return i;
            if (paramInt != i)
                i = -1;
        }
    }

    static int getChannelConfig(int paramInt)
    {
        int i;
        if (paramInt == 1)
            i = 4;
        while (true)
        {
            return i;
            if (paramInt == 2)
                i = 12;
            else
                i = 0;
        }
    }

    private static void setupVolume(AudioTrack paramAudioTrack, float paramFloat1, float paramFloat2)
    {
        float f1 = clip(paramFloat1, 0.0F, 1.0F);
        float f2 = clip(paramFloat2, -1.0F, 1.0F);
        float f3 = f1;
        float f4 = f1;
        if (f2 > 0.0F)
            f3 *= (1.0F - f2);
        while (true)
        {
            if (paramAudioTrack.setStereoVolume(f3, f4) != 0)
                Log.e("TTS.BlockingAudioTrack", "Failed to set volume");
            return;
            if (f2 < 0.0F)
                f4 *= (1.0F + f2);
        }
    }

    private static int writeToAudioTrack(AudioTrack paramAudioTrack, byte[] paramArrayOfByte)
    {
        if (paramAudioTrack.getPlayState() != 3)
            paramAudioTrack.play();
        int i = 0;
        while (true)
        {
            int j;
            if (i < paramArrayOfByte.length)
            {
                j = paramAudioTrack.write(paramArrayOfByte, i, paramArrayOfByte.length);
                if (j > 0);
            }
            else
            {
                return i;
            }
            i += j;
        }
    }

    long getAudioLengthMs(int paramInt)
    {
        return 1000 * (paramInt / this.mBytesPerFrame) / this.mSampleRateInHz;
    }

    public void init()
    {
        AudioTrack localAudioTrack = createStreamingAudioTrack();
        synchronized (this.mAudioTrackLock)
        {
            this.mAudioTrack = localAudioTrack;
            return;
        }
    }

    public void stop()
    {
        synchronized (this.mAudioTrackLock)
        {
            if (this.mAudioTrack != null)
                this.mAudioTrack.stop();
            this.mStopped = true;
            return;
        }
    }

    public void waitAndRelease()
    {
        if ((this.mBytesWritten < this.mAudioBufferSize) && (!this.mStopped))
        {
            this.mIsShortUtterance = true;
            this.mAudioTrack.stop();
        }
        if (!this.mStopped)
            blockUntilDone(this.mAudioTrack);
        synchronized (this.mAudioTrackLock)
        {
            this.mAudioTrack.release();
            this.mAudioTrack = null;
            return;
        }
    }

    public int write(byte[] paramArrayOfByte)
    {
        int i;
        if ((this.mAudioTrack == null) || (this.mStopped))
            i = -1;
        while (true)
        {
            return i;
            i = writeToAudioTrack(this.mAudioTrack, paramArrayOfByte);
            this.mBytesWritten = (i + this.mBytesWritten);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.BlockingAudioTrack
 * JD-Core Version:        0.6.2
 */