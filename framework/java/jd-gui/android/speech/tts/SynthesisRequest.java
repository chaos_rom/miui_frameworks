package android.speech.tts;

import android.os.Bundle;

public final class SynthesisRequest
{
    private String mCountry;
    private String mLanguage;
    private final Bundle mParams;
    private int mPitch;
    private int mSpeechRate;
    private final String mText;
    private String mVariant;

    public SynthesisRequest(String paramString, Bundle paramBundle)
    {
        this.mText = paramString;
        this.mParams = new Bundle(paramBundle);
    }

    public String getCountry()
    {
        return this.mCountry;
    }

    public String getLanguage()
    {
        return this.mLanguage;
    }

    public Bundle getParams()
    {
        return this.mParams;
    }

    public int getPitch()
    {
        return this.mPitch;
    }

    public int getSpeechRate()
    {
        return this.mSpeechRate;
    }

    public String getText()
    {
        return this.mText;
    }

    public String getVariant()
    {
        return this.mVariant;
    }

    void setLanguage(String paramString1, String paramString2, String paramString3)
    {
        this.mLanguage = paramString1;
        this.mCountry = paramString2;
        this.mVariant = paramString3;
    }

    void setPitch(int paramInt)
    {
        this.mPitch = paramInt;
    }

    void setSpeechRate(int paramInt)
    {
        this.mSpeechRate = paramInt;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.SynthesisRequest
 * JD-Core Version:        0.6.2
 */