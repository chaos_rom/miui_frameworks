package android.speech.tts;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class SynthesisPlaybackQueueItem extends PlaybackQueueItem
{
    private static final boolean DBG = false;
    private static final long MAX_UNCONSUMED_AUDIO_MS = 500L;
    private static final String TAG = "TTS.SynthQueueItem";
    private final BlockingAudioTrack mAudioTrack;
    private final LinkedList<ListEntry> mDataBufferList = new LinkedList();
    private volatile boolean mDone = false;
    private volatile boolean mIsError = false;
    private final Lock mListLock = new ReentrantLock();
    private final EventLogger mLogger;
    private final Condition mNotFull = this.mListLock.newCondition();
    private final Condition mReadReady = this.mListLock.newCondition();
    private volatile boolean mStopped = false;
    private int mUnconsumedBytes = 0;

    SynthesisPlaybackQueueItem(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject, EventLogger paramEventLogger)
    {
        super(paramUtteranceProgressDispatcher, paramObject);
        this.mAudioTrack = new BlockingAudioTrack(paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2);
        this.mLogger = paramEventLogger;
    }

    private byte[] take()
        throws InterruptedException
    {
        byte[] arrayOfByte = null;
        try
        {
            this.mListLock.lock();
            while ((this.mDataBufferList.size() == 0) && (!this.mStopped) && (!this.mDone))
                this.mReadReady.await();
        }
        finally
        {
            this.mListLock.unlock();
        }
        boolean bool = this.mStopped;
        if (bool)
            this.mListLock.unlock();
        while (true)
        {
            return arrayOfByte;
            ListEntry localListEntry = (ListEntry)this.mDataBufferList.poll();
            if (localListEntry == null)
            {
                this.mListLock.unlock();
            }
            else
            {
                this.mUnconsumedBytes -= localListEntry.mBytes.length;
                this.mNotFull.signal();
                arrayOfByte = localListEntry.mBytes;
                this.mListLock.unlock();
            }
        }
    }

    void done()
    {
        try
        {
            this.mListLock.lock();
            this.mDone = true;
            this.mReadReady.signal();
            this.mNotFull.signal();
            return;
        }
        finally
        {
            this.mListLock.unlock();
        }
    }

    void put(byte[] paramArrayOfByte)
        throws InterruptedException
    {
        try
        {
            this.mListLock.lock();
            while ((this.mAudioTrack.getAudioLengthMs(this.mUnconsumedBytes) > 500L) && (!this.mStopped))
                this.mNotFull.await();
        }
        finally
        {
            this.mListLock.unlock();
        }
        boolean bool = this.mStopped;
        if (bool)
            this.mListLock.unlock();
        while (true)
        {
            return;
            this.mDataBufferList.add(new ListEntry(paramArrayOfByte));
            this.mUnconsumedBytes += paramArrayOfByte.length;
            this.mReadReady.signal();
            this.mListLock.unlock();
        }
    }

    public void run()
    {
        TextToSpeechService.UtteranceProgressDispatcher localUtteranceProgressDispatcher = getDispatcher();
        localUtteranceProgressDispatcher.dispatchOnStart();
        this.mAudioTrack.init();
        try
        {
            while (true)
            {
                byte[] arrayOfByte = take();
                if (arrayOfByte == null)
                    break;
                this.mAudioTrack.write(arrayOfByte);
                this.mLogger.onAudioDataWritten();
            }
        }
        catch (InterruptedException localInterruptedException)
        {
            this.mAudioTrack.waitAndRelease();
            if (!this.mIsError)
                break label75;
        }
        localUtteranceProgressDispatcher.dispatchOnError();
        while (true)
        {
            this.mLogger.onWriteData();
            return;
            label75: localUtteranceProgressDispatcher.dispatchOnDone();
        }
    }

    void stop(boolean paramBoolean)
    {
        try
        {
            this.mListLock.lock();
            this.mStopped = true;
            this.mIsError = paramBoolean;
            this.mReadReady.signal();
            this.mNotFull.signal();
            this.mListLock.unlock();
            this.mAudioTrack.stop();
            return;
        }
        finally
        {
            this.mListLock.unlock();
        }
    }

    static final class ListEntry
    {
        final byte[] mBytes;

        ListEntry(byte[] paramArrayOfByte)
        {
            this.mBytes = paramArrayOfByte;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.SynthesisPlaybackQueueItem
 * JD-Core Version:        0.6.2
 */