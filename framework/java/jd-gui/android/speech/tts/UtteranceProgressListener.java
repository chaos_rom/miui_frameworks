package android.speech.tts;

public abstract class UtteranceProgressListener
{
    static UtteranceProgressListener from(TextToSpeech.OnUtteranceCompletedListener paramOnUtteranceCompletedListener)
    {
        return new UtteranceProgressListener()
        {
            /** @deprecated */
            public void onDone(String paramAnonymousString)
            {
                try
                {
                    UtteranceProgressListener.this.onUtteranceCompleted(paramAnonymousString);
                    return;
                }
                finally
                {
                    localObject = finally;
                    throw localObject;
                }
            }

            public void onError(String paramAnonymousString)
            {
                UtteranceProgressListener.this.onUtteranceCompleted(paramAnonymousString);
            }

            public void onStart(String paramAnonymousString)
            {
            }
        };
    }

    public abstract void onDone(String paramString);

    public abstract void onError(String paramString);

    public abstract void onStart(String paramString);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.UtteranceProgressListener
 * JD-Core Version:        0.6.2
 */