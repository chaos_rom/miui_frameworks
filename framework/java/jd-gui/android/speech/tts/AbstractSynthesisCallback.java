package android.speech.tts;

abstract class AbstractSynthesisCallback
    implements SynthesisCallback
{
    abstract boolean isDone();

    abstract void stop();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.AbstractSynthesisCallback
 * JD-Core Version:        0.6.2
 */