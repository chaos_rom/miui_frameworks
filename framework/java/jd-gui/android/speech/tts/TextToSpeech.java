package android.speech.tts;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TextToSpeech
{
    public static final String ACTION_TTS_QUEUE_PROCESSING_COMPLETED = "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED";
    public static final int ERROR = -1;
    public static final int LANG_AVAILABLE = 0;
    public static final int LANG_COUNTRY_AVAILABLE = 1;
    public static final int LANG_COUNTRY_VAR_AVAILABLE = 2;
    public static final int LANG_MISSING_DATA = -1;
    public static final int LANG_NOT_SUPPORTED = -2;
    public static final int QUEUE_ADD = 1;
    static final int QUEUE_DESTROY = 2;
    public static final int QUEUE_FLUSH = 0;
    public static final int SUCCESS = 0;
    private static final String TAG = "TextToSpeech";
    private final Context mContext;
    private volatile String mCurrentEngine = null;
    private final Map<String, Uri> mEarcons;
    private final TtsEngines mEnginesHelper;
    private OnInitListener mInitListener;
    private final String mPackageName;
    private final Bundle mParams = new Bundle();
    private String mRequestedEngine;
    private Connection mServiceConnection;
    private final Object mStartLock = new Object();
    private final boolean mUseFallback;
    private volatile UtteranceProgressListener mUtteranceProgressListener;
    private final Map<String, Uri> mUtterances;

    public TextToSpeech(Context paramContext, OnInitListener paramOnInitListener)
    {
        this(paramContext, paramOnInitListener, null);
    }

    public TextToSpeech(Context paramContext, OnInitListener paramOnInitListener, String paramString)
    {
        this(paramContext, paramOnInitListener, paramString, null, true);
    }

    public TextToSpeech(Context paramContext, OnInitListener paramOnInitListener, String paramString1, String paramString2, boolean paramBoolean)
    {
        this.mContext = paramContext;
        this.mInitListener = paramOnInitListener;
        this.mRequestedEngine = paramString1;
        this.mUseFallback = paramBoolean;
        this.mEarcons = new HashMap();
        this.mUtterances = new HashMap();
        this.mUtteranceProgressListener = null;
        this.mEnginesHelper = new TtsEngines(this.mContext);
        if (paramString2 != null);
        for (this.mPackageName = paramString2; ; this.mPackageName = this.mContext.getPackageName())
        {
            initTts();
            return;
        }
    }

    private boolean connectToEngine(String paramString)
    {
        int i = 1;
        Connection localConnection = new Connection(null);
        Intent localIntent = new Intent("android.intent.action.TTS_SERVICE");
        localIntent.setPackage(paramString);
        if (!this.mContext.bindService(localIntent, localConnection, i))
        {
            Log.e("TextToSpeech", "Failed to bind to " + paramString);
            i = 0;
        }
        while (true)
        {
            return i;
            Log.i("TextToSpeech", "Sucessfully bound to " + paramString);
        }
    }

    private void copyFloatParam(Bundle paramBundle, HashMap<String, String> paramHashMap, String paramString)
    {
        String str = (String)paramHashMap.get(paramString);
        if (!TextUtils.isEmpty(str));
        try
        {
            paramBundle.putFloat(paramString, Float.parseFloat(str));
            label28: return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label28;
        }
    }

    private void copyIntParam(Bundle paramBundle, HashMap<String, String> paramHashMap, String paramString)
    {
        String str = (String)paramHashMap.get(paramString);
        if (!TextUtils.isEmpty(str));
        try
        {
            paramBundle.putInt(paramString, Integer.parseInt(str));
            label28: return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label28;
        }
    }

    private void copyStringParam(Bundle paramBundle, HashMap<String, String> paramHashMap, String paramString)
    {
        String str = (String)paramHashMap.get(paramString);
        if (str != null)
            paramBundle.putString(paramString, str);
    }

    private void dispatchOnInit(int paramInt)
    {
        synchronized (this.mStartLock)
        {
            if (this.mInitListener != null)
            {
                this.mInitListener.onInit(paramInt);
                this.mInitListener = null;
            }
            return;
        }
    }

    private IBinder getCallerIdentity()
    {
        return this.mServiceConnection.getCallerIdentity();
    }

    private Bundle getParams(HashMap<String, String> paramHashMap)
    {
        Bundle localBundle;
        Iterator localIterator;
        if ((paramHashMap != null) && (!paramHashMap.isEmpty()))
        {
            localBundle = new Bundle(this.mParams);
            copyIntParam(localBundle, paramHashMap, "streamType");
            copyStringParam(localBundle, paramHashMap, "utteranceId");
            copyFloatParam(localBundle, paramHashMap, "volume");
            copyFloatParam(localBundle, paramHashMap, "pan");
            copyStringParam(localBundle, paramHashMap, "networkTts");
            copyStringParam(localBundle, paramHashMap, "embeddedTts");
            if (!TextUtils.isEmpty(this.mCurrentEngine))
                localIterator = paramHashMap.entrySet().iterator();
        }
        else
        {
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                String str = (String)localEntry.getKey();
                if ((str != null) && (str.startsWith(this.mCurrentEngine)))
                {
                    localBundle.putString(str, (String)localEntry.getValue());
                    continue;
                    localBundle = this.mParams;
                }
            }
        }
        return localBundle;
    }

    private int initTts()
    {
        int i = 0;
        if (this.mRequestedEngine != null)
            if (this.mEnginesHelper.isEngineInstalled(this.mRequestedEngine))
                if (connectToEngine(this.mRequestedEngine))
                    this.mCurrentEngine = this.mRequestedEngine;
        while (true)
        {
            return i;
            if (!this.mUseFallback)
            {
                this.mCurrentEngine = null;
                dispatchOnInit(-1);
                i = -1;
                continue;
                if (!this.mUseFallback)
                {
                    Log.i("TextToSpeech", "Requested engine not installed: " + this.mRequestedEngine);
                    this.mCurrentEngine = null;
                    dispatchOnInit(-1);
                    i = -1;
                }
            }
            else
            {
                String str1 = getDefaultEngine();
                if ((str1 != null) && (!str1.equals(this.mRequestedEngine)) && (connectToEngine(str1)))
                {
                    this.mCurrentEngine = str1;
                }
                else
                {
                    String str2 = this.mEnginesHelper.getHighestRankedEngineName();
                    if ((str2 != null) && (!str2.equals(this.mRequestedEngine)) && (!str2.equals(str1)) && (connectToEngine(str2)))
                    {
                        this.mCurrentEngine = str2;
                    }
                    else
                    {
                        this.mCurrentEngine = null;
                        dispatchOnInit(-1);
                        i = -1;
                    }
                }
            }
        }
    }

    private Uri makeResourceUri(String paramString, int paramInt)
    {
        return new Uri.Builder().scheme("android.resource").encodedAuthority(paramString).appendEncodedPath(String.valueOf(paramInt)).build();
    }

    private <R> R runAction(Action<R> paramAction, R paramR, String paramString)
    {
        return runAction(paramAction, paramR, paramString, true);
    }

    private <R> R runAction(Action<R> paramAction, R paramR, String paramString, boolean paramBoolean)
    {
        synchronized (this.mStartLock)
        {
            if (this.mServiceConnection == null)
                Log.w("TextToSpeech", paramString + " failed: not bound to TTS engine");
            else
                paramR = this.mServiceConnection.runAction(paramAction, paramR, paramString, paramBoolean);
        }
        return paramR;
    }

    private <R> R runActionNoReconnect(Action<R> paramAction, R paramR, String paramString)
    {
        return runAction(paramAction, paramR, paramString, false);
    }

    public int addEarcon(String paramString1, String paramString2)
    {
        synchronized (this.mStartLock)
        {
            this.mEarcons.put(paramString1, Uri.parse(paramString2));
            return 0;
        }
    }

    public int addEarcon(String paramString1, String paramString2, int paramInt)
    {
        synchronized (this.mStartLock)
        {
            this.mEarcons.put(paramString1, makeResourceUri(paramString2, paramInt));
            return 0;
        }
    }

    public int addSpeech(String paramString1, String paramString2)
    {
        synchronized (this.mStartLock)
        {
            this.mUtterances.put(paramString1, Uri.parse(paramString2));
            return 0;
        }
    }

    public int addSpeech(String paramString1, String paramString2, int paramInt)
    {
        synchronized (this.mStartLock)
        {
            this.mUtterances.put(paramString1, makeResourceUri(paramString2, paramInt));
            return 0;
        }
    }

    public boolean areDefaultsEnforced()
    {
        return false;
    }

    public String getCurrentEngine()
    {
        return this.mCurrentEngine;
    }

    public String getDefaultEngine()
    {
        return this.mEnginesHelper.getDefaultEngine();
    }

    public List<EngineInfo> getEngines()
    {
        return this.mEnginesHelper.getEngines();
    }

    public Set<String> getFeatures(final Locale paramLocale)
    {
        return (Set)runAction(new Action()
        {
            public Set<String> run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                String[] arrayOfString = paramAnonymousITextToSpeechService.getFeaturesForLanguage(paramLocale.getISO3Language(), paramLocale.getISO3Country(), paramLocale.getVariant());
                HashSet localHashSet;
                if (arrayOfString != null)
                {
                    localHashSet = new HashSet();
                    Collections.addAll(localHashSet, arrayOfString);
                }
                while (true)
                {
                    return localHashSet;
                    localHashSet = null;
                }
            }
        }
        , null, "getFeatures");
    }

    public Locale getLanguage()
    {
        return (Locale)runAction(new Action()
        {
            public Locale run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                String[] arrayOfString = paramAnonymousITextToSpeechService.getLanguage();
                if ((arrayOfString != null) && (arrayOfString.length == 3));
                for (Locale localLocale = new Locale(arrayOfString[0], arrayOfString[1], arrayOfString[2]); ; localLocale = null)
                    return localLocale;
            }
        }
        , null, "getLanguage");
    }

    public int isLanguageAvailable(final Locale paramLocale)
    {
        return ((Integer)runAction(new Action()
        {
            public Integer run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                return Integer.valueOf(paramAnonymousITextToSpeechService.isLanguageAvailable(paramLocale.getISO3Language(), paramLocale.getISO3Country(), paramLocale.getVariant()));
            }
        }
        , Integer.valueOf(-2), "isLanguageAvailable")).intValue();
    }

    public boolean isSpeaking()
    {
        return ((Boolean)runAction(new Action()
        {
            public Boolean run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                return Boolean.valueOf(paramAnonymousITextToSpeechService.isSpeaking());
            }
        }
        , Boolean.valueOf(false), "isSpeaking")).booleanValue();
    }

    public int playEarcon(final String paramString, final int paramInt, final HashMap<String, String> paramHashMap)
    {
        return ((Integer)runAction(new Action()
        {
            public Integer run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                Uri localUri = (Uri)TextToSpeech.this.mEarcons.get(paramString);
                if (localUri == null);
                for (Integer localInteger = Integer.valueOf(-1); ; localInteger = Integer.valueOf(paramAnonymousITextToSpeechService.playAudio(TextToSpeech.this.getCallerIdentity(), localUri, paramInt, TextToSpeech.this.getParams(paramHashMap))))
                    return localInteger;
            }
        }
        , Integer.valueOf(-1), "playEarcon")).intValue();
    }

    public int playSilence(final long paramLong, int paramInt, final HashMap<String, String> paramHashMap)
    {
        return ((Integer)runAction(new Action()
        {
            public Integer run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                return Integer.valueOf(paramAnonymousITextToSpeechService.playSilence(TextToSpeech.this.getCallerIdentity(), paramLong, paramHashMap, TextToSpeech.this.getParams(this.val$params)));
            }
        }
        , Integer.valueOf(-1), "playSilence")).intValue();
    }

    @Deprecated
    public int setEngineByPackageName(String paramString)
    {
        this.mRequestedEngine = paramString;
        return initTts();
    }

    public int setLanguage(final Locale paramLocale)
    {
        return ((Integer)runAction(new Action()
        {
            public Integer run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                if (paramLocale == null);
                int i;
                for (Integer localInteger = Integer.valueOf(-2); ; localInteger = Integer.valueOf(i))
                {
                    return localInteger;
                    String str1 = paramLocale.getISO3Language();
                    String str2 = paramLocale.getISO3Country();
                    String str3 = paramLocale.getVariant();
                    i = paramAnonymousITextToSpeechService.loadLanguage(str1, str2, str3);
                    if (i >= 0)
                    {
                        if (i < 2)
                        {
                            str3 = "";
                            if (i < 1)
                                str2 = "";
                        }
                        TextToSpeech.this.mParams.putString("language", str1);
                        TextToSpeech.this.mParams.putString("country", str2);
                        TextToSpeech.this.mParams.putString("variant", str3);
                    }
                }
            }
        }
        , Integer.valueOf(-2), "setLanguage")).intValue();
    }

    @Deprecated
    public int setOnUtteranceCompletedListener(OnUtteranceCompletedListener paramOnUtteranceCompletedListener)
    {
        this.mUtteranceProgressListener = UtteranceProgressListener.from(paramOnUtteranceCompletedListener);
        return 0;
    }

    public int setOnUtteranceProgressListener(UtteranceProgressListener paramUtteranceProgressListener)
    {
        this.mUtteranceProgressListener = paramUtteranceProgressListener;
        return 0;
    }

    public int setPitch(float paramFloat)
    {
        int j;
        if (paramFloat > 0.0F)
        {
            j = (int)(100.0F * paramFloat);
            if (j <= 0);
        }
        int i;
        synchronized (this.mStartLock)
        {
            this.mParams.putInt("pitch", j);
            i = 0;
        }
        return i;
    }

    public int setSpeechRate(float paramFloat)
    {
        int j;
        if (paramFloat > 0.0F)
        {
            j = (int)(100.0F * paramFloat);
            if (j <= 0);
        }
        int i;
        synchronized (this.mStartLock)
        {
            this.mParams.putInt("rate", j);
            i = 0;
        }
        return i;
    }

    public void shutdown()
    {
        runActionNoReconnect(new Action()
        {
            public Void run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                paramAnonymousITextToSpeechService.setCallback(TextToSpeech.this.getCallerIdentity(), null);
                paramAnonymousITextToSpeechService.stop(TextToSpeech.this.getCallerIdentity());
                TextToSpeech.this.mServiceConnection.disconnect();
                TextToSpeech.access$202(TextToSpeech.this, null);
                TextToSpeech.access$302(TextToSpeech.this, null);
                return null;
            }
        }
        , null, "shutdown");
    }

    public int speak(final String paramString, final int paramInt, final HashMap<String, String> paramHashMap)
    {
        return ((Integer)runAction(new Action()
        {
            public Integer run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                Uri localUri = (Uri)TextToSpeech.this.mUtterances.get(paramString);
                if (localUri != null);
                for (Integer localInteger = Integer.valueOf(paramAnonymousITextToSpeechService.playAudio(TextToSpeech.this.getCallerIdentity(), localUri, paramInt, TextToSpeech.this.getParams(paramHashMap))); ; localInteger = Integer.valueOf(paramAnonymousITextToSpeechService.speak(TextToSpeech.this.getCallerIdentity(), paramString, paramInt, TextToSpeech.this.getParams(paramHashMap))))
                    return localInteger;
            }
        }
        , Integer.valueOf(-1), "speak")).intValue();
    }

    public int stop()
    {
        return ((Integer)runAction(new Action()
        {
            public Integer run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                return Integer.valueOf(paramAnonymousITextToSpeechService.stop(TextToSpeech.this.getCallerIdentity()));
            }
        }
        , Integer.valueOf(-1), "stop")).intValue();
    }

    public int synthesizeToFile(final String paramString1, final HashMap<String, String> paramHashMap, final String paramString2)
    {
        return ((Integer)runAction(new Action()
        {
            public Integer run(ITextToSpeechService paramAnonymousITextToSpeechService)
                throws RemoteException
            {
                return Integer.valueOf(paramAnonymousITextToSpeechService.synthesizeToFile(TextToSpeech.this.getCallerIdentity(), paramString1, paramString2, TextToSpeech.this.getParams(paramHashMap)));
            }
        }
        , Integer.valueOf(-1), "synthesizeToFile")).intValue();
    }

    public static class EngineInfo
    {
        public int icon;
        public String label;
        public String name;
        public int priority;
        public boolean system;

        public String toString()
        {
            return "EngineInfo{name=" + this.name + "}";
        }
    }

    private static abstract interface Action<R>
    {
        public abstract R run(ITextToSpeechService paramITextToSpeechService)
            throws RemoteException;
    }

    private class Connection
        implements ServiceConnection
    {
        private final ITextToSpeechCallback.Stub mCallback = new ITextToSpeechCallback.Stub()
        {
            public void onDone(String paramAnonymousString)
            {
                UtteranceProgressListener localUtteranceProgressListener = TextToSpeech.this.mUtteranceProgressListener;
                if (localUtteranceProgressListener != null)
                    localUtteranceProgressListener.onDone(paramAnonymousString);
            }

            public void onError(String paramAnonymousString)
            {
                UtteranceProgressListener localUtteranceProgressListener = TextToSpeech.this.mUtteranceProgressListener;
                if (localUtteranceProgressListener != null)
                    localUtteranceProgressListener.onError(paramAnonymousString);
            }

            public void onStart(String paramAnonymousString)
            {
                UtteranceProgressListener localUtteranceProgressListener = TextToSpeech.this.mUtteranceProgressListener;
                if (localUtteranceProgressListener != null)
                    localUtteranceProgressListener.onStart(paramAnonymousString);
            }
        };
        private ITextToSpeechService mService;

        private Connection()
        {
        }

        public void disconnect()
        {
            TextToSpeech.this.mContext.unbindService(this);
        }

        public IBinder getCallerIdentity()
        {
            return this.mCallback;
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            Log.i("TextToSpeech", "Connected to " + paramComponentName);
            synchronized (TextToSpeech.this.mStartLock)
            {
                if (TextToSpeech.this.mServiceConnection != null)
                    TextToSpeech.this.mServiceConnection.disconnect();
                TextToSpeech.access$202(TextToSpeech.this, this);
                this.mService = ITextToSpeechService.Stub.asInterface(paramIBinder);
                try
                {
                    this.mService.setCallback(getCallerIdentity(), this.mCallback);
                    TextToSpeech.this.dispatchOnInit(0);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                    {
                        Log.e("TextToSpeech", "Error connecting to service, setCallback() failed");
                        TextToSpeech.this.dispatchOnInit(-1);
                    }
                }
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            synchronized (TextToSpeech.this.mStartLock)
            {
                this.mService = null;
                if (TextToSpeech.this.mServiceConnection == this)
                    TextToSpeech.access$202(TextToSpeech.this, null);
                return;
            }
        }

        // ERROR //
        public <R> R runAction(TextToSpeech.Action<R> paramAction, R paramR, String paramString, boolean paramBoolean)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 21	android/speech/tts/TextToSpeech$Connection:this$0	Landroid/speech/tts/TextToSpeech;
            //     4: invokestatic 77	android/speech/tts/TextToSpeech:access$900	(Landroid/speech/tts/TextToSpeech;)Ljava/lang/Object;
            //     7: astore 8
            //     9: aload 8
            //     11: monitorenter
            //     12: aload_0
            //     13: getfield 95	android/speech/tts/TextToSpeech$Connection:mService	Landroid/speech/tts/ITextToSpeechService;
            //     16: ifnonnull +34 -> 50
            //     19: ldc 51
            //     21: new 53	java/lang/StringBuilder
            //     24: dup
            //     25: invokespecial 54	java/lang/StringBuilder:<init>	()V
            //     28: aload_3
            //     29: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     32: ldc 118
            //     34: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     37: invokevirtual 67	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     40: invokestatic 121	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     43: pop
            //     44: aload 8
            //     46: monitorexit
            //     47: goto +78 -> 125
            //     50: aload_1
            //     51: aload_0
            //     52: getfield 95	android/speech/tts/TextToSpeech$Connection:mService	Landroid/speech/tts/ITextToSpeechService;
            //     55: invokeinterface 127 2 0
            //     60: astore 10
            //     62: aload 8
            //     64: monitorexit
            //     65: aload 10
            //     67: astore_2
            //     68: goto +57 -> 125
            //     71: astore 9
            //     73: aload 8
            //     75: monitorexit
            //     76: aload 9
            //     78: athrow
            //     79: astore 5
            //     81: ldc 51
            //     83: new 53	java/lang/StringBuilder
            //     86: dup
            //     87: invokespecial 54	java/lang/StringBuilder:<init>	()V
            //     90: aload_3
            //     91: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     94: ldc 129
            //     96: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     99: invokevirtual 67	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     102: aload 5
            //     104: invokestatic 132	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     107: pop
            //     108: iload 4
            //     110: ifeq +15 -> 125
            //     113: aload_0
            //     114: invokevirtual 83	android/speech/tts/TextToSpeech$Connection:disconnect	()V
            //     117: aload_0
            //     118: getfield 21	android/speech/tts/TextToSpeech$Connection:this$0	Landroid/speech/tts/TextToSpeech;
            //     121: invokestatic 136	android/speech/tts/TextToSpeech:access$1200	(Landroid/speech/tts/TextToSpeech;)I
            //     124: pop
            //     125: aload_2
            //     126: areturn
            //
            // Exception table:
            //     from	to	target	type
            //     12	76	71	finally
            //     0	12	79	android/os/RemoteException
            //     76	79	79	android/os/RemoteException
        }
    }

    public class Engine
    {
        public static final String ACTION_CHECK_TTS_DATA = "android.speech.tts.engine.CHECK_TTS_DATA";
        public static final String ACTION_GET_SAMPLE_TEXT = "android.speech.tts.engine.GET_SAMPLE_TEXT";
        public static final String ACTION_INSTALL_TTS_DATA = "android.speech.tts.engine.INSTALL_TTS_DATA";
        public static final String ACTION_TTS_DATA_INSTALLED = "android.speech.tts.engine.TTS_DATA_INSTALLED";
        public static final int CHECK_VOICE_DATA_BAD_DATA = -1;
        public static final int CHECK_VOICE_DATA_FAIL = 0;
        public static final int CHECK_VOICE_DATA_MISSING_DATA = -2;
        public static final int CHECK_VOICE_DATA_MISSING_VOLUME = -3;
        public static final int CHECK_VOICE_DATA_PASS = 1;

        @Deprecated
        public static final String DEFAULT_ENGINE = "com.svox.pico";
        public static final float DEFAULT_PAN = 0.0F;
        public static final int DEFAULT_PITCH = 100;
        public static final int DEFAULT_RATE = 100;
        public static final int DEFAULT_STREAM = 3;
        public static final float DEFAULT_VOLUME = 1.0F;
        public static final String EXTRA_AVAILABLE_VOICES = "availableVoices";
        public static final String EXTRA_CHECK_VOICE_DATA_FOR = "checkVoiceDataFor";
        public static final String EXTRA_TTS_DATA_INSTALLED = "dataInstalled";
        public static final String EXTRA_UNAVAILABLE_VOICES = "unavailableVoices";
        public static final String EXTRA_VOICE_DATA_FILES = "dataFiles";
        public static final String EXTRA_VOICE_DATA_FILES_INFO = "dataFilesInfo";
        public static final String EXTRA_VOICE_DATA_ROOT_DIRECTORY = "dataRoot";
        public static final String INTENT_ACTION_TTS_SERVICE = "android.intent.action.TTS_SERVICE";
        public static final String KEY_FEATURE_EMBEDDED_SYNTHESIS = "embeddedTts";
        public static final String KEY_FEATURE_NETWORK_SYNTHESIS = "networkTts";
        public static final String KEY_PARAM_COUNTRY = "country";
        public static final String KEY_PARAM_ENGINE = "engine";
        public static final String KEY_PARAM_LANGUAGE = "language";
        public static final String KEY_PARAM_PAN = "pan";
        public static final String KEY_PARAM_PITCH = "pitch";
        public static final String KEY_PARAM_RATE = "rate";
        public static final String KEY_PARAM_STREAM = "streamType";
        public static final String KEY_PARAM_UTTERANCE_ID = "utteranceId";
        public static final String KEY_PARAM_VARIANT = "variant";
        public static final String KEY_PARAM_VOLUME = "volume";
        public static final String SERVICE_META_DATA = "android.speech.tts";
        public static final int USE_DEFAULTS;

        public Engine()
        {
        }
    }

    public static abstract interface OnUtteranceCompletedListener
    {
        public abstract void onUtteranceCompleted(String paramString);
    }

    public static abstract interface OnInitListener
    {
        public abstract void onInit(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.TextToSpeech
 * JD-Core Version:        0.6.2
 */