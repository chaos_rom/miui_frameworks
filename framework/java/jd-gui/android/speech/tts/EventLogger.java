package android.speech.tts;

import android.os.SystemClock;
import android.text.TextUtils;

class EventLogger
{
    private final int mCallerPid;
    private final int mCallerUid;
    private volatile long mEngineCompleteTime = -1L;
    private volatile long mEngineStartTime = -1L;
    private volatile boolean mError = false;
    private boolean mLogWritten = false;
    private long mPlaybackStartTime = -1L;
    private final long mReceivedTime;
    private final SynthesisRequest mRequest;
    private volatile long mRequestProcessingStartTime = -1L;
    private final String mServiceApp;
    private volatile boolean mStopped = false;

    EventLogger(SynthesisRequest paramSynthesisRequest, int paramInt1, int paramInt2, String paramString)
    {
        this.mRequest = paramSynthesisRequest;
        this.mCallerUid = paramInt1;
        this.mCallerPid = paramInt2;
        this.mServiceApp = paramString;
        this.mReceivedTime = SystemClock.elapsedRealtime();
    }

    private String getLocaleString()
    {
        StringBuilder localStringBuilder = new StringBuilder(this.mRequest.getLanguage());
        if (!TextUtils.isEmpty(this.mRequest.getCountry()))
        {
            localStringBuilder.append('-');
            localStringBuilder.append(this.mRequest.getCountry());
            if (!TextUtils.isEmpty(this.mRequest.getVariant()))
            {
                localStringBuilder.append('-');
                localStringBuilder.append(this.mRequest.getVariant());
            }
        }
        return localStringBuilder.toString();
    }

    private int getUtteranceLength()
    {
        String str = this.mRequest.getText();
        if (str == null);
        for (int i = 0; ; i = str.length())
            return i;
    }

    public void onAudioDataWritten()
    {
        if (this.mPlaybackStartTime == -1L)
            this.mPlaybackStartTime = SystemClock.elapsedRealtime();
    }

    public void onEngineComplete()
    {
        this.mEngineCompleteTime = SystemClock.elapsedRealtime();
    }

    public void onEngineDataReceived()
    {
        if (this.mEngineStartTime == -1L)
            this.mEngineStartTime = SystemClock.elapsedRealtime();
    }

    public void onError()
    {
        this.mError = true;
    }

    public void onRequestProcessingStart()
    {
        this.mRequestProcessingStartTime = SystemClock.elapsedRealtime();
    }

    public void onStopped()
    {
        this.mStopped = false;
    }

    public void onWriteData()
    {
        if (this.mLogWritten);
        while (true)
        {
            return;
            this.mLogWritten = true;
            SystemClock.elapsedRealtime();
            if ((this.mError) || (this.mPlaybackStartTime == -1L) || (this.mEngineCompleteTime == -1L))
            {
                EventLogTags.writeTtsSpeakFailure(this.mServiceApp, this.mCallerUid, this.mCallerPid, getUtteranceLength(), getLocaleString(), this.mRequest.getSpeechRate(), this.mRequest.getPitch());
            }
            else if (!this.mStopped)
            {
                long l1 = this.mPlaybackStartTime - this.mReceivedTime;
                long l2 = this.mEngineStartTime - this.mRequestProcessingStartTime;
                long l3 = this.mEngineCompleteTime - this.mRequestProcessingStartTime;
                EventLogTags.writeTtsSpeakSuccess(this.mServiceApp, this.mCallerUid, this.mCallerPid, getUtteranceLength(), getLocaleString(), this.mRequest.getSpeechRate(), this.mRequest.getPitch(), l2, l3, l1);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.EventLogger
 * JD-Core Version:        0.6.2
 */