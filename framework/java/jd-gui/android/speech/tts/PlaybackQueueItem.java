package android.speech.tts;

abstract class PlaybackQueueItem
    implements Runnable
{
    private final Object mCallerIdentity;
    private final TextToSpeechService.UtteranceProgressDispatcher mDispatcher;

    PlaybackQueueItem(TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject)
    {
        this.mDispatcher = paramUtteranceProgressDispatcher;
        this.mCallerIdentity = paramObject;
    }

    Object getCallerIdentity()
    {
        return this.mCallerIdentity;
    }

    protected TextToSpeechService.UtteranceProgressDispatcher getDispatcher()
    {
        return this.mDispatcher;
    }

    public abstract void run();

    abstract void stop(boolean paramBoolean);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.PlaybackQueueItem
 * JD-Core Version:        0.6.2
 */