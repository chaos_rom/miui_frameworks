package android.speech.tts;

public abstract interface SynthesisCallback
{
    public abstract int audioAvailable(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

    public abstract int done();

    public abstract void error();

    public abstract int getMaxBufferSize();

    public abstract int start(int paramInt1, int paramInt2, int paramInt3);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.SynthesisCallback
 * JD-Core Version:        0.6.2
 */