package android.speech.tts;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.ConditionVariable;
import android.util.Log;

class AudioPlaybackQueueItem extends PlaybackQueueItem
{
    private static final String TAG = "TTS.AudioQueueItem";
    private final Context mContext;
    private final ConditionVariable mDone;
    private volatile boolean mFinished;
    private MediaPlayer mPlayer;
    private final int mStreamType;
    private final Uri mUri;

    AudioPlaybackQueueItem(TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject, Context paramContext, Uri paramUri, int paramInt)
    {
        super(paramUtteranceProgressDispatcher, paramObject);
        this.mContext = paramContext;
        this.mUri = paramUri;
        this.mStreamType = paramInt;
        this.mDone = new ConditionVariable();
        this.mPlayer = null;
        this.mFinished = false;
    }

    private void finish()
    {
        try
        {
            this.mPlayer.stop();
            label7: this.mPlayer.release();
            return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            break label7;
        }
    }

    public void run()
    {
        TextToSpeechService.UtteranceProgressDispatcher localUtteranceProgressDispatcher = getDispatcher();
        localUtteranceProgressDispatcher.dispatchOnStart();
        this.mPlayer = MediaPlayer.create(this.mContext, this.mUri);
        if (this.mPlayer == null)
            localUtteranceProgressDispatcher.dispatchOnError();
        while (true)
        {
            return;
            try
            {
                this.mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener()
                {
                    public boolean onError(MediaPlayer paramAnonymousMediaPlayer, int paramAnonymousInt1, int paramAnonymousInt2)
                    {
                        Log.w("TTS.AudioQueueItem", "Audio playback error: " + paramAnonymousInt1 + ", " + paramAnonymousInt2);
                        AudioPlaybackQueueItem.this.mDone.open();
                        return true;
                    }
                });
                this.mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
                {
                    public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
                    {
                        AudioPlaybackQueueItem.access$102(AudioPlaybackQueueItem.this, true);
                        AudioPlaybackQueueItem.this.mDone.open();
                    }
                });
                this.mPlayer.setAudioStreamType(this.mStreamType);
                this.mPlayer.start();
                this.mDone.block();
                finish();
                if (this.mFinished)
                    localUtteranceProgressDispatcher.dispatchOnDone();
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                while (true)
                {
                    Log.w("TTS.AudioQueueItem", "MediaPlayer failed", localIllegalArgumentException);
                    this.mDone.open();
                }
                localUtteranceProgressDispatcher.dispatchOnError();
            }
        }
    }

    void stop(boolean paramBoolean)
    {
        this.mDone.open();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.AudioPlaybackQueueItem
 * JD-Core Version:        0.6.2
 */