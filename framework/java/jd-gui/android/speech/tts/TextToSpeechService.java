package android.speech.tts;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageItemInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.os.MessageQueue.IdleHandler;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public abstract class TextToSpeechService extends Service
{
    private static final boolean DBG = false;
    private static final int MAX_SPEECH_ITEM_CHAR_LENGTH = 4000;
    private static final String SYNTH_THREAD_NAME = "SynthThread";
    private static final String TAG = "TextToSpeechService";
    private AudioPlaybackHandler mAudioPlaybackHandler;
    private final ITextToSpeechService.Stub mBinder = new ITextToSpeechService.Stub()
    {
        private boolean checkNonNull(Object[] paramAnonymousArrayOfObject)
        {
            int i = paramAnonymousArrayOfObject.length;
            int j = 0;
            if (j < i)
                if (paramAnonymousArrayOfObject[j] != null);
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                j++;
                break;
            }
        }

        private String intern(String paramAnonymousString)
        {
            return paramAnonymousString.intern();
        }

        public String[] getFeaturesForLanguage(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3)
        {
            Set localSet = TextToSpeechService.this.onGetFeaturesForLanguage(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3);
            String[] arrayOfString;
            if (localSet != null)
            {
                arrayOfString = new String[localSet.size()];
                localSet.toArray(arrayOfString);
            }
            while (true)
            {
                return arrayOfString;
                arrayOfString = new String[0];
            }
        }

        public String[] getLanguage()
        {
            return TextToSpeechService.this.onGetLanguage();
        }

        public int isLanguageAvailable(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3)
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = paramAnonymousString1;
            if (!checkNonNull(arrayOfObject));
            for (int i = -1; ; i = TextToSpeechService.this.onIsLanguageAvailable(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3))
                return i;
        }

        public boolean isSpeaking()
        {
            if ((TextToSpeechService.this.mSynthHandler.isSpeaking()) || (TextToSpeechService.this.mAudioPlaybackHandler.isSpeaking()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public int loadLanguage(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3)
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = paramAnonymousString1;
            if (!checkNonNull(arrayOfObject));
            for (int i = -1; ; i = TextToSpeechService.this.onLoadLanguage(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3))
                return i;
        }

        public int playAudio(IBinder paramAnonymousIBinder, Uri paramAnonymousUri, int paramAnonymousInt, Bundle paramAnonymousBundle)
        {
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = paramAnonymousIBinder;
            arrayOfObject[1] = paramAnonymousUri;
            arrayOfObject[2] = paramAnonymousBundle;
            if (!checkNonNull(arrayOfObject));
            TextToSpeechService.AudioSpeechItem localAudioSpeechItem;
            for (int i = -1; ; i = TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(paramAnonymousInt, localAudioSpeechItem))
            {
                return i;
                localAudioSpeechItem = new TextToSpeechService.AudioSpeechItem(TextToSpeechService.this, paramAnonymousIBinder, Binder.getCallingUid(), Binder.getCallingPid(), paramAnonymousBundle, paramAnonymousUri);
            }
        }

        public int playSilence(IBinder paramAnonymousIBinder, long paramAnonymousLong, int paramAnonymousInt, Bundle paramAnonymousBundle)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = paramAnonymousIBinder;
            arrayOfObject[1] = paramAnonymousBundle;
            if (!checkNonNull(arrayOfObject));
            TextToSpeechService.SilenceSpeechItem localSilenceSpeechItem;
            for (int i = -1; ; i = TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(paramAnonymousInt, localSilenceSpeechItem))
            {
                return i;
                localSilenceSpeechItem = new TextToSpeechService.SilenceSpeechItem(TextToSpeechService.this, paramAnonymousIBinder, Binder.getCallingUid(), Binder.getCallingPid(), paramAnonymousBundle, paramAnonymousLong);
            }
        }

        public void setCallback(IBinder paramAnonymousIBinder, ITextToSpeechCallback paramAnonymousITextToSpeechCallback)
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = paramAnonymousIBinder;
            if (!checkNonNull(arrayOfObject));
            while (true)
            {
                return;
                TextToSpeechService.this.mCallbacks.setCallback(paramAnonymousIBinder, paramAnonymousITextToSpeechCallback);
            }
        }

        public int speak(IBinder paramAnonymousIBinder, String paramAnonymousString, int paramAnonymousInt, Bundle paramAnonymousBundle)
        {
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = paramAnonymousIBinder;
            arrayOfObject[1] = paramAnonymousString;
            arrayOfObject[2] = paramAnonymousBundle;
            if (!checkNonNull(arrayOfObject));
            TextToSpeechService.SynthesisSpeechItem localSynthesisSpeechItem;
            for (int i = -1; ; i = TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(paramAnonymousInt, localSynthesisSpeechItem))
            {
                return i;
                localSynthesisSpeechItem = new TextToSpeechService.SynthesisSpeechItem(TextToSpeechService.this, paramAnonymousIBinder, Binder.getCallingUid(), Binder.getCallingPid(), paramAnonymousBundle, paramAnonymousString);
            }
        }

        public int stop(IBinder paramAnonymousIBinder)
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = paramAnonymousIBinder;
            if (!checkNonNull(arrayOfObject));
            for (int i = -1; ; i = TextToSpeechService.this.mSynthHandler.stopForApp(paramAnonymousIBinder))
                return i;
        }

        public int synthesizeToFile(IBinder paramAnonymousIBinder, String paramAnonymousString1, String paramAnonymousString2, Bundle paramAnonymousBundle)
        {
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = paramAnonymousIBinder;
            arrayOfObject[1] = paramAnonymousString1;
            arrayOfObject[2] = paramAnonymousString2;
            arrayOfObject[3] = paramAnonymousBundle;
            if (!checkNonNull(arrayOfObject));
            TextToSpeechService.SynthesisToFileSpeechItem localSynthesisToFileSpeechItem;
            for (int i = -1; ; i = TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(1, localSynthesisToFileSpeechItem))
            {
                return i;
                File localFile = new File(paramAnonymousString2);
                localSynthesisToFileSpeechItem = new TextToSpeechService.SynthesisToFileSpeechItem(TextToSpeechService.this, paramAnonymousIBinder, Binder.getCallingUid(), Binder.getCallingPid(), paramAnonymousBundle, paramAnonymousString1, localFile);
            }
        }
    };
    private CallbackMap mCallbacks;
    private TtsEngines mEngineHelper;
    private String mPackageName;
    private SynthHandler mSynthHandler;

    private int getDefaultSpeechRate()
    {
        return getSecureSettingInt("tts_default_rate", 100);
    }

    private int getSecureSettingInt(String paramString, int paramInt)
    {
        return Settings.Secure.getInt(getContentResolver(), paramString, paramInt);
    }

    private String[] getSettingsLocale()
    {
        return TtsEngines.parseLocalePref(this.mEngineHelper.getLocalePrefForEngine(this.mPackageName));
    }

    public IBinder onBind(Intent paramIntent)
    {
        if ("android.intent.action.TTS_SERVICE".equals(paramIntent.getAction()));
        for (ITextToSpeechService.Stub localStub = this.mBinder; ; localStub = null)
            return localStub;
    }

    public void onCreate()
    {
        super.onCreate();
        SynthThread localSynthThread = new SynthThread();
        localSynthThread.start();
        this.mSynthHandler = new SynthHandler(localSynthThread.getLooper());
        this.mAudioPlaybackHandler = new AudioPlaybackHandler();
        this.mAudioPlaybackHandler.start();
        this.mEngineHelper = new TtsEngines(this);
        this.mCallbacks = new CallbackMap(null);
        this.mPackageName = getApplicationInfo().packageName;
        String[] arrayOfString = getSettingsLocale();
        onLoadLanguage(arrayOfString[0], arrayOfString[1], arrayOfString[2]);
    }

    public void onDestroy()
    {
        this.mSynthHandler.quit();
        this.mAudioPlaybackHandler.quit();
        this.mCallbacks.kill();
        super.onDestroy();
    }

    protected Set<String> onGetFeaturesForLanguage(String paramString1, String paramString2, String paramString3)
    {
        return null;
    }

    protected abstract String[] onGetLanguage();

    protected abstract int onIsLanguageAvailable(String paramString1, String paramString2, String paramString3);

    protected abstract int onLoadLanguage(String paramString1, String paramString2, String paramString3);

    protected abstract void onStop();

    protected abstract void onSynthesizeText(SynthesisRequest paramSynthesisRequest, SynthesisCallback paramSynthesisCallback);

    private class CallbackMap extends RemoteCallbackList<ITextToSpeechCallback>
    {
        private final HashMap<IBinder, ITextToSpeechCallback> mCallerToCallback = new HashMap();

        private CallbackMap()
        {
        }

        private ITextToSpeechCallback getCallbackFor(Object paramObject)
        {
            IBinder localIBinder = (IBinder)paramObject;
            synchronized (this.mCallerToCallback)
            {
                ITextToSpeechCallback localITextToSpeechCallback = (ITextToSpeechCallback)this.mCallerToCallback.get(localIBinder);
                return localITextToSpeechCallback;
            }
        }

        public void dispatchOnDone(Object paramObject, String paramString)
        {
            ITextToSpeechCallback localITextToSpeechCallback = getCallbackFor(paramObject);
            if (localITextToSpeechCallback == null);
            while (true)
            {
                return;
                try
                {
                    localITextToSpeechCallback.onDone(paramString);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("TextToSpeechService", "Callback onDone failed: " + localRemoteException);
                }
            }
        }

        public void dispatchOnError(Object paramObject, String paramString)
        {
            ITextToSpeechCallback localITextToSpeechCallback = getCallbackFor(paramObject);
            if (localITextToSpeechCallback == null);
            while (true)
            {
                return;
                try
                {
                    localITextToSpeechCallback.onError(paramString);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("TextToSpeechService", "Callback onError failed: " + localRemoteException);
                }
            }
        }

        public void dispatchOnStart(Object paramObject, String paramString)
        {
            ITextToSpeechCallback localITextToSpeechCallback = getCallbackFor(paramObject);
            if (localITextToSpeechCallback == null);
            while (true)
            {
                return;
                try
                {
                    localITextToSpeechCallback.onStart(paramString);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("TextToSpeechService", "Callback onStart failed: " + localRemoteException);
                }
            }
        }

        public void kill()
        {
            synchronized (this.mCallerToCallback)
            {
                this.mCallerToCallback.clear();
                super.kill();
                return;
            }
        }

        public void onCallbackDied(ITextToSpeechCallback paramITextToSpeechCallback, Object paramObject)
        {
            IBinder localIBinder = (IBinder)paramObject;
            synchronized (this.mCallerToCallback)
            {
                this.mCallerToCallback.remove(localIBinder);
                TextToSpeechService.this.mSynthHandler.stopForApp(localIBinder);
                return;
            }
        }

        public void setCallback(IBinder paramIBinder, ITextToSpeechCallback paramITextToSpeechCallback)
        {
            HashMap localHashMap = this.mCallerToCallback;
            if (paramITextToSpeechCallback != null);
            try
            {
                register(paramITextToSpeechCallback, paramIBinder);
                ITextToSpeechCallback localITextToSpeechCallback = (ITextToSpeechCallback)this.mCallerToCallback.put(paramIBinder, paramITextToSpeechCallback);
                if ((localITextToSpeechCallback != null) && (localITextToSpeechCallback != paramITextToSpeechCallback))
                    unregister(localITextToSpeechCallback);
                return;
                localITextToSpeechCallback = (ITextToSpeechCallback)this.mCallerToCallback.remove(paramIBinder);
            }
            finally
            {
            }
        }
    }

    private class SilenceSpeechItem extends TextToSpeechService.SpeechItem
    {
        private final long mDuration;

        public SilenceSpeechItem(Object paramInt1, int paramInt2, int paramBundle, Bundle paramLong, long arg6)
        {
            super(paramInt1, paramInt2, paramBundle, paramLong);
            Object localObject;
            this.mDuration = localObject;
        }

        public boolean isValid()
        {
            return true;
        }

        protected int playImpl()
        {
            TextToSpeechService.this.mAudioPlaybackHandler.enqueue(new SilencePlaybackQueueItem(this, getCallerIdentity(), this.mDuration));
            return 0;
        }

        protected void stopImpl()
        {
        }
    }

    private class AudioSpeechItem extends TextToSpeechService.SpeechItem
    {
        private final AudioPlaybackQueueItem mItem;

        public AudioSpeechItem(Object paramInt1, int paramInt2, int paramBundle, Bundle paramUri, Uri arg6)
        {
            super(paramInt1, paramInt2, paramBundle, paramUri);
            Uri localUri;
            this.mItem = new AudioPlaybackQueueItem(this, getCallerIdentity(), TextToSpeechService.this, localUri, getStreamType());
        }

        public boolean isValid()
        {
            return true;
        }

        protected int playImpl()
        {
            TextToSpeechService.this.mAudioPlaybackHandler.enqueue(this.mItem);
            return 0;
        }

        protected void stopImpl()
        {
        }
    }

    private class SynthesisToFileSpeechItem extends TextToSpeechService.SynthesisSpeechItem
    {
        private final File mFile;

        public SynthesisToFileSpeechItem(Object paramInt1, int paramInt2, int paramBundle, Bundle paramString, String paramFile, File arg7)
        {
            super(paramInt1, paramInt2, paramBundle, paramString, paramFile);
            Object localObject;
            this.mFile = localObject;
        }

        private boolean checkFile(File paramFile)
        {
            boolean bool = false;
            try
            {
                if (paramFile.exists())
                {
                    Log.v("TextToSpeechService", "File " + paramFile + " exists, deleting.");
                    if (!paramFile.delete())
                    {
                        Log.e("TextToSpeechService", "Failed to delete " + paramFile);
                        break label184;
                    }
                }
                if (!paramFile.createNewFile())
                    Log.e("TextToSpeechService", "Can't create file " + paramFile);
            }
            catch (IOException localIOException)
            {
                Log.e("TextToSpeechService", "Can't use " + paramFile + " due to exception " + localIOException);
            }
            if (!paramFile.delete())
                Log.e("TextToSpeechService", "Failed to delete " + paramFile);
            else
                bool = true;
            label184: return bool;
        }

        protected AbstractSynthesisCallback createSynthesisCallback()
        {
            return new FileSynthesisCallback(this.mFile);
        }

        public boolean isValid()
        {
            if (!super.isValid());
            for (boolean bool = false; ; bool = checkFile(this.mFile))
                return bool;
        }

        protected int playImpl()
        {
            dispatchOnStart();
            int i = super.playImpl();
            if (i == 0)
                dispatchOnDone();
            while (true)
            {
                return i;
                dispatchOnError();
            }
        }
    }

    class SynthesisSpeechItem extends TextToSpeechService.SpeechItem
    {
        private final String[] mDefaultLocale;
        private final EventLogger mEventLogger;
        private AbstractSynthesisCallback mSynthesisCallback;
        private final SynthesisRequest mSynthesisRequest;
        private final String mText;

        public SynthesisSpeechItem(Object paramInt1, int paramInt2, int paramBundle, Bundle paramString, String arg6)
        {
            super(paramInt1, paramInt2, paramBundle, paramString);
            Object localObject;
            this.mText = localObject;
            this.mSynthesisRequest = new SynthesisRequest(this.mText, this.mParams);
            this.mDefaultLocale = TextToSpeechService.this.getSettingsLocale();
            setRequestParams(this.mSynthesisRequest);
            this.mEventLogger = new EventLogger(this.mSynthesisRequest, paramInt2, paramBundle, TextToSpeechService.this.mPackageName);
        }

        private String getCountry()
        {
            if (!hasLanguage());
            for (String str = this.mDefaultLocale[1]; ; str = getStringParam("country", ""))
                return str;
        }

        private int getPitch()
        {
            return getIntParam("pitch", 100);
        }

        private int getSpeechRate()
        {
            return getIntParam("rate", TextToSpeechService.this.getDefaultSpeechRate());
        }

        private String getVariant()
        {
            if (!hasLanguage());
            for (String str = this.mDefaultLocale[2]; ; str = getStringParam("variant", ""))
                return str;
        }

        private boolean hasLanguage()
        {
            if (!TextUtils.isEmpty(getStringParam("language", null)));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private void setRequestParams(SynthesisRequest paramSynthesisRequest)
        {
            paramSynthesisRequest.setLanguage(getLanguage(), getCountry(), getVariant());
            paramSynthesisRequest.setSpeechRate(getSpeechRate());
            paramSynthesisRequest.setPitch(getPitch());
        }

        protected AbstractSynthesisCallback createSynthesisCallback()
        {
            return new PlaybackSynthesisCallback(getStreamType(), getVolume(), getPan(), TextToSpeechService.this.mAudioPlaybackHandler, this, getCallerIdentity(), this.mEventLogger);
        }

        public String getLanguage()
        {
            return getStringParam("language", this.mDefaultLocale[0]);
        }

        public String getText()
        {
            return this.mText;
        }

        public boolean isValid()
        {
            boolean bool = false;
            if (this.mText == null)
                Log.wtf("TextToSpeechService", "Got null text");
            while (true)
            {
                return bool;
                if (this.mText.length() >= 4000)
                    Log.w("TextToSpeechService", "Text too long: " + this.mText.length() + " chars");
                else
                    bool = true;
            }
        }

        protected int playImpl()
        {
            int i = -1;
            this.mEventLogger.onRequestProcessingStart();
            try
            {
                if (isStopped())
                    break label68;
                this.mSynthesisCallback = createSynthesisCallback();
                AbstractSynthesisCallback localAbstractSynthesisCallback = this.mSynthesisCallback;
                TextToSpeechService.this.onSynthesizeText(this.mSynthesisRequest, localAbstractSynthesisCallback);
                if (localAbstractSynthesisCallback.isDone())
                    i = 0;
            }
            finally
            {
            }
            label68: return i;
        }

        protected void stopImpl()
        {
            try
            {
                AbstractSynthesisCallback localAbstractSynthesisCallback = this.mSynthesisCallback;
                if (localAbstractSynthesisCallback != null)
                {
                    localAbstractSynthesisCallback.stop();
                    TextToSpeechService.this.onStop();
                }
                return;
            }
            finally
            {
            }
        }
    }

    private abstract class SpeechItem
        implements TextToSpeechService.UtteranceProgressDispatcher
    {
        private final Object mCallerIdentity;
        private final int mCallerPid;
        private final int mCallerUid;
        protected final Bundle mParams;
        private boolean mStarted = false;
        private boolean mStopped = false;

        public SpeechItem(Object paramInt1, int paramInt2, int paramBundle, Bundle arg5)
        {
            this.mCallerIdentity = paramInt1;
            Object localObject;
            this.mParams = localObject;
            this.mCallerUid = paramInt2;
            this.mCallerPid = paramBundle;
        }

        public void dispatchOnDone()
        {
            String str = getUtteranceId();
            if (str != null)
                TextToSpeechService.this.mCallbacks.dispatchOnDone(getCallerIdentity(), str);
        }

        public void dispatchOnError()
        {
            String str = getUtteranceId();
            if (str != null)
                TextToSpeechService.this.mCallbacks.dispatchOnError(getCallerIdentity(), str);
        }

        public void dispatchOnStart()
        {
            String str = getUtteranceId();
            if (str != null)
                TextToSpeechService.this.mCallbacks.dispatchOnStart(getCallerIdentity(), str);
        }

        public Object getCallerIdentity()
        {
            return this.mCallerIdentity;
        }

        public int getCallerPid()
        {
            return this.mCallerPid;
        }

        public int getCallerUid()
        {
            return this.mCallerUid;
        }

        protected float getFloatParam(String paramString, float paramFloat)
        {
            if (this.mParams == null);
            while (true)
            {
                return paramFloat;
                paramFloat = this.mParams.getFloat(paramString, paramFloat);
            }
        }

        protected int getIntParam(String paramString, int paramInt)
        {
            if (this.mParams == null);
            while (true)
            {
                return paramInt;
                paramInt = this.mParams.getInt(paramString, paramInt);
            }
        }

        public float getPan()
        {
            return getFloatParam("pan", 0.0F);
        }

        public int getStreamType()
        {
            return getIntParam("streamType", 3);
        }

        protected String getStringParam(String paramString1, String paramString2)
        {
            if (this.mParams == null);
            while (true)
            {
                return paramString2;
                paramString2 = this.mParams.getString(paramString1, paramString2);
            }
        }

        public String getUtteranceId()
        {
            return getStringParam("utteranceId", null);
        }

        public float getVolume()
        {
            return getFloatParam("volume", 1.0F);
        }

        /** @deprecated */
        protected boolean isStopped()
        {
            try
            {
                boolean bool = this.mStopped;
                return bool;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public abstract boolean isValid();

        // ERROR //
        public int play()
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: getfield 30	android/speech/tts/TextToSpeechService$SpeechItem:mStarted	Z
            //     6: ifeq +18 -> 24
            //     9: new 108	java/lang/IllegalStateException
            //     12: dup
            //     13: ldc 110
            //     15: invokespecial 113	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     18: athrow
            //     19: astore_1
            //     20: aload_0
            //     21: monitorexit
            //     22: aload_1
            //     23: athrow
            //     24: aload_0
            //     25: iconst_1
            //     26: putfield 30	android/speech/tts/TextToSpeechService$SpeechItem:mStarted	Z
            //     29: aload_0
            //     30: monitorexit
            //     31: aload_0
            //     32: invokevirtual 116	android/speech/tts/TextToSpeechService$SpeechItem:playImpl	()I
            //     35: ireturn
            //
            // Exception table:
            //     from	to	target	type
            //     2	22	19	finally
            //     24	31	19	finally
        }

        protected abstract int playImpl();

        // ERROR //
        public void stop()
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: getfield 32	android/speech/tts/TextToSpeechService$SpeechItem:mStopped	Z
            //     6: ifeq +18 -> 24
            //     9: new 108	java/lang/IllegalStateException
            //     12: dup
            //     13: ldc 119
            //     15: invokespecial 113	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     18: athrow
            //     19: astore_1
            //     20: aload_0
            //     21: monitorexit
            //     22: aload_1
            //     23: athrow
            //     24: aload_0
            //     25: iconst_1
            //     26: putfield 32	android/speech/tts/TextToSpeechService$SpeechItem:mStopped	Z
            //     29: aload_0
            //     30: monitorexit
            //     31: aload_0
            //     32: invokevirtual 122	android/speech/tts/TextToSpeechService$SpeechItem:stopImpl	()V
            //     35: return
            //
            // Exception table:
            //     from	to	target	type
            //     2	22	19	finally
            //     24	31	19	finally
        }

        protected abstract void stopImpl();
    }

    static abstract interface UtteranceProgressDispatcher
    {
        public abstract void dispatchOnDone();

        public abstract void dispatchOnError();

        public abstract void dispatchOnStart();
    }

    private class SynthHandler extends Handler
    {
        private TextToSpeechService.SpeechItem mCurrentSpeechItem = null;

        public SynthHandler(Looper arg2)
        {
            super();
        }

        /** @deprecated */
        private TextToSpeechService.SpeechItem getCurrentSpeechItem()
        {
            try
            {
                TextToSpeechService.SpeechItem localSpeechItem = this.mCurrentSpeechItem;
                return localSpeechItem;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        private TextToSpeechService.SpeechItem maybeRemoveCurrentSpeechItem(Object paramObject)
        {
            TextToSpeechService.SpeechItem localSpeechItem = null;
            try
            {
                if ((this.mCurrentSpeechItem != null) && (this.mCurrentSpeechItem.getCallerIdentity() == paramObject))
                {
                    localSpeechItem = this.mCurrentSpeechItem;
                    this.mCurrentSpeechItem = null;
                }
                return localSpeechItem;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        private TextToSpeechService.SpeechItem setCurrentSpeechItem(TextToSpeechService.SpeechItem paramSpeechItem)
        {
            try
            {
                TextToSpeechService.SpeechItem localSpeechItem = this.mCurrentSpeechItem;
                this.mCurrentSpeechItem = paramSpeechItem;
                return localSpeechItem;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public int enqueueSpeechItem(int paramInt, final TextToSpeechService.SpeechItem paramSpeechItem)
        {
            int i = -1;
            if (!paramSpeechItem.isValid())
                paramSpeechItem.dispatchOnError();
            while (true)
            {
                return i;
                if (paramInt == 0)
                    stopForApp(paramSpeechItem.getCallerIdentity());
                while (true)
                {
                    Message localMessage = Message.obtain(this, new Runnable()
                    {
                        public void run()
                        {
                            TextToSpeechService.SynthHandler.this.setCurrentSpeechItem(paramSpeechItem);
                            paramSpeechItem.play();
                            TextToSpeechService.SynthHandler.this.setCurrentSpeechItem(null);
                        }
                    });
                    localMessage.obj = paramSpeechItem.getCallerIdentity();
                    if (!sendMessage(localMessage))
                        break label80;
                    i = 0;
                    break;
                    if (paramInt == 2)
                        stopAll();
                }
                label80: Log.w("TextToSpeechService", "SynthThread has quit");
                paramSpeechItem.dispatchOnError();
            }
        }

        public boolean isSpeaking()
        {
            if (getCurrentSpeechItem() != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void quit()
        {
            getLooper().quit();
            TextToSpeechService.SpeechItem localSpeechItem = setCurrentSpeechItem(null);
            if (localSpeechItem != null)
                localSpeechItem.stop();
        }

        public int stopAll()
        {
            TextToSpeechService.SpeechItem localSpeechItem = setCurrentSpeechItem(null);
            if (localSpeechItem != null)
                localSpeechItem.stop();
            removeCallbacksAndMessages(null);
            TextToSpeechService.this.mAudioPlaybackHandler.stop();
            return 0;
        }

        public int stopForApp(Object paramObject)
        {
            if (paramObject == null);
            for (int i = -1; ; i = 0)
            {
                return i;
                removeCallbacksAndMessages(paramObject);
                TextToSpeechService.SpeechItem localSpeechItem = maybeRemoveCurrentSpeechItem(paramObject);
                if (localSpeechItem != null)
                    localSpeechItem.stop();
                TextToSpeechService.this.mAudioPlaybackHandler.stopForApp(paramObject);
            }
        }
    }

    private class SynthThread extends HandlerThread
        implements MessageQueue.IdleHandler
    {
        private boolean mFirstIdle = true;

        public SynthThread()
        {
            super(0);
        }

        private void broadcastTtsQueueProcessingCompleted()
        {
            Intent localIntent = new Intent("android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED");
            TextToSpeechService.this.sendBroadcast(localIntent);
        }

        protected void onLooperPrepared()
        {
            getLooper().getQueue().addIdleHandler(this);
        }

        public boolean queueIdle()
        {
            if (this.mFirstIdle)
                this.mFirstIdle = false;
            while (true)
            {
                return true;
                broadcastTtsQueueProcessingCompleted();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.TextToSpeechService
 * JD-Core Version:        0.6.2
 */