package android.speech.tts;

import android.util.Log;

class PlaybackSynthesisCallback extends AbstractSynthesisCallback
{
    private static final boolean DBG = false;
    private static final int MIN_AUDIO_BUFFER_SIZE = 8192;
    private static final String TAG = "PlaybackSynthesisRequest";
    private final AudioPlaybackHandler mAudioTrackHandler;
    private final Object mCallerIdentity;
    private final TextToSpeechService.UtteranceProgressDispatcher mDispatcher;
    private volatile boolean mDone = false;
    private SynthesisPlaybackQueueItem mItem = null;
    private final EventLogger mLogger;
    private final float mPan;
    private final Object mStateLock = new Object();
    private boolean mStopped = false;
    private final int mStreamType;
    private final float mVolume;

    PlaybackSynthesisCallback(int paramInt, float paramFloat1, float paramFloat2, AudioPlaybackHandler paramAudioPlaybackHandler, TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject, EventLogger paramEventLogger)
    {
        this.mStreamType = paramInt;
        this.mVolume = paramFloat1;
        this.mPan = paramFloat2;
        this.mAudioTrackHandler = paramAudioPlaybackHandler;
        this.mDispatcher = paramUtteranceProgressDispatcher;
        this.mCallerIdentity = paramObject;
        this.mLogger = paramEventLogger;
    }

    public int audioAvailable(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        int i = 0;
        if ((paramInt2 > getMaxBufferSize()) || (paramInt2 <= 0))
            throw new IllegalArgumentException("buffer is too large or of zero length (" + paramInt2 + " bytes)");
        SynthesisPlaybackQueueItem localSynthesisPlaybackQueueItem;
        byte[] arrayOfByte;
        synchronized (this.mStateLock)
        {
            if ((this.mItem == null) || (this.mStopped))
            {
                i = -1;
                break label134;
            }
            localSynthesisPlaybackQueueItem = this.mItem;
            arrayOfByte = new byte[paramInt2];
            System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, 0, paramInt2);
        }
        try
        {
            localSynthesisPlaybackQueueItem.put(arrayOfByte);
            this.mLogger.onEngineDataReceived();
            break label134;
            localObject2 = finally;
            throw localObject2;
        }
        catch (InterruptedException localInterruptedException)
        {
            i = -1;
        }
        label134: return i;
    }

    // ERROR //
    public int done()
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore_1
        //     3: aload_0
        //     4: getfield 41	android/speech/tts/PlaybackSynthesisCallback:mStateLock	Ljava/lang/Object;
        //     7: astore_2
        //     8: aload_2
        //     9: monitorenter
        //     10: aload_0
        //     11: getfield 47	android/speech/tts/PlaybackSynthesisCallback:mDone	Z
        //     14: ifeq +16 -> 30
        //     17: ldc 14
        //     19: ldc 112
        //     21: invokestatic 118	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     24: pop
        //     25: aload_2
        //     26: monitorexit
        //     27: goto +47 -> 74
        //     30: aload_0
        //     31: iconst_1
        //     32: putfield 47	android/speech/tts/PlaybackSynthesisCallback:mDone	Z
        //     35: aload_0
        //     36: getfield 43	android/speech/tts/PlaybackSynthesisCallback:mItem	Landroid/speech/tts/SynthesisPlaybackQueueItem;
        //     39: ifnonnull +13 -> 52
        //     42: aload_2
        //     43: monitorexit
        //     44: goto +30 -> 74
        //     47: astore_3
        //     48: aload_2
        //     49: monitorexit
        //     50: aload_3
        //     51: athrow
        //     52: aload_0
        //     53: getfield 43	android/speech/tts/PlaybackSynthesisCallback:mItem	Landroid/speech/tts/SynthesisPlaybackQueueItem;
        //     56: astore 4
        //     58: aload_2
        //     59: monitorexit
        //     60: aload 4
        //     62: invokevirtual 120	android/speech/tts/SynthesisPlaybackQueueItem:done	()V
        //     65: aload_0
        //     66: getfield 61	android/speech/tts/PlaybackSynthesisCallback:mLogger	Landroid/speech/tts/EventLogger;
        //     69: invokevirtual 123	android/speech/tts/EventLogger:onEngineComplete	()V
        //     72: iconst_0
        //     73: istore_1
        //     74: iload_1
        //     75: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     10	50	47	finally
        //     52	60	47	finally
    }

    public void error()
    {
        this.mLogger.onError();
        stopImpl(true);
    }

    public int getMaxBufferSize()
    {
        return 8192;
    }

    boolean isDone()
    {
        return this.mDone;
    }

    // ERROR //
    public int start(int paramInt1, int paramInt2, int paramInt3)
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore 4
        //     4: iload_3
        //     5: invokestatic 141	android/speech/tts/BlockingAudioTrack:getChannelConfig	(I)I
        //     8: ifne +31 -> 39
        //     11: ldc 14
        //     13: new 73	java/lang/StringBuilder
        //     16: dup
        //     17: invokespecial 74	java/lang/StringBuilder:<init>	()V
        //     20: ldc 143
        //     22: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     25: iload_3
        //     26: invokevirtual 83	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     29: invokevirtual 89	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     32: invokestatic 146	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     35: pop
        //     36: iload 4
        //     38: ireturn
        //     39: aload_0
        //     40: getfield 41	android/speech/tts/PlaybackSynthesisCallback:mStateLock	Ljava/lang/Object;
        //     43: astore 5
        //     45: aload 5
        //     47: monitorenter
        //     48: aload_0
        //     49: getfield 45	android/speech/tts/PlaybackSynthesisCallback:mStopped	Z
        //     52: ifeq +17 -> 69
        //     55: aload 5
        //     57: monitorexit
        //     58: goto -22 -> 36
        //     61: astore 6
        //     63: aload 5
        //     65: monitorexit
        //     66: aload 6
        //     68: athrow
        //     69: new 100	android/speech/tts/SynthesisPlaybackQueueItem
        //     72: dup
        //     73: aload_0
        //     74: getfield 49	android/speech/tts/PlaybackSynthesisCallback:mStreamType	I
        //     77: iload_1
        //     78: iload_2
        //     79: iload_3
        //     80: aload_0
        //     81: getfield 51	android/speech/tts/PlaybackSynthesisCallback:mVolume	F
        //     84: aload_0
        //     85: getfield 53	android/speech/tts/PlaybackSynthesisCallback:mPan	F
        //     88: aload_0
        //     89: getfield 57	android/speech/tts/PlaybackSynthesisCallback:mDispatcher	Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;
        //     92: aload_0
        //     93: getfield 59	android/speech/tts/PlaybackSynthesisCallback:mCallerIdentity	Ljava/lang/Object;
        //     96: aload_0
        //     97: getfield 61	android/speech/tts/PlaybackSynthesisCallback:mLogger	Landroid/speech/tts/EventLogger;
        //     100: invokespecial 149	android/speech/tts/SynthesisPlaybackQueueItem:<init>	(IIIIFFLandroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;Ljava/lang/Object;Landroid/speech/tts/EventLogger;)V
        //     103: astore 7
        //     105: aload_0
        //     106: getfield 55	android/speech/tts/PlaybackSynthesisCallback:mAudioTrackHandler	Landroid/speech/tts/AudioPlaybackHandler;
        //     109: aload 7
        //     111: invokevirtual 155	android/speech/tts/AudioPlaybackHandler:enqueue	(Landroid/speech/tts/PlaybackQueueItem;)V
        //     114: aload_0
        //     115: aload 7
        //     117: putfield 43	android/speech/tts/PlaybackSynthesisCallback:mItem	Landroid/speech/tts/SynthesisPlaybackQueueItem;
        //     120: aload 5
        //     122: monitorexit
        //     123: iconst_0
        //     124: istore 4
        //     126: goto -90 -> 36
        //
        // Exception table:
        //     from	to	target	type
        //     48	66	61	finally
        //     69	123	61	finally
    }

    void stop()
    {
        stopImpl(false);
    }

    void stopImpl(boolean paramBoolean)
    {
        this.mLogger.onStopped();
        synchronized (this.mStateLock)
        {
            if (this.mStopped)
            {
                Log.w("PlaybackSynthesisRequest", "stop() called twice");
            }
            else
            {
                SynthesisPlaybackQueueItem localSynthesisPlaybackQueueItem = this.mItem;
                this.mStopped = true;
                if (localSynthesisPlaybackQueueItem != null)
                    localSynthesisPlaybackQueueItem.stop(paramBoolean);
            }
        }
        this.mLogger.onWriteData();
        if (paramBoolean)
            this.mDispatcher.dispatchOnError();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.PlaybackSynthesisCallback
 * JD-Core Version:        0.6.2
 */