package android.speech.tts;

import android.os.FileUtils;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

class FileSynthesisCallback extends AbstractSynthesisCallback
{
    private static final boolean DBG = false;
    private static final int MAX_AUDIO_BUFFER_SIZE = 8192;
    private static final String TAG = "FileSynthesisRequest";
    private static final short WAV_FORMAT_PCM = 1;
    private static final int WAV_HEADER_LENGTH = 44;
    private int mAudioFormat;
    private int mChannelCount;
    private boolean mDone = false;
    private RandomAccessFile mFile;
    private final File mFileName;
    private int mSampleRateInHz;
    private final Object mStateLock = new Object();
    private boolean mStopped = false;

    FileSynthesisCallback(File paramFile)
    {
        this.mFileName = paramFile;
    }

    private void cleanUp()
    {
        closeFileAndWidenPermissions();
        if (this.mFile != null)
            this.mFileName.delete();
    }

    private void closeFileAndWidenPermissions()
    {
        try
        {
            if (this.mFile != null)
            {
                this.mFile.close();
                this.mFile = null;
            }
        }
        catch (IOException localIOException)
        {
            try
            {
                while (true)
                {
                    FileUtils.setPermissions(this.mFileName.getAbsolutePath(), 438, -1, -1);
                    return;
                    localIOException = localIOException;
                    Log.e("FileSynthesisRequest", "Failed to close " + this.mFileName + ": " + localIOException);
                }
            }
            catch (SecurityException localSecurityException)
            {
                while (true)
                    Log.e("FileSynthesisRequest", "Security exception setting rw permissions on : " + this.mFileName);
            }
        }
    }

    private byte[] makeWavHeader(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (paramInt2 == 3);
        for (int i = 1; ; i = 2)
        {
            int j = paramInt3 * (paramInt1 * i);
            short s1 = (short)(i * paramInt3);
            short s2 = (short)(i * 8);
            byte[] arrayOfByte1 = new byte[44];
            ByteBuffer localByteBuffer = ByteBuffer.wrap(arrayOfByte1);
            localByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            byte[] arrayOfByte2 = new byte[4];
            arrayOfByte2[0] = 82;
            arrayOfByte2[1] = 73;
            arrayOfByte2[2] = 70;
            arrayOfByte2[3] = 70;
            localByteBuffer.put(arrayOfByte2);
            localByteBuffer.putInt(-8 + (paramInt4 + 44));
            byte[] arrayOfByte3 = new byte[4];
            arrayOfByte3[0] = 87;
            arrayOfByte3[1] = 65;
            arrayOfByte3[2] = 86;
            arrayOfByte3[3] = 69;
            localByteBuffer.put(arrayOfByte3);
            byte[] arrayOfByte4 = new byte[4];
            arrayOfByte4[0] = 102;
            arrayOfByte4[1] = 109;
            arrayOfByte4[2] = 116;
            arrayOfByte4[3] = 32;
            localByteBuffer.put(arrayOfByte4);
            localByteBuffer.putInt(16);
            localByteBuffer.putShort((short)1);
            localByteBuffer.putShort((short)paramInt3);
            localByteBuffer.putInt(paramInt1);
            localByteBuffer.putInt(j);
            localByteBuffer.putShort(s1);
            localByteBuffer.putShort(s2);
            byte[] arrayOfByte5 = new byte[4];
            arrayOfByte5[0] = 100;
            arrayOfByte5[1] = 97;
            arrayOfByte5[2] = 116;
            arrayOfByte5[3] = 97;
            localByteBuffer.put(arrayOfByte5);
            localByteBuffer.putInt(paramInt4);
            return arrayOfByte1;
        }
    }

    // ERROR //
    public int audioAvailable(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore 4
        //     4: aload_0
        //     5: getfield 40	android/speech/tts/FileSynthesisCallback:mStateLock	Ljava/lang/Object;
        //     8: astore 5
        //     10: aload 5
        //     12: monitorenter
        //     13: aload_0
        //     14: getfield 42	android/speech/tts/FileSynthesisCallback:mStopped	Z
        //     17: ifeq +9 -> 26
        //     20: aload 5
        //     22: monitorexit
        //     23: goto +98 -> 121
        //     26: aload_0
        //     27: getfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     30: ifnonnull +25 -> 55
        //     33: ldc 14
        //     35: ldc 148
        //     37: invokestatic 100	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     40: pop
        //     41: aload 5
        //     43: monitorexit
        //     44: goto +77 -> 121
        //     47: astore 6
        //     49: aload 5
        //     51: monitorexit
        //     52: aload 6
        //     54: athrow
        //     55: aload_0
        //     56: getfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     59: aload_1
        //     60: iload_2
        //     61: iload_3
        //     62: invokevirtual 152	java/io/RandomAccessFile:write	([BII)V
        //     65: iconst_0
        //     66: istore 4
        //     68: aload 5
        //     70: monitorexit
        //     71: goto +50 -> 121
        //     74: astore 7
        //     76: ldc 14
        //     78: new 79	java/lang/StringBuilder
        //     81: dup
        //     82: invokespecial 80	java/lang/StringBuilder:<init>	()V
        //     85: ldc 154
        //     87: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     90: aload_0
        //     91: getfield 46	android/speech/tts/FileSynthesisCallback:mFileName	Ljava/io/File;
        //     94: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     97: ldc 91
        //     99: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     102: aload 7
        //     104: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     107: invokevirtual 94	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     110: invokestatic 100	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     113: pop
        //     114: aload_0
        //     115: invokespecial 156	android/speech/tts/FileSynthesisCallback:cleanUp	()V
        //     118: aload 5
        //     120: monitorexit
        //     121: iload 4
        //     123: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     13	52	47	finally
        //     55	65	47	finally
        //     68	121	47	finally
        //     55	65	74	java/io/IOException
    }

    // ERROR //
    public int done()
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore_1
        //     3: aload_0
        //     4: getfield 40	android/speech/tts/FileSynthesisCallback:mStateLock	Ljava/lang/Object;
        //     7: astore_2
        //     8: aload_2
        //     9: monitorenter
        //     10: aload_0
        //     11: getfield 42	android/speech/tts/FileSynthesisCallback:mStopped	Z
        //     14: ifeq +8 -> 22
        //     17: aload_2
        //     18: monitorexit
        //     19: goto +137 -> 156
        //     22: aload_0
        //     23: getfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     26: ifnonnull +21 -> 47
        //     29: ldc 14
        //     31: ldc 148
        //     33: invokestatic 100	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     36: pop
        //     37: aload_2
        //     38: monitorexit
        //     39: goto +117 -> 156
        //     42: astore_3
        //     43: aload_2
        //     44: monitorexit
        //     45: aload_3
        //     46: athrow
        //     47: aload_0
        //     48: getfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     51: lconst_0
        //     52: invokevirtual 162	java/io/RandomAccessFile:seek	(J)V
        //     55: aload_0
        //     56: getfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     59: invokevirtual 166	java/io/RandomAccessFile:length	()J
        //     62: ldc2_w 167
        //     65: lsub
        //     66: l2i
        //     67: istore 6
        //     69: aload_0
        //     70: getfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     73: aload_0
        //     74: aload_0
        //     75: getfield 170	android/speech/tts/FileSynthesisCallback:mSampleRateInHz	I
        //     78: aload_0
        //     79: getfield 172	android/speech/tts/FileSynthesisCallback:mAudioFormat	I
        //     82: aload_0
        //     83: getfield 174	android/speech/tts/FileSynthesisCallback:mChannelCount	I
        //     86: iload 6
        //     88: invokespecial 176	android/speech/tts/FileSynthesisCallback:makeWavHeader	(IIII)[B
        //     91: invokevirtual 179	java/io/RandomAccessFile:write	([B)V
        //     94: aload_0
        //     95: invokespecial 50	android/speech/tts/FileSynthesisCallback:closeFileAndWidenPermissions	()V
        //     98: aload_0
        //     99: iconst_1
        //     100: putfield 44	android/speech/tts/FileSynthesisCallback:mDone	Z
        //     103: iconst_0
        //     104: istore_1
        //     105: aload_2
        //     106: monitorexit
        //     107: goto +49 -> 156
        //     110: astore 4
        //     112: ldc 14
        //     114: new 79	java/lang/StringBuilder
        //     117: dup
        //     118: invokespecial 80	java/lang/StringBuilder:<init>	()V
        //     121: ldc 154
        //     123: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     126: aload_0
        //     127: getfield 46	android/speech/tts/FileSynthesisCallback:mFileName	Ljava/io/File;
        //     130: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     133: ldc 91
        //     135: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: aload 4
        //     140: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     143: invokevirtual 94	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     146: invokestatic 100	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     149: pop
        //     150: aload_0
        //     151: invokespecial 156	android/speech/tts/FileSynthesisCallback:cleanUp	()V
        //     154: aload_2
        //     155: monitorexit
        //     156: iload_1
        //     157: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     10	45	42	finally
        //     47	103	42	finally
        //     105	156	42	finally
        //     47	103	110	java/io/IOException
    }

    public void error()
    {
        synchronized (this.mStateLock)
        {
            cleanUp();
            return;
        }
    }

    public int getMaxBufferSize()
    {
        return 8192;
    }

    boolean isDone()
    {
        return this.mDone;
    }

    // ERROR //
    public int start(int paramInt1, int paramInt2, int paramInt3)
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore 4
        //     4: aload_0
        //     5: getfield 40	android/speech/tts/FileSynthesisCallback:mStateLock	Ljava/lang/Object;
        //     8: astore 5
        //     10: aload 5
        //     12: monitorenter
        //     13: aload_0
        //     14: getfield 42	android/speech/tts/FileSynthesisCallback:mStopped	Z
        //     17: ifeq +9 -> 26
        //     20: aload 5
        //     22: monitorexit
        //     23: goto +131 -> 154
        //     26: aload_0
        //     27: getfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     30: ifnull +25 -> 55
        //     33: aload_0
        //     34: invokespecial 156	android/speech/tts/FileSynthesisCallback:cleanUp	()V
        //     37: new 186	java/lang/IllegalArgumentException
        //     40: dup
        //     41: ldc 188
        //     43: invokespecial 191	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     46: athrow
        //     47: astore 6
        //     49: aload 5
        //     51: monitorexit
        //     52: aload 6
        //     54: athrow
        //     55: aload_0
        //     56: iload_1
        //     57: putfield 170	android/speech/tts/FileSynthesisCallback:mSampleRateInHz	I
        //     60: aload_0
        //     61: iload_2
        //     62: putfield 172	android/speech/tts/FileSynthesisCallback:mAudioFormat	I
        //     65: aload_0
        //     66: iload_3
        //     67: putfield 174	android/speech/tts/FileSynthesisCallback:mChannelCount	I
        //     70: aload_0
        //     71: new 64	java/io/RandomAccessFile
        //     74: dup
        //     75: aload_0
        //     76: getfield 46	android/speech/tts/FileSynthesisCallback:mFileName	Ljava/io/File;
        //     79: ldc 193
        //     81: invokespecial 196	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     84: putfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     87: aload_0
        //     88: getfield 52	android/speech/tts/FileSynthesisCallback:mFile	Ljava/io/RandomAccessFile;
        //     91: bipush 44
        //     93: newarray byte
        //     95: invokevirtual 179	java/io/RandomAccessFile:write	([B)V
        //     98: iconst_0
        //     99: istore 4
        //     101: aload 5
        //     103: monitorexit
        //     104: goto +50 -> 154
        //     107: astore 7
        //     109: ldc 14
        //     111: new 79	java/lang/StringBuilder
        //     114: dup
        //     115: invokespecial 80	java/lang/StringBuilder:<init>	()V
        //     118: ldc 198
        //     120: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     123: aload_0
        //     124: getfield 46	android/speech/tts/FileSynthesisCallback:mFileName	Ljava/io/File;
        //     127: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     130: ldc 91
        //     132: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     135: aload 7
        //     137: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     140: invokevirtual 94	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     143: invokestatic 100	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     146: pop
        //     147: aload_0
        //     148: invokespecial 156	android/speech/tts/FileSynthesisCallback:cleanUp	()V
        //     151: aload 5
        //     153: monitorexit
        //     154: iload 4
        //     156: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     13	52	47	finally
        //     55	70	47	finally
        //     70	98	47	finally
        //     101	154	47	finally
        //     70	98	107	java/io/IOException
    }

    void stop()
    {
        synchronized (this.mStateLock)
        {
            this.mStopped = true;
            cleanUp();
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.FileSynthesisCallback
 * JD-Core Version:        0.6.2
 */