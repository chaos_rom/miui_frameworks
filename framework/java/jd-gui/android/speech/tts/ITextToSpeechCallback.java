package android.speech.tts;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface ITextToSpeechCallback extends IInterface
{
    public abstract void onDone(String paramString)
        throws RemoteException;

    public abstract void onError(String paramString)
        throws RemoteException;

    public abstract void onStart(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ITextToSpeechCallback
    {
        private static final String DESCRIPTOR = "android.speech.tts.ITextToSpeechCallback";
        static final int TRANSACTION_onDone = 2;
        static final int TRANSACTION_onError = 3;
        static final int TRANSACTION_onStart = 1;

        public Stub()
        {
            attachInterface(this, "android.speech.tts.ITextToSpeechCallback");
        }

        public static ITextToSpeechCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.speech.tts.ITextToSpeechCallback");
                if ((localIInterface != null) && ((localIInterface instanceof ITextToSpeechCallback)))
                    localObject = (ITextToSpeechCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.speech.tts.ITextToSpeechCallback");
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechCallback");
                onStart(paramParcel1.readString());
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechCallback");
                onDone(paramParcel1.readString());
                continue;
                paramParcel1.enforceInterface("android.speech.tts.ITextToSpeechCallback");
                onError(paramParcel1.readString());
            }
        }

        private static class Proxy
            implements ITextToSpeechCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.speech.tts.ITextToSpeechCallback";
            }

            public void onDone(String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
                    localParcel.writeString(paramString);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onError(String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
                    localParcel.writeString(paramString);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onStart(String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.tts.ITextToSpeechCallback");
                    localParcel.writeString(paramString);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.ITextToSpeechCallback
 * JD-Core Version:        0.6.2
 */