package android.speech.tts;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

class AudioPlaybackHandler
{
    private static final boolean DBG = false;
    private static final String TAG = "TTS.AudioPlaybackHandler";
    private volatile PlaybackQueueItem mCurrentWorkItem = null;
    private final Thread mHandlerThread = new Thread(new MessageLoop(null), "TTS.AudioPlaybackThread");
    private final LinkedBlockingQueue<PlaybackQueueItem> mQueue = new LinkedBlockingQueue();

    private void removeAllMessages()
    {
        this.mQueue.clear();
    }

    private void removeWorkItemsFor(Object paramObject)
    {
        Iterator localIterator = this.mQueue.iterator();
        while (localIterator.hasNext())
            if (((PlaybackQueueItem)localIterator.next()).getCallerIdentity() == paramObject)
                localIterator.remove();
    }

    private void stop(PlaybackQueueItem paramPlaybackQueueItem)
    {
        if (paramPlaybackQueueItem == null);
        while (true)
        {
            return;
            paramPlaybackQueueItem.stop(false);
        }
    }

    public void enqueue(PlaybackQueueItem paramPlaybackQueueItem)
    {
        try
        {
            this.mQueue.put(paramPlaybackQueueItem);
            label8: return;
        }
        catch (InterruptedException localInterruptedException)
        {
            break label8;
        }
    }

    public boolean isSpeaking()
    {
        if ((this.mQueue.peek() != null) || (this.mCurrentWorkItem != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void quit()
    {
        removeAllMessages();
        stop(this.mCurrentWorkItem);
        this.mHandlerThread.interrupt();
    }

    public void start()
    {
        this.mHandlerThread.start();
    }

    public void stop()
    {
        removeAllMessages();
        stop(this.mCurrentWorkItem);
    }

    public void stopForApp(Object paramObject)
    {
        removeWorkItemsFor(paramObject);
        PlaybackQueueItem localPlaybackQueueItem = this.mCurrentWorkItem;
        if ((localPlaybackQueueItem != null) && (localPlaybackQueueItem.getCallerIdentity() == paramObject))
            stop(localPlaybackQueueItem);
    }

    private final class MessageLoop
        implements Runnable
    {
        private MessageLoop()
        {
        }

        public void run()
        {
            try
            {
                while (true)
                {
                    PlaybackQueueItem localPlaybackQueueItem = (PlaybackQueueItem)AudioPlaybackHandler.this.mQueue.take();
                    AudioPlaybackHandler.access$202(AudioPlaybackHandler.this, localPlaybackQueueItem);
                    localPlaybackQueueItem.run();
                    AudioPlaybackHandler.access$202(AudioPlaybackHandler.this, null);
                }
            }
            catch (InterruptedException localInterruptedException)
            {
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.tts.AudioPlaybackHandler
 * JD-Core Version:        0.6.2
 */