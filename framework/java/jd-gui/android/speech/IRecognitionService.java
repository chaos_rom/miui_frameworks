package android.speech;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IRecognitionService extends IInterface
{
    public abstract void cancel(IRecognitionListener paramIRecognitionListener)
        throws RemoteException;

    public abstract void startListening(Intent paramIntent, IRecognitionListener paramIRecognitionListener)
        throws RemoteException;

    public abstract void stopListening(IRecognitionListener paramIRecognitionListener)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRecognitionService
    {
        private static final String DESCRIPTOR = "android.speech.IRecognitionService";
        static final int TRANSACTION_cancel = 3;
        static final int TRANSACTION_startListening = 1;
        static final int TRANSACTION_stopListening = 2;

        public Stub()
        {
            attachInterface(this, "android.speech.IRecognitionService");
        }

        public static IRecognitionService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.speech.IRecognitionService");
                if ((localIInterface != null) && ((localIInterface instanceof IRecognitionService)))
                    localObject = (IRecognitionService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.speech.IRecognitionService");
                continue;
                paramParcel1.enforceInterface("android.speech.IRecognitionService");
                if (paramParcel1.readInt() != 0);
                for (Intent localIntent = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent = null)
                {
                    startListening(localIntent, IRecognitionListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    break;
                }
                paramParcel1.enforceInterface("android.speech.IRecognitionService");
                stopListening(IRecognitionListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                continue;
                paramParcel1.enforceInterface("android.speech.IRecognitionService");
                cancel(IRecognitionListener.Stub.asInterface(paramParcel1.readStrongBinder()));
            }
        }

        private static class Proxy
            implements IRecognitionService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancel(IRecognitionListener paramIRecognitionListener)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionService");
                    if (paramIRecognitionListener != null)
                        localIBinder = paramIRecognitionListener.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.speech.IRecognitionService";
            }

            public void startListening(Intent paramIntent, IRecognitionListener paramIRecognitionListener)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionService");
                    if (paramIntent != null)
                    {
                        localParcel.writeInt(1);
                        paramIntent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        if (paramIRecognitionListener != null)
                            localIBinder = paramIRecognitionListener.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void stopListening(IRecognitionListener paramIRecognitionListener)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.speech.IRecognitionService");
                    if (paramIRecognitionListener != null)
                        localIBinder = paramIRecognitionListener.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.IRecognitionService
 * JD-Core Version:        0.6.2
 */