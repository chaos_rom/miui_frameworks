package android.speech;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

public abstract class RecognitionService extends Service
{
    private static final boolean DBG = false;
    private static final int MSG_CANCEL = 3;
    private static final int MSG_RESET = 4;
    private static final int MSG_START_LISTENING = 1;
    private static final int MSG_STOP_LISTENING = 2;
    public static final String SERVICE_INTERFACE = "android.speech.RecognitionService";
    public static final String SERVICE_META_DATA = "android.speech";
    private static final String TAG = "RecognitionService";
    private RecognitionServiceBinder mBinder = new RecognitionServiceBinder(this);
    private Callback mCurrentCallback = null;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return;
                RecognitionService.StartListeningArgs localStartListeningArgs = (RecognitionService.StartListeningArgs)paramAnonymousMessage.obj;
                RecognitionService.this.dispatchStartListening(localStartListeningArgs.mIntent, localStartListeningArgs.mListener);
                continue;
                RecognitionService.this.dispatchStopListening((IRecognitionListener)paramAnonymousMessage.obj);
                continue;
                RecognitionService.this.dispatchCancel((IRecognitionListener)paramAnonymousMessage.obj);
                continue;
                RecognitionService.this.dispatchClearCallback();
            }
        }
    };

    private boolean checkPermissions(IRecognitionListener paramIRecognitionListener)
    {
        boolean bool;
        if (checkCallingOrSelfPermission("android.permission.RECORD_AUDIO") == 0)
            bool = true;
        while (true)
        {
            return bool;
            try
            {
                Log.e("RecognitionService", "call for recognition service without RECORD_AUDIO permissions");
                paramIRecognitionListener.onError(9);
                bool = false;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("RecognitionService", "sending ERROR_INSUFFICIENT_PERMISSIONS message failed", localRemoteException);
            }
        }
    }

    private void dispatchCancel(IRecognitionListener paramIRecognitionListener)
    {
        if (this.mCurrentCallback == null);
        while (true)
        {
            return;
            if (this.mCurrentCallback.mListener.asBinder() != paramIRecognitionListener.asBinder())
            {
                Log.w("RecognitionService", "cancel called by client who did not call startListening - ignoring");
            }
            else
            {
                onCancel(this.mCurrentCallback);
                this.mCurrentCallback = null;
            }
        }
    }

    private void dispatchClearCallback()
    {
        this.mCurrentCallback = null;
    }

    private void dispatchStartListening(Intent paramIntent, IRecognitionListener paramIRecognitionListener)
    {
        if (this.mCurrentCallback == null)
        {
            this.mCurrentCallback = new Callback(paramIRecognitionListener, null);
            onStartListening(paramIntent, this.mCurrentCallback);
        }
        while (true)
        {
            return;
            try
            {
                paramIRecognitionListener.onError(8);
                Log.i("RecognitionService", "concurrent startListening received - ignoring this call");
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.d("RecognitionService", "onError call from startListening failed");
            }
        }
    }

    private void dispatchStopListening(IRecognitionListener paramIRecognitionListener)
    {
        try
        {
            if (this.mCurrentCallback == null)
            {
                paramIRecognitionListener.onError(5);
                Log.w("RecognitionService", "stopListening called with no preceding startListening - ignoring");
            }
            else if (this.mCurrentCallback.mListener.asBinder() != paramIRecognitionListener.asBinder())
            {
                paramIRecognitionListener.onError(8);
                Log.w("RecognitionService", "stopListening called by other caller than startListening - ignoring");
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.d("RecognitionService", "onError call from stopListening failed");
        }
        onStopListening(this.mCurrentCallback);
    }

    public final IBinder onBind(Intent paramIntent)
    {
        return this.mBinder;
    }

    protected abstract void onCancel(Callback paramCallback);

    public void onDestroy()
    {
        this.mCurrentCallback = null;
        this.mBinder.clearReference();
        super.onDestroy();
    }

    protected abstract void onStartListening(Intent paramIntent, Callback paramCallback);

    protected abstract void onStopListening(Callback paramCallback);

    private static class RecognitionServiceBinder extends IRecognitionService.Stub
    {
        private RecognitionService mInternalService;

        public RecognitionServiceBinder(RecognitionService paramRecognitionService)
        {
            this.mInternalService = paramRecognitionService;
        }

        public void cancel(IRecognitionListener paramIRecognitionListener)
        {
            if ((this.mInternalService != null) && (this.mInternalService.checkPermissions(paramIRecognitionListener)))
                this.mInternalService.mHandler.sendMessage(Message.obtain(this.mInternalService.mHandler, 3, paramIRecognitionListener));
        }

        public void clearReference()
        {
            this.mInternalService = null;
        }

        public void startListening(Intent paramIntent, IRecognitionListener paramIRecognitionListener)
        {
            if ((this.mInternalService != null) && (this.mInternalService.checkPermissions(paramIRecognitionListener)))
            {
                Handler localHandler1 = this.mInternalService.mHandler;
                Handler localHandler2 = this.mInternalService.mHandler;
                RecognitionService localRecognitionService = this.mInternalService;
                localRecognitionService.getClass();
                localHandler1.sendMessage(Message.obtain(localHandler2, 1, new RecognitionService.StartListeningArgs(localRecognitionService, paramIntent, paramIRecognitionListener)));
            }
        }

        public void stopListening(IRecognitionListener paramIRecognitionListener)
        {
            if ((this.mInternalService != null) && (this.mInternalService.checkPermissions(paramIRecognitionListener)))
                this.mInternalService.mHandler.sendMessage(Message.obtain(this.mInternalService.mHandler, 2, paramIRecognitionListener));
        }
    }

    public class Callback
    {
        private final IRecognitionListener mListener;

        private Callback(IRecognitionListener arg2)
        {
            Object localObject;
            this.mListener = localObject;
        }

        public void beginningOfSpeech()
            throws RemoteException
        {
            this.mListener.onBeginningOfSpeech();
        }

        public void bufferReceived(byte[] paramArrayOfByte)
            throws RemoteException
        {
            this.mListener.onBufferReceived(paramArrayOfByte);
        }

        public void endOfSpeech()
            throws RemoteException
        {
            this.mListener.onEndOfSpeech();
        }

        public void error(int paramInt)
            throws RemoteException
        {
            Message.obtain(RecognitionService.this.mHandler, 4).sendToTarget();
            this.mListener.onError(paramInt);
        }

        public void partialResults(Bundle paramBundle)
            throws RemoteException
        {
            this.mListener.onPartialResults(paramBundle);
        }

        public void readyForSpeech(Bundle paramBundle)
            throws RemoteException
        {
            this.mListener.onReadyForSpeech(paramBundle);
        }

        public void results(Bundle paramBundle)
            throws RemoteException
        {
            Message.obtain(RecognitionService.this.mHandler, 4).sendToTarget();
            this.mListener.onResults(paramBundle);
        }

        public void rmsChanged(float paramFloat)
            throws RemoteException
        {
            this.mListener.onRmsChanged(paramFloat);
        }
    }

    private class StartListeningArgs
    {
        public final Intent mIntent;
        public final IRecognitionListener mListener;

        public StartListeningArgs(Intent paramIRecognitionListener, IRecognitionListener arg3)
        {
            this.mIntent = paramIRecognitionListener;
            Object localObject;
            this.mListener = localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.speech.RecognitionService
 * JD-Core Version:        0.6.2
 */