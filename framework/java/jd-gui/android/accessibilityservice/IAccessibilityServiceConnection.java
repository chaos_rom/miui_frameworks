package android.accessibilityservice;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.view.accessibility.IAccessibilityInteractionConnectionCallback;
import android.view.accessibility.IAccessibilityInteractionConnectionCallback.Stub;

public abstract interface IAccessibilityServiceConnection extends IInterface
{
    public abstract float findAccessibilityNodeInfoByAccessibilityId(int paramInt1, long paramLong1, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, long paramLong2)
        throws RemoteException;

    public abstract float findAccessibilityNodeInfoByViewId(int paramInt1, long paramLong1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
        throws RemoteException;

    public abstract float findAccessibilityNodeInfosByText(int paramInt1, long paramLong1, String paramString, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
        throws RemoteException;

    public abstract float findFocus(int paramInt1, long paramLong1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
        throws RemoteException;

    public abstract float focusSearch(int paramInt1, long paramLong1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
        throws RemoteException;

    public abstract AccessibilityServiceInfo getServiceInfo()
        throws RemoteException;

    public abstract boolean performAccessibilityAction(int paramInt1, long paramLong1, int paramInt2, Bundle paramBundle, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
        throws RemoteException;

    public abstract boolean performGlobalAction(int paramInt)
        throws RemoteException;

    public abstract void setServiceInfo(AccessibilityServiceInfo paramAccessibilityServiceInfo)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccessibilityServiceConnection
    {
        private static final String DESCRIPTOR = "android.accessibilityservice.IAccessibilityServiceConnection";
        static final int TRANSACTION_findAccessibilityNodeInfoByAccessibilityId = 2;
        static final int TRANSACTION_findAccessibilityNodeInfoByViewId = 4;
        static final int TRANSACTION_findAccessibilityNodeInfosByText = 3;
        static final int TRANSACTION_findFocus = 5;
        static final int TRANSACTION_focusSearch = 6;
        static final int TRANSACTION_getServiceInfo = 8;
        static final int TRANSACTION_performAccessibilityAction = 7;
        static final int TRANSACTION_performGlobalAction = 9;
        static final int TRANSACTION_setServiceInfo = 1;

        public Stub()
        {
            attachInterface(this, "android.accessibilityservice.IAccessibilityServiceConnection");
        }

        public static IAccessibilityServiceConnection asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                if ((localIInterface != null) && ((localIInterface instanceof IAccessibilityServiceConnection)))
                    localObject = (IAccessibilityServiceConnection)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool2;
            switch (paramInt1)
            {
            default:
                bool2 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                while (true)
                {
                    return bool2;
                    paramParcel2.writeString("android.accessibilityservice.IAccessibilityServiceConnection");
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                    if (paramParcel1.readInt() != 0);
                    for (AccessibilityServiceInfo localAccessibilityServiceInfo2 = (AccessibilityServiceInfo)AccessibilityServiceInfo.CREATOR.createFromParcel(paramParcel1); ; localAccessibilityServiceInfo2 = null)
                    {
                        setServiceInfo(localAccessibilityServiceInfo2);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                    float f5 = findAccessibilityNodeInfoByAccessibilityId(paramParcel1.readInt(), paramParcel1.readLong(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    paramParcel2.writeFloat(f5);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                    float f4 = findAccessibilityNodeInfosByText(paramParcel1.readInt(), paramParcel1.readLong(), paramParcel1.readString(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    paramParcel2.writeFloat(f4);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                    float f3 = findAccessibilityNodeInfoByViewId(paramParcel1.readInt(), paramParcel1.readLong(), paramParcel1.readInt(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    paramParcel2.writeFloat(f3);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                    float f2 = findFocus(paramParcel1.readInt(), paramParcel1.readLong(), paramParcel1.readInt(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    paramParcel2.writeFloat(f2);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                    float f1 = focusSearch(paramParcel1.readInt(), paramParcel1.readLong(), paramParcel1.readInt(), paramParcel1.readInt(), IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    paramParcel2.writeFloat(f1);
                    bool2 = true;
                }
            case 7:
                paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                int j = paramParcel1.readInt();
                long l1 = paramParcel1.readLong();
                int k = paramParcel1.readInt();
                Bundle localBundle;
                if (paramParcel1.readInt() != 0)
                {
                    localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                    int m = paramParcel1.readInt();
                    IAccessibilityInteractionConnectionCallback localIAccessibilityInteractionConnectionCallback = IAccessibilityInteractionConnectionCallback.Stub.asInterface(paramParcel1.readStrongBinder());
                    long l2 = paramParcel1.readLong();
                    boolean bool3 = performAccessibilityAction(j, l1, k, localBundle, m, localIAccessibilityInteractionConnectionCallback, l2);
                    paramParcel2.writeNoException();
                    if (!bool3)
                        break label558;
                }
                for (int n = 1; ; n = 0)
                {
                    paramParcel2.writeInt(n);
                    bool2 = true;
                    break;
                    localBundle = null;
                    break label487;
                }
            case 8:
                label487: paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
                label558: AccessibilityServiceInfo localAccessibilityServiceInfo1 = getServiceInfo();
                paramParcel2.writeNoException();
                if (localAccessibilityServiceInfo1 != null)
                {
                    paramParcel2.writeInt(1);
                    localAccessibilityServiceInfo1.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool2 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
            case 9:
            }
            paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceConnection");
            boolean bool1 = performGlobalAction(paramParcel1.readInt());
            paramParcel2.writeNoException();
            if (bool1);
            for (int i = 1; ; i = 0)
            {
                paramParcel2.writeInt(i);
                bool2 = true;
                break;
            }
        }

        private static class Proxy
            implements IAccessibilityServiceConnection
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public float findAccessibilityNodeInfoByAccessibilityId(int paramInt1, long paramLong1, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeLong(paramLong1);
                    localParcel1.writeInt(paramInt2);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt3);
                        localParcel1.writeLong(paramLong2);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        float f = localParcel2.readFloat();
                        return f;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public float findAccessibilityNodeInfoByViewId(int paramInt1, long paramLong1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeLong(paramLong1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeLong(paramLong2);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        float f = localParcel2.readFloat();
                        return f;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public float findAccessibilityNodeInfosByText(int paramInt1, long paramLong1, String paramString, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeLong(paramLong1);
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt2);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeLong(paramLong2);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        float f = localParcel2.readFloat();
                        return f;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public float findFocus(int paramInt1, long paramLong1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeLong(paramLong1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeLong(paramLong2);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        float f = localParcel2.readFloat();
                        return f;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public float focusSearch(int paramInt1, long paramLong1, int paramInt2, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeLong(paramLong1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    if (paramIAccessibilityInteractionConnectionCallback != null)
                    {
                        localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeLong(paramLong2);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        float f = localParcel2.readFloat();
                        return f;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.accessibilityservice.IAccessibilityServiceConnection";
            }

            public AccessibilityServiceInfo getServiceInfo()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localAccessibilityServiceInfo = (AccessibilityServiceInfo)AccessibilityServiceInfo.CREATOR.createFromParcel(localParcel2);
                        return localAccessibilityServiceInfo;
                    }
                    AccessibilityServiceInfo localAccessibilityServiceInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean performAccessibilityAction(int paramInt1, long paramLong1, int paramInt2, Bundle paramBundle, int paramInt3, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeLong(paramLong1);
                        localParcel1.writeInt(paramInt2);
                        if (paramBundle != null)
                        {
                            localParcel1.writeInt(1);
                            paramBundle.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt3);
                            if (paramIAccessibilityInteractionConnectionCallback != null)
                            {
                                localIBinder = paramIAccessibilityInteractionConnectionCallback.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                localParcel1.writeLong(paramLong2);
                                this.mRemote.transact(7, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label170;
                                bool = true;
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                    continue;
                    label170: boolean bool = false;
                }
            }

            public boolean performGlobalAction(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setServiceInfo(AccessibilityServiceInfo paramAccessibilityServiceInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceConnection");
                    if (paramAccessibilityServiceInfo != null)
                    {
                        localParcel1.writeInt(1);
                        paramAccessibilityServiceInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accessibilityservice.IAccessibilityServiceConnection
 * JD-Core Version:        0.6.2
 */