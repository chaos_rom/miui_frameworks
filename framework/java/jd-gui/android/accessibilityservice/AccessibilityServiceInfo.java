package android.accessibilityservice;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class AccessibilityServiceInfo
    implements Parcelable
{
    public static final Parcelable.Creator<AccessibilityServiceInfo> CREATOR = new Parcelable.Creator()
    {
        public AccessibilityServiceInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            AccessibilityServiceInfo localAccessibilityServiceInfo = new AccessibilityServiceInfo();
            localAccessibilityServiceInfo.initFromParcel(paramAnonymousParcel);
            return localAccessibilityServiceInfo;
        }

        public AccessibilityServiceInfo[] newArray(int paramAnonymousInt)
        {
            return new AccessibilityServiceInfo[paramAnonymousInt];
        }
    };
    public static final int DEFAULT = 1;
    public static final int FEEDBACK_ALL_MASK = -1;
    public static final int FEEDBACK_AUDIBLE = 4;
    public static final int FEEDBACK_GENERIC = 16;
    public static final int FEEDBACK_HAPTIC = 2;
    public static final int FEEDBACK_SPOKEN = 1;
    public static final int FEEDBACK_VISUAL = 8;
    public static final int FLAG_INCLUDE_NOT_IMPORTANT_VIEWS = 2;
    public static final int FLAG_REQUEST_TOUCH_EXPLORATION_MODE = 4;
    private static final String TAG_ACCESSIBILITY_SERVICE = "accessibility-service";
    public int eventTypes;
    public int feedbackType;
    public int flags;
    private boolean mCanRetrieveWindowContent;
    private int mDescriptionResId;
    private String mId;
    private String mNonLocalizedDescription;
    private ResolveInfo mResolveInfo;
    private String mSettingsActivityName;
    public long notificationTimeout;
    public String[] packageNames;

    public AccessibilityServiceInfo()
    {
    }

    public AccessibilityServiceInfo(ResolveInfo paramResolveInfo, Context paramContext)
        throws XmlPullParserException, IOException
    {
        ServiceInfo localServiceInfo = paramResolveInfo.serviceInfo;
        this.mId = new ComponentName(localServiceInfo.packageName, localServiceInfo.name).flattenToShortString();
        this.mResolveInfo = paramResolveInfo;
        Object localObject1 = null;
        while (true)
        {
            PackageManager localPackageManager;
            try
            {
                localPackageManager = paramContext.getPackageManager();
                XmlResourceParser localXmlResourceParser = localServiceInfo.loadXmlMetaData(localPackageManager, "android.accessibilityservice");
                localObject1 = localXmlResourceParser;
                if (localObject1 == null)
                    return;
                int i = 0;
                if ((i != 1) && (i != 2))
                {
                    i = localObject1.next();
                    continue;
                }
                if (!"accessibility-service".equals(localObject1.getName()))
                    throw new XmlPullParserException("Meta-data does not start withaccessibility-service tag");
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                throw new XmlPullParserException("Unable to create context for: " + localServiceInfo.packageName);
            }
            finally
            {
                if (localObject1 != null)
                    localObject1.close();
            }
            AttributeSet localAttributeSet = Xml.asAttributeSet(localObject1);
            TypedArray localTypedArray = localPackageManager.getResourcesForApplication(localServiceInfo.applicationInfo).obtainAttributes(localAttributeSet, R.styleable.AccessibilityService);
            this.eventTypes = localTypedArray.getInt(2, 0);
            String str = localTypedArray.getString(3);
            if (str != null)
                this.packageNames = str.split("(\\s)*,(\\s)*");
            this.feedbackType = localTypedArray.getInt(4, 0);
            this.notificationTimeout = localTypedArray.getInt(5, 0);
            this.flags = localTypedArray.getInt(6, 0);
            this.mSettingsActivityName = localTypedArray.getString(1);
            this.mCanRetrieveWindowContent = localTypedArray.getBoolean(7, false);
            TypedValue localTypedValue = localTypedArray.peekValue(0);
            if (localTypedValue != null)
            {
                this.mDescriptionResId = localTypedValue.resourceId;
                CharSequence localCharSequence = localTypedValue.coerceToString();
                if (localCharSequence != null)
                    this.mNonLocalizedDescription = localCharSequence.toString().trim();
            }
            localTypedArray.recycle();
            if (localObject1 != null)
                localObject1.close();
        }
    }

    private static void appendEventTypes(StringBuilder paramStringBuilder, int paramInt)
    {
        paramStringBuilder.append("eventTypes:");
        paramStringBuilder.append("[");
        while (paramInt != 0)
        {
            int i = 1 << Integer.numberOfTrailingZeros(paramInt);
            paramStringBuilder.append(AccessibilityEvent.eventTypeToString(i));
            paramInt &= (i ^ 0xFFFFFFFF);
            if (paramInt != 0)
                paramStringBuilder.append(", ");
        }
        paramStringBuilder.append("]");
    }

    private static void appendFeedbackTypes(StringBuilder paramStringBuilder, int paramInt)
    {
        paramStringBuilder.append("feedbackTypes:");
        paramStringBuilder.append("[");
        while (paramInt != 0)
        {
            int i = 1 << Integer.numberOfTrailingZeros(paramInt);
            paramStringBuilder.append(feedbackTypeToString(i));
            paramInt &= (i ^ 0xFFFFFFFF);
            if (paramInt != 0)
                paramStringBuilder.append(", ");
        }
        paramStringBuilder.append("]");
    }

    private static void appendFlags(StringBuilder paramStringBuilder, int paramInt)
    {
        paramStringBuilder.append("flags:");
        paramStringBuilder.append("[");
        while (paramInt != 0)
        {
            int i = 1 << Integer.numberOfTrailingZeros(paramInt);
            paramStringBuilder.append(flagToString(i));
            paramInt &= (i ^ 0xFFFFFFFF);
            if (paramInt != 0)
                paramStringBuilder.append(", ");
        }
        paramStringBuilder.append("]");
    }

    private static void appendPackageNames(StringBuilder paramStringBuilder, String[] paramArrayOfString)
    {
        paramStringBuilder.append("packageNames:");
        paramStringBuilder.append("[");
        if (paramArrayOfString != null)
        {
            int i = paramArrayOfString.length;
            for (int j = 0; j < i; j++)
            {
                paramStringBuilder.append(paramArrayOfString[j]);
                if (j < i - 1)
                    paramStringBuilder.append(", ");
            }
        }
        paramStringBuilder.append("]");
    }

    public static String feedbackTypeToString(int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("[");
        while (paramInt != 0)
        {
            int i = 1 << Integer.numberOfTrailingZeros(paramInt);
            paramInt &= (i ^ 0xFFFFFFFF);
            switch (i)
            {
            default:
                break;
            case 1:
                if (localStringBuilder.length() > 1)
                    localStringBuilder.append(", ");
                localStringBuilder.append("FEEDBACK_SPOKEN");
                break;
            case 4:
                if (localStringBuilder.length() > 1)
                    localStringBuilder.append(", ");
                localStringBuilder.append("FEEDBACK_AUDIBLE");
                break;
            case 2:
                if (localStringBuilder.length() > 1)
                    localStringBuilder.append(", ");
                localStringBuilder.append("FEEDBACK_HAPTIC");
                break;
            case 16:
                if (localStringBuilder.length() > 1)
                    localStringBuilder.append(", ");
                localStringBuilder.append("FEEDBACK_GENERIC");
                break;
            case 8:
                if (localStringBuilder.length() > 1)
                    localStringBuilder.append(", ");
                localStringBuilder.append("FEEDBACK_VISUAL");
            }
        }
        localStringBuilder.append("]");
        return localStringBuilder.toString();
    }

    public static String flagToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        case 3:
        default:
            str = null;
        case 1:
        case 2:
        case 4:
        }
        while (true)
        {
            return str;
            str = "DEFAULT";
            continue;
            str = "FLAG_INCLUDE_NOT_IMPORTANT_VIEWS";
            continue;
            str = "FLAG_REQUEST_TOUCH_EXPLORATION_MODE";
        }
    }

    private void initFromParcel(Parcel paramParcel)
    {
        this.eventTypes = paramParcel.readInt();
        this.packageNames = paramParcel.readStringArray();
        this.feedbackType = paramParcel.readInt();
        this.notificationTimeout = paramParcel.readLong();
        this.flags = paramParcel.readInt();
        this.mId = paramParcel.readString();
        this.mResolveInfo = ((ResolveInfo)paramParcel.readParcelable(null));
        this.mSettingsActivityName = paramParcel.readString();
        if (paramParcel.readInt() == 1);
        for (boolean bool = true; ; bool = false)
        {
            this.mCanRetrieveWindowContent = bool;
            this.mDescriptionResId = paramParcel.readInt();
            this.mNonLocalizedDescription = paramParcel.readString();
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean getCanRetrieveWindowContent()
    {
        return this.mCanRetrieveWindowContent;
    }

    public String getDescription()
    {
        return this.mNonLocalizedDescription;
    }

    public String getId()
    {
        return this.mId;
    }

    public ResolveInfo getResolveInfo()
    {
        return this.mResolveInfo;
    }

    public String getSettingsActivityName()
    {
        return this.mSettingsActivityName;
    }

    public String loadDescription(PackageManager paramPackageManager)
    {
        String str;
        if (this.mDescriptionResId == 0)
            str = this.mNonLocalizedDescription;
        while (true)
        {
            return str;
            ServiceInfo localServiceInfo = this.mResolveInfo.serviceInfo;
            CharSequence localCharSequence = paramPackageManager.getText(localServiceInfo.packageName, this.mDescriptionResId, localServiceInfo.applicationInfo);
            if (localCharSequence != null)
                str = localCharSequence.toString().trim();
            else
                str = null;
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        appendEventTypes(localStringBuilder, this.eventTypes);
        localStringBuilder.append(", ");
        appendPackageNames(localStringBuilder, this.packageNames);
        localStringBuilder.append(", ");
        appendFeedbackTypes(localStringBuilder, this.feedbackType);
        localStringBuilder.append(", ");
        localStringBuilder.append("notificationTimeout: ").append(this.notificationTimeout);
        localStringBuilder.append(", ");
        appendFlags(localStringBuilder, this.flags);
        localStringBuilder.append(", ");
        localStringBuilder.append("id: ").append(this.mId);
        localStringBuilder.append(", ");
        localStringBuilder.append("resolveInfo: ").append(this.mResolveInfo);
        localStringBuilder.append(", ");
        localStringBuilder.append("settingsActivityName: ").append(this.mSettingsActivityName);
        localStringBuilder.append(", ");
        localStringBuilder.append("retrieveScreenContent: ").append(this.mCanRetrieveWindowContent);
        return localStringBuilder.toString();
    }

    public void updateDynamicallyConfigurableProperties(AccessibilityServiceInfo paramAccessibilityServiceInfo)
    {
        this.eventTypes = paramAccessibilityServiceInfo.eventTypes;
        this.packageNames = paramAccessibilityServiceInfo.packageNames;
        this.feedbackType = paramAccessibilityServiceInfo.feedbackType;
        this.notificationTimeout = paramAccessibilityServiceInfo.notificationTimeout;
        this.flags = paramAccessibilityServiceInfo.flags;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 0;
        paramParcel.writeInt(this.eventTypes);
        paramParcel.writeStringArray(this.packageNames);
        paramParcel.writeInt(this.feedbackType);
        paramParcel.writeLong(this.notificationTimeout);
        paramParcel.writeInt(this.flags);
        paramParcel.writeString(this.mId);
        paramParcel.writeParcelable(this.mResolveInfo, 0);
        paramParcel.writeString(this.mSettingsActivityName);
        if (this.mCanRetrieveWindowContent)
            i = 1;
        paramParcel.writeInt(i);
        paramParcel.writeInt(this.mDescriptionResId);
        paramParcel.writeString(this.mNonLocalizedDescription);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accessibilityservice.AccessibilityServiceInfo
 * JD-Core Version:        0.6.2
 */