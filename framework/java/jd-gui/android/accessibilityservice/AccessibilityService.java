package android.accessibilityservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityInteractionClient;
import android.view.accessibility.AccessibilityNodeInfo;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.HandlerCaller.Callback;

public abstract class AccessibilityService extends Service
{
    public static final int GESTURE_SWIPE_DOWN = 2;
    public static final int GESTURE_SWIPE_DOWN_AND_LEFT = 15;
    public static final int GESTURE_SWIPE_DOWN_AND_RIGHT = 16;
    public static final int GESTURE_SWIPE_DOWN_AND_UP = 8;
    public static final int GESTURE_SWIPE_LEFT = 3;
    public static final int GESTURE_SWIPE_LEFT_AND_DOWN = 10;
    public static final int GESTURE_SWIPE_LEFT_AND_RIGHT = 5;
    public static final int GESTURE_SWIPE_LEFT_AND_UP = 9;
    public static final int GESTURE_SWIPE_RIGHT = 4;
    public static final int GESTURE_SWIPE_RIGHT_AND_DOWN = 12;
    public static final int GESTURE_SWIPE_RIGHT_AND_LEFT = 6;
    public static final int GESTURE_SWIPE_RIGHT_AND_UP = 11;
    public static final int GESTURE_SWIPE_UP = 1;
    public static final int GESTURE_SWIPE_UP_AND_DOWN = 7;
    public static final int GESTURE_SWIPE_UP_AND_LEFT = 13;
    public static final int GESTURE_SWIPE_UP_AND_RIGHT = 14;
    public static final int GLOBAL_ACTION_BACK = 1;
    public static final int GLOBAL_ACTION_HOME = 2;
    public static final int GLOBAL_ACTION_NOTIFICATIONS = 4;
    public static final int GLOBAL_ACTION_RECENTS = 3;
    private static final String LOG_TAG = "AccessibilityService";
    public static final String SERVICE_INTERFACE = "android.accessibilityservice.AccessibilityService";
    public static final String SERVICE_META_DATA = "android.accessibilityservice";
    private int mConnectionId;
    private AccessibilityServiceInfo mInfo;

    private void sendServiceInfo()
    {
        IAccessibilityServiceConnection localIAccessibilityServiceConnection = AccessibilityInteractionClient.getInstance().getConnection(this.mConnectionId);
        if ((this.mInfo != null) && (localIAccessibilityServiceConnection != null));
        try
        {
            localIAccessibilityServiceConnection.setServiceInfo(this.mInfo);
            this.mInfo = null;
            AccessibilityInteractionClient.getInstance().clearCache();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("AccessibilityService", "Error while setting AccessibilityServiceInfo", localRemoteException);
        }
    }

    public AccessibilityNodeInfo getRootInActiveWindow()
    {
        return AccessibilityInteractionClient.getInstance().getRootInActiveWindow(this.mConnectionId);
    }

    public final AccessibilityServiceInfo getServiceInfo()
    {
        IAccessibilityServiceConnection localIAccessibilityServiceConnection = AccessibilityInteractionClient.getInstance().getConnection(this.mConnectionId);
        if (localIAccessibilityServiceConnection != null);
        while (true)
        {
            try
            {
                AccessibilityServiceInfo localAccessibilityServiceInfo2 = localIAccessibilityServiceConnection.getServiceInfo();
                localAccessibilityServiceInfo1 = localAccessibilityServiceInfo2;
                return localAccessibilityServiceInfo1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("AccessibilityService", "Error while getting AccessibilityServiceInfo", localRemoteException);
            }
            AccessibilityServiceInfo localAccessibilityServiceInfo1 = null;
        }
    }

    public abstract void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent);

    public final IBinder onBind(Intent paramIntent)
    {
        return new IAccessibilityServiceClientWrapper(this, getMainLooper(), new Callbacks()
        {
            public void onAccessibilityEvent(AccessibilityEvent paramAnonymousAccessibilityEvent)
            {
                AccessibilityService.this.onAccessibilityEvent(paramAnonymousAccessibilityEvent);
            }

            public boolean onGesture(int paramAnonymousInt)
            {
                return AccessibilityService.this.onGesture(paramAnonymousInt);
            }

            public void onInterrupt()
            {
                AccessibilityService.this.onInterrupt();
            }

            public void onServiceConnected()
            {
                AccessibilityService.this.onServiceConnected();
            }

            public void onSetConnectionId(int paramAnonymousInt)
            {
                AccessibilityService.access$002(AccessibilityService.this, paramAnonymousInt);
            }
        });
    }

    protected boolean onGesture(int paramInt)
    {
        return false;
    }

    public abstract void onInterrupt();

    protected void onServiceConnected()
    {
    }

    public final boolean performGlobalAction(int paramInt)
    {
        IAccessibilityServiceConnection localIAccessibilityServiceConnection = AccessibilityInteractionClient.getInstance().getConnection(this.mConnectionId);
        if (localIAccessibilityServiceConnection != null);
        while (true)
        {
            try
            {
                boolean bool2 = localIAccessibilityServiceConnection.performGlobalAction(paramInt);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("AccessibilityService", "Error while calling performGlobalAction", localRemoteException);
            }
            boolean bool1 = false;
        }
    }

    public final void setServiceInfo(AccessibilityServiceInfo paramAccessibilityServiceInfo)
    {
        this.mInfo = paramAccessibilityServiceInfo;
        sendServiceInfo();
    }

    static class IAccessibilityServiceClientWrapper extends IAccessibilityServiceClient.Stub
        implements HandlerCaller.Callback
    {
        private static final int DO_ON_ACCESSIBILITY_EVENT = 30;
        private static final int DO_ON_GESTURE = 40;
        private static final int DO_ON_INTERRUPT = 20;
        private static final int DO_SET_SET_CONNECTION = 10;
        static final int NO_ID = -1;
        private final AccessibilityService.Callbacks mCallback;
        private final HandlerCaller mCaller;

        public IAccessibilityServiceClientWrapper(Context paramContext, Looper paramLooper, AccessibilityService.Callbacks paramCallbacks)
        {
            this.mCallback = paramCallbacks;
            this.mCaller = new HandlerCaller(paramContext, paramLooper, this);
        }

        public void executeMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                Log.w("AccessibilityService", "Unknown message type " + paramMessage.what);
            case 30:
            case 20:
            case 10:
            case 40:
            }
            while (true)
            {
                return;
                AccessibilityEvent localAccessibilityEvent = (AccessibilityEvent)paramMessage.obj;
                if (localAccessibilityEvent != null)
                {
                    AccessibilityInteractionClient.getInstance().onAccessibilityEvent(localAccessibilityEvent);
                    this.mCallback.onAccessibilityEvent(localAccessibilityEvent);
                    localAccessibilityEvent.recycle();
                    continue;
                    this.mCallback.onInterrupt();
                    continue;
                    int j = paramMessage.arg1;
                    IAccessibilityServiceConnection localIAccessibilityServiceConnection = (IAccessibilityServiceConnection)paramMessage.obj;
                    if (localIAccessibilityServiceConnection != null)
                    {
                        AccessibilityInteractionClient.getInstance().addConnection(j, localIAccessibilityServiceConnection);
                        this.mCallback.onSetConnectionId(j);
                        this.mCallback.onServiceConnected();
                    }
                    else
                    {
                        AccessibilityInteractionClient.getInstance().removeConnection(j);
                        this.mCallback.onSetConnectionId(-1);
                        continue;
                        int i = paramMessage.arg1;
                        this.mCallback.onGesture(i);
                    }
                }
            }
        }

        public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
        {
            Message localMessage = this.mCaller.obtainMessageO(30, paramAccessibilityEvent);
            this.mCaller.sendMessage(localMessage);
        }

        public void onGesture(int paramInt)
        {
            Message localMessage = this.mCaller.obtainMessageI(40, paramInt);
            this.mCaller.sendMessage(localMessage);
        }

        public void onInterrupt()
        {
            Message localMessage = this.mCaller.obtainMessage(20);
            this.mCaller.sendMessage(localMessage);
        }

        public void setConnection(IAccessibilityServiceConnection paramIAccessibilityServiceConnection, int paramInt)
        {
            Message localMessage = this.mCaller.obtainMessageIO(10, paramInt, paramIAccessibilityServiceConnection);
            this.mCaller.sendMessage(localMessage);
        }
    }

    static abstract interface Callbacks
    {
        public abstract void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent);

        public abstract boolean onGesture(int paramInt);

        public abstract void onInterrupt();

        public abstract void onServiceConnected();

        public abstract void onSetConnectionId(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accessibilityservice.AccessibilityService
 * JD-Core Version:        0.6.2
 */