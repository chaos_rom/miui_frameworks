package android.accessibilityservice;

import android.os.Bundle;
import android.os.HandlerThread;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityInteractionClient;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.IAccessibilityManager;
import android.view.accessibility.IAccessibilityManager.Stub;
import java.util.List;

public class UiTestAutomationBridge
{
    public static final int ACTIVE_WINDOW_ID = -1;
    private static final int FIND_ACCESSIBILITY_NODE_INFO_PREFETCH_FLAGS = 7;
    private static final String LOG_TAG = UiTestAutomationBridge.class.getSimpleName();
    public static final long ROOT_NODE_ID = 0L;
    private static final int TIMEOUT_REGISTER_SERVICE = 5000;
    public static final int UNDEFINED = -1;
    private volatile int mConnectionId = -1;
    private HandlerThread mHandlerThread;
    private AccessibilityEvent mLastEvent;
    private AccessibilityService.IAccessibilityServiceClientWrapper mListener;
    private final Object mLock = new Object();
    private volatile boolean mUnprocessedEventAvailable;
    private volatile boolean mWaitingForEventDelivery;

    private void ensureValidConnection(int paramInt)
    {
        if (paramInt == -1)
            throw new IllegalStateException("UiAutomationService not connected. Did you call #register()?");
    }

    // ERROR //
    public void connect()
    {
        // Byte code:
        //     0: aload_0
        //     1: invokevirtual 89	android/accessibilityservice/UiTestAutomationBridge:isConnected	()Z
        //     4: ifeq +13 -> 17
        //     7: new 75	java/lang/IllegalStateException
        //     10: dup
        //     11: ldc 91
        //     13: invokespecial 80	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: aload_0
        //     18: new 93	android/os/HandlerThread
        //     21: dup
        //     22: ldc 95
        //     24: invokespecial 96	android/os/HandlerThread:<init>	(Ljava/lang/String;)V
        //     27: putfield 98	android/accessibilityservice/UiTestAutomationBridge:mHandlerThread	Landroid/os/HandlerThread;
        //     30: aload_0
        //     31: getfield 98	android/accessibilityservice/UiTestAutomationBridge:mHandlerThread	Landroid/os/HandlerThread;
        //     34: iconst_1
        //     35: invokevirtual 102	android/os/HandlerThread:setDaemon	(Z)V
        //     38: aload_0
        //     39: getfield 98	android/accessibilityservice/UiTestAutomationBridge:mHandlerThread	Landroid/os/HandlerThread;
        //     42: invokevirtual 105	android/os/HandlerThread:start	()V
        //     45: aload_0
        //     46: new 107	android/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper
        //     49: dup
        //     50: aconst_null
        //     51: aload_0
        //     52: getfield 98	android/accessibilityservice/UiTestAutomationBridge:mHandlerThread	Landroid/os/HandlerThread;
        //     55: invokevirtual 111	android/os/HandlerThread:getLooper	()Landroid/os/Looper;
        //     58: new 6	android/accessibilityservice/UiTestAutomationBridge$1
        //     61: dup
        //     62: aload_0
        //     63: invokespecial 114	android/accessibilityservice/UiTestAutomationBridge$1:<init>	(Landroid/accessibilityservice/UiTestAutomationBridge;)V
        //     66: invokespecial 117	android/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper:<init>	(Landroid/content/Context;Landroid/os/Looper;Landroid/accessibilityservice/AccessibilityService$Callbacks;)V
        //     69: putfield 119	android/accessibilityservice/UiTestAutomationBridge:mListener	Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;
        //     72: ldc 121
        //     74: invokestatic 127	android/os/ServiceManager:getService	(Ljava/lang/String;)Landroid/os/IBinder;
        //     77: invokestatic 133	android/view/accessibility/IAccessibilityManager$Stub:asInterface	(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityManager;
        //     80: astore_1
        //     81: new 135	android/accessibilityservice/AccessibilityServiceInfo
        //     84: dup
        //     85: invokespecial 136	android/accessibilityservice/AccessibilityServiceInfo:<init>	()V
        //     88: astore_2
        //     89: aload_2
        //     90: bipush 255
        //     92: putfield 139	android/accessibilityservice/AccessibilityServiceInfo:eventTypes	I
        //     95: aload_2
        //     96: bipush 16
        //     98: putfield 142	android/accessibilityservice/AccessibilityServiceInfo:feedbackType	I
        //     101: aload_2
        //     102: iconst_2
        //     103: aload_2
        //     104: getfield 145	android/accessibilityservice/AccessibilityServiceInfo:flags	I
        //     107: ior
        //     108: putfield 145	android/accessibilityservice/AccessibilityServiceInfo:flags	I
        //     111: aload_1
        //     112: aload_0
        //     113: getfield 119	android/accessibilityservice/UiTestAutomationBridge:mListener	Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;
        //     116: aload_2
        //     117: invokeinterface 151 3 0
        //     122: aload_0
        //     123: getfield 52	android/accessibilityservice/UiTestAutomationBridge:mLock	Ljava/lang/Object;
        //     126: astore 4
        //     128: aload 4
        //     130: monitorenter
        //     131: invokestatic 157	android/os/SystemClock:uptimeMillis	()J
        //     134: lstore 6
        //     136: aload_0
        //     137: invokevirtual 89	android/accessibilityservice/UiTestAutomationBridge:isConnected	()Z
        //     140: ifeq +19 -> 159
        //     143: aload 4
        //     145: monitorexit
        //     146: return
        //     147: astore_3
        //     148: new 75	java/lang/IllegalStateException
        //     151: dup
        //     152: ldc 159
        //     154: aload_3
        //     155: invokespecial 162	java/lang/IllegalStateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     158: athrow
        //     159: ldc2_w 163
        //     162: invokestatic 157	android/os/SystemClock:uptimeMillis	()J
        //     165: lload 6
        //     167: lsub
        //     168: lsub
        //     169: lstore 8
        //     171: lload 8
        //     173: lconst_0
        //     174: lcmp
        //     175: ifgt +21 -> 196
        //     178: new 75	java/lang/IllegalStateException
        //     181: dup
        //     182: ldc 159
        //     184: invokespecial 80	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     187: athrow
        //     188: astore 5
        //     190: aload 4
        //     192: monitorexit
        //     193: aload 5
        //     195: athrow
        //     196: aload_0
        //     197: getfield 52	android/accessibilityservice/UiTestAutomationBridge:mLock	Ljava/lang/Object;
        //     200: lload 8
        //     202: invokevirtual 168	java/lang/Object:wait	(J)V
        //     205: goto -69 -> 136
        //     208: astore 10
        //     210: goto -74 -> 136
        //
        // Exception table:
        //     from	to	target	type
        //     111	122	147	android/os/RemoteException
        //     131	146	188	finally
        //     159	193	188	finally
        //     196	205	188	finally
        //     196	205	208	java/lang/InterruptedException
    }

    public void disconnect()
    {
        if (!isConnected())
            throw new IllegalStateException("Already disconnected.");
        this.mHandlerThread.quit();
        IAccessibilityManager localIAccessibilityManager = IAccessibilityManager.Stub.asInterface(ServiceManager.getService("accessibility"));
        try
        {
            localIAccessibilityManager.unregisterUiTestAutomationService(this.mListener);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e(LOG_TAG, "Error while unregistering UiTestAutomationService", localRemoteException);
        }
    }

    // ERROR //
    public AccessibilityEvent executeCommandAndWaitForAccessibilityEvent(java.lang.Runnable paramRunnable, com.android.internal.util.Predicate<AccessibilityEvent> paramPredicate, long paramLong)
        throws java.util.concurrent.TimeoutException, java.lang.Exception
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 52	android/accessibilityservice/UiTestAutomationBridge:mLock	Ljava/lang/Object;
        //     4: astore 5
        //     6: aload 5
        //     8: monitorenter
        //     9: aload_0
        //     10: iconst_1
        //     11: putfield 64	android/accessibilityservice/UiTestAutomationBridge:mWaitingForEventDelivery	Z
        //     14: aload_0
        //     15: iconst_0
        //     16: putfield 67	android/accessibilityservice/UiTestAutomationBridge:mUnprocessedEventAvailable	Z
        //     19: aload_0
        //     20: getfield 60	android/accessibilityservice/UiTestAutomationBridge:mLastEvent	Landroid/view/accessibility/AccessibilityEvent;
        //     23: ifnull +15 -> 38
        //     26: aload_0
        //     27: getfield 60	android/accessibilityservice/UiTestAutomationBridge:mLastEvent	Landroid/view/accessibility/AccessibilityEvent;
        //     30: invokevirtual 197	android/view/accessibility/AccessibilityEvent:recycle	()V
        //     33: aload_0
        //     34: aconst_null
        //     35: putfield 60	android/accessibilityservice/UiTestAutomationBridge:mLastEvent	Landroid/view/accessibility/AccessibilityEvent;
        //     38: aload_1
        //     39: invokeinterface 202 1 0
        //     44: invokestatic 157	android/os/SystemClock:uptimeMillis	()J
        //     47: lstore 7
        //     49: aload_0
        //     50: getfield 67	android/accessibilityservice/UiTestAutomationBridge:mUnprocessedEventAvailable	Z
        //     53: ifeq +45 -> 98
        //     56: aload_2
        //     57: aload_0
        //     58: getfield 60	android/accessibilityservice/UiTestAutomationBridge:mLastEvent	Landroid/view/accessibility/AccessibilityEvent;
        //     61: invokeinterface 208 2 0
        //     66: ifeq +32 -> 98
        //     69: aload_0
        //     70: iconst_0
        //     71: putfield 64	android/accessibilityservice/UiTestAutomationBridge:mWaitingForEventDelivery	Z
        //     74: aload_0
        //     75: iconst_0
        //     76: putfield 67	android/accessibilityservice/UiTestAutomationBridge:mUnprocessedEventAvailable	Z
        //     79: aload_0
        //     80: getfield 52	android/accessibilityservice/UiTestAutomationBridge:mLock	Ljava/lang/Object;
        //     83: invokevirtual 211	java/lang/Object:notifyAll	()V
        //     86: aload_0
        //     87: getfield 60	android/accessibilityservice/UiTestAutomationBridge:mLastEvent	Landroid/view/accessibility/AccessibilityEvent;
        //     90: astore 12
        //     92: aload 5
        //     94: monitorexit
        //     95: aload 12
        //     97: areturn
        //     98: aload_0
        //     99: iconst_1
        //     100: putfield 64	android/accessibilityservice/UiTestAutomationBridge:mWaitingForEventDelivery	Z
        //     103: aload_0
        //     104: iconst_0
        //     105: putfield 67	android/accessibilityservice/UiTestAutomationBridge:mUnprocessedEventAvailable	Z
        //     108: aload_0
        //     109: getfield 52	android/accessibilityservice/UiTestAutomationBridge:mLock	Ljava/lang/Object;
        //     112: invokevirtual 211	java/lang/Object:notifyAll	()V
        //     115: lload_3
        //     116: invokestatic 157	android/os/SystemClock:uptimeMillis	()J
        //     119: lload 7
        //     121: lsub
        //     122: lsub
        //     123: lstore 9
        //     125: lload 9
        //     127: lconst_0
        //     128: lcmp
        //     129: ifgt +60 -> 189
        //     132: aload_0
        //     133: iconst_0
        //     134: putfield 64	android/accessibilityservice/UiTestAutomationBridge:mWaitingForEventDelivery	Z
        //     137: aload_0
        //     138: iconst_0
        //     139: putfield 67	android/accessibilityservice/UiTestAutomationBridge:mUnprocessedEventAvailable	Z
        //     142: aload_0
        //     143: getfield 52	android/accessibilityservice/UiTestAutomationBridge:mLock	Ljava/lang/Object;
        //     146: invokevirtual 211	java/lang/Object:notifyAll	()V
        //     149: new 190	java/util/concurrent/TimeoutException
        //     152: dup
        //     153: new 213	java/lang/StringBuilder
        //     156: dup
        //     157: invokespecial 214	java/lang/StringBuilder:<init>	()V
        //     160: ldc 216
        //     162: invokevirtual 220	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     165: lload_3
        //     166: invokevirtual 223	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     169: ldc 225
        //     171: invokevirtual 220	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     174: invokevirtual 228	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     177: invokespecial 229	java/util/concurrent/TimeoutException:<init>	(Ljava/lang/String;)V
        //     180: athrow
        //     181: astore 6
        //     183: aload 5
        //     185: monitorexit
        //     186: aload 6
        //     188: athrow
        //     189: aload_0
        //     190: getfield 52	android/accessibilityservice/UiTestAutomationBridge:mLock	Ljava/lang/Object;
        //     193: lload 9
        //     195: invokevirtual 168	java/lang/Object:wait	(J)V
        //     198: goto -149 -> 49
        //     201: astore 11
        //     203: goto -154 -> 49
        //
        // Exception table:
        //     from	to	target	type
        //     9	186	181	finally
        //     189	198	181	finally
        //     189	198	201	java/lang/InterruptedException
    }

    public AccessibilityNodeInfo findAccessibilityNodeInfoByAccessibilityId(int paramInt, long paramLong)
    {
        ensureValidConnection(this.mConnectionId);
        return AccessibilityInteractionClient.getInstance().findAccessibilityNodeInfoByAccessibilityId(this.mConnectionId, paramInt, paramLong, 7);
    }

    public AccessibilityNodeInfo findAccessibilityNodeInfoByAccessibilityIdInActiveWindow(long paramLong)
    {
        return findAccessibilityNodeInfoByAccessibilityId(-1, paramLong);
    }

    public AccessibilityNodeInfo findAccessibilityNodeInfoByViewId(int paramInt1, long paramLong, int paramInt2)
    {
        int i = this.mConnectionId;
        ensureValidConnection(i);
        return AccessibilityInteractionClient.getInstance().findAccessibilityNodeInfoByViewId(i, paramInt1, paramLong, paramInt2);
    }

    public AccessibilityNodeInfo findAccessibilityNodeInfoByViewIdInActiveWindow(int paramInt)
    {
        return findAccessibilityNodeInfoByViewId(-1, ROOT_NODE_ID, paramInt);
    }

    public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(int paramInt, long paramLong, String paramString)
    {
        int i = this.mConnectionId;
        ensureValidConnection(i);
        return AccessibilityInteractionClient.getInstance().findAccessibilityNodeInfosByText(i, paramInt, paramLong, paramString);
    }

    public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByTextInActiveWindow(String paramString)
    {
        return findAccessibilityNodeInfosByText(-1, ROOT_NODE_ID, paramString);
    }

    public AccessibilityEvent getLastAccessibilityEvent()
    {
        return this.mLastEvent;
    }

    public AccessibilityNodeInfo getRootAccessibilityNodeInfoInActiveWindow()
    {
        int i = this.mConnectionId;
        ensureValidConnection(i);
        return AccessibilityInteractionClient.getInstance().findAccessibilityNodeInfoByAccessibilityId(i, -1, ROOT_NODE_ID, 4);
    }

    public boolean isConnected()
    {
        if (this.mConnectionId != -1);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
    }

    public void onInterrupt()
    {
    }

    public boolean performAccessibilityAction(int paramInt1, long paramLong, int paramInt2, Bundle paramBundle)
    {
        int i = this.mConnectionId;
        ensureValidConnection(i);
        return AccessibilityInteractionClient.getInstance().performAccessibilityAction(i, paramInt1, paramLong, paramInt2, paramBundle);
    }

    public boolean performAccessibilityActionInActiveWindow(long paramLong, int paramInt, Bundle paramBundle)
    {
        return performAccessibilityAction(-1, paramLong, paramInt, paramBundle);
    }

    public void waitForIdle(long paramLong1, long paramLong2)
    {
        long l1 = SystemClock.uptimeMillis();
        long l2;
        if (this.mLastEvent != null)
            l2 = this.mLastEvent.getEventTime();
        while (true)
        {
            synchronized (this.mLock)
            {
                if (SystemClock.uptimeMillis() - l2 > paramLong1)
                {
                    return;
                    l2 = SystemClock.uptimeMillis();
                    continue;
                }
                if (this.mLastEvent != null)
                    l2 = this.mLastEvent.getEventTime();
                if (paramLong2 - (SystemClock.uptimeMillis() - l1) > 0L);
            }
            try
            {
                this.mLock.wait(paramLong1);
            }
            catch (InterruptedException localInterruptedException)
            {
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accessibilityservice.UiTestAutomationBridge
 * JD-Core Version:        0.6.2
 */