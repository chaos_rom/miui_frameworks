package android.accessibilityservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.view.accessibility.AccessibilityEvent;

public abstract interface IAccessibilityServiceClient extends IInterface
{
    public abstract void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
        throws RemoteException;

    public abstract void onGesture(int paramInt)
        throws RemoteException;

    public abstract void onInterrupt()
        throws RemoteException;

    public abstract void setConnection(IAccessibilityServiceConnection paramIAccessibilityServiceConnection, int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAccessibilityServiceClient
    {
        private static final String DESCRIPTOR = "android.accessibilityservice.IAccessibilityServiceClient";
        static final int TRANSACTION_onAccessibilityEvent = 2;
        static final int TRANSACTION_onGesture = 4;
        static final int TRANSACTION_onInterrupt = 3;
        static final int TRANSACTION_setConnection = 1;

        public Stub()
        {
            attachInterface(this, "android.accessibilityservice.IAccessibilityServiceClient");
        }

        public static IAccessibilityServiceClient asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.accessibilityservice.IAccessibilityServiceClient");
                if ((localIInterface != null) && ((localIInterface instanceof IAccessibilityServiceClient)))
                    localObject = (IAccessibilityServiceClient)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.accessibilityservice.IAccessibilityServiceClient");
                continue;
                paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceClient");
                setConnection(IAccessibilityServiceConnection.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceClient");
                if (paramParcel1.readInt() != 0);
                for (AccessibilityEvent localAccessibilityEvent = (AccessibilityEvent)AccessibilityEvent.CREATOR.createFromParcel(paramParcel1); ; localAccessibilityEvent = null)
                {
                    onAccessibilityEvent(localAccessibilityEvent);
                    break;
                }
                paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceClient");
                onInterrupt();
                continue;
                paramParcel1.enforceInterface("android.accessibilityservice.IAccessibilityServiceClient");
                onGesture(paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements IAccessibilityServiceClient
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.accessibilityservice.IAccessibilityServiceClient";
            }

            public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceClient");
                    if (paramAccessibilityEvent != null)
                    {
                        localParcel.writeInt(1);
                        paramAccessibilityEvent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onGesture(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceClient");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onInterrupt()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceClient");
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setConnection(IAccessibilityServiceConnection paramIAccessibilityServiceConnection, int paramInt)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.accessibilityservice.IAccessibilityServiceClient");
                    if (paramIAccessibilityServiceConnection != null)
                        localIBinder = paramIAccessibilityServiceConnection.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.accessibilityservice.IAccessibilityServiceClient
 * JD-Core Version:        0.6.2
 */