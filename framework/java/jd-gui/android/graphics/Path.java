package android.graphics;

import android.view.HardwareRenderer;

public class Path
{
    static final FillType[] sFillTypeArray = arrayOfFillType;
    public boolean isSimplePath = true;
    private boolean mDetectSimplePaths;
    private Direction mLastDirection = null;
    public final int mNativePath;
    public Region rects;

    static
    {
        FillType[] arrayOfFillType = new FillType[4];
        arrayOfFillType[0] = FillType.WINDING;
        arrayOfFillType[1] = FillType.EVEN_ODD;
        arrayOfFillType[2] = FillType.INVERSE_WINDING;
        arrayOfFillType[3] = FillType.INVERSE_EVEN_ODD;
    }

    public Path()
    {
        this.mNativePath = init1();
        this.mDetectSimplePaths = HardwareRenderer.isAvailable();
    }

    public Path(Path paramPath)
    {
        int i = 0;
        if (paramPath != null)
            i = paramPath.mNativePath;
        this.mNativePath = init2(i);
        this.mDetectSimplePaths = HardwareRenderer.isAvailable();
    }

    private void detectSimplePath(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Direction paramDirection)
    {
        if (this.mDetectSimplePaths)
        {
            if (this.mLastDirection == null)
                this.mLastDirection = paramDirection;
            if (this.mLastDirection == paramDirection)
                break label35;
            this.isSimplePath = false;
        }
        while (true)
        {
            return;
            label35: if (this.rects == null)
                this.rects = new Region();
            this.rects.op((int)paramFloat1, (int)paramFloat2, (int)paramFloat3, (int)paramFloat4, Region.Op.UNION);
        }
    }

    private static native void finalizer(int paramInt);

    private static native int init1();

    private static native int init2(int paramInt);

    private static native void native_addArc(int paramInt, RectF paramRectF, float paramFloat1, float paramFloat2);

    private static native void native_addCircle(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, int paramInt2);

    private static native void native_addOval(int paramInt1, RectF paramRectF, int paramInt2);

    private static native void native_addPath(int paramInt1, int paramInt2);

    private static native void native_addPath(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2);

    private static native void native_addPath(int paramInt1, int paramInt2, int paramInt3);

    private static native void native_addRect(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native void native_addRect(int paramInt1, RectF paramRectF, int paramInt2);

    private static native void native_addRoundRect(int paramInt1, RectF paramRectF, float paramFloat1, float paramFloat2, int paramInt2);

    private static native void native_addRoundRect(int paramInt1, RectF paramRectF, float[] paramArrayOfFloat, int paramInt2);

    private static native void native_arcTo(int paramInt, RectF paramRectF, float paramFloat1, float paramFloat2, boolean paramBoolean);

    private static native void native_close(int paramInt);

    private static native void native_computeBounds(int paramInt, RectF paramRectF);

    private static native void native_cubicTo(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6);

    private static native int native_getFillType(int paramInt);

    private static native void native_incReserve(int paramInt1, int paramInt2);

    private static native boolean native_isEmpty(int paramInt);

    private static native boolean native_isRect(int paramInt, RectF paramRectF);

    private static native void native_lineTo(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_moveTo(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_offset(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_offset(int paramInt1, float paramFloat1, float paramFloat2, int paramInt2);

    private static native void native_quadTo(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native void native_rCubicTo(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6);

    private static native void native_rLineTo(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_rMoveTo(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_rQuadTo(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native void native_reset(int paramInt);

    private static native void native_rewind(int paramInt);

    private static native void native_set(int paramInt1, int paramInt2);

    private static native void native_setFillType(int paramInt1, int paramInt2);

    private static native void native_setLastPoint(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_transform(int paramInt1, int paramInt2);

    private static native void native_transform(int paramInt1, int paramInt2, int paramInt3);

    public void addArc(RectF paramRectF, float paramFloat1, float paramFloat2)
    {
        if (paramRectF == null)
            throw new NullPointerException("need oval parameter");
        this.isSimplePath = false;
        native_addArc(this.mNativePath, paramRectF, paramFloat1, paramFloat2);
    }

    public void addCircle(float paramFloat1, float paramFloat2, float paramFloat3, Direction paramDirection)
    {
        this.isSimplePath = false;
        native_addCircle(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramDirection.nativeInt);
    }

    public void addOval(RectF paramRectF, Direction paramDirection)
    {
        if (paramRectF == null)
            throw new NullPointerException("need oval parameter");
        this.isSimplePath = false;
        native_addOval(this.mNativePath, paramRectF, paramDirection.nativeInt);
    }

    public void addPath(Path paramPath)
    {
        this.isSimplePath = false;
        native_addPath(this.mNativePath, paramPath.mNativePath);
    }

    public void addPath(Path paramPath, float paramFloat1, float paramFloat2)
    {
        this.isSimplePath = false;
        native_addPath(this.mNativePath, paramPath.mNativePath, paramFloat1, paramFloat2);
    }

    public void addPath(Path paramPath, Matrix paramMatrix)
    {
        if (!paramPath.isSimplePath)
            this.isSimplePath = false;
        native_addPath(this.mNativePath, paramPath.mNativePath, paramMatrix.native_instance);
    }

    public void addRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Direction paramDirection)
    {
        detectSimplePath(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramDirection);
        native_addRect(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramDirection.nativeInt);
    }

    public void addRect(RectF paramRectF, Direction paramDirection)
    {
        if (paramRectF == null)
            throw new NullPointerException("need rect parameter");
        detectSimplePath(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramDirection);
        native_addRect(this.mNativePath, paramRectF, paramDirection.nativeInt);
    }

    public void addRoundRect(RectF paramRectF, float paramFloat1, float paramFloat2, Direction paramDirection)
    {
        if (paramRectF == null)
            throw new NullPointerException("need rect parameter");
        this.isSimplePath = false;
        native_addRoundRect(this.mNativePath, paramRectF, paramFloat1, paramFloat2, paramDirection.nativeInt);
    }

    public void addRoundRect(RectF paramRectF, float[] paramArrayOfFloat, Direction paramDirection)
    {
        if (paramRectF == null)
            throw new NullPointerException("need rect parameter");
        if (paramArrayOfFloat.length < 8)
            throw new ArrayIndexOutOfBoundsException("radii[] needs 8 values");
        this.isSimplePath = false;
        native_addRoundRect(this.mNativePath, paramRectF, paramArrayOfFloat, paramDirection.nativeInt);
    }

    public void arcTo(RectF paramRectF, float paramFloat1, float paramFloat2)
    {
        this.isSimplePath = false;
        native_arcTo(this.mNativePath, paramRectF, paramFloat1, paramFloat2, false);
    }

    public void arcTo(RectF paramRectF, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this.isSimplePath = false;
        native_arcTo(this.mNativePath, paramRectF, paramFloat1, paramFloat2, paramBoolean);
    }

    public void close()
    {
        this.isSimplePath = false;
        native_close(this.mNativePath);
    }

    public void computeBounds(RectF paramRectF, boolean paramBoolean)
    {
        native_computeBounds(this.mNativePath, paramRectF);
    }

    public void cubicTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
    {
        this.isSimplePath = false;
        native_cubicTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            finalizer(this.mNativePath);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public FillType getFillType()
    {
        return sFillTypeArray[native_getFillType(this.mNativePath)];
    }

    public void incReserve(int paramInt)
    {
        native_incReserve(this.mNativePath, paramInt);
    }

    public boolean isEmpty()
    {
        return native_isEmpty(this.mNativePath);
    }

    public boolean isInverseFillType()
    {
        if ((0x2 & native_getFillType(this.mNativePath)) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isRect(RectF paramRectF)
    {
        return native_isRect(this.mNativePath, paramRectF);
    }

    public void lineTo(float paramFloat1, float paramFloat2)
    {
        this.isSimplePath = false;
        native_lineTo(this.mNativePath, paramFloat1, paramFloat2);
    }

    public void moveTo(float paramFloat1, float paramFloat2)
    {
        native_moveTo(this.mNativePath, paramFloat1, paramFloat2);
    }

    final int ni()
    {
        return this.mNativePath;
    }

    public void offset(float paramFloat1, float paramFloat2)
    {
        native_offset(this.mNativePath, paramFloat1, paramFloat2);
    }

    public void offset(float paramFloat1, float paramFloat2, Path paramPath)
    {
        int i = 0;
        if (paramPath != null)
            i = paramPath.mNativePath;
        native_offset(this.mNativePath, paramFloat1, paramFloat2, i);
    }

    public void quadTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.isSimplePath = false;
        native_quadTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public void rCubicTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
    {
        this.isSimplePath = false;
        native_rCubicTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
    }

    public void rLineTo(float paramFloat1, float paramFloat2)
    {
        this.isSimplePath = false;
        native_rLineTo(this.mNativePath, paramFloat1, paramFloat2);
    }

    public void rMoveTo(float paramFloat1, float paramFloat2)
    {
        native_rMoveTo(this.mNativePath, paramFloat1, paramFloat2);
    }

    public void rQuadTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.isSimplePath = false;
        native_rQuadTo(this.mNativePath, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public void reset()
    {
        this.isSimplePath = true;
        if (this.mDetectSimplePaths)
        {
            this.mLastDirection = null;
            if (this.rects != null)
                this.rects.setEmpty();
        }
        native_reset(this.mNativePath);
    }

    public void rewind()
    {
        this.isSimplePath = true;
        if (this.mDetectSimplePaths)
        {
            this.mLastDirection = null;
            if (this.rects != null)
                this.rects.setEmpty();
        }
        native_rewind(this.mNativePath);
    }

    public void set(Path paramPath)
    {
        if (this != paramPath)
        {
            this.isSimplePath = paramPath.isSimplePath;
            native_set(this.mNativePath, paramPath.mNativePath);
        }
    }

    public void setFillType(FillType paramFillType)
    {
        native_setFillType(this.mNativePath, paramFillType.nativeInt);
    }

    public void setLastPoint(float paramFloat1, float paramFloat2)
    {
        this.isSimplePath = false;
        native_setLastPoint(this.mNativePath, paramFloat1, paramFloat2);
    }

    public void toggleInverseFillType()
    {
        int i = 0x2 ^ native_getFillType(this.mNativePath);
        native_setFillType(this.mNativePath, i);
    }

    public void transform(Matrix paramMatrix)
    {
        native_transform(this.mNativePath, paramMatrix.native_instance);
    }

    public void transform(Matrix paramMatrix, Path paramPath)
    {
        int i = 0;
        if (paramPath != null)
            i = paramPath.mNativePath;
        native_transform(this.mNativePath, paramMatrix.native_instance, i);
    }

    public static enum Direction
    {
        final int nativeInt;

        static
        {
            CCW = new Direction("CCW", 1, 1);
            Direction[] arrayOfDirection = new Direction[2];
            arrayOfDirection[0] = CW;
            arrayOfDirection[1] = CCW;
        }

        private Direction(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }

    public static enum FillType
    {
        final int nativeInt;

        static
        {
            EVEN_ODD = new FillType("EVEN_ODD", 1, 1);
            INVERSE_WINDING = new FillType("INVERSE_WINDING", 2, 2);
            INVERSE_EVEN_ODD = new FillType("INVERSE_EVEN_ODD", 3, 3);
            FillType[] arrayOfFillType = new FillType[4];
            arrayOfFillType[0] = WINDING;
            arrayOfFillType[1] = EVEN_ODD;
            arrayOfFillType[2] = INVERSE_WINDING;
            arrayOfFillType[3] = INVERSE_EVEN_ODD;
        }

        private FillType(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Path
 * JD-Core Version:        0.6.2
 */