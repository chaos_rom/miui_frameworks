package android.graphics;

public class PathEffect
{
    int native_instance;

    private static native void nativeDestructor(int paramInt);

    protected void finalize()
        throws Throwable
    {
        nativeDestructor(this.native_instance);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PathEffect
 * JD-Core Version:        0.6.2
 */