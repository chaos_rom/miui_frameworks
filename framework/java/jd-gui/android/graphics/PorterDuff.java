package android.graphics;

public class PorterDuff
{
    public static enum Mode
    {
        public final int nativeInt;

        static
        {
            DST = new Mode("DST", 2, 2);
            SRC_OVER = new Mode("SRC_OVER", 3, 3);
            DST_OVER = new Mode("DST_OVER", 4, 4);
            SRC_IN = new Mode("SRC_IN", 5, 5);
            DST_IN = new Mode("DST_IN", 6, 6);
            SRC_OUT = new Mode("SRC_OUT", 7, 7);
            DST_OUT = new Mode("DST_OUT", 8, 8);
            SRC_ATOP = new Mode("SRC_ATOP", 9, 9);
            DST_ATOP = new Mode("DST_ATOP", 10, 10);
            XOR = new Mode("XOR", 11, 11);
            DARKEN = new Mode("DARKEN", 12, 12);
            LIGHTEN = new Mode("LIGHTEN", 13, 13);
            MULTIPLY = new Mode("MULTIPLY", 14, 14);
            SCREEN = new Mode("SCREEN", 15, 15);
            ADD = new Mode("ADD", 16, 16);
            OVERLAY = new Mode("OVERLAY", 17, 17);
            Mode[] arrayOfMode = new Mode[18];
            arrayOfMode[0] = CLEAR;
            arrayOfMode[1] = SRC;
            arrayOfMode[2] = DST;
            arrayOfMode[3] = SRC_OVER;
            arrayOfMode[4] = DST_OVER;
            arrayOfMode[5] = SRC_IN;
            arrayOfMode[6] = DST_IN;
            arrayOfMode[7] = SRC_OUT;
            arrayOfMode[8] = DST_OUT;
            arrayOfMode[9] = SRC_ATOP;
            arrayOfMode[10] = DST_ATOP;
            arrayOfMode[11] = XOR;
            arrayOfMode[12] = DARKEN;
            arrayOfMode[13] = LIGHTEN;
            arrayOfMode[14] = MULTIPLY;
            arrayOfMode[15] = SCREEN;
            arrayOfMode[16] = ADD;
            arrayOfMode[17] = OVERLAY;
        }

        private Mode(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PorterDuff
 * JD-Core Version:        0.6.2
 */