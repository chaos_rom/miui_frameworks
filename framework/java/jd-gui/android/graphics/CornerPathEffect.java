package android.graphics;

public class CornerPathEffect extends PathEffect
{
    public CornerPathEffect(float paramFloat)
    {
        this.native_instance = nativeCreate(paramFloat);
    }

    private static native int nativeCreate(float paramFloat);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.CornerPathEffect
 * JD-Core Version:        0.6.2
 */