package android.graphics;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.DisplayMetrics;
import java.io.OutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public final class Bitmap
    implements Parcelable
{
    public static final Parcelable.Creator<Bitmap> CREATOR = new Parcelable.Creator()
    {
        public Bitmap createFromParcel(Parcel paramAnonymousParcel)
        {
            Bitmap localBitmap = Bitmap.nativeCreateFromParcel(paramAnonymousParcel);
            if (localBitmap == null)
                throw new RuntimeException("Failed to unparcel Bitmap");
            return localBitmap;
        }

        public Bitmap[] newArray(int paramAnonymousInt)
        {
            return new Bitmap[paramAnonymousInt];
        }
    };
    public static final int DENSITY_NONE = 0;
    private static final int WORKING_COMPRESS_STORAGE = 4096;
    private static volatile int sDefaultDensity = -1;
    private static volatile Matrix sScaleMatrix;
    public byte[] mBuffer;
    int mDensity;
    private final BitmapFinalizer mFinalizer;
    private int mHeight = -1;
    private final boolean mIsMutable;
    private int[] mLayoutBounds;
    public final int mNativeBitmap;
    private byte[] mNinePatchChunk;
    private boolean mRecycled;
    private int mWidth = -1;

    Bitmap(int paramInt1, byte[] paramArrayOfByte1, boolean paramBoolean, byte[] paramArrayOfByte2, int paramInt2)
    {
        this(paramInt1, paramArrayOfByte1, paramBoolean, paramArrayOfByte2, null, paramInt2);
    }

    Bitmap(int paramInt1, byte[] paramArrayOfByte1, boolean paramBoolean, byte[] paramArrayOfByte2, int[] paramArrayOfInt, int paramInt2)
    {
        int i = getDefaultDensity();
        sDefaultDensity = i;
        this.mDensity = i;
        if (paramInt1 == 0)
            throw new RuntimeException("internal error: native bitmap is 0");
        this.mBuffer = paramArrayOfByte1;
        this.mNativeBitmap = paramInt1;
        this.mFinalizer = new BitmapFinalizer(paramInt1);
        this.mIsMutable = paramBoolean;
        this.mNinePatchChunk = paramArrayOfByte2;
        this.mLayoutBounds = paramArrayOfInt;
        if (paramInt2 >= 0)
            this.mDensity = paramInt2;
    }

    private void checkPixelAccess(int paramInt1, int paramInt2)
    {
        checkXYSign(paramInt1, paramInt2);
        if (paramInt1 >= getWidth())
            throw new IllegalArgumentException("x must be < bitmap.width()");
        if (paramInt2 >= getHeight())
            throw new IllegalArgumentException("y must be < bitmap.height()");
    }

    private void checkPixelsAccess(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int[] paramArrayOfInt)
    {
        checkXYSign(paramInt1, paramInt2);
        if (paramInt3 < 0)
            throw new IllegalArgumentException("width must be >= 0");
        if (paramInt4 < 0)
            throw new IllegalArgumentException("height must be >= 0");
        if (paramInt1 + paramInt3 > getWidth())
            throw new IllegalArgumentException("x + width must be <= bitmap.width()");
        if (paramInt2 + paramInt4 > getHeight())
            throw new IllegalArgumentException("y + height must be <= bitmap.height()");
        if (Math.abs(paramInt6) < paramInt3)
            throw new IllegalArgumentException("abs(stride) must be >= width");
        int i = paramInt5 + paramInt6 * (paramInt4 - 1);
        int j = paramArrayOfInt.length;
        if ((paramInt5 < 0) || (paramInt5 + paramInt3 > j) || (i < 0) || (i + paramInt3 > j))
            throw new ArrayIndexOutOfBoundsException();
    }

    private void checkRecycled(String paramString)
    {
        if (this.mRecycled)
            throw new IllegalStateException(paramString);
    }

    private static void checkWidthHeight(int paramInt1, int paramInt2)
    {
        if (paramInt1 <= 0)
            throw new IllegalArgumentException("width must be > 0");
        if (paramInt2 <= 0)
            throw new IllegalArgumentException("height must be > 0");
    }

    private static void checkXYSign(int paramInt1, int paramInt2)
    {
        if (paramInt1 < 0)
            throw new IllegalArgumentException("x must be >= 0");
        if (paramInt2 < 0)
            throw new IllegalArgumentException("y must be >= 0");
    }

    public static Bitmap createBitmap(int paramInt1, int paramInt2, Config paramConfig)
    {
        return createBitmap(paramInt1, paramInt2, paramConfig, true);
    }

    private static Bitmap createBitmap(int paramInt1, int paramInt2, Config paramConfig, boolean paramBoolean)
    {
        if ((paramInt1 <= 0) || (paramInt2 <= 0))
            throw new IllegalArgumentException("width and height must be > 0");
        Bitmap localBitmap = nativeCreate(null, 0, paramInt1, paramInt1, paramInt2, paramConfig.nativeInt, true);
        if ((paramConfig == Config.ARGB_8888) && (!paramBoolean))
        {
            nativeErase(localBitmap.mNativeBitmap, -16777216);
            nativeSetHasAlpha(localBitmap.mNativeBitmap, paramBoolean);
        }
        return localBitmap;
    }

    public static Bitmap createBitmap(Bitmap paramBitmap)
    {
        return createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
    }

    public static Bitmap createBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        return createBitmap(paramBitmap, paramInt1, paramInt2, paramInt3, paramInt4, null, false);
    }

    public static Bitmap createBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Matrix paramMatrix, boolean paramBoolean)
    {
        checkXYSign(paramInt1, paramInt2);
        checkWidthHeight(paramInt3, paramInt4);
        if (paramInt1 + paramInt3 > paramBitmap.getWidth())
            throw new IllegalArgumentException("x + width must be <= bitmap.width()");
        if (paramInt2 + paramInt4 > paramBitmap.getHeight())
            throw new IllegalArgumentException("y + height must be <= bitmap.height()");
        if ((!paramBitmap.isMutable()) && (paramInt1 == 0) && (paramInt2 == 0) && (paramInt3 == paramBitmap.getWidth()) && (paramInt4 == paramBitmap.getHeight()) && ((paramMatrix == null) || (paramMatrix.isIdentity())))
            return paramBitmap;
        Canvas localCanvas = new Canvas();
        Rect localRect = new Rect(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
        RectF localRectF1 = new RectF(0.0F, 0.0F, paramInt3, paramInt4);
        Config localConfig1 = Config.ARGB_8888;
        Config localConfig2 = paramBitmap.getConfig();
        if (localConfig2 != null)
            switch (2.$SwitchMap$android$graphics$Bitmap$Config[localConfig2.ordinal()])
            {
            default:
                localConfig1 = Config.ARGB_8888;
            case 1:
            case 2:
            }
        Bitmap localBitmap;
        Paint localPaint;
        while (true)
        {
            if ((paramMatrix != null) && (!paramMatrix.isIdentity()))
                break label279;
            localBitmap = createBitmap(paramInt3, paramInt4, localConfig1, paramBitmap.hasAlpha());
            localPaint = null;
            localBitmap.mDensity = paramBitmap.mDensity;
            localCanvas.setBitmap(localBitmap);
            localCanvas.drawBitmap(paramBitmap, localRect, localRectF1, localPaint);
            localCanvas.setBitmap(null);
            paramBitmap = localBitmap;
            break;
            localConfig1 = Config.RGB_565;
            continue;
            localConfig1 = Config.ALPHA_8;
        }
        label279: int i;
        label290: RectF localRectF2;
        int j;
        int k;
        if (!paramMatrix.rectStaysRect())
        {
            i = 1;
            localRectF2 = new RectF();
            paramMatrix.mapRect(localRectF2, localRectF1);
            j = Math.round(localRectF2.width());
            k = Math.round(localRectF2.height());
            if (i != 0)
                localConfig1 = Config.ARGB_8888;
            if ((i == 0) && (!paramBitmap.hasAlpha()))
                break label427;
        }
        label427: for (boolean bool = true; ; bool = false)
        {
            localBitmap = createBitmap(j, k, localConfig1, bool);
            localCanvas.translate(-localRectF2.left, -localRectF2.top);
            localCanvas.concat(paramMatrix);
            localPaint = new Paint();
            localPaint.setFilterBitmap(paramBoolean);
            if (i == 0)
                break;
            localPaint.setAntiAlias(true);
            break;
            i = 0;
            break label290;
        }
    }

    public static Bitmap createBitmap(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Config paramConfig)
    {
        checkWidthHeight(paramInt3, paramInt4);
        if (Math.abs(paramInt2) < paramInt3)
            throw new IllegalArgumentException("abs(stride) must be >= width");
        int i = paramInt1 + paramInt2 * (paramInt4 - 1);
        int j = paramArrayOfInt.length;
        if ((paramInt1 < 0) || (paramInt1 + paramInt3 > j) || (i < 0) || (i + paramInt3 > j))
            throw new ArrayIndexOutOfBoundsException();
        if ((paramInt3 <= 0) || (paramInt4 <= 0))
            throw new IllegalArgumentException("width and height must be > 0");
        return nativeCreate(paramArrayOfInt, paramInt1, paramInt2, paramInt3, paramInt4, paramConfig.nativeInt, false);
    }

    public static Bitmap createBitmap(int[] paramArrayOfInt, int paramInt1, int paramInt2, Config paramConfig)
    {
        return createBitmap(paramArrayOfInt, 0, paramInt1, paramInt1, paramInt2, paramConfig);
    }

    // ERROR //
    public static Bitmap createScaledBitmap(Bitmap paramBitmap, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        // Byte code:
        //     0: ldc 2
        //     2: monitorenter
        //     3: getstatic 289	android/graphics/Bitmap:sScaleMatrix	Landroid/graphics/Matrix;
        //     6: astore 5
        //     8: aconst_null
        //     9: putstatic 289	android/graphics/Bitmap:sScaleMatrix	Landroid/graphics/Matrix;
        //     12: ldc 2
        //     14: monitorexit
        //     15: aload 5
        //     17: ifnonnull +12 -> 29
        //     20: new 194	android/graphics/Matrix
        //     23: dup
        //     24: invokespecial 290	android/graphics/Matrix:<init>	()V
        //     27: astore 5
        //     29: aload_0
        //     30: invokevirtual 107	android/graphics/Bitmap:getWidth	()I
        //     33: istore 6
        //     35: aload_0
        //     36: invokevirtual 115	android/graphics/Bitmap:getHeight	()I
        //     39: istore 7
        //     41: aload 5
        //     43: iload_1
        //     44: i2f
        //     45: iload 6
        //     47: i2f
        //     48: fdiv
        //     49: iload_2
        //     50: i2f
        //     51: iload 7
        //     53: i2f
        //     54: fdiv
        //     55: invokevirtual 293	android/graphics/Matrix:setScale	(FF)V
        //     58: aload_0
        //     59: iconst_0
        //     60: iconst_0
        //     61: iload 6
        //     63: iload 7
        //     65: aload 5
        //     67: iload_3
        //     68: invokestatic 186	android/graphics/Bitmap:createBitmap	(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
        //     71: astore 8
        //     73: ldc 2
        //     75: monitorenter
        //     76: getstatic 289	android/graphics/Bitmap:sScaleMatrix	Landroid/graphics/Matrix;
        //     79: ifnonnull +8 -> 87
        //     82: aload 5
        //     84: putstatic 289	android/graphics/Bitmap:sScaleMatrix	Landroid/graphics/Matrix;
        //     87: ldc 2
        //     89: monitorexit
        //     90: aload 8
        //     92: areturn
        //     93: astore 4
        //     95: ldc 2
        //     97: monitorexit
        //     98: aload 4
        //     100: athrow
        //     101: astore 9
        //     103: ldc 2
        //     105: monitorexit
        //     106: aload 9
        //     108: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     3	15	93	finally
        //     95	98	93	finally
        //     76	90	101	finally
        //     103	106	101	finally
    }

    static int getDefaultDensity()
    {
        if (sDefaultDensity >= 0);
        for (int i = sDefaultDensity; ; i = sDefaultDensity)
        {
            return i;
            sDefaultDensity = DisplayMetrics.DENSITY_DEVICE;
        }
    }

    private static native boolean nativeCompress(int paramInt1, int paramInt2, int paramInt3, OutputStream paramOutputStream, byte[] paramArrayOfByte);

    private static native int nativeConfig(int paramInt);

    private static native Bitmap nativeCopy(int paramInt1, int paramInt2, boolean paramBoolean);

    private static native void nativeCopyPixelsFromBuffer(int paramInt, Buffer paramBuffer);

    private static native void nativeCopyPixelsToBuffer(int paramInt, Buffer paramBuffer);

    private static native Bitmap nativeCreate(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean);

    private static native Bitmap nativeCreateFromParcel(Parcel paramParcel);

    private static native void nativeDestructor(int paramInt);

    private static native void nativeErase(int paramInt1, int paramInt2);

    private static native Bitmap nativeExtractAlpha(int paramInt1, int paramInt2, int[] paramArrayOfInt);

    private static native int nativeGenerationId(int paramInt);

    private static native int nativeGetPixel(int paramInt1, int paramInt2, int paramInt3);

    private static native void nativeGetPixels(int paramInt1, int[] paramArrayOfInt, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7);

    private static native boolean nativeHasAlpha(int paramInt);

    private static native int nativeHeight(int paramInt);

    private static native void nativePrepareToDraw(int paramInt);

    private static native void nativeRecycle(int paramInt);

    private static native int nativeRowBytes(int paramInt);

    private static native boolean nativeSameAs(int paramInt1, int paramInt2);

    private static native void nativeSetHasAlpha(int paramInt, boolean paramBoolean);

    private static native void nativeSetPixel(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native void nativeSetPixels(int paramInt1, int[] paramArrayOfInt, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7);

    private static native int nativeWidth(int paramInt);

    private static native boolean nativeWriteToParcel(int paramInt1, boolean paramBoolean, int paramInt2, Parcel paramParcel);

    public static int scaleFromDensity(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((paramInt2 == 0) || (paramInt2 == paramInt3));
        while (true)
        {
            return paramInt1;
            paramInt1 = (paramInt1 * paramInt3 + (paramInt2 >> 1)) / paramInt2;
        }
    }

    public static void setDefaultDensity(int paramInt)
    {
        sDefaultDensity = paramInt;
    }

    public boolean compress(CompressFormat paramCompressFormat, int paramInt, OutputStream paramOutputStream)
    {
        checkRecycled("Can't compress a recycled bitmap");
        if (paramOutputStream == null)
            throw new NullPointerException();
        if ((paramInt < 0) || (paramInt > 100))
            throw new IllegalArgumentException("quality must be 0..100");
        return nativeCompress(this.mNativeBitmap, paramCompressFormat.nativeInt, paramInt, paramOutputStream, new byte[4096]);
    }

    public Bitmap copy(Config paramConfig, boolean paramBoolean)
    {
        checkRecycled("Can't copy a recycled bitmap");
        Bitmap localBitmap = nativeCopy(this.mNativeBitmap, paramConfig.nativeInt, paramBoolean);
        if (localBitmap != null)
            localBitmap.mDensity = this.mDensity;
        return localBitmap;
    }

    public void copyPixelsFromBuffer(Buffer paramBuffer)
    {
        checkRecycled("copyPixelsFromBuffer called on recycled bitmap");
        int i = paramBuffer.remaining();
        int j;
        if ((paramBuffer instanceof ByteBuffer))
            j = 0;
        while (i << j < getByteCount())
        {
            throw new RuntimeException("Buffer not large enough for pixels");
            if ((paramBuffer instanceof ShortBuffer))
                j = 1;
            else if ((paramBuffer instanceof IntBuffer))
                j = 2;
            else
                throw new RuntimeException("unsupported Buffer subclass");
        }
        nativeCopyPixelsFromBuffer(this.mNativeBitmap, paramBuffer);
    }

    public void copyPixelsToBuffer(Buffer paramBuffer)
    {
        int i = paramBuffer.remaining();
        int j;
        if ((paramBuffer instanceof ByteBuffer))
            j = 0;
        long l2;
        while (true)
        {
            long l1 = i << j;
            l2 = getByteCount();
            if (l1 >= l2)
                break label81;
            throw new RuntimeException("Buffer not large enough for pixels");
            if ((paramBuffer instanceof ShortBuffer))
            {
                j = 1;
            }
            else
            {
                if (!(paramBuffer instanceof IntBuffer))
                    break;
                j = 2;
            }
        }
        throw new RuntimeException("unsupported Buffer subclass");
        label81: nativeCopyPixelsToBuffer(this.mNativeBitmap, paramBuffer);
        paramBuffer.position((int)(paramBuffer.position() + (l2 >> j)));
    }

    public int describeContents()
    {
        return 0;
    }

    public void eraseColor(int paramInt)
    {
        checkRecycled("Can't erase a recycled bitmap");
        if (!isMutable())
            throw new IllegalStateException("cannot erase immutable bitmaps");
        nativeErase(this.mNativeBitmap, paramInt);
    }

    public Bitmap extractAlpha()
    {
        return extractAlpha(null, null);
    }

    public Bitmap extractAlpha(Paint paramPaint, int[] paramArrayOfInt)
    {
        checkRecycled("Can't extractAlpha on a recycled bitmap");
        if (paramPaint != null);
        Bitmap localBitmap;
        for (int i = paramPaint.mNativePaint; ; i = 0)
        {
            localBitmap = nativeExtractAlpha(this.mNativeBitmap, i, paramArrayOfInt);
            if (localBitmap != null)
                break;
            throw new RuntimeException("Failed to extractAlpha on Bitmap");
        }
        localBitmap.mDensity = this.mDensity;
        return localBitmap;
    }

    public final int getByteCount()
    {
        return getRowBytes() * getHeight();
    }

    public final Config getConfig()
    {
        return Config.nativeToConfig(nativeConfig(this.mNativeBitmap));
    }

    public int getDensity()
    {
        return this.mDensity;
    }

    public int getGenerationId()
    {
        return nativeGenerationId(this.mNativeBitmap);
    }

    public final int getHeight()
    {
        int i;
        if (this.mHeight == -1)
        {
            i = nativeHeight(this.mNativeBitmap);
            this.mHeight = i;
        }
        while (true)
        {
            return i;
            i = this.mHeight;
        }
    }

    public int[] getLayoutBounds()
    {
        return this.mLayoutBounds;
    }

    public byte[] getNinePatchChunk()
    {
        return this.mNinePatchChunk;
    }

    public int getPixel(int paramInt1, int paramInt2)
    {
        checkRecycled("Can't call getPixel() on a recycled bitmap");
        checkPixelAccess(paramInt1, paramInt2);
        return nativeGetPixel(this.mNativeBitmap, paramInt1, paramInt2);
    }

    public void getPixels(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        checkRecycled("Can't call getPixels() on a recycled bitmap");
        if ((paramInt5 == 0) || (paramInt6 == 0));
        while (true)
        {
            return;
            checkPixelsAccess(paramInt3, paramInt4, paramInt5, paramInt6, paramInt1, paramInt2, paramArrayOfInt);
            nativeGetPixels(this.mNativeBitmap, paramArrayOfInt, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
        }
    }

    public final int getRowBytes()
    {
        return nativeRowBytes(this.mNativeBitmap);
    }

    public int getScaledHeight(int paramInt)
    {
        return scaleFromDensity(getHeight(), this.mDensity, paramInt);
    }

    public int getScaledHeight(Canvas paramCanvas)
    {
        return scaleFromDensity(getHeight(), this.mDensity, paramCanvas.mDensity);
    }

    public int getScaledHeight(DisplayMetrics paramDisplayMetrics)
    {
        return scaleFromDensity(getHeight(), this.mDensity, paramDisplayMetrics.densityDpi);
    }

    public int getScaledWidth(int paramInt)
    {
        return scaleFromDensity(getWidth(), this.mDensity, paramInt);
    }

    public int getScaledWidth(Canvas paramCanvas)
    {
        return scaleFromDensity(getWidth(), this.mDensity, paramCanvas.mDensity);
    }

    public int getScaledWidth(DisplayMetrics paramDisplayMetrics)
    {
        return scaleFromDensity(getWidth(), this.mDensity, paramDisplayMetrics.densityDpi);
    }

    public final int getWidth()
    {
        int i;
        if (this.mWidth == -1)
        {
            i = nativeWidth(this.mNativeBitmap);
            this.mWidth = i;
        }
        while (true)
        {
            return i;
            i = this.mWidth;
        }
    }

    public final boolean hasAlpha()
    {
        return nativeHasAlpha(this.mNativeBitmap);
    }

    public final boolean isMutable()
    {
        return this.mIsMutable;
    }

    public final boolean isRecycled()
    {
        return this.mRecycled;
    }

    final int ni()
    {
        return this.mNativeBitmap;
    }

    public void prepareToDraw()
    {
        nativePrepareToDraw(this.mNativeBitmap);
    }

    public void recycle()
    {
        if (!this.mRecycled)
        {
            this.mBuffer = null;
            nativeRecycle(this.mNativeBitmap);
            this.mNinePatchChunk = null;
            this.mRecycled = true;
        }
    }

    public boolean sameAs(Bitmap paramBitmap)
    {
        if ((this == paramBitmap) || ((paramBitmap != null) && (nativeSameAs(this.mNativeBitmap, paramBitmap.mNativeBitmap))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setDensity(int paramInt)
    {
        this.mDensity = paramInt;
    }

    public void setHasAlpha(boolean paramBoolean)
    {
        nativeSetHasAlpha(this.mNativeBitmap, paramBoolean);
    }

    public void setLayoutBounds(int[] paramArrayOfInt)
    {
        this.mLayoutBounds = paramArrayOfInt;
    }

    public void setNinePatchChunk(byte[] paramArrayOfByte)
    {
        this.mNinePatchChunk = paramArrayOfByte;
    }

    public void setPixel(int paramInt1, int paramInt2, int paramInt3)
    {
        checkRecycled("Can't call setPixel() on a recycled bitmap");
        if (!isMutable())
            throw new IllegalStateException();
        checkPixelAccess(paramInt1, paramInt2);
        nativeSetPixel(this.mNativeBitmap, paramInt1, paramInt2, paramInt3);
    }

    public void setPixels(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        checkRecycled("Can't call setPixels() on a recycled bitmap");
        if (!isMutable())
            throw new IllegalStateException();
        if ((paramInt5 == 0) || (paramInt6 == 0));
        while (true)
        {
            return;
            checkPixelsAccess(paramInt3, paramInt4, paramInt5, paramInt6, paramInt1, paramInt2, paramArrayOfInt);
            nativeSetPixels(this.mNativeBitmap, paramArrayOfInt, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        checkRecycled("Can't parcel a recycled bitmap");
        if (!nativeWriteToParcel(this.mNativeBitmap, this.mIsMutable, this.mDensity, paramParcel))
            throw new RuntimeException("native writeToParcel failed");
    }

    private static class BitmapFinalizer
    {
        private final int mNativeBitmap;

        BitmapFinalizer(int paramInt)
        {
            this.mNativeBitmap = paramInt;
        }

        // ERROR //
        public void finalize()
        {
            // Byte code:
            //     0: aload_0
            //     1: invokespecial 21	java/lang/Object:finalize	()V
            //     4: aload_0
            //     5: getfield 16	android/graphics/Bitmap$BitmapFinalizer:mNativeBitmap	I
            //     8: invokestatic 24	android/graphics/Bitmap:access$100	(I)V
            //     11: return
            //     12: astore_2
            //     13: aload_0
            //     14: getfield 16	android/graphics/Bitmap$BitmapFinalizer:mNativeBitmap	I
            //     17: invokestatic 24	android/graphics/Bitmap:access$100	(I)V
            //     20: goto -9 -> 11
            //     23: astore_1
            //     24: aload_0
            //     25: getfield 16	android/graphics/Bitmap$BitmapFinalizer:mNativeBitmap	I
            //     28: invokestatic 24	android/graphics/Bitmap:access$100	(I)V
            //     31: aload_1
            //     32: athrow
            //
            // Exception table:
            //     from	to	target	type
            //     0	4	12	java/lang/Throwable
            //     0	4	23	finally
        }
    }

    public static enum CompressFormat
    {
        final int nativeInt;

        static
        {
            CompressFormat[] arrayOfCompressFormat = new CompressFormat[3];
            arrayOfCompressFormat[0] = JPEG;
            arrayOfCompressFormat[1] = PNG;
            arrayOfCompressFormat[2] = WEBP;
        }

        private CompressFormat(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }

    public static enum Config
    {
        private static Config[] sConfigs = arrayOfConfig2;
        final int nativeInt;

        static
        {
            ARGB_4444 = new Config("ARGB_4444", 2, 5);
            ARGB_8888 = new Config("ARGB_8888", 3, 6);
            Config[] arrayOfConfig1 = new Config[4];
            arrayOfConfig1[0] = ALPHA_8;
            arrayOfConfig1[1] = RGB_565;
            arrayOfConfig1[2] = ARGB_4444;
            arrayOfConfig1[3] = ARGB_8888;
            $VALUES = arrayOfConfig1;
            Config[] arrayOfConfig2 = new Config[7];
            arrayOfConfig2[0] = null;
            arrayOfConfig2[1] = null;
            arrayOfConfig2[2] = ALPHA_8;
            arrayOfConfig2[3] = null;
            arrayOfConfig2[4] = RGB_565;
            arrayOfConfig2[5] = ARGB_4444;
            arrayOfConfig2[6] = ARGB_8888;
        }

        private Config(int paramInt)
        {
            this.nativeInt = paramInt;
        }

        static Config nativeToConfig(int paramInt)
        {
            return sConfigs[paramInt];
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Bitmap
 * JD-Core Version:        0.6.2
 */