package android.graphics;

public class DiscretePathEffect extends PathEffect
{
    public DiscretePathEffect(float paramFloat1, float paramFloat2)
    {
        this.native_instance = nativeCreate(paramFloat1, paramFloat2);
    }

    private static native int nativeCreate(float paramFloat1, float paramFloat2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.DiscretePathEffect
 * JD-Core Version:        0.6.2
 */