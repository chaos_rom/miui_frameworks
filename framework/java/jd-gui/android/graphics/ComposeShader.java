package android.graphics;

public class ComposeShader extends Shader
{
    private final Shader mShaderA;
    private final Shader mShaderB;

    public ComposeShader(Shader paramShader1, Shader paramShader2, PorterDuff.Mode paramMode)
    {
        this.mShaderA = paramShader1;
        this.mShaderB = paramShader2;
        this.native_instance = nativeCreate2(paramShader1.native_instance, paramShader2.native_instance, paramMode.nativeInt);
        this.native_shader = nativePostCreate2(this.native_instance, paramShader1.native_shader, paramShader2.native_shader, paramMode.nativeInt);
    }

    public ComposeShader(Shader paramShader1, Shader paramShader2, Xfermode paramXfermode)
    {
        this.mShaderA = paramShader1;
        this.mShaderB = paramShader2;
        int j = paramShader1.native_instance;
        int k = paramShader2.native_instance;
        int m;
        int i3;
        int i4;
        int i5;
        if (paramXfermode != null)
        {
            m = paramXfermode.native_instance;
            this.native_instance = nativeCreate1(j, k, m);
            if (!(paramXfermode instanceof PorterDuffXfermode))
                break label120;
            PorterDuff.Mode localMode = ((PorterDuffXfermode)paramXfermode).mode;
            i3 = this.native_instance;
            i4 = paramShader1.native_shader;
            i5 = paramShader2.native_shader;
            if (localMode != null)
                i = localMode.nativeInt;
        }
        label120: int n;
        int i1;
        int i2;
        for (this.native_shader = nativePostCreate2(i3, i4, i5, i); ; this.native_shader = nativePostCreate1(n, i1, i2, i))
        {
            return;
            m = 0;
            break;
            n = this.native_instance;
            i1 = paramShader1.native_shader;
            i2 = paramShader2.native_shader;
            if (paramXfermode != null)
                i = paramXfermode.native_instance;
        }
    }

    private static native int nativeCreate1(int paramInt1, int paramInt2, int paramInt3);

    private static native int nativeCreate2(int paramInt1, int paramInt2, int paramInt3);

    private static native int nativePostCreate1(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native int nativePostCreate2(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.ComposeShader
 * JD-Core Version:        0.6.2
 */