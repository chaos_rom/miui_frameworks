package android.graphics;

import android.text.GraphicsOperations;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextUtils;
import javax.microedition.khronos.opengles.GL;

public class Canvas
{
    public static final int ALL_SAVE_FLAG = 31;
    public static final int CLIP_SAVE_FLAG = 2;
    public static final int CLIP_TO_LAYER_SAVE_FLAG = 16;
    public static final int DIRECTION_LTR = 0;
    public static final int DIRECTION_RTL = 1;
    public static final int FULL_COLOR_LAYER_SAVE_FLAG = 8;
    public static final int HAS_ALPHA_LAYER_SAVE_FLAG = 4;
    public static final int MATRIX_SAVE_FLAG = 1;
    private static final int MAXMIMUM_BITMAP_SIZE = 32766;
    private Bitmap mBitmap;
    protected int mDensity = 0;
    private DrawFilter mDrawFilter;
    private final CanvasFinalizer mFinalizer;
    final int mNativeCanvas;
    protected int mScreenDensity = 0;
    private int mSurfaceFormat;

    public Canvas()
    {
        this.mNativeCanvas = initRaster(0);
        this.mFinalizer = new CanvasFinalizer(this.mNativeCanvas);
    }

    Canvas(int paramInt)
    {
        if (paramInt == 0)
            throw new IllegalStateException();
        this.mNativeCanvas = paramInt;
        this.mFinalizer = new CanvasFinalizer(paramInt);
        this.mDensity = Bitmap.getDefaultDensity();
    }

    public Canvas(Bitmap paramBitmap)
    {
        if (!paramBitmap.isMutable())
            throw new IllegalStateException("Immutable bitmap passed to Canvas constructor");
        throwIfRecycled(paramBitmap);
        this.mNativeCanvas = initRaster(paramBitmap.ni());
        this.mFinalizer = new CanvasFinalizer(this.mNativeCanvas);
        this.mBitmap = paramBitmap;
        this.mDensity = paramBitmap.mDensity;
    }

    protected static void checkRange(int paramInt1, int paramInt2, int paramInt3)
    {
        if (((paramInt2 | paramInt3) < 0) || (paramInt2 + paramInt3 > paramInt1))
            throw new ArrayIndexOutOfBoundsException();
    }

    private static native void finalizer(int paramInt);

    public static native void freeCaches();

    public static native void freeTextLayoutCaches();

    private static native int initRaster(int paramInt);

    private static native void nativeDrawBitmapMatrix(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native void nativeDrawBitmapMesh(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float[] paramArrayOfFloat, int paramInt5, int[] paramArrayOfInt, int paramInt6, int paramInt7);

    private static native void nativeDrawVertices(int paramInt1, int paramInt2, int paramInt3, float[] paramArrayOfFloat1, int paramInt4, float[] paramArrayOfFloat2, int paramInt5, int[] paramArrayOfInt, int paramInt6, short[] paramArrayOfShort, int paramInt7, int paramInt8, int paramInt9);

    private static native void nativeSetDrawFilter(int paramInt1, int paramInt2);

    private static native boolean native_clipPath(int paramInt1, int paramInt2, int paramInt3);

    private static native boolean native_clipRect(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native boolean native_clipRegion(int paramInt1, int paramInt2, int paramInt3);

    private static native void native_concat(int paramInt1, int paramInt2);

    private static native void native_drawARGB(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private static native void native_drawArc(int paramInt1, RectF paramRectF, float paramFloat1, float paramFloat2, boolean paramBoolean, int paramInt2);

    private native void native_drawBitmap(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

    private static native void native_drawBitmap(int paramInt1, int paramInt2, Rect paramRect1, Rect paramRect2, int paramInt3, int paramInt4, int paramInt5);

    private native void native_drawBitmap(int paramInt1, int paramInt2, Rect paramRect, RectF paramRectF, int paramInt3, int paramInt4, int paramInt5);

    private static native void native_drawBitmap(int paramInt1, int[] paramArrayOfInt, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, int paramInt4, int paramInt5, boolean paramBoolean, int paramInt6);

    private static native void native_drawCircle(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, int paramInt2);

    private static native void native_drawColor(int paramInt1, int paramInt2);

    private static native void native_drawColor(int paramInt1, int paramInt2, int paramInt3);

    private static native void native_drawLine(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native void native_drawOval(int paramInt1, RectF paramRectF, int paramInt2);

    private static native void native_drawPaint(int paramInt1, int paramInt2);

    private static native void native_drawPath(int paramInt1, int paramInt2, int paramInt3);

    private static native void native_drawPicture(int paramInt1, int paramInt2);

    private static native void native_drawPosText(int paramInt1, String paramString, float[] paramArrayOfFloat, int paramInt2);

    private static native void native_drawPosText(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, float[] paramArrayOfFloat, int paramInt4);

    private static native void native_drawRGB(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native void native_drawRect(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native void native_drawRect(int paramInt1, RectF paramRectF, int paramInt2);

    private static native void native_drawRoundRect(int paramInt1, RectF paramRectF, float paramFloat1, float paramFloat2, int paramInt2);

    private static native void native_drawText(int paramInt1, String paramString, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, int paramInt4, int paramInt5);

    private static native void native_drawText(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, float paramFloat1, float paramFloat2, int paramInt4, int paramInt5);

    private static native void native_drawTextOnPath(int paramInt1, String paramString, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, int paramInt4);

    private static native void native_drawTextOnPath(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, int paramInt6);

    private static native void native_drawTextRun(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float paramFloat1, float paramFloat2, int paramInt6, int paramInt7);

    private static native void native_drawTextRun(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float paramFloat1, float paramFloat2, int paramInt6, int paramInt7);

    private static native void native_getCTM(int paramInt1, int paramInt2);

    private static native boolean native_getClipBounds(int paramInt, Rect paramRect);

    private static native boolean native_quickReject(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2);

    private static native boolean native_quickReject(int paramInt1, int paramInt2, int paramInt3);

    private static native boolean native_quickReject(int paramInt1, RectF paramRectF, int paramInt2);

    private static native int native_saveLayer(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2, int paramInt3);

    private static native int native_saveLayer(int paramInt1, RectF paramRectF, int paramInt2, int paramInt3);

    private static native int native_saveLayerAlpha(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2, int paramInt3);

    private static native int native_saveLayerAlpha(int paramInt1, RectF paramRectF, int paramInt2, int paramInt3);

    private static native void native_setBitmap(int paramInt1, int paramInt2);

    private static native void native_setMatrix(int paramInt1, int paramInt2);

    private static void throwIfRecycled(Bitmap paramBitmap)
    {
        if (paramBitmap.isRecycled())
            throw new RuntimeException("Canvas: trying to use a recycled bitmap " + paramBitmap);
    }

    public boolean clipPath(Path paramPath)
    {
        return clipPath(paramPath, Region.Op.INTERSECT);
    }

    public boolean clipPath(Path paramPath, Region.Op paramOp)
    {
        return native_clipPath(this.mNativeCanvas, paramPath.ni(), paramOp.nativeInt);
    }

    public native boolean clipRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    public boolean clipRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Region.Op paramOp)
    {
        return native_clipRect(this.mNativeCanvas, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramOp.nativeInt);
    }

    public native boolean clipRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    public native boolean clipRect(Rect paramRect);

    public boolean clipRect(Rect paramRect, Region.Op paramOp)
    {
        return native_clipRect(this.mNativeCanvas, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, paramOp.nativeInt);
    }

    public native boolean clipRect(RectF paramRectF);

    public boolean clipRect(RectF paramRectF, Region.Op paramOp)
    {
        return native_clipRect(this.mNativeCanvas, paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom, paramOp.nativeInt);
    }

    public boolean clipRegion(Region paramRegion)
    {
        return clipRegion(paramRegion, Region.Op.INTERSECT);
    }

    public boolean clipRegion(Region paramRegion, Region.Op paramOp)
    {
        return native_clipRegion(this.mNativeCanvas, paramRegion.ni(), paramOp.nativeInt);
    }

    public void concat(Matrix paramMatrix)
    {
        native_concat(this.mNativeCanvas, paramMatrix.native_instance);
    }

    public void drawARGB(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        native_drawARGB(this.mNativeCanvas, paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void drawArc(RectF paramRectF, float paramFloat1, float paramFloat2, boolean paramBoolean, Paint paramPaint)
    {
        if (paramRectF == null)
            throw new NullPointerException();
        native_drawArc(this.mNativeCanvas, paramRectF, paramFloat1, paramFloat2, paramBoolean, paramPaint.mNativePaint);
    }

    public void drawBitmap(Bitmap paramBitmap, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        throwIfRecycled(paramBitmap);
        int i = this.mNativeCanvas;
        int j = paramBitmap.ni();
        if (paramPaint != null);
        for (int k = paramPaint.mNativePaint; ; k = 0)
        {
            native_drawBitmap(i, j, paramFloat1, paramFloat2, k, this.mDensity, this.mScreenDensity, paramBitmap.mDensity);
            return;
        }
    }

    public void drawBitmap(Bitmap paramBitmap, Matrix paramMatrix, Paint paramPaint)
    {
        int i = this.mNativeCanvas;
        int j = paramBitmap.ni();
        int k = paramMatrix.ni();
        if (paramPaint != null);
        for (int m = paramPaint.mNativePaint; ; m = 0)
        {
            nativeDrawBitmapMatrix(i, j, k, m);
            return;
        }
    }

    public void drawBitmap(Bitmap paramBitmap, Rect paramRect1, Rect paramRect2, Paint paramPaint)
    {
        if (paramRect2 == null)
            throw new NullPointerException();
        throwIfRecycled(paramBitmap);
        int i = this.mNativeCanvas;
        int j = paramBitmap.ni();
        if (paramPaint != null);
        for (int k = paramPaint.mNativePaint; ; k = 0)
        {
            native_drawBitmap(i, j, paramRect1, paramRect2, k, this.mScreenDensity, paramBitmap.mDensity);
            return;
        }
    }

    public void drawBitmap(Bitmap paramBitmap, Rect paramRect, RectF paramRectF, Paint paramPaint)
    {
        if (paramRectF == null)
            throw new NullPointerException();
        throwIfRecycled(paramBitmap);
        int i = this.mNativeCanvas;
        int j = paramBitmap.ni();
        if (paramPaint != null);
        for (int k = paramPaint.mNativePaint; ; k = 0)
        {
            native_drawBitmap(i, j, paramRect, paramRectF, k, this.mScreenDensity, paramBitmap.mDensity);
            return;
        }
    }

    public void drawBitmap(int[] paramArrayOfInt, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, int paramInt3, int paramInt4, boolean paramBoolean, Paint paramPaint)
    {
        if (paramInt3 < 0)
            throw new IllegalArgumentException("width must be >= 0");
        if (paramInt4 < 0)
            throw new IllegalArgumentException("height must be >= 0");
        if (Math.abs(paramInt2) < paramInt3)
            throw new IllegalArgumentException("abs(stride) must be >= width");
        int i = paramInt1 + paramInt2 * (paramInt4 - 1);
        int j = paramArrayOfInt.length;
        if ((paramInt1 < 0) || (paramInt1 + paramInt3 > j) || (i < 0) || (i + paramInt3 > j))
            throw new ArrayIndexOutOfBoundsException();
        if ((paramInt3 == 0) || (paramInt4 == 0))
            return;
        int k = this.mNativeCanvas;
        if (paramPaint != null);
        for (int m = paramPaint.mNativePaint; ; m = 0)
        {
            native_drawBitmap(k, paramArrayOfInt, paramInt1, paramInt2, paramFloat1, paramFloat2, paramInt3, paramInt4, paramBoolean, m);
            break;
        }
    }

    public void drawBitmap(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean, Paint paramPaint)
    {
        drawBitmap(paramArrayOfInt, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramBoolean, paramPaint);
    }

    public void drawBitmapMesh(Bitmap paramBitmap, int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3, int[] paramArrayOfInt, int paramInt4, Paint paramPaint)
    {
        if ((paramInt4 | (paramInt3 | (paramInt1 | paramInt2))) < 0)
            throw new ArrayIndexOutOfBoundsException();
        if ((paramInt1 == 0) || (paramInt2 == 0))
            return;
        int i = (paramInt1 + 1) * (paramInt2 + 1);
        checkRange(paramArrayOfFloat.length, paramInt3, i * 2);
        if (paramArrayOfInt != null)
            checkRange(paramArrayOfInt.length, paramInt4, i);
        int j = this.mNativeCanvas;
        int k = paramBitmap.ni();
        if (paramPaint != null);
        for (int m = paramPaint.mNativePaint; ; m = 0)
        {
            nativeDrawBitmapMesh(j, k, paramInt1, paramInt2, paramArrayOfFloat, paramInt3, paramArrayOfInt, paramInt4, m);
            break;
        }
    }

    public void drawCircle(float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint)
    {
        native_drawCircle(this.mNativeCanvas, paramFloat1, paramFloat2, paramFloat3, paramPaint.mNativePaint);
    }

    public void drawColor(int paramInt)
    {
        native_drawColor(this.mNativeCanvas, paramInt);
    }

    public void drawColor(int paramInt, PorterDuff.Mode paramMode)
    {
        native_drawColor(this.mNativeCanvas, paramInt, paramMode.nativeInt);
    }

    public void drawLine(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint)
    {
        native_drawLine(this.mNativeCanvas, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramPaint.mNativePaint);
    }

    public native void drawLines(float[] paramArrayOfFloat, int paramInt1, int paramInt2, Paint paramPaint);

    public void drawLines(float[] paramArrayOfFloat, Paint paramPaint)
    {
        drawLines(paramArrayOfFloat, 0, paramArrayOfFloat.length, paramPaint);
    }

    public void drawOval(RectF paramRectF, Paint paramPaint)
    {
        if (paramRectF == null)
            throw new NullPointerException();
        native_drawOval(this.mNativeCanvas, paramRectF, paramPaint.mNativePaint);
    }

    public void drawPaint(Paint paramPaint)
    {
        native_drawPaint(this.mNativeCanvas, paramPaint.mNativePaint);
    }

    public void drawPatch(Bitmap paramBitmap, byte[] paramArrayOfByte, RectF paramRectF, Paint paramPaint)
    {
    }

    public void drawPath(Path paramPath, Paint paramPaint)
    {
        native_drawPath(this.mNativeCanvas, paramPath.ni(), paramPaint.mNativePaint);
    }

    public void drawPicture(Picture paramPicture)
    {
        paramPicture.endRecording();
        native_drawPicture(this.mNativeCanvas, paramPicture.ni());
    }

    public void drawPicture(Picture paramPicture, Rect paramRect)
    {
        save();
        translate(paramRect.left, paramRect.top);
        if ((paramPicture.getWidth() > 0) && (paramPicture.getHeight() > 0))
            scale(paramRect.width() / paramPicture.getWidth(), paramRect.height() / paramPicture.getHeight());
        drawPicture(paramPicture);
        restore();
    }

    public void drawPicture(Picture paramPicture, RectF paramRectF)
    {
        save();
        translate(paramRectF.left, paramRectF.top);
        if ((paramPicture.getWidth() > 0) && (paramPicture.getHeight() > 0))
            scale(paramRectF.width() / paramPicture.getWidth(), paramRectF.height() / paramPicture.getHeight());
        drawPicture(paramPicture);
        restore();
    }

    public native void drawPoint(float paramFloat1, float paramFloat2, Paint paramPaint);

    public native void drawPoints(float[] paramArrayOfFloat, int paramInt1, int paramInt2, Paint paramPaint);

    public void drawPoints(float[] paramArrayOfFloat, Paint paramPaint)
    {
        drawPoints(paramArrayOfFloat, 0, paramArrayOfFloat.length, paramPaint);
    }

    @Deprecated
    public void drawPosText(String paramString, float[] paramArrayOfFloat, Paint paramPaint)
    {
        if (2 * paramString.length() > paramArrayOfFloat.length)
            throw new ArrayIndexOutOfBoundsException();
        native_drawPosText(this.mNativeCanvas, paramString, paramArrayOfFloat, paramPaint.mNativePaint);
    }

    @Deprecated
    public void drawPosText(char[] paramArrayOfChar, int paramInt1, int paramInt2, float[] paramArrayOfFloat, Paint paramPaint)
    {
        if ((paramInt1 < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length) || (paramInt2 * 2 > paramArrayOfFloat.length))
            throw new IndexOutOfBoundsException();
        native_drawPosText(this.mNativeCanvas, paramArrayOfChar, paramInt1, paramInt2, paramArrayOfFloat, paramPaint.mNativePaint);
    }

    public void drawRGB(int paramInt1, int paramInt2, int paramInt3)
    {
        native_drawRGB(this.mNativeCanvas, paramInt1, paramInt2, paramInt3);
    }

    public void drawRect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint)
    {
        native_drawRect(this.mNativeCanvas, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramPaint.mNativePaint);
    }

    public void drawRect(Rect paramRect, Paint paramPaint)
    {
        drawRect(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, paramPaint);
    }

    public void drawRect(RectF paramRectF, Paint paramPaint)
    {
        native_drawRect(this.mNativeCanvas, paramRectF, paramPaint.mNativePaint);
    }

    public void drawRoundRect(RectF paramRectF, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if (paramRectF == null)
            throw new NullPointerException();
        native_drawRoundRect(this.mNativeCanvas, paramRectF, paramFloat1, paramFloat2, paramPaint.mNativePaint);
    }

    public void drawText(CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if (((paramCharSequence instanceof String)) || ((paramCharSequence instanceof SpannedString)) || ((paramCharSequence instanceof SpannableString)))
            native_drawText(this.mNativeCanvas, paramCharSequence.toString(), paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
        while (true)
        {
            return;
            if ((paramCharSequence instanceof GraphicsOperations))
            {
                ((GraphicsOperations)paramCharSequence).drawText(this, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint);
            }
            else
            {
                char[] arrayOfChar = TemporaryBuffer.obtain(paramInt2 - paramInt1);
                TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
                native_drawText(this.mNativeCanvas, arrayOfChar, 0, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
                TemporaryBuffer.recycle(arrayOfChar);
            }
        }
    }

    public void drawText(String paramString, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        native_drawText(this.mNativeCanvas, paramString, 0, paramString.length(), paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
    }

    public void drawText(String paramString, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramString.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        native_drawText(this.mNativeCanvas, paramString, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
    }

    public void drawText(char[] paramArrayOfChar, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if ((paramInt1 | paramInt2 | paramInt1 + paramInt2 | paramArrayOfChar.length - paramInt1 - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        native_drawText(this.mNativeCanvas, paramArrayOfChar, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
    }

    public void drawTextOnPath(String paramString, Path paramPath, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if (paramString.length() > 0)
            native_drawTextOnPath(this.mNativeCanvas, paramString, paramPath.ni(), paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
    }

    public void drawTextOnPath(char[] paramArrayOfChar, int paramInt1, int paramInt2, Path paramPath, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        if ((paramInt1 < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length))
            throw new ArrayIndexOutOfBoundsException();
        native_drawTextOnPath(this.mNativeCanvas, paramArrayOfChar, paramInt1, paramInt2, paramPath.ni(), paramFloat1, paramFloat2, paramPaint.mBidiFlags, paramPaint.mNativePaint);
    }

    public void drawTextRun(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint)
    {
        if (paramCharSequence == null)
            throw new NullPointerException("text is null");
        if (paramPaint == null)
            throw new NullPointerException("paint is null");
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramCharSequence.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        int i;
        if (paramInt5 == 0)
        {
            i = 0;
            if ((!(paramCharSequence instanceof String)) && (!(paramCharSequence instanceof SpannedString)) && (!(paramCharSequence instanceof SpannableString)))
                break label122;
            native_drawTextRun(this.mNativeCanvas, paramCharSequence.toString(), paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, i, paramPaint.mNativePaint);
        }
        while (true)
        {
            return;
            i = 1;
            break;
            label122: if ((paramCharSequence instanceof GraphicsOperations))
            {
                ((GraphicsOperations)paramCharSequence).drawTextRun(this, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, i, paramPaint);
            }
            else
            {
                int j = paramInt4 - paramInt3;
                int k = paramInt2 - paramInt1;
                char[] arrayOfChar = TemporaryBuffer.obtain(j);
                TextUtils.getChars(paramCharSequence, paramInt3, paramInt4, arrayOfChar, 0);
                native_drawTextRun(this.mNativeCanvas, arrayOfChar, paramInt1 - paramInt3, k, 0, j, paramFloat1, paramFloat2, i, paramPaint.mNativePaint);
                TemporaryBuffer.recycle(arrayOfChar);
            }
        }
    }

    public void drawTextRun(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint)
    {
        if (paramArrayOfChar == null)
            throw new NullPointerException("text is null");
        if (paramPaint == null)
            throw new NullPointerException("paint is null");
        if ((paramInt1 | paramInt2 | paramArrayOfChar.length - paramInt1 - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        if ((paramInt5 != 0) && (paramInt5 != 1))
            throw new IllegalArgumentException("unknown dir: " + paramInt5);
        native_drawTextRun(this.mNativeCanvas, paramArrayOfChar, paramInt1, paramInt2, paramInt3, paramInt4, paramFloat1, paramFloat2, paramInt5, paramPaint.mNativePaint);
    }

    public void drawVertices(VertexMode paramVertexMode, int paramInt1, float[] paramArrayOfFloat1, int paramInt2, float[] paramArrayOfFloat2, int paramInt3, int[] paramArrayOfInt, int paramInt4, short[] paramArrayOfShort, int paramInt5, int paramInt6, Paint paramPaint)
    {
        checkRange(paramArrayOfFloat1.length, paramInt2, paramInt1);
        if (paramArrayOfFloat2 != null)
            checkRange(paramArrayOfFloat2.length, paramInt3, paramInt1);
        if (paramArrayOfInt != null)
            checkRange(paramArrayOfInt.length, paramInt4, paramInt1 / 2);
        if (paramArrayOfShort != null)
            checkRange(paramArrayOfShort.length, paramInt5, paramInt6);
        nativeDrawVertices(this.mNativeCanvas, paramVertexMode.nativeInt, paramInt1, paramArrayOfFloat1, paramInt2, paramArrayOfFloat2, paramInt3, paramArrayOfInt, paramInt4, paramArrayOfShort, paramInt5, paramInt6, paramPaint.mNativePaint);
    }

    public final Rect getClipBounds()
    {
        Rect localRect = new Rect();
        getClipBounds(localRect);
        return localRect;
    }

    public boolean getClipBounds(Rect paramRect)
    {
        return native_getClipBounds(this.mNativeCanvas, paramRect);
    }

    public int getDensity()
    {
        return this.mDensity;
    }

    public DrawFilter getDrawFilter()
    {
        return this.mDrawFilter;
    }

    @Deprecated
    protected GL getGL()
    {
        return null;
    }

    public native int getHeight();

    @Deprecated
    public final Matrix getMatrix()
    {
        Matrix localMatrix = new Matrix();
        getMatrix(localMatrix);
        return localMatrix;
    }

    @Deprecated
    public void getMatrix(Matrix paramMatrix)
    {
        native_getCTM(this.mNativeCanvas, paramMatrix.native_instance);
    }

    public int getMaximumBitmapHeight()
    {
        return 32766;
    }

    public int getMaximumBitmapWidth()
    {
        return 32766;
    }

    public native int getSaveCount();

    public native int getWidth();

    public boolean isHardwareAccelerated()
    {
        return false;
    }

    public native boolean isOpaque();

    public boolean quickReject(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, EdgeType paramEdgeType)
    {
        return native_quickReject(this.mNativeCanvas, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramEdgeType.nativeInt);
    }

    public boolean quickReject(Path paramPath, EdgeType paramEdgeType)
    {
        return native_quickReject(this.mNativeCanvas, paramPath.ni(), paramEdgeType.nativeInt);
    }

    public boolean quickReject(RectF paramRectF, EdgeType paramEdgeType)
    {
        return native_quickReject(this.mNativeCanvas, paramRectF, paramEdgeType.nativeInt);
    }

    public native void restore();

    public native void restoreToCount(int paramInt);

    public native void rotate(float paramFloat);

    public final void rotate(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        translate(paramFloat2, paramFloat3);
        rotate(paramFloat1);
        translate(-paramFloat2, -paramFloat3);
    }

    public native int save();

    public native int save(int paramInt);

    public int saveLayer(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Paint paramPaint, int paramInt)
    {
        int i = this.mNativeCanvas;
        if (paramPaint != null);
        for (int j = paramPaint.mNativePaint; ; j = 0)
            return native_saveLayer(i, paramFloat1, paramFloat2, paramFloat3, paramFloat4, j, paramInt);
    }

    public int saveLayer(RectF paramRectF, Paint paramPaint, int paramInt)
    {
        int i = this.mNativeCanvas;
        if (paramPaint != null);
        for (int j = paramPaint.mNativePaint; ; j = 0)
            return native_saveLayer(i, paramRectF, j, paramInt);
    }

    public int saveLayerAlpha(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt1, int paramInt2)
    {
        return native_saveLayerAlpha(this.mNativeCanvas, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramInt1, paramInt2);
    }

    public int saveLayerAlpha(RectF paramRectF, int paramInt1, int paramInt2)
    {
        int i = Math.min(255, Math.max(0, paramInt1));
        return native_saveLayerAlpha(this.mNativeCanvas, paramRectF, i, paramInt2);
    }

    public native void scale(float paramFloat1, float paramFloat2);

    public final void scale(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        translate(paramFloat3, paramFloat4);
        scale(paramFloat1, paramFloat2);
        translate(-paramFloat3, -paramFloat4);
    }

    public void setBitmap(Bitmap paramBitmap)
    {
        if (isHardwareAccelerated())
            throw new RuntimeException("Can't set a bitmap device on a GL canvas");
        int i = 0;
        if (paramBitmap != null)
        {
            if (!paramBitmap.isMutable())
                throw new IllegalStateException();
            throwIfRecycled(paramBitmap);
            this.mDensity = paramBitmap.mDensity;
            i = paramBitmap.ni();
        }
        native_setBitmap(this.mNativeCanvas, i);
        this.mBitmap = paramBitmap;
    }

    public void setDensity(int paramInt)
    {
        if (this.mBitmap != null)
            this.mBitmap.setDensity(paramInt);
        this.mDensity = paramInt;
    }

    public void setDrawFilter(DrawFilter paramDrawFilter)
    {
        int i = 0;
        if (paramDrawFilter != null)
            i = paramDrawFilter.mNativeInt;
        this.mDrawFilter = paramDrawFilter;
        nativeSetDrawFilter(this.mNativeCanvas, i);
    }

    public void setMatrix(Matrix paramMatrix)
    {
        int i = this.mNativeCanvas;
        if (paramMatrix == null);
        for (int j = 0; ; j = paramMatrix.native_instance)
        {
            native_setMatrix(i, j);
            return;
        }
    }

    public void setScreenDensity(int paramInt)
    {
        this.mScreenDensity = paramInt;
    }

    public void setViewport(int paramInt1, int paramInt2)
    {
    }

    public native void skew(float paramFloat1, float paramFloat2);

    public native void translate(float paramFloat1, float paramFloat2);

    public static enum VertexMode
    {
        public final int nativeInt;

        static
        {
            TRIANGLE_FAN = new VertexMode("TRIANGLE_FAN", 2, 2);
            VertexMode[] arrayOfVertexMode = new VertexMode[3];
            arrayOfVertexMode[0] = TRIANGLES;
            arrayOfVertexMode[1] = TRIANGLE_STRIP;
            arrayOfVertexMode[2] = TRIANGLE_FAN;
        }

        private VertexMode(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }

    public static enum EdgeType
    {
        public final int nativeInt;

        static
        {
            AA = new EdgeType("AA", 1, 1);
            EdgeType[] arrayOfEdgeType = new EdgeType[2];
            arrayOfEdgeType[0] = BW;
            arrayOfEdgeType[1] = AA;
        }

        private EdgeType(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }

    private static class CanvasFinalizer
    {
        private final int mNativeCanvas;

        public CanvasFinalizer(int paramInt)
        {
            this.mNativeCanvas = paramInt;
        }

        protected void finalize()
            throws Throwable
        {
            try
            {
                if (this.mNativeCanvas != 0)
                    Canvas.finalizer(this.mNativeCanvas);
                return;
            }
            finally
            {
                super.finalize();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Canvas
 * JD-Core Version:        0.6.2
 */