package android.graphics;

public class BlurMaskFilter extends MaskFilter
{
    public BlurMaskFilter(float paramFloat, Blur paramBlur)
    {
        this.native_instance = nativeConstructor(paramFloat, paramBlur.native_int);
    }

    private static native int nativeConstructor(float paramFloat, int paramInt);

    public static enum Blur
    {
        final int native_int;

        static
        {
            OUTER = new Blur("OUTER", 2, 2);
            INNER = new Blur("INNER", 3, 3);
            Blur[] arrayOfBlur = new Blur[4];
            arrayOfBlur[0] = NORMAL;
            arrayOfBlur[1] = SOLID;
            arrayOfBlur[2] = OUTER;
            arrayOfBlur[3] = INNER;
        }

        private Blur(int paramInt)
        {
            this.native_int = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.BlurMaskFilter
 * JD-Core Version:        0.6.2
 */