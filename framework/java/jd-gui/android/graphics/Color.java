package android.graphics;

import android.util.MathUtils;
import java.util.HashMap;
import java.util.Locale;

public class Color
{
    public static final int BLACK = -16777216;
    public static final int BLUE = -16776961;
    public static final int CYAN = -16711681;
    public static final int DKGRAY = -12303292;
    public static final int GRAY = -7829368;
    public static final int GREEN = -16711936;
    public static final int LTGRAY = -3355444;
    public static final int MAGENTA = -65281;
    public static final int RED = -65536;
    public static final int TRANSPARENT = 0;
    public static final int WHITE = -1;
    public static final int YELLOW = -256;
    private static final HashMap<String, Integer> sColorNameMap = new HashMap();

    static
    {
        sColorNameMap.put("black", Integer.valueOf(-16777216));
        sColorNameMap.put("darkgray", Integer.valueOf(-12303292));
        sColorNameMap.put("gray", Integer.valueOf(-7829368));
        sColorNameMap.put("lightgray", Integer.valueOf(-3355444));
        sColorNameMap.put("white", Integer.valueOf(-1));
        sColorNameMap.put("red", Integer.valueOf(-65536));
        sColorNameMap.put("green", Integer.valueOf(-16711936));
        sColorNameMap.put("blue", Integer.valueOf(-16776961));
        sColorNameMap.put("yellow", Integer.valueOf(-256));
        sColorNameMap.put("cyan", Integer.valueOf(-16711681));
        sColorNameMap.put("magenta", Integer.valueOf(-65281));
    }

    public static int HSBtoColor(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        float f1 = MathUtils.constrain(paramFloat1, 0.0F, 1.0F);
        float f2 = MathUtils.constrain(paramFloat2, 0.0F, 1.0F);
        float f3 = MathUtils.constrain(paramFloat3, 0.0F, 1.0F);
        float f4 = 0.0F;
        float f5 = 0.0F;
        float f6 = 0.0F;
        float f7 = 6.0F * (f1 - (int)f1);
        int i = (int)f7;
        float f8 = f7 - i;
        float f9 = f3 * (1.0F - f2);
        float f10 = f3 * (1.0F - f2 * f8);
        float f11 = f3 * (1.0F - f2 * (1.0F - f8));
        switch (i)
        {
        default:
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return 0xFF000000 | (int)(f4 * 255.0F) << 16 | (int)(f5 * 255.0F) << 8 | (int)(f6 * 255.0F);
            f4 = f3;
            f5 = f11;
            f6 = f9;
            continue;
            f4 = f10;
            f5 = f3;
            f6 = f9;
            continue;
            f4 = f9;
            f5 = f3;
            f6 = f11;
            continue;
            f4 = f9;
            f5 = f10;
            f6 = f3;
            continue;
            f4 = f11;
            f5 = f9;
            f6 = f3;
            continue;
            f4 = f3;
            f5 = f9;
            f6 = f10;
        }
    }

    public static int HSBtoColor(float[] paramArrayOfFloat)
    {
        return HSBtoColor(paramArrayOfFloat[0], paramArrayOfFloat[1], paramArrayOfFloat[2]);
    }

    public static int HSVToColor(int paramInt, float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat.length < 3)
            throw new RuntimeException("3 components required for hsv");
        return nativeHSVToColor(paramInt, paramArrayOfFloat);
    }

    public static int HSVToColor(float[] paramArrayOfFloat)
    {
        return HSVToColor(255, paramArrayOfFloat);
    }

    public static void RGBToHSV(int paramInt1, int paramInt2, int paramInt3, float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat.length < 3)
            throw new RuntimeException("3 components required for hsv");
        nativeRGBToHSV(paramInt1, paramInt2, paramInt3, paramArrayOfFloat);
    }

    public static int alpha(int paramInt)
    {
        return paramInt >>> 24;
    }

    public static int argb(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        return paramInt4 | (paramInt1 << 24 | paramInt2 << 16 | paramInt3 << 8);
    }

    public static int blue(int paramInt)
    {
        return paramInt & 0xFF;
    }

    public static float brightness(int paramInt)
    {
        int i = 0xFF & paramInt >> 16;
        int j = 0xFF & paramInt >> 8;
        return Math.max(paramInt & 0xFF, Math.max(i, j)) / 255.0F;
    }

    public static void colorToHSV(int paramInt, float[] paramArrayOfFloat)
    {
        RGBToHSV(0xFF & paramInt >> 16, 0xFF & paramInt >> 8, paramInt & 0xFF, paramArrayOfFloat);
    }

    public static int green(int paramInt)
    {
        return 0xFF & paramInt >> 8;
    }

    public static float hue(int paramInt)
    {
        int i = 0xFF & paramInt >> 16;
        int j = 0xFF & paramInt >> 8;
        int k = paramInt & 0xFF;
        int m = Math.max(k, Math.max(i, j));
        int n = Math.min(k, Math.min(i, j));
        float f6;
        if (m == n)
        {
            f6 = 0.0F;
            return f6;
        }
        float f1 = m - n;
        float f2 = (m - i) / f1;
        float f3 = (m - j) / f1;
        float f4 = (m - k) / f1;
        float f5;
        if (i == m)
            f5 = f4 - f3;
        while (true)
        {
            f6 = f5 / 6.0F;
            if (f6 >= 0.0F)
                break;
            f6 += 1.0F;
            break;
            if (j == m)
                f5 = 2.0F + f2 - f4;
            else
                f5 = 4.0F + f3 - f2;
        }
    }

    private static native int nativeHSVToColor(int paramInt, float[] paramArrayOfFloat);

    private static native void nativeRGBToHSV(int paramInt1, int paramInt2, int paramInt3, float[] paramArrayOfFloat);

    public static int parseColor(String paramString)
    {
        long l;
        if (paramString.charAt(0) == '#')
        {
            l = Long.parseLong(paramString.substring(1), 16);
            if (paramString.length() == 7)
                l |= -16777216L;
        }
        Integer localInteger;
        for (int i = (int)l; ; i = localInteger.intValue())
        {
            return i;
            if (paramString.length() == 9)
                break;
            throw new IllegalArgumentException("Unknown color");
            localInteger = (Integer)sColorNameMap.get(paramString.toLowerCase(Locale.US));
            if (localInteger == null)
                break label89;
        }
        label89: throw new IllegalArgumentException("Unknown color");
    }

    public static int red(int paramInt)
    {
        return 0xFF & paramInt >> 16;
    }

    public static int rgb(int paramInt1, int paramInt2, int paramInt3)
    {
        return paramInt3 | (0xFF000000 | paramInt1 << 16 | paramInt2 << 8);
    }

    public static float saturation(int paramInt)
    {
        int i = 0xFF & paramInt >> 16;
        int j = 0xFF & paramInt >> 8;
        int k = paramInt & 0xFF;
        int m = Math.max(k, Math.max(i, j));
        int n = Math.min(k, Math.min(i, j));
        if (m == n);
        for (float f = 0.0F; ; f = (m - n) / m)
            return f;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Color
 * JD-Core Version:        0.6.2
 */