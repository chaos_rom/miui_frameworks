package android.graphics;

public class RegionIterator
{
    private final int mNativeIter;

    public RegionIterator(Region paramRegion)
    {
        this.mNativeIter = nativeConstructor(paramRegion.ni());
    }

    private static native int nativeConstructor(int paramInt);

    private static native void nativeDestructor(int paramInt);

    private static native boolean nativeNext(int paramInt, Rect paramRect);

    protected void finalize()
        throws Throwable
    {
        nativeDestructor(this.mNativeIter);
    }

    public final boolean next(Rect paramRect)
    {
        if (paramRect == null)
            throw new NullPointerException("The Rect must be provided");
        return nativeNext(this.mNativeIter, paramRect);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.RegionIterator
 * JD-Core Version:        0.6.2
 */