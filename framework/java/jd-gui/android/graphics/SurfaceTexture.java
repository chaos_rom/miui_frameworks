package android.graphics;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

public class SurfaceTexture
{
    private EventHandler mEventHandler;
    private OnFrameAvailableListener mOnFrameAvailableListener;
    private int mSurfaceTexture;

    static
    {
        nativeClassInit();
    }

    public SurfaceTexture(int paramInt)
    {
        this(paramInt, false);
    }

    public SurfaceTexture(int paramInt, boolean paramBoolean)
    {
        Looper localLooper1 = Looper.myLooper();
        if (localLooper1 != null)
            this.mEventHandler = new EventHandler(localLooper1);
        while (true)
        {
            nativeInit(paramInt, new WeakReference(this), paramBoolean);
            return;
            Looper localLooper2 = Looper.getMainLooper();
            if (localLooper2 != null)
                this.mEventHandler = new EventHandler(localLooper2);
            else
                this.mEventHandler = null;
        }
    }

    private native int nativeAttachToGLContext(int paramInt);

    private static native void nativeClassInit();

    private native int nativeDetachFromGLContext();

    private native void nativeFinalize();

    private native int nativeGetQueuedCount();

    private native long nativeGetTimestamp();

    private native void nativeGetTransformMatrix(float[] paramArrayOfFloat);

    private native void nativeInit(int paramInt, Object paramObject, boolean paramBoolean);

    private native void nativeRelease();

    private native void nativeSetDefaultBufferSize(int paramInt1, int paramInt2);

    private native void nativeUpdateTexImage();

    private static void postEventFromNative(Object paramObject)
    {
        SurfaceTexture localSurfaceTexture = (SurfaceTexture)((WeakReference)paramObject).get();
        if (localSurfaceTexture == null);
        while (true)
        {
            return;
            if (localSurfaceTexture.mEventHandler != null)
            {
                Message localMessage = localSurfaceTexture.mEventHandler.obtainMessage();
                localSurfaceTexture.mEventHandler.sendMessage(localMessage);
            }
        }
    }

    public void attachToGLContext(int paramInt)
    {
        if (nativeAttachToGLContext(paramInt) != 0)
            throw new RuntimeException("Error during detachFromGLContext (see logcat for details)");
    }

    public void detachFromGLContext()
    {
        if (nativeDetachFromGLContext() != 0)
            throw new RuntimeException("Error during detachFromGLContext (see logcat for details)");
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeFinalize();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public long getTimestamp()
    {
        return nativeGetTimestamp();
    }

    public void getTransformMatrix(float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat.length != 16)
            throw new IllegalArgumentException();
        nativeGetTransformMatrix(paramArrayOfFloat);
    }

    public void release()
    {
        nativeRelease();
    }

    public void setDefaultBufferSize(int paramInt1, int paramInt2)
    {
        nativeSetDefaultBufferSize(paramInt1, paramInt2);
    }

    public void setOnFrameAvailableListener(OnFrameAvailableListener paramOnFrameAvailableListener)
    {
        this.mOnFrameAvailableListener = paramOnFrameAvailableListener;
    }

    public void updateTexImage()
    {
        nativeUpdateTexImage();
    }

    private class EventHandler extends Handler
    {
        public EventHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            if (SurfaceTexture.this.mOnFrameAvailableListener != null)
                SurfaceTexture.this.mOnFrameAvailableListener.onFrameAvailable(SurfaceTexture.this);
        }
    }

    public static class OutOfResourcesException extends Exception
    {
        public OutOfResourcesException()
        {
        }

        public OutOfResourcesException(String paramString)
        {
            super();
        }
    }

    public static abstract interface OnFrameAvailableListener
    {
        public abstract void onFrameAvailable(SurfaceTexture paramSurfaceTexture);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.SurfaceTexture
 * JD-Core Version:        0.6.2
 */