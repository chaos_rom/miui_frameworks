package android.graphics;

public class SumPathEffect extends PathEffect
{
    public SumPathEffect(PathEffect paramPathEffect1, PathEffect paramPathEffect2)
    {
        this.native_instance = nativeCreate(paramPathEffect1.native_instance, paramPathEffect2.native_instance);
    }

    private static native int nativeCreate(int paramInt1, int paramInt2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.SumPathEffect
 * JD-Core Version:        0.6.2
 */