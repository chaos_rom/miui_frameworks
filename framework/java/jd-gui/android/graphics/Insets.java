package android.graphics;

public class Insets
{
    public static final Insets NONE = new Insets(0, 0, 0, 0);
    public final int bottom;
    public final int left;
    public final int right;
    public final int top;

    private Insets(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.left = paramInt1;
        this.top = paramInt2;
        this.right = paramInt3;
        this.bottom = paramInt4;
    }

    public static Insets of(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((paramInt1 == 0) && (paramInt2 == 0) && (paramInt3 == 0) && (paramInt4 == 0));
        for (Insets localInsets = NONE; ; localInsets = new Insets(paramInt1, paramInt2, paramInt3, paramInt4))
            return localInsets;
    }

    public static Insets of(Rect paramRect)
    {
        if (paramRect == null);
        for (Insets localInsets = NONE; ; localInsets = of(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom))
            return localInsets;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (getClass() != paramObject.getClass()))
            {
                bool = false;
            }
            else
            {
                Insets localInsets = (Insets)paramObject;
                if (this.bottom != localInsets.bottom)
                    bool = false;
                else if (this.left != localInsets.left)
                    bool = false;
                else if (this.right != localInsets.right)
                    bool = false;
                else if (this.top != localInsets.top)
                    bool = false;
            }
        }
    }

    public int hashCode()
    {
        return 31 * (31 * (31 * this.left + this.top) + this.right) + this.bottom;
    }

    public String toString()
    {
        return "Insets{left=" + this.left + ", top=" + this.top + ", right=" + this.right + ", bottom=" + this.bottom + '}';
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Insets
 * JD-Core Version:        0.6.2
 */