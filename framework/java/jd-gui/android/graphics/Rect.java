package android.graphics;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Rect
    implements Parcelable
{
    public static final Parcelable.Creator<Rect> CREATOR = new Parcelable.Creator()
    {
        public Rect createFromParcel(Parcel paramAnonymousParcel)
        {
            Rect localRect = new Rect();
            localRect.readFromParcel(paramAnonymousParcel);
            return localRect;
        }

        public Rect[] newArray(int paramAnonymousInt)
        {
            return new Rect[paramAnonymousInt];
        }
    };
    private static final Pattern FLATTENED_PATTERN = Pattern.compile("(-?\\d+) (-?\\d+) (-?\\d+) (-?\\d+)");
    public int bottom;
    public int left;
    public int right;
    public int top;

    public Rect()
    {
    }

    public Rect(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.left = paramInt1;
        this.top = paramInt2;
        this.right = paramInt3;
        this.bottom = paramInt4;
    }

    public Rect(Rect paramRect)
    {
        this.left = paramRect.left;
        this.top = paramRect.top;
        this.right = paramRect.right;
        this.bottom = paramRect.bottom;
    }

    public static boolean intersects(Rect paramRect1, Rect paramRect2)
    {
        if ((paramRect1.left < paramRect2.right) && (paramRect2.left < paramRect1.right) && (paramRect1.top < paramRect2.bottom) && (paramRect2.top < paramRect1.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static Rect unflattenFromString(String paramString)
    {
        Matcher localMatcher = FLATTENED_PATTERN.matcher(paramString);
        if (!localMatcher.matches());
        for (Rect localRect = null; ; localRect = new Rect(Integer.parseInt(localMatcher.group(1)), Integer.parseInt(localMatcher.group(2)), Integer.parseInt(localMatcher.group(3)), Integer.parseInt(localMatcher.group(4))))
            return localRect;
    }

    public final int centerX()
    {
        return this.left + this.right >> 1;
    }

    public final int centerY()
    {
        return this.top + this.bottom >> 1;
    }

    public boolean contains(int paramInt1, int paramInt2)
    {
        if ((this.left < this.right) && (this.top < this.bottom) && (paramInt1 >= this.left) && (paramInt1 < this.right) && (paramInt2 >= this.top) && (paramInt2 < this.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean contains(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((this.left < this.right) && (this.top < this.bottom) && (this.left <= paramInt1) && (this.top <= paramInt2) && (this.right >= paramInt3) && (this.bottom >= paramInt4));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean contains(Rect paramRect)
    {
        if ((this.left < this.right) && (this.top < this.bottom) && (this.left <= paramRect.left) && (this.top <= paramRect.top) && (this.right >= paramRect.right) && (this.bottom >= paramRect.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (getClass() != paramObject.getClass()))
            {
                bool = false;
            }
            else
            {
                Rect localRect = (Rect)paramObject;
                if ((this.left != localRect.left) || (this.top != localRect.top) || (this.right != localRect.right) || (this.bottom != localRect.bottom))
                    bool = false;
            }
        }
    }

    public final float exactCenterX()
    {
        return 0.5F * (this.left + this.right);
    }

    public final float exactCenterY()
    {
        return 0.5F * (this.top + this.bottom);
    }

    public String flattenToString()
    {
        StringBuilder localStringBuilder = new StringBuilder(32);
        localStringBuilder.append(this.left);
        localStringBuilder.append(' ');
        localStringBuilder.append(this.top);
        localStringBuilder.append(' ');
        localStringBuilder.append(this.right);
        localStringBuilder.append(' ');
        localStringBuilder.append(this.bottom);
        return localStringBuilder.toString();
    }

    public int hashCode()
    {
        return 31 * (31 * (31 * this.left + this.top) + this.right) + this.bottom;
    }

    public final int height()
    {
        return this.bottom - this.top;
    }

    public void inset(int paramInt1, int paramInt2)
    {
        this.left = (paramInt1 + this.left);
        this.top = (paramInt2 + this.top);
        this.right -= paramInt1;
        this.bottom -= paramInt2;
    }

    public boolean intersect(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((this.left < paramInt3) && (paramInt1 < this.right) && (this.top < paramInt4) && (paramInt2 < this.bottom))
        {
            if (this.left < paramInt1)
                this.left = paramInt1;
            if (this.top < paramInt2)
                this.top = paramInt2;
            if (this.right > paramInt3)
                this.right = paramInt3;
            if (this.bottom > paramInt4)
                this.bottom = paramInt4;
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean intersect(Rect paramRect)
    {
        return intersect(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    public boolean intersects(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((this.left < paramInt3) && (paramInt1 < this.right) && (this.top < paramInt4) && (paramInt2 < this.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isEmpty()
    {
        if ((this.left >= this.right) || (this.top >= this.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void offset(int paramInt1, int paramInt2)
    {
        this.left = (paramInt1 + this.left);
        this.top = (paramInt2 + this.top);
        this.right = (paramInt1 + this.right);
        this.bottom = (paramInt2 + this.bottom);
    }

    public void offsetTo(int paramInt1, int paramInt2)
    {
        this.right += paramInt1 - this.left;
        this.bottom += paramInt2 - this.top;
        this.left = paramInt1;
        this.top = paramInt2;
    }

    public void printShortString(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print('[');
        paramPrintWriter.print(this.left);
        paramPrintWriter.print(',');
        paramPrintWriter.print(this.top);
        paramPrintWriter.print("][");
        paramPrintWriter.print(this.right);
        paramPrintWriter.print(',');
        paramPrintWriter.print(this.bottom);
        paramPrintWriter.print(']');
    }

    public void readFromParcel(Parcel paramParcel)
    {
        this.left = paramParcel.readInt();
        this.top = paramParcel.readInt();
        this.right = paramParcel.readInt();
        this.bottom = paramParcel.readInt();
    }

    public void scale(float paramFloat)
    {
        if (paramFloat != 1.0F)
        {
            this.left = ((int)(0.5F + paramFloat * this.left));
            this.top = ((int)(0.5F + paramFloat * this.top));
            this.right = ((int)(0.5F + paramFloat * this.right));
            this.bottom = ((int)(0.5F + paramFloat * this.bottom));
        }
    }

    public void set(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.left = paramInt1;
        this.top = paramInt2;
        this.right = paramInt3;
        this.bottom = paramInt4;
    }

    public void set(Rect paramRect)
    {
        this.left = paramRect.left;
        this.top = paramRect.top;
        this.right = paramRect.right;
        this.bottom = paramRect.bottom;
    }

    public void setEmpty()
    {
        this.bottom = 0;
        this.top = 0;
        this.right = 0;
        this.left = 0;
    }

    public boolean setIntersect(Rect paramRect1, Rect paramRect2)
    {
        if ((paramRect1.left < paramRect2.right) && (paramRect2.left < paramRect1.right) && (paramRect1.top < paramRect2.bottom) && (paramRect2.top < paramRect1.bottom))
        {
            this.left = Math.max(paramRect1.left, paramRect2.left);
            this.top = Math.max(paramRect1.top, paramRect2.top);
            this.right = Math.min(paramRect1.right, paramRect2.right);
            this.bottom = Math.min(paramRect1.bottom, paramRect2.bottom);
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void sort()
    {
        if (this.left > this.right)
        {
            int j = this.left;
            this.left = this.right;
            this.right = j;
        }
        if (this.top > this.bottom)
        {
            int i = this.top;
            this.top = this.bottom;
            this.bottom = i;
        }
    }

    public String toShortString()
    {
        return toShortString(new StringBuilder(32));
    }

    public String toShortString(StringBuilder paramStringBuilder)
    {
        paramStringBuilder.setLength(0);
        paramStringBuilder.append('[');
        paramStringBuilder.append(this.left);
        paramStringBuilder.append(',');
        paramStringBuilder.append(this.top);
        paramStringBuilder.append("][");
        paramStringBuilder.append(this.right);
        paramStringBuilder.append(',');
        paramStringBuilder.append(this.bottom);
        paramStringBuilder.append(']');
        return paramStringBuilder.toString();
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(32);
        localStringBuilder.append("Rect(");
        localStringBuilder.append(this.left);
        localStringBuilder.append(", ");
        localStringBuilder.append(this.top);
        localStringBuilder.append(" - ");
        localStringBuilder.append(this.right);
        localStringBuilder.append(", ");
        localStringBuilder.append(this.bottom);
        localStringBuilder.append(")");
        return localStringBuilder.toString();
    }

    public void union(int paramInt1, int paramInt2)
    {
        if (paramInt1 < this.left)
            this.left = paramInt1;
        label57: 
        while (true)
        {
            if (paramInt2 < this.top)
                this.top = paramInt2;
            while (true)
            {
                return;
                if (paramInt1 <= this.right)
                    break label57;
                this.right = paramInt1;
                break;
                if (paramInt2 > this.bottom)
                    this.bottom = paramInt2;
            }
        }
    }

    public void union(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((paramInt1 < paramInt3) && (paramInt2 < paramInt4))
        {
            if ((this.left >= this.right) || (this.top >= this.bottom))
                break label88;
            if (this.left > paramInt1)
                this.left = paramInt1;
            if (this.top > paramInt2)
                this.top = paramInt2;
            if (this.right < paramInt3)
                this.right = paramInt3;
            if (this.bottom >= paramInt4);
        }
        for (this.bottom = paramInt4; ; this.bottom = paramInt4)
        {
            return;
            label88: this.left = paramInt1;
            this.top = paramInt2;
            this.right = paramInt3;
        }
    }

    public void union(Rect paramRect)
    {
        union(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    public final int width()
    {
        return this.right - this.left;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.left);
        paramParcel.writeInt(this.top);
        paramParcel.writeInt(this.right);
        paramParcel.writeInt(this.bottom);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Rect
 * JD-Core Version:        0.6.2
 */