package android.graphics;

import java.io.InputStream;
import java.io.OutputStream;

public class Picture
{
    private static final int WORKING_STREAM_STORAGE = 16384;
    public final boolean createdFromStream;
    private final int mNativePicture;
    private Canvas mRecordingCanvas;

    public Picture()
    {
        this(nativeConstructor(0), false);
    }

    private Picture(int paramInt, boolean paramBoolean)
    {
        if (paramInt == 0)
            throw new RuntimeException();
        this.mNativePicture = paramInt;
        this.createdFromStream = paramBoolean;
    }

    public Picture(Picture paramPicture)
    {
    }

    public static Picture createFromStream(InputStream paramInputStream)
    {
        return new Picture(nativeCreateFromStream(paramInputStream, new byte[16384]), true);
    }

    private static native int nativeBeginRecording(int paramInt1, int paramInt2, int paramInt3);

    private static native int nativeConstructor(int paramInt);

    private static native int nativeCreateFromStream(InputStream paramInputStream, byte[] paramArrayOfByte);

    private static native void nativeDestructor(int paramInt);

    private static native void nativeDraw(int paramInt1, int paramInt2);

    private static native void nativeEndRecording(int paramInt);

    private static native boolean nativeWriteToStream(int paramInt, OutputStream paramOutputStream, byte[] paramArrayOfByte);

    public Canvas beginRecording(int paramInt1, int paramInt2)
    {
        this.mRecordingCanvas = new RecordingCanvas(this, nativeBeginRecording(this.mNativePicture, paramInt1, paramInt2));
        return this.mRecordingCanvas;
    }

    public void draw(Canvas paramCanvas)
    {
        if (this.mRecordingCanvas != null)
            endRecording();
        nativeDraw(paramCanvas.mNativeCanvas, this.mNativePicture);
    }

    public void endRecording()
    {
        if (this.mRecordingCanvas != null)
        {
            this.mRecordingCanvas = null;
            nativeEndRecording(this.mNativePicture);
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDestructor(this.mNativePicture);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public native int getHeight();

    public native int getWidth();

    final int ni()
    {
        return this.mNativePicture;
    }

    public void writeToStream(OutputStream paramOutputStream)
    {
        if (paramOutputStream == null)
            throw new NullPointerException();
        if (!nativeWriteToStream(this.mNativePicture, paramOutputStream, new byte[16384]))
            throw new RuntimeException();
    }

    private static class RecordingCanvas extends Canvas
    {
        private final Picture mPicture;

        public RecordingCanvas(Picture paramPicture, int paramInt)
        {
            super();
            this.mPicture = paramPicture;
        }

        public void drawPicture(Picture paramPicture)
        {
            if (this.mPicture == paramPicture)
                throw new RuntimeException("Cannot draw a picture into its recording canvas");
            super.drawPicture(paramPicture);
        }

        public void setBitmap(Bitmap paramBitmap)
        {
            throw new RuntimeException("Cannot call setBitmap on a picture canvas");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Picture
 * JD-Core Version:        0.6.2
 */