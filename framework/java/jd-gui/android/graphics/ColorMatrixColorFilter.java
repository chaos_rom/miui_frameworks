package android.graphics;

public class ColorMatrixColorFilter extends ColorFilter
{
    public ColorMatrixColorFilter(ColorMatrix paramColorMatrix)
    {
        float[] arrayOfFloat = paramColorMatrix.getArray();
        this.native_instance = nativeColorMatrixFilter(arrayOfFloat);
        this.nativeColorFilter = nColorMatrixFilter(this.native_instance, arrayOfFloat);
    }

    public ColorMatrixColorFilter(float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat.length < 20)
            throw new ArrayIndexOutOfBoundsException();
        this.native_instance = nativeColorMatrixFilter(paramArrayOfFloat);
        this.nativeColorFilter = nColorMatrixFilter(this.native_instance, paramArrayOfFloat);
    }

    private static native int nColorMatrixFilter(int paramInt, float[] paramArrayOfFloat);

    private static native int nativeColorMatrixFilter(float[] paramArrayOfFloat);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.ColorMatrixColorFilter
 * JD-Core Version:        0.6.2
 */