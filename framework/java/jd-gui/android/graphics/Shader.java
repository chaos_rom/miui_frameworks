package android.graphics;

public class Shader
{
    private Matrix mLocalMatrix;
    public int native_instance;
    public int native_shader;

    private static native void nativeDestructor(int paramInt1, int paramInt2);

    private static native void nativeSetLocalMatrix(int paramInt1, int paramInt2, int paramInt3);

    protected void finalize()
        throws Throwable
    {
        try
        {
            super.finalize();
            return;
        }
        finally
        {
            nativeDestructor(this.native_instance, this.native_shader);
        }
    }

    public boolean getLocalMatrix(Matrix paramMatrix)
    {
        boolean bool = false;
        if (this.mLocalMatrix != null)
        {
            paramMatrix.set(this.mLocalMatrix);
            if (!this.mLocalMatrix.isIdentity())
                bool = true;
        }
        return bool;
    }

    public void setLocalMatrix(Matrix paramMatrix)
    {
        this.mLocalMatrix = paramMatrix;
        int i = this.native_instance;
        int j = this.native_shader;
        if (paramMatrix == null);
        for (int k = 0; ; k = paramMatrix.native_instance)
        {
            nativeSetLocalMatrix(i, j, k);
            return;
        }
    }

    public static enum TileMode
    {
        final int nativeInt;

        static
        {
            MIRROR = new TileMode("MIRROR", 2, 2);
            TileMode[] arrayOfTileMode = new TileMode[3];
            arrayOfTileMode[0] = CLAMP;
            arrayOfTileMode[1] = REPEAT;
            arrayOfTileMode[2] = MIRROR;
        }

        private TileMode(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Shader
 * JD-Core Version:        0.6.2
 */