package android.graphics;

public class PorterDuffColorFilter extends ColorFilter
{
    public PorterDuffColorFilter(int paramInt, PorterDuff.Mode paramMode)
    {
        this.native_instance = native_CreatePorterDuffFilter(paramInt, paramMode.nativeInt);
        this.nativeColorFilter = nCreatePorterDuffFilter(this.native_instance, paramInt, paramMode.nativeInt);
    }

    private static native int nCreatePorterDuffFilter(int paramInt1, int paramInt2, int paramInt3);

    private static native int native_CreatePorterDuffFilter(int paramInt1, int paramInt2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PorterDuffColorFilter
 * JD-Core Version:        0.6.2
 */