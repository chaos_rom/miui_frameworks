package android.graphics;

import android.text.GraphicsOperations;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextUtils;
import java.util.Locale;

public class Paint
{
    public static final int ANTI_ALIAS_FLAG = 1;
    public static final int BIDI_DEFAULT_LTR = 2;
    public static final int BIDI_DEFAULT_RTL = 3;
    private static final int BIDI_FLAG_MASK = 7;
    public static final int BIDI_FORCE_LTR = 4;
    public static final int BIDI_FORCE_RTL = 5;
    public static final int BIDI_LTR = 0;
    private static final int BIDI_MAX_FLAG_VALUE = 5;
    public static final int BIDI_RTL = 1;
    public static final int CURSOR_AFTER = 0;
    public static final int CURSOR_AT = 4;
    public static final int CURSOR_AT_OR_AFTER = 1;
    public static final int CURSOR_AT_OR_BEFORE = 3;
    public static final int CURSOR_BEFORE = 2;
    private static final int CURSOR_OPT_MAX_VALUE = 4;
    static final int DEFAULT_PAINT_FLAGS = 256;
    public static final int DEV_KERN_TEXT_FLAG = 256;
    public static final int DIRECTION_LTR = 0;
    public static final int DIRECTION_RTL = 1;
    public static final int DITHER_FLAG = 4;
    public static final int FAKE_BOLD_TEXT_FLAG = 32;
    public static final int FILTER_BITMAP_FLAG = 2;
    public static final int HINTING_OFF = 0;
    public static final int HINTING_ON = 1;
    public static final int LINEAR_TEXT_FLAG = 64;
    public static final int STRIKE_THRU_TEXT_FLAG = 16;
    public static final int SUBPIXEL_TEXT_FLAG = 128;
    public static final int UNDERLINE_TEXT_FLAG = 8;
    static final Align[] sAlignArray = arrayOfAlign;
    static final Cap[] sCapArray;
    static final Join[] sJoinArray;
    static final Style[] sStyleArray;
    public boolean hasShadow;
    public int mBidiFlags = 2;
    private ColorFilter mColorFilter;
    private float mCompatScaling;
    private boolean mHasCompatScaling;
    private float mInvCompatScaling;
    private Locale mLocale;
    private MaskFilter mMaskFilter;
    public int mNativePaint;
    private PathEffect mPathEffect;
    private Rasterizer mRasterizer;
    private Shader mShader;
    private Typeface mTypeface;
    private Xfermode mXfermode;
    public int shadowColor;
    public float shadowDx;
    public float shadowDy;
    public float shadowRadius;

    static
    {
        Style[] arrayOfStyle = new Style[3];
        arrayOfStyle[0] = Style.FILL;
        arrayOfStyle[1] = Style.STROKE;
        arrayOfStyle[2] = Style.FILL_AND_STROKE;
        sStyleArray = arrayOfStyle;
        Cap[] arrayOfCap = new Cap[3];
        arrayOfCap[0] = Cap.BUTT;
        arrayOfCap[1] = Cap.ROUND;
        arrayOfCap[2] = Cap.SQUARE;
        sCapArray = arrayOfCap;
        Join[] arrayOfJoin = new Join[3];
        arrayOfJoin[0] = Join.MITER;
        arrayOfJoin[1] = Join.ROUND;
        arrayOfJoin[2] = Join.BEVEL;
        sJoinArray = arrayOfJoin;
        Align[] arrayOfAlign = new Align[3];
        arrayOfAlign[0] = Align.LEFT;
        arrayOfAlign[1] = Align.CENTER;
        arrayOfAlign[2] = Align.RIGHT;
    }

    public Paint()
    {
        this(0);
    }

    public Paint(int paramInt)
    {
        this.mNativePaint = native_init();
        setFlags(paramInt | 0x100);
        this.mInvCompatScaling = 1.0F;
        this.mCompatScaling = 1.0F;
        setTextLocale(Locale.getDefault());
    }

    public Paint(Paint paramPaint)
    {
        this.mNativePaint = native_initWithPaint(paramPaint.mNativePaint);
        setClassVariablesFrom(paramPaint);
    }

    private static native void finalizer(int paramInt);

    private native void nSetShadowLayer(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt);

    private static native void nativeGetCharArrayBounds(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, Rect paramRect);

    private static native void nativeGetStringBounds(int paramInt1, String paramString, int paramInt2, int paramInt3, Rect paramRect);

    private native int native_breakText(String paramString, boolean paramBoolean, float paramFloat, float[] paramArrayOfFloat);

    private native int native_breakText(char[] paramArrayOfChar, int paramInt1, int paramInt2, float paramFloat, float[] paramArrayOfFloat);

    private static native boolean native_getFillPath(int paramInt1, int paramInt2, int paramInt3);

    private static native int native_getStrokeCap(int paramInt);

    private static native int native_getStrokeJoin(int paramInt);

    private static native int native_getStyle(int paramInt);

    private static native int native_getTextAlign(int paramInt);

    private static native int native_getTextGlyphs(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, char[] paramArrayOfChar);

    private static native void native_getTextPath(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5);

    private static native void native_getTextPath(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5);

    private static native float native_getTextRunAdvances(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, float[] paramArrayOfFloat, int paramInt7, int paramInt8);

    private static native float native_getTextRunAdvances(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, float[] paramArrayOfFloat, int paramInt7, int paramInt8);

    private native int native_getTextRunCursor(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

    private native int native_getTextRunCursor(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

    private static native int native_getTextWidths(int paramInt1, String paramString, int paramInt2, int paramInt3, float[] paramArrayOfFloat);

    private static native int native_getTextWidths(int paramInt1, char[] paramArrayOfChar, int paramInt2, int paramInt3, float[] paramArrayOfFloat);

    private static native int native_init();

    private static native int native_initWithPaint(int paramInt);

    private native float native_measureText(String paramString);

    private native float native_measureText(String paramString, int paramInt1, int paramInt2);

    private native float native_measureText(char[] paramArrayOfChar, int paramInt1, int paramInt2);

    private static native void native_reset(int paramInt);

    private static native void native_set(int paramInt1, int paramInt2);

    private static native int native_setColorFilter(int paramInt1, int paramInt2);

    private static native int native_setMaskFilter(int paramInt1, int paramInt2);

    private static native int native_setPathEffect(int paramInt1, int paramInt2);

    private static native int native_setRasterizer(int paramInt1, int paramInt2);

    private static native int native_setShader(int paramInt1, int paramInt2);

    private static native void native_setStrokeCap(int paramInt1, int paramInt2);

    private static native void native_setStrokeJoin(int paramInt1, int paramInt2);

    private static native void native_setStyle(int paramInt1, int paramInt2);

    private static native void native_setTextAlign(int paramInt1, int paramInt2);

    private static native void native_setTextLocale(int paramInt, String paramString);

    private static native int native_setTypeface(int paramInt1, int paramInt2);

    private static native int native_setXfermode(int paramInt1, int paramInt2);

    private void setClassVariablesFrom(Paint paramPaint)
    {
        this.mColorFilter = paramPaint.mColorFilter;
        this.mMaskFilter = paramPaint.mMaskFilter;
        this.mPathEffect = paramPaint.mPathEffect;
        this.mRasterizer = paramPaint.mRasterizer;
        this.mShader = paramPaint.mShader;
        this.mTypeface = paramPaint.mTypeface;
        this.mXfermode = paramPaint.mXfermode;
        this.mHasCompatScaling = paramPaint.mHasCompatScaling;
        this.mCompatScaling = paramPaint.mCompatScaling;
        this.mInvCompatScaling = paramPaint.mInvCompatScaling;
        this.hasShadow = paramPaint.hasShadow;
        this.shadowDx = paramPaint.shadowDx;
        this.shadowDy = paramPaint.shadowDy;
        this.shadowRadius = paramPaint.shadowRadius;
        this.shadowColor = paramPaint.shadowColor;
        this.mBidiFlags = paramPaint.mBidiFlags;
        this.mLocale = paramPaint.mLocale;
    }

    public native float ascent();

    public int breakText(CharSequence paramCharSequence, int paramInt1, int paramInt2, boolean paramBoolean, float paramFloat, float[] paramArrayOfFloat)
    {
        if (paramCharSequence == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramCharSequence.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        if ((paramCharSequence.length() == 0) || (paramInt1 == paramInt2));
        for (int i = 0; ; i = breakText((String)paramCharSequence, paramBoolean, paramFloat, paramArrayOfFloat))
        {
            return i;
            if ((paramInt1 != 0) || (!(paramCharSequence instanceof String)) || (paramInt2 != paramCharSequence.length()))
                break;
        }
        char[] arrayOfChar = TemporaryBuffer.obtain(paramInt2 - paramInt1);
        TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
        if (paramBoolean);
        for (i = breakText(arrayOfChar, 0, paramInt2 - paramInt1, paramFloat, paramArrayOfFloat); ; i = breakText(arrayOfChar, 0, -(paramInt2 - paramInt1), paramFloat, paramArrayOfFloat))
        {
            TemporaryBuffer.recycle(arrayOfChar);
            break;
        }
    }

    public int breakText(String paramString, boolean paramBoolean, float paramFloat, float[] paramArrayOfFloat)
    {
        if (paramString == null)
            throw new IllegalArgumentException("text cannot be null");
        int i;
        if (paramString.length() == 0)
            i = 0;
        while (true)
        {
            return i;
            if (!this.mHasCompatScaling)
            {
                i = native_breakText(paramString, paramBoolean, paramFloat, paramArrayOfFloat);
            }
            else
            {
                float f = getTextSize();
                setTextSize(f * this.mCompatScaling);
                i = native_breakText(paramString, paramBoolean, paramFloat * this.mCompatScaling, paramArrayOfFloat);
                setTextSize(f);
                if (paramArrayOfFloat != null)
                    paramArrayOfFloat[0] *= this.mInvCompatScaling;
            }
        }
    }

    public int breakText(char[] paramArrayOfChar, int paramInt1, int paramInt2, float paramFloat, float[] paramArrayOfFloat)
    {
        if (paramArrayOfChar == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt1 < 0) || (paramArrayOfChar.length - paramInt1 < Math.abs(paramInt2)))
            throw new ArrayIndexOutOfBoundsException();
        int i;
        if ((paramArrayOfChar.length == 0) || (paramInt2 == 0))
            i = 0;
        while (true)
        {
            return i;
            if (!this.mHasCompatScaling)
            {
                i = native_breakText(paramArrayOfChar, paramInt1, paramInt2, paramFloat, paramArrayOfFloat);
            }
            else
            {
                float f = getTextSize();
                setTextSize(f * this.mCompatScaling);
                i = native_breakText(paramArrayOfChar, paramInt1, paramInt2, paramFloat * this.mCompatScaling, paramArrayOfFloat);
                setTextSize(f);
                if (paramArrayOfFloat != null)
                    paramArrayOfFloat[0] *= this.mInvCompatScaling;
            }
        }
    }

    public void clearShadowLayer()
    {
        this.hasShadow = false;
        nSetShadowLayer(0.0F, 0.0F, 0.0F, 0);
    }

    public native float descent();

    protected void finalize()
        throws Throwable
    {
        try
        {
            finalizer(this.mNativePaint);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public native int getAlpha();

    public int getBidiFlags()
    {
        return this.mBidiFlags;
    }

    public native int getColor();

    public ColorFilter getColorFilter()
    {
        return this.mColorFilter;
    }

    public boolean getFillPath(Path paramPath1, Path paramPath2)
    {
        return native_getFillPath(this.mNativePaint, paramPath1.ni(), paramPath2.ni());
    }

    public native int getFlags();

    public native float getFontMetrics(FontMetrics paramFontMetrics);

    public FontMetrics getFontMetrics()
    {
        FontMetrics localFontMetrics = new FontMetrics();
        getFontMetrics(localFontMetrics);
        return localFontMetrics;
    }

    public native int getFontMetricsInt(FontMetricsInt paramFontMetricsInt);

    public FontMetricsInt getFontMetricsInt()
    {
        FontMetricsInt localFontMetricsInt = new FontMetricsInt();
        getFontMetricsInt(localFontMetricsInt);
        return localFontMetricsInt;
    }

    public float getFontSpacing()
    {
        return getFontMetrics(null);
    }

    public native int getHinting();

    public MaskFilter getMaskFilter()
    {
        return this.mMaskFilter;
    }

    public PathEffect getPathEffect()
    {
        return this.mPathEffect;
    }

    public Rasterizer getRasterizer()
    {
        return this.mRasterizer;
    }

    public Shader getShader()
    {
        return this.mShader;
    }

    public Cap getStrokeCap()
    {
        return sCapArray[native_getStrokeCap(this.mNativePaint)];
    }

    public Join getStrokeJoin()
    {
        return sJoinArray[native_getStrokeJoin(this.mNativePaint)];
    }

    public native float getStrokeMiter();

    public native float getStrokeWidth();

    public Style getStyle()
    {
        return sStyleArray[native_getStyle(this.mNativePaint)];
    }

    public Align getTextAlign()
    {
        return sAlignArray[native_getTextAlign(this.mNativePaint)];
    }

    public void getTextBounds(String paramString, int paramInt1, int paramInt2, Rect paramRect)
    {
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramString.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        if (paramRect == null)
            throw new NullPointerException("need bounds Rect");
        nativeGetStringBounds(this.mNativePaint, paramString, paramInt1, paramInt2, paramRect);
    }

    public void getTextBounds(char[] paramArrayOfChar, int paramInt1, int paramInt2, Rect paramRect)
    {
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length))
            throw new ArrayIndexOutOfBoundsException();
        if (paramRect == null)
            throw new NullPointerException("need bounds Rect");
        nativeGetCharArrayBounds(this.mNativePaint, paramArrayOfChar, paramInt1, paramInt2, paramRect);
    }

    public int getTextGlyphs(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, char[] paramArrayOfChar)
    {
        if (paramString == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt5 != 0) && (paramInt5 != 1))
            throw new IllegalArgumentException("unknown flags value: " + paramInt5);
        if ((paramInt4 | (paramInt3 | (paramInt1 | paramInt2)) | paramInt2 - paramInt1 | paramInt1 - paramInt3 | paramInt4 - paramInt2 | paramString.length() - paramInt2 | paramString.length() - paramInt4) < 0)
            throw new IndexOutOfBoundsException();
        if (paramInt2 - paramInt1 > paramArrayOfChar.length)
            throw new ArrayIndexOutOfBoundsException();
        return native_getTextGlyphs(this.mNativePaint, paramString, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfChar);
    }

    public Locale getTextLocale()
    {
        return this.mLocale;
    }

    public void getTextPath(String paramString, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Path paramPath)
    {
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramString.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        native_getTextPath(this.mNativePaint, this.mBidiFlags, paramString, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPath.ni());
    }

    public void getTextPath(char[] paramArrayOfChar, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Path paramPath)
    {
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length))
            throw new ArrayIndexOutOfBoundsException();
        native_getTextPath(this.mNativePaint, this.mBidiFlags, paramArrayOfChar, paramInt1, paramInt2, paramFloat1, paramFloat2, paramPath.ni());
    }

    public float getTextRunAdvances(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6)
    {
        return getTextRunAdvances(paramCharSequence, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, 0);
    }

    public float getTextRunAdvances(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, int paramInt7)
    {
        if (paramCharSequence == null)
            throw new IllegalArgumentException("text cannot be null");
        int i = paramInt6 | (paramInt4 | (paramInt3 | (paramInt1 | paramInt2))) | paramInt2 - paramInt1 | paramInt1 - paramInt3 | paramInt4 - paramInt2 | paramCharSequence.length() - paramInt4;
        if (paramArrayOfFloat == null);
        for (int j = 0; (j | i) < 0; j = paramArrayOfFloat.length - paramInt6 - (paramInt2 - paramInt1))
            throw new IndexOutOfBoundsException();
        float f;
        if ((paramCharSequence instanceof String))
            f = getTextRunAdvances((String)paramCharSequence, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
        while (true)
        {
            return f;
            if (((paramCharSequence instanceof SpannedString)) || ((paramCharSequence instanceof SpannableString)))
            {
                f = getTextRunAdvances(paramCharSequence.toString(), paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
            }
            else if ((paramCharSequence instanceof GraphicsOperations))
            {
                f = ((GraphicsOperations)paramCharSequence).getTextRunAdvances(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, this);
            }
            else if ((paramCharSequence.length() == 0) || (paramInt2 == paramInt1))
            {
                f = 0.0F;
            }
            else
            {
                int k = paramInt4 - paramInt3;
                int m = paramInt2 - paramInt1;
                char[] arrayOfChar = TemporaryBuffer.obtain(k);
                TextUtils.getChars(paramCharSequence, paramInt3, paramInt4, arrayOfChar, 0);
                f = getTextRunAdvances(arrayOfChar, paramInt1 - paramInt3, m, 0, k, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
                TemporaryBuffer.recycle(arrayOfChar);
            }
        }
    }

    public float getTextRunAdvances(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6)
    {
        return getTextRunAdvances(paramString, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, 0);
    }

    public float getTextRunAdvances(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, int paramInt7)
    {
        if (paramString == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt5 != 0) && (paramInt5 != 1))
            throw new IllegalArgumentException("unknown flags value: " + paramInt5);
        int i = paramInt6 | (paramInt4 | (paramInt3 | (paramInt1 | paramInt2))) | paramInt2 - paramInt1 | paramInt1 - paramInt3 | paramInt4 - paramInt2 | paramString.length() - paramInt4;
        if (paramArrayOfFloat == null);
        for (int j = 0; (j | i) < 0; j = paramArrayOfFloat.length - paramInt6 - (paramInt2 - paramInt1))
            throw new IndexOutOfBoundsException();
        float f1;
        if ((paramString.length() == 0) || (paramInt1 == paramInt2))
            f1 = 0.0F;
        while (true)
        {
            return f1;
            if (!this.mHasCompatScaling)
            {
                f1 = native_getTextRunAdvances(this.mNativePaint, paramString, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
            }
            else
            {
                float f2 = getTextSize();
                setTextSize(f2 * this.mCompatScaling);
                float f3 = native_getTextRunAdvances(this.mNativePaint, paramString, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
                setTextSize(f2);
                if (paramArrayOfFloat != null)
                {
                    int k = paramInt6;
                    int m = k + (paramInt2 - paramInt1);
                    while (k < m)
                    {
                        paramArrayOfFloat[k] *= this.mInvCompatScaling;
                        k++;
                    }
                }
                f1 = f3 * this.mInvCompatScaling;
            }
        }
    }

    public float getTextRunAdvances(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6)
    {
        return getTextRunAdvances(paramArrayOfChar, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, 0);
    }

    public float getTextRunAdvances(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, int paramInt7)
    {
        if (paramArrayOfChar == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt5 != 0) && (paramInt5 != 1))
            throw new IllegalArgumentException("unknown flags value: " + paramInt5);
        int i = paramInt6 | (paramInt4 | (paramInt3 | (paramInt1 | paramInt2))) | paramInt1 - paramInt3 | paramInt4 - paramInt2 | paramInt3 + paramInt4 - (paramInt1 + paramInt2) | paramArrayOfChar.length - (paramInt3 + paramInt4);
        if (paramArrayOfFloat == null);
        for (int j = 0; (j | i) < 0; j = paramArrayOfFloat.length - (paramInt6 + paramInt2))
            throw new IndexOutOfBoundsException();
        float f1;
        if ((paramArrayOfChar.length == 0) || (paramInt2 == 0))
            f1 = 0.0F;
        while (true)
        {
            return f1;
            if (!this.mHasCompatScaling)
            {
                f1 = native_getTextRunAdvances(this.mNativePaint, paramArrayOfChar, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
            }
            else
            {
                float f2 = getTextSize();
                setTextSize(f2 * this.mCompatScaling);
                float f3 = native_getTextRunAdvances(this.mNativePaint, paramArrayOfChar, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
                setTextSize(f2);
                if (paramArrayOfFloat != null)
                {
                    int k = paramInt6;
                    int m = k + paramInt2;
                    while (k < m)
                    {
                        paramArrayOfFloat[k] *= this.mInvCompatScaling;
                        k++;
                    }
                }
                f1 = f3 * this.mInvCompatScaling;
            }
        }
    }

    public int getTextRunCursor(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        int i;
        if (((paramCharSequence instanceof String)) || ((paramCharSequence instanceof SpannedString)) || ((paramCharSequence instanceof SpannableString)))
            i = getTextRunCursor(paramCharSequence.toString(), paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
        while (true)
        {
            return i;
            if ((paramCharSequence instanceof GraphicsOperations))
            {
                i = ((GraphicsOperations)paramCharSequence).getTextRunCursor(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, this);
            }
            else
            {
                int j = paramInt2 - paramInt1;
                char[] arrayOfChar = TemporaryBuffer.obtain(j);
                TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
                i = getTextRunCursor(arrayOfChar, 0, j, paramInt3, paramInt4 - paramInt1, paramInt5);
                TemporaryBuffer.recycle(arrayOfChar);
            }
        }
    }

    public int getTextRunCursor(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        if (((paramInt5 | (paramInt4 | (paramInt1 | paramInt2) | paramInt2 - paramInt1 | paramInt4 - paramInt1 | paramInt2 - paramInt4 | paramString.length() - paramInt2)) < 0) || (paramInt5 > 4))
            throw new IndexOutOfBoundsException();
        return native_getTextRunCursor(this.mNativePaint, paramString, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    }

    public int getTextRunCursor(char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        int i = paramInt1 + paramInt2;
        if (((paramInt5 | (paramInt4 | (paramInt1 | i) | i - paramInt1 | paramInt4 - paramInt1 | i - paramInt4 | paramArrayOfChar.length - i)) < 0) || (paramInt5 > 4))
            throw new IndexOutOfBoundsException();
        return native_getTextRunCursor(this.mNativePaint, paramArrayOfChar, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    }

    public native float getTextScaleX();

    public native float getTextSize();

    public native float getTextSkewX();

    public int getTextWidths(CharSequence paramCharSequence, int paramInt1, int paramInt2, float[] paramArrayOfFloat)
    {
        int i = 0;
        if (paramCharSequence == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramCharSequence.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        if (paramInt2 - paramInt1 > paramArrayOfFloat.length)
            throw new ArrayIndexOutOfBoundsException();
        if ((paramCharSequence.length() == 0) || (paramInt1 == paramInt2));
        while (true)
        {
            return i;
            if ((paramCharSequence instanceof String))
            {
                i = getTextWidths((String)paramCharSequence, paramInt1, paramInt2, paramArrayOfFloat);
            }
            else if (((paramCharSequence instanceof SpannedString)) || ((paramCharSequence instanceof SpannableString)))
            {
                i = getTextWidths(paramCharSequence.toString(), paramInt1, paramInt2, paramArrayOfFloat);
            }
            else if ((paramCharSequence instanceof GraphicsOperations))
            {
                i = ((GraphicsOperations)paramCharSequence).getTextWidths(paramInt1, paramInt2, paramArrayOfFloat, this);
            }
            else
            {
                char[] arrayOfChar = TemporaryBuffer.obtain(paramInt2 - paramInt1);
                TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
                i = getTextWidths(arrayOfChar, 0, paramInt2 - paramInt1, paramArrayOfFloat);
                TemporaryBuffer.recycle(arrayOfChar);
            }
        }
    }

    public int getTextWidths(String paramString, int paramInt1, int paramInt2, float[] paramArrayOfFloat)
    {
        if (paramString == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramString.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        if (paramInt2 - paramInt1 > paramArrayOfFloat.length)
            throw new ArrayIndexOutOfBoundsException();
        int i;
        if ((paramString.length() == 0) || (paramInt1 == paramInt2))
            i = 0;
        while (true)
        {
            return i;
            if (!this.mHasCompatScaling)
            {
                i = native_getTextWidths(this.mNativePaint, paramString, paramInt1, paramInt2, paramArrayOfFloat);
            }
            else
            {
                float f = getTextSize();
                setTextSize(f * this.mCompatScaling);
                i = native_getTextWidths(this.mNativePaint, paramString, paramInt1, paramInt2, paramArrayOfFloat);
                setTextSize(f);
                for (int j = 0; j < i; j++)
                    paramArrayOfFloat[j] *= this.mInvCompatScaling;
            }
        }
    }

    public int getTextWidths(String paramString, float[] paramArrayOfFloat)
    {
        return getTextWidths(paramString, 0, paramString.length(), paramArrayOfFloat);
    }

    public int getTextWidths(char[] paramArrayOfChar, int paramInt1, int paramInt2, float[] paramArrayOfFloat)
    {
        if (paramArrayOfChar == null)
            throw new IllegalArgumentException("text cannot be null");
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length) || (paramInt2 > paramArrayOfFloat.length))
            throw new ArrayIndexOutOfBoundsException();
        int i;
        if ((paramArrayOfChar.length == 0) || (paramInt2 == 0))
            i = 0;
        while (true)
        {
            return i;
            if (!this.mHasCompatScaling)
            {
                i = native_getTextWidths(this.mNativePaint, paramArrayOfChar, paramInt1, paramInt2, paramArrayOfFloat);
            }
            else
            {
                float f = getTextSize();
                setTextSize(f * this.mCompatScaling);
                i = native_getTextWidths(this.mNativePaint, paramArrayOfChar, paramInt1, paramInt2, paramArrayOfFloat);
                setTextSize(f);
                for (int j = 0; j < i; j++)
                    paramArrayOfFloat[j] *= this.mInvCompatScaling;
            }
        }
    }

    public Typeface getTypeface()
    {
        return this.mTypeface;
    }

    public Xfermode getXfermode()
    {
        return this.mXfermode;
    }

    public final boolean isAntiAlias()
    {
        if ((0x1 & getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isDither()
    {
        if ((0x4 & getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isFakeBoldText()
    {
        if ((0x20 & getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isFilterBitmap()
    {
        if ((0x2 & getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @Deprecated
    public final boolean isLinearText()
    {
        if ((0x40 & getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isStrikeThruText()
    {
        if ((0x10 & getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isSubpixelText()
    {
        if ((0x80 & getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isUnderlineText()
    {
        if ((0x8 & getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public float measureText(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        if (paramCharSequence == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramCharSequence.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        float f;
        if ((paramCharSequence.length() == 0) || (paramInt1 == paramInt2))
            f = 0.0F;
        while (true)
        {
            return f;
            if ((paramCharSequence instanceof String))
            {
                f = measureText((String)paramCharSequence, paramInt1, paramInt2);
            }
            else if (((paramCharSequence instanceof SpannedString)) || ((paramCharSequence instanceof SpannableString)))
            {
                f = measureText(paramCharSequence.toString(), paramInt1, paramInt2);
            }
            else if ((paramCharSequence instanceof GraphicsOperations))
            {
                f = ((GraphicsOperations)paramCharSequence).measureText(paramInt1, paramInt2, this);
            }
            else
            {
                char[] arrayOfChar = TemporaryBuffer.obtain(paramInt2 - paramInt1);
                TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
                f = measureText(arrayOfChar, 0, paramInt2 - paramInt1);
                TemporaryBuffer.recycle(arrayOfChar);
            }
        }
    }

    public float measureText(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("text cannot be null");
        float f3;
        if (paramString.length() == 0)
            f3 = 0.0F;
        while (true)
        {
            return f3;
            if (!this.mHasCompatScaling)
            {
                f3 = native_measureText(paramString);
            }
            else
            {
                float f1 = getTextSize();
                setTextSize(f1 * this.mCompatScaling);
                float f2 = native_measureText(paramString);
                setTextSize(f1);
                f3 = f2 * this.mInvCompatScaling;
            }
        }
    }

    public float measureText(String paramString, int paramInt1, int paramInt2)
    {
        if (paramString == null)
            throw new IllegalArgumentException("text cannot be null");
        if ((paramInt1 | paramInt2 | paramInt2 - paramInt1 | paramString.length() - paramInt2) < 0)
            throw new IndexOutOfBoundsException();
        float f1;
        if ((paramString.length() == 0) || (paramInt1 == paramInt2))
            f1 = 0.0F;
        while (true)
        {
            return f1;
            if (!this.mHasCompatScaling)
            {
                f1 = native_measureText(paramString, paramInt1, paramInt2);
            }
            else
            {
                float f2 = getTextSize();
                setTextSize(f2 * this.mCompatScaling);
                float f3 = native_measureText(paramString, paramInt1, paramInt2);
                setTextSize(f2);
                f1 = f3 * this.mInvCompatScaling;
            }
        }
    }

    public float measureText(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        if (paramArrayOfChar == null)
            throw new IllegalArgumentException("text cannot be null");
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 + paramInt2 > paramArrayOfChar.length))
            throw new ArrayIndexOutOfBoundsException();
        float f1;
        if ((paramArrayOfChar.length == 0) || (paramInt2 == 0))
            f1 = 0.0F;
        while (true)
        {
            return f1;
            if (!this.mHasCompatScaling)
            {
                f1 = native_measureText(paramArrayOfChar, paramInt1, paramInt2);
            }
            else
            {
                float f2 = getTextSize();
                setTextSize(f2 * this.mCompatScaling);
                float f3 = native_measureText(paramArrayOfChar, paramInt1, paramInt2);
                setTextSize(f2);
                f1 = f3 * this.mInvCompatScaling;
            }
        }
    }

    public void reset()
    {
        native_reset(this.mNativePaint);
        setFlags(256);
        this.mHasCompatScaling = false;
        this.mInvCompatScaling = 1.0F;
        this.mCompatScaling = 1.0F;
        this.mBidiFlags = 2;
        setTextLocale(Locale.getDefault());
    }

    public void set(Paint paramPaint)
    {
        if (this != paramPaint)
        {
            native_set(this.mNativePaint, paramPaint.mNativePaint);
            setClassVariablesFrom(paramPaint);
        }
    }

    public void setARGB(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        setColor(paramInt4 | (paramInt1 << 24 | paramInt2 << 16 | paramInt3 << 8));
    }

    public native void setAlpha(int paramInt);

    public native void setAntiAlias(boolean paramBoolean);

    public void setBidiFlags(int paramInt)
    {
        int i = paramInt & 0x7;
        if (i > 5)
            throw new IllegalArgumentException("unknown bidi flag: " + i);
        this.mBidiFlags = i;
    }

    public native void setColor(int paramInt);

    public ColorFilter setColorFilter(ColorFilter paramColorFilter)
    {
        int i = 0;
        if (paramColorFilter != null)
            i = paramColorFilter.native_instance;
        native_setColorFilter(this.mNativePaint, i);
        this.mColorFilter = paramColorFilter;
        return paramColorFilter;
    }

    public void setCompatibilityScaling(float paramFloat)
    {
        if (paramFloat == 1.0D)
        {
            this.mHasCompatScaling = false;
            this.mInvCompatScaling = 1.0F;
            this.mCompatScaling = 1.0F;
        }
        while (true)
        {
            return;
            this.mHasCompatScaling = true;
            this.mCompatScaling = paramFloat;
            this.mInvCompatScaling = (1.0F / paramFloat);
        }
    }

    public native void setDither(boolean paramBoolean);

    public native void setFakeBoldText(boolean paramBoolean);

    public native void setFilterBitmap(boolean paramBoolean);

    public native void setFlags(int paramInt);

    public native void setHinting(int paramInt);

    @Deprecated
    public native void setLinearText(boolean paramBoolean);

    public MaskFilter setMaskFilter(MaskFilter paramMaskFilter)
    {
        int i = 0;
        if (paramMaskFilter != null)
            i = paramMaskFilter.native_instance;
        native_setMaskFilter(this.mNativePaint, i);
        this.mMaskFilter = paramMaskFilter;
        return paramMaskFilter;
    }

    public PathEffect setPathEffect(PathEffect paramPathEffect)
    {
        int i = 0;
        if (paramPathEffect != null)
            i = paramPathEffect.native_instance;
        native_setPathEffect(this.mNativePaint, i);
        this.mPathEffect = paramPathEffect;
        return paramPathEffect;
    }

    public Rasterizer setRasterizer(Rasterizer paramRasterizer)
    {
        int i = 0;
        if (paramRasterizer != null)
            i = paramRasterizer.native_instance;
        native_setRasterizer(this.mNativePaint, i);
        this.mRasterizer = paramRasterizer;
        return paramRasterizer;
    }

    public Shader setShader(Shader paramShader)
    {
        int i = 0;
        if (paramShader != null)
            i = paramShader.native_instance;
        native_setShader(this.mNativePaint, i);
        this.mShader = paramShader;
        return paramShader;
    }

    public void setShadowLayer(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt)
    {
        if (paramFloat1 > 0.0F);
        for (boolean bool = true; ; bool = false)
        {
            this.hasShadow = bool;
            this.shadowRadius = paramFloat1;
            this.shadowDx = paramFloat2;
            this.shadowDy = paramFloat3;
            this.shadowColor = paramInt;
            nSetShadowLayer(paramFloat1, paramFloat2, paramFloat3, paramInt);
            return;
        }
    }

    public native void setStrikeThruText(boolean paramBoolean);

    public void setStrokeCap(Cap paramCap)
    {
        native_setStrokeCap(this.mNativePaint, paramCap.nativeInt);
    }

    public void setStrokeJoin(Join paramJoin)
    {
        native_setStrokeJoin(this.mNativePaint, paramJoin.nativeInt);
    }

    public native void setStrokeMiter(float paramFloat);

    public native void setStrokeWidth(float paramFloat);

    public void setStyle(Style paramStyle)
    {
        native_setStyle(this.mNativePaint, paramStyle.nativeInt);
    }

    public native void setSubpixelText(boolean paramBoolean);

    public void setTextAlign(Align paramAlign)
    {
        native_setTextAlign(this.mNativePaint, paramAlign.nativeInt);
    }

    public void setTextLocale(Locale paramLocale)
    {
        if (paramLocale == null)
            throw new IllegalArgumentException("locale cannot be null");
        if (paramLocale.equals(this.mLocale));
        while (true)
        {
            return;
            this.mLocale = paramLocale;
            native_setTextLocale(this.mNativePaint, paramLocale.toString());
        }
    }

    public native void setTextScaleX(float paramFloat);

    public native void setTextSize(float paramFloat);

    public native void setTextSkewX(float paramFloat);

    public Typeface setTypeface(Typeface paramTypeface)
    {
        int i = 0;
        if (paramTypeface != null)
            i = paramTypeface.native_instance;
        native_setTypeface(this.mNativePaint, i);
        this.mTypeface = paramTypeface;
        return paramTypeface;
    }

    public native void setUnderlineText(boolean paramBoolean);

    public Xfermode setXfermode(Xfermode paramXfermode)
    {
        int i = 0;
        if (paramXfermode != null)
            i = paramXfermode.native_instance;
        native_setXfermode(this.mNativePaint, i);
        this.mXfermode = paramXfermode;
        return paramXfermode;
    }

    public static class FontMetricsInt
    {
        public int ascent;
        public int bottom;
        public int descent;
        public int leading;
        public int top;

        public String toString()
        {
            return "FontMetricsInt: top=" + this.top + " ascent=" + this.ascent + " descent=" + this.descent + " bottom=" + this.bottom + " leading=" + this.leading;
        }
    }

    public static class FontMetrics
    {
        public float ascent;
        public float bottom;
        public float descent;
        public float leading;
        public float top;
    }

    public static enum Align
    {
        final int nativeInt;

        static
        {
            CENTER = new Align("CENTER", 1, 1);
            RIGHT = new Align("RIGHT", 2, 2);
            Align[] arrayOfAlign = new Align[3];
            arrayOfAlign[0] = LEFT;
            arrayOfAlign[1] = CENTER;
            arrayOfAlign[2] = RIGHT;
        }

        private Align(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }

    public static enum Join
    {
        final int nativeInt;

        static
        {
            BEVEL = new Join("BEVEL", 2, 2);
            Join[] arrayOfJoin = new Join[3];
            arrayOfJoin[0] = MITER;
            arrayOfJoin[1] = ROUND;
            arrayOfJoin[2] = BEVEL;
        }

        private Join(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }

    public static enum Cap
    {
        final int nativeInt;

        static
        {
            Cap[] arrayOfCap = new Cap[3];
            arrayOfCap[0] = BUTT;
            arrayOfCap[1] = ROUND;
            arrayOfCap[2] = SQUARE;
        }

        private Cap(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }

    public static enum Style
    {
        final int nativeInt;

        static
        {
            FILL_AND_STROKE = new Style("FILL_AND_STROKE", 2, 2);
            Style[] arrayOfStyle = new Style[3];
            arrayOfStyle[0] = FILL;
            arrayOfStyle[1] = STROKE;
            arrayOfStyle[2] = FILL_AND_STROKE;
        }

        private Style(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Paint
 * JD-Core Version:        0.6.2
 */