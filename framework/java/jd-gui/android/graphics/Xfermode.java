package android.graphics;

public class Xfermode
{
    int native_instance;

    private static native void finalizer(int paramInt);

    protected void finalize()
        throws Throwable
    {
        try
        {
            finalizer(this.native_instance);
            return;
        }
        finally
        {
            super.finalize();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Xfermode
 * JD-Core Version:        0.6.2
 */