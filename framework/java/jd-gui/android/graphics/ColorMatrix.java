package android.graphics;

import android.util.FloatMath;

public class ColorMatrix
{
    private final float[] mArray = new float[20];

    public ColorMatrix()
    {
        reset();
    }

    public ColorMatrix(ColorMatrix paramColorMatrix)
    {
        System.arraycopy(paramColorMatrix.mArray, 0, this.mArray, 0, 20);
    }

    public ColorMatrix(float[] paramArrayOfFloat)
    {
        System.arraycopy(paramArrayOfFloat, 0, this.mArray, 0, 20);
    }

    public final float[] getArray()
    {
        return this.mArray;
    }

    public void postConcat(ColorMatrix paramColorMatrix)
    {
        setConcat(paramColorMatrix, this);
    }

    public void preConcat(ColorMatrix paramColorMatrix)
    {
        setConcat(this, paramColorMatrix);
    }

    public void reset()
    {
        float[] arrayOfFloat = this.mArray;
        for (int i = 19; i > 0; i--)
            arrayOfFloat[i] = 0.0F;
        arrayOfFloat[18] = 1.0F;
        arrayOfFloat[12] = 1.0F;
        arrayOfFloat[6] = 1.0F;
        arrayOfFloat[0] = 1.0F;
    }

    public void set(ColorMatrix paramColorMatrix)
    {
        System.arraycopy(paramColorMatrix.mArray, 0, this.mArray, 0, 20);
    }

    public void set(float[] paramArrayOfFloat)
    {
        System.arraycopy(paramArrayOfFloat, 0, this.mArray, 0, 20);
    }

    public void setConcat(ColorMatrix paramColorMatrix1, ColorMatrix paramColorMatrix2)
    {
        float[] arrayOfFloat1;
        float[] arrayOfFloat2;
        float[] arrayOfFloat3;
        int i;
        if ((paramColorMatrix1 == this) || (paramColorMatrix2 == this))
        {
            arrayOfFloat1 = new float[20];
            arrayOfFloat2 = paramColorMatrix1.mArray;
            arrayOfFloat3 = paramColorMatrix2.mArray;
            i = 0;
        }
        for (int j = 0; ; j += 5)
        {
            if (j >= 20)
                break label224;
            int k = 0;
            int m = i;
            while (true)
                if (k < 4)
                {
                    int n = m + 1;
                    arrayOfFloat1[m] = (arrayOfFloat2[(j + 0)] * arrayOfFloat3[(k + 0)] + arrayOfFloat2[(j + 1)] * arrayOfFloat3[(k + 5)] + arrayOfFloat2[(j + 2)] * arrayOfFloat3[(k + 10)] + arrayOfFloat2[(j + 3)] * arrayOfFloat3[(k + 15)]);
                    k++;
                    m = n;
                    continue;
                    arrayOfFloat1 = this.mArray;
                    break;
                }
            i = m + 1;
            arrayOfFloat1[m] = (arrayOfFloat2[(j + 0)] * arrayOfFloat3[4] + arrayOfFloat2[(j + 1)] * arrayOfFloat3[9] + arrayOfFloat2[(j + 2)] * arrayOfFloat3[14] + arrayOfFloat2[(j + 3)] * arrayOfFloat3[19] + arrayOfFloat2[(j + 4)]);
        }
        label224: if (arrayOfFloat1 != this.mArray)
            System.arraycopy(arrayOfFloat1, 0, this.mArray, 0, 20);
    }

    public void setRGB2YUV()
    {
        reset();
        float[] arrayOfFloat = this.mArray;
        arrayOfFloat[0] = 0.299F;
        arrayOfFloat[1] = 0.587F;
        arrayOfFloat[2] = 0.114F;
        arrayOfFloat[5] = -0.16874F;
        arrayOfFloat[6] = -0.33126F;
        arrayOfFloat[7] = 0.5F;
        arrayOfFloat[10] = 0.5F;
        arrayOfFloat[11] = -0.41869F;
        arrayOfFloat[12] = -0.08131F;
    }

    public void setRotate(int paramInt, float paramFloat)
    {
        reset();
        float f1 = 3.141593F * paramFloat / 180.0F;
        float f2 = FloatMath.cos(f1);
        float f3 = FloatMath.sin(f1);
        switch (paramInt)
        {
        default:
            throw new RuntimeException();
        case 0:
            float[] arrayOfFloat3 = this.mArray;
            this.mArray[12] = f2;
            arrayOfFloat3[6] = f2;
            this.mArray[7] = f3;
            this.mArray[11] = (-f3);
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            float[] arrayOfFloat2 = this.mArray;
            this.mArray[12] = f2;
            arrayOfFloat2[0] = f2;
            this.mArray[2] = (-f3);
            this.mArray[10] = f3;
            continue;
            float[] arrayOfFloat1 = this.mArray;
            this.mArray[6] = f2;
            arrayOfFloat1[0] = f2;
            this.mArray[1] = f3;
            this.mArray[5] = (-f3);
        }
    }

    public void setSaturation(float paramFloat)
    {
        reset();
        float[] arrayOfFloat = this.mArray;
        float f1 = 1.0F - paramFloat;
        float f2 = 0.213F * f1;
        float f3 = 0.715F * f1;
        float f4 = 0.072F * f1;
        arrayOfFloat[0] = (f2 + paramFloat);
        arrayOfFloat[1] = f3;
        arrayOfFloat[2] = f4;
        arrayOfFloat[5] = f2;
        arrayOfFloat[6] = (f3 + paramFloat);
        arrayOfFloat[7] = f4;
        arrayOfFloat[10] = f2;
        arrayOfFloat[11] = f3;
        arrayOfFloat[12] = (f4 + paramFloat);
    }

    public void setScale(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        float[] arrayOfFloat = this.mArray;
        for (int i = 19; i > 0; i--)
            arrayOfFloat[i] = 0.0F;
        arrayOfFloat[0] = paramFloat1;
        arrayOfFloat[6] = paramFloat2;
        arrayOfFloat[12] = paramFloat3;
        arrayOfFloat[18] = paramFloat4;
    }

    public void setYUV2RGB()
    {
        reset();
        float[] arrayOfFloat = this.mArray;
        arrayOfFloat[2] = 1.402F;
        arrayOfFloat[5] = 1.0F;
        arrayOfFloat[6] = -0.34414F;
        arrayOfFloat[7] = -0.71414F;
        arrayOfFloat[10] = 1.0F;
        arrayOfFloat[11] = 1.772F;
        arrayOfFloat[12] = 0.0F;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.ColorMatrix
 * JD-Core Version:        0.6.2
 */