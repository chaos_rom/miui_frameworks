package android.graphics;

public class RadialGradient extends Shader
{
    public RadialGradient(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt1, int paramInt2, Shader.TileMode paramTileMode)
    {
        if (paramFloat3 <= 0.0F)
            throw new IllegalArgumentException("radius must be > 0");
        this.native_instance = nativeCreate2(paramFloat1, paramFloat2, paramFloat3, paramInt1, paramInt2, paramTileMode.nativeInt);
        this.native_shader = nativePostCreate2(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramInt1, paramInt2, paramTileMode.nativeInt);
    }

    public RadialGradient(float paramFloat1, float paramFloat2, float paramFloat3, int[] paramArrayOfInt, float[] paramArrayOfFloat, Shader.TileMode paramTileMode)
    {
        if (paramFloat3 <= 0.0F)
            throw new IllegalArgumentException("radius must be > 0");
        if (paramArrayOfInt.length < 2)
            throw new IllegalArgumentException("needs >= 2 number of colors");
        if ((paramArrayOfFloat != null) && (paramArrayOfInt.length != paramArrayOfFloat.length))
            throw new IllegalArgumentException("color and position arrays must be of equal length");
        this.native_instance = nativeCreate1(paramFloat1, paramFloat2, paramFloat3, paramArrayOfInt, paramArrayOfFloat, paramTileMode.nativeInt);
        this.native_shader = nativePostCreate1(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramArrayOfInt, paramArrayOfFloat, paramTileMode.nativeInt);
    }

    private static native int nativeCreate1(float paramFloat1, float paramFloat2, float paramFloat3, int[] paramArrayOfInt, float[] paramArrayOfFloat, int paramInt);

    private static native int nativeCreate2(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt1, int paramInt2, int paramInt3);

    private static native int nativePostCreate1(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, int[] paramArrayOfInt, float[] paramArrayOfFloat, int paramInt2);

    private static native int nativePostCreate2(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, int paramInt2, int paramInt3, int paramInt4);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.RadialGradient
 * JD-Core Version:        0.6.2
 */