package android.graphics;

public class DashPathEffect extends PathEffect
{
    public DashPathEffect(float[] paramArrayOfFloat, float paramFloat)
    {
        if (paramArrayOfFloat.length < 2)
            throw new ArrayIndexOutOfBoundsException();
        this.native_instance = nativeCreate(paramArrayOfFloat, paramFloat);
    }

    private static native int nativeCreate(float[] paramArrayOfFloat, float paramFloat);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.DashPathEffect
 * JD-Core Version:        0.6.2
 */