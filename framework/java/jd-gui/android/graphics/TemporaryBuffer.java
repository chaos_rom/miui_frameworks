package android.graphics;

import com.android.internal.util.ArrayUtils;

public class TemporaryBuffer
{
    private static char[] sTemp = null;

    public static char[] obtain(int paramInt)
    {
        try
        {
            char[] arrayOfChar = sTemp;
            sTemp = null;
            if ((arrayOfChar == null) || (arrayOfChar.length < paramInt))
                arrayOfChar = new char[ArrayUtils.idealCharArraySize(paramInt)];
            return arrayOfChar;
        }
        finally
        {
        }
    }

    public static void recycle(char[] paramArrayOfChar)
    {
        if (paramArrayOfChar.length > 1000);
        while (true)
        {
            return;
            try
            {
                sTemp = paramArrayOfChar;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.TemporaryBuffer
 * JD-Core Version:        0.6.2
 */