package android.graphics;

import java.io.OutputStream;

public class YuvImage
{
    private static final int WORKING_COMPRESS_STORAGE = 4096;
    private byte[] mData;
    private int mFormat;
    private int mHeight;
    private int[] mStrides;
    private int mWidth;

    public YuvImage(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt)
    {
        if ((paramInt1 != 17) && (paramInt1 != 20))
            throw new IllegalArgumentException("only support ImageFormat.NV21 and ImageFormat.YUY2 for now");
        if ((paramInt2 <= 0) || (paramInt3 <= 0))
            throw new IllegalArgumentException("width and height must large than 0");
        if (paramArrayOfByte == null)
            throw new IllegalArgumentException("yuv cannot be null");
        if (paramArrayOfInt == null);
        for (this.mStrides = calculateStrides(paramInt2, paramInt1); ; this.mStrides = paramArrayOfInt)
        {
            this.mData = paramArrayOfByte;
            this.mFormat = paramInt1;
            this.mWidth = paramInt2;
            this.mHeight = paramInt3;
            return;
        }
    }

    private void adjustRectangle(Rect paramRect)
    {
        int i = paramRect.width();
        int j = paramRect.height();
        if (this.mFormat == 17)
        {
            i &= -2;
            int m = j & 0xFFFFFFFE;
            paramRect.left = (0xFFFFFFFE & paramRect.left);
            paramRect.top = (0xFFFFFFFE & paramRect.top);
            paramRect.right = (i + paramRect.left);
            paramRect.bottom = (m + paramRect.top);
        }
        if (this.mFormat == 20)
        {
            int k = i & 0xFFFFFFFE;
            paramRect.left = (0xFFFFFFFE & paramRect.left);
            paramRect.right = (k + paramRect.left);
        }
    }

    private int[] calculateStrides(int paramInt1, int paramInt2)
    {
        Object localObject;
        if (paramInt2 == 17)
        {
            int[] arrayOfInt2 = new int[2];
            arrayOfInt2[0] = paramInt1;
            arrayOfInt2[1] = paramInt1;
            localObject = arrayOfInt2;
        }
        while (true)
        {
            return localObject;
            if (paramInt2 == 20)
            {
                int[] arrayOfInt1 = new int[1];
                arrayOfInt1[0] = (paramInt1 * 2);
                localObject = arrayOfInt1;
            }
            else
            {
                localObject = null;
            }
        }
    }

    private static native boolean nativeCompressToJpeg(byte[] paramArrayOfByte1, int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt4, OutputStream paramOutputStream, byte[] paramArrayOfByte2);

    int[] calculateOffsets(int paramInt1, int paramInt2)
    {
        Object localObject;
        if (this.mFormat == 17)
        {
            int[] arrayOfInt2 = new int[2];
            arrayOfInt2[0] = (paramInt1 + paramInt2 * this.mStrides[0]);
            arrayOfInt2[1] = (this.mHeight * this.mStrides[0] + paramInt2 / 2 * this.mStrides[1] + 2 * (paramInt1 / 2));
            localObject = arrayOfInt2;
        }
        while (true)
        {
            return localObject;
            if (this.mFormat == 20)
            {
                int[] arrayOfInt1 = new int[1];
                arrayOfInt1[0] = (paramInt2 * this.mStrides[0] + 4 * (paramInt1 / 2));
                localObject = arrayOfInt1;
            }
            else
            {
                localObject = null;
            }
        }
    }

    public boolean compressToJpeg(Rect paramRect, int paramInt, OutputStream paramOutputStream)
    {
        if (!new Rect(0, 0, this.mWidth, this.mHeight).contains(paramRect))
            throw new IllegalArgumentException("rectangle is not inside the image");
        if ((paramInt < 0) || (paramInt > 100))
            throw new IllegalArgumentException("quality must be 0..100");
        if (paramOutputStream == null)
            throw new IllegalArgumentException("stream cannot be null");
        adjustRectangle(paramRect);
        int[] arrayOfInt = calculateOffsets(paramRect.left, paramRect.top);
        return nativeCompressToJpeg(this.mData, this.mFormat, paramRect.width(), paramRect.height(), arrayOfInt, this.mStrides, paramInt, paramOutputStream, new byte[4096]);
    }

    public int getHeight()
    {
        return this.mHeight;
    }

    public int[] getStrides()
    {
        return this.mStrides;
    }

    public int getWidth()
    {
        return this.mWidth;
    }

    public byte[] getYuvData()
    {
        return this.mData;
    }

    public int getYuvFormat()
    {
        return this.mFormat;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.YuvImage
 * JD-Core Version:        0.6.2
 */