package android.graphics;

public class DrawFilter
{
    int mNativeInt;

    private static native void nativeDestructor(int paramInt);

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDestructor(this.mNativeInt);
            return;
        }
        finally
        {
            super.finalize();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.DrawFilter
 * JD-Core Version:        0.6.2
 */