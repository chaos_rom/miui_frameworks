package android.graphics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Movie
{
    private final int mNativeMovie;

    private Movie(int paramInt)
    {
        if (paramInt == 0)
            throw new RuntimeException("native movie creation failed");
        this.mNativeMovie = paramInt;
    }

    public static native Movie decodeByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2);

    public static Movie decodeFile(String paramString)
    {
        try
        {
            FileInputStream localFileInputStream = new FileInputStream(paramString);
            localMovie = decodeTempStream(localFileInputStream);
            return localMovie;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            while (true)
                Movie localMovie = null;
        }
    }

    public static native Movie decodeStream(InputStream paramInputStream);

    private static Movie decodeTempStream(InputStream paramInputStream)
    {
        Movie localMovie = null;
        try
        {
            localMovie = decodeStream(paramInputStream);
            paramInputStream.close();
            label11: return localMovie;
        }
        catch (IOException localIOException)
        {
            break label11;
        }
    }

    private static native void nativeDestructor(int paramInt);

    public void draw(Canvas paramCanvas, float paramFloat1, float paramFloat2)
    {
        draw(paramCanvas, paramFloat1, paramFloat2, null);
    }

    public native void draw(Canvas paramCanvas, float paramFloat1, float paramFloat2, Paint paramPaint);

    public native int duration();

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDestructor(this.mNativeMovie);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public native int height();

    public native boolean isOpaque();

    public native boolean setTime(int paramInt);

    public native int width();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Movie
 * JD-Core Version:        0.6.2
 */