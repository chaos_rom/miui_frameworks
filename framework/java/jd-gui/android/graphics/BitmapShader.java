package android.graphics;

public class BitmapShader extends Shader
{
    public final Bitmap mBitmap;

    public BitmapShader(Bitmap paramBitmap, Shader.TileMode paramTileMode1, Shader.TileMode paramTileMode2)
    {
        this.mBitmap = paramBitmap;
        int i = paramBitmap.ni();
        this.native_instance = nativeCreate(i, paramTileMode1.nativeInt, paramTileMode2.nativeInt);
        this.native_shader = nativePostCreate(this.native_instance, i, paramTileMode1.nativeInt, paramTileMode2.nativeInt);
    }

    private static native int nativeCreate(int paramInt1, int paramInt2, int paramInt3);

    private static native int nativePostCreate(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.BitmapShader
 * JD-Core Version:        0.6.2
 */