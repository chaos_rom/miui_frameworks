package android.graphics;

public class PathMeasure
{
    public static final int POSITION_MATRIX_FLAG = 1;
    public static final int TANGENT_MATRIX_FLAG = 2;
    private Path mPath;
    private final int native_instance;

    public PathMeasure()
    {
        this.mPath = null;
        this.native_instance = native_create(0, false);
    }

    public PathMeasure(Path paramPath, boolean paramBoolean)
    {
        this.mPath = paramPath;
        if (paramPath != null);
        for (int i = paramPath.ni(); ; i = 0)
        {
            this.native_instance = native_create(i, paramBoolean);
            return;
        }
    }

    private static native int native_create(int paramInt, boolean paramBoolean);

    private static native void native_destroy(int paramInt);

    private static native float native_getLength(int paramInt);

    private static native boolean native_getMatrix(int paramInt1, float paramFloat, int paramInt2, int paramInt3);

    private static native boolean native_getPosTan(int paramInt, float paramFloat, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2);

    private static native boolean native_getSegment(int paramInt1, float paramFloat1, float paramFloat2, int paramInt2, boolean paramBoolean);

    private static native boolean native_isClosed(int paramInt);

    private static native boolean native_nextContour(int paramInt);

    private static native void native_setPath(int paramInt1, int paramInt2, boolean paramBoolean);

    protected void finalize()
        throws Throwable
    {
        native_destroy(this.native_instance);
    }

    public float getLength()
    {
        return native_getLength(this.native_instance);
    }

    public boolean getMatrix(float paramFloat, Matrix paramMatrix, int paramInt)
    {
        return native_getMatrix(this.native_instance, paramFloat, paramMatrix.native_instance, paramInt);
    }

    public boolean getPosTan(float paramFloat, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        if (((paramArrayOfFloat1 != null) && (paramArrayOfFloat1.length < 2)) || ((paramArrayOfFloat2 != null) && (paramArrayOfFloat2.length < 2)))
            throw new ArrayIndexOutOfBoundsException();
        return native_getPosTan(this.native_instance, paramFloat, paramArrayOfFloat1, paramArrayOfFloat2);
    }

    public boolean getSegment(float paramFloat1, float paramFloat2, Path paramPath, boolean paramBoolean)
    {
        return native_getSegment(this.native_instance, paramFloat1, paramFloat2, paramPath.ni(), paramBoolean);
    }

    public boolean isClosed()
    {
        return native_isClosed(this.native_instance);
    }

    public boolean nextContour()
    {
        return native_nextContour(this.native_instance);
    }

    public void setPath(Path paramPath, boolean paramBoolean)
    {
        this.mPath = paramPath;
        int i = this.native_instance;
        if (paramPath != null);
        for (int j = paramPath.ni(); ; j = 0)
        {
            native_setPath(i, j, paramBoolean);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PathMeasure
 * JD-Core Version:        0.6.2
 */