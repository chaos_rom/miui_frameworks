package android.graphics;

public class PixelFormat
{
    public static final int A_8 = 8;

    @Deprecated
    public static final int JPEG = 256;

    @Deprecated
    public static final int LA_88 = 10;
    public static final int L_8 = 9;
    public static final int OPAQUE = -1;

    @Deprecated
    public static final int RGBA_4444 = 7;

    @Deprecated
    public static final int RGBA_5551 = 6;
    public static final int RGBA_8888 = 1;
    public static final int RGBX_8888 = 2;

    @Deprecated
    public static final int RGB_332 = 11;
    public static final int RGB_565 = 4;
    public static final int RGB_888 = 3;
    public static final int TRANSLUCENT = -3;
    public static final int TRANSPARENT = -2;
    public static final int UNKNOWN = 0;

    @Deprecated
    public static final int YCbCr_420_SP = 17;

    @Deprecated
    public static final int YCbCr_422_I = 20;

    @Deprecated
    public static final int YCbCr_422_SP = 16;
    public int bitsPerPixel;
    public int bytesPerPixel;

    static
    {
        nativeClassInit();
    }

    public static boolean formatHasAlpha(int paramInt)
    {
        switch (paramInt)
        {
        case -1:
        case 0:
        case 2:
        case 3:
        case 4:
        case 5:
        case 9:
        default:
        case -3:
        case -2:
        case 1:
        case 6:
        case 7:
        case 8:
        case 10:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public static native void getPixelFormatInfo(int paramInt, PixelFormat paramPixelFormat);

    private static native void nativeClassInit();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PixelFormat
 * JD-Core Version:        0.6.2
 */