package android.graphics;

public class EmbossMaskFilter extends MaskFilter
{
    public EmbossMaskFilter(float[] paramArrayOfFloat, float paramFloat1, float paramFloat2, float paramFloat3)
    {
        if (paramArrayOfFloat.length < 3)
            throw new ArrayIndexOutOfBoundsException();
        this.native_instance = nativeConstructor(paramArrayOfFloat, paramFloat1, paramFloat2, paramFloat3);
    }

    private static native int nativeConstructor(float[] paramArrayOfFloat, float paramFloat1, float paramFloat2, float paramFloat3);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.EmbossMaskFilter
 * JD-Core Version:        0.6.2
 */