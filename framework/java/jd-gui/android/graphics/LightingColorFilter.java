package android.graphics;

public class LightingColorFilter extends ColorFilter
{
    public LightingColorFilter(int paramInt1, int paramInt2)
    {
        this.native_instance = native_CreateLightingFilter(paramInt1, paramInt2);
        this.nativeColorFilter = nCreateLightingFilter(this.native_instance, paramInt1, paramInt2);
    }

    private static native int nCreateLightingFilter(int paramInt1, int paramInt2, int paramInt3);

    private static native int native_CreateLightingFilter(int paramInt1, int paramInt2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.LightingColorFilter
 * JD-Core Version:        0.6.2
 */