package android.graphics;

public class TableMaskFilter extends MaskFilter
{
    private TableMaskFilter(int paramInt)
    {
        this.native_instance = paramInt;
    }

    public TableMaskFilter(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte.length < 256)
            throw new RuntimeException("table.length must be >= 256");
        this.native_instance = nativeNewTable(paramArrayOfByte);
    }

    public static TableMaskFilter CreateClipTable(int paramInt1, int paramInt2)
    {
        return new TableMaskFilter(nativeNewClip(paramInt1, paramInt2));
    }

    public static TableMaskFilter CreateGammaTable(float paramFloat)
    {
        return new TableMaskFilter(nativeNewGamma(paramFloat));
    }

    private static native int nativeNewClip(int paramInt1, int paramInt2);

    private static native int nativeNewGamma(float paramFloat);

    private static native int nativeNewTable(byte[] paramArrayOfByte);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.TableMaskFilter
 * JD-Core Version:        0.6.2
 */