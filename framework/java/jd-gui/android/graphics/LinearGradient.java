package android.graphics;

public class LinearGradient extends Shader
{
    public LinearGradient(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt1, int paramInt2, Shader.TileMode paramTileMode)
    {
        this.native_instance = nativeCreate2(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramInt1, paramInt2, paramTileMode.nativeInt);
        this.native_shader = nativePostCreate2(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramInt1, paramInt2, paramTileMode.nativeInt);
    }

    public LinearGradient(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int[] paramArrayOfInt, float[] paramArrayOfFloat, Shader.TileMode paramTileMode)
    {
        if (paramArrayOfInt.length < 2)
            throw new IllegalArgumentException("needs >= 2 number of colors");
        if ((paramArrayOfFloat != null) && (paramArrayOfInt.length != paramArrayOfFloat.length))
            throw new IllegalArgumentException("color and position arrays must be of equal length");
        this.native_instance = nativeCreate1(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramArrayOfInt, paramArrayOfFloat, paramTileMode.nativeInt);
        this.native_shader = nativePostCreate1(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramArrayOfInt, paramArrayOfFloat, paramTileMode.nativeInt);
    }

    private native int nativeCreate1(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int[] paramArrayOfInt, float[] paramArrayOfFloat, int paramInt);

    private native int nativeCreate2(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt1, int paramInt2, int paramInt3);

    private native int nativePostCreate1(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int[] paramArrayOfInt, float[] paramArrayOfFloat, int paramInt2);

    private native int nativePostCreate2(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2, int paramInt3, int paramInt4);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.LinearGradient
 * JD-Core Version:        0.6.2
 */