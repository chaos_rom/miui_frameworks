package android.graphics;

public class ImageFormat
{
    public static final int BAYER_RGGB = 512;
    public static final int JPEG = 256;
    public static final int NV16 = 16;
    public static final int NV21 = 17;
    public static final int RGB_565 = 4;
    public static final int UNKNOWN = 0;
    public static final int YUY2 = 20;
    public static final int YV12 = 842094169;

    public static int getBitsPerPixel(int paramInt)
    {
        int i = 16;
        switch (paramInt)
        {
        default:
            i = -1;
        case 4:
        case 16:
        case 20:
        case 512:
        case 842094169:
        case 17:
        }
        while (true)
        {
            return i;
            i = 12;
            continue;
            i = 12;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.ImageFormat
 * JD-Core Version:        0.6.2
 */