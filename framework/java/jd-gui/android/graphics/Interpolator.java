package android.graphics;

import android.os.SystemClock;

public class Interpolator
{
    private int mFrameCount;
    private int mValueCount;
    private final int native_instance;

    public Interpolator(int paramInt)
    {
        this.mValueCount = paramInt;
        this.mFrameCount = 2;
        this.native_instance = nativeConstructor(paramInt, 2);
    }

    public Interpolator(int paramInt1, int paramInt2)
    {
        this.mValueCount = paramInt1;
        this.mFrameCount = paramInt2;
        this.native_instance = nativeConstructor(paramInt1, paramInt2);
    }

    private static native int nativeConstructor(int paramInt1, int paramInt2);

    private static native void nativeDestructor(int paramInt);

    private static native void nativeReset(int paramInt1, int paramInt2, int paramInt3);

    private static native void nativeSetKeyFrame(int paramInt1, int paramInt2, int paramInt3, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2);

    private static native void nativeSetRepeatMirror(int paramInt, float paramFloat, boolean paramBoolean);

    private static native int nativeTimeToValues(int paramInt1, int paramInt2, float[] paramArrayOfFloat);

    protected void finalize()
        throws Throwable
    {
        nativeDestructor(this.native_instance);
    }

    public final int getKeyFrameCount()
    {
        return this.mFrameCount;
    }

    public final int getValueCount()
    {
        return this.mValueCount;
    }

    public void reset(int paramInt)
    {
        reset(paramInt, 2);
    }

    public void reset(int paramInt1, int paramInt2)
    {
        this.mValueCount = paramInt1;
        this.mFrameCount = paramInt2;
        nativeReset(this.native_instance, paramInt1, paramInt2);
    }

    public void setKeyFrame(int paramInt1, int paramInt2, float[] paramArrayOfFloat)
    {
        setKeyFrame(paramInt1, paramInt2, paramArrayOfFloat, null);
    }

    public void setKeyFrame(int paramInt1, int paramInt2, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        if ((paramInt1 < 0) || (paramInt1 >= this.mFrameCount))
            throw new IndexOutOfBoundsException();
        if (paramArrayOfFloat1.length < this.mValueCount)
            throw new ArrayStoreException();
        if ((paramArrayOfFloat2 != null) && (paramArrayOfFloat2.length < 4))
            throw new ArrayStoreException();
        nativeSetKeyFrame(this.native_instance, paramInt1, paramInt2, paramArrayOfFloat1, paramArrayOfFloat2);
    }

    public void setRepeatMirror(float paramFloat, boolean paramBoolean)
    {
        if (paramFloat >= 0.0F)
            nativeSetRepeatMirror(this.native_instance, paramFloat, paramBoolean);
    }

    public Result timeToValues(int paramInt, float[] paramArrayOfFloat)
    {
        if ((paramArrayOfFloat != null) && (paramArrayOfFloat.length < this.mValueCount))
            throw new ArrayStoreException();
        Result localResult;
        switch (nativeTimeToValues(this.native_instance, paramInt, paramArrayOfFloat))
        {
        default:
            localResult = Result.FREEZE_END;
        case 0:
        case 1:
        }
        while (true)
        {
            return localResult;
            localResult = Result.NORMAL;
            continue;
            localResult = Result.FREEZE_START;
        }
    }

    public Result timeToValues(float[] paramArrayOfFloat)
    {
        return timeToValues((int)SystemClock.uptimeMillis(), paramArrayOfFloat);
    }

    public static enum Result
    {
        static
        {
            FREEZE_START = new Result("FREEZE_START", 1);
            FREEZE_END = new Result("FREEZE_END", 2);
            Result[] arrayOfResult = new Result[3];
            arrayOfResult[0] = NORMAL;
            arrayOfResult[1] = FREEZE_START;
            arrayOfResult[2] = FREEZE_END;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Interpolator
 * JD-Core Version:        0.6.2
 */