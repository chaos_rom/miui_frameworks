package android.graphics;

@Deprecated
public class PixelXorXfermode extends Xfermode
{
    public PixelXorXfermode(int paramInt)
    {
        this.native_instance = nativeCreate(paramInt);
    }

    private static native int nativeCreate(int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PixelXorXfermode
 * JD-Core Version:        0.6.2
 */