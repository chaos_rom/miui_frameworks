package android.graphics;

import java.io.PrintWriter;

public class Matrix
{
    public static Matrix IDENTITY_MATRIX = new Matrix()
    {
        void oops()
        {
            throw new IllegalStateException("Matrix can not be modified");
        }

        public boolean postConcat(Matrix paramAnonymousMatrix)
        {
            oops();
            return false;
        }

        public boolean postRotate(float paramAnonymousFloat)
        {
            oops();
            return false;
        }

        public boolean postRotate(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3)
        {
            oops();
            return false;
        }

        public boolean postScale(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
            return false;
        }

        public boolean postScale(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3, float paramAnonymousFloat4)
        {
            oops();
            return false;
        }

        public boolean postSkew(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
            return false;
        }

        public boolean postSkew(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3, float paramAnonymousFloat4)
        {
            oops();
            return false;
        }

        public boolean postTranslate(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
            return false;
        }

        public boolean preConcat(Matrix paramAnonymousMatrix)
        {
            oops();
            return false;
        }

        public boolean preRotate(float paramAnonymousFloat)
        {
            oops();
            return false;
        }

        public boolean preRotate(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3)
        {
            oops();
            return false;
        }

        public boolean preScale(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
            return false;
        }

        public boolean preScale(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3, float paramAnonymousFloat4)
        {
            oops();
            return false;
        }

        public boolean preSkew(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
            return false;
        }

        public boolean preSkew(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3, float paramAnonymousFloat4)
        {
            oops();
            return false;
        }

        public boolean preTranslate(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
            return false;
        }

        public void reset()
        {
            oops();
        }

        public void set(Matrix paramAnonymousMatrix)
        {
            oops();
        }

        public boolean setConcat(Matrix paramAnonymousMatrix1, Matrix paramAnonymousMatrix2)
        {
            oops();
            return false;
        }

        public boolean setPolyToPoly(float[] paramAnonymousArrayOfFloat1, int paramAnonymousInt1, float[] paramAnonymousArrayOfFloat2, int paramAnonymousInt2, int paramAnonymousInt3)
        {
            oops();
            return false;
        }

        public boolean setRectToRect(RectF paramAnonymousRectF1, RectF paramAnonymousRectF2, Matrix.ScaleToFit paramAnonymousScaleToFit)
        {
            oops();
            return false;
        }

        public void setRotate(float paramAnonymousFloat)
        {
            oops();
        }

        public void setRotate(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3)
        {
            oops();
        }

        public void setScale(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
        }

        public void setScale(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3, float paramAnonymousFloat4)
        {
            oops();
        }

        public void setSinCos(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
        }

        public void setSinCos(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3, float paramAnonymousFloat4)
        {
            oops();
        }

        public void setSkew(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
        }

        public void setSkew(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3, float paramAnonymousFloat4)
        {
            oops();
        }

        public void setTranslate(float paramAnonymousFloat1, float paramAnonymousFloat2)
        {
            oops();
        }

        public void setValues(float[] paramAnonymousArrayOfFloat)
        {
            oops();
        }
    };
    public static final int MPERSP_0 = 6;
    public static final int MPERSP_1 = 7;
    public static final int MPERSP_2 = 8;
    public static final int MSCALE_X = 0;
    public static final int MSCALE_Y = 4;
    public static final int MSKEW_X = 1;
    public static final int MSKEW_Y = 3;
    public static final int MTRANS_X = 2;
    public static final int MTRANS_Y = 5;
    public int native_instance;

    public Matrix()
    {
        this.native_instance = native_create(0);
    }

    public Matrix(Matrix paramMatrix)
    {
        if (paramMatrix != null);
        for (int i = paramMatrix.native_instance; ; i = 0)
        {
            this.native_instance = native_create(i);
            return;
        }
    }

    private static void checkPointArrays(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, int paramInt3)
    {
        int i = paramInt1 + (paramInt3 << 1);
        int j = paramInt2 + (paramInt3 << 1);
        if (((j | (i | (paramInt2 | (paramInt3 | paramInt1)))) < 0) || (i > paramArrayOfFloat1.length) || (j > paramArrayOfFloat2.length))
            throw new ArrayIndexOutOfBoundsException();
    }

    private static native void finalizer(int paramInt);

    private static native int native_create(int paramInt);

    private static native boolean native_equals(int paramInt1, int paramInt2);

    private static native void native_getValues(int paramInt, float[] paramArrayOfFloat);

    private static native boolean native_invert(int paramInt1, int paramInt2);

    private static native boolean native_isIdentity(int paramInt);

    private static native void native_mapPoints(int paramInt1, float[] paramArrayOfFloat1, int paramInt2, float[] paramArrayOfFloat2, int paramInt3, int paramInt4, boolean paramBoolean);

    private static native float native_mapRadius(int paramInt, float paramFloat);

    private static native boolean native_mapRect(int paramInt, RectF paramRectF1, RectF paramRectF2);

    private static native boolean native_postConcat(int paramInt1, int paramInt2);

    private static native boolean native_postRotate(int paramInt, float paramFloat);

    private static native boolean native_postRotate(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3);

    private static native boolean native_postScale(int paramInt, float paramFloat1, float paramFloat2);

    private static native boolean native_postScale(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native boolean native_postSkew(int paramInt, float paramFloat1, float paramFloat2);

    private static native boolean native_postSkew(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native boolean native_postTranslate(int paramInt, float paramFloat1, float paramFloat2);

    private static native boolean native_preConcat(int paramInt1, int paramInt2);

    private static native boolean native_preRotate(int paramInt, float paramFloat);

    private static native boolean native_preRotate(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3);

    private static native boolean native_preScale(int paramInt, float paramFloat1, float paramFloat2);

    private static native boolean native_preScale(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native boolean native_preSkew(int paramInt, float paramFloat1, float paramFloat2);

    private static native boolean native_preSkew(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native boolean native_preTranslate(int paramInt, float paramFloat1, float paramFloat2);

    private static native boolean native_rectStaysRect(int paramInt);

    private static native void native_reset(int paramInt);

    private static native void native_set(int paramInt1, int paramInt2);

    private static native boolean native_setConcat(int paramInt1, int paramInt2, int paramInt3);

    private static native boolean native_setPolyToPoly(int paramInt1, float[] paramArrayOfFloat1, int paramInt2, float[] paramArrayOfFloat2, int paramInt3, int paramInt4);

    private static native boolean native_setRectToRect(int paramInt1, RectF paramRectF1, RectF paramRectF2, int paramInt2);

    private static native void native_setRotate(int paramInt, float paramFloat);

    private static native void native_setRotate(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3);

    private static native void native_setScale(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_setScale(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native void native_setSinCos(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_setSinCos(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native void native_setSkew(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_setSkew(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4);

    private static native void native_setTranslate(int paramInt, float paramFloat1, float paramFloat2);

    private static native void native_setValues(int paramInt, float[] paramArrayOfFloat);

    public boolean equals(Object paramObject)
    {
        if ((paramObject != null) && ((paramObject instanceof Matrix)) && (native_equals(this.native_instance, ((Matrix)paramObject).native_instance)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void finalize()
        throws Throwable
    {
        finalizer(this.native_instance);
    }

    public void getValues(float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat.length < 9)
            throw new ArrayIndexOutOfBoundsException();
        native_getValues(this.native_instance, paramArrayOfFloat);
    }

    public boolean invert(Matrix paramMatrix)
    {
        return native_invert(this.native_instance, paramMatrix.native_instance);
    }

    public boolean isIdentity()
    {
        return native_isIdentity(this.native_instance);
    }

    public void mapPoints(float[] paramArrayOfFloat)
    {
        mapPoints(paramArrayOfFloat, 0, paramArrayOfFloat, 0, paramArrayOfFloat.length >> 1);
    }

    public void mapPoints(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, int paramInt3)
    {
        checkPointArrays(paramArrayOfFloat2, paramInt2, paramArrayOfFloat1, paramInt1, paramInt3);
        native_mapPoints(this.native_instance, paramArrayOfFloat1, paramInt1, paramArrayOfFloat2, paramInt2, paramInt3, true);
    }

    public void mapPoints(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        if (paramArrayOfFloat1.length != paramArrayOfFloat2.length)
            throw new ArrayIndexOutOfBoundsException();
        mapPoints(paramArrayOfFloat1, 0, paramArrayOfFloat2, 0, paramArrayOfFloat1.length >> 1);
    }

    public float mapRadius(float paramFloat)
    {
        return native_mapRadius(this.native_instance, paramFloat);
    }

    public boolean mapRect(RectF paramRectF)
    {
        return mapRect(paramRectF, paramRectF);
    }

    public boolean mapRect(RectF paramRectF1, RectF paramRectF2)
    {
        if ((paramRectF1 == null) || (paramRectF2 == null))
            throw new NullPointerException();
        return native_mapRect(this.native_instance, paramRectF1, paramRectF2);
    }

    public void mapVectors(float[] paramArrayOfFloat)
    {
        mapVectors(paramArrayOfFloat, 0, paramArrayOfFloat, 0, paramArrayOfFloat.length >> 1);
    }

    public void mapVectors(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, int paramInt3)
    {
        checkPointArrays(paramArrayOfFloat2, paramInt2, paramArrayOfFloat1, paramInt1, paramInt3);
        native_mapPoints(this.native_instance, paramArrayOfFloat1, paramInt1, paramArrayOfFloat2, paramInt2, paramInt3, false);
    }

    public void mapVectors(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        if (paramArrayOfFloat1.length != paramArrayOfFloat2.length)
            throw new ArrayIndexOutOfBoundsException();
        mapVectors(paramArrayOfFloat1, 0, paramArrayOfFloat2, 0, paramArrayOfFloat1.length >> 1);
    }

    final int ni()
    {
        return this.native_instance;
    }

    public boolean postConcat(Matrix paramMatrix)
    {
        return native_postConcat(this.native_instance, paramMatrix.native_instance);
    }

    public boolean postRotate(float paramFloat)
    {
        return native_postRotate(this.native_instance, paramFloat);
    }

    public boolean postRotate(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        return native_postRotate(this.native_instance, paramFloat1, paramFloat2, paramFloat3);
    }

    public boolean postScale(float paramFloat1, float paramFloat2)
    {
        return native_postScale(this.native_instance, paramFloat1, paramFloat2);
    }

    public boolean postScale(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        return native_postScale(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public boolean postSkew(float paramFloat1, float paramFloat2)
    {
        return native_postSkew(this.native_instance, paramFloat1, paramFloat2);
    }

    public boolean postSkew(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        return native_postSkew(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public boolean postTranslate(float paramFloat1, float paramFloat2)
    {
        return native_postTranslate(this.native_instance, paramFloat1, paramFloat2);
    }

    public boolean preConcat(Matrix paramMatrix)
    {
        return native_preConcat(this.native_instance, paramMatrix.native_instance);
    }

    public boolean preRotate(float paramFloat)
    {
        return native_preRotate(this.native_instance, paramFloat);
    }

    public boolean preRotate(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        return native_preRotate(this.native_instance, paramFloat1, paramFloat2, paramFloat3);
    }

    public boolean preScale(float paramFloat1, float paramFloat2)
    {
        return native_preScale(this.native_instance, paramFloat1, paramFloat2);
    }

    public boolean preScale(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        return native_preScale(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public boolean preSkew(float paramFloat1, float paramFloat2)
    {
        return native_preSkew(this.native_instance, paramFloat1, paramFloat2);
    }

    public boolean preSkew(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        return native_preSkew(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public boolean preTranslate(float paramFloat1, float paramFloat2)
    {
        return native_preTranslate(this.native_instance, paramFloat1, paramFloat2);
    }

    public void printShortString(PrintWriter paramPrintWriter)
    {
        float[] arrayOfFloat = new float[9];
        getValues(arrayOfFloat);
        paramPrintWriter.print('[');
        paramPrintWriter.print(arrayOfFloat[0]);
        paramPrintWriter.print(", ");
        paramPrintWriter.print(arrayOfFloat[1]);
        paramPrintWriter.print(", ");
        paramPrintWriter.print(arrayOfFloat[2]);
        paramPrintWriter.print("][");
        paramPrintWriter.print(arrayOfFloat[3]);
        paramPrintWriter.print(", ");
        paramPrintWriter.print(arrayOfFloat[4]);
        paramPrintWriter.print(", ");
        paramPrintWriter.print(arrayOfFloat[5]);
        paramPrintWriter.print("][");
        paramPrintWriter.print(arrayOfFloat[6]);
        paramPrintWriter.print(", ");
        paramPrintWriter.print(arrayOfFloat[7]);
        paramPrintWriter.print(", ");
        paramPrintWriter.print(arrayOfFloat[8]);
        paramPrintWriter.print(']');
    }

    public boolean rectStaysRect()
    {
        return native_rectStaysRect(this.native_instance);
    }

    public void reset()
    {
        native_reset(this.native_instance);
    }

    public void set(Matrix paramMatrix)
    {
        if (paramMatrix == null)
            reset();
        while (true)
        {
            return;
            native_set(this.native_instance, paramMatrix.native_instance);
        }
    }

    public boolean setConcat(Matrix paramMatrix1, Matrix paramMatrix2)
    {
        return native_setConcat(this.native_instance, paramMatrix1.native_instance, paramMatrix2.native_instance);
    }

    public boolean setPolyToPoly(float[] paramArrayOfFloat1, int paramInt1, float[] paramArrayOfFloat2, int paramInt2, int paramInt3)
    {
        if (paramInt3 > 4)
            throw new IllegalArgumentException();
        checkPointArrays(paramArrayOfFloat1, paramInt1, paramArrayOfFloat2, paramInt2, paramInt3);
        return native_setPolyToPoly(this.native_instance, paramArrayOfFloat1, paramInt1, paramArrayOfFloat2, paramInt2, paramInt3);
    }

    public boolean setRectToRect(RectF paramRectF1, RectF paramRectF2, ScaleToFit paramScaleToFit)
    {
        if ((paramRectF2 == null) || (paramRectF1 == null))
            throw new NullPointerException();
        return native_setRectToRect(this.native_instance, paramRectF1, paramRectF2, paramScaleToFit.nativeInt);
    }

    public void setRotate(float paramFloat)
    {
        native_setRotate(this.native_instance, paramFloat);
    }

    public void setRotate(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        native_setRotate(this.native_instance, paramFloat1, paramFloat2, paramFloat3);
    }

    public void setScale(float paramFloat1, float paramFloat2)
    {
        native_setScale(this.native_instance, paramFloat1, paramFloat2);
    }

    public void setScale(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        native_setScale(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public void setSinCos(float paramFloat1, float paramFloat2)
    {
        native_setSinCos(this.native_instance, paramFloat1, paramFloat2);
    }

    public void setSinCos(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        native_setSinCos(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public void setSkew(float paramFloat1, float paramFloat2)
    {
        native_setSkew(this.native_instance, paramFloat1, paramFloat2);
    }

    public void setSkew(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        native_setSkew(this.native_instance, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    }

    public void setTranslate(float paramFloat1, float paramFloat2)
    {
        native_setTranslate(this.native_instance, paramFloat1, paramFloat2);
    }

    public void setValues(float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat.length < 9)
            throw new ArrayIndexOutOfBoundsException();
        native_setValues(this.native_instance, paramArrayOfFloat);
    }

    public String toShortString()
    {
        StringBuilder localStringBuilder = new StringBuilder(64);
        toShortString(localStringBuilder);
        return localStringBuilder.toString();
    }

    public void toShortString(StringBuilder paramStringBuilder)
    {
        float[] arrayOfFloat = new float[9];
        getValues(arrayOfFloat);
        paramStringBuilder.append('[');
        paramStringBuilder.append(arrayOfFloat[0]);
        paramStringBuilder.append(", ");
        paramStringBuilder.append(arrayOfFloat[1]);
        paramStringBuilder.append(", ");
        paramStringBuilder.append(arrayOfFloat[2]);
        paramStringBuilder.append("][");
        paramStringBuilder.append(arrayOfFloat[3]);
        paramStringBuilder.append(", ");
        paramStringBuilder.append(arrayOfFloat[4]);
        paramStringBuilder.append(", ");
        paramStringBuilder.append(arrayOfFloat[5]);
        paramStringBuilder.append("][");
        paramStringBuilder.append(arrayOfFloat[6]);
        paramStringBuilder.append(", ");
        paramStringBuilder.append(arrayOfFloat[7]);
        paramStringBuilder.append(", ");
        paramStringBuilder.append(arrayOfFloat[8]);
        paramStringBuilder.append(']');
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(64);
        localStringBuilder.append("Matrix{");
        toShortString(localStringBuilder);
        localStringBuilder.append('}');
        return localStringBuilder.toString();
    }

    public static enum ScaleToFit
    {
        final int nativeInt;

        static
        {
            CENTER = new ScaleToFit("CENTER", 2, 2);
            END = new ScaleToFit("END", 3, 3);
            ScaleToFit[] arrayOfScaleToFit = new ScaleToFit[4];
            arrayOfScaleToFit[0] = FILL;
            arrayOfScaleToFit[1] = START;
            arrayOfScaleToFit[2] = CENTER;
            arrayOfScaleToFit[3] = END;
        }

        private ScaleToFit(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Matrix
 * JD-Core Version:        0.6.2
 */