package android.graphics;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Point
    implements Parcelable
{
    public static final Parcelable.Creator<Point> CREATOR = new Parcelable.Creator()
    {
        public Point createFromParcel(Parcel paramAnonymousParcel)
        {
            Point localPoint = new Point();
            localPoint.readFromParcel(paramAnonymousParcel);
            return localPoint;
        }

        public Point[] newArray(int paramAnonymousInt)
        {
            return new Point[paramAnonymousInt];
        }
    };
    public int x;
    public int y;

    public Point()
    {
    }

    public Point(int paramInt1, int paramInt2)
    {
        this.x = paramInt1;
        this.y = paramInt2;
    }

    public Point(Point paramPoint)
    {
        this.x = paramPoint.x;
        this.y = paramPoint.y;
    }

    public int describeContents()
    {
        return 0;
    }

    public final boolean equals(int paramInt1, int paramInt2)
    {
        if ((this.x == paramInt1) && (this.y == paramInt2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if ((paramObject instanceof Point))
        {
            Point localPoint = (Point)paramObject;
            if ((this.x == localPoint.x) && (this.y == localPoint.y))
                bool = true;
        }
        return bool;
    }

    public int hashCode()
    {
        return 32713 * this.x + this.y;
    }

    public final void negate()
    {
        this.x = (-this.x);
        this.y = (-this.y);
    }

    public final void offset(int paramInt1, int paramInt2)
    {
        this.x = (paramInt1 + this.x);
        this.y = (paramInt2 + this.y);
    }

    public void readFromParcel(Parcel paramParcel)
    {
        this.x = paramParcel.readInt();
        this.y = paramParcel.readInt();
    }

    public void set(int paramInt1, int paramInt2)
    {
        this.x = paramInt1;
        this.y = paramInt2;
    }

    public String toString()
    {
        return "Point(" + this.x + ", " + this.y + ")";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.x);
        paramParcel.writeInt(this.y);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Point
 * JD-Core Version:        0.6.2
 */