package android.graphics;

public class NinePatch
{
    private final Bitmap mBitmap;
    private final byte[] mChunk;
    private Paint mPaint;
    private final RectF mRect = new RectF();
    private String mSrcName;

    public NinePatch(Bitmap paramBitmap, byte[] paramArrayOfByte, String paramString)
    {
        this.mBitmap = paramBitmap;
        this.mChunk = paramArrayOfByte;
        this.mSrcName = paramString;
        validateNinePatchChunk(this.mBitmap.ni(), paramArrayOfByte);
    }

    public NinePatch(NinePatch paramNinePatch)
    {
        this.mBitmap = paramNinePatch.mBitmap;
        this.mChunk = paramNinePatch.mChunk;
        this.mSrcName = paramNinePatch.mSrcName;
        if (paramNinePatch.mPaint != null)
            this.mPaint = new Paint(paramNinePatch.mPaint);
        validateNinePatchChunk(this.mBitmap.ni(), this.mChunk);
    }

    public static native boolean isNinePatchChunk(byte[] paramArrayOfByte);

    private static native void nativeDraw(int paramInt1, Rect paramRect, int paramInt2, byte[] paramArrayOfByte, int paramInt3, int paramInt4, int paramInt5);

    private static native void nativeDraw(int paramInt1, RectF paramRectF, int paramInt2, byte[] paramArrayOfByte, int paramInt3, int paramInt4, int paramInt5);

    private static native int nativeGetTransparentRegion(int paramInt, byte[] paramArrayOfByte, Rect paramRect);

    private static native void validateNinePatchChunk(int paramInt, byte[] paramArrayOfByte);

    public void draw(Canvas paramCanvas, Rect paramRect)
    {
        int k;
        if (!paramCanvas.isHardwareAccelerated())
        {
            int i = paramCanvas.mNativeCanvas;
            int j = this.mBitmap.ni();
            byte[] arrayOfByte = this.mChunk;
            if (this.mPaint != null)
            {
                k = this.mPaint.mNativePaint;
                nativeDraw(i, paramRect, j, arrayOfByte, k, paramCanvas.mDensity, this.mBitmap.mDensity);
            }
        }
        while (true)
        {
            return;
            k = 0;
            break;
            this.mRect.set(paramRect);
            paramCanvas.drawPatch(this.mBitmap, this.mChunk, this.mRect, this.mPaint);
        }
    }

    public void draw(Canvas paramCanvas, Rect paramRect, Paint paramPaint)
    {
        int k;
        if (!paramCanvas.isHardwareAccelerated())
        {
            int i = paramCanvas.mNativeCanvas;
            int j = this.mBitmap.ni();
            byte[] arrayOfByte = this.mChunk;
            if (paramPaint != null)
            {
                k = paramPaint.mNativePaint;
                nativeDraw(i, paramRect, j, arrayOfByte, k, paramCanvas.mDensity, this.mBitmap.mDensity);
            }
        }
        while (true)
        {
            return;
            k = 0;
            break;
            this.mRect.set(paramRect);
            paramCanvas.drawPatch(this.mBitmap, this.mChunk, this.mRect, paramPaint);
        }
    }

    public void draw(Canvas paramCanvas, RectF paramRectF)
    {
        int k;
        if (!paramCanvas.isHardwareAccelerated())
        {
            int i = paramCanvas.mNativeCanvas;
            int j = this.mBitmap.ni();
            byte[] arrayOfByte = this.mChunk;
            if (this.mPaint != null)
            {
                k = this.mPaint.mNativePaint;
                nativeDraw(i, paramRectF, j, arrayOfByte, k, paramCanvas.mDensity, this.mBitmap.mDensity);
            }
        }
        while (true)
        {
            return;
            k = 0;
            break;
            paramCanvas.drawPatch(this.mBitmap, this.mChunk, paramRectF, this.mPaint);
        }
    }

    public int getDensity()
    {
        return this.mBitmap.mDensity;
    }

    public int getHeight()
    {
        return this.mBitmap.getHeight();
    }

    public final Region getTransparentRegion(Rect paramRect)
    {
        int i = nativeGetTransparentRegion(this.mBitmap.ni(), this.mChunk, paramRect);
        if (i != 0);
        for (Region localRegion = new Region(i); ; localRegion = null)
            return localRegion;
    }

    public int getWidth()
    {
        return this.mBitmap.getWidth();
    }

    public final boolean hasAlpha()
    {
        return this.mBitmap.hasAlpha();
    }

    public void setPaint(Paint paramPaint)
    {
        this.mPaint = paramPaint;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.NinePatch
 * JD-Core Version:        0.6.2
 */