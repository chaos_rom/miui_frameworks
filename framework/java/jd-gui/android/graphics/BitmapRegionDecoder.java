package android.graphics;

import android.content.res.AssetManager.AssetInputStream;
import java.io.BufferedInputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;

public final class BitmapRegionDecoder
{
    private int mNativeBitmapRegionDecoder;
    private boolean mRecycled;

    private BitmapRegionDecoder(int paramInt)
    {
        this.mNativeBitmapRegionDecoder = paramInt;
        this.mRecycled = false;
    }

    private void checkRecycled(String paramString)
    {
        if (this.mRecycled)
            throw new IllegalStateException(paramString);
    }

    private static native void nativeClean(int paramInt);

    private static native Bitmap nativeDecodeRegion(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, BitmapFactory.Options paramOptions);

    private static native int nativeGetHeight(int paramInt);

    private static native int nativeGetWidth(int paramInt);

    private static native BitmapRegionDecoder nativeNewInstance(int paramInt, boolean paramBoolean);

    private static native BitmapRegionDecoder nativeNewInstance(FileDescriptor paramFileDescriptor, boolean paramBoolean);

    private static native BitmapRegionDecoder nativeNewInstance(InputStream paramInputStream, byte[] paramArrayOfByte, boolean paramBoolean);

    private static native BitmapRegionDecoder nativeNewInstance(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean);

    public static BitmapRegionDecoder newInstance(FileDescriptor paramFileDescriptor, boolean paramBoolean)
        throws IOException
    {
        return nativeNewInstance(paramFileDescriptor, paramBoolean);
    }

    public static BitmapRegionDecoder newInstance(InputStream paramInputStream, boolean paramBoolean)
        throws IOException
    {
        if (!paramInputStream.markSupported())
            paramInputStream = new BufferedInputStream(paramInputStream, 16384);
        if ((paramInputStream instanceof AssetManager.AssetInputStream));
        for (BitmapRegionDecoder localBitmapRegionDecoder = nativeNewInstance(((AssetManager.AssetInputStream)paramInputStream).getAssetInt(), paramBoolean); ; localBitmapRegionDecoder = nativeNewInstance(paramInputStream, new byte[16384], paramBoolean))
            return localBitmapRegionDecoder;
    }

    // ERROR //
    public static BitmapRegionDecoder newInstance(String paramString, boolean paramBoolean)
        throws IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 64	java/io/FileInputStream
        //     5: dup
        //     6: aload_0
        //     7: invokespecial 65	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     10: astore_3
        //     11: aload_3
        //     12: iload_1
        //     13: invokestatic 67	android/graphics/BitmapRegionDecoder:newInstance	(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
        //     16: astore 6
        //     18: aload_3
        //     19: ifnull +7 -> 26
        //     22: aload_3
        //     23: invokevirtual 70	java/io/FileInputStream:close	()V
        //     26: aload 6
        //     28: areturn
        //     29: astore 4
        //     31: aload_2
        //     32: ifnull +7 -> 39
        //     35: aload_2
        //     36: invokevirtual 70	java/io/FileInputStream:close	()V
        //     39: aload 4
        //     41: athrow
        //     42: astore 5
        //     44: goto -5 -> 39
        //     47: astore 7
        //     49: goto -23 -> 26
        //     52: astore 4
        //     54: aload_3
        //     55: astore_2
        //     56: goto -25 -> 31
        //
        // Exception table:
        //     from	to	target	type
        //     2	11	29	finally
        //     35	39	42	java/io/IOException
        //     22	26	47	java/io/IOException
        //     11	18	52	finally
    }

    public static BitmapRegionDecoder newInstance(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
        throws IOException
    {
        if (((paramInt1 | paramInt2) < 0) || (paramArrayOfByte.length < paramInt1 + paramInt2))
            throw new ArrayIndexOutOfBoundsException();
        return nativeNewInstance(paramArrayOfByte, paramInt1, paramInt2, paramBoolean);
    }

    public Bitmap decodeRegion(Rect paramRect, BitmapFactory.Options paramOptions)
    {
        checkRecycled("decodeRegion called on recycled region decoder");
        if ((paramRect.right <= 0) || (paramRect.bottom <= 0) || (paramRect.left >= getWidth()) || (paramRect.top >= getHeight()))
            throw new IllegalArgumentException("rectangle is outside the image");
        return nativeDecodeRegion(this.mNativeBitmapRegionDecoder, paramRect.left, paramRect.top, paramRect.right - paramRect.left, paramRect.bottom - paramRect.top, paramOptions);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            recycle();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getHeight()
    {
        checkRecycled("getHeight called on recycled region decoder");
        return nativeGetHeight(this.mNativeBitmapRegionDecoder);
    }

    public int getWidth()
    {
        checkRecycled("getWidth called on recycled region decoder");
        return nativeGetWidth(this.mNativeBitmapRegionDecoder);
    }

    public final boolean isRecycled()
    {
        return this.mRecycled;
    }

    public void recycle()
    {
        if (!this.mRecycled)
        {
            nativeClean(this.mNativeBitmapRegionDecoder);
            this.mRecycled = true;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.BitmapRegionDecoder
 * JD-Core Version:        0.6.2
 */