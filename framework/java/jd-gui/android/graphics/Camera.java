package android.graphics;

public class Camera
{
    int native_instance;

    public Camera()
    {
        nativeConstructor();
    }

    private native void nativeApplyToCanvas(int paramInt);

    private native void nativeConstructor();

    private native void nativeDestructor();

    private native void nativeGetMatrix(int paramInt);

    public void applyToCanvas(Canvas paramCanvas)
    {
        nativeApplyToCanvas(paramCanvas.mNativeCanvas);
    }

    public native float dotWithNormal(float paramFloat1, float paramFloat2, float paramFloat3);

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDestructor();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public native float getLocationX();

    public native float getLocationY();

    public native float getLocationZ();

    public void getMatrix(Matrix paramMatrix)
    {
        nativeGetMatrix(paramMatrix.native_instance);
    }

    public native void restore();

    public native void rotate(float paramFloat1, float paramFloat2, float paramFloat3);

    public native void rotateX(float paramFloat);

    public native void rotateY(float paramFloat);

    public native void rotateZ(float paramFloat);

    public native void save();

    public native void setLocation(float paramFloat1, float paramFloat2, float paramFloat3);

    public native void translate(float paramFloat1, float paramFloat2, float paramFloat3);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Camera
 * JD-Core Version:        0.6.2
 */