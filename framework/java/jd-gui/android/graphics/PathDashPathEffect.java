package android.graphics;

public class PathDashPathEffect extends PathEffect
{
    public PathDashPathEffect(Path paramPath, float paramFloat1, float paramFloat2, Style paramStyle)
    {
        this.native_instance = nativeCreate(paramPath.ni(), paramFloat1, paramFloat2, paramStyle.native_style);
    }

    private static native int nativeCreate(int paramInt1, float paramFloat1, float paramFloat2, int paramInt2);

    public static enum Style
    {
        int native_style;

        static
        {
            ROTATE = new Style("ROTATE", 1, 1);
            MORPH = new Style("MORPH", 2, 2);
            Style[] arrayOfStyle = new Style[3];
            arrayOfStyle[0] = TRANSLATE;
            arrayOfStyle[1] = ROTATE;
            arrayOfStyle[2] = MORPH;
        }

        private Style(int paramInt)
        {
            this.native_style = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PathDashPathEffect
 * JD-Core Version:        0.6.2
 */