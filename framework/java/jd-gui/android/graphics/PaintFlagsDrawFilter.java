package android.graphics;

public class PaintFlagsDrawFilter extends DrawFilter
{
    public final int clearBits;
    public final int setBits;

    public PaintFlagsDrawFilter(int paramInt1, int paramInt2)
    {
        this.clearBits = paramInt1;
        this.setBits = paramInt2;
        this.mNativeInt = nativeConstructor(paramInt1, paramInt2);
    }

    private static native int nativeConstructor(int paramInt1, int paramInt2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PaintFlagsDrawFilter
 * JD-Core Version:        0.6.2
 */