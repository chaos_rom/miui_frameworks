package android.graphics;

public class LayerRasterizer extends Rasterizer
{
    public LayerRasterizer()
    {
        this.native_instance = nativeConstructor();
    }

    private static native void nativeAddLayer(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2);

    private static native int nativeConstructor();

    public void addLayer(Paint paramPaint)
    {
        nativeAddLayer(this.native_instance, paramPaint.mNativePaint, 0.0F, 0.0F);
    }

    public void addLayer(Paint paramPaint, float paramFloat1, float paramFloat2)
    {
        nativeAddLayer(this.native_instance, paramPaint.mNativePaint, paramFloat1, paramFloat2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.LayerRasterizer
 * JD-Core Version:        0.6.2
 */