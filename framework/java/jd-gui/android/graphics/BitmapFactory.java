package android.graphics;

import android.content.res.AssetManager.AssetInputStream;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import java.io.BufferedInputStream;
import java.io.FileDescriptor;
import java.io.InputStream;

public class BitmapFactory
{
    private static final int DECODE_BUFFER_SIZE = 16384;

    public static Bitmap decodeByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        return decodeByteArray(paramArrayOfByte, paramInt1, paramInt2, null);
    }

    public static Bitmap decodeByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2, Options paramOptions)
    {
        if (((paramInt1 | paramInt2) < 0) || (paramArrayOfByte.length < paramInt1 + paramInt2))
            throw new ArrayIndexOutOfBoundsException();
        Bitmap localBitmap = nativeDecodeByteArray(paramArrayOfByte, paramInt1, paramInt2, paramOptions);
        if ((localBitmap == null) && (paramOptions != null) && (paramOptions.inBitmap != null))
            throw new IllegalArgumentException("Problem decoding into existing bitmap");
        return localBitmap;
    }

    public static Bitmap decodeFile(String paramString)
    {
        return decodeFile(paramString, null);
    }

    // ERROR //
    public static Bitmap decodeFile(String paramString, Options paramOptions)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aconst_null
        //     3: astore_3
        //     4: new 47	java/io/FileInputStream
        //     7: dup
        //     8: aload_0
        //     9: invokespecial 48	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     12: astore 4
        //     14: aload 4
        //     16: aconst_null
        //     17: aload_1
        //     18: invokestatic 52	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     21: astore 9
        //     23: aload 9
        //     25: astore_2
        //     26: aload 4
        //     28: ifnull +67 -> 95
        //     31: aload 4
        //     33: invokevirtual 55	java/io/FileInputStream:close	()V
        //     36: aload_2
        //     37: areturn
        //     38: astore 10
        //     40: goto -4 -> 36
        //     43: astore 11
        //     45: aload_3
        //     46: ifnull -10 -> 36
        //     49: aload_3
        //     50: invokevirtual 55	java/io/FileInputStream:close	()V
        //     53: goto -17 -> 36
        //     56: astore 6
        //     58: goto -22 -> 36
        //     61: astore 7
        //     63: aload_3
        //     64: ifnull +7 -> 71
        //     67: aload_3
        //     68: invokevirtual 55	java/io/FileInputStream:close	()V
        //     71: aload 7
        //     73: athrow
        //     74: astore 8
        //     76: goto -5 -> 71
        //     79: astore 7
        //     81: aload 4
        //     83: astore_3
        //     84: goto -21 -> 63
        //     87: astore 5
        //     89: aload 4
        //     91: astore_3
        //     92: goto -47 -> 45
        //     95: goto -59 -> 36
        //
        // Exception table:
        //     from	to	target	type
        //     31	36	38	java/io/IOException
        //     4	14	43	java/lang/Exception
        //     49	53	56	java/io/IOException
        //     4	14	61	finally
        //     67	71	74	java/io/IOException
        //     14	23	79	finally
        //     14	23	87	java/lang/Exception
    }

    public static Bitmap decodeFileDescriptor(FileDescriptor paramFileDescriptor)
    {
        return decodeFileDescriptor(paramFileDescriptor, null, null);
    }

    // ERROR //
    public static Bitmap decodeFileDescriptor(FileDescriptor paramFileDescriptor, Rect paramRect, Options paramOptions)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokestatic 66	android/graphics/BitmapFactory:nativeIsSeekable	(Ljava/io/FileDescriptor;)Z
        //     4: ifeq +49 -> 53
        //     7: aload_0
        //     8: aload_1
        //     9: aload_2
        //     10: invokestatic 69	android/graphics/BitmapFactory:nativeDecodeFileDescriptor	(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     13: astore 9
        //     15: aload 9
        //     17: ifnonnull +24 -> 41
        //     20: aload_2
        //     21: ifnull +20 -> 41
        //     24: aload_2
        //     25: getfield 29	android/graphics/BitmapFactory$Options:inBitmap	Landroid/graphics/Bitmap;
        //     28: ifnull +13 -> 41
        //     31: new 31	java/lang/IllegalArgumentException
        //     34: dup
        //     35: ldc 33
        //     37: invokespecial 36	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     40: athrow
        //     41: aload 9
        //     43: aload_1
        //     44: aload_2
        //     45: invokestatic 73	android/graphics/BitmapFactory:finishDecode	(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     48: astore 7
        //     50: aload 7
        //     52: areturn
        //     53: new 47	java/io/FileInputStream
        //     56: dup
        //     57: aload_0
        //     58: invokespecial 76	java/io/FileInputStream:<init>	(Ljava/io/FileDescriptor;)V
        //     61: astore_3
        //     62: aload_3
        //     63: aload_1
        //     64: aload_2
        //     65: invokestatic 52	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     68: astore 6
        //     70: aload 6
        //     72: astore 7
        //     74: aload_3
        //     75: invokevirtual 55	java/io/FileInputStream:close	()V
        //     78: goto -28 -> 50
        //     81: astore 8
        //     83: goto -33 -> 50
        //     86: astore 4
        //     88: aload_3
        //     89: invokevirtual 55	java/io/FileInputStream:close	()V
        //     92: aload 4
        //     94: athrow
        //     95: astore 5
        //     97: goto -5 -> 92
        //
        // Exception table:
        //     from	to	target	type
        //     74	78	81	java/lang/Throwable
        //     62	70	86	finally
        //     88	92	95	java/lang/Throwable
    }

    public static Bitmap decodeResource(Resources paramResources, int paramInt)
    {
        return decodeResource(paramResources, paramInt, null);
    }

    // ERROR //
    public static Bitmap decodeResource(Resources paramResources, int paramInt, Options paramOptions)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aconst_null
        //     3: astore 4
        //     5: new 83	android/util/TypedValue
        //     8: dup
        //     9: invokespecial 84	android/util/TypedValue:<init>	()V
        //     12: astore 5
        //     14: aload_0
        //     15: iload_1
        //     16: aload 5
        //     18: invokevirtual 90	android/content/res/Resources:openRawResource	(ILandroid/util/TypedValue;)Ljava/io/InputStream;
        //     21: astore 4
        //     23: aload_0
        //     24: aload 5
        //     26: aload 4
        //     28: aconst_null
        //     29: aload_2
        //     30: invokestatic 94	android/graphics/BitmapFactory:decodeResourceStream	(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     33: astore 10
        //     35: aload 10
        //     37: astore_3
        //     38: aload 4
        //     40: ifnull +8 -> 48
        //     43: aload 4
        //     45: invokevirtual 97	java/io/InputStream:close	()V
        //     48: aload_3
        //     49: ifnonnull +69 -> 118
        //     52: aload_2
        //     53: ifnull +65 -> 118
        //     56: aload_2
        //     57: getfield 29	android/graphics/BitmapFactory$Options:inBitmap	Landroid/graphics/Bitmap;
        //     60: ifnull +58 -> 118
        //     63: new 31	java/lang/IllegalArgumentException
        //     66: dup
        //     67: ldc 33
        //     69: invokespecial 36	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     72: athrow
        //     73: astore 8
        //     75: aload 4
        //     77: ifnull -29 -> 48
        //     80: aload 4
        //     82: invokevirtual 97	java/io/InputStream:close	()V
        //     85: goto -37 -> 48
        //     88: astore 9
        //     90: goto -42 -> 48
        //     93: astore 6
        //     95: aload 4
        //     97: ifnull +8 -> 105
        //     100: aload 4
        //     102: invokevirtual 97	java/io/InputStream:close	()V
        //     105: aload 6
        //     107: athrow
        //     108: astore 11
        //     110: goto -62 -> 48
        //     113: astore 7
        //     115: goto -10 -> 105
        //     118: aload_3
        //     119: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     5	35	73	java/lang/Exception
        //     80	85	88	java/io/IOException
        //     5	35	93	finally
        //     43	48	108	java/io/IOException
        //     100	105	113	java/io/IOException
    }

    public static Bitmap decodeResourceStream(Resources paramResources, TypedValue paramTypedValue, InputStream paramInputStream, Rect paramRect, Options paramOptions)
    {
        if (paramOptions == null)
            paramOptions = new Options();
        int i;
        if ((paramOptions.inDensity == 0) && (paramTypedValue != null))
        {
            i = paramTypedValue.density;
            if (i != 0)
                break label77;
            paramOptions.inDensity = 160;
        }
        while (true)
        {
            if ((paramOptions.inTargetDensity == 0) && (paramResources != null))
                paramOptions.inTargetDensity = paramResources.getDisplayMetrics().densityDpi;
            return decodeStream(paramInputStream, paramRect, paramOptions);
            label77: if (i != 65535)
                paramOptions.inDensity = i;
        }
    }

    public static Bitmap decodeStream(InputStream paramInputStream)
    {
        return decodeStream(paramInputStream, null, null);
    }

    public static Bitmap decodeStream(InputStream paramInputStream, Rect paramRect, Options paramOptions)
    {
        Bitmap localBitmap;
        if (paramInputStream == null)
            localBitmap = null;
        while (true)
        {
            return localBitmap;
            if (!paramInputStream.markSupported())
                paramInputStream = new BufferedInputStream(paramInputStream, 16384);
            paramInputStream.mark(1024);
            int i = 1;
            int m;
            if ((paramInputStream instanceof AssetManager.AssetInputStream))
            {
                m = ((AssetManager.AssetInputStream)paramInputStream).getAssetInt();
                if ((paramOptions == null) || ((paramOptions.inScaled) && (paramOptions.inBitmap == null)))
                {
                    float f2 = 1.0F;
                    int n = 0;
                    if (paramOptions != null)
                    {
                        int i1 = paramOptions.inDensity;
                        n = paramOptions.inTargetDensity;
                        if ((i1 != 0) && (n != 0))
                            f2 = n / i1;
                    }
                    localBitmap = nativeDecodeAsset(m, paramRect, paramOptions, true, f2);
                    if ((localBitmap != null) && (n != 0))
                        localBitmap.setDensity(n);
                    i = 0;
                }
            }
            while ((localBitmap == null) && (paramOptions != null) && (paramOptions.inBitmap != null))
            {
                throw new IllegalArgumentException("Problem decoding into existing bitmap");
                localBitmap = nativeDecodeAsset(m, paramRect, paramOptions);
                continue;
                byte[] arrayOfByte = null;
                if (paramOptions != null)
                    arrayOfByte = paramOptions.inTempStorage;
                if (arrayOfByte == null)
                    arrayOfByte = new byte[16384];
                if ((paramOptions == null) || ((paramOptions.inScaled) && (paramOptions.inBitmap == null)))
                {
                    float f1 = 1.0F;
                    int j = 0;
                    if (paramOptions != null)
                    {
                        int k = paramOptions.inDensity;
                        j = paramOptions.inTargetDensity;
                        if ((k != 0) && (j != 0))
                            f1 = j / k;
                    }
                    localBitmap = nativeDecodeStream(paramInputStream, arrayOfByte, paramRect, paramOptions, true, f1);
                    if ((localBitmap != null) && (j != 0))
                        localBitmap.setDensity(j);
                    i = 0;
                }
                else
                {
                    localBitmap = nativeDecodeStream(paramInputStream, arrayOfByte, paramRect, paramOptions);
                }
            }
            if (i != 0)
                localBitmap = finishDecode(localBitmap, paramRect, paramOptions);
        }
    }

    private static Bitmap finishDecode(Bitmap paramBitmap, Rect paramRect, Options paramOptions)
    {
        if ((paramBitmap == null) || (paramOptions == null));
        while (true)
        {
            return paramBitmap;
            int i = paramOptions.inDensity;
            if (i != 0)
            {
                paramBitmap.setDensity(i);
                int j = paramOptions.inTargetDensity;
                if ((j != 0) && (i != j) && (i != paramOptions.inScreenDensity))
                {
                    byte[] arrayOfByte = paramBitmap.getNinePatchChunk();
                    int[] arrayOfInt1 = paramBitmap.getLayoutBounds();
                    if ((arrayOfByte != null) && (NinePatch.isNinePatchChunk(arrayOfByte)));
                    int[] arrayOfInt2;
                    for (int k = 1; (paramOptions.inScaled) || (k != 0); k = 0)
                    {
                        float f = j / i;
                        if (f == 1.0F)
                            break label225;
                        Bitmap localBitmap = paramBitmap;
                        paramBitmap = Bitmap.createScaledBitmap(localBitmap, (int)(0.5F + f * paramBitmap.getWidth()), (int)(0.5F + f * paramBitmap.getHeight()), true);
                        if (paramBitmap != localBitmap)
                            localBitmap.recycle();
                        if (k != 0)
                            paramBitmap.setNinePatchChunk(nativeScaleNinePatch(arrayOfByte, f, paramRect));
                        if (arrayOfInt1 == null)
                            break label225;
                        arrayOfInt2 = new int[arrayOfInt1.length];
                        for (int m = 0; m < arrayOfInt1.length; m++)
                            arrayOfInt2[m] = ((int)(0.5F + f * arrayOfInt1[m]));
                    }
                    continue;
                    paramBitmap.setLayoutBounds(arrayOfInt2);
                    label225: paramBitmap.setDensity(j);
                }
            }
        }
    }

    private static native Bitmap nativeDecodeAsset(int paramInt, Rect paramRect, Options paramOptions);

    private static native Bitmap nativeDecodeAsset(int paramInt, Rect paramRect, Options paramOptions, boolean paramBoolean, float paramFloat);

    private static native Bitmap nativeDecodeByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2, Options paramOptions);

    private static native Bitmap nativeDecodeFileDescriptor(FileDescriptor paramFileDescriptor, Rect paramRect, Options paramOptions);

    private static native Bitmap nativeDecodeStream(InputStream paramInputStream, byte[] paramArrayOfByte, Rect paramRect, Options paramOptions);

    private static native Bitmap nativeDecodeStream(InputStream paramInputStream, byte[] paramArrayOfByte, Rect paramRect, Options paramOptions, boolean paramBoolean, float paramFloat);

    private static native boolean nativeIsSeekable(FileDescriptor paramFileDescriptor);

    private static native byte[] nativeScaleNinePatch(byte[] paramArrayOfByte, float paramFloat, Rect paramRect);

    public static class Options
    {
        public Bitmap inBitmap;
        public int inDensity;
        public boolean inDither = false;
        public boolean inInputShareable;
        public boolean inJustDecodeBounds;
        public boolean inMutable;
        public boolean inPreferQualityOverSpeed;
        public Bitmap.Config inPreferredConfig = Bitmap.Config.ARGB_8888;
        public boolean inPurgeable;
        public int inSampleSize;
        public boolean inScaled = true;
        public int inScreenDensity;
        public int inTargetDensity;
        public byte[] inTempStorage;
        public boolean mCancel;
        public int outHeight;
        public String outMimeType;
        public int outWidth;

        private native void requestCancel();

        public void requestCancelDecode()
        {
            this.mCancel = true;
            requestCancel();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.BitmapFactory
 * JD-Core Version:        0.6.2
 */