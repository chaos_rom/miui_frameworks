package android.graphics;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.FloatMath;
import com.android.internal.util.FastMath;
import java.io.PrintWriter;

public class RectF
    implements Parcelable
{
    public static final Parcelable.Creator<RectF> CREATOR = new Parcelable.Creator()
    {
        public RectF createFromParcel(Parcel paramAnonymousParcel)
        {
            RectF localRectF = new RectF();
            localRectF.readFromParcel(paramAnonymousParcel);
            return localRectF;
        }

        public RectF[] newArray(int paramAnonymousInt)
        {
            return new RectF[paramAnonymousInt];
        }
    };
    public float bottom;
    public float left;
    public float right;
    public float top;

    public RectF()
    {
    }

    public RectF(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.left = paramFloat1;
        this.top = paramFloat2;
        this.right = paramFloat3;
        this.bottom = paramFloat4;
    }

    public RectF(Rect paramRect)
    {
        this.left = paramRect.left;
        this.top = paramRect.top;
        this.right = paramRect.right;
        this.bottom = paramRect.bottom;
    }

    public RectF(RectF paramRectF)
    {
        this.left = paramRectF.left;
        this.top = paramRectF.top;
        this.right = paramRectF.right;
        this.bottom = paramRectF.bottom;
    }

    public static boolean intersects(RectF paramRectF1, RectF paramRectF2)
    {
        if ((paramRectF1.left < paramRectF2.right) && (paramRectF2.left < paramRectF1.right) && (paramRectF1.top < paramRectF2.bottom) && (paramRectF2.top < paramRectF1.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final float centerX()
    {
        return 0.5F * (this.left + this.right);
    }

    public final float centerY()
    {
        return 0.5F * (this.top + this.bottom);
    }

    public boolean contains(float paramFloat1, float paramFloat2)
    {
        if ((this.left < this.right) && (this.top < this.bottom) && (paramFloat1 >= this.left) && (paramFloat1 < this.right) && (paramFloat2 >= this.top) && (paramFloat2 < this.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean contains(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        if ((this.left < this.right) && (this.top < this.bottom) && (this.left <= paramFloat1) && (this.top <= paramFloat2) && (this.right >= paramFloat3) && (this.bottom >= paramFloat4));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean contains(RectF paramRectF)
    {
        if ((this.left < this.right) && (this.top < this.bottom) && (this.left <= paramRectF.left) && (this.top <= paramRectF.top) && (this.right >= paramRectF.right) && (this.bottom >= paramRectF.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (getClass() != paramObject.getClass()))
            {
                bool = false;
            }
            else
            {
                RectF localRectF = (RectF)paramObject;
                if ((this.left != localRectF.left) || (this.top != localRectF.top) || (this.right != localRectF.right) || (this.bottom != localRectF.bottom))
                    bool = false;
            }
        }
    }

    public int hashCode()
    {
        int i = 0;
        int j;
        int m;
        label42: int n;
        if (this.left != 0.0F)
        {
            j = Float.floatToIntBits(this.left);
            int k = j * 31;
            if (this.top == 0.0F)
                break label106;
            m = Float.floatToIntBits(this.top);
            n = 31 * (k + m);
            if (this.right == 0.0F)
                break label112;
        }
        label106: label112: for (int i1 = Float.floatToIntBits(this.right); ; i1 = 0)
        {
            int i2 = 31 * (n + i1);
            if (this.bottom != 0.0F)
                i = Float.floatToIntBits(this.bottom);
            return i2 + i;
            j = 0;
            break;
            m = 0;
            break label42;
        }
    }

    public final float height()
    {
        return this.bottom - this.top;
    }

    public void inset(float paramFloat1, float paramFloat2)
    {
        this.left = (paramFloat1 + this.left);
        this.top = (paramFloat2 + this.top);
        this.right -= paramFloat1;
        this.bottom -= paramFloat2;
    }

    public boolean intersect(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        if ((this.left < paramFloat3) && (paramFloat1 < this.right) && (this.top < paramFloat4) && (paramFloat2 < this.bottom))
        {
            if (this.left < paramFloat1)
                this.left = paramFloat1;
            if (this.top < paramFloat2)
                this.top = paramFloat2;
            if (this.right > paramFloat3)
                this.right = paramFloat3;
            if (this.bottom > paramFloat4)
                this.bottom = paramFloat4;
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean intersect(RectF paramRectF)
    {
        return intersect(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom);
    }

    public boolean intersects(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        if ((this.left < paramFloat3) && (paramFloat1 < this.right) && (this.top < paramFloat4) && (paramFloat2 < this.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isEmpty()
    {
        if ((this.left >= this.right) || (this.top >= this.bottom));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void offset(float paramFloat1, float paramFloat2)
    {
        this.left = (paramFloat1 + this.left);
        this.top = (paramFloat2 + this.top);
        this.right = (paramFloat1 + this.right);
        this.bottom = (paramFloat2 + this.bottom);
    }

    public void offsetTo(float paramFloat1, float paramFloat2)
    {
        this.right += paramFloat1 - this.left;
        this.bottom += paramFloat2 - this.top;
        this.left = paramFloat1;
        this.top = paramFloat2;
    }

    public void printShortString(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print('[');
        paramPrintWriter.print(this.left);
        paramPrintWriter.print(',');
        paramPrintWriter.print(this.top);
        paramPrintWriter.print("][");
        paramPrintWriter.print(this.right);
        paramPrintWriter.print(',');
        paramPrintWriter.print(this.bottom);
        paramPrintWriter.print(']');
    }

    public void readFromParcel(Parcel paramParcel)
    {
        this.left = paramParcel.readFloat();
        this.top = paramParcel.readFloat();
        this.right = paramParcel.readFloat();
        this.bottom = paramParcel.readFloat();
    }

    public void round(Rect paramRect)
    {
        paramRect.set(FastMath.round(this.left), FastMath.round(this.top), FastMath.round(this.right), FastMath.round(this.bottom));
    }

    public void roundOut(Rect paramRect)
    {
        paramRect.set((int)FloatMath.floor(this.left), (int)FloatMath.floor(this.top), (int)FloatMath.ceil(this.right), (int)FloatMath.ceil(this.bottom));
    }

    public void set(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        this.left = paramFloat1;
        this.top = paramFloat2;
        this.right = paramFloat3;
        this.bottom = paramFloat4;
    }

    public void set(Rect paramRect)
    {
        this.left = paramRect.left;
        this.top = paramRect.top;
        this.right = paramRect.right;
        this.bottom = paramRect.bottom;
    }

    public void set(RectF paramRectF)
    {
        this.left = paramRectF.left;
        this.top = paramRectF.top;
        this.right = paramRectF.right;
        this.bottom = paramRectF.bottom;
    }

    public void setEmpty()
    {
        this.bottom = 0.0F;
        this.top = 0.0F;
        this.right = 0.0F;
        this.left = 0.0F;
    }

    public boolean setIntersect(RectF paramRectF1, RectF paramRectF2)
    {
        if ((paramRectF1.left < paramRectF2.right) && (paramRectF2.left < paramRectF1.right) && (paramRectF1.top < paramRectF2.bottom) && (paramRectF2.top < paramRectF1.bottom))
        {
            this.left = Math.max(paramRectF1.left, paramRectF2.left);
            this.top = Math.max(paramRectF1.top, paramRectF2.top);
            this.right = Math.min(paramRectF1.right, paramRectF2.right);
            this.bottom = Math.min(paramRectF1.bottom, paramRectF2.bottom);
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void sort()
    {
        if (this.left > this.right)
        {
            float f2 = this.left;
            this.left = this.right;
            this.right = f2;
        }
        if (this.top > this.bottom)
        {
            float f1 = this.top;
            this.top = this.bottom;
            this.bottom = f1;
        }
    }

    public String toShortString()
    {
        return toShortString(new StringBuilder(32));
    }

    public String toShortString(StringBuilder paramStringBuilder)
    {
        paramStringBuilder.setLength(0);
        paramStringBuilder.append('[');
        paramStringBuilder.append(this.left);
        paramStringBuilder.append(',');
        paramStringBuilder.append(this.top);
        paramStringBuilder.append("][");
        paramStringBuilder.append(this.right);
        paramStringBuilder.append(',');
        paramStringBuilder.append(this.bottom);
        paramStringBuilder.append(']');
        return paramStringBuilder.toString();
    }

    public String toString()
    {
        return "RectF(" + this.left + ", " + this.top + ", " + this.right + ", " + this.bottom + ")";
    }

    public void union(float paramFloat1, float paramFloat2)
    {
        if (paramFloat1 < this.left)
            this.left = paramFloat1;
        label61: 
        while (true)
        {
            if (paramFloat2 < this.top)
                this.top = paramFloat2;
            while (true)
            {
                return;
                if (paramFloat1 <= this.right)
                    break label61;
                this.right = paramFloat1;
                break;
                if (paramFloat2 > this.bottom)
                    this.bottom = paramFloat2;
            }
        }
    }

    public void union(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        if ((paramFloat1 < paramFloat3) && (paramFloat2 < paramFloat4))
        {
            if ((this.left >= this.right) || (this.top >= this.bottom))
                break label96;
            if (this.left > paramFloat1)
                this.left = paramFloat1;
            if (this.top > paramFloat2)
                this.top = paramFloat2;
            if (this.right < paramFloat3)
                this.right = paramFloat3;
            if (this.bottom >= paramFloat4);
        }
        for (this.bottom = paramFloat4; ; this.bottom = paramFloat4)
        {
            return;
            label96: this.left = paramFloat1;
            this.top = paramFloat2;
            this.right = paramFloat3;
        }
    }

    public void union(RectF paramRectF)
    {
        union(paramRectF.left, paramRectF.top, paramRectF.right, paramRectF.bottom);
    }

    public final float width()
    {
        return this.right - this.left;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeFloat(this.left);
        paramParcel.writeFloat(this.top);
        paramParcel.writeFloat(this.right);
        paramParcel.writeFloat(this.bottom);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.RectF
 * JD-Core Version:        0.6.2
 */