package android.graphics;

import android.content.res.AssetManager;
import android.util.SparseArray;
import java.io.File;

public class Typeface
{
    public static final int BOLD = 1;
    public static final int BOLD_ITALIC = 3;
    public static final Typeface DEFAULT;
    public static final Typeface DEFAULT_BOLD;
    public static final int ITALIC = 2;
    public static final Typeface MONOSPACE;
    public static final int NORMAL;
    public static final Typeface SANS_SERIF;
    public static final Typeface SERIF;
    static Typeface[] sDefaults = arrayOfTypeface;
    private static final SparseArray<SparseArray<Typeface>> sTypefaceCache = new SparseArray(3);
    private int mStyle = 0;
    int native_instance;

    static
    {
        DEFAULT = create((String)null, 0);
        DEFAULT_BOLD = create((String)null, 1);
        SANS_SERIF = create("sans-serif", 0);
        SERIF = create("serif", 0);
        MONOSPACE = create("monospace", 0);
        Typeface[] arrayOfTypeface = new Typeface[4];
        arrayOfTypeface[0] = DEFAULT;
        arrayOfTypeface[1] = DEFAULT_BOLD;
        arrayOfTypeface[2] = create((String)null, 2);
        arrayOfTypeface[3] = create((String)null, 3);
    }

    private Typeface(int paramInt)
    {
        if (paramInt == 0)
            throw new RuntimeException("native typeface cannot be made");
        this.native_instance = paramInt;
        this.mStyle = nativeGetStyle(paramInt);
    }

    public static Typeface create(Typeface paramTypeface, int paramInt)
    {
        int i = 0;
        Typeface localTypeface;
        if (paramTypeface != null)
            if (paramTypeface.mStyle == paramInt)
                localTypeface = paramTypeface;
        while (true)
        {
            return localTypeface;
            i = paramTypeface.native_instance;
            SparseArray localSparseArray = (SparseArray)sTypefaceCache.get(i);
            if (localSparseArray != null)
            {
                localTypeface = (Typeface)localSparseArray.get(paramInt);
                if (localTypeface != null);
            }
            else
            {
                localTypeface = new Typeface(nativeCreateFromTypeface(i, paramInt));
                if (localSparseArray == null)
                {
                    localSparseArray = new SparseArray(4);
                    sTypefaceCache.put(i, localSparseArray);
                }
                localSparseArray.put(paramInt, localTypeface);
            }
        }
    }

    public static Typeface create(String paramString, int paramInt)
    {
        return new Typeface(nativeCreate(paramString, paramInt));
    }

    public static Typeface createFromAsset(AssetManager paramAssetManager, String paramString)
    {
        return new Typeface(nativeCreateFromAsset(paramAssetManager, paramString));
    }

    public static Typeface createFromFile(File paramFile)
    {
        return new Typeface(nativeCreateFromFile(paramFile.getAbsolutePath()));
    }

    public static Typeface createFromFile(String paramString)
    {
        return new Typeface(nativeCreateFromFile(paramString));
    }

    public static Typeface defaultFromStyle(int paramInt)
    {
        return sDefaults[paramInt];
    }

    private static native int nativeCreate(String paramString, int paramInt);

    private static native int nativeCreateFromAsset(AssetManager paramAssetManager, String paramString);

    private static native int nativeCreateFromFile(String paramString);

    private static native int nativeCreateFromTypeface(int paramInt1, int paramInt2);

    private static native int nativeGetStyle(int paramInt);

    private static native void nativeUnref(int paramInt);

    public static native void setGammaForText(float paramFloat1, float paramFloat2);

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (getClass() != paramObject.getClass()))
            {
                bool = false;
            }
            else
            {
                Typeface localTypeface = (Typeface)paramObject;
                if ((this.mStyle != localTypeface.mStyle) || (this.native_instance != localTypeface.native_instance))
                    bool = false;
            }
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeUnref(this.native_instance);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getStyle()
    {
        return this.mStyle;
    }

    public int hashCode()
    {
        return 31 * this.native_instance + this.mStyle;
    }

    public final boolean isBold()
    {
        if ((0x1 & this.mStyle) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final boolean isItalic()
    {
        if ((0x2 & this.mStyle) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Typeface
 * JD-Core Version:        0.6.2
 */