package android.graphics;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.FloatMath;

public class PointF
    implements Parcelable
{
    public static final Parcelable.Creator<PointF> CREATOR = new Parcelable.Creator()
    {
        public PointF createFromParcel(Parcel paramAnonymousParcel)
        {
            PointF localPointF = new PointF();
            localPointF.readFromParcel(paramAnonymousParcel);
            return localPointF;
        }

        public PointF[] newArray(int paramAnonymousInt)
        {
            return new PointF[paramAnonymousInt];
        }
    };
    public float x;
    public float y;

    public PointF()
    {
    }

    public PointF(float paramFloat1, float paramFloat2)
    {
        this.x = paramFloat1;
        this.y = paramFloat2;
    }

    public PointF(Point paramPoint)
    {
        this.x = paramPoint.x;
        this.y = paramPoint.y;
    }

    public static float length(float paramFloat1, float paramFloat2)
    {
        return FloatMath.sqrt(paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2);
    }

    public int describeContents()
    {
        return 0;
    }

    public final boolean equals(float paramFloat1, float paramFloat2)
    {
        if ((this.x == paramFloat1) && (this.y == paramFloat2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final float length()
    {
        return length(this.x, this.y);
    }

    public final void negate()
    {
        this.x = (-this.x);
        this.y = (-this.y);
    }

    public final void offset(float paramFloat1, float paramFloat2)
    {
        this.x = (paramFloat1 + this.x);
        this.y = (paramFloat2 + this.y);
    }

    public void readFromParcel(Parcel paramParcel)
    {
        this.x = paramParcel.readFloat();
        this.y = paramParcel.readFloat();
    }

    public final void set(float paramFloat1, float paramFloat2)
    {
        this.x = paramFloat1;
        this.y = paramFloat2;
    }

    public final void set(PointF paramPointF)
    {
        this.x = paramPointF.x;
        this.y = paramPointF.y;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeFloat(this.x);
        paramParcel.writeFloat(this.y);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PointF
 * JD-Core Version:        0.6.2
 */