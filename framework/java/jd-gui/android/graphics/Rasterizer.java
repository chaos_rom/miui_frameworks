package android.graphics;

public class Rasterizer
{
    int native_instance;

    private static native void finalizer(int paramInt);

    protected void finalize()
        throws Throwable
    {
        finalizer(this.native_instance);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Rasterizer
 * JD-Core Version:        0.6.2
 */