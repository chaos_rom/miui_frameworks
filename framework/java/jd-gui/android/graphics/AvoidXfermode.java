package android.graphics;

@Deprecated
public class AvoidXfermode extends Xfermode
{
    public AvoidXfermode(int paramInt1, int paramInt2, Mode paramMode)
    {
        if ((paramInt2 < 0) || (paramInt2 > 255))
            throw new IllegalArgumentException("tolerance must be 0..255");
        this.native_instance = nativeCreate(paramInt1, paramInt2, paramMode.nativeInt);
    }

    private static native int nativeCreate(int paramInt1, int paramInt2, int paramInt3);

    public static enum Mode
    {
        final int nativeInt;

        static
        {
            Mode[] arrayOfMode = new Mode[2];
            arrayOfMode[0] = AVOID;
            arrayOfMode[1] = TARGET;
        }

        private Mode(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.AvoidXfermode
 * JD-Core Version:        0.6.2
 */