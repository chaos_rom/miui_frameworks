package android.graphics;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Region
    implements Parcelable
{
    public static final Parcelable.Creator<Region> CREATOR = new Parcelable.Creator()
    {
        public Region createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = Region.nativeCreateFromParcel(paramAnonymousParcel);
            if (i == 0)
                throw new RuntimeException();
            return new Region(i);
        }

        public Region[] newArray(int paramAnonymousInt)
        {
            return new Region[paramAnonymousInt];
        }
    };
    public final int mNativeRegion;

    public Region()
    {
        this(nativeConstructor());
    }

    Region(int paramInt)
    {
        if (paramInt == 0)
            throw new RuntimeException();
        this.mNativeRegion = paramInt;
    }

    private Region(int paramInt1, int paramInt2)
    {
        this(paramInt1);
    }

    public Region(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mNativeRegion = nativeConstructor();
        nativeSetRect(this.mNativeRegion, paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public Region(Rect paramRect)
    {
        this.mNativeRegion = nativeConstructor();
        nativeSetRect(this.mNativeRegion, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    public Region(Region paramRegion)
    {
        this(nativeConstructor());
        nativeSetRegion(this.mNativeRegion, paramRegion.mNativeRegion);
    }

    private static native int nativeConstructor();

    private static native int nativeCreateFromParcel(Parcel paramParcel);

    private static native void nativeDestructor(int paramInt);

    private static native boolean nativeEquals(int paramInt1, int paramInt2);

    private static native boolean nativeGetBoundaryPath(int paramInt1, int paramInt2);

    private static native boolean nativeGetBounds(int paramInt, Rect paramRect);

    private static native boolean nativeOp(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native boolean nativeOp(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

    private static native boolean nativeOp(int paramInt1, Rect paramRect, int paramInt2, int paramInt3);

    private static native boolean nativeSetPath(int paramInt1, int paramInt2, int paramInt3);

    private static native boolean nativeSetRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private static native boolean nativeSetRegion(int paramInt1, int paramInt2);

    private static native String nativeToString(int paramInt);

    private static native boolean nativeWriteToParcel(int paramInt, Parcel paramParcel);

    public native boolean contains(int paramInt1, int paramInt2);

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        if ((paramObject == null) || (!(paramObject instanceof Region)));
        Region localRegion;
        for (boolean bool = false; ; bool = nativeEquals(this.mNativeRegion, localRegion.mNativeRegion))
        {
            return bool;
            localRegion = (Region)paramObject;
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDestructor(this.mNativeRegion);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public Path getBoundaryPath()
    {
        Path localPath = new Path();
        nativeGetBoundaryPath(this.mNativeRegion, localPath.ni());
        return localPath;
    }

    public boolean getBoundaryPath(Path paramPath)
    {
        return nativeGetBoundaryPath(this.mNativeRegion, paramPath.ni());
    }

    public Rect getBounds()
    {
        Rect localRect = new Rect();
        nativeGetBounds(this.mNativeRegion, localRect);
        return localRect;
    }

    public boolean getBounds(Rect paramRect)
    {
        if (paramRect == null)
            throw new NullPointerException();
        return nativeGetBounds(this.mNativeRegion, paramRect);
    }

    public native boolean isComplex();

    public native boolean isEmpty();

    public native boolean isRect();

    final int ni()
    {
        return this.mNativeRegion;
    }

    public boolean op(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Op paramOp)
    {
        return nativeOp(this.mNativeRegion, paramInt1, paramInt2, paramInt3, paramInt4, paramOp.nativeInt);
    }

    public boolean op(Rect paramRect, Op paramOp)
    {
        return nativeOp(this.mNativeRegion, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom, paramOp.nativeInt);
    }

    public boolean op(Rect paramRect, Region paramRegion, Op paramOp)
    {
        return nativeOp(this.mNativeRegion, paramRect, paramRegion.mNativeRegion, paramOp.nativeInt);
    }

    public boolean op(Region paramRegion, Op paramOp)
    {
        return op(this, paramRegion, paramOp);
    }

    public boolean op(Region paramRegion1, Region paramRegion2, Op paramOp)
    {
        return nativeOp(this.mNativeRegion, paramRegion1.mNativeRegion, paramRegion2.mNativeRegion, paramOp.nativeInt);
    }

    public native boolean quickContains(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    public boolean quickContains(Rect paramRect)
    {
        return quickContains(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    public native boolean quickReject(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    public boolean quickReject(Rect paramRect)
    {
        return quickReject(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    public native boolean quickReject(Region paramRegion);

    public void scale(float paramFloat)
    {
        scale(paramFloat, null);
    }

    public native void scale(float paramFloat, Region paramRegion);

    public boolean set(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        return nativeSetRect(this.mNativeRegion, paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public boolean set(Rect paramRect)
    {
        return nativeSetRect(this.mNativeRegion, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    public boolean set(Region paramRegion)
    {
        return nativeSetRegion(this.mNativeRegion, paramRegion.mNativeRegion);
    }

    public void setEmpty()
    {
        nativeSetRect(this.mNativeRegion, 0, 0, 0, 0);
    }

    public boolean setPath(Path paramPath, Region paramRegion)
    {
        return nativeSetPath(this.mNativeRegion, paramPath.ni(), paramRegion.mNativeRegion);
    }

    public String toString()
    {
        return nativeToString(this.mNativeRegion);
    }

    public void translate(int paramInt1, int paramInt2)
    {
        translate(paramInt1, paramInt2, null);
    }

    public native void translate(int paramInt1, int paramInt2, Region paramRegion);

    public final boolean union(Rect paramRect)
    {
        return op(paramRect, Op.UNION);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (!nativeWriteToParcel(this.mNativeRegion, paramParcel))
            throw new RuntimeException();
    }

    public static enum Op
    {
        public final int nativeInt;

        static
        {
            REVERSE_DIFFERENCE = new Op("REVERSE_DIFFERENCE", 4, 4);
            REPLACE = new Op("REPLACE", 5, 5);
            Op[] arrayOfOp = new Op[6];
            arrayOfOp[0] = DIFFERENCE;
            arrayOfOp[1] = INTERSECT;
            arrayOfOp[2] = UNION;
            arrayOfOp[3] = XOR;
            arrayOfOp[4] = REVERSE_DIFFERENCE;
            arrayOfOp[5] = REPLACE;
        }

        private Op(int paramInt)
        {
            this.nativeInt = paramInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.Region
 * JD-Core Version:        0.6.2
 */