package android.graphics;

public class ColorFilter
{
    public int nativeColorFilter;
    int native_instance;

    private static native void finalizer(int paramInt1, int paramInt2);

    protected void finalize()
        throws Throwable
    {
        try
        {
            super.finalize();
            return;
        }
        finally
        {
            finalizer(this.native_instance, this.nativeColorFilter);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.ColorFilter
 * JD-Core Version:        0.6.2
 */