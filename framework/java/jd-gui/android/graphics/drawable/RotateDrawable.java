package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class RotateDrawable extends Drawable
    implements Drawable.Callback
{
    private static final float MAX_LEVEL = 10000.0F;
    private boolean mMutated;
    private RotateState mState;

    public RotateDrawable()
    {
        this(null, null);
    }

    private RotateDrawable(RotateState paramRotateState, Resources paramResources)
    {
        this.mState = new RotateState(paramRotateState, this, paramResources);
    }

    public void draw(Canvas paramCanvas)
    {
        int i = paramCanvas.save();
        Rect localRect = this.mState.mDrawable.getBounds();
        int j = localRect.right - localRect.left;
        int k = localRect.bottom - localRect.top;
        RotateState localRotateState = this.mState;
        float f1;
        if (localRotateState.mPivotXRel)
        {
            f1 = j * localRotateState.mPivotX;
            if (!localRotateState.mPivotYRel)
                break label132;
        }
        label132: for (float f2 = k * localRotateState.mPivotY; ; f2 = localRotateState.mPivotY)
        {
            paramCanvas.rotate(localRotateState.mCurrentDegrees, f1 + localRect.left, f2 + localRect.top);
            localRotateState.mDrawable.draw(paramCanvas);
            paramCanvas.restoreToCount(i);
            return;
            f1 = localRotateState.mPivotX;
            break;
        }
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mState.mChangingConfigurations | this.mState.mDrawable.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState()
    {
        if (this.mState.canConstantState())
            this.mState.mChangingConfigurations = getChangingConfigurations();
        for (RotateState localRotateState = this.mState; ; localRotateState = null)
            return localRotateState;
    }

    public Drawable getDrawable()
    {
        return this.mState.mDrawable;
    }

    public int getIntrinsicHeight()
    {
        return this.mState.mDrawable.getIntrinsicHeight();
    }

    public int getIntrinsicWidth()
    {
        return this.mState.mDrawable.getIntrinsicWidth();
    }

    public int getOpacity()
    {
        return this.mState.mDrawable.getOpacity();
    }

    public boolean getPadding(Rect paramRect)
    {
        return this.mState.mDrawable.getPadding(paramRect);
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.RotateDrawable);
        super.inflateWithAttributes(paramResources, paramXmlPullParser, localTypedArray, 0);
        TypedValue localTypedValue1 = localTypedArray.peekValue(4);
        boolean bool1;
        float f1;
        TypedValue localTypedValue2;
        boolean bool2;
        float f3;
        float f4;
        Drawable localDrawable;
        if (localTypedValue1 == null)
        {
            bool1 = true;
            f1 = 0.5F;
            localTypedValue2 = localTypedArray.peekValue(5);
            if (localTypedValue2 == null)
            {
                bool2 = true;
                f2 = 0.5F;
                f3 = localTypedArray.getFloat(2, 0.0F);
                f4 = localTypedArray.getFloat(3, 360.0F);
                int i = localTypedArray.getResourceId(1, 0);
                localDrawable = null;
                if (i > 0)
                    localDrawable = paramResources.getDrawable(i);
                localTypedArray.recycle();
                int j = paramXmlPullParser.getDepth();
                while (true)
                {
                    int k = paramXmlPullParser.next();
                    if ((k == 1) || ((k == 3) && (paramXmlPullParser.getDepth() <= j)))
                        break;
                    if (k == 2)
                    {
                        localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet);
                        if (localDrawable == null)
                            Log.w("drawable", "Bad element under <rotate>: " + paramXmlPullParser.getName());
                    }
                }
            }
        }
        else
        {
            if (localTypedValue1.type == 6)
            {
                bool1 = true;
                label212: if (!bool1)
                    break label235;
            }
            label235: for (f1 = localTypedValue1.getFraction(1.0F, 1.0F); ; f1 = localTypedValue1.getFloat())
            {
                break;
                bool1 = false;
                break label212;
            }
        }
        if (localTypedValue2.type == 6)
        {
            bool2 = true;
            label258: if (!bool2)
                break label281;
        }
        label281: for (float f2 = localTypedValue2.getFraction(1.0F, 1.0F); ; f2 = localTypedValue2.getFloat())
        {
            break;
            bool2 = false;
            break label258;
        }
        if (localDrawable == null)
            Log.w("drawable", "No drawable specified for <rotate>");
        this.mState.mDrawable = localDrawable;
        this.mState.mPivotXRel = bool1;
        this.mState.mPivotX = f1;
        this.mState.mPivotYRel = bool2;
        this.mState.mPivotY = f2;
        RotateState localRotateState = this.mState;
        this.mState.mCurrentDegrees = f3;
        localRotateState.mFromDegrees = f3;
        this.mState.mToDegrees = f4;
        if (localDrawable != null)
            localDrawable.setCallback(this);
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.invalidateDrawable(this);
    }

    public boolean isStateful()
    {
        return this.mState.mDrawable.isStateful();
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            this.mState.mDrawable.mutate();
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        this.mState.mDrawable.setBounds(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    protected boolean onLevelChange(int paramInt)
    {
        this.mState.mDrawable.setLevel(paramInt);
        onBoundsChange(getBounds());
        this.mState.mCurrentDegrees = (this.mState.mFromDegrees + (this.mState.mToDegrees - this.mState.mFromDegrees) * (paramInt / 10000.0F));
        invalidateSelf();
        return true;
    }

    protected boolean onStateChange(int[] paramArrayOfInt)
    {
        boolean bool = this.mState.mDrawable.setState(paramArrayOfInt);
        onBoundsChange(getBounds());
        return bool;
    }

    public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.scheduleDrawable(this, paramRunnable, paramLong);
    }

    public void setAlpha(int paramInt)
    {
        this.mState.mDrawable.setAlpha(paramInt);
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        this.mState.mDrawable.setColorFilter(paramColorFilter);
    }

    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mState.mDrawable.setVisible(paramBoolean1, paramBoolean2);
        return super.setVisible(paramBoolean1, paramBoolean2);
    }

    public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.unscheduleDrawable(this, paramRunnable);
    }

    static final class RotateState extends Drawable.ConstantState
    {
        private boolean mCanConstantState;
        int mChangingConfigurations;
        private boolean mCheckedConstantState;
        float mCurrentDegrees;
        Drawable mDrawable;
        float mFromDegrees;
        float mPivotX;
        boolean mPivotXRel;
        float mPivotY;
        boolean mPivotYRel;
        float mToDegrees;

        public RotateState(RotateState paramRotateState, RotateDrawable paramRotateDrawable, Resources paramResources)
        {
            if (paramRotateState != null)
                if (paramResources == null)
                    break label104;
            label104: for (this.mDrawable = paramRotateState.mDrawable.getConstantState().newDrawable(paramResources); ; this.mDrawable = paramRotateState.mDrawable.getConstantState().newDrawable())
            {
                this.mDrawable.setCallback(paramRotateDrawable);
                this.mPivotXRel = paramRotateState.mPivotXRel;
                this.mPivotX = paramRotateState.mPivotX;
                this.mPivotYRel = paramRotateState.mPivotYRel;
                this.mPivotY = paramRotateState.mPivotY;
                float f = paramRotateState.mFromDegrees;
                this.mCurrentDegrees = f;
                this.mFromDegrees = f;
                this.mToDegrees = paramRotateState.mToDegrees;
                this.mCheckedConstantState = true;
                this.mCanConstantState = true;
                return;
            }
        }

        public boolean canConstantState()
        {
            if (!this.mCheckedConstantState)
                if (this.mDrawable.getConstantState() == null)
                    break label34;
            label34: for (boolean bool = true; ; bool = false)
            {
                this.mCanConstantState = bool;
                this.mCheckedConstantState = true;
                return this.mCanConstantState;
            }
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new RotateDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new RotateDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.RotateDrawable
 * JD-Core Version:        0.6.2
 */