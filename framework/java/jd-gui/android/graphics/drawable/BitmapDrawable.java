package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import com.android.internal.R.styleable;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class BitmapDrawable extends Drawable
{
    private static final int DEFAULT_PAINT_FLAGS = 6;
    private boolean mApplyGravity;
    private Bitmap mBitmap;
    private int mBitmapHeight;
    private BitmapState mBitmapState;
    private int mBitmapWidth;
    private final Rect mDstRect = new Rect();
    private boolean mMutated;
    private int mTargetDensity;

    @Deprecated
    public BitmapDrawable()
    {
        this.mBitmapState = new BitmapState((Bitmap)null);
    }

    public BitmapDrawable(Resources paramResources)
    {
        this.mBitmapState = new BitmapState((Bitmap)null);
        this.mBitmapState.mTargetDensity = this.mTargetDensity;
    }

    public BitmapDrawable(Resources paramResources, Bitmap paramBitmap)
    {
        this(new BitmapState(paramBitmap), paramResources);
        this.mBitmapState.mTargetDensity = this.mTargetDensity;
    }

    public BitmapDrawable(Resources paramResources, InputStream paramInputStream)
    {
        this(new BitmapState(BitmapFactory.decodeStream(paramInputStream)), null);
        this.mBitmapState.mTargetDensity = this.mTargetDensity;
        if (this.mBitmap == null)
            Log.w("BitmapDrawable", "BitmapDrawable cannot decode " + paramInputStream);
    }

    public BitmapDrawable(Resources paramResources, String paramString)
    {
        this(new BitmapState(BitmapFactory.decodeFile(paramString)), null);
        this.mBitmapState.mTargetDensity = this.mTargetDensity;
        if (this.mBitmap == null)
            Log.w("BitmapDrawable", "BitmapDrawable cannot decode " + paramString);
    }

    @Deprecated
    public BitmapDrawable(Bitmap paramBitmap)
    {
        this(new BitmapState(paramBitmap), null);
    }

    private BitmapDrawable(BitmapState paramBitmapState, Resources paramResources)
    {
        this.mBitmapState = paramBitmapState;
        if (paramResources != null)
        {
            this.mTargetDensity = paramResources.getDisplayMetrics().densityDpi;
            if (paramBitmapState == null)
                break label61;
        }
        label61: for (Bitmap localBitmap = paramBitmapState.mBitmap; ; localBitmap = null)
        {
            setBitmap(localBitmap);
            return;
            this.mTargetDensity = paramBitmapState.mTargetDensity;
            break;
        }
    }

    @Deprecated
    public BitmapDrawable(InputStream paramInputStream)
    {
        this(new BitmapState(BitmapFactory.decodeStream(paramInputStream)), null);
        if (this.mBitmap == null)
            Log.w("BitmapDrawable", "BitmapDrawable cannot decode " + paramInputStream);
    }

    @Deprecated
    public BitmapDrawable(String paramString)
    {
        this(new BitmapState(BitmapFactory.decodeFile(paramString)), null);
        if (this.mBitmap == null)
            Log.w("BitmapDrawable", "BitmapDrawable cannot decode " + paramString);
    }

    private void computeBitmapSize()
    {
        this.mBitmapWidth = this.mBitmap.getScaledWidth(this.mTargetDensity);
        this.mBitmapHeight = this.mBitmap.getScaledHeight(this.mTargetDensity);
    }

    private void setBitmap(Bitmap paramBitmap)
    {
        if (paramBitmap != this.mBitmap)
        {
            this.mBitmap = paramBitmap;
            if (paramBitmap == null)
                break label26;
            computeBitmapSize();
        }
        while (true)
        {
            invalidateSelf();
            return;
            label26: this.mBitmapHeight = -1;
            this.mBitmapWidth = -1;
        }
    }

    public void draw(Canvas paramCanvas)
    {
        Bitmap localBitmap = this.mBitmap;
        BitmapState localBitmapState;
        Shader.TileMode localTileMode1;
        Shader.TileMode localTileMode2;
        if (localBitmap != null)
        {
            localBitmapState = this.mBitmapState;
            if (localBitmapState.mRebuildShader)
            {
                localTileMode1 = localBitmapState.mTileModeX;
                localTileMode2 = localBitmapState.mTileModeY;
                if ((localTileMode1 != null) || (localTileMode2 != null))
                    break label133;
                localBitmapState.mPaint.setShader(null);
                localBitmapState.mRebuildShader = false;
                copyBounds(this.mDstRect);
            }
            if (localBitmapState.mPaint.getShader() != null)
                break label180;
            if (this.mApplyGravity)
            {
                int i = getResolvedLayoutDirectionSelf();
                Gravity.apply(localBitmapState.mGravity, this.mBitmapWidth, this.mBitmapHeight, getBounds(), this.mDstRect, i);
                this.mApplyGravity = false;
            }
            paramCanvas.drawBitmap(localBitmap, null, this.mDstRect, localBitmapState.mPaint);
        }
        while (true)
        {
            return;
            label133: Paint localPaint = localBitmapState.mPaint;
            if (localTileMode1 == null)
                localTileMode1 = Shader.TileMode.CLAMP;
            if (localTileMode2 == null)
                localTileMode2 = Shader.TileMode.CLAMP;
            localPaint.setShader(new BitmapShader(localBitmap, localTileMode1, localTileMode2));
            break;
            label180: if (this.mApplyGravity)
            {
                copyBounds(this.mDstRect);
                this.mApplyGravity = false;
            }
            paramCanvas.drawRect(this.mDstRect, localBitmapState.mPaint);
        }
    }

    public final Bitmap getBitmap()
    {
        return this.mBitmap;
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mBitmapState.mChangingConfigurations;
    }

    public final Drawable.ConstantState getConstantState()
    {
        this.mBitmapState.mChangingConfigurations = getChangingConfigurations();
        return this.mBitmapState;
    }

    public int getGravity()
    {
        return this.mBitmapState.mGravity;
    }

    public int getIntrinsicHeight()
    {
        return this.mBitmapHeight;
    }

    public int getIntrinsicWidth()
    {
        return this.mBitmapWidth;
    }

    public int getOpacity()
    {
        int i = -3;
        if (this.mBitmapState.mGravity != 119);
        while (true)
        {
            return i;
            Bitmap localBitmap = this.mBitmap;
            if ((localBitmap != null) && (!localBitmap.hasAlpha()) && (this.mBitmapState.mPaint.getAlpha() >= 255))
                i = -1;
        }
    }

    public final Paint getPaint()
    {
        return this.mBitmapState.mPaint;
    }

    public Shader.TileMode getTileModeX()
    {
        return this.mBitmapState.mTileModeX;
    }

    public Shader.TileMode getTileModeY()
    {
        return this.mBitmapState.mTileModeY;
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        super.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.BitmapDrawable);
        int i = localTypedArray.getResourceId(1, 0);
        if (i == 0)
            throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <bitmap> requires a valid src attribute");
        Bitmap localBitmap = BitmapFactory.decodeResource(paramResources, i);
        if (localBitmap == null)
            throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <bitmap> requires a valid src attribute");
        this.mBitmapState.mBitmap = localBitmap;
        setBitmap(localBitmap);
        setTargetDensity(paramResources.getDisplayMetrics());
        Paint localPaint = this.mBitmapState.mPaint;
        localPaint.setAntiAlias(localTypedArray.getBoolean(2, localPaint.isAntiAlias()));
        localPaint.setFilterBitmap(localTypedArray.getBoolean(3, localPaint.isFilterBitmap()));
        localPaint.setDither(localTypedArray.getBoolean(4, localPaint.isDither()));
        setGravity(localTypedArray.getInt(0, 119));
        int j = localTypedArray.getInt(5, -1);
        if (j != -1)
            switch (j)
            {
            default:
            case 0:
            case 1:
            case 2:
            }
        while (true)
        {
            localTypedArray.recycle();
            return;
            setTileModeXY(Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            continue;
            setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            continue;
            setTileModeXY(Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
        }
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            this.mBitmapState = new BitmapState(this.mBitmapState);
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        super.onBoundsChange(paramRect);
        this.mApplyGravity = true;
    }

    public void setAlpha(int paramInt)
    {
        if (paramInt != this.mBitmapState.mPaint.getAlpha())
        {
            this.mBitmapState.mPaint.setAlpha(paramInt);
            invalidateSelf();
        }
    }

    public void setAntiAlias(boolean paramBoolean)
    {
        this.mBitmapState.mPaint.setAntiAlias(paramBoolean);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        this.mBitmapState.mPaint.setColorFilter(paramColorFilter);
        invalidateSelf();
    }

    public void setDither(boolean paramBoolean)
    {
        this.mBitmapState.mPaint.setDither(paramBoolean);
        invalidateSelf();
    }

    public void setFilterBitmap(boolean paramBoolean)
    {
        this.mBitmapState.mPaint.setFilterBitmap(paramBoolean);
        invalidateSelf();
    }

    public void setGravity(int paramInt)
    {
        if (this.mBitmapState.mGravity != paramInt)
        {
            this.mBitmapState.mGravity = paramInt;
            this.mApplyGravity = true;
            invalidateSelf();
        }
    }

    public void setTargetDensity(int paramInt)
    {
        if (this.mTargetDensity != paramInt)
        {
            if (paramInt == 0)
                paramInt = 160;
            this.mTargetDensity = paramInt;
            if (this.mBitmap != null)
                computeBitmapSize();
            invalidateSelf();
        }
    }

    public void setTargetDensity(Canvas paramCanvas)
    {
        setTargetDensity(paramCanvas.getDensity());
    }

    public void setTargetDensity(DisplayMetrics paramDisplayMetrics)
    {
        setTargetDensity(paramDisplayMetrics.densityDpi);
    }

    public void setTileModeX(Shader.TileMode paramTileMode)
    {
        setTileModeXY(paramTileMode, this.mBitmapState.mTileModeY);
    }

    public void setTileModeXY(Shader.TileMode paramTileMode1, Shader.TileMode paramTileMode2)
    {
        BitmapState localBitmapState = this.mBitmapState;
        if ((localBitmapState.mTileModeX != paramTileMode1) || (localBitmapState.mTileModeY != paramTileMode2))
        {
            localBitmapState.mTileModeX = paramTileMode1;
            localBitmapState.mTileModeY = paramTileMode2;
            localBitmapState.mRebuildShader = true;
            invalidateSelf();
        }
    }

    public final void setTileModeY(Shader.TileMode paramTileMode)
    {
        setTileModeXY(this.mBitmapState.mTileModeX, paramTileMode);
    }

    static final class BitmapState extends Drawable.ConstantState
    {
        Bitmap mBitmap;
        int mChangingConfigurations;
        int mGravity = 119;
        Paint mPaint = new Paint(6);
        boolean mRebuildShader;
        int mTargetDensity = 160;
        Shader.TileMode mTileModeX = null;
        Shader.TileMode mTileModeY = null;

        BitmapState(Bitmap paramBitmap)
        {
            this.mBitmap = paramBitmap;
        }

        BitmapState(BitmapState paramBitmapState)
        {
            this(paramBitmapState.mBitmap);
            this.mChangingConfigurations = paramBitmapState.mChangingConfigurations;
            this.mGravity = paramBitmapState.mGravity;
            this.mTileModeX = paramBitmapState.mTileModeX;
            this.mTileModeY = paramBitmapState.mTileModeY;
            this.mTargetDensity = paramBitmapState.mTargetDensity;
            this.mPaint = new Paint(paramBitmapState.mPaint);
            this.mRebuildShader = paramBitmapState.mRebuildShader;
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new BitmapDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new BitmapDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.BitmapDrawable
 * JD-Core Version:        0.6.2
 */