package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Gravity;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ScaleDrawable extends Drawable
    implements Drawable.Callback
{
    private boolean mMutated;
    private ScaleState mScaleState;
    private final Rect mTmpRect = new Rect();

    ScaleDrawable()
    {
        this(null, null);
    }

    public ScaleDrawable(Drawable paramDrawable, int paramInt, float paramFloat1, float paramFloat2)
    {
        this(null, null);
        this.mScaleState.mDrawable = paramDrawable;
        this.mScaleState.mGravity = paramInt;
        this.mScaleState.mScaleWidth = paramFloat1;
        this.mScaleState.mScaleHeight = paramFloat2;
        if (paramDrawable != null)
            paramDrawable.setCallback(this);
    }

    private ScaleDrawable(ScaleState paramScaleState, Resources paramResources)
    {
        this.mScaleState = new ScaleState(paramScaleState, this, paramResources);
    }

    private static float getPercent(TypedArray paramTypedArray, int paramInt)
    {
        String str = paramTypedArray.getString(paramInt);
        if ((str != null) && (str.endsWith("%")));
        for (float f = Float.parseFloat(str.substring(0, -1 + str.length())) / 100.0F; ; f = -1.0F)
            return f;
    }

    public void draw(Canvas paramCanvas)
    {
        if (this.mScaleState.mDrawable.getLevel() != 0)
            this.mScaleState.mDrawable.draw(paramCanvas);
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mScaleState.mChangingConfigurations | this.mScaleState.mDrawable.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState()
    {
        if (this.mScaleState.canConstantState())
            this.mScaleState.mChangingConfigurations = getChangingConfigurations();
        for (ScaleState localScaleState = this.mScaleState; ; localScaleState = null)
            return localScaleState;
    }

    public Drawable getDrawable()
    {
        return this.mScaleState.mDrawable;
    }

    public int getIntrinsicHeight()
    {
        return this.mScaleState.mDrawable.getIntrinsicHeight();
    }

    public int getIntrinsicWidth()
    {
        return this.mScaleState.mDrawable.getIntrinsicWidth();
    }

    public int getOpacity()
    {
        return this.mScaleState.mDrawable.getOpacity();
    }

    public boolean getPadding(Rect paramRect)
    {
        return this.mScaleState.mDrawable.getPadding(paramRect);
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        super.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.ScaleDrawable);
        float f1 = getPercent(localTypedArray, 1);
        float f2 = getPercent(localTypedArray, 2);
        int i = localTypedArray.getInt(3, 3);
        boolean bool = localTypedArray.getBoolean(4, false);
        Drawable localDrawable = localTypedArray.getDrawable(0);
        localTypedArray.recycle();
        int j = paramXmlPullParser.getDepth();
        while (true)
        {
            int k = paramXmlPullParser.next();
            if ((k == 1) || ((k == 3) && (paramXmlPullParser.getDepth() <= j)))
                break;
            if (k == 2)
                localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet);
        }
        if (localDrawable == null)
            throw new IllegalArgumentException("No drawable specified for <scale>");
        this.mScaleState.mDrawable = localDrawable;
        this.mScaleState.mScaleWidth = f1;
        this.mScaleState.mScaleHeight = f2;
        this.mScaleState.mGravity = i;
        this.mScaleState.mUseIntrinsicSizeAsMin = bool;
        if (localDrawable != null)
            localDrawable.setCallback(this);
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        if (getCallback() != null)
            getCallback().invalidateDrawable(this);
    }

    public boolean isStateful()
    {
        return this.mScaleState.mDrawable.isStateful();
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            this.mScaleState.mDrawable.mutate();
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        Rect localRect = this.mTmpRect;
        boolean bool = this.mScaleState.mUseIntrinsicSizeAsMin;
        int i = getLevel();
        int j = paramRect.width();
        int i1;
        int k;
        if (this.mScaleState.mScaleWidth > 0.0F)
        {
            if (bool)
            {
                i1 = this.mScaleState.mDrawable.getIntrinsicWidth();
                j -= (int)((j - i1) * (10000 - i) * this.mScaleState.mScaleWidth / 10000.0F);
            }
        }
        else
        {
            k = paramRect.height();
            if (this.mScaleState.mScaleHeight > 0.0F)
                if (!bool)
                    break label214;
        }
        label214: for (int n = this.mScaleState.mDrawable.getIntrinsicHeight(); ; n = 0)
        {
            k -= (int)((k - n) * (10000 - i) * this.mScaleState.mScaleHeight / 10000.0F);
            int m = getResolvedLayoutDirectionSelf();
            Gravity.apply(this.mScaleState.mGravity, j, k, paramRect, localRect, m);
            if ((j > 0) && (k > 0))
                this.mScaleState.mDrawable.setBounds(localRect.left, localRect.top, localRect.right, localRect.bottom);
            return;
            i1 = 0;
            break;
        }
    }

    protected boolean onLevelChange(int paramInt)
    {
        this.mScaleState.mDrawable.setLevel(paramInt);
        onBoundsChange(getBounds());
        invalidateSelf();
        return true;
    }

    protected boolean onStateChange(int[] paramArrayOfInt)
    {
        boolean bool = this.mScaleState.mDrawable.setState(paramArrayOfInt);
        onBoundsChange(getBounds());
        return bool;
    }

    public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
    {
        if (getCallback() != null)
            getCallback().scheduleDrawable(this, paramRunnable, paramLong);
    }

    public void setAlpha(int paramInt)
    {
        this.mScaleState.mDrawable.setAlpha(paramInt);
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        this.mScaleState.mDrawable.setColorFilter(paramColorFilter);
    }

    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mScaleState.mDrawable.setVisible(paramBoolean1, paramBoolean2);
        return super.setVisible(paramBoolean1, paramBoolean2);
    }

    public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
    {
        if (getCallback() != null)
            getCallback().unscheduleDrawable(this, paramRunnable);
    }

    static final class ScaleState extends Drawable.ConstantState
    {
        private boolean mCanConstantState;
        int mChangingConfigurations;
        private boolean mCheckedConstantState;
        Drawable mDrawable;
        int mGravity;
        float mScaleHeight;
        float mScaleWidth;
        boolean mUseIntrinsicSizeAsMin;

        ScaleState(ScaleState paramScaleState, ScaleDrawable paramScaleDrawable, Resources paramResources)
        {
            if (paramScaleState != null)
                if (paramResources == null)
                    break label78;
            label78: for (this.mDrawable = paramScaleState.mDrawable.getConstantState().newDrawable(paramResources); ; this.mDrawable = paramScaleState.mDrawable.getConstantState().newDrawable())
            {
                this.mDrawable.setCallback(paramScaleDrawable);
                this.mScaleWidth = paramScaleState.mScaleWidth;
                this.mScaleHeight = paramScaleState.mScaleHeight;
                this.mGravity = paramScaleState.mGravity;
                this.mUseIntrinsicSizeAsMin = paramScaleState.mUseIntrinsicSizeAsMin;
                this.mCanConstantState = true;
                this.mCheckedConstantState = true;
                return;
            }
        }

        boolean canConstantState()
        {
            if (!this.mCheckedConstantState)
                if (this.mDrawable.getConstantState() == null)
                    break label34;
            label34: for (boolean bool = true; ; bool = false)
            {
                this.mCanConstantState = bool;
                this.mCheckedConstantState = true;
                return this.mCanConstantState;
            }
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new ScaleDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new ScaleDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.ScaleDrawable
 * JD-Core Version:        0.6.2
 */