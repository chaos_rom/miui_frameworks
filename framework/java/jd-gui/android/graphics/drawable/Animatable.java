package android.graphics.drawable;

public abstract interface Animatable
{
    public abstract boolean isRunning();

    public abstract void start();

    public abstract void stop();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.Animatable
 * JD-Core Version:        0.6.2
 */