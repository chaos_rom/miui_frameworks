package android.graphics.drawable;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import java.util.List;
import miui.util.DecodeGifImageHelper;
import miui.util.DecodeGifImageHelper.GifDecodeResult;
import miui.util.DecodeGifImageHelper.GifFrame;
import miui.util.GifDecoder;
import miui.util.InputStreamLoader;

public class GifAnimationDrawable extends AnimationDrawable
{
    private final DecodeGifImageHelper mHelper = new DecodeGifImageHelper();
    private Resources mResources;

    private boolean handleFirstDecodeResult(DecodeGifImageHelper.GifDecodeResult paramGifDecodeResult)
    {
        boolean bool = false;
        if ((paramGifDecodeResult.mGifDecoder == null) || (!paramGifDecodeResult.mIsDecodeOk));
        while (true)
        {
            return bool;
            GifDecoder localGifDecoder = paramGifDecodeResult.mGifDecoder;
            this.mHelper.mDecodedAllFrames = localGifDecoder.isDecodeToTheEnd();
            int i = localGifDecoder.getFrameCount();
            if (i > 0)
            {
                int j = 0;
                if (j < i)
                {
                    if (this.mHelper.mDecodedAllFrames)
                        addFrame(new BitmapDrawable(this.mResources, localGifDecoder.getFrame(j)), localGifDecoder.getDelay(j));
                    while (true)
                    {
                        j++;
                        break;
                        Bitmap localBitmap = localGifDecoder.getFrame(j);
                        int k = localGifDecoder.getDelay(j);
                        this.mHelper.mFrames.add(new DecodeGifImageHelper.GifFrame(localBitmap, k, j));
                    }
                }
                if (!this.mHelper.mDecodedAllFrames)
                {
                    this.mHelper.firstDecodeNextFrames();
                    DecodeGifImageHelper.GifFrame localGifFrame = (DecodeGifImageHelper.GifFrame)this.mHelper.mFrames.get(0);
                    BitmapDrawable localBitmapDrawable = new BitmapDrawable(this.mResources, localGifFrame.mImage);
                    addFrame(localBitmapDrawable, localGifFrame.mDuration);
                    addFrame(localBitmapDrawable, localGifFrame.mDuration);
                }
                setOneShot(false);
                super.selectDrawable(0);
                bool = true;
            }
        }
    }

    private boolean internalLoad(Resources paramResources, InputStreamLoader paramInputStreamLoader)
    {
        this.mResources = paramResources;
        this.mHelper.mGifSource = paramInputStreamLoader;
        return handleFirstDecodeResult(this.mHelper.decodeFrom(0));
    }

    public boolean load(Resources paramResources, Context paramContext, Uri paramUri)
    {
        return internalLoad(paramResources, new InputStreamLoader(paramContext, paramUri));
    }

    public boolean load(Resources paramResources, String paramString)
    {
        return internalLoad(paramResources, new InputStreamLoader(paramString));
    }

    public boolean load(Resources paramResources, byte[] paramArrayOfByte)
    {
        return internalLoad(paramResources, new InputStreamLoader(paramArrayOfByte));
    }

    public void preSelectDrawable(int paramInt)
    {
        if (this.mHelper.mFrames.isEmpty());
        while (true)
        {
            return;
            DecodeGifImageHelper.GifFrame localGifFrame = (DecodeGifImageHelper.GifFrame)this.mHelper.mFrames.get(0);
            if (this.mHelper.mFrames.size() > 1)
                this.mHelper.mFrames.remove(0);
            this.mHelper.decodeNextFrames();
            BitmapDrawable localBitmapDrawable = new BitmapDrawable(this.mResources, localGifFrame.mImage);
            getAnimationState().mDrawables[paramInt] = localBitmapDrawable;
            getAnimationState().setDuration(paramInt, localGifFrame.mDuration);
        }
    }

    public boolean selectDrawable(int paramInt)
    {
        preSelectDrawable(paramInt);
        return super.selectDrawable(paramInt);
    }

    public void setMaxDecodeSize(long paramLong)
    {
        this.mHelper.mMaxDecodeSize = paramLong;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.GifAnimationDrawable
 * JD-Core Version:        0.6.2
 */