package android.graphics.drawable;

import android.content.res.Resources;

public class ListDrawable extends DrawableContainer
{
    private final ListState mListState;
    private Resources mResources;

    public ListDrawable(ListState paramListState, Resources paramResources)
    {
        this.mListState = new ListState(paramListState, this, paramResources);
        setConstantState(this.mListState);
        if (paramResources != null)
        {
            this.mResources = paramResources;
            enableFade(true);
        }
    }

    public void addDrawable(int paramInt)
    {
        if (this.mResources != null)
            this.mListState.addChild(this.mResources.getDrawable(paramInt));
    }

    public void enableFade(boolean paramBoolean)
    {
        if (paramBoolean)
        {
            setEnterFadeDuration(this.mResources.getInteger(17694720));
            setExitFadeDuration(this.mResources.getInteger(17694721));
        }
        while (true)
        {
            return;
            setEnterFadeDuration(1);
            setExitFadeDuration(1);
        }
    }

    protected boolean onLevelChange(int paramInt)
    {
        if (selectDrawable(paramInt));
        for (boolean bool = true; ; bool = super.onLevelChange(paramInt))
            return bool;
    }

    private static final class ListState extends DrawableContainer.DrawableContainerState
    {
        ListState(ListState paramListState, ListDrawable paramListDrawable, Resources paramResources)
        {
            super(paramListDrawable, paramResources);
        }

        public Drawable newDrawable()
        {
            return new ListDrawable(this, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new ListDrawable(this, paramResources);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.ListDrawable
 * JD-Core Version:        0.6.2
 */