package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class LayerDrawable extends Drawable
    implements Drawable.Callback
{
    LayerState mLayerState;
    private boolean mMutated;
    private int mOpacityOverride = 0;
    private int[] mPaddingB;
    private int[] mPaddingL;
    private int[] mPaddingR;
    private int[] mPaddingT;
    private final Rect mTmpRect = new Rect();

    LayerDrawable()
    {
        this((LayerState)null, null);
    }

    LayerDrawable(LayerState paramLayerState, Resources paramResources)
    {
        LayerState localLayerState = createConstantState(paramLayerState, paramResources);
        this.mLayerState = localLayerState;
        if (localLayerState.mNum > 0)
            ensurePadding();
    }

    public LayerDrawable(Drawable[] paramArrayOfDrawable)
    {
        this(paramArrayOfDrawable, null);
    }

    LayerDrawable(Drawable[] paramArrayOfDrawable, LayerState paramLayerState)
    {
        this(paramLayerState, null);
        int i = paramArrayOfDrawable.length;
        ChildDrawable[] arrayOfChildDrawable = new ChildDrawable[i];
        for (int j = 0; j < i; j++)
        {
            arrayOfChildDrawable[j] = new ChildDrawable();
            arrayOfChildDrawable[j].mDrawable = paramArrayOfDrawable[j];
            paramArrayOfDrawable[j].setCallback(this);
            LayerState localLayerState = this.mLayerState;
            localLayerState.mChildrenChangingConfigurations |= paramArrayOfDrawable[j].getChangingConfigurations();
        }
        this.mLayerState.mNum = i;
        this.mLayerState.mChildren = arrayOfChildDrawable;
        ensurePadding();
    }

    private void addLayer(Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        LayerState localLayerState1 = this.mLayerState;
        if (localLayerState1.mChildren != null);
        for (int i = localLayerState1.mChildren.length; ; i = 0)
        {
            int j = localLayerState1.mNum;
            if (j >= i)
            {
                ChildDrawable[] arrayOfChildDrawable = new ChildDrawable[i + 10];
                if (j > 0)
                    System.arraycopy(localLayerState1.mChildren, 0, arrayOfChildDrawable, 0, j);
                localLayerState1.mChildren = arrayOfChildDrawable;
            }
            LayerState localLayerState2 = this.mLayerState;
            localLayerState2.mChildrenChangingConfigurations |= paramDrawable.getChangingConfigurations();
            ChildDrawable localChildDrawable = new ChildDrawable();
            localLayerState1.mChildren[j] = localChildDrawable;
            localChildDrawable.mId = paramInt1;
            localChildDrawable.mDrawable = paramDrawable;
            localChildDrawable.mInsetL = paramInt2;
            localChildDrawable.mInsetT = paramInt3;
            localChildDrawable.mInsetR = paramInt4;
            localChildDrawable.mInsetB = paramInt5;
            localLayerState1.mNum = (1 + localLayerState1.mNum);
            paramDrawable.setCallback(this);
            return;
        }
    }

    private void ensurePadding()
    {
        int i = this.mLayerState.mNum;
        if ((this.mPaddingL != null) && (this.mPaddingL.length >= i));
        while (true)
        {
            return;
            this.mPaddingL = new int[i];
            this.mPaddingT = new int[i];
            this.mPaddingR = new int[i];
            this.mPaddingB = new int[i];
        }
    }

    private boolean reapplyPadding(int paramInt, ChildDrawable paramChildDrawable)
    {
        Rect localRect = this.mTmpRect;
        paramChildDrawable.mDrawable.getPadding(localRect);
        if ((localRect.left != this.mPaddingL[paramInt]) || (localRect.top != this.mPaddingT[paramInt]) || (localRect.right != this.mPaddingR[paramInt]) || (localRect.bottom != this.mPaddingB[paramInt]))
        {
            this.mPaddingL[paramInt] = localRect.left;
            this.mPaddingT[paramInt] = localRect.top;
            this.mPaddingR[paramInt] = localRect.right;
            this.mPaddingB[paramInt] = localRect.bottom;
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    LayerState createConstantState(LayerState paramLayerState, Resources paramResources)
    {
        return new LayerState(paramLayerState, this, paramResources);
    }

    public void draw(Canvas paramCanvas)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        for (int j = 0; j < i; j++)
            arrayOfChildDrawable[j].mDrawable.draw(paramCanvas);
    }

    public Drawable findDrawableByLayerId(int paramInt)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = -1 + this.mLayerState.mNum;
        if (i >= 0)
            if (arrayOfChildDrawable[i].mId != paramInt);
        for (Drawable localDrawable = arrayOfChildDrawable[i].mDrawable; ; localDrawable = null)
        {
            return localDrawable;
            i--;
            break;
        }
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mLayerState.mChangingConfigurations | this.mLayerState.mChildrenChangingConfigurations;
    }

    public Drawable.ConstantState getConstantState()
    {
        if (this.mLayerState.canConstantState())
            this.mLayerState.mChangingConfigurations = getChangingConfigurations();
        for (LayerState localLayerState = this.mLayerState; ; localLayerState = null)
            return localLayerState;
    }

    public Drawable getDrawable(int paramInt)
    {
        return this.mLayerState.mChildren[paramInt].mDrawable;
    }

    public int getId(int paramInt)
    {
        return this.mLayerState.mChildren[paramInt].mId;
    }

    public int getIntrinsicHeight()
    {
        int i = -1;
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int j = this.mLayerState.mNum;
        int k = 0;
        int m = 0;
        for (int n = 0; n < j; n++)
        {
            ChildDrawable localChildDrawable = arrayOfChildDrawable[n];
            int i1 = m + (k + (localChildDrawable.mDrawable.getIntrinsicHeight() + localChildDrawable.mInsetT + localChildDrawable.mInsetB));
            if (i1 > i)
                i = i1;
            k += this.mPaddingT[n];
            m += this.mPaddingB[n];
        }
        return i;
    }

    public int getIntrinsicWidth()
    {
        int i = -1;
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int j = this.mLayerState.mNum;
        int k = 0;
        int m = 0;
        for (int n = 0; n < j; n++)
        {
            ChildDrawable localChildDrawable = arrayOfChildDrawable[n];
            int i1 = m + (k + (localChildDrawable.mDrawable.getIntrinsicWidth() + localChildDrawable.mInsetL + localChildDrawable.mInsetR));
            if (i1 > i)
                i = i1;
            k += this.mPaddingL[n];
            m += this.mPaddingR[n];
        }
        return i;
    }

    public int getNumberOfLayers()
    {
        return this.mLayerState.mNum;
    }

    public int getOpacity()
    {
        if (this.mOpacityOverride != 0);
        for (int i = this.mOpacityOverride; ; i = this.mLayerState.getOpacity())
            return i;
    }

    public boolean getPadding(Rect paramRect)
    {
        paramRect.left = 0;
        paramRect.top = 0;
        paramRect.right = 0;
        paramRect.bottom = 0;
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        for (int j = 0; j < i; j++)
        {
            reapplyPadding(j, arrayOfChildDrawable[j]);
            paramRect.left += this.mPaddingL[j];
            paramRect.top += this.mPaddingT[j];
            paramRect.right += this.mPaddingR[j];
            paramRect.bottom += this.mPaddingB[j];
        }
        return true;
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        super.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
        TypedArray localTypedArray1 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.LayerDrawable);
        this.mOpacityOverride = localTypedArray1.getInt(0, 0);
        localTypedArray1.recycle();
        int i = 1 + paramXmlPullParser.getDepth();
        int j;
        int k;
        do
        {
            j = paramXmlPullParser.next();
            if (j == 1)
                break;
            k = paramXmlPullParser.getDepth();
            if ((k < i) && (j == 3))
                break;
        }
        while ((j != 2) || (k > i) || (!paramXmlPullParser.getName().equals("item")));
        TypedArray localTypedArray2 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.LayerDrawableItem);
        int m = localTypedArray2.getDimensionPixelOffset(2, 0);
        int n = localTypedArray2.getDimensionPixelOffset(3, 0);
        int i1 = localTypedArray2.getDimensionPixelOffset(4, 0);
        int i2 = localTypedArray2.getDimensionPixelOffset(5, 0);
        int i3 = localTypedArray2.getResourceId(1, 0);
        int i4 = localTypedArray2.getResourceId(0, -1);
        localTypedArray2.recycle();
        if (i3 != 0);
        for (Drawable localDrawable = paramResources.getDrawable(i3); ; localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet))
        {
            addLayer(localDrawable, i4, m, n, i1, i2);
            break;
            int i5;
            do
                i5 = paramXmlPullParser.next();
            while (i5 == 4);
            if (i5 != 2)
                throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or " + "child tag defining a drawable");
        }
        ensurePadding();
        onStateChange(getState());
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.invalidateDrawable(this);
    }

    public boolean isStateful()
    {
        return this.mLayerState.isStateful();
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            if (!this.mLayerState.canConstantState())
                throw new IllegalStateException("One or more children of this LayerDrawable does not have constant state; this drawable cannot be mutated.");
            this.mLayerState = new LayerState(this.mLayerState, this, null);
            ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
            int i = this.mLayerState.mNum;
            for (int j = 0; j < i; j++)
                arrayOfChildDrawable[j].mDrawable.mutate();
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        int j = 0;
        int k = 0;
        int m = 0;
        int n = 0;
        for (int i1 = 0; i1 < i; i1++)
        {
            ChildDrawable localChildDrawable = arrayOfChildDrawable[i1];
            localChildDrawable.mDrawable.setBounds(j + (paramRect.left + localChildDrawable.mInsetL), k + (paramRect.top + localChildDrawable.mInsetT), paramRect.right - localChildDrawable.mInsetR - m, paramRect.bottom - localChildDrawable.mInsetB - n);
            j += this.mPaddingL[i1];
            m += this.mPaddingR[i1];
            k += this.mPaddingT[i1];
            n += this.mPaddingB[i1];
        }
    }

    protected boolean onLevelChange(int paramInt)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        int j = 0;
        boolean bool = false;
        for (int k = 0; k < i; k++)
        {
            ChildDrawable localChildDrawable = arrayOfChildDrawable[k];
            if (localChildDrawable.mDrawable.setLevel(paramInt))
                bool = true;
            if (reapplyPadding(k, localChildDrawable))
                j = 1;
        }
        if (j != 0)
            onBoundsChange(getBounds());
        return bool;
    }

    protected boolean onStateChange(int[] paramArrayOfInt)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        int j = 0;
        boolean bool = false;
        for (int k = 0; k < i; k++)
        {
            ChildDrawable localChildDrawable = arrayOfChildDrawable[k];
            if (localChildDrawable.mDrawable.setState(paramArrayOfInt))
                bool = true;
            if (reapplyPadding(k, localChildDrawable))
                j = 1;
        }
        if (j != 0)
            onBoundsChange(getBounds());
        return bool;
    }

    public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.scheduleDrawable(this, paramRunnable, paramLong);
    }

    public void setAlpha(int paramInt)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        for (int j = 0; j < i; j++)
            arrayOfChildDrawable[j].mDrawable.setAlpha(paramInt);
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        for (int j = 0; j < i; j++)
            arrayOfChildDrawable[j].mDrawable.setColorFilter(paramColorFilter);
    }

    public void setDither(boolean paramBoolean)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        for (int j = 0; j < i; j++)
            arrayOfChildDrawable[j].mDrawable.setDither(paramBoolean);
    }

    public boolean setDrawableByLayerId(int paramInt, Drawable paramDrawable)
    {
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = -1 + this.mLayerState.mNum;
        if (i >= 0)
            if (arrayOfChildDrawable[i].mId == paramInt)
            {
                if (arrayOfChildDrawable[i].mDrawable != null)
                {
                    if (paramDrawable != null)
                        paramDrawable.setBounds(arrayOfChildDrawable[i].mDrawable.getBounds());
                    arrayOfChildDrawable[i].mDrawable.setCallback(null);
                }
                if (paramDrawable != null)
                    paramDrawable.setCallback(this);
                arrayOfChildDrawable[i].mDrawable = paramDrawable;
            }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    public void setId(int paramInt1, int paramInt2)
    {
        this.mLayerState.mChildren[paramInt1].mId = paramInt2;
    }

    public void setLayerInset(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        ChildDrawable localChildDrawable = this.mLayerState.mChildren[paramInt1];
        localChildDrawable.mInsetL = paramInt2;
        localChildDrawable.mInsetT = paramInt3;
        localChildDrawable.mInsetR = paramInt4;
        localChildDrawable.mInsetB = paramInt5;
    }

    public void setOpacity(int paramInt)
    {
        this.mOpacityOverride = paramInt;
    }

    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool = super.setVisible(paramBoolean1, paramBoolean2);
        ChildDrawable[] arrayOfChildDrawable = this.mLayerState.mChildren;
        int i = this.mLayerState.mNum;
        for (int j = 0; j < i; j++)
            arrayOfChildDrawable[j].mDrawable.setVisible(paramBoolean1, paramBoolean2);
        return bool;
    }

    public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.unscheduleDrawable(this, paramRunnable);
    }

    static class LayerState extends Drawable.ConstantState
    {
        private boolean mCanConstantState;
        int mChangingConfigurations;
        private boolean mCheckedConstantState;
        LayerDrawable.ChildDrawable[] mChildren;
        int mChildrenChangingConfigurations;
        private boolean mHaveOpacity = false;
        private boolean mHaveStateful = false;
        int mNum;
        private int mOpacity;
        private boolean mStateful;

        LayerState(LayerState paramLayerState, LayerDrawable paramLayerDrawable, Resources paramResources)
        {
            if (paramLayerState != null)
            {
                LayerDrawable.ChildDrawable[] arrayOfChildDrawable1 = paramLayerState.mChildren;
                int i = paramLayerState.mNum;
                this.mNum = i;
                this.mChildren = new LayerDrawable.ChildDrawable[i];
                this.mChangingConfigurations = paramLayerState.mChangingConfigurations;
                this.mChildrenChangingConfigurations = paramLayerState.mChildrenChangingConfigurations;
                int j = 0;
                if (j < i)
                {
                    LayerDrawable.ChildDrawable[] arrayOfChildDrawable2 = this.mChildren;
                    LayerDrawable.ChildDrawable localChildDrawable1 = new LayerDrawable.ChildDrawable();
                    arrayOfChildDrawable2[j] = localChildDrawable1;
                    LayerDrawable.ChildDrawable localChildDrawable2 = arrayOfChildDrawable1[j];
                    if (paramResources != null);
                    for (localChildDrawable1.mDrawable = localChildDrawable2.mDrawable.getConstantState().newDrawable(paramResources); ; localChildDrawable1.mDrawable = localChildDrawable2.mDrawable.getConstantState().newDrawable())
                    {
                        localChildDrawable1.mDrawable.setCallback(paramLayerDrawable);
                        localChildDrawable1.mInsetL = localChildDrawable2.mInsetL;
                        localChildDrawable1.mInsetT = localChildDrawable2.mInsetT;
                        localChildDrawable1.mInsetR = localChildDrawable2.mInsetR;
                        localChildDrawable1.mInsetB = localChildDrawable2.mInsetB;
                        localChildDrawable1.mId = localChildDrawable2.mId;
                        j++;
                        break;
                    }
                }
                this.mHaveOpacity = paramLayerState.mHaveOpacity;
                this.mOpacity = paramLayerState.mOpacity;
                this.mHaveStateful = paramLayerState.mHaveStateful;
                this.mStateful = paramLayerState.mStateful;
                this.mCanConstantState = true;
                this.mCheckedConstantState = true;
            }
            while (true)
            {
                return;
                this.mNum = 0;
                this.mChildren = null;
            }
        }

        public boolean canConstantState()
        {
            int i;
            if ((!this.mCheckedConstantState) && (this.mChildren != null))
            {
                this.mCanConstantState = true;
                i = this.mNum;
            }
            for (int j = 0; ; j++)
                if (j < i)
                {
                    if (this.mChildren[j].mDrawable.getConstantState() == null)
                        this.mCanConstantState = false;
                }
                else
                {
                    this.mCheckedConstantState = true;
                    return this.mCanConstantState;
                }
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public final int getOpacity()
        {
            int j;
            if (this.mHaveOpacity)
                j = this.mOpacity;
            while (true)
            {
                return j;
                int i = this.mNum;
                if (i > 0);
                for (j = this.mChildren[0].mDrawable.getOpacity(); ; j = -2)
                    for (int k = 1; k < i; k++)
                        j = Drawable.resolveOpacity(j, this.mChildren[k].mDrawable.getOpacity());
                this.mOpacity = j;
                this.mHaveOpacity = true;
            }
        }

        public final boolean isStateful()
        {
            if (this.mHaveStateful)
            {
                bool = this.mStateful;
                return bool;
            }
            boolean bool = false;
            int i = this.mNum;
            for (int j = 0; ; j++)
                if (j < i)
                {
                    if (this.mChildren[j].mDrawable.isStateful())
                        bool = true;
                }
                else
                {
                    this.mStateful = bool;
                    this.mHaveStateful = true;
                    break;
                }
        }

        public Drawable newDrawable()
        {
            return new LayerDrawable(this, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new LayerDrawable(this, paramResources);
        }
    }

    static class ChildDrawable
    {
        public Drawable mDrawable;
        public int mId;
        public int mInsetB;
        public int mInsetL;
        public int mInsetR;
        public int mInsetT;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.LayerDrawable
 * JD-Core Version:        0.6.2
 */