package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.shapes.Shape;
import android.util.AttributeSet;
import android.util.Log;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ShapeDrawable extends Drawable
{
    private boolean mMutated;
    private ShapeState mShapeState;

    public ShapeDrawable()
    {
        this((ShapeState)null);
    }

    private ShapeDrawable(ShapeState paramShapeState)
    {
        this.mShapeState = new ShapeState(paramShapeState);
    }

    public ShapeDrawable(Shape paramShape)
    {
        this((ShapeState)null);
        this.mShapeState.mShape = paramShape;
    }

    private static int modulateAlpha(int paramInt1, int paramInt2)
    {
        return paramInt1 * (paramInt2 + (paramInt2 >>> 7)) >>> 8;
    }

    private void updateShape()
    {
        if (this.mShapeState.mShape != null)
        {
            Rect localRect = getBounds();
            int i = localRect.width();
            int j = localRect.height();
            this.mShapeState.mShape.resize(i, j);
            if (this.mShapeState.mShaderFactory != null)
                this.mShapeState.mPaint.setShader(this.mShapeState.mShaderFactory.resize(i, j));
        }
        invalidateSelf();
    }

    public void draw(Canvas paramCanvas)
    {
        Rect localRect = getBounds();
        Paint localPaint = this.mShapeState.mPaint;
        int i = localPaint.getAlpha();
        localPaint.setAlpha(modulateAlpha(i, this.mShapeState.mAlpha));
        if (this.mShapeState.mShape != null)
        {
            int j = paramCanvas.save();
            paramCanvas.translate(localRect.left, localRect.top);
            onDraw(this.mShapeState.mShape, paramCanvas, localPaint);
            paramCanvas.restoreToCount(j);
        }
        while (true)
        {
            localPaint.setAlpha(i);
            return;
            paramCanvas.drawRect(localRect, localPaint);
        }
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mShapeState.mChangingConfigurations;
    }

    public Drawable.ConstantState getConstantState()
    {
        this.mShapeState.mChangingConfigurations = getChangingConfigurations();
        return this.mShapeState;
    }

    public int getIntrinsicHeight()
    {
        return this.mShapeState.mIntrinsicHeight;
    }

    public int getIntrinsicWidth()
    {
        return this.mShapeState.mIntrinsicWidth;
    }

    public int getOpacity()
    {
        int j;
        int i;
        if (this.mShapeState.mShape == null)
        {
            Paint localPaint = this.mShapeState.mPaint;
            if (localPaint.getXfermode() == null)
            {
                j = localPaint.getAlpha();
                if (j == 0)
                    i = -2;
            }
        }
        while (true)
        {
            return i;
            if (j == 255)
                i = -1;
            else
                i = -3;
        }
    }

    public boolean getPadding(Rect paramRect)
    {
        if (this.mShapeState.mPadding != null)
            paramRect.set(this.mShapeState.mPadding);
        for (boolean bool = true; ; bool = super.getPadding(paramRect))
            return bool;
    }

    public Paint getPaint()
    {
        return this.mShapeState.mPaint;
    }

    public ShaderFactory getShaderFactory()
    {
        return this.mShapeState.mShaderFactory;
    }

    public Shape getShape()
    {
        return this.mShapeState.mShape;
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        super.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.ShapeDrawable);
        int i = localTypedArray.getColor(3, this.mShapeState.mPaint.getColor());
        this.mShapeState.mPaint.setColor(i);
        boolean bool = localTypedArray.getBoolean(0, false);
        this.mShapeState.mPaint.setDither(bool);
        setIntrinsicWidth((int)localTypedArray.getDimension(2, 0.0F));
        setIntrinsicHeight((int)localTypedArray.getDimension(1, 0.0F));
        localTypedArray.recycle();
        int j = paramXmlPullParser.getDepth();
        while (true)
        {
            int k = paramXmlPullParser.next();
            if ((k == 1) || ((k == 3) && (paramXmlPullParser.getDepth() <= j)))
                break;
            if (k == 2)
            {
                String str = paramXmlPullParser.getName();
                if (!inflateTag(str, paramResources, paramXmlPullParser, paramAttributeSet))
                    Log.w("drawable", "Unknown element: " + str + " for ShapeDrawable " + this);
            }
        }
    }

    protected boolean inflateTag(String paramString, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
    {
        int i = 1;
        if ("padding".equals(paramString))
        {
            TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.ShapeDrawablePadding);
            setPadding(localTypedArray.getDimensionPixelOffset(0, 0), localTypedArray.getDimensionPixelOffset(i, 0), localTypedArray.getDimensionPixelOffset(2, 0), localTypedArray.getDimensionPixelOffset(3, 0));
            localTypedArray.recycle();
        }
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            this.mShapeState.mPaint = new Paint(this.mShapeState.mPaint);
            this.mShapeState.mPadding = new Rect(this.mShapeState.mPadding);
        }
        try
        {
            this.mShapeState.mShape = this.mShapeState.mShape.clone();
            this.mMutated = true;
            return this;
        }
        catch (CloneNotSupportedException localCloneNotSupportedException)
        {
            while (true)
                this = null;
        }
    }

    protected void onBoundsChange(Rect paramRect)
    {
        super.onBoundsChange(paramRect);
        updateShape();
    }

    protected void onDraw(Shape paramShape, Canvas paramCanvas, Paint paramPaint)
    {
        paramShape.draw(paramCanvas, paramPaint);
    }

    public void setAlpha(int paramInt)
    {
        this.mShapeState.mAlpha = paramInt;
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        this.mShapeState.mPaint.setColorFilter(paramColorFilter);
        invalidateSelf();
    }

    public void setDither(boolean paramBoolean)
    {
        this.mShapeState.mPaint.setDither(paramBoolean);
        invalidateSelf();
    }

    public void setIntrinsicHeight(int paramInt)
    {
        this.mShapeState.mIntrinsicHeight = paramInt;
        invalidateSelf();
    }

    public void setIntrinsicWidth(int paramInt)
    {
        this.mShapeState.mIntrinsicWidth = paramInt;
        invalidateSelf();
    }

    public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if ((paramInt4 | (paramInt3 | (paramInt1 | paramInt2))) == 0)
            this.mShapeState.mPadding = null;
        while (true)
        {
            invalidateSelf();
            return;
            if (this.mShapeState.mPadding == null)
                this.mShapeState.mPadding = new Rect();
            this.mShapeState.mPadding.set(paramInt1, paramInt2, paramInt3, paramInt4);
        }
    }

    public void setPadding(Rect paramRect)
    {
        if (paramRect == null)
            this.mShapeState.mPadding = null;
        while (true)
        {
            invalidateSelf();
            return;
            if (this.mShapeState.mPadding == null)
                this.mShapeState.mPadding = new Rect();
            this.mShapeState.mPadding.set(paramRect);
        }
    }

    public void setShaderFactory(ShaderFactory paramShaderFactory)
    {
        this.mShapeState.mShaderFactory = paramShaderFactory;
    }

    public void setShape(Shape paramShape)
    {
        this.mShapeState.mShape = paramShape;
        updateShape();
    }

    public static abstract class ShaderFactory
    {
        public abstract Shader resize(int paramInt1, int paramInt2);
    }

    static final class ShapeState extends Drawable.ConstantState
    {
        int mAlpha = 255;
        int mChangingConfigurations;
        int mIntrinsicHeight;
        int mIntrinsicWidth;
        Rect mPadding;
        Paint mPaint;
        ShapeDrawable.ShaderFactory mShaderFactory;
        Shape mShape;

        ShapeState(ShapeState paramShapeState)
        {
            if (paramShapeState != null)
            {
                this.mPaint = paramShapeState.mPaint;
                this.mShape = paramShapeState.mShape;
                this.mPadding = paramShapeState.mPadding;
                this.mIntrinsicWidth = paramShapeState.mIntrinsicWidth;
                this.mIntrinsicHeight = paramShapeState.mIntrinsicHeight;
                this.mAlpha = paramShapeState.mAlpha;
                this.mShaderFactory = paramShapeState.mShaderFactory;
            }
            while (true)
            {
                return;
                this.mPaint = new Paint(1);
            }
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new ShapeDrawable(this, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new ShapeDrawable(this, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.ShapeDrawable
 * JD-Core Version:        0.6.2
 */