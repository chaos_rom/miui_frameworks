package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Path.FillType;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class GradientDrawable extends Drawable
{
    public static final int LINE = 2;
    public static final int LINEAR_GRADIENT = 0;
    public static final int OVAL = 1;
    public static final int RADIAL_GRADIENT = 1;
    public static final int RECTANGLE = 0;
    public static final int RING = 3;
    public static final int SWEEP_GRADIENT = 2;
    private int mAlpha = 255;
    private ColorFilter mColorFilter;
    private boolean mDither;
    private final Paint mFillPaint = new Paint(1);
    private GradientState mGradientState;
    private Paint mLayerPaint;
    private boolean mMutated;
    private Rect mPadding;
    private final Path mPath = new Path();
    private boolean mPathIsDirty = true;
    private final RectF mRect = new RectF();
    private boolean mRectIsDirty;
    private Path mRingPath;
    private Paint mStrokePaint;

    public GradientDrawable()
    {
        this(new GradientState(Orientation.TOP_BOTTOM, null));
    }

    private GradientDrawable(GradientState paramGradientState)
    {
        this.mGradientState = paramGradientState;
        initializeWithState(paramGradientState);
        this.mRectIsDirty = true;
    }

    public GradientDrawable(Orientation paramOrientation, int[] paramArrayOfInt)
    {
        this(new GradientState(paramOrientation, paramArrayOfInt));
    }

    private Path buildRing(GradientState paramGradientState)
    {
        Path localPath;
        if ((this.mRingPath != null) && ((!paramGradientState.mUseLevelForShape) || (!this.mPathIsDirty)))
            localPath = this.mRingPath;
        while (true)
        {
            return localPath;
            this.mPathIsDirty = false;
            float f1;
            label54: RectF localRectF1;
            float f2;
            float f3;
            float f4;
            label98: float f5;
            label114: RectF localRectF2;
            RectF localRectF3;
            if (paramGradientState.mUseLevelForShape)
            {
                f1 = 360.0F * getLevel() / 10000.0F;
                localRectF1 = new RectF(this.mRect);
                f2 = localRectF1.width() / 2.0F;
                f3 = localRectF1.height() / 2.0F;
                if (paramGradientState.mThickness == -1)
                    break label269;
                f4 = paramGradientState.mThickness;
                if (paramGradientState.mInnerRadius == -1)
                    break label283;
                f5 = paramGradientState.mInnerRadius;
                localRectF2 = new RectF(localRectF1);
                localRectF2.inset(f2 - f5, f3 - f5);
                localRectF3 = new RectF(localRectF2);
                localRectF3.inset(-f4, -f4);
                if (this.mRingPath != null)
                    break label297;
                this.mRingPath = new Path();
            }
            while (true)
            {
                localPath = this.mRingPath;
                if ((f1 >= 360.0F) || (f1 <= -360.0F))
                    break label307;
                localPath.setFillType(Path.FillType.EVEN_ODD);
                localPath.moveTo(f2 + f5, f3);
                localPath.lineTo(f4 + (f2 + f5), f3);
                localPath.arcTo(localRectF3, 0.0F, f1, false);
                localPath.arcTo(localRectF2, f1, -f1, false);
                localPath.close();
                break;
                f1 = 360.0F;
                break label54;
                label269: f4 = localRectF1.width() / paramGradientState.mThicknessRatio;
                break label98;
                label283: f5 = localRectF1.width() / paramGradientState.mInnerRadiusRatio;
                break label114;
                label297: this.mRingPath.reset();
            }
            label307: localPath.addOval(localRectF3, Path.Direction.CW);
            localPath.addOval(localRectF2, Path.Direction.CCW);
        }
    }

    private boolean ensureValidRect()
    {
        GradientState localGradientState;
        int[] arrayOfInt1;
        RectF localRectF;
        float f9;
        if (this.mRectIsDirty)
        {
            this.mRectIsDirty = false;
            Rect localRect = getBounds();
            float f1 = 0.0F;
            if (this.mStrokePaint != null)
                f1 = 0.5F * this.mStrokePaint.getStrokeWidth();
            localGradientState = this.mGradientState;
            this.mRect.set(f1 + localRect.left, f1 + localRect.top, localRect.right - f1, localRect.bottom - f1);
            arrayOfInt1 = localGradientState.mColors;
            if (arrayOfInt1 != null)
            {
                localRectF = this.mRect;
                if (localGradientState.mGradient != 0)
                    break label498;
                if (!localGradientState.mUseLevel)
                    break label257;
                f9 = getLevel() / 10000.0F;
            }
        }
        float f10;
        float f11;
        float f12;
        float f13;
        switch (1.$SwitchMap$android$graphics$drawable$GradientDrawable$Orientation[localGradientState.mOrientation.ordinal()])
        {
        default:
            f10 = localRectF.left;
            f11 = localRectF.top;
            f12 = f9 * localRectF.right;
            f13 = f9 * localRectF.bottom;
            label210: this.mFillPaint.setShader(new LinearGradient(f10, f11, f12, f13, arrayOfInt1, localGradientState.mPositions, Shader.TileMode.CLAMP));
            label243: if (this.mRect.isEmpty())
                break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            label257: f9 = 1.0F;
            break;
            f10 = localRectF.left;
            f11 = localRectF.top;
            f12 = f10;
            f13 = f9 * localRectF.bottom;
            break label210;
            f10 = localRectF.right;
            f11 = localRectF.top;
            f12 = f9 * localRectF.left;
            f13 = f9 * localRectF.bottom;
            break label210;
            f10 = localRectF.right;
            f11 = localRectF.top;
            f12 = f9 * localRectF.left;
            f13 = f11;
            break label210;
            f10 = localRectF.right;
            f11 = localRectF.bottom;
            f12 = f9 * localRectF.left;
            f13 = f9 * localRectF.top;
            break label210;
            f10 = localRectF.left;
            f11 = localRectF.bottom;
            f12 = f10;
            f13 = f9 * localRectF.top;
            break label210;
            f10 = localRectF.left;
            f11 = localRectF.bottom;
            f12 = f9 * localRectF.right;
            f13 = f9 * localRectF.top;
            break label210;
            f10 = localRectF.left;
            f11 = localRectF.top;
            f12 = f9 * localRectF.right;
            f13 = f11;
            break label210;
            label498: if (localGradientState.mGradient == 1)
            {
                float f6 = localRectF.left + (localRectF.right - localRectF.left) * localGradientState.mCenterX;
                float f7 = localRectF.top + (localRectF.bottom - localRectF.top) * localGradientState.mCenterY;
                if (localGradientState.mUseLevel);
                for (float f8 = getLevel() / 10000.0F; ; f8 = 1.0F)
                {
                    this.mFillPaint.setShader(new RadialGradient(f6, f7, f8 * localGradientState.mGradientRadius, arrayOfInt1, null, Shader.TileMode.CLAMP));
                    break;
                }
            }
            if (localGradientState.mGradient != 2)
                break label243;
            float f2 = localRectF.left + (localRectF.right - localRectF.left) * localGradientState.mCenterX;
            float f3 = localRectF.top + (localRectF.bottom - localRectF.top) * localGradientState.mCenterY;
            int[] arrayOfInt2 = arrayOfInt1;
            float[] arrayOfFloat = null;
            if (localGradientState.mUseLevel)
            {
                arrayOfInt2 = localGradientState.mTempColors;
                int i = arrayOfInt1.length;
                if ((arrayOfInt2 == null) || (arrayOfInt2.length != i + 1))
                {
                    arrayOfInt2 = new int[i + 1];
                    localGradientState.mTempColors = arrayOfInt2;
                }
                System.arraycopy(arrayOfInt1, 0, arrayOfInt2, 0, i);
                arrayOfInt2[i] = arrayOfInt1[(i - 1)];
                arrayOfFloat = localGradientState.mTempPositions;
                float f4 = 1.0F / (i - 1);
                if ((arrayOfFloat == null) || (arrayOfFloat.length != i + 1))
                {
                    arrayOfFloat = new float[i + 1];
                    localGradientState.mTempPositions = arrayOfFloat;
                }
                float f5 = getLevel() / 10000.0F;
                for (int j = 0; j < i; j++)
                    arrayOfFloat[j] = (f5 * (f4 * j));
                arrayOfFloat[i] = 1.0F;
            }
            this.mFillPaint.setShader(new SweepGradient(f2, f3, arrayOfInt2, arrayOfFloat));
            break label243;
        }
    }

    private static float getFloatOrFraction(TypedArray paramTypedArray, int paramInt, float paramFloat)
    {
        TypedValue localTypedValue = paramTypedArray.peekValue(paramInt);
        float f = paramFloat;
        int i;
        if (localTypedValue != null)
        {
            if (localTypedValue.type != 6)
                break label41;
            i = 1;
            if (i == 0)
                break label47;
        }
        label41: label47: for (f = localTypedValue.getFraction(1.0F, 1.0F); ; f = localTypedValue.getFloat())
        {
            return f;
            i = 0;
            break;
        }
    }

    private void initializeWithState(GradientState paramGradientState)
    {
        if (paramGradientState.mHasSolidColor)
            this.mFillPaint.setColor(paramGradientState.mSolidColor);
        this.mPadding = paramGradientState.mPadding;
        if (paramGradientState.mStrokeWidth >= 0)
        {
            this.mStrokePaint = new Paint(1);
            this.mStrokePaint.setStyle(Paint.Style.STROKE);
            this.mStrokePaint.setStrokeWidth(paramGradientState.mStrokeWidth);
            this.mStrokePaint.setColor(paramGradientState.mStrokeColor);
            if (paramGradientState.mStrokeDashWidth != 0.0F)
            {
                float[] arrayOfFloat = new float[2];
                arrayOfFloat[0] = paramGradientState.mStrokeDashWidth;
                arrayOfFloat[1] = paramGradientState.mStrokeDashGap;
                DashPathEffect localDashPathEffect = new DashPathEffect(arrayOfFloat, 0.0F);
                this.mStrokePaint.setPathEffect(localDashPathEffect);
            }
        }
    }

    private int modulateAlpha(int paramInt)
    {
        return paramInt * (this.mAlpha + (this.mAlpha >> 7)) >> 8;
    }

    public void draw(Canvas paramCanvas)
    {
        if (!ensureValidRect());
        while (true)
        {
            return;
            int i = this.mFillPaint.getAlpha();
            int j;
            label31: int k;
            int m;
            int n;
            label65: int i1;
            label73: GradientState localGradientState;
            int i2;
            if (this.mStrokePaint != null)
            {
                j = this.mStrokePaint.getAlpha();
                k = modulateAlpha(i);
                m = modulateAlpha(j);
                if ((m <= 0) || (this.mStrokePaint.getStrokeWidth() <= 0.0F))
                    break label313;
                n = 1;
                if (k <= 0)
                    break label319;
                i1 = 1;
                localGradientState = this.mGradientState;
                if ((n == 0) || (i1 == 0) || (localGradientState.mShape == 2) || (m >= 255) || ((this.mAlpha >= 255) && (this.mColorFilter == null)))
                    break label325;
                i2 = 1;
                label126: if (i2 == 0)
                    break label331;
                if (this.mLayerPaint == null)
                    this.mLayerPaint = new Paint();
                this.mLayerPaint.setDither(this.mDither);
                this.mLayerPaint.setAlpha(this.mAlpha);
                this.mLayerPaint.setColorFilter(this.mColorFilter);
                float f4 = this.mStrokePaint.getStrokeWidth();
                paramCanvas.saveLayer(this.mRect.left - f4, this.mRect.top - f4, f4 + this.mRect.right, f4 + this.mRect.bottom, this.mLayerPaint, 4);
                this.mFillPaint.setColorFilter(null);
                this.mStrokePaint.setColorFilter(null);
                label260: switch (localGradientState.mShape)
                {
                default:
                case 0:
                case 1:
                case 2:
                case 3:
                }
            }
            while (true)
            {
                if (i2 == 0)
                    break label738;
                paramCanvas.restore();
                break;
                j = 0;
                break label31;
                label313: n = 0;
                break label65;
                label319: i1 = 0;
                break label73;
                label325: i2 = 0;
                break label126;
                label331: this.mFillPaint.setAlpha(k);
                this.mFillPaint.setDither(this.mDither);
                this.mFillPaint.setColorFilter(this.mColorFilter);
                if (n == 0)
                    break label260;
                this.mStrokePaint.setAlpha(m);
                this.mStrokePaint.setDither(this.mDither);
                this.mStrokePaint.setColorFilter(this.mColorFilter);
                break label260;
                if (localGradientState.mRadiusArray != null)
                {
                    if ((this.mPathIsDirty) || (this.mRectIsDirty))
                    {
                        this.mPath.reset();
                        this.mPath.addRoundRect(this.mRect, localGradientState.mRadiusArray, Path.Direction.CW);
                        this.mRectIsDirty = false;
                        this.mPathIsDirty = false;
                    }
                    paramCanvas.drawPath(this.mPath, this.mFillPaint);
                    if (n != 0)
                        paramCanvas.drawPath(this.mPath, this.mStrokePaint);
                }
                else if (localGradientState.mRadius > 0.0F)
                {
                    float f2 = localGradientState.mRadius;
                    float f3 = 0.5F * Math.min(this.mRect.width(), this.mRect.height());
                    if (f2 > f3)
                        f2 = f3;
                    RectF localRectF2 = this.mRect;
                    Paint localPaint1 = this.mFillPaint;
                    paramCanvas.drawRoundRect(localRectF2, f2, f2, localPaint1);
                    if (n != 0)
                    {
                        RectF localRectF3 = this.mRect;
                        Paint localPaint2 = this.mStrokePaint;
                        paramCanvas.drawRoundRect(localRectF3, f2, f2, localPaint2);
                    }
                }
                else
                {
                    paramCanvas.drawRect(this.mRect, this.mFillPaint);
                    if (n != 0)
                    {
                        paramCanvas.drawRect(this.mRect, this.mStrokePaint);
                        continue;
                        paramCanvas.drawOval(this.mRect, this.mFillPaint);
                        if (n != 0)
                        {
                            paramCanvas.drawOval(this.mRect, this.mStrokePaint);
                            continue;
                            RectF localRectF1 = this.mRect;
                            float f1 = localRectF1.centerY();
                            paramCanvas.drawLine(localRectF1.left, f1, localRectF1.right, f1, this.mStrokePaint);
                            continue;
                            Path localPath = buildRing(localGradientState);
                            paramCanvas.drawPath(localPath, this.mFillPaint);
                            if (n != 0)
                                paramCanvas.drawPath(localPath, this.mStrokePaint);
                        }
                    }
                }
            }
            label738: this.mFillPaint.setAlpha(i);
            if (n != 0)
                this.mStrokePaint.setAlpha(j);
        }
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mGradientState.mChangingConfigurations;
    }

    public Drawable.ConstantState getConstantState()
    {
        this.mGradientState.mChangingConfigurations = getChangingConfigurations();
        return this.mGradientState;
    }

    public int getIntrinsicHeight()
    {
        return this.mGradientState.mHeight;
    }

    public int getIntrinsicWidth()
    {
        return this.mGradientState.mWidth;
    }

    public int getOpacity()
    {
        return -3;
    }

    public Orientation getOrientation()
    {
        return this.mGradientState.mOrientation;
    }

    public boolean getPadding(Rect paramRect)
    {
        if (this.mPadding != null)
            paramRect.set(this.mPadding);
        for (boolean bool = true; ; bool = super.getPadding(paramRect))
            return bool;
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        GradientState localGradientState = this.mGradientState;
        TypedArray localTypedArray1 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.GradientDrawable);
        super.inflateWithAttributes(paramResources, paramXmlPullParser, localTypedArray1, 1);
        int i = localTypedArray1.getInt(2, 0);
        boolean bool1 = localTypedArray1.getBoolean(0, false);
        if (i == 3)
        {
            localGradientState.mInnerRadius = localTypedArray1.getDimensionPixelSize(6, -1);
            if (localGradientState.mInnerRadius == -1)
                localGradientState.mInnerRadiusRatio = localTypedArray1.getFloat(3, 3.0F);
            localGradientState.mThickness = localTypedArray1.getDimensionPixelSize(7, -1);
            if (localGradientState.mThickness == -1)
                localGradientState.mThicknessRatio = localTypedArray1.getFloat(4, 9.0F);
            GradientState.access$102(localGradientState, localTypedArray1.getBoolean(5, true));
        }
        localTypedArray1.recycle();
        setShape(i);
        setDither(bool1);
        int j = 1 + paramXmlPullParser.getDepth();
        while (true)
        {
            int k = paramXmlPullParser.next();
            if (k == 1)
                break;
            int m = paramXmlPullParser.getDepth();
            if ((m < j) && (k == 3))
                break;
            if ((k == 2) && (m <= j))
            {
                String str = paramXmlPullParser.getName();
                if (str.equals("size"))
                {
                    TypedArray localTypedArray7 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.GradientDrawableSize);
                    int i14 = localTypedArray7.getDimensionPixelSize(1, -1);
                    int i15 = localTypedArray7.getDimensionPixelSize(0, -1);
                    localTypedArray7.recycle();
                    setSize(i14, i15);
                }
                else if (str.equals("gradient"))
                {
                    TypedArray localTypedArray6 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.GradientDrawableGradient);
                    int i8 = localTypedArray6.getColor(0, 0);
                    boolean bool2 = localTypedArray6.hasValue(8);
                    int i9 = localTypedArray6.getColor(8, 0);
                    int i10 = localTypedArray6.getColor(1, 0);
                    int i11 = localTypedArray6.getInt(4, 0);
                    GradientState.access$202(localGradientState, getFloatOrFraction(localTypedArray6, 5, 0.5F));
                    GradientState.access$302(localGradientState, getFloatOrFraction(localTypedArray6, 6, 0.5F));
                    GradientState.access$002(localGradientState, localTypedArray6.getBoolean(2, false));
                    localGradientState.mGradient = i11;
                    label536: float[] arrayOfFloat2;
                    if (i11 == 0)
                    {
                        int i13 = (int)localTypedArray6.getFloat(3, 0.0F) % 360;
                        if (i13 % 45 != 0)
                            throw new XmlPullParserException(localTypedArray6.getPositionDescription() + "<gradient> tag requires 'angle' attribute to " + "be a multiple of 45");
                        switch (i13)
                        {
                        default:
                            localTypedArray6.recycle();
                            if (!bool2)
                                break label849;
                            localGradientState.mColors = new int[3];
                            localGradientState.mColors[0] = i8;
                            localGradientState.mColors[1] = i9;
                            localGradientState.mColors[2] = i10;
                            localGradientState.mPositions = new float[3];
                            localGradientState.mPositions[0] = 0.0F;
                            arrayOfFloat2 = localGradientState.mPositions;
                            if (localGradientState.mCenterX == 0.5F)
                                break;
                        case 0:
                        case 45:
                        case 90:
                        case 135:
                        case 180:
                        case 225:
                        case 270:
                        case 315:
                        }
                    }
                    for (float f2 = localGradientState.mCenterX; ; f2 = localGradientState.mCenterY)
                    {
                        arrayOfFloat2[1] = f2;
                        localGradientState.mPositions[2] = 1.0F;
                        break;
                        localGradientState.mOrientation = Orientation.LEFT_RIGHT;
                        break label536;
                        localGradientState.mOrientation = Orientation.BL_TR;
                        break label536;
                        localGradientState.mOrientation = Orientation.BOTTOM_TOP;
                        break label536;
                        localGradientState.mOrientation = Orientation.BR_TL;
                        break label536;
                        localGradientState.mOrientation = Orientation.RIGHT_LEFT;
                        break label536;
                        localGradientState.mOrientation = Orientation.TR_BL;
                        break label536;
                        localGradientState.mOrientation = Orientation.TOP_BOTTOM;
                        break label536;
                        localGradientState.mOrientation = Orientation.TL_BR;
                        break label536;
                        TypedValue localTypedValue = localTypedArray6.peekValue(7);
                        if (localTypedValue != null)
                        {
                            int i12;
                            if (localTypedValue.type == 6)
                            {
                                i12 = 1;
                                label754: if (i12 == 0)
                                    break label785;
                            }
                            label785: for (float f3 = localTypedValue.getFraction(1.0F, 1.0F); ; f3 = localTypedValue.getFloat())
                            {
                                GradientState.access$402(localGradientState, f3);
                                break;
                                i12 = 0;
                                break label754;
                            }
                        }
                        if (i11 != 1)
                            break label536;
                        throw new XmlPullParserException(localTypedArray6.getPositionDescription() + "<gradient> tag requires 'gradientRadius' " + "attribute with radial type");
                    }
                    label849: localGradientState.mColors = new int[2];
                    localGradientState.mColors[0] = i8;
                    localGradientState.mColors[1] = i10;
                }
                else if (str.equals("solid"))
                {
                    TypedArray localTypedArray5 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.GradientDrawableSolid);
                    int i7 = localTypedArray5.getColor(0, 0);
                    localTypedArray5.recycle();
                    setColor(i7);
                }
                else
                {
                    if (str.equals("stroke"))
                    {
                        TypedArray localTypedArray4 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.GradientDrawableStroke);
                        int i5 = localTypedArray4.getDimensionPixelSize(0, 0);
                        int i6 = localTypedArray4.getColor(1, 0);
                        float f1 = localTypedArray4.getDimension(2, 0.0F);
                        if (f1 != 0.0F)
                            setStroke(i5, i6, f1, localTypedArray4.getDimension(3, 0.0F));
                        while (true)
                        {
                            localTypedArray4.recycle();
                            break;
                            setStroke(i5, i6);
                        }
                    }
                    if (str.equals("corners"))
                    {
                        TypedArray localTypedArray3 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.DrawableCorners);
                        int n = localTypedArray3.getDimensionPixelSize(0, 0);
                        setCornerRadius(n);
                        int i1 = localTypedArray3.getDimensionPixelSize(1, n);
                        int i2 = localTypedArray3.getDimensionPixelSize(2, n);
                        int i3 = localTypedArray3.getDimensionPixelSize(3, n);
                        int i4 = localTypedArray3.getDimensionPixelSize(4, n);
                        if ((i1 != n) || (i2 != n) || (i3 != n) || (i4 != n))
                        {
                            float[] arrayOfFloat1 = new float[8];
                            arrayOfFloat1[0] = i1;
                            arrayOfFloat1[1] = i1;
                            arrayOfFloat1[2] = i2;
                            arrayOfFloat1[3] = i2;
                            arrayOfFloat1[4] = i4;
                            arrayOfFloat1[5] = i4;
                            arrayOfFloat1[6] = i3;
                            arrayOfFloat1[7] = i3;
                            setCornerRadii(arrayOfFloat1);
                        }
                        localTypedArray3.recycle();
                    }
                    else if (str.equals("padding"))
                    {
                        TypedArray localTypedArray2 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.GradientDrawablePadding);
                        this.mPadding = new Rect(localTypedArray2.getDimensionPixelOffset(0, 0), localTypedArray2.getDimensionPixelOffset(1, 0), localTypedArray2.getDimensionPixelOffset(2, 0), localTypedArray2.getDimensionPixelOffset(3, 0));
                        localTypedArray2.recycle();
                        this.mGradientState.mPadding = this.mPadding;
                    }
                    else
                    {
                        Log.w("drawable", "Bad element under <shape>: " + str);
                    }
                }
            }
        }
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            this.mGradientState = new GradientState(this.mGradientState);
            initializeWithState(this.mGradientState);
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        super.onBoundsChange(paramRect);
        this.mRingPath = null;
        this.mPathIsDirty = true;
        this.mRectIsDirty = true;
    }

    protected boolean onLevelChange(int paramInt)
    {
        super.onLevelChange(paramInt);
        this.mRectIsDirty = true;
        this.mPathIsDirty = true;
        invalidateSelf();
        return true;
    }

    public void setAlpha(int paramInt)
    {
        if (paramInt != this.mAlpha)
        {
            this.mAlpha = paramInt;
            invalidateSelf();
        }
    }

    public void setColor(int paramInt)
    {
        this.mGradientState.setSolidColor(paramInt);
        this.mFillPaint.setColor(paramInt);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        if (paramColorFilter != this.mColorFilter)
        {
            this.mColorFilter = paramColorFilter;
            invalidateSelf();
        }
    }

    public void setColors(int[] paramArrayOfInt)
    {
        this.mGradientState.setColors(paramArrayOfInt);
        this.mRectIsDirty = true;
        invalidateSelf();
    }

    public void setCornerRadii(float[] paramArrayOfFloat)
    {
        this.mGradientState.setCornerRadii(paramArrayOfFloat);
        this.mPathIsDirty = true;
        invalidateSelf();
    }

    public void setCornerRadius(float paramFloat)
    {
        this.mGradientState.setCornerRadius(paramFloat);
        this.mPathIsDirty = true;
        invalidateSelf();
    }

    public void setDither(boolean paramBoolean)
    {
        if (paramBoolean != this.mDither)
        {
            this.mDither = paramBoolean;
            invalidateSelf();
        }
    }

    public void setGradientCenter(float paramFloat1, float paramFloat2)
    {
        this.mGradientState.setGradientCenter(paramFloat1, paramFloat2);
        this.mRectIsDirty = true;
        invalidateSelf();
    }

    public void setGradientRadius(float paramFloat)
    {
        this.mGradientState.setGradientRadius(paramFloat);
        this.mRectIsDirty = true;
        invalidateSelf();
    }

    public void setGradientType(int paramInt)
    {
        this.mGradientState.setGradientType(paramInt);
        this.mRectIsDirty = true;
        invalidateSelf();
    }

    public void setOrientation(Orientation paramOrientation)
    {
        this.mGradientState.mOrientation = paramOrientation;
        this.mRectIsDirty = true;
        invalidateSelf();
    }

    public void setShape(int paramInt)
    {
        this.mRingPath = null;
        this.mPathIsDirty = true;
        this.mGradientState.setShape(paramInt);
        invalidateSelf();
    }

    public void setSize(int paramInt1, int paramInt2)
    {
        this.mGradientState.setSize(paramInt1, paramInt2);
        this.mPathIsDirty = true;
        invalidateSelf();
    }

    public void setStroke(int paramInt1, int paramInt2)
    {
        setStroke(paramInt1, paramInt2, 0.0F, 0.0F);
    }

    public void setStroke(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2)
    {
        this.mGradientState.setStroke(paramInt1, paramInt2, paramFloat1, paramFloat2);
        if (this.mStrokePaint == null)
        {
            this.mStrokePaint = new Paint(1);
            this.mStrokePaint.setStyle(Paint.Style.STROKE);
        }
        this.mStrokePaint.setStrokeWidth(paramInt1);
        this.mStrokePaint.setColor(paramInt2);
        DashPathEffect localDashPathEffect = null;
        if (paramFloat1 > 0.0F)
        {
            float[] arrayOfFloat = new float[2];
            arrayOfFloat[0] = paramFloat1;
            arrayOfFloat[1] = paramFloat2;
            localDashPathEffect = new DashPathEffect(arrayOfFloat, 0.0F);
        }
        this.mStrokePaint.setPathEffect(localDashPathEffect);
        invalidateSelf();
    }

    public void setUseLevel(boolean paramBoolean)
    {
        GradientState.access$002(this.mGradientState, paramBoolean);
        this.mRectIsDirty = true;
        invalidateSelf();
    }

    static final class GradientState extends Drawable.ConstantState
    {
        private float mCenterX = 0.5F;
        private float mCenterY = 0.5F;
        public int mChangingConfigurations;
        public int[] mColors;
        public int mGradient = 0;
        private float mGradientRadius = 0.5F;
        public boolean mHasSolidColor;
        public int mHeight = -1;
        public int mInnerRadius;
        public float mInnerRadiusRatio;
        public GradientDrawable.Orientation mOrientation;
        public Rect mPadding;
        public float[] mPositions;
        public float mRadius;
        public float[] mRadiusArray;
        public int mShape = 0;
        public int mSolidColor;
        public int mStrokeColor;
        public float mStrokeDashGap;
        public float mStrokeDashWidth;
        public int mStrokeWidth = -1;
        public int[] mTempColors;
        public float[] mTempPositions;
        public int mThickness;
        public float mThicknessRatio;
        private boolean mUseLevel;
        private boolean mUseLevelForShape;
        public int mWidth = -1;

        public GradientState(GradientState paramGradientState)
        {
            this.mChangingConfigurations = paramGradientState.mChangingConfigurations;
            this.mShape = paramGradientState.mShape;
            this.mGradient = paramGradientState.mGradient;
            this.mOrientation = paramGradientState.mOrientation;
            if (paramGradientState.mColors != null)
                this.mColors = ((int[])paramGradientState.mColors.clone());
            if (paramGradientState.mPositions != null)
                this.mPositions = ((float[])paramGradientState.mPositions.clone());
            this.mHasSolidColor = paramGradientState.mHasSolidColor;
            this.mSolidColor = paramGradientState.mSolidColor;
            this.mStrokeWidth = paramGradientState.mStrokeWidth;
            this.mStrokeColor = paramGradientState.mStrokeColor;
            this.mStrokeDashWidth = paramGradientState.mStrokeDashWidth;
            this.mStrokeDashGap = paramGradientState.mStrokeDashGap;
            this.mRadius = paramGradientState.mRadius;
            if (paramGradientState.mRadiusArray != null)
                this.mRadiusArray = ((float[])paramGradientState.mRadiusArray.clone());
            if (paramGradientState.mPadding != null)
                this.mPadding = new Rect(paramGradientState.mPadding);
            this.mWidth = paramGradientState.mWidth;
            this.mHeight = paramGradientState.mHeight;
            this.mInnerRadiusRatio = paramGradientState.mInnerRadiusRatio;
            this.mThicknessRatio = paramGradientState.mThicknessRatio;
            this.mInnerRadius = paramGradientState.mInnerRadius;
            this.mThickness = paramGradientState.mThickness;
            this.mCenterX = paramGradientState.mCenterX;
            this.mCenterY = paramGradientState.mCenterY;
            this.mGradientRadius = paramGradientState.mGradientRadius;
            this.mUseLevel = paramGradientState.mUseLevel;
            this.mUseLevelForShape = paramGradientState.mUseLevelForShape;
        }

        GradientState(GradientDrawable.Orientation paramOrientation, int[] paramArrayOfInt)
        {
            this.mOrientation = paramOrientation;
            this.mColors = paramArrayOfInt;
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new GradientDrawable(this, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new GradientDrawable(this, null);
        }

        public void setColors(int[] paramArrayOfInt)
        {
            this.mHasSolidColor = false;
            this.mColors = paramArrayOfInt;
        }

        public void setCornerRadii(float[] paramArrayOfFloat)
        {
            this.mRadiusArray = paramArrayOfFloat;
            if (paramArrayOfFloat == null)
                this.mRadius = 0.0F;
        }

        public void setCornerRadius(float paramFloat)
        {
            if (paramFloat < 0.0F)
                paramFloat = 0.0F;
            this.mRadius = paramFloat;
            this.mRadiusArray = null;
        }

        public void setGradientCenter(float paramFloat1, float paramFloat2)
        {
            this.mCenterX = paramFloat1;
            this.mCenterY = paramFloat2;
        }

        public void setGradientRadius(float paramFloat)
        {
            this.mGradientRadius = paramFloat;
        }

        public void setGradientType(int paramInt)
        {
            this.mGradient = paramInt;
        }

        public void setShape(int paramInt)
        {
            this.mShape = paramInt;
        }

        public void setSize(int paramInt1, int paramInt2)
        {
            this.mWidth = paramInt1;
            this.mHeight = paramInt2;
        }

        public void setSolidColor(int paramInt)
        {
            this.mHasSolidColor = true;
            this.mSolidColor = paramInt;
            this.mColors = null;
        }

        public void setStroke(int paramInt1, int paramInt2)
        {
            this.mStrokeWidth = paramInt1;
            this.mStrokeColor = paramInt2;
        }

        public void setStroke(int paramInt1, int paramInt2, float paramFloat1, float paramFloat2)
        {
            this.mStrokeWidth = paramInt1;
            this.mStrokeColor = paramInt2;
            this.mStrokeDashWidth = paramFloat1;
            this.mStrokeDashGap = paramFloat2;
        }
    }

    public static enum Orientation
    {
        static
        {
            RIGHT_LEFT = new Orientation("RIGHT_LEFT", 2);
            BR_TL = new Orientation("BR_TL", 3);
            BOTTOM_TOP = new Orientation("BOTTOM_TOP", 4);
            BL_TR = new Orientation("BL_TR", 5);
            LEFT_RIGHT = new Orientation("LEFT_RIGHT", 6);
            TL_BR = new Orientation("TL_BR", 7);
            Orientation[] arrayOfOrientation = new Orientation[8];
            arrayOfOrientation[0] = TOP_BOTTOM;
            arrayOfOrientation[1] = TR_BL;
            arrayOfOrientation[2] = RIGHT_LEFT;
            arrayOfOrientation[3] = BR_TL;
            arrayOfOrientation[4] = BOTTOM_TOP;
            arrayOfOrientation[5] = BL_TR;
            arrayOfOrientation[6] = LEFT_RIGHT;
            arrayOfOrientation[7] = TL_BR;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.GradientDrawable
 * JD-Core Version:        0.6.2
 */