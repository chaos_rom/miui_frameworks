package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.StateSet;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class StateListDrawable extends DrawableContainer
{
    private static final boolean DEBUG = false;
    private static final boolean DEFAULT_DITHER = true;
    private static final String TAG = "StateListDrawable";
    private boolean mMutated;
    private final StateListState mStateListState;

    public StateListDrawable()
    {
        this(null, null);
    }

    private StateListDrawable(StateListState paramStateListState, Resources paramResources)
    {
        StateListState localStateListState = new StateListState(paramStateListState, this, paramResources);
        this.mStateListState = localStateListState;
        setConstantState(localStateListState);
        onStateChange(getState());
    }

    public void addState(int[] paramArrayOfInt, Drawable paramDrawable)
    {
        if (paramDrawable != null)
        {
            this.mStateListState.addStateSet(paramArrayOfInt, paramDrawable);
            onStateChange(getState());
        }
    }

    public int getStateCount()
    {
        return this.mStateListState.getChildCount();
    }

    public Drawable getStateDrawable(int paramInt)
    {
        return this.mStateListState.getChildren()[paramInt];
    }

    public int getStateDrawableIndex(int[] paramArrayOfInt)
    {
        return this.mStateListState.indexOfStateSet(paramArrayOfInt);
    }

    StateListState getStateListState()
    {
        return this.mStateListState;
    }

    public int[] getStateSet(int paramInt)
    {
        return this.mStateListState.mStateSets[paramInt];
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.StateListDrawable);
        super.inflateWithAttributes(paramResources, paramXmlPullParser, localTypedArray, 1);
        this.mStateListState.setVariablePadding(localTypedArray.getBoolean(2, false));
        this.mStateListState.setConstantSize(localTypedArray.getBoolean(3, false));
        this.mStateListState.setEnterFadeDuration(localTypedArray.getInt(4, 0));
        this.mStateListState.setExitFadeDuration(localTypedArray.getInt(5, 0));
        setDither(localTypedArray.getBoolean(0, true));
        localTypedArray.recycle();
        int i = 1 + paramXmlPullParser.getDepth();
        int j;
        int k;
        do
        {
            j = paramXmlPullParser.next();
            if (j == 1)
                break;
            k = paramXmlPullParser.getDepth();
            if ((k < i) && (j == 3))
                break;
        }
        while ((j != 2) || (k > i) || (!paramXmlPullParser.getName().equals("item")));
        int m = 0;
        int n = paramAttributeSet.getAttributeCount();
        int[] arrayOfInt1 = new int[n];
        int i1 = 0;
        int i2 = 0;
        label186: int i4;
        int[] arrayOfInt2;
        if (i1 < n)
        {
            i4 = paramAttributeSet.getAttributeNameResource(i1);
            if (i4 != 0);
        }
        else
        {
            arrayOfInt2 = StateSet.trimStateSet(arrayOfInt1, i2);
            if (m == 0)
                break label313;
        }
        for (Drawable localDrawable = paramResources.getDrawable(m); ; localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet))
        {
            this.mStateListState.addStateSet(arrayOfInt2, localDrawable);
            break;
            if (i4 == 16843161)
            {
                m = paramAttributeSet.getAttributeResourceValue(i1, 0);
                i5 = i2;
                i1++;
                i2 = i5;
                break label186;
            }
            int i5 = i2 + 1;
            if (paramAttributeSet.getAttributeBooleanValue(i1, false));
            while (true)
            {
                arrayOfInt1[i2] = i4;
                break;
                i4 = -i4;
            }
            label313: int i3;
            do
                i3 = paramXmlPullParser.next();
            while (i3 == 4);
            if (i3 != 2)
                throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or " + "child tag defining a drawable");
        }
        onStateChange(getState());
    }

    public boolean isStateful()
    {
        return true;
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            int[][] arrayOfInt = this.mStateListState.mStateSets;
            int i = arrayOfInt.length;
            this.mStateListState.mStateSets = new int[i][];
            for (int j = 0; j < i; j++)
            {
                int[] arrayOfInt1 = arrayOfInt[j];
                if (arrayOfInt1 != null)
                    this.mStateListState.mStateSets[j] = ((int[])arrayOfInt1.clone());
            }
            this.mMutated = true;
        }
        return this;
    }

    protected boolean onStateChange(int[] paramArrayOfInt)
    {
        int i = this.mStateListState.indexOfStateSet(paramArrayOfInt);
        if (i < 0)
            i = this.mStateListState.indexOfStateSet(StateSet.WILD_CARD);
        if (selectDrawable(i));
        for (boolean bool = true; ; bool = super.onStateChange(paramArrayOfInt))
            return bool;
    }

    static final class StateListState extends DrawableContainer.DrawableContainerState
    {
        int[][] mStateSets;

        StateListState(StateListState paramStateListState, StateListDrawable paramStateListDrawable, Resources paramResources)
        {
            super(paramStateListDrawable, paramResources);
            if (paramStateListState != null);
            for (this.mStateSets = paramStateListState.mStateSets; ; this.mStateSets = new int[getChildren().length][])
                return;
        }

        private int indexOfStateSet(int[] paramArrayOfInt)
        {
            int[][] arrayOfInt = this.mStateSets;
            int i = getChildCount();
            int j = 0;
            if (j < i)
                if (!StateSet.stateSetMatches(arrayOfInt[j], paramArrayOfInt));
            while (true)
            {
                return j;
                j++;
                break;
                j = -1;
            }
        }

        int addStateSet(int[] paramArrayOfInt, Drawable paramDrawable)
        {
            int i = addChild(paramDrawable);
            this.mStateSets[i] = paramArrayOfInt;
            return i;
        }

        public void growArray(int paramInt1, int paramInt2)
        {
            super.growArray(paramInt1, paramInt2);
            int[][] arrayOfInt = new int[paramInt2][];
            System.arraycopy(this.mStateSets, 0, arrayOfInt, 0, paramInt1);
            this.mStateSets = arrayOfInt;
        }

        public Drawable newDrawable()
        {
            return new StateListDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new StateListDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.StateListDrawable
 * JD-Core Version:        0.6.2
 */