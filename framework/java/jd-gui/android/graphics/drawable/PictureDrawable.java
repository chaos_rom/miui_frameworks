package android.graphics.drawable;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Picture;
import android.graphics.Rect;

public class PictureDrawable extends Drawable
{
    private Picture mPicture;

    public PictureDrawable(Picture paramPicture)
    {
        this.mPicture = paramPicture;
    }

    public void draw(Canvas paramCanvas)
    {
        if (this.mPicture != null)
        {
            Rect localRect = getBounds();
            paramCanvas.save();
            paramCanvas.clipRect(localRect);
            paramCanvas.translate(localRect.left, localRect.top);
            paramCanvas.drawPicture(this.mPicture);
            paramCanvas.restore();
        }
    }

    public int getIntrinsicHeight()
    {
        if (this.mPicture != null);
        for (int i = this.mPicture.getHeight(); ; i = -1)
            return i;
    }

    public int getIntrinsicWidth()
    {
        if (this.mPicture != null);
        for (int i = this.mPicture.getWidth(); ; i = -1)
            return i;
    }

    public int getOpacity()
    {
        return -3;
    }

    public Picture getPicture()
    {
        return this.mPicture;
    }

    public void setAlpha(int paramInt)
    {
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
    }

    public void setDither(boolean paramBoolean)
    {
    }

    public void setFilterBitmap(boolean paramBoolean)
    {
    }

    public void setPicture(Picture paramPicture)
    {
        this.mPicture = paramPicture;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.PictureDrawable
 * JD-Core Version:        0.6.2
 */