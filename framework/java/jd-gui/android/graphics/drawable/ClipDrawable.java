package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Gravity;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ClipDrawable extends Drawable
    implements Drawable.Callback
{
    public static final int HORIZONTAL = 1;
    public static final int VERTICAL = 2;
    private ClipState mClipState;
    private final Rect mTmpRect = new Rect();

    ClipDrawable()
    {
        this(null, null);
    }

    private ClipDrawable(ClipState paramClipState, Resources paramResources)
    {
        this.mClipState = new ClipState(paramClipState, this, paramResources);
    }

    public ClipDrawable(Drawable paramDrawable, int paramInt1, int paramInt2)
    {
        this(null, null);
        this.mClipState.mDrawable = paramDrawable;
        this.mClipState.mGravity = paramInt1;
        this.mClipState.mOrientation = paramInt2;
        if (paramDrawable != null)
            paramDrawable.setCallback(this);
    }

    public void draw(Canvas paramCanvas)
    {
        if (this.mClipState.mDrawable.getLevel() == 0);
        while (true)
        {
            return;
            Rect localRect1 = this.mTmpRect;
            Rect localRect2 = getBounds();
            int i = getLevel();
            int j = localRect2.width();
            if ((0x1 & this.mClipState.mOrientation) != 0)
                j -= (j + 0) * (10000 - i) / 10000;
            int k = localRect2.height();
            if ((0x2 & this.mClipState.mOrientation) != 0)
                k -= (k + 0) * (10000 - i) / 10000;
            int m = getResolvedLayoutDirectionSelf();
            Gravity.apply(this.mClipState.mGravity, j, k, localRect2, localRect1, m);
            if ((j > 0) && (k > 0))
            {
                paramCanvas.save();
                paramCanvas.clipRect(localRect1);
                this.mClipState.mDrawable.draw(paramCanvas);
                paramCanvas.restore();
            }
        }
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mClipState.mChangingConfigurations | this.mClipState.mDrawable.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState()
    {
        if (this.mClipState.canConstantState())
            this.mClipState.mChangingConfigurations = getChangingConfigurations();
        for (ClipState localClipState = this.mClipState; ; localClipState = null)
            return localClipState;
    }

    public int getIntrinsicHeight()
    {
        return this.mClipState.mDrawable.getIntrinsicHeight();
    }

    public int getIntrinsicWidth()
    {
        return this.mClipState.mDrawable.getIntrinsicWidth();
    }

    public int getOpacity()
    {
        return this.mClipState.mDrawable.getOpacity();
    }

    public boolean getPadding(Rect paramRect)
    {
        return this.mClipState.mDrawable.getPadding(paramRect);
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        super.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.ClipDrawable);
        int i = localTypedArray.getInt(2, 1);
        int j = localTypedArray.getInt(0, 3);
        Drawable localDrawable = localTypedArray.getDrawable(1);
        localTypedArray.recycle();
        int k = paramXmlPullParser.getDepth();
        while (true)
        {
            int m = paramXmlPullParser.next();
            if ((m == 1) || ((m == 3) && (paramXmlPullParser.getDepth() <= k)))
                break;
            if (m == 2)
                localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet);
        }
        if (localDrawable == null)
            throw new IllegalArgumentException("No drawable specified for <clip>");
        this.mClipState.mDrawable = localDrawable;
        this.mClipState.mOrientation = i;
        this.mClipState.mGravity = j;
        localDrawable.setCallback(this);
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.invalidateDrawable(this);
    }

    public boolean isStateful()
    {
        return this.mClipState.mDrawable.isStateful();
    }

    protected void onBoundsChange(Rect paramRect)
    {
        this.mClipState.mDrawable.setBounds(paramRect);
    }

    protected boolean onLevelChange(int paramInt)
    {
        this.mClipState.mDrawable.setLevel(paramInt);
        invalidateSelf();
        return true;
    }

    protected boolean onStateChange(int[] paramArrayOfInt)
    {
        return this.mClipState.mDrawable.setState(paramArrayOfInt);
    }

    public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.scheduleDrawable(this, paramRunnable, paramLong);
    }

    public void setAlpha(int paramInt)
    {
        this.mClipState.mDrawable.setAlpha(paramInt);
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        this.mClipState.mDrawable.setColorFilter(paramColorFilter);
    }

    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mClipState.mDrawable.setVisible(paramBoolean1, paramBoolean2);
        return super.setVisible(paramBoolean1, paramBoolean2);
    }

    public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.unscheduleDrawable(this, paramRunnable);
    }

    static final class ClipState extends Drawable.ConstantState
    {
        private boolean mCanConstantState;
        int mChangingConfigurations;
        private boolean mCheckedConstantState;
        Drawable mDrawable;
        int mGravity;
        int mOrientation;

        ClipState(ClipState paramClipState, ClipDrawable paramClipDrawable, Resources paramResources)
        {
            if (paramClipState != null)
                if (paramResources == null)
                    break label62;
            label62: for (this.mDrawable = paramClipState.mDrawable.getConstantState().newDrawable(paramResources); ; this.mDrawable = paramClipState.mDrawable.getConstantState().newDrawable())
            {
                this.mDrawable.setCallback(paramClipDrawable);
                this.mOrientation = paramClipState.mOrientation;
                this.mGravity = paramClipState.mGravity;
                this.mCanConstantState = true;
                this.mCheckedConstantState = true;
                return;
            }
        }

        boolean canConstantState()
        {
            if (!this.mCheckedConstantState)
                if (this.mDrawable.getConstantState() == null)
                    break label34;
            label34: for (boolean bool = true; ; bool = false)
            {
                this.mCanConstantState = bool;
                this.mCheckedConstantState = true;
                return this.mCanConstantState;
            }
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new ClipDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new ClipDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.ClipDrawable
 * JD-Core Version:        0.6.2
 */