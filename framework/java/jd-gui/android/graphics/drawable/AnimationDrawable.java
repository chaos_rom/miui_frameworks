package android.graphics.drawable;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.SystemClock;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimationDrawable extends DrawableContainer
    implements Runnable, Animatable
{
    private AnimationState mAnimationState;
    private int mCurFrame = -1;
    private boolean mMutated;

    public AnimationDrawable()
    {
        this(null, null);
    }

    private AnimationDrawable(AnimationState paramAnimationState, Resources paramResources)
    {
        AnimationState localAnimationState = new AnimationState(paramAnimationState, this, paramResources);
        this.mAnimationState = localAnimationState;
        setConstantState(localAnimationState);
        if (paramAnimationState != null)
            setFrame(0, true, false);
    }

    private void nextFrame(boolean paramBoolean)
    {
        int i = 1 + this.mCurFrame;
        int j = this.mAnimationState.getChildCount();
        if (i >= j)
            i = 0;
        if ((!this.mAnimationState.mOneShot) || (i < j - 1));
        for (boolean bool = true; ; bool = false)
        {
            setFrame(i, paramBoolean, bool);
            return;
        }
    }

    private void setFrame(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (paramInt >= this.mAnimationState.getChildCount());
        while (true)
        {
            return;
            this.mCurFrame = paramInt;
            selectDrawable(paramInt);
            if (paramBoolean1)
                unscheduleSelf(this);
            if (paramBoolean2)
            {
                this.mCurFrame = paramInt;
                scheduleSelf(this, SystemClock.uptimeMillis() + this.mAnimationState.mDurations[paramInt]);
            }
        }
    }

    public void addFrame(Drawable paramDrawable, int paramInt)
    {
        this.mAnimationState.addFrame(paramDrawable, paramInt);
        if (this.mCurFrame < 0)
            setFrame(0, true, false);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    AnimationState getAnimationState()
    {
        return this.mAnimationState;
    }

    public int getDuration(int paramInt)
    {
        return this.mAnimationState.mDurations[paramInt];
    }

    public Drawable getFrame(int paramInt)
    {
        return this.mAnimationState.getChildren()[paramInt];
    }

    public int getNumberOfFrames()
    {
        return this.mAnimationState.getChildCount();
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray1 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AnimationDrawable);
        super.inflateWithAttributes(paramResources, paramXmlPullParser, localTypedArray1, 0);
        this.mAnimationState.setVariablePadding(localTypedArray1.getBoolean(1, false));
        AnimationState.access$102(this.mAnimationState, localTypedArray1.getBoolean(2, false));
        localTypedArray1.recycle();
        int i = 1 + paramXmlPullParser.getDepth();
        int j;
        int k;
        do
        {
            j = paramXmlPullParser.next();
            if (j == 1)
                break;
            k = paramXmlPullParser.getDepth();
            if ((k < i) && (j == 3))
                break;
        }
        while ((j != 2) || (k > i) || (!paramXmlPullParser.getName().equals("item")));
        TypedArray localTypedArray2 = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AnimationDrawableItem);
        int m = localTypedArray2.getInt(0, -1);
        if (m < 0)
            throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <item> tag requires a 'duration' attribute");
        int n = localTypedArray2.getResourceId(1, 0);
        localTypedArray2.recycle();
        if (n != 0);
        for (Drawable localDrawable = paramResources.getDrawable(n); ; localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet))
        {
            this.mAnimationState.addFrame(localDrawable, m);
            if (localDrawable == null)
                break;
            localDrawable.setCallback(this);
            break;
            int i1;
            do
                i1 = paramXmlPullParser.next();
            while (i1 == 4);
            if (i1 != 2)
                throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag" + " defining a drawable");
        }
        setFrame(0, true, false);
    }

    public boolean isOneShot()
    {
        return this.mAnimationState.mOneShot;
    }

    public boolean isRunning()
    {
        if (this.mCurFrame > -1);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            AnimationState.access$002(this.mAnimationState, (int[])this.mAnimationState.mDurations.clone());
            this.mMutated = true;
        }
        return this;
    }

    public void run()
    {
        nextFrame(false);
    }

    public void setOneShot(boolean paramBoolean)
    {
        AnimationState.access$102(this.mAnimationState, paramBoolean);
    }

    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool = super.setVisible(paramBoolean1, paramBoolean2);
        if (paramBoolean1)
            if ((bool) || (paramBoolean2))
                setFrame(0, true, true);
        while (true)
        {
            return bool;
            unscheduleSelf(this);
        }
    }

    public void start()
    {
        if (!isRunning())
            run();
    }

    public void stop()
    {
        if (isRunning())
            unscheduleSelf(this);
    }

    public void unscheduleSelf(Runnable paramRunnable)
    {
        this.mCurFrame = -1;
        super.unscheduleSelf(paramRunnable);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    protected static final class AnimationState extends DrawableContainer.DrawableContainerState
    {
        private int[] mDurations;
        private boolean mOneShot;

        AnimationState(AnimationState paramAnimationState, AnimationDrawable paramAnimationDrawable, Resources paramResources)
        {
            super(paramAnimationDrawable, paramResources);
            if (paramAnimationState != null)
                this.mDurations = paramAnimationState.mDurations;
            for (this.mOneShot = paramAnimationState.mOneShot; ; this.mOneShot = true)
            {
                return;
                this.mDurations = new int[getChildren().length];
            }
        }

        public void addFrame(Drawable paramDrawable, int paramInt)
        {
            int i = super.addChild(paramDrawable);
            this.mDurations[i] = paramInt;
        }

        public void growArray(int paramInt1, int paramInt2)
        {
            super.growArray(paramInt1, paramInt2);
            int[] arrayOfInt = new int[paramInt2];
            System.arraycopy(this.mDurations, 0, arrayOfInt, 0, paramInt1);
            this.mDurations = arrayOfInt;
        }

        public Drawable newDrawable()
        {
            return new AnimationDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new AnimationDrawable(this, paramResources, null);
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        void setDuration(int paramInt1, int paramInt2)
        {
            this.mDurations[paramInt1] = paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.AnimationDrawable
 * JD-Core Version:        0.6.2
 */