package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class MipmapDrawable extends DrawableContainer
{
    private final MipmapContainerState mMipmapContainerState;
    private boolean mMutated;

    public MipmapDrawable()
    {
        this(null, null);
    }

    private MipmapDrawable(MipmapContainerState paramMipmapContainerState, Resources paramResources)
    {
        MipmapContainerState localMipmapContainerState = new MipmapContainerState(paramMipmapContainerState, this, paramResources);
        this.mMipmapContainerState = localMipmapContainerState;
        setConstantState(localMipmapContainerState);
        onDrawableAdded();
    }

    private void onDrawableAdded()
    {
        selectDrawable(-1);
        onBoundsChange(getBounds());
    }

    public void addDrawable(Drawable paramDrawable)
    {
        if (paramDrawable != null)
        {
            this.mMipmapContainerState.addDrawable(paramDrawable);
            onDrawableAdded();
        }
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        super.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
        int i = 1 + paramXmlPullParser.getDepth();
        int j;
        int k;
        do
        {
            j = paramXmlPullParser.next();
            if (j == 1)
                break;
            k = paramXmlPullParser.getDepth();
            if ((k < i) && (j == 3))
                break;
        }
        while ((j != 2) || (k > i) || (!paramXmlPullParser.getName().equals("item")));
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.MipmapDrawableItem);
        int m = localTypedArray.getResourceId(0, 0);
        localTypedArray.recycle();
        if (m != 0);
        for (Drawable localDrawable = paramResources.getDrawable(m); ; localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet))
        {
            this.mMipmapContainerState.addDrawable(localDrawable);
            break;
            int n;
            do
                n = paramXmlPullParser.next();
            while (n == 4);
            if (n != 2)
                throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or " + "child tag defining a drawable");
        }
        onDrawableAdded();
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            MipmapContainerState.access$002(this.mMipmapContainerState, (int[])this.mMipmapContainerState.mMipmapHeights.clone());
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        selectDrawable(this.mMipmapContainerState.indexForBounds(paramRect));
        super.onBoundsChange(paramRect);
    }

    private static final class MipmapContainerState extends DrawableContainer.DrawableContainerState
    {
        private int[] mMipmapHeights;

        MipmapContainerState(MipmapContainerState paramMipmapContainerState, MipmapDrawable paramMipmapDrawable, Resources paramResources)
        {
            super(paramMipmapDrawable, paramResources);
            if (paramMipmapContainerState != null);
            for (this.mMipmapHeights = paramMipmapContainerState.mMipmapHeights; ; this.mMipmapHeights = new int[getChildren().length])
            {
                setConstantSize(true);
                return;
            }
        }

        public void addDrawable(Drawable paramDrawable)
        {
            int i = addChild(paramDrawable);
            int j = paramDrawable.getIntrinsicHeight();
            while (i > 0)
            {
                Drawable localDrawable = this.mDrawables[(i - 1)];
                int k = localDrawable.getIntrinsicHeight();
                if (j >= k)
                    break;
                this.mDrawables[i] = localDrawable;
                this.mMipmapHeights[i] = k;
                this.mDrawables[(i - 1)] = paramDrawable;
                this.mMipmapHeights[(i - 1)] = j;
                i--;
            }
        }

        protected void computeConstantSize()
        {
            int i = getChildCount();
            if (i > 0)
            {
                Drawable localDrawable1 = this.mDrawables[0];
                this.mConstantMinimumWidth = localDrawable1.getMinimumWidth();
                this.mConstantMinimumHeight = localDrawable1.getMinimumHeight();
                Drawable localDrawable2 = this.mDrawables[(i - 1)];
                this.mConstantWidth = localDrawable2.getIntrinsicWidth();
                this.mConstantHeight = localDrawable2.getIntrinsicHeight();
            }
            while (true)
            {
                this.mComputedConstantSize = true;
                return;
                this.mConstantHeight = -1;
                this.mConstantWidth = -1;
                this.mConstantMinimumHeight = 0;
                this.mConstantMinimumWidth = 0;
            }
        }

        public void growArray(int paramInt1, int paramInt2)
        {
            super.growArray(paramInt1, paramInt2);
            int[] arrayOfInt = new int[paramInt2];
            System.arraycopy(this.mMipmapHeights, 0, arrayOfInt, 0, paramInt1);
            this.mMipmapHeights = arrayOfInt;
        }

        public int indexForBounds(Rect paramRect)
        {
            int i = paramRect.height();
            int j = getChildCount();
            int k = 0;
            if (k < j)
                if (i > this.mMipmapHeights[k]);
            while (true)
            {
                return k;
                k++;
                break;
                if (j > 0)
                    k = j - 1;
                else
                    k = -1;
            }
        }

        public Drawable newDrawable()
        {
            return new MipmapDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new MipmapDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.MipmapDrawable
 * JD-Core Version:        0.6.2
 */