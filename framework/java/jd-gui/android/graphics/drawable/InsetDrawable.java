package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class InsetDrawable extends Drawable
    implements Drawable.Callback
{
    private InsetState mInsetState;
    private boolean mMutated;
    private final Rect mTmpRect = new Rect();

    InsetDrawable()
    {
        this(null, null);
    }

    public InsetDrawable(Drawable paramDrawable, int paramInt)
    {
        this(paramDrawable, paramInt, paramInt, paramInt, paramInt);
    }

    public InsetDrawable(Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this(null, null);
        this.mInsetState.mDrawable = paramDrawable;
        this.mInsetState.mInsetLeft = paramInt1;
        this.mInsetState.mInsetTop = paramInt2;
        this.mInsetState.mInsetRight = paramInt3;
        this.mInsetState.mInsetBottom = paramInt4;
        if (paramDrawable != null)
            paramDrawable.setCallback(this);
    }

    private InsetDrawable(InsetState paramInsetState, Resources paramResources)
    {
        this.mInsetState = new InsetState(paramInsetState, this, paramResources);
    }

    public void draw(Canvas paramCanvas)
    {
        this.mInsetState.mDrawable.draw(paramCanvas);
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mInsetState.mChangingConfigurations | this.mInsetState.mDrawable.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState()
    {
        if (this.mInsetState.canConstantState())
            this.mInsetState.mChangingConfigurations = getChangingConfigurations();
        for (InsetState localInsetState = this.mInsetState; ; localInsetState = null)
            return localInsetState;
    }

    public int getIntrinsicHeight()
    {
        return this.mInsetState.mDrawable.getIntrinsicHeight();
    }

    public int getIntrinsicWidth()
    {
        return this.mInsetState.mDrawable.getIntrinsicWidth();
    }

    public int getOpacity()
    {
        return this.mInsetState.mDrawable.getOpacity();
    }

    public boolean getPadding(Rect paramRect)
    {
        boolean bool1 = this.mInsetState.mDrawable.getPadding(paramRect);
        paramRect.left += this.mInsetState.mInsetLeft;
        paramRect.right += this.mInsetState.mInsetRight;
        paramRect.top += this.mInsetState.mInsetTop;
        paramRect.bottom += this.mInsetState.mInsetBottom;
        if ((bool1) || ((this.mInsetState.mInsetLeft | this.mInsetState.mInsetRight | this.mInsetState.mInsetTop | this.mInsetState.mInsetBottom) != 0));
        for (boolean bool2 = true; ; bool2 = false)
            return bool2;
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.InsetDrawable);
        super.inflateWithAttributes(paramResources, paramXmlPullParser, localTypedArray, 0);
        int i = localTypedArray.getResourceId(1, 0);
        int j = localTypedArray.getDimensionPixelOffset(2, 0);
        int k = localTypedArray.getDimensionPixelOffset(4, 0);
        int m = localTypedArray.getDimensionPixelOffset(3, 0);
        int n = localTypedArray.getDimensionPixelOffset(5, 0);
        localTypedArray.recycle();
        if (i != 0);
        for (Drawable localDrawable = paramResources.getDrawable(i); ; localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet))
        {
            if (localDrawable == null)
                Log.w("drawable", "No drawable specified for <inset>");
            this.mInsetState.mDrawable = localDrawable;
            this.mInsetState.mInsetLeft = j;
            this.mInsetState.mInsetRight = m;
            this.mInsetState.mInsetTop = k;
            this.mInsetState.mInsetBottom = n;
            if (localDrawable != null)
                localDrawable.setCallback(this);
            return;
            int i1;
            do
                i1 = paramXmlPullParser.next();
            while (i1 == 4);
            if (i1 != 2)
                throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <inset> tag requires a 'drawable' attribute or " + "child tag defining a drawable");
        }
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.invalidateDrawable(this);
    }

    public boolean isStateful()
    {
        return this.mInsetState.mDrawable.isStateful();
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            this.mInsetState.mDrawable.mutate();
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        Rect localRect = this.mTmpRect;
        localRect.set(paramRect);
        localRect.left += this.mInsetState.mInsetLeft;
        localRect.top += this.mInsetState.mInsetTop;
        localRect.right -= this.mInsetState.mInsetRight;
        localRect.bottom -= this.mInsetState.mInsetBottom;
        this.mInsetState.mDrawable.setBounds(localRect.left, localRect.top, localRect.right, localRect.bottom);
    }

    protected boolean onStateChange(int[] paramArrayOfInt)
    {
        boolean bool = this.mInsetState.mDrawable.setState(paramArrayOfInt);
        onBoundsChange(getBounds());
        return bool;
    }

    public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.scheduleDrawable(this, paramRunnable, paramLong);
    }

    public void setAlpha(int paramInt)
    {
        this.mInsetState.mDrawable.setAlpha(paramInt);
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        this.mInsetState.mDrawable.setColorFilter(paramColorFilter);
    }

    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mInsetState.mDrawable.setVisible(paramBoolean1, paramBoolean2);
        return super.setVisible(paramBoolean1, paramBoolean2);
    }

    public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.unscheduleDrawable(this, paramRunnable);
    }

    static final class InsetState extends Drawable.ConstantState
    {
        boolean mCanConstantState;
        int mChangingConfigurations;
        boolean mCheckedConstantState;
        Drawable mDrawable;
        int mInsetBottom;
        int mInsetLeft;
        int mInsetRight;
        int mInsetTop;

        InsetState(InsetState paramInsetState, InsetDrawable paramInsetDrawable, Resources paramResources)
        {
            if (paramInsetState != null)
                if (paramResources == null)
                    break label78;
            label78: for (this.mDrawable = paramInsetState.mDrawable.getConstantState().newDrawable(paramResources); ; this.mDrawable = paramInsetState.mDrawable.getConstantState().newDrawable())
            {
                this.mDrawable.setCallback(paramInsetDrawable);
                this.mInsetLeft = paramInsetState.mInsetLeft;
                this.mInsetTop = paramInsetState.mInsetTop;
                this.mInsetRight = paramInsetState.mInsetRight;
                this.mInsetBottom = paramInsetState.mInsetBottom;
                this.mCanConstantState = true;
                this.mCheckedConstantState = true;
                return;
            }
        }

        boolean canConstantState()
        {
            if (!this.mCheckedConstantState)
                if (this.mDrawable.getConstantState() == null)
                    break label34;
            label34: for (boolean bool = true; ; bool = false)
            {
                this.mCanConstantState = bool;
                this.mCheckedConstantState = true;
                return this.mCanConstantState;
            }
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new InsetDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new InsetDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.InsetDrawable
 * JD-Core Version:        0.6.2
 */