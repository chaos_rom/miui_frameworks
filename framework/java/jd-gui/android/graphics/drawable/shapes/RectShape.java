package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class RectShape extends Shape
{
    private RectF mRect = new RectF();

    public RectShape clone()
        throws CloneNotSupportedException
    {
        RectShape localRectShape = (RectShape)super.clone();
        localRectShape.mRect = new RectF(this.mRect);
        return localRectShape;
    }

    public void draw(Canvas paramCanvas, Paint paramPaint)
    {
        paramCanvas.drawRect(this.mRect, paramPaint);
    }

    protected void onResize(float paramFloat1, float paramFloat2)
    {
        this.mRect.set(0.0F, 0.0F, paramFloat1, paramFloat2);
    }

    protected final RectF rect()
    {
        return this.mRect;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.shapes.RectShape
 * JD-Core Version:        0.6.2
 */