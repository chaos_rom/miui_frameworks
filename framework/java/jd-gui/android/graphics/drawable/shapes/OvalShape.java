package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Paint;

public class OvalShape extends RectShape
{
    public void draw(Canvas paramCanvas, Paint paramPaint)
    {
        paramCanvas.drawOval(rect(), paramPaint);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.shapes.OvalShape
 * JD-Core Version:        0.6.2
 */