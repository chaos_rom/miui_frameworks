package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;

public class RoundRectShape extends RectShape
{
    private float[] mInnerRadii;
    private RectF mInnerRect;
    private RectF mInset;
    private float[] mOuterRadii;
    private Path mPath;

    public RoundRectShape(float[] paramArrayOfFloat1, RectF paramRectF, float[] paramArrayOfFloat2)
    {
        if ((paramArrayOfFloat1 != null) && (paramArrayOfFloat1.length < 8))
            throw new ArrayIndexOutOfBoundsException("outer radii must have >= 8 values");
        if ((paramArrayOfFloat2 != null) && (paramArrayOfFloat2.length < 8))
            throw new ArrayIndexOutOfBoundsException("inner radii must have >= 8 values");
        this.mOuterRadii = paramArrayOfFloat1;
        this.mInset = paramRectF;
        this.mInnerRadii = paramArrayOfFloat2;
        if (paramRectF != null)
            this.mInnerRect = new RectF();
        this.mPath = new Path();
    }

    public RoundRectShape clone()
        throws CloneNotSupportedException
    {
        RoundRectShape localRoundRectShape = (RoundRectShape)super.clone();
        float[] arrayOfFloat1;
        if (this.mOuterRadii != null)
        {
            arrayOfFloat1 = (float[])this.mOuterRadii.clone();
            localRoundRectShape.mOuterRadii = arrayOfFloat1;
            if (this.mInnerRadii == null)
                break label106;
        }
        label106: for (float[] arrayOfFloat2 = (float[])this.mInnerRadii.clone(); ; arrayOfFloat2 = null)
        {
            localRoundRectShape.mInnerRadii = arrayOfFloat2;
            localRoundRectShape.mInset = new RectF(this.mInset);
            localRoundRectShape.mInnerRect = new RectF(this.mInnerRect);
            localRoundRectShape.mPath = new Path(this.mPath);
            return localRoundRectShape;
            arrayOfFloat1 = null;
            break;
        }
    }

    public void draw(Canvas paramCanvas, Paint paramPaint)
    {
        paramCanvas.drawPath(this.mPath, paramPaint);
    }

    protected void onResize(float paramFloat1, float paramFloat2)
    {
        super.onResize(paramFloat1, paramFloat2);
        RectF localRectF = rect();
        this.mPath.reset();
        if (this.mOuterRadii != null)
        {
            this.mPath.addRoundRect(localRectF, this.mOuterRadii, Path.Direction.CW);
            if (this.mInnerRect != null)
            {
                this.mInnerRect.set(localRectF.left + this.mInset.left, localRectF.top + this.mInset.top, localRectF.right - this.mInset.right, localRectF.bottom - this.mInset.bottom);
                if ((this.mInnerRect.width() < paramFloat1) && (this.mInnerRect.height() < paramFloat2))
                {
                    if (this.mInnerRadii == null)
                        break label166;
                    this.mPath.addRoundRect(this.mInnerRect, this.mInnerRadii, Path.Direction.CCW);
                }
            }
        }
        while (true)
        {
            return;
            this.mPath.addRect(localRectF, Path.Direction.CW);
            break;
            label166: this.mPath.addRect(this.mInnerRect, Path.Direction.CCW);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.shapes.RoundRectShape
 * JD-Core Version:        0.6.2
 */