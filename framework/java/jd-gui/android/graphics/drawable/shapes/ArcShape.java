package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Paint;

public class ArcShape extends RectShape
{
    private float mStart;
    private float mSweep;

    public ArcShape(float paramFloat1, float paramFloat2)
    {
        this.mStart = paramFloat1;
        this.mSweep = paramFloat2;
    }

    public void draw(Canvas paramCanvas, Paint paramPaint)
    {
        paramCanvas.drawArc(rect(), this.mStart, this.mSweep, true, paramPaint);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.shapes.ArcShape
 * JD-Core Version:        0.6.2
 */