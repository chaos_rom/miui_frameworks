package android.graphics.drawable.shapes;

import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class Shape
    implements Cloneable
{
    private float mHeight;
    private float mWidth;

    public Shape clone()
        throws CloneNotSupportedException
    {
        return (Shape)super.clone();
    }

    public abstract void draw(Canvas paramCanvas, Paint paramPaint);

    public final float getHeight()
    {
        return this.mHeight;
    }

    public final float getWidth()
    {
        return this.mWidth;
    }

    public boolean hasAlpha()
    {
        return true;
    }

    protected void onResize(float paramFloat1, float paramFloat2)
    {
    }

    public final void resize(float paramFloat1, float paramFloat2)
    {
        if (paramFloat1 < 0.0F)
            paramFloat1 = 0.0F;
        if (paramFloat2 < 0.0F)
            paramFloat2 = 0.0F;
        if ((this.mWidth != paramFloat1) || (this.mHeight != paramFloat2))
        {
            this.mWidth = paramFloat1;
            this.mHeight = paramFloat2;
            onResize(paramFloat1, paramFloat2);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.shapes.Shape
 * JD-Core Version:        0.6.2
 */