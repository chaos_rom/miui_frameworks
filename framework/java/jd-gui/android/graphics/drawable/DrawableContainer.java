package android.graphics.drawable;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Insets;
import android.graphics.Rect;
import android.os.SystemClock;

public class DrawableContainer extends Drawable
    implements Drawable.Callback
{
    private static final boolean DEBUG = false;
    private static final boolean DEFAULT_DITHER = true;
    private static final String TAG = "DrawableContainer";
    private int mAlpha = 255;
    private Runnable mAnimationRunnable;
    private ColorFilter mColorFilter;
    private int mCurIndex = -1;
    private Drawable mCurrDrawable;
    private DrawableContainerState mDrawableContainerState;
    private long mEnterAnimationEnd;
    private long mExitAnimationEnd;
    private Drawable mLastDrawable;
    private boolean mMutated;

    void animate(boolean paramBoolean)
    {
        long l = SystemClock.uptimeMillis();
        int i = 0;
        if (this.mCurrDrawable != null)
            if (this.mEnterAnimationEnd != 0L)
            {
                if (this.mEnterAnimationEnd <= l)
                {
                    this.mCurrDrawable.setAlpha(this.mAlpha);
                    this.mEnterAnimationEnd = 0L;
                }
            }
            else
            {
                if (this.mLastDrawable == null)
                    break label218;
                if (this.mExitAnimationEnd != 0L)
                {
                    if (this.mExitAnimationEnd > l)
                        break label173;
                    this.mLastDrawable.setVisible(false, false);
                    this.mLastDrawable = null;
                    this.mExitAnimationEnd = 0L;
                }
            }
        while (true)
        {
            if ((paramBoolean) && (i != 0))
                scheduleSelf(this.mAnimationRunnable, 16L + l);
            return;
            int k = (int)(255L * (this.mEnterAnimationEnd - l)) / this.mDrawableContainerState.mEnterFadeDuration;
            this.mCurrDrawable.setAlpha((255 - k) * this.mAlpha / 255);
            i = 1;
            break;
            this.mEnterAnimationEnd = 0L;
            break;
            label173: int j = (int)(255L * (this.mExitAnimationEnd - l)) / this.mDrawableContainerState.mExitFadeDuration;
            this.mLastDrawable.setAlpha(j * this.mAlpha / 255);
            i = 1;
            continue;
            label218: this.mExitAnimationEnd = 0L;
        }
    }

    public void draw(Canvas paramCanvas)
    {
        if (this.mCurrDrawable != null)
            this.mCurrDrawable.draw(paramCanvas);
        if (this.mLastDrawable != null)
            this.mLastDrawable.draw(paramCanvas);
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mDrawableContainerState.mChangingConfigurations | this.mDrawableContainerState.mChildrenChangingConfigurations;
    }

    public Drawable.ConstantState getConstantState()
    {
        if (this.mDrawableContainerState.canConstantState())
            this.mDrawableContainerState.mChangingConfigurations = getChangingConfigurations();
        for (DrawableContainerState localDrawableContainerState = this.mDrawableContainerState; ; localDrawableContainerState = null)
            return localDrawableContainerState;
    }

    public Drawable getCurrent()
    {
        return this.mCurrDrawable;
    }

    public int getIntrinsicHeight()
    {
        int i;
        if (this.mDrawableContainerState.isConstantSize())
            i = this.mDrawableContainerState.getConstantHeight();
        while (true)
        {
            return i;
            if (this.mCurrDrawable != null)
                i = this.mCurrDrawable.getIntrinsicHeight();
            else
                i = -1;
        }
    }

    public int getIntrinsicWidth()
    {
        int i;
        if (this.mDrawableContainerState.isConstantSize())
            i = this.mDrawableContainerState.getConstantWidth();
        while (true)
        {
            return i;
            if (this.mCurrDrawable != null)
                i = this.mCurrDrawable.getIntrinsicWidth();
            else
                i = -1;
        }
    }

    public Insets getLayoutInsets()
    {
        if (this.mCurrDrawable == null);
        for (Insets localInsets = Insets.NONE; ; localInsets = this.mCurrDrawable.getLayoutInsets())
            return localInsets;
    }

    public int getMinimumHeight()
    {
        int i;
        if (this.mDrawableContainerState.isConstantSize())
            i = this.mDrawableContainerState.getConstantMinimumHeight();
        while (true)
        {
            return i;
            if (this.mCurrDrawable != null)
                i = this.mCurrDrawable.getMinimumHeight();
            else
                i = 0;
        }
    }

    public int getMinimumWidth()
    {
        int i;
        if (this.mDrawableContainerState.isConstantSize())
            i = this.mDrawableContainerState.getConstantMinimumWidth();
        while (true)
        {
            return i;
            if (this.mCurrDrawable != null)
                i = this.mCurrDrawable.getMinimumWidth();
            else
                i = 0;
        }
    }

    public int getOpacity()
    {
        if ((this.mCurrDrawable == null) || (!this.mCurrDrawable.isVisible()));
        for (int i = -2; ; i = this.mDrawableContainerState.getOpacity())
            return i;
    }

    public boolean getPadding(Rect paramRect)
    {
        Rect localRect = this.mDrawableContainerState.getConstantPadding();
        boolean bool;
        if (localRect != null)
        {
            paramRect.set(localRect);
            bool = true;
        }
        while (true)
        {
            return bool;
            if (this.mCurrDrawable != null)
                bool = this.mCurrDrawable.getPadding(paramRect);
            else
                bool = super.getPadding(paramRect);
        }
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        if ((paramDrawable == this.mCurrDrawable) && (getCallback() != null))
            getCallback().invalidateDrawable(this);
    }

    public boolean isStateful()
    {
        return this.mDrawableContainerState.isStateful();
    }

    public void jumpToCurrentState()
    {
        int i = 0;
        if (this.mLastDrawable != null)
        {
            this.mLastDrawable.jumpToCurrentState();
            this.mLastDrawable = null;
            i = 1;
        }
        if (this.mCurrDrawable != null)
        {
            this.mCurrDrawable.jumpToCurrentState();
            this.mCurrDrawable.setAlpha(this.mAlpha);
        }
        if (this.mExitAnimationEnd != 0L)
        {
            this.mExitAnimationEnd = 0L;
            i = 1;
        }
        if (this.mEnterAnimationEnd != 0L)
        {
            this.mEnterAnimationEnd = 0L;
            i = 1;
        }
        if (i != 0)
            invalidateSelf();
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            int i = this.mDrawableContainerState.getChildCount();
            Drawable[] arrayOfDrawable = this.mDrawableContainerState.getChildren();
            for (int j = 0; j < i; j++)
                if (arrayOfDrawable[j] != null)
                    arrayOfDrawable[j].mutate();
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        if (this.mLastDrawable != null)
            this.mLastDrawable.setBounds(paramRect);
        if (this.mCurrDrawable != null)
            this.mCurrDrawable.setBounds(paramRect);
    }

    protected boolean onLevelChange(int paramInt)
    {
        boolean bool;
        if (this.mLastDrawable != null)
            bool = this.mLastDrawable.setLevel(paramInt);
        while (true)
        {
            return bool;
            if (this.mCurrDrawable != null)
                bool = this.mCurrDrawable.setLevel(paramInt);
            else
                bool = false;
        }
    }

    protected boolean onStateChange(int[] paramArrayOfInt)
    {
        boolean bool;
        if (this.mLastDrawable != null)
            bool = this.mLastDrawable.setState(paramArrayOfInt);
        while (true)
        {
            return bool;
            if (this.mCurrDrawable != null)
                bool = this.mCurrDrawable.setState(paramArrayOfInt);
            else
                bool = false;
        }
    }

    public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
    {
        if ((paramDrawable == this.mCurrDrawable) && (getCallback() != null))
            getCallback().scheduleDrawable(this, paramRunnable, paramLong);
    }

    public boolean selectDrawable(int paramInt)
    {
        boolean bool = false;
        if (paramInt == this.mCurIndex)
            return bool;
        long l = SystemClock.uptimeMillis();
        label72: Drawable localDrawable;
        if (this.mDrawableContainerState.mExitFadeDuration > 0)
        {
            if (this.mLastDrawable != null)
                this.mLastDrawable.setVisible(false, false);
            if (this.mCurrDrawable != null)
            {
                this.mLastDrawable = this.mCurrDrawable;
                this.mExitAnimationEnd = (l + this.mDrawableContainerState.mExitFadeDuration);
                if ((paramInt < 0) || (paramInt >= this.mDrawableContainerState.mNumChildren))
                    break label295;
                localDrawable = this.mDrawableContainerState.mDrawables[paramInt];
                this.mCurrDrawable = localDrawable;
                this.mCurIndex = paramInt;
                if (localDrawable != null)
                {
                    if (this.mDrawableContainerState.mEnterFadeDuration <= 0)
                        break label283;
                    this.mEnterAnimationEnd = (l + this.mDrawableContainerState.mEnterFadeDuration);
                    label138: localDrawable.setVisible(isVisible(), true);
                    localDrawable.setDither(this.mDrawableContainerState.mDither);
                    localDrawable.setColorFilter(this.mColorFilter);
                    localDrawable.setState(getState());
                    localDrawable.setLevel(getLevel());
                    localDrawable.setBounds(getBounds());
                }
                label199: if ((this.mEnterAnimationEnd != 0L) || (this.mExitAnimationEnd != 0L))
                {
                    if (this.mAnimationRunnable != null)
                        break label309;
                    this.mAnimationRunnable = new Runnable()
                    {
                        public void run()
                        {
                            DrawableContainer.this.animate(true);
                            DrawableContainer.this.invalidateSelf();
                        }
                    };
                }
            }
        }
        while (true)
        {
            animate(true);
            invalidateSelf();
            bool = true;
            break;
            this.mLastDrawable = null;
            this.mExitAnimationEnd = 0L;
            break label72;
            if (this.mCurrDrawable == null)
                break label72;
            this.mCurrDrawable.setVisible(false, false);
            break label72;
            label283: localDrawable.setAlpha(this.mAlpha);
            break label138;
            label295: this.mCurrDrawable = null;
            this.mCurIndex = -1;
            break label199;
            label309: unscheduleSelf(this.mAnimationRunnable);
        }
    }

    public void setAlpha(int paramInt)
    {
        if (this.mAlpha != paramInt)
        {
            this.mAlpha = paramInt;
            if (this.mCurrDrawable != null)
            {
                if (this.mEnterAnimationEnd != 0L)
                    break label38;
                this.mCurrDrawable.setAlpha(paramInt);
            }
        }
        while (true)
        {
            return;
            label38: animate(false);
        }
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        if (this.mColorFilter != paramColorFilter)
        {
            this.mColorFilter = paramColorFilter;
            if (this.mCurrDrawable != null)
                this.mCurrDrawable.setColorFilter(paramColorFilter);
        }
    }

    protected void setConstantState(DrawableContainerState paramDrawableContainerState)
    {
        this.mDrawableContainerState = paramDrawableContainerState;
    }

    public void setDither(boolean paramBoolean)
    {
        if (this.mDrawableContainerState.mDither != paramBoolean)
        {
            this.mDrawableContainerState.mDither = paramBoolean;
            if (this.mCurrDrawable != null)
                this.mCurrDrawable.setDither(this.mDrawableContainerState.mDither);
        }
    }

    public void setEnterFadeDuration(int paramInt)
    {
        this.mDrawableContainerState.mEnterFadeDuration = paramInt;
    }

    public void setExitFadeDuration(int paramInt)
    {
        this.mDrawableContainerState.mExitFadeDuration = paramInt;
    }

    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool = super.setVisible(paramBoolean1, paramBoolean2);
        if (this.mLastDrawable != null)
            this.mLastDrawable.setVisible(paramBoolean1, paramBoolean2);
        if (this.mCurrDrawable != null)
            this.mCurrDrawable.setVisible(paramBoolean1, paramBoolean2);
        return bool;
    }

    public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
    {
        if ((paramDrawable == this.mCurrDrawable) && (getCallback() != null))
            getCallback().unscheduleDrawable(this, paramRunnable);
    }

    public static abstract class DrawableContainerState extends Drawable.ConstantState
    {
        boolean mCanConstantState;
        int mChangingConfigurations;
        boolean mCheckedConstantState;
        int mChildrenChangingConfigurations;
        boolean mComputedConstantSize = false;
        int mConstantHeight;
        int mConstantMinimumHeight;
        int mConstantMinimumWidth;
        Rect mConstantPadding = null;
        boolean mConstantSize = false;
        int mConstantWidth;
        boolean mDither = true;
        Drawable[] mDrawables;
        int mEnterFadeDuration;
        int mExitFadeDuration;
        boolean mHaveOpacity = false;
        boolean mHaveStateful = false;
        int mNumChildren;
        int mOpacity;
        final DrawableContainer mOwner;
        boolean mPaddingChecked = false;
        boolean mStateful;
        boolean mVariablePadding = false;

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        DrawableContainerState(DrawableContainerState paramDrawableContainerState, DrawableContainer paramDrawableContainer, Resources paramResources)
        {
            this.mOwner = paramDrawableContainer;
            if (paramDrawableContainerState != null)
            {
                this.mChangingConfigurations = paramDrawableContainerState.mChangingConfigurations;
                this.mChildrenChangingConfigurations = paramDrawableContainerState.mChildrenChangingConfigurations;
                Drawable[] arrayOfDrawable = paramDrawableContainerState.mDrawables;
                this.mDrawables = new Drawable[arrayOfDrawable.length];
                this.mNumChildren = paramDrawableContainerState.mNumChildren;
                int i = this.mNumChildren;
                int j = 0;
                if (j < i)
                {
                    if (paramResources != null)
                        this.mDrawables[j] = arrayOfDrawable[j].getConstantState().newDrawable(paramResources);
                    while (true)
                    {
                        this.mDrawables[j].setCallback(paramDrawableContainer);
                        j++;
                        break;
                        this.mDrawables[j] = arrayOfDrawable[j].getConstantState().newDrawable();
                    }
                }
                this.mCanConstantState = true;
                this.mCheckedConstantState = true;
                this.mVariablePadding = paramDrawableContainerState.mVariablePadding;
                if (paramDrawableContainerState.mConstantPadding != null)
                    this.mConstantPadding = new Rect(paramDrawableContainerState.mConstantPadding);
                this.mConstantSize = paramDrawableContainerState.mConstantSize;
                this.mComputedConstantSize = paramDrawableContainerState.mComputedConstantSize;
                this.mConstantWidth = paramDrawableContainerState.mConstantWidth;
                this.mConstantHeight = paramDrawableContainerState.mConstantHeight;
                this.mConstantMinimumWidth = paramDrawableContainerState.mConstantMinimumWidth;
                this.mConstantMinimumHeight = paramDrawableContainerState.mConstantMinimumHeight;
                this.mHaveOpacity = paramDrawableContainerState.mHaveOpacity;
                this.mOpacity = paramDrawableContainerState.mOpacity;
                this.mHaveStateful = paramDrawableContainerState.mHaveStateful;
                this.mStateful = paramDrawableContainerState.mStateful;
                this.mDither = paramDrawableContainerState.mDither;
                this.mEnterFadeDuration = paramDrawableContainerState.mEnterFadeDuration;
                this.mExitFadeDuration = paramDrawableContainerState.mExitFadeDuration;
            }
            while (true)
            {
                return;
                this.mDrawables = new Drawable[10];
                this.mNumChildren = 0;
                this.mCanConstantState = false;
                this.mCheckedConstantState = false;
            }
        }

        public final int addChild(Drawable paramDrawable)
        {
            int i = this.mNumChildren;
            if (i >= this.mDrawables.length)
                growArray(i, i + 10);
            paramDrawable.setVisible(false, true);
            paramDrawable.setCallback(this.mOwner);
            this.mDrawables[i] = paramDrawable;
            this.mNumChildren = (1 + this.mNumChildren);
            this.mChildrenChangingConfigurations |= paramDrawable.getChangingConfigurations();
            this.mHaveOpacity = false;
            this.mHaveStateful = false;
            this.mConstantPadding = null;
            this.mPaddingChecked = false;
            this.mComputedConstantSize = false;
            return i;
        }

        /** @deprecated */
        public boolean canConstantState()
        {
            try
            {
                int j;
                if (!this.mCheckedConstantState)
                {
                    this.mCanConstantState = true;
                    int i = this.mNumChildren;
                    j = 0;
                    if (j < i)
                    {
                        if (this.mDrawables[j].getConstantState() == null)
                            this.mCanConstantState = false;
                    }
                    else
                        this.mCheckedConstantState = true;
                }
                else
                {
                    boolean bool = this.mCanConstantState;
                    return bool;
                }
                j++;
            }
            finally
            {
            }
        }

        protected void computeConstantSize()
        {
            this.mComputedConstantSize = true;
            int i = getChildCount();
            Drawable[] arrayOfDrawable = this.mDrawables;
            this.mConstantHeight = -1;
            this.mConstantWidth = -1;
            this.mConstantMinimumHeight = 0;
            this.mConstantMinimumWidth = 0;
            for (int j = 0; j < i; j++)
            {
                Drawable localDrawable = arrayOfDrawable[j];
                int k = localDrawable.getIntrinsicWidth();
                if (k > this.mConstantWidth)
                    this.mConstantWidth = k;
                int m = localDrawable.getIntrinsicHeight();
                if (m > this.mConstantHeight)
                    this.mConstantHeight = m;
                int n = localDrawable.getMinimumWidth();
                if (n > this.mConstantMinimumWidth)
                    this.mConstantMinimumWidth = n;
                int i1 = localDrawable.getMinimumHeight();
                if (i1 > this.mConstantMinimumHeight)
                    this.mConstantMinimumHeight = i1;
            }
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public final int getChildCount()
        {
            return this.mNumChildren;
        }

        public final Drawable[] getChildren()
        {
            return this.mDrawables;
        }

        public final int getConstantHeight()
        {
            if (!this.mComputedConstantSize)
                computeConstantSize();
            return this.mConstantHeight;
        }

        public final int getConstantMinimumHeight()
        {
            if (!this.mComputedConstantSize)
                computeConstantSize();
            return this.mConstantMinimumHeight;
        }

        public final int getConstantMinimumWidth()
        {
            if (!this.mComputedConstantSize)
                computeConstantSize();
            return this.mConstantMinimumWidth;
        }

        public final Rect getConstantPadding()
        {
            Rect localRect1;
            if (this.mVariablePadding)
                localRect1 = null;
            while (true)
            {
                return localRect1;
                if ((this.mConstantPadding != null) || (this.mPaddingChecked))
                {
                    localRect1 = this.mConstantPadding;
                }
                else
                {
                    localRect1 = null;
                    Rect localRect2 = new Rect();
                    int i = getChildCount();
                    Drawable[] arrayOfDrawable = this.mDrawables;
                    for (int j = 0; j < i; j++)
                        if (arrayOfDrawable[j].getPadding(localRect2))
                        {
                            if (localRect1 == null)
                                localRect1 = new Rect(0, 0, 0, 0);
                            if (localRect2.left > localRect1.left)
                                localRect1.left = localRect2.left;
                            if (localRect2.top > localRect1.top)
                                localRect1.top = localRect2.top;
                            if (localRect2.right > localRect1.right)
                                localRect1.right = localRect2.right;
                            if (localRect2.bottom > localRect1.bottom)
                                localRect1.bottom = localRect2.bottom;
                        }
                    this.mPaddingChecked = true;
                    this.mConstantPadding = localRect1;
                }
            }
        }

        public final int getConstantWidth()
        {
            if (!this.mComputedConstantSize)
                computeConstantSize();
            return this.mConstantWidth;
        }

        public final int getEnterFadeDuration()
        {
            return this.mEnterFadeDuration;
        }

        public final int getExitFadeDuration()
        {
            return this.mExitFadeDuration;
        }

        public final int getOpacity()
        {
            int j;
            if (this.mHaveOpacity)
                j = this.mOpacity;
            while (true)
            {
                return j;
                int i = getChildCount();
                Drawable[] arrayOfDrawable = this.mDrawables;
                if (i > 0);
                for (j = arrayOfDrawable[0].getOpacity(); ; j = -2)
                    for (int k = 1; k < i; k++)
                        j = Drawable.resolveOpacity(j, arrayOfDrawable[k].getOpacity());
                this.mOpacity = j;
                this.mHaveOpacity = true;
            }
        }

        public void growArray(int paramInt1, int paramInt2)
        {
            Drawable[] arrayOfDrawable = new Drawable[paramInt2];
            System.arraycopy(this.mDrawables, 0, arrayOfDrawable, 0, paramInt1);
            this.mDrawables = arrayOfDrawable;
        }

        public final boolean isConstantSize()
        {
            return this.mConstantSize;
        }

        public final boolean isStateful()
        {
            if (this.mHaveStateful)
            {
                bool = this.mStateful;
                return bool;
            }
            boolean bool = false;
            int i = getChildCount();
            for (int j = 0; ; j++)
                if (j < i)
                {
                    if (this.mDrawables[j].isStateful())
                        bool = true;
                }
                else
                {
                    this.mStateful = bool;
                    this.mHaveStateful = true;
                    break;
                }
        }

        public final void setConstantSize(boolean paramBoolean)
        {
            this.mConstantSize = paramBoolean;
        }

        public final void setEnterFadeDuration(int paramInt)
        {
            this.mEnterFadeDuration = paramInt;
        }

        public final void setExitFadeDuration(int paramInt)
        {
            this.mExitFadeDuration = paramInt;
        }

        public final void setVariablePadding(boolean paramBoolean)
        {
            this.mVariablePadding = paramBoolean;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.DrawableContainer
 * JD-Core Version:        0.6.2
 */