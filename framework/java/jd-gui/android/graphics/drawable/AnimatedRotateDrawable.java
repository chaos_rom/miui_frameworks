package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimatedRotateDrawable extends Drawable
    implements Drawable.Callback, Runnable, Animatable
{
    private float mCurrentDegrees;
    private float mIncrement;
    private boolean mMutated;
    private boolean mRunning;
    private AnimatedRotateState mState;

    public AnimatedRotateDrawable()
    {
        this(null, null);
    }

    private AnimatedRotateDrawable(AnimatedRotateState paramAnimatedRotateState, Resources paramResources)
    {
        this.mState = new AnimatedRotateState(paramAnimatedRotateState, this, paramResources);
        init();
    }

    private void init()
    {
        AnimatedRotateState localAnimatedRotateState = this.mState;
        this.mIncrement = (360.0F / localAnimatedRotateState.mFramesCount);
        Drawable localDrawable = localAnimatedRotateState.mDrawable;
        if (localDrawable != null)
        {
            localDrawable.setFilterBitmap(true);
            if ((localDrawable instanceof BitmapDrawable))
                ((BitmapDrawable)localDrawable).setAntiAlias(true);
        }
    }

    private void nextFrame()
    {
        unscheduleSelf(this);
        scheduleSelf(this, SystemClock.uptimeMillis() + this.mState.mFrameDuration);
    }

    public void draw(Canvas paramCanvas)
    {
        int i = paramCanvas.save();
        AnimatedRotateState localAnimatedRotateState = this.mState;
        Drawable localDrawable = localAnimatedRotateState.mDrawable;
        Rect localRect = localDrawable.getBounds();
        int j = localRect.right - localRect.left;
        int k = localRect.bottom - localRect.top;
        float f1;
        if (localAnimatedRotateState.mPivotXRel)
        {
            f1 = j * localAnimatedRotateState.mPivotX;
            if (!localAnimatedRotateState.mPivotYRel)
                break label130;
        }
        label130: for (float f2 = k * localAnimatedRotateState.mPivotY; ; f2 = localAnimatedRotateState.mPivotY)
        {
            paramCanvas.rotate(this.mCurrentDegrees, f1 + localRect.left, f2 + localRect.top);
            localDrawable.draw(paramCanvas);
            paramCanvas.restoreToCount(i);
            return;
            f1 = localAnimatedRotateState.mPivotX;
            break;
        }
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mState.mChangingConfigurations | this.mState.mDrawable.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState()
    {
        if (this.mState.canConstantState())
            this.mState.mChangingConfigurations = getChangingConfigurations();
        for (AnimatedRotateState localAnimatedRotateState = this.mState; ; localAnimatedRotateState = null)
            return localAnimatedRotateState;
    }

    public Drawable getDrawable()
    {
        return this.mState.mDrawable;
    }

    public int getIntrinsicHeight()
    {
        return this.mState.mDrawable.getIntrinsicHeight();
    }

    public int getIntrinsicWidth()
    {
        return this.mState.mDrawable.getIntrinsicWidth();
    }

    public int getOpacity()
    {
        return this.mState.mDrawable.getOpacity();
    }

    public boolean getPadding(Rect paramRect)
    {
        return this.mState.mDrawable.getPadding(paramRect);
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AnimatedRotateDrawable);
        super.inflateWithAttributes(paramResources, paramXmlPullParser, localTypedArray, 0);
        TypedValue localTypedValue1 = localTypedArray.peekValue(2);
        boolean bool1;
        float f1;
        label54: TypedValue localTypedValue2;
        boolean bool2;
        if (localTypedValue1.type == 6)
        {
            bool1 = true;
            if (!bool1)
                break label241;
            f1 = localTypedValue1.getFraction(1.0F, 1.0F);
            localTypedValue2 = localTypedArray.peekValue(3);
            if (localTypedValue2.type != 6)
                break label251;
            bool2 = true;
            label75: if (!bool2)
                break label257;
        }
        Drawable localDrawable;
        label257: for (float f2 = localTypedValue2.getFraction(1.0F, 1.0F); ; f2 = localTypedValue2.getFloat())
        {
            setFramesCount(localTypedArray.getInt(5, 12));
            setFramesDuration(localTypedArray.getInt(4, 150));
            int i = localTypedArray.getResourceId(1, 0);
            localDrawable = null;
            if (i > 0)
                localDrawable = paramResources.getDrawable(i);
            localTypedArray.recycle();
            int j = paramXmlPullParser.getDepth();
            while (true)
            {
                int k = paramXmlPullParser.next();
                if ((k == 1) || ((k == 3) && (paramXmlPullParser.getDepth() <= j)))
                    break;
                if (k == 2)
                {
                    localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet);
                    if (localDrawable == null)
                        Log.w("drawable", "Bad element under <animated-rotate>: " + paramXmlPullParser.getName());
                }
            }
            bool1 = false;
            break;
            label241: f1 = localTypedValue1.getFloat();
            break label54;
            label251: bool2 = false;
            break label75;
        }
        if (localDrawable == null)
            Log.w("drawable", "No drawable specified for <animated-rotate>");
        AnimatedRotateState localAnimatedRotateState = this.mState;
        localAnimatedRotateState.mDrawable = localDrawable;
        localAnimatedRotateState.mPivotXRel = bool1;
        localAnimatedRotateState.mPivotX = f1;
        localAnimatedRotateState.mPivotYRel = bool2;
        localAnimatedRotateState.mPivotY = f2;
        init();
        if (localDrawable != null)
            localDrawable.setCallback(this);
    }

    public void invalidateDrawable(Drawable paramDrawable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.invalidateDrawable(this);
    }

    public boolean isRunning()
    {
        return this.mRunning;
    }

    public boolean isStateful()
    {
        return this.mState.mDrawable.isStateful();
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            this.mState.mDrawable.mutate();
            this.mMutated = true;
        }
        return this;
    }

    protected void onBoundsChange(Rect paramRect)
    {
        this.mState.mDrawable.setBounds(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    }

    public void run()
    {
        this.mCurrentDegrees += this.mIncrement;
        if (this.mCurrentDegrees > 360.0F - this.mIncrement)
            this.mCurrentDegrees = 0.0F;
        invalidateSelf();
        nextFrame();
    }

    public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.scheduleDrawable(this, paramRunnable, paramLong);
    }

    public void setAlpha(int paramInt)
    {
        this.mState.mDrawable.setAlpha(paramInt);
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        this.mState.mDrawable.setColorFilter(paramColorFilter);
    }

    public void setFramesCount(int paramInt)
    {
        this.mState.mFramesCount = paramInt;
        this.mIncrement = (360.0F / this.mState.mFramesCount);
    }

    public void setFramesDuration(int paramInt)
    {
        this.mState.mFrameDuration = paramInt;
    }

    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mState.mDrawable.setVisible(paramBoolean1, paramBoolean2);
        boolean bool = super.setVisible(paramBoolean1, paramBoolean2);
        if (paramBoolean1)
            if ((bool) || (paramBoolean2))
            {
                this.mCurrentDegrees = 0.0F;
                nextFrame();
            }
        while (true)
        {
            return bool;
            unscheduleSelf(this);
        }
    }

    public void start()
    {
        if (!this.mRunning)
        {
            this.mRunning = true;
            nextFrame();
        }
    }

    public void stop()
    {
        this.mRunning = false;
        unscheduleSelf(this);
    }

    public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
    {
        Drawable.Callback localCallback = getCallback();
        if (localCallback != null)
            localCallback.unscheduleDrawable(this, paramRunnable);
    }

    static final class AnimatedRotateState extends Drawable.ConstantState
    {
        private boolean mCanConstantState;
        int mChangingConfigurations;
        private boolean mCheckedConstantState;
        Drawable mDrawable;
        int mFrameDuration;
        int mFramesCount;
        float mPivotX;
        boolean mPivotXRel;
        float mPivotY;
        boolean mPivotYRel;

        public AnimatedRotateState(AnimatedRotateState paramAnimatedRotateState, AnimatedRotateDrawable paramAnimatedRotateDrawable, Resources paramResources)
        {
            if (paramAnimatedRotateState != null)
                if (paramResources == null)
                    break label94;
            label94: for (this.mDrawable = paramAnimatedRotateState.mDrawable.getConstantState().newDrawable(paramResources); ; this.mDrawable = paramAnimatedRotateState.mDrawable.getConstantState().newDrawable())
            {
                this.mDrawable.setCallback(paramAnimatedRotateDrawable);
                this.mPivotXRel = paramAnimatedRotateState.mPivotXRel;
                this.mPivotX = paramAnimatedRotateState.mPivotX;
                this.mPivotYRel = paramAnimatedRotateState.mPivotYRel;
                this.mPivotY = paramAnimatedRotateState.mPivotY;
                this.mFramesCount = paramAnimatedRotateState.mFramesCount;
                this.mFrameDuration = paramAnimatedRotateState.mFrameDuration;
                this.mCheckedConstantState = true;
                this.mCanConstantState = true;
                return;
            }
        }

        public boolean canConstantState()
        {
            if (!this.mCheckedConstantState)
                if (this.mDrawable.getConstantState() == null)
                    break label34;
            label34: for (boolean bool = true; ; bool = false)
            {
                this.mCanConstantState = bool;
                this.mCheckedConstantState = true;
                return this.mCanConstantState;
            }
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new AnimatedRotateDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new AnimatedRotateDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.AnimatedRotateDrawable
 * JD-Core Version:        0.6.2
 */