package android.graphics.drawable;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.SystemClock;

public class TransitionDrawable extends LayerDrawable
    implements Drawable.Callback
{
    private static final int TRANSITION_NONE = 2;
    private static final int TRANSITION_RUNNING = 1;
    private static final int TRANSITION_STARTING;
    private int mAlpha = 0;
    private boolean mCrossFade;
    private int mDuration;
    private int mFrom;
    private int mOriginalDuration;
    private boolean mReverse;
    private long mStartTimeMillis;
    private int mTo;
    private int mTransitionState = 2;

    TransitionDrawable()
    {
        this(new TransitionState(null, null, null), (Resources)null);
    }

    private TransitionDrawable(TransitionState paramTransitionState, Resources paramResources)
    {
        super(paramTransitionState, paramResources);
    }

    private TransitionDrawable(TransitionState paramTransitionState, Drawable[] paramArrayOfDrawable)
    {
        super(paramArrayOfDrawable, paramTransitionState);
    }

    public TransitionDrawable(Drawable[] paramArrayOfDrawable)
    {
        this(new TransitionState(null, null, null), paramArrayOfDrawable);
    }

    LayerDrawable.LayerState createConstantState(LayerDrawable.LayerState paramLayerState, Resources paramResources)
    {
        return new TransitionState((TransitionState)paramLayerState, this, paramResources);
    }

    public void draw(Canvas paramCanvas)
    {
        int i = 1;
        int j;
        boolean bool;
        LayerDrawable.ChildDrawable[] arrayOfChildDrawable;
        switch (this.mTransitionState)
        {
        default:
            j = this.mAlpha;
            bool = this.mCrossFade;
            arrayOfChildDrawable = this.mLayerState.mChildren;
            if (i != 0)
            {
                if ((!bool) || (j == 0))
                    arrayOfChildDrawable[0].mDrawable.draw(paramCanvas);
                if (j == 255)
                    arrayOfChildDrawable[1].mDrawable.draw(paramCanvas);
            }
            break;
        case 0:
        case 1:
        }
        while (true)
        {
            return;
            this.mStartTimeMillis = SystemClock.uptimeMillis();
            i = 0;
            this.mTransitionState = 1;
            break;
            if (this.mStartTimeMillis < 0L)
                break;
            float f1 = (float)(SystemClock.uptimeMillis() - this.mStartTimeMillis) / this.mDuration;
            if (f1 >= 1.0F);
            for (i = 1; ; i = 0)
            {
                float f2 = Math.min(f1, 1.0F);
                this.mAlpha = ((int)(this.mFrom + f2 * (this.mTo - this.mFrom)));
                break;
            }
            Drawable localDrawable1 = arrayOfChildDrawable[0].mDrawable;
            if (bool)
                localDrawable1.setAlpha(255 - j);
            localDrawable1.draw(paramCanvas);
            if (bool)
                localDrawable1.setAlpha(255);
            if (j > 0)
            {
                Drawable localDrawable2 = arrayOfChildDrawable[1].mDrawable;
                localDrawable2.setAlpha(j);
                localDrawable2.draw(paramCanvas);
                localDrawable2.setAlpha(255);
            }
            if (i == 0)
                invalidateSelf();
        }
    }

    public boolean isCrossFadeEnabled()
    {
        return this.mCrossFade;
    }

    public void resetTransition()
    {
        this.mAlpha = 0;
        this.mTransitionState = 2;
        invalidateSelf();
    }

    public void reverseTransition(int paramInt)
    {
        boolean bool = true;
        long l1 = SystemClock.uptimeMillis();
        if (l1 - this.mStartTimeMillis > this.mDuration)
        {
            if (this.mTo == 0)
            {
                this.mFrom = 0;
                this.mTo = 255;
                this.mAlpha = 0;
            }
            for (this.mReverse = false; ; this.mReverse = bool)
            {
                this.mOriginalDuration = paramInt;
                this.mDuration = paramInt;
                this.mTransitionState = 0;
                invalidateSelf();
                return;
                this.mFrom = 255;
                this.mTo = 0;
                this.mAlpha = 255;
            }
        }
        label104: int i;
        if (!this.mReverse)
        {
            this.mReverse = bool;
            this.mFrom = this.mAlpha;
            if (!this.mReverse)
                break label168;
            i = 0;
            label127: this.mTo = i;
            if (!this.mReverse)
                break label176;
        }
        label168: label176: for (long l2 = l1 - this.mStartTimeMillis; ; l2 = this.mOriginalDuration - (l1 - this.mStartTimeMillis))
        {
            this.mDuration = ((int)l2);
            this.mTransitionState = 0;
            break;
            bool = false;
            break label104;
            i = 255;
            break label127;
        }
    }

    public void setCrossFadeEnabled(boolean paramBoolean)
    {
        this.mCrossFade = paramBoolean;
    }

    public void startTransition(int paramInt)
    {
        this.mFrom = 0;
        this.mTo = 255;
        this.mAlpha = 0;
        this.mOriginalDuration = paramInt;
        this.mDuration = paramInt;
        this.mReverse = false;
        this.mTransitionState = 0;
        invalidateSelf();
    }

    static class TransitionState extends LayerDrawable.LayerState
    {
        TransitionState(TransitionState paramTransitionState, TransitionDrawable paramTransitionDrawable, Resources paramResources)
        {
            super(paramTransitionDrawable, paramResources);
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new TransitionDrawable(this, (Resources)null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new TransitionDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.TransitionDrawable
 * JD-Core Version:        0.6.2
 */