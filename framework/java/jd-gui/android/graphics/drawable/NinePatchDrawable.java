package android.graphics.drawable;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Insets;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Region;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.android.internal.R.styleable;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class NinePatchDrawable extends Drawable
{
    private static final boolean DEFAULT_DITHER = true;
    private int mBitmapHeight;
    private int mBitmapWidth;
    private Insets mLayoutInsets = Insets.NONE;
    private boolean mMutated;
    private NinePatch mNinePatch;
    private NinePatchState mNinePatchState;
    private Rect mPadding;
    private Paint mPaint;
    private int mTargetDensity = 160;

    NinePatchDrawable()
    {
    }

    public NinePatchDrawable(Resources paramResources, Bitmap paramBitmap, byte[] paramArrayOfByte, Rect paramRect1, Rect paramRect2, String paramString)
    {
        this(new NinePatchState(new NinePatch(paramBitmap, paramArrayOfByte, paramString), paramRect1, paramRect2), paramResources);
        this.mNinePatchState.mTargetDensity = this.mTargetDensity;
    }

    public NinePatchDrawable(Resources paramResources, Bitmap paramBitmap, byte[] paramArrayOfByte, Rect paramRect, String paramString)
    {
        this(new NinePatchState(new NinePatch(paramBitmap, paramArrayOfByte, paramString), paramRect), paramResources);
        this.mNinePatchState.mTargetDensity = this.mTargetDensity;
    }

    public NinePatchDrawable(Resources paramResources, NinePatch paramNinePatch)
    {
        this(new NinePatchState(paramNinePatch, new Rect()), paramResources);
        this.mNinePatchState.mTargetDensity = this.mTargetDensity;
    }

    @Deprecated
    public NinePatchDrawable(Bitmap paramBitmap, byte[] paramArrayOfByte, Rect paramRect, String paramString)
    {
        this(new NinePatchState(new NinePatch(paramBitmap, paramArrayOfByte, paramString), paramRect), null);
    }

    @Deprecated
    public NinePatchDrawable(NinePatch paramNinePatch)
    {
        this(new NinePatchState(paramNinePatch, new Rect()), null);
    }

    private NinePatchDrawable(NinePatchState paramNinePatchState, Resources paramResources)
    {
        setNinePatchState(paramNinePatchState, paramResources);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void computeBitmapSize()
    {
        int i = this.mNinePatch.getDensity();
        int j = this.mTargetDensity;
        if (i == j)
        {
            this.mBitmapWidth = this.mNinePatch.getWidth();
            this.mBitmapHeight = this.mNinePatch.getHeight();
            this.mLayoutInsets = this.mNinePatchState.mLayoutInsets;
            this.mPadding = this.mNinePatchState.mPadding;
        }
        while (true)
        {
            return;
            this.mBitmapWidth = Bitmap.scaleFromDensity(this.mNinePatch.getWidth(), i, j);
            this.mBitmapHeight = Bitmap.scaleFromDensity(this.mNinePatch.getHeight(), i, j);
            if ((this.mNinePatchState.mPadding != null) && (this.mPadding != null))
            {
                Rect localRect1 = this.mPadding;
                Rect localRect2 = this.mNinePatchState.mPadding;
                if (localRect1 == localRect2)
                {
                    localRect1 = new Rect(localRect2);
                    this.mPadding = localRect1;
                }
                localRect1.left = Bitmap.scaleFromDensity(localRect2.left, i, j);
                localRect1.top = Bitmap.scaleFromDensity(localRect2.top, i, j);
                localRect1.right = Bitmap.scaleFromDensity(localRect2.right, i, j);
                localRect1.bottom = Bitmap.scaleFromDensity(localRect2.bottom, i, j);
            }
            this.mLayoutInsets = scaleFromDensity(this.mNinePatchState.mLayoutInsets, i, j);
        }
    }

    private Insets scaleFromDensity(Insets paramInsets, int paramInt1, int paramInt2)
    {
        return Insets.of(Bitmap.scaleFromDensity(paramInsets.left, paramInt1, paramInt2), Bitmap.scaleFromDensity(paramInsets.top, paramInt1, paramInt2), Bitmap.scaleFromDensity(paramInsets.right, paramInt1, paramInt2), Bitmap.scaleFromDensity(paramInsets.bottom, paramInt1, paramInt2));
    }

    private void setNinePatchState(NinePatchState paramNinePatchState, Resources paramResources)
    {
        this.mNinePatchState = paramNinePatchState;
        this.mNinePatch = paramNinePatchState.mNinePatch;
        this.mPadding = paramNinePatchState.mPadding;
        if (paramResources != null);
        for (int i = paramResources.getDisplayMetrics().densityDpi; ; i = paramNinePatchState.mTargetDensity)
        {
            this.mTargetDensity = i;
            if (paramNinePatchState.mDither != true)
                setDither(paramNinePatchState.mDither);
            if (this.mNinePatch != null)
                computeBitmapSize();
            return;
        }
    }

    public void draw(Canvas paramCanvas)
    {
        this.mNinePatch.draw(paramCanvas, getBounds(), this.mPaint);
    }

    public int getChangingConfigurations()
    {
        return super.getChangingConfigurations() | this.mNinePatchState.mChangingConfigurations;
    }

    public Drawable.ConstantState getConstantState()
    {
        this.mNinePatchState.mChangingConfigurations = getChangingConfigurations();
        return this.mNinePatchState;
    }

    public int getIntrinsicHeight()
    {
        return this.mBitmapHeight;
    }

    public int getIntrinsicWidth()
    {
        return this.mBitmapWidth;
    }

    public Insets getLayoutInsets()
    {
        return this.mLayoutInsets;
    }

    public int getMinimumHeight()
    {
        return this.mBitmapHeight;
    }

    public int getMinimumWidth()
    {
        return this.mBitmapWidth;
    }

    public int getOpacity()
    {
        if ((this.mNinePatch.hasAlpha()) || ((this.mPaint != null) && (this.mPaint.getAlpha() < 255)));
        for (int i = -3; ; i = -1)
            return i;
    }

    public boolean getPadding(Rect paramRect)
    {
        paramRect.set(this.mPadding);
        return true;
    }

    public Paint getPaint()
    {
        if (this.mPaint == null)
        {
            this.mPaint = new Paint();
            this.mPaint.setDither(true);
        }
        return this.mPaint;
    }

    public Region getTransparentRegion()
    {
        return this.mNinePatch.getTransparentRegion(getBounds());
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        super.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.NinePatchDrawable);
        int i = localTypedArray.getResourceId(0, 0);
        if (i == 0)
            throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <nine-patch> requires a valid src attribute");
        boolean bool = localTypedArray.getBoolean(1, true);
        BitmapFactory.Options localOptions = new BitmapFactory.Options();
        if (bool)
            localOptions.inDither = false;
        localOptions.inScreenDensity = DisplayMetrics.DENSITY_DEVICE;
        Rect localRect1 = new Rect();
        Rect localRect2 = new Rect();
        Bitmap localBitmap = null;
        try
        {
            TypedValue localTypedValue = new TypedValue();
            InputStream localInputStream = paramResources.openRawResource(i, localTypedValue);
            localBitmap = BitmapFactory.decodeResourceStream(paramResources, localTypedValue, localInputStream, localRect1, localOptions);
            localInputStream.close();
            label159: if (localBitmap == null)
                throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <nine-patch> requires a valid src attribute");
            if (localBitmap.getNinePatchChunk() == null)
                throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <nine-patch> requires a valid 9-patch source image");
            setNinePatchState(new NinePatchState(new NinePatch(localBitmap, localBitmap.getNinePatchChunk(), "XML 9-patch"), localRect1, localRect2, bool), paramResources);
            this.mNinePatchState.mTargetDensity = this.mTargetDensity;
            localTypedArray.recycle();
            return;
        }
        catch (IOException localIOException)
        {
            break label159;
        }
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            this.mNinePatchState = new NinePatchState(this.mNinePatchState);
            this.mNinePatch = this.mNinePatchState.mNinePatch;
            this.mMutated = true;
        }
        return this;
    }

    public void setAlpha(int paramInt)
    {
        if ((this.mPaint == null) && (paramInt == 255));
        while (true)
        {
            return;
            getPaint().setAlpha(paramInt);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter paramColorFilter)
    {
        if ((this.mPaint == null) && (paramColorFilter == null));
        while (true)
        {
            return;
            getPaint().setColorFilter(paramColorFilter);
            invalidateSelf();
        }
    }

    public void setDither(boolean paramBoolean)
    {
        if ((this.mPaint == null) && (paramBoolean == true));
        while (true)
        {
            return;
            getPaint().setDither(paramBoolean);
            invalidateSelf();
        }
    }

    public void setFilterBitmap(boolean paramBoolean)
    {
        getPaint().setFilterBitmap(paramBoolean);
        invalidateSelf();
    }

    public void setTargetDensity(int paramInt)
    {
        if (paramInt != this.mTargetDensity)
        {
            if (paramInt == 0)
                paramInt = 160;
            this.mTargetDensity = paramInt;
            if (this.mNinePatch != null)
                computeBitmapSize();
            invalidateSelf();
        }
    }

    public void setTargetDensity(Canvas paramCanvas)
    {
        setTargetDensity(paramCanvas.getDensity());
    }

    public void setTargetDensity(DisplayMetrics paramDisplayMetrics)
    {
        setTargetDensity(paramDisplayMetrics.densityDpi);
    }

    private static final class NinePatchState extends Drawable.ConstantState
    {
        int mChangingConfigurations;
        final boolean mDither;
        final Insets mLayoutInsets;
        final NinePatch mNinePatch;
        final Rect mPadding;
        int mTargetDensity = 160;

        NinePatchState(NinePatch paramNinePatch, Rect paramRect)
        {
            this(paramNinePatch, paramRect, new Rect(), true);
        }

        NinePatchState(NinePatch paramNinePatch, Rect paramRect1, Rect paramRect2)
        {
            this(paramNinePatch, paramRect1, paramRect2, true);
        }

        NinePatchState(NinePatch paramNinePatch, Rect paramRect1, Rect paramRect2, boolean paramBoolean)
        {
            this.mNinePatch = paramNinePatch;
            this.mPadding = paramRect1;
            this.mLayoutInsets = Insets.of(paramRect2);
            this.mDither = paramBoolean;
        }

        NinePatchState(NinePatchState paramNinePatchState)
        {
            this.mNinePatch = new NinePatch(paramNinePatchState.mNinePatch);
            this.mPadding = paramNinePatchState.mPadding;
            this.mLayoutInsets = paramNinePatchState.mLayoutInsets;
            this.mDither = paramNinePatchState.mDither;
            this.mChangingConfigurations = paramNinePatchState.mChangingConfigurations;
            this.mTargetDensity = paramNinePatchState.mTargetDensity;
        }

        public int getChangingConfigurations()
        {
            return this.mChangingConfigurations;
        }

        public Drawable newDrawable()
        {
            return new NinePatchDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new NinePatchDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.NinePatchDrawable
 * JD-Core Version:        0.6.2
 */