package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import org.xmlpull.v1.XmlPullParser;

public class PaintDrawable extends ShapeDrawable
{
    public PaintDrawable()
    {
    }

    public PaintDrawable(int paramInt)
    {
        getPaint().setColor(paramInt);
    }

    protected boolean inflateTag(String paramString, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
    {
        if (paramString.equals("corners"))
        {
            TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.DrawableCorners);
            int i = localTypedArray.getDimensionPixelSize(0, 0);
            setCornerRadius(i);
            int j = localTypedArray.getDimensionPixelSize(1, i);
            int k = localTypedArray.getDimensionPixelSize(2, i);
            int m = localTypedArray.getDimensionPixelSize(3, i);
            int n = localTypedArray.getDimensionPixelSize(4, i);
            if ((j != i) || (k != i) || (m != i) || (n != i))
            {
                float[] arrayOfFloat = new float[8];
                arrayOfFloat[0] = j;
                arrayOfFloat[1] = j;
                arrayOfFloat[2] = k;
                arrayOfFloat[3] = k;
                arrayOfFloat[4] = m;
                arrayOfFloat[5] = m;
                arrayOfFloat[6] = n;
                arrayOfFloat[7] = n;
                setCornerRadii(arrayOfFloat);
            }
            localTypedArray.recycle();
        }
        for (boolean bool = true; ; bool = super.inflateTag(paramString, paramResources, paramXmlPullParser, paramAttributeSet))
            return bool;
    }

    public void setCornerRadii(float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat == null)
            if (getShape() != null)
                setShape(null);
        while (true)
        {
            invalidateSelf();
            return;
            setShape(new RoundRectShape(paramArrayOfFloat, null, null));
        }
    }

    public void setCornerRadius(float paramFloat)
    {
        float[] arrayOfFloat = null;
        if (paramFloat > 0.0F)
        {
            arrayOfFloat = new float[8];
            for (int i = 0; i < 8; i++)
                arrayOfFloat[i] = paramFloat;
        }
        setCornerRadii(arrayOfFloat);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.PaintDrawable
 * JD-Core Version:        0.6.2
 */