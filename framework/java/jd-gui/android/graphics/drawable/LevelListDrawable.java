package android.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.android.internal.R.styleable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class LevelListDrawable extends DrawableContainer
{
    private final LevelListState mLevelListState;
    private boolean mMutated;

    public LevelListDrawable()
    {
        this(null, null);
    }

    private LevelListDrawable(LevelListState paramLevelListState, Resources paramResources)
    {
        LevelListState localLevelListState = new LevelListState(paramLevelListState, this, paramResources);
        this.mLevelListState = localLevelListState;
        setConstantState(localLevelListState);
        onLevelChange(getLevel());
    }

    public void addLevel(int paramInt1, int paramInt2, Drawable paramDrawable)
    {
        if (paramDrawable != null)
        {
            this.mLevelListState.addLevel(paramInt1, paramInt2, paramDrawable);
            onLevelChange(getLevel());
        }
    }

    public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
        throws XmlPullParserException, IOException
    {
        super.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
        int i = 1 + paramXmlPullParser.getDepth();
        int j;
        int k;
        do
        {
            j = paramXmlPullParser.next();
            if (j == 1)
                break;
            k = paramXmlPullParser.getDepth();
            if ((k < i) && (j == 3))
                break;
        }
        while ((j != 2) || (k > i) || (!paramXmlPullParser.getName().equals("item")));
        TypedArray localTypedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.LevelListDrawableItem);
        int m = localTypedArray.getInt(1, 0);
        int n = localTypedArray.getInt(2, 0);
        int i1 = localTypedArray.getResourceId(0, 0);
        localTypedArray.recycle();
        if (n < 0)
            throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <item> tag requires a 'maxLevel' attribute");
        if (i1 != 0);
        for (Drawable localDrawable = paramResources.getDrawable(i1); ; localDrawable = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet))
        {
            this.mLevelListState.addLevel(m, n, localDrawable);
            break;
            int i2;
            do
                i2 = paramXmlPullParser.next();
            while (i2 == 4);
            if (i2 != 2)
                throw new XmlPullParserException(paramXmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or " + "child tag defining a drawable");
        }
        onLevelChange(getLevel());
    }

    public Drawable mutate()
    {
        if ((!this.mMutated) && (super.mutate() == this))
        {
            LevelListState.access$002(this.mLevelListState, (int[])this.mLevelListState.mLows.clone());
            LevelListState.access$102(this.mLevelListState, (int[])this.mLevelListState.mHighs.clone());
            this.mMutated = true;
        }
        return this;
    }

    protected boolean onLevelChange(int paramInt)
    {
        if (selectDrawable(this.mLevelListState.indexOfLevel(paramInt)));
        for (boolean bool = true; ; bool = super.onLevelChange(paramInt))
            return bool;
    }

    private static final class LevelListState extends DrawableContainer.DrawableContainerState
    {
        private int[] mHighs;
        private int[] mLows;

        LevelListState(LevelListState paramLevelListState, LevelListDrawable paramLevelListDrawable, Resources paramResources)
        {
            super(paramLevelListDrawable, paramResources);
            if (paramLevelListState != null)
                this.mLows = paramLevelListState.mLows;
            for (this.mHighs = paramLevelListState.mHighs; ; this.mHighs = new int[getChildren().length])
            {
                return;
                this.mLows = new int[getChildren().length];
            }
        }

        public void addLevel(int paramInt1, int paramInt2, Drawable paramDrawable)
        {
            int i = addChild(paramDrawable);
            this.mLows[i] = paramInt1;
            this.mHighs[i] = paramInt2;
        }

        public void growArray(int paramInt1, int paramInt2)
        {
            super.growArray(paramInt1, paramInt2);
            int[] arrayOfInt1 = new int[paramInt2];
            System.arraycopy(this.mLows, 0, arrayOfInt1, 0, paramInt1);
            this.mLows = arrayOfInt1;
            int[] arrayOfInt2 = new int[paramInt2];
            System.arraycopy(this.mHighs, 0, arrayOfInt2, 0, paramInt1);
            this.mHighs = arrayOfInt2;
        }

        public int indexOfLevel(int paramInt)
        {
            int[] arrayOfInt1 = this.mLows;
            int[] arrayOfInt2 = this.mHighs;
            int i = getChildCount();
            int j = 0;
            if (j < i)
                if ((paramInt < arrayOfInt1[j]) || (paramInt > arrayOfInt2[j]));
            while (true)
            {
                return j;
                j++;
                break;
                j = -1;
            }
        }

        public Drawable newDrawable()
        {
            return new LevelListDrawable(this, null, null);
        }

        public Drawable newDrawable(Resources paramResources)
        {
            return new LevelListDrawable(this, paramResources, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.drawable.LevelListDrawable
 * JD-Core Version:        0.6.2
 */