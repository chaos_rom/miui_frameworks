package android.graphics;

public class PorterDuffXfermode extends Xfermode
{
    public final PorterDuff.Mode mode;

    public PorterDuffXfermode(PorterDuff.Mode paramMode)
    {
        this.mode = paramMode;
        this.native_instance = nativeCreateXfermode(paramMode.nativeInt);
    }

    private static native int nativeCreateXfermode(int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.graphics.PorterDuffXfermode
 * JD-Core Version:        0.6.2
 */