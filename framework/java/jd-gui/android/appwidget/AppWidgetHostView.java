package android.appwidget;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Filter;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RemoteViews;
import android.widget.RemoteViews.RemoteView;
import android.widget.RemoteViewsAdapter.RemoteAdapterConnectionCallback;
import android.widget.TextView;

public class AppWidgetHostView extends FrameLayout
{
    static final boolean CROSSFADE = false;
    static final int FADE_DURATION = 1000;
    static final boolean LOGD = false;
    static final String TAG = "AppWidgetHostView";
    static final int VIEW_MODE_CONTENT = 1;
    static final int VIEW_MODE_DEFAULT = 3;
    static final int VIEW_MODE_ERROR = 2;
    static final int VIEW_MODE_NOINIT;
    static final LayoutInflater.Filter sInflaterFilter = new LayoutInflater.Filter()
    {
        public boolean onLoadClass(Class paramAnonymousClass)
        {
            return paramAnonymousClass.isAnnotationPresent(RemoteViews.RemoteView.class);
        }
    };
    int mAppWidgetId;
    Context mContext;
    long mFadeStartTime = -1L;
    AppWidgetProviderInfo mInfo;
    int mLayoutId = -1;
    Bitmap mOld;
    Paint mOldPaint = new Paint();
    Context mRemoteContext;
    View mView;
    int mViewMode = 0;

    public AppWidgetHostView(Context paramContext)
    {
        this(paramContext, 17432576, 17432577);
    }

    public AppWidgetHostView(Context paramContext, int paramInt1, int paramInt2)
    {
        super(paramContext);
        this.mContext = paramContext;
        setIsRootNamespace(true);
    }

    private int generateId()
    {
        int i = getId();
        if (i == -1)
            i = this.mAppWidgetId;
        return i;
    }

    public static Rect getDefaultPaddingForWidget(Context paramContext, ComponentName paramComponentName, Rect paramRect)
    {
        PackageManager localPackageManager = paramContext.getPackageManager();
        if (paramRect == null)
            paramRect = new Rect(0, 0, 0, 0);
        try
        {
            while (true)
            {
                ApplicationInfo localApplicationInfo = localPackageManager.getApplicationInfo(paramComponentName.getPackageName(), 0);
                if (localApplicationInfo.targetSdkVersion >= 14)
                {
                    Resources localResources = paramContext.getResources();
                    paramRect.left = localResources.getDimensionPixelSize(17104971);
                    paramRect.right = localResources.getDimensionPixelSize(17104973);
                    paramRect.top = localResources.getDimensionPixelSize(17104972);
                    paramRect.bottom = localResources.getDimensionPixelSize(17104974);
                }
                label92: return paramRect;
                paramRect.set(0, 0, 0, 0);
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            break label92;
        }
    }

    private Context getRemoteContext(RemoteViews paramRemoteViews)
    {
        String str = paramRemoteViews.getPackage();
        Object localObject;
        if (str == null)
            localObject = this.mContext;
        while (true)
        {
            return localObject;
            try
            {
                Context localContext = this.mContext.createPackageContext(str, 4);
                localObject = localContext;
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                Log.e("AppWidgetHostView", "Package name " + str + " not found");
                localObject = this.mContext;
            }
        }
    }

    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray)
    {
        Parcelable localParcelable = (Parcelable)paramSparseArray.get(generateId());
        ParcelableSparseArray localParcelableSparseArray = null;
        if ((localParcelable != null) && ((localParcelable instanceof ParcelableSparseArray)))
            localParcelableSparseArray = (ParcelableSparseArray)localParcelable;
        if (localParcelableSparseArray == null)
            localParcelableSparseArray = new ParcelableSparseArray(null);
        super.dispatchRestoreInstanceState(localParcelableSparseArray);
    }

    protected void dispatchSaveInstanceState(SparseArray<Parcelable> paramSparseArray)
    {
        ParcelableSparseArray localParcelableSparseArray = new ParcelableSparseArray(null);
        super.dispatchSaveInstanceState(localParcelableSparseArray);
        paramSparseArray.put(generateId(), localParcelableSparseArray);
    }

    protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
    {
        return super.drawChild(paramCanvas, paramView, paramLong);
    }

    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        if (this.mRemoteContext != null);
        for (Context localContext = this.mRemoteContext; ; localContext = this.mContext)
            return new FrameLayout.LayoutParams(localContext, paramAttributeSet);
    }

    public int getAppWidgetId()
    {
        return this.mAppWidgetId;
    }

    public AppWidgetProviderInfo getAppWidgetInfo()
    {
        return this.mInfo;
    }

    protected View getDefaultView()
    {
        Object localObject1 = null;
        Object localObject2 = null;
        try
        {
            if (this.mInfo != null)
            {
                Context localContext = this.mContext.createPackageContext(this.mInfo.provider.getPackageName(), 4);
                this.mRemoteContext = localContext;
                LayoutInflater localLayoutInflater = ((LayoutInflater)localContext.getSystemService("layout_inflater")).cloneInContext(localContext);
                localLayoutInflater.setFilter(sInflaterFilter);
                View localView = localLayoutInflater.inflate(this.mInfo.initialLayout, this, false);
                localObject1 = localView;
            }
            while (true)
            {
                if (localObject2 != null)
                    Log.w("AppWidgetHostView", "Error inflating AppWidget " + this.mInfo + ": " + localObject2.toString());
                if (localObject1 == null)
                    localObject1 = getErrorView();
                return localObject1;
                Log.w("AppWidgetHostView", "can't inflate defaultView because mInfo is missing");
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
                localObject2 = localNameNotFoundException;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                localObject2 = localRuntimeException;
        }
    }

    protected View getErrorView()
    {
        TextView localTextView = new TextView(this.mContext);
        localTextView.setText(17040484);
        localTextView.setBackgroundColor(Color.argb(127, 0, 0, 0));
        return localTextView;
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
        super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
        paramAccessibilityNodeInfo.setClassName(AppWidgetHostView.class.getName());
    }

    protected void prepareView(View paramView)
    {
        FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams)paramView.getLayoutParams();
        if (localLayoutParams == null)
            localLayoutParams = new FrameLayout.LayoutParams(-1, -1);
        localLayoutParams.gravity = 17;
        paramView.setLayoutParams(localLayoutParams);
    }

    void resetAppWidget(AppWidgetProviderInfo paramAppWidgetProviderInfo)
    {
        this.mInfo = paramAppWidgetProviderInfo;
        this.mViewMode = 0;
        updateAppWidget(null);
    }

    public void setAppWidget(int paramInt, AppWidgetProviderInfo paramAppWidgetProviderInfo)
    {
        this.mAppWidgetId = paramInt;
        this.mInfo = paramAppWidgetProviderInfo;
        if (paramAppWidgetProviderInfo != null)
        {
            Rect localRect = getDefaultPaddingForWidget(this.mContext, paramAppWidgetProviderInfo.provider, null);
            setPadding(localRect.left, localRect.top, localRect.right, localRect.bottom);
        }
    }

    public void updateAppWidget(RemoteViews paramRemoteViews)
    {
        int i = 0;
        Object localObject1 = null;
        Object localObject2 = null;
        if (paramRemoteViews == null)
        {
            if (this.mViewMode == 3)
                return;
            localObject1 = getDefaultView();
            this.mLayoutId = -1;
            this.mViewMode = 3;
        }
        while (true)
        {
            if (localObject1 == null)
            {
                if (this.mViewMode == 2)
                    break;
                Log.w("AppWidgetHostView", "updateAppWidget couldn't find any view, using error view", (Throwable)localObject2);
                localObject1 = getErrorView();
                this.mViewMode = 2;
            }
            if (i == 0)
            {
                prepareView((View)localObject1);
                addView((View)localObject1);
            }
            if (this.mView == localObject1)
                break;
            removeView(this.mView);
            this.mView = ((View)localObject1);
            break;
            this.mRemoteContext = getRemoteContext(paramRemoteViews);
            int j = paramRemoteViews.getLayoutId();
            if ((0 == 0) && (j == this.mLayoutId));
            try
            {
                paramRemoteViews.reapply(this.mContext, this.mView);
                localObject1 = this.mView;
                i = 1;
                if (localObject1 != null);
            }
            catch (RuntimeException localRuntimeException2)
            {
                try
                {
                    View localView = paramRemoteViews.apply(this.mContext, this);
                    localObject1 = localView;
                    this.mLayoutId = j;
                    this.mViewMode = 1;
                    continue;
                    localRuntimeException2 = localRuntimeException2;
                    localObject2 = localRuntimeException2;
                }
                catch (RuntimeException localRuntimeException1)
                {
                    while (true)
                        localObject2 = localRuntimeException1;
                }
            }
        }
    }

    public void updateAppWidgetOptions(Bundle paramBundle)
    {
        AppWidgetManager.getInstance(this.mContext).updateAppWidgetOptions(this.mAppWidgetId, paramBundle);
    }

    public void updateAppWidgetSize(Bundle paramBundle, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (paramBundle == null)
            paramBundle = new Bundle();
        Rect localRect = new Rect();
        if (this.mInfo != null)
            localRect = getDefaultPaddingForWidget(this.mContext, this.mInfo.provider, localRect);
        float f = getResources().getDisplayMetrics().density;
        int i = (int)((localRect.left + localRect.right) / f);
        int j = (int)((localRect.top + localRect.bottom) / f);
        paramBundle.putInt("appWidgetMinWidth", paramInt1 - i);
        paramBundle.putInt("appWidgetMinHeight", paramInt2 - j);
        paramBundle.putInt("appWidgetMaxWidth", paramInt3 - i);
        paramBundle.putInt("appWidgetMaxHeight", paramInt4 - j);
        updateAppWidgetOptions(paramBundle);
    }

    void viewDataChanged(int paramInt)
    {
        View localView = findViewById(paramInt);
        AdapterView localAdapterView;
        Adapter localAdapter;
        if ((localView != null) && ((localView instanceof AdapterView)))
        {
            localAdapterView = (AdapterView)localView;
            localAdapter = localAdapterView.getAdapter();
            if (!(localAdapter instanceof BaseAdapter))
                break label45;
            ((BaseAdapter)localAdapter).notifyDataSetChanged();
        }
        while (true)
        {
            return;
            label45: if ((localAdapter == null) && ((localAdapterView instanceof RemoteViewsAdapter.RemoteAdapterConnectionCallback)))
                ((RemoteViewsAdapter.RemoteAdapterConnectionCallback)localAdapterView).deferNotifyDataSetChanged();
        }
    }

    private static class ParcelableSparseArray extends SparseArray<Parcelable>
        implements Parcelable
    {
        public static final Parcelable.Creator<ParcelableSparseArray> CREATOR = new Parcelable.Creator()
        {
            public AppWidgetHostView.ParcelableSparseArray createFromParcel(Parcel paramAnonymousParcel)
            {
                AppWidgetHostView.ParcelableSparseArray localParcelableSparseArray = new AppWidgetHostView.ParcelableSparseArray(null);
                ClassLoader localClassLoader = localParcelableSparseArray.getClass().getClassLoader();
                int i = paramAnonymousParcel.readInt();
                for (int j = 0; j < i; j++)
                    localParcelableSparseArray.put(paramAnonymousParcel.readInt(), paramAnonymousParcel.readParcelable(localClassLoader));
                return localParcelableSparseArray;
            }

            public AppWidgetHostView.ParcelableSparseArray[] newArray(int paramAnonymousInt)
            {
                return new AppWidgetHostView.ParcelableSparseArray[paramAnonymousInt];
            }
        };

        public int describeContents()
        {
            return 0;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            int i = size();
            paramParcel.writeInt(i);
            for (int j = 0; j < i; j++)
            {
                paramParcel.writeInt(keyAt(j));
                paramParcel.writeParcelable((Parcelable)valueAt(j), 0);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.appwidget.AppWidgetHostView
 * JD-Core Version:        0.6.2
 */