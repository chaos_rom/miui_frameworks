package android.appwidget;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.RemoteViews;
import com.android.internal.appwidget.IAppWidgetHost.Stub;
import com.android.internal.appwidget.IAppWidgetService;
import com.android.internal.appwidget.IAppWidgetService.Stub;
import java.util.ArrayList;
import java.util.HashMap;

public class AppWidgetHost
{
    static final int HANDLE_PROVIDER_CHANGED = 2;
    static final int HANDLE_UPDATE = 1;
    static final int HANDLE_VIEW_DATA_CHANGED = 3;
    static IAppWidgetService sService;
    static final Object sServiceLock = new Object();
    Callbacks mCallbacks = new Callbacks();
    Context mContext;
    private DisplayMetrics mDisplayMetrics;
    Handler mHandler;
    int mHostId;
    String mPackageName;
    final HashMap<Integer, AppWidgetHostView> mViews = new HashMap();

    public AppWidgetHost(Context paramContext, int paramInt)
    {
        this.mContext = paramContext;
        this.mHostId = paramInt;
        this.mHandler = new UpdateHandler(paramContext.getMainLooper());
        this.mDisplayMetrics = paramContext.getResources().getDisplayMetrics();
        synchronized (sServiceLock)
        {
            if (sService == null)
                sService = IAppWidgetService.Stub.asInterface(ServiceManager.getService("appwidget"));
            return;
        }
    }

    public static void deleteAllHosts()
    {
        try
        {
            sService.deleteAllHosts();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public int allocateAppWidgetId()
    {
        try
        {
            if (this.mPackageName == null)
                this.mPackageName = this.mContext.getPackageName();
            int i = sService.allocateAppWidgetId(this.mPackageName, this.mHostId);
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    protected void clearViews()
    {
        this.mViews.clear();
    }

    public final AppWidgetHostView createView(Context paramContext, int paramInt, AppWidgetProviderInfo paramAppWidgetProviderInfo)
    {
        AppWidgetHostView localAppWidgetHostView = onCreateView(paramContext, paramInt, paramAppWidgetProviderInfo);
        localAppWidgetHostView.setAppWidget(paramInt, paramAppWidgetProviderInfo);
        synchronized (this.mViews)
        {
            this.mViews.put(Integer.valueOf(paramInt), localAppWidgetHostView);
        }
        try
        {
            RemoteViews localRemoteViews = sService.getAppWidgetViews(paramInt);
            localAppWidgetHostView.updateAppWidget(localRemoteViews);
            return localAppWidgetHostView;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void deleteAppWidgetId(int paramInt)
    {
        synchronized (this.mViews)
        {
            this.mViews.remove(Integer.valueOf(paramInt));
            try
            {
                sService.deleteAppWidgetId(paramInt);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                throw new RuntimeException("system server dead?", localRemoteException);
            }
        }
    }

    public void deleteHost()
    {
        try
        {
            sService.deleteHost(this.mHostId);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    protected AppWidgetHostView onCreateView(Context paramContext, int paramInt, AppWidgetProviderInfo paramAppWidgetProviderInfo)
    {
        return new AppWidgetHostView(paramContext);
    }

    protected void onProviderChanged(int paramInt, AppWidgetProviderInfo paramAppWidgetProviderInfo)
    {
        paramAppWidgetProviderInfo.minWidth = TypedValue.complexToDimensionPixelSize(paramAppWidgetProviderInfo.minWidth, this.mDisplayMetrics);
        paramAppWidgetProviderInfo.minHeight = TypedValue.complexToDimensionPixelSize(paramAppWidgetProviderInfo.minHeight, this.mDisplayMetrics);
        paramAppWidgetProviderInfo.minResizeWidth = TypedValue.complexToDimensionPixelSize(paramAppWidgetProviderInfo.minResizeWidth, this.mDisplayMetrics);
        paramAppWidgetProviderInfo.minResizeHeight = TypedValue.complexToDimensionPixelSize(paramAppWidgetProviderInfo.minResizeHeight, this.mDisplayMetrics);
        synchronized (this.mViews)
        {
            AppWidgetHostView localAppWidgetHostView = (AppWidgetHostView)this.mViews.get(Integer.valueOf(paramInt));
            if (localAppWidgetHostView != null)
                localAppWidgetHostView.resetAppWidget(paramAppWidgetProviderInfo);
            return;
        }
    }

    public void startListening()
    {
        ArrayList localArrayList = new ArrayList();
        try
        {
            if (this.mPackageName == null)
                this.mPackageName = this.mContext.getPackageName();
            int[] arrayOfInt = sService.startListening(this.mCallbacks, this.mPackageName, this.mHostId, localArrayList);
            int i = arrayOfInt.length;
            for (int j = 0; j < i; j++)
                updateAppWidgetView(arrayOfInt[j], (RemoteViews)localArrayList.get(j));
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void stopListening()
    {
        try
        {
            sService.stopListening(this.mHostId);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    void updateAppWidgetView(int paramInt, RemoteViews paramRemoteViews)
    {
        synchronized (this.mViews)
        {
            AppWidgetHostView localAppWidgetHostView = (AppWidgetHostView)this.mViews.get(Integer.valueOf(paramInt));
            if (localAppWidgetHostView != null)
                localAppWidgetHostView.updateAppWidget(paramRemoteViews);
            return;
        }
    }

    void viewDataChanged(int paramInt1, int paramInt2)
    {
        synchronized (this.mViews)
        {
            AppWidgetHostView localAppWidgetHostView = (AppWidgetHostView)this.mViews.get(Integer.valueOf(paramInt1));
            if (localAppWidgetHostView != null)
                localAppWidgetHostView.viewDataChanged(paramInt2);
            return;
        }
    }

    class UpdateHandler extends Handler
    {
        public UpdateHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                AppWidgetHost.this.updateAppWidgetView(paramMessage.arg1, (RemoteViews)paramMessage.obj);
                continue;
                AppWidgetHost.this.onProviderChanged(paramMessage.arg1, (AppWidgetProviderInfo)paramMessage.obj);
                continue;
                AppWidgetHost.this.viewDataChanged(paramMessage.arg1, paramMessage.arg2);
            }
        }
    }

    class Callbacks extends IAppWidgetHost.Stub
    {
        Callbacks()
        {
        }

        public void providerChanged(int paramInt, AppWidgetProviderInfo paramAppWidgetProviderInfo)
        {
            Message localMessage = AppWidgetHost.this.mHandler.obtainMessage(2);
            localMessage.arg1 = paramInt;
            localMessage.obj = paramAppWidgetProviderInfo;
            localMessage.sendToTarget();
        }

        public void updateAppWidget(int paramInt, RemoteViews paramRemoteViews)
        {
            Message localMessage = AppWidgetHost.this.mHandler.obtainMessage(1);
            localMessage.arg1 = paramInt;
            localMessage.obj = paramRemoteViews;
            localMessage.sendToTarget();
        }

        public void viewDataChanged(int paramInt1, int paramInt2)
        {
            Message localMessage = AppWidgetHost.this.mHandler.obtainMessage(3);
            localMessage.arg1 = paramInt1;
            localMessage.arg2 = paramInt2;
            localMessage.sendToTarget();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.appwidget.AppWidgetHost
 * JD-Core Version:        0.6.2
 */