package android.appwidget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class AppWidgetProvider extends BroadcastReceiver
{
    public void onAppWidgetOptionsChanged(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, Bundle paramBundle)
    {
    }

    public void onDeleted(Context paramContext, int[] paramArrayOfInt)
    {
    }

    public void onDisabled(Context paramContext)
    {
    }

    public void onEnabled(Context paramContext)
    {
    }

    public void onReceive(Context paramContext, Intent paramIntent)
    {
        String str = paramIntent.getAction();
        if ("android.appwidget.action.APPWIDGET_UPDATE".equals(str))
        {
            Bundle localBundle4 = paramIntent.getExtras();
            if (localBundle4 != null)
            {
                int[] arrayOfInt2 = localBundle4.getIntArray("appWidgetIds");
                if ((arrayOfInt2 != null) && (arrayOfInt2.length > 0))
                    onUpdate(paramContext, AppWidgetManager.getInstance(paramContext), arrayOfInt2);
            }
        }
        while (true)
        {
            return;
            if ("android.appwidget.action.APPWIDGET_DELETED".equals(str))
            {
                Bundle localBundle3 = paramIntent.getExtras();
                if ((localBundle3 != null) && (localBundle3.containsKey("appWidgetId")))
                {
                    int j = localBundle3.getInt("appWidgetId");
                    int[] arrayOfInt1 = new int[1];
                    arrayOfInt1[0] = j;
                    onDeleted(paramContext, arrayOfInt1);
                }
            }
            else if ("android.appwidget.action.APPWIDGET_UPDATE_OPTIONS".equals(str))
            {
                Bundle localBundle1 = paramIntent.getExtras();
                if ((localBundle1 != null) && (localBundle1.containsKey("appWidgetId")) && (localBundle1.containsKey("appWidgetOptions")))
                {
                    int i = localBundle1.getInt("appWidgetId");
                    Bundle localBundle2 = localBundle1.getBundle("appWidgetOptions");
                    onAppWidgetOptionsChanged(paramContext, AppWidgetManager.getInstance(paramContext), i, localBundle2);
                }
            }
            else if ("android.appwidget.action.APPWIDGET_ENABLED".equals(str))
            {
                onEnabled(paramContext);
            }
            else if ("android.appwidget.action.APPWIDGET_DISABLED".equals(str))
            {
                onDisabled(paramContext);
            }
        }
    }

    public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.appwidget.AppWidgetProvider
 * JD-Core Version:        0.6.2
 */