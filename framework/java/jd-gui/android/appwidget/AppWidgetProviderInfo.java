package android.appwidget;

import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class AppWidgetProviderInfo
    implements Parcelable
{
    public static final Parcelable.Creator<AppWidgetProviderInfo> CREATOR = new Parcelable.Creator()
    {
        public AppWidgetProviderInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new AppWidgetProviderInfo(paramAnonymousParcel);
        }

        public AppWidgetProviderInfo[] newArray(int paramAnonymousInt)
        {
            return new AppWidgetProviderInfo[paramAnonymousInt];
        }
    };
    public static final int RESIZE_BOTH = 3;
    public static final int RESIZE_HORIZONTAL = 1;
    public static final int RESIZE_NONE = 0;
    public static final int RESIZE_VERTICAL = 2;
    public int autoAdvanceViewId;
    public ComponentName configure;
    public int icon;
    public int initialLayout;
    public String label;
    public int minHeight;
    public int minResizeHeight;
    public int minResizeWidth;
    public int minWidth;
    public int previewImage;
    public ComponentName provider;
    public int resizeMode;
    public int updatePeriodMillis;

    public AppWidgetProviderInfo()
    {
    }

    public AppWidgetProviderInfo(Parcel paramParcel)
    {
        if (paramParcel.readInt() != 0)
            this.provider = new ComponentName(paramParcel);
        this.minWidth = paramParcel.readInt();
        this.minHeight = paramParcel.readInt();
        this.minResizeWidth = paramParcel.readInt();
        this.minResizeHeight = paramParcel.readInt();
        this.updatePeriodMillis = paramParcel.readInt();
        this.initialLayout = paramParcel.readInt();
        if (paramParcel.readInt() != 0)
            this.configure = new ComponentName(paramParcel);
        this.label = paramParcel.readString();
        this.icon = paramParcel.readInt();
        this.previewImage = paramParcel.readInt();
        this.autoAdvanceViewId = paramParcel.readInt();
        this.resizeMode = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        return "AppWidgetProviderInfo(provider=" + this.provider + ")";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (this.provider != null)
        {
            paramParcel.writeInt(1);
            this.provider.writeToParcel(paramParcel, paramInt);
            paramParcel.writeInt(this.minWidth);
            paramParcel.writeInt(this.minHeight);
            paramParcel.writeInt(this.minResizeWidth);
            paramParcel.writeInt(this.minResizeHeight);
            paramParcel.writeInt(this.updatePeriodMillis);
            paramParcel.writeInt(this.initialLayout);
            if (this.configure == null)
                break label139;
            paramParcel.writeInt(1);
            this.configure.writeToParcel(paramParcel, paramInt);
        }
        while (true)
        {
            paramParcel.writeString(this.label);
            paramParcel.writeInt(this.icon);
            paramParcel.writeInt(this.previewImage);
            paramParcel.writeInt(this.autoAdvanceViewId);
            paramParcel.writeInt(this.resizeMode);
            return;
            paramParcel.writeInt(0);
            break;
            label139: paramParcel.writeInt(0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.appwidget.AppWidgetProviderInfo
 * JD-Core Version:        0.6.2
 */