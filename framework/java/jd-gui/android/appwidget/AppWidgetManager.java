package android.appwidget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.RemoteViews;
import com.android.internal.appwidget.IAppWidgetService;
import com.android.internal.appwidget.IAppWidgetService.Stub;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;

public class AppWidgetManager
{
    public static final String ACTION_APPWIDGET_BIND = "android.appwidget.action.APPWIDGET_BIND";
    public static final String ACTION_APPWIDGET_CONFIGURE = "android.appwidget.action.APPWIDGET_CONFIGURE";
    public static final String ACTION_APPWIDGET_DELETED = "android.appwidget.action.APPWIDGET_DELETED";
    public static final String ACTION_APPWIDGET_DISABLED = "android.appwidget.action.APPWIDGET_DISABLED";
    public static final String ACTION_APPWIDGET_ENABLED = "android.appwidget.action.APPWIDGET_ENABLED";
    public static final String ACTION_APPWIDGET_OPTIONS_CHANGED = "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS";
    public static final String ACTION_APPWIDGET_PICK = "android.appwidget.action.APPWIDGET_PICK";
    public static final String ACTION_APPWIDGET_UPDATE = "android.appwidget.action.APPWIDGET_UPDATE";
    public static final String EXTRA_APPWIDGET_ID = "appWidgetId";
    public static final String EXTRA_APPWIDGET_IDS = "appWidgetIds";
    public static final String EXTRA_APPWIDGET_OPTIONS = "appWidgetOptions";
    public static final String EXTRA_APPWIDGET_PROVIDER = "appWidgetProvider";
    public static final String EXTRA_CUSTOM_EXTRAS = "customExtras";
    public static final String EXTRA_CUSTOM_INFO = "customInfo";
    public static final int INVALID_APPWIDGET_ID = 0;
    public static final String META_DATA_APPWIDGET_PROVIDER = "android.appwidget.provider";
    public static final String OPTION_APPWIDGET_MAX_HEIGHT = "appWidgetMaxHeight";
    public static final String OPTION_APPWIDGET_MAX_WIDTH = "appWidgetMaxWidth";
    public static final String OPTION_APPWIDGET_MIN_HEIGHT = "appWidgetMinHeight";
    public static final String OPTION_APPWIDGET_MIN_WIDTH = "appWidgetMinWidth";
    static final String TAG = "AppWidgetManager";
    static WeakHashMap<Context, WeakReference<AppWidgetManager>> sManagerCache = new WeakHashMap();
    static IAppWidgetService sService;
    Context mContext;
    private DisplayMetrics mDisplayMetrics;

    private AppWidgetManager(Context paramContext)
    {
        this.mContext = paramContext;
        this.mDisplayMetrics = paramContext.getResources().getDisplayMetrics();
    }

    public static AppWidgetManager getInstance(Context paramContext)
    {
        synchronized (sManagerCache)
        {
            if (sService == null)
                sService = IAppWidgetService.Stub.asInterface(ServiceManager.getService("appwidget"));
            WeakReference localWeakReference = (WeakReference)sManagerCache.get(paramContext);
            AppWidgetManager localAppWidgetManager = null;
            if (localWeakReference != null)
                localAppWidgetManager = (AppWidgetManager)localWeakReference.get();
            if (localAppWidgetManager == null)
            {
                localAppWidgetManager = new AppWidgetManager(paramContext);
                sManagerCache.put(paramContext, new WeakReference(localAppWidgetManager));
            }
            return localAppWidgetManager;
        }
    }

    public void bindAppWidgetId(int paramInt, ComponentName paramComponentName)
    {
        try
        {
            sService.bindAppWidgetId(paramInt, paramComponentName);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public boolean bindAppWidgetIdIfAllowed(int paramInt, ComponentName paramComponentName)
    {
        boolean bool2;
        if (this.mContext == null)
            bool2 = false;
        while (true)
        {
            return bool2;
            try
            {
                boolean bool1 = sService.bindAppWidgetIdIfAllowed(this.mContext.getPackageName(), paramInt, paramComponentName);
                bool2 = bool1;
            }
            catch (RemoteException localRemoteException)
            {
                throw new RuntimeException("system server dead?", localRemoteException);
            }
        }
    }

    public void bindRemoteViewsService(int paramInt, Intent paramIntent, IBinder paramIBinder)
    {
        try
        {
            sService.bindRemoteViewsService(paramInt, paramIntent, paramIBinder);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public int[] getAppWidgetIds(ComponentName paramComponentName)
    {
        try
        {
            int[] arrayOfInt = sService.getAppWidgetIds(paramComponentName);
            return arrayOfInt;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public AppWidgetProviderInfo getAppWidgetInfo(int paramInt)
    {
        try
        {
            AppWidgetProviderInfo localAppWidgetProviderInfo = sService.getAppWidgetInfo(paramInt);
            if (localAppWidgetProviderInfo != null)
            {
                localAppWidgetProviderInfo.minWidth = TypedValue.complexToDimensionPixelSize(localAppWidgetProviderInfo.minWidth, this.mDisplayMetrics);
                localAppWidgetProviderInfo.minHeight = TypedValue.complexToDimensionPixelSize(localAppWidgetProviderInfo.minHeight, this.mDisplayMetrics);
                localAppWidgetProviderInfo.minResizeWidth = TypedValue.complexToDimensionPixelSize(localAppWidgetProviderInfo.minResizeWidth, this.mDisplayMetrics);
                localAppWidgetProviderInfo.minResizeHeight = TypedValue.complexToDimensionPixelSize(localAppWidgetProviderInfo.minResizeHeight, this.mDisplayMetrics);
            }
            return localAppWidgetProviderInfo;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public Bundle getAppWidgetOptions(int paramInt)
    {
        try
        {
            Bundle localBundle = sService.getAppWidgetOptions(paramInt);
            return localBundle;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public List<AppWidgetProviderInfo> getInstalledProviders()
    {
        List localList;
        try
        {
            localList = sService.getInstalledProviders();
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
            {
                AppWidgetProviderInfo localAppWidgetProviderInfo = (AppWidgetProviderInfo)localIterator.next();
                localAppWidgetProviderInfo.minWidth = TypedValue.complexToDimensionPixelSize(localAppWidgetProviderInfo.minWidth, this.mDisplayMetrics);
                localAppWidgetProviderInfo.minHeight = TypedValue.complexToDimensionPixelSize(localAppWidgetProviderInfo.minHeight, this.mDisplayMetrics);
                localAppWidgetProviderInfo.minResizeWidth = TypedValue.complexToDimensionPixelSize(localAppWidgetProviderInfo.minResizeWidth, this.mDisplayMetrics);
                localAppWidgetProviderInfo.minResizeHeight = TypedValue.complexToDimensionPixelSize(localAppWidgetProviderInfo.minResizeHeight, this.mDisplayMetrics);
            }
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
        return localList;
    }

    public boolean hasBindAppWidgetPermission(String paramString)
    {
        try
        {
            boolean bool = sService.hasBindAppWidgetPermission(paramString);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void notifyAppWidgetViewDataChanged(int paramInt1, int paramInt2)
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = paramInt1;
        notifyAppWidgetViewDataChanged(arrayOfInt, paramInt2);
    }

    public void notifyAppWidgetViewDataChanged(int[] paramArrayOfInt, int paramInt)
    {
        try
        {
            sService.notifyAppWidgetViewDataChanged(paramArrayOfInt, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void partiallyUpdateAppWidget(int paramInt, RemoteViews paramRemoteViews)
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = paramInt;
        partiallyUpdateAppWidget(arrayOfInt, paramRemoteViews);
    }

    public void partiallyUpdateAppWidget(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
    {
        try
        {
            sService.partiallyUpdateAppWidgetIds(paramArrayOfInt, paramRemoteViews);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void setBindAppWidgetPermission(String paramString, boolean paramBoolean)
    {
        try
        {
            sService.setBindAppWidgetPermission(paramString, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void unbindRemoteViewsService(int paramInt, Intent paramIntent)
    {
        try
        {
            sService.unbindRemoteViewsService(paramInt, paramIntent);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void updateAppWidget(int paramInt, RemoteViews paramRemoteViews)
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = paramInt;
        updateAppWidget(arrayOfInt, paramRemoteViews);
    }

    public void updateAppWidget(ComponentName paramComponentName, RemoteViews paramRemoteViews)
    {
        try
        {
            sService.updateAppWidgetProvider(paramComponentName, paramRemoteViews);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void updateAppWidget(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
    {
        try
        {
            sService.updateAppWidgetIds(paramArrayOfInt, paramRemoteViews);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }

    public void updateAppWidgetOptions(int paramInt, Bundle paramBundle)
    {
        try
        {
            sService.updateAppWidgetOptions(paramInt, paramBundle);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("system server dead?", localRemoteException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.appwidget.AppWidgetManager
 * JD-Core Version:        0.6.2
 */