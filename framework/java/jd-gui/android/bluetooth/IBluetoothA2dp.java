package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IBluetoothA2dp extends IInterface
{
    public abstract boolean allowIncomingConnect(BluetoothDevice paramBluetoothDevice, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean connect(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean connectSinkInternal(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean disconnect(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean disconnectSinkInternal(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract List<BluetoothDevice> getConnectedDevices()
        throws RemoteException;

    public abstract int getConnectionState(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
        throws RemoteException;

    public abstract int getPriority(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean isA2dpPlaying(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean resumeSink(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean setPriority(BluetoothDevice paramBluetoothDevice, int paramInt)
        throws RemoteException;

    public abstract boolean suspendSink(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBluetoothA2dp
    {
        private static final String DESCRIPTOR = "android.bluetooth.IBluetoothA2dp";
        static final int TRANSACTION_allowIncomingConnect = 13;
        static final int TRANSACTION_connect = 1;
        static final int TRANSACTION_connectSinkInternal = 11;
        static final int TRANSACTION_disconnect = 2;
        static final int TRANSACTION_disconnectSinkInternal = 12;
        static final int TRANSACTION_getConnectedDevices = 3;
        static final int TRANSACTION_getConnectionState = 5;
        static final int TRANSACTION_getDevicesMatchingConnectionStates = 4;
        static final int TRANSACTION_getPriority = 7;
        static final int TRANSACTION_isA2dpPlaying = 8;
        static final int TRANSACTION_resumeSink = 10;
        static final int TRANSACTION_setPriority = 6;
        static final int TRANSACTION_suspendSink = 9;

        public Stub()
        {
            attachInterface(this, "android.bluetooth.IBluetoothA2dp");
        }

        public static IBluetoothA2dp asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.bluetooth.IBluetoothA2dp");
                if ((localIInterface != null) && ((localIInterface instanceof IBluetoothA2dp)))
                    localObject = (IBluetoothA2dp)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
                while (true)
                {
                    return j;
                    paramParcel2.writeString("android.bluetooth.IBluetoothA2dp");
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice11 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice11 = null)
                    {
                        boolean bool10 = connect(localBluetoothDevice11);
                        paramParcel2.writeNoException();
                        if (bool10)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice10 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice10 = null)
                    {
                        boolean bool9 = disconnect(localBluetoothDevice10);
                        paramParcel2.writeNoException();
                        if (bool9)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                    List localList2 = getConnectedDevices();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList2);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                    List localList1 = getDevicesMatchingConnectionStates(paramParcel1.createIntArray());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList1);
                }
            case 5:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice9 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice9 = null)
                {
                    int m = getConnectionState(localBluetoothDevice9);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(m);
                    break;
                }
            case 6:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice8 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice8 = null)
                {
                    boolean bool8 = setPriority(localBluetoothDevice8, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool8)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 7:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice7 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice7 = null)
                {
                    int k = getPriority(localBluetoothDevice7);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(k);
                    break;
                }
            case 8:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice6 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice6 = null)
                {
                    boolean bool7 = isA2dpPlaying(localBluetoothDevice6);
                    paramParcel2.writeNoException();
                    if (bool7)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 9:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice5 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice5 = null)
                {
                    boolean bool6 = suspendSink(localBluetoothDevice5);
                    paramParcel2.writeNoException();
                    if (bool6)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 10:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice4 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice4 = null)
                {
                    boolean bool5 = resumeSink(localBluetoothDevice4);
                    paramParcel2.writeNoException();
                    if (bool5)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 11:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice3 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice3 = null)
                {
                    boolean bool4 = connectSinkInternal(localBluetoothDevice3);
                    paramParcel2.writeNoException();
                    if (bool4)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 12:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice2 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice2 = null)
                {
                    boolean bool3 = disconnectSinkInternal(localBluetoothDevice2);
                    paramParcel2.writeNoException();
                    if (bool3)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 13:
            }
            paramParcel1.enforceInterface("android.bluetooth.IBluetoothA2dp");
            BluetoothDevice localBluetoothDevice1;
            if (paramParcel1.readInt() != 0)
            {
                localBluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1);
                label848: if (paramParcel1.readInt() == 0)
                    break label897;
            }
            label897: for (boolean bool1 = j; ; bool1 = false)
            {
                boolean bool2 = allowIncomingConnect(localBluetoothDevice1, bool1);
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                break;
                localBluetoothDevice1 = null;
                break label848;
            }
        }

        private static class Proxy
            implements IBluetoothA2dp
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public boolean allowIncomingConnect(BluetoothDevice paramBluetoothDevice, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            break label127;
                            localParcel1.writeInt(j);
                            this.mRemote.transact(13, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int k = localParcel2.readInt();
                            if (k != 0)
                                label80: return i;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label127: 
                    do
                    {
                        j = 0;
                        break;
                        i = 0;
                        break label80;
                    }
                    while (!paramBoolean);
                    int j = i;
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean connect(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(1, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean connectSinkInternal(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(11, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean disconnect(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(2, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean disconnectSinkInternal(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(12, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public List<BluetoothDevice> getConnectedDevices()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getConnectionState(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                    localParcel1.writeIntArray(paramArrayOfInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.bluetooth.IBluetoothA2dp";
            }

            public int getPriority(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isA2dpPlaying(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(8, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean resumeSink(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(10, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean setPriority(BluetoothDevice paramBluetoothDevice, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(6, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean suspendSink(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothA2dp");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(9, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.IBluetoothA2dp
 * JD-Core Version:        0.6.2
 */