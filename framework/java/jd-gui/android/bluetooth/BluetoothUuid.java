package android.bluetooth;

import android.os.ParcelUuid;
import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

public final class BluetoothUuid
{
    public static final ParcelUuid AdvAudioDist;
    public static final ParcelUuid AudioSink = ParcelUuid.fromString("0000110B-0000-1000-8000-00805F9B34FB");
    public static final ParcelUuid AudioSource = ParcelUuid.fromString("0000110A-0000-1000-8000-00805F9B34FB");
    public static final ParcelUuid AvrcpController;
    public static final ParcelUuid AvrcpTarget;
    public static final ParcelUuid BNEP;
    public static final ParcelUuid HSP;
    public static final ParcelUuid HSP_AG;
    public static final ParcelUuid Handsfree;
    public static final ParcelUuid Handsfree_AG;
    public static final ParcelUuid Hid;
    public static final ParcelUuid NAP;
    public static final ParcelUuid ObexObjectPush;
    public static final ParcelUuid PANU;
    public static final ParcelUuid PBAP_PSE;
    public static final ParcelUuid[] RESERVED_UUIDS = arrayOfParcelUuid;

    static
    {
        AdvAudioDist = ParcelUuid.fromString("0000110D-0000-1000-8000-00805F9B34FB");
        HSP = ParcelUuid.fromString("00001108-0000-1000-8000-00805F9B34FB");
        HSP_AG = ParcelUuid.fromString("00001112-0000-1000-8000-00805F9B34FB");
        Handsfree = ParcelUuid.fromString("0000111E-0000-1000-8000-00805F9B34FB");
        Handsfree_AG = ParcelUuid.fromString("0000111F-0000-1000-8000-00805F9B34FB");
        AvrcpController = ParcelUuid.fromString("0000110E-0000-1000-8000-00805F9B34FB");
        AvrcpTarget = ParcelUuid.fromString("0000110C-0000-1000-8000-00805F9B34FB");
        ObexObjectPush = ParcelUuid.fromString("00001105-0000-1000-8000-00805f9b34fb");
        Hid = ParcelUuid.fromString("00001124-0000-1000-8000-00805f9b34fb");
        PANU = ParcelUuid.fromString("00001115-0000-1000-8000-00805F9B34FB");
        NAP = ParcelUuid.fromString("00001116-0000-1000-8000-00805F9B34FB");
        BNEP = ParcelUuid.fromString("0000000f-0000-1000-8000-00805F9B34FB");
        PBAP_PSE = ParcelUuid.fromString("0000112f-0000-1000-8000-00805F9B34FB");
        ParcelUuid[] arrayOfParcelUuid = new ParcelUuid[10];
        arrayOfParcelUuid[0] = AudioSink;
        arrayOfParcelUuid[1] = AudioSource;
        arrayOfParcelUuid[2] = AdvAudioDist;
        arrayOfParcelUuid[3] = HSP;
        arrayOfParcelUuid[4] = Handsfree;
        arrayOfParcelUuid[5] = AvrcpController;
        arrayOfParcelUuid[6] = AvrcpTarget;
        arrayOfParcelUuid[7] = ObexObjectPush;
        arrayOfParcelUuid[8] = PANU;
        arrayOfParcelUuid[9] = NAP;
    }

    public static boolean containsAllUuids(ParcelUuid[] paramArrayOfParcelUuid1, ParcelUuid[] paramArrayOfParcelUuid2)
    {
        boolean bool = true;
        if ((paramArrayOfParcelUuid1 == null) && (paramArrayOfParcelUuid2 == null));
        label76: 
        while (true)
        {
            return bool;
            if (paramArrayOfParcelUuid1 == null)
            {
                if (paramArrayOfParcelUuid2.length != 0)
                    bool = false;
            }
            else if (paramArrayOfParcelUuid2 != null)
            {
                HashSet localHashSet = new HashSet(Arrays.asList(paramArrayOfParcelUuid1));
                int i = paramArrayOfParcelUuid2.length;
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label76;
                    if (!localHashSet.contains(paramArrayOfParcelUuid2[j]))
                    {
                        bool = false;
                        break;
                    }
                }
            }
        }
    }

    public static boolean containsAnyUuid(ParcelUuid[] paramArrayOfParcelUuid1, ParcelUuid[] paramArrayOfParcelUuid2)
    {
        boolean bool = true;
        if ((paramArrayOfParcelUuid1 == null) && (paramArrayOfParcelUuid2 == null));
        while (true)
        {
            return bool;
            if (paramArrayOfParcelUuid1 == null)
            {
                if (paramArrayOfParcelUuid2.length != 0)
                    bool = false;
            }
            else if (paramArrayOfParcelUuid2 == null)
            {
                if (paramArrayOfParcelUuid1.length != 0)
                    bool = false;
            }
            else
            {
                HashSet localHashSet = new HashSet(Arrays.asList(paramArrayOfParcelUuid1));
                int i = paramArrayOfParcelUuid2.length;
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label83;
                    if (localHashSet.contains(paramArrayOfParcelUuid2[j]))
                        break;
                }
                label83: bool = false;
            }
        }
    }

    public static int getServiceIdentifierFromParcelUuid(ParcelUuid paramParcelUuid)
    {
        return (int)((0x0 & paramParcelUuid.getUuid().getMostSignificantBits()) >>> 32);
    }

    public static boolean isAdvAudioDist(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(AdvAudioDist);
    }

    public static boolean isAudioSink(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(AudioSink);
    }

    public static boolean isAudioSource(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(AudioSource);
    }

    public static boolean isAvrcpController(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(AvrcpController);
    }

    public static boolean isAvrcpTarget(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(AvrcpTarget);
    }

    public static boolean isBnep(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(BNEP);
    }

    public static boolean isHandsfree(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(Handsfree);
    }

    public static boolean isHeadset(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(HSP);
    }

    public static boolean isInputDevice(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(Hid);
    }

    public static boolean isNap(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(NAP);
    }

    public static boolean isPanu(ParcelUuid paramParcelUuid)
    {
        return paramParcelUuid.equals(PANU);
    }

    public static boolean isUuidPresent(ParcelUuid[] paramArrayOfParcelUuid, ParcelUuid paramParcelUuid)
    {
        boolean bool = false;
        if (((paramArrayOfParcelUuid == null) || (paramArrayOfParcelUuid.length == 0)) && (paramParcelUuid == null))
            bool = true;
        label55: 
        while (true)
        {
            return bool;
            if (paramArrayOfParcelUuid != null)
            {
                int i = paramArrayOfParcelUuid.length;
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label55;
                    if (paramArrayOfParcelUuid[j].equals(paramParcelUuid))
                    {
                        bool = true;
                        break;
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothUuid
 * JD-Core Version:        0.6.2
 */