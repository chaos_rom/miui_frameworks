package android.bluetooth;

import android.content.Context;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class BluetoothA2dp
    implements BluetoothProfile
{
    public static final String ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED";
    public static final String ACTION_PLAYING_STATE_CHANGED = "android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED";
    private static final boolean DBG = false;
    public static final int STATE_NOT_PLAYING = 11;
    public static final int STATE_PLAYING = 10;
    private static final String TAG = "BluetoothA2dp";
    private BluetoothAdapter mAdapter;
    private IBluetoothA2dp mService;
    private BluetoothProfile.ServiceListener mServiceListener;

    BluetoothA2dp(Context paramContext, BluetoothProfile.ServiceListener paramServiceListener)
    {
        IBinder localIBinder = ServiceManager.getService("bluetooth_a2dp");
        this.mServiceListener = paramServiceListener;
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (localIBinder != null)
        {
            this.mService = IBluetoothA2dp.Stub.asInterface(localIBinder);
            if (this.mServiceListener != null)
                this.mServiceListener.onServiceConnected(2, this);
        }
        while (true)
        {
            return;
            Log.w("BluetoothA2dp", "Bluetooth A2DP service not available!");
            this.mService = null;
        }
    }

    private boolean isEnabled()
    {
        if (this.mAdapter.getState() == 12);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isValidDevice(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        if (paramBluetoothDevice == null);
        while (true)
        {
            return bool;
            if (BluetoothAdapter.checkBluetoothAddress(paramBluetoothDevice.getAddress()))
                bool = true;
        }
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothA2dp", paramString);
    }

    public static String stateToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        default:
            str = "<unknown state " + paramInt + ">";
        case 0:
        case 1:
        case 2:
        case 3:
        case 10:
        case 11:
        }
        while (true)
        {
            return str;
            str = "disconnected";
            continue;
            str = "connecting";
            continue;
            str = "connected";
            continue;
            str = "disconnecting";
            continue;
            str = "playing";
            continue;
            str = "not playing";
        }
    }

    public boolean allowIncomingConnect(BluetoothDevice paramBluetoothDevice, boolean paramBoolean)
    {
        try
        {
            boolean bool2 = this.mService.allowIncomingConnect(paramBluetoothDevice, paramBoolean);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothA2dp", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    void close()
    {
        this.mServiceListener = null;
    }

    public boolean connect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.connect(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
        }
    }

    public boolean disconnect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.disconnect(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getConnectedDevices()
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getConnectedDevices();
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public int getConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        int i = 0;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                int j = this.mService.getConnectionState(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getDevicesMatchingConnectionStates(paramArrayOfInt);
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public int getPriority(BluetoothDevice paramBluetoothDevice)
    {
        int i = 0;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                int j = this.mService.getPriority(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
        }
    }

    public boolean isA2dpPlaying(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.isA2dpPlaying(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
        }
    }

    public boolean resumeSink(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.resumeSink(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
        }
    }

    public boolean setPriority(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)))
            if ((paramInt == 0) || (paramInt == 100));
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.setPriority(paramBluetoothDevice, paramInt);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
            }
            continue;
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
        }
    }

    public boolean shouldSendVolumeKeys(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        ParcelUuid[] arrayOfParcelUuid;
        if ((isEnabled()) && (isValidDevice(paramBluetoothDevice)))
        {
            arrayOfParcelUuid = paramBluetoothDevice.getUuids();
            if (arrayOfParcelUuid != null)
                break label28;
        }
        label28: label61: 
        while (true)
        {
            return bool;
            int i = arrayOfParcelUuid.length;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label61;
                if (BluetoothUuid.isAvrcpTarget(arrayOfParcelUuid[j]))
                {
                    bool = true;
                    break;
                }
            }
        }
    }

    public boolean suspendSink(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.suspendSink(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothA2dp", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothA2dp", "Proxy not attached to service");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothA2dp
 * JD-Core Version:        0.6.2
 */