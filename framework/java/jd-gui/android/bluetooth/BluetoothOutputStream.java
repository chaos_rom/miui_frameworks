package android.bluetooth;

import java.io.IOException;
import java.io.OutputStream;

final class BluetoothOutputStream extends OutputStream
{
    private BluetoothSocket mSocket;

    BluetoothOutputStream(BluetoothSocket paramBluetoothSocket)
    {
        this.mSocket = paramBluetoothSocket;
    }

    public void close()
        throws IOException
    {
        this.mSocket.close();
    }

    public void write(int paramInt)
        throws IOException
    {
        byte[] arrayOfByte = new byte[1];
        arrayOfByte[0] = ((byte)paramInt);
        this.mSocket.write(arrayOfByte, 0, 1);
    }

    public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        if (paramArrayOfByte == null)
            throw new NullPointerException("buffer is null");
        if (((paramInt1 | paramInt2) < 0) || (paramInt2 > paramArrayOfByte.length - paramInt1))
            throw new IndexOutOfBoundsException("invalid offset or length");
        this.mSocket.write(paramArrayOfByte, paramInt1, paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothOutputStream
 * JD-Core Version:        0.6.2
 */