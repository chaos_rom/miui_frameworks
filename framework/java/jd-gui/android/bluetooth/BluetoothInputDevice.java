package android.bluetooth;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class BluetoothInputDevice
    implements BluetoothProfile
{
    public static final String ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED";
    private static final boolean DBG = false;
    public static final int INPUT_CONNECT_FAILED_ALREADY_CONNECTED = 5001;
    public static final int INPUT_CONNECT_FAILED_ATTEMPT_FAILED = 5002;
    public static final int INPUT_DISCONNECT_FAILED_NOT_CONNECTED = 5000;
    public static final int INPUT_OPERATION_GENERIC_FAILURE = 5003;
    public static final int INPUT_OPERATION_SUCCESS = 5004;
    private static final String TAG = "BluetoothInputDevice";
    private BluetoothAdapter mAdapter;
    private IBluetooth mService;
    private BluetoothProfile.ServiceListener mServiceListener;

    BluetoothInputDevice(Context paramContext, BluetoothProfile.ServiceListener paramServiceListener)
    {
        IBinder localIBinder = ServiceManager.getService("bluetooth");
        this.mServiceListener = paramServiceListener;
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (localIBinder != null)
        {
            this.mService = IBluetooth.Stub.asInterface(localIBinder);
            if (this.mServiceListener != null)
                this.mServiceListener.onServiceConnected(4, this);
        }
        while (true)
        {
            return;
            Log.w("BluetoothInputDevice", "Bluetooth Service not available!");
            this.mService = null;
        }
    }

    private boolean isEnabled()
    {
        if (this.mAdapter.getState() == 12);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isValidDevice(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        if (paramBluetoothDevice == null);
        while (true)
        {
            return bool;
            if (BluetoothAdapter.checkBluetoothAddress(paramBluetoothDevice.getAddress()))
                bool = true;
        }
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothInputDevice", paramString);
    }

    void close()
    {
        this.mServiceListener = null;
    }

    public boolean connect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.connectInputDevice(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothInputDevice", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothInputDevice", "Proxy not attached to service");
        }
    }

    public boolean disconnect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.disconnectInputDevice(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothInputDevice", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothInputDevice", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getConnectedDevices()
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getConnectedInputDevices();
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothInputDevice", "Stack:" + Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothInputDevice", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public int getConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        int i = 0;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                int j = this.mService.getInputDeviceConnectionState(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothInputDevice", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothInputDevice", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getInputDevicesMatchingConnectionStates(paramArrayOfInt);
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothInputDevice", "Stack:" + Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothInputDevice", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public int getPriority(BluetoothDevice paramBluetoothDevice)
    {
        int i = 0;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                int j = this.mService.getInputDevicePriority(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothInputDevice", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothInputDevice", "Proxy not attached to service");
        }
    }

    public boolean setPriority(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)))
            if ((paramInt == 0) || (paramInt == 100));
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.setInputDevicePriority(paramBluetoothDevice, paramInt);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothInputDevice", "Stack:" + Log.getStackTraceString(new Throwable()));
            }
            continue;
            if (this.mService == null)
                Log.w("BluetoothInputDevice", "Proxy not attached to service");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothInputDevice
 * JD-Core Version:        0.6.2
 */