package android.bluetooth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.server.BluetoothA2dpService;
import android.server.BluetoothService;
import android.util.Log;
import android.util.Pair;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.util.List;

public final class BluetoothDeviceProfileState extends StateMachine
{
    private static final String ACCESS_AUTHORITY_CLASS = "com.android.settings.bluetooth.BluetoothPermissionRequest";
    private static final String ACCESS_AUTHORITY_PACKAGE = "com.android.settings";
    public static final int AUTO_CONNECT_PROFILES = 101;
    private static final String BLUETOOTH_ADMIN_PERM = "android.permission.BLUETOOTH_ADMIN";
    private static final int CONNECTION_ACCESS_REQUEST_EXPIRY = 105;
    private static final int CONNECTION_ACCESS_REQUEST_EXPIRY_TIMEOUT = 7000;
    private static final int CONNECTION_ACCESS_REQUEST_REPLY = 104;
    private static final int CONNECTION_ACCESS_UNDEFINED = -1;
    public static final int CONNECT_A2DP_INCOMING = 4;
    public static final int CONNECT_A2DP_OUTGOING = 3;
    public static final int CONNECT_HFP_INCOMING = 2;
    public static final int CONNECT_HFP_OUTGOING = 1;
    public static final int CONNECT_HID_INCOMING = 6;
    public static final int CONNECT_HID_OUTGOING = 5;
    public static final int CONNECT_OTHER_PROFILES = 103;
    public static final int CONNECT_OTHER_PROFILES_DELAY = 4000;
    private static final boolean DBG = false;
    public static final int DISCONNECT_A2DP_INCOMING = 53;
    public static final int DISCONNECT_A2DP_OUTGOING = 52;
    private static final int DISCONNECT_HFP_INCOMING = 51;
    public static final int DISCONNECT_HFP_OUTGOING = 50;
    public static final int DISCONNECT_HID_INCOMING = 55;
    public static final int DISCONNECT_HID_OUTGOING = 54;
    public static final int DISCONNECT_PBAP_OUTGOING = 56;
    private static final long INIT_INCOMING_REJECT_TIMER = 1000L;
    private static final long MAX_INCOMING_REJECT_TIMER = 14400000L;
    private static final String TAG = "BluetoothDeviceProfileState";
    public static final int TRANSITION_TO_STABLE = 102;
    public static final int UNPAIR = 100;
    private BluetoothA2dpService mA2dpService;
    private int mA2dpState = 0;
    private BluetoothAdapter mAdapter;
    private boolean mAutoConnectionPending;
    private BluetoothProfile.ServiceListener mBluetoothProfileServiceListener = new BluetoothProfile.ServiceListener()
    {
        public void onServiceConnected(int paramAnonymousInt, BluetoothProfile paramAnonymousBluetoothProfile)
        {
            synchronized (BluetoothDeviceProfileState.this)
            {
                BluetoothDeviceProfileState.access$1302(BluetoothDeviceProfileState.this, (BluetoothHeadset)paramAnonymousBluetoothProfile);
                if (BluetoothDeviceProfileState.this.mAutoConnectionPending)
                {
                    BluetoothDeviceProfileState.this.sendMessage(101);
                    BluetoothDeviceProfileState.access$1402(BluetoothDeviceProfileState.this, false);
                }
                return;
            }
        }

        public void onServiceDisconnected(int paramAnonymousInt)
        {
            synchronized (BluetoothDeviceProfileState.this)
            {
                BluetoothDeviceProfileState.access$1302(BluetoothDeviceProfileState.this, null);
                return;
            }
        }
    };
    private BondedDevice mBondedDevice = new BondedDevice(null);
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)paramAnonymousIntent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if ((localBluetoothDevice == null) || (!localBluetoothDevice.equals(BluetoothDeviceProfileState.this.mDevice)));
            while (true)
            {
                return;
                if (str.equals("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"))
                {
                    int i2 = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                    int i3 = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", 0);
                    if (i2 == 2)
                        BluetoothDeviceProfileState.this.setTrust(1);
                    BluetoothDeviceProfileState.access$902(BluetoothDeviceProfileState.this, i2);
                    if ((i3 == 2) && (i2 == 0))
                        BluetoothDeviceProfileState.this.sendMessage(53);
                    if ((i2 == 2) || (i2 == 0))
                        BluetoothDeviceProfileState.this.sendMessage(102);
                }
                else if (str.equals("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"))
                {
                    int n = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                    int i1 = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", 0);
                    if (n == 2)
                        BluetoothDeviceProfileState.this.setTrust(1);
                    BluetoothDeviceProfileState.access$1002(BluetoothDeviceProfileState.this, n);
                    if ((i1 == 2) && (n == 0))
                        BluetoothDeviceProfileState.this.sendMessage(51);
                    if ((n == 2) || (n == 0))
                        BluetoothDeviceProfileState.this.sendMessage(102);
                }
                else if (str.equals("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"))
                {
                    int k = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                    int m = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", 0);
                    if (k == 2)
                        BluetoothDeviceProfileState.this.setTrust(1);
                    if ((m == 2) && (k == 0))
                        BluetoothDeviceProfileState.this.sendMessage(55);
                    if ((k == 2) || (k == 0))
                        BluetoothDeviceProfileState.this.sendMessage(102);
                }
                else if (str.equals("android.bluetooth.device.action.ACL_DISCONNECTED"))
                {
                    BluetoothDeviceProfileState.this.sendMessage(102);
                }
                else if (str.equals("android.bluetooth.device.action.CONNECTION_ACCESS_REPLY"))
                {
                    BluetoothDeviceProfileState.this.mWakeLock.release();
                    int j = paramAnonymousIntent.getIntExtra("android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT", 2);
                    Message localMessage = BluetoothDeviceProfileState.this.obtainMessage(104);
                    localMessage.arg1 = j;
                    BluetoothDeviceProfileState.this.sendMessage(localMessage);
                }
                else if (str.equals("android.bluetooth.device.action.PAIRING_REQUEST"))
                {
                    BluetoothDeviceProfileState.access$1202(BluetoothDeviceProfileState.this, true);
                }
                else if (str.equals("android.bluetooth.device.action.BOND_STATE_CHANGED"))
                {
                    int i = paramAnonymousIntent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -2147483648);
                    if ((i == 12) && (BluetoothDeviceProfileState.this.mPairingRequestRcvd))
                    {
                        BluetoothDeviceProfileState.this.setTrust(1);
                        BluetoothDeviceProfileState.access$1202(BluetoothDeviceProfileState.this, false);
                    }
                    else if (i == 10)
                    {
                        BluetoothDeviceProfileState.access$1202(BluetoothDeviceProfileState.this, false);
                    }
                }
            }
        }
    };
    private boolean mConnectionAccessReplyReceived = false;
    private Context mContext;
    private BluetoothDevice mDevice;
    private BluetoothHeadset mHeadsetService;
    private int mHeadsetState = 0;
    private IncomingA2dp mIncomingA2dp = new IncomingA2dp(null);
    private Pair<Integer, String> mIncomingConnections;
    private IncomingHandsfree mIncomingHandsfree = new IncomingHandsfree(null);
    private IncomingHid mIncomingHid = new IncomingHid(null);
    private long mIncomingRejectTimer;
    private OutgoingA2dp mOutgoingA2dp = new OutgoingA2dp(null);
    private OutgoingHandsfree mOutgoingHandsfree = new OutgoingHandsfree(null);
    private OutgoingHid mOutgoingHid = new OutgoingHid(null);
    private boolean mPairingRequestRcvd = false;
    private PbapServiceListener mPbap;
    private BluetoothPbap mPbapService;
    private boolean mPbapServiceConnected;
    private PowerManager mPowerManager;
    private BluetoothService mService;
    private PowerManager.WakeLock mWakeLock;

    public BluetoothDeviceProfileState(Context paramContext, String paramString, BluetoothService paramBluetoothService, BluetoothA2dpService paramBluetoothA2dpService, boolean paramBoolean)
    {
        super(paramString);
        this.mContext = paramContext;
        this.mDevice = new BluetoothDevice(paramString);
        this.mService = paramBluetoothService;
        this.mA2dpService = paramBluetoothA2dpService;
        addState(this.mBondedDevice);
        addState(this.mOutgoingHandsfree);
        addState(this.mIncomingHandsfree);
        addState(this.mIncomingA2dp);
        addState(this.mOutgoingA2dp);
        addState(this.mOutgoingHid);
        addState(this.mIncomingHid);
        setInitialState(this.mBondedDevice);
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        localIntentFilter.addAction("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED");
        localIntentFilter.addAction("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED");
        localIntentFilter.addAction("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED");
        localIntentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        localIntentFilter.addAction("android.bluetooth.device.action.CONNECTION_ACCESS_REPLY");
        localIntentFilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        localIntentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        this.mContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter);
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mAdapter.getProfileProxy(this.mContext, this.mBluetoothProfileServiceListener, 1);
        this.mPbap = new PbapServiceListener();
        this.mIncomingConnections = this.mService.getIncomingState(paramString);
        this.mIncomingRejectTimer = readTimerValue();
        this.mPowerManager = ((PowerManager)this.mContext.getSystemService("power"));
        this.mWakeLock = this.mPowerManager.newWakeLock(805306374, "BluetoothDeviceProfileState");
        this.mWakeLock.setReferenceCounted(false);
        if (paramBoolean)
            setTrust(1);
    }

    private String getStringValue(long paramLong)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(Long.toString(System.currentTimeMillis()));
        localStringBuilder.append("-");
        localStringBuilder.append(Long.toString(paramLong));
        return localStringBuilder.toString();
    }

    private int getTrust()
    {
        this.mDevice.getAddress();
        if (this.mIncomingConnections != null);
        for (int i = ((Integer)this.mIncomingConnections.first).intValue(); ; i = -1)
            return i;
    }

    private void handleConnectionOfOtherProfiles(int paramInt)
    {
        switch (paramInt)
        {
        case 3:
        default:
        case 2:
        case 4:
        }
        while (true)
        {
            return;
            if ((this.mA2dpService.getPriority(this.mDevice) == 100) || (this.mA2dpService.getPriority(this.mDevice) == 1000))
            {
                Message localMessage2 = new Message();
                localMessage2.what = 103;
                localMessage2.arg1 = 3;
                sendMessageDelayed(localMessage2, 4000L);
                continue;
                if ((this.mHeadsetService != null) && ((this.mHeadsetService.getPriority(this.mDevice) == 100) || (this.mHeadsetService.getPriority(this.mDevice) == 1000)))
                {
                    Message localMessage1 = new Message();
                    localMessage1.what = 103;
                    localMessage1.arg1 = 1;
                    sendMessageDelayed(localMessage1, 4000L);
                }
            }
        }
    }

    private boolean handleIncomingConnection(int paramInt, boolean paramBoolean)
    {
        boolean bool = false;
        Log.i("BluetoothDeviceProfileState", "handleIncomingConnection:" + paramInt + ":" + paramBoolean);
        switch (paramInt)
        {
        case 3:
        case 5:
        default:
            Log.e("BluetoothDeviceProfileState", "Waiting for incoming connection but state changed to:" + paramInt);
        case 2:
        case 4:
        case 6:
        }
        while (true)
        {
            return bool;
            if (!paramBoolean)
            {
                bool = this.mHeadsetService.rejectIncomingConnect(this.mDevice);
                sendMessage(102);
                updateIncomingAllowedTimer();
            }
            else if (this.mHeadsetState == 1)
            {
                writeTimerValue(0L);
                bool = this.mHeadsetService.acceptIncomingConnect(this.mDevice);
            }
            else if (this.mHeadsetState == 0)
            {
                writeTimerValue(0L);
                handleConnectionOfOtherProfiles(paramInt);
                bool = this.mHeadsetService.createIncomingConnect(this.mDevice);
                continue;
                if (!paramBoolean)
                {
                    bool = this.mA2dpService.allowIncomingConnect(this.mDevice, false);
                    sendMessage(102);
                    updateIncomingAllowedTimer();
                }
                else
                {
                    writeTimerValue(0L);
                    bool = this.mA2dpService.allowIncomingConnect(this.mDevice, true);
                    handleConnectionOfOtherProfiles(paramInt);
                    continue;
                    if (!paramBoolean)
                    {
                        bool = this.mService.allowIncomingProfileConnect(this.mDevice, false);
                        sendMessage(102);
                        updateIncomingAllowedTimer();
                    }
                    else
                    {
                        writeTimerValue(0L);
                        bool = this.mService.allowIncomingProfileConnect(this.mDevice, true);
                    }
                }
            }
        }
    }

    private boolean isPhoneDocked(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        Intent localIntent = this.mContext.registerReceiver(null, new IntentFilter("android.intent.action.DOCK_EVENT"));
        if ((localIntent != null) && (localIntent.getIntExtra("android.intent.extra.DOCK_STATE", 0) != 0))
        {
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIntent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if ((localBluetoothDevice != null) && (paramBluetoothDevice.equals(localBluetoothDevice)))
                bool = true;
        }
        return bool;
    }

    private void log(String paramString)
    {
    }

    private void processIncomingConnectCommand(int paramInt)
    {
        int i = getTrust();
        if (i == 1)
            handleIncomingConnection(paramInt, true);
        while (true)
        {
            return;
            if ((i == 2) && (!readIncomingAllowedValue()))
            {
                handleIncomingConnection(paramInt, false);
            }
            else
            {
                sendConnectionAccessIntent();
                sendMessageDelayed(obtainMessage(105), 7000L);
            }
        }
    }

    private boolean readIncomingAllowedValue()
    {
        boolean bool = true;
        if (readTimerValue() == 0L);
        while (true)
        {
            return bool;
            String[] arrayOfString = ((String)this.mIncomingConnections.second).split("-");
            if ((arrayOfString == null) || (arrayOfString.length != 2) || (Long.parseLong(arrayOfString[0]) + Long.parseLong(arrayOfString[bool]) > System.currentTimeMillis()))
                bool = false;
        }
    }

    private long readTimerValue()
    {
        long l = 0L;
        if (this.mIncomingConnections == null);
        while (true)
        {
            return l;
            String[] arrayOfString = ((String)this.mIncomingConnections.second).split("-");
            if ((arrayOfString != null) && (arrayOfString.length == 2))
                l = Long.parseLong(arrayOfString[1]);
        }
    }

    private void sendConnectionAccessIntent()
    {
        this.mConnectionAccessReplyReceived = false;
        if (!this.mPowerManager.isScreenOn())
            this.mWakeLock.acquire();
        Intent localIntent = new Intent("android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST");
        localIntent.setClassName("com.android.settings", "com.android.settings.bluetooth.BluetoothPermissionRequest");
        localIntent.putExtra("android.bluetooth.device.extra.ACCESS_REQUEST_TYPE", 1);
        localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mDevice);
        this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
    }

    private void sendConnectionAccessRemovalIntent()
    {
        this.mWakeLock.release();
        Intent localIntent = new Intent("android.bluetooth.device.action.CONNECTION_ACCESS_CANCEL");
        localIntent.putExtra("android.bluetooth.device.extra.DEVICE", this.mDevice);
        this.mContext.sendBroadcast(localIntent, "android.permission.BLUETOOTH_ADMIN");
    }

    private void setTrust(int paramInt)
    {
        if (this.mIncomingConnections == null);
        for (String str = getStringValue(1000L); ; str = (String)this.mIncomingConnections.second)
        {
            this.mIncomingConnections = new Pair(Integer.valueOf(paramInt), str);
            this.mService.writeIncomingConnectionState(this.mDevice.getAddress(), this.mIncomingConnections);
            return;
        }
    }

    private void updateIncomingAllowedTimer()
    {
        if (this.mIncomingRejectTimer == 0L)
            this.mIncomingRejectTimer = 1000L;
        this.mIncomingRejectTimer = (5L * this.mIncomingRejectTimer);
        if (this.mIncomingRejectTimer > 14400000L)
            this.mIncomingRejectTimer = 14400000L;
        writeTimerValue(this.mIncomingRejectTimer);
    }

    private void writeTimerValue(long paramLong)
    {
        if (this.mIncomingConnections == null);
        for (Integer localInteger = Integer.valueOf(-1); ; localInteger = (Integer)this.mIncomingConnections.first)
        {
            this.mIncomingConnections = new Pair(localInteger, getStringValue(paramLong));
            this.mService.writeIncomingConnectionState(this.mDevice.getAddress(), this.mIncomingConnections);
            return;
        }
    }

    /** @deprecated */
    void cancelCommand(int paramInt)
    {
        if (paramInt == 1);
        try
        {
            if (this.mHeadsetService != null)
                this.mHeadsetService.cancelConnectThread();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void deferProfileServiceMessage(int paramInt)
    {
        try
        {
            Message localMessage = new Message();
            localMessage.what = paramInt;
            deferMessage(localMessage);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    BluetoothDevice getDevice()
    {
        return this.mDevice;
    }

    /** @deprecated */
    boolean processCommand(int paramInt)
    {
        boolean bool1 = true;
        while (true)
        {
            try
            {
                log("Processing command:" + paramInt);
                switch (paramInt)
                {
                default:
                    Log.e("BluetoothDeviceProfileState", "Error: Unknown Command");
                    bool1 = false;
                case 51:
                case 53:
                case 55:
                    return bool1;
                case 1:
                    if (this.mHeadsetService == null)
                    {
                        deferProfileServiceMessage(paramInt);
                        continue;
                    }
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 50:
                case 52:
                case 54:
                case 56:
                case 100:
                }
            }
            finally
            {
            }
            bool1 = this.mHeadsetService.connectHeadsetInternal(this.mDevice);
            continue;
            if (this.mHeadsetService == null)
            {
                deferProfileServiceMessage(paramInt);
            }
            else
            {
                processIncomingConnectCommand(paramInt);
                continue;
                if (this.mA2dpService != null)
                {
                    bool1 = this.mA2dpService.connectSinkInternal(this.mDevice);
                    continue;
                    processIncomingConnectCommand(paramInt);
                    continue;
                    bool1 = this.mService.connectInputDeviceInternal(this.mDevice);
                    continue;
                    processIncomingConnectCommand(paramInt);
                    continue;
                    if (this.mHeadsetService == null)
                    {
                        deferProfileServiceMessage(paramInt);
                    }
                    else
                    {
                        Message localMessage = new Message();
                        localMessage.what = 56;
                        deferMessage(localMessage);
                        if (this.mHeadsetService.getPriority(this.mDevice) == 1000)
                            this.mHeadsetService.setPriority(this.mDevice, 100);
                        bool1 = this.mHeadsetService.disconnectHeadsetInternal(this.mDevice);
                        continue;
                        if (this.mA2dpService != null)
                        {
                            if (this.mA2dpService.getPriority(this.mDevice) == 1000)
                                this.mA2dpService.setPriority(this.mDevice, 100);
                            bool1 = this.mA2dpService.disconnectSinkInternal(this.mDevice);
                            continue;
                            if (this.mService.getInputDevicePriority(this.mDevice) == 1000)
                                this.mService.setInputDevicePriority(this.mDevice, 100);
                            bool1 = this.mService.disconnectInputDeviceInternal(this.mDevice);
                            continue;
                            if (!this.mPbapServiceConnected)
                            {
                                deferProfileServiceMessage(paramInt);
                            }
                            else
                            {
                                bool1 = this.mPbapService.disconnect();
                                continue;
                                writeTimerValue(1000L);
                                setTrust(-1);
                                boolean bool2 = this.mService.removeBondInternal(this.mDevice.getAddress());
                                bool1 = bool2;
                            }
                        }
                    }
                }
            }
        }
    }

    private class IncomingHid extends State
    {
        private int mCommand;
        private boolean mStatus = false;

        private IncomingHid()
        {
        }

        public void enter()
        {
            BluetoothDeviceProfileState.this.log("Entering IncomingHid state with: " + BluetoothDeviceProfileState.access$11000(BluetoothDeviceProfileState.this).what);
            this.mCommand = BluetoothDeviceProfileState.this.getCurrentMessage().what;
            if ((this.mCommand != 6) && (this.mCommand != 55))
                Log.e("BluetoothDeviceProfileState", "Error: IncomingHid state with command:" + this.mCommand);
            this.mStatus = BluetoothDeviceProfileState.this.processCommand(this.mCommand);
            if (!this.mStatus)
                BluetoothDeviceProfileState.this.sendMessage(102);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            BluetoothDeviceProfileState.this.log("IncomingHid State->Processing Message: " + paramMessage.what);
            new Message();
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 50:
            case 52:
            case 54:
                BluetoothDeviceProfileState.this.deferMessage(paramMessage);
            case 51:
            case 53:
            case 55:
            case 104:
            case 105:
            case 56:
            case 100:
            case 101:
            case 102:
            }
            while (true)
            {
                bool = true;
                break;
                BluetoothDeviceProfileState.access$6602(BluetoothDeviceProfileState.this, true);
                int i = paramMessage.arg1;
                BluetoothDeviceProfileState.this.setTrust(i);
                BluetoothDeviceProfileState localBluetoothDeviceProfileState = BluetoothDeviceProfileState.this;
                if (i == 1)
                    bool = true;
                localBluetoothDeviceProfileState.handleIncomingConnection(6, bool);
                continue;
                if (!BluetoothDeviceProfileState.this.mConnectionAccessReplyReceived)
                {
                    BluetoothDeviceProfileState.this.handleIncomingConnection(6, false);
                    BluetoothDeviceProfileState.this.sendConnectionAccessRemovalIntent();
                    BluetoothDeviceProfileState.this.sendMessage(102);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mBondedDevice);
                }
            }
        }
    }

    private class OutgoingHid extends State
    {
        private int mCommand;
        private boolean mStatus = false;

        private OutgoingHid()
        {
        }

        public void enter()
        {
            BluetoothDeviceProfileState.this.log("Entering OutgoingHid state with: " + BluetoothDeviceProfileState.access$10000(BluetoothDeviceProfileState.this).what);
            this.mCommand = BluetoothDeviceProfileState.this.getCurrentMessage().what;
            if ((this.mCommand != 5) && (this.mCommand != 54))
                Log.e("BluetoothDeviceProfileState", "Error: OutgoingHid state with command:" + this.mCommand);
            this.mStatus = BluetoothDeviceProfileState.this.processCommand(this.mCommand);
            if (!this.mStatus)
                BluetoothDeviceProfileState.this.sendMessage(102);
        }

        public boolean processMessage(Message paramMessage)
        {
            BluetoothDeviceProfileState.this.log("OutgoingHid State->Processing Message: " + paramMessage.what);
            Message localMessage = new Message();
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 1:
            case 3:
            case 5:
            case 50:
            case 52:
            case 54:
                BluetoothDeviceProfileState.this.deferMessage(paramMessage);
            case 55:
            case 2:
            case 4:
            case 6:
            case 51:
            case 53:
            case 56:
            case 100:
            case 101:
            case 102:
            }
            while (true)
            {
                bool = true;
                break;
                BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingHandsfree);
                continue;
                BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingA2dp);
                if (this.mStatus)
                {
                    localMessage.what = this.mCommand;
                    BluetoothDeviceProfileState.this.deferMessage(localMessage);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingHid);
                    continue;
                    if (this.mStatus)
                    {
                        localMessage.what = this.mCommand;
                        BluetoothDeviceProfileState.this.deferMessage(localMessage);
                        continue;
                        BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                        continue;
                        BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mBondedDevice);
                    }
                }
            }
        }
    }

    private class IncomingA2dp extends State
    {
        private int mCommand;
        private boolean mStatus = false;

        private IncomingA2dp()
        {
        }

        public void enter()
        {
            Log.i("BluetoothDeviceProfileState", "Entering IncomingA2dp state with: " + BluetoothDeviceProfileState.this.getCurrentMessage().what);
            this.mCommand = BluetoothDeviceProfileState.this.getCurrentMessage().what;
            if ((this.mCommand != 4) && (this.mCommand != 53))
                Log.e("BluetoothDeviceProfileState", "Error: IncomingA2DP state with command:" + this.mCommand);
            this.mStatus = BluetoothDeviceProfileState.this.processCommand(this.mCommand);
            if (!this.mStatus)
            {
                BluetoothDeviceProfileState.this.sendMessage(102);
                BluetoothDeviceProfileState.this.mService.sendProfileStateMessage(1, 100);
            }
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool1 = false;
            BluetoothDeviceProfileState.this.log("IncomingA2dp State->Processing Message: " + paramMessage.what);
            switch (paramMessage.what)
            {
            default:
                return bool1;
            case 1:
                BluetoothDeviceProfileState.this.deferMessage(paramMessage);
            case 4:
            case 6:
            case 51:
            case 53:
            case 55:
            case 2:
            case 104:
            case 105:
            case 3:
            case 50:
            case 52:
            case 5:
            case 54:
            case 56:
            case 100:
            case 101:
            case 103:
            case 102:
            }
            while (true)
            {
                bool1 = true;
                break;
                BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                continue;
                int i = paramMessage.arg1;
                BluetoothDeviceProfileState.access$6602(BluetoothDeviceProfileState.this, true);
                boolean bool2 = false;
                if (i == 1)
                    bool2 = true;
                BluetoothDeviceProfileState.this.setTrust(i);
                BluetoothDeviceProfileState.this.handleIncomingConnection(4, bool2);
                continue;
                if (!BluetoothDeviceProfileState.this.mConnectionAccessReplyReceived)
                {
                    BluetoothDeviceProfileState.this.handleIncomingConnection(4, false);
                    BluetoothDeviceProfileState.this.sendConnectionAccessRemovalIntent();
                    BluetoothDeviceProfileState.this.sendMessage(102);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mBondedDevice);
                }
            }
        }
    }

    private class OutgoingA2dp extends State
    {
        private int mCommand;
        private boolean mStatus = false;

        private OutgoingA2dp()
        {
        }

        public void enter()
        {
            Log.i("BluetoothDeviceProfileState", "Entering OutgoingA2dp state with: " + BluetoothDeviceProfileState.this.getCurrentMessage().what);
            this.mCommand = BluetoothDeviceProfileState.this.getCurrentMessage().what;
            if ((this.mCommand != 3) && (this.mCommand != 52))
                Log.e("BluetoothDeviceProfileState", "Error: OutgoingA2DP state with command:" + this.mCommand);
            this.mStatus = BluetoothDeviceProfileState.this.processCommand(this.mCommand);
            if (!this.mStatus)
            {
                BluetoothDeviceProfileState.this.sendMessage(102);
                BluetoothDeviceProfileState.this.mService.sendProfileStateMessage(1, 100);
            }
        }

        public boolean processMessage(Message paramMessage)
        {
            int i = 1;
            BluetoothDeviceProfileState.this.log("OutgoingA2dp State->Processing Message: " + paramMessage.what);
            Message localMessage = new Message();
            switch (paramMessage.what)
            {
            default:
                i = 0;
            case 3:
            case 53:
            case 1:
            case 2:
            case 4:
            case 50:
            case 51:
            case 52:
            case 5:
            case 54:
            case 6:
            case 55:
            case 56:
            case 100:
            case 101:
            case 103:
            case 102:
            }
            while (true)
            {
                return i;
                BluetoothDeviceProfileState.this.processCommand(i);
                if (this.mStatus)
                {
                    localMessage.what = this.mCommand;
                    BluetoothDeviceProfileState.this.deferMessage(localMessage);
                    continue;
                    BluetoothDeviceProfileState.this.processCommand(2);
                    if (this.mStatus)
                    {
                        localMessage.what = this.mCommand;
                        BluetoothDeviceProfileState.this.deferMessage(localMessage);
                        continue;
                        BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingA2dp);
                        continue;
                        BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                        continue;
                        if (this.mStatus)
                        {
                            localMessage.what = this.mCommand;
                            BluetoothDeviceProfileState.this.deferMessage(localMessage);
                            continue;
                            BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                            continue;
                            BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                            continue;
                            BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingHid);
                            if (this.mStatus)
                            {
                                localMessage.what = this.mCommand;
                                BluetoothDeviceProfileState.this.deferMessage(localMessage);
                                continue;
                                if (this.mStatus)
                                {
                                    localMessage.what = this.mCommand;
                                    BluetoothDeviceProfileState.this.deferMessage(localMessage);
                                    continue;
                                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                                    continue;
                                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mBondedDevice);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private class IncomingHandsfree extends State
    {
        private int mCommand;
        private boolean mStatus = false;

        private IncomingHandsfree()
        {
        }

        public void enter()
        {
            Log.i("BluetoothDeviceProfileState", "Entering IncomingHandsfree state with: " + BluetoothDeviceProfileState.this.getCurrentMessage().what);
            this.mCommand = BluetoothDeviceProfileState.this.getCurrentMessage().what;
            if ((this.mCommand != 2) && (this.mCommand != 51))
                Log.e("BluetoothDeviceProfileState", "Error: IncomingHandsfree state with command:" + this.mCommand);
            this.mStatus = BluetoothDeviceProfileState.this.processCommand(this.mCommand);
            if (!this.mStatus)
            {
                BluetoothDeviceProfileState.this.sendMessage(102);
                BluetoothDeviceProfileState.this.mService.sendProfileStateMessage(0, 100);
            }
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool1 = false;
            BluetoothDeviceProfileState.this.log("IncomingHandsfree State -> Processing Message: " + paramMessage.what);
            switch (paramMessage.what)
            {
            default:
                return bool1;
            case 1:
                BluetoothDeviceProfileState.this.deferMessage(paramMessage);
            case 6:
            case 51:
            case 53:
            case 55:
            case 2:
            case 104:
            case 105:
            case 4:
            case 3:
            case 50:
            case 52:
            case 5:
            case 54:
            case 56:
            case 100:
            case 101:
            case 103:
            case 102:
            }
            while (true)
            {
                bool1 = true;
                break;
                Log.e("BluetoothDeviceProfileState", "Error: Incoming connection with a pending incoming connection");
                continue;
                int i = paramMessage.arg1;
                BluetoothDeviceProfileState.access$6602(BluetoothDeviceProfileState.this, true);
                boolean bool2 = false;
                if (i == 1)
                    bool2 = true;
                BluetoothDeviceProfileState.this.setTrust(i);
                BluetoothDeviceProfileState.this.handleIncomingConnection(2, bool2);
                continue;
                if (!BluetoothDeviceProfileState.this.mConnectionAccessReplyReceived)
                {
                    BluetoothDeviceProfileState.this.handleIncomingConnection(2, false);
                    BluetoothDeviceProfileState.this.sendConnectionAccessRemovalIntent();
                    BluetoothDeviceProfileState.this.sendMessage(102);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mBondedDevice);
                }
            }
        }
    }

    private class OutgoingHandsfree extends State
    {
        private int mCommand;
        private boolean mStatus = false;

        private OutgoingHandsfree()
        {
        }

        public void enter()
        {
            Log.i("BluetoothDeviceProfileState", "Entering OutgoingHandsfree state with: " + BluetoothDeviceProfileState.this.getCurrentMessage().what);
            this.mCommand = BluetoothDeviceProfileState.this.getCurrentMessage().what;
            if ((this.mCommand != 1) && (this.mCommand != 50))
                Log.e("BluetoothDeviceProfileState", "Error: OutgoingHandsfree state with command:" + this.mCommand);
            this.mStatus = BluetoothDeviceProfileState.this.processCommand(this.mCommand);
            if (!this.mStatus)
            {
                BluetoothDeviceProfileState.this.sendMessage(102);
                BluetoothDeviceProfileState.this.mService.sendProfileStateMessage(0, 100);
            }
        }

        public boolean processMessage(Message paramMessage)
        {
            int i = 1;
            BluetoothDeviceProfileState.this.log("OutgoingHandsfree State -> Processing Message: " + paramMessage.what);
            Message localMessage = new Message();
            int j = paramMessage.what;
            switch (j)
            {
            default:
                i = 0;
            case 1:
            case 2:
            case 4:
            case 3:
            case 50:
            case 51:
            case 52:
            case 53:
            case 5:
            case 54:
            case 6:
            case 55:
            case 56:
            case 100:
            case 101:
            case 103:
            case 102:
            }
            while (true)
            {
                return i;
                if (j != this.mCommand)
                {
                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    continue;
                    if (this.mCommand == i)
                    {
                        BluetoothDeviceProfileState.this.cancelCommand(i);
                        BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingHandsfree);
                    }
                    else
                    {
                        BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                        continue;
                        BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingA2dp);
                        if (this.mStatus)
                        {
                            localMessage.what = this.mCommand;
                            BluetoothDeviceProfileState.this.deferMessage(localMessage);
                            continue;
                            BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                            continue;
                            if (this.mCommand == i)
                            {
                                BluetoothDeviceProfileState.this.cancelCommand(i);
                                BluetoothDeviceProfileState.this.processCommand(50);
                                continue;
                                BluetoothDeviceProfileState.this.cancelCommand(i);
                                continue;
                                BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                                continue;
                                if (this.mStatus)
                                {
                                    localMessage.what = this.mCommand;
                                    BluetoothDeviceProfileState.this.deferMessage(localMessage);
                                    continue;
                                    BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                                    continue;
                                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingHid);
                                    if (this.mStatus)
                                    {
                                        localMessage.what = this.mCommand;
                                        BluetoothDeviceProfileState.this.deferMessage(localMessage);
                                        continue;
                                        if (this.mStatus)
                                        {
                                            localMessage.what = this.mCommand;
                                            BluetoothDeviceProfileState.this.deferMessage(localMessage);
                                            continue;
                                            BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                                            continue;
                                            BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mBondedDevice);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private class BondedDevice extends State
    {
        private BondedDevice()
        {
        }

        public void enter()
        {
            Log.i("BluetoothDeviceProfileState", "Entering ACL Connected state with: " + BluetoothDeviceProfileState.this.getCurrentMessage().what);
            Message localMessage = new Message();
            localMessage.copyFrom(BluetoothDeviceProfileState.this.getCurrentMessage());
            BluetoothDeviceProfileState.this.sendMessageAtFrontOfQueue(localMessage);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            BluetoothDeviceProfileState.this.log("ACL Connected State -> Processing Message: " + paramMessage.what);
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 50:
            case 102:
            case 2:
            case 51:
            case 3:
            case 52:
            case 4:
            case 53:
            case 5:
            case 54:
            case 6:
            case 55:
            case 56:
            case 100:
            case 101:
            case 103:
            case -1:
            }
            while (true)
            {
                return bool;
                BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mOutgoingHandsfree);
                while (true)
                {
                    bool = true;
                    break;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingHandsfree);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingHandsfree);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mOutgoingA2dp);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingA2dp);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mOutgoingHid);
                    continue;
                    BluetoothDeviceProfileState.this.transitionTo(BluetoothDeviceProfileState.this.mIncomingHid);
                    continue;
                    BluetoothDeviceProfileState.this.processCommand(56);
                    continue;
                    if (BluetoothDeviceProfileState.this.mHeadsetState != 0)
                    {
                        BluetoothDeviceProfileState.this.sendMessage(50);
                        BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    }
                    else if (BluetoothDeviceProfileState.this.mA2dpState != 0)
                    {
                        BluetoothDeviceProfileState.this.sendMessage(52);
                        BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    }
                    else if (BluetoothDeviceProfileState.this.mService.getInputDeviceConnectionState(BluetoothDeviceProfileState.this.mDevice) != 0)
                    {
                        BluetoothDeviceProfileState.this.sendMessage(54);
                        BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                    }
                    else
                    {
                        BluetoothDeviceProfileState.this.processCommand(100);
                        continue;
                        if (!BluetoothDeviceProfileState.this.isPhoneDocked(BluetoothDeviceProfileState.this.mDevice))
                        {
                            if (BluetoothDeviceProfileState.this.mHeadsetService == null)
                                BluetoothDeviceProfileState.access$1402(BluetoothDeviceProfileState.this, true);
                            while (true)
                            {
                                if ((BluetoothDeviceProfileState.this.mA2dpService != null) && (BluetoothDeviceProfileState.this.mA2dpService.getPriority(BluetoothDeviceProfileState.this.mDevice) == 1000))
                                {
                                    BluetoothA2dpService localBluetoothA2dpService = BluetoothDeviceProfileState.this.mA2dpService;
                                    int[] arrayOfInt2 = new int[3];
                                    arrayOfInt2[0] = 2;
                                    arrayOfInt2[1] = 1;
                                    arrayOfInt2[2] = 3;
                                    if (localBluetoothA2dpService.getDevicesMatchingConnectionStates(arrayOfInt2).size() == 0)
                                        BluetoothDeviceProfileState.this.mA2dpService.connect(BluetoothDeviceProfileState.this.mDevice);
                                }
                                if (BluetoothDeviceProfileState.this.mService.getInputDevicePriority(BluetoothDeviceProfileState.this.mDevice) != 1000)
                                    break;
                                BluetoothDeviceProfileState.this.mService.connectInputDevice(BluetoothDeviceProfileState.this.mDevice);
                                break;
                                if (BluetoothDeviceProfileState.this.mHeadsetService.getPriority(BluetoothDeviceProfileState.this.mDevice) == 1000)
                                {
                                    BluetoothHeadset localBluetoothHeadset = BluetoothDeviceProfileState.this.mHeadsetService;
                                    int[] arrayOfInt1 = new int[3];
                                    arrayOfInt1[0] = 2;
                                    arrayOfInt1[1] = 1;
                                    arrayOfInt1[2] = 3;
                                    if (localBluetoothHeadset.getDevicesMatchingConnectionStates(arrayOfInt1).size() == 0)
                                        BluetoothDeviceProfileState.this.mHeadsetService.connect(BluetoothDeviceProfileState.this.mDevice);
                                }
                            }
                            if (!BluetoothDeviceProfileState.this.isPhoneDocked(BluetoothDeviceProfileState.this.mDevice))
                                if (paramMessage.arg1 == 3)
                                {
                                    if ((BluetoothDeviceProfileState.this.mA2dpService != null) && (BluetoothDeviceProfileState.this.mA2dpService.getConnectedDevices().size() == 0))
                                    {
                                        Log.i("BluetoothDeviceProfileState", "A2dp:Connect Other Profiles");
                                        BluetoothDeviceProfileState.this.mA2dpService.connect(BluetoothDeviceProfileState.this.mDevice);
                                    }
                                }
                                else if (paramMessage.arg1 == 1)
                                    if (BluetoothDeviceProfileState.this.mHeadsetService == null)
                                    {
                                        BluetoothDeviceProfileState.this.deferMessage(paramMessage);
                                    }
                                    else if (BluetoothDeviceProfileState.this.mHeadsetService.getConnectedDevices().size() == 0)
                                    {
                                        Log.i("BluetoothDeviceProfileState", "Headset:Connect Other Profiles");
                                        BluetoothDeviceProfileState.this.mHeadsetService.connect(BluetoothDeviceProfileState.this.mDevice);
                                    }
                        }
                    }
                }
                BluetoothDeviceProfileState.this.mContext.unregisterReceiver(BluetoothDeviceProfileState.this.mBroadcastReceiver);
                BluetoothDeviceProfileState.access$4202(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.this.mAdapter.closeProfileProxy(1, BluetoothDeviceProfileState.this.mHeadsetService);
                BluetoothDeviceProfileState.access$4402(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$2202(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$4502(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.this.mPbapService.close();
                BluetoothDeviceProfileState.access$1502(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$3302(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$3102(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$2402(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$2202(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$2902(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$2702(BluetoothDeviceProfileState.this, null);
                BluetoothDeviceProfileState.access$4602(BluetoothDeviceProfileState.this, null);
            }
        }
    }

    private class PbapServiceListener
        implements BluetoothPbap.ServiceListener
    {
        public PbapServiceListener()
        {
            BluetoothDeviceProfileState.access$1502(BluetoothDeviceProfileState.this, new BluetoothPbap(BluetoothDeviceProfileState.this.mContext, this));
        }

        public void onServiceConnected()
        {
            synchronized (BluetoothDeviceProfileState.this)
            {
                BluetoothDeviceProfileState.access$1702(BluetoothDeviceProfileState.this, true);
                return;
            }
        }

        public void onServiceDisconnected()
        {
            synchronized (BluetoothDeviceProfileState.this)
            {
                BluetoothDeviceProfileState.access$1702(BluetoothDeviceProfileState.this, false);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothDeviceProfileState
 * JD-Core Version:        0.6.2
 */