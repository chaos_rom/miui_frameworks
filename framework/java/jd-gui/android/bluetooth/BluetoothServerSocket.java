package android.bluetooth;

import android.os.Handler;
import android.os.Message;
import java.io.Closeable;
import java.io.IOException;

public final class BluetoothServerSocket
    implements Closeable
{
    private final int mChannel;
    private Handler mHandler;
    private int mMessage;
    final BluetoothSocket mSocket;

    BluetoothServerSocket(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2)
        throws IOException
    {
        this.mChannel = paramInt2;
        this.mSocket = new BluetoothSocket(paramInt1, -1, paramBoolean1, paramBoolean2, null, paramInt2, null);
    }

    public BluetoothSocket accept()
        throws IOException
    {
        return accept(-1);
    }

    public BluetoothSocket accept(int paramInt)
        throws IOException
    {
        return this.mSocket.accept(paramInt);
    }

    public void close()
        throws IOException
    {
        try
        {
            if (this.mHandler != null)
                this.mHandler.obtainMessage(this.mMessage).sendToTarget();
            this.mSocket.close();
            return;
        }
        finally
        {
        }
    }

    public int getChannel()
    {
        return this.mChannel;
    }

    /** @deprecated */
    void setCloseHandler(Handler paramHandler, int paramInt)
    {
        try
        {
            this.mHandler = paramHandler;
            this.mMessage = paramInt;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothServerSocket
 * JD-Core Version:        0.6.2
 */