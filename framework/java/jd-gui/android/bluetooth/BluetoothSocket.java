package android.bluetooth;

import android.os.ParcelUuid;
import android.os.RemoteException;
import android.util.Log;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

public final class BluetoothSocket
    implements Closeable
{
    static final int EADDRINUSE = 98;
    static final int EBADFD = 77;
    public static final int MAX_RFCOMM_CHANNEL = 30;
    private static final String TAG = "BluetoothSocket";
    static final int TYPE_L2CAP = 3;
    static final int TYPE_RFCOMM = 1;
    static final int TYPE_SCO = 2;
    private final String mAddress;
    private final boolean mAuth;
    private final BluetoothDevice mDevice;
    private final boolean mEncrypt;
    private final BluetoothInputStream mInputStream;
    private final ReentrantReadWriteLock mLock;
    private final BluetoothOutputStream mOutputStream;
    private int mPort;
    private final SdpHelper mSdp;
    private int mSocketData;
    private SocketState mSocketState;
    private final int mType;

    BluetoothSocket(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, BluetoothDevice paramBluetoothDevice, int paramInt3, ParcelUuid paramParcelUuid)
        throws IOException
    {
        if ((paramInt1 == 1) && (paramParcelUuid == null) && (paramInt2 == -1) && ((paramInt3 < 1) || (paramInt3 > 30)))
            throw new IOException("Invalid RFCOMM channel: " + paramInt3);
        if (paramParcelUuid == null)
        {
            this.mPort = paramInt3;
            this.mSdp = null;
            this.mType = paramInt1;
            this.mAuth = paramBoolean1;
            this.mEncrypt = paramBoolean2;
            this.mDevice = paramBluetoothDevice;
            if (paramBluetoothDevice != null)
                break label186;
            this.mAddress = null;
            label109: if (paramInt2 != -1)
                break label198;
            initSocketNative();
        }
        while (true)
        {
            this.mInputStream = new BluetoothInputStream(this);
            this.mOutputStream = new BluetoothOutputStream(this);
            this.mSocketState = SocketState.INIT;
            this.mLock = new ReentrantReadWriteLock();
            return;
            this.mSdp = new SdpHelper(paramBluetoothDevice, paramParcelUuid);
            this.mPort = -1;
            break;
            label186: this.mAddress = paramBluetoothDevice.getAddress();
            break label109;
            label198: initSocketFromFdNative(paramInt2);
        }
    }

    private BluetoothSocket(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, String paramString, int paramInt3)
        throws IOException
    {
        this(paramInt1, paramInt2, paramBoolean1, paramBoolean2, new BluetoothDevice(paramString), paramInt3, null);
    }

    private native void abortNative()
        throws IOException;

    private native BluetoothSocket acceptNative(int paramInt)
        throws IOException;

    private native int availableNative()
        throws IOException;

    private native int bindListenNative();

    private native void connectNative()
        throws IOException;

    private native void destroyNative()
        throws IOException;

    private native void initSocketFromFdNative(int paramInt)
        throws IOException;

    private native void initSocketNative()
        throws IOException;

    private native int readNative(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException;

    private native int writeNative(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException;

    BluetoothSocket accept(int paramInt)
        throws IOException
    {
        this.mLock.readLock().lock();
        try
        {
            if (this.mSocketState == SocketState.CLOSED)
                throw new IOException("socket closed");
        }
        finally
        {
            this.mLock.readLock().unlock();
        }
        BluetoothSocket localBluetoothSocket = acceptNative(paramInt);
        this.mSocketState = SocketState.CONNECTED;
        this.mLock.readLock().unlock();
        return localBluetoothSocket;
    }

    int available()
        throws IOException
    {
        this.mLock.readLock().lock();
        try
        {
            if (this.mSocketState == SocketState.CLOSED)
                throw new IOException("socket closed");
        }
        finally
        {
            this.mLock.readLock().unlock();
        }
        int i = availableNative();
        this.mLock.readLock().unlock();
        return i;
    }

    int bindListen()
    {
        this.mLock.readLock().lock();
        try
        {
            SocketState localSocketState1 = this.mSocketState;
            SocketState localSocketState2 = SocketState.CLOSED;
            int j;
            if (localSocketState1 == localSocketState2)
                j = 77;
            for (ReentrantReadWriteLock.ReadLock localReadLock = this.mLock.readLock(); ; localReadLock = this.mLock.readLock())
            {
                localReadLock.unlock();
                return j;
                int i = bindListenNative();
                j = i;
            }
        }
        finally
        {
            this.mLock.readLock().unlock();
        }
    }

    // ERROR //
    public void close()
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 113	android/bluetooth/BluetoothSocket:mLock	Ljava/util/concurrent/locks/ReentrantReadWriteLock;
        //     4: invokevirtual 145	java/util/concurrent/locks/ReentrantReadWriteLock:readLock	()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
        //     7: invokevirtual 150	java/util/concurrent/locks/ReentrantReadWriteLock$ReadLock:lock	()V
        //     10: aload_0
        //     11: getfield 108	android/bluetooth/BluetoothSocket:mSocketState	Landroid/bluetooth/BluetoothSocket$SocketState;
        //     14: astore_2
        //     15: getstatic 153	android/bluetooth/BluetoothSocket$SocketState:CLOSED	Landroid/bluetooth/BluetoothSocket$SocketState;
        //     18: astore_3
        //     19: aload_2
        //     20: aload_3
        //     21: if_acmpne +14 -> 35
        //     24: aload_0
        //     25: getfield 113	android/bluetooth/BluetoothSocket:mLock	Ljava/util/concurrent/locks/ReentrantReadWriteLock;
        //     28: invokevirtual 145	java/util/concurrent/locks/ReentrantReadWriteLock:readLock	()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
        //     31: invokevirtual 158	java/util/concurrent/locks/ReentrantReadWriteLock$ReadLock:unlock	()V
        //     34: return
        //     35: aload_0
        //     36: getfield 78	android/bluetooth/BluetoothSocket:mSdp	Landroid/bluetooth/BluetoothSocket$SdpHelper;
        //     39: ifnull +10 -> 49
        //     42: aload_0
        //     43: getfield 78	android/bluetooth/BluetoothSocket:mSdp	Landroid/bluetooth/BluetoothSocket$SdpHelper;
        //     46: invokevirtual 173	android/bluetooth/BluetoothSocket$SdpHelper:cancel	()V
        //     49: aload_0
        //     50: invokespecial 175	android/bluetooth/BluetoothSocket:abortNative	()V
        //     53: aload_0
        //     54: getfield 113	android/bluetooth/BluetoothSocket:mLock	Ljava/util/concurrent/locks/ReentrantReadWriteLock;
        //     57: invokevirtual 145	java/util/concurrent/locks/ReentrantReadWriteLock:readLock	()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
        //     60: invokevirtual 158	java/util/concurrent/locks/ReentrantReadWriteLock$ReadLock:unlock	()V
        //     63: aload_0
        //     64: getfield 113	android/bluetooth/BluetoothSocket:mLock	Ljava/util/concurrent/locks/ReentrantReadWriteLock;
        //     67: invokevirtual 179	java/util/concurrent/locks/ReentrantReadWriteLock:writeLock	()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;
        //     70: invokevirtual 182	java/util/concurrent/locks/ReentrantReadWriteLock$WriteLock:lock	()V
        //     73: aload_0
        //     74: getstatic 153	android/bluetooth/BluetoothSocket$SocketState:CLOSED	Landroid/bluetooth/BluetoothSocket$SocketState;
        //     77: putfield 108	android/bluetooth/BluetoothSocket:mSocketState	Landroid/bluetooth/BluetoothSocket$SocketState;
        //     80: aload_0
        //     81: invokespecial 184	android/bluetooth/BluetoothSocket:destroyNative	()V
        //     84: aload_0
        //     85: getfield 113	android/bluetooth/BluetoothSocket:mLock	Ljava/util/concurrent/locks/ReentrantReadWriteLock;
        //     88: invokevirtual 179	java/util/concurrent/locks/ReentrantReadWriteLock:writeLock	()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;
        //     91: invokevirtual 185	java/util/concurrent/locks/ReentrantReadWriteLock$WriteLock:unlock	()V
        //     94: goto -60 -> 34
        //     97: astore_1
        //     98: aload_0
        //     99: getfield 113	android/bluetooth/BluetoothSocket:mLock	Ljava/util/concurrent/locks/ReentrantReadWriteLock;
        //     102: invokevirtual 145	java/util/concurrent/locks/ReentrantReadWriteLock:readLock	()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
        //     105: invokevirtual 158	java/util/concurrent/locks/ReentrantReadWriteLock$ReadLock:unlock	()V
        //     108: aload_1
        //     109: athrow
        //     110: astore 4
        //     112: aload_0
        //     113: getfield 113	android/bluetooth/BluetoothSocket:mLock	Ljava/util/concurrent/locks/ReentrantReadWriteLock;
        //     116: invokevirtual 179	java/util/concurrent/locks/ReentrantReadWriteLock:writeLock	()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;
        //     119: invokevirtual 185	java/util/concurrent/locks/ReentrantReadWriteLock$WriteLock:unlock	()V
        //     122: aload 4
        //     124: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     10	19	97	finally
        //     35	53	97	finally
        //     73	84	110	finally
    }

    public void connect()
        throws IOException
    {
        this.mLock.readLock().lock();
        try
        {
            if (this.mSocketState == SocketState.CLOSED)
                throw new IOException("socket closed");
        }
        finally
        {
            this.mLock.readLock().unlock();
        }
        if (this.mSdp != null)
            this.mPort = this.mSdp.doSdp();
        connectNative();
        this.mSocketState = SocketState.CONNECTED;
        this.mLock.readLock().unlock();
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            close();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public InputStream getInputStream()
        throws IOException
    {
        return this.mInputStream;
    }

    public OutputStream getOutputStream()
        throws IOException
    {
        return this.mOutputStream;
    }

    public BluetoothDevice getRemoteDevice()
    {
        return this.mDevice;
    }

    public boolean isConnected()
    {
        if (this.mSocketState == SocketState.CONNECTED);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        this.mLock.readLock().lock();
        try
        {
            if (this.mSocketState == SocketState.CLOSED)
                throw new IOException("socket closed");
        }
        finally
        {
            this.mLock.readLock().unlock();
        }
        int i = readNative(paramArrayOfByte, paramInt1, paramInt2);
        this.mLock.readLock().unlock();
        return i;
    }

    native void throwErrnoNative(int paramInt)
        throws IOException;

    int write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        this.mLock.readLock().lock();
        try
        {
            if (this.mSocketState == SocketState.CLOSED)
                throw new IOException("socket closed");
        }
        finally
        {
            this.mLock.readLock().unlock();
        }
        int i = writeNative(paramArrayOfByte, paramInt1, paramInt2);
        this.mLock.readLock().unlock();
        return i;
    }

    private static class SdpHelper extends IBluetoothCallback.Stub
    {
        private boolean canceled;
        private int channel;
        private final BluetoothDevice device;
        private final IBluetooth service = BluetoothDevice.getService();
        private final ParcelUuid uuid;

        public SdpHelper(BluetoothDevice paramBluetoothDevice, ParcelUuid paramParcelUuid)
        {
            this.device = paramBluetoothDevice;
            this.uuid = paramParcelUuid;
            this.canceled = false;
        }

        /** @deprecated */
        public void cancel()
        {
            try
            {
                if (!this.canceled)
                {
                    this.canceled = true;
                    this.channel = -1;
                    notifyAll();
                }
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public int doSdp()
            throws IOException
        {
            try
            {
                if (this.canceled)
                    throw new IOException("Service discovery canceled");
            }
            finally
            {
            }
            this.channel = -1;
            int i = 0;
            try
            {
                boolean bool = this.service.fetchRemoteUuids(this.device.getAddress(), this.uuid, this);
                i = bool;
                if (i == 0)
                    throw new IOException("Unable to start Service Discovery");
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("BluetoothSocket", "", localRemoteException);
            }
            try
            {
                wait(12000L);
                label92: if (this.canceled)
                    throw new IOException("Service discovery canceled");
                if (this.channel < 1)
                    throw new IOException("Service discovery failed");
                int j = this.channel;
                return j;
            }
            catch (InterruptedException localInterruptedException)
            {
                break label92;
            }
        }

        /** @deprecated */
        public void onRfcommChannelFound(int paramInt)
        {
            try
            {
                if (!this.canceled)
                {
                    this.channel = paramInt;
                    notifyAll();
                }
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }

    private static enum SocketState
    {
        static
        {
            CONNECTED = new SocketState("CONNECTED", 1);
            CLOSED = new SocketState("CLOSED", 2);
            SocketState[] arrayOfSocketState = new SocketState[3];
            arrayOfSocketState[0] = INIT;
            arrayOfSocketState[1] = CONNECTED;
            arrayOfSocketState[2] = CLOSED;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothSocket
 * JD-Core Version:        0.6.2
 */