package android.bluetooth;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class BluetoothPbap
{
    private static final boolean DBG = false;
    public static final String PBAP_PREVIOUS_STATE = "android.bluetooth.pbap.intent.PBAP_PREVIOUS_STATE";
    public static final String PBAP_STATE = "android.bluetooth.pbap.intent.PBAP_STATE";
    public static final String PBAP_STATE_CHANGED_ACTION = "android.bluetooth.pbap.intent.action.PBAP_STATE_CHANGED";
    public static final int RESULT_CANCELED = 2;
    public static final int RESULT_FAILURE = 0;
    public static final int RESULT_SUCCESS = 1;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_ERROR = -1;
    private static final String TAG = "BluetoothPbap";
    private ServiceConnection mConnection = new ServiceConnection()
    {
        public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
        {
            BluetoothPbap.access$002(BluetoothPbap.this, IBluetoothPbap.Stub.asInterface(paramAnonymousIBinder));
            if (BluetoothPbap.this.mServiceListener != null)
                BluetoothPbap.this.mServiceListener.onServiceConnected();
        }

        public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
        {
            BluetoothPbap.access$002(BluetoothPbap.this, null);
            if (BluetoothPbap.this.mServiceListener != null)
                BluetoothPbap.this.mServiceListener.onServiceDisconnected();
        }
    };
    private final Context mContext;
    private IBluetoothPbap mService;
    private ServiceListener mServiceListener;

    public BluetoothPbap(Context paramContext, ServiceListener paramServiceListener)
    {
        this.mContext = paramContext;
        this.mServiceListener = paramServiceListener;
        if (!paramContext.bindService(new Intent(IBluetoothPbap.class.getName()), this.mConnection, 0))
            Log.e("BluetoothPbap", "Could not bind to Bluetooth Pbap Service");
    }

    public static boolean doesClassMatchSink(BluetoothClass paramBluetoothClass)
    {
        switch (paramBluetoothClass.getDeviceClass())
        {
        default:
        case 256:
        case 260:
        case 264:
        case 268:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothPbap", paramString);
    }

    /** @deprecated */
    public void close()
    {
        try
        {
            if (this.mConnection != null)
            {
                this.mContext.unbindService(this.mConnection);
                this.mConnection = null;
            }
            this.mServiceListener = null;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean disconnect()
    {
        boolean bool;
        if (this.mService != null)
            try
            {
                this.mService.disconnect();
                bool = true;
                return bool;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPbap", localRemoteException.toString());
            }
        while (true)
        {
            bool = false;
            break;
            Log.w("BluetoothPbap", "Proxy not attached to service");
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            close();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public BluetoothDevice getClient()
    {
        BluetoothDevice localBluetoothDevice1;
        if (this.mService != null)
            try
            {
                BluetoothDevice localBluetoothDevice2 = this.mService.getClient();
                localBluetoothDevice1 = localBluetoothDevice2;
                return localBluetoothDevice1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPbap", localRemoteException.toString());
            }
        while (true)
        {
            localBluetoothDevice1 = null;
            break;
            Log.w("BluetoothPbap", "Proxy not attached to service");
        }
    }

    public int getState()
    {
        int i;
        if (this.mService != null)
            try
            {
                int j = this.mService.getState();
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPbap", localRemoteException.toString());
            }
        while (true)
        {
            i = -1;
            break;
            Log.w("BluetoothPbap", "Proxy not attached to service");
        }
    }

    public boolean isConnected(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1;
        if (this.mService != null)
            try
            {
                boolean bool2 = this.mService.isConnected(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPbap", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothPbap", "Proxy not attached to service");
        }
    }

    public static abstract interface ServiceListener
    {
        public abstract void onServiceConnected();

        public abstract void onServiceDisconnected();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothPbap
 * JD-Core Version:        0.6.2
 */