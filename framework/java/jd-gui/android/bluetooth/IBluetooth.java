package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.ParcelUuid;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IBluetooth extends IInterface
{
    public abstract int addRfcommServiceRecord(String paramString, ParcelUuid paramParcelUuid, int paramInt, IBinder paramIBinder)
        throws RemoteException;

    public abstract boolean allowIncomingProfileConnect(BluetoothDevice paramBluetoothDevice, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean cancelBondProcess(String paramString)
        throws RemoteException;

    public abstract boolean cancelDiscovery()
        throws RemoteException;

    public abstract boolean cancelPairingUserInput(String paramString)
        throws RemoteException;

    public abstract boolean changeApplicationBluetoothState(boolean paramBoolean, IBluetoothStateChangeCallback paramIBluetoothStateChangeCallback, IBinder paramIBinder)
        throws RemoteException;

    public abstract boolean connectChannelToSink(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
        throws RemoteException;

    public abstract boolean connectChannelToSource(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
        throws RemoteException;

    public abstract boolean connectHeadset(String paramString)
        throws RemoteException;

    public abstract boolean connectInputDevice(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean connectPanDevice(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean createBond(String paramString)
        throws RemoteException;

    public abstract boolean createBondOutOfBand(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
        throws RemoteException;

    public abstract boolean disable(boolean paramBoolean)
        throws RemoteException;

    public abstract boolean disconnectChannel(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
        throws RemoteException;

    public abstract boolean disconnectHeadset(String paramString)
        throws RemoteException;

    public abstract boolean disconnectInputDevice(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean disconnectPanDevice(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean enable()
        throws RemoteException;

    public abstract boolean enableNoAutoConnect()
        throws RemoteException;

    public abstract boolean fetchRemoteUuids(String paramString, ParcelUuid paramParcelUuid, IBluetoothCallback paramIBluetoothCallback)
        throws RemoteException;

    public abstract int getAdapterConnectionState()
        throws RemoteException;

    public abstract String getAddress()
        throws RemoteException;

    public abstract int getBluetoothState()
        throws RemoteException;

    public abstract int getBondState(String paramString)
        throws RemoteException;

    public abstract List<BluetoothDevice> getConnectedHealthDevices()
        throws RemoteException;

    public abstract List<BluetoothDevice> getConnectedInputDevices()
        throws RemoteException;

    public abstract List<BluetoothDevice> getConnectedPanDevices()
        throws RemoteException;

    public abstract int getDiscoverableTimeout()
        throws RemoteException;

    public abstract int getHealthDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract List<BluetoothDevice> getHealthDevicesMatchingConnectionStates(int[] paramArrayOfInt)
        throws RemoteException;

    public abstract int getInputDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract int getInputDevicePriority(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract List<BluetoothDevice> getInputDevicesMatchingConnectionStates(int[] paramArrayOfInt)
        throws RemoteException;

    public abstract ParcelFileDescriptor getMainChannelFd(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
        throws RemoteException;

    public abstract String getName()
        throws RemoteException;

    public abstract int getPanDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract List<BluetoothDevice> getPanDevicesMatchingConnectionStates(int[] paramArrayOfInt)
        throws RemoteException;

    public abstract int getProfileConnectionState(int paramInt)
        throws RemoteException;

    public abstract String getRemoteAlias(String paramString)
        throws RemoteException;

    public abstract int getRemoteClass(String paramString)
        throws RemoteException;

    public abstract String getRemoteName(String paramString)
        throws RemoteException;

    public abstract int getRemoteServiceChannel(String paramString, ParcelUuid paramParcelUuid)
        throws RemoteException;

    public abstract ParcelUuid[] getRemoteUuids(String paramString)
        throws RemoteException;

    public abstract int getScanMode()
        throws RemoteException;

    public abstract boolean getTrustState(String paramString)
        throws RemoteException;

    public abstract ParcelUuid[] getUuids()
        throws RemoteException;

    public abstract boolean isBluetoothDock(String paramString)
        throws RemoteException;

    public abstract boolean isDiscovering()
        throws RemoteException;

    public abstract boolean isEnabled()
        throws RemoteException;

    public abstract boolean isTetheringOn()
        throws RemoteException;

    public abstract String[] listBonds()
        throws RemoteException;

    public abstract boolean notifyIncomingConnection(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract byte[] readOutOfBandData()
        throws RemoteException;

    public abstract boolean registerAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, IBluetoothHealthCallback paramIBluetoothHealthCallback)
        throws RemoteException;

    public abstract boolean removeBond(String paramString)
        throws RemoteException;

    public abstract void removeServiceRecord(int paramInt)
        throws RemoteException;

    public abstract void sendConnectionStateChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract void setBluetoothTethering(boolean paramBoolean)
        throws RemoteException;

    public abstract boolean setDeviceOutOfBandData(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
        throws RemoteException;

    public abstract boolean setDiscoverableTimeout(int paramInt)
        throws RemoteException;

    public abstract boolean setInputDevicePriority(BluetoothDevice paramBluetoothDevice, int paramInt)
        throws RemoteException;

    public abstract boolean setName(String paramString)
        throws RemoteException;

    public abstract boolean setPairingConfirmation(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean setPasskey(String paramString, int paramInt)
        throws RemoteException;

    public abstract boolean setPin(String paramString, byte[] paramArrayOfByte)
        throws RemoteException;

    public abstract boolean setRemoteAlias(String paramString1, String paramString2)
        throws RemoteException;

    public abstract boolean setRemoteOutOfBandData(String paramString)
        throws RemoteException;

    public abstract boolean setScanMode(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract boolean setTrust(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean startDiscovery()
        throws RemoteException;

    public abstract boolean unregisterAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBluetooth
    {
        private static final String DESCRIPTOR = "android.bluetooth.IBluetooth";
        static final int TRANSACTION_addRfcommServiceRecord = 43;
        static final int TRANSACTION_allowIncomingProfileConnect = 45;
        static final int TRANSACTION_cancelBondProcess = 23;
        static final int TRANSACTION_cancelDiscovery = 15;
        static final int TRANSACTION_cancelPairingUserInput = 39;
        static final int TRANSACTION_changeApplicationBluetoothState = 20;
        static final int TRANSACTION_connectChannelToSink = 66;
        static final int TRANSACTION_connectChannelToSource = 65;
        static final int TRANSACTION_connectHeadset = 46;
        static final int TRANSACTION_connectInputDevice = 49;
        static final int TRANSACTION_connectPanDevice = 61;
        static final int TRANSACTION_createBond = 21;
        static final int TRANSACTION_createBondOutOfBand = 22;
        static final int TRANSACTION_disable = 5;
        static final int TRANSACTION_disconnectChannel = 67;
        static final int TRANSACTION_disconnectHeadset = 47;
        static final int TRANSACTION_disconnectInputDevice = 50;
        static final int TRANSACTION_disconnectPanDevice = 62;
        static final int TRANSACTION_enable = 3;
        static final int TRANSACTION_enableNoAutoConnect = 4;
        static final int TRANSACTION_fetchRemoteUuids = 33;
        static final int TRANSACTION_getAdapterConnectionState = 18;
        static final int TRANSACTION_getAddress = 6;
        static final int TRANSACTION_getBluetoothState = 2;
        static final int TRANSACTION_getBondState = 26;
        static final int TRANSACTION_getConnectedHealthDevices = 69;
        static final int TRANSACTION_getConnectedInputDevices = 51;
        static final int TRANSACTION_getConnectedPanDevices = 59;
        static final int TRANSACTION_getDiscoverableTimeout = 12;
        static final int TRANSACTION_getHealthDeviceConnectionState = 71;
        static final int TRANSACTION_getHealthDevicesMatchingConnectionStates = 70;
        static final int TRANSACTION_getInputDeviceConnectionState = 53;
        static final int TRANSACTION_getInputDevicePriority = 55;
        static final int TRANSACTION_getInputDevicesMatchingConnectionStates = 52;
        static final int TRANSACTION_getMainChannelFd = 68;
        static final int TRANSACTION_getName = 7;
        static final int TRANSACTION_getPanDeviceConnectionState = 58;
        static final int TRANSACTION_getPanDevicesMatchingConnectionStates = 60;
        static final int TRANSACTION_getProfileConnectionState = 19;
        static final int TRANSACTION_getRemoteAlias = 29;
        static final int TRANSACTION_getRemoteClass = 31;
        static final int TRANSACTION_getRemoteName = 28;
        static final int TRANSACTION_getRemoteServiceChannel = 34;
        static final int TRANSACTION_getRemoteUuids = 32;
        static final int TRANSACTION_getScanMode = 10;
        static final int TRANSACTION_getTrustState = 41;
        static final int TRANSACTION_getUuids = 9;
        static final int TRANSACTION_isBluetoothDock = 42;
        static final int TRANSACTION_isDiscovering = 16;
        static final int TRANSACTION_isEnabled = 1;
        static final int TRANSACTION_isTetheringOn = 56;
        static final int TRANSACTION_listBonds = 25;
        static final int TRANSACTION_notifyIncomingConnection = 48;
        static final int TRANSACTION_readOutOfBandData = 17;
        static final int TRANSACTION_registerAppConfiguration = 63;
        static final int TRANSACTION_removeBond = 24;
        static final int TRANSACTION_removeServiceRecord = 44;
        static final int TRANSACTION_sendConnectionStateChange = 72;
        static final int TRANSACTION_setBluetoothTethering = 57;
        static final int TRANSACTION_setDeviceOutOfBandData = 27;
        static final int TRANSACTION_setDiscoverableTimeout = 13;
        static final int TRANSACTION_setInputDevicePriority = 54;
        static final int TRANSACTION_setName = 8;
        static final int TRANSACTION_setPairingConfirmation = 37;
        static final int TRANSACTION_setPasskey = 36;
        static final int TRANSACTION_setPin = 35;
        static final int TRANSACTION_setRemoteAlias = 30;
        static final int TRANSACTION_setRemoteOutOfBandData = 38;
        static final int TRANSACTION_setScanMode = 11;
        static final int TRANSACTION_setTrust = 40;
        static final int TRANSACTION_startDiscovery = 14;
        static final int TRANSACTION_unregisterAppConfiguration = 64;

        public Stub()
        {
            attachInterface(this, "android.bluetooth.IBluetooth");
        }

        public static IBluetooth asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.bluetooth.IBluetooth");
                if ((localIInterface != null) && ((localIInterface instanceof IBluetooth)))
                    localObject = (IBluetooth)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
                while (true)
                {
                    return j;
                    paramParcel2.writeString("android.bluetooth.IBluetooth");
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool41 = isEnabled();
                    paramParcel2.writeNoException();
                    if (bool41)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    int i24 = getBluetoothState();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i24);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool40 = enable();
                    paramParcel2.writeNoException();
                    if (bool40)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool39 = enableNoAutoConnect();
                    paramParcel2.writeNoException();
                    if (bool39)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    int i23;
                    for (int i22 = j; ; i23 = 0)
                    {
                        boolean bool38 = disable(i22);
                        paramParcel2.writeNoException();
                        if (bool38)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str10 = getAddress();
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str10);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str9 = getName();
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str9);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool37 = setName(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool37)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    ParcelUuid[] arrayOfParcelUuid2 = getUuids();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedArray(arrayOfParcelUuid2, j);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    int i21 = getScanMode();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i21);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool36 = setScanMode(paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool36)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    int i20 = getDiscoverableTimeout();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i20);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool35 = setDiscoverableTimeout(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool35)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool34 = startDiscovery();
                    paramParcel2.writeNoException();
                    if (bool34)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool33 = cancelDiscovery();
                    paramParcel2.writeNoException();
                    if (bool33)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool32 = isDiscovering();
                    paramParcel2.writeNoException();
                    if (bool32)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    byte[] arrayOfByte = readOutOfBandData();
                    paramParcel2.writeNoException();
                    paramParcel2.writeByteArray(arrayOfByte);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    int i19 = getAdapterConnectionState();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i19);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    int i18 = getProfileConnectionState(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i18);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    int i17;
                    for (int i16 = j; ; i17 = 0)
                    {
                        boolean bool31 = changeApplicationBluetoothState(i16, IBluetoothStateChangeCallback.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readStrongBinder());
                        paramParcel2.writeNoException();
                        if (bool31)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool30 = createBond(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool30)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool29 = createBondOutOfBand(paramParcel1.readString(), paramParcel1.createByteArray(), paramParcel1.createByteArray());
                    paramParcel2.writeNoException();
                    if (bool29)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool28 = cancelBondProcess(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool28)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool27 = removeBond(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool27)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String[] arrayOfString = listBonds();
                    paramParcel2.writeNoException();
                    paramParcel2.writeStringArray(arrayOfString);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    int i15 = getBondState(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i15);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool26 = setDeviceOutOfBandData(paramParcel1.readString(), paramParcel1.createByteArray(), paramParcel1.createByteArray());
                    paramParcel2.writeNoException();
                    if (bool26)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str8 = getRemoteName(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str8);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str7 = getRemoteAlias(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str7);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool25 = setRemoteAlias(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool25)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    int i14 = getRemoteClass(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i14);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    ParcelUuid[] arrayOfParcelUuid1 = getRemoteUuids(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedArray(arrayOfParcelUuid1, j);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str6 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    for (ParcelUuid localParcelUuid3 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel(paramParcel1); ; localParcelUuid3 = null)
                    {
                        boolean bool24 = fetchRemoteUuids(str6, localParcelUuid3, IBluetoothCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                        paramParcel2.writeNoException();
                        if (bool24)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str5 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    for (ParcelUuid localParcelUuid2 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel(paramParcel1); ; localParcelUuid2 = null)
                    {
                        int i13 = getRemoteServiceChannel(str5, localParcelUuid2);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i13);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool23 = setPin(paramParcel1.readString(), paramParcel1.createByteArray());
                    paramParcel2.writeNoException();
                    if (bool23)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool22 = setPasskey(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool22)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str4 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    int i12;
                    for (int i11 = j; ; i12 = 0)
                    {
                        boolean bool21 = setPairingConfirmation(str4, i11);
                        paramParcel2.writeNoException();
                        if (bool21)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool20 = setRemoteOutOfBandData(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool20)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool19 = cancelPairingUserInput(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool19)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str3 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    int i10;
                    for (int i9 = j; ; i10 = 0)
                    {
                        boolean bool18 = setTrust(str3, i9);
                        paramParcel2.writeNoException();
                        if (bool18)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool17 = getTrustState(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool17)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool16 = isBluetoothDock(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool16)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str2 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    for (ParcelUuid localParcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel(paramParcel1); ; localParcelUuid1 = null)
                    {
                        int i8 = addRfcommServiceRecord(str2, localParcelUuid1, paramParcel1.readInt(), paramParcel1.readStrongBinder());
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i8);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    removeServiceRecord(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    BluetoothDevice localBluetoothDevice15;
                    if (paramParcel1.readInt() != 0)
                    {
                        localBluetoothDevice15 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label2378;
                    }
                    int i7;
                    for (int i6 = j; ; i7 = 0)
                    {
                        boolean bool15 = allowIncomingProfileConnect(localBluetoothDevice15, i6);
                        paramParcel2.writeNoException();
                        if (bool15)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                        localBluetoothDevice15 = null;
                        break label2329;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool14 = connectHeadset(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool14)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool13 = disconnectHeadset(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool13)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    String str1 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    int i5;
                    for (int i4 = j; ; i5 = 0)
                    {
                        boolean bool12 = notifyIncomingConnection(str1, i4);
                        paramParcel2.writeNoException();
                        if (bool12)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice14 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice14 = null)
                    {
                        boolean bool11 = connectInputDevice(localBluetoothDevice14);
                        paramParcel2.writeNoException();
                        if (bool11)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice13 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice13 = null)
                    {
                        boolean bool10 = disconnectInputDevice(localBluetoothDevice13);
                        paramParcel2.writeNoException();
                        if (bool10)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    List localList6 = getConnectedInputDevices();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList6);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    List localList5 = getInputDevicesMatchingConnectionStates(paramParcel1.createIntArray());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList5);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice12 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice12 = null)
                    {
                        int i3 = getInputDeviceConnectionState(localBluetoothDevice12);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i3);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice11 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice11 = null)
                    {
                        boolean bool9 = setInputDevicePriority(localBluetoothDevice11, paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (bool9)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice10 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice10 = null)
                    {
                        int i2 = getInputDevicePriority(localBluetoothDevice10);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i2);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    boolean bool8 = isTetheringOn();
                    paramParcel2.writeNoException();
                    if (bool8)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    int i1;
                    for (int n = j; ; i1 = 0)
                    {
                        setBluetoothTethering(n);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice9 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice9 = null)
                    {
                        int m = getPanDeviceConnectionState(localBluetoothDevice9);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(m);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    List localList4 = getConnectedPanDevices();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList4);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    List localList3 = getPanDevicesMatchingConnectionStates(paramParcel1.createIntArray());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList3);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice8 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice8 = null)
                    {
                        boolean bool7 = connectPanDevice(localBluetoothDevice8);
                        paramParcel2.writeNoException();
                        if (bool7)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice7 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice7 = null)
                    {
                        boolean bool6 = disconnectPanDevice(localBluetoothDevice7);
                        paramParcel2.writeNoException();
                        if (bool6)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration6 = (BluetoothHealthAppConfiguration)BluetoothHealthAppConfiguration.CREATOR.createFromParcel(paramParcel1); ; localBluetoothHealthAppConfiguration6 = null)
                    {
                        boolean bool5 = registerAppConfiguration(localBluetoothHealthAppConfiguration6, IBluetoothHealthCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                        paramParcel2.writeNoException();
                        if (bool5)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration5 = (BluetoothHealthAppConfiguration)BluetoothHealthAppConfiguration.CREATOR.createFromParcel(paramParcel1); ; localBluetoothHealthAppConfiguration5 = null)
                    {
                        boolean bool4 = unregisterAppConfiguration(localBluetoothHealthAppConfiguration5);
                        paramParcel2.writeNoException();
                        if (bool4)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    BluetoothDevice localBluetoothDevice6;
                    if (paramParcel1.readInt() != 0)
                    {
                        localBluetoothDevice6 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label3399;
                    }
                    for (BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration4 = (BluetoothHealthAppConfiguration)BluetoothHealthAppConfiguration.CREATOR.createFromParcel(paramParcel1); ; localBluetoothHealthAppConfiguration4 = null)
                    {
                        boolean bool3 = connectChannelToSource(localBluetoothDevice6, localBluetoothHealthAppConfiguration4);
                        paramParcel2.writeNoException();
                        if (bool3)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                        localBluetoothDevice6 = null;
                        break label3340;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    BluetoothDevice localBluetoothDevice5;
                    if (paramParcel1.readInt() != 0)
                    {
                        localBluetoothDevice5 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label3495;
                    }
                    for (BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration3 = (BluetoothHealthAppConfiguration)BluetoothHealthAppConfiguration.CREATOR.createFromParcel(paramParcel1); ; localBluetoothHealthAppConfiguration3 = null)
                    {
                        boolean bool2 = connectChannelToSink(localBluetoothDevice5, localBluetoothHealthAppConfiguration3, paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (bool2)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                        localBluetoothDevice5 = null;
                        break label3432;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    BluetoothDevice localBluetoothDevice4;
                    if (paramParcel1.readInt() != 0)
                    {
                        localBluetoothDevice4 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label3591;
                    }
                    label3591: for (BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration2 = (BluetoothHealthAppConfiguration)BluetoothHealthAppConfiguration.CREATOR.createFromParcel(paramParcel1); ; localBluetoothHealthAppConfiguration2 = null)
                    {
                        boolean bool1 = disconnectChannel(localBluetoothDevice4, localBluetoothHealthAppConfiguration2, paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (bool1)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                        localBluetoothDevice4 = null;
                        break label3528;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    BluetoothDevice localBluetoothDevice3;
                    if (paramParcel1.readInt() != 0)
                    {
                        localBluetoothDevice3 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label3687;
                    }
                    for (BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration1 = (BluetoothHealthAppConfiguration)BluetoothHealthAppConfiguration.CREATOR.createFromParcel(paramParcel1); ; localBluetoothHealthAppConfiguration1 = null)
                    {
                        ParcelFileDescriptor localParcelFileDescriptor = getMainChannelFd(localBluetoothDevice3, localBluetoothHealthAppConfiguration1);
                        paramParcel2.writeNoException();
                        if (localParcelFileDescriptor == null)
                            break label3693;
                        paramParcel2.writeInt(j);
                        localParcelFileDescriptor.writeToParcel(paramParcel2, j);
                        break;
                        localBluetoothDevice3 = null;
                        break label3624;
                    }
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    List localList2 = getConnectedHealthDevices();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList2);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                    List localList1 = getHealthDevicesMatchingConnectionStates(paramParcel1.createIntArray());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList1);
                }
            case 71:
                label2329: label2378: label3432: label3693: paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
                label3340: label3399: label3528: label3687: if (paramParcel1.readInt() != 0);
                label3495: label3624: for (BluetoothDevice localBluetoothDevice2 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice2 = null)
                {
                    int k = getHealthDeviceConnectionState(localBluetoothDevice2);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(k);
                    break;
                }
            case 72:
            }
            paramParcel1.enforceInterface("android.bluetooth.IBluetooth");
            if (paramParcel1.readInt() != 0);
            for (BluetoothDevice localBluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice1 = null)
            {
                sendConnectionStateChange(localBluetoothDevice1, paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                break;
            }
        }

        private static class Proxy
            implements IBluetooth
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public int addRfcommServiceRecord(String paramString, ParcelUuid paramParcelUuid, int paramInt, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    if (paramParcelUuid != null)
                    {
                        localParcel1.writeInt(1);
                        paramParcelUuid.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        localParcel1.writeStrongBinder(paramIBinder);
                        this.mRemote.transact(43, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean allowIncomingProfileConnect(BluetoothDevice paramBluetoothDevice, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            break label127;
                            localParcel1.writeInt(j);
                            this.mRemote.transact(45, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int k = localParcel2.readInt();
                            if (k != 0)
                                label80: return i;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label127: 
                    do
                    {
                        j = 0;
                        break;
                        i = 0;
                        break label80;
                    }
                    while (!paramBoolean);
                    int j = i;
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean cancelBondProcess(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean cancelDiscovery()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean cancelPairingUserInput(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(39, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean changeApplicationBluetoothState(boolean paramBoolean, IBluetoothStateChangeCallback paramIBluetoothStateChangeCallback, IBinder paramIBinder)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    if (paramBoolean)
                    {
                        int j = i;
                        localParcel1.writeInt(j);
                        if (paramIBluetoothStateChangeCallback == null)
                            break label113;
                    }
                    label113: for (IBinder localIBinder = paramIBluetoothStateChangeCallback.asBinder(); ; localIBinder = null)
                    {
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeStrongBinder(paramIBinder);
                        this.mRemote.transact(20, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break label119;
                        return i;
                        int k = 0;
                        break;
                    }
                    label119: i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean connectChannelToSink(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            if (paramBluetoothHealthAppConfiguration != null)
                            {
                                localParcel1.writeInt(1);
                                paramBluetoothHealthAppConfiguration.writeToParcel(localParcel1, 0);
                                localParcel1.writeInt(paramInt);
                                this.mRemote.transact(66, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label140;
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label140: bool = false;
                }
            }

            public boolean connectChannelToSource(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            if (paramBluetoothHealthAppConfiguration != null)
                            {
                                localParcel1.writeInt(1);
                                paramBluetoothHealthAppConfiguration.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(65, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label132;
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label132: bool = false;
                }
            }

            public boolean connectHeadset(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(46, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean connectInputDevice(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(49, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean connectPanDevice(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(61, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean createBond(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean createBondOutOfBand(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    localParcel1.writeByteArray(paramArrayOfByte1);
                    localParcel1.writeByteArray(paramArrayOfByte2);
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean disable(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean disconnectChannel(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            if (paramBluetoothHealthAppConfiguration != null)
                            {
                                localParcel1.writeInt(1);
                                paramBluetoothHealthAppConfiguration.writeToParcel(localParcel1, 0);
                                localParcel1.writeInt(paramInt);
                                this.mRemote.transact(67, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label140;
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label140: bool = false;
                }
            }

            public boolean disconnectHeadset(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(47, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean disconnectInputDevice(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(50, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean disconnectPanDevice(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(62, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean enable()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean enableNoAutoConnect()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean fetchRemoteUuids(String paramString, ParcelUuid paramParcelUuid, IBluetoothCallback paramIBluetoothCallback)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        localParcel1.writeString(paramString);
                        if (paramParcelUuid != null)
                        {
                            localParcel1.writeInt(1);
                            paramParcelUuid.writeToParcel(localParcel1, 0);
                            if (paramIBluetoothCallback != null)
                            {
                                localIBinder = paramIBluetoothCallback.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                this.mRemote.transact(33, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label139;
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                    continue;
                    label139: bool = false;
                }
            }

            public int getAdapterConnectionState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getAddress()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getBluetoothState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getBondState(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getConnectedHealthDevices()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(69, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getConnectedInputDevices()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(51, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getConnectedPanDevices()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(59, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getDiscoverableTimeout()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getHealthDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(71, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getHealthDevicesMatchingConnectionStates(int[] paramArrayOfInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeIntArray(paramArrayOfInt);
                    this.mRemote.transact(70, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getInputDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(53, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getInputDevicePriority(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(55, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getInputDevicesMatchingConnectionStates(int[] paramArrayOfInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeIntArray(paramArrayOfInt);
                    this.mRemote.transact(52, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.bluetooth.IBluetooth";
            }

            public ParcelFileDescriptor getMainChannelFd(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            if (paramBluetoothHealthAppConfiguration != null)
                            {
                                localParcel1.writeInt(1);
                                paramBluetoothHealthAppConfiguration.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(68, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                if (localParcel2.readInt() == 0)
                                    break label131;
                                localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
                                return localParcelFileDescriptor;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label131: ParcelFileDescriptor localParcelFileDescriptor = null;
                }
            }

            public String getName()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPanDeviceConnectionState(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(58, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getPanDevicesMatchingConnectionStates(int[] paramArrayOfInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeIntArray(paramArrayOfInt);
                    this.mRemote.transact(60, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getProfileConnectionState(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getRemoteAlias(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getRemoteClass(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(31, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getRemoteName(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(28, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getRemoteServiceChannel(String paramString, ParcelUuid paramParcelUuid)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    if (paramParcelUuid != null)
                    {
                        localParcel1.writeInt(1);
                        paramParcelUuid.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(34, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ParcelUuid[] getRemoteUuids(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ParcelUuid[] arrayOfParcelUuid = (ParcelUuid[])localParcel2.createTypedArray(ParcelUuid.CREATOR);
                    return arrayOfParcelUuid;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getScanMode()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getTrustState(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(41, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ParcelUuid[] getUuids()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ParcelUuid[] arrayOfParcelUuid = (ParcelUuid[])localParcel2.createTypedArray(ParcelUuid.CREATOR);
                    return arrayOfParcelUuid;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isBluetoothDock(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(42, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isDiscovering()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isEnabled()
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        return bool;
                    bool = false;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isTetheringOn()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(56, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] listBonds()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean notifyIncomingConnection(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(48, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public byte[] readOutOfBandData()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    byte[] arrayOfByte = localParcel2.createByteArray();
                    return arrayOfByte;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean registerAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, IBluetoothHealthCallback paramIBluetoothHealthCallback)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothHealthAppConfiguration != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothHealthAppConfiguration.writeToParcel(localParcel1, 0);
                            if (paramIBluetoothHealthCallback != null)
                            {
                                localIBinder = paramIBluetoothHealthCallback.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                this.mRemote.transact(63, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label131;
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                    continue;
                    label131: bool = false;
                }
            }

            public boolean removeBond(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(24, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeServiceRecord(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(44, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void sendConnectionStateChange(BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        localParcel1.writeInt(paramInt3);
                        this.mRemote.transact(72, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBluetoothTethering(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(57, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setDeviceOutOfBandData(String paramString, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    localParcel1.writeByteArray(paramArrayOfByte1);
                    localParcel1.writeByteArray(paramArrayOfByte2);
                    this.mRemote.transact(27, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setDiscoverableTimeout(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setInputDevicePriority(BluetoothDevice paramBluetoothDevice, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(54, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean setName(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setPairingConfirmation(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(37, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setPasskey(String paramString, int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(36, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setPin(String paramString, byte[] paramArrayOfByte)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    localParcel1.writeByteArray(paramArrayOfByte);
                    this.mRemote.transact(35, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setRemoteAlias(String paramString1, String paramString2)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setRemoteOutOfBandData(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(38, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setScanMode(int paramInt1, int paramInt2)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setTrust(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    localParcel1.writeString(paramString);
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(40, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean startDiscovery()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean unregisterAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetooth");
                        if (paramBluetoothHealthAppConfiguration != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothHealthAppConfiguration.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(64, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.IBluetooth
 * JD-Core Version:        0.6.2
 */