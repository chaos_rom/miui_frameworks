package android.bluetooth;

public class AtCommandResult
{
    public static final int ERROR = 1;
    private static final String ERROR_STRING = "ERROR";
    public static final int OK = 0;
    private static final String OK_STRING = "OK";
    public static final int UNSOLICITED = 2;
    private StringBuilder mResponse;
    private int mResultCode;

    public AtCommandResult(int paramInt)
    {
        this.mResultCode = paramInt;
        this.mResponse = new StringBuilder();
    }

    public AtCommandResult(String paramString)
    {
        this(0);
        addResponse(paramString);
    }

    public static void appendWithCrlf(StringBuilder paramStringBuilder, String paramString)
    {
        if ((paramStringBuilder.length() > 0) && (paramString.length() > 0))
            paramStringBuilder.append("\r\n\r\n");
        paramStringBuilder.append(paramString);
    }

    public void addResponse(String paramString)
    {
        appendWithCrlf(this.mResponse, paramString);
    }

    public void addResult(AtCommandResult paramAtCommandResult)
    {
        if (paramAtCommandResult != null)
        {
            appendWithCrlf(this.mResponse, paramAtCommandResult.mResponse.toString());
            this.mResultCode = paramAtCommandResult.mResultCode;
        }
    }

    public int getResultCode()
    {
        return this.mResultCode;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(this.mResponse.toString());
        switch (this.mResultCode)
        {
        default:
        case 0:
        case 1:
        }
        while (true)
        {
            return localStringBuilder.toString();
            appendWithCrlf(localStringBuilder, "OK");
            continue;
            appendWithCrlf(localStringBuilder, "ERROR");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.AtCommandResult
 * JD-Core Version:        0.6.2
 */