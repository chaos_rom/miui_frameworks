package android.bluetooth;

public abstract class AtCommandHandler
{
    public AtCommandResult handleActionCommand()
    {
        return new AtCommandResult(1);
    }

    public AtCommandResult handleBasicCommand(String paramString)
    {
        return new AtCommandResult(1);
    }

    public AtCommandResult handleReadCommand()
    {
        return new AtCommandResult(1);
    }

    public AtCommandResult handleSetCommand(Object[] paramArrayOfObject)
    {
        return new AtCommandResult(1);
    }

    public AtCommandResult handleTestCommand()
    {
        return new AtCommandResult(0);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.AtCommandHandler
 * JD-Core Version:        0.6.2
 */