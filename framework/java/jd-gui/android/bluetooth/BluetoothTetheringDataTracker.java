package android.bluetooth;

import android.content.Context;
import android.net.DhcpInfoInternal;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.NetworkStateTracker;
import android.net.NetworkUtils;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class BluetoothTetheringDataTracker
    implements NetworkStateTracker
{
    private static final String NETWORKTYPE = "BLUETOOTH_TETHER";
    private static final String TAG = "BluetoothTethering";
    private static String mIface;
    public static BluetoothTetheringDataTracker sInstance;
    private BluetoothPan mBluetoothPan;
    private Context mContext;
    private Handler mCsHandler;
    private AtomicInteger mDefaultGatewayAddr = new AtomicInteger(0);
    private AtomicBoolean mDefaultRouteSet = new AtomicBoolean(false);
    private BluetoothDevice mDevice;
    private LinkCapabilities mLinkCapabilities = new LinkCapabilities();
    private LinkProperties mLinkProperties = new LinkProperties();
    private NetworkInfo mNetworkInfo = new NetworkInfo(7, 0, "BLUETOOTH_TETHER", "");
    private AtomicBoolean mPrivateDnsRouteSet = new AtomicBoolean(false);
    private BluetoothProfile.ServiceListener mProfileServiceListener = new BluetoothProfile.ServiceListener()
    {
        public void onServiceConnected(int paramAnonymousInt, BluetoothProfile paramAnonymousBluetoothProfile)
        {
            BluetoothTetheringDataTracker.access$002(BluetoothTetheringDataTracker.this, (BluetoothPan)paramAnonymousBluetoothProfile);
        }

        public void onServiceDisconnected(int paramAnonymousInt)
        {
            BluetoothTetheringDataTracker.access$002(BluetoothTetheringDataTracker.this, null);
        }
    };
    private AtomicBoolean mTeardownRequested = new AtomicBoolean(false);

    private BluetoothTetheringDataTracker()
    {
        this.mNetworkInfo.setIsAvailable(false);
        setTeardownRequested(false);
    }

    /** @deprecated */
    public static BluetoothTetheringDataTracker getInstance()
    {
        try
        {
            if (sInstance == null)
                sInstance = new BluetoothTetheringDataTracker();
            BluetoothTetheringDataTracker localBluetoothTetheringDataTracker = sInstance;
            return localBluetoothTetheringDataTracker;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public Object Clone()
        throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }

    public void defaultRouteSet(boolean paramBoolean)
    {
        this.mDefaultRouteSet.set(paramBoolean);
    }

    public int getDefaultGatewayAddr()
    {
        return this.mDefaultGatewayAddr.get();
    }

    public LinkCapabilities getLinkCapabilities()
    {
        return new LinkCapabilities(this.mLinkCapabilities);
    }

    /** @deprecated */
    public LinkProperties getLinkProperties()
    {
        try
        {
            LinkProperties localLinkProperties = new LinkProperties(this.mLinkProperties);
            return localLinkProperties;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public NetworkInfo getNetworkInfo()
    {
        try
        {
            NetworkInfo localNetworkInfo = this.mNetworkInfo;
            return localNetworkInfo;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getTcpBufferSizesPropName()
    {
        return "net.tcp.buffersize.wifi";
    }

    /** @deprecated */
    public boolean isAvailable()
    {
        try
        {
            boolean bool = this.mNetworkInfo.isAvailable();
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isDefaultRouteSet()
    {
        return this.mDefaultRouteSet.get();
    }

    public boolean isPrivateDnsRouteSet()
    {
        return this.mPrivateDnsRouteSet.get();
    }

    public boolean isTeardownRequested()
    {
        return this.mTeardownRequested.get();
    }

    public void privateDnsRouteSet(boolean paramBoolean)
    {
        this.mPrivateDnsRouteSet.set(paramBoolean);
    }

    public boolean reconnect()
    {
        this.mTeardownRequested.set(false);
        return true;
    }

    public void setDependencyMet(boolean paramBoolean)
    {
    }

    public void setPolicyDataEnable(boolean paramBoolean)
    {
        Log.w("BluetoothTethering", "ignoring setPolicyDataEnable(" + paramBoolean + ")");
    }

    public boolean setRadio(boolean paramBoolean)
    {
        return true;
    }

    public void setTeardownRequested(boolean paramBoolean)
    {
        this.mTeardownRequested.set(paramBoolean);
    }

    public void setUserDataEnable(boolean paramBoolean)
    {
        Log.w("BluetoothTethering", "ignoring setUserDataEnable(" + paramBoolean + ")");
    }

    public void startMonitoring(Context paramContext, Handler paramHandler)
    {
        this.mContext = paramContext;
        this.mCsHandler = paramHandler;
        BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (localBluetoothAdapter != null)
            localBluetoothAdapter.getProfileProxy(this.mContext, this.mProfileServiceListener, 5);
    }

    /** @deprecated */
    public void startReverseTether(String paramString, BluetoothDevice paramBluetoothDevice)
    {
        try
        {
            mIface = paramString;
            this.mDevice = paramBluetoothDevice;
            new Thread(new Runnable()
            {
                public void run()
                {
                    DhcpInfoInternal localDhcpInfoInternal = new DhcpInfoInternal();
                    if (!NetworkUtils.runDhcp(BluetoothTetheringDataTracker.mIface, localDhcpInfoInternal))
                        Log.e("BluetoothTethering", "DHCP request error:" + NetworkUtils.getDhcpError());
                    while (true)
                    {
                        return;
                        BluetoothTetheringDataTracker.access$202(BluetoothTetheringDataTracker.this, localDhcpInfoInternal.makeLinkProperties());
                        BluetoothTetheringDataTracker.this.mLinkProperties.setInterfaceName(BluetoothTetheringDataTracker.mIface);
                        BluetoothTetheringDataTracker.this.mNetworkInfo.setIsAvailable(true);
                        BluetoothTetheringDataTracker.this.mNetworkInfo.setDetailedState(NetworkInfo.DetailedState.CONNECTED, null, null);
                        BluetoothTetheringDataTracker.this.mCsHandler.obtainMessage(3, BluetoothTetheringDataTracker.this.mNetworkInfo).sendToTarget();
                        BluetoothTetheringDataTracker.this.mCsHandler.obtainMessage(1, BluetoothTetheringDataTracker.this.mNetworkInfo).sendToTarget();
                    }
                }
            }).start();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int startUsingNetworkFeature(String paramString, int paramInt1, int paramInt2)
    {
        return -1;
    }

    /** @deprecated */
    public void stopReverseTether(String paramString)
    {
        try
        {
            NetworkUtils.stopDhcp(paramString);
            this.mLinkProperties.clear();
            this.mNetworkInfo.setIsAvailable(false);
            this.mNetworkInfo.setDetailedState(NetworkInfo.DetailedState.DISCONNECTED, null, null);
            this.mCsHandler.obtainMessage(3, this.mNetworkInfo).sendToTarget();
            this.mCsHandler.obtainMessage(1, this.mNetworkInfo).sendToTarget();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int stopUsingNetworkFeature(String paramString, int paramInt1, int paramInt2)
    {
        return -1;
    }

    public boolean teardown()
    {
        this.mTeardownRequested.set(true);
        if (this.mBluetoothPan != null)
        {
            Iterator localIterator = this.mBluetoothPan.getConnectedDevices().iterator();
            while (localIterator.hasNext())
            {
                BluetoothDevice localBluetoothDevice = (BluetoothDevice)localIterator.next();
                this.mBluetoothPan.disconnect(localBluetoothDevice);
            }
        }
        return true;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothTetheringDataTracker
 * JD-Core Version:        0.6.2
 */