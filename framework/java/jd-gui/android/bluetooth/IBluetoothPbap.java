package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IBluetoothPbap extends IInterface
{
    public abstract boolean connect(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract void disconnect()
        throws RemoteException;

    public abstract BluetoothDevice getClient()
        throws RemoteException;

    public abstract int getState()
        throws RemoteException;

    public abstract boolean isConnected(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBluetoothPbap
    {
        private static final String DESCRIPTOR = "android.bluetooth.IBluetoothPbap";
        static final int TRANSACTION_connect = 3;
        static final int TRANSACTION_disconnect = 4;
        static final int TRANSACTION_getClient = 2;
        static final int TRANSACTION_getState = 1;
        static final int TRANSACTION_isConnected = 5;

        public Stub()
        {
            attachInterface(this, "android.bluetooth.IBluetoothPbap");
        }

        public static IBluetoothPbap asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.bluetooth.IBluetoothPbap");
                if ((localIInterface != null) && ((localIInterface instanceof IBluetoothPbap)))
                    localObject = (IBluetoothPbap)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
                while (true)
                {
                    return j;
                    paramParcel2.writeString("android.bluetooth.IBluetoothPbap");
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
                    int k = getState();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(k);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
                    BluetoothDevice localBluetoothDevice3 = getClient();
                    paramParcel2.writeNoException();
                    if (localBluetoothDevice3 != null)
                    {
                        paramParcel2.writeInt(j);
                        localBluetoothDevice3.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
                        if (paramParcel1.readInt() != 0);
                        for (BluetoothDevice localBluetoothDevice2 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice2 = null)
                        {
                            boolean bool2 = connect(localBluetoothDevice2);
                            paramParcel2.writeNoException();
                            if (bool2)
                                i = j;
                            paramParcel2.writeInt(i);
                            break;
                        }
                        paramParcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
                        disconnect();
                        paramParcel2.writeNoException();
                    }
                }
            case 5:
            }
            paramParcel1.enforceInterface("android.bluetooth.IBluetoothPbap");
            if (paramParcel1.readInt() != 0);
            for (BluetoothDevice localBluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice1 = null)
            {
                boolean bool1 = isConnected(localBluetoothDevice1);
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                break;
            }
        }

        private static class Proxy
            implements IBluetoothPbap
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean connect(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(3, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void disconnect()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public BluetoothDevice getClient()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localBluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(localParcel2);
                        return localBluetoothDevice;
                    }
                    BluetoothDevice localBluetoothDevice = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.bluetooth.IBluetoothPbap";
            }

            public int getState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isConnected(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothPbap");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(5, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.IBluetoothPbap
 * JD-Core Version:        0.6.2
 */