package android.bluetooth;

import android.content.Context;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class BluetoothHealth
    implements BluetoothProfile
{
    public static final int APP_CONFIG_REGISTRATION_FAILURE = 1;
    public static final int APP_CONFIG_REGISTRATION_SUCCESS = 0;
    public static final int APP_CONFIG_UNREGISTRATION_FAILURE = 3;
    public static final int APP_CONFIG_UNREGISTRATION_SUCCESS = 2;
    public static final int CHANNEL_TYPE_ANY = 12;
    public static final int CHANNEL_TYPE_RELIABLE = 10;
    public static final int CHANNEL_TYPE_STREAMING = 11;
    private static final boolean DBG = false;
    public static final int HEALTH_OPERATION_ERROR = 6001;
    public static final int HEALTH_OPERATION_GENERIC_FAILURE = 6003;
    public static final int HEALTH_OPERATION_INVALID_ARGS = 6002;
    public static final int HEALTH_OPERATION_NOT_ALLOWED = 6005;
    public static final int HEALTH_OPERATION_NOT_FOUND = 6004;
    public static final int HEALTH_OPERATION_SUCCESS = 6000;
    public static final int SINK_ROLE = 2;
    public static final int SOURCE_ROLE = 1;
    public static final int STATE_CHANNEL_CONNECTED = 2;
    public static final int STATE_CHANNEL_CONNECTING = 1;
    public static final int STATE_CHANNEL_DISCONNECTED = 0;
    public static final int STATE_CHANNEL_DISCONNECTING = 3;
    private static final String TAG = "BluetoothHealth";
    BluetoothAdapter mAdapter;
    private IBluetooth mService;
    private BluetoothProfile.ServiceListener mServiceListener;

    BluetoothHealth(Context paramContext, BluetoothProfile.ServiceListener paramServiceListener)
    {
        IBinder localIBinder = ServiceManager.getService("bluetooth");
        this.mServiceListener = paramServiceListener;
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (localIBinder != null)
        {
            this.mService = IBluetooth.Stub.asInterface(localIBinder);
            if (this.mServiceListener != null)
                this.mServiceListener.onServiceConnected(3, this);
        }
        while (true)
        {
            return;
            Log.w("BluetoothHealth", "Bluetooth Service not available!");
            this.mService = null;
        }
    }

    private boolean checkAppParam(String paramString, int paramInt1, int paramInt2, BluetoothHealthCallback paramBluetoothHealthCallback)
    {
        boolean bool = false;
        if ((paramString == null) || ((paramInt1 != 1) && (paramInt1 != 2)) || ((paramInt2 != 10) && (paramInt2 != 11) && (paramInt2 != 12)) || (paramBluetoothHealthCallback == null));
        while (true)
        {
            return bool;
            if ((paramInt1 != 1) || (paramInt2 != 12))
                bool = true;
        }
    }

    private boolean isEnabled()
    {
        BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if ((localBluetoothAdapter != null) && (localBluetoothAdapter.getState() == 12));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            log("Bluetooth is Not enabled");
        }
    }

    private boolean isValidDevice(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        if (paramBluetoothDevice == null);
        while (true)
        {
            return bool;
            if (BluetoothAdapter.checkBluetoothAddress(paramBluetoothDevice.getAddress()))
                bool = true;
        }
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothHealth", paramString);
    }

    void close()
    {
        this.mServiceListener = null;
    }

    public boolean connectChannelToSink(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)) && (paramBluetoothHealthAppConfiguration != null))
            try
            {
                boolean bool2 = this.mService.connectChannelToSink(paramBluetoothDevice, paramBluetoothHealthAppConfiguration, paramInt);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHealth", "Proxy not attached to service");
        }
    }

    public boolean connectChannelToSource(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)) && (paramBluetoothHealthAppConfiguration != null))
            try
            {
                boolean bool2 = this.mService.connectChannelToSource(paramBluetoothDevice, paramBluetoothHealthAppConfiguration);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHealth", "Proxy not attached to service");
        }
    }

    public boolean disconnectChannel(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)) && (paramBluetoothHealthAppConfiguration != null))
            try
            {
                boolean bool2 = this.mService.disconnectChannel(paramBluetoothDevice, paramBluetoothHealthAppConfiguration, paramInt);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHealth", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getConnectedDevices()
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getConnectedHealthDevices();
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", "Stack:" + Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothHealth", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public int getConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        int i;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)))
            try
            {
                int j = this.mService.getHealthDeviceConnectionState(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", localRemoteException.toString());
            }
        while (true)
        {
            i = 0;
            break;
            Log.w("BluetoothHealth", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getHealthDevicesMatchingConnectionStates(paramArrayOfInt);
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", "Stack:" + Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothHealth", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public ParcelFileDescriptor getMainChannelFd(BluetoothDevice paramBluetoothDevice, BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        ParcelFileDescriptor localParcelFileDescriptor1;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)) && (paramBluetoothHealthAppConfiguration != null))
            try
            {
                ParcelFileDescriptor localParcelFileDescriptor2 = this.mService.getMainChannelFd(paramBluetoothDevice, paramBluetoothHealthAppConfiguration);
                localParcelFileDescriptor1 = localParcelFileDescriptor2;
                return localParcelFileDescriptor1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", localRemoteException.toString());
            }
        while (true)
        {
            localParcelFileDescriptor1 = null;
            break;
            Log.w("BluetoothHealth", "Proxy not attached to service");
        }
    }

    public boolean registerAppConfiguration(String paramString, int paramInt1, int paramInt2, int paramInt3, BluetoothHealthCallback paramBluetoothHealthCallback)
    {
        boolean bool1 = false;
        boolean bool2;
        if ((!isEnabled()) || (!checkAppParam(paramString, paramInt2, paramInt3, paramBluetoothHealthCallback)))
        {
            bool2 = false;
            return bool2;
        }
        BluetoothHealthCallbackWrapper localBluetoothHealthCallbackWrapper = new BluetoothHealthCallbackWrapper(paramBluetoothHealthCallback);
        BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration = new BluetoothHealthAppConfiguration(paramString, paramInt1, paramInt2, paramInt3);
        if (this.mService != null);
        while (true)
        {
            try
            {
                boolean bool3 = this.mService.registerAppConfiguration(localBluetoothHealthAppConfiguration, localBluetoothHealthCallbackWrapper);
                bool1 = bool3;
                bool2 = bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", localRemoteException.toString());
                continue;
            }
            Log.w("BluetoothHealth", "Proxy not attached to service");
        }
    }

    public boolean registerSinkAppConfiguration(String paramString, int paramInt, BluetoothHealthCallback paramBluetoothHealthCallback)
    {
        if ((!isEnabled()) || (paramString == null));
        for (boolean bool = false; ; bool = registerAppConfiguration(paramString, paramInt, 2, 12, paramBluetoothHealthCallback))
            return bool;
    }

    public boolean unregisterAppConfiguration(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (paramBluetoothHealthAppConfiguration != null));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.unregisterAppConfiguration(paramBluetoothHealthAppConfiguration);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHealth", localRemoteException.toString());
                continue;
            }
            Log.w("BluetoothHealth", "Proxy not attached to service");
        }
    }

    private static class BluetoothHealthCallbackWrapper extends IBluetoothHealthCallback.Stub
    {
        private BluetoothHealthCallback mCallback;

        public BluetoothHealthCallbackWrapper(BluetoothHealthCallback paramBluetoothHealthCallback)
        {
            this.mCallback = paramBluetoothHealthCallback;
        }

        public void onHealthAppConfigurationStatusChange(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
        {
            this.mCallback.onHealthAppConfigurationStatusChange(paramBluetoothHealthAppConfiguration, paramInt);
        }

        public void onHealthChannelStateChange(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt3)
        {
            this.mCallback.onHealthChannelStateChange(paramBluetoothHealthAppConfiguration, paramBluetoothDevice, paramInt1, paramInt2, paramParcelFileDescriptor, paramInt3);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothHealth
 * JD-Core Version:        0.6.2
 */