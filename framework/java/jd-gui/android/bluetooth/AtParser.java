package android.bluetooth;

import java.util.ArrayList;
import java.util.HashMap;

public class AtParser
{
    private static final int TYPE_ACTION = 0;
    private static final int TYPE_READ = 1;
    private static final int TYPE_SET = 2;
    private static final int TYPE_TEST = 3;
    private HashMap<Character, AtCommandHandler> mBasicHandlers = new HashMap();
    private HashMap<String, AtCommandHandler> mExtHandlers = new HashMap();
    private String mLastInput = "";

    private static String clean(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder(paramString.length());
        int i = 0;
        char c;
        int j;
        if (i < paramString.length())
        {
            c = paramString.charAt(i);
            if (c != '"')
                break label101;
            j = paramString.indexOf('"', i + 1);
            if (j == -1)
            {
                localStringBuilder.append(paramString.substring(i, paramString.length()));
                localStringBuilder.append('"');
            }
        }
        else
        {
            return localStringBuilder.toString();
        }
        localStringBuilder.append(paramString.substring(i, j + 1));
        i = j;
        while (true)
        {
            i++;
            break;
            label101: if (c != ' ')
                localStringBuilder.append(Character.toUpperCase(c));
        }
    }

    private static int findChar(char paramChar, String paramString, int paramInt)
    {
        int i = paramInt;
        char c;
        int j;
        if (i < paramString.length())
        {
            c = paramString.charAt(i);
            if (c == '"')
            {
                i = paramString.indexOf('"', i + 1);
                if (i != -1)
                    break label61;
                j = paramString.length();
            }
        }
        while (true)
        {
            return j;
            if (c == paramChar)
            {
                j = i;
            }
            else
            {
                label61: i++;
                break;
                j = paramString.length();
            }
        }
    }

    private static int findEndExtendedName(String paramString, int paramInt)
    {
        int i = paramInt;
        if (i < paramString.length())
        {
            char c = paramString.charAt(i);
            if (isAtoZ(c));
            while ((c >= '0') && (c <= '9'))
            {
                i++;
                break;
            }
            switch (c)
            {
            case '!':
            case '%':
            case '-':
            case '.':
            case '/':
            case ':':
            case '_':
            }
        }
        while (true)
        {
            return i;
            i = paramString.length();
        }
    }

    private static Object[] generateArgs(String paramString)
    {
        int i = 0;
        ArrayList localArrayList = new ArrayList();
        while (true)
            if (i <= paramString.length())
            {
                int j = findChar(',', paramString, i);
                String str = paramString.substring(i, j);
                try
                {
                    localArrayList.add(new Integer(str));
                    i = j + 1;
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    while (true)
                        localArrayList.add(str);
                }
            }
        return localArrayList.toArray();
    }

    private static boolean isAtoZ(char paramChar)
    {
        if ((paramChar >= 'A') && (paramChar <= 'Z'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public AtCommandResult process(String paramString)
    {
        String str1 = clean(paramString);
        AtCommandResult localAtCommandResult;
        if (str1.regionMatches(0, "A/", 0, 2))
        {
            str1 = new String(this.mLastInput);
            if (!str1.equals(""))
                break label64;
            localAtCommandResult = new AtCommandResult(2);
        }
        label64: label459: label465: 
        while (true)
        {
            return localAtCommandResult;
            this.mLastInput = new String(str1);
            break;
            if (!str1.regionMatches(0, "AT", 0, 2))
            {
                localAtCommandResult = new AtCommandResult(1);
            }
            else
            {
                int i = 2;
                localAtCommandResult = new AtCommandResult(2);
                while (true)
                {
                    if (i >= str1.length())
                        break label465;
                    char c = str1.charAt(i);
                    if (isAtoZ(c))
                    {
                        String str3 = str1.substring(i + 1);
                        if (this.mBasicHandlers.containsKey(Character.valueOf(c)))
                        {
                            localAtCommandResult.addResult(((AtCommandHandler)this.mBasicHandlers.get(Character.valueOf(c))).handleBasicCommand(str3));
                            break;
                        }
                        localAtCommandResult.addResult(new AtCommandResult(1));
                        break;
                    }
                    if (c == '+')
                    {
                        int j = findEndExtendedName(str1, i + 1);
                        String str2 = str1.substring(i, j);
                        if (!this.mExtHandlers.containsKey(str2))
                        {
                            localAtCommandResult.addResult(new AtCommandResult(1));
                            break;
                        }
                        AtCommandHandler localAtCommandHandler = (AtCommandHandler)this.mExtHandlers.get(str2);
                        int k = findChar(';', str1, i);
                        int m;
                        if (j >= k)
                        {
                            m = 0;
                            switch (m)
                            {
                            default:
                            case 0:
                            case 1:
                            case 3:
                            case 2:
                            }
                        }
                        while (true)
                        {
                            if (localAtCommandResult.getResultCode() != 0)
                                break label459;
                            i = k;
                            break;
                            if (str1.charAt(j) == '?')
                            {
                                m = 1;
                                break label280;
                            }
                            if (str1.charAt(j) == '=')
                            {
                                if (j + 1 < k)
                                {
                                    if (str1.charAt(j + 1) == '?')
                                    {
                                        m = 3;
                                        break label280;
                                    }
                                    m = 2;
                                    break label280;
                                }
                                m = 2;
                                break label280;
                            }
                            m = 0;
                            break label280;
                            localAtCommandResult.addResult(localAtCommandHandler.handleActionCommand());
                            continue;
                            localAtCommandResult.addResult(localAtCommandHandler.handleReadCommand());
                            continue;
                            localAtCommandResult.addResult(localAtCommandHandler.handleTestCommand());
                            continue;
                            localAtCommandResult.addResult(localAtCommandHandler.handleSetCommand(generateArgs(str1.substring(j + 1, k))));
                        }
                        break;
                    }
                    i++;
                }
            }
        }
    }

    public void register(Character paramCharacter, AtCommandHandler paramAtCommandHandler)
    {
        this.mBasicHandlers.put(paramCharacter, paramAtCommandHandler);
    }

    public void register(String paramString, AtCommandHandler paramAtCommandHandler)
    {
        this.mExtHandlers.put(paramString, paramAtCommandHandler);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.AtParser
 * JD-Core Version:        0.6.2
 */