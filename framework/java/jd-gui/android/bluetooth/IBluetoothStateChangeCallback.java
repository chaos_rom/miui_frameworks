package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IBluetoothStateChangeCallback extends IInterface
{
    public abstract void onBluetoothStateChange(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBluetoothStateChangeCallback
    {
        private static final String DESCRIPTOR = "android.bluetooth.IBluetoothStateChangeCallback";
        static final int TRANSACTION_onBluetoothStateChange = 1;

        public Stub()
        {
            attachInterface(this, "android.bluetooth.IBluetoothStateChangeCallback");
        }

        public static IBluetoothStateChangeCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.bluetooth.IBluetoothStateChangeCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IBluetoothStateChangeCallback)))
                    localObject = (IBluetoothStateChangeCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.bluetooth.IBluetoothStateChangeCallback");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.bluetooth.IBluetoothStateChangeCallback");
            if (paramParcel1.readInt() != 0);
            for (boolean bool2 = bool1; ; bool2 = false)
            {
                onBluetoothStateChange(bool2);
                paramParcel2.writeNoException();
                break;
            }
        }

        private static class Proxy
            implements IBluetoothStateChangeCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.bluetooth.IBluetoothStateChangeCallback";
            }

            public void onBluetoothStateChange(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothStateChangeCallback");
                    if (paramBoolean)
                    {
                        localParcel1.writeInt(i);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.IBluetoothStateChangeCallback
 * JD-Core Version:        0.6.2
 */