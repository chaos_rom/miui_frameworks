package android.bluetooth;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

public final class BluetoothAudioGateway
{
    private static final boolean DBG = false;
    public static final int DEFAULT_HF_AG_CHANNEL = 10;
    public static final int DEFAULT_HS_AG_CHANNEL = 11;
    public static final int MSG_INCOMING_HANDSFREE_CONNECTION = 101;
    public static final int MSG_INCOMING_HEADSET_CONNECTION = 100;
    private static final int SELECT_WAIT_TIMEOUT = 1000;
    private static final String TAG = "BT Audio Gateway";
    private final BluetoothAdapter mAdapter;
    private Handler mCallback;
    private Thread mConnectThead;
    private String mConnectingHandsfreeAddress;
    private int mConnectingHandsfreeRfcommChannel;
    private int mConnectingHandsfreeSocketFd;
    private String mConnectingHeadsetAddress;
    private int mConnectingHeadsetRfcommChannel;
    private int mConnectingHeadsetSocketFd;
    private int mHandsfreeAgRfcommChannel = -1;
    private int mHeadsetAgRfcommChannel = -1;
    private volatile boolean mInterrupted;
    private int mNativeData;
    private int mTimeoutRemainingMs;

    static
    {
        classInitNative();
    }

    public BluetoothAudioGateway(BluetoothAdapter paramBluetoothAdapter)
    {
        this(paramBluetoothAdapter, 10, 11);
    }

    public BluetoothAudioGateway(BluetoothAdapter paramBluetoothAdapter, int paramInt1, int paramInt2)
    {
        this.mAdapter = paramBluetoothAdapter;
        this.mHandsfreeAgRfcommChannel = paramInt1;
        this.mHeadsetAgRfcommChannel = paramInt2;
        initializeNativeDataNative();
    }

    private static native void classInitNative();

    private native void cleanupNativeDataNative();

    private native void initializeNativeDataNative();

    private static void log(String paramString)
    {
        Log.d("BT Audio Gateway", paramString);
    }

    private native boolean setUpListeningSocketsNative();

    private native void tearDownListeningSocketsNative();

    private native boolean waitForHandsfreeConnectNative(int paramInt);

    protected void finalize()
        throws Throwable
    {
        try
        {
            cleanupNativeDataNative();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    /** @deprecated */
    public boolean start(Handler paramHandler)
    {
        boolean bool = false;
        try
        {
            if (this.mConnectThead == null)
            {
                this.mCallback = paramHandler;
                this.mConnectThead = new Thread("BT Audio Gateway")
                {
                    public void run()
                    {
                        while (!BluetoothAudioGateway.this.mInterrupted)
                        {
                            BluetoothAudioGateway.access$102(BluetoothAudioGateway.this, -1);
                            BluetoothAudioGateway.access$202(BluetoothAudioGateway.this, -1);
                            if (!BluetoothAudioGateway.this.waitForHandsfreeConnectNative(1000))
                            {
                                if (BluetoothAudioGateway.this.mTimeoutRemainingMs > 0)
                                    try
                                    {
                                        Log.i("BT Audio Gateway", "select thread timed out, but " + BluetoothAudioGateway.this.mTimeoutRemainingMs + "ms of waiting remain.");
                                        Thread.sleep(BluetoothAudioGateway.this.mTimeoutRemainingMs);
                                    }
                                    catch (InterruptedException localInterruptedException)
                                    {
                                        Log.i("BT Audio Gateway", "select thread was interrupted (2), exiting");
                                        BluetoothAudioGateway.access$002(BluetoothAudioGateway.this, true);
                                    }
                            }
                            else
                            {
                                Log.i("BT Audio Gateway", "connect notification!");
                                if (BluetoothAudioGateway.this.mConnectingHeadsetRfcommChannel >= 0)
                                {
                                    Log.i("BT Audio Gateway", "Incoming connection from headset " + BluetoothAudioGateway.this.mConnectingHeadsetAddress + " on channel " + BluetoothAudioGateway.this.mConnectingHeadsetRfcommChannel);
                                    Message localMessage2 = Message.obtain(BluetoothAudioGateway.this.mCallback);
                                    localMessage2.what = 100;
                                    localMessage2.obj = new BluetoothAudioGateway.IncomingConnectionInfo(BluetoothAudioGateway.this, BluetoothAudioGateway.this.mAdapter, BluetoothAudioGateway.this.mAdapter.getRemoteDevice(BluetoothAudioGateway.this.mConnectingHeadsetAddress), BluetoothAudioGateway.this.mConnectingHeadsetSocketFd, BluetoothAudioGateway.this.mConnectingHeadsetRfcommChannel);
                                    localMessage2.sendToTarget();
                                }
                                if (BluetoothAudioGateway.this.mConnectingHandsfreeRfcommChannel >= 0)
                                {
                                    Log.i("BT Audio Gateway", "Incoming connection from handsfree " + BluetoothAudioGateway.this.mConnectingHandsfreeAddress + " on channel " + BluetoothAudioGateway.this.mConnectingHandsfreeRfcommChannel);
                                    Message localMessage1 = Message.obtain();
                                    localMessage1.setTarget(BluetoothAudioGateway.this.mCallback);
                                    localMessage1.what = 101;
                                    localMessage1.obj = new BluetoothAudioGateway.IncomingConnectionInfo(BluetoothAudioGateway.this, BluetoothAudioGateway.this.mAdapter, BluetoothAudioGateway.this.mAdapter.getRemoteDevice(BluetoothAudioGateway.this.mConnectingHandsfreeAddress), BluetoothAudioGateway.this.mConnectingHandsfreeSocketFd, BluetoothAudioGateway.this.mConnectingHandsfreeRfcommChannel);
                                    localMessage1.sendToTarget();
                                }
                            }
                        }
                    }
                };
                if (!setUpListeningSocketsNative())
                    Log.e("BT Audio Gateway", "Could not set up listening socket, exiting");
            }
            while (true)
            {
                return bool;
                this.mInterrupted = false;
                this.mConnectThead.start();
                bool = true;
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void stop()
    {
        try
        {
            if (this.mConnectThead != null)
                this.mInterrupted = true;
            try
            {
                this.mConnectThead.interrupt();
                this.mConnectThead.join();
                this.mConnectThead = null;
                this.mCallback = null;
                tearDownListeningSocketsNative();
                return;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    Log.w("BT Audio Gateway", "Interrupted waiting for Connect Thread to join");
            }
        }
        finally
        {
        }
    }

    public class IncomingConnectionInfo
    {
        public BluetoothAdapter mAdapter;
        public BluetoothDevice mRemoteDevice;
        public int mRfcommChan;
        public int mSocketFd;

        IncomingConnectionInfo(BluetoothAdapter paramBluetoothDevice, BluetoothDevice paramInt1, int paramInt2, int arg5)
        {
            this.mAdapter = paramBluetoothDevice;
            this.mRemoteDevice = paramInt1;
            this.mSocketFd = paramInt2;
            int i;
            this.mRfcommChan = i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothAudioGateway
 * JD-Core Version:        0.6.2
 */