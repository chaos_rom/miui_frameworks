package android.bluetooth;

import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

public final class BluetoothDevice
    implements Parcelable
{
    public static final String ACTION_ACL_CONNECTED = "android.bluetooth.device.action.ACL_CONNECTED";
    public static final String ACTION_ACL_DISCONNECTED = "android.bluetooth.device.action.ACL_DISCONNECTED";
    public static final String ACTION_ACL_DISCONNECT_REQUESTED = "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED";
    public static final String ACTION_ALIAS_CHANGED = "android.bluetooth.device.action.ALIAS_CHANGED";
    public static final String ACTION_BOND_STATE_CHANGED = "android.bluetooth.device.action.BOND_STATE_CHANGED";
    public static final String ACTION_CLASS_CHANGED = "android.bluetooth.device.action.CLASS_CHANGED";
    public static final String ACTION_CONNECTION_ACCESS_CANCEL = "android.bluetooth.device.action.CONNECTION_ACCESS_CANCEL";
    public static final String ACTION_CONNECTION_ACCESS_REPLY = "android.bluetooth.device.action.CONNECTION_ACCESS_REPLY";
    public static final String ACTION_CONNECTION_ACCESS_REQUEST = "android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST";
    public static final String ACTION_DISAPPEARED = "android.bluetooth.device.action.DISAPPEARED";
    public static final String ACTION_FOUND = "android.bluetooth.device.action.FOUND";
    public static final String ACTION_NAME_CHANGED = "android.bluetooth.device.action.NAME_CHANGED";
    public static final String ACTION_NAME_FAILED = "android.bluetooth.device.action.NAME_FAILED";
    public static final String ACTION_PAIRING_CANCEL = "android.bluetooth.device.action.PAIRING_CANCEL";
    public static final String ACTION_PAIRING_REQUEST = "android.bluetooth.device.action.PAIRING_REQUEST";
    public static final String ACTION_UUID = "android.bluetooth.device.action.UUID";
    public static final int BOND_BONDED = 12;
    public static final int BOND_BONDING = 11;
    public static final int BOND_NONE = 10;
    public static final int BOND_SUCCESS = 0;
    public static final int CONNECTION_ACCESS_NO = 2;
    public static final int CONNECTION_ACCESS_YES = 1;
    public static final Parcelable.Creator<BluetoothDevice> CREATOR = new Parcelable.Creator()
    {
        public BluetoothDevice createFromParcel(Parcel paramAnonymousParcel)
        {
            return new BluetoothDevice(paramAnonymousParcel.readString());
        }

        public BluetoothDevice[] newArray(int paramAnonymousInt)
        {
            return new BluetoothDevice[paramAnonymousInt];
        }
    };
    public static final int ERROR = -2147483648;
    public static final String EXTRA_ACCESS_REQUEST_TYPE = "android.bluetooth.device.extra.ACCESS_REQUEST_TYPE";
    public static final String EXTRA_ALWAYS_ALLOWED = "android.bluetooth.device.extra.ALWAYS_ALLOWED";
    public static final String EXTRA_BOND_STATE = "android.bluetooth.device.extra.BOND_STATE";
    public static final String EXTRA_CLASS = "android.bluetooth.device.extra.CLASS";
    public static final String EXTRA_CLASS_NAME = "android.bluetooth.device.extra.CLASS_NAME";
    public static final String EXTRA_CONNECTION_ACCESS_RESULT = "android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT";
    public static final String EXTRA_DEVICE = "android.bluetooth.device.extra.DEVICE";
    public static final String EXTRA_NAME = "android.bluetooth.device.extra.NAME";
    public static final String EXTRA_PACKAGE_NAME = "android.bluetooth.device.extra.PACKAGE_NAME";
    public static final String EXTRA_PAIRING_KEY = "android.bluetooth.device.extra.PAIRING_KEY";
    public static final String EXTRA_PAIRING_VARIANT = "android.bluetooth.device.extra.PAIRING_VARIANT";
    public static final String EXTRA_PREVIOUS_BOND_STATE = "android.bluetooth.device.extra.PREVIOUS_BOND_STATE";
    public static final String EXTRA_REASON = "android.bluetooth.device.extra.REASON";
    public static final String EXTRA_RSSI = "android.bluetooth.device.extra.RSSI";
    public static final String EXTRA_UUID = "android.bluetooth.device.extra.UUID";
    public static final int PAIRING_VARIANT_CONSENT = 3;
    public static final int PAIRING_VARIANT_DISPLAY_PASSKEY = 4;
    public static final int PAIRING_VARIANT_DISPLAY_PIN = 5;
    public static final int PAIRING_VARIANT_OOB_CONSENT = 6;
    public static final int PAIRING_VARIANT_PASSKEY = 1;
    public static final int PAIRING_VARIANT_PASSKEY_CONFIRMATION = 2;
    public static final int PAIRING_VARIANT_PIN = 0;
    public static final int REQUEST_TYPE_PHONEBOOK_ACCESS = 2;
    public static final int REQUEST_TYPE_PROFILE_CONNECTION = 1;
    private static final String TAG = "BluetoothDevice";
    public static final int UNBOND_REASON_AUTH_CANCELED = 3;
    public static final int UNBOND_REASON_AUTH_FAILED = 1;
    public static final int UNBOND_REASON_AUTH_REJECTED = 2;
    public static final int UNBOND_REASON_AUTH_TIMEOUT = 6;
    public static final int UNBOND_REASON_DISCOVERY_IN_PROGRESS = 5;
    public static final int UNBOND_REASON_REMOTE_AUTH_CANCELED = 8;
    public static final int UNBOND_REASON_REMOTE_DEVICE_DOWN = 4;
    public static final int UNBOND_REASON_REMOVED = 9;
    public static final int UNBOND_REASON_REPEATED_ATTEMPTS = 7;
    private static IBluetooth sService;
    private final String mAddress;

    BluetoothDevice(String paramString)
    {
        getService();
        if (!BluetoothAdapter.checkBluetoothAddress(paramString))
            throw new IllegalArgumentException(paramString + " is not a valid Bluetooth address");
        this.mAddress = paramString;
    }

    public static byte[] convertPinToBytes(String paramString)
    {
        Object localObject;
        if (paramString == null)
            localObject = null;
        while (true)
        {
            return localObject;
            try
            {
                byte[] arrayOfByte = paramString.getBytes("UTF-8");
                localObject = arrayOfByte;
                if ((localObject.length <= 0) || (localObject.length > 16))
                    localObject = null;
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                Log.e("BluetoothDevice", "UTF-8 not supported?!?");
                localObject = null;
            }
        }
    }

    // ERROR //
    static IBluetooth getService()
    {
        // Byte code:
        //     0: ldc 2
        //     2: monitorenter
        //     3: getstatic 210	android/bluetooth/BluetoothDevice:sService	Landroid/bluetooth/IBluetooth;
        //     6: ifnonnull +36 -> 42
        //     9: ldc 212
        //     11: invokestatic 217	android/os/ServiceManager:getService	(Ljava/lang/String;)Landroid/os/IBinder;
        //     14: astore_1
        //     15: aload_1
        //     16: ifnonnull +19 -> 35
        //     19: new 219	java/lang/RuntimeException
        //     22: dup
        //     23: ldc 221
        //     25: invokespecial 222	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     28: athrow
        //     29: astore_0
        //     30: ldc 2
        //     32: monitorexit
        //     33: aload_0
        //     34: athrow
        //     35: aload_1
        //     36: invokestatic 228	android/bluetooth/IBluetooth$Stub:asInterface	(Landroid/os/IBinder;)Landroid/bluetooth/IBluetooth;
        //     39: putstatic 210	android/bluetooth/BluetoothDevice:sService	Landroid/bluetooth/IBluetooth;
        //     42: ldc 2
        //     44: monitorexit
        //     45: getstatic 210	android/bluetooth/BluetoothDevice:sService	Landroid/bluetooth/IBluetooth;
        //     48: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     3	33	29	finally
        //     35	45	29	finally
    }

    public boolean cancelBondProcess()
    {
        try
        {
            boolean bool2 = sService.cancelBondProcess(this.mAddress);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean cancelPairingUserInput()
    {
        try
        {
            boolean bool2 = sService.cancelPairingUserInput(this.mAddress);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean createBond()
    {
        try
        {
            boolean bool2 = sService.createBond(this.mAddress);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean createBondOutOfBand(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        try
        {
            boolean bool2 = sService.createBondOutOfBand(this.mAddress, paramArrayOfByte1, paramArrayOfByte2);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public BluetoothSocket createInsecureRfcommSocket(int paramInt)
        throws IOException
    {
        return new BluetoothSocket(1, -1, false, false, this, paramInt, null);
    }

    public BluetoothSocket createInsecureRfcommSocketToServiceRecord(UUID paramUUID)
        throws IOException
    {
        return new BluetoothSocket(1, -1, false, false, this, -1, new ParcelUuid(paramUUID));
    }

    public BluetoothSocket createRfcommSocket(int paramInt)
        throws IOException
    {
        return new BluetoothSocket(1, -1, true, true, this, paramInt, null);
    }

    public BluetoothSocket createRfcommSocketToServiceRecord(UUID paramUUID)
        throws IOException
    {
        return new BluetoothSocket(1, -1, true, true, this, -1, new ParcelUuid(paramUUID));
    }

    public BluetoothSocket createScoSocket()
        throws IOException
    {
        return new BluetoothSocket(2, -1, true, true, this, -1, null);
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        if ((paramObject instanceof BluetoothDevice));
        for (boolean bool = this.mAddress.equals(((BluetoothDevice)paramObject).getAddress()); ; bool = false)
            return bool;
    }

    public boolean fetchUuidsWithSdp()
    {
        try
        {
            boolean bool2 = sService.fetchRemoteUuids(this.mAddress, null, null);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public String getAddress()
    {
        return this.mAddress;
    }

    public String getAlias()
    {
        try
        {
            String str2 = sService.getRemoteAlias(this.mAddress);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                String str1 = null;
            }
        }
    }

    public String getAliasName()
    {
        String str = getAlias();
        if (str == null)
            str = getName();
        return str;
    }

    public BluetoothClass getBluetoothClass()
    {
        Object localObject = null;
        try
        {
            int i = sService.getRemoteClass(this.mAddress);
            if (i != -16777216)
            {
                BluetoothClass localBluetoothClass = new BluetoothClass(i);
                localObject = localBluetoothClass;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("BluetoothDevice", "", localRemoteException);
        }
        return localObject;
    }

    public int getBondState()
    {
        try
        {
            int j = sService.getBondState(this.mAddress);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                int i = 10;
            }
        }
    }

    public String getName()
    {
        try
        {
            String str2 = sService.getRemoteName(this.mAddress);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                String str1 = null;
            }
        }
    }

    public int getServiceChannel(ParcelUuid paramParcelUuid)
    {
        try
        {
            int j = sService.getRemoteServiceChannel(this.mAddress, paramParcelUuid);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                int i = -2147483648;
            }
        }
    }

    public boolean getTrustState()
    {
        try
        {
            boolean bool2 = sService.getTrustState(this.mAddress);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public ParcelUuid[] getUuids()
    {
        try
        {
            ParcelUuid[] arrayOfParcelUuid2 = sService.getRemoteUuids(this.mAddress);
            arrayOfParcelUuid1 = arrayOfParcelUuid2;
            return arrayOfParcelUuid1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                ParcelUuid[] arrayOfParcelUuid1 = null;
            }
        }
    }

    public int hashCode()
    {
        return this.mAddress.hashCode();
    }

    public boolean isBluetoothDock()
    {
        try
        {
            boolean bool2 = sService.isBluetoothDock(this.mAddress);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean removeBond()
    {
        try
        {
            boolean bool2 = sService.removeBond(this.mAddress);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean setAlias(String paramString)
    {
        try
        {
            boolean bool2 = sService.setRemoteAlias(this.mAddress, paramString);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean setDeviceOutOfBandData(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        try
        {
            boolean bool2 = sService.setDeviceOutOfBandData(this.mAddress, paramArrayOfByte1, paramArrayOfByte2);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean setPairingConfirmation(boolean paramBoolean)
    {
        try
        {
            boolean bool2 = sService.setPairingConfirmation(this.mAddress, paramBoolean);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean setPasskey(int paramInt)
    {
        try
        {
            boolean bool2 = sService.setPasskey(this.mAddress, paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean setPin(byte[] paramArrayOfByte)
    {
        try
        {
            boolean bool2 = sService.setPin(this.mAddress, paramArrayOfByte);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean setRemoteOutOfBandData()
    {
        try
        {
            boolean bool2 = sService.setRemoteOutOfBandData(this.mAddress);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean setTrust(boolean paramBoolean)
    {
        try
        {
            boolean bool2 = sService.setTrust(this.mAddress, paramBoolean);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothDevice", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public String toString()
    {
        return this.mAddress;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mAddress);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothDevice
 * JD-Core Version:        0.6.2
 */