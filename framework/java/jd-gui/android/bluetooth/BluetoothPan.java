package android.bluetooth;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class BluetoothPan
    implements BluetoothProfile
{
    public static final String ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.pan.profile.action.CONNECTION_STATE_CHANGED";
    private static final boolean DBG = false;
    public static final String EXTRA_LOCAL_ROLE = "android.bluetooth.pan.extra.LOCAL_ROLE";
    public static final int LOCAL_NAP_ROLE = 1;
    public static final int LOCAL_PANU_ROLE = 2;
    public static final int PAN_CONNECT_FAILED_ALREADY_CONNECTED = 1001;
    public static final int PAN_CONNECT_FAILED_ATTEMPT_FAILED = 1002;
    public static final int PAN_DISCONNECT_FAILED_NOT_CONNECTED = 1000;
    public static final int PAN_OPERATION_GENERIC_FAILURE = 1003;
    public static final int PAN_OPERATION_SUCCESS = 1004;
    private static final String TAG = "BluetoothPan";
    private BluetoothAdapter mAdapter;
    private IBluetooth mService;
    private BluetoothProfile.ServiceListener mServiceListener;

    BluetoothPan(Context paramContext, BluetoothProfile.ServiceListener paramServiceListener)
    {
        IBinder localIBinder = ServiceManager.getService("bluetooth");
        this.mServiceListener = paramServiceListener;
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (localIBinder != null)
        {
            this.mService = IBluetooth.Stub.asInterface(localIBinder);
            if (this.mServiceListener != null)
                this.mServiceListener.onServiceConnected(5, this);
        }
        while (true)
        {
            return;
            Log.w("BluetoothPan", "Bluetooth Service not available!");
            this.mService = null;
        }
    }

    private boolean isEnabled()
    {
        if (this.mAdapter.getState() == 12);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isValidDevice(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        if (paramBluetoothDevice == null);
        while (true)
        {
            return bool;
            if (BluetoothAdapter.checkBluetoothAddress(paramBluetoothDevice.getAddress()))
                bool = true;
        }
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothPan", paramString);
    }

    void close()
    {
        this.mServiceListener = null;
    }

    public boolean connect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.connectPanDevice(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPan", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothPan", "Proxy not attached to service");
        }
    }

    public boolean disconnect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.disconnectPanDevice(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPan", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothPan", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getConnectedDevices()
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getConnectedPanDevices();
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPan", "Stack:" + Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothPan", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public int getConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        int i = 0;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                int j = this.mService.getPanDeviceConnectionState(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPan", "Stack:" + Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothPan", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getPanDevicesMatchingConnectionStates(paramArrayOfInt);
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothPan", "Stack:" + Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothPan", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public boolean isTetheringOn()
    {
        try
        {
            boolean bool2 = this.mService.isTetheringOn();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothPan", "Stack:" + Log.getStackTraceString(new Throwable()));
                boolean bool1 = false;
            }
        }
    }

    public void setBluetoothTethering(boolean paramBoolean)
    {
        try
        {
            this.mService.setBluetoothTethering(paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("BluetoothPan", "Stack:" + Log.getStackTraceString(new Throwable()));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothPan
 * JD-Core Version:        0.6.2
 */