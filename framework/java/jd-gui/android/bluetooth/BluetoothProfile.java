package android.bluetooth;

import java.util.List;

public abstract interface BluetoothProfile
{
    public static final int A2DP = 2;
    public static final String EXTRA_PREVIOUS_STATE = "android.bluetooth.profile.extra.PREVIOUS_STATE";
    public static final String EXTRA_STATE = "android.bluetooth.profile.extra.STATE";
    public static final int HEADSET = 1;
    public static final int HEALTH = 3;
    public static final int INPUT_DEVICE = 4;
    public static final int PAN = 5;
    public static final int PBAP = 6;
    public static final int PRIORITY_AUTO_CONNECT = 1000;
    public static final int PRIORITY_OFF = 0;
    public static final int PRIORITY_ON = 100;
    public static final int PRIORITY_UNDEFINED = -1;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_DISCONNECTING = 3;

    public abstract List<BluetoothDevice> getConnectedDevices();

    public abstract int getConnectionState(BluetoothDevice paramBluetoothDevice);

    public abstract List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt);

    public static abstract interface ServiceListener
    {
        public abstract void onServiceConnected(int paramInt, BluetoothProfile paramBluetoothProfile);

        public abstract void onServiceDisconnected(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothProfile
 * JD-Core Version:        0.6.2
 */