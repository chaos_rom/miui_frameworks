package android.bluetooth;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class BluetoothHeadset
    implements BluetoothProfile
{
    public static final String ACTION_AUDIO_STATE_CHANGED = "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED";
    public static final String ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED";
    public static final String ACTION_VENDOR_SPECIFIC_HEADSET_EVENT = "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT";
    public static final int AT_CMD_TYPE_ACTION = 4;
    public static final int AT_CMD_TYPE_BASIC = 3;
    public static final int AT_CMD_TYPE_READ = 0;
    public static final int AT_CMD_TYPE_SET = 2;
    public static final int AT_CMD_TYPE_TEST = 1;
    private static final boolean DBG = false;
    public static final String EXTRA_VENDOR_SPECIFIC_HEADSET_EVENT_ARGS = "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_ARGS";
    public static final String EXTRA_VENDOR_SPECIFIC_HEADSET_EVENT_CMD = "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_CMD";
    public static final String EXTRA_VENDOR_SPECIFIC_HEADSET_EVENT_CMD_TYPE = "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_CMD_TYPE";
    public static final int STATE_AUDIO_CONNECTED = 12;
    public static final int STATE_AUDIO_CONNECTING = 11;
    public static final int STATE_AUDIO_DISCONNECTED = 10;
    private static final String TAG = "BluetoothHeadset";
    public static final String VENDOR_SPECIFIC_HEADSET_EVENT_COMPANY_ID_CATEGORY = "android.bluetooth.headset.intent.category.companyid";
    BluetoothAdapter mAdapter;
    private ServiceConnection mConnection = new ServiceConnection()
    {
        public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
        {
            BluetoothHeadset.access$002(BluetoothHeadset.this, IBluetoothHeadset.Stub.asInterface(paramAnonymousIBinder));
            if (BluetoothHeadset.this.mServiceListener != null)
                BluetoothHeadset.this.mServiceListener.onServiceConnected(1, BluetoothHeadset.this);
        }

        public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
        {
            BluetoothHeadset.access$002(BluetoothHeadset.this, null);
            if (BluetoothHeadset.this.mServiceListener != null)
                BluetoothHeadset.this.mServiceListener.onServiceDisconnected(1);
        }
    };
    private Context mContext;
    private IBluetoothHeadset mService;
    private BluetoothProfile.ServiceListener mServiceListener;

    BluetoothHeadset(Context paramContext, BluetoothProfile.ServiceListener paramServiceListener)
    {
        this.mContext = paramContext;
        this.mServiceListener = paramServiceListener;
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!paramContext.bindService(new Intent(IBluetoothHeadset.class.getName()), this.mConnection, 0))
            Log.e("BluetoothHeadset", "Could not bind to Bluetooth Headset Service");
    }

    public static boolean isBluetoothVoiceDialingEnabled(Context paramContext)
    {
        return paramContext.getResources().getBoolean(17891365);
    }

    private boolean isDisabled()
    {
        if (this.mAdapter.getState() == 10);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isEnabled()
    {
        if (this.mAdapter.getState() == 12);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isValidDevice(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool = false;
        if (paramBluetoothDevice == null);
        while (true)
        {
            return bool;
            if (BluetoothAdapter.checkBluetoothAddress(paramBluetoothDevice.getAddress()))
                bool = true;
        }
    }

    private static void log(String paramString)
    {
        Log.d("BluetoothHeadset", paramString);
    }

    public boolean acceptIncomingConnect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()))
            try
            {
                boolean bool2 = this.mService.acceptIncomingConnect(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean cancelConnectThread()
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()))
            try
            {
                boolean bool2 = this.mService.cancelConnectThread();
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    /** @deprecated */
    void close()
    {
        try
        {
            if (this.mConnection != null)
            {
                this.mContext.unbindService(this.mConnection);
                this.mConnection = null;
            }
            this.mServiceListener = null;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean connect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.connect(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean connectHeadsetInternal(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()))
            try
            {
                boolean bool2 = this.mService.connectHeadsetInternal(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean createIncomingConnect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()))
            try
            {
                boolean bool2 = this.mService.createIncomingConnect(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean disconnect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.disconnect(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean disconnectHeadsetInternal(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1;
        if ((this.mService != null) && (!isDisabled()))
            try
            {
                boolean bool2 = this.mService.disconnectHeadsetInternal(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public int getAudioState(BluetoothDevice paramBluetoothDevice)
    {
        int i;
        if ((this.mService != null) && (!isDisabled()))
            try
            {
                int j = this.mService.getAudioState(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            i = 10;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public int getBatteryUsageHint(BluetoothDevice paramBluetoothDevice)
    {
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                int j = this.mService.getBatteryUsageHint(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
            int i = -1;
        }
    }

    public List<BluetoothDevice> getConnectedDevices()
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getConnectedDevices();
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public int getConnectionState(BluetoothDevice paramBluetoothDevice)
    {
        int i = 0;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                int j = this.mService.getConnectionState(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
    {
        if ((this.mService != null) && (isEnabled()));
        while (true)
        {
            try
            {
                List localList = this.mService.getDevicesMatchingConnectionStates(paramArrayOfInt);
                localObject = localList;
                return localObject;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
                localObject = new ArrayList();
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
            Object localObject = new ArrayList();
        }
    }

    public int getPriority(BluetoothDevice paramBluetoothDevice)
    {
        int i = 0;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                int j = this.mService.getPriority(paramBluetoothDevice);
                i = j;
                return i;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
                continue;
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean isAudioConnected(BluetoothDevice paramBluetoothDevice)
    {
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.isAudioConnected(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
            boolean bool1 = false;
        }
    }

    public boolean rejectIncomingConnect(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1;
        if (this.mService != null)
            try
            {
                boolean bool2 = this.mService.rejectIncomingConnect(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean setAudioState(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        boolean bool1;
        if ((this.mService != null) && (!isDisabled()))
            try
            {
                boolean bool2 = this.mService.setAudioState(paramBluetoothDevice, paramInt);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean setPriority(BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        boolean bool1 = false;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)))
            if ((paramInt == 0) || (paramInt == 100));
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.setPriority(paramBluetoothDevice, paramInt);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
            }
            continue;
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean startScoUsingVirtualVoiceCall(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)))
            try
            {
                boolean bool2 = this.mService.startScoUsingVirtualVoiceCall(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean startVoiceRecognition(BluetoothDevice paramBluetoothDevice)
    {
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.startVoiceRecognition(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
            boolean bool1 = false;
        }
    }

    public boolean stopScoUsingVirtualVoiceCall(BluetoothDevice paramBluetoothDevice)
    {
        boolean bool1;
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)))
            try
            {
                boolean bool2 = this.mService.stopScoUsingVirtualVoiceCall(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", localRemoteException.toString());
            }
        while (true)
        {
            bool1 = false;
            break;
            Log.w("BluetoothHeadset", "Proxy not attached to service");
        }
    }

    public boolean stopVoiceRecognition(BluetoothDevice paramBluetoothDevice)
    {
        if ((this.mService != null) && (isEnabled()) && (isValidDevice(paramBluetoothDevice)));
        while (true)
        {
            try
            {
                boolean bool2 = this.mService.stopVoiceRecognition(paramBluetoothDevice);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothHeadset", Log.getStackTraceString(new Throwable()));
            }
            if (this.mService == null)
                Log.w("BluetoothHeadset", "Proxy not attached to service");
            boolean bool1 = false;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothHeadset
 * JD-Core Version:        0.6.2
 */