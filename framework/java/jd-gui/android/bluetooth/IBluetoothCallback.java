package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IBluetoothCallback extends IInterface
{
    public abstract void onRfcommChannelFound(int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBluetoothCallback
    {
        private static final String DESCRIPTOR = "android.bluetooth.IBluetoothCallback";
        static final int TRANSACTION_onRfcommChannelFound = 1;

        public Stub()
        {
            attachInterface(this, "android.bluetooth.IBluetoothCallback");
        }

        public static IBluetoothCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.bluetooth.IBluetoothCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IBluetoothCallback)))
                    localObject = (IBluetoothCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.bluetooth.IBluetoothCallback");
                continue;
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothCallback");
                onRfcommChannelFound(paramParcel1.readInt());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IBluetoothCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.bluetooth.IBluetoothCallback";
            }

            public void onRfcommChannelFound(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothCallback");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.IBluetoothCallback
 * JD-Core Version:        0.6.2
 */