package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IBluetoothHealthCallback extends IInterface
{
    public abstract void onHealthAppConfigurationStatusChange(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
        throws RemoteException;

    public abstract void onHealthChannelStateChange(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt3)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBluetoothHealthCallback
    {
        private static final String DESCRIPTOR = "android.bluetooth.IBluetoothHealthCallback";
        static final int TRANSACTION_onHealthAppConfigurationStatusChange = 1;
        static final int TRANSACTION_onHealthChannelStateChange = 2;

        public Stub()
        {
            attachInterface(this, "android.bluetooth.IBluetoothHealthCallback");
        }

        public static IBluetoothHealthCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.bluetooth.IBluetoothHealthCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IBluetoothHealthCallback)))
                    localObject = (IBluetoothHealthCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
            case 1598968902:
                for (bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2); ; bool = true)
                {
                    return bool;
                    paramParcel2.writeString("android.bluetooth.IBluetoothHealthCallback");
                }
            case 1:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothHealthCallback");
                if (paramParcel1.readInt() != 0);
                for (BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration2 = (BluetoothHealthAppConfiguration)BluetoothHealthAppConfiguration.CREATOR.createFromParcel(paramParcel1); ; localBluetoothHealthAppConfiguration2 = null)
                {
                    onHealthAppConfigurationStatusChange(localBluetoothHealthAppConfiguration2, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool = true;
                    break;
                }
            case 2:
            }
            paramParcel1.enforceInterface("android.bluetooth.IBluetoothHealthCallback");
            BluetoothHealthAppConfiguration localBluetoothHealthAppConfiguration1;
            label142: BluetoothDevice localBluetoothDevice;
            label163: int i;
            int j;
            if (paramParcel1.readInt() != 0)
            {
                localBluetoothHealthAppConfiguration1 = (BluetoothHealthAppConfiguration)BluetoothHealthAppConfiguration.CREATOR.createFromParcel(paramParcel1);
                if (paramParcel1.readInt() == 0)
                    break label230;
                localBluetoothDevice = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1);
                i = paramParcel1.readInt();
                j = paramParcel1.readInt();
                if (paramParcel1.readInt() == 0)
                    break label236;
            }
            label230: label236: for (ParcelFileDescriptor localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor = null)
            {
                onHealthChannelStateChange(localBluetoothHealthAppConfiguration1, localBluetoothDevice, i, j, localParcelFileDescriptor, paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool = true;
                break;
                localBluetoothHealthAppConfiguration1 = null;
                break label142;
                localBluetoothDevice = null;
                break label163;
            }
        }

        private static class Proxy
            implements IBluetoothHealthCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.bluetooth.IBluetoothHealthCallback";
            }

            public void onHealthAppConfigurationStatusChange(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHealthCallback");
                    if (paramBluetoothHealthAppConfiguration != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothHealthAppConfiguration.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onHealthChannelStateChange(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHealthCallback");
                        if (paramBluetoothHealthAppConfiguration != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothHealthAppConfiguration.writeToParcel(localParcel1, 0);
                            if (paramBluetoothDevice != null)
                            {
                                localParcel1.writeInt(1);
                                paramBluetoothDevice.writeToParcel(localParcel1, 0);
                                localParcel1.writeInt(paramInt1);
                                localParcel1.writeInt(paramInt2);
                                if (paramParcelFileDescriptor == null)
                                    break label155;
                                localParcel1.writeInt(1);
                                paramParcelFileDescriptor.writeToParcel(localParcel1, 0);
                                localParcel1.writeInt(paramInt3);
                                this.mRemote.transact(2, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label155: localParcel1.writeInt(0);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.IBluetoothHealthCallback
 * JD-Core Version:        0.6.2
 */