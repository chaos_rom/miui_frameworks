package android.bluetooth;

import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public final class HeadsetBase
{
    private static final boolean DBG = false;
    public static final int DIRECTION_INCOMING = 1;
    public static final int DIRECTION_OUTGOING = 2;
    public static final int RFCOMM_DISCONNECTED = 1;
    private static final String TAG = "Bluetooth HeadsetBase";
    private static int sAtInputCount = 0;
    private final BluetoothAdapter mAdapter;
    private final String mAddress;
    protected AtParser mAtParser;
    private final long mConnectTimestamp;
    private final int mDirection;
    private Thread mEventThread;
    private Handler mEventThreadHandler;
    private volatile boolean mEventThreadInterrupted;
    private int mNativeData;
    private final BluetoothDevice mRemoteDevice;
    private final int mRfcommChannel;
    private int mTimeoutRemainingMs;
    private PowerManager.WakeLock mWakeLock;

    static
    {
        classInitNative();
    }

    public HeadsetBase(PowerManager paramPowerManager, BluetoothAdapter paramBluetoothAdapter, BluetoothDevice paramBluetoothDevice, int paramInt)
    {
        this.mDirection = 2;
        this.mConnectTimestamp = System.currentTimeMillis();
        this.mAdapter = paramBluetoothAdapter;
        this.mRemoteDevice = paramBluetoothDevice;
        this.mAddress = paramBluetoothDevice.getAddress();
        this.mRfcommChannel = paramInt;
        this.mWakeLock = paramPowerManager.newWakeLock(1, "HeadsetBase");
        this.mWakeLock.setReferenceCounted(false);
        initializeAtParser();
        initializeNativeDataNative(-1);
    }

    public HeadsetBase(PowerManager paramPowerManager, BluetoothAdapter paramBluetoothAdapter, BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, Handler paramHandler)
    {
        this.mDirection = 1;
        this.mConnectTimestamp = System.currentTimeMillis();
        this.mAdapter = paramBluetoothAdapter;
        this.mRemoteDevice = paramBluetoothDevice;
        this.mAddress = paramBluetoothDevice.getAddress();
        this.mRfcommChannel = paramInt2;
        this.mEventThreadHandler = paramHandler;
        this.mWakeLock = paramPowerManager.newWakeLock(1, "HeadsetBase");
        this.mWakeLock.setReferenceCounted(false);
        initializeAtParser();
        initializeNativeDataNative(paramInt1);
    }

    /** @deprecated */
    private void acquireWakeLock()
    {
        try
        {
            if (!this.mWakeLock.isHeld())
                this.mWakeLock.acquire();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private static native void classInitNative();

    private native void cleanupNativeDataNative();

    private native int connectAsyncNative();

    private native boolean connectNative();

    private native void disconnectNative();

    public static int getAtInputCount()
    {
        return sAtInputCount;
    }

    private native int getLastReadStatusNative();

    private native void initializeNativeDataNative(int paramInt);

    private static void log(String paramString)
    {
        Log.d("Bluetooth HeadsetBase", paramString);
    }

    private native String readNative(int paramInt);

    /** @deprecated */
    private void releaseWakeLock()
    {
        try
        {
            if (this.mWakeLock.isHeld())
                this.mWakeLock.release();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private native boolean sendURCNative(String paramString);

    private void stopEventThread()
    {
        this.mEventThreadInterrupted = true;
        this.mEventThread.interrupt();
        try
        {
            this.mEventThread.join();
            label19: this.mEventThread = null;
            return;
        }
        catch (InterruptedException localInterruptedException)
        {
            break label19;
        }
    }

    private native int waitForAsyncConnectNative(int paramInt);

    public boolean connect(Handler paramHandler)
    {
        if (this.mEventThread == null)
            if (connectNative());
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.mEventThreadHandler = paramHandler;
        }
    }

    public boolean connectAsync()
    {
        if (connectAsyncNative() == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void disconnect()
    {
        if (this.mEventThread != null)
            stopEventThread();
        disconnectNative();
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            cleanupNativeDataNative();
            releaseWakeLock();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public AtParser getAtParser()
    {
        return this.mAtParser;
    }

    public long getConnectTimestamp()
    {
        return this.mConnectTimestamp;
    }

    public int getDirection()
    {
        return this.mDirection;
    }

    public int getRemainingAsyncConnectWaitingTimeMs()
    {
        return this.mTimeoutRemainingMs;
    }

    public BluetoothDevice getRemoteDevice()
    {
        return this.mRemoteDevice;
    }

    protected void handleInput(String paramString)
    {
        acquireWakeLock();
        try
        {
            if (sAtInputCount == 2147483647);
            for (sAtInputCount = 0; ; sAtInputCount = 1 + sAtInputCount)
            {
                AtCommandResult localAtCommandResult = this.mAtParser.process(paramString);
                if (localAtCommandResult.getResultCode() == 1)
                    Log.i("Bluetooth HeadsetBase", "Error processing <" + paramString + ">");
                sendURC(localAtCommandResult.toString());
                releaseWakeLock();
                return;
            }
        }
        finally
        {
        }
    }

    protected void initializeAtParser()
    {
        this.mAtParser = new AtParser();
    }

    public boolean isConnected()
    {
        if (this.mEventThread != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    /** @deprecated */
    public boolean sendURC(String paramString)
    {
        try
        {
            if (paramString.length() > 0)
            {
                boolean bool2 = sendURCNative(paramString);
                bool1 = bool2;
                return bool1;
            }
            boolean bool1 = true;
        }
        finally
        {
        }
    }

    public void startEventThread()
    {
        this.mEventThread = new Thread("HeadsetBase Event Thread")
        {
            public void run()
            {
                while (!HeadsetBase.this.mEventThreadInterrupted)
                {
                    String str = HeadsetBase.this.readNative(500);
                    if (str != null)
                    {
                        HeadsetBase.this.handleInput(str);
                    }
                    else
                    {
                        int i = HeadsetBase.this.getLastReadStatusNative();
                        if (i != 0)
                        {
                            Log.i("Bluetooth HeadsetBase", "headset read error " + i);
                            if (HeadsetBase.this.mEventThreadHandler != null)
                                HeadsetBase.this.mEventThreadHandler.obtainMessage(1).sendToTarget();
                            HeadsetBase.this.disconnectNative();
                        }
                    }
                }
            }
        };
        this.mEventThreadInterrupted = false;
        this.mEventThread.start();
    }

    public int waitForAsyncConnect(int paramInt, Handler paramHandler)
    {
        int i = waitForAsyncConnectNative(paramInt);
        if (i > 0)
            this.mEventThreadHandler = paramHandler;
        return i;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.HeadsetBase
 * JD-Core Version:        0.6.2
 */