package android.bluetooth;

import java.io.IOException;
import java.io.InputStream;

final class BluetoothInputStream extends InputStream
{
    private BluetoothSocket mSocket;

    BluetoothInputStream(BluetoothSocket paramBluetoothSocket)
    {
        this.mSocket = paramBluetoothSocket;
    }

    public int available()
        throws IOException
    {
        return this.mSocket.available();
    }

    public void close()
        throws IOException
    {
        this.mSocket.close();
    }

    public int read()
        throws IOException
    {
        byte[] arrayOfByte = new byte[1];
        if (this.mSocket.read(arrayOfByte, 0, 1) == 1);
        for (int i = 0xFF & arrayOfByte[0]; ; i = -1)
            return i;
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        if (paramArrayOfByte == null)
            throw new NullPointerException("byte array is null");
        if (((paramInt1 | paramInt2) < 0) || (paramInt2 > paramArrayOfByte.length - paramInt1))
            throw new ArrayIndexOutOfBoundsException("invalid offset or length");
        return this.mSocket.read(paramArrayOfByte, paramInt1, paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothInputStream
 * JD-Core Version:        0.6.2
 */