package android.bluetooth;

import android.os.ParcelFileDescriptor;
import android.util.Log;

public abstract class BluetoothHealthCallback
{
    private static final String TAG = "BluetoothHealthCallback";

    public void onHealthAppConfigurationStatusChange(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, int paramInt)
    {
        Log.d("BluetoothHealthCallback", "onHealthAppConfigurationStatusChange: " + paramBluetoothHealthAppConfiguration + "Status: " + paramInt);
    }

    public void onHealthChannelStateChange(BluetoothHealthAppConfiguration paramBluetoothHealthAppConfiguration, BluetoothDevice paramBluetoothDevice, int paramInt1, int paramInt2, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt3)
    {
        Log.d("BluetoothHealthCallback", "onHealthChannelStateChange: " + paramBluetoothHealthAppConfiguration + "Device: " + paramBluetoothDevice + "prevState:" + paramInt1 + "newState:" + paramInt2 + "ParcelFd:" + paramParcelFileDescriptor + "ChannelId:" + paramInt3);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothHealthCallback
 * JD-Core Version:        0.6.2
 */