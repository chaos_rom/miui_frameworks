package android.bluetooth;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IBluetoothHeadset extends IInterface
{
    public abstract boolean acceptIncomingConnect(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean cancelConnectThread()
        throws RemoteException;

    public abstract boolean connect(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean connectHeadsetInternal(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean createIncomingConnect(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean disconnect(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean disconnectHeadsetInternal(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract int getAudioState(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract int getBatteryUsageHint(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract List<BluetoothDevice> getConnectedDevices()
        throws RemoteException;

    public abstract int getConnectionState(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
        throws RemoteException;

    public abstract int getPriority(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean isAudioConnected(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean rejectIncomingConnect(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean setAudioState(BluetoothDevice paramBluetoothDevice, int paramInt)
        throws RemoteException;

    public abstract boolean setPriority(BluetoothDevice paramBluetoothDevice, int paramInt)
        throws RemoteException;

    public abstract boolean startScoUsingVirtualVoiceCall(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean startVoiceRecognition(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean stopScoUsingVirtualVoiceCall(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public abstract boolean stopVoiceRecognition(BluetoothDevice paramBluetoothDevice)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBluetoothHeadset
    {
        private static final String DESCRIPTOR = "android.bluetooth.IBluetoothHeadset";
        static final int TRANSACTION_acceptIncomingConnect = 13;
        static final int TRANSACTION_cancelConnectThread = 15;
        static final int TRANSACTION_connect = 1;
        static final int TRANSACTION_connectHeadsetInternal = 16;
        static final int TRANSACTION_createIncomingConnect = 12;
        static final int TRANSACTION_disconnect = 2;
        static final int TRANSACTION_disconnectHeadsetInternal = 17;
        static final int TRANSACTION_getAudioState = 19;
        static final int TRANSACTION_getBatteryUsageHint = 11;
        static final int TRANSACTION_getConnectedDevices = 3;
        static final int TRANSACTION_getConnectionState = 5;
        static final int TRANSACTION_getDevicesMatchingConnectionStates = 4;
        static final int TRANSACTION_getPriority = 7;
        static final int TRANSACTION_isAudioConnected = 10;
        static final int TRANSACTION_rejectIncomingConnect = 14;
        static final int TRANSACTION_setAudioState = 18;
        static final int TRANSACTION_setPriority = 6;
        static final int TRANSACTION_startScoUsingVirtualVoiceCall = 20;
        static final int TRANSACTION_startVoiceRecognition = 8;
        static final int TRANSACTION_stopScoUsingVirtualVoiceCall = 21;
        static final int TRANSACTION_stopVoiceRecognition = 9;

        public Stub()
        {
            attachInterface(this, "android.bluetooth.IBluetoothHeadset");
        }

        public static IBluetoothHeadset asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.bluetooth.IBluetoothHeadset");
                if ((localIInterface != null) && ((localIInterface instanceof IBluetoothHeadset)))
                    localObject = (IBluetoothHeadset)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                while (true)
                {
                    return j;
                    paramParcel2.writeString("android.bluetooth.IBluetoothHeadset");
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice18 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice18 = null)
                    {
                        boolean bool15 = connect(localBluetoothDevice18);
                        paramParcel2.writeNoException();
                        if (bool15)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice17 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice17 = null)
                    {
                        boolean bool14 = disconnect(localBluetoothDevice17);
                        paramParcel2.writeNoException();
                        if (bool14)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    List localList2 = getConnectedDevices();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList2);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    List localList1 = getDevicesMatchingConnectionStates(paramParcel1.createIntArray());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList1);
                    continue;
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice16 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice16 = null)
                    {
                        int i1 = getConnectionState(localBluetoothDevice16);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i1);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice15 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice15 = null)
                    {
                        boolean bool13 = setPriority(localBluetoothDevice15, paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (bool13)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice14 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice14 = null)
                    {
                        int n = getPriority(localBluetoothDevice14);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(n);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice13 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice13 = null)
                    {
                        boolean bool12 = startVoiceRecognition(localBluetoothDevice13);
                        paramParcel2.writeNoException();
                        if (bool12)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice12 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice12 = null)
                    {
                        boolean bool11 = stopVoiceRecognition(localBluetoothDevice12);
                        paramParcel2.writeNoException();
                        if (bool11)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice11 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice11 = null)
                    {
                        boolean bool10 = isAudioConnected(localBluetoothDevice11);
                        paramParcel2.writeNoException();
                        if (bool10)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice10 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice10 = null)
                    {
                        int m = getBatteryUsageHint(localBluetoothDevice10);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(m);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice9 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice9 = null)
                    {
                        boolean bool9 = createIncomingConnect(localBluetoothDevice9);
                        paramParcel2.writeNoException();
                        if (bool9)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice8 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice8 = null)
                    {
                        boolean bool8 = acceptIncomingConnect(localBluetoothDevice8);
                        paramParcel2.writeNoException();
                        if (bool8)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    if (paramParcel1.readInt() != 0);
                    for (BluetoothDevice localBluetoothDevice7 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice7 = null)
                    {
                        boolean bool7 = rejectIncomingConnect(localBluetoothDevice7);
                        paramParcel2.writeNoException();
                        if (bool7)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                    boolean bool6 = cancelConnectThread();
                    paramParcel2.writeNoException();
                    if (bool6)
                        i = j;
                    paramParcel2.writeInt(i);
                }
            case 16:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice6 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice6 = null)
                {
                    boolean bool5 = connectHeadsetInternal(localBluetoothDevice6);
                    paramParcel2.writeNoException();
                    if (bool5)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 17:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice5 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice5 = null)
                {
                    boolean bool4 = disconnectHeadsetInternal(localBluetoothDevice5);
                    paramParcel2.writeNoException();
                    if (bool4)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 18:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice4 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice4 = null)
                {
                    boolean bool3 = setAudioState(localBluetoothDevice4, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool3)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 19:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice3 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice3 = null)
                {
                    int k = getAudioState(localBluetoothDevice3);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(k);
                    break;
                }
            case 20:
                paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
                if (paramParcel1.readInt() != 0);
                for (BluetoothDevice localBluetoothDevice2 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice2 = null)
                {
                    boolean bool2 = startScoUsingVirtualVoiceCall(localBluetoothDevice2);
                    paramParcel2.writeNoException();
                    if (bool2)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 21:
            }
            paramParcel1.enforceInterface("android.bluetooth.IBluetoothHeadset");
            if (paramParcel1.readInt() != 0);
            for (BluetoothDevice localBluetoothDevice1 = (BluetoothDevice)BluetoothDevice.CREATOR.createFromParcel(paramParcel1); ; localBluetoothDevice1 = null)
            {
                boolean bool1 = stopScoUsingVirtualVoiceCall(localBluetoothDevice1);
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                break;
            }
        }

        private static class Proxy
            implements IBluetoothHeadset
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public boolean acceptIncomingConnect(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(13, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean cancelConnectThread()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean connect(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(1, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean connectHeadsetInternal(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(16, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean createIncomingConnect(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(12, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean disconnect(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(2, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean disconnectHeadsetInternal(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(17, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public int getAudioState(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(19, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getBatteryUsageHint(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getConnectedDevices()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getConnectionState(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<BluetoothDevice> getDevicesMatchingConnectionStates(int[] paramArrayOfInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                    localParcel1.writeIntArray(paramArrayOfInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(BluetoothDevice.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.bluetooth.IBluetoothHeadset";
            }

            public int getPriority(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                    if (paramBluetoothDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramBluetoothDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isAudioConnected(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(10, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean rejectIncomingConnect(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(14, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean setAudioState(BluetoothDevice paramBluetoothDevice, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(18, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean setPriority(BluetoothDevice paramBluetoothDevice, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(6, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean startScoUsingVirtualVoiceCall(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(20, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean startVoiceRecognition(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(8, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean stopScoUsingVirtualVoiceCall(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(21, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean stopVoiceRecognition(BluetoothDevice paramBluetoothDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.bluetooth.IBluetoothHeadset");
                        if (paramBluetoothDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramBluetoothDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(9, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.IBluetoothHeadset
 * JD-Core Version:        0.6.2
 */