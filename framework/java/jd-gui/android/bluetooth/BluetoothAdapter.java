package android.bluetooth;

import android.content.Context;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.util.Pair;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

public final class BluetoothAdapter
{
    public static final String ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED";
    public static final String ACTION_DISCOVERY_FINISHED = "android.bluetooth.adapter.action.DISCOVERY_FINISHED";
    public static final String ACTION_DISCOVERY_STARTED = "android.bluetooth.adapter.action.DISCOVERY_STARTED";
    public static final String ACTION_LOCAL_NAME_CHANGED = "android.bluetooth.adapter.action.LOCAL_NAME_CHANGED";
    public static final String ACTION_REQUEST_DISCOVERABLE = "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE";
    public static final String ACTION_REQUEST_ENABLE = "android.bluetooth.adapter.action.REQUEST_ENABLE";
    public static final String ACTION_SCAN_MODE_CHANGED = "android.bluetooth.adapter.action.SCAN_MODE_CHANGED";
    public static final String ACTION_STATE_CHANGED = "android.bluetooth.adapter.action.STATE_CHANGED";
    private static final int ADDRESS_LENGTH = 17;
    public static final String BLUETOOTH_SERVICE = "bluetooth";
    private static final boolean DBG = false;
    public static final int ERROR = -2147483648;
    public static final String EXTRA_CONNECTION_STATE = "android.bluetooth.adapter.extra.CONNECTION_STATE";
    public static final String EXTRA_DISCOVERABLE_DURATION = "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION";
    public static final String EXTRA_LOCAL_NAME = "android.bluetooth.adapter.extra.LOCAL_NAME";
    public static final String EXTRA_PREVIOUS_CONNECTION_STATE = "android.bluetooth.adapter.extra.PREVIOUS_CONNECTION_STATE";
    public static final String EXTRA_PREVIOUS_SCAN_MODE = "android.bluetooth.adapter.extra.PREVIOUS_SCAN_MODE";
    public static final String EXTRA_PREVIOUS_STATE = "android.bluetooth.adapter.extra.PREVIOUS_STATE";
    public static final String EXTRA_SCAN_MODE = "android.bluetooth.adapter.extra.SCAN_MODE";
    public static final String EXTRA_STATE = "android.bluetooth.adapter.extra.STATE";
    public static final int SCAN_MODE_CONNECTABLE = 21;
    public static final int SCAN_MODE_CONNECTABLE_DISCOVERABLE = 23;
    public static final int SCAN_MODE_NONE = 20;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_DISCONNECTING = 3;
    public static final int STATE_OFF = 10;
    public static final int STATE_ON = 12;
    public static final int STATE_TURNING_OFF = 13;
    public static final int STATE_TURNING_ON = 11;
    private static final String TAG = "BluetoothAdapter";
    private static BluetoothAdapter sAdapter;
    private final IBluetooth mService;
    private Handler mServiceRecordHandler;

    public BluetoothAdapter(IBluetooth paramIBluetooth)
    {
        if (paramIBluetooth == null)
            throw new IllegalArgumentException("service is null");
        this.mService = paramIBluetooth;
        this.mServiceRecordHandler = null;
    }

    public static boolean checkBluetoothAddress(String paramString)
    {
        boolean bool = false;
        if ((paramString == null) || (paramString.length() != 17));
        while (true)
        {
            return bool;
            int i = 0;
            label19: if (i < 17)
            {
                int j = paramString.charAt(i);
                switch (i % 3)
                {
                default:
                case 0:
                case 1:
                case 2:
                }
                while (true)
                {
                    i++;
                    break label19;
                    if ((j < 48) || (j > 57))
                    {
                        if ((j < 65) || (j > 70))
                            break;
                        continue;
                        if (j != 58)
                            break;
                    }
                }
            }
            bool = true;
        }
    }

    // ERROR //
    private BluetoothServerSocket createNewRfcommSocketAndRecord(String paramString, UUID paramUUID, boolean paramBoolean1, boolean paramBoolean2)
        throws IOException
    {
        // Byte code:
        //     0: new 14	android/bluetooth/BluetoothAdapter$RfcommChannelPicker
        //     3: dup
        //     4: aload_2
        //     5: invokespecial 144	android/bluetooth/BluetoothAdapter$RfcommChannelPicker:<init>	(Ljava/util/UUID;)V
        //     8: astore 5
        //     10: aload 5
        //     12: invokevirtual 147	android/bluetooth/BluetoothAdapter$RfcommChannelPicker:nextChannel	()I
        //     15: istore 6
        //     17: iload 6
        //     19: bipush 255
        //     21: if_icmpne +13 -> 34
        //     24: new 139	java/io/IOException
        //     27: dup
        //     28: ldc 149
        //     30: invokespecial 150	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     33: athrow
        //     34: new 152	android/bluetooth/BluetoothServerSocket
        //     37: dup
        //     38: iconst_1
        //     39: iload_3
        //     40: iload 4
        //     42: iload 6
        //     44: invokespecial 155	android/bluetooth/BluetoothServerSocket:<init>	(IZZI)V
        //     47: astore 7
        //     49: aload 7
        //     51: getfield 159	android/bluetooth/BluetoothServerSocket:mSocket	Landroid/bluetooth/BluetoothSocket;
        //     54: invokevirtual 164	android/bluetooth/BluetoothSocket:bindListen	()I
        //     57: istore 8
        //     59: iload 8
        //     61: ifne +79 -> 140
        //     64: bipush 255
        //     66: istore 11
        //     68: aload_0
        //     69: getfield 119	android/bluetooth/BluetoothAdapter:mService	Landroid/bluetooth/IBluetooth;
        //     72: aload_1
        //     73: new 166	android/os/ParcelUuid
        //     76: dup
        //     77: aload_2
        //     78: invokespecial 167	android/os/ParcelUuid:<init>	(Ljava/util/UUID;)V
        //     81: iload 6
        //     83: new 169	android/os/Binder
        //     86: dup
        //     87: invokespecial 170	android/os/Binder:<init>	()V
        //     90: invokeinterface 176 5 0
        //     95: istore 15
        //     97: iload 15
        //     99: istore 11
        //     101: iload 11
        //     103: bipush 255
        //     105: if_icmpne +88 -> 193
        //     108: aload 7
        //     110: invokevirtual 179	android/bluetooth/BluetoothServerSocket:close	()V
        //     113: new 139	java/io/IOException
        //     116: dup
        //     117: new 181	java/lang/StringBuilder
        //     120: dup
        //     121: invokespecial 182	java/lang/StringBuilder:<init>	()V
        //     124: ldc 184
        //     126: invokevirtual 188	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     129: aload_1
        //     130: invokevirtual 188	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     133: invokevirtual 192	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     136: invokespecial 150	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     139: athrow
        //     140: iload 8
        //     142: bipush 98
        //     144: if_icmpne +16 -> 160
        //     147: aload 7
        //     149: invokevirtual 179	android/bluetooth/BluetoothServerSocket:close	()V
        //     152: goto -142 -> 10
        //     155: astore 10
        //     157: goto -147 -> 10
        //     160: aload 7
        //     162: invokevirtual 179	android/bluetooth/BluetoothServerSocket:close	()V
        //     165: aload 7
        //     167: getfield 159	android/bluetooth/BluetoothServerSocket:mSocket	Landroid/bluetooth/BluetoothSocket;
        //     170: iload 8
        //     172: invokevirtual 196	android/bluetooth/BluetoothSocket:throwErrnoNative	(I)V
        //     175: goto -165 -> 10
        //     178: astore 12
        //     180: ldc 99
        //     182: ldc 198
        //     184: aload 12
        //     186: invokestatic 204	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     189: pop
        //     190: goto -89 -> 101
        //     193: aload_0
        //     194: getfield 121	android/bluetooth/BluetoothAdapter:mServiceRecordHandler	Landroid/os/Handler;
        //     197: ifnonnull +18 -> 215
        //     200: aload_0
        //     201: new 6	android/bluetooth/BluetoothAdapter$1
        //     204: dup
        //     205: aload_0
        //     206: invokestatic 210	android/os/Looper:getMainLooper	()Landroid/os/Looper;
        //     209: invokespecial 213	android/bluetooth/BluetoothAdapter$1:<init>	(Landroid/bluetooth/BluetoothAdapter;Landroid/os/Looper;)V
        //     212: putfield 121	android/bluetooth/BluetoothAdapter:mServiceRecordHandler	Landroid/os/Handler;
        //     215: aload 7
        //     217: aload_0
        //     218: getfield 121	android/bluetooth/BluetoothAdapter:mServiceRecordHandler	Landroid/os/Handler;
        //     221: iload 11
        //     223: invokevirtual 217	android/bluetooth/BluetoothServerSocket:setCloseHandler	(Landroid/os/Handler;I)V
        //     226: aload 7
        //     228: areturn
        //     229: astore 9
        //     231: goto -66 -> 165
        //     234: astore 14
        //     236: goto -123 -> 113
        //
        // Exception table:
        //     from	to	target	type
        //     147	152	155	java/io/IOException
        //     68	97	178	android/os/RemoteException
        //     160	165	229	java/io/IOException
        //     108	113	234	java/io/IOException
    }

    /** @deprecated */
    public static BluetoothAdapter getDefaultAdapter()
    {
        try
        {
            if (sAdapter == null)
            {
                IBinder localIBinder = ServiceManager.getService("bluetooth");
                if (localIBinder != null)
                    sAdapter = new BluetoothAdapter(IBluetooth.Stub.asInterface(localIBinder));
            }
            BluetoothAdapter localBluetoothAdapter = sAdapter;
            return localBluetoothAdapter;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static BluetoothServerSocket listenUsingScoOn()
        throws IOException
    {
        BluetoothServerSocket localBluetoothServerSocket = new BluetoothServerSocket(2, false, false, -1);
        int i = localBluetoothServerSocket.mSocket.bindListen();
        if (i != 0);
        try
        {
            localBluetoothServerSocket.close();
            label29: localBluetoothServerSocket.mSocket.throwErrnoNative(i);
            return localBluetoothServerSocket;
        }
        catch (IOException localIOException)
        {
            break label29;
        }
    }

    private Set<BluetoothDevice> toDeviceSet(String[] paramArrayOfString)
    {
        HashSet localHashSet = new HashSet(paramArrayOfString.length);
        for (int i = 0; i < paramArrayOfString.length; i++)
            localHashSet.add(getRemoteDevice(paramArrayOfString[i]));
        return Collections.unmodifiableSet(localHashSet);
    }

    public boolean cancelDiscovery()
    {
        boolean bool1 = false;
        if (getState() != 12);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.cancelDiscovery();
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public boolean changeApplicationBluetoothState(boolean paramBoolean, BluetoothStateChangeCallback paramBluetoothStateChangeCallback)
    {
        boolean bool1 = false;
        if (paramBluetoothStateChangeCallback == null);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.changeApplicationBluetoothState(paramBoolean, new StateChangeCallbackWrapper(paramBluetoothStateChangeCallback), new Binder());
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "changeBluetoothState", localRemoteException);
            }
        }
    }

    public void closeProfileProxy(int paramInt, BluetoothProfile paramBluetoothProfile)
    {
        if (paramBluetoothProfile == null);
        while (true)
        {
            return;
            switch (paramInt)
            {
            default:
                break;
            case 1:
                ((BluetoothHeadset)paramBluetoothProfile).close();
                break;
            case 2:
                ((BluetoothA2dp)paramBluetoothProfile).close();
                break;
            case 4:
                ((BluetoothInputDevice)paramBluetoothProfile).close();
                break;
            case 5:
                ((BluetoothPan)paramBluetoothProfile).close();
                break;
            case 3:
                ((BluetoothHealth)paramBluetoothProfile).close();
            }
        }
    }

    public boolean disable()
    {
        try
        {
            boolean bool2 = this.mService.disable(true);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean enable()
    {
        try
        {
            boolean bool2 = this.mService.enable();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean enableNoAutoConnect()
    {
        try
        {
            boolean bool2 = this.mService.enableNoAutoConnect();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public String getAddress()
    {
        try
        {
            String str2 = this.mService.getAddress();
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
                String str1 = null;
            }
        }
    }

    public Set<BluetoothDevice> getBondedDevices()
    {
        Object localObject;
        if (getState() != 12)
            localObject = toDeviceSet(new String[0]);
        while (true)
        {
            return localObject;
            try
            {
                Set localSet = toDeviceSet(this.mService.listBonds());
                localObject = localSet;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
                localObject = null;
            }
        }
    }

    public int getConnectionState()
    {
        int i = 0;
        if (getState() != 12);
        while (true)
        {
            return i;
            try
            {
                int j = this.mService.getAdapterConnectionState();
                i = j;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "getConnectionState:", localRemoteException);
            }
        }
    }

    public int getDiscoverableTimeout()
    {
        int i = -1;
        if (getState() != 12);
        while (true)
        {
            return i;
            try
            {
                int j = this.mService.getDiscoverableTimeout();
                i = j;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public String getName()
    {
        try
        {
            String str2 = this.mService.getName();
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
                String str1 = null;
            }
        }
    }

    public int getProfileConnectionState(int paramInt)
    {
        int i = 0;
        if (getState() != 12);
        while (true)
        {
            return i;
            try
            {
                int j = this.mService.getProfileConnectionState(paramInt);
                i = j;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "getProfileConnectionState:", localRemoteException);
            }
        }
    }

    public boolean getProfileProxy(Context paramContext, BluetoothProfile.ServiceListener paramServiceListener, int paramInt)
    {
        int i = 1;
        if ((paramContext == null) || (paramServiceListener == null))
            i = 0;
        while (true)
        {
            return i;
            if (paramInt == i)
                new BluetoothHeadset(paramContext, paramServiceListener);
            else if (paramInt == 2)
                new BluetoothA2dp(paramContext, paramServiceListener);
            else if (paramInt == 4)
                new BluetoothInputDevice(paramContext, paramServiceListener);
            else if (paramInt == 5)
                new BluetoothPan(paramContext, paramServiceListener);
            else if (paramInt == 3)
                new BluetoothHealth(paramContext, paramServiceListener);
            else
                i = 0;
        }
    }

    public BluetoothDevice getRemoteDevice(String paramString)
    {
        return new BluetoothDevice(paramString);
    }

    public BluetoothDevice getRemoteDevice(byte[] paramArrayOfByte)
    {
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length != 6))
            throw new IllegalArgumentException("Bluetooth address must have 6 bytes");
        Object[] arrayOfObject = new Object[6];
        arrayOfObject[0] = Byte.valueOf(paramArrayOfByte[0]);
        arrayOfObject[1] = Byte.valueOf(paramArrayOfByte[1]);
        arrayOfObject[2] = Byte.valueOf(paramArrayOfByte[2]);
        arrayOfObject[3] = Byte.valueOf(paramArrayOfByte[3]);
        arrayOfObject[4] = Byte.valueOf(paramArrayOfByte[4]);
        arrayOfObject[5] = Byte.valueOf(paramArrayOfByte[5]);
        return new BluetoothDevice(String.format("%02X:%02X:%02X:%02X:%02X:%02X", arrayOfObject));
    }

    public int getScanMode()
    {
        int i = 20;
        if (getState() != 12);
        while (true)
        {
            return i;
            try
            {
                int j = this.mService.getScanMode();
                i = j;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public int getState()
    {
        try
        {
            int j = this.mService.getBluetoothState();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
                int i = 10;
            }
        }
    }

    public ParcelUuid[] getUuids()
    {
        Object localObject = null;
        if (getState() != 12);
        while (true)
        {
            return localObject;
            try
            {
                ParcelUuid[] arrayOfParcelUuid = this.mService.getUuids();
                localObject = arrayOfParcelUuid;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public boolean isDiscovering()
    {
        boolean bool1 = false;
        if (getState() != 12);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.isDiscovering();
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public boolean isEnabled()
    {
        try
        {
            boolean bool2 = this.mService.isEnabled();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public BluetoothServerSocket listenUsingEncryptedRfcommOn(int paramInt)
        throws IOException
    {
        BluetoothServerSocket localBluetoothServerSocket = new BluetoothServerSocket(1, false, true, paramInt);
        int i = localBluetoothServerSocket.mSocket.bindListen();
        if (i != 0);
        try
        {
            localBluetoothServerSocket.close();
            label28: localBluetoothServerSocket.mSocket.throwErrnoNative(i);
            return localBluetoothServerSocket;
        }
        catch (IOException localIOException)
        {
            break label28;
        }
    }

    public BluetoothServerSocket listenUsingEncryptedRfcommWithServiceRecord(String paramString, UUID paramUUID)
        throws IOException
    {
        return createNewRfcommSocketAndRecord(paramString, paramUUID, false, true);
    }

    public BluetoothServerSocket listenUsingInsecureRfcommOn(int paramInt)
        throws IOException
    {
        BluetoothServerSocket localBluetoothServerSocket = new BluetoothServerSocket(1, false, false, paramInt);
        int i = localBluetoothServerSocket.mSocket.bindListen();
        if (i != 0);
        try
        {
            localBluetoothServerSocket.close();
            label28: localBluetoothServerSocket.mSocket.throwErrnoNative(i);
            return localBluetoothServerSocket;
        }
        catch (IOException localIOException)
        {
            break label28;
        }
    }

    public BluetoothServerSocket listenUsingInsecureRfcommWithServiceRecord(String paramString, UUID paramUUID)
        throws IOException
    {
        return createNewRfcommSocketAndRecord(paramString, paramUUID, false, false);
    }

    public BluetoothServerSocket listenUsingRfcommOn(int paramInt)
        throws IOException
    {
        BluetoothServerSocket localBluetoothServerSocket = new BluetoothServerSocket(1, true, true, paramInt);
        int i = localBluetoothServerSocket.mSocket.bindListen();
        if (i != 0);
        try
        {
            localBluetoothServerSocket.close();
            label28: localBluetoothServerSocket.mSocket.throwErrnoNative(i);
            return localBluetoothServerSocket;
        }
        catch (IOException localIOException)
        {
            break label28;
        }
    }

    public BluetoothServerSocket listenUsingRfcommWithServiceRecord(String paramString, UUID paramUUID)
        throws IOException
    {
        return createNewRfcommSocketAndRecord(paramString, paramUUID, true, true);
    }

    public Pair<byte[], byte[]> readOutOfBandData()
    {
        Object localObject = null;
        if (getState() != 12);
        while (true)
        {
            return localObject;
            try
            {
                byte[] arrayOfByte = this.mService.readOutOfBandData();
                if ((arrayOfByte != null) && (arrayOfByte.length == 32))
                {
                    Pair localPair = new Pair(Arrays.copyOfRange(arrayOfByte, 0, 16), Arrays.copyOfRange(arrayOfByte, 16, 32));
                    localObject = localPair;
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public void setDiscoverableTimeout(int paramInt)
    {
        if (getState() != 12);
        while (true)
        {
            return;
            try
            {
                this.mService.setDiscoverableTimeout(paramInt);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public boolean setName(String paramString)
    {
        boolean bool1 = false;
        if (getState() != 12);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.setName(paramString);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public boolean setScanMode(int paramInt)
    {
        if (getState() != 12);
        for (boolean bool = false; ; bool = setScanMode(paramInt, 120))
            return bool;
    }

    public boolean setScanMode(int paramInt1, int paramInt2)
    {
        boolean bool1 = false;
        if (getState() != 12);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.setScanMode(paramInt1, paramInt2);
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public boolean startDiscovery()
    {
        boolean bool1 = false;
        if (getState() != 12);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.startDiscovery();
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("BluetoothAdapter", "", localRemoteException);
            }
        }
    }

    public class StateChangeCallbackWrapper extends IBluetoothStateChangeCallback.Stub
    {
        private BluetoothAdapter.BluetoothStateChangeCallback mCallback;

        StateChangeCallbackWrapper(BluetoothAdapter.BluetoothStateChangeCallback arg2)
        {
            Object localObject;
            this.mCallback = localObject;
        }

        public void onBluetoothStateChange(boolean paramBoolean)
        {
            this.mCallback.onBluetoothStateChange(paramBoolean);
        }
    }

    public static abstract interface BluetoothStateChangeCallback
    {
        public abstract void onBluetoothStateChange(boolean paramBoolean);
    }

    private static class RfcommChannelPicker
    {
        private static final int[] RESERVED_RFCOMM_CHANNELS = arrayOfInt;
        private static LinkedList<Integer> sChannels;
        private static Random sRandom;
        private final LinkedList<Integer> mChannels;
        private final UUID mUuid;

        static
        {
            int[] arrayOfInt = new int[4];
            arrayOfInt[0] = 10;
            arrayOfInt[1] = 11;
            arrayOfInt[2] = 12;
            arrayOfInt[3] = 19;
        }

        public RfcommChannelPicker(UUID paramUUID)
        {
            try
            {
                if (sChannels == null)
                {
                    sChannels = new LinkedList();
                    for (int i = 1; i <= 30; i++)
                        sChannels.addLast(new Integer(i));
                    for (int m : RESERVED_RFCOMM_CHANNELS)
                        sChannels.remove(new Integer(m));
                    sRandom = new Random();
                }
                this.mChannels = ((LinkedList)sChannels.clone());
                this.mUuid = paramUUID;
                return;
            }
            finally
            {
            }
        }

        public int nextChannel()
        {
            if (this.mChannels.size() == 0);
            for (int i = -1; ; i = ((Integer)this.mChannels.remove(sRandom.nextInt(this.mChannels.size()))).intValue())
                return i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothAdapter
 * JD-Core Version:        0.6.2
 */