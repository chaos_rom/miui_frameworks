package android.bluetooth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.util.Log;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;

public class BluetoothProfileState extends StateMachine
{
    public static final int A2DP = 1;
    private static final boolean DBG = true;
    public static final int HFP = 0;
    public static final int HID = 2;
    private static final String TAG = "BluetoothProfileState";
    static final int TRANSITION_TO_STABLE = 100;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            BluetoothDevice localBluetoothDevice = (BluetoothDevice)paramAnonymousIntent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if (localBluetoothDevice == null);
            while (true)
            {
                return;
                if (str.equals("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"))
                {
                    int k = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                    if ((BluetoothProfileState.this.mProfile == 0) && ((k == 2) || (k == 0)))
                        BluetoothProfileState.this.sendMessage(100);
                }
                else if (str.equals("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"))
                {
                    int j = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                    if ((BluetoothProfileState.this.mProfile == 1) && ((j == 2) || (j == 0)))
                        BluetoothProfileState.this.sendMessage(100);
                }
                else if (str.equals("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"))
                {
                    int i = paramAnonymousIntent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                    if ((BluetoothProfileState.this.mProfile == 2) && ((i == 2) || (i == 0)))
                        BluetoothProfileState.this.sendMessage(100);
                }
                else if ((str.equals("android.bluetooth.device.action.ACL_DISCONNECTED")) && (localBluetoothDevice.equals(BluetoothProfileState.this.mPendingDevice)))
                {
                    BluetoothProfileState.this.sendMessage(100);
                }
            }
        }
    };
    private PendingCommandState mPendingCommandState = new PendingCommandState(null);
    private BluetoothDevice mPendingDevice;
    private int mProfile;
    private StableState mStableState = new StableState(null);

    public BluetoothProfileState(Context paramContext, int paramInt)
    {
        super("BluetoothProfileState:" + paramInt);
        this.mProfile = paramInt;
        addState(this.mStableState);
        addState(this.mPendingCommandState);
        setInitialState(this.mStableState);
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED");
        localIntentFilter.addAction("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED");
        localIntentFilter.addAction("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED");
        localIntentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        paramContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter);
    }

    private void log(String paramString)
    {
        Log.i("BluetoothProfileState", "Message:" + paramString);
    }

    private class PendingCommandState extends State
    {
        private PendingCommandState()
        {
        }

        private void dispatchMessage(Message paramMessage)
        {
            BluetoothDeviceProfileState localBluetoothDeviceProfileState = (BluetoothDeviceProfileState)paramMessage.obj;
            int i = paramMessage.arg1;
            if ((BluetoothProfileState.this.mPendingDevice == null) || (BluetoothProfileState.this.mPendingDevice.equals(localBluetoothDeviceProfileState.getDevice())))
            {
                BluetoothProfileState.access$302(BluetoothProfileState.this, localBluetoothDeviceProfileState.getDevice());
                localBluetoothDeviceProfileState.sendMessage(i);
            }
            while (true)
            {
                return;
                Message localMessage = new Message();
                localMessage.arg1 = i;
                localMessage.obj = localBluetoothDeviceProfileState;
                BluetoothProfileState.this.deferMessage(localMessage);
            }
        }

        public void enter()
        {
            BluetoothProfileState.this.log("Entering PendingCommandState State");
            dispatchMessage(BluetoothProfileState.this.getCurrentMessage());
        }

        public boolean processMessage(Message paramMessage)
        {
            if (paramMessage.what == 100)
                BluetoothProfileState.this.transitionTo(BluetoothProfileState.this.mStableState);
            while (true)
            {
                return true;
                dispatchMessage(paramMessage);
            }
        }
    }

    private class StableState extends State
    {
        private StableState()
        {
        }

        public void enter()
        {
            BluetoothProfileState.this.log("Entering Stable State");
            BluetoothProfileState.access$302(BluetoothProfileState.this, null);
        }

        public boolean processMessage(Message paramMessage)
        {
            if (paramMessage.what != 100)
                BluetoothProfileState.this.transitionTo(BluetoothProfileState.this.mPendingCommandState);
            return true;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.bluetooth.BluetoothProfileState
 * JD-Core Version:        0.6.2
 */