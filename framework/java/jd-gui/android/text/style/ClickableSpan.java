package android.text.style;

import android.text.TextPaint;
import android.view.View;

public abstract class ClickableSpan extends CharacterStyle
    implements UpdateAppearance
{
    public abstract void onClick(View paramView);

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.setColor(paramTextPaint.linkColor);
        paramTextPaint.setUnderlineText(true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.ClickableSpan
 * JD-Core Version:        0.6.2
 */