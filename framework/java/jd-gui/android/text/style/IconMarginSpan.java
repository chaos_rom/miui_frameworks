package android.text.style;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.text.Layout;
import android.text.Spanned;

public class IconMarginSpan
    implements LeadingMarginSpan, LineHeightSpan
{
    private Bitmap mBitmap;
    private int mPad;

    public IconMarginSpan(Bitmap paramBitmap)
    {
        this.mBitmap = paramBitmap;
    }

    public IconMarginSpan(Bitmap paramBitmap, int paramInt)
    {
        this.mBitmap = paramBitmap;
        this.mPad = paramInt;
    }

    public void chooseHeight(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Paint.FontMetricsInt paramFontMetricsInt)
    {
        if (paramInt2 == ((Spanned)paramCharSequence).getSpanEnd(this))
        {
            int i = this.mBitmap.getHeight();
            int j = i - (paramInt4 + paramFontMetricsInt.descent - paramFontMetricsInt.ascent - paramInt3);
            if (j > 0)
                paramFontMetricsInt.descent = (j + paramFontMetricsInt.descent);
            int k = i - (paramInt4 + paramFontMetricsInt.bottom - paramFontMetricsInt.top - paramInt3);
            if (k > 0)
                paramFontMetricsInt.bottom = (k + paramFontMetricsInt.bottom);
        }
    }

    public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout)
    {
        int i = paramLayout.getLineTop(paramLayout.getLineForOffset(((Spanned)paramCharSequence).getSpanStart(this)));
        if (paramInt2 < 0)
            paramInt1 -= this.mBitmap.getWidth();
        paramCanvas.drawBitmap(this.mBitmap, paramInt1, i, paramPaint);
    }

    public int getLeadingMargin(boolean paramBoolean)
    {
        return this.mBitmap.getWidth() + this.mPad;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.IconMarginSpan
 * JD-Core Version:        0.6.2
 */