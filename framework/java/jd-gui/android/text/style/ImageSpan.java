package android.text.style;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;

public class ImageSpan extends DynamicDrawableSpan
{
    private Uri mContentUri;
    private Context mContext;
    private Drawable mDrawable;
    private int mResourceId;
    private String mSource;

    public ImageSpan(Context paramContext, int paramInt)
    {
        this(paramContext, paramInt, 0);
    }

    public ImageSpan(Context paramContext, int paramInt1, int paramInt2)
    {
        super(paramInt2);
        this.mContext = paramContext;
        this.mResourceId = paramInt1;
    }

    public ImageSpan(Context paramContext, Bitmap paramBitmap)
    {
        this(paramContext, paramBitmap, 0);
    }

    public ImageSpan(Context paramContext, Bitmap paramBitmap, int paramInt)
    {
        super(paramInt);
        this.mContext = paramContext;
        BitmapDrawable localBitmapDrawable;
        int i;
        int j;
        Drawable localDrawable;
        if (paramContext != null)
        {
            localBitmapDrawable = new BitmapDrawable(paramContext.getResources(), paramBitmap);
            this.mDrawable = localBitmapDrawable;
            i = this.mDrawable.getIntrinsicWidth();
            j = this.mDrawable.getIntrinsicHeight();
            localDrawable = this.mDrawable;
            if (i <= 0)
                break label93;
            label63: if (j <= 0)
                break label99;
        }
        while (true)
        {
            localDrawable.setBounds(0, 0, i, j);
            return;
            localBitmapDrawable = new BitmapDrawable(paramBitmap);
            break;
            label93: i = 0;
            break label63;
            label99: j = 0;
        }
    }

    public ImageSpan(Context paramContext, Uri paramUri)
    {
        this(paramContext, paramUri, 0);
    }

    public ImageSpan(Context paramContext, Uri paramUri, int paramInt)
    {
        super(paramInt);
        this.mContext = paramContext;
        this.mContentUri = paramUri;
        this.mSource = paramUri.toString();
    }

    @Deprecated
    public ImageSpan(Bitmap paramBitmap)
    {
        this(null, paramBitmap, 0);
    }

    @Deprecated
    public ImageSpan(Bitmap paramBitmap, int paramInt)
    {
        this(null, paramBitmap, paramInt);
    }

    public ImageSpan(Drawable paramDrawable)
    {
        this(paramDrawable, 0);
    }

    public ImageSpan(Drawable paramDrawable, int paramInt)
    {
        super(paramInt);
        this.mDrawable = paramDrawable;
    }

    public ImageSpan(Drawable paramDrawable, String paramString)
    {
        this(paramDrawable, paramString, 0);
    }

    public ImageSpan(Drawable paramDrawable, String paramString, int paramInt)
    {
        super(paramInt);
        this.mDrawable = paramDrawable;
        this.mSource = paramString;
    }

    // ERROR //
    public Drawable getDrawable()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: aload_0
        //     3: getfield 43	android/text/style/ImageSpan:mDrawable	Landroid/graphics/drawable/Drawable;
        //     6: ifnull +10 -> 16
        //     9: aload_0
        //     10: getfield 43	android/text/style/ImageSpan:mDrawable	Landroid/graphics/drawable/Drawable;
        //     13: astore_1
        //     14: aload_1
        //     15: areturn
        //     16: aload_0
        //     17: getfield 65	android/text/style/ImageSpan:mContentUri	Landroid/net/Uri;
        //     20: ifnull +107 -> 127
        //     23: aload_0
        //     24: getfield 24	android/text/style/ImageSpan:mContext	Landroid/content/Context;
        //     27: invokevirtual 91	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     30: aload_0
        //     31: getfield 65	android/text/style/ImageSpan:mContentUri	Landroid/net/Uri;
        //     34: invokevirtual 97	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
        //     37: astore 6
        //     39: aload 6
        //     41: invokestatic 103	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
        //     44: astore 7
        //     46: new 32	android/graphics/drawable/BitmapDrawable
        //     49: dup
        //     50: aload_0
        //     51: getfield 24	android/text/style/ImageSpan:mContext	Landroid/content/Context;
        //     54: invokevirtual 38	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     57: aload 7
        //     59: invokespecial 41	android/graphics/drawable/BitmapDrawable:<init>	(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
        //     62: astore 8
        //     64: aload 8
        //     66: iconst_0
        //     67: iconst_0
        //     68: aload 8
        //     70: invokevirtual 104	android/graphics/drawable/BitmapDrawable:getIntrinsicWidth	()I
        //     73: aload 8
        //     75: invokevirtual 105	android/graphics/drawable/BitmapDrawable:getIntrinsicHeight	()I
        //     78: invokevirtual 106	android/graphics/drawable/BitmapDrawable:setBounds	(IIII)V
        //     81: aload 6
        //     83: invokevirtual 112	java/io/InputStream:close	()V
        //     86: aload 8
        //     88: astore_1
        //     89: goto -75 -> 14
        //     92: astore 4
        //     94: ldc 114
        //     96: new 116	java/lang/StringBuilder
        //     99: dup
        //     100: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     103: ldc 120
        //     105: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     108: aload_0
        //     109: getfield 65	android/text/style/ImageSpan:mContentUri	Landroid/net/Uri;
        //     112: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     115: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     118: aload 4
        //     120: invokestatic 134	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     123: pop
        //     124: goto -110 -> 14
        //     127: aload_0
        //     128: getfield 24	android/text/style/ImageSpan:mContext	Landroid/content/Context;
        //     131: invokevirtual 38	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     134: aload_0
        //     135: getfield 26	android/text/style/ImageSpan:mResourceId	I
        //     138: invokevirtual 139	android/content/res/Resources:getDrawable	(I)Landroid/graphics/drawable/Drawable;
        //     141: astore_1
        //     142: aload_1
        //     143: iconst_0
        //     144: iconst_0
        //     145: aload_1
        //     146: invokevirtual 49	android/graphics/drawable/Drawable:getIntrinsicWidth	()I
        //     149: aload_1
        //     150: invokevirtual 52	android/graphics/drawable/Drawable:getIntrinsicHeight	()I
        //     153: invokevirtual 56	android/graphics/drawable/Drawable:setBounds	(IIII)V
        //     156: goto -142 -> 14
        //     159: astore_2
        //     160: ldc 114
        //     162: new 116	java/lang/StringBuilder
        //     165: dup
        //     166: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     169: ldc 141
        //     171: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     174: aload_0
        //     175: getfield 26	android/text/style/ImageSpan:mResourceId	I
        //     178: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     181: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     184: invokestatic 147	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     187: pop
        //     188: goto -174 -> 14
        //     191: astore 4
        //     193: aload 8
        //     195: astore_1
        //     196: goto -102 -> 94
        //
        // Exception table:
        //     from	to	target	type
        //     23	64	92	java/lang/Exception
        //     127	156	159	java/lang/Exception
        //     64	86	191	java/lang/Exception
    }

    public String getSource()
    {
        return this.mSource;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.ImageSpan
 * JD-Core Version:        0.6.2
 */