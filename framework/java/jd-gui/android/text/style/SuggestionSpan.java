package android.text.style;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import android.util.Log;
import com.android.internal.R.styleable;
import java.util.Arrays;
import java.util.Locale;

public class SuggestionSpan extends CharacterStyle
    implements ParcelableSpan
{
    public static final String ACTION_SUGGESTION_PICKED = "android.text.style.SUGGESTION_PICKED";
    public static final Parcelable.Creator<SuggestionSpan> CREATOR = new Parcelable.Creator()
    {
        public SuggestionSpan createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SuggestionSpan(paramAnonymousParcel);
        }

        public SuggestionSpan[] newArray(int paramAnonymousInt)
        {
            return new SuggestionSpan[paramAnonymousInt];
        }
    };
    public static final int FLAG_AUTO_CORRECTION = 4;
    public static final int FLAG_EASY_CORRECT = 1;
    public static final int FLAG_MISSPELLED = 2;
    public static final int SUGGESTIONS_MAX_SIZE = 5;
    public static final String SUGGESTION_SPAN_PICKED_AFTER = "after";
    public static final String SUGGESTION_SPAN_PICKED_BEFORE = "before";
    public static final String SUGGESTION_SPAN_PICKED_HASHCODE = "hashcode";
    private int mAutoCorrectionUnderlineColor;
    private float mAutoCorrectionUnderlineThickness;
    private int mEasyCorrectUnderlineColor;
    private float mEasyCorrectUnderlineThickness;
    private int mFlags;
    private final int mHashCode;
    private final String mLocaleString;
    private int mMisspelledUnderlineColor;
    private float mMisspelledUnderlineThickness;
    private final String mNotificationTargetClassName;
    private final String[] mSuggestions;

    public SuggestionSpan(Context paramContext, Locale paramLocale, String[] paramArrayOfString, int paramInt, Class<?> paramClass)
    {
        this.mSuggestions = ((String[])Arrays.copyOf(paramArrayOfString, Math.min(5, paramArrayOfString.length)));
        this.mFlags = paramInt;
        if (paramLocale != null)
        {
            this.mLocaleString = paramLocale.toString();
            if (paramClass == null)
                break label119;
        }
        label119: for (this.mNotificationTargetClassName = paramClass.getCanonicalName(); ; this.mNotificationTargetClassName = "")
        {
            this.mHashCode = hashCodeInternal(this.mSuggestions, this.mLocaleString, this.mNotificationTargetClassName);
            initStyle(paramContext);
            return;
            if (paramContext != null)
            {
                this.mLocaleString = paramContext.getResources().getConfiguration().locale.toString();
                break;
            }
            Log.e("SuggestionSpan", "No locale or context specified in SuggestionSpan constructor");
            this.mLocaleString = "";
            break;
        }
    }

    public SuggestionSpan(Context paramContext, String[] paramArrayOfString, int paramInt)
    {
        this(paramContext, null, paramArrayOfString, paramInt, null);
    }

    public SuggestionSpan(Parcel paramParcel)
    {
        this.mSuggestions = paramParcel.readStringArray();
        this.mFlags = paramParcel.readInt();
        this.mLocaleString = paramParcel.readString();
        this.mNotificationTargetClassName = paramParcel.readString();
        this.mHashCode = paramParcel.readInt();
        this.mEasyCorrectUnderlineColor = paramParcel.readInt();
        this.mEasyCorrectUnderlineThickness = paramParcel.readFloat();
        this.mMisspelledUnderlineColor = paramParcel.readInt();
        this.mMisspelledUnderlineThickness = paramParcel.readFloat();
        this.mAutoCorrectionUnderlineColor = paramParcel.readInt();
        this.mAutoCorrectionUnderlineThickness = paramParcel.readFloat();
    }

    public SuggestionSpan(Locale paramLocale, String[] paramArrayOfString, int paramInt)
    {
        this(null, paramLocale, paramArrayOfString, paramInt, null);
    }

    private static int hashCodeInternal(String[] paramArrayOfString, String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Long.valueOf(SystemClock.uptimeMillis());
        arrayOfObject[1] = paramArrayOfString;
        arrayOfObject[2] = paramString1;
        arrayOfObject[3] = paramString2;
        return Arrays.hashCode(arrayOfObject);
    }

    private void initStyle(Context paramContext)
    {
        if (paramContext == null)
        {
            this.mMisspelledUnderlineThickness = 0.0F;
            this.mEasyCorrectUnderlineThickness = 0.0F;
            this.mAutoCorrectionUnderlineThickness = 0.0F;
            this.mMisspelledUnderlineColor = -16777216;
            this.mEasyCorrectUnderlineColor = -16777216;
        }
        TypedArray localTypedArray3;
        for (this.mAutoCorrectionUnderlineColor = -16777216; ; this.mAutoCorrectionUnderlineColor = localTypedArray3.getColor(0, -16777216))
        {
            return;
            TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(null, R.styleable.SuggestionSpan, 16843697, 0);
            this.mMisspelledUnderlineThickness = localTypedArray1.getDimension(1, 0.0F);
            this.mMisspelledUnderlineColor = localTypedArray1.getColor(0, -16777216);
            TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(null, R.styleable.SuggestionSpan, 16843696, 0);
            this.mEasyCorrectUnderlineThickness = localTypedArray2.getDimension(1, 0.0F);
            this.mEasyCorrectUnderlineColor = localTypedArray2.getColor(0, -16777216);
            localTypedArray3 = paramContext.obtainStyledAttributes(null, R.styleable.SuggestionSpan, 16843698, 0);
            this.mAutoCorrectionUnderlineThickness = localTypedArray3.getDimension(1, 0.0F);
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (((paramObject instanceof SuggestionSpan)) && (((SuggestionSpan)paramObject).hashCode() == this.mHashCode))
            bool = true;
        return bool;
    }

    public int getFlags()
    {
        return this.mFlags;
    }

    public String getLocale()
    {
        return this.mLocaleString;
    }

    public String getNotificationTargetClassName()
    {
        return this.mNotificationTargetClassName;
    }

    public int getSpanTypeId()
    {
        return 19;
    }

    public String[] getSuggestions()
    {
        return this.mSuggestions;
    }

    public int getUnderlineColor()
    {
        int i = 0;
        int j;
        int k;
        label24: int m;
        if ((0x2 & this.mFlags) != 0)
        {
            j = 1;
            if ((0x1 & this.mFlags) == 0)
                break label56;
            k = 1;
            if ((0x4 & this.mFlags) == 0)
                break label61;
            m = 1;
            label36: if (k == 0)
                break label75;
            if (j != 0)
                break label67;
            i = this.mEasyCorrectUnderlineColor;
        }
        while (true)
        {
            return i;
            j = 0;
            break;
            label56: k = 0;
            break label24;
            label61: m = 0;
            break label36;
            label67: i = this.mMisspelledUnderlineColor;
            continue;
            label75: if (m != 0)
                i = this.mAutoCorrectionUnderlineColor;
        }
    }

    public int hashCode()
    {
        return this.mHashCode;
    }

    public void setFlags(int paramInt)
    {
        this.mFlags = paramInt;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        int i;
        int j;
        label22: int k;
        if ((0x2 & this.mFlags) != 0)
        {
            i = 1;
            if ((0x1 & this.mFlags) == 0)
                break label60;
            j = 1;
            if ((0x4 & this.mFlags) == 0)
                break label65;
            k = 1;
            label34: if (j == 0)
                break label93;
            if (i != 0)
                break label71;
            paramTextPaint.setUnderlineText(this.mEasyCorrectUnderlineColor, this.mEasyCorrectUnderlineThickness);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label60: j = 0;
            break label22;
            label65: k = 0;
            break label34;
            label71: if (paramTextPaint.underlineColor == 0)
            {
                paramTextPaint.setUnderlineText(this.mMisspelledUnderlineColor, this.mMisspelledUnderlineThickness);
                continue;
                label93: if (k != 0)
                    paramTextPaint.setUnderlineText(this.mAutoCorrectionUnderlineColor, this.mAutoCorrectionUnderlineThickness);
            }
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStringArray(this.mSuggestions);
        paramParcel.writeInt(this.mFlags);
        paramParcel.writeString(this.mLocaleString);
        paramParcel.writeString(this.mNotificationTargetClassName);
        paramParcel.writeInt(this.mHashCode);
        paramParcel.writeInt(this.mEasyCorrectUnderlineColor);
        paramParcel.writeFloat(this.mEasyCorrectUnderlineThickness);
        paramParcel.writeInt(this.mMisspelledUnderlineColor);
        paramParcel.writeFloat(this.mMisspelledUnderlineThickness);
        paramParcel.writeInt(this.mAutoCorrectionUnderlineColor);
        paramParcel.writeFloat(this.mAutoCorrectionUnderlineThickness);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.SuggestionSpan
 * JD-Core Version:        0.6.2
 */