package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

public abstract class DynamicDrawableSpan extends ReplacementSpan
{
    public static final int ALIGN_BASELINE = 1;
    public static final int ALIGN_BOTTOM = 0;
    private static final String TAG = "DynamicDrawableSpan";
    private WeakReference<Drawable> mDrawableRef;
    protected final int mVerticalAlignment;

    public DynamicDrawableSpan()
    {
        this.mVerticalAlignment = 0;
    }

    protected DynamicDrawableSpan(int paramInt)
    {
        this.mVerticalAlignment = paramInt;
    }

    private Drawable getCachedDrawable()
    {
        WeakReference localWeakReference = this.mDrawableRef;
        Drawable localDrawable = null;
        if (localWeakReference != null)
            localDrawable = (Drawable)localWeakReference.get();
        if (localDrawable == null)
        {
            localDrawable = getDrawable();
            this.mDrawableRef = new WeakReference(localDrawable);
        }
        return localDrawable;
    }

    public void draw(Canvas paramCanvas, CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint)
    {
        Drawable localDrawable = getCachedDrawable();
        paramCanvas.save();
        int i = paramInt5 - localDrawable.getBounds().bottom;
        if (this.mVerticalAlignment == 1)
            i -= paramPaint.getFontMetricsInt().descent;
        paramCanvas.translate(paramFloat, i);
        localDrawable.draw(paramCanvas);
        paramCanvas.restore();
    }

    public abstract Drawable getDrawable();

    public int getSize(Paint paramPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, Paint.FontMetricsInt paramFontMetricsInt)
    {
        Rect localRect = getCachedDrawable().getBounds();
        if (paramFontMetricsInt != null)
        {
            paramFontMetricsInt.ascent = (-localRect.bottom);
            paramFontMetricsInt.descent = 0;
            paramFontMetricsInt.top = paramFontMetricsInt.ascent;
            paramFontMetricsInt.bottom = 0;
        }
        return localRect.right;
    }

    public int getVerticalAlignment()
    {
        return this.mVerticalAlignment;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.DynamicDrawableSpan
 * JD-Core Version:        0.6.2
 */