package android.text.style;

import android.os.Parcel;
import android.text.Layout.Alignment;
import android.text.ParcelableSpan;

public abstract interface AlignmentSpan extends ParagraphStyle
{
    public abstract Layout.Alignment getAlignment();

    public static class Standard
        implements AlignmentSpan, ParcelableSpan
    {
        private final Layout.Alignment mAlignment;

        public Standard(Parcel paramParcel)
        {
            this.mAlignment = Layout.Alignment.valueOf(paramParcel.readString());
        }

        public Standard(Layout.Alignment paramAlignment)
        {
            this.mAlignment = paramAlignment;
        }

        public int describeContents()
        {
            return 0;
        }

        public Layout.Alignment getAlignment()
        {
            return this.mAlignment;
        }

        public int getSpanTypeId()
        {
            return 1;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeString(this.mAlignment.name());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.AlignmentSpan
 * JD-Core Version:        0.6.2
 */