package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class RelativeSizeSpan extends MetricAffectingSpan
    implements ParcelableSpan
{
    private final float mProportion;

    public RelativeSizeSpan(float paramFloat)
    {
        this.mProportion = paramFloat;
    }

    public RelativeSizeSpan(Parcel paramParcel)
    {
        this.mProportion = paramParcel.readFloat();
    }

    public int describeContents()
    {
        return 0;
    }

    public float getSizeChange()
    {
        return this.mProportion;
    }

    public int getSpanTypeId()
    {
        return 3;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.setTextSize(paramTextPaint.getTextSize() * this.mProportion);
    }

    public void updateMeasureState(TextPaint paramTextPaint)
    {
        paramTextPaint.setTextSize(paramTextPaint.getTextSize() * this.mProportion);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeFloat(this.mProportion);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.RelativeSizeSpan
 * JD-Core Version:        0.6.2
 */