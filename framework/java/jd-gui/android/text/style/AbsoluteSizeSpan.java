package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class AbsoluteSizeSpan extends MetricAffectingSpan
    implements ParcelableSpan
{
    private boolean mDip;
    private final int mSize;

    public AbsoluteSizeSpan(int paramInt)
    {
        this.mSize = paramInt;
    }

    public AbsoluteSizeSpan(int paramInt, boolean paramBoolean)
    {
        this.mSize = paramInt;
        this.mDip = paramBoolean;
    }

    public AbsoluteSizeSpan(Parcel paramParcel)
    {
        this.mSize = paramParcel.readInt();
        if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mDip = bool;
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean getDip()
    {
        return this.mDip;
    }

    public int getSize()
    {
        return this.mSize;
    }

    public int getSpanTypeId()
    {
        return 16;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        if (this.mDip)
            paramTextPaint.setTextSize(this.mSize * paramTextPaint.density);
        while (true)
        {
            return;
            paramTextPaint.setTextSize(this.mSize);
        }
    }

    public void updateMeasureState(TextPaint paramTextPaint)
    {
        if (this.mDip)
            paramTextPaint.setTextSize(this.mSize * paramTextPaint.density);
        while (true)
        {
            return;
            paramTextPaint.setTextSize(this.mSize);
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mSize);
        if (this.mDip);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.AbsoluteSizeSpan
 * JD-Core Version:        0.6.2
 */