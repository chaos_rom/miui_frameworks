package android.text.style;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.view.View;

public class URLSpan extends ClickableSpan
    implements ParcelableSpan
{
    private final String mURL;

    public URLSpan(Parcel paramParcel)
    {
        this.mURL = paramParcel.readString();
    }

    public URLSpan(String paramString)
    {
        this.mURL = paramString;
    }

    public int describeContents()
    {
        return 0;
    }

    public int getSpanTypeId()
    {
        return 11;
    }

    public String getURL()
    {
        return this.mURL;
    }

    public void onClick(View paramView)
    {
        Uri localUri = Uri.parse(getURL());
        Context localContext = paramView.getContext();
        Intent localIntent = new Intent("android.intent.action.VIEW", localUri);
        localIntent.putExtra("com.android.browser.application_id", localContext.getPackageName());
        localContext.startActivity(localIntent);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mURL);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.URLSpan
 * JD-Core Version:        0.6.2
 */