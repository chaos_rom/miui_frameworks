package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class SuperscriptSpan extends MetricAffectingSpan
    implements ParcelableSpan
{
    public SuperscriptSpan()
    {
    }

    public SuperscriptSpan(Parcel paramParcel)
    {
    }

    public int describeContents()
    {
        return 0;
    }

    public int getSpanTypeId()
    {
        return 14;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.baselineShift += (int)(paramTextPaint.ascent() / 2.0F);
    }

    public void updateMeasureState(TextPaint paramTextPaint)
    {
        paramTextPaint.baselineShift += (int)(paramTextPaint.ascent() / 2.0F);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.SuperscriptSpan
 * JD-Core Version:        0.6.2
 */