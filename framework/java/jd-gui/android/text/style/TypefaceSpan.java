package android.text.style;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class TypefaceSpan extends MetricAffectingSpan
    implements ParcelableSpan
{
    private final String mFamily;

    public TypefaceSpan(Parcel paramParcel)
    {
        this.mFamily = paramParcel.readString();
    }

    public TypefaceSpan(String paramString)
    {
        this.mFamily = paramString;
    }

    private static void apply(Paint paramPaint, String paramString)
    {
        Typeface localTypeface1 = paramPaint.getTypeface();
        if (localTypeface1 == null);
        for (int i = 0; ; i = localTypeface1.getStyle())
        {
            Typeface localTypeface2 = Typeface.create(paramString, i);
            int j = i & (0xFFFFFFFF ^ localTypeface2.getStyle());
            if ((j & 0x1) != 0)
                paramPaint.setFakeBoldText(true);
            if ((j & 0x2) != 0)
                paramPaint.setTextSkewX(-0.25F);
            paramPaint.setTypeface(localTypeface2);
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getFamily()
    {
        return this.mFamily;
    }

    public int getSpanTypeId()
    {
        return 13;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        apply(paramTextPaint, this.mFamily);
    }

    public void updateMeasureState(TextPaint paramTextPaint)
    {
        apply(paramTextPaint, this.mFamily);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mFamily);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.TypefaceSpan
 * JD-Core Version:        0.6.2
 */