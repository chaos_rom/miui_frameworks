package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class ForegroundColorSpan extends CharacterStyle
    implements UpdateAppearance, ParcelableSpan
{
    private final int mColor;

    public ForegroundColorSpan(int paramInt)
    {
        this.mColor = paramInt;
    }

    public ForegroundColorSpan(Parcel paramParcel)
    {
        this.mColor = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public int getForegroundColor()
    {
        return this.mColor;
    }

    public int getSpanTypeId()
    {
        return 2;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.setColor(this.mColor);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mColor);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.ForegroundColorSpan
 * JD-Core Version:        0.6.2
 */