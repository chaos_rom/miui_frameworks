package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class ScaleXSpan extends MetricAffectingSpan
    implements ParcelableSpan
{
    private final float mProportion;

    public ScaleXSpan(float paramFloat)
    {
        this.mProportion = paramFloat;
    }

    public ScaleXSpan(Parcel paramParcel)
    {
        this.mProportion = paramParcel.readFloat();
    }

    public int describeContents()
    {
        return 0;
    }

    public float getScaleX()
    {
        return this.mProportion;
    }

    public int getSpanTypeId()
    {
        return 4;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.setTextScaleX(paramTextPaint.getTextScaleX() * this.mProportion);
    }

    public void updateMeasureState(TextPaint paramTextPaint)
    {
        paramTextPaint.setTextScaleX(paramTextPaint.getTextScaleX() * this.mProportion);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeFloat(this.mProportion);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.ScaleXSpan
 * JD-Core Version:        0.6.2
 */