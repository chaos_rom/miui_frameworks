package android.text.style;

public abstract interface TabStopSpan extends ParagraphStyle
{
    public abstract int getTabStop();

    public static class Standard
        implements TabStopSpan
    {
        private int mTab;

        public Standard(int paramInt)
        {
            this.mTab = paramInt;
        }

        public int getTabStop()
        {
            return this.mTab;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.TabStopSpan
 * JD-Core Version:        0.6.2
 */