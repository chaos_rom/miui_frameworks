package android.text.style;

import android.graphics.Rasterizer;
import android.text.TextPaint;

public class RasterizerSpan extends CharacterStyle
    implements UpdateAppearance
{
    private Rasterizer mRasterizer;

    public RasterizerSpan(Rasterizer paramRasterizer)
    {
        this.mRasterizer = paramRasterizer;
    }

    public Rasterizer getRasterizer()
    {
        return this.mRasterizer;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.setRasterizer(this.mRasterizer);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.RasterizerSpan
 * JD-Core Version:        0.6.2
 */