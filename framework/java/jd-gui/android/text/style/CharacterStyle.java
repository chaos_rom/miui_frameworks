package android.text.style;

import android.text.TextPaint;

public abstract class CharacterStyle
{
    public static CharacterStyle wrap(CharacterStyle paramCharacterStyle)
    {
        if ((paramCharacterStyle instanceof MetricAffectingSpan));
        for (Object localObject = new MetricAffectingSpan.Passthrough((MetricAffectingSpan)paramCharacterStyle); ; localObject = new Passthrough(paramCharacterStyle))
            return localObject;
    }

    public CharacterStyle getUnderlying()
    {
        return this;
    }

    public abstract void updateDrawState(TextPaint paramTextPaint);

    private static class Passthrough extends CharacterStyle
    {
        private CharacterStyle mStyle;

        public Passthrough(CharacterStyle paramCharacterStyle)
        {
            this.mStyle = paramCharacterStyle;
        }

        public CharacterStyle getUnderlying()
        {
            return this.mStyle.getUnderlying();
        }

        public void updateDrawState(TextPaint paramTextPaint)
        {
            this.mStyle.updateDrawState(paramTextPaint);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.CharacterStyle
 * JD-Core Version:        0.6.2
 */