package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;
import android.text.Layout;
import android.text.ParcelableSpan;

public abstract interface LeadingMarginSpan extends ParagraphStyle
{
    public abstract void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout);

    public abstract int getLeadingMargin(boolean paramBoolean);

    public static class Standard
        implements LeadingMarginSpan, ParcelableSpan
    {
        private final int mFirst;
        private final int mRest;

        public Standard(int paramInt)
        {
            this(paramInt, paramInt);
        }

        public Standard(int paramInt1, int paramInt2)
        {
            this.mFirst = paramInt1;
            this.mRest = paramInt2;
        }

        public Standard(Parcel paramParcel)
        {
            this.mFirst = paramParcel.readInt();
            this.mRest = paramParcel.readInt();
        }

        public int describeContents()
        {
            return 0;
        }

        public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout)
        {
        }

        public int getLeadingMargin(boolean paramBoolean)
        {
            if (paramBoolean);
            for (int i = this.mFirst; ; i = this.mRest)
                return i;
        }

        public int getSpanTypeId()
        {
            return 10;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.mFirst);
            paramParcel.writeInt(this.mRest);
        }
    }

    public static abstract interface LeadingMarginSpan2 extends LeadingMarginSpan, WrapTogetherSpan
    {
        public abstract int getLeadingMarginLineCount();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.LeadingMarginSpan
 * JD-Core Version:        0.6.2
 */