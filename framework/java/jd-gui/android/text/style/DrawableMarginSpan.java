package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.Spanned;

public class DrawableMarginSpan
    implements LeadingMarginSpan, LineHeightSpan
{
    private Drawable mDrawable;
    private int mPad;

    public DrawableMarginSpan(Drawable paramDrawable)
    {
        this.mDrawable = paramDrawable;
    }

    public DrawableMarginSpan(Drawable paramDrawable, int paramInt)
    {
        this.mDrawable = paramDrawable;
        this.mPad = paramInt;
    }

    public void chooseHeight(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Paint.FontMetricsInt paramFontMetricsInt)
    {
        if (paramInt2 == ((Spanned)paramCharSequence).getSpanEnd(this))
        {
            int i = this.mDrawable.getIntrinsicHeight();
            int j = i - (paramInt4 + paramFontMetricsInt.descent - paramFontMetricsInt.ascent - paramInt3);
            if (j > 0)
                paramFontMetricsInt.descent = (j + paramFontMetricsInt.descent);
            int k = i - (paramInt4 + paramFontMetricsInt.bottom - paramFontMetricsInt.top - paramInt3);
            if (k > 0)
                paramFontMetricsInt.bottom = (k + paramFontMetricsInt.bottom);
        }
    }

    public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout)
    {
        int i = paramLayout.getLineTop(paramLayout.getLineForOffset(((Spanned)paramCharSequence).getSpanStart(this)));
        int j = this.mDrawable.getIntrinsicWidth();
        int k = this.mDrawable.getIntrinsicHeight();
        this.mDrawable.setBounds(paramInt1, i, paramInt1 + j, i + k);
        this.mDrawable.draw(paramCanvas);
    }

    public int getLeadingMargin(boolean paramBoolean)
    {
        return this.mDrawable.getIntrinsicWidth() + this.mPad;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.DrawableMarginSpan
 * JD-Core Version:        0.6.2
 */