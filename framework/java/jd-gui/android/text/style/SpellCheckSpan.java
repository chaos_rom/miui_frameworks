package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;

public class SpellCheckSpan
    implements ParcelableSpan
{
    private boolean mSpellCheckInProgress;

    public SpellCheckSpan()
    {
        this.mSpellCheckInProgress = false;
    }

    public SpellCheckSpan(Parcel paramParcel)
    {
        if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mSpellCheckInProgress = bool;
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public int getSpanTypeId()
    {
        return 20;
    }

    public boolean isSpellCheckInProgress()
    {
        return this.mSpellCheckInProgress;
    }

    public void setSpellCheckInProgress(boolean paramBoolean)
    {
        this.mSpellCheckInProgress = paramBoolean;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (this.mSpellCheckInProgress);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.SpellCheckSpan
 * JD-Core Version:        0.6.2
 */