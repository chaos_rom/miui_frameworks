package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Parcel;
import android.text.Layout;
import android.text.ParcelableSpan;

public class QuoteSpan
    implements LeadingMarginSpan, ParcelableSpan
{
    private static final int GAP_WIDTH = 2;
    private static final int STRIPE_WIDTH = 2;
    private final int mColor;

    public QuoteSpan()
    {
        this.mColor = -16776961;
    }

    public QuoteSpan(int paramInt)
    {
        this.mColor = paramInt;
    }

    public QuoteSpan(Parcel paramParcel)
    {
        this.mColor = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout)
    {
        Paint.Style localStyle = paramPaint.getStyle();
        int i = paramPaint.getColor();
        paramPaint.setStyle(Paint.Style.FILL);
        paramPaint.setColor(this.mColor);
        paramCanvas.drawRect(paramInt1, paramInt3, paramInt1 + paramInt2 * 2, paramInt5, paramPaint);
        paramPaint.setStyle(localStyle);
        paramPaint.setColor(i);
    }

    public int getColor()
    {
        return this.mColor;
    }

    public int getLeadingMargin(boolean paramBoolean)
    {
        return 4;
    }

    public int getSpanTypeId()
    {
        return 9;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mColor);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.QuoteSpan
 * JD-Core Version:        0.6.2
 */