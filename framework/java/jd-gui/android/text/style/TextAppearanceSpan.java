package android.text.style;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import com.android.internal.R.styleable;

public class TextAppearanceSpan extends MetricAffectingSpan
    implements ParcelableSpan
{
    private final int mStyle;
    private final ColorStateList mTextColor;
    private final ColorStateList mTextColorLink;
    private final int mTextSize;
    private final String mTypeface;

    public TextAppearanceSpan(Context paramContext, int paramInt)
    {
        this(paramContext, paramInt, -1);
    }

    public TextAppearanceSpan(Context paramContext, int paramInt1, int paramInt2)
    {
        TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramInt1, R.styleable.TextAppearance);
        ColorStateList localColorStateList = localTypedArray1.getColorStateList(3);
        this.mTextColorLink = localTypedArray1.getColorStateList(6);
        this.mTextSize = localTypedArray1.getDimensionPixelSize(0, -1);
        this.mStyle = localTypedArray1.getInt(2, 0);
        String str = localTypedArray1.getString(8);
        if (str != null)
            this.mTypeface = str;
        while (true)
        {
            localTypedArray1.recycle();
            if (paramInt2 >= 0)
            {
                TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(16973829, R.styleable.Theme);
                localColorStateList = localTypedArray2.getColorStateList(paramInt2);
                localTypedArray2.recycle();
            }
            this.mTextColor = localColorStateList;
            return;
            switch (localTypedArray1.getInt(1, 0))
            {
            default:
                this.mTypeface = null;
                break;
            case 1:
                this.mTypeface = "sans";
                break;
            case 2:
                this.mTypeface = "serif";
                break;
            case 3:
                this.mTypeface = "monospace";
            }
        }
    }

    public TextAppearanceSpan(Parcel paramParcel)
    {
        this.mTypeface = paramParcel.readString();
        this.mStyle = paramParcel.readInt();
        this.mTextSize = paramParcel.readInt();
        if (paramParcel.readInt() != 0)
        {
            this.mTextColor = ((ColorStateList)ColorStateList.CREATOR.createFromParcel(paramParcel));
            if (paramParcel.readInt() == 0)
                break label83;
        }
        label83: for (this.mTextColorLink = ((ColorStateList)ColorStateList.CREATOR.createFromParcel(paramParcel)); ; this.mTextColorLink = null)
        {
            return;
            this.mTextColor = null;
            break;
        }
    }

    public TextAppearanceSpan(String paramString, int paramInt1, int paramInt2, ColorStateList paramColorStateList1, ColorStateList paramColorStateList2)
    {
        this.mTypeface = paramString;
        this.mStyle = paramInt1;
        this.mTextSize = paramInt2;
        this.mTextColor = paramColorStateList1;
        this.mTextColorLink = paramColorStateList2;
    }

    public int describeContents()
    {
        return 0;
    }

    public String getFamily()
    {
        return this.mTypeface;
    }

    public ColorStateList getLinkTextColor()
    {
        return this.mTextColorLink;
    }

    public int getSpanTypeId()
    {
        return 17;
    }

    public ColorStateList getTextColor()
    {
        return this.mTextColor;
    }

    public int getTextSize()
    {
        return this.mTextSize;
    }

    public int getTextStyle()
    {
        return this.mStyle;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        updateMeasureState(paramTextPaint);
        if (this.mTextColor != null)
            paramTextPaint.setColor(this.mTextColor.getColorForState(paramTextPaint.drawableState, 0));
        if (this.mTextColorLink != null)
            paramTextPaint.linkColor = this.mTextColorLink.getColorForState(paramTextPaint.drawableState, 0);
    }

    public void updateMeasureState(TextPaint paramTextPaint)
    {
        Typeface localTypeface1;
        int j;
        Typeface localTypeface2;
        if ((this.mTypeface != null) || (this.mStyle != 0))
        {
            localTypeface1 = paramTextPaint.getTypeface();
            int i = 0;
            if (localTypeface1 != null)
                i = localTypeface1.getStyle();
            j = i | this.mStyle;
            if (this.mTypeface == null)
                break label118;
            localTypeface2 = Typeface.create(this.mTypeface, j);
        }
        while (true)
        {
            int k = j & (0xFFFFFFFF ^ localTypeface2.getStyle());
            if ((k & 0x1) != 0)
                paramTextPaint.setFakeBoldText(true);
            if ((k & 0x2) != 0)
                paramTextPaint.setTextSkewX(-0.25F);
            paramTextPaint.setTypeface(localTypeface2);
            if (this.mTextSize > 0)
                paramTextPaint.setTextSize(this.mTextSize);
            return;
            label118: if (localTypeface1 == null)
                localTypeface2 = Typeface.defaultFromStyle(j);
            else
                localTypeface2 = Typeface.create(localTypeface1, j);
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mTypeface);
        paramParcel.writeInt(this.mStyle);
        paramParcel.writeInt(this.mTextSize);
        if (this.mTextColor != null)
        {
            paramParcel.writeInt(1);
            this.mTextColor.writeToParcel(paramParcel, paramInt);
            if (this.mTextColorLink == null)
                break label75;
            paramParcel.writeInt(1);
            this.mTextColorLink.writeToParcel(paramParcel, paramInt);
        }
        while (true)
        {
            return;
            paramParcel.writeInt(0);
            break;
            label75: paramParcel.writeInt(0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.TextAppearanceSpan
 * JD-Core Version:        0.6.2
 */