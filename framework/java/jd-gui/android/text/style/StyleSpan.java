package android.text.style;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class StyleSpan extends MetricAffectingSpan
    implements ParcelableSpan
{
    private final int mStyle;

    public StyleSpan(int paramInt)
    {
        this.mStyle = paramInt;
    }

    public StyleSpan(Parcel paramParcel)
    {
        this.mStyle = paramParcel.readInt();
    }

    private static void apply(Paint paramPaint, int paramInt)
    {
        Typeface localTypeface1 = paramPaint.getTypeface();
        int i;
        int j;
        if (localTypeface1 == null)
        {
            i = 0;
            j = i | paramInt;
            if (localTypeface1 != null)
                break label81;
        }
        label81: for (Typeface localTypeface2 = Typeface.defaultFromStyle(j); ; localTypeface2 = Typeface.create(localTypeface1, j))
        {
            int k = j & (0xFFFFFFFF ^ localTypeface2.getStyle());
            if ((k & 0x1) != 0)
                paramPaint.setFakeBoldText(true);
            if ((k & 0x2) != 0)
                paramPaint.setTextSkewX(-0.25F);
            paramPaint.setTypeface(localTypeface2);
            return;
            i = localTypeface1.getStyle();
            break;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public int getSpanTypeId()
    {
        return 7;
    }

    public int getStyle()
    {
        return this.mStyle;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        apply(paramTextPaint, this.mStyle);
    }

    public void updateMeasureState(TextPaint paramTextPaint)
    {
        apply(paramTextPaint, this.mStyle);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mStyle);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.StyleSpan
 * JD-Core Version:        0.6.2
 */