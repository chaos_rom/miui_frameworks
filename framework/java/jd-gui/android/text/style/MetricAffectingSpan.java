package android.text.style;

import android.text.TextPaint;

public abstract class MetricAffectingSpan extends CharacterStyle
    implements UpdateLayout
{
    public MetricAffectingSpan getUnderlying()
    {
        return this;
    }

    public abstract void updateMeasureState(TextPaint paramTextPaint);

    static class Passthrough extends MetricAffectingSpan
    {
        private MetricAffectingSpan mStyle;

        public Passthrough(MetricAffectingSpan paramMetricAffectingSpan)
        {
            this.mStyle = paramMetricAffectingSpan;
        }

        public MetricAffectingSpan getUnderlying()
        {
            return this.mStyle.getUnderlying();
        }

        public void updateDrawState(TextPaint paramTextPaint)
        {
            this.mStyle.updateDrawState(paramTextPaint);
        }

        public void updateMeasureState(TextPaint paramTextPaint)
        {
            this.mStyle.updateMeasureState(paramTextPaint);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.MetricAffectingSpan
 * JD-Core Version:        0.6.2
 */