package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class SuggestionRangeSpan extends CharacterStyle
    implements ParcelableSpan
{
    private int mBackgroundColor;

    public SuggestionRangeSpan()
    {
        this.mBackgroundColor = 0;
    }

    public SuggestionRangeSpan(Parcel paramParcel)
    {
        this.mBackgroundColor = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public int getSpanTypeId()
    {
        return 21;
    }

    public void setBackgroundColor(int paramInt)
    {
        this.mBackgroundColor = paramInt;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.bgColor = this.mBackgroundColor;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mBackgroundColor);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.SuggestionRangeSpan
 * JD-Core Version:        0.6.2
 */