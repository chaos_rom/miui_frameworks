package android.text.style;

import android.graphics.MaskFilter;
import android.text.TextPaint;

public class MaskFilterSpan extends CharacterStyle
    implements UpdateAppearance
{
    private MaskFilter mFilter;

    public MaskFilterSpan(MaskFilter paramMaskFilter)
    {
        this.mFilter = paramMaskFilter;
    }

    public MaskFilter getMaskFilter()
    {
        return this.mFilter;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.setMaskFilter(this.mFilter);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.MaskFilterSpan
 * JD-Core Version:        0.6.2
 */