package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class BackgroundColorSpan extends CharacterStyle
    implements UpdateAppearance, ParcelableSpan
{
    private final int mColor;

    public BackgroundColorSpan(int paramInt)
    {
        this.mColor = paramInt;
    }

    public BackgroundColorSpan(Parcel paramParcel)
    {
        this.mColor = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public int getBackgroundColor()
    {
        return this.mColor;
    }

    public int getSpanTypeId()
    {
        return 12;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.bgColor = this.mColor;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mColor);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.BackgroundColorSpan
 * JD-Core Version:        0.6.2
 */