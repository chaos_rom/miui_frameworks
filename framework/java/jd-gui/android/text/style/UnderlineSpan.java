package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class UnderlineSpan extends CharacterStyle
    implements UpdateAppearance, ParcelableSpan
{
    public UnderlineSpan()
    {
    }

    public UnderlineSpan(Parcel paramParcel)
    {
    }

    public int describeContents()
    {
        return 0;
    }

    public int getSpanTypeId()
    {
        return 6;
    }

    public void updateDrawState(TextPaint paramTextPaint)
    {
        paramTextPaint.setUnderlineText(true);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.UnderlineSpan
 * JD-Core Version:        0.6.2
 */