package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.os.Parcel;
import android.text.Layout;
import android.text.ParcelableSpan;
import android.text.Spanned;

public class BulletSpan
    implements LeadingMarginSpan, ParcelableSpan
{
    private static final int BULLET_RADIUS = 3;
    public static final int STANDARD_GAP_WIDTH = 2;
    private static Path sBulletPath = null;
    private final int mColor;
    private final int mGapWidth;
    private final boolean mWantColor;

    public BulletSpan()
    {
        this.mGapWidth = 2;
        this.mWantColor = false;
        this.mColor = 0;
    }

    public BulletSpan(int paramInt)
    {
        this.mGapWidth = paramInt;
        this.mWantColor = false;
        this.mColor = 0;
    }

    public BulletSpan(int paramInt1, int paramInt2)
    {
        this.mGapWidth = paramInt1;
        this.mWantColor = true;
        this.mColor = paramInt2;
    }

    public BulletSpan(Parcel paramParcel)
    {
        this.mGapWidth = paramParcel.readInt();
        if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mWantColor = bool;
            this.mColor = paramParcel.readInt();
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout)
    {
        Paint.Style localStyle;
        int i;
        if (((Spanned)paramCharSequence).getSpanStart(this) == paramInt6)
        {
            localStyle = paramPaint.getStyle();
            i = 0;
            if (this.mWantColor)
            {
                i = paramPaint.getColor();
                paramPaint.setColor(this.mColor);
            }
            paramPaint.setStyle(Paint.Style.FILL);
            if (!paramCanvas.isHardwareAccelerated())
                break label145;
            if (sBulletPath == null)
            {
                sBulletPath = new Path();
                sBulletPath.addCircle(0.0F, 0.0F, 3.6F, Path.Direction.CW);
            }
            paramCanvas.save();
            paramCanvas.translate(paramInt1 + paramInt2 * 3, (paramInt3 + paramInt5) / 2.0F);
            paramCanvas.drawPath(sBulletPath, paramPaint);
            paramCanvas.restore();
        }
        while (true)
        {
            if (this.mWantColor)
                paramPaint.setColor(i);
            paramPaint.setStyle(localStyle);
            return;
            label145: paramCanvas.drawCircle(paramInt1 + paramInt2 * 3, (paramInt3 + paramInt5) / 2.0F, 3.0F, paramPaint);
        }
    }

    public int getLeadingMargin(boolean paramBoolean)
    {
        return 6 + this.mGapWidth;
    }

    public int getSpanTypeId()
    {
        return 8;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mGapWidth);
        if (this.mWantColor);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            paramParcel.writeInt(this.mColor);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.style.BulletSpan
 * JD-Core Version:        0.6.2
 */