package android.text;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import com.android.internal.util.ArrayUtils;
import java.lang.reflect.Array;

public class SpannableStringBuilder
    implements CharSequence, GetChars, Spannable, Editable, Appendable, GraphicsOperations
{
    private static final int END_MASK = 15;
    private static final int MARK = 1;
    private static final InputFilter[] NO_FILTERS = new InputFilter[0];
    private static final int PARAGRAPH = 3;
    private static final int POINT = 2;
    private static final int SPAN_END_AT_END = 32768;
    private static final int SPAN_END_AT_START = 16384;
    private static final int SPAN_START_AT_END = 8192;
    private static final int SPAN_START_AT_START = 4096;
    private static final int SPAN_START_END_MASK = 61440;
    private static final int START_MASK = 240;
    private static final int START_SHIFT = 4;
    private InputFilter[] mFilters = NO_FILTERS;
    private int mGapLength;
    private int mGapStart;
    private int mSpanCount;
    private int mSpanCountBeforeAdd;
    private int[] mSpanEnds;
    private int[] mSpanFlags;
    private int[] mSpanStarts;
    private Object[] mSpans;
    private char[] mText;

    public SpannableStringBuilder()
    {
        this("");
    }

    public SpannableStringBuilder(CharSequence paramCharSequence)
    {
        this(paramCharSequence, 0, paramCharSequence.length());
    }

    public SpannableStringBuilder(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        int i = paramInt2 - paramInt1;
        if (i < 0)
            throw new StringIndexOutOfBoundsException();
        int j = ArrayUtils.idealCharArraySize(i + 1);
        this.mText = new char[j];
        this.mGapStart = i;
        this.mGapLength = (j - i);
        TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, this.mText, 0);
        this.mSpanCount = 0;
        int k = ArrayUtils.idealIntArraySize(0);
        this.mSpans = new Object[k];
        this.mSpanStarts = new int[k];
        this.mSpanEnds = new int[k];
        this.mSpanFlags = new int[k];
        if ((paramCharSequence instanceof Spanned))
        {
            Spanned localSpanned = (Spanned)paramCharSequence;
            Object[] arrayOfObject = localSpanned.getSpans(paramInt1, paramInt2, Object.class);
            int m = 0;
            if (m < arrayOfObject.length)
            {
                if ((arrayOfObject[m] instanceof NoCopySpan));
                while (true)
                {
                    m++;
                    break;
                    int n = localSpanned.getSpanStart(arrayOfObject[m]) - paramInt1;
                    int i1 = localSpanned.getSpanEnd(arrayOfObject[m]) - paramInt1;
                    int i2 = localSpanned.getSpanFlags(arrayOfObject[m]);
                    if (n < 0)
                        n = 0;
                    if (n > paramInt2 - paramInt1)
                        n = paramInt2 - paramInt1;
                    if (i1 < 0)
                        i1 = 0;
                    if (i1 > paramInt2 - paramInt1)
                        i1 = paramInt2 - paramInt1;
                    setSpan(false, arrayOfObject[m], n, i1, i2);
                }
            }
        }
    }

    private void change(int paramInt1, int paramInt2, CharSequence paramCharSequence, int paramInt3, int paramInt4)
    {
        int i = paramInt2 - paramInt1;
        int j = paramInt4 - paramInt3;
        int k = j - i;
        int m = -1 + this.mSpanCount;
        if (m >= 0)
        {
            int i10 = this.mSpanStarts[m];
            if (i10 > this.mGapStart)
                i10 -= this.mGapLength;
            int i11 = this.mSpanEnds[m];
            if (i11 > this.mGapStart)
                i11 -= this.mGapLength;
            if ((0x33 & this.mSpanFlags[m]) == 51)
            {
                int i13 = i10;
                int i14 = i11;
                int i15 = length();
                if ((i10 > paramInt1) && (i10 <= paramInt2))
                {
                    i10 = paramInt2;
                    label131: if ((i10 < i15) && ((i10 <= paramInt2) || (charAt(i10 - 1) != '\n')))
                        break label290;
                }
                if ((i11 > paramInt1) && (i11 <= paramInt2))
                {
                    i11 = paramInt2;
                    label172: if ((i11 < i15) && ((i11 <= paramInt2) || (charAt(i11 - 1) != '\n')))
                        break label296;
                }
                if ((i10 != i13) || (i11 != i14))
                    setSpan(false, this.mSpans[m], i10, i11, this.mSpanFlags[m]);
            }
            int i12 = 0;
            if (i10 == paramInt1)
            {
                i12 = 0x0 | 0x1000;
                label251: if (i11 != paramInt1)
                    break label321;
                i12 = 0x4000 | i12;
            }
            while (true)
            {
                int[] arrayOfInt = this.mSpanFlags;
                arrayOfInt[m] = (i12 | arrayOfInt[m]);
                m--;
                break;
                label290: i10++;
                break label131;
                label296: i11++;
                break label172;
                if (i10 != paramInt2 + k)
                    break label251;
                i12 = 0x0 | 0x2000;
                break label251;
                label321: if (i11 == paramInt2 + k)
                    i12 |= 32768;
            }
        }
        moveGapTo(paramInt2);
        if (k >= this.mGapLength)
            resizeFor(k + this.mText.length - this.mGapLength);
        boolean bool1;
        int i8;
        if (j == 0)
        {
            bool1 = true;
            if (i > 0)
                i8 = 0;
        }
        else
        {
            while (true)
            {
                int i9 = this.mSpanCount;
                if (i8 >= i9)
                    break label526;
                if (((0x21 & this.mSpanFlags[i8]) == 33) && (this.mSpanStarts[i8] >= paramInt1) && (this.mSpanStarts[i8] < this.mGapStart + this.mGapLength) && (this.mSpanEnds[i8] >= paramInt1) && (this.mSpanEnds[i8] < this.mGapStart + this.mGapLength) && ((bool1) || (this.mSpanStarts[i8] > paramInt1) || (this.mSpanEnds[i8] < this.mGapStart)))
                {
                    removeSpan(i8);
                    continue;
                    bool1 = false;
                    break;
                }
                i8++;
            }
        }
        label526: this.mGapStart = (k + this.mGapStart);
        this.mGapLength -= k;
        if (this.mGapLength < 1)
            new Exception("mGapLength < 1").printStackTrace();
        TextUtils.getChars(paramCharSequence, paramInt3, paramInt4, this.mText, paramInt1);
        if (i > 0)
        {
            if (this.mGapStart + this.mGapLength == this.mText.length);
            for (boolean bool2 = true; ; bool2 = false)
                for (int i4 = 0; ; i4++)
                {
                    int i5 = this.mSpanCount;
                    if (i4 >= i5)
                        break;
                    int i6 = (0xF0 & this.mSpanFlags[i4]) >> 4;
                    this.mSpanStarts[i4] = updatedIntervalBound(this.mSpanStarts[i4], paramInt1, k, i6, bool2, bool1);
                    int i7 = 0xF & this.mSpanFlags[i4];
                    this.mSpanEnds[i4] = updatedIntervalBound(this.mSpanEnds[i4], paramInt1, k, i7, bool2, bool1);
                }
        }
        this.mSpanCountBeforeAdd = this.mSpanCount;
        if ((paramCharSequence instanceof Spanned))
        {
            Spanned localSpanned = (Spanned)paramCharSequence;
            Object[] arrayOfObject = localSpanned.getSpans(paramInt3, paramInt4, Object.class);
            for (int n = 0; ; n++)
            {
                int i1 = arrayOfObject.length;
                if (n >= i1)
                    break;
                int i2 = localSpanned.getSpanStart(arrayOfObject[n]);
                int i3 = localSpanned.getSpanEnd(arrayOfObject[n]);
                if (i2 < paramInt3)
                    i2 = paramInt3;
                if (i3 > paramInt4)
                    i3 = paramInt4;
                if (getSpanStart(arrayOfObject[n]) < 0)
                    setSpan(false, arrayOfObject[n], paramInt1 + (i2 - paramInt3), paramInt1 + (i3 - paramInt3), localSpanned.getSpanFlags(arrayOfObject[n]));
            }
        }
    }

    private void checkRange(String paramString, int paramInt1, int paramInt2)
    {
        if (paramInt2 < paramInt1)
            throw new IndexOutOfBoundsException(paramString + " " + region(paramInt1, paramInt2) + " has end before start");
        int i = length();
        if ((paramInt1 > i) || (paramInt2 > i))
            throw new IndexOutOfBoundsException(paramString + " " + region(paramInt1, paramInt2) + " ends beyond length " + i);
        if ((paramInt1 < 0) || (paramInt2 < 0))
            throw new IndexOutOfBoundsException(paramString + " " + region(paramInt1, paramInt2) + " starts before 0");
    }

    private static boolean hasNonExclusiveExclusiveSpanAt(CharSequence paramCharSequence, int paramInt)
    {
        int j;
        if ((paramCharSequence instanceof Spanned))
        {
            Spanned localSpanned = (Spanned)paramCharSequence;
            Object[] arrayOfObject = localSpanned.getSpans(paramInt, paramInt, Object.class);
            int i = arrayOfObject.length;
            j = 0;
            if (j < i)
                if (localSpanned.getSpanFlags(arrayOfObject[j]) == 33);
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    private void moveGapTo(int paramInt)
    {
        if (paramInt == this.mGapStart);
        while (true)
        {
            return;
            int i;
            label61: int k;
            label64: int m;
            int n;
            if (paramInt == length())
            {
                i = 1;
                if (paramInt >= this.mGapStart)
                    break label186;
                int i3 = this.mGapStart - paramInt;
                System.arraycopy(this.mText, paramInt, this.mText, this.mGapStart + this.mGapLength - i3, i3);
                k = 0;
                if (k >= this.mSpanCount)
                    break label315;
                m = this.mSpanStarts[k];
                n = this.mSpanEnds[k];
                if (m > this.mGapStart)
                    m -= this.mGapLength;
                if (m <= paramInt)
                    break label220;
                m += this.mGapLength;
                label124: if (n > this.mGapStart)
                    n -= this.mGapLength;
                if (n <= paramInt)
                    break label269;
                n += this.mGapLength;
            }
            while (true)
            {
                this.mSpanStarts[k] = m;
                this.mSpanEnds[k] = n;
                k++;
                break label64;
                i = 0;
                break;
                label186: int j = paramInt - this.mGapStart;
                System.arraycopy(this.mText, paramInt + this.mGapLength - j, this.mText, this.mGapStart, j);
                break label61;
                label220: if (m != paramInt)
                    break label124;
                int i1 = (0xF0 & this.mSpanFlags[k]) >> 4;
                if ((i1 != 2) && ((i == 0) || (i1 != 3)))
                    break label124;
                m += this.mGapLength;
                break label124;
                label269: if (n == paramInt)
                {
                    int i2 = 0xF & this.mSpanFlags[k];
                    if ((i2 == 2) || ((i != 0) && (i2 == 3)))
                        n += this.mGapLength;
                }
            }
            label315: this.mGapStart = paramInt;
        }
    }

    private static String region(int paramInt1, int paramInt2)
    {
        return "(" + paramInt1 + " ... " + paramInt2 + ")";
    }

    private void removeSpan(int paramInt)
    {
        Object localObject = this.mSpans[paramInt];
        int i = this.mSpanStarts[paramInt];
        int j = this.mSpanEnds[paramInt];
        if (i > this.mGapStart)
            i -= this.mGapLength;
        if (j > this.mGapStart)
            j -= this.mGapLength;
        int k = this.mSpanCount - (paramInt + 1);
        System.arraycopy(this.mSpans, paramInt + 1, this.mSpans, paramInt, k);
        System.arraycopy(this.mSpanStarts, paramInt + 1, this.mSpanStarts, paramInt, k);
        System.arraycopy(this.mSpanEnds, paramInt + 1, this.mSpanEnds, paramInt, k);
        System.arraycopy(this.mSpanFlags, paramInt + 1, this.mSpanFlags, paramInt, k);
        this.mSpanCount = (-1 + this.mSpanCount);
        this.mSpans[this.mSpanCount] = null;
        sendSpanRemoved(localObject, i, j);
    }

    private void resizeFor(int paramInt)
    {
        int i = this.mText.length;
        int j = ArrayUtils.idealCharArraySize(paramInt + 1);
        int k = j - i;
        if (k == 0);
        while (true)
        {
            return;
            char[] arrayOfChar = new char[j];
            System.arraycopy(this.mText, 0, arrayOfChar, 0, this.mGapStart);
            int m = i - (this.mGapStart + this.mGapLength);
            System.arraycopy(this.mText, i - m, arrayOfChar, j - m, m);
            this.mText = arrayOfChar;
            this.mGapLength = (k + this.mGapLength);
            if (this.mGapLength < 1)
                new Exception("mGapLength < 1").printStackTrace();
            for (int n = 0; n < this.mSpanCount; n++)
            {
                if (this.mSpanStarts[n] > this.mGapStart)
                {
                    int[] arrayOfInt2 = this.mSpanStarts;
                    arrayOfInt2[n] = (k + arrayOfInt2[n]);
                }
                if (this.mSpanEnds[n] > this.mGapStart)
                {
                    int[] arrayOfInt1 = this.mSpanEnds;
                    arrayOfInt1[n] = (k + arrayOfInt1[n]);
                }
            }
        }
    }

    private void sendAfterTextChanged(TextWatcher[] paramArrayOfTextWatcher)
    {
        int i = paramArrayOfTextWatcher.length;
        for (int j = 0; j < i; j++)
            paramArrayOfTextWatcher[j].afterTextChanged(this);
    }

    private void sendBeforeTextChanged(TextWatcher[] paramArrayOfTextWatcher, int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramArrayOfTextWatcher.length;
        for (int j = 0; j < i; j++)
            paramArrayOfTextWatcher[j].beforeTextChanged(this, paramInt1, paramInt2, paramInt3);
    }

    private void sendSpanAdded(Object paramObject, int paramInt1, int paramInt2)
    {
        SpanWatcher[] arrayOfSpanWatcher = (SpanWatcher[])getSpans(paramInt1, paramInt2, SpanWatcher.class);
        int i = arrayOfSpanWatcher.length;
        for (int j = 0; j < i; j++)
            arrayOfSpanWatcher[j].onSpanAdded(this, paramObject, paramInt1, paramInt2);
    }

    private void sendSpanChanged(Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        SpanWatcher[] arrayOfSpanWatcher = (SpanWatcher[])getSpans(Math.min(paramInt1, paramInt3), Math.min(Math.max(paramInt2, paramInt4), length()), SpanWatcher.class);
        int i = arrayOfSpanWatcher.length;
        for (int j = 0; j < i; j++)
            arrayOfSpanWatcher[j].onSpanChanged(this, paramObject, paramInt1, paramInt2, paramInt3, paramInt4);
    }

    private void sendSpanRemoved(Object paramObject, int paramInt1, int paramInt2)
    {
        SpanWatcher[] arrayOfSpanWatcher = (SpanWatcher[])getSpans(paramInt1, paramInt2, SpanWatcher.class);
        int i = arrayOfSpanWatcher.length;
        for (int j = 0; j < i; j++)
            arrayOfSpanWatcher[j].onSpanRemoved(this, paramObject, paramInt1, paramInt2);
    }

    private void sendTextChanged(TextWatcher[] paramArrayOfTextWatcher, int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramArrayOfTextWatcher.length;
        for (int j = 0; j < i; j++)
            paramArrayOfTextWatcher[j].onTextChanged(this, paramInt1, paramInt2, paramInt3);
    }

    private void sendToSpanWatchers(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 0;
        if (i < this.mSpanCountBeforeAdd)
        {
            int n = this.mSpanStarts[i];
            int i1 = this.mSpanEnds[i];
            if (n > this.mGapStart)
                n -= this.mGapLength;
            if (i1 > this.mGapStart)
                i1 -= this.mGapLength;
            int i2 = this.mSpanFlags[i];
            int i3 = paramInt2 + paramInt3;
            int i4 = 0;
            int i5 = n;
            label107: int i6;
            if (n > i3)
            {
                if (paramInt3 != 0)
                {
                    i5 -= paramInt3;
                    i4 = 1;
                }
                i6 = i1;
                if (i1 <= i3)
                    break label230;
                if (paramInt3 != 0)
                    i6 -= paramInt3;
            }
            for (i4 = 1; ; i4 = 1)
                label230: 
                do
                {
                    if (i4 != 0)
                        sendSpanChanged(this.mSpans[i], i5, i6, n, i1);
                    int[] arrayOfInt = this.mSpanFlags;
                    arrayOfInt[i] = (0xFFFF0FFF & arrayOfInt[i]);
                    i++;
                    break;
                    if ((n < paramInt1) || ((n == paramInt1) && ((i2 & 0x1000) == 4096)) || ((n == i3) && ((i2 & 0x2000) == 8192)))
                        break label107;
                    i4 = 1;
                    break label107;
                }
                while ((i1 < paramInt1) || ((i1 == paramInt1) && ((i2 & 0x4000) == 16384)) || ((i1 == i3) && ((i2 & 0x8000) == 32768)));
        }
        for (int j = this.mSpanCountBeforeAdd; j < this.mSpanCount; j++)
        {
            int k = this.mSpanStarts[j];
            int m = this.mSpanEnds[j];
            if (k > this.mGapStart)
                k -= this.mGapLength;
            if (m > this.mGapStart)
                m -= this.mGapLength;
            sendSpanAdded(this.mSpans[j], k, m);
        }
    }

    private void setSpan(boolean paramBoolean, Object paramObject, int paramInt1, int paramInt2, int paramInt3)
    {
        checkRange("setSpan", paramInt1, paramInt2);
        int i = (paramInt3 & 0xF0) >> 4;
        if ((i == 3) && (paramInt1 != 0))
        {
            int i12 = length();
            if ((paramInt1 != i12) && (charAt(paramInt1 - 1) != '\n'))
                throw new RuntimeException("PARAGRAPH span must start at paragraph boundary");
        }
        int j = paramInt3 & 0xF;
        if ((j == 3) && (paramInt2 != 0))
        {
            int i11 = length();
            if ((paramInt2 != i11) && (charAt(paramInt2 - 1) != '\n'))
                throw new RuntimeException("PARAGRAPH span must end at paragraph boundary");
        }
        if ((i == 2) && (j == 1) && (paramInt1 == paramInt2))
            if (paramBoolean)
                Log.e("SpannableStringBuilder", "SPAN_EXCLUSIVE_EXCLUSIVE spans cannot have a zero length");
        while (true)
        {
            return;
            int k = paramInt1;
            int m = paramInt2;
            int n = this.mGapStart;
            label179: label201: int i4;
            Object[] arrayOfObject1;
            if (paramInt1 > n)
            {
                paramInt1 += this.mGapLength;
                int i2 = this.mGapStart;
                if (paramInt2 <= i2)
                    break label378;
                paramInt2 += this.mGapLength;
                i4 = this.mSpanCount;
                arrayOfObject1 = this.mSpans;
            }
            for (int i5 = 0; ; i5++)
            {
                if (i5 >= i4)
                    break label434;
                if (arrayOfObject1[i5] == paramObject)
                {
                    int i7 = this.mSpanStarts[i5];
                    int i8 = this.mSpanEnds[i5];
                    if (i7 > this.mGapStart)
                        i7 -= this.mGapLength;
                    if (i8 > this.mGapStart)
                        i8 -= this.mGapLength;
                    this.mSpanStarts[i5] = paramInt1;
                    this.mSpanEnds[i5] = paramInt2;
                    this.mSpanFlags[i5] = paramInt3;
                    if (!paramBoolean)
                        break;
                    sendSpanChanged(paramObject, i7, i8, k, m);
                    break;
                    int i1 = this.mGapStart;
                    if (paramInt1 != i1)
                        break label179;
                    if (i != 2)
                    {
                        if (i != 3)
                            break label179;
                        int i10 = length();
                        if (paramInt1 != i10)
                            break label179;
                    }
                    paramInt1 += this.mGapLength;
                    break label179;
                    label378: int i3 = this.mGapStart;
                    if (paramInt2 != i3)
                        break label201;
                    if (j != 2)
                    {
                        if (j != 3)
                            break label201;
                        int i9 = length();
                        if (paramInt2 != i9)
                            break label201;
                    }
                    paramInt2 += this.mGapLength;
                    break label201;
                }
            }
            label434: if (1 + this.mSpanCount >= this.mSpans.length)
            {
                int i6 = ArrayUtils.idealIntArraySize(1 + this.mSpanCount);
                Object[] arrayOfObject2 = new Object[i6];
                int[] arrayOfInt1 = new int[i6];
                int[] arrayOfInt2 = new int[i6];
                int[] arrayOfInt3 = new int[i6];
                System.arraycopy(this.mSpans, 0, arrayOfObject2, 0, this.mSpanCount);
                System.arraycopy(this.mSpanStarts, 0, arrayOfInt1, 0, this.mSpanCount);
                System.arraycopy(this.mSpanEnds, 0, arrayOfInt2, 0, this.mSpanCount);
                System.arraycopy(this.mSpanFlags, 0, arrayOfInt3, 0, this.mSpanCount);
                this.mSpans = arrayOfObject2;
                this.mSpanStarts = arrayOfInt1;
                this.mSpanEnds = arrayOfInt2;
                this.mSpanFlags = arrayOfInt3;
            }
            this.mSpans[this.mSpanCount] = paramObject;
            this.mSpanStarts[this.mSpanCount] = paramInt1;
            this.mSpanEnds[this.mSpanCount] = paramInt2;
            this.mSpanFlags[this.mSpanCount] = paramInt3;
            this.mSpanCount = (1 + this.mSpanCount);
            if (paramBoolean)
                sendSpanAdded(paramObject, k, m);
        }
    }

    private int updatedIntervalBound(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2)
    {
        if ((paramInt1 >= paramInt2) && (paramInt1 < this.mGapStart + this.mGapLength))
            if (paramInt4 == 2)
            {
                if ((!paramBoolean2) && (paramInt1 <= paramInt2))
                    break label93;
                paramInt2 = this.mGapStart + this.mGapLength;
            }
        while (true)
        {
            return paramInt2;
            if (paramInt4 == 3)
            {
                if (paramBoolean1)
                    paramInt2 = this.mGapStart + this.mGapLength;
            }
            else if ((!paramBoolean2) && (paramInt1 >= this.mGapStart - paramInt3))
            {
                paramInt2 = this.mGapStart;
                continue;
                label93: paramInt2 = paramInt1;
            }
        }
    }

    public static SpannableStringBuilder valueOf(CharSequence paramCharSequence)
    {
        if ((paramCharSequence instanceof SpannableStringBuilder));
        for (SpannableStringBuilder localSpannableStringBuilder = (SpannableStringBuilder)paramCharSequence; ; localSpannableStringBuilder = new SpannableStringBuilder(paramCharSequence))
            return localSpannableStringBuilder;
    }

    public SpannableStringBuilder append(char paramChar)
    {
        return append(String.valueOf(paramChar));
    }

    public SpannableStringBuilder append(CharSequence paramCharSequence)
    {
        int i = length();
        return replace(i, i, paramCharSequence, 0, paramCharSequence.length());
    }

    public SpannableStringBuilder append(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        int i = length();
        return replace(i, i, paramCharSequence, paramInt1, paramInt2);
    }

    public char charAt(int paramInt)
    {
        int i = length();
        if (paramInt < 0)
            throw new IndexOutOfBoundsException("charAt: " + paramInt + " < 0");
        if (paramInt >= i)
            throw new IndexOutOfBoundsException("charAt: " + paramInt + " >= length " + i);
        if (paramInt >= this.mGapStart);
        for (char c = this.mText[(paramInt + this.mGapLength)]; ; c = this.mText[paramInt])
            return c;
    }

    public void clear()
    {
        replace(0, length(), "", 0, 0);
    }

    public void clearSpans()
    {
        for (int i = -1 + this.mSpanCount; i >= 0; i--)
        {
            Object localObject = this.mSpans[i];
            int j = this.mSpanStarts[i];
            int k = this.mSpanEnds[i];
            if (j > this.mGapStart)
                j -= this.mGapLength;
            if (k > this.mGapStart)
                k -= this.mGapLength;
            this.mSpanCount = i;
            this.mSpans[i] = null;
            sendSpanRemoved(localObject, j, k);
        }
    }

    public SpannableStringBuilder delete(int paramInt1, int paramInt2)
    {
        SpannableStringBuilder localSpannableStringBuilder = replace(paramInt1, paramInt2, "", 0, 0);
        if (this.mGapLength > 2 * length())
            resizeFor(length());
        return localSpannableStringBuilder;
    }

    public void drawText(Canvas paramCanvas, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint)
    {
        checkRange("drawText", paramInt1, paramInt2);
        if (paramInt2 <= this.mGapStart)
            paramCanvas.drawText(this.mText, paramInt1, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint);
        while (true)
        {
            return;
            if (paramInt1 >= this.mGapStart)
            {
                paramCanvas.drawText(this.mText, paramInt1 + this.mGapLength, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint);
            }
            else
            {
                char[] arrayOfChar = TextUtils.obtain(paramInt2 - paramInt1);
                getChars(paramInt1, paramInt2, arrayOfChar, 0);
                paramCanvas.drawText(arrayOfChar, 0, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint);
                TextUtils.recycle(arrayOfChar);
            }
        }
    }

    public void drawTextRun(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint)
    {
        checkRange("drawTextRun", paramInt1, paramInt2);
        int i = paramInt4 - paramInt3;
        int j = paramInt2 - paramInt1;
        if (paramInt4 <= this.mGapStart)
            paramCanvas.drawTextRun(this.mText, paramInt1, j, paramInt3, i, paramFloat1, paramFloat2, paramInt5, paramPaint);
        while (true)
        {
            return;
            if (paramInt3 >= this.mGapStart)
            {
                paramCanvas.drawTextRun(this.mText, paramInt1 + this.mGapLength, j, paramInt3 + this.mGapLength, i, paramFloat1, paramFloat2, paramInt5, paramPaint);
            }
            else
            {
                char[] arrayOfChar = TextUtils.obtain(i);
                getChars(paramInt3, paramInt4, arrayOfChar, 0);
                paramCanvas.drawTextRun(arrayOfChar, paramInt1 - paramInt3, j, 0, i, paramFloat1, paramFloat2, paramInt5, paramPaint);
                TextUtils.recycle(arrayOfChar);
            }
        }
    }

    public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
    {
        checkRange("getChars", paramInt1, paramInt2);
        if (paramInt2 <= this.mGapStart)
            System.arraycopy(this.mText, paramInt1, paramArrayOfChar, paramInt3, paramInt2 - paramInt1);
        while (true)
        {
            return;
            if (paramInt1 >= this.mGapStart)
            {
                System.arraycopy(this.mText, paramInt1 + this.mGapLength, paramArrayOfChar, paramInt3, paramInt2 - paramInt1);
            }
            else
            {
                System.arraycopy(this.mText, paramInt1, paramArrayOfChar, paramInt3, this.mGapStart - paramInt1);
                System.arraycopy(this.mText, this.mGapStart + this.mGapLength, paramArrayOfChar, paramInt3 + (this.mGapStart - paramInt1), paramInt2 - this.mGapStart);
            }
        }
    }

    public InputFilter[] getFilters()
    {
        return this.mFilters;
    }

    public int getSpanEnd(Object paramObject)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int j = i - 1;
        int k;
        if (j >= 0)
            if (arrayOfObject[j] == paramObject)
            {
                k = this.mSpanEnds[j];
                if (k > this.mGapStart)
                    k -= this.mGapLength;
            }
        while (true)
        {
            return k;
            j--;
            break;
            k = -1;
        }
    }

    public int getSpanFlags(Object paramObject)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int j = i - 1;
        if (j >= 0)
            if (arrayOfObject[j] != paramObject);
        for (int k = this.mSpanFlags[j]; ; k = 0)
        {
            return k;
            j--;
            break;
        }
    }

    public int getSpanStart(Object paramObject)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int j = i - 1;
        int k;
        if (j >= 0)
            if (arrayOfObject[j] == paramObject)
            {
                k = this.mSpanStarts[j];
                if (k > this.mGapStart)
                    k -= this.mGapLength;
            }
        while (true)
        {
            return k;
            j--;
            break;
            k = -1;
        }
    }

    public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass)
    {
        Object localObject1;
        if (paramClass == null)
            localObject1 = ArrayUtils.emptyArray(paramClass);
        while (true)
        {
            return localObject1;
            int i = this.mSpanCount;
            Object[] arrayOfObject1 = this.mSpans;
            int[] arrayOfInt1 = this.mSpanStarts;
            int[] arrayOfInt2 = this.mSpanEnds;
            int[] arrayOfInt3 = this.mSpanFlags;
            int j = this.mGapStart;
            int k = this.mGapLength;
            localObject1 = null;
            Object localObject2 = null;
            int m = 0;
            int n = 0;
            if (m < i)
            {
                int i1 = arrayOfInt1[m];
                if (i1 > j)
                    i1 -= k;
                int i4;
                if (i1 > paramInt2)
                    i4 = n;
                while (true)
                {
                    m++;
                    n = i4;
                    break;
                    int i2 = arrayOfInt2[m];
                    if (i2 > j)
                        i2 -= k;
                    if (i2 < paramInt1)
                    {
                        i4 = n;
                    }
                    else if ((i1 != i2) && (paramInt1 != paramInt2))
                    {
                        if (i1 == paramInt2)
                            i4 = n;
                        else if (i2 == paramInt1)
                            i4 = n;
                    }
                    else if (!paramClass.isInstance(arrayOfObject1[m]))
                    {
                        i4 = n;
                    }
                    else if (n == 0)
                    {
                        localObject2 = arrayOfObject1[m];
                        i4 = n + 1;
                    }
                    else
                    {
                        if (n == 1)
                        {
                            localObject1 = (Object[])Array.newInstance(paramClass, 1 + (i - m));
                            localObject1[0] = localObject2;
                        }
                        int i3 = 0xFF0000 & arrayOfInt3[m];
                        if (i3 != 0)
                            for (int i5 = 0; ; i5++)
                                if ((i5 >= n) || (i3 > (0xFF0000 & getSpanFlags(localObject1[i5]))))
                                {
                                    System.arraycopy(localObject1, i5, localObject1, i5 + 1, n - i5);
                                    localObject1[i5] = arrayOfObject1[m];
                                    i4 = n + 1;
                                    break;
                                }
                        i4 = n + 1;
                        localObject1[n] = arrayOfObject1[m];
                    }
                }
            }
            if (n == 0)
            {
                localObject1 = ArrayUtils.emptyArray(paramClass);
            }
            else if (n == 1)
            {
                localObject1 = (Object[])Array.newInstance(paramClass, 1);
                localObject1[0] = localObject2;
            }
            else if (n != localObject1.length)
            {
                Object[] arrayOfObject2 = (Object[])Array.newInstance(paramClass, n);
                System.arraycopy(localObject1, 0, arrayOfObject2, 0, n);
                localObject1 = arrayOfObject2;
            }
        }
    }

    public float getTextRunAdvances(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, Paint paramPaint)
    {
        int i = paramInt4 - paramInt3;
        int j = paramInt2 - paramInt1;
        float f;
        if (paramInt2 <= this.mGapStart)
            f = paramPaint.getTextRunAdvances(this.mText, paramInt1, j, paramInt3, i, paramInt5, paramArrayOfFloat, paramInt6);
        while (true)
        {
            return f;
            if (paramInt1 >= this.mGapStart)
            {
                f = paramPaint.getTextRunAdvances(this.mText, paramInt1 + this.mGapLength, j, paramInt3 + this.mGapLength, i, paramInt5, paramArrayOfFloat, paramInt6);
            }
            else
            {
                char[] arrayOfChar = TextUtils.obtain(i);
                getChars(paramInt3, paramInt4, arrayOfChar, 0);
                f = paramPaint.getTextRunAdvances(arrayOfChar, paramInt1 - paramInt3, j, 0, i, paramInt5, paramArrayOfFloat, paramInt6);
                TextUtils.recycle(arrayOfChar);
            }
        }
    }

    public float getTextRunAdvances(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, Paint paramPaint, int paramInt7)
    {
        int i = paramInt4 - paramInt3;
        int j = paramInt2 - paramInt1;
        float f;
        if (paramInt2 <= this.mGapStart)
            f = paramPaint.getTextRunAdvances(this.mText, paramInt1, j, paramInt3, i, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
        while (true)
        {
            return f;
            if (paramInt1 >= this.mGapStart)
            {
                f = paramPaint.getTextRunAdvances(this.mText, paramInt1 + this.mGapLength, j, paramInt3 + this.mGapLength, i, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
            }
            else
            {
                char[] arrayOfChar = TextUtils.obtain(i);
                getChars(paramInt3, paramInt4, arrayOfChar, 0);
                f = paramPaint.getTextRunAdvances(arrayOfChar, paramInt1 - paramInt3, j, 0, i, paramInt5, paramArrayOfFloat, paramInt6, paramInt7);
                TextUtils.recycle(arrayOfChar);
            }
        }
    }

    @Deprecated
    public int getTextRunCursor(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint)
    {
        int i = paramInt2 - paramInt1;
        int j;
        if (paramInt2 <= this.mGapStart)
            j = paramPaint.getTextRunCursor(this.mText, paramInt1, i, paramInt3, paramInt4, paramInt5);
        while (true)
        {
            return j;
            if (paramInt1 >= this.mGapStart)
            {
                j = paramPaint.getTextRunCursor(this.mText, paramInt1 + this.mGapLength, i, paramInt3, paramInt4 + this.mGapLength, paramInt5) - this.mGapLength;
            }
            else
            {
                char[] arrayOfChar = TextUtils.obtain(i);
                getChars(paramInt1, paramInt2, arrayOfChar, 0);
                j = paramInt1 + paramPaint.getTextRunCursor(arrayOfChar, 0, i, paramInt3, paramInt4 - paramInt1, paramInt5);
                TextUtils.recycle(arrayOfChar);
            }
        }
    }

    public int getTextWidths(int paramInt1, int paramInt2, float[] paramArrayOfFloat, Paint paramPaint)
    {
        checkRange("getTextWidths", paramInt1, paramInt2);
        int i;
        if (paramInt2 <= this.mGapStart)
            i = paramPaint.getTextWidths(this.mText, paramInt1, paramInt2 - paramInt1, paramArrayOfFloat);
        while (true)
        {
            return i;
            if (paramInt1 >= this.mGapStart)
            {
                i = paramPaint.getTextWidths(this.mText, paramInt1 + this.mGapLength, paramInt2 - paramInt1, paramArrayOfFloat);
            }
            else
            {
                char[] arrayOfChar = TextUtils.obtain(paramInt2 - paramInt1);
                getChars(paramInt1, paramInt2, arrayOfChar, 0);
                i = paramPaint.getTextWidths(arrayOfChar, 0, paramInt2 - paramInt1, paramArrayOfFloat);
                TextUtils.recycle(arrayOfChar);
            }
        }
    }

    public SpannableStringBuilder insert(int paramInt, CharSequence paramCharSequence)
    {
        return replace(paramInt, paramInt, paramCharSequence, 0, paramCharSequence.length());
    }

    public SpannableStringBuilder insert(int paramInt1, CharSequence paramCharSequence, int paramInt2, int paramInt3)
    {
        return replace(paramInt1, paramInt1, paramCharSequence, paramInt2, paramInt3);
    }

    public int length()
    {
        return this.mText.length - this.mGapLength;
    }

    public float measureText(int paramInt1, int paramInt2, Paint paramPaint)
    {
        checkRange("measureText", paramInt1, paramInt2);
        float f;
        if (paramInt2 <= this.mGapStart)
            f = paramPaint.measureText(this.mText, paramInt1, paramInt2 - paramInt1);
        while (true)
        {
            return f;
            if (paramInt1 >= this.mGapStart)
            {
                f = paramPaint.measureText(this.mText, paramInt1 + this.mGapLength, paramInt2 - paramInt1);
            }
            else
            {
                char[] arrayOfChar = TextUtils.obtain(paramInt2 - paramInt1);
                getChars(paramInt1, paramInt2, arrayOfChar, 0);
                f = paramPaint.measureText(arrayOfChar, 0, paramInt2 - paramInt1);
                TextUtils.recycle(arrayOfChar);
            }
        }
    }

    public int nextSpanTransition(int paramInt1, int paramInt2, Class paramClass)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int[] arrayOfInt1 = this.mSpanStarts;
        int[] arrayOfInt2 = this.mSpanEnds;
        int j = this.mGapStart;
        int k = this.mGapLength;
        if (paramClass == null)
            paramClass = Object.class;
        for (int m = 0; m < i; m++)
        {
            int n = arrayOfInt1[m];
            int i1 = arrayOfInt2[m];
            if (n > j)
                n -= k;
            if (i1 > j)
                i1 -= k;
            if ((n > paramInt1) && (n < paramInt2) && (paramClass.isInstance(arrayOfObject[m])))
                paramInt2 = n;
            if ((i1 > paramInt1) && (i1 < paramInt2) && (paramClass.isInstance(arrayOfObject[m])))
                paramInt2 = i1;
        }
        return paramInt2;
    }

    public void removeSpan(Object paramObject)
    {
        for (int i = -1 + this.mSpanCount; ; i--)
            if (i >= 0)
            {
                if (this.mSpans[i] == paramObject)
                    removeSpan(i);
            }
            else
                return;
    }

    public SpannableStringBuilder replace(int paramInt1, int paramInt2, CharSequence paramCharSequence)
    {
        return replace(paramInt1, paramInt2, paramCharSequence, 0, paramCharSequence.length());
    }

    public SpannableStringBuilder replace(int paramInt1, int paramInt2, CharSequence paramCharSequence, int paramInt3, int paramInt4)
    {
        checkRange("replace", paramInt1, paramInt2);
        int i = this.mFilters.length;
        for (int j = 0; j < i; j++)
        {
            CharSequence localCharSequence = this.mFilters[j].filter(paramCharSequence, paramInt3, paramInt4, this, paramInt1, paramInt2);
            if (localCharSequence != null)
            {
                paramCharSequence = localCharSequence;
                paramInt3 = 0;
                paramInt4 = localCharSequence.length();
            }
        }
        int k = paramInt2 - paramInt1;
        int m = paramInt4 - paramInt3;
        if ((k == 0) && (m == 0) && (!hasNonExclusiveExclusiveSpanAt(paramCharSequence, paramInt3)))
            return this;
        TextWatcher[] arrayOfTextWatcher = (TextWatcher[])getSpans(paramInt1, paramInt1 + k, TextWatcher.class);
        sendBeforeTextChanged(arrayOfTextWatcher, paramInt1, k, m);
        if ((k != 0) && (m != 0));
        for (int n = 1; ; n = 0)
        {
            int i1 = 0;
            int i2 = 0;
            if (n != 0)
            {
                i1 = Selection.getSelectionStart(this);
                i2 = Selection.getSelectionEnd(this);
            }
            change(paramInt1, paramInt2, paramCharSequence, paramInt3, paramInt4);
            if (n != 0)
            {
                if ((i1 > paramInt1) && (i1 < paramInt2))
                {
                    int i4 = paramInt1 + m * (i1 - paramInt1) / k;
                    setSpan(false, Selection.SELECTION_START, i4, i4, 34);
                }
                if ((i2 > paramInt1) && (i2 < paramInt2))
                {
                    int i3 = paramInt1 + m * (i2 - paramInt1) / k;
                    setSpan(false, Selection.SELECTION_END, i3, i3, 34);
                }
            }
            sendTextChanged(arrayOfTextWatcher, paramInt1, k, m);
            sendAfterTextChanged(arrayOfTextWatcher);
            sendToSpanWatchers(paramInt1, paramInt2, m - k);
            break;
        }
    }

    public void setFilters(InputFilter[] paramArrayOfInputFilter)
    {
        if (paramArrayOfInputFilter == null)
            throw new IllegalArgumentException();
        this.mFilters = paramArrayOfInputFilter;
    }

    public void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3)
    {
        setSpan(true, paramObject, paramInt1, paramInt2, paramInt3);
    }

    public CharSequence subSequence(int paramInt1, int paramInt2)
    {
        return new SpannableStringBuilder(this, paramInt1, paramInt2);
    }

    public String substring(int paramInt1, int paramInt2)
    {
        char[] arrayOfChar = new char[paramInt2 - paramInt1];
        getChars(paramInt1, paramInt2, arrayOfChar, 0);
        return new String(arrayOfChar);
    }

    public String toString()
    {
        int i = length();
        char[] arrayOfChar = new char[i];
        getChars(0, i, arrayOfChar, 0);
        return new String(arrayOfChar);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.SpannableStringBuilder
 * JD-Core Version:        0.6.2
 */