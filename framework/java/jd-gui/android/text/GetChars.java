package android.text;

public abstract interface GetChars extends CharSequence
{
    public abstract void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.GetChars
 * JD-Core Version:        0.6.2
 */