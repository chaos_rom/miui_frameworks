package android.text;

public abstract interface TextDirectionHeuristic
{
    public abstract boolean isRtl(char[] paramArrayOfChar, int paramInt1, int paramInt2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.TextDirectionHeuristic
 * JD-Core Version:        0.6.2
 */