package android.text;

public abstract interface Spannable extends Spanned
{
    public abstract void removeSpan(Object paramObject);

    public abstract void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3);

    public static class Factory
    {
        private static Factory sInstance = new Factory();

        public static Factory getInstance()
        {
            return sInstance;
        }

        public Spannable newSpannable(CharSequence paramCharSequence)
        {
            return new SpannableString(paramCharSequence);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.Spannable
 * JD-Core Version:        0.6.2
 */