package android.text;

public abstract interface TextWatcher extends NoCopySpan
{
    public abstract void afterTextChanged(Editable paramEditable);

    public abstract void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3);

    public abstract void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.TextWatcher
 * JD-Core Version:        0.6.2
 */