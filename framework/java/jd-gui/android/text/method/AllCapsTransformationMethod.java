package android.text.method;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import java.util.Locale;

public class AllCapsTransformationMethod
    implements TransformationMethod2
{
    private static final String TAG = "AllCapsTransformationMethod";
    private boolean mEnabled;
    private Locale mLocale;

    public AllCapsTransformationMethod(Context paramContext)
    {
        this.mLocale = paramContext.getResources().getConfiguration().locale;
    }

    public CharSequence getTransformation(CharSequence paramCharSequence, View paramView)
    {
        Object localObject;
        if (this.mEnabled)
            if (paramCharSequence != null)
                localObject = paramCharSequence.toString().toUpperCase(this.mLocale);
        while (true)
        {
            return localObject;
            localObject = null;
            continue;
            Log.w("AllCapsTransformationMethod", "Caller did not enable length changes; not transforming text");
            localObject = paramCharSequence;
        }
    }

    public void onFocusChanged(View paramView, CharSequence paramCharSequence, boolean paramBoolean, int paramInt, Rect paramRect)
    {
    }

    public void setLengthChangesAllowed(boolean paramBoolean)
    {
        this.mEnabled = paramBoolean;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.AllCapsTransformationMethod
 * JD-Core Version:        0.6.2
 */