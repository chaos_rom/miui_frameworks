package android.text.method;

import android.text.Layout;
import android.text.Spannable;
import android.text.TextPaint;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

public class BaseMovementMethod
    implements MovementMethod
{
    private int getBottomLine(TextView paramTextView)
    {
        return paramTextView.getLayout().getLineForVertical(paramTextView.getScrollY() + getInnerHeight(paramTextView));
    }

    private int getCharacterWidth(TextView paramTextView)
    {
        return (int)Math.ceil(paramTextView.getPaint().getFontSpacing());
    }

    private int getInnerHeight(TextView paramTextView)
    {
        return paramTextView.getHeight() - paramTextView.getTotalPaddingTop() - paramTextView.getTotalPaddingBottom();
    }

    private int getInnerWidth(TextView paramTextView)
    {
        return paramTextView.getWidth() - paramTextView.getTotalPaddingLeft() - paramTextView.getTotalPaddingRight();
    }

    private int getScrollBoundsLeft(TextView paramTextView)
    {
        Layout localLayout = paramTextView.getLayout();
        int i = getTopLine(paramTextView);
        int j = getBottomLine(paramTextView);
        int k;
        if (i > j)
            k = 0;
        while (true)
        {
            return k;
            k = 2147483647;
            for (int m = i; m <= j; m++)
            {
                int n = (int)Math.floor(localLayout.getLineLeft(m));
                if (n < k)
                    k = n;
            }
        }
    }

    private int getScrollBoundsRight(TextView paramTextView)
    {
        Layout localLayout = paramTextView.getLayout();
        int i = getTopLine(paramTextView);
        int j = getBottomLine(paramTextView);
        int k;
        if (i > j)
            k = 0;
        while (true)
        {
            return k;
            k = -2147483648;
            for (int m = i; m <= j; m++)
            {
                int n = (int)Math.ceil(localLayout.getLineRight(m));
                if (n > k)
                    k = n;
            }
        }
    }

    private int getTopLine(TextView paramTextView)
    {
        return paramTextView.getLayout().getLineForVertical(paramTextView.getScrollY());
    }

    protected boolean bottom(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    public boolean canSelectArbitrarily()
    {
        return false;
    }

    protected boolean down(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean end(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected int getMovementMetaState(Spannable paramSpannable, KeyEvent paramKeyEvent)
    {
        return 0xFFFFFF3E & KeyEvent.normalizeMetaState(0xFFFFF9FF & (paramKeyEvent.getMetaState() | MetaKeyKeyListener.getMetaState(paramSpannable)));
    }

    protected boolean handleMovementKey(TextView paramTextView, Spannable paramSpannable, int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        boolean bool;
        switch (paramInt1)
        {
        default:
            bool = false;
        case 21:
        case 22:
        case 19:
        case 20:
        case 92:
        case 93:
        case 122:
        case 123:
        }
        while (true)
        {
            return bool;
            if (KeyEvent.metaStateHasNoModifiers(paramInt2))
            {
                bool = left(paramTextView, paramSpannable);
            }
            else if (KeyEvent.metaStateHasModifiers(paramInt2, 4096))
            {
                bool = leftWord(paramTextView, paramSpannable);
            }
            else
            {
                if (!KeyEvent.metaStateHasModifiers(paramInt2, 2))
                    break;
                bool = lineStart(paramTextView, paramSpannable);
                continue;
                if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                {
                    bool = right(paramTextView, paramSpannable);
                }
                else if (KeyEvent.metaStateHasModifiers(paramInt2, 4096))
                {
                    bool = rightWord(paramTextView, paramSpannable);
                }
                else
                {
                    if (!KeyEvent.metaStateHasModifiers(paramInt2, 2))
                        break;
                    bool = lineEnd(paramTextView, paramSpannable);
                    continue;
                    if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                    {
                        bool = up(paramTextView, paramSpannable);
                    }
                    else
                    {
                        if (!KeyEvent.metaStateHasModifiers(paramInt2, 2))
                            break;
                        bool = top(paramTextView, paramSpannable);
                        continue;
                        if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                        {
                            bool = down(paramTextView, paramSpannable);
                        }
                        else
                        {
                            if (!KeyEvent.metaStateHasModifiers(paramInt2, 2))
                                break;
                            bool = bottom(paramTextView, paramSpannable);
                            continue;
                            if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                            {
                                bool = pageUp(paramTextView, paramSpannable);
                            }
                            else
                            {
                                if (!KeyEvent.metaStateHasModifiers(paramInt2, 2))
                                    break;
                                bool = top(paramTextView, paramSpannable);
                                continue;
                                if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                                {
                                    bool = pageDown(paramTextView, paramSpannable);
                                }
                                else
                                {
                                    if (!KeyEvent.metaStateHasModifiers(paramInt2, 2))
                                        break;
                                    bool = bottom(paramTextView, paramSpannable);
                                    continue;
                                    if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                                    {
                                        bool = home(paramTextView, paramSpannable);
                                    }
                                    else
                                    {
                                        if (!KeyEvent.metaStateHasModifiers(paramInt2, 4096))
                                            break;
                                        bool = top(paramTextView, paramSpannable);
                                        continue;
                                        if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                                        {
                                            bool = end(paramTextView, paramSpannable);
                                        }
                                        else
                                        {
                                            if (!KeyEvent.metaStateHasModifiers(paramInt2, 4096))
                                                break;
                                            bool = bottom(paramTextView, paramSpannable);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected boolean home(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    public void initialize(TextView paramTextView, Spannable paramSpannable)
    {
    }

    protected boolean left(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean leftWord(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean lineEnd(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean lineStart(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    public boolean onGenericMotionEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent)
    {
        if ((0x2 & paramMotionEvent.getSource()) != 0);
        boolean bool;
        switch (paramMotionEvent.getAction())
        {
        default:
            bool = false;
        case 8:
        }
        while (true)
        {
            return bool;
            float f1;
            float f2;
            if ((0x1 & paramMotionEvent.getMetaState()) != 0)
            {
                f1 = 0.0F;
                f2 = paramMotionEvent.getAxisValue(9);
                label58: bool = false;
                if (f2 >= 0.0F)
                    break label135;
                bool = false | scrollLeft(paramTextView, paramSpannable, (int)Math.ceil(-f2));
            }
            while (true)
            {
                if (f1 >= 0.0F)
                    break label162;
                bool |= scrollUp(paramTextView, paramSpannable, (int)Math.ceil(-f1));
                break;
                f1 = -paramMotionEvent.getAxisValue(9);
                f2 = paramMotionEvent.getAxisValue(10);
                break label58;
                label135: if (f2 > 0.0F)
                    bool = false | scrollRight(paramTextView, paramSpannable, (int)Math.ceil(f2));
            }
            label162: if (f1 > 0.0F)
                bool |= scrollDown(paramTextView, paramSpannable, (int)Math.ceil(f1));
        }
    }

    public boolean onKeyDown(TextView paramTextView, Spannable paramSpannable, int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = handleMovementKey(paramTextView, paramSpannable, paramInt, getMovementMetaState(paramSpannable, paramKeyEvent), paramKeyEvent);
        if (bool)
        {
            MetaKeyKeyListener.adjustMetaAfterKeypress(paramSpannable);
            MetaKeyKeyListener.resetLockedMeta(paramSpannable);
        }
        return bool;
    }

    public boolean onKeyOther(TextView paramTextView, Spannable paramSpannable, KeyEvent paramKeyEvent)
    {
        int i = getMovementMetaState(paramSpannable, paramKeyEvent);
        int j = paramKeyEvent.getKeyCode();
        boolean bool;
        int m;
        if ((j != 0) && (paramKeyEvent.getAction() == 2))
        {
            int k = paramKeyEvent.getRepeatCount();
            bool = false;
            m = 0;
            if ((m >= k) || (!handleMovementKey(paramTextView, paramSpannable, j, i, paramKeyEvent)))
                if (bool)
                {
                    MetaKeyKeyListener.adjustMetaAfterKeypress(paramSpannable);
                    MetaKeyKeyListener.resetLockedMeta(paramSpannable);
                }
        }
        while (true)
        {
            return bool;
            bool = true;
            m++;
            break;
            bool = false;
        }
    }

    public boolean onKeyUp(TextView paramTextView, Spannable paramSpannable, int paramInt, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public void onTakeFocus(TextView paramTextView, Spannable paramSpannable, int paramInt)
    {
    }

    public boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent)
    {
        return false;
    }

    public boolean onTrackballEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent)
    {
        return false;
    }

    protected boolean pageDown(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean pageUp(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean right(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean rightWord(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean scrollBottom(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        int i = localLayout.getLineCount();
        if (getBottomLine(paramTextView) <= i - 1)
            Touch.scrollTo(paramTextView, localLayout, paramTextView.getScrollX(), localLayout.getLineTop(i) - getInnerHeight(paramTextView));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean scrollDown(TextView paramTextView, Spannable paramSpannable, int paramInt)
    {
        Layout localLayout = paramTextView.getLayout();
        int i = getInnerHeight(paramTextView);
        int j = i + paramTextView.getScrollY();
        int k = localLayout.getLineForVertical(j);
        if (localLayout.getLineTop(k + 1) < j + 1)
            k++;
        int m = -1 + localLayout.getLineCount();
        if (k <= m)
        {
            int n = Math.min(-1 + (k + paramInt), m);
            Touch.scrollTo(paramTextView, localLayout, paramTextView.getScrollX(), localLayout.getLineTop(n + 1) - i);
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean scrollLeft(TextView paramTextView, Spannable paramSpannable, int paramInt)
    {
        int i = getScrollBoundsLeft(paramTextView);
        int j = paramTextView.getScrollX();
        if (j > i)
            paramTextView.scrollTo(Math.max(j - paramInt * getCharacterWidth(paramTextView), i), paramTextView.getScrollY());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean scrollLineEnd(TextView paramTextView, Spannable paramSpannable)
    {
        int i = getScrollBoundsRight(paramTextView) - getInnerWidth(paramTextView);
        if (paramTextView.getScrollX() < i)
            paramTextView.scrollTo(i, paramTextView.getScrollY());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean scrollLineStart(TextView paramTextView, Spannable paramSpannable)
    {
        int i = getScrollBoundsLeft(paramTextView);
        if (paramTextView.getScrollX() > i)
            paramTextView.scrollTo(i, paramTextView.getScrollY());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean scrollPageDown(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        int i = getInnerHeight(paramTextView);
        int j = localLayout.getLineForVertical(i + (i + paramTextView.getScrollY()));
        if (j <= -1 + localLayout.getLineCount())
            Touch.scrollTo(paramTextView, localLayout, paramTextView.getScrollX(), localLayout.getLineTop(j + 1) - i);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean scrollPageUp(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        int i = localLayout.getLineForVertical(paramTextView.getScrollY() - getInnerHeight(paramTextView));
        if (i >= 0)
            Touch.scrollTo(paramTextView, localLayout, paramTextView.getScrollX(), localLayout.getLineTop(i));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean scrollRight(TextView paramTextView, Spannable paramSpannable, int paramInt)
    {
        int i = getScrollBoundsRight(paramTextView) - getInnerWidth(paramTextView);
        int j = paramTextView.getScrollX();
        if (j < i)
            paramTextView.scrollTo(Math.min(j + paramInt * getCharacterWidth(paramTextView), i), paramTextView.getScrollY());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean scrollTop(TextView paramTextView, Spannable paramSpannable)
    {
        boolean bool = false;
        Layout localLayout = paramTextView.getLayout();
        if (getTopLine(paramTextView) >= 0)
        {
            Touch.scrollTo(paramTextView, localLayout, paramTextView.getScrollX(), localLayout.getLineTop(0));
            bool = true;
        }
        return bool;
    }

    protected boolean scrollUp(TextView paramTextView, Spannable paramSpannable, int paramInt)
    {
        boolean bool = false;
        Layout localLayout = paramTextView.getLayout();
        int i = paramTextView.getScrollY();
        int j = localLayout.getLineForVertical(i);
        if (localLayout.getLineTop(j) == i)
            j--;
        if (j >= 0)
        {
            int k = Math.max(1 + (j - paramInt), 0);
            Touch.scrollTo(paramTextView, localLayout, paramTextView.getScrollX(), localLayout.getLineTop(k));
            bool = true;
        }
        return bool;
    }

    protected boolean top(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }

    protected boolean up(TextView paramTextView, Spannable paramSpannable)
    {
        return false;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.BaseMovementMethod
 * JD-Core Version:        0.6.2
 */