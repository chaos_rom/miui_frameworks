package android.text.method;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings.System;
import android.text.Editable;
import android.text.NoCopySpan.Concrete;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import java.lang.ref.WeakReference;

public class TextKeyListener extends BaseKeyListener
    implements SpanWatcher
{
    static final Object ACTIVE = new NoCopySpan.Concrete();
    static final int AUTO_CAP = 1;
    static final int AUTO_PERIOD = 4;
    static final int AUTO_TEXT = 2;
    static final Object CAPPED = new NoCopySpan.Concrete();
    static final Object INHIBIT_REPLACEMENT = new NoCopySpan.Concrete();
    static final Object LAST_TYPED = new NoCopySpan.Concrete();
    static final int SHOW_PASSWORD = 8;
    private static TextKeyListener[] sInstance = new TextKeyListener[2 * Capitalize.values().length];
    private Capitalize mAutoCap;
    private boolean mAutoText;
    private SettingsObserver mObserver;
    private int mPrefs;
    private boolean mPrefsInited;
    private WeakReference<ContentResolver> mResolver;

    public TextKeyListener(Capitalize paramCapitalize, boolean paramBoolean)
    {
        this.mAutoCap = paramCapitalize;
        this.mAutoText = paramBoolean;
    }

    public static void clear(Editable paramEditable)
    {
        paramEditable.clear();
        paramEditable.removeSpan(ACTIVE);
        paramEditable.removeSpan(CAPPED);
        paramEditable.removeSpan(INHIBIT_REPLACEMENT);
        paramEditable.removeSpan(LAST_TYPED);
        QwertyKeyListener.Replaced[] arrayOfReplaced = (QwertyKeyListener.Replaced[])paramEditable.getSpans(0, paramEditable.length(), QwertyKeyListener.Replaced.class);
        int i = arrayOfReplaced.length;
        for (int j = 0; j < i; j++)
            paramEditable.removeSpan(arrayOfReplaced[j]);
    }

    public static TextKeyListener getInstance()
    {
        return getInstance(false, Capitalize.NONE);
    }

    public static TextKeyListener getInstance(boolean paramBoolean, Capitalize paramCapitalize)
    {
        int i = 2 * paramCapitalize.ordinal();
        if (paramBoolean);
        for (int j = 1; ; j = 0)
        {
            int k = i + j;
            if (sInstance[k] == null)
                sInstance[k] = new TextKeyListener(paramCapitalize, paramBoolean);
            return sInstance[k];
        }
    }

    private KeyListener getKeyListener(KeyEvent paramKeyEvent)
    {
        int i = paramKeyEvent.getKeyCharacterMap().getKeyboardType();
        Object localObject;
        if (i == 3)
            localObject = QwertyKeyListener.getInstance(this.mAutoText, this.mAutoCap);
        while (true)
        {
            return localObject;
            if (i == 1)
                localObject = MultiTapKeyListener.getInstance(this.mAutoText, this.mAutoCap);
            else if ((i == 4) || (i == 5))
                localObject = QwertyKeyListener.getInstanceForFullKeyboard();
            else
                localObject = NullKeyListener.getInstance();
        }
    }

    private void initPrefs(Context paramContext)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        this.mResolver = new WeakReference(localContentResolver);
        if (this.mObserver == null)
        {
            this.mObserver = new SettingsObserver();
            localContentResolver.registerContentObserver(Settings.System.CONTENT_URI, true, this.mObserver);
        }
        updatePrefs(localContentResolver);
        this.mPrefsInited = true;
    }

    public static boolean shouldCap(Capitalize paramCapitalize, CharSequence paramCharSequence, int paramInt)
    {
        boolean bool1 = false;
        if (paramCapitalize == Capitalize.NONE);
        while (true)
        {
            return bool1;
            if (paramCapitalize != Capitalize.CHARACTERS)
                break;
            bool1 = true;
        }
        int i;
        if (paramCapitalize == Capitalize.WORDS)
        {
            i = 8192;
            label35: if (TextUtils.getCapsMode(paramCharSequence, paramInt, i) == 0)
                break label62;
        }
        label62: for (boolean bool2 = true; ; bool2 = false)
        {
            bool1 = bool2;
            break;
            i = 16384;
            break label35;
        }
    }

    private void updatePrefs(ContentResolver paramContentResolver)
    {
        int i = 0;
        int j;
        int k;
        label27: int m;
        label40: int n;
        label53: int i1;
        label60: int i2;
        label68: int i3;
        if (Settings.System.getInt(paramContentResolver, "auto_caps", 1) > 0)
        {
            j = 1;
            if (Settings.System.getInt(paramContentResolver, "auto_replace", 1) <= 0)
                break label112;
            k = 1;
            if (Settings.System.getInt(paramContentResolver, "auto_punctuate", 1) <= 0)
                break label118;
            m = 1;
            if (Settings.System.getInt(paramContentResolver, "show_password", 1) <= 0)
                break label124;
            n = 1;
            if (j == 0)
                break label130;
            i1 = 1;
            if (k == 0)
                break label136;
            i2 = 2;
            i3 = i1 | i2;
            if (m == 0)
                break label142;
        }
        label130: label136: label142: for (int i4 = 4; ; i4 = 0)
        {
            int i5 = i4 | i3;
            if (n != 0)
                i = 8;
            this.mPrefs = (i5 | i);
            return;
            j = 0;
            break;
            label112: k = 0;
            break label27;
            label118: m = 0;
            break label40;
            label124: n = 0;
            break label53;
            i1 = 0;
            break label60;
            i2 = 0;
            break label68;
        }
    }

    public int getInputType()
    {
        return makeTextContentType(this.mAutoCap, this.mAutoText);
    }

    int getPrefs(Context paramContext)
    {
        try
        {
            if ((!this.mPrefsInited) || (this.mResolver.get() == null))
                initPrefs(paramContext);
            return this.mPrefs;
        }
        finally
        {
        }
    }

    public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
    {
        return getKeyListener(paramKeyEvent).onKeyDown(paramView, paramEditable, paramInt, paramKeyEvent);
    }

    public boolean onKeyOther(View paramView, Editable paramEditable, KeyEvent paramKeyEvent)
    {
        return getKeyListener(paramKeyEvent).onKeyOther(paramView, paramEditable, paramKeyEvent);
    }

    public boolean onKeyUp(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
    {
        return getKeyListener(paramKeyEvent).onKeyUp(paramView, paramEditable, paramInt, paramKeyEvent);
    }

    public void onSpanAdded(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
    {
    }

    public void onSpanChanged(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (paramObject == Selection.SELECTION_END)
            paramSpannable.removeSpan(ACTIVE);
    }

    public void onSpanRemoved(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
    {
    }

    public void release()
    {
        if (this.mResolver != null)
        {
            ContentResolver localContentResolver = (ContentResolver)this.mResolver.get();
            if (localContentResolver != null)
            {
                localContentResolver.unregisterContentObserver(this.mObserver);
                this.mResolver.clear();
            }
            this.mObserver = null;
            this.mResolver = null;
            this.mPrefsInited = false;
        }
    }

    private class SettingsObserver extends ContentObserver
    {
        public SettingsObserver()
        {
            super();
        }

        public void onChange(boolean paramBoolean)
        {
            ContentResolver localContentResolver;
            if (TextKeyListener.this.mResolver != null)
            {
                localContentResolver = (ContentResolver)TextKeyListener.this.mResolver.get();
                if (localContentResolver == null)
                    TextKeyListener.access$102(TextKeyListener.this, false);
            }
            while (true)
            {
                return;
                TextKeyListener.this.updatePrefs(localContentResolver);
                continue;
                TextKeyListener.access$102(TextKeyListener.this, false);
            }
        }
    }

    private static class NullKeyListener
        implements KeyListener
    {
        private static NullKeyListener sInstance;

        public static NullKeyListener getInstance()
        {
            if (sInstance != null);
            for (NullKeyListener localNullKeyListener = sInstance; ; localNullKeyListener = sInstance)
            {
                return localNullKeyListener;
                sInstance = new NullKeyListener();
            }
        }

        public void clearMetaKeyState(View paramView, Editable paramEditable, int paramInt)
        {
        }

        public int getInputType()
        {
            return 0;
        }

        public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
        {
            return false;
        }

        public boolean onKeyOther(View paramView, Editable paramEditable, KeyEvent paramKeyEvent)
        {
            return false;
        }

        public boolean onKeyUp(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
        {
            return false;
        }
    }

    public static enum Capitalize
    {
        static
        {
            CHARACTERS = new Capitalize("CHARACTERS", 3);
            Capitalize[] arrayOfCapitalize = new Capitalize[4];
            arrayOfCapitalize[0] = NONE;
            arrayOfCapitalize[1] = SENTENCES;
            arrayOfCapitalize[2] = WORDS;
            arrayOfCapitalize[3] = CHARACTERS;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.TextKeyListener
 * JD-Core Version:        0.6.2
 */