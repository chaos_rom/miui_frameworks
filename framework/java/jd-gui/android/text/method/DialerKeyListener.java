package android.text.method;

import android.text.Spannable;
import android.view.KeyCharacterMap.KeyData;
import android.view.KeyEvent;

public class DialerKeyListener extends NumberKeyListener
{
    public static final char[] CHARACTERS = arrayOfChar;
    private static DialerKeyListener sInstance;

    static
    {
        char[] arrayOfChar = new char[22];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
        arrayOfChar[10] = 35;
        arrayOfChar[11] = 42;
        arrayOfChar[12] = 43;
        arrayOfChar[13] = 45;
        arrayOfChar[14] = 40;
        arrayOfChar[15] = 41;
        arrayOfChar[16] = 44;
        arrayOfChar[17] = 47;
        arrayOfChar[18] = 78;
        arrayOfChar[19] = 46;
        arrayOfChar[20] = 32;
        arrayOfChar[21] = 59;
    }

    public static DialerKeyListener getInstance()
    {
        if (sInstance != null);
        for (DialerKeyListener localDialerKeyListener = sInstance; ; localDialerKeyListener = sInstance)
        {
            return localDialerKeyListener;
            sInstance = new DialerKeyListener();
        }
    }

    protected char[] getAcceptedChars()
    {
        return CHARACTERS;
    }

    public int getInputType()
    {
        return 3;
    }

    protected int lookup(KeyEvent paramKeyEvent, Spannable paramSpannable)
    {
        int i = paramKeyEvent.getMetaState() | getMetaState(paramSpannable);
        int j = paramKeyEvent.getNumber();
        if (((i & 0x3) == 0) && (j != 0));
        label125: 
        while (true)
        {
            return j;
            int k = super.lookup(paramKeyEvent, paramSpannable);
            if (k != 0)
            {
                j = k;
            }
            else if (i != 0)
            {
                KeyCharacterMap.KeyData localKeyData = new KeyCharacterMap.KeyData();
                char[] arrayOfChar = getAcceptedChars();
                if (paramKeyEvent.getKeyData(localKeyData))
                    for (int m = 1; ; m++)
                    {
                        if (m >= localKeyData.meta.length)
                            break label125;
                        if (ok(arrayOfChar, localKeyData.meta[m]))
                        {
                            j = localKeyData.meta[m];
                            break;
                        }
                    }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.DialerKeyListener
 * JD-Core Version:        0.6.2
 */