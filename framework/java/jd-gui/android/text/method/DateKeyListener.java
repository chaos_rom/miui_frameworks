package android.text.method;

public class DateKeyListener extends NumberKeyListener
{
    public static final char[] CHARACTERS = arrayOfChar;
    private static DateKeyListener sInstance;

    static
    {
        char[] arrayOfChar = new char[13];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
        arrayOfChar[10] = 47;
        arrayOfChar[11] = 45;
        arrayOfChar[12] = 46;
    }

    public static DateKeyListener getInstance()
    {
        if (sInstance != null);
        for (DateKeyListener localDateKeyListener = sInstance; ; localDateKeyListener = sInstance)
        {
            return localDateKeyListener;
            sInstance = new DateKeyListener();
        }
    }

    protected char[] getAcceptedChars()
    {
        return CHARACTERS;
    }

    public int getInputType()
    {
        return 20;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.DateKeyListener
 * JD-Core Version:        0.6.2
 */