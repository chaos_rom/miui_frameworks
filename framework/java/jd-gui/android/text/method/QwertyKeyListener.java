package android.text.method;

import android.text.AutoText;
import android.text.Editable;
import android.text.NoCopySpan;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;

public class QwertyKeyListener extends BaseKeyListener
{
    private static SparseArray<String> PICKER_SETS;
    private static QwertyKeyListener sFullKeyboardInstance;
    private static QwertyKeyListener[] sInstance = new QwertyKeyListener[2 * TextKeyListener.Capitalize.values().length];
    private TextKeyListener.Capitalize mAutoCap;
    private boolean mAutoText;
    private boolean mFullKeyboard;

    static
    {
        PICKER_SETS = new SparseArray();
        PICKER_SETS.put(65, "ÀÁÂÄÆÃÅĄĀ");
        PICKER_SETS.put(67, "ÇĆČ");
        PICKER_SETS.put(68, "Ď");
        PICKER_SETS.put(69, "ÈÉÊËĘĚĒ");
        PICKER_SETS.put(71, "Ğ");
        PICKER_SETS.put(76, "Ł");
        PICKER_SETS.put(73, "ÌÍÎÏĪİ");
        PICKER_SETS.put(78, "ÑŃŇ");
        PICKER_SETS.put(79, "ØŒÕÒÓÔÖŌ");
        PICKER_SETS.put(82, "Ř");
        PICKER_SETS.put(83, "ŚŠŞ");
        PICKER_SETS.put(84, "Ť");
        PICKER_SETS.put(85, "ÙÚÛÜŮŪ");
        PICKER_SETS.put(89, "ÝŸ");
        PICKER_SETS.put(90, "ŹŻŽ");
        PICKER_SETS.put(97, "àáâäæãåąā");
        PICKER_SETS.put(99, "çćč");
        PICKER_SETS.put(100, "ď");
        PICKER_SETS.put(101, "èéêëęěē");
        PICKER_SETS.put(103, "ğ");
        PICKER_SETS.put(105, "ìíîïīı");
        PICKER_SETS.put(108, "ł");
        PICKER_SETS.put(110, "ñńň");
        PICKER_SETS.put(111, "øœõòóôöō");
        PICKER_SETS.put(114, "ř");
        PICKER_SETS.put(115, "§ßśšş");
        PICKER_SETS.put(116, "ť");
        PICKER_SETS.put(117, "ùúûüůū");
        PICKER_SETS.put(121, "ýÿ");
        PICKER_SETS.put(122, "źżž");
        PICKER_SETS.put(61185, "…¥•®©±[]{}\\|");
        PICKER_SETS.put(47, "\\");
        PICKER_SETS.put(49, "¹½⅓¼⅛");
        PICKER_SETS.put(50, "²⅔");
        PICKER_SETS.put(51, "³¾⅜");
        PICKER_SETS.put(52, "⁴");
        PICKER_SETS.put(53, "⅝");
        PICKER_SETS.put(55, "⅞");
        PICKER_SETS.put(48, "ⁿ∅");
        PICKER_SETS.put(36, "¢£€¥₣₤₱");
        PICKER_SETS.put(37, "‰");
        PICKER_SETS.put(42, "†‡");
        PICKER_SETS.put(45, "–—");
        PICKER_SETS.put(43, "±");
        PICKER_SETS.put(40, "[{<");
        PICKER_SETS.put(41, "]}>");
        PICKER_SETS.put(33, "¡");
        PICKER_SETS.put(34, "“”«»˝");
        PICKER_SETS.put(63, "¿");
        PICKER_SETS.put(44, "‚„");
        PICKER_SETS.put(61, "≠≈∞");
        PICKER_SETS.put(60, "≤«‹");
        PICKER_SETS.put(62, "≥»›");
    }

    public QwertyKeyListener(TextKeyListener.Capitalize paramCapitalize, boolean paramBoolean)
    {
        this(paramCapitalize, paramBoolean, false);
    }

    private QwertyKeyListener(TextKeyListener.Capitalize paramCapitalize, boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mAutoCap = paramCapitalize;
        this.mAutoText = paramBoolean1;
        this.mFullKeyboard = paramBoolean2;
    }

    public static QwertyKeyListener getInstance(boolean paramBoolean, TextKeyListener.Capitalize paramCapitalize)
    {
        int i = 2 * paramCapitalize.ordinal();
        if (paramBoolean);
        for (int j = 1; ; j = 0)
        {
            int k = i + j;
            if (sInstance[k] == null)
                sInstance[k] = new QwertyKeyListener(paramCapitalize, paramBoolean);
            return sInstance[k];
        }
    }

    public static QwertyKeyListener getInstanceForFullKeyboard()
    {
        if (sFullKeyboardInstance == null)
            sFullKeyboardInstance = new QwertyKeyListener(TextKeyListener.Capitalize.NONE, false, true);
        return sFullKeyboardInstance;
    }

    private String getReplacement(CharSequence paramCharSequence, int paramInt1, int paramInt2, View paramView)
    {
        int i = paramInt2 - paramInt1;
        int j = 0;
        String str1 = AutoText.get(paramCharSequence, paramInt1, paramInt2, paramView);
        String str2;
        if (str1 == null)
        {
            str1 = AutoText.get(TextUtils.substring(paramCharSequence, paramInt1, paramInt2).toLowerCase(), 0, paramInt2 - paramInt1, paramView);
            j = 1;
            if (str1 == null)
                str2 = null;
        }
        label176: 
        while (true)
        {
            return str2;
            int k = 0;
            if (j != 0)
                for (int m = paramInt1; m < paramInt2; m++)
                    if (Character.isUpperCase(paramCharSequence.charAt(m)))
                        k++;
            if (k == 0)
                str2 = str1;
            while (true)
            {
                if ((str2.length() != i) || (!TextUtils.regionMatches(paramCharSequence, paramInt1, str2, 0, i)))
                    break label176;
                str2 = null;
                break;
                if (k == 1)
                    str2 = toTitleCase(str1);
                else if (k == i)
                    str2 = str1.toUpperCase();
                else
                    str2 = toTitleCase(str1);
            }
        }
    }

    public static void markAsReplaced(Spannable paramSpannable, int paramInt1, int paramInt2, String paramString)
    {
        Replaced[] arrayOfReplaced = (Replaced[])paramSpannable.getSpans(0, paramSpannable.length(), Replaced.class);
        for (int i = 0; i < arrayOfReplaced.length; i++)
            paramSpannable.removeSpan(arrayOfReplaced[i]);
        int j = paramString.length();
        char[] arrayOfChar = new char[j];
        paramString.getChars(0, j, arrayOfChar, 0);
        paramSpannable.setSpan(new Replaced(arrayOfChar), paramInt1, paramInt2, 33);
    }

    private boolean showCharacterPicker(View paramView, Editable paramEditable, char paramChar, boolean paramBoolean, int paramInt)
    {
        String str = (String)PICKER_SETS.get(paramChar);
        if (str == null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            if (paramInt == 1)
                new CharacterPickerDialog(paramView.getContext(), paramView, paramEditable, str, paramBoolean).show();
        }
    }

    private static String toTitleCase(String paramString)
    {
        return Character.toUpperCase(paramString.charAt(0)) + paramString.substring(1);
    }

    public int getInputType()
    {
        return makeTextContentType(this.mAutoCap, this.mAutoText);
    }

    public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 0;
        if (paramView != null)
            i = TextKeyListener.getInstance().getPrefs(paramView.getContext());
        int j = Selection.getSelectionStart(paramEditable);
        int k = Selection.getSelectionEnd(paramEditable);
        int m = Math.min(j, k);
        int n = Math.max(j, k);
        if ((m < 0) || (n < 0))
        {
            n = 0;
            m = 0;
            Selection.setSelection(paramEditable, 0, 0);
        }
        int i1 = paramEditable.getSpanStart(TextKeyListener.ACTIVE);
        int i2 = paramEditable.getSpanEnd(TextKeyListener.ACTIVE);
        int i3 = paramKeyEvent.getUnicodeChar(paramKeyEvent.getMetaState() | getMetaState(paramEditable));
        boolean bool;
        if (!this.mFullKeyboard)
        {
            int i26 = paramKeyEvent.getRepeatCount();
            if ((i26 > 0) && (m == n) && (m > 0))
            {
                int i27 = paramEditable.charAt(m - 1);
                if (((i27 == i3) || ((i27 == Character.toUpperCase(i3)) && (paramView != null))) && (showCharacterPicker(paramView, paramEditable, i27, false, i26)))
                {
                    resetMetaState(paramEditable);
                    bool = true;
                }
            }
        }
        while (true)
        {
            return bool;
            if (i3 == 61185)
            {
                if (paramView != null)
                    showCharacterPicker(paramView, paramEditable, 61185, true, 1);
                resetMetaState(paramEditable);
                bool = true;
                continue;
            }
            int i23;
            int i24;
            if (i3 == 61184)
            {
                if (m == n)
                    for (i23 = n; (i23 > 0) && (n - i23 < 4) && (Character.digit(paramEditable.charAt(i23 - 1), 16) >= 0); i23--);
                i23 = m;
                i24 = -1;
            }
            try
            {
                int i25 = Integer.parseInt(TextUtils.substring(paramEditable, i23, n), 16);
                i24 = i25;
                label314: int i9;
                if (i24 >= 0)
                {
                    m = i23;
                    Selection.setSelection(paramEditable, m, n);
                    i3 = i24;
                    if (i3 == 0)
                        break label1135;
                    int i8 = 0;
                    if ((0x80000000 & i3) != 0)
                    {
                        i8 = 1;
                        i3 &= 2147483647;
                    }
                    if ((i1 == m) && (i2 == n))
                    {
                        int i21 = 0;
                        if (-1 + (n - m) == 0)
                        {
                            int i22 = KeyEvent.getDeadChar(paramEditable.charAt(m), i3);
                            if (i22 != 0)
                            {
                                i3 = i22;
                                i21 = 1;
                            }
                        }
                        if (i21 == 0)
                        {
                            Selection.setSelection(paramEditable, n);
                            paramEditable.removeSpan(TextKeyListener.ACTIVE);
                            m = n;
                        }
                    }
                    if (((i & 0x1) != 0) && (Character.isLowerCase(i3)) && (TextKeyListener.shouldCap(this.mAutoCap, paramEditable, m)))
                    {
                        int i16 = paramEditable.getSpanEnd(TextKeyListener.CAPPED);
                        int i17 = paramEditable.getSpanFlags(TextKeyListener.CAPPED);
                        if ((i16 != m) || ((0xFFFF & i17 >> 16) != i3))
                            break label847;
                        paramEditable.removeSpan(TextKeyListener.CAPPED);
                    }
                    label522: if (m != n)
                        Selection.setSelection(paramEditable, n);
                    paramEditable.setSpan(OLD_SEL_START, m, m, 17);
                    String str2 = String.valueOf((char)i3);
                    paramEditable.replace(m, n, str2);
                    i9 = paramEditable.getSpanStart(OLD_SEL_START);
                    int i10 = Selection.getSelectionEnd(paramEditable);
                    if (i9 < i10)
                    {
                        paramEditable.setSpan(TextKeyListener.LAST_TYPED, i9, i10, 33);
                        if (i8 != 0)
                        {
                            Selection.setSelection(paramEditable, i9, i10);
                            paramEditable.setSpan(TextKeyListener.ACTIVE, i9, i10, 33);
                        }
                    }
                    adjustMetaAfterKeypress(paramEditable);
                    if (((i & 0x2) == 0) || (!this.mAutoText) || ((i3 != 32) && (i3 != 9) && (i3 != 10) && (i3 != 44) && (i3 != 46) && (i3 != 33) && (i3 != 63) && (i3 != 34) && (Character.getType(i3) != 22)) || (paramEditable.getSpanEnd(TextKeyListener.INHIBIT_REPLACEMENT) == i9))
                        break label980;
                }
                String str3;
                for (int i13 = i9; ; i13--)
                    if (i13 > 0)
                    {
                        char c2 = paramEditable.charAt(i13 - 1);
                        if ((c2 == '\'') || (Character.isLetter(c2)));
                    }
                    else
                    {
                        str3 = getReplacement(paramEditable, i13, i9, paramView);
                        if (str3 == null)
                            break label980;
                        Replaced[] arrayOfReplaced2 = (Replaced[])paramEditable.getSpans(0, paramEditable.length(), Replaced.class);
                        for (int i14 = 0; ; i14++)
                        {
                            int i15 = arrayOfReplaced2.length;
                            if (i14 >= i15)
                                break;
                            paramEditable.removeSpan(arrayOfReplaced2[i14]);
                        }
                        i3 = 0;
                        break;
                        label847: int i18 = i3 << 16;
                        i3 = Character.toUpperCase(i3);
                        if (m == 0)
                        {
                            paramEditable.setSpan(TextKeyListener.CAPPED, 0, 0, i18 | 0x11);
                            break label522;
                        }
                        Object localObject = TextKeyListener.CAPPED;
                        int i19 = m - 1;
                        int i20 = i18 | 0x21;
                        paramEditable.setSpan(localObject, i19, m, i20);
                        break label522;
                    }
                char[] arrayOfChar = new char[i9 - i13];
                TextUtils.getChars(paramEditable, i13, i9, arrayOfChar, 0);
                paramEditable.setSpan(new Replaced(arrayOfChar), i13, i9, 33);
                paramEditable.replace(i13, i9, str3);
                label980: if (((i & 0x4) != 0) && (this.mAutoText))
                {
                    int i11 = Selection.getSelectionEnd(paramEditable);
                    if ((i11 - 3 >= 0) && (paramEditable.charAt(i11 - 1) == ' ') && (paramEditable.charAt(i11 - 2) == ' '))
                    {
                        char c1 = paramEditable.charAt(i11 - 3);
                        for (int i12 = i11 - 3; (i12 > 0) && ((c1 == '"') || (Character.getType(c1) == 22)); i12--)
                            c1 = paramEditable.charAt(i12 - 1);
                        if ((Character.isLetter(c1)) || (Character.isDigit(c1)))
                            paramEditable.replace(i11 - 2, i11 - 1, ".");
                    }
                }
                bool = true;
                continue;
                label1135: if ((paramInt == 67) && ((paramKeyEvent.hasNoModifiers()) || (paramKeyEvent.hasModifiers(2))) && (m == n))
                {
                    int i4 = 1;
                    if ((paramEditable.getSpanEnd(TextKeyListener.LAST_TYPED) == m) && (paramEditable.charAt(m - 1) != '\n'))
                        i4 = 2;
                    Replaced[] arrayOfReplaced1 = (Replaced[])paramEditable.getSpans(m - i4, m, Replaced.class);
                    if (arrayOfReplaced1.length > 0)
                    {
                        int i5 = paramEditable.getSpanStart(arrayOfReplaced1[0]);
                        int i6 = paramEditable.getSpanEnd(arrayOfReplaced1[0]);
                        String str1 = new String(arrayOfReplaced1[0].mText);
                        paramEditable.removeSpan(arrayOfReplaced1[0]);
                        if (m >= i6)
                        {
                            paramEditable.setSpan(TextKeyListener.INHIBIT_REPLACEMENT, i6, i6, 34);
                            paramEditable.replace(i5, i6, str1);
                            int i7 = paramEditable.getSpanStart(TextKeyListener.INHIBIT_REPLACEMENT);
                            if (i7 - 1 >= 0)
                                paramEditable.setSpan(TextKeyListener.INHIBIT_REPLACEMENT, i7 - 1, i7, 33);
                            while (true)
                            {
                                adjustMetaAfterKeypress(paramEditable);
                                bool = true;
                                break;
                                paramEditable.removeSpan(TextKeyListener.INHIBIT_REPLACEMENT);
                            }
                        }
                        adjustMetaAfterKeypress(paramEditable);
                        bool = super.onKeyDown(paramView, paramEditable, paramInt, paramKeyEvent);
                        continue;
                    }
                }
                bool = super.onKeyDown(paramView, paramEditable, paramInt, paramKeyEvent);
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label314;
            }
        }
    }

    static class Replaced
        implements NoCopySpan
    {
        private char[] mText;

        public Replaced(char[] paramArrayOfChar)
        {
            this.mText = paramArrayOfChar;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.QwertyKeyListener
 * JD-Core Version:        0.6.2
 */