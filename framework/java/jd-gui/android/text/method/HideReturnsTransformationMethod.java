package android.text.method;

public class HideReturnsTransformationMethod extends ReplacementTransformationMethod
{
    private static char[] ORIGINAL;
    private static char[] REPLACEMENT = arrayOfChar2;
    private static HideReturnsTransformationMethod sInstance;

    static
    {
        char[] arrayOfChar1 = new char[1];
        arrayOfChar1[0] = '\r';
        ORIGINAL = arrayOfChar1;
        char[] arrayOfChar2 = new char[1];
        arrayOfChar2[0] = 65279;
    }

    public static HideReturnsTransformationMethod getInstance()
    {
        if (sInstance != null);
        for (HideReturnsTransformationMethod localHideReturnsTransformationMethod = sInstance; ; localHideReturnsTransformationMethod = sInstance)
        {
            return localHideReturnsTransformationMethod;
            sInstance = new HideReturnsTransformationMethod();
        }
    }

    protected char[] getOriginal()
    {
        return ORIGINAL;
    }

    protected char[] getReplacement()
    {
        return REPLACEMENT;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.HideReturnsTransformationMethod
 * JD-Core Version:        0.6.2
 */