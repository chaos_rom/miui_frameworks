package android.text.method;

import android.graphics.Rect;
import android.text.Editable;
import android.text.GetChars;
import android.text.Spannable;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.view.View;

public abstract class ReplacementTransformationMethod
    implements TransformationMethod
{
    protected abstract char[] getOriginal();

    protected abstract char[] getReplacement();

    public CharSequence getTransformation(CharSequence paramCharSequence, View paramView)
    {
        char[] arrayOfChar1 = getOriginal();
        char[] arrayOfChar2 = getReplacement();
        int k;
        if (!(paramCharSequence instanceof Editable))
        {
            int i = 1;
            int j = arrayOfChar1.length;
            k = 0;
            if (k < j)
            {
                if (TextUtils.indexOf(paramCharSequence, arrayOfChar1[k]) >= 0)
                    i = 0;
            }
            else
                if (i == 0)
                    break label62;
        }
        while (true)
        {
            return paramCharSequence;
            k++;
            break;
            label62: if (!(paramCharSequence instanceof Spannable))
            {
                if ((paramCharSequence instanceof Spanned))
                    paramCharSequence = new SpannedString(new SpannedReplacementCharSequence((Spanned)paramCharSequence, arrayOfChar1, arrayOfChar2));
                else
                    paramCharSequence = new ReplacementCharSequence(paramCharSequence, arrayOfChar1, arrayOfChar2).toString();
            }
            else if ((paramCharSequence instanceof Spanned))
                paramCharSequence = new SpannedReplacementCharSequence((Spanned)paramCharSequence, arrayOfChar1, arrayOfChar2);
            else
                paramCharSequence = new ReplacementCharSequence(paramCharSequence, arrayOfChar1, arrayOfChar2);
        }
    }

    public void onFocusChanged(View paramView, CharSequence paramCharSequence, boolean paramBoolean, int paramInt, Rect paramRect)
    {
    }

    private static class SpannedReplacementCharSequence extends ReplacementTransformationMethod.ReplacementCharSequence
        implements Spanned
    {
        private Spanned mSpanned;

        public SpannedReplacementCharSequence(Spanned paramSpanned, char[] paramArrayOfChar1, char[] paramArrayOfChar2)
        {
            super(paramArrayOfChar1, paramArrayOfChar2);
            this.mSpanned = paramSpanned;
        }

        public int getSpanEnd(Object paramObject)
        {
            return this.mSpanned.getSpanEnd(paramObject);
        }

        public int getSpanFlags(Object paramObject)
        {
            return this.mSpanned.getSpanFlags(paramObject);
        }

        public int getSpanStart(Object paramObject)
        {
            return this.mSpanned.getSpanStart(paramObject);
        }

        public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass)
        {
            return this.mSpanned.getSpans(paramInt1, paramInt2, paramClass);
        }

        public int nextSpanTransition(int paramInt1, int paramInt2, Class paramClass)
        {
            return this.mSpanned.nextSpanTransition(paramInt1, paramInt2, paramClass);
        }

        public CharSequence subSequence(int paramInt1, int paramInt2)
        {
            return new SpannedString(this).subSequence(paramInt1, paramInt2);
        }
    }

    private static class ReplacementCharSequence
        implements CharSequence, GetChars
    {
        private char[] mOriginal;
        private char[] mReplacement;
        private CharSequence mSource;

        public ReplacementCharSequence(CharSequence paramCharSequence, char[] paramArrayOfChar1, char[] paramArrayOfChar2)
        {
            this.mSource = paramCharSequence;
            this.mOriginal = paramArrayOfChar1;
            this.mReplacement = paramArrayOfChar2;
        }

        public char charAt(int paramInt)
        {
            char c = this.mSource.charAt(paramInt);
            int i = this.mOriginal.length;
            for (int j = 0; j < i; j++)
                if (c == this.mOriginal[j])
                    c = this.mReplacement[j];
            return c;
        }

        public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
        {
            TextUtils.getChars(this.mSource, paramInt1, paramInt2, paramArrayOfChar, paramInt3);
            int i = paramInt3 + (paramInt2 - paramInt1);
            int j = this.mOriginal.length;
            for (int k = paramInt3; k < i; k++)
            {
                int m = paramArrayOfChar[k];
                for (int n = 0; n < j; n++)
                    if (m == this.mOriginal[n])
                        paramArrayOfChar[k] = this.mReplacement[n];
            }
        }

        public int length()
        {
            return this.mSource.length();
        }

        public CharSequence subSequence(int paramInt1, int paramInt2)
        {
            char[] arrayOfChar = new char[paramInt2 - paramInt1];
            getChars(paramInt1, paramInt2, arrayOfChar, 0);
            return new String(arrayOfChar);
        }

        public String toString()
        {
            char[] arrayOfChar = new char[length()];
            getChars(0, length(), arrayOfChar, 0);
            return new String(arrayOfChar);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.ReplacementTransformationMethod
 * JD-Core Version:        0.6.2
 */