package android.text.method;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;

public class CharacterPickerDialog extends Dialog
    implements AdapterView.OnItemClickListener, View.OnClickListener
{
    private Button mCancelButton;
    private LayoutInflater mInflater;
    private boolean mInsert;
    private String mOptions;
    private Editable mText;
    private View mView;

    public CharacterPickerDialog(Context paramContext, View paramView, Editable paramEditable, String paramString, boolean paramBoolean)
    {
        super(paramContext, 16973913);
        this.mView = paramView;
        this.mText = paramEditable;
        this.mOptions = paramString;
        this.mInsert = paramBoolean;
        this.mInflater = LayoutInflater.from(paramContext);
    }

    private void replaceCharacterAndClose(CharSequence paramCharSequence)
    {
        int i = Selection.getSelectionEnd(this.mText);
        if ((this.mInsert) || (i == 0))
            this.mText.insert(i, paramCharSequence);
        while (true)
        {
            dismiss();
            return;
            this.mText.replace(i - 1, i, paramCharSequence);
        }
    }

    public void onClick(View paramView)
    {
        if (paramView == this.mCancelButton)
            dismiss();
        while (true)
        {
            return;
            if ((paramView instanceof Button))
                replaceCharacterAndClose(((Button)paramView).getText());
        }
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        localLayoutParams.token = this.mView.getApplicationWindowToken();
        localLayoutParams.type = 1003;
        localLayoutParams.flags = (0x1 | localLayoutParams.flags);
        setContentView(17367087);
        GridView localGridView = (GridView)findViewById(16908904);
        localGridView.setAdapter(new OptionsAdapter(getContext()));
        localGridView.setOnItemClickListener(this);
        this.mCancelButton = ((Button)findViewById(16908905));
        this.mCancelButton.setOnClickListener(this);
    }

    public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
    {
        replaceCharacterAndClose(String.valueOf(this.mOptions.charAt(paramInt)));
    }

    private class OptionsAdapter extends BaseAdapter
    {
        public OptionsAdapter(Context arg2)
        {
        }

        public final int getCount()
        {
            return CharacterPickerDialog.this.mOptions.length();
        }

        public final Object getItem(int paramInt)
        {
            return String.valueOf(CharacterPickerDialog.this.mOptions.charAt(paramInt));
        }

        public final long getItemId(int paramInt)
        {
            return paramInt;
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            Button localButton = (Button)CharacterPickerDialog.this.mInflater.inflate(17367088, null);
            localButton.setText(String.valueOf(CharacterPickerDialog.this.mOptions.charAt(paramInt)));
            localButton.setOnClickListener(CharacterPickerDialog.this);
            return localButton;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.CharacterPickerDialog
 * JD-Core Version:        0.6.2
 */