package android.text.method;

import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;

public class MultiTapKeyListener extends BaseKeyListener
    implements SpanWatcher
{
    private static MultiTapKeyListener[] sInstance = new MultiTapKeyListener[2 * TextKeyListener.Capitalize.values().length];
    private static final SparseArray<String> sRecs = new SparseArray();
    private boolean mAutoText;
    private TextKeyListener.Capitalize mCapitalize;

    static
    {
        sRecs.put(8, ".,1!@#$%^&*:/?'=()");
        sRecs.put(9, "abc2ABC");
        sRecs.put(10, "def3DEF");
        sRecs.put(11, "ghi4GHI");
        sRecs.put(12, "jkl5JKL");
        sRecs.put(13, "mno6MNO");
        sRecs.put(14, "pqrs7PQRS");
        sRecs.put(15, "tuv8TUV");
        sRecs.put(16, "wxyz9WXYZ");
        sRecs.put(7, "0+");
        sRecs.put(18, " ");
    }

    public MultiTapKeyListener(TextKeyListener.Capitalize paramCapitalize, boolean paramBoolean)
    {
        this.mCapitalize = paramCapitalize;
        this.mAutoText = paramBoolean;
    }

    public static MultiTapKeyListener getInstance(boolean paramBoolean, TextKeyListener.Capitalize paramCapitalize)
    {
        int i = 2 * paramCapitalize.ordinal();
        if (paramBoolean);
        for (int j = 1; ; j = 0)
        {
            int k = i + j;
            if (sInstance[k] == null)
                sInstance[k] = new MultiTapKeyListener(paramCapitalize, paramBoolean);
            return sInstance[k];
        }
    }

    private static void removeTimeouts(Spannable paramSpannable)
    {
        Timeout[] arrayOfTimeout = (Timeout[])paramSpannable.getSpans(0, paramSpannable.length(), Timeout.class);
        for (int i = 0; i < arrayOfTimeout.length; i++)
        {
            Timeout localTimeout = arrayOfTimeout[i];
            localTimeout.removeCallbacks(localTimeout);
            Timeout.access$002(localTimeout, null);
            paramSpannable.removeSpan(localTimeout);
        }
    }

    public int getInputType()
    {
        return makeTextContentType(this.mCapitalize, this.mAutoText);
    }

    public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 0;
        if (paramView != null)
            i = TextKeyListener.getInstance().getPrefs(paramView.getContext());
        int j = Selection.getSelectionStart(paramEditable);
        int k = Selection.getSelectionEnd(paramEditable);
        int m = Math.min(j, k);
        int n = Math.max(j, k);
        int i1 = paramEditable.getSpanStart(TextKeyListener.ACTIVE);
        int i2 = paramEditable.getSpanEnd(TextKeyListener.ACTIVE);
        int i3 = (0xFF000000 & paramEditable.getSpanFlags(TextKeyListener.ACTIVE)) >>> 24;
        char c;
        boolean bool;
        if ((i1 == m) && (i2 == n) && (n - m == 1) && (i3 >= 0) && (i3 < sRecs.size()))
            if (paramInt == 17)
            {
                c = paramEditable.charAt(m);
                if (Character.isLowerCase(c))
                {
                    paramEditable.replace(m, n, String.valueOf(c).toUpperCase());
                    removeTimeouts(paramEditable);
                    new Timeout(paramEditable);
                    bool = true;
                }
            }
        while (true)
        {
            return bool;
            if (Character.isUpperCase(c))
            {
                paramEditable.replace(m, n, String.valueOf(c).toLowerCase());
                removeTimeouts(paramEditable);
                new Timeout(paramEditable);
                bool = true;
            }
            else if (sRecs.indexOfKey(paramInt) == i3)
            {
                String str2 = (String)sRecs.valueAt(i3);
                int i13 = str2.indexOf(paramEditable.charAt(m));
                if (i13 >= 0)
                {
                    int i14 = (i13 + 1) % str2.length();
                    paramEditable.replace(m, n, str2, i14, i14 + 1);
                    removeTimeouts(paramEditable);
                    new Timeout(paramEditable);
                    bool = true;
                }
            }
            else
            {
                int i4 = sRecs.indexOfKey(paramInt);
                if (i4 >= 0)
                {
                    Selection.setSelection(paramEditable, n, n);
                    m = n;
                }
                if (i4 >= 0)
                {
                    String str1 = (String)sRecs.valueAt(i4);
                    int i5 = 0;
                    if (((i & 0x1) != 0) && (TextKeyListener.shouldCap(this.mCapitalize, paramEditable, m)));
                    for (int i11 = 0; ; i11++)
                    {
                        int i12 = str1.length();
                        if (i11 < i12)
                        {
                            if (Character.isUpperCase(str1.charAt(i11)))
                                i5 = i11;
                        }
                        else
                        {
                            if (m != n)
                                Selection.setSelection(paramEditable, n);
                            paramEditable.setSpan(OLD_SEL_START, m, m, 17);
                            int i6 = i5 + 1;
                            paramEditable.replace(m, n, str1, i5, i6);
                            int i7 = paramEditable.getSpanStart(OLD_SEL_START);
                            int i8 = Selection.getSelectionEnd(paramEditable);
                            if (i8 != i7)
                            {
                                Selection.setSelection(paramEditable, i7, i8);
                                paramEditable.setSpan(TextKeyListener.LAST_TYPED, i7, i8, 33);
                                paramEditable.setSpan(TextKeyListener.ACTIVE, i7, i8, 0x21 | i4 << 24);
                            }
                            removeTimeouts(paramEditable);
                            new Timeout(paramEditable);
                            if (paramEditable.getSpanStart(this) >= 0)
                                break label663;
                            KeyListener[] arrayOfKeyListener = (KeyListener[])paramEditable.getSpans(0, paramEditable.length(), KeyListener.class);
                            int i9 = arrayOfKeyListener.length;
                            for (int i10 = 0; i10 < i9; i10++)
                                paramEditable.removeSpan(arrayOfKeyListener[i10]);
                            i4 = sRecs.indexOfKey(paramInt);
                            break;
                        }
                    }
                    paramEditable.setSpan(this, 0, paramEditable.length(), 18);
                    label663: bool = true;
                }
                else
                {
                    bool = super.onKeyDown(paramView, paramEditable, paramInt, paramKeyEvent);
                }
            }
        }
    }

    public void onSpanAdded(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
    {
    }

    public void onSpanChanged(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (paramObject == Selection.SELECTION_END)
        {
            paramSpannable.removeSpan(TextKeyListener.ACTIVE);
            removeTimeouts(paramSpannable);
        }
    }

    public void onSpanRemoved(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
    {
    }

    private class Timeout extends Handler
        implements Runnable
    {
        private Editable mBuffer;

        public Timeout(Editable arg2)
        {
            Object localObject;
            this.mBuffer = localObject;
            this.mBuffer.setSpan(this, 0, this.mBuffer.length(), 18);
            postAtTime(this, 2000L + SystemClock.uptimeMillis());
        }

        public void run()
        {
            Editable localEditable = this.mBuffer;
            if (localEditable != null)
            {
                int i = Selection.getSelectionStart(localEditable);
                int j = Selection.getSelectionEnd(localEditable);
                int k = localEditable.getSpanStart(TextKeyListener.ACTIVE);
                int m = localEditable.getSpanEnd(TextKeyListener.ACTIVE);
                if ((i == k) && (j == m))
                    Selection.setSelection(localEditable, Selection.getSelectionEnd(localEditable));
                localEditable.removeSpan(this);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.MultiTapKeyListener
 * JD-Core Version:        0.6.2
 */