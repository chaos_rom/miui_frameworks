package android.text.method;

public class DateTimeKeyListener extends NumberKeyListener
{
    public static final char[] CHARACTERS = arrayOfChar;
    private static DateTimeKeyListener sInstance;

    static
    {
        char[] arrayOfChar = new char[17];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
        arrayOfChar[10] = 97;
        arrayOfChar[11] = 109;
        arrayOfChar[12] = 112;
        arrayOfChar[13] = 58;
        arrayOfChar[14] = 47;
        arrayOfChar[15] = 45;
        arrayOfChar[16] = 32;
    }

    public static DateTimeKeyListener getInstance()
    {
        if (sInstance != null);
        for (DateTimeKeyListener localDateTimeKeyListener = sInstance; ; localDateTimeKeyListener = sInstance)
        {
            return localDateTimeKeyListener;
            sInstance = new DateTimeKeyListener();
        }
    }

    protected char[] getAcceptedChars()
    {
        return CHARACTERS;
    }

    public int getInputType()
    {
        return 4;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.DateTimeKeyListener
 * JD-Core Version:        0.6.2
 */