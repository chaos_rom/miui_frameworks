package android.text.method;

public class TimeKeyListener extends NumberKeyListener
{
    public static final char[] CHARACTERS = arrayOfChar;
    private static TimeKeyListener sInstance;

    static
    {
        char[] arrayOfChar = new char[14];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
        arrayOfChar[10] = 97;
        arrayOfChar[11] = 109;
        arrayOfChar[12] = 112;
        arrayOfChar[13] = 58;
    }

    public static TimeKeyListener getInstance()
    {
        if (sInstance != null);
        for (TimeKeyListener localTimeKeyListener = sInstance; ; localTimeKeyListener = sInstance)
        {
            return localTimeKeyListener;
            sInstance = new TimeKeyListener();
        }
    }

    protected char[] getAcceptedChars()
    {
        return CHARACTERS;
    }

    public int getInputType()
    {
        return 36;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.TimeKeyListener
 * JD-Core Version:        0.6.2
 */