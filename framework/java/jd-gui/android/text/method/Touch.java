package android.text.method;

import android.text.Layout;
import android.text.Layout.Alignment;
import android.text.NoCopySpan;
import android.text.Spannable;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.TextView;

public class Touch
{
    public static int getInitialScrollX(TextView paramTextView, Spannable paramSpannable)
    {
        DragState[] arrayOfDragState = (DragState[])paramSpannable.getSpans(0, paramSpannable.length(), DragState.class);
        if (arrayOfDragState.length > 0);
        for (int i = arrayOfDragState[0].mScrollX; ; i = -1)
            return i;
    }

    public static int getInitialScrollY(TextView paramTextView, Spannable paramSpannable)
    {
        DragState[] arrayOfDragState = (DragState[])paramSpannable.getSpans(0, paramSpannable.length(), DragState.class);
        if (arrayOfDragState.length > 0);
        for (int i = arrayOfDragState[0].mScrollY; ; i = -1)
            return i;
    }

    public static boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent)
    {
        switch (paramMotionEvent.getActionMasked())
        {
        default:
        case 0:
        case 1:
        case 2:
        }
        boolean bool;
        DragState[] arrayOfDragState1;
        do
        {
            do
            {
                bool = false;
                while (true)
                {
                    return bool;
                    DragState[] arrayOfDragState3 = (DragState[])paramSpannable.getSpans(0, paramSpannable.length(), DragState.class);
                    for (int i5 = 0; i5 < arrayOfDragState3.length; i5++)
                        paramSpannable.removeSpan(arrayOfDragState3[i5]);
                    paramSpannable.setSpan(new DragState(paramMotionEvent.getX(), paramMotionEvent.getY(), paramTextView.getScrollX(), paramTextView.getScrollY()), 0, 0, 17);
                    bool = true;
                    continue;
                    DragState[] arrayOfDragState2 = (DragState[])paramSpannable.getSpans(0, paramSpannable.length(), DragState.class);
                    for (int i4 = 0; i4 < arrayOfDragState2.length; i4++)
                        paramSpannable.removeSpan(arrayOfDragState2[i4]);
                    if ((arrayOfDragState2.length > 0) && (arrayOfDragState2[0].mUsed))
                        bool = true;
                    else
                        bool = false;
                }
                arrayOfDragState1 = (DragState[])paramSpannable.getSpans(0, paramSpannable.length(), DragState.class);
            }
            while (arrayOfDragState1.length <= 0);
            if (!arrayOfDragState1[0].mFarEnough)
            {
                int i3 = ViewConfiguration.get(paramTextView.getContext()).getScaledTouchSlop();
                if ((Math.abs(paramMotionEvent.getX() - arrayOfDragState1[0].mX) >= i3) || (Math.abs(paramMotionEvent.getY() - arrayOfDragState1[0].mY) >= i3))
                    arrayOfDragState1[0].mFarEnough = true;
            }
        }
        while (!arrayOfDragState1[0].mFarEnough);
        arrayOfDragState1[0].mUsed = true;
        int i;
        label342: float f1;
        if (((0x1 & paramMotionEvent.getMetaState()) != 0) || (MetaKeyKeyListener.getMetaState(paramSpannable, 1) == 1) || (MetaKeyKeyListener.getMetaState(paramSpannable, 2048) != 0))
        {
            i = 1;
            if (i == 0)
                break label510;
            f1 = paramMotionEvent.getX() - arrayOfDragState1[0].mX;
        }
        for (float f2 = paramMotionEvent.getY() - arrayOfDragState1[0].mY; ; f2 = arrayOfDragState1[0].mY - paramMotionEvent.getY())
        {
            arrayOfDragState1[0].mX = paramMotionEvent.getX();
            arrayOfDragState1[0].mY = paramMotionEvent.getY();
            int j = paramTextView.getScrollX() + (int)f1;
            int k = paramTextView.getScrollY() + (int)f2;
            int m = paramTextView.getTotalPaddingTop() + paramTextView.getTotalPaddingBottom();
            Layout localLayout = paramTextView.getLayout();
            int n = Math.max(Math.min(k, localLayout.getHeight() - (paramTextView.getHeight() - m)), 0);
            int i1 = paramTextView.getScrollX();
            int i2 = paramTextView.getScrollY();
            scrollTo(paramTextView, localLayout, j, n);
            if ((i1 != paramTextView.getScrollX()) || (i2 != paramTextView.getScrollY()))
                paramTextView.cancelLongPress();
            bool = true;
            break;
            i = 0;
            break label342;
            label510: f1 = arrayOfDragState1[0].mX - paramMotionEvent.getX();
        }
    }

    public static void scrollTo(TextView paramTextView, Layout paramLayout, int paramInt1, int paramInt2)
    {
        int i = paramTextView.getTotalPaddingLeft() + paramTextView.getTotalPaddingRight();
        int j = paramTextView.getWidth() - i;
        int k = paramLayout.getLineForVertical(paramInt2);
        Layout.Alignment localAlignment = paramLayout.getParagraphAlignment(k);
        if (paramLayout.getParagraphDirection(k) > 0);
        for (int m = 1; paramTextView.getHorizontallyScrolling(); m = 0)
        {
            int i4 = paramTextView.getTotalPaddingTop() + paramTextView.getTotalPaddingBottom();
            int i5 = paramLayout.getLineForVertical(paramInt2 + paramTextView.getHeight() - i4);
            n = 2147483647;
            i1 = 0;
            for (int i6 = k; i6 <= i5; i6++)
            {
                n = (int)Math.min(n, paramLayout.getLineLeft(i6));
                i1 = (int)Math.max(i1, paramLayout.getLineRight(i6));
            }
        }
        int n = 0;
        int i1 = j;
        int i2 = i1 - n;
        int i3;
        if (i2 < j)
            if (localAlignment == Layout.Alignment.ALIGN_CENTER)
                i3 = n - (j - i2) / 2;
        while (true)
        {
            paramTextView.scrollTo(i3, paramInt2);
            return;
            if (((m != 0) && (localAlignment == Layout.Alignment.ALIGN_OPPOSITE)) || (localAlignment == Layout.Alignment.ALIGN_RIGHT))
            {
                i3 = n - (j - i2);
            }
            else
            {
                i3 = n;
                continue;
                i3 = Math.max(Math.min(paramInt1, i1 - j), n);
            }
        }
    }

    private static class DragState
        implements NoCopySpan
    {
        public boolean mFarEnough;
        public int mScrollX;
        public int mScrollY;
        public boolean mUsed;
        public float mX;
        public float mY;

        public DragState(float paramFloat1, float paramFloat2, int paramInt1, int paramInt2)
        {
            this.mX = paramFloat1;
            this.mY = paramFloat2;
            this.mScrollX = paramInt1;
            this.mScrollY = paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.Touch
 * JD-Core Version:        0.6.2
 */