package android.text.method;

import android.text.Selection.PositionIterator;
import android.text.SpannableStringBuilder;
import java.text.BreakIterator;
import java.util.Locale;

public class WordIterator
    implements Selection.PositionIterator
{
    private static final int WINDOW_WIDTH = 50;
    private BreakIterator mIterator;
    private int mOffsetShift;
    private String mString;

    public WordIterator()
    {
        this(Locale.getDefault());
    }

    public WordIterator(Locale paramLocale)
    {
        this.mIterator = BreakIterator.getWordInstance(paramLocale);
    }

    private void checkOffsetIsValid(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > this.mString.length()))
            throw new IllegalArgumentException("Invalid offset: " + (paramInt + this.mOffsetShift) + ". Valid range is [" + this.mOffsetShift + ", " + (this.mString.length() + this.mOffsetShift) + "]");
    }

    private boolean isAfterLetterOrDigit(int paramInt)
    {
        int i = 1;
        if ((paramInt >= i) && (paramInt <= this.mString.length()) && (Character.isLetterOrDigit(this.mString.codePointBefore(paramInt))));
        while (true)
        {
            return i;
            i = 0;
        }
    }

    private boolean isOnLetterOrDigit(int paramInt)
    {
        if ((paramInt >= 0) && (paramInt < this.mString.length()) && (Character.isLetterOrDigit(this.mString.codePointAt(paramInt))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int following(int paramInt)
    {
        int i = -1;
        int j = paramInt - this.mOffsetShift;
        j = this.mIterator.following(j);
        if (j == i);
        while (true)
        {
            return i;
            if (!isAfterLetterOrDigit(j))
                break;
            i = j + this.mOffsetShift;
        }
    }

    public int getBeginning(int paramInt)
    {
        int i = paramInt - this.mOffsetShift;
        checkOffsetIsValid(i);
        int j;
        if (isOnLetterOrDigit(i))
            if (this.mIterator.isBoundary(i))
                j = i + this.mOffsetShift;
        while (true)
        {
            return j;
            j = this.mIterator.preceding(i) + this.mOffsetShift;
            continue;
            if (isAfterLetterOrDigit(i))
                j = this.mIterator.preceding(i) + this.mOffsetShift;
            else
                j = -1;
        }
    }

    public int getEnd(int paramInt)
    {
        int i = paramInt - this.mOffsetShift;
        checkOffsetIsValid(i);
        int j;
        if (isAfterLetterOrDigit(i))
            if (this.mIterator.isBoundary(i))
                j = i + this.mOffsetShift;
        while (true)
        {
            return j;
            j = this.mIterator.following(i) + this.mOffsetShift;
            continue;
            if (isOnLetterOrDigit(i))
                j = this.mIterator.following(i) + this.mOffsetShift;
            else
                j = -1;
        }
    }

    public int preceding(int paramInt)
    {
        int i = -1;
        int j = paramInt - this.mOffsetShift;
        j = this.mIterator.preceding(j);
        if (j == i);
        while (true)
        {
            return i;
            if (!isOnLetterOrDigit(j))
                break;
            i = j + this.mOffsetShift;
        }
    }

    public void setCharSequence(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        this.mOffsetShift = Math.max(0, paramInt1 - 50);
        int i = Math.min(paramCharSequence.length(), paramInt2 + 50);
        if ((paramCharSequence instanceof SpannableStringBuilder));
        for (this.mString = ((SpannableStringBuilder)paramCharSequence).substring(this.mOffsetShift, i); ; this.mString = paramCharSequence.subSequence(this.mOffsetShift, i).toString())
        {
            this.mIterator.setText(this.mString);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.WordIterator
 * JD-Core Version:        0.6.2
 */