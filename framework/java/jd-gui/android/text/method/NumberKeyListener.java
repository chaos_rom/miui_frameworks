package android.text.method;

import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.View;

public abstract class NumberKeyListener extends BaseKeyListener
    implements InputFilter
{
    protected static boolean ok(char[] paramArrayOfChar, char paramChar)
    {
        int i = -1 + paramArrayOfChar.length;
        if (i >= 0)
            if (paramArrayOfChar[i] != paramChar);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
    {
        char[] arrayOfChar = getAcceptedChars();
        int i = paramInt1;
        Object localObject;
        if ((i >= paramInt2) || (!ok(arrayOfChar, paramCharSequence.charAt(i))))
        {
            if (i != paramInt2)
                break label49;
            localObject = null;
        }
        while (true)
        {
            return localObject;
            i++;
            break;
            label49: if (paramInt2 - paramInt1 == 1)
            {
                localObject = "";
            }
            else
            {
                localObject = new SpannableStringBuilder(paramCharSequence, paramInt1, paramInt2);
                int j = i - paramInt1;
                int k = paramInt2 - paramInt1;
                (k - paramInt1);
                for (int m = k - 1; m >= j; m--)
                    if (!ok(arrayOfChar, paramCharSequence.charAt(m)))
                        ((SpannableStringBuilder)localObject).delete(m, m + 1);
            }
        }
    }

    protected abstract char[] getAcceptedChars();

    protected int lookup(KeyEvent paramKeyEvent, Spannable paramSpannable)
    {
        return paramKeyEvent.getMatch(getAcceptedChars(), paramKeyEvent.getMetaState() | getMetaState(paramSpannable));
    }

    public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        int j = 0;
        int k = Selection.getSelectionStart(paramEditable);
        int m = Selection.getSelectionEnd(paramEditable);
        int n = Math.min(k, m);
        int i1 = Math.max(k, m);
        if ((n < 0) || (i1 < 0))
        {
            i1 = 0;
            n = 0;
            Selection.setSelection(paramEditable, 0);
        }
        if (paramKeyEvent != null)
        {
            int i2 = lookup(paramKeyEvent, paramEditable);
            if (paramKeyEvent != null)
                j = paramKeyEvent.getRepeatCount();
            if (j != 0)
                break label135;
            if (i2 == 0)
                break label201;
            if (n != i1)
                Selection.setSelection(paramEditable, i1);
            paramEditable.replace(n, i1, String.valueOf(i2));
            adjustMetaAfterKeypress(paramEditable);
        }
        while (true)
        {
            return i;
            int i3 = 0;
            break;
            label135: if ((i3 == 48) && (j == i) && (n == i1) && (i1 > 0) && (paramEditable.charAt(n - 1) == '0'))
            {
                paramEditable.replace(n - 1, i1, String.valueOf('+'));
                adjustMetaAfterKeypress(paramEditable);
            }
            else
            {
                label201: adjustMetaAfterKeypress(paramEditable);
                i = super.onKeyDown(paramView, paramEditable, paramInt, paramKeyEvent);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.NumberKeyListener
 * JD-Core Version:        0.6.2
 */