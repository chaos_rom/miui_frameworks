package android.text.method;

import android.text.Editable;
import android.text.Layout;
import android.text.NoCopySpan.Concrete;
import android.text.Selection;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public abstract class BaseKeyListener extends MetaKeyKeyListener
    implements KeyListener
{
    static final Object OLD_SEL_START = new NoCopySpan.Concrete();

    private boolean backspaceOrForwardDelete(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent, boolean paramBoolean)
    {
        boolean bool = false;
        if (!KeyEvent.metaStateHasNoModifiers(0xFFFFFF0C & paramKeyEvent.getMetaState()));
        label147: 
        while (true)
        {
            return bool;
            if (deleteSelection(paramView, paramEditable))
            {
                bool = true;
            }
            else if (((paramKeyEvent.isAltPressed()) || (getMetaState(paramEditable, 2) == 1)) && (deleteLine(paramView, paramEditable)))
            {
                bool = true;
            }
            else
            {
                int i = Selection.getSelectionEnd(paramEditable);
                if ((paramBoolean) || (paramKeyEvent.isShiftPressed()) || (getMetaState(paramEditable, 1) == 1));
                for (int j = TextUtils.getOffsetAfter(paramEditable, i); ; j = TextUtils.getOffsetBefore(paramEditable, i))
                {
                    if (i == j)
                        break label147;
                    paramEditable.delete(Math.min(i, j), Math.max(i, j));
                    bool = true;
                    break;
                }
            }
        }
    }

    private boolean deleteLine(View paramView, Editable paramEditable)
    {
        if ((paramView instanceof TextView))
        {
            Layout localLayout = ((TextView)paramView).getLayout();
            if (localLayout != null)
            {
                int i = localLayout.getLineForOffset(Selection.getSelectionStart(paramEditable));
                int j = localLayout.getLineStart(i);
                int k = localLayout.getLineEnd(i);
                if (k != j)
                    paramEditable.delete(j, k);
            }
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean deleteSelection(View paramView, Editable paramEditable)
    {
        int i = Selection.getSelectionStart(paramEditable);
        int j = Selection.getSelectionEnd(paramEditable);
        if (j < i)
        {
            int k = j;
            j = i;
            i = k;
        }
        if (i != j)
            paramEditable.delete(i, j);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    static int makeTextContentType(TextKeyListener.Capitalize paramCapitalize, boolean paramBoolean)
    {
        int i = 1;
        switch (1.$SwitchMap$android$text$method$TextKeyListener$Capitalize[paramCapitalize.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            if (paramBoolean)
                i |= 32768;
            return i;
            i |= 4096;
            continue;
            i |= 8192;
            continue;
            i |= 16384;
        }
    }

    public boolean backspace(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
    {
        return backspaceOrForwardDelete(paramView, paramEditable, paramInt, paramKeyEvent, false);
    }

    public boolean forwardDelete(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
    {
        return backspaceOrForwardDelete(paramView, paramEditable, paramInt, paramKeyEvent, true);
    }

    public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool;
        switch (paramInt)
        {
        default:
            bool = false;
        case 67:
        case 112:
        }
        while (true)
        {
            if (bool)
                adjustMetaAfterKeypress(paramEditable);
            return super.onKeyDown(paramView, paramEditable, paramInt, paramKeyEvent);
            bool = backspace(paramView, paramEditable, paramInt, paramKeyEvent);
            continue;
            bool = forwardDelete(paramView, paramEditable, paramInt, paramKeyEvent);
        }
    }

    public boolean onKeyOther(View paramView, Editable paramEditable, KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        if ((paramKeyEvent.getAction() != 2) || (paramKeyEvent.getKeyCode() != 0));
        while (true)
        {
            return bool;
            int i = Selection.getSelectionStart(paramEditable);
            int j = Selection.getSelectionEnd(paramEditable);
            if (j < i)
            {
                int k = j;
                j = i;
                i = k;
            }
            String str = paramKeyEvent.getCharacters();
            if (str != null)
            {
                paramEditable.replace(i, j, str);
                bool = true;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.BaseKeyListener
 * JD-Core Version:        0.6.2
 */