package android.text.method;

import android.text.SpannableStringBuilder;
import android.text.Spanned;

public class DigitsKeyListener extends NumberKeyListener
{
    private static final char[][] CHARACTERS = arrayOfChar;
    private static final int DECIMAL = 2;
    private static final int SIGN = 1;
    private static DigitsKeyListener[] sInstance = new DigitsKeyListener[4];
    private char[] mAccepted;
    private boolean mDecimal;
    private boolean mSign;

    static
    {
        char[][] arrayOfChar = new char[4][];
        char[] arrayOfChar1 = new char[10];
        arrayOfChar1[0] = 48;
        arrayOfChar1[1] = 49;
        arrayOfChar1[2] = 50;
        arrayOfChar1[3] = 51;
        arrayOfChar1[4] = 52;
        arrayOfChar1[5] = 53;
        arrayOfChar1[6] = 54;
        arrayOfChar1[7] = 55;
        arrayOfChar1[8] = 56;
        arrayOfChar1[9] = 57;
        arrayOfChar[0] = arrayOfChar1;
        char[] arrayOfChar2 = new char[11];
        arrayOfChar2[0] = 48;
        arrayOfChar2[1] = 49;
        arrayOfChar2[2] = 50;
        arrayOfChar2[3] = 51;
        arrayOfChar2[4] = 52;
        arrayOfChar2[5] = 53;
        arrayOfChar2[6] = 54;
        arrayOfChar2[7] = 55;
        arrayOfChar2[8] = 56;
        arrayOfChar2[9] = 57;
        arrayOfChar2[10] = 45;
        arrayOfChar[1] = arrayOfChar2;
        char[] arrayOfChar3 = new char[11];
        arrayOfChar3[0] = 48;
        arrayOfChar3[1] = 49;
        arrayOfChar3[2] = 50;
        arrayOfChar3[3] = 51;
        arrayOfChar3[4] = 52;
        arrayOfChar3[5] = 53;
        arrayOfChar3[6] = 54;
        arrayOfChar3[7] = 55;
        arrayOfChar3[8] = 56;
        arrayOfChar3[9] = 57;
        arrayOfChar3[10] = 46;
        arrayOfChar[2] = arrayOfChar3;
        char[] arrayOfChar4 = new char[12];
        arrayOfChar4[0] = 48;
        arrayOfChar4[1] = 49;
        arrayOfChar4[2] = 50;
        arrayOfChar4[3] = 51;
        arrayOfChar4[4] = 52;
        arrayOfChar4[5] = 53;
        arrayOfChar4[6] = 54;
        arrayOfChar4[7] = 55;
        arrayOfChar4[8] = 56;
        arrayOfChar4[9] = 57;
        arrayOfChar4[10] = 45;
        arrayOfChar4[11] = 46;
        arrayOfChar[3] = arrayOfChar4;
    }

    public DigitsKeyListener()
    {
        this(false, false);
    }

    public DigitsKeyListener(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mSign = paramBoolean1;
        this.mDecimal = paramBoolean2;
        if (paramBoolean1);
        for (int j = 1; ; j = 0)
        {
            if (paramBoolean2)
                i = 2;
            int k = j | i;
            this.mAccepted = CHARACTERS[k];
            return;
        }
    }

    public static DigitsKeyListener getInstance()
    {
        return getInstance(false, false);
    }

    public static DigitsKeyListener getInstance(String paramString)
    {
        DigitsKeyListener localDigitsKeyListener = new DigitsKeyListener();
        localDigitsKeyListener.mAccepted = new char[paramString.length()];
        paramString.getChars(0, paramString.length(), localDigitsKeyListener.mAccepted, 0);
        return localDigitsKeyListener;
    }

    public static DigitsKeyListener getInstance(boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = 0;
        int j;
        int k;
        if (paramBoolean1)
        {
            j = 1;
            if (paramBoolean2)
                i = 2;
            k = j | i;
            if (sInstance[k] == null)
                break label44;
        }
        for (DigitsKeyListener localDigitsKeyListener = sInstance[k]; ; localDigitsKeyListener = sInstance[k])
        {
            return localDigitsKeyListener;
            j = 0;
            break;
            label44: sInstance[k] = new DigitsKeyListener(paramBoolean1, paramBoolean2);
        }
    }

    public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
    {
        Object localObject = super.filter(paramCharSequence, paramInt1, paramInt2, paramSpanned, paramInt3, paramInt4);
        if ((!this.mSign) && (!this.mDecimal));
        while (true)
        {
            return localObject;
            if (localObject != null)
            {
                paramCharSequence = (CharSequence)localObject;
                paramInt1 = 0;
                paramInt2 = ((CharSequence)localObject).length();
            }
            int i = -1;
            int j = -1;
            int k = paramSpanned.length();
            int m = 0;
            if (m < paramInt3)
            {
                int i5 = paramSpanned.charAt(m);
                if (i5 == 45)
                    i = m;
                while (true)
                {
                    m++;
                    break;
                    if (i5 == 46)
                        j = m;
                }
            }
            for (int n = paramInt4; ; n++)
            {
                if (n >= k)
                    break label172;
                int i4 = paramSpanned.charAt(n);
                if (i4 == 45)
                {
                    localObject = "";
                    break;
                }
                if (i4 == 46)
                    j = n;
            }
            label172: SpannableStringBuilder localSpannableStringBuilder = null;
            for (int i1 = paramInt2 - 1; ; i1--)
            {
                if (i1 < paramInt1)
                    break label321;
                int i2 = paramCharSequence.charAt(i1);
                int i3 = 0;
                if (i2 == 45)
                    if ((i1 != paramInt1) || (paramInt3 != 0))
                        i3 = 1;
                while (true)
                    if (i3 != 0)
                    {
                        if (paramInt2 == paramInt1 + 1)
                        {
                            localObject = "";
                            break;
                            if (i >= 0)
                            {
                                i3 = 1;
                                continue;
                            }
                            i = i1;
                            continue;
                            if (i2 != 46)
                                continue;
                            if (j >= 0)
                            {
                                i3 = 1;
                                continue;
                            }
                            j = i1;
                            continue;
                        }
                        if (localSpannableStringBuilder == null)
                            localSpannableStringBuilder = new SpannableStringBuilder(paramCharSequence, paramInt1, paramInt2);
                        localSpannableStringBuilder.delete(i1 - paramInt1, i1 + 1 - paramInt1);
                    }
            }
            label321: if (localSpannableStringBuilder != null)
                localObject = localSpannableStringBuilder;
            else if (localObject == null)
                localObject = null;
        }
    }

    protected char[] getAcceptedChars()
    {
        return this.mAccepted;
    }

    public int getInputType()
    {
        int i = 2;
        if (this.mSign)
            i |= 4096;
        if (this.mDecimal)
            i |= 8192;
        return i;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.DigitsKeyListener
 * JD-Core Version:        0.6.2
 */