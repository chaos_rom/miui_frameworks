package android.text.method;

import android.graphics.Rect;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.TextView;

public class ArrowKeyMovementMethod extends BaseMovementMethod
    implements MovementMethod
{
    private static final Object LAST_TAP_DOWN = new Object();
    private static ArrowKeyMovementMethod sInstance;

    private static int getCurrentLineTop(Spannable paramSpannable, Layout paramLayout)
    {
        return paramLayout.getLineTop(paramLayout.getLineForOffset(Selection.getSelectionEnd(paramSpannable)));
    }

    public static MovementMethod getInstance()
    {
        if (sInstance == null)
            sInstance = new ArrowKeyMovementMethod();
        return sInstance;
    }

    private static int getPageHeight(TextView paramTextView)
    {
        Rect localRect = new Rect();
        if (paramTextView.getGlobalVisibleRect(localRect));
        for (int i = localRect.height(); ; i = 0)
            return i;
    }

    private static boolean isSelecting(Spannable paramSpannable)
    {
        int i = 1;
        if ((MetaKeyKeyListener.getMetaState(paramSpannable, i) == i) || (MetaKeyKeyListener.getMetaState(paramSpannable, 2048) != 0));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    protected boolean bottom(TextView paramTextView, Spannable paramSpannable)
    {
        if (isSelecting(paramSpannable))
            Selection.extendSelection(paramSpannable, paramSpannable.length());
        while (true)
        {
            return true;
            Selection.setSelection(paramSpannable, paramSpannable.length());
        }
    }

    public boolean canSelectArbitrarily()
    {
        return true;
    }

    protected boolean down(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        if (isSelecting(paramSpannable));
        for (boolean bool = Selection.extendDown(paramSpannable, localLayout); ; bool = Selection.moveDown(paramSpannable, localLayout))
            return bool;
    }

    protected boolean end(TextView paramTextView, Spannable paramSpannable)
    {
        return lineEnd(paramTextView, paramSpannable);
    }

    protected boolean handleMovementKey(TextView paramTextView, Spannable paramSpannable, int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        switch (paramInt1)
        {
        default:
        case 23:
        }
        for (boolean bool = super.handleMovementKey(paramTextView, paramSpannable, paramInt1, paramInt2, paramKeyEvent); ; bool = paramTextView.showContextMenu())
        {
            return bool;
            if ((!KeyEvent.metaStateHasNoModifiers(paramInt2)) || (paramKeyEvent.getAction() != 0) || (paramKeyEvent.getRepeatCount() != 0) || (MetaKeyKeyListener.getMetaState(paramSpannable, 2048) == 0))
                break;
        }
    }

    protected boolean home(TextView paramTextView, Spannable paramSpannable)
    {
        return lineStart(paramTextView, paramSpannable);
    }

    public void initialize(TextView paramTextView, Spannable paramSpannable)
    {
        Selection.setSelection(paramSpannable, 0);
    }

    protected boolean left(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        if (isSelecting(paramSpannable));
        for (boolean bool = Selection.extendLeft(paramSpannable, localLayout); ; bool = Selection.moveLeft(paramSpannable, localLayout))
            return bool;
    }

    protected boolean leftWord(TextView paramTextView, Spannable paramSpannable)
    {
        int i = paramTextView.getSelectionEnd();
        WordIterator localWordIterator = paramTextView.getWordIterator();
        localWordIterator.setCharSequence(paramSpannable, i, i);
        return Selection.moveToPreceding(paramSpannable, localWordIterator, isSelecting(paramSpannable));
    }

    protected boolean lineEnd(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        if (isSelecting(paramSpannable));
        for (boolean bool = Selection.extendToRightEdge(paramSpannable, localLayout); ; bool = Selection.moveToRightEdge(paramSpannable, localLayout))
            return bool;
    }

    protected boolean lineStart(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        if (isSelecting(paramSpannable));
        for (boolean bool = Selection.extendToLeftEdge(paramSpannable, localLayout); ; bool = Selection.moveToLeftEdge(paramSpannable, localLayout))
            return bool;
    }

    public void onTakeFocus(TextView paramTextView, Spannable paramSpannable, int paramInt)
    {
        if ((paramInt & 0x82) != 0)
            if (paramTextView.getLayout() == null)
                Selection.setSelection(paramSpannable, paramSpannable.length());
        while (true)
        {
            return;
            Selection.setSelection(paramSpannable, paramSpannable.length());
        }
    }

    public boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent)
    {
        int i = -1;
        int j = -1;
        int k = paramMotionEvent.getAction();
        if (k == 1)
        {
            i = Touch.getInitialScrollX(paramTextView, paramSpannable);
            j = Touch.getInitialScrollY(paramTextView, paramSpannable);
        }
        boolean bool = Touch.onTouchEvent(paramTextView, paramSpannable, paramMotionEvent);
        if ((paramTextView.isFocused()) && (!paramTextView.didTouchFocusSelect()))
        {
            if (k != 0)
                break label110;
            if (isSelecting(paramSpannable))
            {
                int n = paramTextView.getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY());
                paramSpannable.setSpan(LAST_TAP_DOWN, n, n, 34);
                paramTextView.getParent().requestDisallowInterceptTouchEvent(true);
            }
        }
        while (true)
        {
            return bool;
            label110: if (k == 2)
            {
                if ((isSelecting(paramSpannable)) && (bool))
                {
                    paramTextView.cancelLongPress();
                    Selection.extendSelection(paramSpannable, paramTextView.getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY()));
                    bool = true;
                }
            }
            else if (k == 1)
                if (((j >= 0) && (j != paramTextView.getScrollY())) || ((i >= 0) && (i != paramTextView.getScrollX())))
                {
                    paramTextView.moveCursorToVisibleOffset();
                    bool = true;
                }
                else
                {
                    int m = paramTextView.getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY());
                    if (isSelecting(paramSpannable))
                    {
                        paramSpannable.removeSpan(LAST_TAP_DOWN);
                        Selection.extendSelection(paramSpannable, m);
                    }
                    MetaKeyKeyListener.adjustMetaAfterKeypress(paramSpannable);
                    MetaKeyKeyListener.resetLockedMeta(paramSpannable);
                    bool = true;
                }
        }
    }

    protected boolean pageDown(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        boolean bool1 = isSelecting(paramSpannable);
        int i = getCurrentLineTop(paramSpannable, localLayout) + getPageHeight(paramTextView);
        boolean bool2 = false;
        int j = Selection.getSelectionEnd(paramSpannable);
        if (bool1)
        {
            Selection.extendDown(paramSpannable, localLayout);
            label43: if (Selection.getSelectionEnd(paramSpannable) != j)
                break label64;
        }
        while (true)
        {
            return bool2;
            Selection.moveDown(paramSpannable, localLayout);
            break label43;
            label64: bool2 = true;
            if (getCurrentLineTop(paramSpannable, localLayout) < i)
                break;
        }
    }

    protected boolean pageUp(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        boolean bool1 = isSelecting(paramSpannable);
        int i = getCurrentLineTop(paramSpannable, localLayout) - getPageHeight(paramTextView);
        boolean bool2 = false;
        int j = Selection.getSelectionEnd(paramSpannable);
        if (bool1)
        {
            Selection.extendUp(paramSpannable, localLayout);
            label43: if (Selection.getSelectionEnd(paramSpannable) != j)
                break label64;
        }
        while (true)
        {
            return bool2;
            Selection.moveUp(paramSpannable, localLayout);
            break label43;
            label64: bool2 = true;
            if (getCurrentLineTop(paramSpannable, localLayout) > i)
                break;
        }
    }

    protected boolean right(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        if (isSelecting(paramSpannable));
        for (boolean bool = Selection.extendRight(paramSpannable, localLayout); ; bool = Selection.moveRight(paramSpannable, localLayout))
            return bool;
    }

    protected boolean rightWord(TextView paramTextView, Spannable paramSpannable)
    {
        int i = paramTextView.getSelectionEnd();
        WordIterator localWordIterator = paramTextView.getWordIterator();
        localWordIterator.setCharSequence(paramSpannable, i, i);
        return Selection.moveToFollowing(paramSpannable, localWordIterator, isSelecting(paramSpannable));
    }

    protected boolean top(TextView paramTextView, Spannable paramSpannable)
    {
        if (isSelecting(paramSpannable))
            Selection.extendSelection(paramSpannable, 0);
        while (true)
        {
            return true;
            Selection.setSelection(paramSpannable, 0);
        }
    }

    protected boolean up(TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        if (isSelecting(paramSpannable));
        for (boolean bool = Selection.extendUp(paramSpannable, localLayout); ; bool = Selection.moveUp(paramSpannable, localLayout))
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.ArrowKeyMovementMethod
 * JD-Core Version:        0.6.2
 */