package android.text.method;

import android.text.Layout;
import android.text.NoCopySpan.Concrete;
import android.text.Selection;
import android.text.Spannable;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

public class LinkMovementMethod extends ScrollingMovementMethod
{
    private static final int CLICK = 1;
    private static final int DOWN = 3;
    private static Object FROM_BELOW = new NoCopySpan.Concrete();
    private static final int UP = 2;
    private static LinkMovementMethod sInstance;

    private boolean action(int paramInt, TextView paramTextView, Spannable paramSpannable)
    {
        Layout localLayout = paramTextView.getLayout();
        int i = paramTextView.getTotalPaddingTop() + paramTextView.getTotalPaddingBottom();
        int j = paramTextView.getScrollY();
        int k = j + paramTextView.getHeight() - i;
        int m = localLayout.getLineForVertical(j);
        int n = localLayout.getLineForVertical(k);
        int i1 = localLayout.getLineStart(m);
        int i2 = localLayout.getLineEnd(n);
        ClickableSpan[] arrayOfClickableSpan1 = (ClickableSpan[])paramSpannable.getSpans(i1, i2, ClickableSpan.class);
        int i3 = Selection.getSelectionStart(paramSpannable);
        int i4 = Selection.getSelectionEnd(paramSpannable);
        int i5 = Math.min(i3, i4);
        int i6 = Math.max(i3, i4);
        if ((i5 < 0) && (paramSpannable.getSpanStart(FROM_BELOW) >= 0))
        {
            i6 = paramSpannable.length();
            i5 = i6;
        }
        if (i5 > i2)
        {
            i6 = 2147483647;
            i5 = i6;
        }
        if (i6 < i1)
        {
            i6 = -1;
            i5 = i6;
        }
        boolean bool;
        switch (paramInt)
        {
        default:
            bool = false;
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return bool;
            if (i5 == i6)
            {
                bool = false;
            }
            else
            {
                ClickableSpan[] arrayOfClickableSpan2 = (ClickableSpan[])paramSpannable.getSpans(i5, i6, ClickableSpan.class);
                if (arrayOfClickableSpan2.length != 1)
                {
                    bool = false;
                }
                else
                {
                    arrayOfClickableSpan2[0].onClick(paramTextView);
                    break;
                    int i11 = -1;
                    int i12 = -1;
                    for (int i13 = 0; i13 < arrayOfClickableSpan1.length; i13++)
                    {
                        int i14 = paramSpannable.getSpanEnd(arrayOfClickableSpan1[i13]);
                        if (((i14 < i6) || (i5 == i6)) && (i14 > i12))
                        {
                            i11 = paramSpannable.getSpanStart(arrayOfClickableSpan1[i13]);
                            i12 = i14;
                        }
                    }
                    if (i11 < 0)
                        break;
                    Selection.setSelection(paramSpannable, i12, i11);
                    bool = true;
                    continue;
                    int i7 = 2147483647;
                    int i8 = 2147483647;
                    for (int i9 = 0; i9 < arrayOfClickableSpan1.length; i9++)
                    {
                        int i10 = paramSpannable.getSpanStart(arrayOfClickableSpan1[i9]);
                        if (((i10 > i5) || (i5 == i6)) && (i10 < i7))
                        {
                            i7 = i10;
                            i8 = paramSpannable.getSpanEnd(arrayOfClickableSpan1[i9]);
                        }
                    }
                    if (i8 >= 2147483647)
                        break;
                    Selection.setSelection(paramSpannable, i7, i8);
                    bool = true;
                }
            }
        }
    }

    public static MovementMethod getInstance()
    {
        if (sInstance == null)
            sInstance = new LinkMovementMethod();
        return sInstance;
    }

    protected boolean down(TextView paramTextView, Spannable paramSpannable)
    {
        if (action(3, paramTextView, paramSpannable));
        for (boolean bool = true; ; bool = super.down(paramTextView, paramSpannable))
            return bool;
    }

    protected boolean handleMovementKey(TextView paramTextView, Spannable paramSpannable, int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        int i = 1;
        switch (paramInt1)
        {
        default:
            i = super.handleMovementKey(paramTextView, paramSpannable, paramInt1, paramInt2, paramKeyEvent);
        case 23:
        case 66:
        }
        while (true)
        {
            return i;
            if ((!KeyEvent.metaStateHasNoModifiers(paramInt2)) || (paramKeyEvent.getAction() != 0) || (paramKeyEvent.getRepeatCount() != 0) || (!action(i, paramTextView, paramSpannable)))
                break;
        }
    }

    public void initialize(TextView paramTextView, Spannable paramSpannable)
    {
        Selection.removeSelection(paramSpannable);
        paramSpannable.removeSpan(FROM_BELOW);
    }

    protected boolean left(TextView paramTextView, Spannable paramSpannable)
    {
        if (action(2, paramTextView, paramSpannable));
        for (boolean bool = true; ; bool = super.left(paramTextView, paramSpannable))
            return bool;
    }

    public void onTakeFocus(TextView paramTextView, Spannable paramSpannable, int paramInt)
    {
        Selection.removeSelection(paramSpannable);
        if ((paramInt & 0x1) != 0)
            paramSpannable.setSpan(FROM_BELOW, 0, 0, 34);
        while (true)
        {
            return;
            paramSpannable.removeSpan(FROM_BELOW);
        }
    }

    public boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent)
    {
        int i = 1;
        int j = paramMotionEvent.getAction();
        ClickableSpan[] arrayOfClickableSpan;
        if ((j == i) || (j == 0))
        {
            int k = (int)paramMotionEvent.getX();
            int m = (int)paramMotionEvent.getY();
            int n = k - paramTextView.getTotalPaddingLeft();
            int i1 = m - paramTextView.getTotalPaddingTop();
            int i2 = n + paramTextView.getScrollX();
            int i3 = i1 + paramTextView.getScrollY();
            Layout localLayout = paramTextView.getLayout();
            int i4 = localLayout.getOffsetForHorizontal(localLayout.getLineForVertical(i3), i2);
            arrayOfClickableSpan = (ClickableSpan[])paramSpannable.getSpans(i4, i4, ClickableSpan.class);
            if (arrayOfClickableSpan.length != 0)
                if (j == i)
                    arrayOfClickableSpan[0].onClick(paramTextView);
        }
        while (true)
        {
            return i;
            if (j == 0)
            {
                Selection.setSelection(paramSpannable, paramSpannable.getSpanStart(arrayOfClickableSpan[0]), paramSpannable.getSpanEnd(arrayOfClickableSpan[0]));
                continue;
                Selection.removeSelection(paramSpannable);
                i = super.onTouchEvent(paramTextView, paramSpannable, paramMotionEvent);
            }
        }
    }

    protected boolean right(TextView paramTextView, Spannable paramSpannable)
    {
        if (action(3, paramTextView, paramSpannable));
        for (boolean bool = true; ; bool = super.right(paramTextView, paramSpannable))
            return bool;
    }

    protected boolean up(TextView paramTextView, Spannable paramSpannable)
    {
        if (action(2, paramTextView, paramSpannable));
        for (boolean bool = true; ; bool = super.up(paramTextView, paramSpannable))
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.LinkMovementMethod
 * JD-Core Version:        0.6.2
 */