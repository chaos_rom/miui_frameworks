package android.text.method;

import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.GetChars;
import android.text.NoCopySpan;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.UpdateLayout;
import android.view.View;
import java.lang.ref.WeakReference;

public class PasswordTransformationMethod
    implements TransformationMethod, TextWatcher
{
    private static char DOT = '•';
    private static PasswordTransformationMethod sInstance;

    public static PasswordTransformationMethod getInstance()
    {
        if (sInstance != null);
        for (PasswordTransformationMethod localPasswordTransformationMethod = sInstance; ; localPasswordTransformationMethod = sInstance)
        {
            return localPasswordTransformationMethod;
            sInstance = new PasswordTransformationMethod();
        }
    }

    private static void removeVisibleSpans(Spannable paramSpannable)
    {
        Visible[] arrayOfVisible = (Visible[])paramSpannable.getSpans(0, paramSpannable.length(), Visible.class);
        for (int i = 0; i < arrayOfVisible.length; i++)
            paramSpannable.removeSpan(arrayOfVisible[i]);
    }

    public void afterTextChanged(Editable paramEditable)
    {
    }

    public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public CharSequence getTransformation(CharSequence paramCharSequence, View paramView)
    {
        if ((paramCharSequence instanceof Spannable))
        {
            Spannable localSpannable = (Spannable)paramCharSequence;
            ViewReference[] arrayOfViewReference = (ViewReference[])localSpannable.getSpans(0, localSpannable.length(), ViewReference.class);
            for (int i = 0; i < arrayOfViewReference.length; i++)
                localSpannable.removeSpan(arrayOfViewReference[i]);
            removeVisibleSpans(localSpannable);
            localSpannable.setSpan(new ViewReference(paramView), 0, 0, 34);
        }
        return new PasswordCharSequence(paramCharSequence);
    }

    public void onFocusChanged(View paramView, CharSequence paramCharSequence, boolean paramBoolean, int paramInt, Rect paramRect)
    {
        if ((!paramBoolean) && ((paramCharSequence instanceof Spannable)))
            removeVisibleSpans((Spannable)paramCharSequence);
    }

    public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        Spannable localSpannable;
        ViewReference[] arrayOfViewReference;
        if ((paramCharSequence instanceof Spannable))
        {
            localSpannable = (Spannable)paramCharSequence;
            arrayOfViewReference = (ViewReference[])localSpannable.getSpans(0, paramCharSequence.length(), ViewReference.class);
            if (arrayOfViewReference.length != 0)
                break label41;
        }
        while (true)
        {
            return;
            label41: View localView = null;
            for (int i = 0; (localView == null) && (i < arrayOfViewReference.length); i++)
                localView = (View)arrayOfViewReference[i].get();
            if ((localView != null) && ((0x8 & TextKeyListener.getInstance().getPrefs(localView.getContext())) != 0) && (paramInt3 > 0))
            {
                removeVisibleSpans(localSpannable);
                if (paramInt3 == 1)
                    localSpannable.setSpan(new Visible(localSpannable, this), paramInt1, paramInt1 + paramInt3, 33);
            }
        }
    }

    private static class ViewReference extends WeakReference<View>
        implements NoCopySpan
    {
        public ViewReference(View paramView)
        {
            super();
        }
    }

    private static class Visible extends Handler
        implements UpdateLayout, Runnable
    {
        private Spannable mText;
        private PasswordTransformationMethod mTransformer;

        public Visible(Spannable paramSpannable, PasswordTransformationMethod paramPasswordTransformationMethod)
        {
            this.mText = paramSpannable;
            this.mTransformer = paramPasswordTransformationMethod;
            postAtTime(this, 1500L + SystemClock.uptimeMillis());
        }

        public void run()
        {
            this.mText.removeSpan(this);
        }
    }

    private static class PasswordCharSequence
        implements CharSequence, GetChars
    {
        private CharSequence mSource;

        public PasswordCharSequence(CharSequence paramCharSequence)
        {
            this.mSource = paramCharSequence;
        }

        public char charAt(int paramInt)
        {
            Spanned localSpanned;
            char c;
            if ((this.mSource instanceof Spanned))
            {
                localSpanned = (Spanned)this.mSource;
                int i = localSpanned.getSpanStart(TextKeyListener.ACTIVE);
                int j = localSpanned.getSpanEnd(TextKeyListener.ACTIVE);
                if ((paramInt >= i) && (paramInt < j))
                    c = this.mSource.charAt(paramInt);
            }
            while (true)
            {
                return c;
                PasswordTransformationMethod.Visible[] arrayOfVisible = (PasswordTransformationMethod.Visible[])localSpanned.getSpans(0, localSpanned.length(), PasswordTransformationMethod.Visible.class);
                for (int k = 0; ; k++)
                {
                    if (k >= arrayOfVisible.length)
                        break label171;
                    if (localSpanned.getSpanStart(arrayOfVisible[k].mTransformer) >= 0)
                    {
                        int m = localSpanned.getSpanStart(arrayOfVisible[k]);
                        int n = localSpanned.getSpanEnd(arrayOfVisible[k]);
                        if ((paramInt >= m) && (paramInt < n))
                        {
                            c = this.mSource.charAt(paramInt);
                            break;
                        }
                    }
                }
                label171: c = PasswordTransformationMethod.DOT;
            }
        }

        public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
        {
            TextUtils.getChars(this.mSource, paramInt1, paramInt2, paramArrayOfChar, paramInt3);
            int i = -1;
            int j = -1;
            int k = 0;
            int[] arrayOfInt1 = null;
            int[] arrayOfInt2 = null;
            if ((this.mSource instanceof Spanned))
            {
                Spanned localSpanned = (Spanned)this.mSource;
                i = localSpanned.getSpanStart(TextKeyListener.ACTIVE);
                j = localSpanned.getSpanEnd(TextKeyListener.ACTIVE);
                PasswordTransformationMethod.Visible[] arrayOfVisible = (PasswordTransformationMethod.Visible[])localSpanned.getSpans(0, localSpanned.length(), PasswordTransformationMethod.Visible.class);
                k = arrayOfVisible.length;
                arrayOfInt1 = new int[k];
                arrayOfInt2 = new int[k];
                for (int i2 = 0; i2 < k; i2++)
                    if (localSpanned.getSpanStart(arrayOfVisible[i2].mTransformer) >= 0)
                    {
                        arrayOfInt1[i2] = localSpanned.getSpanStart(arrayOfVisible[i2]);
                        arrayOfInt2[i2] = localSpanned.getSpanEnd(arrayOfVisible[i2]);
                    }
            }
            int m = paramInt1;
            if (m < paramInt2)
            {
                int n;
                if ((m < i) || (m >= j))
                    n = 0;
                for (int i1 = 0; ; i1++)
                    if (i1 < k)
                    {
                        if ((m >= arrayOfInt1[i1]) && (m < arrayOfInt2[i1]))
                            n = 1;
                    }
                    else
                    {
                        if (n == 0)
                            paramArrayOfChar[(paramInt3 + (m - paramInt1))] = PasswordTransformationMethod.DOT;
                        m++;
                        break;
                    }
            }
        }

        public int length()
        {
            return this.mSource.length();
        }

        public CharSequence subSequence(int paramInt1, int paramInt2)
        {
            char[] arrayOfChar = new char[paramInt2 - paramInt1];
            getChars(paramInt1, paramInt2, arrayOfChar, 0);
            return new String(arrayOfChar);
        }

        public String toString()
        {
            return subSequence(0, length()).toString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.PasswordTransformationMethod
 * JD-Core Version:        0.6.2
 */