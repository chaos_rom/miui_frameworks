package android.text.method;

public class SingleLineTransformationMethod extends ReplacementTransformationMethod
{
    private static char[] ORIGINAL;
    private static char[] REPLACEMENT = arrayOfChar2;
    private static SingleLineTransformationMethod sInstance;

    static
    {
        char[] arrayOfChar1 = new char[2];
        arrayOfChar1[0] = 10;
        arrayOfChar1[1] = 13;
        ORIGINAL = arrayOfChar1;
        char[] arrayOfChar2 = new char[2];
        arrayOfChar2[0] = 32;
        arrayOfChar2[1] = -257;
    }

    public static SingleLineTransformationMethod getInstance()
    {
        if (sInstance != null);
        for (SingleLineTransformationMethod localSingleLineTransformationMethod = sInstance; ; localSingleLineTransformationMethod = sInstance)
        {
            return localSingleLineTransformationMethod;
            sInstance = new SingleLineTransformationMethod();
        }
    }

    protected char[] getOriginal()
    {
        return ORIGINAL;
    }

    protected char[] getReplacement()
    {
        return REPLACEMENT;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.method.SingleLineTransformationMethod
 * JD-Core Version:        0.6.2
 */