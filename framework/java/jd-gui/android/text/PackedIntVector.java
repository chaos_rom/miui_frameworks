package android.text;

import com.android.internal.util.ArrayUtils;

class PackedIntVector
{
    private final int mColumns;
    private int mRowGapLength;
    private int mRowGapStart;
    private int mRows;
    private int[] mValueGap;
    private int[] mValues;

    public PackedIntVector(int paramInt)
    {
        this.mColumns = paramInt;
        this.mRows = 0;
        this.mRowGapStart = 0;
        this.mRowGapLength = this.mRows;
        this.mValues = null;
        this.mValueGap = new int[paramInt * 2];
    }

    private final void growBuffer()
    {
        int i = this.mColumns;
        int j = ArrayUtils.idealIntArraySize(i * (1 + size())) / i;
        int[] arrayOfInt1 = new int[j * i];
        int[] arrayOfInt2 = this.mValueGap;
        int k = this.mRowGapStart;
        int m = this.mRows - (k + this.mRowGapLength);
        if (this.mValues != null)
        {
            System.arraycopy(this.mValues, 0, arrayOfInt1, 0, i * k);
            System.arraycopy(this.mValues, i * (this.mRows - m), arrayOfInt1, i * (j - m), m * i);
        }
        for (int n = 0; n < i; n++)
            if (arrayOfInt2[n] >= k)
            {
                arrayOfInt2[n] += j - this.mRows;
                if (arrayOfInt2[n] < k)
                    arrayOfInt2[n] = k;
            }
        this.mRowGapLength += j - this.mRows;
        this.mRows = j;
        this.mValues = arrayOfInt1;
    }

    private final void moveRowGapTo(int paramInt)
    {
        if (paramInt == this.mRowGapStart);
        while (true)
        {
            return;
            if (paramInt > this.mRowGapStart)
            {
                int i3 = paramInt + this.mRowGapLength - (this.mRowGapStart + this.mRowGapLength);
                int i4 = this.mColumns;
                int[] arrayOfInt3 = this.mValueGap;
                int[] arrayOfInt4 = this.mValues;
                int i5 = this.mRowGapStart + this.mRowGapLength;
                for (int i6 = i5; i6 < i5 + i3; i6++)
                {
                    int i7 = i6 - i5 + this.mRowGapStart;
                    for (int i8 = 0; i8 < i4; i8++)
                    {
                        int i9 = arrayOfInt4[(i8 + i6 * i4)];
                        if (i6 >= arrayOfInt3[i8])
                            i9 += arrayOfInt3[(i8 + i4)];
                        if (i7 >= arrayOfInt3[i8])
                            i9 -= arrayOfInt3[(i8 + i4)];
                        arrayOfInt4[(i8 + i7 * i4)] = i9;
                    }
                }
            }
            int i = this.mRowGapStart - paramInt;
            int j = this.mColumns;
            int[] arrayOfInt1 = this.mValueGap;
            int[] arrayOfInt2 = this.mValues;
            int k = this.mRowGapStart + this.mRowGapLength;
            for (int m = -1 + (paramInt + i); m >= paramInt; m--)
            {
                int n = k + (m - paramInt) - i;
                for (int i1 = 0; i1 < j; i1++)
                {
                    int i2 = arrayOfInt2[(i1 + m * j)];
                    if (m >= arrayOfInt1[i1])
                        i2 += arrayOfInt1[(i1 + j)];
                    if (n >= arrayOfInt1[i1])
                        i2 -= arrayOfInt1[(i1 + j)];
                    arrayOfInt2[(i1 + n * j)] = i2;
                }
            }
            this.mRowGapStart = paramInt;
        }
    }

    private final void moveValueGapTo(int paramInt1, int paramInt2)
    {
        int[] arrayOfInt1 = this.mValueGap;
        int[] arrayOfInt2 = this.mValues;
        int i = this.mColumns;
        if (paramInt2 == arrayOfInt1[paramInt1]);
        while (true)
        {
            return;
            if (paramInt2 > arrayOfInt1[paramInt1])
                for (int m = arrayOfInt1[paramInt1]; m < paramInt2; m++)
                {
                    int n = paramInt1 + m * i;
                    arrayOfInt2[n] += arrayOfInt1[(paramInt1 + i)];
                }
            for (int j = paramInt2; j < arrayOfInt1[paramInt1]; j++)
            {
                int k = paramInt1 + j * i;
                arrayOfInt2[k] -= arrayOfInt1[(paramInt1 + i)];
            }
            arrayOfInt1[paramInt1] = paramInt2;
        }
    }

    private void setValueInternal(int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt1 >= this.mRowGapStart)
            paramInt1 += this.mRowGapLength;
        int[] arrayOfInt = this.mValueGap;
        if (paramInt1 >= arrayOfInt[paramInt2])
            paramInt3 -= arrayOfInt[(paramInt2 + this.mColumns)];
        this.mValues[(paramInt2 + paramInt1 * this.mColumns)] = paramInt3;
    }

    public void adjustValuesBelow(int paramInt1, int paramInt2, int paramInt3)
    {
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 > size()) || (paramInt2 >= width()))
            throw new IndexOutOfBoundsException(paramInt1 + ", " + paramInt2);
        if (paramInt1 >= this.mRowGapStart)
            paramInt1 += this.mRowGapLength;
        moveValueGapTo(paramInt2, paramInt1);
        int[] arrayOfInt = this.mValueGap;
        int i = paramInt2 + this.mColumns;
        arrayOfInt[i] = (paramInt3 + arrayOfInt[i]);
    }

    public void deleteAt(int paramInt1, int paramInt2)
    {
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 + paramInt2 > size()))
            throw new IndexOutOfBoundsException(paramInt1 + ", " + paramInt2);
        moveRowGapTo(paramInt1 + paramInt2);
        this.mRowGapStart -= paramInt2;
        this.mRowGapLength = (paramInt2 + this.mRowGapLength);
    }

    public int getValue(int paramInt1, int paramInt2)
    {
        int i = this.mColumns;
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 >= size()) || (paramInt2 >= i))
            throw new IndexOutOfBoundsException(paramInt1 + ", " + paramInt2);
        if (paramInt1 >= this.mRowGapStart)
            paramInt1 += this.mRowGapLength;
        int j = this.mValues[(paramInt2 + paramInt1 * i)];
        int[] arrayOfInt = this.mValueGap;
        if (paramInt1 >= arrayOfInt[paramInt2])
            j += arrayOfInt[(paramInt2 + i)];
        return j;
    }

    public void insertAt(int paramInt, int[] paramArrayOfInt)
    {
        if ((paramInt < 0) || (paramInt > size()))
            throw new IndexOutOfBoundsException("row " + paramInt);
        if ((paramArrayOfInt != null) && (paramArrayOfInt.length < width()))
            throw new IndexOutOfBoundsException("value count " + paramArrayOfInt.length);
        moveRowGapTo(paramInt);
        if (this.mRowGapLength == 0)
            growBuffer();
        this.mRowGapStart = (1 + this.mRowGapStart);
        this.mRowGapLength = (-1 + this.mRowGapLength);
        if (paramArrayOfInt == null)
            for (int j = -1 + this.mColumns; j >= 0; j--)
                setValueInternal(paramInt, j, 0);
        for (int i = -1 + this.mColumns; i >= 0; i--)
            setValueInternal(paramInt, i, paramArrayOfInt[i]);
    }

    public void setValue(int paramInt1, int paramInt2, int paramInt3)
    {
        if (((paramInt1 | paramInt2) < 0) || (paramInt1 >= size()) || (paramInt2 >= this.mColumns))
            throw new IndexOutOfBoundsException(paramInt1 + ", " + paramInt2);
        if (paramInt1 >= this.mRowGapStart)
            paramInt1 += this.mRowGapLength;
        int[] arrayOfInt = this.mValueGap;
        if (paramInt1 >= arrayOfInt[paramInt2])
            paramInt3 -= arrayOfInt[(paramInt2 + this.mColumns)];
        this.mValues[(paramInt2 + paramInt1 * this.mColumns)] = paramInt3;
    }

    public int size()
    {
        return this.mRows - this.mRowGapLength;
    }

    public int width()
    {
        return this.mColumns;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.PackedIntVector
 * JD-Core Version:        0.6.2
 */