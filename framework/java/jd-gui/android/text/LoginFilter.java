package android.text;

public abstract class LoginFilter
    implements InputFilter
{
    private boolean mAppendInvalid;

    LoginFilter()
    {
        this.mAppendInvalid = false;
    }

    LoginFilter(boolean paramBoolean)
    {
        this.mAppendInvalid = paramBoolean;
    }

    public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
    {
        onStart();
        for (int i = 0; i < paramInt3; i++)
        {
            char c3 = paramSpanned.charAt(i);
            if (!isAllowed(c3))
                onInvalidCharacter(c3);
        }
        SpannableStringBuilder localSpannableStringBuilder = null;
        int j = 0;
        int k = paramInt1;
        while (k < paramInt2)
        {
            char c2 = paramCharSequence.charAt(k);
            if (isAllowed(c2))
            {
                j++;
                k++;
            }
            else
            {
                if (this.mAppendInvalid)
                    j++;
                while (true)
                {
                    onInvalidCharacter(c2);
                    break;
                    if (localSpannableStringBuilder == null)
                    {
                        localSpannableStringBuilder = new SpannableStringBuilder(paramCharSequence, paramInt1, paramInt2);
                        j = k - paramInt1;
                    }
                    localSpannableStringBuilder.delete(j, j + 1);
                }
            }
        }
        for (int m = paramInt4; m < paramSpanned.length(); m++)
        {
            char c1 = paramSpanned.charAt(m);
            if (!isAllowed(c1))
                onInvalidCharacter(c1);
        }
        onStop();
        return localSpannableStringBuilder;
    }

    public abstract boolean isAllowed(char paramChar);

    public void onInvalidCharacter(char paramChar)
    {
    }

    public void onStart()
    {
    }

    public void onStop()
    {
    }

    public static class PasswordFilterGMail extends LoginFilter
    {
        public PasswordFilterGMail()
        {
            super();
        }

        public PasswordFilterGMail(boolean paramBoolean)
        {
            super();
        }

        public boolean isAllowed(char paramChar)
        {
            boolean bool = true;
            if ((' ' <= paramChar) && (paramChar <= ''));
            while (true)
            {
                return bool;
                if ((' ' > paramChar) || (paramChar > 'ÿ'))
                    bool = false;
            }
        }
    }

    public static class UsernameFilterGeneric extends LoginFilter
    {
        private static final String mAllowed = "@_-+.";

        public UsernameFilterGeneric()
        {
            super();
        }

        public UsernameFilterGeneric(boolean paramBoolean)
        {
            super();
        }

        public boolean isAllowed(char paramChar)
        {
            boolean bool = true;
            if (('0' <= paramChar) && (paramChar <= '9'));
            while (true)
            {
                return bool;
                if ((('a' > paramChar) || (paramChar > 'z')) && (('A' > paramChar) || (paramChar > 'Z')) && ("@_-+.".indexOf(paramChar) == -1))
                    bool = false;
            }
        }
    }

    public static class UsernameFilterGMail extends LoginFilter
    {
        public UsernameFilterGMail()
        {
            super();
        }

        public UsernameFilterGMail(boolean paramBoolean)
        {
            super();
        }

        public boolean isAllowed(char paramChar)
        {
            boolean bool = true;
            if (('0' <= paramChar) && (paramChar <= '9'));
            while (true)
            {
                return bool;
                if ((('a' > paramChar) || (paramChar > 'z')) && (('A' > paramChar) || (paramChar > 'Z')) && ('.' != paramChar))
                    bool = false;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.LoginFilter
 * JD-Core Version:        0.6.2
 */