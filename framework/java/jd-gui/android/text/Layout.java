package android.text;

import android.emoji.EmojiFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.text.method.TextKeyListener;
import android.text.style.AlignmentSpan;
import android.text.style.LeadingMarginSpan;
import android.text.style.LeadingMarginSpan.LeadingMarginSpan2;
import android.text.style.LineBackgroundSpan;
import android.text.style.ParagraphStyle;
import android.text.style.ReplacementSpan;
import android.text.style.TabStopSpan;
import com.android.internal.util.ArrayUtils;
import java.util.Arrays;

public abstract class Layout
{
    static final Directions DIRS_ALL_LEFT_TO_RIGHT;
    static final Directions DIRS_ALL_RIGHT_TO_LEFT;
    public static final int DIR_LEFT_TO_RIGHT = 1;
    static final int DIR_REQUEST_DEFAULT_LTR = 2;
    static final int DIR_REQUEST_DEFAULT_RTL = -2;
    static final int DIR_REQUEST_LTR = 1;
    static final int DIR_REQUEST_RTL = -1;
    public static final int DIR_RIGHT_TO_LEFT = -1;
    static final char[] ELLIPSIS_NORMAL;
    static final char[] ELLIPSIS_TWO_DOTS;
    static final EmojiFactory EMOJI_FACTORY;
    static final int MAX_EMOJI = 0;
    static final int MIN_EMOJI = 0;
    private static final ParagraphStyle[] NO_PARA_SPANS = (ParagraphStyle[])ArrayUtils.emptyArray(ParagraphStyle.class);
    static final int RUN_LENGTH_MASK = 67108863;
    static final int RUN_LEVEL_MASK = 63;
    static final int RUN_LEVEL_SHIFT = 26;
    static final int RUN_RTL_FLAG = 67108864;
    private static final int TAB_INCREMENT = 20;
    private static final Rect sTempRect;
    private Alignment mAlignment = Alignment.ALIGN_NORMAL;
    private SpanSet<LineBackgroundSpan> mLineBackgroundSpans;
    private TextPaint mPaint;
    private float mSpacingAdd;
    private float mSpacingMult;
    private boolean mSpannedText;
    private CharSequence mText;
    private TextDirectionHeuristic mTextDir;
    private int mWidth;
    TextPaint mWorkPaint;

    static
    {
        EMOJI_FACTORY = EmojiFactory.newAvailableInstance();
        if (EMOJI_FACTORY != null)
            MIN_EMOJI = EMOJI_FACTORY.getMinimumAndroidPua();
        for (MAX_EMOJI = EMOJI_FACTORY.getMaximumAndroidPua(); ; MAX_EMOJI = -1)
        {
            sTempRect = new Rect();
            int[] arrayOfInt1 = new int[2];
            arrayOfInt1[0] = 0;
            arrayOfInt1[1] = 67108863;
            DIRS_ALL_LEFT_TO_RIGHT = new Directions(arrayOfInt1);
            int[] arrayOfInt2 = new int[2];
            arrayOfInt2[0] = 0;
            arrayOfInt2[1] = 134217727;
            DIRS_ALL_RIGHT_TO_LEFT = new Directions(arrayOfInt2);
            char[] arrayOfChar1 = new char[1];
            arrayOfChar1[0] = '…';
            ELLIPSIS_NORMAL = arrayOfChar1;
            char[] arrayOfChar2 = new char[1];
            arrayOfChar2[0] = '‥';
            ELLIPSIS_TWO_DOTS = arrayOfChar2;
            return;
            MIN_EMOJI = -1;
        }
    }

    protected Layout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Alignment paramAlignment, float paramFloat1, float paramFloat2)
    {
        this(paramCharSequence, paramTextPaint, paramInt, paramAlignment, TextDirectionHeuristics.FIRSTSTRONG_LTR, paramFloat1, paramFloat2);
    }

    protected Layout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Alignment paramAlignment, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2)
    {
        if (paramInt < 0)
            throw new IllegalArgumentException("Layout: " + paramInt + " < 0");
        if (paramTextPaint != null)
        {
            paramTextPaint.bgColor = 0;
            paramTextPaint.baselineShift = 0;
        }
        this.mText = paramCharSequence;
        this.mPaint = paramTextPaint;
        this.mWorkPaint = new TextPaint();
        this.mWidth = paramInt;
        this.mAlignment = paramAlignment;
        this.mSpacingMult = paramFloat1;
        this.mSpacingAdd = paramFloat2;
        this.mSpannedText = (paramCharSequence instanceof Spanned);
        this.mTextDir = paramTextDirectionHeuristic;
    }

    private void addSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Path paramPath)
    {
        int i = getLineStart(paramInt1);
        int j = getLineEnd(paramInt1);
        Directions localDirections = getLineDirections(paramInt1);
        if ((j > i) && (this.mText.charAt(j - 1) == '\n'))
            j--;
        for (int k = 0; k < localDirections.mDirections.length; k += 2)
        {
            int m = i + localDirections.mDirections[k];
            int n = m + (0x3FFFFFF & localDirections.mDirections[(k + 1)]);
            if (n > j)
                n = j;
            if ((paramInt2 <= n) && (paramInt3 >= m))
            {
                int i1 = Math.max(paramInt2, m);
                int i2 = Math.min(paramInt3, n);
                if (i1 != i2)
                {
                    float f1 = getHorizontal(i1, false, paramInt1);
                    float f2 = getHorizontal(i2, true, paramInt1);
                    float f3 = Math.min(f1, f2);
                    float f4 = Math.max(f1, f2);
                    paramPath.addRect(f3, paramInt4, f4, paramInt5, Path.Direction.CW);
                }
            }
        }
    }

    private void ellipsize(int paramInt1, int paramInt2, int paramInt3, char[] paramArrayOfChar, int paramInt4, TextUtils.TruncateAt paramTruncateAt)
    {
        int i = getEllipsisCount(paramInt3);
        if (i == 0)
            return;
        int j = getEllipsisStart(paramInt3);
        int k = getLineStart(paramInt3);
        int m = j;
        label31: if (m < j + i)
            if (m != j)
                break label93;
        label93: for (int n = getEllipsisChar(paramTruncateAt); ; n = 65279)
        {
            int i1 = m + k;
            if ((i1 >= paramInt1) && (i1 < paramInt2))
                paramArrayOfChar[(paramInt4 + i1 - paramInt1)] = n;
            m++;
            break label31;
            break;
        }
    }

    public static float getDesiredWidth(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint)
    {
        float f1 = 0.0F;
        int j;
        for (int i = paramInt1; i <= paramInt2; i = j + 1)
        {
            j = TextUtils.indexOf(paramCharSequence, '\n', i, paramInt2);
            if (j < 0)
                j = paramInt2;
            float f2 = measurePara(paramTextPaint, paramCharSequence, i, j);
            if (f2 > f1)
                f1 = f2;
        }
        return f1;
    }

    public static float getDesiredWidth(CharSequence paramCharSequence, TextPaint paramTextPaint)
    {
        return getDesiredWidth(paramCharSequence, 0, paramCharSequence.length(), paramTextPaint);
    }

    private char getEllipsisChar(TextUtils.TruncateAt paramTruncateAt)
    {
        if (paramTruncateAt == TextUtils.TruncateAt.END_SMALL);
        for (char c = ELLIPSIS_TWO_DOTS[0]; ; c = ELLIPSIS_NORMAL[0])
            return c;
    }

    private float getHorizontal(int paramInt, boolean paramBoolean)
    {
        return getHorizontal(paramInt, paramBoolean, getLineForOffset(paramInt));
    }

    private float getHorizontal(int paramInt1, boolean paramBoolean, int paramInt2)
    {
        int i = getLineStart(paramInt2);
        int j = getLineEnd(paramInt2);
        int k = getParagraphDirection(paramInt2);
        boolean bool = getLineContainsTab(paramInt2);
        Directions localDirections = getLineDirections(paramInt2);
        TabStops localTabStops = null;
        if ((bool) && ((this.mText instanceof Spanned)))
        {
            TabStopSpan[] arrayOfTabStopSpan = (TabStopSpan[])getParagraphSpans((Spanned)this.mText, i, j, TabStopSpan.class);
            if (arrayOfTabStopSpan.length > 0)
                localTabStops = new TabStops(20, arrayOfTabStopSpan);
        }
        TextLine localTextLine = TextLine.obtain();
        localTextLine.set(this.mPaint, this.mText, i, j, k, localDirections, bool, localTabStops);
        float f = localTextLine.measure(paramInt1 - i, paramBoolean, null);
        TextLine.recycle(localTextLine);
        return f + getLineStartPos(paramInt2, getParagraphLeft(paramInt2), getParagraphRight(paramInt2));
    }

    private float getLineExtent(int paramInt, TabStops paramTabStops, boolean paramBoolean)
    {
        int i = getLineStart(paramInt);
        if (paramBoolean);
        for (int j = getLineEnd(paramInt); ; j = getLineVisibleEnd(paramInt))
        {
            boolean bool = getLineContainsTab(paramInt);
            Directions localDirections = getLineDirections(paramInt);
            int k = getParagraphDirection(paramInt);
            TextLine localTextLine = TextLine.obtain();
            localTextLine.set(this.mPaint, this.mText, i, j, k, localDirections, bool, paramTabStops);
            float f = localTextLine.metrics(null);
            TextLine.recycle(localTextLine);
            return f;
        }
    }

    private float getLineExtent(int paramInt, boolean paramBoolean)
    {
        int i = getLineStart(paramInt);
        int j;
        boolean bool;
        TabStops localTabStops;
        Directions localDirections;
        float f;
        if (paramBoolean)
        {
            j = getLineEnd(paramInt);
            bool = getLineContainsTab(paramInt);
            localTabStops = null;
            if ((bool) && ((this.mText instanceof Spanned)))
            {
                TabStopSpan[] arrayOfTabStopSpan = (TabStopSpan[])getParagraphSpans((Spanned)this.mText, i, j, TabStopSpan.class);
                if (arrayOfTabStopSpan.length > 0)
                    localTabStops = new TabStops(20, arrayOfTabStopSpan);
            }
            localDirections = getLineDirections(paramInt);
            if (localDirections != null)
                break label110;
            f = 0.0F;
        }
        while (true)
        {
            return f;
            j = getLineVisibleEnd(paramInt);
            break;
            label110: int k = getParagraphDirection(paramInt);
            TextLine localTextLine = TextLine.obtain();
            localTextLine.set(this.mPaint, this.mText, i, j, k, localDirections, bool, localTabStops);
            f = localTextLine.metrics(null);
            TextLine.recycle(localTextLine);
        }
    }

    private int getLineStartPos(int paramInt1, int paramInt2, int paramInt3)
    {
        Alignment localAlignment = getParagraphAlignment(paramInt1);
        int i = getParagraphDirection(paramInt1);
        int m;
        if (localAlignment == Alignment.ALIGN_LEFT)
            m = paramInt2;
        while (true)
        {
            return m;
            if (localAlignment == Alignment.ALIGN_NORMAL)
            {
                if (i == 1)
                    m = paramInt2;
                else
                    m = paramInt3;
            }
            else
            {
                TabStops localTabStops = null;
                if ((this.mSpannedText) && (getLineContainsTab(paramInt1)))
                {
                    Spanned localSpanned = (Spanned)this.mText;
                    int n = getLineStart(paramInt1);
                    TabStopSpan[] arrayOfTabStopSpan = (TabStopSpan[])getParagraphSpans(localSpanned, n, localSpanned.nextSpanTransition(n, localSpanned.length(), TabStopSpan.class), TabStopSpan.class);
                    if (arrayOfTabStopSpan.length > 0)
                        localTabStops = new TabStops(20, arrayOfTabStopSpan);
                }
                int j = (int)getLineExtent(paramInt1, localTabStops, false);
                if (localAlignment == Alignment.ALIGN_RIGHT)
                {
                    m = paramInt3 - j;
                }
                else if (localAlignment == Alignment.ALIGN_OPPOSITE)
                {
                    if (i == 1)
                        m = paramInt3 - j;
                    else
                        m = paramInt2 - j;
                }
                else
                {
                    int k = j & 0xFFFFFFFE;
                    m = paramInt2 + paramInt3 - k >> 1;
                }
            }
        }
    }

    private int getLineVisibleEnd(int paramInt1, int paramInt2, int paramInt3)
    {
        CharSequence localCharSequence = this.mText;
        int i;
        if (paramInt1 == -1 + getLineCount())
            i = paramInt3;
        while (true)
        {
            return i;
            int j;
            do
            {
                paramInt3--;
                if (paramInt3 <= paramInt2)
                    break label72;
                j = localCharSequence.charAt(paramInt3 - 1);
                if (j == 10)
                {
                    i = paramInt3 - 1;
                    break;
                }
            }
            while ((j == 32) || (j == 9));
            label72: i = paramInt3;
        }
    }

    private int getOffsetAtStartOf(int paramInt)
    {
        if (paramInt == 0);
        for (int j = 0; ; j = paramInt)
        {
            return j;
            CharSequence localCharSequence = this.mText;
            int i = localCharSequence.charAt(paramInt);
            if ((i >= 56320) && (i <= 57343))
            {
                int i1 = localCharSequence.charAt(paramInt - 1);
                if ((i1 >= 55296) && (i1 <= 56319))
                    paramInt--;
            }
            if (this.mSpannedText)
            {
                ReplacementSpan[] arrayOfReplacementSpan = (ReplacementSpan[])((Spanned)localCharSequence).getSpans(paramInt, paramInt, ReplacementSpan.class);
                for (int k = 0; k < arrayOfReplacementSpan.length; k++)
                {
                    int m = ((Spanned)localCharSequence).getSpanStart(arrayOfReplacementSpan[k]);
                    int n = ((Spanned)localCharSequence).getSpanEnd(arrayOfReplacementSpan[k]);
                    if ((m < paramInt) && (n > paramInt))
                        paramInt = m;
                }
            }
        }
    }

    private int getOffsetToLeftRightOf(int paramInt, boolean paramBoolean)
    {
        int i = getLineForOffset(paramInt);
        int j = getLineStart(i);
        int k = getLineEnd(i);
        int m = getParagraphDirection(i);
        int n = 0;
        boolean bool;
        int i1;
        label49: label77: int i4;
        if (m == -1)
        {
            bool = true;
            if (paramBoolean != bool)
                break label189;
            i1 = 1;
            if (i1 == 0)
                break label201;
            if (paramInt == k)
            {
                if (i >= -1 + getLineCount())
                    break label195;
                n = 1;
                i++;
            }
            if (n != 0)
            {
                j = getLineStart(i);
                k = getLineEnd(i);
                i4 = getParagraphDirection(i);
                if (i4 != m)
                    if (paramBoolean)
                        break label226;
            }
        }
        label189: label195: label201: label226: for (paramBoolean = true; ; paramBoolean = false)
        {
            m = i4;
            Directions localDirections = getLineDirections(i);
            TextLine localTextLine = TextLine.obtain();
            localTextLine.set(this.mPaint, this.mText, j, k, m, localDirections, false, null);
            int i3 = j + localTextLine.getOffsetToLeftRightOf(paramInt - j, paramBoolean);
            TextLine.recycle(localTextLine);
            int i2 = i3;
            while (true)
            {
                return i2;
                bool = false;
                break;
                i1 = 0;
                break label49;
                i2 = paramInt;
                continue;
                if (paramInt != j)
                    break label77;
                if (i > 0)
                {
                    n = 1;
                    i--;
                    break label77;
                }
                i2 = paramInt;
            }
        }
    }

    private int getParagraphLeadingMargin(int paramInt)
    {
        if (!this.mSpannedText);
        Spanned localSpanned;
        int i;
        LeadingMarginSpan[] arrayOfLeadingMarginSpan;
        for (int j = 0; ; j = 0)
        {
            return j;
            localSpanned = (Spanned)this.mText;
            i = getLineStart(paramInt);
            arrayOfLeadingMarginSpan = (LeadingMarginSpan[])getParagraphSpans(localSpanned, i, localSpanned.nextSpanTransition(i, getLineEnd(paramInt), LeadingMarginSpan.class), LeadingMarginSpan.class);
            if (arrayOfLeadingMarginSpan.length != 0)
                break;
        }
        j = 0;
        boolean bool1;
        label91: int k;
        label94: LeadingMarginSpan localLeadingMarginSpan;
        if ((i == 0) || (localSpanned.charAt(i - 1) == '\n'))
        {
            bool1 = true;
            k = 0;
            if (k < arrayOfLeadingMarginSpan.length)
            {
                localLeadingMarginSpan = arrayOfLeadingMarginSpan[k];
                bool2 = bool1;
                if ((localLeadingMarginSpan instanceof LeadingMarginSpan.LeadingMarginSpan2))
                    if (paramInt >= getLineForOffset(localSpanned.getSpanStart(localLeadingMarginSpan)) + ((LeadingMarginSpan.LeadingMarginSpan2)localLeadingMarginSpan).getLeadingMarginLineCount())
                        break label177;
            }
        }
        label177: for (boolean bool2 = true; ; bool2 = false)
        {
            j += localLeadingMarginSpan.getLeadingMargin(bool2);
            k++;
            break label94;
            break;
            bool1 = false;
            break label91;
        }
    }

    static <T> T[] getParagraphSpans(Spanned paramSpanned, int paramInt1, int paramInt2, Class<T> paramClass)
    {
        if ((paramInt1 == paramInt2) && (paramInt1 > 0));
        for (Object[] arrayOfObject = ArrayUtils.emptyArray(paramClass); ; arrayOfObject = paramSpanned.getSpans(paramInt1, paramInt2, paramClass))
            return arrayOfObject;
    }

    static float measurePara(TextPaint paramTextPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        MeasuredText localMeasuredText = MeasuredText.obtain();
        TextLine localTextLine = TextLine.obtain();
        try
        {
            localMeasuredText.setPara(paramCharSequence, paramInt1, paramInt2, TextDirectionHeuristics.LTR);
            Directions localDirections;
            int i;
            char[] arrayOfChar;
            int j;
            boolean bool;
            TabStops localTabStops;
            if (localMeasuredText.mEasy)
            {
                localDirections = DIRS_ALL_LEFT_TO_RIGHT;
                i = 1;
                arrayOfChar = localMeasuredText.mChars;
                j = localMeasuredText.mLen;
                bool = false;
                localTabStops = null;
            }
            for (int k = 0; ; k++)
            {
                if (k < j)
                {
                    if (arrayOfChar[k] != '\t')
                        continue;
                    bool = true;
                    if ((paramCharSequence instanceof Spanned))
                    {
                        Spanned localSpanned = (Spanned)paramCharSequence;
                        TabStopSpan[] arrayOfTabStopSpan = (TabStopSpan[])getParagraphSpans(localSpanned, paramInt1, localSpanned.nextSpanTransition(paramInt1, paramInt2, TabStopSpan.class), TabStopSpan.class);
                        if (arrayOfTabStopSpan.length > 0)
                            localTabStops = new TabStops(20, arrayOfTabStopSpan);
                    }
                }
                localTextLine.set(paramTextPaint, paramCharSequence, paramInt1, paramInt2, i, localDirections, bool, localTabStops);
                float f = localTextLine.metrics(null);
                return f;
                localDirections = AndroidBidi.directions(localMeasuredText.mDir, localMeasuredText.mLevels, 0, localMeasuredText.mChars, 0, localMeasuredText.mLen);
                i = localMeasuredText.mDir;
                break;
            }
        }
        finally
        {
            TextLine.recycle(localTextLine);
            MeasuredText.recycle(localMeasuredText);
        }
    }

    static float nextTab(CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat, Object[] paramArrayOfObject)
    {
        float f1 = 3.4028235E+38F;
        int i = 0;
        if ((paramCharSequence instanceof Spanned))
        {
            if (paramArrayOfObject == null)
            {
                paramArrayOfObject = getParagraphSpans((Spanned)paramCharSequence, paramInt1, paramInt2, TabStopSpan.class);
                i = 1;
            }
            int j = 0;
            if (j < paramArrayOfObject.length)
            {
                if ((i == 0) && (!(paramArrayOfObject[j] instanceof TabStopSpan)));
                while (true)
                {
                    j++;
                    break;
                    int k = ((TabStopSpan)paramArrayOfObject[j]).getTabStop();
                    if ((k < f1) && (k > paramFloat))
                        f1 = k;
                }
            }
            if (f1 == 3.4028235E+38F);
        }
        for (float f2 = f1; ; f2 = 20 * (int)((paramFloat + 20.0F) / 20.0F))
            return f2;
    }

    private boolean primaryIsTrailingPrevious(int paramInt)
    {
        boolean bool = false;
        int i = 1;
        int j = getLineForOffset(paramInt);
        int k = getLineStart(j);
        int m = getLineEnd(j);
        int[] arrayOfInt = getLineDirections(j).mDirections;
        int n = -1;
        int i1 = 0;
        label107: int i2;
        if (i1 < arrayOfInt.length)
        {
            int i7 = k + arrayOfInt[i1];
            int i8 = i7 + (0x3FFFFFF & arrayOfInt[(i1 + 1)]);
            if (i8 > m)
                i8 = m;
            if ((paramInt >= i7) && (paramInt < i8))
            {
                if (paramInt > i7)
                    return bool;
                n = 0x3F & arrayOfInt[(i1 + 1)] >>> 26;
            }
        }
        else
        {
            if (n == -1)
            {
                if (getParagraphDirection(j) != i)
                    break label185;
                n = 0;
            }
            label144: i2 = -1;
            if (paramInt != k)
                break label197;
            if (getParagraphDirection(j) != i)
                break label191;
            i2 = 0;
            label167: if (i2 >= n)
                break label287;
        }
        while (true)
        {
            bool = i;
            break label107;
            i1 += 2;
            break;
            label185: n = i;
            break label144;
            label191: i2 = i;
            break label167;
            label197: int i3 = paramInt - 1;
            for (int i4 = 0; ; i4 += 2)
            {
                if (i4 >= arrayOfInt.length)
                    break label285;
                int i5 = k + arrayOfInt[i4];
                int i6 = i5 + (0x3FFFFFF & arrayOfInt[(i4 + 1)]);
                if (i6 > m)
                    i6 = m;
                if ((i3 >= i5) && (i3 < i6))
                {
                    i2 = 0x3F & arrayOfInt[(i4 + 1)] >>> 26;
                    break;
                }
            }
            label285: break label167;
            label287: i = 0;
        }
    }

    public void draw(Canvas paramCanvas)
    {
        draw(paramCanvas, null, null, 0);
    }

    public void draw(Canvas paramCanvas, Path paramPath, Paint paramPaint, int paramInt)
    {
        long l = getLineRangeForDraw(paramCanvas);
        int i = TextUtils.unpackRangeStartFromLong(l);
        int j = TextUtils.unpackRangeEndFromLong(l);
        if (j < 0);
        while (true)
        {
            return;
            drawBackground(paramCanvas, paramPath, paramPaint, paramInt, i, j);
            drawText(paramCanvas, i, j);
        }
    }

    public void drawBackground(Canvas paramCanvas, Path paramPath, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mSpannedText)
        {
            if (this.mLineBackgroundSpans == null)
                this.mLineBackgroundSpans = new SpanSet(LineBackgroundSpan.class);
            Spanned localSpanned = (Spanned)this.mText;
            int i = localSpanned.length();
            this.mLineBackgroundSpans.init(localSpanned, 0, i);
            if (this.mLineBackgroundSpans.numberOfSpans > 0)
            {
                int j = getLineTop(paramInt2);
                int k = getLineStart(paramInt2);
                Object localObject = NO_PARA_SPANS;
                int m = 0;
                TextPaint localTextPaint = this.mPaint;
                int n = 0;
                int i1 = this.mWidth;
                for (int i2 = paramInt2; i2 <= paramInt3; i2++)
                {
                    int i3 = k;
                    int i4 = getLineStart(i2 + 1);
                    k = i4;
                    int i5 = j;
                    int i6 = getLineTop(i2 + 1);
                    j = i6;
                    int i7 = i6 - getLineDescent(i2);
                    if (i3 >= n)
                    {
                        n = this.mLineBackgroundSpans.getNextTransition(i3, i);
                        m = 0;
                        if ((i3 != i4) || (i3 == 0))
                        {
                            int i9 = 0;
                            if (i9 < this.mLineBackgroundSpans.numberOfSpans)
                            {
                                if ((this.mLineBackgroundSpans.spanStarts[i9] >= i4) || (this.mLineBackgroundSpans.spanEnds[i9] <= i3));
                                while (true)
                                {
                                    i9++;
                                    break;
                                    int i10 = localObject.length;
                                    if (m == i10)
                                    {
                                        ParagraphStyle[] arrayOfParagraphStyle = new ParagraphStyle[ArrayUtils.idealObjectArraySize(m * 2)];
                                        System.arraycopy(localObject, 0, arrayOfParagraphStyle, 0, m);
                                        localObject = arrayOfParagraphStyle;
                                    }
                                    int i11 = m + 1;
                                    localObject[m] = ((LineBackgroundSpan[])this.mLineBackgroundSpans.spans)[i9];
                                    m = i11;
                                }
                            }
                        }
                    }
                    for (int i8 = 0; i8 < m; i8++)
                        ((LineBackgroundSpan)localObject[i8]).drawBackground(paramCanvas, localTextPaint, 0, i1, i5, i7, i6, localSpanned, i3, i4, i2);
                }
            }
            this.mLineBackgroundSpans.recycle();
        }
        if (paramPath != null)
        {
            if (paramInt1 != 0)
                paramCanvas.translate(0.0F, paramInt1);
            paramCanvas.drawPath(paramPath, paramPaint);
            if (paramInt1 != 0)
                paramCanvas.translate(0.0F, -paramInt1);
        }
    }

    public void drawText(Canvas paramCanvas, int paramInt1, int paramInt2)
    {
        int i = getLineTop(paramInt1);
        int j = getLineStart(paramInt1);
        ParagraphStyle[] arrayOfParagraphStyle = NO_PARA_SPANS;
        int k = 0;
        TextPaint localTextPaint = this.mPaint;
        CharSequence localCharSequence = this.mText;
        Alignment localAlignment1 = this.mAlignment;
        int m = 0;
        TextLine localTextLine = TextLine.obtain();
        int n = paramInt1;
        Object localObject1 = null;
        int i1;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        label242: boolean bool1;
        label284: label360: Object localObject2;
        if (n <= paramInt2)
        {
            i1 = j;
            j = getLineStart(n + 1);
            i2 = getLineVisibleEnd(n, i1, j);
            i3 = i;
            i4 = getLineTop(n + 1);
            i = i4;
            i5 = i4 - getLineDescent(n);
            i6 = getParagraphDirection(n);
            i7 = 0;
            i8 = this.mWidth;
            if (this.mSpannedText)
            {
                Spanned localSpanned = (Spanned)localCharSequence;
                int i12 = localCharSequence.length();
                boolean bool2;
                int i16;
                int i14;
                LeadingMarginSpan localLeadingMarginSpan;
                boolean bool3;
                if ((i1 == 0) || (localCharSequence.charAt(i1 - 1) == '\n'))
                {
                    bool2 = true;
                    if ((i1 >= k) && ((n == paramInt1) || (bool2)))
                    {
                        k = localSpanned.nextSpanTransition(i1, i12, ParagraphStyle.class);
                        arrayOfParagraphStyle = (ParagraphStyle[])getParagraphSpans(localSpanned, i1, k, ParagraphStyle.class);
                        localAlignment1 = this.mAlignment;
                        i16 = -1 + arrayOfParagraphStyle.length;
                        if (i16 >= 0)
                        {
                            if (!(arrayOfParagraphStyle[i16] instanceof AlignmentSpan))
                                break label422;
                            localAlignment1 = ((AlignmentSpan)arrayOfParagraphStyle[i16]).getAlignment();
                        }
                        m = 0;
                    }
                    int i13 = arrayOfParagraphStyle.length;
                    i14 = 0;
                    if (i14 >= i13)
                        break label480;
                    if ((arrayOfParagraphStyle[i14] instanceof LeadingMarginSpan))
                    {
                        localLeadingMarginSpan = (LeadingMarginSpan)arrayOfParagraphStyle[i14];
                        bool3 = bool2;
                        if ((localLeadingMarginSpan instanceof LeadingMarginSpan.LeadingMarginSpan2))
                        {
                            int i15 = ((LeadingMarginSpan.LeadingMarginSpan2)localLeadingMarginSpan).getLeadingMarginLineCount() + getLineForOffset(localSpanned.getSpanStart(localLeadingMarginSpan));
                            if (n >= i15)
                                break label428;
                            bool3 = true;
                        }
                        if (i6 != -1)
                            break label434;
                        localLeadingMarginSpan.drawLeadingMargin(paramCanvas, localTextPaint, i8, i6, i3, i5, i4, localCharSequence, i1, i2, bool2, this);
                        i8 -= localLeadingMarginSpan.getLeadingMargin(bool3);
                    }
                }
                while (true)
                {
                    i14++;
                    break label284;
                    bool2 = false;
                    break;
                    label422: i16--;
                    break label242;
                    label428: bool3 = false;
                    break label360;
                    label434: localLeadingMarginSpan.drawLeadingMargin(paramCanvas, localTextPaint, i7, i6, i3, i5, i4, localCharSequence, i1, i2, bool2, this);
                    i7 += localLeadingMarginSpan.getLeadingMargin(bool3);
                }
            }
            label480: bool1 = getLineContainsTab(n);
            if ((!bool1) || (m != 0))
                break label807;
            if (localObject1 == null)
            {
                localObject2 = new TabStops(20, arrayOfParagraphStyle);
                label516: m = 1;
            }
        }
        while (true)
        {
            Alignment localAlignment2 = localAlignment1;
            Alignment localAlignment3 = Alignment.ALIGN_LEFT;
            label546: int i11;
            label568: Directions localDirections;
            if (localAlignment2 == localAlignment3)
                if (i6 == 1)
                {
                    localAlignment2 = Alignment.ALIGN_NORMAL;
                    Alignment localAlignment5 = Alignment.ALIGN_NORMAL;
                    if (localAlignment2 != localAlignment5)
                        break label689;
                    if (i6 != 1)
                        break label682;
                    i11 = i7;
                    localDirections = getLineDirections(n);
                    if ((localDirections != DIRS_ALL_LEFT_TO_RIGHT) || (this.mSpannedText) || (bool1))
                        break label761;
                    paramCanvas.drawText(localCharSequence, i1, i2, i11, i5, localTextPaint);
                }
            while (true)
            {
                n++;
                localObject1 = localObject2;
                break;
                localObject1.reset(20, arrayOfParagraphStyle);
                localObject2 = localObject1;
                break label516;
                localAlignment2 = Alignment.ALIGN_OPPOSITE;
                break label546;
                Alignment localAlignment4 = Alignment.ALIGN_RIGHT;
                if (localAlignment2 != localAlignment4)
                    break label546;
                if (i6 == 1);
                for (localAlignment2 = Alignment.ALIGN_OPPOSITE; ; localAlignment2 = Alignment.ALIGN_NORMAL)
                    break;
                label682: i11 = i8;
                break label568;
                label689: int i9 = (int)getLineExtent(n, (TabStops)localObject2, false);
                Alignment localAlignment6 = Alignment.ALIGN_OPPOSITE;
                if (localAlignment2 == localAlignment6)
                {
                    if (i6 == 1)
                    {
                        i11 = i8 - i9;
                        break label568;
                    }
                    i11 = i7 - i9;
                    break label568;
                }
                int i10 = i9 & 0xFFFFFFFE;
                i11 = i8 + i7 - i10 >> 1;
                break label568;
                label761: localTextLine.set(localTextPaint, localCharSequence, i1, i2, i6, localDirections, bool1, (TabStops)localObject2);
                localTextLine.draw(paramCanvas, i11, i3, i5, i4);
            }
            TextLine.recycle(localTextLine);
            return;
            label807: localObject2 = localObject1;
        }
    }

    public final Alignment getAlignment()
    {
        return this.mAlignment;
    }

    public abstract int getBottomPadding();

    public void getCursorPath(int paramInt, Path paramPath, CharSequence paramCharSequence)
    {
        paramPath.reset();
        int i = getLineForOffset(paramInt);
        int j = getLineTop(i);
        int k = getLineTop(i + 1);
        float f1 = getPrimaryHorizontal(paramInt) - 0.5F;
        float f2;
        int m;
        int n;
        int i1;
        if (isLevelBoundary(paramInt))
        {
            f2 = getSecondaryHorizontal(paramInt) - 0.5F;
            m = TextKeyListener.getMetaState(paramCharSequence, 1) | TextKeyListener.getMetaState(paramCharSequence, 2048);
            n = TextKeyListener.getMetaState(paramCharSequence, 2);
            i1 = 0;
            if ((m != 0) || (n != 0))
            {
                i1 = k - j >> 2;
                if (n != 0)
                    j += i1;
                if (m != 0)
                    k -= i1;
            }
            if (f1 < 0.5F)
                f1 = 0.5F;
            if (f2 < 0.5F)
                f2 = 0.5F;
            if (Float.compare(f1, f2) != 0)
                break label303;
            paramPath.moveTo(f1, j);
            paramPath.lineTo(f1, k);
            label183: if (m != 2)
                break label352;
            paramPath.moveTo(f2, k);
            paramPath.lineTo(f2 - i1, k + i1);
            paramPath.lineTo(f2, k);
            paramPath.lineTo(f2 + i1, k + i1);
            label239: if (n != 2)
                break label451;
            paramPath.moveTo(f1, j);
            paramPath.lineTo(f1 - i1, j - i1);
            paramPath.lineTo(f1, j);
            paramPath.lineTo(f1 + i1, j - i1);
        }
        while (true)
        {
            return;
            f2 = f1;
            break;
            label303: paramPath.moveTo(f1, j);
            paramPath.lineTo(f1, j + k >> 1);
            paramPath.moveTo(f2, j + k >> 1);
            paramPath.lineTo(f2, k);
            break label183;
            label352: if (m != 1)
                break label239;
            paramPath.moveTo(f2, k);
            paramPath.lineTo(f2 - i1, k + i1);
            paramPath.moveTo(f2 - i1, k + i1 - 0.5F);
            paramPath.lineTo(f2 + i1, k + i1 - 0.5F);
            paramPath.moveTo(f2 + i1, k + i1);
            paramPath.lineTo(f2, k);
            break label239;
            label451: if (n == 1)
            {
                paramPath.moveTo(f1, j);
                paramPath.lineTo(f1 - i1, j - i1);
                paramPath.moveTo(f1 - i1, 0.5F + (j - i1));
                paramPath.lineTo(f1 + i1, 0.5F + (j - i1));
                paramPath.moveTo(f1 + i1, j - i1);
                paramPath.lineTo(f1, j);
            }
        }
    }

    public abstract int getEllipsisCount(int paramInt);

    public abstract int getEllipsisStart(int paramInt);

    public int getEllipsizedWidth()
    {
        return this.mWidth;
    }

    public int getHeight()
    {
        return getLineTop(getLineCount());
    }

    public final int getLineAscent(int paramInt)
    {
        return getLineTop(paramInt) - (getLineTop(paramInt + 1) - getLineDescent(paramInt));
    }

    public final int getLineBaseline(int paramInt)
    {
        return getLineTop(paramInt + 1) - getLineDescent(paramInt);
    }

    public final int getLineBottom(int paramInt)
    {
        return getLineTop(paramInt + 1);
    }

    public int getLineBounds(int paramInt, Rect paramRect)
    {
        if (paramRect != null)
        {
            paramRect.left = 0;
            paramRect.top = getLineTop(paramInt);
            paramRect.right = this.mWidth;
            paramRect.bottom = getLineTop(paramInt + 1);
        }
        return getLineBaseline(paramInt);
    }

    public abstract boolean getLineContainsTab(int paramInt);

    public abstract int getLineCount();

    public abstract int getLineDescent(int paramInt);

    public abstract Directions getLineDirections(int paramInt);

    public final int getLineEnd(int paramInt)
    {
        return getLineStart(paramInt + 1);
    }

    public int getLineForOffset(int paramInt)
    {
        int i = getLineCount();
        int j = -1;
        while (i - j > 1)
        {
            int k = (i + j) / 2;
            if (getLineStart(k) > paramInt)
                i = k;
            else
                j = k;
        }
        if (j < 0)
            j = 0;
        return j;
    }

    public int getLineForVertical(int paramInt)
    {
        int i = getLineCount();
        int j = -1;
        while (i - j > 1)
        {
            int k = (i + j) / 2;
            if (getLineTop(k) > paramInt)
                i = k;
            else
                j = k;
        }
        if (j < 0)
            j = 0;
        return j;
    }

    public float getLineLeft(int paramInt)
    {
        float f = 0.0F;
        int i = getParagraphDirection(paramInt);
        Alignment localAlignment = getParagraphAlignment(paramInt);
        if (localAlignment == Alignment.ALIGN_LEFT);
        while (true)
        {
            return f;
            if (localAlignment == Alignment.ALIGN_NORMAL)
            {
                if (i == -1)
                    f = getParagraphRight(paramInt) - getLineMax(paramInt);
            }
            else if (localAlignment == Alignment.ALIGN_RIGHT)
            {
                f = this.mWidth - getLineMax(paramInt);
            }
            else if (localAlignment == Alignment.ALIGN_OPPOSITE)
            {
                if (i != -1)
                    f = this.mWidth - getLineMax(paramInt);
            }
            else
            {
                int j = getParagraphLeft(paramInt);
                int k = getParagraphRight(paramInt);
                int m = 0xFFFFFFFE & (int)getLineMax(paramInt);
                f = j + (k - j - m) / 2;
            }
        }
    }

    public float getLineMax(int paramInt)
    {
        float f1 = getParagraphLeadingMargin(paramInt);
        float f2 = getLineExtent(paramInt, false);
        if (f1 + f2 >= 0.0F);
        while (true)
        {
            return f2;
            f2 = -f2;
        }
    }

    public long getLineRangeForDraw(Canvas paramCanvas)
    {
        int k;
        int m;
        synchronized (sTempRect)
        {
            if (!paramCanvas.getClipBounds(sTempRect))
            {
                l = TextUtils.packRangeInLong(0, -1);
            }
            else
            {
                int i = sTempRect.top;
                int j = sTempRect.bottom;
                k = Math.max(i, 0);
                m = Math.min(getLineTop(getLineCount()), j);
                if (k >= m)
                    l = TextUtils.packRangeInLong(0, -1);
            }
        }
        long l = TextUtils.packRangeInLong(getLineForVertical(k), getLineForVertical(m));
        return l;
    }

    public float getLineRight(int paramInt)
    {
        int i = getParagraphDirection(paramInt);
        Alignment localAlignment = getParagraphAlignment(paramInt);
        float f;
        if (localAlignment == Alignment.ALIGN_LEFT)
            f = getParagraphLeft(paramInt) + getLineMax(paramInt);
        while (true)
        {
            return f;
            if (localAlignment == Alignment.ALIGN_NORMAL)
            {
                if (i == -1)
                    f = this.mWidth;
                else
                    f = getParagraphLeft(paramInt) + getLineMax(paramInt);
            }
            else if (localAlignment == Alignment.ALIGN_RIGHT)
            {
                f = this.mWidth;
            }
            else if (localAlignment == Alignment.ALIGN_OPPOSITE)
            {
                if (i == -1)
                    f = getLineMax(paramInt);
                else
                    f = this.mWidth;
            }
            else
            {
                int j = getParagraphLeft(paramInt);
                int k = getParagraphRight(paramInt);
                int m = 0xFFFFFFFE & (int)getLineMax(paramInt);
                f = k - (k - j - m) / 2;
            }
        }
    }

    public abstract int getLineStart(int paramInt);

    public abstract int getLineTop(int paramInt);

    public int getLineVisibleEnd(int paramInt)
    {
        return getLineVisibleEnd(paramInt, getLineStart(paramInt), getLineStart(paramInt + 1));
    }

    public float getLineWidth(int paramInt)
    {
        float f1 = getParagraphLeadingMargin(paramInt);
        float f2 = getLineExtent(paramInt, true);
        if (f1 + f2 >= 0.0F);
        while (true)
        {
            return f2;
            f2 = -f2;
        }
    }

    public int getOffsetForHorizontal(int paramInt, float paramFloat)
    {
        int i = -1 + getLineEnd(paramInt);
        int j = getLineStart(paramInt);
        Directions localDirections = getLineDirections(paramInt);
        if (paramInt == -1 + getLineCount())
            i++;
        int k = j;
        float f1 = Math.abs(getPrimaryHorizontal(k) - paramFloat);
        for (int m = 0; m < localDirections.mDirections.length; m += 2)
        {
            int n = j + localDirections.mDirections[m];
            int i1 = n + (0x3FFFFFF & localDirections.mDirections[(m + 1)]);
            int i2;
            int i3;
            int i4;
            if ((0x4000000 & localDirections.mDirections[(m + 1)]) != 0)
            {
                i2 = -1;
                if (i1 > i)
                    i1 = i;
                i3 = 1 + (i1 - 1);
                i4 = -1 + (n + 1);
            }
            while (true)
            {
                if (i3 - i4 <= 1)
                    break label206;
                int i7 = (i3 + i4) / 2;
                if (getPrimaryHorizontal(getOffsetAtStartOf(i7)) * i2 >= paramFloat * i2)
                {
                    i3 = i7;
                    continue;
                    i2 = 1;
                    break;
                }
                i4 = i7;
            }
            label206: if (i4 < n + 1)
                i4 = n + 1;
            if (i4 < i1)
            {
                int i5 = getOffsetAtStartOf(i4);
                float f3 = Math.abs(getPrimaryHorizontal(i5) - paramFloat);
                int i6 = TextUtils.getOffsetAfter(this.mText, i5);
                if (i6 < i1)
                {
                    float f4 = Math.abs(getPrimaryHorizontal(i6) - paramFloat);
                    if (f4 < f3)
                    {
                        f3 = f4;
                        i5 = i6;
                    }
                }
                if (f3 < f1)
                {
                    f1 = f3;
                    k = i5;
                }
            }
            float f2 = Math.abs(getPrimaryHorizontal(n) - paramFloat);
            if (f2 < f1)
            {
                f1 = f2;
                k = n;
            }
        }
        if (Math.abs(getPrimaryHorizontal(i) - paramFloat) < f1)
            k = i;
        return k;
    }

    public int getOffsetToLeftOf(int paramInt)
    {
        return getOffsetToLeftRightOf(paramInt, true);
    }

    public int getOffsetToRightOf(int paramInt)
    {
        return getOffsetToLeftRightOf(paramInt, false);
    }

    public final TextPaint getPaint()
    {
        return this.mPaint;
    }

    public final Alignment getParagraphAlignment(int paramInt)
    {
        Alignment localAlignment = this.mAlignment;
        if (this.mSpannedText)
        {
            AlignmentSpan[] arrayOfAlignmentSpan = (AlignmentSpan[])getParagraphSpans((Spanned)this.mText, getLineStart(paramInt), getLineEnd(paramInt), AlignmentSpan.class);
            int i = arrayOfAlignmentSpan.length;
            if (i > 0)
                localAlignment = arrayOfAlignmentSpan[(i - 1)].getAlignment();
        }
        return localAlignment;
    }

    public abstract int getParagraphDirection(int paramInt);

    public final int getParagraphLeft(int paramInt)
    {
        int i = 0;
        if ((getParagraphDirection(paramInt) == -1) || (!this.mSpannedText));
        while (true)
        {
            return i;
            i = getParagraphLeadingMargin(paramInt);
        }
    }

    public final int getParagraphRight(int paramInt)
    {
        int i = this.mWidth;
        if ((getParagraphDirection(paramInt) == 1) || (!this.mSpannedText));
        while (true)
        {
            return i;
            i -= getParagraphLeadingMargin(paramInt);
        }
    }

    public float getPrimaryHorizontal(int paramInt)
    {
        return getHorizontal(paramInt, primaryIsTrailingPrevious(paramInt));
    }

    public float getSecondaryHorizontal(int paramInt)
    {
        if (!primaryIsTrailingPrevious(paramInt));
        for (boolean bool = true; ; bool = false)
            return getHorizontal(paramInt, bool);
    }

    public void getSelectionPath(int paramInt1, int paramInt2, Path paramPath)
    {
        paramPath.reset();
        if (paramInt1 == paramInt2);
        while (true)
        {
            return;
            if (paramInt2 < paramInt1)
            {
                int i7 = paramInt2;
                paramInt2 = paramInt1;
                paramInt1 = i7;
            }
            int i = getLineForOffset(paramInt1);
            int j = getLineForOffset(paramInt2);
            int k = getLineTop(i);
            int m = getLineBottom(j);
            if (i == j)
            {
                addSelection(i, paramInt1, paramInt2, k, m, paramPath);
            }
            else
            {
                float f = this.mWidth;
                int n = getLineEnd(i);
                int i1 = getLineBottom(i);
                addSelection(i, paramInt1, n, k, i1, paramPath);
                if (getParagraphDirection(i) == -1)
                    paramPath.addRect(getLineLeft(i), k, 0.0F, getLineBottom(i), Path.Direction.CW);
                while (true)
                {
                    for (int i2 = i + 1; i2 < j; i2++)
                    {
                        int i5 = getLineTop(i2);
                        int i6 = getLineBottom(i2);
                        paramPath.addRect(0.0F, i5, f, i6, Path.Direction.CW);
                    }
                    paramPath.addRect(getLineRight(i), k, f, getLineBottom(i), Path.Direction.CW);
                }
                int i3 = getLineTop(j);
                int i4 = getLineBottom(j);
                addSelection(j, getLineStart(j), paramInt2, i3, i4, paramPath);
                if (getParagraphDirection(j) == -1)
                    paramPath.addRect(f, i3, getLineRight(j), i4, Path.Direction.CW);
                else
                    paramPath.addRect(0.0F, i3, getLineLeft(j), i4, Path.Direction.CW);
            }
        }
    }

    public final float getSpacingAdd()
    {
        return this.mSpacingAdd;
    }

    public final float getSpacingMultiplier()
    {
        return this.mSpacingMult;
    }

    public final CharSequence getText()
    {
        return this.mText;
    }

    public final TextDirectionHeuristic getTextDirectionHeuristic()
    {
        return this.mTextDir;
    }

    public abstract int getTopPadding();

    public final int getWidth()
    {
        return this.mWidth;
    }

    public final void increaseWidthTo(int paramInt)
    {
        if (paramInt < this.mWidth)
            throw new RuntimeException("attempted to reduce Layout width");
        this.mWidth = paramInt;
    }

    public boolean isLevelBoundary(int paramInt)
    {
        boolean bool = false;
        int i = getLineForOffset(paramInt);
        Directions localDirections = getLineDirections(i);
        if ((localDirections == DIRS_ALL_LEFT_TO_RIGHT) || (localDirections == DIRS_ALL_RIGHT_TO_LEFT));
        label163: 
        while (true)
        {
            return bool;
            int[] arrayOfInt = localDirections.mDirections;
            int j = getLineStart(i);
            int k = getLineEnd(i);
            if ((paramInt == j) || (paramInt == k))
            {
                int m;
                if (getParagraphDirection(i) == 1)
                {
                    m = 0;
                    label78: if (paramInt != j)
                        break label116;
                }
                label116: for (int n = 0; ; n = -2 + arrayOfInt.length)
                {
                    if ((0x3F & arrayOfInt[(n + 1)] >>> 26) == m)
                        break label125;
                    bool = true;
                    break;
                    m = 1;
                    break label78;
                }
            }
            else
            {
                label125: int i1 = paramInt - j;
                for (int i2 = 0; ; i2 += 2)
                {
                    if (i2 >= arrayOfInt.length)
                        break label163;
                    if (i1 == arrayOfInt[i2])
                    {
                        bool = true;
                        break;
                    }
                }
            }
        }
    }

    public boolean isRtlCharAt(int paramInt)
    {
        boolean bool1 = true;
        boolean bool2 = false;
        int i = getLineForOffset(paramInt);
        Directions localDirections = getLineDirections(i);
        if (localDirections == DIRS_ALL_LEFT_TO_RIGHT);
        label115: 
        while (true)
        {
            return bool2;
            if (localDirections == DIRS_ALL_RIGHT_TO_LEFT)
            {
                bool2 = bool1;
            }
            else
            {
                int[] arrayOfInt = localDirections.mDirections;
                int j = getLineStart(i);
                for (int k = 0; ; k += 2)
                {
                    if (k >= arrayOfInt.length)
                        break label115;
                    if (paramInt >= j + (0x3FFFFFF & arrayOfInt[k]))
                    {
                        if ((0x1 & (0x3F & arrayOfInt[(k + 1)] >>> 26)) != 0);
                        while (true)
                        {
                            bool2 = bool1;
                            break;
                            bool1 = false;
                        }
                    }
                }
            }
        }
    }

    protected final boolean isSpanned()
    {
        return this.mSpannedText;
    }

    void replaceWith(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Alignment paramAlignment, float paramFloat1, float paramFloat2)
    {
        if (paramInt < 0)
            throw new IllegalArgumentException("Layout: " + paramInt + " < 0");
        this.mText = paramCharSequence;
        this.mPaint = paramTextPaint;
        this.mWidth = paramInt;
        this.mAlignment = paramAlignment;
        this.mSpacingMult = paramFloat1;
        this.mSpacingAdd = paramFloat2;
        this.mSpannedText = (paramCharSequence instanceof Spanned);
    }

    public static enum Alignment
    {
        static
        {
            ALIGN_CENTER = new Alignment("ALIGN_CENTER", 2);
            ALIGN_LEFT = new Alignment("ALIGN_LEFT", 3);
            ALIGN_RIGHT = new Alignment("ALIGN_RIGHT", 4);
            Alignment[] arrayOfAlignment = new Alignment[5];
            arrayOfAlignment[0] = ALIGN_NORMAL;
            arrayOfAlignment[1] = ALIGN_OPPOSITE;
            arrayOfAlignment[2] = ALIGN_CENTER;
            arrayOfAlignment[3] = ALIGN_LEFT;
            arrayOfAlignment[4] = ALIGN_RIGHT;
        }
    }

    static class SpannedEllipsizer extends Layout.Ellipsizer
        implements Spanned
    {
        private Spanned mSpanned;

        public SpannedEllipsizer(CharSequence paramCharSequence)
        {
            super();
            this.mSpanned = ((Spanned)paramCharSequence);
        }

        public int getSpanEnd(Object paramObject)
        {
            return this.mSpanned.getSpanEnd(paramObject);
        }

        public int getSpanFlags(Object paramObject)
        {
            return this.mSpanned.getSpanFlags(paramObject);
        }

        public int getSpanStart(Object paramObject)
        {
            return this.mSpanned.getSpanStart(paramObject);
        }

        public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass)
        {
            return this.mSpanned.getSpans(paramInt1, paramInt2, paramClass);
        }

        public int nextSpanTransition(int paramInt1, int paramInt2, Class paramClass)
        {
            return this.mSpanned.nextSpanTransition(paramInt1, paramInt2, paramClass);
        }

        public CharSequence subSequence(int paramInt1, int paramInt2)
        {
            char[] arrayOfChar = new char[paramInt2 - paramInt1];
            getChars(paramInt1, paramInt2, arrayOfChar, 0);
            SpannableString localSpannableString = new SpannableString(new String(arrayOfChar));
            TextUtils.copySpansFrom(this.mSpanned, paramInt1, paramInt2, Object.class, localSpannableString, 0);
            return localSpannableString;
        }
    }

    static class Ellipsizer
        implements CharSequence, GetChars
    {
        Layout mLayout;
        TextUtils.TruncateAt mMethod;
        CharSequence mText;
        int mWidth;

        public Ellipsizer(CharSequence paramCharSequence)
        {
            this.mText = paramCharSequence;
        }

        public char charAt(int paramInt)
        {
            char[] arrayOfChar = TextUtils.obtain(1);
            getChars(paramInt, paramInt + 1, arrayOfChar, 0);
            char c = arrayOfChar[0];
            TextUtils.recycle(arrayOfChar);
            return c;
        }

        public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
        {
            int i = this.mLayout.getLineForOffset(paramInt1);
            int j = this.mLayout.getLineForOffset(paramInt2);
            TextUtils.getChars(this.mText, paramInt1, paramInt2, paramArrayOfChar, paramInt3);
            for (int k = i; k <= j; k++)
                this.mLayout.ellipsize(paramInt1, paramInt2, k, paramArrayOfChar, paramInt3, this.mMethod);
        }

        public int length()
        {
            return this.mText.length();
        }

        public CharSequence subSequence(int paramInt1, int paramInt2)
        {
            char[] arrayOfChar = new char[paramInt2 - paramInt1];
            getChars(paramInt1, paramInt2, arrayOfChar, 0);
            return new String(arrayOfChar);
        }

        public String toString()
        {
            char[] arrayOfChar = new char[length()];
            getChars(0, length(), arrayOfChar, 0);
            return new String(arrayOfChar);
        }
    }

    public static class Directions
    {
        int[] mDirections;

        Directions(int[] paramArrayOfInt)
        {
            this.mDirections = paramArrayOfInt;
        }
    }

    static class TabStops
    {
        private int mIncrement;
        private int mNumStops;
        private int[] mStops;

        TabStops(int paramInt, Object[] paramArrayOfObject)
        {
            reset(paramInt, paramArrayOfObject);
        }

        public static float nextDefaultStop(float paramFloat, int paramInt)
        {
            return paramInt * (int)((paramFloat + paramInt) / paramInt);
        }

        float nextTab(float paramFloat)
        {
            int i = this.mNumStops;
            int j;
            int k;
            if (i > 0)
            {
                int[] arrayOfInt = this.mStops;
                j = 0;
                if (j < i)
                {
                    k = arrayOfInt[j];
                    if (k <= paramFloat);
                }
            }
            for (float f = k; ; f = nextDefaultStop(paramFloat, this.mIncrement))
            {
                return f;
                j++;
                break;
            }
        }

        void reset(int paramInt, Object[] paramArrayOfObject)
        {
            this.mIncrement = paramInt;
            int i = 0;
            Object localObject1;
            int k;
            int m;
            label59: int n;
            if (paramArrayOfObject != null)
            {
                localObject1 = this.mStops;
                int j = paramArrayOfObject.length;
                k = 0;
                m = 0;
                if (k < j)
                {
                    Object localObject2 = paramArrayOfObject[k];
                    if (!(localObject2 instanceof TabStopSpan))
                        break label177;
                    if (localObject1 == null)
                    {
                        localObject1 = new int[10];
                        n = m + 1;
                        localObject1[m] = ((TabStopSpan)localObject2).getTabStop();
                    }
                }
            }
            while (true)
            {
                k++;
                m = n;
                break;
                if (m != localObject1.length)
                    break label59;
                int[] arrayOfInt = new int[m * 2];
                for (int i1 = 0; i1 < m; i1++)
                    arrayOfInt[i1] = localObject1[i1];
                localObject1 = arrayOfInt;
                break label59;
                if (m > 1)
                    Arrays.sort((int[])localObject1, 0, m);
                if (localObject1 != this.mStops)
                    this.mStops = ((int[])localObject1);
                i = m;
                this.mNumStops = i;
                return;
                label177: n = m;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.Layout
 * JD-Core Version:        0.6.2
 */