package android.text;

import com.android.internal.util.ArrayUtils;
import java.lang.reflect.Array;

abstract class SpannableStringInternal
{
    private static final int COLUMNS = 3;
    static final Object[] EMPTY = new Object[0];
    private static final int END = 1;
    private static final int FLAGS = 2;
    private static final int START;
    private int mSpanCount;
    private int[] mSpanData;
    private Object[] mSpans;
    private String mText;

    SpannableStringInternal(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        if ((paramInt1 == 0) && (paramInt2 == paramCharSequence.length()));
        for (this.mText = paramCharSequence.toString(); ; this.mText = paramCharSequence.toString().substring(paramInt1, paramInt2))
        {
            int i = ArrayUtils.idealIntArraySize(0);
            this.mSpans = new Object[i];
            this.mSpanData = new int[i * 3];
            if (!(paramCharSequence instanceof Spanned))
                break;
            Spanned localSpanned = (Spanned)paramCharSequence;
            Object[] arrayOfObject = localSpanned.getSpans(paramInt1, paramInt2, Object.class);
            for (int j = 0; j < arrayOfObject.length; j++)
            {
                int k = localSpanned.getSpanStart(arrayOfObject[j]);
                int m = localSpanned.getSpanEnd(arrayOfObject[j]);
                int n = localSpanned.getSpanFlags(arrayOfObject[j]);
                if (k < paramInt1)
                    k = paramInt1;
                if (m > paramInt2)
                    m = paramInt2;
                setSpan(arrayOfObject[j], k - paramInt1, m - paramInt1, n);
            }
        }
    }

    private void checkRange(String paramString, int paramInt1, int paramInt2)
    {
        if (paramInt2 < paramInt1)
            throw new IndexOutOfBoundsException(paramString + " " + region(paramInt1, paramInt2) + " has end before start");
        int i = length();
        if ((paramInt1 > i) || (paramInt2 > i))
            throw new IndexOutOfBoundsException(paramString + " " + region(paramInt1, paramInt2) + " ends beyond length " + i);
        if ((paramInt1 < 0) || (paramInt2 < 0))
            throw new IndexOutOfBoundsException(paramString + " " + region(paramInt1, paramInt2) + " starts before 0");
    }

    private static String region(int paramInt1, int paramInt2)
    {
        return "(" + paramInt1 + " ... " + paramInt2 + ")";
    }

    private void sendSpanAdded(Object paramObject, int paramInt1, int paramInt2)
    {
        SpanWatcher[] arrayOfSpanWatcher = (SpanWatcher[])getSpans(paramInt1, paramInt2, SpanWatcher.class);
        int i = arrayOfSpanWatcher.length;
        for (int j = 0; j < i; j++)
            arrayOfSpanWatcher[j].onSpanAdded((Spannable)this, paramObject, paramInt1, paramInt2);
    }

    private void sendSpanChanged(Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        SpanWatcher[] arrayOfSpanWatcher = (SpanWatcher[])getSpans(Math.min(paramInt1, paramInt3), Math.max(paramInt2, paramInt4), SpanWatcher.class);
        int i = arrayOfSpanWatcher.length;
        for (int j = 0; j < i; j++)
            arrayOfSpanWatcher[j].onSpanChanged((Spannable)this, paramObject, paramInt1, paramInt2, paramInt3, paramInt4);
    }

    private void sendSpanRemoved(Object paramObject, int paramInt1, int paramInt2)
    {
        SpanWatcher[] arrayOfSpanWatcher = (SpanWatcher[])getSpans(paramInt1, paramInt2, SpanWatcher.class);
        int i = arrayOfSpanWatcher.length;
        for (int j = 0; j < i; j++)
            arrayOfSpanWatcher[j].onSpanRemoved((Spannable)this, paramObject, paramInt1, paramInt2);
    }

    public final char charAt(int paramInt)
    {
        return this.mText.charAt(paramInt);
    }

    public final void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
    {
        this.mText.getChars(paramInt1, paramInt2, paramArrayOfChar, paramInt3);
    }

    public int getSpanEnd(Object paramObject)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int[] arrayOfInt = this.mSpanData;
        int j = i - 1;
        if (j >= 0)
            if (arrayOfObject[j] != paramObject);
        for (int k = arrayOfInt[(1 + j * 3)]; ; k = -1)
        {
            return k;
            j--;
            break;
        }
    }

    public int getSpanFlags(Object paramObject)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int[] arrayOfInt = this.mSpanData;
        int j = i - 1;
        if (j >= 0)
            if (arrayOfObject[j] != paramObject);
        for (int k = arrayOfInt[(2 + j * 3)]; ; k = 0)
        {
            return k;
            j--;
            break;
        }
    }

    public int getSpanStart(Object paramObject)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int[] arrayOfInt = this.mSpanData;
        int j = i - 1;
        if (j >= 0)
            if (arrayOfObject[j] != paramObject);
        for (int k = arrayOfInt[(0 + j * 3)]; ; k = -1)
        {
            return k;
            j--;
            break;
        }
    }

    public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject1 = this.mSpans;
        int[] arrayOfInt = this.mSpanData;
        Object[] arrayOfObject2 = null;
        Object localObject = null;
        int j = 0;
        int k = 0;
        if (j < i)
        {
            int i2;
            if ((paramClass != null) && (!paramClass.isInstance(arrayOfObject1[j])))
                i2 = k;
            while (true)
            {
                j++;
                k = i2;
                break;
                int m = arrayOfInt[(0 + j * 3)];
                int n = arrayOfInt[(1 + j * 3)];
                if (m > paramInt2)
                {
                    i2 = k;
                }
                else if (n < paramInt1)
                {
                    i2 = k;
                }
                else if ((m != n) && (paramInt1 != paramInt2))
                {
                    if (m == paramInt2)
                        i2 = k;
                    else if (n == paramInt1)
                        i2 = k;
                }
                else if (k == 0)
                {
                    localObject = arrayOfObject1[j];
                    i2 = k + 1;
                }
                else
                {
                    if (k == 1)
                    {
                        arrayOfObject2 = (Object[])Array.newInstance(paramClass, 1 + (i - j));
                        arrayOfObject2[0] = localObject;
                    }
                    int i1 = 0xFF0000 & arrayOfInt[(2 + j * 3)];
                    if (i1 != 0)
                        for (int i3 = 0; ; i3++)
                            if ((i3 >= k) || (i1 > (0xFF0000 & getSpanFlags(arrayOfObject2[i3]))))
                            {
                                System.arraycopy(arrayOfObject2, i3, arrayOfObject2, i3 + 1, k - i3);
                                arrayOfObject2[i3] = arrayOfObject1[j];
                                i2 = k + 1;
                                break;
                            }
                    i2 = k + 1;
                    arrayOfObject2[k] = arrayOfObject1[j];
                }
            }
        }
        Object[] arrayOfObject4;
        if (k == 0)
            arrayOfObject4 = (Object[])ArrayUtils.emptyArray(paramClass);
        while (true)
        {
            return arrayOfObject4;
            if (k == 1)
            {
                Object[] arrayOfObject5 = (Object[])Array.newInstance(paramClass, 1);
                arrayOfObject5[0] = localObject;
                arrayOfObject4 = (Object[])arrayOfObject5;
            }
            else if (k == arrayOfObject2.length)
            {
                arrayOfObject4 = (Object[])arrayOfObject2;
            }
            else
            {
                Object[] arrayOfObject3 = (Object[])Array.newInstance(paramClass, k);
                System.arraycopy(arrayOfObject2, 0, arrayOfObject3, 0, k);
                arrayOfObject4 = (Object[])arrayOfObject3;
            }
        }
    }

    public final int length()
    {
        return this.mText.length();
    }

    public int nextSpanTransition(int paramInt1, int paramInt2, Class paramClass)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int[] arrayOfInt = this.mSpanData;
        if (paramClass == null)
            paramClass = Object.class;
        for (int j = 0; j < i; j++)
        {
            int k = arrayOfInt[(0 + j * 3)];
            int m = arrayOfInt[(1 + j * 3)];
            if ((k > paramInt1) && (k < paramInt2) && (paramClass.isInstance(arrayOfObject[j])))
                paramInt2 = k;
            if ((m > paramInt1) && (m < paramInt2) && (paramClass.isInstance(arrayOfObject[j])))
                paramInt2 = m;
        }
        return paramInt2;
    }

    void removeSpan(Object paramObject)
    {
        int i = this.mSpanCount;
        Object[] arrayOfObject = this.mSpans;
        int[] arrayOfInt = this.mSpanData;
        for (int j = i - 1; ; j--)
            if (j >= 0)
            {
                if (arrayOfObject[j] == paramObject)
                {
                    int k = arrayOfInt[(0 + j * 3)];
                    int m = arrayOfInt[(1 + j * 3)];
                    int n = i - (j + 1);
                    System.arraycopy(arrayOfObject, j + 1, arrayOfObject, j, n);
                    System.arraycopy(arrayOfInt, 3 * (j + 1), arrayOfInt, j * 3, n * 3);
                    this.mSpanCount = (-1 + this.mSpanCount);
                    sendSpanRemoved(paramObject, k, m);
                }
            }
            else
                return;
    }

    void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3)
    {
        checkRange("setSpan", paramInt1, paramInt2);
        if ((paramInt3 & 0x33) == 51)
        {
            if ((paramInt1 != 0) && (paramInt1 != length()))
            {
                char c2 = charAt(paramInt1 - 1);
                if (c2 != '\n')
                    throw new RuntimeException("PARAGRAPH span must start at paragraph boundary (" + paramInt1 + " follows " + c2 + ")");
            }
            if ((paramInt2 != 0) && (paramInt2 != length()))
            {
                char c1 = charAt(paramInt2 - 1);
                if (c1 != '\n')
                    throw new RuntimeException("PARAGRAPH span must end at paragraph boundary (" + paramInt2 + " follows " + c1 + ")");
            }
        }
        int i = this.mSpanCount;
        Object[] arrayOfObject1 = this.mSpans;
        int[] arrayOfInt1 = this.mSpanData;
        int j = 0;
        if (j < i)
            if (arrayOfObject1[j] == paramObject)
            {
                int m = arrayOfInt1[(0 + j * 3)];
                int n = arrayOfInt1[(1 + j * 3)];
                arrayOfInt1[(0 + j * 3)] = paramInt1;
                arrayOfInt1[(1 + j * 3)] = paramInt2;
                arrayOfInt1[(2 + j * 3)] = paramInt3;
                sendSpanChanged(paramObject, m, n, paramInt1, paramInt2);
            }
        while (true)
        {
            return;
            j++;
            break;
            if (1 + this.mSpanCount >= this.mSpans.length)
            {
                int k = ArrayUtils.idealIntArraySize(1 + this.mSpanCount);
                Object[] arrayOfObject2 = new Object[k];
                int[] arrayOfInt2 = new int[k * 3];
                System.arraycopy(this.mSpans, 0, arrayOfObject2, 0, this.mSpanCount);
                System.arraycopy(this.mSpanData, 0, arrayOfInt2, 0, 3 * this.mSpanCount);
                this.mSpans = arrayOfObject2;
                this.mSpanData = arrayOfInt2;
            }
            this.mSpans[this.mSpanCount] = paramObject;
            this.mSpanData[(0 + 3 * this.mSpanCount)] = paramInt1;
            this.mSpanData[(1 + 3 * this.mSpanCount)] = paramInt2;
            this.mSpanData[(2 + 3 * this.mSpanCount)] = paramInt3;
            this.mSpanCount = (1 + this.mSpanCount);
            if ((this instanceof Spannable))
                sendSpanAdded(paramObject, paramInt1, paramInt2);
        }
    }

    public final String toString()
    {
        return this.mText;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.SpannableStringInternal
 * JD-Core Version:        0.6.2
 */