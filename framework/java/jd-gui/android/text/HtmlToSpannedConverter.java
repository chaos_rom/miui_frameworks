package android.text;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.ParagraphStyle;
import android.text.style.QuoteSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

class HtmlToSpannedConverter
    implements ContentHandler
{
    private static HashMap<String, Integer> COLORS = buildColorMap();
    private static final float[] HEADER_SIZES;
    private Html.ImageGetter mImageGetter;
    private XMLReader mReader;
    private String mSource;
    private SpannableStringBuilder mSpannableStringBuilder;
    private Html.TagHandler mTagHandler;

    static
    {
        float[] arrayOfFloat = new float[6];
        arrayOfFloat[0] = 1.5F;
        arrayOfFloat[1] = 1.4F;
        arrayOfFloat[2] = 1.3F;
        arrayOfFloat[3] = 1.2F;
        arrayOfFloat[4] = 1.1F;
        arrayOfFloat[5] = 1.0F;
        HEADER_SIZES = arrayOfFloat;
    }

    public HtmlToSpannedConverter(String paramString, Html.ImageGetter paramImageGetter, Html.TagHandler paramTagHandler, Parser paramParser)
    {
        this.mSource = paramString;
        this.mSpannableStringBuilder = new SpannableStringBuilder();
        this.mImageGetter = paramImageGetter;
        this.mTagHandler = paramTagHandler;
        this.mReader = paramParser;
    }

    private static HashMap<String, Integer> buildColorMap()
    {
        HashMap localHashMap = new HashMap();
        localHashMap.put("aqua", Integer.valueOf(65535));
        localHashMap.put("black", Integer.valueOf(0));
        localHashMap.put("blue", Integer.valueOf(255));
        localHashMap.put("fuchsia", Integer.valueOf(16711935));
        localHashMap.put("green", Integer.valueOf(32768));
        localHashMap.put("grey", Integer.valueOf(8421504));
        localHashMap.put("lime", Integer.valueOf(65280));
        localHashMap.put("maroon", Integer.valueOf(8388608));
        localHashMap.put("navy", Integer.valueOf(128));
        localHashMap.put("olive", Integer.valueOf(8421376));
        localHashMap.put("purple", Integer.valueOf(8388736));
        localHashMap.put("red", Integer.valueOf(16711680));
        localHashMap.put("silver", Integer.valueOf(12632256));
        localHashMap.put("teal", Integer.valueOf(32896));
        localHashMap.put("white", Integer.valueOf(16777215));
        localHashMap.put("yellow", Integer.valueOf(16776960));
        return localHashMap;
    }

    private static void end(SpannableStringBuilder paramSpannableStringBuilder, Class paramClass, Object paramObject)
    {
        int i = paramSpannableStringBuilder.length();
        Object localObject = getLast(paramSpannableStringBuilder, paramClass);
        int j = paramSpannableStringBuilder.getSpanStart(localObject);
        paramSpannableStringBuilder.removeSpan(localObject);
        if (j != i)
            paramSpannableStringBuilder.setSpan(paramObject, j, i, 33);
    }

    private static void endA(SpannableStringBuilder paramSpannableStringBuilder)
    {
        int i = paramSpannableStringBuilder.length();
        Object localObject = getLast(paramSpannableStringBuilder, Href.class);
        int j = paramSpannableStringBuilder.getSpanStart(localObject);
        paramSpannableStringBuilder.removeSpan(localObject);
        if (j != i)
        {
            Href localHref = (Href)localObject;
            if (localHref.mHref != null)
                paramSpannableStringBuilder.setSpan(new URLSpan(localHref.mHref), j, i, 33);
        }
    }

    private static void endFont(SpannableStringBuilder paramSpannableStringBuilder)
    {
        int i = paramSpannableStringBuilder.length();
        Object localObject = getLast(paramSpannableStringBuilder, Font.class);
        int j = paramSpannableStringBuilder.getSpanStart(localObject);
        paramSpannableStringBuilder.removeSpan(localObject);
        Font localFont;
        if (j != i)
        {
            localFont = (Font)localObject;
            if (!TextUtils.isEmpty(localFont.mColor))
            {
                if (!localFont.mColor.startsWith("@"))
                    break label143;
                Resources localResources = Resources.getSystem();
                int m = localResources.getIdentifier(localFont.mColor.substring(1), "color", "android");
                if (m != 0)
                    paramSpannableStringBuilder.setSpan(new TextAppearanceSpan(null, 0, 0, localResources.getColorStateList(m), null), j, i, 33);
            }
        }
        while (true)
        {
            if (localFont.mFace != null)
                paramSpannableStringBuilder.setSpan(new TypefaceSpan(localFont.mFace), j, i, 33);
            return;
            label143: int k = getHtmlColor(localFont.mColor);
            if (k != -1)
                paramSpannableStringBuilder.setSpan(new ForegroundColorSpan(0xFF000000 | k), j, i, 33);
        }
    }

    private static void endHeader(SpannableStringBuilder paramSpannableStringBuilder)
    {
        int i = paramSpannableStringBuilder.length();
        Object localObject = getLast(paramSpannableStringBuilder, Header.class);
        int j = paramSpannableStringBuilder.getSpanStart(localObject);
        paramSpannableStringBuilder.removeSpan(localObject);
        while ((i > j) && (paramSpannableStringBuilder.charAt(i - 1) == '\n'))
            i--;
        if (j != i)
        {
            Header localHeader = (Header)localObject;
            paramSpannableStringBuilder.setSpan(new RelativeSizeSpan(HEADER_SIZES[localHeader.mLevel]), j, i, 33);
            paramSpannableStringBuilder.setSpan(new StyleSpan(1), j, i, 33);
        }
    }

    private static int getHtmlColor(String paramString)
    {
        int i = -1;
        Integer localInteger = (Integer)COLORS.get(paramString.toLowerCase());
        if (localInteger != null)
            i = localInteger.intValue();
        while (true)
        {
            return i;
            try
            {
                int j = XmlUtils.convertValueToInt(paramString, -1);
                i = j;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
        }
    }

    private static Object getLast(Spanned paramSpanned, Class paramClass)
    {
        Object[] arrayOfObject = paramSpanned.getSpans(0, paramSpanned.length(), paramClass);
        if (arrayOfObject.length == 0);
        for (Object localObject = null; ; localObject = arrayOfObject[(-1 + arrayOfObject.length)])
            return localObject;
    }

    private static void handleBr(SpannableStringBuilder paramSpannableStringBuilder)
    {
        paramSpannableStringBuilder.append("\n");
    }

    private void handleEndTag(String paramString)
    {
        if (paramString.equalsIgnoreCase("br"))
            handleBr(this.mSpannableStringBuilder);
        while (true)
        {
            return;
            if (paramString.equalsIgnoreCase("p"))
            {
                handleP(this.mSpannableStringBuilder);
            }
            else if (paramString.equalsIgnoreCase("div"))
            {
                handleP(this.mSpannableStringBuilder);
            }
            else if (paramString.equalsIgnoreCase("strong"))
            {
                end(this.mSpannableStringBuilder, Bold.class, new StyleSpan(1));
            }
            else if (paramString.equalsIgnoreCase("b"))
            {
                end(this.mSpannableStringBuilder, Bold.class, new StyleSpan(1));
            }
            else if (paramString.equalsIgnoreCase("em"))
            {
                end(this.mSpannableStringBuilder, Italic.class, new StyleSpan(2));
            }
            else if (paramString.equalsIgnoreCase("cite"))
            {
                end(this.mSpannableStringBuilder, Italic.class, new StyleSpan(2));
            }
            else if (paramString.equalsIgnoreCase("dfn"))
            {
                end(this.mSpannableStringBuilder, Italic.class, new StyleSpan(2));
            }
            else if (paramString.equalsIgnoreCase("i"))
            {
                end(this.mSpannableStringBuilder, Italic.class, new StyleSpan(2));
            }
            else if (paramString.equalsIgnoreCase("big"))
            {
                end(this.mSpannableStringBuilder, Big.class, new RelativeSizeSpan(1.25F));
            }
            else if (paramString.equalsIgnoreCase("small"))
            {
                end(this.mSpannableStringBuilder, Small.class, new RelativeSizeSpan(0.8F));
            }
            else if (paramString.equalsIgnoreCase("font"))
            {
                endFont(this.mSpannableStringBuilder);
            }
            else if (paramString.equalsIgnoreCase("blockquote"))
            {
                handleP(this.mSpannableStringBuilder);
                end(this.mSpannableStringBuilder, Blockquote.class, new QuoteSpan());
            }
            else if (paramString.equalsIgnoreCase("tt"))
            {
                end(this.mSpannableStringBuilder, Monospace.class, new TypefaceSpan("monospace"));
            }
            else if (paramString.equalsIgnoreCase("a"))
            {
                endA(this.mSpannableStringBuilder);
            }
            else if (paramString.equalsIgnoreCase("u"))
            {
                end(this.mSpannableStringBuilder, Underline.class, new UnderlineSpan());
            }
            else if (paramString.equalsIgnoreCase("sup"))
            {
                end(this.mSpannableStringBuilder, Super.class, new SuperscriptSpan());
            }
            else if (paramString.equalsIgnoreCase("sub"))
            {
                end(this.mSpannableStringBuilder, Sub.class, new SubscriptSpan());
            }
            else if ((paramString.length() == 2) && (Character.toLowerCase(paramString.charAt(0)) == 'h') && (paramString.charAt(1) >= '1') && (paramString.charAt(1) <= '6'))
            {
                handleP(this.mSpannableStringBuilder);
                endHeader(this.mSpannableStringBuilder);
            }
            else if (this.mTagHandler != null)
            {
                this.mTagHandler.handleTag(false, paramString, this.mSpannableStringBuilder, this.mReader);
            }
        }
    }

    private static void handleP(SpannableStringBuilder paramSpannableStringBuilder)
    {
        int i = paramSpannableStringBuilder.length();
        if ((i >= 1) && (paramSpannableStringBuilder.charAt(i - 1) == '\n'))
            if ((i < 2) || (paramSpannableStringBuilder.charAt(i - 2) != '\n'));
        while (true)
        {
            return;
            paramSpannableStringBuilder.append("\n");
            continue;
            if (i != 0)
                paramSpannableStringBuilder.append("\n\n");
        }
    }

    private void handleStartTag(String paramString, Attributes paramAttributes)
    {
        if (paramString.equalsIgnoreCase("br"));
        while (true)
        {
            return;
            if (paramString.equalsIgnoreCase("p"))
            {
                handleP(this.mSpannableStringBuilder);
            }
            else if (paramString.equalsIgnoreCase("div"))
            {
                handleP(this.mSpannableStringBuilder);
            }
            else if (paramString.equalsIgnoreCase("strong"))
            {
                start(this.mSpannableStringBuilder, new Bold(null));
            }
            else if (paramString.equalsIgnoreCase("b"))
            {
                start(this.mSpannableStringBuilder, new Bold(null));
            }
            else if (paramString.equalsIgnoreCase("em"))
            {
                start(this.mSpannableStringBuilder, new Italic(null));
            }
            else if (paramString.equalsIgnoreCase("cite"))
            {
                start(this.mSpannableStringBuilder, new Italic(null));
            }
            else if (paramString.equalsIgnoreCase("dfn"))
            {
                start(this.mSpannableStringBuilder, new Italic(null));
            }
            else if (paramString.equalsIgnoreCase("i"))
            {
                start(this.mSpannableStringBuilder, new Italic(null));
            }
            else if (paramString.equalsIgnoreCase("big"))
            {
                start(this.mSpannableStringBuilder, new Big(null));
            }
            else if (paramString.equalsIgnoreCase("small"))
            {
                start(this.mSpannableStringBuilder, new Small(null));
            }
            else if (paramString.equalsIgnoreCase("font"))
            {
                startFont(this.mSpannableStringBuilder, paramAttributes);
            }
            else if (paramString.equalsIgnoreCase("blockquote"))
            {
                handleP(this.mSpannableStringBuilder);
                start(this.mSpannableStringBuilder, new Blockquote(null));
            }
            else if (paramString.equalsIgnoreCase("tt"))
            {
                start(this.mSpannableStringBuilder, new Monospace(null));
            }
            else if (paramString.equalsIgnoreCase("a"))
            {
                startA(this.mSpannableStringBuilder, paramAttributes);
            }
            else if (paramString.equalsIgnoreCase("u"))
            {
                start(this.mSpannableStringBuilder, new Underline(null));
            }
            else if (paramString.equalsIgnoreCase("sup"))
            {
                start(this.mSpannableStringBuilder, new Super(null));
            }
            else if (paramString.equalsIgnoreCase("sub"))
            {
                start(this.mSpannableStringBuilder, new Sub(null));
            }
            else if ((paramString.length() == 2) && (Character.toLowerCase(paramString.charAt(0)) == 'h') && (paramString.charAt(1) >= '1') && (paramString.charAt(1) <= '6'))
            {
                handleP(this.mSpannableStringBuilder);
                start(this.mSpannableStringBuilder, new Header('\0)7' + paramString.charAt(1)));
            }
            else if (paramString.equalsIgnoreCase("img"))
            {
                startImg(this.mSpannableStringBuilder, paramAttributes, this.mImageGetter);
            }
            else if (this.mTagHandler != null)
            {
                this.mTagHandler.handleTag(true, paramString, this.mSpannableStringBuilder, this.mReader);
            }
        }
    }

    private static void start(SpannableStringBuilder paramSpannableStringBuilder, Object paramObject)
    {
        int i = paramSpannableStringBuilder.length();
        paramSpannableStringBuilder.setSpan(paramObject, i, i, 17);
    }

    private static void startA(SpannableStringBuilder paramSpannableStringBuilder, Attributes paramAttributes)
    {
        String str = paramAttributes.getValue("", "href");
        int i = paramSpannableStringBuilder.length();
        paramSpannableStringBuilder.setSpan(new Href(str), i, i, 17);
    }

    private static void startFont(SpannableStringBuilder paramSpannableStringBuilder, Attributes paramAttributes)
    {
        String str1 = paramAttributes.getValue("", "color");
        String str2 = paramAttributes.getValue("", "face");
        int i = paramSpannableStringBuilder.length();
        paramSpannableStringBuilder.setSpan(new Font(str1, str2), i, i, 17);
    }

    private static void startImg(SpannableStringBuilder paramSpannableStringBuilder, Attributes paramAttributes, Html.ImageGetter paramImageGetter)
    {
        String str = paramAttributes.getValue("", "src");
        Drawable localDrawable = null;
        if (paramImageGetter != null)
            localDrawable = paramImageGetter.getDrawable(str);
        if (localDrawable == null)
        {
            localDrawable = Resources.getSystem().getDrawable(17303029);
            localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        }
        int i = paramSpannableStringBuilder.length();
        paramSpannableStringBuilder.append("￼");
        paramSpannableStringBuilder.setSpan(new ImageSpan(localDrawable, str), i, paramSpannableStringBuilder.length(), 33);
    }

    public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        throws SAXException
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = 0;
        if (i < paramInt2)
        {
            char c = paramArrayOfChar[(i + paramInt1)];
            int j;
            int m;
            int k;
            if ((c == ' ') || (c == '\n'))
            {
                j = localStringBuilder.length();
                if (j == 0)
                {
                    m = this.mSpannableStringBuilder.length();
                    if (m == 0)
                    {
                        k = 10;
                        label70: if ((k != 32) && (k != 10))
                            localStringBuilder.append(' ');
                    }
                }
            }
            while (true)
            {
                i++;
                break;
                k = this.mSpannableStringBuilder.charAt(m - 1);
                break label70;
                k = localStringBuilder.charAt(j - 1);
                break label70;
                localStringBuilder.append(c);
            }
        }
        this.mSpannableStringBuilder.append(localStringBuilder);
    }

    public Spanned convert()
    {
        this.mReader.setContentHandler(this);
        while (true)
        {
            Object[] arrayOfObject;
            int i;
            int j;
            int k;
            try
            {
                this.mReader.parse(new InputSource(new StringReader(this.mSource)));
                arrayOfObject = this.mSpannableStringBuilder.getSpans(0, this.mSpannableStringBuilder.length(), ParagraphStyle.class);
                i = 0;
                if (i >= arrayOfObject.length)
                    break;
                j = this.mSpannableStringBuilder.getSpanStart(arrayOfObject[i]);
                k = this.mSpannableStringBuilder.getSpanEnd(arrayOfObject[i]);
                if ((k - 2 >= 0) && (this.mSpannableStringBuilder.charAt(k - 1) == '\n') && (this.mSpannableStringBuilder.charAt(k - 2) == '\n'))
                    k--;
                if (k == j)
                {
                    this.mSpannableStringBuilder.removeSpan(arrayOfObject[i]);
                    i++;
                    continue;
                }
            }
            catch (IOException localIOException)
            {
                throw new RuntimeException(localIOException);
            }
            catch (SAXException localSAXException)
            {
                throw new RuntimeException(localSAXException);
            }
            this.mSpannableStringBuilder.setSpan(arrayOfObject[i], j, k, 51);
        }
        return this.mSpannableStringBuilder;
    }

    public void endDocument()
        throws SAXException
    {
    }

    public void endElement(String paramString1, String paramString2, String paramString3)
        throws SAXException
    {
        handleEndTag(paramString2);
    }

    public void endPrefixMapping(String paramString)
        throws SAXException
    {
    }

    public void ignorableWhitespace(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        throws SAXException
    {
    }

    public void processingInstruction(String paramString1, String paramString2)
        throws SAXException
    {
    }

    public void setDocumentLocator(Locator paramLocator)
    {
    }

    public void skippedEntity(String paramString)
        throws SAXException
    {
    }

    public void startDocument()
        throws SAXException
    {
    }

    public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes)
        throws SAXException
    {
        handleStartTag(paramString2, paramAttributes);
    }

    public void startPrefixMapping(String paramString1, String paramString2)
        throws SAXException
    {
    }

    private static class Header
    {
        private int mLevel;

        public Header(int paramInt)
        {
            this.mLevel = paramInt;
        }
    }

    private static class Href
    {
        public String mHref;

        public Href(String paramString)
        {
            this.mHref = paramString;
        }
    }

    private static class Font
    {
        public String mColor;
        public String mFace;

        public Font(String paramString1, String paramString2)
        {
            this.mColor = paramString1;
            this.mFace = paramString2;
        }
    }

    private static class Sub
    {
    }

    private static class Super
    {
    }

    private static class Blockquote
    {
    }

    private static class Monospace
    {
    }

    private static class Small
    {
    }

    private static class Big
    {
    }

    private static class Underline
    {
    }

    private static class Italic
    {
    }

    private static class Bold
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.HtmlToSpannedConverter
 * JD-Core Version:        0.6.2
 */