package android.text;

import android.graphics.drawable.Drawable;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.AlignmentSpan;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.ParagraphStyle;
import android.text.style.QuoteSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import com.android.internal.util.ArrayUtils;
import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;

public class Html
{
    public static String escapeHtml(CharSequence paramCharSequence)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        withinStyle(localStringBuilder, paramCharSequence, 0, paramCharSequence.length());
        return localStringBuilder.toString();
    }

    public static Spanned fromHtml(String paramString)
    {
        return fromHtml(paramString, null, null);
    }

    public static Spanned fromHtml(String paramString, ImageGetter paramImageGetter, TagHandler paramTagHandler)
    {
        Parser localParser = new Parser();
        try
        {
            localParser.setProperty("http://www.ccil.org/~cowan/tagsoup/properties/schema", HtmlParser.schema);
            return new HtmlToSpannedConverter(paramString, paramImageGetter, paramTagHandler, localParser).convert();
        }
        catch (SAXNotRecognizedException localSAXNotRecognizedException)
        {
            throw new RuntimeException(localSAXNotRecognizedException);
        }
        catch (SAXNotSupportedException localSAXNotSupportedException)
        {
            throw new RuntimeException(localSAXNotSupportedException);
        }
    }

    private static String getOpenParaTagWithDirection(Spanned paramSpanned, int paramInt1, int paramInt2)
    {
        int i = paramInt2 - paramInt1;
        byte[] arrayOfByte = new byte[ArrayUtils.idealByteArraySize(i)];
        char[] arrayOfChar = TextUtils.obtain(i);
        TextUtils.getChars(paramSpanned, paramInt1, paramInt2, arrayOfChar, 0);
        switch (AndroidBidi.bidi(2, arrayOfChar, arrayOfByte, i, false))
        {
        default:
        case -1:
        }
        for (String str = "<p dir=ltr>"; ; str = "<p dir=rtl>")
            return str;
    }

    public static String toHtml(Spanned paramSpanned)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        withinHtml(localStringBuilder, paramSpanned);
        return localStringBuilder.toString();
    }

    private static void withinBlockquote(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt1, int paramInt2)
    {
        paramStringBuilder.append(getOpenParaTagWithDirection(paramSpanned, paramInt1, paramInt2));
        int i = paramInt1;
        if (i < paramInt2)
        {
            int j = TextUtils.indexOf(paramSpanned, '\n', i, paramInt2);
            if (j < 0)
                j = paramInt2;
            int k = 0;
            while ((j < paramInt2) && (paramSpanned.charAt(j) == '\n'))
            {
                k++;
                j++;
            }
            int m = j - k;
            if (j == paramInt2);
            for (boolean bool = true; ; bool = false)
            {
                withinParagraph(paramStringBuilder, paramSpanned, i, m, k, bool);
                i = j;
                break;
            }
        }
        paramStringBuilder.append("</p>\n");
    }

    private static void withinDiv(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt1, int paramInt2)
    {
        int j;
        for (int i = paramInt1; i < paramInt2; i = j)
        {
            j = paramSpanned.nextSpanTransition(i, paramInt2, QuoteSpan.class);
            QuoteSpan[] arrayOfQuoteSpan = (QuoteSpan[])paramSpanned.getSpans(i, j, QuoteSpan.class);
            int k = arrayOfQuoteSpan.length;
            for (int m = 0; m < k; m++)
            {
                arrayOfQuoteSpan[m];
                paramStringBuilder.append("<blockquote>");
            }
            withinBlockquote(paramStringBuilder, paramSpanned, i, j);
            int n = arrayOfQuoteSpan.length;
            for (int i1 = 0; i1 < n; i1++)
            {
                arrayOfQuoteSpan[i1];
                paramStringBuilder.append("</blockquote>\n");
            }
        }
    }

    private static void withinHtml(StringBuilder paramStringBuilder, Spanned paramSpanned)
    {
        int i = paramSpanned.length();
        int k;
        for (int j = 0; j < paramSpanned.length(); j = k)
        {
            k = paramSpanned.nextSpanTransition(j, i, ParagraphStyle.class);
            ParagraphStyle[] arrayOfParagraphStyle = (ParagraphStyle[])paramSpanned.getSpans(j, k, ParagraphStyle.class);
            String str = " ";
            int m = 0;
            int n = 0;
            if (n < arrayOfParagraphStyle.length)
            {
                Layout.Alignment localAlignment;
                if ((arrayOfParagraphStyle[n] instanceof AlignmentSpan))
                {
                    localAlignment = ((AlignmentSpan)arrayOfParagraphStyle[n]).getAlignment();
                    m = 1;
                    if (localAlignment != Layout.Alignment.ALIGN_CENTER)
                        break label130;
                    str = "align=\"center\" " + str;
                }
                while (true)
                {
                    n++;
                    break;
                    label130: if (localAlignment == Layout.Alignment.ALIGN_OPPOSITE)
                        str = "align=\"right\" " + str;
                    else
                        str = "align=\"left\" " + str;
                }
            }
            if (m != 0)
                paramStringBuilder.append("<div " + str + ">");
            withinDiv(paramStringBuilder, paramSpanned, j, k);
            if (m != 0)
                paramStringBuilder.append("</div>");
        }
    }

    private static void withinParagraph(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        int k;
        for (int i = paramInt1; i < paramInt2; i = k)
        {
            k = paramSpanned.nextSpanTransition(i, paramInt2, CharacterStyle.class);
            CharacterStyle[] arrayOfCharacterStyle = (CharacterStyle[])paramSpanned.getSpans(i, k, CharacterStyle.class);
            for (int m = 0; m < arrayOfCharacterStyle.length; m++)
            {
                if ((arrayOfCharacterStyle[m] instanceof StyleSpan))
                {
                    int i2 = ((StyleSpan)arrayOfCharacterStyle[m]).getStyle();
                    if ((i2 & 0x1) != 0)
                        paramStringBuilder.append("<b>");
                    if ((i2 & 0x2) != 0)
                        paramStringBuilder.append("<i>");
                }
                if (((arrayOfCharacterStyle[m] instanceof TypefaceSpan)) && (((TypefaceSpan)arrayOfCharacterStyle[m]).getFamily().equals("monospace")))
                    paramStringBuilder.append("<tt>");
                if ((arrayOfCharacterStyle[m] instanceof SuperscriptSpan))
                    paramStringBuilder.append("<sup>");
                if ((arrayOfCharacterStyle[m] instanceof SubscriptSpan))
                    paramStringBuilder.append("<sub>");
                if ((arrayOfCharacterStyle[m] instanceof UnderlineSpan))
                    paramStringBuilder.append("<u>");
                if ((arrayOfCharacterStyle[m] instanceof StrikethroughSpan))
                    paramStringBuilder.append("<strike>");
                if ((arrayOfCharacterStyle[m] instanceof URLSpan))
                {
                    paramStringBuilder.append("<a href=\"");
                    paramStringBuilder.append(((URLSpan)arrayOfCharacterStyle[m]).getURL());
                    paramStringBuilder.append("\">");
                }
                if ((arrayOfCharacterStyle[m] instanceof ImageSpan))
                {
                    paramStringBuilder.append("<img src=\"");
                    paramStringBuilder.append(((ImageSpan)arrayOfCharacterStyle[m]).getSource());
                    paramStringBuilder.append("\">");
                    i = k;
                }
                if ((arrayOfCharacterStyle[m] instanceof AbsoluteSizeSpan))
                {
                    paramStringBuilder.append("<font size =\"");
                    paramStringBuilder.append(((AbsoluteSizeSpan)arrayOfCharacterStyle[m]).getSize() / 6);
                    paramStringBuilder.append("\">");
                }
                if ((arrayOfCharacterStyle[m] instanceof ForegroundColorSpan))
                {
                    paramStringBuilder.append("<font color =\"#");
                    for (String str2 = Integer.toHexString(16777216 + ((ForegroundColorSpan)arrayOfCharacterStyle[m]).getForegroundColor()); str2.length() < 6; str2 = "0" + str2);
                    paramStringBuilder.append(str2);
                    paramStringBuilder.append("\">");
                }
            }
            withinStyle(paramStringBuilder, paramSpanned, i, k);
            for (int n = -1 + arrayOfCharacterStyle.length; n >= 0; n--)
            {
                if ((arrayOfCharacterStyle[n] instanceof ForegroundColorSpan))
                    paramStringBuilder.append("</font>");
                if ((arrayOfCharacterStyle[n] instanceof AbsoluteSizeSpan))
                    paramStringBuilder.append("</font>");
                if ((arrayOfCharacterStyle[n] instanceof URLSpan))
                    paramStringBuilder.append("</a>");
                if ((arrayOfCharacterStyle[n] instanceof StrikethroughSpan))
                    paramStringBuilder.append("</strike>");
                if ((arrayOfCharacterStyle[n] instanceof UnderlineSpan))
                    paramStringBuilder.append("</u>");
                if ((arrayOfCharacterStyle[n] instanceof SubscriptSpan))
                    paramStringBuilder.append("</sub>");
                if ((arrayOfCharacterStyle[n] instanceof SuperscriptSpan))
                    paramStringBuilder.append("</sup>");
                if (((arrayOfCharacterStyle[n] instanceof TypefaceSpan)) && (((TypefaceSpan)arrayOfCharacterStyle[n]).getFamily().equals("monospace")))
                    paramStringBuilder.append("</tt>");
                if ((arrayOfCharacterStyle[n] instanceof StyleSpan))
                {
                    int i1 = ((StyleSpan)arrayOfCharacterStyle[n]).getStyle();
                    if ((i1 & 0x1) != 0)
                        paramStringBuilder.append("</b>");
                    if ((i1 & 0x2) != 0)
                        paramStringBuilder.append("</i>");
                }
            }
        }
        String str1;
        if (paramBoolean)
        {
            str1 = "";
            if (paramInt3 != 1)
                break label750;
            paramStringBuilder.append("<br>\n");
        }
        while (true)
        {
            return;
            str1 = "</p>\n" + getOpenParaTagWithDirection(paramSpanned, paramInt1, paramInt2);
            break;
            label750: if (paramInt3 == 2)
            {
                paramStringBuilder.append(str1);
            }
            else
            {
                for (int j = 2; j < paramInt3; j++)
                    paramStringBuilder.append("<br>");
                paramStringBuilder.append(str1);
            }
        }
    }

    private static void withinStyle(StringBuilder paramStringBuilder, CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        if (i < paramInt2)
        {
            char c = paramCharSequence.charAt(i);
            if (c == '<')
                paramStringBuilder.append("&lt;");
            while (true)
            {
                i++;
                break;
                if (c == '>')
                {
                    paramStringBuilder.append("&gt;");
                }
                else if (c == '&')
                {
                    paramStringBuilder.append("&amp;");
                }
                else if ((c > '~') || (c < ' '))
                {
                    paramStringBuilder.append("&#" + c + ";");
                }
                else if (c == ' ')
                {
                    while ((i + 1 < paramInt2) && (paramCharSequence.charAt(i + 1) == ' '))
                    {
                        paramStringBuilder.append("&nbsp;");
                        i++;
                    }
                    paramStringBuilder.append(' ');
                }
                else
                {
                    paramStringBuilder.append(c);
                }
            }
        }
    }

    private static class HtmlParser
    {
        private static final HTMLSchema schema = new HTMLSchema();
    }

    public static abstract interface TagHandler
    {
        public abstract void handleTag(boolean paramBoolean, String paramString, Editable paramEditable, XMLReader paramXMLReader);
    }

    public static abstract interface ImageGetter
    {
        public abstract Drawable getDrawable(String paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.Html
 * JD-Core Version:        0.6.2
 */