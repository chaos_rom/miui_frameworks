package android.text;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.emoji.EmojiFactory;
import android.graphics.Bitmap;
import android.graphics.Paint.FontMetricsInt;
import android.text.style.LeadingMarginSpan;
import android.text.style.LeadingMarginSpan.LeadingMarginSpan2;
import android.text.style.LineHeightSpan;
import android.text.style.LineHeightSpan.WithDensity;
import android.text.style.MetricAffectingSpan;
import android.text.style.TabStopSpan;
import android.util.Log;
import com.android.internal.util.ArrayUtils;

public class StaticLayout extends Layout
{
    private static final char CHAR_COLON = ':';
    private static final char CHAR_COMMA = ',';
    private static final char CHAR_DOT = '.';
    private static final char CHAR_FIRST_CJK = '⺀';
    private static final int CHAR_FIRST_HIGH_SURROGATE = 55296;
    private static final char CHAR_HYPHEN = '-';
    private static final int CHAR_LAST_LOW_SURROGATE = 57343;
    private static final char CHAR_NEW_LINE = '\n';
    private static final char CHAR_SEMICOLON = ';';
    private static final char CHAR_SLASH = '/';
    private static final char CHAR_SPACE = ' ';
    private static final char CHAR_TAB = '\t';
    private static final int COLUMNS_ELLIPSIZE = 5;
    private static final int COLUMNS_NORMAL = 3;
    private static final int DESCENT = 2;
    private static final int DIR = 0;
    private static final int DIR_SHIFT = 30;
    private static final int ELLIPSIS_COUNT = 4;
    private static final int ELLIPSIS_START = 3;
    private static final double EXTRA_ROUNDING = 0.5D;
    private static final int START = 0;
    private static final int START_MASK = 536870911;
    private static final int TAB = 0;
    private static final int TAB_INCREMENT = 20;
    private static final int TAB_MASK = 536870912;
    static final String TAG = "StaticLayout";
    private static final int TOP = 1;
    private int mBottomPadding;
    private int mColumns = 5;
    private int mEllipsizedWidth;
    private Paint.FontMetricsInt mFontMetricsInt = new Paint.FontMetricsInt();
    private int mLineCount;
    private Layout.Directions[] mLineDirections = new Layout.Directions[ArrayUtils.idealIntArraySize(2 * this.mColumns)];
    private int[] mLines = new int[ArrayUtils.idealIntArraySize(2 * this.mColumns)];
    private int mMaximumVisibleLineCount = 2147483647;
    private MeasuredText mMeasured = MeasuredText.obtain();
    private int mTopPadding;

    StaticLayout(CharSequence paramCharSequence)
    {
        super(paramCharSequence, null, 0, null, 0.0F, 0.0F);
    }

    public StaticLayout(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, int paramInt3, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this(paramCharSequence, paramInt1, paramInt2, paramTextPaint, paramInt3, paramAlignment, paramFloat1, paramFloat2, paramBoolean, null, 0);
    }

    public StaticLayout(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, int paramInt3, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt4)
    {
        this(paramCharSequence, paramInt1, paramInt2, paramTextPaint, paramInt3, paramAlignment, TextDirectionHeuristics.FIRSTSTRONG_LTR, paramFloat1, paramFloat2, paramBoolean, paramTruncateAt, paramInt4, 2147483647);
    }

    public StaticLayout(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, int paramInt3, Layout.Alignment paramAlignment, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this(paramCharSequence, paramInt1, paramInt2, paramTextPaint, paramInt3, paramAlignment, paramTextDirectionHeuristic, paramFloat1, paramFloat2, paramBoolean, null, 0, 2147483647);
    }

    public StaticLayout(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, int paramInt3, Layout.Alignment paramAlignment, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt4, int paramInt5)
    {
    }

    public StaticLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this(paramCharSequence, 0, paramCharSequence.length(), paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramBoolean);
    }

    public StaticLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this(paramCharSequence, 0, paramCharSequence.length(), paramTextPaint, paramInt, paramAlignment, paramTextDirectionHeuristic, paramFloat1, paramFloat2, paramBoolean);
    }

    private void calculateEllipsis(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3, float paramFloat1, TextUtils.TruncateAt paramTruncateAt, int paramInt4, float paramFloat2, TextPaint paramTextPaint, boolean paramBoolean)
    {
        if ((paramFloat2 <= paramFloat1) && (!paramBoolean))
        {
            this.mLines[(3 + paramInt4 * this.mColumns)] = 0;
            this.mLines[(4 + paramInt4 * this.mColumns)] = 0;
            return;
        }
        char[] arrayOfChar;
        label57: float f1;
        int i;
        int j;
        int k;
        float f10;
        int i2;
        label102: float f11;
        if (paramTruncateAt == TextUtils.TruncateAt.END_SMALL)
        {
            arrayOfChar = ELLIPSIS_TWO_DOTS;
            f1 = paramTextPaint.measureText(arrayOfChar, 0, 1);
            i = 0;
            j = 0;
            k = paramInt2 - paramInt1;
            if (paramTruncateAt != TextUtils.TruncateAt.START)
                break label217;
            if (this.mMaximumVisibleLineCount != 1)
                break label197;
            f10 = 0.0F;
            i2 = k;
            if (i2 >= 0)
            {
                f11 = paramArrayOfFloat[(paramInt1 + (i2 - 1) - paramInt3)];
                if (f1 + (f11 + f10) <= paramFloat1)
                    break label184;
            }
            i = 0;
            j = i2;
        }
        while (true)
        {
            this.mLines[(3 + paramInt4 * this.mColumns)] = i;
            this.mLines[(4 + paramInt4 * this.mColumns)] = j;
            break;
            arrayOfChar = ELLIPSIS_NORMAL;
            break label57;
            label184: f10 += f11;
            i2--;
            break label102;
            label197: if (Log.isLoggable("StaticLayout", 5))
            {
                Log.w("StaticLayout", "Start Ellipsis only supported with one line");
                continue;
                label217: if ((paramTruncateAt == TextUtils.TruncateAt.END) || (paramTruncateAt == TextUtils.TruncateAt.MARQUEE) || (paramTruncateAt == TextUtils.TruncateAt.END_SMALL))
                {
                    float f2 = 0.0F;
                    for (int m = 0; ; m++)
                    {
                        float f3;
                        if (m < k)
                        {
                            f3 = paramArrayOfFloat[(m + paramInt1 - paramInt3)];
                            if (f1 + (f3 + f2) <= paramFloat1);
                        }
                        else
                        {
                            i = m;
                            j = k - m;
                            if ((!paramBoolean) || (j != 0) || (k <= 0))
                                break;
                            i = k - 1;
                            j = 1;
                            break;
                        }
                        f2 += f3;
                    }
                }
                if (this.mMaximumVisibleLineCount == 1)
                {
                    float f4 = 0.0F;
                    float f5 = 0.0F;
                    float f6 = (paramFloat1 - f1) / 2.0F;
                    int n = k;
                    label357: float f9;
                    float f7;
                    if (n >= 0)
                    {
                        f9 = paramArrayOfFloat[(paramInt1 + (n - 1) - paramInt3)];
                        if (f9 + f5 <= f6);
                    }
                    else
                    {
                        f7 = paramFloat1 - f1 - f5;
                    }
                    for (int i1 = 0; ; i1++)
                    {
                        float f8;
                        if (i1 < n)
                        {
                            f8 = paramArrayOfFloat[(i1 + paramInt1 - paramInt3)];
                            if (f8 + f4 <= f7);
                        }
                        else
                        {
                            i = i1;
                            j = n - i1;
                            break;
                            f5 += f9;
                            n--;
                            break label357;
                        }
                        f4 += f8;
                    }
                }
                if (Log.isLoggable("StaticLayout", 5))
                    Log.w("StaticLayout", "Middle Ellipsis only supported with one line");
            }
        }
    }

    private static final boolean isIdeographic(char paramChar, boolean paramBoolean)
    {
        boolean bool = true;
        if ((paramChar >= '⺀') && (paramChar <= '⿿'));
        while (true)
        {
            return bool;
            if (paramChar != '　')
                if ((paramChar >= '぀') && (paramChar <= 'ゟ'))
                {
                    if (paramBoolean);
                }
                else
                    switch (paramChar)
                    {
                    default:
                        break;
                    case 'ぁ':
                    case 'ぃ':
                    case 'ぅ':
                    case 'ぇ':
                    case 'ぉ':
                    case 'っ':
                    case 'ゃ':
                    case 'ゅ':
                    case 'ょ':
                    case 'ゎ':
                    case 'ゕ':
                    case 'ゖ':
                    case '゛':
                    case '゜':
                    case 'ゝ':
                    case 'ゞ':
                        bool = false;
                        continue;
                        if ((paramChar >= '゠') && (paramChar <= 'ヿ'))
                        {
                            if (paramBoolean);
                        }
                        else
                            switch (paramChar)
                            {
                            default:
                                break;
                            case '゠':
                            case 'ァ':
                            case 'ィ':
                            case 'ゥ':
                            case 'ェ':
                            case 'ォ':
                            case 'ッ':
                            case 'ャ':
                            case 'ュ':
                            case 'ョ':
                            case 'ヮ':
                            case 'ヵ':
                            case 'ヶ':
                            case '・':
                            case 'ー':
                            case 'ヽ':
                            case 'ヾ':
                                bool = false;
                                continue;
                                if (((paramChar < '㐀') || (paramChar > '䶵')) && ((paramChar < '一') || (paramChar > 40891)) && ((paramChar < 63744) || (paramChar > 64217)) && ((paramChar < 40960) || (paramChar > 42127)) && ((paramChar < 42128) || (paramChar > 42191)) && ((paramChar < 65122) || (paramChar > 65126)) && ((paramChar < 65296) || (paramChar > 65305)))
                                    bool = Injector.isIdeographic(paramChar, paramBoolean);
                                break;
                            }
                        break;
                    }
        }
    }

    private int out(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, float paramFloat1, float paramFloat2, LineHeightSpan[] paramArrayOfLineHeightSpan, int[] paramArrayOfInt, Paint.FontMetricsInt paramFontMetricsInt, boolean paramBoolean1, boolean paramBoolean2, byte[] paramArrayOfByte, int paramInt8, boolean paramBoolean3, int paramInt9, boolean paramBoolean4, boolean paramBoolean5, char[] paramArrayOfChar, float[] paramArrayOfFloat, int paramInt10, TextUtils.TruncateAt paramTruncateAt, float paramFloat3, float paramFloat4, TextPaint paramTextPaint, boolean paramBoolean6)
    {
        int i = this.mLineCount;
        int j = i * this.mColumns;
        int k = 1 + (j + this.mColumns);
        Object localObject = this.mLines;
        if (k >= localObject.length)
        {
            int i8 = ArrayUtils.idealIntArraySize(k + 1);
            int[] arrayOfInt = new int[i8];
            int i9 = localObject.length;
            System.arraycopy(localObject, 0, arrayOfInt, 0, i9);
            this.mLines = arrayOfInt;
            localObject = arrayOfInt;
            Layout.Directions[] arrayOfDirections = new Layout.Directions[i8];
            System.arraycopy(this.mLineDirections, 0, arrayOfDirections, 0, this.mLineDirections.length);
            this.mLineDirections = arrayOfDirections;
        }
        if (paramArrayOfLineHeightSpan != null)
        {
            paramFontMetricsInt.ascent = paramInt3;
            paramFontMetricsInt.descent = paramInt4;
            paramFontMetricsInt.top = paramInt5;
            paramFontMetricsInt.bottom = paramInt6;
            int i6 = 0;
            int i7 = paramArrayOfLineHeightSpan.length;
            if (i6 < i7)
            {
                if ((paramArrayOfLineHeightSpan[i6] instanceof LineHeightSpan.WithDensity))
                    ((LineHeightSpan.WithDensity)paramArrayOfLineHeightSpan[i6]).chooseHeight(paramCharSequence, paramInt1, paramInt2, paramArrayOfInt[i6], paramInt7, paramFontMetricsInt, paramTextPaint);
                while (true)
                {
                    i6++;
                    break;
                    paramArrayOfLineHeightSpan[i6].chooseHeight(paramCharSequence, paramInt1, paramInt2, paramArrayOfInt[i6], paramInt7, paramFontMetricsInt);
                }
            }
            paramInt3 = paramFontMetricsInt.ascent;
            paramInt4 = paramFontMetricsInt.descent;
            paramInt5 = paramFontMetricsInt.top;
            paramInt6 = paramFontMetricsInt.bottom;
        }
        if (i == 0)
        {
            if (paramBoolean5)
                this.mTopPadding = (paramInt5 - paramInt3);
            if (paramBoolean4)
                paramInt3 = paramInt5;
        }
        if (paramInt2 == paramInt9)
        {
            if (paramBoolean5)
                this.mBottomPadding = (paramInt6 - paramInt4);
            if (paramBoolean4)
                paramInt4 = paramInt6;
        }
        double d;
        int m;
        int n;
        label484: int i2;
        label497: int i3;
        label511: boolean bool;
        if (paramBoolean2)
        {
            d = paramFloat2 + (paramInt4 - paramInt3) * (paramFloat1 - 1.0F);
            if (d >= 0.0D)
            {
                m = (int)(0.5D + d);
                localObject[(j + 0)] = paramInt1;
                localObject[(j + 1)] = paramInt7;
                localObject[(j + 2)] = (paramInt4 + m);
                n = paramInt7 + (m + (paramInt4 - paramInt3));
                localObject[(0 + (j + this.mColumns))] = paramInt2;
                localObject[(1 + (j + this.mColumns))] = n;
                if (paramBoolean1)
                {
                    int i5 = j + 0;
                    localObject[i5] = (0x20000000 | localObject[i5]);
                }
                int i1 = j + 0;
                localObject[i1] |= paramInt8 << 30;
                Layout.Directions localDirections = DIRS_ALL_LEFT_TO_RIGHT;
                if (!paramBoolean3)
                    break label649;
                this.mLineDirections[i] = localDirections;
                if (paramTruncateAt != null)
                {
                    if (i != 0)
                        break label679;
                    i2 = 1;
                    if (i + 1 != this.mMaximumVisibleLineCount)
                        break label685;
                    i3 = 1;
                    if ((!paramBoolean6) || (1 + this.mLineCount != this.mMaximumVisibleLineCount))
                        break label691;
                    bool = true;
                    label532: if (((this.mMaximumVisibleLineCount != 1) || (!paramBoolean6)) && (((i2 == 0) || (paramBoolean6) || (paramTruncateAt == TextUtils.TruncateAt.MARQUEE)) && ((i2 != 0) || ((i3 == 0) && (paramBoolean6)) || (paramTruncateAt != TextUtils.TruncateAt.END))))
                        break label697;
                }
            }
        }
        label649: label679: label685: label691: label697: for (int i4 = 1; ; i4 = 0)
        {
            if (i4 != 0)
                calculateEllipsis(paramInt1, paramInt2, paramArrayOfFloat, paramInt10, paramFloat3, paramTruncateAt, i, paramFloat4, paramTextPaint, bool);
            this.mLineCount = (1 + this.mLineCount);
            return n;
            m = -(int)(0.5D + -d);
            break;
            m = 0;
            break;
            this.mLineDirections[i] = AndroidBidi.directions(paramInt8, paramArrayOfByte, paramInt1 - paramInt10, paramArrayOfChar, paramInt1 - paramInt10, paramInt2 - paramInt1);
            break label484;
            i2 = 0;
            break label497;
            i3 = 0;
            break label511;
            bool = false;
            break label532;
        }
    }

    void finish()
    {
        this.mMeasured = MeasuredText.recycle(this.mMeasured);
    }

    void generate(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, int paramInt3, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2, boolean paramBoolean1, boolean paramBoolean2, float paramFloat3, TextUtils.TruncateAt paramTruncateAt)
    {
        this.mLineCount = 0;
        int i = 0;
        boolean bool1;
        Paint.FontMetricsInt localFontMetricsInt;
        int[] arrayOfInt;
        MeasuredText localMeasuredText;
        Spanned localSpanned;
        if ((paramFloat1 != 1.0F) || (paramFloat2 != 0.0F))
        {
            bool1 = true;
            localFontMetricsInt = this.mFontMetricsInt;
            arrayOfInt = null;
            localMeasuredText = this.mMeasured;
            localSpanned = null;
            if ((paramCharSequence instanceof Spanned))
                localSpanned = (Spanned)paramCharSequence;
        }
        int m;
        for (int j = paramInt1; ; j = m)
        {
            LineHeightSpan[] arrayOfLineHeightSpan;
            label356: char[] arrayOfChar;
            float[] arrayOfFloat;
            byte[] arrayOfByte;
            int i3;
            boolean bool2;
            float f1;
            int i5;
            int i12;
            int i13;
            int i14;
            int i15;
            boolean bool3;
            if (j <= paramInt2)
            {
                int k = TextUtils.indexOf(paramCharSequence, '\n', j, paramInt2);
                if (k < 0);
                int n;
                int i1;
                int i2;
                for (m = paramInt2; ; m = k + 1)
                {
                    n = 1 + this.mLineCount;
                    i1 = paramInt3;
                    i2 = paramInt3;
                    arrayOfLineHeightSpan = null;
                    if (localSpanned == null)
                        break label356;
                    LeadingMarginSpan[] arrayOfLeadingMarginSpan = (LeadingMarginSpan[])getParagraphSpans(localSpanned, j, m, LeadingMarginSpan.class);
                    for (int i33 = 0; ; i33++)
                    {
                        int i34 = arrayOfLeadingMarginSpan.length;
                        if (i33 >= i34)
                            break;
                        LeadingMarginSpan localLeadingMarginSpan = arrayOfLeadingMarginSpan[i33];
                        i1 -= arrayOfLeadingMarginSpan[i33].getLeadingMargin(true);
                        i2 -= arrayOfLeadingMarginSpan[i33].getLeadingMargin(false);
                        if ((localLeadingMarginSpan instanceof LeadingMarginSpan.LeadingMarginSpan2))
                        {
                            LeadingMarginSpan.LeadingMarginSpan2 localLeadingMarginSpan2 = (LeadingMarginSpan.LeadingMarginSpan2)localLeadingMarginSpan;
                            n = getLineForOffset(localSpanned.getSpanStart(localLeadingMarginSpan2)) + localLeadingMarginSpan2.getLeadingMarginLineCount();
                        }
                    }
                    bool1 = false;
                    break;
                }
                arrayOfLineHeightSpan = (LineHeightSpan[])getParagraphSpans(localSpanned, j, m, LineHeightSpan.class);
                if (arrayOfLineHeightSpan.length != 0)
                {
                    if ((arrayOfInt == null) || (arrayOfInt.length < arrayOfLineHeightSpan.length))
                        arrayOfInt = new int[ArrayUtils.idealIntArraySize(arrayOfLineHeightSpan.length)];
                    int i35 = 0;
                    int i36 = arrayOfLineHeightSpan.length;
                    if (i35 < i36)
                    {
                        LineHeightSpan localLineHeightSpan = arrayOfLineHeightSpan[i35];
                        int i37 = localSpanned.getSpanStart(localLineHeightSpan);
                        if (i37 < j)
                            arrayOfInt[i35] = getLineTop(getLineForOffset(i37));
                        while (true)
                        {
                            i35++;
                            break;
                            arrayOfInt[i35] = i;
                        }
                    }
                }
                localMeasuredText.setPara(paramCharSequence, j, m, paramTextDirectionHeuristic);
                arrayOfChar = localMeasuredText.mChars;
                arrayOfFloat = localMeasuredText.mWidths;
                arrayOfByte = localMeasuredText.mLevels;
                i3 = localMeasuredText.mDir;
                bool2 = localMeasuredText.mEasy;
                int i4 = i1;
                f1 = 0.0F;
                i5 = j;
                int i6 = j;
                float f2 = 0.0F;
                int i7 = 0;
                int i8 = 0;
                int i9 = 0;
                int i10 = 0;
                int i11 = j;
                float f3 = 0.0F;
                i12 = 0;
                i13 = 0;
                i14 = 0;
                i15 = 0;
                bool3 = false;
                int i16 = 0;
                Layout.TabStops localTabStops = null;
                int i17 = j;
                label1196: if (i17 < m)
                {
                    int i18;
                    label496: int i20;
                    int i21;
                    int i22;
                    int i23;
                    if (localSpanned == null)
                    {
                        i18 = m;
                        localMeasuredText.addStyleRun(paramTextPaint, i18 - i17, localFontMetricsInt);
                        i20 = localFontMetricsInt.top;
                        i21 = localFontMetricsInt.bottom;
                        i22 = localFontMetricsInt.ascent;
                        i23 = localFontMetricsInt.descent;
                    }
                    label942: label1505: for (int i24 = i17; ; i24++)
                    {
                        int i25;
                        if (i24 < i18)
                        {
                            i25 = arrayOfChar[(i24 - j)];
                            if (i25 != 10)
                                break label942;
                        }
                        while (true)
                        {
                            if (f1 > i4)
                                break label1196;
                            f3 = f1;
                            i11 = i24 + 1;
                            if (i20 < i14)
                                i14 = i20;
                            if (i22 < i12)
                                i12 = i22;
                            if (i23 > i13)
                                i13 = i23;
                            if (i21 > i15)
                                i15 = i21;
                            int i31 = Injector.validateCJKCharAsSpace(i25, arrayOfChar, i24, i18, j);
                            if ((i31 == 32) || (i31 == 9) || (((i31 != 46) && (i31 != 44) && (i31 != 58) && (i31 != 59)) || ((i24 - 1 >= i5) && (Character.isDigit(arrayOfChar[(i24 - 1 - j)]))) || ((i24 + 1 >= i18) || (!Character.isDigit(arrayOfChar[(i24 + 1 - j)])) || (((i31 != 47) && (i31 != 45)) || ((i24 + 1 >= i18) || (!Character.isDigit(arrayOfChar[(i24 + 1 - j)])) || ((i31 >= 11904) && (isIdeographic(i31, true)) && (i24 + 1 < i18) && (isIdeographic(arrayOfChar[(i24 + 1 - j)], false))))))))
                            {
                                f2 = f1;
                                i6 = i24 + 1;
                                if (i14 < i9)
                                    i9 = i14;
                                if (i12 < i7)
                                    i7 = i12;
                                if (i13 > i8)
                                    i8 = i13;
                                if (i15 > i10)
                                    i10 = i15;
                            }
                            if (this.mLineCount < this.mMaximumVisibleLineCount)
                                break label1505;
                            label869: i17 = i18;
                            break;
                            i18 = localSpanned.nextSpanTransition(i17, m, MetricAffectingSpan.class);
                            int i19 = i18 - i17;
                            localMeasuredText.addStyleRun(paramTextPaint, (MetricAffectingSpan[])TextUtils.removeEmptySpans((MetricAffectingSpan[])localSpanned.getSpans(i17, i18, MetricAffectingSpan.class), localSpanned, MetricAffectingSpan.class), i19, localFontMetricsInt);
                            break label496;
                            if (i25 == 9)
                            {
                                if (i16 == 0)
                                {
                                    i16 = 1;
                                    bool3 = true;
                                    if (localSpanned != null)
                                    {
                                        TabStopSpan[] arrayOfTabStopSpan = (TabStopSpan[])getParagraphSpans(localSpanned, j, m, TabStopSpan.class);
                                        if (arrayOfTabStopSpan.length > 0)
                                            localTabStops = new Layout.TabStops(20, arrayOfTabStopSpan);
                                    }
                                }
                                if (localTabStops != null)
                                    f1 = localTabStops.nextTab(f1);
                                else
                                    f1 = Layout.TabStops.nextDefaultStop(f1, 20);
                            }
                            else if ((i25 >= 55296) && (i25 <= 57343) && (i24 + 1 < i18))
                            {
                                int i32 = Character.codePointAt(arrayOfChar, i24 - j);
                                if ((i32 >= MIN_EMOJI) && (i32 <= MAX_EMOJI))
                                {
                                    Bitmap localBitmap = EMOJI_FACTORY.getBitmapFromAndroidPua(i32);
                                    if (localBitmap != null)
                                    {
                                        if (localSpanned == null);
                                        for (TextPaint localTextPaint = paramTextPaint; ; localTextPaint = this.mWorkPaint)
                                        {
                                            f1 += localBitmap.getWidth() * -localTextPaint.ascent() / localBitmap.getHeight();
                                            bool3 = true;
                                            i24++;
                                            break;
                                        }
                                    }
                                    f1 += arrayOfFloat[(i24 - j)];
                                }
                                else
                                {
                                    f1 += arrayOfFloat[(i24 - j)];
                                }
                            }
                            else
                            {
                                f1 += arrayOfFloat[(i24 - j)];
                            }
                        }
                        boolean bool5;
                        if (i24 + 1 < i18)
                            bool5 = true;
                        int i26;
                        int i27;
                        int i28;
                        int i29;
                        int i30;
                        float f4;
                        while (i6 != i5)
                        {
                            if (i25 == 32)
                                i6 = i24 + 1;
                            while (true)
                                if ((i6 < i18) && (arrayOfChar[(i6 - j)] == ' '))
                                {
                                    i6++;
                                    continue;
                                    bool5 = false;
                                    break;
                                }
                            i26 = i6;
                            i27 = i7;
                            i28 = i8;
                            i29 = i9;
                            i30 = i10;
                            f4 = f2;
                        }
                        while (true)
                        {
                            i = out(paramCharSequence, i5, i26, i27, i28, i29, i30, i, paramFloat1, paramFloat2, arrayOfLineHeightSpan, arrayOfInt, localFontMetricsInt, bool3, bool1, arrayOfByte, i3, bool2, paramInt2, paramBoolean1, paramBoolean2, arrayOfChar, arrayOfFloat, j, paramTruncateAt, paramFloat3, f4, paramTextPaint, bool5);
                            i5 = i26;
                            i24 = i5 - 1;
                            i11 = i5;
                            i6 = i5;
                            f1 = 0.0F;
                            i15 = 0;
                            i14 = 0;
                            i13 = 0;
                            i12 = 0;
                            i10 = 0;
                            i9 = 0;
                            i8 = 0;
                            i7 = 0;
                            n--;
                            if (n <= 0)
                                i4 = i2;
                            if (i5 >= i17)
                                break;
                            localMeasuredText.setPos(i5);
                            i18 = i5;
                            break label869;
                            if (i11 != i5)
                            {
                                i26 = i11;
                                i27 = i12;
                                i28 = i13;
                                i29 = i14;
                                i30 = i15;
                                f4 = f3;
                            }
                            else
                            {
                                i26 = i5 + 1;
                                i27 = localFontMetricsInt.ascent;
                                i28 = localFontMetricsInt.descent;
                                i29 = localFontMetricsInt.top;
                                i30 = localFontMetricsInt.bottom;
                                f4 = arrayOfFloat[(i5 - j)];
                            }
                        }
                    }
                }
                if ((m != i5) && (this.mLineCount < this.mMaximumVisibleLineCount))
                {
                    if ((i12 | (i13 | (i14 | i15))) == 0)
                    {
                        paramTextPaint.getFontMetricsInt(localFontMetricsInt);
                        i14 = localFontMetricsInt.top;
                        i15 = localFontMetricsInt.bottom;
                        i12 = localFontMetricsInt.ascent;
                        i13 = localFontMetricsInt.descent;
                    }
                    if (m == paramInt2)
                        break label1758;
                }
            }
            label1758: for (boolean bool4 = true; ; bool4 = false)
            {
                i = out(paramCharSequence, i5, m, i12, i13, i14, i15, i, paramFloat1, paramFloat2, arrayOfLineHeightSpan, arrayOfInt, localFontMetricsInt, bool3, bool1, arrayOfByte, i3, bool2, paramInt2, paramBoolean1, paramBoolean2, arrayOfChar, arrayOfFloat, j, paramTruncateAt, paramFloat3, f1, paramTextPaint, bool4);
                if (m != paramInt2)
                    break;
                if (((paramInt2 == paramInt1) || (paramCharSequence.charAt(paramInt2 - 1) == '\n')) && (this.mLineCount < this.mMaximumVisibleLineCount))
                {
                    paramTextPaint.getFontMetricsInt(localFontMetricsInt);
                    out(paramCharSequence, paramInt2, paramInt2, localFontMetricsInt.ascent, localFontMetricsInt.descent, localFontMetricsInt.top, localFontMetricsInt.bottom, i, paramFloat1, paramFloat2, null, null, localFontMetricsInt, false, bool1, null, 1, true, paramInt2, paramBoolean1, paramBoolean2, null, null, paramInt1, paramTruncateAt, paramFloat3, 0.0F, paramTextPaint, false);
                }
                return;
            }
        }
    }

    public int getBottomPadding()
    {
        return this.mBottomPadding;
    }

    public int getEllipsisCount(int paramInt)
    {
        if (this.mColumns < 5);
        for (int i = 0; ; i = this.mLines[(4 + paramInt * this.mColumns)])
            return i;
    }

    public int getEllipsisStart(int paramInt)
    {
        if (this.mColumns < 5);
        for (int i = 0; ; i = this.mLines[(3 + paramInt * this.mColumns)])
            return i;
    }

    public int getEllipsizedWidth()
    {
        return this.mEllipsizedWidth;
    }

    public boolean getLineContainsTab(int paramInt)
    {
        if ((0x20000000 & this.mLines[(0 + paramInt * this.mColumns)]) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int getLineCount()
    {
        return this.mLineCount;
    }

    public int getLineDescent(int paramInt)
    {
        int i = this.mLines[(2 + paramInt * this.mColumns)];
        if ((this.mMaximumVisibleLineCount > 0) && (paramInt >= -1 + this.mMaximumVisibleLineCount) && (paramInt != this.mLineCount))
            i += getBottomPadding();
        return i;
    }

    public final Layout.Directions getLineDirections(int paramInt)
    {
        return this.mLineDirections[paramInt];
    }

    public int getLineForVertical(int paramInt)
    {
        int i = this.mLineCount;
        int j = -1;
        int[] arrayOfInt = this.mLines;
        while (i - j > 1)
        {
            int k = i + j >> 1;
            if (arrayOfInt[(1 + k * this.mColumns)] > paramInt)
                i = k;
            else
                j = k;
        }
        if (j < 0)
            j = 0;
        return j;
    }

    public int getLineStart(int paramInt)
    {
        return 0x1FFFFFFF & this.mLines[(0 + paramInt * this.mColumns)];
    }

    public int getLineTop(int paramInt)
    {
        int i = this.mLines[(1 + paramInt * this.mColumns)];
        if ((this.mMaximumVisibleLineCount > 0) && (paramInt >= this.mMaximumVisibleLineCount) && (paramInt != this.mLineCount))
            i += getBottomPadding();
        return i;
    }

    public int getParagraphDirection(int paramInt)
    {
        return this.mLines[(0 + paramInt * this.mColumns)] >> 30;
    }

    public int getTopPadding()
    {
        return this.mTopPadding;
    }

    void prepare()
    {
        this.mMeasured = MeasuredText.obtain();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static char CHAR_UNKNOWN = '⹿';

        static boolean isIdeographic(char paramChar, boolean paramBoolean)
        {
            if ((paramChar >= '、') && (paramChar <= '〗'));
            switch (paramChar)
            {
            default:
                switch (paramChar)
                {
                default:
                    if (paramChar == 65117)
                        paramBoolean = false;
                    break;
                case '、':
                case '。':
                case '》':
                case '』':
                case '】':
                case '〗':
                }
                break;
            case '《':
            case '『':
            case '【':
            case '〖':
            }
            while (true)
            {
                return paramBoolean;
                paramBoolean = false;
                continue;
                if (paramChar != 65118)
                    if ((paramChar >= 65281) && (paramChar <= 65311))
                    {
                        if (paramChar == 65288)
                            paramBoolean = false;
                        else
                            switch (paramChar)
                            {
                            case '！':
                            case '）':
                            case '，':
                            case '：':
                            case '；':
                            case '？':
                            }
                    }
                    else
                        paramBoolean = false;
            }
        }

        static char validateCJKCharAsSpace(char paramChar, char[] paramArrayOfChar, int paramInt1, int paramInt2, int paramInt3)
        {
            if (((paramChar >= '⺀') && (isIdeographic(paramChar, true)) && (paramInt1 + 1 < paramInt2) && ((paramArrayOfChar[(paramInt1 + 1 - paramInt3)] < '⺀') || (isIdeographic(paramArrayOfChar[(paramInt1 + 1 - paramInt3)], false)))) || ((paramChar < '⺀') && (paramInt1 + 1 < paramInt2) && (paramArrayOfChar[(paramInt1 + 1 - paramInt3)] >= '⺀') && (!isIdeographic(paramChar, true)) && (isIdeographic(paramArrayOfChar[(paramInt1 + 1 - paramInt3)], false))));
            for (paramChar = ' '; ; paramChar = CHAR_UNKNOWN)
                do
                    return paramChar;
                while ((paramChar < '⺀') || (!isIdeographic(paramChar, true)) || (paramInt1 + 1 >= paramInt2) || (!isIdeographic(paramArrayOfChar[(paramInt1 + 1 - paramInt3)], false)));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.StaticLayout
 * JD-Core Version:        0.6.2
 */