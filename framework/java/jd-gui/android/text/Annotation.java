package android.text;

import android.os.Parcel;

public class Annotation
    implements ParcelableSpan
{
    private final String mKey;
    private final String mValue;

    public Annotation(Parcel paramParcel)
    {
        this.mKey = paramParcel.readString();
        this.mValue = paramParcel.readString();
    }

    public Annotation(String paramString1, String paramString2)
    {
        this.mKey = paramString1;
        this.mValue = paramString2;
    }

    public int describeContents()
    {
        return 0;
    }

    public String getKey()
    {
        return this.mKey;
    }

    public int getSpanTypeId()
    {
        return 18;
    }

    public String getValue()
    {
        return this.mValue;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mKey);
        paramParcel.writeString(this.mValue);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.Annotation
 * JD-Core Version:        0.6.2
 */