package android.text;

public final class SpannedString extends SpannableStringInternal
    implements CharSequence, GetChars, Spanned
{
    public SpannedString(CharSequence paramCharSequence)
    {
        super(paramCharSequence, 0, paramCharSequence.length());
    }

    private SpannedString(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        super(paramCharSequence, paramInt1, paramInt2);
    }

    public static SpannedString valueOf(CharSequence paramCharSequence)
    {
        if ((paramCharSequence instanceof SpannedString));
        for (SpannedString localSpannedString = (SpannedString)paramCharSequence; ; localSpannedString = new SpannedString(paramCharSequence))
            return localSpannedString;
    }

    public CharSequence subSequence(int paramInt1, int paramInt2)
    {
        return new SpannedString(this, paramInt1, paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.SpannedString
 * JD-Core Version:        0.6.2
 */