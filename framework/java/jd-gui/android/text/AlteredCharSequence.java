package android.text;

public class AlteredCharSequence
    implements CharSequence, GetChars
{
    private char[] mChars;
    private int mEnd;
    private CharSequence mSource;
    private int mStart;

    private AlteredCharSequence(CharSequence paramCharSequence, char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        this.mSource = paramCharSequence;
        this.mChars = paramArrayOfChar;
        this.mStart = paramInt1;
        this.mEnd = paramInt2;
    }

    public static AlteredCharSequence make(CharSequence paramCharSequence, char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        if ((paramCharSequence instanceof Spanned));
        for (Object localObject = new AlteredSpanned(paramCharSequence, paramArrayOfChar, paramInt1, paramInt2, null); ; localObject = new AlteredCharSequence(paramCharSequence, paramArrayOfChar, paramInt1, paramInt2))
            return localObject;
    }

    public char charAt(int paramInt)
    {
        if ((paramInt >= this.mStart) && (paramInt < this.mEnd));
        for (char c = this.mChars[(paramInt - this.mStart)]; ; c = this.mSource.charAt(paramInt))
            return c;
    }

    public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
    {
        TextUtils.getChars(this.mSource, paramInt1, paramInt2, paramArrayOfChar, paramInt3);
        int i = Math.max(this.mStart, paramInt1);
        int j = Math.min(this.mEnd, paramInt2);
        if (i > j)
            System.arraycopy(this.mChars, i - this.mStart, paramArrayOfChar, paramInt3, j - i);
    }

    public int length()
    {
        return this.mSource.length();
    }

    public CharSequence subSequence(int paramInt1, int paramInt2)
    {
        return make(this.mSource.subSequence(paramInt1, paramInt2), this.mChars, this.mStart - paramInt1, this.mEnd - paramInt1);
    }

    public String toString()
    {
        int i = length();
        char[] arrayOfChar = new char[i];
        getChars(0, i, arrayOfChar, 0);
        return String.valueOf(arrayOfChar);
    }

    void update(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        this.mChars = paramArrayOfChar;
        this.mStart = paramInt1;
        this.mEnd = paramInt2;
    }

    private static class AlteredSpanned extends AlteredCharSequence
        implements Spanned
    {
        private Spanned mSpanned;

        private AlteredSpanned(CharSequence paramCharSequence, char[] paramArrayOfChar, int paramInt1, int paramInt2)
        {
            super(paramArrayOfChar, paramInt1, paramInt2, null);
            this.mSpanned = ((Spanned)paramCharSequence);
        }

        public int getSpanEnd(Object paramObject)
        {
            return this.mSpanned.getSpanEnd(paramObject);
        }

        public int getSpanFlags(Object paramObject)
        {
            return this.mSpanned.getSpanFlags(paramObject);
        }

        public int getSpanStart(Object paramObject)
        {
            return this.mSpanned.getSpanStart(paramObject);
        }

        public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass)
        {
            return this.mSpanned.getSpans(paramInt1, paramInt2, paramClass);
        }

        public int nextSpanTransition(int paramInt1, int paramInt2, Class paramClass)
        {
            return this.mSpanned.nextSpanTransition(paramInt1, paramInt2, paramClass);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.AlteredCharSequence
 * JD-Core Version:        0.6.2
 */