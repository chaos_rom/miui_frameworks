package android.text;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.AlignmentSpan.Standard;
import android.text.style.BackgroundColorSpan;
import android.text.style.BulletSpan;
import android.text.style.CharacterStyle;
import android.text.style.EasyEditSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.LeadingMarginSpan.Standard;
import android.text.style.MetricAffectingSpan;
import android.text.style.QuoteSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.ReplacementSpan;
import android.text.style.ScaleXSpan;
import android.text.style.SpellCheckSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuggestionRangeSpan;
import android.text.style.SuggestionSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.Printer;
import com.android.internal.util.ArrayUtils;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.regex.Pattern;

public class TextUtils
{
    public static final int ABSOLUTE_SIZE_SPAN = 16;
    public static final int ALIGNMENT_SPAN = 1;
    public static final int ANNOTATION = 18;
    public static final int BACKGROUND_COLOR_SPAN = 12;
    public static final int BULLET_SPAN = 8;
    public static final int CAP_MODE_CHARACTERS = 4096;
    public static final int CAP_MODE_SENTENCES = 16384;
    public static final int CAP_MODE_WORDS = 8192;
    public static final Parcelable.Creator<CharSequence> CHAR_SEQUENCE_CREATOR = new Parcelable.Creator()
    {
        public CharSequence createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = paramAnonymousParcel.readInt();
            Object localObject = paramAnonymousParcel.readString();
            if (localObject == null)
                localObject = null;
            while (i == 1)
                return localObject;
            SpannableString localSpannableString = new SpannableString((CharSequence)localObject);
            while (true)
            {
                int j = paramAnonymousParcel.readInt();
                if (j == 0)
                {
                    localObject = localSpannableString;
                    break;
                }
                switch (j)
                {
                default:
                    throw new RuntimeException("bogus span encoding " + j);
                case 1:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new AlignmentSpan.Standard(paramAnonymousParcel));
                    break;
                case 2:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new ForegroundColorSpan(paramAnonymousParcel));
                    break;
                case 3:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new RelativeSizeSpan(paramAnonymousParcel));
                    break;
                case 4:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new ScaleXSpan(paramAnonymousParcel));
                    break;
                case 5:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new StrikethroughSpan(paramAnonymousParcel));
                    break;
                case 6:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new UnderlineSpan(paramAnonymousParcel));
                    break;
                case 7:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new StyleSpan(paramAnonymousParcel));
                    break;
                case 8:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new BulletSpan(paramAnonymousParcel));
                    break;
                case 9:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new QuoteSpan(paramAnonymousParcel));
                    break;
                case 10:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new LeadingMarginSpan.Standard(paramAnonymousParcel));
                    break;
                case 11:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new URLSpan(paramAnonymousParcel));
                    break;
                case 12:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new BackgroundColorSpan(paramAnonymousParcel));
                    break;
                case 13:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new TypefaceSpan(paramAnonymousParcel));
                    break;
                case 14:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new SuperscriptSpan(paramAnonymousParcel));
                    break;
                case 15:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new SubscriptSpan(paramAnonymousParcel));
                    break;
                case 16:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new AbsoluteSizeSpan(paramAnonymousParcel));
                    break;
                case 17:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new TextAppearanceSpan(paramAnonymousParcel));
                    break;
                case 18:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new Annotation(paramAnonymousParcel));
                    break;
                case 19:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new SuggestionSpan(paramAnonymousParcel));
                    break;
                case 20:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new SpellCheckSpan(paramAnonymousParcel));
                    break;
                case 21:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new SuggestionRangeSpan(paramAnonymousParcel));
                    break;
                case 22:
                    TextUtils.readSpan(paramAnonymousParcel, localSpannableString, new EasyEditSpan());
                }
            }
        }

        public CharSequence[] newArray(int paramAnonymousInt)
        {
            return new CharSequence[paramAnonymousInt];
        }
    };
    public static final int EASY_EDIT_SPAN = 22;
    private static final String ELLIPSIS_NORMAL = Resources.getSystem().getString(17039556);
    private static final String ELLIPSIS_TWO_DOTS = Resources.getSystem().getString(17039557);
    private static String[] EMPTY_STRING_ARRAY;
    private static final char FIRST_RIGHT_TO_LEFT = '֐';
    public static final int FOREGROUND_COLOR_SPAN = 2;
    public static final int LEADING_MARGIN_SPAN = 10;
    public static final int QUOTE_SPAN = 9;
    public static final int RELATIVE_SIZE_SPAN = 3;
    public static final int SCALE_X_SPAN = 4;
    public static final int SPELL_CHECK_SPAN = 20;
    public static final int STRIKETHROUGH_SPAN = 5;
    public static final int STYLE_SPAN = 7;
    public static final int SUBSCRIPT_SPAN = 15;
    public static final int SUGGESTION_RANGE_SPAN = 21;
    public static final int SUGGESTION_SPAN = 19;
    public static final int SUPERSCRIPT_SPAN = 14;
    public static final int TEXT_APPEARANCE_SPAN = 17;
    public static final int TYPEFACE_SPAN = 13;
    public static final int UNDERLINE_SPAN = 6;
    public static final int URL_SPAN = 11;
    private static final char ZWNBS_CHAR = '﻿';
    private static Object sLock = new Object();
    private static char[] sTemp = null;

    static
    {
        EMPTY_STRING_ARRAY = new String[0];
    }

    public static CharSequence commaEllipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, String paramString1, String paramString2)
    {
        return commaEllipsize(paramCharSequence, paramTextPaint, paramFloat, paramString1, paramString2, TextDirectionHeuristics.FIRSTSTRONG_LTR);
    }

    public static CharSequence commaEllipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, String paramString1, String paramString2, TextDirectionHeuristic paramTextDirectionHeuristic)
    {
        MeasuredText localMeasuredText1 = MeasuredText.obtain();
        while (true)
        {
            int k;
            int i3;
            try
            {
                int i = paramCharSequence.length();
                float f = setPara(localMeasuredText1, paramTextPaint, paramCharSequence, 0, i, paramTextDirectionHeuristic);
                if (f <= paramFloat)
                    return paramCharSequence;
                char[] arrayOfChar = localMeasuredText1.mChars;
                int j = 0;
                k = 0;
                if (k < i)
                {
                    if (arrayOfChar[k] == ',')
                        j++;
                }
                else
                {
                    int m = j + 1;
                    int n = 0;
                    Object localObject2 = "";
                    int i1 = 0;
                    int i2 = 0;
                    float[] arrayOfFloat = localMeasuredText1.mWidths;
                    MeasuredText localMeasuredText2 = MeasuredText.obtain();
                    i3 = 0;
                    if (i3 < i)
                    {
                        i1 = (int)(i1 + arrayOfFloat[i3]);
                        if (arrayOfChar[i3] != ',')
                            break label329;
                        i2++;
                        m--;
                        if (m == 1)
                        {
                            str = " " + paramString1;
                            localMeasuredText2.setPara(str, 0, str.length(), paramTextDirectionHeuristic);
                            if (localMeasuredText2.addStyleRun(paramTextPaint, localMeasuredText2.mLen, null) + i1 > paramFloat)
                                break label329;
                            n = i3 + 1;
                            localObject2 = str;
                            break label329;
                        }
                        StringBuilder localStringBuilder = new StringBuilder().append(" ");
                        Object[] arrayOfObject = new Object[1];
                        arrayOfObject[0] = Integer.valueOf(m);
                        String str = String.format(paramString2, arrayOfObject);
                        continue;
                    }
                    MeasuredText.recycle(localMeasuredText2);
                    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder((CharSequence)localObject2);
                    localSpannableStringBuilder.insert(0, paramCharSequence, 0, n);
                    MeasuredText.recycle(localMeasuredText1);
                    paramCharSequence = localSpannableStringBuilder;
                    continue;
                }
            }
            finally
            {
                MeasuredText.recycle(localMeasuredText1);
            }
            k++;
            continue;
            label329: i3++;
        }
    }

    public static CharSequence concat(CharSequence[] paramArrayOfCharSequence)
    {
        Object localObject;
        if (paramArrayOfCharSequence.length == 0)
            localObject = "";
        while (true)
        {
            return localObject;
            if (paramArrayOfCharSequence.length == 1)
            {
                localObject = paramArrayOfCharSequence[0];
            }
            else
            {
                int i = 0;
                StringBuilder localStringBuilder;
                for (int j = 0; ; j++)
                    if (j < paramArrayOfCharSequence.length)
                    {
                        if ((paramArrayOfCharSequence[j] instanceof Spanned))
                            i = 1;
                    }
                    else
                    {
                        localStringBuilder = new StringBuilder();
                        for (int k = 0; k < paramArrayOfCharSequence.length; k++)
                            localStringBuilder.append(paramArrayOfCharSequence[k]);
                    }
                if (i == 0)
                {
                    localObject = localStringBuilder.toString();
                }
                else
                {
                    SpannableString localSpannableString = new SpannableString(localStringBuilder);
                    int m = 0;
                    for (int n = 0; n < paramArrayOfCharSequence.length; n++)
                    {
                        int i1 = paramArrayOfCharSequence[n].length();
                        if ((paramArrayOfCharSequence[n] instanceof Spanned))
                            copySpansFrom((Spanned)paramArrayOfCharSequence[n], 0, i1, Object.class, localSpannableString, m);
                        m += i1;
                    }
                    localObject = new SpannedString(localSpannableString);
                }
            }
        }
    }

    public static void copySpansFrom(Spanned paramSpanned, int paramInt1, int paramInt2, Class paramClass, Spannable paramSpannable, int paramInt3)
    {
        if (paramClass == null)
            paramClass = Object.class;
        Object[] arrayOfObject = paramSpanned.getSpans(paramInt1, paramInt2, paramClass);
        for (int i = 0; i < arrayOfObject.length; i++)
        {
            int j = paramSpanned.getSpanStart(arrayOfObject[i]);
            int k = paramSpanned.getSpanEnd(arrayOfObject[i]);
            int m = paramSpanned.getSpanFlags(arrayOfObject[i]);
            if (j < paramInt1)
                j = paramInt1;
            if (k > paramInt2)
                k = paramInt2;
            paramSpannable.setSpan(arrayOfObject[i], paramInt3 + (j - paramInt1), paramInt3 + (k - paramInt1), m);
        }
    }

    public static boolean delimitedStringContains(String paramString1, char paramChar, String paramString2)
    {
        boolean bool = true;
        if ((isEmpty(paramString1)) || (isEmpty(paramString2)))
            bool = false;
        while (true)
        {
            return bool;
            int i = -1;
            int j = paramString1.length();
            int k;
            do
            {
                do
                {
                    i = paramString1.indexOf(paramString2, i + 1);
                    if (i == -1)
                        break;
                }
                while ((i > 0) && (paramString1.charAt(i - 1) != paramChar));
                k = i + paramString2.length();
                if (k == j)
                    break;
            }
            while (paramString1.charAt(k) != paramChar);
            continue;
            bool = false;
        }
    }

    static boolean doesNotNeedBidi(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        if (i < paramInt2)
            if (paramCharSequence.charAt(i) < '֐');
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            i++;
            break;
        }
    }

    static boolean doesNotNeedBidi(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        int j = i + paramInt2;
        if (i < j)
            if (paramArrayOfChar[i] < '֐');
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            i++;
            break;
        }
    }

    public static void dumpSpans(CharSequence paramCharSequence, Printer paramPrinter, String paramString)
    {
        if ((paramCharSequence instanceof Spanned))
        {
            Spanned localSpanned = (Spanned)paramCharSequence;
            Object[] arrayOfObject = localSpanned.getSpans(0, paramCharSequence.length(), Object.class);
            for (int i = 0; i < arrayOfObject.length; i++)
            {
                Object localObject = arrayOfObject[i];
                paramPrinter.println(paramString + paramCharSequence.subSequence(localSpanned.getSpanStart(localObject), localSpanned.getSpanEnd(localObject)) + ": " + Integer.toHexString(System.identityHashCode(localObject)) + " " + localObject.getClass().getCanonicalName() + " (" + localSpanned.getSpanStart(localObject) + "-" + localSpanned.getSpanEnd(localObject) + ") fl=#" + localSpanned.getSpanFlags(localObject));
            }
        }
        paramPrinter.println(paramString + paramCharSequence + ": (no spans)");
    }

    public static CharSequence ellipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, TruncateAt paramTruncateAt)
    {
        return ellipsize(paramCharSequence, paramTextPaint, paramFloat, paramTruncateAt, false, null);
    }

    public static CharSequence ellipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, TruncateAt paramTruncateAt, boolean paramBoolean, EllipsizeCallback paramEllipsizeCallback)
    {
        TextDirectionHeuristic localTextDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
        if (paramTruncateAt == TruncateAt.END_SMALL);
        for (String str = ELLIPSIS_TWO_DOTS; ; str = ELLIPSIS_NORMAL)
            return ellipsize(paramCharSequence, paramTextPaint, paramFloat, paramTruncateAt, paramBoolean, paramEllipsizeCallback, localTextDirectionHeuristic, str);
    }

    public static CharSequence ellipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, TruncateAt paramTruncateAt, boolean paramBoolean, EllipsizeCallback paramEllipsizeCallback, TextDirectionHeuristic paramTextDirectionHeuristic, String paramString)
    {
        int i = paramCharSequence.length();
        MeasuredText localMeasuredText = MeasuredText.obtain();
        while (true)
        {
            int m;
            try
            {
                int j = paramCharSequence.length();
                if (setPara(localMeasuredText, paramTextPaint, paramCharSequence, 0, j, paramTextDirectionHeuristic) <= paramFloat)
                {
                    if (paramEllipsizeCallback != null)
                        paramEllipsizeCallback.ellipsized(0, 0);
                    return paramCharSequence;
                }
                float f1 = paramFloat - paramTextPaint.measureText(paramString);
                int k = i;
                char[] arrayOfChar;
                int n;
                float f2;
                if (f1 < 0.0F)
                {
                    m = 0;
                    if (paramEllipsizeCallback != null)
                        paramEllipsizeCallback.ellipsized(m, k);
                    arrayOfChar = localMeasuredText.mChars;
                    if (!(paramCharSequence instanceof Spanned))
                        break label518;
                    localSpanned = (Spanned)paramCharSequence;
                    n = i - (k - m);
                    if (!paramBoolean)
                        continue;
                    if (n <= 0)
                        break label507;
                    i2 = m + 1;
                    arrayOfChar[m] = paramString.charAt(0);
                    break label511;
                    if (i3 < k)
                    {
                        arrayOfChar[i3] = 65279;
                        i3++;
                        continue;
                    }
                }
                else
                {
                    if (paramTruncateAt == TruncateAt.START)
                    {
                        k = i - localMeasuredText.breakText(i, false, f1);
                        m = 0;
                        continue;
                    }
                    if ((paramTruncateAt == TruncateAt.END) || (paramTruncateAt == TruncateAt.END_SMALL))
                    {
                        m = localMeasuredText.breakText(i, true, f1);
                        continue;
                    }
                    k = i - localMeasuredText.breakText(i, false, f1 / 2.0F);
                    f2 = f1 - localMeasuredText.measure(k, i);
                    m = localMeasuredText.breakText(k, true, f2);
                    continue;
                }
                String str2 = new String(arrayOfChar, 0, i);
                if (localSpanned == null)
                {
                    MeasuredText.recycle(localMeasuredText);
                    paramCharSequence = str2;
                    continue;
                }
                SpannableString localSpannableString = new SpannableString(str2);
                copySpansFrom(localSpanned, 0, i, Object.class, localSpannableString, 0);
                MeasuredText.recycle(localMeasuredText);
                paramCharSequence = localSpannableString;
                continue;
                if (n == 0)
                {
                    paramCharSequence = "";
                    MeasuredText.recycle(localMeasuredText);
                    continue;
                }
                if (localSpanned == null)
                {
                    StringBuilder localStringBuilder = new StringBuilder(n + paramString.length());
                    localStringBuilder.append(arrayOfChar, 0, m);
                    localStringBuilder.append(paramString);
                    int i1 = i - k;
                    localStringBuilder.append(arrayOfChar, k, i1);
                    String str1 = localStringBuilder.toString();
                    paramCharSequence = str1;
                    MeasuredText.recycle(localMeasuredText);
                    continue;
                }
                SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder();
                localSpannableStringBuilder.append(paramCharSequence, 0, m);
                localSpannableStringBuilder.append(paramString);
                localSpannableStringBuilder.append(paramCharSequence, k, i);
                MeasuredText.recycle(localMeasuredText);
                paramCharSequence = localSpannableStringBuilder;
                continue;
            }
            finally
            {
                MeasuredText.recycle(localMeasuredText);
            }
            label507: int i2 = m;
            label511: int i3 = i2;
            continue;
            label518: Spanned localSpanned = null;
        }
    }

    public static boolean equals(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        boolean bool = true;
        if (paramCharSequence1 == paramCharSequence2);
        while (true)
        {
            return bool;
            if ((paramCharSequence1 != null) && (paramCharSequence2 != null))
            {
                int i = paramCharSequence1.length();
                if (i == paramCharSequence2.length())
                {
                    if (((paramCharSequence1 instanceof String)) && ((paramCharSequence2 instanceof String)))
                    {
                        bool = paramCharSequence1.equals(paramCharSequence2);
                        continue;
                    }
                    for (int j = 0; ; j++)
                    {
                        if (j >= i)
                            break label94;
                        if (paramCharSequence1.charAt(j) != paramCharSequence2.charAt(j))
                        {
                            bool = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                label94: bool = false;
            }
        }
    }

    public static CharSequence expandTemplate(CharSequence paramCharSequence, CharSequence[] paramArrayOfCharSequence)
    {
        if (paramArrayOfCharSequence.length > 9)
            throw new IllegalArgumentException("max of 9 values are supported");
        SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramCharSequence);
        int i = 0;
        try
        {
            while (i < localSpannableStringBuilder.length())
                if (localSpannableStringBuilder.charAt(i) == '^')
                {
                    char c = localSpannableStringBuilder.charAt(i + 1);
                    if (c == '^')
                    {
                        localSpannableStringBuilder.delete(i + 1, i + 2);
                        i++;
                    }
                    else if (Character.isDigit(c))
                    {
                        int j = -1 + Character.getNumericValue(c);
                        if (j < 0)
                            throw new IllegalArgumentException("template requests value ^" + (j + 1));
                        if (j >= paramArrayOfCharSequence.length)
                            throw new IllegalArgumentException("template requests value ^" + (j + 1) + "; only " + paramArrayOfCharSequence.length + " provided");
                        localSpannableStringBuilder.replace(i, i + 2, paramArrayOfCharSequence[j]);
                        int k = paramArrayOfCharSequence[j].length();
                        i += k;
                    }
                }
                else
                {
                    i++;
                }
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
        }
        return localSpannableStringBuilder;
    }

    public static int getCapsMode(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        int i;
        if (paramInt1 < 0)
            i = 0;
        int k;
        label183: int m;
        do
        {
            do
            {
                int j;
                do
                    while (true)
                    {
                        return i;
                        i = 0;
                        if ((paramInt2 & 0x1000) != 0)
                            i = 0x0 | 0x1000;
                        if ((paramInt2 & 0x6000) != 0)
                        {
                            for (j = paramInt1; ; j--)
                                if (j > 0)
                                {
                                    char c3 = paramCharSequence.charAt(j - 1);
                                    if ((c3 == '"') || (c3 == '\'') || (Character.getType(c3) == 21));
                                }
                                else
                                {
                                    for (k = j; k > 0; k--)
                                    {
                                        int i1 = paramCharSequence.charAt(k - 1);
                                        if ((i1 != 32) && (i1 != 9))
                                            break;
                                    }
                                }
                            if ((k == 0) || (paramCharSequence.charAt(k - 1) == '\n'))
                            {
                                i |= 8192;
                            }
                            else
                            {
                                if ((paramInt2 & 0x4000) != 0)
                                    break;
                                if (j != k)
                                    i |= 8192;
                            }
                        }
                    }
                while (j == k);
                if (k > 0)
                {
                    char c2 = paramCharSequence.charAt(k - 1);
                    if ((c2 == '"') || (c2 == '\'') || (Character.getType(c2) == 22))
                        break;
                }
            }
            while (k <= 0);
            m = paramCharSequence.charAt(k - 1);
        }
        while ((m != 46) && (m != 63) && (m != 33));
        if (m == 46);
        for (int n = k - 2; ; n--)
        {
            if (n >= 0)
            {
                char c1 = paramCharSequence.charAt(n);
                if (c1 == '.')
                    break;
                if (Character.isLetter(c1))
                    continue;
            }
            i |= 16384;
            break;
            k--;
            break label183;
        }
    }

    public static void getChars(CharSequence paramCharSequence, int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
    {
        Class localClass = paramCharSequence.getClass();
        if (localClass == String.class)
            ((String)paramCharSequence).getChars(paramInt1, paramInt2, paramArrayOfChar, paramInt3);
        int j;
        while (true)
        {
            return;
            if (localClass == StringBuffer.class)
            {
                ((StringBuffer)paramCharSequence).getChars(paramInt1, paramInt2, paramArrayOfChar, paramInt3);
            }
            else if (localClass == StringBuilder.class)
            {
                ((StringBuilder)paramCharSequence).getChars(paramInt1, paramInt2, paramArrayOfChar, paramInt3);
            }
            else if ((paramCharSequence instanceof GetChars))
            {
                ((GetChars)paramCharSequence).getChars(paramInt1, paramInt2, paramArrayOfChar, paramInt3);
            }
            else
            {
                int i = paramInt1;
                int k;
                for (j = paramInt3; i < paramInt2; j = k)
                {
                    k = j + 1;
                    paramArrayOfChar[j] = paramCharSequence.charAt(i);
                    i++;
                }
            }
        }
    }

    public static int getOffsetAfter(CharSequence paramCharSequence, int paramInt)
    {
        int i = paramCharSequence.length();
        if (paramInt == i);
        while (true)
        {
            return i;
            if (paramInt != i - 1)
            {
                int j = paramCharSequence.charAt(paramInt);
                int k;
                if ((j >= 55296) && (j <= 56319))
                {
                    int i2 = paramCharSequence.charAt(paramInt + 1);
                    if ((i2 >= 56320) && (i2 <= 57343))
                        k = paramInt + 2;
                }
                while ((paramCharSequence instanceof Spanned))
                {
                    ReplacementSpan[] arrayOfReplacementSpan = (ReplacementSpan[])((Spanned)paramCharSequence).getSpans(k, k, ReplacementSpan.class);
                    for (int m = 0; m < arrayOfReplacementSpan.length; m++)
                    {
                        int n = ((Spanned)paramCharSequence).getSpanStart(arrayOfReplacementSpan[m]);
                        int i1 = ((Spanned)paramCharSequence).getSpanEnd(arrayOfReplacementSpan[m]);
                        if ((n < k) && (i1 > k))
                            k = i1;
                    }
                    k = paramInt + 1;
                    continue;
                    k = paramInt + 1;
                }
                i = k;
            }
        }
    }

    public static int getOffsetBefore(CharSequence paramCharSequence, int paramInt)
    {
        int i = 0;
        if (paramInt == 0);
        while (true)
        {
            return i;
            if (paramInt != 1)
            {
                int j = paramCharSequence.charAt(paramInt - 1);
                int k;
                if ((j >= 56320) && (j <= 57343))
                {
                    int i2 = paramCharSequence.charAt(paramInt - 2);
                    if ((i2 >= 55296) && (i2 <= 56319))
                        k = paramInt - 2;
                }
                while ((paramCharSequence instanceof Spanned))
                {
                    ReplacementSpan[] arrayOfReplacementSpan = (ReplacementSpan[])((Spanned)paramCharSequence).getSpans(k, k, ReplacementSpan.class);
                    for (int m = 0; m < arrayOfReplacementSpan.length; m++)
                    {
                        int n = ((Spanned)paramCharSequence).getSpanStart(arrayOfReplacementSpan[m]);
                        int i1 = ((Spanned)paramCharSequence).getSpanEnd(arrayOfReplacementSpan[m]);
                        if ((n < k) && (i1 > k))
                            k = n;
                    }
                    k = paramInt - 1;
                    continue;
                    k = paramInt - 1;
                }
                i = k;
            }
        }
    }

    public static CharSequence getReverse(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        return new Reverser(paramCharSequence, paramInt1, paramInt2);
    }

    public static int getTrimmedLength(CharSequence paramCharSequence)
    {
        int i = paramCharSequence.length();
        for (int j = 0; (j < i) && (paramCharSequence.charAt(j) <= ' '); j++);
        for (int k = i; (k > j) && (paramCharSequence.charAt(k - 1) <= ' '); k--);
        return k - j;
    }

    public static String htmlEncode(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = 0;
        if (i < paramString.length())
        {
            char c = paramString.charAt(i);
            switch (c)
            {
            default:
                localStringBuilder.append(c);
            case '<':
            case '>':
            case '&':
            case '\'':
            case '"':
            }
            while (true)
            {
                i++;
                break;
                localStringBuilder.append("&lt;");
                continue;
                localStringBuilder.append("&gt;");
                continue;
                localStringBuilder.append("&amp;");
                continue;
                localStringBuilder.append("&#39;");
                continue;
                localStringBuilder.append("&quot;");
            }
        }
        return localStringBuilder.toString();
    }

    public static int indexOf(CharSequence paramCharSequence, char paramChar)
    {
        return indexOf(paramCharSequence, paramChar, 0);
    }

    public static int indexOf(CharSequence paramCharSequence, char paramChar, int paramInt)
    {
        if (paramCharSequence.getClass() == String.class);
        for (int i = ((String)paramCharSequence).indexOf(paramChar, paramInt); ; i = indexOf(paramCharSequence, paramChar, paramInt, paramCharSequence.length()))
            return i;
    }

    public static int indexOf(CharSequence paramCharSequence, char paramChar, int paramInt1, int paramInt2)
    {
        Class localClass = paramCharSequence.getClass();
        char[] arrayOfChar;
        int j;
        int m;
        label83: int i;
        if (((paramCharSequence instanceof GetChars)) || (localClass == StringBuffer.class) || (localClass == StringBuilder.class) || (localClass == String.class))
        {
            arrayOfChar = obtain(500);
            if (paramInt1 < paramInt2)
            {
                j = paramInt1 + 500;
                if (j > paramInt2)
                    j = paramInt2;
                getChars(paramCharSequence, paramInt1, j, arrayOfChar, 0);
                int k = j - paramInt1;
                m = 0;
                if (m < k)
                    if (arrayOfChar[m] == paramChar)
                    {
                        recycle(arrayOfChar);
                        i = m + paramInt1;
                    }
            }
        }
        while (true)
        {
            return i;
            m++;
            break label83;
            paramInt1 = j;
            break;
            recycle(arrayOfChar);
            i = -1;
            continue;
            for (i = paramInt1; ; i++)
            {
                if (i >= paramInt2)
                    break label164;
                if (paramCharSequence.charAt(i) == paramChar)
                    break;
            }
            label164: i = -1;
        }
    }

    public static int indexOf(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        return indexOf(paramCharSequence1, paramCharSequence2, 0, paramCharSequence1.length());
    }

    public static int indexOf(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt)
    {
        return indexOf(paramCharSequence1, paramCharSequence2, paramInt, paramCharSequence1.length());
    }

    public static int indexOf(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt1, int paramInt2)
    {
        int i = -1;
        int j = paramCharSequence2.length();
        if (j == 0)
        {
            i = paramInt1;
            return i;
        }
        char c = paramCharSequence2.charAt(0);
        while (true)
        {
            int k = indexOf(paramCharSequence1, c, paramInt1);
            if ((k > paramInt2 - j) || (k < 0))
                break;
            if (regionMatches(paramCharSequence1, k, paramCharSequence2, 0, j))
            {
                i = k;
                break;
            }
            paramInt1 = k + 1;
        }
    }

    public static boolean isDigitsOnly(CharSequence paramCharSequence)
    {
        int i = paramCharSequence.length();
        int j = 0;
        if (j < i)
            if (Character.isDigit(paramCharSequence.charAt(j)));
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            j++;
            break;
        }
    }

    public static boolean isEmpty(CharSequence paramCharSequence)
    {
        if ((paramCharSequence == null) || (paramCharSequence.length() == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isGraphic(char paramChar)
    {
        int i = Character.getType(paramChar);
        if ((i != 15) && (i != 16) && (i != 19) && (i != 0) && (i != 13) && (i != 14) && (i != 12));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isGraphic(CharSequence paramCharSequence)
    {
        int i = paramCharSequence.length();
        int j = 0;
        if (j < i)
        {
            int k = Character.getType(paramCharSequence.charAt(j));
            if ((k == 15) || (k == 16) || (k == 19) || (k == 0) || (k == 13) || (k == 14) || (k == 12));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    public static boolean isPrintableAscii(char paramChar)
    {
        if (((' ' <= paramChar) && (paramChar <= '~')) || (paramChar == '\r') || (paramChar == '\n'));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isPrintableAsciiOnly(CharSequence paramCharSequence)
    {
        int i = paramCharSequence.length();
        int j = 0;
        if (j < i)
            if (isPrintableAscii(paramCharSequence.charAt(j)));
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            j++;
            break;
        }
    }

    public static CharSequence join(Iterable<CharSequence> paramIterable)
    {
        return join(Resources.getSystem().getText(17040631), paramIterable);
    }

    public static String join(CharSequence paramCharSequence, Iterable paramIterable)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = 1;
        Iterator localIterator = paramIterable.iterator();
        if (localIterator.hasNext())
        {
            Object localObject = localIterator.next();
            if (i != 0)
                i = 0;
            while (true)
            {
                localStringBuilder.append(localObject);
                break;
                localStringBuilder.append(paramCharSequence);
            }
        }
        return localStringBuilder.toString();
    }

    public static String join(CharSequence paramCharSequence, Object[] paramArrayOfObject)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = 1;
        int j = paramArrayOfObject.length;
        int k = 0;
        if (k < j)
        {
            Object localObject = paramArrayOfObject[k];
            if (i != 0)
                i = 0;
            while (true)
            {
                localStringBuilder.append(localObject);
                k++;
                break;
                localStringBuilder.append(paramCharSequence);
            }
        }
        return localStringBuilder.toString();
    }

    public static int lastIndexOf(CharSequence paramCharSequence, char paramChar)
    {
        return lastIndexOf(paramCharSequence, paramChar, -1 + paramCharSequence.length());
    }

    public static int lastIndexOf(CharSequence paramCharSequence, char paramChar, int paramInt)
    {
        if (paramCharSequence.getClass() == String.class);
        for (int i = ((String)paramCharSequence).lastIndexOf(paramChar, paramInt); ; i = lastIndexOf(paramCharSequence, paramChar, 0, paramInt))
            return i;
    }

    public static int lastIndexOf(CharSequence paramCharSequence, char paramChar, int paramInt1, int paramInt2)
    {
        int j;
        if (paramInt2 < 0)
            j = -1;
        while (true)
        {
            return j;
            if (paramInt2 >= paramCharSequence.length())
                paramInt2 = -1 + paramCharSequence.length();
            int i = paramInt2 + 1;
            Class localClass = paramCharSequence.getClass();
            if (((paramCharSequence instanceof GetChars)) || (localClass == StringBuffer.class) || (localClass == StringBuilder.class) || (localClass == String.class))
            {
                char[] arrayOfChar = obtain(500);
                while (true)
                {
                    if (paramInt1 >= i)
                        break label165;
                    int k = i - 500;
                    if (k < paramInt1)
                        k = paramInt1;
                    getChars(paramCharSequence, k, i, arrayOfChar, 0);
                    for (int m = -1 + (i - k); ; m--)
                    {
                        if (m < 0)
                            break label158;
                        if (arrayOfChar[m] == paramChar)
                        {
                            recycle(arrayOfChar);
                            j = m + k;
                            break;
                        }
                    }
                    label158: i = k;
                }
                label165: recycle(arrayOfChar);
                j = -1;
            }
            else
            {
                for (j = i - 1; ; j--)
                {
                    if (j < paramInt1)
                        break label207;
                    if (paramCharSequence.charAt(j) == paramChar)
                        break;
                }
                label207: j = -1;
            }
        }
    }

    static char[] obtain(int paramInt)
    {
        synchronized (sLock)
        {
            char[] arrayOfChar = sTemp;
            sTemp = null;
            if ((arrayOfChar == null) || (arrayOfChar.length < paramInt))
                arrayOfChar = new char[ArrayUtils.idealCharArraySize(paramInt)];
            return arrayOfChar;
        }
    }

    public static long packRangeInLong(int paramInt1, int paramInt2)
    {
        return paramInt1 << 32 | paramInt2;
    }

    private static void readSpan(Parcel paramParcel, Spannable paramSpannable, Object paramObject)
    {
        paramSpannable.setSpan(paramObject, paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt());
    }

    static void recycle(char[] paramArrayOfChar)
    {
        if (paramArrayOfChar.length > 1000);
        while (true)
        {
            return;
            synchronized (sLock)
            {
                sTemp = paramArrayOfChar;
            }
        }
    }

    public static boolean regionMatches(CharSequence paramCharSequence1, int paramInt1, CharSequence paramCharSequence2, int paramInt2, int paramInt3)
    {
        char[] arrayOfChar = obtain(paramInt3 * 2);
        getChars(paramCharSequence1, paramInt1, paramInt1 + paramInt3, arrayOfChar, 0);
        getChars(paramCharSequence2, paramInt2, paramInt2 + paramInt3, arrayOfChar, paramInt3);
        boolean bool = true;
        for (int i = 0; ; i++)
            if (i < paramInt3)
            {
                if (arrayOfChar[i] != arrayOfChar[(i + paramInt3)])
                    bool = false;
            }
            else
            {
                recycle(arrayOfChar);
                return bool;
            }
    }

    public static <T> T[] removeEmptySpans(T[] paramArrayOfT, Spanned paramSpanned, Class<T> paramClass)
    {
        Object[] arrayOfObject = null;
        int i = 0;
        int j = 0;
        if (j < paramArrayOfT.length)
        {
            T ? = paramArrayOfT[j];
            if (paramSpanned.getSpanStart(?) == paramSpanned.getSpanEnd(?))
                if (arrayOfObject == null)
                {
                    arrayOfObject = (Object[])Array.newInstance(paramClass, -1 + paramArrayOfT.length);
                    System.arraycopy(paramArrayOfT, 0, arrayOfObject, 0, j);
                    i = j;
                }
            while (true)
            {
                j++;
                break;
                if (arrayOfObject != null)
                {
                    arrayOfObject[i] = ?;
                    i++;
                }
            }
        }
        Object localObject;
        if (arrayOfObject != null)
        {
            localObject = (Object[])Array.newInstance(paramClass, i);
            System.arraycopy(arrayOfObject, 0, localObject, 0, i);
        }
        while (true)
        {
            return localObject;
            localObject = paramArrayOfT;
        }
    }

    public static CharSequence replace(CharSequence paramCharSequence, String[] paramArrayOfString, CharSequence[] paramArrayOfCharSequence)
    {
        SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramCharSequence);
        for (int i = 0; i < paramArrayOfString.length; i++)
        {
            int n = indexOf(localSpannableStringBuilder, paramArrayOfString[i]);
            if (n >= 0)
                localSpannableStringBuilder.setSpan(paramArrayOfString[i], n, n + paramArrayOfString[i].length(), 33);
        }
        for (int j = 0; j < paramArrayOfString.length; j++)
        {
            int k = localSpannableStringBuilder.getSpanStart(paramArrayOfString[j]);
            int m = localSpannableStringBuilder.getSpanEnd(paramArrayOfString[j]);
            if (k >= 0)
                localSpannableStringBuilder.replace(k, m, paramArrayOfCharSequence[j]);
        }
        return localSpannableStringBuilder;
    }

    private static float setPara(MeasuredText paramMeasuredText, TextPaint paramTextPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, TextDirectionHeuristic paramTextDirectionHeuristic)
    {
        paramMeasuredText.setPara(paramCharSequence, paramInt1, paramInt2, paramTextDirectionHeuristic);
        Spanned localSpanned;
        int i;
        float f;
        if ((paramCharSequence instanceof Spanned))
        {
            localSpanned = (Spanned)paramCharSequence;
            i = paramInt2 - paramInt1;
            if (localSpanned != null)
                break label53;
            f = paramMeasuredText.addStyleRun(paramTextPaint, i, null);
        }
        while (true)
        {
            return f;
            localSpanned = null;
            break;
            label53: f = 0.0F;
            int k;
            for (int j = 0; j < i; j = k)
            {
                k = localSpanned.nextSpanTransition(j, i, MetricAffectingSpan.class);
                f += paramMeasuredText.addStyleRun(paramTextPaint, (MetricAffectingSpan[])removeEmptySpans((MetricAffectingSpan[])localSpanned.getSpans(j, k, MetricAffectingSpan.class), localSpanned, MetricAffectingSpan.class), k - j, null);
            }
        }
    }

    public static String[] split(String paramString1, String paramString2)
    {
        if (paramString1.length() == 0);
        for (String[] arrayOfString = EMPTY_STRING_ARRAY; ; arrayOfString = paramString1.split(paramString2, -1))
            return arrayOfString;
    }

    public static String[] split(String paramString, Pattern paramPattern)
    {
        if (paramString.length() == 0);
        for (String[] arrayOfString = EMPTY_STRING_ARRAY; ; arrayOfString = paramPattern.split(paramString, -1))
            return arrayOfString;
    }

    public static CharSequence stringOrSpannedString(CharSequence paramCharSequence)
    {
        if (paramCharSequence == null)
            paramCharSequence = null;
        while (true)
        {
            return paramCharSequence;
            if (!(paramCharSequence instanceof SpannedString))
                if ((paramCharSequence instanceof Spanned))
                    paramCharSequence = new SpannedString(paramCharSequence);
                else
                    paramCharSequence = paramCharSequence.toString();
        }
    }

    public static String substring(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        String str;
        if ((paramCharSequence instanceof String))
            str = ((String)paramCharSequence).substring(paramInt1, paramInt2);
        while (true)
        {
            return str;
            if ((paramCharSequence instanceof StringBuilder))
            {
                str = ((StringBuilder)paramCharSequence).substring(paramInt1, paramInt2);
            }
            else if ((paramCharSequence instanceof StringBuffer))
            {
                str = ((StringBuffer)paramCharSequence).substring(paramInt1, paramInt2);
            }
            else
            {
                char[] arrayOfChar = obtain(paramInt2 - paramInt1);
                getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
                str = new String(arrayOfChar, 0, paramInt2 - paramInt1);
                recycle(arrayOfChar);
            }
        }
    }

    public static int unpackRangeEndFromLong(long paramLong)
    {
        return (int)(0xFFFFFFFF & paramLong);
    }

    public static int unpackRangeStartFromLong(long paramLong)
    {
        return (int)(paramLong >>> 32);
    }

    public static void writeToParcel(CharSequence paramCharSequence, Parcel paramParcel, int paramInt)
    {
        if ((paramCharSequence instanceof Spanned))
        {
            paramParcel.writeInt(0);
            paramParcel.writeString(paramCharSequence.toString());
            Spanned localSpanned = (Spanned)paramCharSequence;
            Object[] arrayOfObject = localSpanned.getSpans(0, paramCharSequence.length(), Object.class);
            for (int i = 0; i < arrayOfObject.length; i++)
            {
                Object localObject1 = arrayOfObject[i];
                Object localObject2 = arrayOfObject[i];
                if ((localObject2 instanceof CharacterStyle))
                    localObject2 = ((CharacterStyle)localObject2).getUnderlying();
                if ((localObject2 instanceof ParcelableSpan))
                {
                    ParcelableSpan localParcelableSpan = (ParcelableSpan)localObject2;
                    paramParcel.writeInt(localParcelableSpan.getSpanTypeId());
                    localParcelableSpan.writeToParcel(paramParcel, paramInt);
                    writeWhere(paramParcel, localSpanned, localObject1);
                }
            }
            paramParcel.writeInt(0);
        }
        while (true)
        {
            return;
            paramParcel.writeInt(1);
            if (paramCharSequence != null)
                paramParcel.writeString(paramCharSequence.toString());
            else
                paramParcel.writeString(null);
        }
    }

    private static void writeWhere(Parcel paramParcel, Spanned paramSpanned, Object paramObject)
    {
        paramParcel.writeInt(paramSpanned.getSpanStart(paramObject));
        paramParcel.writeInt(paramSpanned.getSpanEnd(paramObject));
        paramParcel.writeInt(paramSpanned.getSpanFlags(paramObject));
    }

    public static abstract interface EllipsizeCallback
    {
        public abstract void ellipsized(int paramInt1, int paramInt2);
    }

    public static enum TruncateAt
    {
        static
        {
            MIDDLE = new TruncateAt("MIDDLE", 1);
            END = new TruncateAt("END", 2);
            MARQUEE = new TruncateAt("MARQUEE", 3);
            END_SMALL = new TruncateAt("END_SMALL", 4);
            TruncateAt[] arrayOfTruncateAt = new TruncateAt[5];
            arrayOfTruncateAt[0] = START;
            arrayOfTruncateAt[1] = MIDDLE;
            arrayOfTruncateAt[2] = END;
            arrayOfTruncateAt[3] = MARQUEE;
            arrayOfTruncateAt[4] = END_SMALL;
        }
    }

    private static class Reverser
        implements CharSequence, GetChars
    {
        private int mEnd;
        private CharSequence mSource;
        private int mStart;

        public Reverser(CharSequence paramCharSequence, int paramInt1, int paramInt2)
        {
            this.mSource = paramCharSequence;
            this.mStart = paramInt1;
            this.mEnd = paramInt2;
        }

        public char charAt(int paramInt)
        {
            return AndroidCharacter.getMirror(this.mSource.charAt(-1 + this.mEnd - paramInt));
        }

        public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar, int paramInt3)
        {
            TextUtils.getChars(this.mSource, paramInt1 + this.mStart, paramInt2 + this.mStart, paramArrayOfChar, paramInt3);
            AndroidCharacter.mirror(paramArrayOfChar, 0, paramInt2 - paramInt1);
            int i = paramInt2 - paramInt1;
            int j = (paramInt2 - paramInt1) / 2;
            for (int k = 0; k < j; k++)
            {
                int m = paramArrayOfChar[(paramInt3 + k)];
                paramArrayOfChar[(paramInt3 + k)] = paramArrayOfChar[(-1 + (paramInt3 + i - k))];
                paramArrayOfChar[(-1 + (paramInt3 + i - k))] = m;
            }
        }

        public int length()
        {
            return this.mEnd - this.mStart;
        }

        public CharSequence subSequence(int paramInt1, int paramInt2)
        {
            char[] arrayOfChar = new char[paramInt2 - paramInt1];
            getChars(paramInt1, paramInt2, arrayOfChar, 0);
            return new String(arrayOfChar);
        }

        public String toString()
        {
            return subSequence(0, length()).toString();
        }
    }

    public static class SimpleStringSplitter
        implements TextUtils.StringSplitter, Iterator<String>
    {
        private char mDelimiter;
        private int mLength;
        private int mPosition;
        private String mString;

        public SimpleStringSplitter(char paramChar)
        {
            this.mDelimiter = paramChar;
        }

        public boolean hasNext()
        {
            if (this.mPosition < this.mLength);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public Iterator<String> iterator()
        {
            return this;
        }

        public String next()
        {
            int i = this.mString.indexOf(this.mDelimiter, this.mPosition);
            if (i == -1)
                i = this.mLength;
            String str = this.mString.substring(this.mPosition, i);
            this.mPosition = (i + 1);
            return str;
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
        }

        public void setString(String paramString)
        {
            this.mString = paramString;
            this.mPosition = 0;
            this.mLength = this.mString.length();
        }
    }

    public static abstract interface StringSplitter extends Iterable<String>
    {
        public abstract void setString(String paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.TextUtils
 * JD-Core Version:        0.6.2
 */