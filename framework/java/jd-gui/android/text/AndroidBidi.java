package android.text;

class AndroidBidi
{
    public static int bidi(int paramInt1, char[] paramArrayOfChar, byte[] paramArrayOfByte, int paramInt2, boolean paramBoolean)
    {
        if ((paramArrayOfChar == null) || (paramArrayOfByte == null))
            throw new NullPointerException();
        if ((paramInt2 < 0) || (paramArrayOfChar.length < paramInt2) || (paramArrayOfByte.length < paramInt2))
            throw new IndexOutOfBoundsException();
        int i;
        switch (paramInt1)
        {
        case 0:
        default:
            i = 0;
            if ((0x1 & runBidi(i, paramArrayOfChar, paramArrayOfByte, paramInt2, paramBoolean)) != 0)
                break;
        case 1:
        case -1:
        case 2:
        case -2:
        }
        for (int j = 1; ; j = -1)
        {
            return j;
            i = 0;
            break;
            i = 1;
            break;
            i = -2;
            break;
            i = -1;
            break;
        }
    }

    public static Layout.Directions directions(int paramInt1, byte[] paramArrayOfByte, int paramInt2, char[] paramArrayOfChar, int paramInt3, int paramInt4)
    {
        if (paramInt1 == 1);
        int j;
        int k;
        int m;
        for (int i = 0; ; i = 1)
        {
            j = paramArrayOfByte[paramInt2];
            k = j;
            m = 1;
            int n = paramInt2 + 1;
            int i1 = paramInt2 + paramInt4;
            while (n < i1)
            {
                int i24 = paramArrayOfByte[n];
                if (i24 != j)
                {
                    j = i24;
                    m++;
                }
                n++;
            }
        }
        int i2 = paramInt4;
        int i23;
        if ((j & 0x1) != (i & 0x1))
        {
            i2--;
            if (i2 >= 0)
            {
                i23 = paramArrayOfChar[(paramInt3 + i2)];
                if (i23 != 10)
                    break label153;
                i2--;
            }
            label112: i2++;
            if (i2 != paramInt4)
                m++;
        }
        Layout.Directions localDirections;
        if ((m == 1) && (k == i))
        {
            if ((k & 0x1) != 0);
            for (localDirections = Layout.DIRS_ALL_RIGHT_TO_LEFT; ; localDirections = Layout.DIRS_ALL_LEFT_TO_RIGHT)
            {
                return localDirections;
                label153: if ((i23 == 32) || (i23 == 9))
                    break;
                break label112;
            }
        }
        int[] arrayOfInt = new int[m * 2];
        int i3 = k;
        int i4 = k << 26;
        int i5 = paramInt2;
        int i6 = k;
        int i7 = paramInt2;
        int i8 = paramInt2 + i2;
        int i9 = 1;
        label216: int i19;
        label251: int i22;
        if (i7 < i8)
        {
            i19 = paramArrayOfByte[i7];
            if (i19 == i6)
                break label616;
            i6 = i19;
            if (i19 > i3)
            {
                i3 = i19;
                int i21 = i9 + 1;
                arrayOfInt[i9] = (i4 | i7 - i5);
                i22 = i21 + 1;
                arrayOfInt[i21] = (i7 - paramInt2);
                i4 = i6 << 26;
                i5 = i7;
            }
        }
        label590: label596: label616: for (int i20 = i22; ; i20 = i9)
        {
            i7++;
            i9 = i20;
            break label216;
            if (i19 >= k)
                break label251;
            k = i19;
            break label251;
            arrayOfInt[i9] = (i4 | paramInt2 + i2 - i5);
            if (i2 < paramInt4)
            {
                int i18 = i9 + 1;
                arrayOfInt[i18] = i2;
                arrayOfInt[(i18 + 1)] = (paramInt4 - i2 | i << 26);
            }
            while (true)
            {
                int i10;
                if ((k & 0x1) == i)
                {
                    k++;
                    if (i3 > k)
                    {
                        i10 = 1;
                        if (i10 == 0)
                            break label596;
                    }
                }
                for (int i11 = i3 - 1; ; i11--)
                {
                    if (i11 < k)
                        break label596;
                    for (int i12 = 0; ; i12 += 2)
                    {
                        if (i12 >= arrayOfInt.length)
                            break label590;
                        if (paramArrayOfByte[arrayOfInt[i12]] >= i11)
                        {
                            int i13 = i12 + 2;
                            while (true)
                                if ((i13 < arrayOfInt.length) && (paramArrayOfByte[arrayOfInt[i13]] >= i11))
                                {
                                    i13 += 2;
                                    continue;
                                    i10 = 0;
                                    break;
                                    if (m > 1);
                                    for (i10 = 1; ; i10 = 0)
                                        break;
                                }
                            int i14 = i12;
                            for (int i15 = i13 - 2; i14 < i15; i15 -= 2)
                            {
                                int i16 = arrayOfInt[i14];
                                arrayOfInt[i14] = arrayOfInt[i15];
                                arrayOfInt[i15] = i16;
                                int i17 = arrayOfInt[(i14 + 1)];
                                arrayOfInt[(i14 + 1)] = arrayOfInt[(i15 + 1)];
                                arrayOfInt[(i15 + 1)] = i17;
                                i14 += 2;
                            }
                            i12 = i13 + 2;
                        }
                    }
                }
                localDirections = new Layout.Directions(arrayOfInt);
                break;
            }
        }
    }

    private static native int runBidi(int paramInt1, char[] paramArrayOfChar, byte[] paramArrayOfByte, int paramInt2, boolean paramBoolean);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.AndroidBidi
 * JD-Core Version:        0.6.2
 */