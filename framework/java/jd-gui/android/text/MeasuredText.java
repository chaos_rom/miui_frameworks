package android.text;

import android.graphics.Paint.FontMetricsInt;
import android.text.style.MetricAffectingSpan;
import android.text.style.ReplacementSpan;
import com.android.internal.util.ArrayUtils;

class MeasuredText
{
    private static final boolean localLOGV;
    private static MeasuredText[] sCached = new MeasuredText[3];
    private static final Object[] sLock = new Object[0];
    char[] mChars;
    int mDir;
    boolean mEasy;
    int mLen;
    byte[] mLevels;
    private int mPos;
    CharSequence mText;
    int mTextStart;
    float[] mWidths;
    private TextPaint mWorkPaint = new TextPaint();

    static MeasuredText obtain()
    {
        MeasuredText localMeasuredText;
        synchronized (sLock)
        {
            int i = sCached.length;
            do
            {
                i--;
                if (i < 0)
                    break;
            }
            while (sCached[i] == null);
            localMeasuredText = sCached[i];
            sCached[i] = null;
            break label61;
            localMeasuredText = new MeasuredText();
        }
        label61: return localMeasuredText;
    }

    static MeasuredText recycle(MeasuredText paramMeasuredText)
    {
        paramMeasuredText.mText = null;
        Object[] arrayOfObject;
        if (paramMeasuredText.mLen < 1000)
            arrayOfObject = sLock;
        for (int i = 0; ; i++)
        {
            try
            {
                if (i < sCached.length)
                {
                    if (sCached[i] != null)
                        continue;
                    sCached[i] = paramMeasuredText;
                    paramMeasuredText.mText = null;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            return null;
        }
    }

    float addStyleRun(TextPaint paramTextPaint, int paramInt, Paint.FontMetricsInt paramFontMetricsInt)
    {
        if (paramFontMetricsInt != null)
            paramTextPaint.getFontMetricsInt(paramFontMetricsInt);
        int i = this.mPos;
        this.mPos = (i + paramInt);
        if (this.mEasy)
        {
            if (this.mDir == 1);
            for (int i4 = 0; ; i4 = 1)
            {
                f = paramTextPaint.getTextRunAdvances(this.mChars, i, paramInt, i, paramInt, i4, this.mWidths, i);
                return f;
            }
        }
        float f = 0.0F;
        int j = this.mLevels[i];
        int k = i;
        int m = i + 1;
        int n = i + paramInt;
        label103: if ((m == n) || (this.mLevels[m] != j))
            if ((j & 0x1) != 0)
                break label209;
        label209: for (int i1 = 0; ; i1 = 1)
        {
            char[] arrayOfChar = this.mChars;
            int i2 = m - k;
            int i3 = m - k;
            float[] arrayOfFloat = this.mWidths;
            f += paramTextPaint.getTextRunAdvances(arrayOfChar, k, i2, k, i3, i1, arrayOfFloat, k);
            if (m == n)
                break;
            k = m;
            j = this.mLevels[m];
            m++;
            break label103;
        }
    }

    float addStyleRun(TextPaint paramTextPaint, MetricAffectingSpan[] paramArrayOfMetricAffectingSpan, int paramInt, Paint.FontMetricsInt paramFontMetricsInt)
    {
        TextPaint localTextPaint = this.mWorkPaint;
        localTextPaint.set(paramTextPaint);
        localTextPaint.baselineShift = 0;
        ReplacementSpan localReplacementSpan = null;
        int i = 0;
        if (i < paramArrayOfMetricAffectingSpan.length)
        {
            MetricAffectingSpan localMetricAffectingSpan = paramArrayOfMetricAffectingSpan[i];
            if ((localMetricAffectingSpan instanceof ReplacementSpan))
                localReplacementSpan = (ReplacementSpan)localMetricAffectingSpan;
            while (true)
            {
                i++;
                break;
                localMetricAffectingSpan.updateMeasureState(localTextPaint);
            }
        }
        float f;
        if (localReplacementSpan == null)
        {
            f = addStyleRun(localTextPaint, paramInt, paramFontMetricsInt);
            if (paramFontMetricsInt != null)
            {
                if (localTextPaint.baselineShift >= 0)
                    break label231;
                paramFontMetricsInt.ascent += localTextPaint.baselineShift;
                paramFontMetricsInt.top += localTextPaint.baselineShift;
            }
        }
        while (true)
        {
            return f;
            f = localReplacementSpan.getSize(localTextPaint, this.mText, this.mTextStart + this.mPos, paramInt + (this.mTextStart + this.mPos), paramFontMetricsInt);
            float[] arrayOfFloat = this.mWidths;
            arrayOfFloat[this.mPos] = f;
            int j = 1 + this.mPos;
            int k = paramInt + this.mPos;
            while (j < k)
            {
                arrayOfFloat[j] = 0.0F;
                j++;
            }
            this.mPos = (paramInt + this.mPos);
            break;
            label231: paramFontMetricsInt.descent += localTextPaint.baselineShift;
            paramFontMetricsInt.bottom += localTextPaint.baselineShift;
        }
    }

    int breakText(int paramInt, boolean paramBoolean, float paramFloat)
    {
        float[] arrayOfFloat = this.mWidths;
        int k;
        if (paramBoolean)
            for (k = 0; ; k++)
                if (k < paramInt)
                {
                    paramFloat -= arrayOfFloat[k];
                    if (paramFloat >= 0.0F);
                }
                else
                {
                    while ((k > 0) && (this.mChars[(k - 1)] == ' '))
                        k--;
                }
        int i;
        for (int j = k; ; j = -1 + (paramInt - i))
        {
            return j;
            for (i = paramInt - 1; ; i--)
                if (i >= 0)
                {
                    paramFloat -= arrayOfFloat[i];
                    if (paramFloat >= 0.0F);
                }
                else
                {
                    while ((i < paramInt - 1) && (this.mChars[(i + 1)] == ' '))
                        i++;
                }
        }
    }

    float measure(int paramInt1, int paramInt2)
    {
        float f = 0.0F;
        float[] arrayOfFloat = this.mWidths;
        for (int i = paramInt1; i < paramInt2; i++)
            f += arrayOfFloat[i];
        return f;
    }

    void setPara(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextDirectionHeuristic paramTextDirectionHeuristic)
    {
        this.mText = paramCharSequence;
        this.mTextStart = paramInt1;
        int i = paramInt2 - paramInt1;
        this.mLen = i;
        this.mPos = 0;
        if ((this.mWidths == null) || (this.mWidths.length < i))
            this.mWidths = new float[ArrayUtils.idealFloatArraySize(i)];
        if ((this.mChars == null) || (this.mChars.length < i))
            this.mChars = new char[ArrayUtils.idealCharArraySize(i)];
        TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, this.mChars, 0);
        if ((paramCharSequence instanceof Spanned))
        {
            Spanned localSpanned = (Spanned)paramCharSequence;
            ReplacementSpan[] arrayOfReplacementSpan = (ReplacementSpan[])localSpanned.getSpans(paramInt1, paramInt2, ReplacementSpan.class);
            for (int k = 0; k < arrayOfReplacementSpan.length; k++)
            {
                int m = localSpanned.getSpanStart(arrayOfReplacementSpan[k]) - paramInt1;
                int n = localSpanned.getSpanEnd(arrayOfReplacementSpan[k]) - paramInt1;
                if (m < 0)
                    m = 0;
                if (n > i)
                    n = i;
                for (int i1 = m; i1 < n; i1++)
                    this.mChars[i1] = 65532;
            }
        }
        if (((paramTextDirectionHeuristic == TextDirectionHeuristics.LTR) || (paramTextDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR) || (paramTextDirectionHeuristic == TextDirectionHeuristics.ANYRTL_LTR)) && (TextUtils.doesNotNeedBidi(this.mChars, 0, i)))
        {
            this.mDir = 1;
            this.mEasy = true;
            return;
        }
        if ((this.mLevels == null) || (this.mLevels.length < i))
            this.mLevels = new byte[ArrayUtils.idealByteArraySize(i)];
        if (paramTextDirectionHeuristic == TextDirectionHeuristics.LTR)
            j = 1;
        while (true)
        {
            this.mDir = AndroidBidi.bidi(j, this.mChars, this.mLevels, i, false);
            this.mEasy = false;
            break;
            if (paramTextDirectionHeuristic == TextDirectionHeuristics.RTL)
            {
                j = -1;
            }
            else if (paramTextDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR)
            {
                j = 2;
            }
            else
            {
                if (paramTextDirectionHeuristic != TextDirectionHeuristics.FIRSTSTRONG_RTL)
                    break label375;
                j = -2;
            }
        }
        label375: if (paramTextDirectionHeuristic.isRtl(this.mChars, 0, i));
        for (int j = -1; ; j = 1)
            break;
    }

    void setPos(int paramInt)
    {
        this.mPos = (paramInt - this.mTextStart);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.MeasuredText
 * JD-Core Version:        0.6.2
 */