package android.text;

public abstract interface NoCopySpan
{
    public static class Concrete
        implements NoCopySpan
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.NoCopySpan
 * JD-Core Version:        0.6.2
 */