package android.text;

public abstract interface InputFilter
{
    public abstract CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4);

    public static class LengthFilter
        implements InputFilter
    {
        private int mMax;

        public LengthFilter(int paramInt)
        {
            this.mMax = paramInt;
        }

        public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
        {
            int i = this.mMax - (paramSpanned.length() - (paramInt4 - paramInt3));
            Object localObject;
            if (i <= 0)
                localObject = "";
            while (true)
            {
                return localObject;
                if (i >= paramInt2 - paramInt1)
                {
                    localObject = null;
                }
                else
                {
                    int j = i + paramInt1;
                    if (Character.isHighSurrogate(paramCharSequence.charAt(j - 1)))
                    {
                        j--;
                        if (j == paramInt1)
                            localObject = "";
                    }
                    else
                    {
                        localObject = paramCharSequence.subSequence(paramInt1, j);
                    }
                }
            }
        }
    }

    public static class AllCaps
        implements InputFilter
    {
        public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
        {
            int i = paramInt1;
            String str;
            Object localObject;
            if (i < paramInt2)
                if (Character.isLowerCase(paramCharSequence.charAt(i)))
                {
                    char[] arrayOfChar = new char[paramInt2 - paramInt1];
                    TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
                    str = new String(arrayOfChar).toUpperCase();
                    if ((paramCharSequence instanceof Spanned))
                    {
                        localObject = new SpannableString(str);
                        TextUtils.copySpansFrom((Spanned)paramCharSequence, paramInt1, paramInt2, null, (Spannable)localObject, 0);
                    }
                }
            while (true)
            {
                return localObject;
                localObject = str;
                continue;
                i++;
                break;
                localObject = null;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.InputFilter
 * JD-Core Version:        0.6.2
 */