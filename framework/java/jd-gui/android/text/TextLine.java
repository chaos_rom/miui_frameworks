package android.text;

import android.emoji.EmojiFactory;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.text.style.CharacterStyle;
import android.text.style.MetricAffectingSpan;
import android.text.style.ReplacementSpan;
import com.android.internal.util.ArrayUtils;

class TextLine
{
    private static final boolean DEBUG = false;
    private static final int TAB_INCREMENT = 20;
    private static final TextLine[] sCached = new TextLine[3];
    private final SpanSet<CharacterStyle> mCharacterStyleSpanSet = new SpanSet(CharacterStyle.class);
    private char[] mChars;
    private boolean mCharsValid;
    private int mDir;
    private Layout.Directions mDirections;
    private boolean mHasTabs;
    private int mLen;
    private final SpanSet<MetricAffectingSpan> mMetricAffectingSpanSpanSet = new SpanSet(MetricAffectingSpan.class);
    private TextPaint mPaint;
    private final SpanSet<ReplacementSpan> mReplacementSpanSpanSet = new SpanSet(ReplacementSpan.class);
    private Spanned mSpanned;
    private int mStart;
    private Layout.TabStops mTabs;
    private CharSequence mText;
    private final TextPaint mWorkPaint = new TextPaint();

    private float drawRun(Canvas paramCanvas, int paramInt1, int paramInt2, boolean paramBoolean1, float paramFloat, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean2)
    {
        boolean bool;
        float f;
        if (this.mDir == 1)
        {
            bool = true;
            if (bool != paramBoolean1)
                break label64;
            f = -measureRun(paramInt1, paramInt2, paramInt2, paramBoolean1, null);
            handleRun(paramInt1, paramInt2, paramInt2, paramBoolean1, paramCanvas, paramFloat + f, paramInt3, paramInt4, paramInt5, null, false);
        }
        while (true)
        {
            return f;
            bool = false;
            break;
            label64: f = handleRun(paramInt1, paramInt2, paramInt2, paramBoolean1, paramCanvas, paramFloat, paramInt3, paramInt4, paramInt5, null, paramBoolean2);
        }
    }

    private void drawTextRun(Canvas paramCanvas, TextPaint paramTextPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean, float paramFloat, int paramInt5)
    {
        int i;
        if (paramBoolean)
        {
            i = 1;
            if (!this.mCharsValid)
                break label58;
            int k = paramInt2 - paramInt1;
            int m = paramInt4 - paramInt3;
            paramCanvas.drawTextRun(this.mChars, paramInt1, k, paramInt3, m, paramFloat, paramInt5, i, paramTextPaint);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label58: int j = this.mStart;
            paramCanvas.drawTextRun(this.mText, j + paramInt1, j + paramInt2, j + paramInt3, j + paramInt4, paramFloat, paramInt5, i, paramTextPaint);
        }
    }

    private static void expandMetricsFromPaint(Paint.FontMetricsInt paramFontMetricsInt, TextPaint paramTextPaint)
    {
        int i = paramFontMetricsInt.top;
        int j = paramFontMetricsInt.ascent;
        int k = paramFontMetricsInt.descent;
        int m = paramFontMetricsInt.bottom;
        int n = paramFontMetricsInt.leading;
        paramTextPaint.getFontMetricsInt(paramFontMetricsInt);
        updateMetrics(paramFontMetricsInt, i, j, k, m, n);
    }

    private int getOffsetBeforeAfter(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, int paramInt4, boolean paramBoolean2)
    {
        int j;
        int i;
        if (paramInt1 >= 0)
        {
            if (paramBoolean2)
            {
                j = this.mLen;
                if (paramInt4 != j)
                    break label81;
            }
        }
        else
        {
            if (!paramBoolean2)
                break label57;
            i = TextUtils.getOffsetAfter(this.mText, paramInt4 + this.mStart) - this.mStart;
        }
        while (true)
        {
            return i;
            j = 0;
            break;
            label57: i = TextUtils.getOffsetBefore(this.mText, paramInt4 + this.mStart) - this.mStart;
            continue;
            label81: TextPaint localTextPaint = this.mWorkPaint;
            localTextPaint.set(this.mPaint);
            int k = paramInt2;
            label109: int i3;
            if (this.mSpanned == null)
            {
                i = paramInt3;
                if (!paramBoolean1)
                    break label347;
                i3 = 1;
                label117: if (!paramBoolean2)
                    break label353;
            }
            label178: label313: label320: label330: label347: label353: for (int i4 = 0; ; i4 = 2)
            {
                if (!this.mCharsValid)
                    break label359;
                i = localTextPaint.getTextRunCursor(this.mChars, k, i - k, i3, paramInt4, i4);
                break;
                int m;
                ReplacementSpan localReplacementSpan;
                int i1;
                MetricAffectingSpan localMetricAffectingSpan;
                if (paramBoolean2)
                {
                    m = paramInt4 + 1;
                    int n = paramInt3 + this.mStart;
                    i = this.mSpanned.nextSpanTransition(k + this.mStart, n, MetricAffectingSpan.class) - this.mStart;
                    if (i < m)
                        break label313;
                    MetricAffectingSpan[] arrayOfMetricAffectingSpan = (MetricAffectingSpan[])TextUtils.removeEmptySpans((MetricAffectingSpan[])this.mSpanned.getSpans(k + this.mStart, i + this.mStart, MetricAffectingSpan.class), this.mSpanned, MetricAffectingSpan.class);
                    if (arrayOfMetricAffectingSpan.length <= 0)
                        break label109;
                    localReplacementSpan = null;
                    i1 = 0;
                    int i2 = arrayOfMetricAffectingSpan.length;
                    if (i1 >= i2)
                        break label330;
                    localMetricAffectingSpan = arrayOfMetricAffectingSpan[i1];
                    if (!(localMetricAffectingSpan instanceof ReplacementSpan))
                        break label320;
                    localReplacementSpan = (ReplacementSpan)localMetricAffectingSpan;
                }
                while (true)
                {
                    i1++;
                    break label266;
                    m = paramInt4;
                    break;
                    k = i;
                    break label178;
                    localMetricAffectingSpan.updateMeasureState(localTextPaint);
                }
                if (localReplacementSpan == null)
                    break label109;
                if (paramBoolean2)
                    break;
                i = k;
                break;
                i3 = 0;
                break label117;
            }
            label266: label359: CharSequence localCharSequence = this.mText;
            int i5 = k + this.mStart;
            int i6 = i + this.mStart;
            int i7 = paramInt4 + this.mStart;
            i = localTextPaint.getTextRunCursor(localCharSequence, i5, i6, i3, i7, i4) - this.mStart;
        }
    }

    private float handleReplacement(ReplacementSpan paramReplacementSpan, TextPaint paramTextPaint, int paramInt1, int paramInt2, boolean paramBoolean1, Canvas paramCanvas, float paramFloat, int paramInt3, int paramInt4, int paramInt5, Paint.FontMetricsInt paramFontMetricsInt, boolean paramBoolean2)
    {
        float f = 0.0F;
        int i = paramInt1 + this.mStart;
        int j = paramInt2 + this.mStart;
        int k;
        int m;
        int n;
        int i1;
        int i2;
        if ((paramBoolean2) || ((paramCanvas != null) && (paramBoolean1)))
        {
            k = 0;
            m = 0;
            n = 0;
            i1 = 0;
            i2 = 0;
            if (paramFontMetricsInt == null)
                break label189;
        }
        label189: for (int i3 = 1; ; i3 = 0)
        {
            if (i3 != 0)
            {
                k = paramFontMetricsInt.top;
                m = paramFontMetricsInt.ascent;
                n = paramFontMetricsInt.descent;
                i1 = paramFontMetricsInt.bottom;
                i2 = paramFontMetricsInt.leading;
            }
            f = paramReplacementSpan.getSize(paramTextPaint, this.mText, i, j, paramFontMetricsInt);
            if (i3 != 0)
                updateMetrics(paramFontMetricsInt, k, m, n, i1, i2);
            if (paramCanvas != null)
            {
                if (paramBoolean1)
                    paramFloat -= f;
                paramReplacementSpan.draw(paramCanvas, this.mText, i, j, paramFloat, paramInt3, paramInt4, paramInt5, paramTextPaint);
            }
            if (paramBoolean1)
                f = -f;
            return f;
        }
    }

    private float handleRun(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, Canvas paramCanvas, float paramFloat, int paramInt4, int paramInt5, int paramInt6, Paint.FontMetricsInt paramFontMetricsInt, boolean paramBoolean2)
    {
        float f2;
        if (paramInt1 == paramInt2)
        {
            TextPaint localTextPaint3 = this.mWorkPaint;
            localTextPaint3.set(this.mPaint);
            if (paramFontMetricsInt != null)
                expandMetricsFromPaint(paramFontMetricsInt, localTextPaint3);
            f2 = 0.0F;
        }
        while (true)
        {
            return f2;
            if (this.mSpanned == null)
            {
                TextPaint localTextPaint2 = this.mWorkPaint;
                localTextPaint2.set(this.mPaint);
                if ((paramBoolean2) || (paramInt2 < paramInt2));
                for (boolean bool4 = true; ; bool4 = false)
                {
                    f2 = handleText(localTextPaint2, paramInt1, paramInt2, paramInt1, paramInt3, paramBoolean1, paramCanvas, paramFloat, paramInt4, paramInt5, paramInt6, paramFontMetricsInt, bool4);
                    break;
                }
            }
            this.mMetricAffectingSpanSpanSet.init(this.mSpanned, paramInt1 + this.mStart, paramInt3 + this.mStart);
            this.mCharacterStyleSpanSet.init(this.mSpanned, paramInt1 + this.mStart, paramInt3 + this.mStart);
            float f1 = paramFloat;
            int i = paramInt1;
            if (i < paramInt2)
            {
                TextPaint localTextPaint1 = this.mWorkPaint;
                localTextPaint1.set(this.mPaint);
                int j = this.mMetricAffectingSpanSpanSet.getNextTransition(i + this.mStart, paramInt3 + this.mStart) - this.mStart;
                int k = Math.min(j, paramInt2);
                ReplacementSpan localReplacementSpan = null;
                int m = 0;
                int n = this.mMetricAffectingSpanSpanSet.numberOfSpans;
                if (m < n)
                {
                    if ((this.mMetricAffectingSpanSpanSet.spanStarts[m] >= k + this.mStart) || (this.mMetricAffectingSpanSpanSet.spanEnds[m] <= i + this.mStart));
                    while (true)
                    {
                        m++;
                        break;
                        MetricAffectingSpan localMetricAffectingSpan = ((MetricAffectingSpan[])this.mMetricAffectingSpanSpanSet.spans)[m];
                        if ((localMetricAffectingSpan instanceof ReplacementSpan))
                            localReplacementSpan = (ReplacementSpan)localMetricAffectingSpan;
                        else
                            localMetricAffectingSpan.updateDrawState(localTextPaint1);
                    }
                }
                if (localReplacementSpan != null)
                {
                    if ((paramBoolean2) || (k < paramInt2));
                    for (boolean bool3 = true; ; bool3 = false)
                    {
                        paramFloat += handleReplacement(localReplacementSpan, localTextPaint1, i, k, paramBoolean1, paramCanvas, paramFloat, paramInt4, paramInt5, paramInt6, paramFontMetricsInt, bool3);
                        i = j;
                        break;
                    }
                }
                if (paramCanvas == null)
                {
                    if ((paramBoolean2) || (k < paramInt2));
                    for (boolean bool2 = true; ; bool2 = false)
                    {
                        paramFloat += handleText(localTextPaint1, i, k, i, j, paramBoolean1, paramCanvas, paramFloat, paramInt4, paramInt5, paramInt6, paramFontMetricsInt, bool2);
                        break;
                    }
                }
                int i1 = i;
                label468: int i2;
                if (i1 < k)
                {
                    i2 = this.mCharacterStyleSpanSet.getNextTransition(i1 + this.mStart, k + this.mStart) - this.mStart;
                    localTextPaint1.set(this.mPaint);
                    int i3 = 0;
                    int i4 = this.mCharacterStyleSpanSet.numberOfSpans;
                    if (i3 < i4)
                    {
                        if ((this.mCharacterStyleSpanSet.spanStarts[i3] >= i2 + this.mStart) || (this.mCharacterStyleSpanSet.spanEnds[i3] <= i1 + this.mStart));
                        while (true)
                        {
                            i3++;
                            break;
                            ((CharacterStyle[])this.mCharacterStyleSpanSet.spans)[i3].updateDrawState(localTextPaint1);
                        }
                    }
                    if ((!paramBoolean2) && (i2 >= paramInt2))
                        break label654;
                }
                label654: for (boolean bool1 = true; ; bool1 = false)
                {
                    paramFloat += handleText(localTextPaint1, i1, i2, i, j, paramBoolean1, paramCanvas, paramFloat, paramInt4, paramInt5, paramInt6, paramFontMetricsInt, bool1);
                    i1 = i2;
                    break label468;
                    break;
                }
            }
            f2 = paramFloat - f1;
        }
    }

    private float handleText(TextPaint paramTextPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, Canvas paramCanvas, float paramFloat, int paramInt5, int paramInt6, int paramInt7, Paint.FontMetricsInt paramFontMetricsInt, boolean paramBoolean2)
    {
        if (paramFontMetricsInt != null)
            expandMetricsFromPaint(paramFontMetricsInt, paramTextPaint);
        int i = paramInt2 - paramInt1;
        if (i == 0)
        {
            f1 = 0.0F;
            return f1;
        }
        float f1 = 0.0F;
        int j = paramInt4 - paramInt3;
        int k;
        if ((paramBoolean2) || ((paramCanvas != null) && ((paramTextPaint.bgColor != 0) || (paramTextPaint.underlineColor != 0) || (paramBoolean1))))
        {
            if (!paramBoolean1)
                break label349;
            k = 1;
            label74: if (!this.mCharsValid)
                break label355;
        }
        label349: label355: int m;
        for (f1 = paramTextPaint.getTextRunAdvances(this.mChars, paramInt1, i, paramInt3, j, k, null, 0); ; f1 = paramTextPaint.getTextRunAdvances(this.mText, m + paramInt1, m + paramInt2, m + paramInt3, m + paramInt4, k, null, 0))
        {
            if (paramCanvas != null)
            {
                if (paramBoolean1)
                    paramFloat -= f1;
                if (paramTextPaint.bgColor != 0)
                {
                    int i2 = paramTextPaint.getColor();
                    Paint.Style localStyle2 = paramTextPaint.getStyle();
                    paramTextPaint.setColor(paramTextPaint.bgColor);
                    paramTextPaint.setStyle(Paint.Style.FILL);
                    float f5 = paramInt5;
                    float f6 = paramFloat + f1;
                    float f7 = paramInt7;
                    paramCanvas.drawRect(paramFloat, f5, f6, f7, paramTextPaint);
                    paramTextPaint.setStyle(localStyle2);
                    paramTextPaint.setColor(i2);
                }
                if (paramTextPaint.underlineColor != 0)
                {
                    float f2 = paramInt6 + paramTextPaint.baselineShift + 0.1111111F * paramTextPaint.getTextSize();
                    int i1 = paramTextPaint.getColor();
                    Paint.Style localStyle1 = paramTextPaint.getStyle();
                    boolean bool = paramTextPaint.isAntiAlias();
                    paramTextPaint.setStyle(Paint.Style.FILL);
                    paramTextPaint.setAntiAlias(true);
                    paramTextPaint.setColor(paramTextPaint.underlineColor);
                    float f3 = paramFloat + f1;
                    float f4 = f2 + paramTextPaint.underlineThickness;
                    paramCanvas.drawRect(paramFloat, f2, f3, f4, paramTextPaint);
                    paramTextPaint.setStyle(localStyle1);
                    paramTextPaint.setColor(i1);
                    paramTextPaint.setAntiAlias(bool);
                }
                int n = paramInt6 + paramTextPaint.baselineShift;
                drawTextRun(paramCanvas, paramTextPaint, paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1, paramFloat, n);
            }
            if (!paramBoolean1)
                break;
            f1 = -f1;
            break;
            k = 0;
            break label74;
            m = this.mStart;
        }
    }

    private float measureRun(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, Paint.FontMetricsInt paramFontMetricsInt)
    {
        return handleRun(paramInt1, paramInt2, paramInt3, paramBoolean, null, 0.0F, 0, 0, 0, paramFontMetricsInt, true);
    }

    static TextLine obtain()
    {
        TextLine localTextLine;
        synchronized (sCached)
        {
            int i = sCached.length;
            do
            {
                i--;
                if (i < 0)
                    break;
            }
            while (sCached[i] == null);
            localTextLine = sCached[i];
            sCached[i] = null;
            break label61;
            localTextLine = new TextLine();
        }
        label61: return localTextLine;
    }

    static TextLine recycle(TextLine paramTextLine)
    {
        paramTextLine.mText = null;
        paramTextLine.mPaint = null;
        paramTextLine.mDirections = null;
        paramTextLine.mMetricAffectingSpanSpanSet.recycle();
        paramTextLine.mCharacterStyleSpanSet.recycle();
        paramTextLine.mReplacementSpanSpanSet.recycle();
        TextLine[] arrayOfTextLine = sCached;
        for (int i = 0; ; i++)
            try
            {
                if (i < sCached.length)
                {
                    if (sCached[i] == null)
                        sCached[i] = paramTextLine;
                }
                else
                    return null;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
    }

    static void updateMetrics(Paint.FontMetricsInt paramFontMetricsInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        paramFontMetricsInt.top = Math.min(paramFontMetricsInt.top, paramInt1);
        paramFontMetricsInt.ascent = Math.min(paramFontMetricsInt.ascent, paramInt2);
        paramFontMetricsInt.descent = Math.max(paramFontMetricsInt.descent, paramInt3);
        paramFontMetricsInt.bottom = Math.max(paramFontMetricsInt.bottom, paramInt4);
        paramFontMetricsInt.leading = Math.max(paramFontMetricsInt.leading, paramInt5);
    }

    float ascent(int paramInt)
    {
        float f;
        if (this.mSpanned == null)
            f = this.mPaint.ascent();
        while (true)
        {
            return f;
            int i = paramInt + this.mStart;
            MetricAffectingSpan[] arrayOfMetricAffectingSpan = (MetricAffectingSpan[])this.mSpanned.getSpans(i, i + 1, MetricAffectingSpan.class);
            if (arrayOfMetricAffectingSpan.length == 0)
            {
                f = this.mPaint.ascent();
            }
            else
            {
                TextPaint localTextPaint = this.mWorkPaint;
                localTextPaint.set(this.mPaint);
                int j = arrayOfMetricAffectingSpan.length;
                for (int k = 0; k < j; k++)
                    arrayOfMetricAffectingSpan[k].updateMeasureState(localTextPaint);
                f = localTextPaint.ascent();
            }
        }
    }

    void draw(Canvas paramCanvas, float paramFloat, int paramInt1, int paramInt2, int paramInt3)
    {
        if (!this.mHasTabs)
        {
            if (this.mDirections == Layout.DIRS_ALL_LEFT_TO_RIGHT)
                drawRun(paramCanvas, 0, this.mLen, false, paramFloat, paramInt1, paramInt2, paramInt3, false);
            while (true)
            {
                return;
                if (this.mDirections != Layout.DIRS_ALL_RIGHT_TO_LEFT)
                    break;
                drawRun(paramCanvas, 0, this.mLen, true, paramFloat, paramInt1, paramInt2, paramInt3, false);
            }
        }
        float f1 = 0.0F;
        int[] arrayOfInt = this.mDirections.mDirections;
        RectF localRectF = null;
        int i = -2 + arrayOfInt.length;
        for (int j = 0; ; j += 2)
        {
            int k = arrayOfInt.length;
            if (j >= k)
                break;
            int m = arrayOfInt[j];
            int n = m + (0x3FFFFFF & arrayOfInt[(j + 1)]);
            int i1 = this.mLen;
            if (n > i1)
                n = this.mLen;
            boolean bool1;
            int i2;
            int i3;
            label181: int i4;
            Bitmap localBitmap;
            label279: boolean bool2;
            if ((0x4000000 & arrayOfInt[(j + 1)]) != 0)
            {
                bool1 = true;
                i2 = m;
                if (!this.mHasTabs)
                    break label393;
                i3 = m;
                if (i3 > n)
                    continue;
                i4 = 0;
                localBitmap = null;
                if ((this.mHasTabs) && (i3 < n))
                {
                    i4 = this.mChars[i3];
                    if ((i4 >= 55296) && (i4 < 56320) && (i3 + 1 < n))
                    {
                        i4 = Character.codePointAt(this.mChars, i3);
                        if ((i4 < Layout.MIN_EMOJI) || (i4 > Layout.MAX_EMOJI))
                            break label400;
                        localBitmap = Layout.EMOJI_FACTORY.getBitmapFromAndroidPua(i4);
                    }
                }
                if ((i3 == n) || (i4 == 9) || (localBitmap != null))
                {
                    float f2 = paramFloat + f1;
                    if ((j == i) && (i3 == this.mLen))
                        break label414;
                    bool2 = true;
                    label323: f1 += drawRun(paramCanvas, i2, i3, bool1, f2, paramInt1, paramInt2, paramInt3, bool2);
                    if (i4 != 9)
                        break label420;
                    f1 = this.mDir * nextTab(f1 * this.mDir);
                }
            }
            while (true)
            {
                i2 = i3 + 1;
                while (true)
                {
                    i3++;
                    break label181;
                    bool1 = false;
                    break;
                    label393: i3 = n;
                    break label181;
                    label400: if (i4 <= 65535)
                        break label279;
                    i3++;
                }
                label414: bool2 = false;
                break label323;
                label420: if (localBitmap != null)
                {
                    float f3 = ascent(i3);
                    float f4 = localBitmap.getHeight();
                    float f5 = -f3 / f4 * localBitmap.getWidth();
                    if (localRectF == null)
                        localRectF = new RectF();
                    float f6 = paramFloat + f1;
                    float f7 = f3 + paramInt2;
                    float f8 = f5 + (paramFloat + f1);
                    float f9 = paramInt2;
                    localRectF.set(f6, f7, f8, f9);
                    TextPaint localTextPaint = this.mPaint;
                    paramCanvas.drawBitmap(localBitmap, null, localRectF, localTextPaint);
                    f1 += f5;
                    i3++;
                }
            }
        }
    }

    int getOffsetToLeftRightOf(int paramInt, boolean paramBoolean)
    {
        int i = this.mLen;
        boolean bool1;
        int[] arrayOfInt;
        int j;
        int k;
        int m;
        int i1;
        int i2;
        label50: int i6;
        label59: int i7;
        label67: int i8;
        int i9;
        int i10;
        int i11;
        boolean bool4;
        label146: boolean bool5;
        label155: int i12;
        if (this.mDir == -1)
        {
            bool1 = true;
            arrayOfInt = this.mDirections.mDirections;
            j = 0;
            k = 0;
            m = i;
            n = -1;
            i1 = 0;
            if (paramInt != 0)
                break label218;
            i2 = -2;
            if (paramBoolean != bool1)
                break label545;
            i6 = 1;
            if (i6 == 0)
                break label551;
            i7 = 2;
            i8 = i2 + i7;
            if ((i8 < 0) || (i8 >= arrayOfInt.length))
                break label614;
            i9 = 0 + arrayOfInt[i8];
            i10 = i9 + (0x3FFFFFF & arrayOfInt[(i8 + 1)]);
            if (i10 > i)
                i10 = i;
            i11 = 0x3F & arrayOfInt[(i8 + 1)] >>> 26;
            if ((i11 & 0x1) == 0)
                break label558;
            bool4 = true;
            if (paramBoolean != bool4)
                break label564;
            bool5 = true;
            if (n != -1)
                break label584;
            if (!bool5)
                break label570;
            i12 = i9;
            label171: n = getOffsetBeforeAfter(i8, i9, i10, bool4, i12, bool5);
            if (!bool5)
                break label577;
        }
        label218: label234: int i5;
        while (true)
        {
            if (n != i10)
                break label600;
            i2 = i8;
            j = i11;
            break label50;
            bool1 = false;
            break;
            if (paramInt == i)
            {
                i2 = arrayOfInt.length;
                break label50;
            }
            i2 = 0;
            int i14;
            if (i2 < arrayOfInt.length)
            {
                k = 0 + arrayOfInt[i2];
                if (paramInt < k)
                    break label513;
                m = k + (0x3FFFFFF & arrayOfInt[(i2 + 1)]);
                if (m > i)
                    m = i;
                if (paramInt >= m)
                    break label513;
                j = 0x3F & arrayOfInt[(i2 + 1)] >>> 26;
                if (paramInt == k)
                {
                    int i13 = paramInt - 1;
                    i14 = 0;
                    label317: int i15 = arrayOfInt.length;
                    if (i14 < i15)
                    {
                        int i16 = 0 + arrayOfInt[i14];
                        if (i13 < i16)
                            break label507;
                        int i17 = i16 + (0x3FFFFFF & arrayOfInt[(i14 + 1)]);
                        if (i17 > i)
                            i17 = i;
                        if (i13 >= i17)
                            break label507;
                        int i18 = 0x3F & arrayOfInt[(i14 + 1)] >>> 26;
                        if (i18 >= j)
                            break label507;
                        i2 = i14;
                        j = i18;
                        k = i16;
                        m = i17;
                        i1 = 1;
                    }
                }
            }
            if (i2 == arrayOfInt.length)
                break label50;
            boolean bool2;
            label436: boolean bool3;
            label445: int i3;
            if ((j & 0x1) != 0)
            {
                bool2 = true;
                if (paramBoolean != bool2)
                    break label525;
                bool3 = true;
                if (!bool3)
                    break label531;
                i3 = m;
                label454: if ((paramInt == i3) && (bool3 == i1))
                    break label536;
                n = getOffsetBeforeAfter(i2, k, m, bool2, paramInt, bool3);
                if (!bool3)
                    break label538;
            }
            label513: label525: label531: label536: label538: for (int i4 = m; ; i4 = k)
            {
                if (n == i4)
                    break label543;
                i5 = n;
                return i5;
                label507: i14 += 2;
                break label317;
                i2 += 2;
                break label234;
                bool2 = false;
                break label436;
                bool3 = false;
                break label445;
                i3 = k;
                break label454;
                break;
            }
            label543: break label50;
            label545: i6 = 0;
            break label59;
            label551: i7 = -2;
            break label67;
            label558: bool4 = false;
            break label146;
            label564: bool5 = false;
            break label155;
            label570: i12 = i10;
            break label171;
            label577: i10 = i9;
        }
        label584: if (i11 < j)
        {
            if (!bool5)
                break label607;
            n = i9;
        }
        label600: label607: label614: 
        do
        {
            while (true)
            {
                i5 = n;
                break;
                n = i10;
            }
            if (n == -1)
            {
                if (i6 != 0);
                for (n = 1 + this.mLen; ; n = -1)
                    break;
            }
        }
        while (n > i);
        if (i6 != 0);
        for (int n = i; ; n = 0)
            break;
    }

    float measure(int paramInt, boolean paramBoolean, Paint.FontMetricsInt paramFontMetricsInt)
    {
        int i;
        float f1;
        if (paramBoolean)
        {
            i = paramInt - 1;
            if (i >= 0)
                break label26;
            f1 = 0.0F;
        }
        label26: label171: label186: label316: label573: 
        while (true)
        {
            return f1;
            i = paramInt;
            break;
            f1 = 0.0F;
            if (!this.mHasTabs)
            {
                if (this.mDirections == Layout.DIRS_ALL_LEFT_TO_RIGHT)
                    f1 = measureRun(0, paramInt, this.mLen, false, paramFontMetricsInt);
                else if (this.mDirections == Layout.DIRS_ALL_RIGHT_TO_LEFT)
                    f1 = measureRun(0, paramInt, this.mLen, true, paramFontMetricsInt);
            }
            else
            {
                char[] arrayOfChar = this.mChars;
                int[] arrayOfInt = this.mDirections.mDirections;
                for (int j = 0; ; j += 2)
                {
                    if (j >= arrayOfInt.length)
                        break label573;
                    int k = arrayOfInt[j];
                    int m = k + (0x3FFFFFF & arrayOfInt[(j + 1)]);
                    if (m > this.mLen)
                        m = this.mLen;
                    boolean bool1;
                    int n;
                    int i1;
                    int i2;
                    Bitmap localBitmap;
                    if ((0x4000000 & arrayOfInt[(j + 1)]) != 0)
                    {
                        bool1 = true;
                        n = k;
                        if (!this.mHasTabs)
                            break label374;
                        i1 = k;
                        if (i1 > m)
                            continue;
                        i2 = 0;
                        localBitmap = null;
                        if ((this.mHasTabs) && (i1 < m))
                        {
                            i2 = arrayOfChar[i1];
                            if ((i2 >= 55296) && (i2 < 56320) && (i1 + 1 < m))
                            {
                                i2 = Character.codePointAt(arrayOfChar, i1);
                                if ((i2 < Layout.MIN_EMOJI) || (i2 > Layout.MAX_EMOJI))
                                    break label381;
                                localBitmap = Layout.EMOJI_FACTORY.getBitmapFromAndroidPua(i2);
                            }
                        }
                    }
                    int i3;
                    boolean bool2;
                    label328: int i4;
                    while (true)
                        if ((i1 == m) || (i2 == 9) || (localBitmap != null))
                        {
                            if ((i < n) || (i >= i1))
                                break label398;
                            i3 = 1;
                            if (this.mDir != -1)
                                break label404;
                            bool2 = true;
                            if (bool2 != bool1)
                                break label410;
                            i4 = 1;
                            label338: if ((i3 == 0) || (i4 == 0))
                                break label416;
                            f1 += measureRun(n, paramInt, i1, bool1, paramFontMetricsInt);
                            break;
                            bool1 = false;
                            break label171;
                            label374: i1 = m;
                            break label186;
                            label381: if (i2 > 65535)
                                i1++;
                        }
                    while (true)
                    {
                        i1++;
                        break label186;
                        i3 = 0;
                        break label316;
                        bool2 = false;
                        break label328;
                        i4 = 0;
                        break label338;
                        float f2 = measureRun(n, i1, i1, bool1, paramFontMetricsInt);
                        if (i4 != 0);
                        while (true)
                        {
                            f1 += f2;
                            if (i3 == 0)
                                break label476;
                            f1 += measureRun(n, paramInt, i1, bool1, null);
                            break;
                            f2 = -f2;
                        }
                        label476: if (i2 == 9)
                        {
                            if (paramInt == i1)
                                break;
                            f1 = this.mDir * nextTab(f1 * this.mDir);
                            if (i == i1)
                                break;
                        }
                        if (localBitmap != null)
                        {
                            float f3 = ascent(i1);
                            f1 += localBitmap.getWidth() * -f3 / localBitmap.getHeight() * this.mDir;
                            i1++;
                        }
                        n = i1 + 1;
                    }
                }
            }
        }
    }

    float metrics(Paint.FontMetricsInt paramFontMetricsInt)
    {
        return measure(this.mLen, false, paramFontMetricsInt);
    }

    float nextTab(float paramFloat)
    {
        if (this.mTabs != null);
        for (float f = this.mTabs.nextTab(paramFloat); ; f = Layout.TabStops.nextDefaultStop(paramFloat, 20))
            return f;
    }

    void set(TextPaint paramTextPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, Layout.Directions paramDirections, boolean paramBoolean, Layout.TabStops paramTabStops)
    {
        this.mPaint = paramTextPaint;
        this.mText = paramCharSequence;
        this.mStart = paramInt1;
        this.mLen = (paramInt2 - paramInt1);
        this.mDir = paramInt3;
        this.mDirections = paramDirections;
        if (this.mDirections == null)
            throw new IllegalArgumentException("Directions cannot be null");
        this.mHasTabs = paramBoolean;
        this.mSpanned = null;
        int i = 0;
        boolean bool;
        label130: char[] arrayOfChar;
        if ((paramCharSequence instanceof Spanned))
        {
            this.mSpanned = ((Spanned)paramCharSequence);
            this.mReplacementSpanSpanSet.init(this.mSpanned, paramInt1, paramInt2);
            if (this.mReplacementSpanSpanSet.numberOfSpans > 0)
                i = 1;
        }
        else
        {
            if ((i == 0) && (!paramBoolean) && (paramDirections == Layout.DIRS_ALL_LEFT_TO_RIGHT))
                break label286;
            bool = true;
            this.mCharsValid = bool;
            if (!this.mCharsValid)
                break label299;
            if ((this.mChars == null) || (this.mChars.length < this.mLen))
                this.mChars = new char[ArrayUtils.idealCharArraySize(this.mLen)];
            TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, this.mChars, 0);
            if (i == 0)
                break label299;
            arrayOfChar = this.mChars;
        }
        int k;
        for (int j = paramInt1; ; j = k)
        {
            if (j >= paramInt2)
                break label299;
            k = this.mReplacementSpanSpanSet.getNextTransition(j, paramInt2);
            if (this.mReplacementSpanSpanSet.hasSpansIntersecting(j, k))
            {
                arrayOfChar[(j - paramInt1)] = 65532;
                int m = 1 + (j - paramInt1);
                int n = k - paramInt1;
                while (true)
                    if (m < n)
                    {
                        arrayOfChar[m] = 65279;
                        m++;
                        continue;
                        i = 0;
                        break;
                        label286: bool = false;
                        break label130;
                    }
            }
        }
        label299: this.mTabs = paramTabStops;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.TextLine
 * JD-Core Version:        0.6.2
 */