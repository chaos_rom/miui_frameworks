package android.text;

import com.android.internal.util.ArrayUtils;
import java.io.PrintStream;

class PackedObjectVector<E>
{
    private int mColumns;
    private int mRowGapLength;
    private int mRowGapStart;
    private int mRows;
    private Object[] mValues;

    public PackedObjectVector(int paramInt)
    {
        this.mColumns = paramInt;
        this.mRows = (ArrayUtils.idealIntArraySize(0) / this.mColumns);
        this.mRowGapStart = 0;
        this.mRowGapLength = this.mRows;
        this.mValues = new Object[this.mRows * this.mColumns];
    }

    private void growBuffer()
    {
        int i = ArrayUtils.idealIntArraySize((1 + size()) * this.mColumns) / this.mColumns;
        Object[] arrayOfObject = new Object[i * this.mColumns];
        int j = this.mRows - (this.mRowGapStart + this.mRowGapLength);
        System.arraycopy(this.mValues, 0, arrayOfObject, 0, this.mColumns * this.mRowGapStart);
        System.arraycopy(this.mValues, (this.mRows - j) * this.mColumns, arrayOfObject, (i - j) * this.mColumns, j * this.mColumns);
        this.mRowGapLength += i - this.mRows;
        this.mRows = i;
        this.mValues = arrayOfObject;
    }

    private void moveRowGapTo(int paramInt)
    {
        if (paramInt == this.mRowGapStart);
        while (true)
        {
            return;
            if (paramInt > this.mRowGapStart)
            {
                int n = paramInt + this.mRowGapLength - (this.mRowGapStart + this.mRowGapLength);
                for (int i1 = this.mRowGapStart + this.mRowGapLength; i1 < n + (this.mRowGapStart + this.mRowGapLength); i1++)
                {
                    int i2 = i1 - (this.mRowGapStart + this.mRowGapLength) + this.mRowGapStart;
                    for (int i3 = 0; i3 < this.mColumns; i3++)
                    {
                        Object localObject2 = this.mValues[(i3 + i1 * this.mColumns)];
                        this.mValues[(i3 + i2 * this.mColumns)] = localObject2;
                    }
                }
            }
            int i = this.mRowGapStart - paramInt;
            for (int j = -1 + (paramInt + i); j >= paramInt; j--)
            {
                int k = j - paramInt + this.mRowGapStart + this.mRowGapLength - i;
                for (int m = 0; m < this.mColumns; m++)
                {
                    Object localObject1 = this.mValues[(m + j * this.mColumns)];
                    this.mValues[(m + k * this.mColumns)] = localObject1;
                }
            }
            this.mRowGapStart = paramInt;
        }
    }

    public void deleteAt(int paramInt1, int paramInt2)
    {
        moveRowGapTo(paramInt1 + paramInt2);
        this.mRowGapStart -= paramInt2;
        this.mRowGapLength = (paramInt2 + this.mRowGapLength);
        if (this.mRowGapLength > 2 * size());
    }

    public void dump()
    {
        for (int i = 0; i < this.mRows; i++)
        {
            int j = 0;
            if (j < this.mColumns)
            {
                Object localObject = this.mValues[(j + i * this.mColumns)];
                if ((i < this.mRowGapStart) || (i >= this.mRowGapStart + this.mRowGapLength))
                    System.out.print(localObject + " ");
                while (true)
                {
                    j++;
                    break;
                    System.out.print("(" + localObject + ") ");
                }
            }
            System.out.print(" << \n");
        }
        System.out.print("-----\n\n");
    }

    public E getValue(int paramInt1, int paramInt2)
    {
        if (paramInt1 >= this.mRowGapStart)
            paramInt1 += this.mRowGapLength;
        return this.mValues[(paramInt2 + paramInt1 * this.mColumns)];
    }

    public void insertAt(int paramInt, E[] paramArrayOfE)
    {
        moveRowGapTo(paramInt);
        if (this.mRowGapLength == 0)
            growBuffer();
        this.mRowGapStart = (1 + this.mRowGapStart);
        this.mRowGapLength = (-1 + this.mRowGapLength);
        if (paramArrayOfE == null)
            for (int j = 0; j < this.mColumns; j++)
                setValue(paramInt, j, null);
        for (int i = 0; i < this.mColumns; i++)
            setValue(paramInt, i, paramArrayOfE[i]);
    }

    public void setValue(int paramInt1, int paramInt2, E paramE)
    {
        if (paramInt1 >= this.mRowGapStart)
            paramInt1 += this.mRowGapLength;
        this.mValues[(paramInt2 + paramInt1 * this.mColumns)] = paramE;
    }

    public int size()
    {
        return this.mRows - this.mRowGapLength;
    }

    public int width()
    {
        return this.mColumns;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.PackedObjectVector
 * JD-Core Version:        0.6.2
 */