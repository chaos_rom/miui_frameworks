package android.text;

import android.graphics.Paint;

public class TextPaint extends Paint
{
    public int baselineShift;
    public int bgColor;
    public float density = 1.0F;
    public int[] drawableState;
    public int linkColor;
    public int underlineColor = 0;
    public float underlineThickness;

    public TextPaint()
    {
    }

    public TextPaint(int paramInt)
    {
        super(paramInt);
    }

    public TextPaint(Paint paramPaint)
    {
        super(paramPaint);
    }

    public void set(TextPaint paramTextPaint)
    {
        super.set(paramTextPaint);
        this.bgColor = paramTextPaint.bgColor;
        this.baselineShift = paramTextPaint.baselineShift;
        this.linkColor = paramTextPaint.linkColor;
        this.drawableState = paramTextPaint.drawableState;
        this.density = paramTextPaint.density;
        this.underlineColor = paramTextPaint.underlineColor;
        this.underlineThickness = paramTextPaint.underlineThickness;
    }

    public void setUnderlineText(int paramInt, float paramFloat)
    {
        this.underlineColor = paramInt;
        this.underlineThickness = paramFloat;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.TextPaint
 * JD-Core Version:        0.6.2
 */