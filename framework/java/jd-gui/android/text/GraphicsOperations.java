package android.text;

import android.graphics.Canvas;
import android.graphics.Paint;

public abstract interface GraphicsOperations extends CharSequence
{
    public abstract void drawText(Canvas paramCanvas, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint);

    public abstract void drawTextRun(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, Paint paramPaint);

    public abstract float getTextRunAdvances(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, Paint paramPaint);

    public abstract float getTextRunAdvances(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float[] paramArrayOfFloat, int paramInt6, Paint paramPaint, int paramInt7);

    public abstract int getTextRunCursor(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint);

    public abstract int getTextWidths(int paramInt1, int paramInt2, float[] paramArrayOfFloat, Paint paramPaint);

    public abstract float measureText(int paramInt1, int paramInt2, Paint paramPaint);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.GraphicsOperations
 * JD-Core Version:        0.6.2
 */