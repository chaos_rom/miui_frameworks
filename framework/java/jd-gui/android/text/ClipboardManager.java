package android.text;

@Deprecated
public abstract class ClipboardManager
{
    public abstract CharSequence getText();

    public abstract boolean hasText();

    public abstract void setText(CharSequence paramCharSequence);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.ClipboardManager
 * JD-Core Version:        0.6.2
 */