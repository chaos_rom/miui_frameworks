package android.text;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Path;
import android.text.style.ParagraphStyle;
import android.util.FloatMath;

public class BoringLayout extends Layout
    implements TextUtils.EllipsizeCallback
{
    private static final char FIRST_RIGHT_TO_LEFT = '֐';
    private static final TextPaint sTemp = new TextPaint();
    int mBottom;
    private int mBottomPadding;
    int mDesc;
    private String mDirect;
    private int mEllipsizedCount;
    private int mEllipsizedStart;
    private int mEllipsizedWidth;
    private float mMax;
    private Paint mPaint;
    private int mTopPadding;

    public BoringLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean)
    {
        super(paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2);
        this.mEllipsizedWidth = paramInt;
        this.mEllipsizedStart = 0;
        this.mEllipsizedCount = 0;
        init(paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramMetrics, paramBoolean, true);
    }

    public BoringLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2)
    {
        super(paramCharSequence, paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2);
        if ((paramTruncateAt == null) || (paramTruncateAt == TextUtils.TruncateAt.MARQUEE))
        {
            this.mEllipsizedWidth = paramInt1;
            this.mEllipsizedStart = 0;
            this.mEllipsizedCount = 0;
        }
        for (boolean bool = true; ; bool = false)
        {
            init(getText(), paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2, paramMetrics, paramBoolean, bool);
            return;
            replaceWith(TextUtils.ellipsize(paramCharSequence, paramTextPaint, paramInt2, paramTruncateAt, true, this), paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2);
            this.mEllipsizedWidth = paramInt2;
        }
    }

    public static Metrics isBoring(CharSequence paramCharSequence, TextPaint paramTextPaint)
    {
        return isBoring(paramCharSequence, paramTextPaint, TextDirectionHeuristics.FIRSTSTRONG_LTR, null);
    }

    public static Metrics isBoring(CharSequence paramCharSequence, TextPaint paramTextPaint, Metrics paramMetrics)
    {
        return isBoring(paramCharSequence, paramTextPaint, TextDirectionHeuristics.FIRSTSTRONG_LTR, paramMetrics);
    }

    public static Metrics isBoring(CharSequence paramCharSequence, TextPaint paramTextPaint, TextDirectionHeuristic paramTextDirectionHeuristic)
    {
        return isBoring(paramCharSequence, paramTextPaint, paramTextDirectionHeuristic, null);
    }

    public static Metrics isBoring(CharSequence paramCharSequence, TextPaint paramTextPaint, TextDirectionHeuristic paramTextDirectionHeuristic, Metrics paramMetrics)
    {
        char[] arrayOfChar = TextUtils.obtain(500);
        int i = paramCharSequence.length();
        int j = 1;
        int k = 0;
        int i1;
        int i2;
        label69: label108: Metrics localMetrics;
        if (k < i)
        {
            int n = 500 + k;
            if (n > i)
                n = i;
            TextUtils.getChars(paramCharSequence, k, n, arrayOfChar, 0);
            i1 = n - k;
            i2 = 0;
            if (i2 >= i1)
                break label224;
            int i3 = arrayOfChar[i2];
            if ((i3 == 10) || (i3 == 9) || (i3 >= 1424))
                j = 0;
        }
        else
        {
            TextUtils.recycle(arrayOfChar);
            if ((j != 0) && ((paramCharSequence instanceof Spanned)) && (((Spanned)paramCharSequence).getSpans(0, i, ParagraphStyle.class).length > 0))
                j = 0;
            if (j == 0)
                break label257;
            localMetrics = paramMetrics;
            if (localMetrics == null)
                localMetrics = new Metrics();
            TextLine localTextLine = TextLine.obtain();
            localTextLine.set(paramTextPaint, paramCharSequence, 0, i, 1, Layout.DIRS_ALL_LEFT_TO_RIGHT, false, null);
            int m = (int)FloatMath.ceil(localTextLine.metrics(localMetrics));
            localMetrics.width = m;
            TextLine.recycle(localTextLine);
        }
        while (true)
        {
            return localMetrics;
            i2++;
            break label69;
            label224: if ((paramTextDirectionHeuristic != null) && (paramTextDirectionHeuristic.isRtl(arrayOfChar, 0, i1)))
            {
                j = 0;
                break label108;
            }
            k += 500;
            break;
            label257: localMetrics = null;
        }
    }

    public static BoringLayout make(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean)
    {
        return new BoringLayout(paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramMetrics, paramBoolean);
    }

    public static BoringLayout make(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2)
    {
        return new BoringLayout(paramCharSequence, paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2, paramMetrics, paramBoolean, paramTruncateAt, paramInt2);
    }

    public void draw(Canvas paramCanvas, Path paramPath, Paint paramPaint, int paramInt)
    {
        if ((this.mDirect != null) && (paramPath == null))
            paramCanvas.drawText(this.mDirect, 0.0F, this.mBottom - this.mDesc, this.mPaint);
        while (true)
        {
            return;
            super.draw(paramCanvas, paramPath, paramPaint, paramInt);
        }
    }

    public void ellipsized(int paramInt1, int paramInt2)
    {
        this.mEllipsizedStart = paramInt1;
        this.mEllipsizedCount = (paramInt2 - paramInt1);
    }

    public int getBottomPadding()
    {
        return this.mBottomPadding;
    }

    public int getEllipsisCount(int paramInt)
    {
        return this.mEllipsizedCount;
    }

    public int getEllipsisStart(int paramInt)
    {
        return this.mEllipsizedStart;
    }

    public int getEllipsizedWidth()
    {
        return this.mEllipsizedWidth;
    }

    public int getHeight()
    {
        return this.mBottom;
    }

    public boolean getLineContainsTab(int paramInt)
    {
        return false;
    }

    public int getLineCount()
    {
        return 1;
    }

    public int getLineDescent(int paramInt)
    {
        return this.mDesc;
    }

    public final Layout.Directions getLineDirections(int paramInt)
    {
        return Layout.DIRS_ALL_LEFT_TO_RIGHT;
    }

    public float getLineMax(int paramInt)
    {
        return this.mMax;
    }

    public int getLineStart(int paramInt)
    {
        if (paramInt == 0);
        for (int i = 0; ; i = getText().length())
            return i;
    }

    public int getLineTop(int paramInt)
    {
        if (paramInt == 0);
        for (int i = 0; ; i = this.mBottom)
            return i;
    }

    public int getParagraphDirection(int paramInt)
    {
        return 1;
    }

    public int getTopPadding()
    {
        return this.mTopPadding;
    }

    void init(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean1, boolean paramBoolean2)
    {
        int i;
        if (((paramCharSequence instanceof String)) && (paramAlignment == Layout.Alignment.ALIGN_NORMAL))
        {
            this.mDirect = paramCharSequence.toString();
            this.mPaint = paramTextPaint;
            if (!paramBoolean1)
                break label157;
            i = paramMetrics.bottom - paramMetrics.top;
            label46: if ((paramFloat1 != 1.0F) || (paramFloat2 != 0.0F))
                i = (int)(0.5F + (paramFloat2 + paramFloat1 * i));
            this.mBottom = i;
            if (!paramBoolean1)
                break label173;
            this.mDesc = (i + paramMetrics.top);
            label98: if (!paramBoolean2)
                break label188;
            this.mMax = paramMetrics.width;
        }
        while (true)
        {
            if (paramBoolean1)
            {
                this.mTopPadding = (paramMetrics.top - paramMetrics.ascent);
                this.mBottomPadding = (paramMetrics.bottom - paramMetrics.descent);
            }
            return;
            this.mDirect = null;
            break;
            label157: i = paramMetrics.descent - paramMetrics.ascent;
            break label46;
            label173: this.mDesc = (i + paramMetrics.ascent);
            break label98;
            label188: TextLine localTextLine = TextLine.obtain();
            localTextLine.set(paramTextPaint, paramCharSequence, 0, paramCharSequence.length(), 1, Layout.DIRS_ALL_LEFT_TO_RIGHT, false, null);
            this.mMax = ((int)FloatMath.ceil(localTextLine.metrics(null)));
            TextLine.recycle(localTextLine);
        }
    }

    public BoringLayout replaceOrMake(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean)
    {
        replaceWith(paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2);
        this.mEllipsizedWidth = paramInt;
        this.mEllipsizedStart = 0;
        this.mEllipsizedCount = 0;
        init(paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramMetrics, paramBoolean, true);
        return this;
    }

    public BoringLayout replaceOrMake(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2)
    {
        if ((paramTruncateAt == null) || (paramTruncateAt == TextUtils.TruncateAt.MARQUEE))
        {
            replaceWith(paramCharSequence, paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2);
            this.mEllipsizedWidth = paramInt1;
            this.mEllipsizedStart = 0;
            this.mEllipsizedCount = 0;
        }
        for (boolean bool = true; ; bool = false)
        {
            init(getText(), paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2, paramMetrics, paramBoolean, bool);
            return this;
            replaceWith(TextUtils.ellipsize(paramCharSequence, paramTextPaint, paramInt2, paramTruncateAt, true, this), paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2);
            this.mEllipsizedWidth = paramInt2;
        }
    }

    public static class Metrics extends Paint.FontMetricsInt
    {
        public int width;

        public String toString()
        {
            return super.toString() + " width=" + this.width;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.BoringLayout
 * JD-Core Version:        0.6.2
 */