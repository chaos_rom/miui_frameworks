package android.text;

import android.graphics.Paint.FontMetricsInt;
import android.text.style.UpdateLayout;
import android.text.style.WrapTogetherSpan;
import com.android.internal.util.ArrayUtils;
import java.lang.ref.WeakReference;

public class DynamicLayout extends Layout
{
    private static final int BLOCK_MINIMUM_CHARACTER_LENGTH = 400;
    private static final int COLUMNS_ELLIPSIZE = 5;
    private static final int COLUMNS_NORMAL = 3;
    private static final int DESCENT = 2;
    private static final int DIR = 0;
    private static final int DIR_SHIFT = 30;
    private static final int ELLIPSIS_COUNT = 4;
    private static final int ELLIPSIS_START = 3;
    private static final int ELLIPSIS_UNDEFINED = -2147483648;
    public static final int INVALID_BLOCK_INDEX = -1;
    private static final int PRIORITY = 128;
    private static final int START = 0;
    private static final int START_MASK = 536870911;
    private static final int TAB = 0;
    private static final int TAB_MASK = 536870912;
    private static final int TOP = 1;
    private static final Object[] sLock = new Object[0];
    private static StaticLayout sStaticLayout = new StaticLayout(null);
    private CharSequence mBase;
    private int[] mBlockEndLines;
    private int[] mBlockIndices;
    private int mBottomPadding;
    private CharSequence mDisplay;
    private boolean mEllipsize;
    private TextUtils.TruncateAt mEllipsizeAt;
    private int mEllipsizedWidth;
    private boolean mIncludePad;
    private PackedIntVector mInts;
    private int mNumberOfBlocks;
    private PackedObjectVector<Layout.Directions> mObjects;
    private int mTopPadding;
    private ChangeWatcher mWatcher;

    public DynamicLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this(paramCharSequence, paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramBoolean);
    }

    public DynamicLayout(CharSequence paramCharSequence1, CharSequence paramCharSequence2, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this(paramCharSequence1, paramCharSequence2, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramBoolean, null, 0);
    }

    public DynamicLayout(CharSequence paramCharSequence1, CharSequence paramCharSequence2, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2)
    {
        this(paramCharSequence1, paramCharSequence2, paramTextPaint, paramInt1, paramAlignment, TextDirectionHeuristics.FIRSTSTRONG_LTR, paramFloat1, paramFloat2, paramBoolean, paramTruncateAt, paramInt2);
    }

    public DynamicLayout(CharSequence paramCharSequence1, CharSequence paramCharSequence2, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2)
    {
    }

    private void addBlockAtOffset(int paramInt)
    {
        int i = getLineForOffset(paramInt);
        if (this.mBlockEndLines == null)
        {
            this.mBlockEndLines = new int[ArrayUtils.idealIntArraySize(1)];
            this.mBlockEndLines[this.mNumberOfBlocks] = i;
            this.mNumberOfBlocks = (1 + this.mNumberOfBlocks);
        }
        while (true)
        {
            return;
            if (i > this.mBlockEndLines[(-1 + this.mNumberOfBlocks)])
            {
                if (this.mNumberOfBlocks == this.mBlockEndLines.length)
                {
                    int[] arrayOfInt = new int[ArrayUtils.idealIntArraySize(1 + this.mNumberOfBlocks)];
                    System.arraycopy(this.mBlockEndLines, 0, arrayOfInt, 0, this.mNumberOfBlocks);
                    this.mBlockEndLines = arrayOfInt;
                }
                this.mBlockEndLines[this.mNumberOfBlocks] = i;
                this.mNumberOfBlocks = (1 + this.mNumberOfBlocks);
            }
        }
    }

    private void createBlocks()
    {
        int i = 400;
        this.mNumberOfBlocks = 0;
        CharSequence localCharSequence = this.mDisplay;
        while (true)
        {
            int j = TextUtils.indexOf(localCharSequence, '\n', i);
            if (j < 0)
            {
                addBlockAtOffset(localCharSequence.length());
                this.mBlockIndices = new int[this.mBlockEndLines.length];
                for (int k = 0; k < this.mBlockEndLines.length; k++)
                    this.mBlockIndices[k] = -1;
            }
            addBlockAtOffset(j);
            i = j + 400;
        }
    }

    private void reflow(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramCharSequence != this.mBase);
        while (true)
        {
            return;
            CharSequence localCharSequence = this.mDisplay;
            int i = localCharSequence.length();
            int j = TextUtils.lastIndexOf(localCharSequence, '\n', paramInt1 - 1);
            int k;
            int i2;
            int i3;
            int i4;
            label92: int i6;
            int i7;
            Spanned localSpanned;
            if (j < 0)
            {
                k = 0;
                int m = paramInt1 - k;
                int n = paramInt2 + m;
                int i1 = paramInt3 + m;
                i2 = paramInt1 - m;
                i3 = TextUtils.indexOf(localCharSequence, '\n', i2 + i1);
                if (i3 >= 0)
                    break label289;
                i4 = i;
                int i5 = i4 - (i2 + i1);
                i6 = n + i5;
                i7 = i1 + i5;
                if (!(localCharSequence instanceof Spanned))
                    break label303;
                localSpanned = (Spanned)localCharSequence;
            }
            int i26;
            label289: 
            do
            {
                i26 = 0;
                int i27 = i2 + i7;
                Object[] arrayOfObject3 = localSpanned.getSpans(i2, i27, WrapTogetherSpan.class);
                for (int i28 = 0; ; i28++)
                {
                    int i29 = arrayOfObject3.length;
                    if (i28 >= i29)
                        break;
                    int i30 = localSpanned.getSpanStart(arrayOfObject3[i28]);
                    int i31 = localSpanned.getSpanEnd(arrayOfObject3[i28]);
                    if (i30 < i2)
                    {
                        i26 = 1;
                        int i33 = i2 - i30;
                        i6 += i33;
                        i7 += i33;
                        i2 -= i33;
                    }
                    if (i31 > i2 + i7)
                    {
                        i26 = 1;
                        int i32 = i31 - (i2 + i7);
                        i6 += i32;
                        i7 += i32;
                    }
                }
                k = j + 1;
                break;
                i4 = i3 + 1;
                break label92;
            }
            while (i26 != 0);
            label303: int i8 = getLineForOffset(i2);
            int i9 = getLineTop(i8);
            int i10 = getLineForOffset(i2 + i6);
            if (i2 + i7 == i)
                i10 = getLineCount();
            int i11 = getLineTop(i10);
            int i12 = getLineCount();
            int i13;
            if (i10 == i12)
                i13 = 1;
            StaticLayout localStaticLayout;
            int i16;
            while (true)
            {
                synchronized (sLock)
                {
                    localStaticLayout = sStaticLayout;
                    sStaticLayout = null;
                    if (localStaticLayout == null)
                    {
                        localStaticLayout = new StaticLayout(null);
                        int i14 = i2 + i7;
                        TextPaint localTextPaint = getPaint();
                        int i15 = getWidth();
                        TextDirectionHeuristic localTextDirectionHeuristic = getTextDirectionHeuristic();
                        float f1 = getSpacingMultiplier();
                        float f2 = getSpacingAdd();
                        float f3 = this.mEllipsizedWidth;
                        TextUtils.TruncateAt localTruncateAt = this.mEllipsizeAt;
                        localStaticLayout.generate(localCharSequence, i2, i14, localTextPaint, i15, localTextDirectionHeuristic, f1, f2, false, true, f3, localTruncateAt);
                        i16 = localStaticLayout.getLineCount();
                        if ((i2 + i7 != i) && (localStaticLayout.getLineStart(i16 - 1) == i2 + i7))
                            i16--;
                        this.mInts.deleteAt(i8, i10 - i8);
                        this.mObjects.deleteAt(i8, i10 - i8);
                        int i17 = localStaticLayout.getLineTop(i16);
                        int i18 = 0;
                        int i19 = 0;
                        if ((this.mIncludePad) && (i8 == 0))
                        {
                            i18 = localStaticLayout.getTopPadding();
                            this.mTopPadding = i18;
                            i17 -= i18;
                        }
                        if ((this.mIncludePad) && (i13 != 0))
                        {
                            i19 = localStaticLayout.getBottomPadding();
                            this.mBottomPadding = i19;
                            i17 += i19;
                        }
                        this.mInts.adjustValuesBelow(i8, 0, i7 - i6);
                        this.mInts.adjustValuesBelow(i8, 1, i17 + (i9 - i11));
                        if (!this.mEllipsize)
                            break label897;
                        arrayOfInt = new int[5];
                        arrayOfInt[3] = -2147483648;
                        Layout.Directions[] arrayOfDirections = new Layout.Directions[1];
                        int i20 = 0;
                        if (i20 >= i16)
                            break;
                        int i21 = localStaticLayout.getLineStart(i20) | localStaticLayout.getParagraphDirection(i20) << 30;
                        if (!localStaticLayout.getLineContainsTab(i20))
                            break label905;
                        i22 = 536870912;
                        arrayOfInt[0] = (i22 | i21);
                        int i23 = i9 + localStaticLayout.getLineTop(i20);
                        if (i20 > 0)
                            i23 -= i18;
                        arrayOfInt[1] = i23;
                        int i24 = localStaticLayout.getLineDescent(i20);
                        int i25 = i16 - 1;
                        if (i20 == i25)
                            i24 += i19;
                        arrayOfInt[2] = i24;
                        arrayOfDirections[0] = localStaticLayout.getLineDirections(i20);
                        if (this.mEllipsize)
                        {
                            arrayOfInt[3] = localStaticLayout.getEllipsisStart(i20);
                            arrayOfInt[4] = localStaticLayout.getEllipsisCount(i20);
                        }
                        this.mInts.insertAt(i8 + i20, arrayOfInt);
                        this.mObjects.insertAt(i8 + i20, arrayOfDirections);
                        i20++;
                        continue;
                        i13 = 0;
                    }
                }
                localStaticLayout.prepare();
                continue;
                label897: int[] arrayOfInt = new int[3];
                continue;
                label905: int i22 = 0;
            }
            updateBlocks(i8, i10 - 1, i16);
            synchronized (sLock)
            {
                sStaticLayout = localStaticLayout;
                localStaticLayout.finish();
            }
        }
    }

    public int[] getBlockEndLines()
    {
        return this.mBlockEndLines;
    }

    public int[] getBlockIndices()
    {
        return this.mBlockIndices;
    }

    public int getBottomPadding()
    {
        return this.mBottomPadding;
    }

    public int getEllipsisCount(int paramInt)
    {
        if (this.mEllipsizeAt == null);
        for (int i = 0; ; i = this.mInts.getValue(paramInt, 4))
            return i;
    }

    public int getEllipsisStart(int paramInt)
    {
        if (this.mEllipsizeAt == null);
        for (int i = 0; ; i = this.mInts.getValue(paramInt, 3))
            return i;
    }

    public int getEllipsizedWidth()
    {
        return this.mEllipsizedWidth;
    }

    public boolean getLineContainsTab(int paramInt)
    {
        boolean bool = false;
        if ((0x20000000 & this.mInts.getValue(paramInt, 0)) != 0)
            bool = true;
        return bool;
    }

    public int getLineCount()
    {
        return -1 + this.mInts.size();
    }

    public int getLineDescent(int paramInt)
    {
        return this.mInts.getValue(paramInt, 2);
    }

    public final Layout.Directions getLineDirections(int paramInt)
    {
        return (Layout.Directions)this.mObjects.getValue(paramInt, 0);
    }

    public int getLineStart(int paramInt)
    {
        return 0x1FFFFFFF & this.mInts.getValue(paramInt, 0);
    }

    public int getLineTop(int paramInt)
    {
        return this.mInts.getValue(paramInt, 1);
    }

    public int getNumberOfBlocks()
    {
        return this.mNumberOfBlocks;
    }

    public int getParagraphDirection(int paramInt)
    {
        return this.mInts.getValue(paramInt, 0) >> 30;
    }

    public int getTopPadding()
    {
        return this.mTopPadding;
    }

    void setBlocksDataForTest(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt)
    {
        this.mBlockEndLines = new int[paramArrayOfInt1.length];
        this.mBlockIndices = new int[paramArrayOfInt2.length];
        System.arraycopy(paramArrayOfInt1, 0, this.mBlockEndLines, 0, paramArrayOfInt1.length);
        System.arraycopy(paramArrayOfInt2, 0, this.mBlockIndices, 0, paramArrayOfInt2.length);
        this.mNumberOfBlocks = paramInt;
    }

    void updateBlocks(int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mBlockEndLines == null)
            createBlocks();
        while (true)
        {
            return;
            int i = -1;
            int j = -1;
            int k = 0;
            label23: int m;
            label51: int n;
            int i1;
            label92: int i2;
            label101: int i3;
            if (k < this.mNumberOfBlocks)
            {
                if (this.mBlockEndLines[k] >= paramInt1)
                    i = k;
            }
            else
            {
                m = i;
                if (m < this.mNumberOfBlocks)
                {
                    if (this.mBlockEndLines[m] < paramInt2)
                        break label206;
                    j = m;
                }
                n = this.mBlockEndLines[j];
                if (i != 0)
                    break label212;
                i1 = 0;
                if (paramInt1 <= i1)
                    break label228;
                i2 = 1;
                if (paramInt3 <= 0)
                    break label234;
                i3 = 1;
                label108: if (paramInt2 >= this.mBlockEndLines[j])
                    break label240;
            }
            int i5;
            int i7;
            label206: label212: label228: label234: label240: for (int i4 = 1; ; i4 = 0)
            {
                i5 = 0;
                if (i2 != 0)
                    i5 = 0 + 1;
                if (i3 != 0)
                    i5++;
                if (i4 != 0)
                    i5++;
                int i6 = 1 + (j - i);
                i7 = i5 + this.mNumberOfBlocks - i6;
                if (i7 != 0)
                    break label246;
                this.mBlockEndLines[0] = 0;
                this.mBlockIndices[0] = -1;
                this.mNumberOfBlocks = 1;
                break;
                k++;
                break label23;
                m++;
                break label51;
                i1 = 1 + this.mBlockEndLines[(i - 1)];
                break label92;
                i2 = 0;
                break label101;
                i3 = 0;
                break label108;
            }
            label246: if (i7 > this.mBlockEndLines.length)
            {
                int i11 = ArrayUtils.idealIntArraySize(i7);
                int[] arrayOfInt2 = new int[i11];
                int[] arrayOfInt3 = new int[i11];
                System.arraycopy(this.mBlockEndLines, 0, arrayOfInt2, 0, i);
                System.arraycopy(this.mBlockIndices, 0, arrayOfInt3, 0, i);
                System.arraycopy(this.mBlockEndLines, j + 1, arrayOfInt2, i + i5, -1 + (this.mNumberOfBlocks - j));
                System.arraycopy(this.mBlockIndices, j + 1, arrayOfInt3, i + i5, -1 + (this.mNumberOfBlocks - j));
                this.mBlockEndLines = arrayOfInt2;
                this.mBlockIndices = arrayOfInt3;
            }
            int i8;
            while (true)
            {
                this.mNumberOfBlocks = i7;
                i8 = paramInt3 - (1 + (paramInt2 - paramInt1));
                for (int i9 = i + i5; i9 < this.mNumberOfBlocks; i9++)
                {
                    int[] arrayOfInt1 = this.mBlockEndLines;
                    arrayOfInt1[i9] = (i8 + arrayOfInt1[i9]);
                }
                System.arraycopy(this.mBlockEndLines, j + 1, this.mBlockEndLines, i + i5, -1 + (this.mNumberOfBlocks - j));
                System.arraycopy(this.mBlockIndices, j + 1, this.mBlockIndices, i + i5, -1 + (this.mNumberOfBlocks - j));
            }
            int i10 = i;
            if (i2 != 0)
            {
                this.mBlockEndLines[i10] = (paramInt1 - 1);
                this.mBlockIndices[i10] = -1;
                i10++;
            }
            if (i3 != 0)
            {
                this.mBlockEndLines[i10] = (-1 + (paramInt1 + paramInt3));
                this.mBlockIndices[i10] = -1;
                i10++;
            }
            if (i4 != 0)
            {
                this.mBlockEndLines[i10] = (n + i8);
                this.mBlockIndices[i10] = -1;
            }
        }
    }

    private static class ChangeWatcher
        implements TextWatcher, SpanWatcher
    {
        private WeakReference<DynamicLayout> mLayout;

        public ChangeWatcher(DynamicLayout paramDynamicLayout)
        {
            this.mLayout = new WeakReference(paramDynamicLayout);
        }

        private void reflow(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
        {
            DynamicLayout localDynamicLayout = (DynamicLayout)this.mLayout.get();
            if (localDynamicLayout != null)
                localDynamicLayout.reflow(paramCharSequence, paramInt1, paramInt2, paramInt3);
            while (true)
            {
                return;
                if ((paramCharSequence instanceof Spannable))
                    ((Spannable)paramCharSequence).removeSpan(this);
            }
        }

        public void afterTextChanged(Editable paramEditable)
        {
        }

        public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
        {
        }

        public void onSpanAdded(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
        {
            if ((paramObject instanceof UpdateLayout))
                reflow(paramSpannable, paramInt1, paramInt2 - paramInt1, paramInt2 - paramInt1);
        }

        public void onSpanChanged(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            if ((paramObject instanceof UpdateLayout))
            {
                reflow(paramSpannable, paramInt1, paramInt2 - paramInt1, paramInt2 - paramInt1);
                reflow(paramSpannable, paramInt3, paramInt4 - paramInt3, paramInt4 - paramInt3);
            }
        }

        public void onSpanRemoved(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2)
        {
            if ((paramObject instanceof UpdateLayout))
                reflow(paramSpannable, paramInt1, paramInt2 - paramInt1, paramInt2 - paramInt1);
        }

        public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
        {
            reflow(paramCharSequence, paramInt1, paramInt2, paramInt3);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.DynamicLayout
 * JD-Core Version:        0.6.2
 */