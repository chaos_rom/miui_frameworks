package android.text.format;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils
{
    public static final String ABBREV_MONTH_FORMAT = "%b";
    public static final String ABBREV_WEEKDAY_FORMAT = "%a";
    public static final long DAY_IN_MILLIS = 86400000L;
    private static final String FAST_FORMAT_HMMSS = "%1$d:%2$02d:%3$02d";
    private static final String FAST_FORMAT_MMSS = "%1$02d:%2$02d";
    public static final int FORMAT_12HOUR = 64;
    public static final int FORMAT_24HOUR = 128;
    public static final int FORMAT_ABBREV_ALL = 524288;
    public static final int FORMAT_ABBREV_MONTH = 65536;
    public static final int FORMAT_ABBREV_RELATIVE = 262144;
    public static final int FORMAT_ABBREV_TIME = 16384;
    public static final int FORMAT_ABBREV_WEEKDAY = 32768;
    public static final int FORMAT_CAP_AMPM = 256;
    public static final int FORMAT_CAP_MIDNIGHT = 4096;
    public static final int FORMAT_CAP_NOON = 1024;
    public static final int FORMAT_CAP_NOON_MIDNIGHT = 5120;
    public static final int FORMAT_NO_MIDNIGHT = 2048;
    public static final int FORMAT_NO_MONTH_DAY = 32;
    public static final int FORMAT_NO_NOON = 512;
    public static final int FORMAT_NO_NOON_MIDNIGHT = 2560;
    public static final int FORMAT_NO_YEAR = 8;
    public static final int FORMAT_NUMERIC_DATE = 131072;
    public static final int FORMAT_SHOW_DATE = 16;
    public static final int FORMAT_SHOW_TIME = 1;
    public static final int FORMAT_SHOW_WEEKDAY = 2;
    public static final int FORMAT_SHOW_YEAR = 4;

    @Deprecated
    public static final int FORMAT_UTC = 8192;
    public static final long HOUR_IN_MILLIS = 3600000L;
    public static final String HOUR_MINUTE_24 = "%H:%M";
    public static final int LENGTH_LONG = 10;
    public static final int LENGTH_MEDIUM = 20;
    public static final int LENGTH_SHORT = 30;
    public static final int LENGTH_SHORTER = 40;
    public static final int LENGTH_SHORTEST = 50;
    public static final long MINUTE_IN_MILLIS = 60000L;
    public static final String MONTH_DAY_FORMAT = "%-d";
    public static final String MONTH_FORMAT = "%B";
    public static final String NUMERIC_MONTH_FORMAT = "%m";
    public static final long SECOND_IN_MILLIS = 1000L;
    private static final char TIME_PADDING = '0';
    private static final char TIME_SEPARATOR = ':';
    public static final String WEEKDAY_FORMAT = "%A";
    public static final long WEEK_IN_MILLIS = 604800000L;
    public static final String YEAR_FORMAT = "%Y";
    public static final String YEAR_FORMAT_TWO_DIGITS = "%g";
    public static final long YEAR_IN_MILLIS = 31449600000L;
    private static final int[] sAmPm;
    private static final int[] sDaysLong;
    private static final int[] sDaysMedium;
    private static final int[] sDaysShort;
    private static final int[] sDaysShortest;
    private static String sElapsedFormatHMMSS;
    private static String sElapsedFormatMMSS;
    private static Configuration sLastConfig;
    private static final Object sLock = new Object();
    private static final int[] sMonthsLong;
    private static final int[] sMonthsMedium;
    private static final int[] sMonthsShortest;
    private static final int[] sMonthsStandaloneLong;
    private static Time sNowTime;
    private static java.text.DateFormat sStatusTimeFormat;
    private static Time sThenTime;
    public static final int[] sameMonthTable = arrayOfInt11;
    public static final int[] sameYearTable;

    static
    {
        int[] arrayOfInt1 = new int[7];
        arrayOfInt1[0] = 17039453;
        arrayOfInt1[1] = 17039454;
        arrayOfInt1[2] = 17039455;
        arrayOfInt1[3] = 17039456;
        arrayOfInt1[4] = 17039457;
        arrayOfInt1[5] = 17039458;
        arrayOfInt1[6] = 17039459;
        sDaysLong = arrayOfInt1;
        int[] arrayOfInt2 = new int[7];
        arrayOfInt2[0] = 17039460;
        arrayOfInt2[1] = 17039461;
        arrayOfInt2[2] = 17039462;
        arrayOfInt2[3] = 17039463;
        arrayOfInt2[4] = 17039464;
        arrayOfInt2[5] = 17039465;
        arrayOfInt2[6] = 17039466;
        sDaysMedium = arrayOfInt2;
        int[] arrayOfInt3 = new int[7];
        arrayOfInt3[0] = 17039467;
        arrayOfInt3[1] = 17039468;
        arrayOfInt3[2] = 17039469;
        arrayOfInt3[3] = 17039470;
        arrayOfInt3[4] = 17039471;
        arrayOfInt3[5] = 17039472;
        arrayOfInt3[6] = 17039473;
        sDaysShort = arrayOfInt3;
        int[] arrayOfInt4 = new int[7];
        arrayOfInt4[0] = 17039474;
        arrayOfInt4[1] = 17039475;
        arrayOfInt4[2] = 17039476;
        arrayOfInt4[3] = 17039477;
        arrayOfInt4[4] = 17039478;
        arrayOfInt4[5] = 17039479;
        arrayOfInt4[6] = 17039480;
        sDaysShortest = arrayOfInt4;
        int[] arrayOfInt5 = new int[12];
        arrayOfInt5[0] = 17039405;
        arrayOfInt5[1] = 17039406;
        arrayOfInt5[2] = 17039407;
        arrayOfInt5[3] = 17039408;
        arrayOfInt5[4] = 17039409;
        arrayOfInt5[5] = 17039410;
        arrayOfInt5[6] = 17039411;
        arrayOfInt5[7] = 17039412;
        arrayOfInt5[8] = 17039413;
        arrayOfInt5[9] = 17039414;
        arrayOfInt5[10] = 17039415;
        arrayOfInt5[11] = 17039416;
        sMonthsStandaloneLong = arrayOfInt5;
        int[] arrayOfInt6 = new int[12];
        arrayOfInt6[0] = 17039417;
        arrayOfInt6[1] = 17039418;
        arrayOfInt6[2] = 17039419;
        arrayOfInt6[3] = 17039420;
        arrayOfInt6[4] = 17039421;
        arrayOfInt6[5] = 17039422;
        arrayOfInt6[6] = 17039423;
        arrayOfInt6[7] = 17039424;
        arrayOfInt6[8] = 17039425;
        arrayOfInt6[9] = 17039426;
        arrayOfInt6[10] = 17039427;
        arrayOfInt6[11] = 17039428;
        sMonthsLong = arrayOfInt6;
        int[] arrayOfInt7 = new int[12];
        arrayOfInt7[0] = 17039429;
        arrayOfInt7[1] = 17039430;
        arrayOfInt7[2] = 17039431;
        arrayOfInt7[3] = 17039432;
        arrayOfInt7[4] = 17039433;
        arrayOfInt7[5] = 17039434;
        arrayOfInt7[6] = 17039435;
        arrayOfInt7[7] = 17039436;
        arrayOfInt7[8] = 17039437;
        arrayOfInt7[9] = 17039438;
        arrayOfInt7[10] = 17039439;
        arrayOfInt7[11] = 17039440;
        sMonthsMedium = arrayOfInt7;
        int[] arrayOfInt8 = new int[12];
        arrayOfInt8[0] = 17039441;
        arrayOfInt8[1] = 17039442;
        arrayOfInt8[2] = 17039443;
        arrayOfInt8[3] = 17039444;
        arrayOfInt8[4] = 17039445;
        arrayOfInt8[5] = 17039446;
        arrayOfInt8[6] = 17039447;
        arrayOfInt8[7] = 17039448;
        arrayOfInt8[8] = 17039449;
        arrayOfInt8[9] = 17039450;
        arrayOfInt8[10] = 17039451;
        arrayOfInt8[11] = 17039452;
        sMonthsShortest = arrayOfInt8;
        int[] arrayOfInt9 = new int[2];
        arrayOfInt9[0] = 17039481;
        arrayOfInt9[1] = 17039482;
        sAmPm = arrayOfInt9;
        int[] arrayOfInt10 = new int[16];
        arrayOfInt10[0] = 17039522;
        arrayOfInt10[1] = 17039523;
        arrayOfInt10[2] = 17039535;
        arrayOfInt10[3] = 17039537;
        arrayOfInt10[4] = 17039524;
        arrayOfInt10[5] = 17039526;
        arrayOfInt10[6] = 17039528;
        arrayOfInt10[7] = 17039530;
        arrayOfInt10[8] = 17039508;
        arrayOfInt10[9] = 17039509;
        arrayOfInt10[10] = 17039510;
        arrayOfInt10[11] = 17039511;
        arrayOfInt10[12] = 17039513;
        arrayOfInt10[13] = 17039514;
        arrayOfInt10[14] = 17039515;
        arrayOfInt10[15] = 17039512;
        sameYearTable = arrayOfInt10;
        int[] arrayOfInt11 = new int[16];
        arrayOfInt11[0] = 17039533;
        arrayOfInt11[1] = 17039534;
        arrayOfInt11[2] = 17039536;
        arrayOfInt11[3] = 17039532;
        arrayOfInt11[4] = 17039525;
        arrayOfInt11[5] = 17039527;
        arrayOfInt11[6] = 17039529;
        arrayOfInt11[7] = 17039531;
        arrayOfInt11[8] = 17039508;
        arrayOfInt11[9] = 17039509;
        arrayOfInt11[10] = 17039510;
        arrayOfInt11[11] = 17039511;
        arrayOfInt11[12] = 17039513;
        arrayOfInt11[13] = 17039514;
        arrayOfInt11[14] = 17039515;
        arrayOfInt11[15] = 17039512;
    }

    public static void assign(Calendar paramCalendar1, Calendar paramCalendar2)
    {
        paramCalendar1.clear();
        paramCalendar1.setTimeInMillis(paramCalendar2.getTimeInMillis());
    }

    public static String formatDateRange(Context paramContext, long paramLong1, long paramLong2, int paramInt)
    {
        return formatDateRange(paramContext, new Formatter(new StringBuilder(50), Locale.getDefault()), paramLong1, paramLong2, paramInt).toString();
    }

    public static Formatter formatDateRange(Context paramContext, Formatter paramFormatter, long paramLong1, long paramLong2, int paramInt)
    {
        return formatDateRange(paramContext, paramFormatter, paramLong1, paramLong2, paramInt, null);
    }

    public static Formatter formatDateRange(Context paramContext, Formatter paramFormatter, long paramLong1, long paramLong2, int paramInt, String paramString)
    {
        Resources localResources = Resources.getSystem();
        int i;
        int j;
        label25: int k;
        label35: int m;
        label46: int n;
        label58: int i1;
        label70: int i2;
        label82: int i3;
        label93: int i4;
        label104: int i5;
        label114: Time localTime1;
        label130: Time localTime2;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        String str1;
        String str2;
        String str24;
        label270: label288: String str3;
        String str4;
        String str22;
        int i17;
        label320: int i18;
        label331: boolean bool;
        label339: String str23;
        label358: label376: label381: String str5;
        label396: String str6;
        label416: Formatter localFormatter;
        if ((paramInt & 0x1) != 0)
        {
            i = 1;
            if ((paramInt & 0x2) == 0)
                break label472;
            j = 1;
            if ((paramInt & 0x4) == 0)
                break label478;
            k = 1;
            if ((paramInt & 0x8) == 0)
                break label484;
            m = 1;
            if ((paramInt & 0x2000) == 0)
                break label490;
            n = 1;
            if ((0x88000 & paramInt) == 0)
                break label496;
            i1 = 1;
            if ((0x90000 & paramInt) == 0)
                break label502;
            i2 = 1;
            if ((paramInt & 0x20) == 0)
                break label508;
            i3 = 1;
            if ((0x20000 & paramInt) == 0)
                break label514;
            i4 = 1;
            if (paramLong1 != paramLong2)
                break label520;
            i5 = 1;
            if (paramString == null)
                break label526;
            localTime1 = new Time(paramString);
            localTime1.set(paramLong1);
            if (i5 == 0)
                break label558;
            localTime2 = localTime1;
            i7 = 0;
            if ((i5 == 0) && ((localTime2.hour | localTime2.minute | localTime2.second) == 0) && ((i == 0) || (i7 <= 1)))
            {
                localTime2.monthDay = (-1 + localTime2.monthDay);
                localTime2.normalize(true);
            }
            i8 = localTime1.monthDay;
            i9 = localTime1.month;
            i10 = localTime1.year;
            i11 = localTime2.monthDay;
            i12 = localTime2.month;
            i13 = localTime2.year;
            str1 = "";
            str2 = "";
            if (j != 0)
            {
                if (i1 == 0)
                    break label642;
                str24 = "%a";
                str1 = localTime1.format(str24);
                if (i5 == 0)
                    break label649;
                str2 = str1;
            }
            str3 = "";
            str4 = "";
            if (i != 0)
            {
                str22 = "";
                if ((paramInt & 0x80) == 0)
                    break label661;
                i17 = 1;
                if ((paramInt & 0x40) == 0)
                    break label667;
                i18 = 1;
                if (i17 == 0)
                    break label673;
                bool = true;
                if (!bool)
                    break label693;
                str22 = localResources.getString(17039486);
                str23 = str22;
                str3 = localTime1.format(str23);
                if (i5 == 0)
                    break label1142;
                str4 = str3;
            }
            if (k == 0)
                break label1154;
            if (i4 == 0)
                break label1214;
            str5 = localResources.getString(17039491);
            if (j == 0)
                break label1366;
            if (i == 0)
                break label1353;
            str6 = localResources.getString(17039516);
            if ((i3 == 0) || (i9 != i12) || (i10 != i13))
                break label1397;
            Object[] arrayOfObject9 = new Object[1];
            arrayOfObject9[0] = localTime1.format(str5);
            localFormatter = paramFormatter.format("%s", arrayOfObject9);
        }
        label472: label478: label484: label490: label496: label753: label2163: 
        while (true)
        {
            return localFormatter;
            i = 0;
            break;
            j = 0;
            break label25;
            k = 0;
            break label35;
            m = 0;
            break label46;
            n = 0;
            break label58;
            i1 = 0;
            break label70;
            label502: i2 = 0;
            break label82;
            label508: i3 = 0;
            break label93;
            i4 = 0;
            break label104;
            i5 = 0;
            break label114;
            if (n != 0)
            {
                localTime1 = new Time("UTC");
                break label130;
            }
            localTime1 = new Time();
            break label130;
            if (paramString != null)
                localTime2 = new Time(paramString);
            while (true)
            {
                localTime2.set(paramLong2);
                int i6 = Time.getJulianDay(paramLong1, localTime1.gmtoff);
                i7 = Time.getJulianDay(paramLong2, localTime2.gmtoff) - i6;
                break;
                if (n != 0)
                    localTime2 = new Time("UTC");
                else
                    localTime2 = new Time();
            }
            str24 = "%A";
            break label270;
            str2 = localTime2.format(str24);
            break label288;
            i17 = 0;
            break label320;
            i18 = 0;
            break label331;
            if (i18 != 0)
            {
                bool = false;
                break label339;
            }
            bool = DateFormat.is24HourFormat(paramContext);
            break label339;
            int i19;
            int i20;
            int i21;
            int i22;
            int i23;
            int i24;
            label765: int i25;
            int i26;
            if ((0x84000 & paramInt) != 0)
            {
                i19 = 1;
                if ((paramInt & 0x100) == 0)
                    break label937;
                i20 = 1;
                if ((paramInt & 0x200) == 0)
                    break label943;
                i21 = 1;
                if ((paramInt & 0x400) == 0)
                    break label949;
                i22 = 1;
                if ((paramInt & 0x800) == 0)
                    break label955;
                i23 = 1;
                if ((paramInt & 0x1000) == 0)
                    break label961;
                i24 = 1;
                if ((localTime1.minute != 0) || (localTime1.second != 0))
                    break label967;
                i25 = 1;
                if ((localTime2.minute != 0) || (localTime2.second != 0))
                    break label973;
                i26 = 1;
                if ((i19 == 0) || (i25 == 0))
                    break label992;
                if (i20 == 0)
                    break label979;
                str23 = localResources.getString(17040182);
                if (i5 == 0)
                {
                    if ((i19 == 0) || (i26 == 0))
                        break label1036;
                    if (i20 == 0)
                        break label1023;
                    str22 = localResources.getString(17040182);
                    if ((localTime2.hour != 12) || (i26 == 0) || (i21 != 0))
                        break label1080;
                    if (i22 == 0)
                        break label1067;
                    str22 = localResources.getString(17040312);
                }
            }
            while (true)
            {
                if ((localTime1.hour != 12) || (i25 == 0) || (i21 != 0))
                    break label1127;
                if (i22 == 0)
                    break label1129;
                str23 = localResources.getString(17040312);
                break;
                i19 = 0;
                break label705;
                i20 = 0;
                break label717;
                i21 = 0;
                break label729;
                i22 = 0;
                break label741;
                i23 = 0;
                break label753;
                i24 = 0;
                break label765;
                i25 = 0;
                break label784;
                i26 = 0;
                break label803;
                str23 = localResources.getString(17040181);
                break label828;
                if (i20 != 0)
                {
                    str23 = localResources.getString(17039488);
                    break label828;
                }
                str23 = localResources.getString(17039487);
                break label828;
                label1023: str22 = localResources.getString(17040181);
                break label858;
                if (i20 != 0)
                {
                    str22 = localResources.getString(17039488);
                    break label858;
                }
                str22 = localResources.getString(17039487);
                break label858;
                str22 = localResources.getString(17040311);
                continue;
                if ((localTime2.hour == 0) && (i26 != 0) && (i23 == 0))
                    if (i24 != 0)
                        str22 = localResources.getString(17040314);
                    else
                        str22 = localResources.getString(17040313);
            }
            break label358;
            str23 = localResources.getString(17040311);
            break label358;
            label1142: str4 = localTime2.format(str22);
            break label376;
            if (m != 0)
            {
                k = 0;
                break label381;
            }
            if (i10 != i13)
            {
                k = 1;
                break label381;
            }
            Time localTime3 = new Time();
            localTime3.setToNow();
            if (i10 != localTime3.year);
            for (k = 1; ; k = 0)
                break;
            if (k != 0)
            {
                if (i2 != 0)
                {
                    if (i3 != 0)
                    {
                        str5 = localResources.getString(17039505);
                        break label396;
                    }
                    str5 = localResources.getString(17039499);
                    break label396;
                }
                if (i3 != 0)
                {
                    str5 = localResources.getString(17039502);
                    break label396;
                }
                str5 = localResources.getString(17039494);
                break label396;
            }
            if (i2 != 0)
            {
                if (i3 != 0)
                {
                    str5 = localResources.getString(17039504);
                    break label396;
                }
                str5 = localResources.getString(17039503);
                break label396;
            }
            if (i3 != 0)
            {
                str5 = localResources.getString(17039501);
                break label396;
            }
            str5 = localResources.getString(17039500);
            break label396;
            str6 = localResources.getString(17039517);
            break label416;
            if (i != 0)
            {
                str6 = localResources.getString(17039518);
                break label416;
            }
            str6 = localResources.getString(17039507);
            break label416;
            label1397: if ((i10 != i13) || (i3 != 0))
            {
                String str7 = localTime1.format(str5);
                String str8 = localTime2.format(str5);
                Object[] arrayOfObject1 = new Object[6];
                arrayOfObject1[0] = str1;
                arrayOfObject1[1] = str7;
                arrayOfObject1[2] = str3;
                arrayOfObject1[3] = str2;
                arrayOfObject1[4] = str8;
                arrayOfObject1[5] = str4;
                localFormatter = paramFormatter.format(str6, arrayOfObject1);
            }
            else
            {
                String str9;
                String str10;
                String str11;
                String str12;
                String str13;
                label1527: String str14;
                if (i4 != 0)
                {
                    str9 = "%m";
                    str10 = localTime1.format(str9);
                    str11 = localTime1.format("%-d");
                    str12 = localTime1.format("%Y");
                    if (i5 == 0)
                        break label1707;
                    str13 = null;
                    if (i5 == 0)
                        break label1719;
                    str14 = null;
                    label1535: if (i5 == 0)
                        break label1731;
                }
                for (String str15 = null; ; str15 = localTime2.format("%Y"))
                {
                    if (i9 == i12)
                        break label1743;
                    int i16 = 0;
                    if (j != 0)
                        i16 = 1;
                    if (k != 0)
                        i16 += 2;
                    if (i != 0)
                        i16 += 4;
                    if (i4 != 0)
                        i16 += 8;
                    String str21 = localResources.getString(sameYearTable[i16]);
                    Object[] arrayOfObject8 = new Object[10];
                    arrayOfObject8[0] = str1;
                    arrayOfObject8[1] = str10;
                    arrayOfObject8[2] = str11;
                    arrayOfObject8[3] = str12;
                    arrayOfObject8[4] = str3;
                    arrayOfObject8[5] = str2;
                    arrayOfObject8[6] = str13;
                    arrayOfObject8[7] = str14;
                    arrayOfObject8[8] = str15;
                    arrayOfObject8[9] = str4;
                    localFormatter = paramFormatter.format(str21, arrayOfObject8);
                    break;
                    if (i2 != 0)
                    {
                        str9 = localResources.getString(17039538);
                        break label1492;
                    }
                    str9 = "%B";
                    break label1492;
                    str13 = localTime2.format(str9);
                    break label1527;
                    str14 = localTime2.format("%-d");
                    break label1535;
                }
                if (i8 != i11)
                {
                    int i15 = 0;
                    if (j != 0)
                        i15 = 1;
                    if (k != 0)
                        i15 += 2;
                    if (i != 0)
                        i15 += 4;
                    if (i4 != 0)
                        i15 += 8;
                    String str20 = localResources.getString(sameMonthTable[i15]);
                    Object[] arrayOfObject7 = new Object[10];
                    arrayOfObject7[0] = str1;
                    arrayOfObject7[1] = str10;
                    arrayOfObject7[2] = str11;
                    arrayOfObject7[3] = str12;
                    arrayOfObject7[4] = str3;
                    arrayOfObject7[5] = str2;
                    arrayOfObject7[6] = str13;
                    arrayOfObject7[7] = str14;
                    arrayOfObject7[8] = str15;
                    arrayOfObject7[9] = str4;
                    localFormatter = paramFormatter.format(str20, arrayOfObject7);
                }
                else
                {
                    int i14;
                    String str16;
                    String str17;
                    String str18;
                    if ((paramInt & 0x10) != 0)
                    {
                        i14 = 1;
                        if ((i == 0) && (i14 == 0) && (j == 0))
                            i14 = 1;
                        str16 = "";
                        if (i != 0)
                        {
                            if (i5 == 0)
                                break label2017;
                            str16 = str3;
                        }
                        str17 = "";
                        str18 = "";
                        if (i14 == 0)
                            break label2114;
                        str18 = localTime1.format(str5);
                        if (j == 0)
                            break label2070;
                        if (i == 0)
                            break label2057;
                        str17 = localResources.getString(17039519);
                    }
                    do
                    {
                        while (true)
                        {
                            Object[] arrayOfObject3 = new Object[3];
                            arrayOfObject3[0] = str16;
                            arrayOfObject3[1] = str1;
                            arrayOfObject3[2] = str18;
                            localFormatter = paramFormatter.format(str17, arrayOfObject3);
                            break;
                            i14 = 0;
                            break label1893;
                            String str19 = localResources.getString(17039506);
                            Object[] arrayOfObject6 = new Object[2];
                            arrayOfObject6[0] = str3;
                            arrayOfObject6[1] = str4;
                            str16 = String.format(str19, arrayOfObject6);
                            break label1930;
                            str17 = localResources.getString(17039520);
                            continue;
                            if (i != 0)
                            {
                                str17 = localResources.getString(17039498);
                            }
                            else
                            {
                                Object[] arrayOfObject5 = new Object[1];
                                arrayOfObject5[0] = str18;
                                localFormatter = paramFormatter.format("%s", arrayOfObject5);
                                break;
                                if (j == 0)
                                    break label2163;
                                if (i == 0)
                                    break label2137;
                                str17 = localResources.getString(17039521);
                            }
                        }
                        Object[] arrayOfObject4 = new Object[1];
                        arrayOfObject4[0] = str1;
                        localFormatter = paramFormatter.format("%s", arrayOfObject4);
                        break;
                    }
                    while (i == 0);
                    Object[] arrayOfObject2 = new Object[1];
                    arrayOfObject2[0] = str16;
                    localFormatter = paramFormatter.format("%s", arrayOfObject2);
                }
            }
        }
    }

    public static String formatDateTime(Context paramContext, long paramLong, int paramInt)
    {
        return formatDateRange(paramContext, paramLong, paramLong, paramInt);
    }

    public static String formatElapsedTime(long paramLong)
    {
        return formatElapsedTime(null, paramLong);
    }

    public static String formatElapsedTime(StringBuilder paramStringBuilder, long paramLong)
    {
        initFormatStrings();
        long l1 = 0L;
        long l2 = 0L;
        if (paramLong >= 3600L)
        {
            l1 = paramLong / 3600L;
            paramLong -= 3600L * l1;
        }
        if (paramLong >= 60L)
        {
            l2 = paramLong / 60L;
            paramLong -= 60L * l2;
        }
        long l3 = paramLong;
        if (l1 > 0L);
        for (String str = formatElapsedTime(paramStringBuilder, sElapsedFormatHMMSS, l1, l2, l3); ; str = formatElapsedTime(paramStringBuilder, sElapsedFormatMMSS, l2, l3))
            return str;
    }

    private static String formatElapsedTime(StringBuilder paramStringBuilder, String paramString, long paramLong1, long paramLong2)
    {
        StringBuilder localStringBuilder;
        if ("%1$02d:%2$02d".equals(paramString))
        {
            localStringBuilder = paramStringBuilder;
            if (localStringBuilder == null)
            {
                localStringBuilder = new StringBuilder(8);
                if (paramLong1 >= 10L)
                    break label117;
                localStringBuilder.append('0');
                label44: localStringBuilder.append(toDigitChar(paramLong1 % 10L));
                localStringBuilder.append(':');
                if (paramLong2 >= 10L)
                    break label134;
                localStringBuilder.append('0');
                label83: localStringBuilder.append(toDigitChar(paramLong2 % 10L));
            }
        }
        label117: Object[] arrayOfObject;
        for (String str = localStringBuilder.toString(); ; str = String.format(paramString, arrayOfObject))
        {
            return str;
            localStringBuilder.setLength(0);
            break;
            localStringBuilder.append(toDigitChar(paramLong1 / 10L));
            break label44;
            label134: localStringBuilder.append(toDigitChar(paramLong2 / 10L));
            break label83;
            arrayOfObject = new Object[2];
            arrayOfObject[0] = Long.valueOf(paramLong1);
            arrayOfObject[1] = Long.valueOf(paramLong2);
        }
    }

    private static String formatElapsedTime(StringBuilder paramStringBuilder, String paramString, long paramLong1, long paramLong2, long paramLong3)
    {
        StringBuilder localStringBuilder;
        if ("%1$d:%2$02d:%3$02d".equals(paramString))
        {
            localStringBuilder = paramStringBuilder;
            if (localStringBuilder == null)
            {
                localStringBuilder = new StringBuilder(8);
                localStringBuilder.append(paramLong1);
                localStringBuilder.append(':');
                if (paramLong2 >= 10L)
                    break label134;
                localStringBuilder.append('0');
                label60: localStringBuilder.append(toDigitChar(paramLong2 % 10L));
                localStringBuilder.append(':');
                if (paramLong3 >= 10L)
                    break label152;
                localStringBuilder.append('0');
                label100: localStringBuilder.append(toDigitChar(paramLong3 % 10L));
            }
        }
        label134: label152: Object[] arrayOfObject;
        for (String str = localStringBuilder.toString(); ; str = String.format(paramString, arrayOfObject))
        {
            return str;
            localStringBuilder.setLength(0);
            break;
            localStringBuilder.append(toDigitChar(paramLong2 / 10L));
            break label60;
            localStringBuilder.append(toDigitChar(paramLong3 / 10L));
            break label100;
            arrayOfObject = new Object[3];
            arrayOfObject[0] = Long.valueOf(paramLong1);
            arrayOfObject[1] = Long.valueOf(paramLong2);
            arrayOfObject[2] = Long.valueOf(paramLong3);
        }
    }

    public static final CharSequence formatSameDayTime(long paramLong1, long paramLong2, int paramInt1, int paramInt2)
    {
        GregorianCalendar localGregorianCalendar1 = new GregorianCalendar();
        localGregorianCalendar1.setTimeInMillis(paramLong1);
        Date localDate = localGregorianCalendar1.getTime();
        GregorianCalendar localGregorianCalendar2 = new GregorianCalendar();
        localGregorianCalendar2.setTimeInMillis(paramLong2);
        if ((localGregorianCalendar1.get(1) == localGregorianCalendar2.get(1)) && (localGregorianCalendar1.get(2) == localGregorianCalendar2.get(2)) && (localGregorianCalendar1.get(5) == localGregorianCalendar2.get(5)));
        for (java.text.DateFormat localDateFormat = java.text.DateFormat.getTimeInstance(paramInt2); ; localDateFormat = java.text.DateFormat.getDateInstance(paramInt1))
            return localDateFormat.format(localDate);
    }

    public static String getAMPMString(int paramInt)
    {
        return Resources.getSystem().getString(sAmPm[(paramInt + 0)]);
    }

    public static String getDayOfWeekString(int paramInt1, int paramInt2)
    {
        int[] arrayOfInt;
        switch (paramInt2)
        {
        default:
            arrayOfInt = sDaysMedium;
        case 10:
        case 20:
        case 30:
        case 40:
        case 50:
        }
        while (true)
        {
            return Resources.getSystem().getString(arrayOfInt[(paramInt1 - 1)]);
            arrayOfInt = sDaysLong;
            continue;
            arrayOfInt = sDaysMedium;
            continue;
            arrayOfInt = sDaysShort;
            continue;
            arrayOfInt = sDaysShort;
            continue;
            arrayOfInt = sDaysShortest;
        }
    }

    public static String getMonthString(int paramInt1, int paramInt2)
    {
        int[] arrayOfInt;
        switch (paramInt2)
        {
        default:
            arrayOfInt = sMonthsMedium;
        case 10:
        case 20:
        case 30:
        case 40:
        case 50:
        }
        while (true)
        {
            return Resources.getSystem().getString(arrayOfInt[(paramInt1 + 0)]);
            arrayOfInt = sMonthsLong;
            continue;
            arrayOfInt = sMonthsMedium;
            continue;
            arrayOfInt = sMonthsMedium;
            continue;
            arrayOfInt = sMonthsMedium;
            continue;
            arrayOfInt = sMonthsShortest;
        }
    }

    /** @deprecated */
    private static long getNumberOfDaysPassed(long paramLong1, long paramLong2)
    {
        try
        {
            if (sThenTime == null)
                sThenTime = new Time();
            sThenTime.set(paramLong1);
            int i = Time.getJulianDay(paramLong1, sThenTime.gmtoff);
            sThenTime.set(paramLong2);
            int j = Math.abs(Time.getJulianDay(paramLong2, sThenTime.gmtoff) - i);
            long l = j;
            return l;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static CharSequence getRelativeDateTimeString(Context paramContext, long paramLong1, long paramLong2, long paramLong3, int paramInt)
    {
        Resources localResources = Resources.getSystem();
        long l1 = System.currentTimeMillis();
        long l2 = Math.abs(l1 - paramLong1);
        String str1;
        Object[] arrayOfObject2;
        if (paramLong3 > 604800000L)
        {
            paramLong3 = 604800000L;
            str1 = formatDateRange(paramContext, paramLong1, paramLong1, 1);
            if (l2 >= paramLong3)
                break label111;
            CharSequence localCharSequence2 = getRelativeTimeSpanString(paramLong1, l1, paramLong2, paramInt);
            arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = localCharSequence2;
            arrayOfObject2[1] = str1;
        }
        label111: Object[] arrayOfObject1;
        for (String str2 = localResources.getString(17040310, arrayOfObject2); ; str2 = localResources.getString(17039497, arrayOfObject1))
        {
            return str2;
            if (paramLong3 >= 86400000L)
                break;
            paramLong3 = 86400000L;
            break;
            CharSequence localCharSequence1 = getRelativeTimeSpanString(paramContext, paramLong1, false);
            arrayOfObject1 = new Object[2];
            arrayOfObject1[0] = localCharSequence1;
            arrayOfObject1[1] = str1;
        }
    }

    private static final String getRelativeDayString(Resources paramResources, long paramLong1, long paramLong2)
    {
        Time localTime1 = new Time();
        localTime1.set(paramLong1);
        Time localTime2 = new Time();
        localTime2.set(paramLong2);
        int i = Time.getJulianDay(paramLong1, localTime1.gmtoff);
        int j = Math.abs(Time.getJulianDay(paramLong2, localTime2.gmtoff) - i);
        int k;
        String str2;
        if (paramLong2 > paramLong1)
        {
            k = 1;
            if (j != 1)
                break label108;
            if (k == 0)
                break label96;
            str2 = paramResources.getString(17039483);
        }
        while (true)
        {
            return str2;
            k = 0;
            break;
            label96: str2 = paramResources.getString(17039485);
            continue;
            label108: if (j != 0)
                break label125;
            str2 = paramResources.getString(17039484);
        }
        label125: if (k != 0);
        for (int m = 18022404; ; m = 18022408)
        {
            String str1 = paramResources.getQuantityString(m, j);
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Integer.valueOf(j);
            str2 = String.format(str1, arrayOfObject);
            break;
        }
    }

    public static CharSequence getRelativeTimeSpanString(long paramLong)
    {
        return getRelativeTimeSpanString(paramLong, System.currentTimeMillis(), 60000L);
    }

    public static CharSequence getRelativeTimeSpanString(long paramLong1, long paramLong2, long paramLong3)
    {
        return getRelativeTimeSpanString(paramLong1, paramLong2, paramLong3, 65556);
    }

    public static CharSequence getRelativeTimeSpanString(long paramLong1, long paramLong2, long paramLong3, int paramInt)
    {
        Resources localResources = Resources.getSystem();
        int i;
        int j;
        label26: long l1;
        long l2;
        int k;
        label75: String str2;
        Object[] arrayOfObject;
        if ((0xC0000 & paramInt) != 0)
        {
            i = 1;
            if (paramLong2 < paramLong1)
                break label120;
            j = 1;
            l1 = Math.abs(paramLong2 - paramLong1);
            if ((l1 >= 60000L) || (paramLong3 >= 60000L))
                break label155;
            l2 = l1 / 1000L;
            if (j == 0)
                break label134;
            if (i == 0)
                break label126;
            k = 18022409;
            str2 = localResources.getQuantityString(k, (int)l2);
            arrayOfObject = new Object[1];
            arrayOfObject[0] = Long.valueOf(l2);
        }
        for (String str1 = String.format(str2, arrayOfObject); ; str1 = formatDateRange(null, paramLong1, paramLong1, paramInt))
        {
            return str1;
            i = 0;
            break;
            label120: j = 0;
            break label26;
            label126: k = 18022400;
            break label75;
            label134: if (i != 0)
            {
                k = 18022413;
                break label75;
            }
            k = 18022405;
            break label75;
            label155: if ((l1 < 3600000L) && (paramLong3 < 3600000L))
            {
                l2 = l1 / 60000L;
                if (j != 0)
                {
                    if (i != 0)
                    {
                        k = 18022410;
                        break label75;
                    }
                    k = 18022401;
                    break label75;
                }
                if (i != 0)
                {
                    k = 18022414;
                    break label75;
                }
                k = 18022406;
                break label75;
            }
            if ((l1 < 86400000L) && (paramLong3 < 86400000L))
            {
                l2 = l1 / 3600000L;
                if (j != 0)
                {
                    if (i != 0)
                    {
                        k = 18022411;
                        break label75;
                    }
                    k = 18022402;
                    break label75;
                }
                if (i != 0)
                {
                    k = 18022415;
                    break label75;
                }
                k = 18022407;
                break label75;
            }
            if ((l1 < 604800000L) && (paramLong3 < 604800000L))
            {
                l2 = getNumberOfDaysPassed(paramLong1, paramLong2);
                if (j != 0)
                {
                    if (i != 0)
                    {
                        k = 18022412;
                        break label75;
                    }
                    k = 18022404;
                    break label75;
                }
                if (i != 0)
                {
                    k = 18022416;
                    break label75;
                }
                k = 18022408;
                break label75;
            }
        }
    }

    public static CharSequence getRelativeTimeSpanString(Context paramContext, long paramLong)
    {
        return getRelativeTimeSpanString(paramContext, paramLong, false);
    }

    public static CharSequence getRelativeTimeSpanString(Context paramContext, long paramLong, boolean paramBoolean)
    {
        long l1 = System.currentTimeMillis();
        long l2 = l1 - paramLong;
        try
        {
            if (sNowTime == null)
                sNowTime = new Time();
            if (sThenTime == null)
                sThenTime = new Time();
            sNowTime.set(l1);
            sThenTime.set(paramLong);
            if ((l2 < 86400000L) && (sNowTime.weekDay == sThenTime.weekDay))
                str = formatDateRange(paramContext, paramLong, paramLong, 1);
            for (int i = 17040296; ; i = 17040295)
            {
                if (paramBoolean)
                {
                    Resources localResources = paramContext.getResources();
                    Object[] arrayOfObject = new Object[1];
                    arrayOfObject[0] = str;
                    str = localResources.getString(i, arrayOfObject);
                }
                return str;
                if (sNowTime.year == sThenTime.year)
                    break;
                str = formatDateRange(paramContext, paramLong, paramLong, 131092);
            }
            String str = formatDateRange(paramContext, paramLong, paramLong, 65552);
            i = 17040295;
        }
        finally
        {
        }
    }

    public static String getStandaloneMonthString(int paramInt1, int paramInt2)
    {
        int[] arrayOfInt;
        switch (paramInt2)
        {
        default:
            arrayOfInt = sMonthsMedium;
        case 10:
        case 20:
        case 30:
        case 40:
        case 50:
        }
        while (true)
        {
            return Resources.getSystem().getString(arrayOfInt[(paramInt1 + 0)]);
            arrayOfInt = sMonthsStandaloneLong;
            continue;
            arrayOfInt = sMonthsMedium;
            continue;
            arrayOfInt = sMonthsMedium;
            continue;
            arrayOfInt = sMonthsMedium;
            continue;
            arrayOfInt = sMonthsShortest;
        }
    }

    private static void initFormatStrings()
    {
        synchronized (sLock)
        {
            initFormatStringsLocked();
            return;
        }
    }

    private static void initFormatStringsLocked()
    {
        Resources localResources = Resources.getSystem();
        Configuration localConfiguration = localResources.getConfiguration();
        if ((sLastConfig == null) || (!sLastConfig.equals(localConfiguration)))
        {
            sLastConfig = localConfiguration;
            sStatusTimeFormat = java.text.DateFormat.getTimeInstance(3);
            sElapsedFormatMMSS = localResources.getString(17040315);
            sElapsedFormatHMMSS = localResources.getString(17040316);
        }
    }

    public static boolean isToday(long paramLong)
    {
        Time localTime = new Time();
        localTime.set(paramLong);
        int i = localTime.year;
        int j = localTime.month;
        int k = localTime.monthDay;
        localTime.set(System.currentTimeMillis());
        if ((i == localTime.year) && (j == localTime.month) && (k == localTime.monthDay));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isUTC(String paramString)
    {
        boolean bool = true;
        if ((paramString.length() == 16) && (paramString.charAt(15) == 'Z'));
        while (true)
        {
            return bool;
            if ((paramString.length() != 9) || (paramString.charAt(8) != 'Z'))
                bool = false;
        }
    }

    public static Calendar newCalendar(boolean paramBoolean)
    {
        if (paramBoolean);
        for (Calendar localCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT")); ; localCalendar = Calendar.getInstance())
            return localCalendar;
    }

    public static final CharSequence timeString(long paramLong)
    {
        synchronized (sLock)
        {
            initFormatStringsLocked();
            String str = sStatusTimeFormat.format(Long.valueOf(paramLong));
            return str;
        }
    }

    private static char toDigitChar(long paramLong)
    {
        return (char)(int)(48L + paramLong);
    }

    public static String writeDateTime(Calendar paramCalendar)
    {
        GregorianCalendar localGregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        localGregorianCalendar.setTimeInMillis(paramCalendar.getTimeInMillis());
        return writeDateTime(localGregorianCalendar, true);
    }

    public static String writeDateTime(Calendar paramCalendar, StringBuilder paramStringBuilder)
    {
        int i = paramCalendar.get(1);
        paramStringBuilder.setCharAt(3, (char)(48 + i % 10));
        int j = i / 10;
        paramStringBuilder.setCharAt(2, (char)(48 + j % 10));
        int k = j / 10;
        paramStringBuilder.setCharAt(1, (char)(48 + k % 10));
        paramStringBuilder.setCharAt(0, (char)(48 + k / 10 % 10));
        int m = 1 + paramCalendar.get(2);
        paramStringBuilder.setCharAt(5, (char)(48 + m % 10));
        paramStringBuilder.setCharAt(4, (char)(48 + m / 10 % 10));
        int n = paramCalendar.get(5);
        paramStringBuilder.setCharAt(7, (char)(48 + n % 10));
        paramStringBuilder.setCharAt(6, (char)(48 + n / 10 % 10));
        paramStringBuilder.setCharAt(8, 'T');
        int i1 = paramCalendar.get(11);
        paramStringBuilder.setCharAt(10, (char)(48 + i1 % 10));
        paramStringBuilder.setCharAt(9, (char)(48 + i1 / 10 % 10));
        int i2 = paramCalendar.get(12);
        paramStringBuilder.setCharAt(12, (char)(48 + i2 % 10));
        paramStringBuilder.setCharAt(11, (char)(48 + i2 / 10 % 10));
        int i3 = paramCalendar.get(13);
        paramStringBuilder.setCharAt(14, (char)(48 + i3 % 10));
        paramStringBuilder.setCharAt(13, (char)(48 + i3 / 10 % 10));
        return paramStringBuilder.toString();
    }

    public static String writeDateTime(Calendar paramCalendar, boolean paramBoolean)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.ensureCapacity(16);
        if (paramBoolean)
        {
            localStringBuilder.setLength(16);
            localStringBuilder.setCharAt(15, 'Z');
        }
        while (true)
        {
            return writeDateTime(paramCalendar, localStringBuilder);
            localStringBuilder.setLength(15);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.format.DateUtils
 * JD-Core Version:        0.6.2
 */