package android.text.format;

import android.content.res.Resources;
import java.util.Locale;
import java.util.TimeZone;

public class Time
{
    private static final int[] DAYS_PER_MONTH;
    public static final int EPOCH_JULIAN_DAY = 2440588;
    public static final int FRIDAY = 5;
    public static final int HOUR = 3;
    public static final int MINUTE = 2;
    public static final int MONDAY = 1;
    public static final int MONDAY_BEFORE_JULIAN_EPOCH = 2440585;
    public static final int MONTH = 5;
    public static final int MONTH_DAY = 4;
    public static final int SATURDAY = 6;
    public static final int SECOND = 1;
    public static final int SUNDAY = 0;
    public static final int THURSDAY = 4;
    public static final String TIMEZONE_UTC = "UTC";
    public static final int TUESDAY = 2;
    public static final int WEDNESDAY = 3;
    public static final int WEEK_DAY = 7;
    public static final int WEEK_NUM = 9;
    public static final int YEAR = 6;
    public static final int YEAR_DAY = 8;
    private static final String Y_M_D = "%Y-%m-%d";
    private static final String Y_M_D_T_H_M_S_000 = "%Y-%m-%dT%H:%M:%S.000";
    private static final String Y_M_D_T_H_M_S_000_Z = "%Y-%m-%dT%H:%M:%S.000Z";
    private static String sAm;
    private static String sDateCommand = "%a %b %e %H:%M:%S %Z %Y";
    private static String sDateOnlyFormat;
    private static String sDateTimeFormat;
    private static Locale sLocale;
    private static String[] sLongMonths;
    private static String[] sLongStandaloneMonths;
    private static String[] sLongWeekdays;
    private static String sPm;
    private static String[] sShortMonths;
    private static String[] sShortWeekdays;
    private static final int[] sThursdayOffset = arrayOfInt2;
    private static String sTimeOnlyFormat;
    public boolean allDay;
    public long gmtoff;
    public int hour;
    public int isDst;
    public int minute;
    public int month;
    public int monthDay;
    public int second;
    public String timezone;
    public int weekDay;
    public int year;
    public int yearDay;

    static
    {
        int[] arrayOfInt1 = new int[12];
        arrayOfInt1[0] = 31;
        arrayOfInt1[1] = 28;
        arrayOfInt1[2] = 31;
        arrayOfInt1[3] = 30;
        arrayOfInt1[4] = 31;
        arrayOfInt1[5] = 30;
        arrayOfInt1[6] = 31;
        arrayOfInt1[7] = 31;
        arrayOfInt1[8] = 30;
        arrayOfInt1[9] = 31;
        arrayOfInt1[10] = 30;
        arrayOfInt1[11] = 31;
        DAYS_PER_MONTH = arrayOfInt1;
        int[] arrayOfInt2 = new int[7];
        arrayOfInt2[0] = -3;
        arrayOfInt2[1] = 3;
        arrayOfInt2[2] = 2;
        arrayOfInt2[3] = 1;
        arrayOfInt2[4] = 0;
        arrayOfInt2[5] = -1;
        arrayOfInt2[6] = -2;
    }

    public Time()
    {
        this(TimeZone.getDefault().getID());
    }

    public Time(Time paramTime)
    {
        set(paramTime);
    }

    public Time(String paramString)
    {
        if (paramString == null)
            throw new NullPointerException("timezone is null!");
        this.timezone = paramString;
        this.year = 1970;
        this.monthDay = 1;
        this.isDst = -1;
    }

    public static int compare(Time paramTime1, Time paramTime2)
    {
        if (paramTime1 == null)
            throw new NullPointerException("a == null");
        if (paramTime2 == null)
            throw new NullPointerException("b == null");
        return nativeCompare(paramTime1, paramTime2);
    }

    private native String format1(String paramString);

    public static String getCurrentTimezone()
    {
        return TimeZone.getDefault().getID();
    }

    public static int getJulianDay(long paramLong1, long paramLong2)
    {
        return 2440588 + (int)((paramLong1 + paramLong2 * 1000L) / 86400000L);
    }

    public static int getJulianMondayFromWeeksSinceEpoch(int paramInt)
    {
        return 2440585 + paramInt * 7;
    }

    public static int getWeeksSinceEpochFromJulianDay(int paramInt1, int paramInt2)
    {
        int i = 4 - paramInt2;
        if (i < 0)
            i += 7;
        return (paramInt1 - (2440588 - i)) / 7;
    }

    public static boolean isEpoch(Time paramTime)
    {
        boolean bool = true;
        if (getJulianDay(paramTime.toMillis(bool), 0L) == 2440588);
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    private static native int nativeCompare(Time paramTime1, Time paramTime2);

    private native boolean nativeParse(String paramString);

    private native boolean nativeParse3339(String paramString);

    public boolean after(Time paramTime)
    {
        if (compare(this, paramTime) > 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean before(Time paramTime)
    {
        if (compare(this, paramTime) < 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void clear(String paramString)
    {
        if (paramString == null)
            throw new NullPointerException("timezone is null!");
        this.timezone = paramString;
        this.allDay = false;
        this.second = 0;
        this.minute = 0;
        this.hour = 0;
        this.monthDay = 0;
        this.month = 0;
        this.year = 0;
        this.weekDay = 0;
        this.yearDay = 0;
        this.gmtoff = 0L;
        this.isDst = -1;
    }

    public String format(String paramString)
    {
        try
        {
            Locale localLocale = Locale.getDefault();
            if ((sLocale == null) || (localLocale == null) || (!localLocale.equals(sLocale)))
            {
                Resources localResources = Resources.getSystem();
                String[] arrayOfString1 = new String[12];
                arrayOfString1[0] = localResources.getString(17039429);
                arrayOfString1[1] = localResources.getString(17039430);
                arrayOfString1[2] = localResources.getString(17039431);
                arrayOfString1[3] = localResources.getString(17039432);
                arrayOfString1[4] = localResources.getString(17039433);
                arrayOfString1[5] = localResources.getString(17039434);
                arrayOfString1[6] = localResources.getString(17039435);
                arrayOfString1[7] = localResources.getString(17039436);
                arrayOfString1[8] = localResources.getString(17039437);
                arrayOfString1[9] = localResources.getString(17039438);
                arrayOfString1[10] = localResources.getString(17039439);
                arrayOfString1[11] = localResources.getString(17039440);
                sShortMonths = arrayOfString1;
                String[] arrayOfString2 = new String[12];
                arrayOfString2[0] = localResources.getString(17039417);
                arrayOfString2[1] = localResources.getString(17039418);
                arrayOfString2[2] = localResources.getString(17039419);
                arrayOfString2[3] = localResources.getString(17039420);
                arrayOfString2[4] = localResources.getString(17039421);
                arrayOfString2[5] = localResources.getString(17039422);
                arrayOfString2[6] = localResources.getString(17039423);
                arrayOfString2[7] = localResources.getString(17039424);
                arrayOfString2[8] = localResources.getString(17039425);
                arrayOfString2[9] = localResources.getString(17039426);
                arrayOfString2[10] = localResources.getString(17039427);
                arrayOfString2[11] = localResources.getString(17039428);
                sLongMonths = arrayOfString2;
                String[] arrayOfString3 = new String[12];
                arrayOfString3[0] = localResources.getString(17039405);
                arrayOfString3[1] = localResources.getString(17039406);
                arrayOfString3[2] = localResources.getString(17039407);
                arrayOfString3[3] = localResources.getString(17039408);
                arrayOfString3[4] = localResources.getString(17039409);
                arrayOfString3[5] = localResources.getString(17039410);
                arrayOfString3[6] = localResources.getString(17039411);
                arrayOfString3[7] = localResources.getString(17039412);
                arrayOfString3[8] = localResources.getString(17039413);
                arrayOfString3[9] = localResources.getString(17039414);
                arrayOfString3[10] = localResources.getString(17039415);
                arrayOfString3[11] = localResources.getString(17039416);
                sLongStandaloneMonths = arrayOfString3;
                String[] arrayOfString4 = new String[7];
                arrayOfString4[0] = localResources.getString(17039460);
                arrayOfString4[1] = localResources.getString(17039461);
                arrayOfString4[2] = localResources.getString(17039462);
                arrayOfString4[3] = localResources.getString(17039463);
                arrayOfString4[4] = localResources.getString(17039464);
                arrayOfString4[5] = localResources.getString(17039465);
                arrayOfString4[6] = localResources.getString(17039466);
                sShortWeekdays = arrayOfString4;
                String[] arrayOfString5 = new String[7];
                arrayOfString5[0] = localResources.getString(17039453);
                arrayOfString5[1] = localResources.getString(17039454);
                arrayOfString5[2] = localResources.getString(17039455);
                arrayOfString5[3] = localResources.getString(17039456);
                arrayOfString5[4] = localResources.getString(17039457);
                arrayOfString5[5] = localResources.getString(17039458);
                arrayOfString5[6] = localResources.getString(17039459);
                sLongWeekdays = arrayOfString5;
                sTimeOnlyFormat = localResources.getString(17039495);
                sDateOnlyFormat = localResources.getString(17039494);
                sDateTimeFormat = localResources.getString(17039496);
                sAm = localResources.getString(17039481);
                sPm = localResources.getString(17039482);
                sLocale = localLocale;
            }
            String str = format1(paramString);
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public native String format2445();

    public String format3339(boolean paramBoolean)
    {
        if (paramBoolean);
        for (String str3 = format("%Y-%m-%d"); ; str3 = format("%Y-%m-%dT%H:%M:%S.000Z"))
        {
            return str3;
            if (!"UTC".equals(this.timezone))
                break;
        }
        String str1 = format("%Y-%m-%dT%H:%M:%S.000");
        if (this.gmtoff < 0L);
        for (String str2 = "-"; ; str2 = "+")
        {
            int i = (int)Math.abs(this.gmtoff);
            int j = i % 3600 / 60;
            int k = i / 3600;
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = str1;
            arrayOfObject[1] = str2;
            arrayOfObject[2] = Integer.valueOf(k);
            arrayOfObject[3] = Integer.valueOf(j);
            str3 = String.format("%s%s%02d:%02d", arrayOfObject);
            break;
        }
    }

    public int getActualMaximum(int paramInt)
    {
        int i = 28;
        switch (paramInt)
        {
        default:
            throw new RuntimeException("bad field=" + paramInt);
        case 1:
            i = 59;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
            while (true)
            {
                return i;
                i = 59;
                continue;
                i = 23;
                continue;
                int k = DAYS_PER_MONTH[this.month];
                if (k != i)
                {
                    i = k;
                }
                else
                {
                    int m = this.year;
                    if ((m % 4 == 0) && ((m % 100 != 0) || (m % 400 == 0)))
                    {
                        i = 29;
                        continue;
                        i = 11;
                        continue;
                        i = 2037;
                        continue;
                        i = 6;
                        continue;
                        int j = this.year;
                        if ((j % 4 == 0) && ((j % 100 != 0) || (j % 400 == 0)))
                            i = 365;
                        else
                            i = 364;
                    }
                }
            }
        case 9:
        }
        throw new RuntimeException("WEEK_NUM not implemented");
    }

    public int getWeekNumber()
    {
        int i = this.yearDay + sThursdayOffset[this.weekDay];
        if ((i >= 0) && (i <= 364));
        Time localTime;
        for (int j = 1 + i / 7; ; j = 1 + localTime.yearDay / 7)
        {
            return j;
            localTime = new Time(this);
            localTime.monthDay += sThursdayOffset[this.weekDay];
            localTime.normalize(true);
        }
    }

    public native long normalize(boolean paramBoolean);

    public boolean parse(String paramString)
    {
        if (nativeParse(paramString))
            this.timezone = "UTC";
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean parse3339(String paramString)
    {
        if (paramString == null)
            throw new NullPointerException("time string is null");
        if (nativeParse3339(paramString))
            this.timezone = "UTC";
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void set(int paramInt1, int paramInt2, int paramInt3)
    {
        this.allDay = true;
        this.second = 0;
        this.minute = 0;
        this.hour = 0;
        this.monthDay = paramInt1;
        this.month = paramInt2;
        this.year = paramInt3;
        this.weekDay = 0;
        this.yearDay = 0;
        this.isDst = -1;
        this.gmtoff = 0L;
    }

    public void set(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        this.allDay = false;
        this.second = paramInt1;
        this.minute = paramInt2;
        this.hour = paramInt3;
        this.monthDay = paramInt4;
        this.month = paramInt5;
        this.year = paramInt6;
        this.weekDay = 0;
        this.yearDay = 0;
        this.isDst = -1;
        this.gmtoff = 0L;
    }

    public native void set(long paramLong);

    public void set(Time paramTime)
    {
        this.timezone = paramTime.timezone;
        this.allDay = paramTime.allDay;
        this.second = paramTime.second;
        this.minute = paramTime.minute;
        this.hour = paramTime.hour;
        this.monthDay = paramTime.monthDay;
        this.month = paramTime.month;
        this.year = paramTime.year;
        this.weekDay = paramTime.weekDay;
        this.yearDay = paramTime.yearDay;
        this.isDst = paramTime.isDst;
        this.gmtoff = paramTime.gmtoff;
    }

    public long setJulianDay(int paramInt)
    {
        long l = 86400000L * (paramInt - 2440588);
        set(l);
        this.monthDay = (paramInt - getJulianDay(l, this.gmtoff) + this.monthDay);
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
        return normalize(true);
    }

    public native void setToNow();

    public native void switchTimezone(String paramString);

    public native long toMillis(boolean paramBoolean);

    public native String toString();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.format.Time
 * JD-Core Version:        0.6.2
 */