package android.text.format;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.provider.Settings.System;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateFormat
{
    public static final char AM_PM = 'a';
    public static final char CAPITAL_AM_PM = 'A';
    public static final char DATE = 'd';
    public static final char DAY = 'E';
    public static final char HOUR = 'h';
    public static final char HOUR_OF_DAY = 'k';
    public static final char MINUTE = 'm';
    public static final char MONTH = 'M';
    public static final char QUOTE = '\'';
    public static final char SECONDS = 's';
    public static final char TIME_ZONE = 'z';
    public static final char YEAR = 'y';
    private static boolean sIs24Hour;
    private static Locale sIs24HourLocale;
    private static final Object sLocaleLock = new Object();

    private static final int appendQuotedText(SpannableStringBuilder paramSpannableStringBuilder, int paramInt1, int paramInt2)
    {
        int i;
        if ((paramInt1 + 1 < paramInt2) && (paramSpannableStringBuilder.charAt(paramInt1 + 1) == '\''))
        {
            paramSpannableStringBuilder.delete(paramInt1, paramInt1 + 1);
            i = 1;
        }
        label124: 
        while (true)
        {
            return i;
            i = 0;
            paramSpannableStringBuilder.delete(paramInt1, paramInt1 + 1);
            int j = paramInt2 - 1;
            while (true)
            {
                if (paramInt1 >= j)
                    break label124;
                if (paramSpannableStringBuilder.charAt(paramInt1) == '\'')
                {
                    if ((paramInt1 + 1 < j) && (paramSpannableStringBuilder.charAt(paramInt1 + 1) == '\''))
                    {
                        paramSpannableStringBuilder.delete(paramInt1, paramInt1 + 1);
                        j--;
                        i++;
                        paramInt1++;
                        continue;
                    }
                    paramSpannableStringBuilder.delete(paramInt1, paramInt1 + 1);
                    break;
                }
                paramInt1++;
                i++;
            }
        }
    }

    public static final CharSequence format(CharSequence paramCharSequence, long paramLong)
    {
        return format(paramCharSequence, new Date(paramLong));
    }

    public static final CharSequence format(CharSequence paramCharSequence, Calendar paramCalendar)
    {
        SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramCharSequence);
        int i = paramCharSequence.length();
        int j = 0;
        while (j < i)
        {
            int k = 1;
            int m = localSpannableStringBuilder.charAt(j);
            if (m == 39)
            {
                k = appendQuotedText(localSpannableStringBuilder, j, i);
                i = localSpannableStringBuilder.length();
                j += k;
            }
            else
            {
                while ((j + k < i) && (localSpannableStringBuilder.charAt(j + k) == m))
                    k++;
                Object localObject2;
                switch (m)
                {
                default:
                    localObject2 = null;
                case 97:
                case 65:
                case 100:
                case 69:
                case 104:
                case 107:
                case 109:
                case 77:
                case 115:
                case 122:
                case 121:
                }
                while (localObject2 != null)
                {
                    localSpannableStringBuilder.replace(j, j + k, (CharSequence)localObject2);
                    k = ((String)localObject2).length();
                    i = localSpannableStringBuilder.length();
                    break;
                    localObject2 = DateUtils.getAMPMString(paramCalendar.get(9));
                    continue;
                    localObject2 = DateUtils.getAMPMString(paramCalendar.get(9));
                    continue;
                    localObject2 = zeroPad(paramCalendar.get(5), k);
                    continue;
                    int i1 = paramCalendar.get(7);
                    if (k < 4);
                    for (int i2 = 20; ; i2 = 10)
                    {
                        localObject2 = DateUtils.getDayOfWeekString(i1, i2);
                        break;
                    }
                    int n = paramCalendar.get(10);
                    if (n == 0)
                        n = 12;
                    localObject2 = zeroPad(n, k);
                    continue;
                    localObject2 = zeroPad(paramCalendar.get(11), k);
                    continue;
                    localObject2 = zeroPad(paramCalendar.get(12), k);
                    continue;
                    localObject2 = getMonthString(paramCalendar, k);
                    continue;
                    localObject2 = zeroPad(paramCalendar.get(13), k);
                    continue;
                    localObject2 = getTimeZoneString(paramCalendar, k);
                    continue;
                    localObject2 = getYearString(paramCalendar, k);
                }
            }
        }
        if ((paramCharSequence instanceof Spanned));
        for (Object localObject1 = new SpannedString(localSpannableStringBuilder); ; localObject1 = localSpannableStringBuilder.toString())
            return localObject1;
    }

    public static final CharSequence format(CharSequence paramCharSequence, Date paramDate)
    {
        GregorianCalendar localGregorianCalendar = new GregorianCalendar();
        localGregorianCalendar.setTime(paramDate);
        return format(paramCharSequence, localGregorianCalendar);
    }

    private static final String formatZoneOffset(int paramInt1, int paramInt2)
    {
        int i = paramInt1 / 1000;
        StringBuilder localStringBuilder = new StringBuilder();
        if (i < 0)
        {
            localStringBuilder.insert(0, "-");
            i = -i;
        }
        while (true)
        {
            int j = i / 3600;
            int k = i % 3600 / 60;
            localStringBuilder.append(zeroPad(j, 2));
            localStringBuilder.append(zeroPad(k, 2));
            return localStringBuilder.toString();
            localStringBuilder.insert(0, "+");
        }
    }

    public static final java.text.DateFormat getDateFormat(Context paramContext)
    {
        return getDateFormatForSetting(paramContext, Settings.System.getString(paramContext.getContentResolver(), "date_format"));
    }

    public static java.text.DateFormat getDateFormatForSetting(Context paramContext, String paramString)
    {
        return new SimpleDateFormat(getDateFormatStringForSetting(paramContext, paramString));
    }

    public static final char[] getDateFormatOrder(Context paramContext)
    {
        char[] arrayOfChar1 = new char[3];
        arrayOfChar1[0] = 100;
        arrayOfChar1[1] = 77;
        arrayOfChar1[2] = 121;
        String str = getDateFormatString(paramContext);
        int i = 0;
        int j = 0;
        int k = 0;
        int m = 0;
        for (int i2 : str.toCharArray())
        {
            if ((j == 0) && (i2 == 100))
            {
                j = 1;
                arrayOfChar1[i] = 'd';
                i++;
            }
            if ((k == 0) && (i2 == 77))
            {
                k = 1;
                arrayOfChar1[i] = 'M';
                i++;
            }
            if ((m == 0) && (i2 == 121))
            {
                m = 1;
                arrayOfChar1[i] = 'y';
                i++;
            }
        }
        return arrayOfChar1;
    }

    private static String getDateFormatString(Context paramContext)
    {
        return getDateFormatStringForSetting(paramContext, Settings.System.getString(paramContext.getContentResolver(), "date_format"));
    }

    private static String getDateFormatStringForSetting(Context paramContext, String paramString)
    {
        int i;
        int j;
        int k;
        String str2;
        String str3;
        if (paramString != null)
        {
            i = paramString.indexOf('M');
            j = paramString.indexOf('d');
            k = paramString.indexOf('y');
            if ((i >= 0) && (j >= 0) && (k >= 0))
            {
                str2 = paramContext.getString(17039493);
                if ((k < i) && (k < j))
                    if (i < j)
                    {
                        Object[] arrayOfObject6 = new Object[3];
                        arrayOfObject6[0] = "yyyy";
                        arrayOfObject6[1] = "MM";
                        arrayOfObject6[2] = "dd";
                        str3 = String.format(str2, arrayOfObject6);
                    }
            }
        }
        for (String str1 = str3; ; str1 = paramContext.getString(17039492))
        {
            return str1;
            Object[] arrayOfObject5 = new Object[3];
            arrayOfObject5[0] = "yyyy";
            arrayOfObject5[1] = "dd";
            arrayOfObject5[2] = "MM";
            str3 = String.format(str2, arrayOfObject5);
            break;
            if (i < j)
            {
                if (j < k)
                {
                    Object[] arrayOfObject4 = new Object[3];
                    arrayOfObject4[0] = "MM";
                    arrayOfObject4[1] = "dd";
                    arrayOfObject4[2] = "yyyy";
                    str3 = String.format(str2, arrayOfObject4);
                    break;
                }
                Object[] arrayOfObject3 = new Object[3];
                arrayOfObject3[0] = "MM";
                arrayOfObject3[1] = "yyyy";
                arrayOfObject3[2] = "dd";
                str3 = String.format(str2, arrayOfObject3);
                break;
            }
            if (i < k)
            {
                Object[] arrayOfObject2 = new Object[3];
                arrayOfObject2[0] = "dd";
                arrayOfObject2[1] = "MM";
                arrayOfObject2[2] = "yyyy";
                str3 = String.format(str2, arrayOfObject2);
                break;
            }
            Object[] arrayOfObject1 = new Object[3];
            arrayOfObject1[0] = "dd";
            arrayOfObject1[1] = "yyyy";
            arrayOfObject1[2] = "MM";
            str3 = String.format(str2, arrayOfObject1);
            break;
        }
    }

    public static final java.text.DateFormat getLongDateFormat(Context paramContext)
    {
        return java.text.DateFormat.getDateInstance(1);
    }

    public static final java.text.DateFormat getMediumDateFormat(Context paramContext)
    {
        return java.text.DateFormat.getDateInstance(2);
    }

    private static final String getMonthString(Calendar paramCalendar, int paramInt)
    {
        int i = paramCalendar.get(2);
        String str;
        if (paramInt >= 4)
            str = DateUtils.getMonthString(i, 10);
        while (true)
        {
            return str;
            if (paramInt == 3)
                str = DateUtils.getMonthString(i, 20);
            else
                str = zeroPad(i + 1, paramInt);
        }
    }

    public static final java.text.DateFormat getTimeFormat(Context paramContext)
    {
        if (is24HourFormat(paramContext));
        for (int i = 17039490; ; i = 17039489)
            return new SimpleDateFormat(paramContext.getString(i));
    }

    private static final String getTimeZoneString(Calendar paramCalendar, int paramInt)
    {
        TimeZone localTimeZone = paramCalendar.getTimeZone();
        String str;
        if (paramInt < 2)
        {
            str = formatZoneOffset(paramCalendar.get(16) + paramCalendar.get(15), paramInt);
            return str;
        }
        if (paramCalendar.get(16) != 0);
        for (boolean bool = true; ; bool = false)
        {
            str = localTimeZone.getDisplayName(bool, 0);
            break;
        }
    }

    private static final String getYearString(Calendar paramCalendar, int paramInt)
    {
        int i = paramCalendar.get(1);
        if (paramInt <= 2);
        for (String str = zeroPad(i % 100, 2); ; str = String.valueOf(i))
            return str;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static boolean is24HourFormat(Context paramContext)
    {
        int i = 1;
        String str = Injector.check24HourFormatForChina(paramContext, Settings.System.getString(paramContext.getContentResolver(), "time_12_24"));
        Locale localLocale;
        if (str == null)
        {
            localLocale = paramContext.getResources().getConfiguration().locale;
            synchronized (sLocaleLock)
            {
                if ((sIs24HourLocale != null) && (sIs24HourLocale.equals(localLocale)))
                {
                    i = sIs24Hour;
                    break label194;
                }
                java.text.DateFormat localDateFormat = java.text.DateFormat.getTimeInstance(i, localLocale);
                if ((localDateFormat instanceof SimpleDateFormat))
                    if (((SimpleDateFormat)localDateFormat).toPattern().indexOf('H') >= 0)
                        str = "24";
            }
        }
        label189: boolean bool;
        while (true)
        {
            synchronized (sLocaleLock)
            {
                sIs24HourLocale = localLocale;
                if (!str.equals("12"))
                {
                    int j = i;
                    sIs24Hour = j;
                    if ((str == null) || (str.equals("12")))
                        break label189;
                    break;
                    localObject2 = finally;
                    throw localObject2;
                    str = "12";
                    continue;
                    str = "12";
                    continue;
                }
                int k = 0;
            }
            bool = false;
        }
        label194: return bool;
    }

    private static final String zeroPad(int paramInt1, int paramInt2)
    {
        String str = String.valueOf(paramInt1);
        if (str.length() < paramInt2)
        {
            char[] arrayOfChar = new char[paramInt2];
            for (int i = 0; i < paramInt2; i++)
                arrayOfChar[i] = '0';
            str.getChars(0, str.length(), arrayOfChar, paramInt2 - str.length());
            str = new String(arrayOfChar);
        }
        return str;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String check24HourFormatForChina(Context paramContext, String paramString)
        {
            if (paramString == null)
            {
                Locale localLocale = paramContext.getResources().getConfiguration().locale;
                if (Locale.CHINA.equals(localLocale))
                    paramString = "24";
            }
            return paramString;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.format.DateFormat
 * JD-Core Version:        0.6.2
 */