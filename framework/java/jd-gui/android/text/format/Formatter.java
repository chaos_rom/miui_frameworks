package android.text.format;

import android.content.Context;
import android.content.res.Resources;
import android.net.NetworkUtils;
import java.net.InetAddress;

public final class Formatter
{
    public static String formatFileSize(Context paramContext, long paramLong)
    {
        return formatFileSize(paramContext, paramLong, false);
    }

    private static String formatFileSize(Context paramContext, long paramLong, boolean paramBoolean)
    {
        String str2;
        if (paramContext == null)
        {
            str2 = "";
            return str2;
        }
        float f = (float)paramLong;
        int i = 17039549;
        if (f > 900.0F)
        {
            i = 17039550;
            f /= 1024.0F;
        }
        if (f > 900.0F)
        {
            i = 17039551;
            f /= 1024.0F;
        }
        if (f > 900.0F)
        {
            i = 17039552;
            f /= 1024.0F;
        }
        if (f > 900.0F)
        {
            i = 17039553;
            f /= 1024.0F;
        }
        if (f > 900.0F)
        {
            i = 17039554;
            f /= 1024.0F;
        }
        String str1;
        if (f < 1.0F)
        {
            Object[] arrayOfObject7 = new Object[1];
            arrayOfObject7[0] = Float.valueOf(f);
            str1 = String.format("%.2f", arrayOfObject7);
        }
        while (true)
        {
            Resources localResources = paramContext.getResources();
            Object[] arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = str1;
            arrayOfObject2[1] = paramContext.getString(i);
            str2 = localResources.getString(17039555, arrayOfObject2);
            break;
            if (f < 10.0F)
            {
                if (paramBoolean)
                {
                    Object[] arrayOfObject6 = new Object[1];
                    arrayOfObject6[0] = Float.valueOf(f);
                    str1 = String.format("%.1f", arrayOfObject6);
                }
                else
                {
                    Object[] arrayOfObject5 = new Object[1];
                    arrayOfObject5[0] = Float.valueOf(f);
                    str1 = String.format("%.2f", arrayOfObject5);
                }
            }
            else if (f < 100.0F)
            {
                if (paramBoolean)
                {
                    Object[] arrayOfObject4 = new Object[1];
                    arrayOfObject4[0] = Float.valueOf(f);
                    str1 = String.format("%.0f", arrayOfObject4);
                }
                else
                {
                    Object[] arrayOfObject3 = new Object[1];
                    arrayOfObject3[0] = Float.valueOf(f);
                    str1 = String.format("%.2f", arrayOfObject3);
                }
            }
            else
            {
                Object[] arrayOfObject1 = new Object[1];
                arrayOfObject1[0] = Float.valueOf(f);
                str1 = String.format("%.0f", arrayOfObject1);
            }
        }
    }

    @Deprecated
    public static String formatIpAddress(int paramInt)
    {
        return NetworkUtils.intToInetAddress(paramInt).getHostAddress();
    }

    public static String formatShortFileSize(Context paramContext, long paramLong)
    {
        return formatFileSize(paramContext, paramLong, true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.format.Formatter
 * JD-Core Version:        0.6.2
 */