package android.text;

import android.os.Parcelable;

public abstract interface ParcelableSpan extends Parcelable
{
    public abstract int getSpanTypeId();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.ParcelableSpan
 * JD-Core Version:        0.6.2
 */