package android.text;

public class SpannableString extends SpannableStringInternal
    implements CharSequence, GetChars, Spannable
{
    public SpannableString(CharSequence paramCharSequence)
    {
        super(paramCharSequence, 0, paramCharSequence.length());
    }

    private SpannableString(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        super(paramCharSequence, paramInt1, paramInt2);
    }

    public static SpannableString valueOf(CharSequence paramCharSequence)
    {
        if ((paramCharSequence instanceof SpannableString));
        for (SpannableString localSpannableString = (SpannableString)paramCharSequence; ; localSpannableString = new SpannableString(paramCharSequence))
            return localSpannableString;
    }

    public void removeSpan(Object paramObject)
    {
        super.removeSpan(paramObject);
    }

    public void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3)
    {
        super.setSpan(paramObject, paramInt1, paramInt2, paramInt3);
    }

    public final CharSequence subSequence(int paramInt1, int paramInt2)
    {
        return new SpannableString(this, paramInt1, paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.SpannableString
 * JD-Core Version:        0.6.2
 */