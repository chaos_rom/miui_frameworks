package android.text;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.View;
import java.util.Locale;

public class AutoText
{
    private static final int DEFAULT = 14337;
    private static final int INCREMENT = 1024;
    private static final int RIGHT = 9300;
    private static final int TRIE_C = 0;
    private static final int TRIE_CHILD = 2;
    private static final int TRIE_NEXT = 3;
    private static final char TRIE_NULL = '￿';
    private static final int TRIE_OFF = 1;
    private static final int TRIE_ROOT = 0;
    private static final int TRIE_SIZEOF = 4;
    private static AutoText sInstance = new AutoText(Resources.getSystem());
    private static Object sLock = new Object();
    private Locale mLocale;
    private int mSize;
    private String mText;
    private char[] mTrie;
    private char mTrieUsed;

    private AutoText(Resources paramResources)
    {
        this.mLocale = paramResources.getConfiguration().locale;
        init(paramResources);
    }

    private void add(String paramString, char paramChar)
    {
        int i = paramString.length();
        int j = 0;
        this.mSize = (1 + this.mSize);
        label217: label242: for (int k = 0; ; k++)
        {
            int m;
            int n;
            if (k < i)
            {
                m = paramString.charAt(k);
                n = 0;
            }
            while (true)
            {
                if (this.mTrie[j] != 65535)
                {
                    if (m != this.mTrie[('\000' + this.mTrie[j])])
                        break label217;
                    if (k == i - 1)
                        this.mTrie[('\001' + this.mTrie[j])] = paramChar;
                }
                while (true)
                {
                    return;
                    j = '\002' + this.mTrie[j];
                    n = 1;
                    if (n != 0)
                        break label242;
                    int i1 = newTrieNode();
                    this.mTrie[j] = i1;
                    this.mTrie[('\000' + this.mTrie[j])] = m;
                    this.mTrie[('\001' + this.mTrie[j])] = 65535;
                    this.mTrie[('\003' + this.mTrie[j])] = 65535;
                    this.mTrie[('\002' + this.mTrie[j])] = 65535;
                    if (k != i - 1)
                        break;
                    this.mTrie[('\001' + this.mTrie[j])] = paramChar;
                }
                j = '\003' + this.mTrie[j];
            }
            j = '\002' + this.mTrie[j];
        }
    }

    public static String get(CharSequence paramCharSequence, int paramInt1, int paramInt2, View paramView)
    {
        return getInstance(paramView).lookup(paramCharSequence, paramInt1, paramInt2);
    }

    private static AutoText getInstance(View paramView)
    {
        Resources localResources = paramView.getContext().getResources();
        Locale localLocale = localResources.getConfiguration().locale;
        synchronized (sLock)
        {
            AutoText localAutoText = sInstance;
            if (!localLocale.equals(localAutoText.mLocale))
            {
                localAutoText = new AutoText(localResources);
                sInstance = localAutoText;
            }
            return localAutoText;
        }
    }

    private int getSize()
    {
        return this.mSize;
    }

    public static int getSize(View paramView)
    {
        return getInstance(paramView).getSize();
    }

    // ERROR //
    private void init(Resources paramResources)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc 125
        //     3: invokevirtual 129	android/content/res/Resources:getXml	(I)Landroid/content/res/XmlResourceParser;
        //     6: astore_2
        //     7: new 131	java/lang/StringBuilder
        //     10: dup
        //     11: sipush 9300
        //     14: invokespecial 134	java/lang/StringBuilder:<init>	(I)V
        //     17: astore_3
        //     18: aload_0
        //     19: sipush 14337
        //     22: newarray char
        //     24: putfield 85	android/text/AutoText:mTrie	[C
        //     27: aload_0
        //     28: getfield 85	android/text/AutoText:mTrie	[C
        //     31: iconst_0
        //     32: ldc 20
        //     34: castore
        //     35: aload_0
        //     36: iconst_1
        //     37: putfield 136	android/text/AutoText:mTrieUsed	C
        //     40: aload_2
        //     41: ldc 138
        //     43: invokestatic 144	com/android/internal/util/XmlUtils:beginDocument	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
        //     46: aload_2
        //     47: invokestatic 148	com/android/internal/util/XmlUtils:nextElement	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     50: aload_2
        //     51: invokeinterface 154 1 0
        //     56: astore 7
        //     58: aload 7
        //     60: ifnull +13 -> 73
        //     63: aload 7
        //     65: ldc 156
        //     67: invokevirtual 157	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     70: ifne +22 -> 92
        //     73: aload_1
        //     74: invokevirtual 160	android/content/res/Resources:flushLayoutCache	()V
        //     77: aload_2
        //     78: invokeinterface 163 1 0
        //     83: aload_0
        //     84: aload_3
        //     85: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     88: putfield 168	android/text/AutoText:mText	Ljava/lang/String;
        //     91: return
        //     92: aload_2
        //     93: aconst_null
        //     94: ldc 170
        //     96: invokeinterface 174 3 0
        //     101: astore 8
        //     103: aload_2
        //     104: invokeinterface 177 1 0
        //     109: iconst_4
        //     110: if_icmpne -64 -> 46
        //     113: aload_2
        //     114: invokeinterface 180 1 0
        //     119: astore 9
        //     121: aload 9
        //     123: ldc 182
        //     125: invokevirtual 157	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     128: ifeq +40 -> 168
        //     131: iconst_0
        //     132: istore 10
        //     134: aload_0
        //     135: aload 8
        //     137: iload 10
        //     139: invokespecial 184	android/text/AutoText:add	(Ljava/lang/String;C)V
        //     142: goto -96 -> 46
        //     145: astore 6
        //     147: new 186	java/lang/RuntimeException
        //     150: dup
        //     151: aload 6
        //     153: invokespecial 189	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     156: athrow
        //     157: astore 5
        //     159: aload_2
        //     160: invokeinterface 163 1 0
        //     165: aload 5
        //     167: athrow
        //     168: aload_3
        //     169: invokevirtual 190	java/lang/StringBuilder:length	()I
        //     172: i2c
        //     173: istore 10
        //     175: aload_3
        //     176: aload 9
        //     178: invokevirtual 77	java/lang/String:length	()I
        //     181: i2c
        //     182: invokevirtual 194	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     185: pop
        //     186: aload_3
        //     187: aload 9
        //     189: invokevirtual 197	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     192: pop
        //     193: goto -59 -> 134
        //     196: astore 4
        //     198: new 186	java/lang/RuntimeException
        //     201: dup
        //     202: aload 4
        //     204: invokespecial 189	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     207: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     40	77	145	org/xmlpull/v1/XmlPullParserException
        //     92	142	145	org/xmlpull/v1/XmlPullParserException
        //     168	193	145	org/xmlpull/v1/XmlPullParserException
        //     40	77	157	finally
        //     92	142	157	finally
        //     147	157	157	finally
        //     168	193	157	finally
        //     198	208	157	finally
        //     40	77	196	java/io/IOException
        //     92	142	196	java/io/IOException
        //     168	193	196	java/io/IOException
    }

    private String lookup(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        String str = null;
        int i = this.mTrie[0];
        int j = paramInt1;
        int k;
        if (j < paramInt2)
            k = paramCharSequence.charAt(j);
        while (true)
        {
            if (i != 65535)
            {
                if (k != this.mTrie[(i + 0)])
                    break label142;
                if ((j == paramInt2 - 1) && (this.mTrie[(i + 1)] != 65535))
                {
                    m = this.mTrie[(i + 1)];
                    n = this.mText.charAt(m);
                    str = this.mText.substring(m + 1, n + (m + 1));
                }
            }
            while (i == 65535)
            {
                int m;
                int n;
                return str;
                i = this.mTrie[(i + 2)];
            }
            j++;
            break;
            label142: i = this.mTrie[(i + 3)];
        }
    }

    private char newTrieNode()
    {
        if ('\004' + this.mTrieUsed > this.mTrie.length)
        {
            char[] arrayOfChar = new char[1024 + this.mTrie.length];
            System.arraycopy(this.mTrie, 0, arrayOfChar, 0, this.mTrie.length);
            this.mTrie = arrayOfChar;
        }
        char c = this.mTrieUsed;
        this.mTrieUsed = ('\004' + this.mTrieUsed);
        return c;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.AutoText
 * JD-Core Version:        0.6.2
 */