package android.text;

import java.lang.reflect.Array;

public class SpanSet<E>
{
    private final Class<? extends E> classType;
    int numberOfSpans;
    int[] spanEnds;
    int[] spanFlags;
    int[] spanStarts;
    E[] spans;

    SpanSet(Class<? extends E> paramClass)
    {
        this.classType = paramClass;
        this.numberOfSpans = 0;
    }

    int getNextTransition(int paramInt1, int paramInt2)
    {
        for (int i = 0; i < this.numberOfSpans; i++)
        {
            int j = this.spanStarts[i];
            int k = this.spanEnds[i];
            if ((j > paramInt1) && (j < paramInt2))
                paramInt2 = j;
            if ((k > paramInt1) && (k < paramInt2))
                paramInt2 = k;
        }
        return paramInt2;
    }

    public boolean hasSpansIntersecting(int paramInt1, int paramInt2)
    {
        for (int i = 0; ; i++)
        {
            if (i >= this.numberOfSpans)
                break label42;
            if ((this.spanStarts[i] < paramInt2) && (this.spanEnds[i] > paramInt1))
                break;
        }
        label42: for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void init(Spanned paramSpanned, int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = paramSpanned.getSpans(paramInt1, paramInt2, this.classType);
        int i = arrayOfObject.length;
        if ((i > 0) && ((this.spans == null) || (this.spans.length < i)))
        {
            this.spans = ((Object[])Array.newInstance(this.classType, i));
            this.spanStarts = new int[i];
            this.spanEnds = new int[i];
            this.spanFlags = new int[i];
        }
        this.numberOfSpans = 0;
        int j = 0;
        if (j < i)
        {
            Object localObject = arrayOfObject[j];
            int k = paramSpanned.getSpanStart(localObject);
            int m = paramSpanned.getSpanEnd(localObject);
            if (k == m);
            while (true)
            {
                j++;
                break;
                int n = paramSpanned.getSpanFlags(localObject);
                this.spans[this.numberOfSpans] = localObject;
                this.spanStarts[this.numberOfSpans] = k;
                this.spanEnds[this.numberOfSpans] = m;
                this.spanFlags[this.numberOfSpans] = n;
                this.numberOfSpans = (1 + this.numberOfSpans);
            }
        }
    }

    public void recycle()
    {
        for (int i = 0; i < this.numberOfSpans; i++)
            this.spans[i] = null;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.SpanSet
 * JD-Core Version:        0.6.2
 */