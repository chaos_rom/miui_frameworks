package android.text;

public class Selection
{
    public static final Object SELECTION_END = new END(null);
    public static final Object SELECTION_START = new START(null);

    private static int chooseHorizontal(Layout paramLayout, int paramInt1, int paramInt2, int paramInt3)
    {
        float f1;
        float f2;
        if (paramLayout.getLineForOffset(paramInt2) == paramLayout.getLineForOffset(paramInt3))
        {
            f1 = paramLayout.getPrimaryHorizontal(paramInt2);
            f2 = paramLayout.getPrimaryHorizontal(paramInt3);
            if (paramInt1 < 0)
                if (f1 >= f2);
        }
        while (true)
        {
            return paramInt2;
            paramInt2 = paramInt3;
            continue;
            if (f1 <= f2)
            {
                paramInt2 = paramInt3;
                continue;
                if (paramLayout.getParagraphDirection(paramLayout.getLineForOffset(paramInt2)) == paramInt1)
                    paramInt2 = Math.max(paramInt2, paramInt3);
                else
                    paramInt2 = Math.min(paramInt2, paramInt3);
            }
        }
    }

    public static boolean extendDown(Spannable paramSpannable, Layout paramLayout)
    {
        int i = getSelectionEnd(paramSpannable);
        int j = paramLayout.getLineForOffset(i);
        int k;
        if (j < -1 + paramLayout.getLineCount())
            if (paramLayout.getParagraphDirection(j) == paramLayout.getParagraphDirection(j + 1))
            {
                float f = paramLayout.getPrimaryHorizontal(i);
                k = paramLayout.getOffsetForHorizontal(j + 1, f);
                extendSelection(paramSpannable, k);
            }
        while (true)
        {
            return true;
            k = paramLayout.getLineStart(j + 1);
            break;
            if (i != paramSpannable.length())
                extendSelection(paramSpannable, paramSpannable.length());
        }
    }

    public static boolean extendLeft(Spannable paramSpannable, Layout paramLayout)
    {
        int i = getSelectionEnd(paramSpannable);
        int j = paramLayout.getOffsetToLeftOf(i);
        if (j != i)
            extendSelection(paramSpannable, j);
        return true;
    }

    public static boolean extendRight(Spannable paramSpannable, Layout paramLayout)
    {
        int i = getSelectionEnd(paramSpannable);
        int j = paramLayout.getOffsetToRightOf(i);
        if (j != i)
            extendSelection(paramSpannable, j);
        return true;
    }

    public static final void extendSelection(Spannable paramSpannable, int paramInt)
    {
        if (paramSpannable.getSpanStart(SELECTION_END) != paramInt)
            paramSpannable.setSpan(SELECTION_END, paramInt, paramInt, 34);
    }

    public static boolean extendToLeftEdge(Spannable paramSpannable, Layout paramLayout)
    {
        extendSelection(paramSpannable, findEdge(paramSpannable, paramLayout, -1));
        return true;
    }

    public static boolean extendToRightEdge(Spannable paramSpannable, Layout paramLayout)
    {
        extendSelection(paramSpannable, findEdge(paramSpannable, paramLayout, 1));
        return true;
    }

    public static boolean extendUp(Spannable paramSpannable, Layout paramLayout)
    {
        int i = getSelectionEnd(paramSpannable);
        int j = paramLayout.getLineForOffset(i);
        int k;
        if (j > 0)
            if (paramLayout.getParagraphDirection(j) == paramLayout.getParagraphDirection(j - 1))
            {
                float f = paramLayout.getPrimaryHorizontal(i);
                k = paramLayout.getOffsetForHorizontal(j - 1, f);
                extendSelection(paramSpannable, k);
            }
        while (true)
        {
            return true;
            k = paramLayout.getLineStart(j - 1);
            break;
            if (i != 0)
                extendSelection(paramSpannable, 0);
        }
    }

    private static int findEdge(Spannable paramSpannable, Layout paramLayout, int paramInt)
    {
        int i = paramLayout.getLineForOffset(getSelectionEnd(paramSpannable));
        int j;
        if (paramInt * paramLayout.getParagraphDirection(i) < 0)
            j = paramLayout.getLineStart(i);
        while (true)
        {
            return j;
            j = paramLayout.getLineEnd(i);
            if (i != -1 + paramLayout.getLineCount())
                j--;
        }
    }

    public static final int getSelectionEnd(CharSequence paramCharSequence)
    {
        if ((paramCharSequence instanceof Spanned));
        for (int i = ((Spanned)paramCharSequence).getSpanStart(SELECTION_END); ; i = -1)
            return i;
    }

    public static final int getSelectionStart(CharSequence paramCharSequence)
    {
        if ((paramCharSequence instanceof Spanned));
        for (int i = ((Spanned)paramCharSequence).getSpanStart(SELECTION_START); ; i = -1)
            return i;
    }

    public static boolean moveDown(Spannable paramSpannable, Layout paramLayout)
    {
        boolean bool = false;
        int i = getSelectionStart(paramSpannable);
        int j = getSelectionEnd(paramSpannable);
        if (i != j)
        {
            int n = Math.min(i, j);
            int i1 = Math.max(i, j);
            setSelection(paramSpannable, i1);
            if ((n != 0) || (i1 != paramSpannable.length()));
        }
        int k;
        do
        {
            while (true)
            {
                return bool;
                bool = true;
            }
            k = paramLayout.getLineForOffset(j);
        }
        while (k >= -1 + paramLayout.getLineCount());
        float f;
        if (paramLayout.getParagraphDirection(k) == paramLayout.getParagraphDirection(k + 1))
            f = paramLayout.getPrimaryHorizontal(j);
        for (int m = paramLayout.getOffsetForHorizontal(k + 1, f); ; m = paramLayout.getLineStart(k + 1))
        {
            setSelection(paramSpannable, m);
            bool = true;
            break;
        }
    }

    public static boolean moveLeft(Spannable paramSpannable, Layout paramLayout)
    {
        boolean bool = true;
        int i = getSelectionStart(paramSpannable);
        int j = getSelectionEnd(paramSpannable);
        if (i != j)
            setSelection(paramSpannable, chooseHorizontal(paramLayout, -1, i, j));
        while (true)
        {
            return bool;
            int k = paramLayout.getOffsetToLeftOf(j);
            if (k != j)
                setSelection(paramSpannable, k);
            else
                bool = false;
        }
    }

    public static boolean moveRight(Spannable paramSpannable, Layout paramLayout)
    {
        int i = 1;
        int k = getSelectionStart(paramSpannable);
        int m = getSelectionEnd(paramSpannable);
        if (k != m)
            setSelection(paramSpannable, chooseHorizontal(paramLayout, i, k, m));
        while (true)
        {
            return i;
            int n = paramLayout.getOffsetToRightOf(m);
            if (n != m)
                setSelection(paramSpannable, n);
            else
                int j = 0;
        }
    }

    public static boolean moveToFollowing(Spannable paramSpannable, PositionIterator paramPositionIterator, boolean paramBoolean)
    {
        int i = paramPositionIterator.following(getSelectionEnd(paramSpannable));
        if (i != -1)
        {
            if (!paramBoolean)
                break label28;
            extendSelection(paramSpannable, i);
        }
        while (true)
        {
            return true;
            label28: setSelection(paramSpannable, i);
        }
    }

    public static boolean moveToLeftEdge(Spannable paramSpannable, Layout paramLayout)
    {
        setSelection(paramSpannable, findEdge(paramSpannable, paramLayout, -1));
        return true;
    }

    public static boolean moveToPreceding(Spannable paramSpannable, PositionIterator paramPositionIterator, boolean paramBoolean)
    {
        int i = paramPositionIterator.preceding(getSelectionEnd(paramSpannable));
        if (i != -1)
        {
            if (!paramBoolean)
                break label28;
            extendSelection(paramSpannable, i);
        }
        while (true)
        {
            return true;
            label28: setSelection(paramSpannable, i);
        }
    }

    public static boolean moveToRightEdge(Spannable paramSpannable, Layout paramLayout)
    {
        setSelection(paramSpannable, findEdge(paramSpannable, paramLayout, 1));
        return true;
    }

    public static boolean moveUp(Spannable paramSpannable, Layout paramLayout)
    {
        boolean bool = false;
        int i = getSelectionStart(paramSpannable);
        int j = getSelectionEnd(paramSpannable);
        if (i != j)
        {
            int n = Math.min(i, j);
            int i1 = Math.max(i, j);
            setSelection(paramSpannable, n);
            if ((n != 0) || (i1 != paramSpannable.length()));
        }
        int k;
        do
        {
            while (true)
            {
                return bool;
                bool = true;
            }
            k = paramLayout.getLineForOffset(j);
        }
        while (k <= 0);
        float f;
        if (paramLayout.getParagraphDirection(k) == paramLayout.getParagraphDirection(k - 1))
            f = paramLayout.getPrimaryHorizontal(j);
        for (int m = paramLayout.getOffsetForHorizontal(k - 1, f); ; m = paramLayout.getLineStart(k - 1))
        {
            setSelection(paramSpannable, m);
            bool = true;
            break;
        }
    }

    public static final void removeSelection(Spannable paramSpannable)
    {
        paramSpannable.removeSpan(SELECTION_START);
        paramSpannable.removeSpan(SELECTION_END);
    }

    public static final void selectAll(Spannable paramSpannable)
    {
        setSelection(paramSpannable, 0, paramSpannable.length());
    }

    public static final void setSelection(Spannable paramSpannable, int paramInt)
    {
        setSelection(paramSpannable, paramInt, paramInt);
    }

    public static void setSelection(Spannable paramSpannable, int paramInt1, int paramInt2)
    {
        int i = getSelectionStart(paramSpannable);
        int j = getSelectionEnd(paramSpannable);
        if ((i != paramInt1) || (j != paramInt2))
        {
            paramSpannable.setSpan(SELECTION_START, paramInt1, paramInt1, 546);
            paramSpannable.setSpan(SELECTION_END, paramInt2, paramInt2, 34);
        }
    }

    private static final class END
        implements NoCopySpan
    {
    }

    private static final class START
        implements NoCopySpan
    {
    }

    public static abstract interface PositionIterator
    {
        public static final int DONE = -1;

        public abstract int following(int paramInt);

        public abstract int preceding(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.Selection
 * JD-Core Version:        0.6.2
 */