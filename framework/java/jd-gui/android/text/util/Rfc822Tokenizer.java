package android.text.util;

import android.widget.MultiAutoCompleteTextView.Tokenizer;
import java.util.ArrayList;
import java.util.Collection;

public class Rfc822Tokenizer
    implements MultiAutoCompleteTextView.Tokenizer
{
    private static void crunch(StringBuilder paramStringBuilder)
    {
        int i = 0;
        int j = paramStringBuilder.length();
        while (i < j)
            if (paramStringBuilder.charAt(i) == 0)
            {
                if ((i == 0) || (i == j - 1) || (paramStringBuilder.charAt(i - 1) == ' ') || (paramStringBuilder.charAt(i - 1) == 0) || (paramStringBuilder.charAt(i + 1) == ' ') || (paramStringBuilder.charAt(i + 1) == 0))
                {
                    paramStringBuilder.deleteCharAt(i);
                    j--;
                }
                else
                {
                    i++;
                }
            }
            else
                i++;
        for (int k = 0; k < j; k++)
            if (paramStringBuilder.charAt(k) == 0)
                paramStringBuilder.setCharAt(k, ' ');
    }

    public static void tokenize(CharSequence paramCharSequence, Collection<Rfc822Token> paramCollection)
    {
        StringBuilder localStringBuilder1 = new StringBuilder();
        StringBuilder localStringBuilder2 = new StringBuilder();
        StringBuilder localStringBuilder3 = new StringBuilder();
        int i = 0;
        int j = paramCharSequence.length();
        while (i < j)
        {
            char c1 = paramCharSequence.charAt(i);
            if ((c1 == ',') || (c1 == ';'))
            {
                i++;
                while ((i < j) && (paramCharSequence.charAt(i) == ' '))
                    i++;
                crunch(localStringBuilder1);
                if (localStringBuilder2.length() > 0)
                    paramCollection.add(new Rfc822Token(localStringBuilder1.toString(), localStringBuilder2.toString(), localStringBuilder3.toString()));
                while (true)
                {
                    localStringBuilder1.setLength(0);
                    localStringBuilder2.setLength(0);
                    localStringBuilder3.setLength(0);
                    break;
                    if (localStringBuilder1.length() > 0)
                        paramCollection.add(new Rfc822Token(null, localStringBuilder1.toString(), localStringBuilder3.toString()));
                }
            }
            if (c1 == '"')
            {
                i++;
                while (true)
                {
                    if (i >= j)
                        break label275;
                    char c4 = paramCharSequence.charAt(i);
                    if (c4 == '"')
                    {
                        i++;
                        break;
                    }
                    if (c4 == '\\')
                    {
                        if (i + 1 < j)
                            localStringBuilder1.append(paramCharSequence.charAt(i + 1));
                        i += 2;
                    }
                    else
                    {
                        localStringBuilder1.append(c4);
                        i++;
                    }
                }
            }
            else
            {
                label275: if (c1 == '(')
                {
                    int k = 1;
                    i++;
                    while ((i < j) && (k > 0))
                    {
                        char c3 = paramCharSequence.charAt(i);
                        if (c3 == ')')
                        {
                            if (k > 1)
                                localStringBuilder3.append(c3);
                            k--;
                            i++;
                        }
                        else if (c3 == '(')
                        {
                            localStringBuilder3.append(c3);
                            k++;
                            i++;
                        }
                        else if (c3 == '\\')
                        {
                            if (i + 1 < j)
                                localStringBuilder3.append(paramCharSequence.charAt(i + 1));
                            i += 2;
                        }
                        else
                        {
                            localStringBuilder3.append(c3);
                            i++;
                        }
                    }
                }
                else if (c1 == '<')
                {
                    i++;
                    while (true)
                    {
                        if (i >= j)
                            break label469;
                        char c2 = paramCharSequence.charAt(i);
                        if (c2 == '>')
                        {
                            i++;
                            break;
                        }
                        localStringBuilder2.append(c2);
                        i++;
                    }
                }
                else
                {
                    label469: if (c1 == ' ')
                    {
                        localStringBuilder1.append('\000');
                        i++;
                    }
                    else
                    {
                        localStringBuilder1.append(c1);
                        i++;
                    }
                }
            }
        }
        crunch(localStringBuilder1);
        if (localStringBuilder2.length() > 0)
            paramCollection.add(new Rfc822Token(localStringBuilder1.toString(), localStringBuilder2.toString(), localStringBuilder3.toString()));
        while (true)
        {
            return;
            if (localStringBuilder1.length() > 0)
                paramCollection.add(new Rfc822Token(null, localStringBuilder1.toString(), localStringBuilder3.toString()));
        }
    }

    public static Rfc822Token[] tokenize(CharSequence paramCharSequence)
    {
        ArrayList localArrayList = new ArrayList();
        tokenize(paramCharSequence, localArrayList);
        return (Rfc822Token[])localArrayList.toArray(new Rfc822Token[localArrayList.size()]);
    }

    public int findTokenEnd(CharSequence paramCharSequence, int paramInt)
    {
        int i = paramCharSequence.length();
        int j = paramInt;
        while (true)
        {
            int k;
            if (j < i)
            {
                k = paramCharSequence.charAt(j);
                if ((k != 44) && (k != 59));
            }
            else
            {
                return j;
            }
            if (k == 34)
            {
                j++;
                while (true)
                {
                    if (j >= i)
                        break label107;
                    int i1 = paramCharSequence.charAt(j);
                    if (i1 == 34)
                    {
                        j++;
                        break;
                    }
                    if ((i1 == 92) && (j + 1 < i))
                        j += 2;
                    else
                        j++;
                }
            }
            else
            {
                label107: if (k == 40)
                {
                    int m = 1;
                    j++;
                    while ((j < i) && (m > 0))
                    {
                        int n = paramCharSequence.charAt(j);
                        if (n == 41)
                        {
                            m--;
                            j++;
                        }
                        else if (n == 40)
                        {
                            m++;
                            j++;
                        }
                        else if ((n == 92) && (j + 1 < i))
                        {
                            j += 2;
                        }
                        else
                        {
                            j++;
                        }
                    }
                }
                else if (k == 60)
                {
                    j++;
                    while (true)
                    {
                        if (j >= i)
                            break label241;
                        if (paramCharSequence.charAt(j) == '>')
                        {
                            j++;
                            break;
                        }
                        j++;
                    }
                }
                else
                {
                    label241: j++;
                }
            }
        }
    }

    public int findTokenStart(CharSequence paramCharSequence, int paramInt)
    {
        int i = 0;
        int j = 0;
        while (j < paramInt)
        {
            j = findTokenEnd(paramCharSequence, j);
            if (j < paramInt)
            {
                j++;
                while ((j < paramInt) && (paramCharSequence.charAt(j) == ' '))
                    j++;
                if (j < paramInt)
                    i = j;
            }
        }
        return i;
    }

    public CharSequence terminateToken(CharSequence paramCharSequence)
    {
        return paramCharSequence + ", ";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.util.Rfc822Tokenizer
 * JD-Core Version:        0.6.2
 */