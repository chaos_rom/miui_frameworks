package android.text.util;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.URLSpan;
import android.util.Patterns;
import android.webkit.WebView;
import android.widget.TextView;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Linkify
{
    public static final int ALL = 15;
    public static final int EMAIL_ADDRESSES = 2;
    public static final int MAP_ADDRESSES = 8;
    public static final int PHONE_NUMBERS = 4;
    private static final int PHONE_NUMBER_MINIMUM_DIGITS = 5;
    public static final int WEB_URLS = 1;
    public static final MatchFilter sPhoneNumberMatchFilter = new MatchFilter()
    {
        public final boolean acceptMatch(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2)
        {
            int i = 0;
            int j = paramAnonymousInt1;
            if (j < paramAnonymousInt2)
                if (Character.isDigit(paramAnonymousCharSequence.charAt(j)))
                {
                    i++;
                    if (i < 5);
                }
            for (boolean bool = true; ; bool = false)
            {
                return bool;
                j++;
                break;
            }
        }
    };
    public static final TransformFilter sPhoneNumberTransformFilter = new TransformFilter()
    {
        public final String transformUrl(Matcher paramAnonymousMatcher, String paramAnonymousString)
        {
            return Patterns.digitsAndPlusOnly(paramAnonymousMatcher);
        }
    };
    public static final MatchFilter sUrlMatchFilter = new MatchFilter()
    {
        public final boolean acceptMatch(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2)
        {
            boolean bool = true;
            if (paramAnonymousInt1 == 0);
            while (true)
            {
                return bool;
                if (paramAnonymousCharSequence.charAt(paramAnonymousInt1 - 1) == '@')
                    bool = false;
            }
        }
    };

    private static final void addLinkMovementMethod(TextView paramTextView)
    {
        MovementMethod localMovementMethod = paramTextView.getMovementMethod();
        if (((localMovementMethod == null) || (!(localMovementMethod instanceof LinkMovementMethod))) && (paramTextView.getLinksClickable()))
            paramTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static final void addLinks(TextView paramTextView, Pattern paramPattern, String paramString)
    {
        addLinks(paramTextView, paramPattern, paramString, null, null);
    }

    public static final void addLinks(TextView paramTextView, Pattern paramPattern, String paramString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter)
    {
        SpannableString localSpannableString = SpannableString.valueOf(paramTextView.getText());
        if (addLinks(localSpannableString, paramPattern, paramString, paramMatchFilter, paramTransformFilter))
        {
            paramTextView.setText(localSpannableString);
            addLinkMovementMethod(paramTextView);
        }
    }

    public static final boolean addLinks(Spannable paramSpannable, int paramInt)
    {
        boolean bool;
        if (paramInt == 0)
            bool = false;
        while (true)
        {
            return bool;
            URLSpan[] arrayOfURLSpan = (URLSpan[])paramSpannable.getSpans(0, paramSpannable.length(), URLSpan.class);
            for (int i = -1 + arrayOfURLSpan.length; i >= 0; i--)
                paramSpannable.removeSpan(arrayOfURLSpan[i]);
            ArrayList localArrayList = new ArrayList();
            if ((paramInt & 0x1) != 0)
            {
                Pattern localPattern3 = Patterns.WEB_URL;
                String[] arrayOfString3 = new String[3];
                arrayOfString3[0] = "http://";
                arrayOfString3[1] = "https://";
                arrayOfString3[2] = "rtsp://";
                gatherLinks(localArrayList, paramSpannable, localPattern3, arrayOfString3, sUrlMatchFilter, null);
            }
            if ((paramInt & 0x2) != 0)
            {
                Pattern localPattern2 = Patterns.EMAIL_ADDRESS;
                String[] arrayOfString2 = new String[1];
                arrayOfString2[0] = "mailto:";
                gatherLinks(localArrayList, paramSpannable, localPattern2, arrayOfString2, null, null);
            }
            if ((paramInt & 0x4) != 0)
            {
                Pattern localPattern1 = Patterns.PHONE;
                String[] arrayOfString1 = new String[1];
                arrayOfString1[0] = "tel:";
                gatherLinks(localArrayList, paramSpannable, localPattern1, arrayOfString1, sPhoneNumberMatchFilter, sPhoneNumberTransformFilter);
            }
            if ((paramInt & 0x8) != 0)
                gatherMapLinks(localArrayList, paramSpannable);
            pruneOverlaps(localArrayList);
            if (localArrayList.size() == 0)
            {
                bool = false;
            }
            else
            {
                Iterator localIterator = localArrayList.iterator();
                while (localIterator.hasNext())
                {
                    LinkSpec localLinkSpec = (LinkSpec)localIterator.next();
                    applyLink(localLinkSpec.url, localLinkSpec.start, localLinkSpec.end, paramSpannable);
                }
                bool = true;
            }
        }
    }

    public static final boolean addLinks(Spannable paramSpannable, Pattern paramPattern, String paramString)
    {
        return addLinks(paramSpannable, paramPattern, paramString, null, null);
    }

    public static final boolean addLinks(Spannable paramSpannable, Pattern paramPattern, String paramString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter)
    {
        boolean bool1 = false;
        if (paramString == null);
        for (String str1 = ""; ; str1 = paramString.toLowerCase())
        {
            Matcher localMatcher = paramPattern.matcher(paramSpannable);
            while (localMatcher.find())
            {
                int i = localMatcher.start();
                int j = localMatcher.end();
                boolean bool2 = true;
                if (paramMatchFilter != null)
                    bool2 = paramMatchFilter.acceptMatch(paramSpannable, i, j);
                if (bool2)
                {
                    String str2 = localMatcher.group(0);
                    String[] arrayOfString = new String[1];
                    arrayOfString[0] = str1;
                    applyLink(makeUrl(str2, arrayOfString, localMatcher, paramTransformFilter), i, j, paramSpannable);
                    bool1 = true;
                }
            }
        }
        return bool1;
    }

    public static final boolean addLinks(TextView paramTextView, int paramInt)
    {
        boolean bool = false;
        if (paramInt == 0);
        while (true)
        {
            return bool;
            CharSequence localCharSequence = paramTextView.getText();
            if ((localCharSequence instanceof Spannable))
            {
                if (addLinks((Spannable)localCharSequence, paramInt))
                {
                    addLinkMovementMethod(paramTextView);
                    bool = true;
                }
            }
            else
            {
                SpannableString localSpannableString = SpannableString.valueOf(localCharSequence);
                if (addLinks(localSpannableString, paramInt))
                {
                    addLinkMovementMethod(paramTextView);
                    paramTextView.setText(localSpannableString);
                    bool = true;
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    public static final void applyLink(String paramString, int paramInt1, int paramInt2, Spannable paramSpannable)
    {
        paramSpannable.setSpan(new URLSpan(paramString), paramInt1, paramInt2, 33);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    public static final void gatherLinks(ArrayList<LinkSpec> paramArrayList, Spannable paramSpannable, Pattern paramPattern, String[] paramArrayOfString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter)
    {
        Matcher localMatcher = paramPattern.matcher(paramSpannable);
        while (localMatcher.find())
        {
            int i = localMatcher.start();
            int j = localMatcher.end();
            if ((paramMatchFilter == null) || (paramMatchFilter.acceptMatch(paramSpannable, i, j)))
            {
                LinkSpec localLinkSpec = new LinkSpec();
                localLinkSpec.url = makeUrl(localMatcher.group(0), paramArrayOfString, localMatcher, paramTransformFilter);
                localLinkSpec.start = i;
                localLinkSpec.end = j;
                paramArrayList.add(localLinkSpec);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    public static final void gatherMapLinks(ArrayList<LinkSpec> paramArrayList, Spannable paramSpannable)
    {
        String str1 = paramSpannable.toString();
        int i = 0;
        while (true)
        {
            String str2 = WebView.findAddress(str1);
            int j;
            if (str2 != null)
            {
                j = str1.indexOf(str2);
                if (j >= 0);
            }
            else
            {
                return;
            }
            LinkSpec localLinkSpec = new LinkSpec();
            int k = j + str2.length();
            localLinkSpec.start = (i + j);
            localLinkSpec.end = (i + k);
            str1 = str1.substring(k);
            i += k;
            try
            {
                String str3 = URLEncoder.encode(str2, "UTF-8");
                localLinkSpec.url = ("geo:0,0?q=" + str3);
                paramArrayList.add(localLinkSpec);
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    public static final String makeUrl(String paramString, String[] paramArrayOfString, Matcher paramMatcher, TransformFilter paramTransformFilter)
    {
        if (paramTransformFilter != null)
            paramString = paramTransformFilter.transformUrl(paramMatcher, paramString);
        int i = 0;
        for (int j = 0; ; j++)
        {
            if (j < paramArrayOfString.length)
            {
                String str1 = paramArrayOfString[j];
                int k = paramArrayOfString[j].length();
                if (!paramString.regionMatches(true, 0, str1, 0, k))
                    continue;
                i = 1;
                String str2 = paramArrayOfString[j];
                int m = paramArrayOfString[j].length();
                if (!paramString.regionMatches(false, 0, str2, 0, m))
                    paramString = paramArrayOfString[j] + paramString.substring(paramArrayOfString[j].length());
            }
            if (i == 0)
                paramString = paramArrayOfString[0] + paramString;
            return paramString;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    public static final void pruneOverlaps(ArrayList<LinkSpec> paramArrayList)
    {
        Collections.sort(paramArrayList, new Comparator()
        {
            public final int compare(Linkify.LinkSpec paramAnonymousLinkSpec1, Linkify.LinkSpec paramAnonymousLinkSpec2)
            {
                int i = -1;
                if (paramAnonymousLinkSpec1.start < paramAnonymousLinkSpec2.start);
                while (true)
                {
                    return i;
                    if (paramAnonymousLinkSpec1.start > paramAnonymousLinkSpec2.start)
                        i = 1;
                    else if (paramAnonymousLinkSpec1.end < paramAnonymousLinkSpec2.end)
                        i = 1;
                    else if (paramAnonymousLinkSpec1.end <= paramAnonymousLinkSpec2.end)
                        i = 0;
                }
            }

            public final boolean equals(Object paramAnonymousObject)
            {
                return false;
            }
        });
        int i = paramArrayList.size();
        int j = 0;
        while (j < i - 1)
        {
            LinkSpec localLinkSpec1 = (LinkSpec)paramArrayList.get(j);
            LinkSpec localLinkSpec2 = (LinkSpec)paramArrayList.get(j + 1);
            int k = -1;
            if ((localLinkSpec1.start <= localLinkSpec2.start) && (localLinkSpec1.end > localLinkSpec2.start))
            {
                if (localLinkSpec2.end <= localLinkSpec1.end)
                    k = j + 1;
                while (true)
                {
                    if (k == -1)
                        break label171;
                    paramArrayList.remove(k);
                    i--;
                    break;
                    if (localLinkSpec1.end - localLinkSpec1.start > localLinkSpec2.end - localLinkSpec2.start)
                        k = j + 1;
                    else if (localLinkSpec1.end - localLinkSpec1.start < localLinkSpec2.end - localLinkSpec2.start)
                        k = j;
                }
            }
            label171: j++;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE_AND_ACCESS)
    public static class LinkSpec
    {
        public int end;
        public int start;
        public String url;
    }

    public static abstract interface TransformFilter
    {
        public abstract String transformUrl(Matcher paramMatcher, String paramString);
    }

    public static abstract interface MatchFilter
    {
        public abstract boolean acceptMatch(CharSequence paramCharSequence, int paramInt1, int paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.util.Linkify
 * JD-Core Version:        0.6.2
 */