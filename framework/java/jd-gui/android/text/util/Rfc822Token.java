package android.text.util;

public class Rfc822Token
{
    private String mAddress;
    private String mComment;
    private String mName;

    public Rfc822Token(String paramString1, String paramString2, String paramString3)
    {
        this.mName = paramString1;
        this.mAddress = paramString2;
        this.mComment = paramString3;
    }

    public static String quoteComment(String paramString)
    {
        int i = paramString.length();
        StringBuilder localStringBuilder = new StringBuilder();
        for (int j = 0; j < i; j++)
        {
            char c = paramString.charAt(j);
            if ((c == '(') || (c == ')') || (c == '\\'))
                localStringBuilder.append('\\');
            localStringBuilder.append(c);
        }
        return localStringBuilder.toString();
    }

    public static String quoteName(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = paramString.length();
        for (int j = 0; j < i; j++)
        {
            char c = paramString.charAt(j);
            if ((c == '\\') || (c == '"'))
                localStringBuilder.append('\\');
            localStringBuilder.append(c);
        }
        return localStringBuilder.toString();
    }

    public static String quoteNameIfNecessary(String paramString)
    {
        int i = paramString.length();
        for (int j = 0; ; j++)
            if (j < i)
            {
                int k = paramString.charAt(j);
                if (((k < 65) || (k > 90)) && ((k < 97) || (k > 122)) && (k != 32) && ((k < 48) || (k > 57)))
                    paramString = '"' + quoteName(paramString) + '"';
            }
            else
            {
                return paramString;
            }
    }

    private static boolean stringEquals(String paramString1, String paramString2)
    {
        boolean bool;
        if (paramString1 == null)
            if (paramString2 == null)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = paramString1.equals(paramString2);
        }
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (!(paramObject instanceof Rfc822Token));
        while (true)
        {
            return bool;
            Rfc822Token localRfc822Token = (Rfc822Token)paramObject;
            if ((stringEquals(this.mName, localRfc822Token.mName)) && (stringEquals(this.mAddress, localRfc822Token.mAddress)) && (stringEquals(this.mComment, localRfc822Token.mComment)))
                bool = true;
        }
    }

    public String getAddress()
    {
        return this.mAddress;
    }

    public String getComment()
    {
        return this.mComment;
    }

    public String getName()
    {
        return this.mName;
    }

    public int hashCode()
    {
        int i = 17;
        if (this.mName != null)
            i = 527 + this.mName.hashCode();
        if (this.mAddress != null)
            i = i * 31 + this.mAddress.hashCode();
        if (this.mComment != null)
            i = i * 31 + this.mComment.hashCode();
        return i;
    }

    public void setAddress(String paramString)
    {
        this.mAddress = paramString;
    }

    public void setComment(String paramString)
    {
        this.mComment = paramString;
    }

    public void setName(String paramString)
    {
        this.mName = paramString;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if ((this.mName != null) && (this.mName.length() != 0))
        {
            localStringBuilder.append(quoteNameIfNecessary(this.mName));
            localStringBuilder.append(' ');
        }
        if ((this.mComment != null) && (this.mComment.length() != 0))
        {
            localStringBuilder.append('(');
            localStringBuilder.append(quoteComment(this.mComment));
            localStringBuilder.append(") ");
        }
        if ((this.mAddress != null) && (this.mAddress.length() != 0))
        {
            localStringBuilder.append('<');
            localStringBuilder.append(this.mAddress);
            localStringBuilder.append('>');
        }
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.text.util.Rfc822Token
 * JD-Core Version:        0.6.2
 */