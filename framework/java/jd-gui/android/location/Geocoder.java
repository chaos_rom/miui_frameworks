package android.location;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class Geocoder
{
    private static final String TAG = "Geocoder";
    private GeocoderParams mParams;
    private ILocationManager mService;

    public Geocoder(Context paramContext)
    {
        this(paramContext, Locale.getDefault());
    }

    public Geocoder(Context paramContext, Locale paramLocale)
    {
        if (paramLocale == null)
            throw new NullPointerException("locale == null");
        this.mParams = new GeocoderParams(paramContext, paramLocale);
        this.mService = ILocationManager.Stub.asInterface(ServiceManager.getService("location"));
    }

    public static boolean isPresent()
    {
        ILocationManager localILocationManager = ILocationManager.Stub.asInterface(ServiceManager.getService("location"));
        try
        {
            boolean bool2 = localILocationManager.geocoderIsPresent();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("Geocoder", "isPresent: got RemoteException", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public List<Address> getFromLocation(double paramDouble1, double paramDouble2, int paramInt)
        throws IOException
    {
        if ((paramDouble1 < -90.0D) || (paramDouble1 > 90.0D))
            throw new IllegalArgumentException("latitude == " + paramDouble1);
        if ((paramDouble2 < -180.0D) || (paramDouble2 > 180.0D))
            throw new IllegalArgumentException("longitude == " + paramDouble2);
        ArrayList localArrayList;
        try
        {
            localArrayList = new ArrayList();
            String str = this.mService.getFromLocation(paramDouble1, paramDouble2, paramInt, this.mParams, localArrayList);
            if (str != null)
                throw new IOException(str);
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("Geocoder", "getFromLocation: got RemoteException", localRemoteException);
            localArrayList = null;
        }
        return localArrayList;
    }

    public List<Address> getFromLocationName(String paramString, int paramInt)
        throws IOException
    {
        if (paramString == null)
            throw new IllegalArgumentException("locationName == null");
        ArrayList localArrayList;
        try
        {
            localArrayList = new ArrayList();
            String str = this.mService.getFromLocationName(paramString, 0.0D, 0.0D, 0.0D, 0.0D, paramInt, this.mParams, localArrayList);
            if (str != null)
                throw new IOException(str);
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("Geocoder", "getFromLocationName: got RemoteException", localRemoteException);
            localArrayList = null;
        }
        return localArrayList;
    }

    public List<Address> getFromLocationName(String paramString, int paramInt, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
        throws IOException
    {
        if (paramString == null)
            throw new IllegalArgumentException("locationName == null");
        if ((paramDouble1 < -90.0D) || (paramDouble1 > 90.0D))
            throw new IllegalArgumentException("lowerLeftLatitude == " + paramDouble1);
        if ((paramDouble2 < -180.0D) || (paramDouble2 > 180.0D))
            throw new IllegalArgumentException("lowerLeftLongitude == " + paramDouble2);
        if ((paramDouble3 < -90.0D) || (paramDouble3 > 90.0D))
            throw new IllegalArgumentException("upperRightLatitude == " + paramDouble3);
        if ((paramDouble4 < -180.0D) || (paramDouble4 > 180.0D))
            throw new IllegalArgumentException("upperRightLongitude == " + paramDouble4);
        ArrayList localArrayList;
        try
        {
            localArrayList = new ArrayList();
            String str = this.mService.getFromLocationName(paramString, paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramInt, this.mParams, localArrayList);
            if (str != null)
                throw new IOException(str);
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("Geocoder", "getFromLocationName: got RemoteException", localRemoteException);
            localArrayList = null;
        }
        return localArrayList;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.Geocoder
 * JD-Core Version:        0.6.2
 */