package android.location;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.location.DummyLocationProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LocationManager
{
    public static final String EXTRA_GPS_ENABLED = "enabled";
    public static final String GPS_ENABLED_CHANGE_ACTION = "android.location.GPS_ENABLED_CHANGE";
    public static final String GPS_FIX_CHANGE_ACTION = "android.location.GPS_FIX_CHANGE";
    public static final String GPS_PROVIDER = "gps";
    public static final String KEY_LOCATION_CHANGED = "location";
    public static final String KEY_PROVIDER_ENABLED = "providerEnabled";
    public static final String KEY_PROXIMITY_ENTERING = "entering";
    public static final String KEY_STATUS_CHANGED = "status";
    public static final String NETWORK_PROVIDER = "network";
    public static final String PASSIVE_PROVIDER = "passive";
    public static final String PROVIDERS_CHANGED_ACTION = "android.location.PROVIDERS_CHANGED";
    private static final String TAG = "LocationManager";
    private final Context mContext;
    private final GpsStatus mGpsStatus = new GpsStatus();
    private final HashMap<GpsStatus.Listener, GpsStatusListenerTransport> mGpsStatusListeners = new HashMap();
    private HashMap<LocationListener, ListenerTransport> mListeners = new HashMap();
    private final HashMap<GpsStatus.NmeaListener, GpsStatusListenerTransport> mNmeaListeners = new HashMap();
    private ILocationManager mService;

    public LocationManager(Context paramContext, ILocationManager paramILocationManager)
    {
        this.mService = paramILocationManager;
        this.mContext = paramContext;
    }

    private void _requestLocationUpdates(String paramString, Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean, PendingIntent paramPendingIntent)
    {
        if (paramLong < 0L)
            paramLong = 0L;
        if (paramFloat < 0.0F)
            paramFloat = 0.0F;
        try
        {
            ILocationManager localILocationManager = this.mService;
            String str = this.mContext.getPackageName();
            localILocationManager.requestLocationUpdatesPI(paramString, paramCriteria, paramLong, paramFloat, paramBoolean, paramPendingIntent, str);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "requestLocationUpdates: RemoteException", localRemoteException);
        }
    }

    private void _requestLocationUpdates(String paramString, Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean, LocationListener paramLocationListener, Looper paramLooper)
    {
        if (paramLong < 0L)
            paramLong = 0L;
        if (paramFloat < 0.0F)
            paramFloat = 0.0F;
        try
        {
            synchronized (this.mListeners)
            {
                ListenerTransport localListenerTransport = (ListenerTransport)this.mListeners.get(paramLocationListener);
                if (localListenerTransport == null)
                    localListenerTransport = new ListenerTransport(paramLocationListener, paramLooper);
                this.mListeners.put(paramLocationListener, localListenerTransport);
                ILocationManager localILocationManager = this.mService;
                String str = this.mContext.getPackageName();
                localILocationManager.requestLocationUpdates(paramString, paramCriteria, paramLong, paramFloat, paramBoolean, localListenerTransport, str);
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("LocationManager", "requestLocationUpdates: DeadObjectException", localRemoteException);
        }
    }

    private LocationProvider createProvider(String paramString, Bundle paramBundle)
    {
        DummyLocationProvider localDummyLocationProvider = new DummyLocationProvider(paramString, this.mService);
        localDummyLocationProvider.setRequiresNetwork(paramBundle.getBoolean("network"));
        localDummyLocationProvider.setRequiresSatellite(paramBundle.getBoolean("satellite"));
        localDummyLocationProvider.setRequiresCell(paramBundle.getBoolean("cell"));
        localDummyLocationProvider.setHasMonetaryCost(paramBundle.getBoolean("cost"));
        localDummyLocationProvider.setSupportsAltitude(paramBundle.getBoolean("altitude"));
        localDummyLocationProvider.setSupportsSpeed(paramBundle.getBoolean("speed"));
        localDummyLocationProvider.setSupportsBearing(paramBundle.getBoolean("bearing"));
        localDummyLocationProvider.setPowerRequirement(paramBundle.getInt("power"));
        localDummyLocationProvider.setAccuracy(paramBundle.getInt("accuracy"));
        return localDummyLocationProvider;
    }

    public boolean addGpsStatusListener(GpsStatus.Listener paramListener)
    {
        boolean bool;
        if (this.mGpsStatusListeners.get(paramListener) != null)
            bool = true;
        while (true)
        {
            return bool;
            try
            {
                GpsStatusListenerTransport localGpsStatusListenerTransport = new GpsStatusListenerTransport(paramListener);
                bool = this.mService.addGpsStatusListener(localGpsStatusListenerTransport);
                if (bool)
                    this.mGpsStatusListeners.put(paramListener, localGpsStatusListenerTransport);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("LocationManager", "RemoteException in registerGpsStatusListener: ", localRemoteException);
                bool = false;
            }
        }
    }

    public boolean addNmeaListener(GpsStatus.NmeaListener paramNmeaListener)
    {
        boolean bool;
        if (this.mNmeaListeners.get(paramNmeaListener) != null)
            bool = true;
        while (true)
        {
            return bool;
            try
            {
                GpsStatusListenerTransport localGpsStatusListenerTransport = new GpsStatusListenerTransport(paramNmeaListener);
                bool = this.mService.addGpsStatusListener(localGpsStatusListenerTransport);
                if (bool)
                    this.mNmeaListeners.put(paramNmeaListener, localGpsStatusListenerTransport);
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("LocationManager", "RemoteException in registerGpsStatusListener: ", localRemoteException);
                bool = false;
            }
        }
    }

    public void addProximityAlert(double paramDouble1, double paramDouble2, float paramFloat, long paramLong, PendingIntent paramPendingIntent)
    {
        try
        {
            this.mService.addProximityAlert(paramDouble1, paramDouble2, paramFloat, paramLong, paramPendingIntent, this.mContext.getPackageName());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "addProximityAlert: RemoteException", localRemoteException);
        }
    }

    public void addTestProvider(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, int paramInt1, int paramInt2)
    {
        try
        {
            this.mService.addTestProvider(paramString, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramBoolean7, paramInt1, paramInt2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "addTestProvider: RemoteException", localRemoteException);
        }
    }

    public void clearTestProviderEnabled(String paramString)
    {
        try
        {
            this.mService.clearTestProviderEnabled(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "clearTestProviderEnabled: RemoteException", localRemoteException);
        }
    }

    public void clearTestProviderLocation(String paramString)
    {
        try
        {
            this.mService.clearTestProviderLocation(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "clearTestProviderLocation: RemoteException", localRemoteException);
        }
    }

    public void clearTestProviderStatus(String paramString)
    {
        try
        {
            this.mService.clearTestProviderStatus(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "clearTestProviderStatus: RemoteException", localRemoteException);
        }
    }

    public List<String> getAllProviders()
    {
        try
        {
            List localList2 = this.mService.getAllProviders();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("LocationManager", "getAllProviders: RemoteException", localRemoteException);
                List localList1 = null;
            }
        }
    }

    public String getBestProvider(Criteria paramCriteria, boolean paramBoolean)
    {
        if (paramCriteria == null)
            throw new IllegalArgumentException("criteria==null");
        try
        {
            String str2 = this.mService.getBestProvider(paramCriteria, paramBoolean);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("LocationManager", "getBestProvider: RemoteException", localRemoteException);
                String str1 = null;
            }
        }
    }

    public GpsStatus getGpsStatus(GpsStatus paramGpsStatus)
    {
        if (paramGpsStatus == null)
            paramGpsStatus = new GpsStatus();
        paramGpsStatus.setStatus(this.mGpsStatus);
        return paramGpsStatus;
    }

    public Location getLastKnownLocation(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("provider==null");
        try
        {
            Location localLocation2 = this.mService.getLastKnownLocation(paramString, this.mContext.getPackageName());
            localLocation1 = localLocation2;
            return localLocation1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("LocationManager", "getLastKnowLocation: RemoteException", localRemoteException);
                Location localLocation1 = null;
            }
        }
    }

    public LocationProvider getProvider(String paramString)
    {
        Object localObject = null;
        if (paramString == null)
            throw new IllegalArgumentException("name==null");
        try
        {
            Bundle localBundle = this.mService.getProviderInfo(paramString);
            if (localBundle != null)
            {
                LocationProvider localLocationProvider = createProvider(paramString, localBundle);
                localObject = localLocationProvider;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("LocationManager", "getProvider: RemoteException", localRemoteException);
        }
        return localObject;
    }

    public List<String> getProviders(Criteria paramCriteria, boolean paramBoolean)
    {
        if (paramCriteria == null)
            throw new IllegalArgumentException("criteria==null");
        try
        {
            List localList2 = this.mService.getProviders(paramCriteria, paramBoolean);
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("LocationManager", "getProviders: RemoteException", localRemoteException);
                List localList1 = null;
            }
        }
    }

    public List<String> getProviders(boolean paramBoolean)
    {
        Object localObject = null;
        try
        {
            List localList = this.mService.getProviders(null, paramBoolean);
            localObject = localList;
            return localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "getProviders: RemoteException", localRemoteException);
        }
    }

    public boolean isProviderEnabled(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("provider==null");
        try
        {
            boolean bool2 = this.mService.isProviderEnabled(paramString);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("LocationManager", "isProviderEnabled: RemoteException", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void removeGpsStatusListener(GpsStatus.Listener paramListener)
    {
        try
        {
            GpsStatusListenerTransport localGpsStatusListenerTransport = (GpsStatusListenerTransport)this.mGpsStatusListeners.remove(paramListener);
            if (localGpsStatusListenerTransport != null)
                this.mService.removeGpsStatusListener(localGpsStatusListenerTransport);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "RemoteException in unregisterGpsStatusListener: ", localRemoteException);
        }
    }

    public void removeNmeaListener(GpsStatus.NmeaListener paramNmeaListener)
    {
        try
        {
            GpsStatusListenerTransport localGpsStatusListenerTransport = (GpsStatusListenerTransport)this.mNmeaListeners.remove(paramNmeaListener);
            if (localGpsStatusListenerTransport != null)
                this.mService.removeGpsStatusListener(localGpsStatusListenerTransport);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "RemoteException in unregisterGpsStatusListener: ", localRemoteException);
        }
    }

    public void removeProximityAlert(PendingIntent paramPendingIntent)
    {
        try
        {
            this.mService.removeProximityAlert(paramPendingIntent);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "removeProximityAlert: RemoteException", localRemoteException);
        }
    }

    public void removeTestProvider(String paramString)
    {
        try
        {
            this.mService.removeTestProvider(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "removeTestProvider: RemoteException", localRemoteException);
        }
    }

    public void removeUpdates(PendingIntent paramPendingIntent)
    {
        if (paramPendingIntent == null)
            throw new IllegalArgumentException("intent==null");
        try
        {
            this.mService.removeUpdatesPI(paramPendingIntent, this.mContext.getPackageName());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "removeUpdates: RemoteException", localRemoteException);
        }
    }

    public void removeUpdates(LocationListener paramLocationListener)
    {
        if (paramLocationListener == null)
            throw new IllegalArgumentException("listener==null");
        try
        {
            ListenerTransport localListenerTransport = (ListenerTransport)this.mListeners.remove(paramLocationListener);
            if (localListenerTransport != null)
                this.mService.removeUpdates(localListenerTransport, this.mContext.getPackageName());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "removeUpdates: DeadObjectException", localRemoteException);
        }
    }

    public void requestLocationUpdates(long paramLong, float paramFloat, Criteria paramCriteria, PendingIntent paramPendingIntent)
    {
        if (paramCriteria == null)
            throw new IllegalArgumentException("criteria==null");
        if (paramPendingIntent == null)
            throw new IllegalArgumentException("intent==null");
        _requestLocationUpdates(null, paramCriteria, paramLong, paramFloat, false, paramPendingIntent);
    }

    public void requestLocationUpdates(long paramLong, float paramFloat, Criteria paramCriteria, LocationListener paramLocationListener, Looper paramLooper)
    {
        if (paramCriteria == null)
            throw new IllegalArgumentException("criteria==null");
        if (paramLocationListener == null)
            throw new IllegalArgumentException("listener==null");
        _requestLocationUpdates(null, paramCriteria, paramLong, paramFloat, false, paramLocationListener, paramLooper);
    }

    public void requestLocationUpdates(String paramString, long paramLong, float paramFloat, PendingIntent paramPendingIntent)
    {
        if (paramString == null)
            throw new IllegalArgumentException("provider==null");
        if (paramPendingIntent == null)
            throw new IllegalArgumentException("intent==null");
        _requestLocationUpdates(paramString, null, paramLong, paramFloat, false, paramPendingIntent);
    }

    public void requestLocationUpdates(String paramString, long paramLong, float paramFloat, LocationListener paramLocationListener)
    {
        if (paramString == null)
            throw new IllegalArgumentException("provider==null");
        if (paramLocationListener == null)
            throw new IllegalArgumentException("listener==null");
        _requestLocationUpdates(paramString, null, paramLong, paramFloat, false, paramLocationListener, null);
    }

    public void requestLocationUpdates(String paramString, long paramLong, float paramFloat, LocationListener paramLocationListener, Looper paramLooper)
    {
        if (paramString == null)
            throw new IllegalArgumentException("provider==null");
        if (paramLocationListener == null)
            throw new IllegalArgumentException("listener==null");
        _requestLocationUpdates(paramString, null, paramLong, paramFloat, false, paramLocationListener, paramLooper);
    }

    public void requestSingleUpdate(Criteria paramCriteria, PendingIntent paramPendingIntent)
    {
        if (paramCriteria == null)
            throw new IllegalArgumentException("criteria==null");
        if (paramPendingIntent == null)
            throw new IllegalArgumentException("intent==null");
        _requestLocationUpdates(null, paramCriteria, 0L, 0.0F, true, paramPendingIntent);
    }

    public void requestSingleUpdate(Criteria paramCriteria, LocationListener paramLocationListener, Looper paramLooper)
    {
        if (paramCriteria == null)
            throw new IllegalArgumentException("criteria==null");
        if (paramLocationListener == null)
            throw new IllegalArgumentException("listener==null");
        _requestLocationUpdates(null, paramCriteria, 0L, 0.0F, true, paramLocationListener, paramLooper);
    }

    public void requestSingleUpdate(String paramString, PendingIntent paramPendingIntent)
    {
        if (paramString == null)
            throw new IllegalArgumentException("provider==null");
        if (paramPendingIntent == null)
            throw new IllegalArgumentException("intent==null");
        _requestLocationUpdates(paramString, null, 0L, 0.0F, true, paramPendingIntent);
    }

    public void requestSingleUpdate(String paramString, LocationListener paramLocationListener, Looper paramLooper)
    {
        if (paramString == null)
            throw new IllegalArgumentException("provider==null");
        if (paramLocationListener == null)
            throw new IllegalArgumentException("listener==null");
        _requestLocationUpdates(paramString, null, 0L, 0.0F, true, paramLocationListener, paramLooper);
    }

    public boolean sendExtraCommand(String paramString1, String paramString2, Bundle paramBundle)
    {
        try
        {
            boolean bool2 = this.mService.sendExtraCommand(paramString1, paramString2, paramBundle);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("LocationManager", "RemoteException in sendExtraCommand: ", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean sendNiResponse(int paramInt1, int paramInt2)
    {
        try
        {
            boolean bool2 = this.mService.sendNiResponse(paramInt1, paramInt2);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("LocationManager", "RemoteException in sendNiResponse: ", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void setTestProviderEnabled(String paramString, boolean paramBoolean)
    {
        try
        {
            this.mService.setTestProviderEnabled(paramString, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "setTestProviderEnabled: RemoteException", localRemoteException);
        }
    }

    public void setTestProviderLocation(String paramString, Location paramLocation)
    {
        try
        {
            this.mService.setTestProviderLocation(paramString, paramLocation);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "setTestProviderLocation: RemoteException", localRemoteException);
        }
    }

    public void setTestProviderStatus(String paramString, int paramInt, Bundle paramBundle, long paramLong)
    {
        try
        {
            this.mService.setTestProviderStatus(paramString, paramInt, paramBundle, paramLong);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LocationManager", "setTestProviderStatus: RemoteException", localRemoteException);
        }
    }

    private class GpsStatusListenerTransport extends IGpsStatusListener.Stub
    {
        private static final int NMEA_RECEIVED = 1000;
        private final Handler mGpsHandler = new Handler()
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                if (paramAnonymousMessage.what == 1000)
                    synchronized (LocationManager.GpsStatusListenerTransport.this.mNmeaBuffer)
                    {
                        int i = LocationManager.GpsStatusListenerTransport.this.mNmeaBuffer.size();
                        for (int j = 0; j < i; j++)
                        {
                            LocationManager.GpsStatusListenerTransport.Nmea localNmea = (LocationManager.GpsStatusListenerTransport.Nmea)LocationManager.GpsStatusListenerTransport.this.mNmeaBuffer.get(j);
                            LocationManager.GpsStatusListenerTransport.this.mNmeaListener.onNmeaReceived(localNmea.mTimestamp, localNmea.mNmea);
                        }
                        LocationManager.GpsStatusListenerTransport.this.mNmeaBuffer.clear();
                    }
                synchronized (LocationManager.this.mGpsStatus)
                {
                    LocationManager.GpsStatusListenerTransport.this.mListener.onGpsStatusChanged(paramAnonymousMessage.what);
                }
            }
        };
        private final GpsStatus.Listener mListener;
        private ArrayList<Nmea> mNmeaBuffer;
        private final GpsStatus.NmeaListener mNmeaListener;

        GpsStatusListenerTransport(GpsStatus.Listener arg2)
        {
            Object localObject;
            this.mListener = localObject;
            this.mNmeaListener = null;
        }

        GpsStatusListenerTransport(GpsStatus.NmeaListener arg2)
        {
            Object localObject;
            this.mNmeaListener = localObject;
            this.mListener = null;
            this.mNmeaBuffer = new ArrayList();
        }

        public void onFirstFix(int paramInt)
        {
            if (this.mListener != null)
            {
                LocationManager.this.mGpsStatus.setTimeToFirstFix(paramInt);
                Message localMessage = Message.obtain();
                localMessage.what = 3;
                this.mGpsHandler.sendMessage(localMessage);
            }
        }

        public void onGpsStarted()
        {
            if (this.mListener != null)
            {
                Message localMessage = Message.obtain();
                localMessage.what = 1;
                this.mGpsHandler.sendMessage(localMessage);
            }
        }

        public void onGpsStopped()
        {
            if (this.mListener != null)
            {
                Message localMessage = Message.obtain();
                localMessage.what = 2;
                this.mGpsHandler.sendMessage(localMessage);
            }
        }

        public void onNmeaReceived(long paramLong, String paramString)
        {
            if (this.mNmeaListener != null);
            synchronized (this.mNmeaBuffer)
            {
                this.mNmeaBuffer.add(new Nmea(paramLong, paramString));
                Message localMessage = Message.obtain();
                localMessage.what = 1000;
                this.mGpsHandler.removeMessages(1000);
                this.mGpsHandler.sendMessage(localMessage);
                return;
            }
        }

        public void onSvStatusChanged(int paramInt1, int[] paramArrayOfInt, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, float[] paramArrayOfFloat3, int paramInt2, int paramInt3, int paramInt4)
        {
            if (this.mListener != null)
            {
                LocationManager.this.mGpsStatus.setStatus(paramInt1, paramArrayOfInt, paramArrayOfFloat1, paramArrayOfFloat2, paramArrayOfFloat3, paramInt2, paramInt3, paramInt4);
                Message localMessage = Message.obtain();
                localMessage.what = 4;
                this.mGpsHandler.removeMessages(4);
                this.mGpsHandler.sendMessage(localMessage);
            }
        }

        private class Nmea
        {
            String mNmea;
            long mTimestamp;

            Nmea(long arg2, String arg4)
            {
                this.mTimestamp = ???;
                Object localObject;
                this.mNmea = localObject;
            }
        }
    }

    private class ListenerTransport extends ILocationListener.Stub
    {
        private static final int TYPE_LOCATION_CHANGED = 1;
        private static final int TYPE_PROVIDER_DISABLED = 4;
        private static final int TYPE_PROVIDER_ENABLED = 3;
        private static final int TYPE_STATUS_CHANGED = 2;
        private LocationListener mListener;
        private final Handler mListenerHandler;

        ListenerTransport(LocationListener paramLooper, Looper arg3)
        {
            this.mListener = paramLooper;
            Looper localLooper;
            if (localLooper == null);
            for (this.mListenerHandler = new Handler()
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    LocationManager.ListenerTransport.this._handleMessage(paramAnonymousMessage);
                }
            }
            ; ; this.mListenerHandler = new Handler(localLooper)
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    LocationManager.ListenerTransport.this._handleMessage(paramAnonymousMessage);
                }
            })
                return;
        }

        private void _handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            try
            {
                while (true)
                {
                    LocationManager.this.mService.locationCallbackFinished(this);
                    return;
                    Location localLocation = new Location((Location)paramMessage.obj);
                    this.mListener.onLocationChanged(localLocation);
                    continue;
                    Bundle localBundle1 = (Bundle)paramMessage.obj;
                    String str = localBundle1.getString("provider");
                    int i = localBundle1.getInt("status");
                    Bundle localBundle2 = localBundle1.getBundle("extras");
                    this.mListener.onStatusChanged(str, i, localBundle2);
                    continue;
                    this.mListener.onProviderEnabled((String)paramMessage.obj);
                    continue;
                    this.mListener.onProviderDisabled((String)paramMessage.obj);
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("LocationManager", "locationCallbackFinished: RemoteException", localRemoteException);
            }
        }

        public void onLocationChanged(Location paramLocation)
        {
            Message localMessage = Message.obtain();
            localMessage.what = 1;
            localMessage.obj = paramLocation;
            this.mListenerHandler.sendMessage(localMessage);
        }

        public void onProviderDisabled(String paramString)
        {
            Message localMessage = Message.obtain();
            localMessage.what = 4;
            localMessage.obj = paramString;
            this.mListenerHandler.sendMessage(localMessage);
        }

        public void onProviderEnabled(String paramString)
        {
            Message localMessage = Message.obtain();
            localMessage.what = 3;
            localMessage.obj = paramString;
            this.mListenerHandler.sendMessage(localMessage);
        }

        public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
        {
            Message localMessage = Message.obtain();
            localMessage.what = 2;
            Bundle localBundle = new Bundle();
            localBundle.putString("provider", paramString);
            localBundle.putInt("status", paramInt);
            if (paramBundle != null)
                localBundle.putBundle("extras", paramBundle);
            localMessage.obj = localBundle;
            this.mListenerHandler.sendMessage(localMessage);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.LocationManager
 * JD-Core Version:        0.6.2
 */