package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IGpsStatusListener extends IInterface
{
    public abstract void onFirstFix(int paramInt)
        throws RemoteException;

    public abstract void onGpsStarted()
        throws RemoteException;

    public abstract void onGpsStopped()
        throws RemoteException;

    public abstract void onNmeaReceived(long paramLong, String paramString)
        throws RemoteException;

    public abstract void onSvStatusChanged(int paramInt1, int[] paramArrayOfInt, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, float[] paramArrayOfFloat3, int paramInt2, int paramInt3, int paramInt4)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IGpsStatusListener
    {
        private static final String DESCRIPTOR = "android.location.IGpsStatusListener";
        static final int TRANSACTION_onFirstFix = 3;
        static final int TRANSACTION_onGpsStarted = 1;
        static final int TRANSACTION_onGpsStopped = 2;
        static final int TRANSACTION_onNmeaReceived = 5;
        static final int TRANSACTION_onSvStatusChanged = 4;

        public Stub()
        {
            attachInterface(this, "android.location.IGpsStatusListener");
        }

        public static IGpsStatusListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.IGpsStatusListener");
                if ((localIInterface != null) && ((localIInterface instanceof IGpsStatusListener)))
                    localObject = (IGpsStatusListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.location.IGpsStatusListener");
                bool = true;
                continue;
                paramParcel1.enforceInterface("android.location.IGpsStatusListener");
                onGpsStarted();
                bool = true;
                continue;
                paramParcel1.enforceInterface("android.location.IGpsStatusListener");
                onGpsStopped();
                bool = true;
                continue;
                paramParcel1.enforceInterface("android.location.IGpsStatusListener");
                onFirstFix(paramParcel1.readInt());
                bool = true;
                continue;
                paramParcel1.enforceInterface("android.location.IGpsStatusListener");
                onSvStatusChanged(paramParcel1.readInt(), paramParcel1.createIntArray(), paramParcel1.createFloatArray(), paramParcel1.createFloatArray(), paramParcel1.createFloatArray(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                bool = true;
                continue;
                paramParcel1.enforceInterface("android.location.IGpsStatusListener");
                onNmeaReceived(paramParcel1.readLong(), paramParcel1.readString());
                bool = true;
            }
        }

        private static class Proxy
            implements IGpsStatusListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.IGpsStatusListener";
            }

            public void onFirstFix(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.IGpsStatusListener");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onGpsStarted()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.IGpsStatusListener");
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onGpsStopped()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.IGpsStatusListener");
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onNmeaReceived(long paramLong, String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.IGpsStatusListener");
                    localParcel.writeLong(paramLong);
                    localParcel.writeString(paramString);
                    this.mRemote.transact(5, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onSvStatusChanged(int paramInt1, int[] paramArrayOfInt, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, float[] paramArrayOfFloat3, int paramInt2, int paramInt3, int paramInt4)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.IGpsStatusListener");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeIntArray(paramArrayOfInt);
                    localParcel.writeFloatArray(paramArrayOfFloat1);
                    localParcel.writeFloatArray(paramArrayOfFloat2);
                    localParcel.writeFloatArray(paramArrayOfFloat3);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    localParcel.writeInt(paramInt4);
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.IGpsStatusListener
 * JD-Core Version:        0.6.2
 */