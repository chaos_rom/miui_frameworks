package android.location;

import android.os.Bundle;

public abstract interface LocationListener
{
    public abstract void onLocationChanged(Location paramLocation);

    public abstract void onProviderDisabled(String paramString);

    public abstract void onProviderEnabled(String paramString);

    public abstract void onStatusChanged(String paramString, int paramInt, Bundle paramBundle);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.LocationListener
 * JD-Core Version:        0.6.2
 */