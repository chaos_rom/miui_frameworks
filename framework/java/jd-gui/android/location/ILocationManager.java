package android.location;

import android.app.PendingIntent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface ILocationManager extends IInterface
{
    public abstract boolean addGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
        throws RemoteException;

    public abstract void addProximityAlert(double paramDouble1, double paramDouble2, float paramFloat, long paramLong, PendingIntent paramPendingIntent, String paramString)
        throws RemoteException;

    public abstract void addTestProvider(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void clearTestProviderEnabled(String paramString)
        throws RemoteException;

    public abstract void clearTestProviderLocation(String paramString)
        throws RemoteException;

    public abstract void clearTestProviderStatus(String paramString)
        throws RemoteException;

    public abstract boolean geocoderIsPresent()
        throws RemoteException;

    public abstract List<String> getAllProviders()
        throws RemoteException;

    public abstract String getBestProvider(Criteria paramCriteria, boolean paramBoolean)
        throws RemoteException;

    public abstract String getFromLocation(double paramDouble1, double paramDouble2, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
        throws RemoteException;

    public abstract String getFromLocationName(String paramString, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
        throws RemoteException;

    public abstract Location getLastKnownLocation(String paramString1, String paramString2)
        throws RemoteException;

    public abstract Bundle getProviderInfo(String paramString)
        throws RemoteException;

    public abstract List<String> getProviders(Criteria paramCriteria, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean isProviderEnabled(String paramString)
        throws RemoteException;

    public abstract void locationCallbackFinished(ILocationListener paramILocationListener)
        throws RemoteException;

    public abstract boolean providerMeetsCriteria(String paramString, Criteria paramCriteria)
        throws RemoteException;

    public abstract void removeGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
        throws RemoteException;

    public abstract void removeProximityAlert(PendingIntent paramPendingIntent)
        throws RemoteException;

    public abstract void removeTestProvider(String paramString)
        throws RemoteException;

    public abstract void removeUpdates(ILocationListener paramILocationListener, String paramString)
        throws RemoteException;

    public abstract void removeUpdatesPI(PendingIntent paramPendingIntent, String paramString)
        throws RemoteException;

    public abstract void reportLocation(Location paramLocation, boolean paramBoolean)
        throws RemoteException;

    public abstract void requestLocationUpdates(String paramString1, Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean, ILocationListener paramILocationListener, String paramString2)
        throws RemoteException;

    public abstract void requestLocationUpdatesPI(String paramString1, Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean, PendingIntent paramPendingIntent, String paramString2)
        throws RemoteException;

    public abstract boolean sendExtraCommand(String paramString1, String paramString2, Bundle paramBundle)
        throws RemoteException;

    public abstract boolean sendNiResponse(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setTestProviderEnabled(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void setTestProviderLocation(String paramString, Location paramLocation)
        throws RemoteException;

    public abstract void setTestProviderStatus(String paramString, int paramInt, Bundle paramBundle, long paramLong)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ILocationManager
    {
        private static final String DESCRIPTOR = "android.location.ILocationManager";
        static final int TRANSACTION_addGpsStatusListener = 9;
        static final int TRANSACTION_addProximityAlert = 13;
        static final int TRANSACTION_addTestProvider = 22;
        static final int TRANSACTION_clearTestProviderEnabled = 27;
        static final int TRANSACTION_clearTestProviderLocation = 25;
        static final int TRANSACTION_clearTestProviderStatus = 29;
        static final int TRANSACTION_geocoderIsPresent = 19;
        static final int TRANSACTION_getAllProviders = 1;
        static final int TRANSACTION_getBestProvider = 3;
        static final int TRANSACTION_getFromLocation = 20;
        static final int TRANSACTION_getFromLocationName = 21;
        static final int TRANSACTION_getLastKnownLocation = 17;
        static final int TRANSACTION_getProviderInfo = 15;
        static final int TRANSACTION_getProviders = 2;
        static final int TRANSACTION_isProviderEnabled = 16;
        static final int TRANSACTION_locationCallbackFinished = 11;
        static final int TRANSACTION_providerMeetsCriteria = 4;
        static final int TRANSACTION_removeGpsStatusListener = 10;
        static final int TRANSACTION_removeProximityAlert = 14;
        static final int TRANSACTION_removeTestProvider = 23;
        static final int TRANSACTION_removeUpdates = 7;
        static final int TRANSACTION_removeUpdatesPI = 8;
        static final int TRANSACTION_reportLocation = 18;
        static final int TRANSACTION_requestLocationUpdates = 5;
        static final int TRANSACTION_requestLocationUpdatesPI = 6;
        static final int TRANSACTION_sendExtraCommand = 12;
        static final int TRANSACTION_sendNiResponse = 30;
        static final int TRANSACTION_setTestProviderEnabled = 26;
        static final int TRANSACTION_setTestProviderLocation = 24;
        static final int TRANSACTION_setTestProviderStatus = 28;

        public Stub()
        {
            attachInterface(this, "android.location.ILocationManager");
        }

        public static ILocationManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.ILocationManager");
                if ((localIInterface != null) && ((localIInterface instanceof ILocationManager)))
                    localObject = (ILocationManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool2;
            switch (paramInt1)
            {
            default:
                bool2 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
                while (true)
                {
                    return bool2;
                    paramParcel2.writeString("android.location.ILocationManager");
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    List localList2 = getAllProviders();
                    paramParcel2.writeNoException();
                    paramParcel2.writeStringList(localList2);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    Criteria localCriteria5;
                    if (paramParcel1.readInt() != 0)
                    {
                        localCriteria5 = (Criteria)Criteria.CREATOR.createFromParcel(paramParcel1);
                        label341: if (paramParcel1.readInt() == 0)
                            break label383;
                    }
                    label383: for (boolean bool20 = true; ; bool20 = false)
                    {
                        List localList1 = getProviders(localCriteria5, bool20);
                        paramParcel2.writeNoException();
                        paramParcel2.writeStringList(localList1);
                        bool2 = true;
                        break;
                        localCriteria5 = null;
                        break label341;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    Criteria localCriteria4;
                    if (paramParcel1.readInt() != 0)
                    {
                        localCriteria4 = (Criteria)Criteria.CREATOR.createFromParcel(paramParcel1);
                        label416: if (paramParcel1.readInt() == 0)
                            break label458;
                    }
                    label458: for (boolean bool19 = true; ; bool19 = false)
                    {
                        String str14 = getBestProvider(localCriteria4, bool19);
                        paramParcel2.writeNoException();
                        paramParcel2.writeString(str14);
                        bool2 = true;
                        break;
                        localCriteria4 = null;
                        break label416;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str13 = paramParcel1.readString();
                    Criteria localCriteria3;
                    if (paramParcel1.readInt() != 0)
                    {
                        localCriteria3 = (Criteria)Criteria.CREATOR.createFromParcel(paramParcel1);
                        label497: boolean bool18 = providerMeetsCriteria(str13, localCriteria3);
                        paramParcel2.writeNoException();
                        if (!bool18)
                            break label537;
                    }
                    label537: for (int i6 = 1; ; i6 = 0)
                    {
                        paramParcel2.writeInt(i6);
                        bool2 = true;
                        break;
                        localCriteria3 = null;
                        break label497;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str12 = paramParcel1.readString();
                    Criteria localCriteria2;
                    label576: long l4;
                    float f3;
                    if (paramParcel1.readInt() != 0)
                    {
                        localCriteria2 = (Criteria)Criteria.CREATOR.createFromParcel(paramParcel1);
                        l4 = paramParcel1.readLong();
                        f3 = paramParcel1.readFloat();
                        if (paramParcel1.readInt() == 0)
                            break label639;
                    }
                    label639: for (boolean bool17 = true; ; bool17 = false)
                    {
                        requestLocationUpdates(str12, localCriteria2, l4, f3, bool17, ILocationListener.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                        localCriteria2 = null;
                        break label576;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str11 = paramParcel1.readString();
                    Criteria localCriteria1;
                    label678: long l3;
                    float f2;
                    boolean bool16;
                    if (paramParcel1.readInt() != 0)
                    {
                        localCriteria1 = (Criteria)Criteria.CREATOR.createFromParcel(paramParcel1);
                        l3 = paramParcel1.readLong();
                        f2 = paramParcel1.readFloat();
                        if (paramParcel1.readInt() == 0)
                            break label757;
                        bool16 = true;
                        label700: if (paramParcel1.readInt() == 0)
                            break label763;
                    }
                    label757: label763: for (PendingIntent localPendingIntent4 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent4 = null)
                    {
                        requestLocationUpdatesPI(str11, localCriteria1, l3, f2, bool16, localPendingIntent4, paramParcel1.readString());
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                        localCriteria1 = null;
                        break label678;
                        bool16 = false;
                        break label700;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    removeUpdates(ILocationListener.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    if (paramParcel1.readInt() != 0);
                    for (PendingIntent localPendingIntent3 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent3 = null)
                    {
                        removeUpdatesPI(localPendingIntent3, paramParcel1.readString());
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    boolean bool15 = addGpsStatusListener(IGpsStatusListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    if (bool15);
                    for (int i5 = 1; ; i5 = 0)
                    {
                        paramParcel2.writeInt(i5);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    removeGpsStatusListener(IGpsStatusListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    locationCallbackFinished(ILocationListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str9 = paramParcel1.readString();
                    String str10 = paramParcel1.readString();
                    Bundle localBundle3;
                    label995: int i4;
                    if (paramParcel1.readInt() != 0)
                    {
                        localBundle3 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                        boolean bool14 = sendExtraCommand(str9, str10, localBundle3);
                        paramParcel2.writeNoException();
                        if (!bool14)
                            break label1054;
                        i4 = 1;
                        label1019: paramParcel2.writeInt(i4);
                        if (localBundle3 == null)
                            break label1060;
                        paramParcel2.writeInt(1);
                        localBundle3.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        localBundle3 = null;
                        break label995;
                        label1054: i4 = 0;
                        break label1019;
                        label1060: paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    double d7 = paramParcel1.readDouble();
                    double d8 = paramParcel1.readDouble();
                    float f1 = paramParcel1.readFloat();
                    long l2 = paramParcel1.readLong();
                    if (paramParcel1.readInt() != 0);
                    for (PendingIntent localPendingIntent2 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent2 = null)
                    {
                        String str8 = paramParcel1.readString();
                        addProximityAlert(d7, d8, f1, l2, localPendingIntent2, str8);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    if (paramParcel1.readInt() != 0);
                    for (PendingIntent localPendingIntent1 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent1 = null)
                    {
                        removeProximityAlert(localPendingIntent1);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    Bundle localBundle2 = getProviderInfo(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (localBundle2 != null)
                    {
                        paramParcel2.writeInt(1);
                        localBundle2.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    boolean bool13 = isProviderEnabled(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool13);
                    for (int i3 = 1; ; i3 = 0)
                    {
                        paramParcel2.writeInt(i3);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    Location localLocation3 = getLastKnownLocation(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (localLocation3 != null)
                    {
                        paramParcel2.writeInt(1);
                        localLocation3.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    Location localLocation2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localLocation2 = (Location)Location.CREATOR.createFromParcel(paramParcel1);
                        label1385: if (paramParcel1.readInt() == 0)
                            break label1419;
                    }
                    label1419: for (boolean bool12 = true; ; bool12 = false)
                    {
                        reportLocation(localLocation2, bool12);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                        localLocation2 = null;
                        break label1385;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    boolean bool11 = geocoderIsPresent();
                    paramParcel2.writeNoException();
                    if (bool11);
                    for (int i2 = 1; ; i2 = 0)
                    {
                        paramParcel2.writeInt(i2);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    double d5 = paramParcel1.readDouble();
                    double d6 = paramParcel1.readDouble();
                    int i1 = paramParcel1.readInt();
                    if (paramParcel1.readInt() != 0);
                    for (GeocoderParams localGeocoderParams2 = (GeocoderParams)GeocoderParams.CREATOR.createFromParcel(paramParcel1); ; localGeocoderParams2 = null)
                    {
                        ArrayList localArrayList2 = new ArrayList();
                        String str7 = getFromLocation(d5, d6, i1, localGeocoderParams2, localArrayList2);
                        paramParcel2.writeNoException();
                        paramParcel2.writeString(str7);
                        paramParcel2.writeTypedList(localArrayList2);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str5 = paramParcel1.readString();
                    double d1 = paramParcel1.readDouble();
                    double d2 = paramParcel1.readDouble();
                    double d3 = paramParcel1.readDouble();
                    double d4 = paramParcel1.readDouble();
                    int n = paramParcel1.readInt();
                    if (paramParcel1.readInt() != 0);
                    for (GeocoderParams localGeocoderParams1 = (GeocoderParams)GeocoderParams.CREATOR.createFromParcel(paramParcel1); ; localGeocoderParams1 = null)
                    {
                        ArrayList localArrayList1 = new ArrayList();
                        String str6 = getFromLocationName(str5, d1, d2, d3, d4, n, localGeocoderParams1, localArrayList1);
                        paramParcel2.writeNoException();
                        paramParcel2.writeString(str6);
                        paramParcel2.writeTypedList(localArrayList1);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str4 = paramParcel1.readString();
                    boolean bool4;
                    label1709: boolean bool5;
                    label1719: boolean bool6;
                    label1729: boolean bool7;
                    label1739: boolean bool8;
                    label1749: boolean bool9;
                    if (paramParcel1.readInt() != 0)
                    {
                        bool4 = true;
                        if (paramParcel1.readInt() == 0)
                            break label1821;
                        bool5 = true;
                        if (paramParcel1.readInt() == 0)
                            break label1827;
                        bool6 = true;
                        if (paramParcel1.readInt() == 0)
                            break label1833;
                        bool7 = true;
                        if (paramParcel1.readInt() == 0)
                            break label1839;
                        bool8 = true;
                        if (paramParcel1.readInt() == 0)
                            break label1845;
                        bool9 = true;
                        label1759: if (paramParcel1.readInt() == 0)
                            break label1851;
                    }
                    label1821: label1827: label1833: label1839: label1845: label1851: for (boolean bool10 = true; ; bool10 = false)
                    {
                        int k = paramParcel1.readInt();
                        int m = paramParcel1.readInt();
                        addTestProvider(str4, bool4, bool5, bool6, bool7, bool8, bool9, bool10, k, m);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                        bool4 = false;
                        break label1709;
                        bool5 = false;
                        break label1719;
                        bool6 = false;
                        break label1729;
                        bool7 = false;
                        break label1739;
                        bool8 = false;
                        break label1749;
                        bool9 = false;
                        break label1759;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    removeTestProvider(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str3 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    for (Location localLocation1 = (Location)Location.CREATOR.createFromParcel(paramParcel1); ; localLocation1 = null)
                    {
                        setTestProviderLocation(str3, localLocation1);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    clearTestProviderLocation(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str2 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    for (boolean bool3 = true; ; bool3 = false)
                    {
                        setTestProviderEnabled(str2, bool3);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    clearTestProviderEnabled(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    String str1 = paramParcel1.readString();
                    int j = paramParcel1.readInt();
                    if (paramParcel1.readInt() != 0);
                    for (Bundle localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle1 = null)
                    {
                        long l1 = paramParcel1.readLong();
                        setTestProviderStatus(str1, j, localBundle1, l1);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationManager");
                    clearTestProviderStatus(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                }
            case 30:
            }
            paramParcel1.enforceInterface("android.location.ILocationManager");
            boolean bool1 = sendNiResponse(paramParcel1.readInt(), paramParcel1.readInt());
            paramParcel2.writeNoException();
            if (bool1);
            for (int i = 1; ; i = 0)
            {
                paramParcel2.writeInt(i);
                bool2 = true;
                break;
            }
        }

        private static class Proxy
            implements ILocationManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public boolean addGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    if (paramIGpsStatusListener != null)
                    {
                        localIBinder = paramIGpsStatusListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        if (i != 0)
                            bool = true;
                        return bool;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void addProximityAlert(double paramDouble1, double paramDouble2, float paramFloat, long paramLong, PendingIntent paramPendingIntent, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeDouble(paramDouble1);
                    localParcel1.writeDouble(paramDouble2);
                    localParcel1.writeFloat(paramFloat);
                    localParcel1.writeLong(paramLong);
                    if (paramPendingIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramPendingIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void addTestProvider(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, int paramInt1, int paramInt2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    int j;
                    int k;
                    label49: int m;
                    label65: int n;
                    label81: int i1;
                    if (paramBoolean1)
                    {
                        j = i;
                        localParcel1.writeInt(j);
                        if (!paramBoolean2)
                            break label185;
                        k = i;
                        localParcel1.writeInt(k);
                        if (!paramBoolean3)
                            break label191;
                        m = i;
                        localParcel1.writeInt(m);
                        if (!paramBoolean4)
                            break label197;
                        n = i;
                        localParcel1.writeInt(n);
                        if (!paramBoolean5)
                            break label203;
                        i1 = i;
                        label97: localParcel1.writeInt(i1);
                        if (!paramBoolean6)
                            break label209;
                    }
                    label185: label191: label197: label203: label209: for (int i2 = i; ; i2 = 0)
                    {
                        localParcel1.writeInt(i2);
                        if (!paramBoolean7)
                            break label215;
                        localParcel1.writeInt(i);
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(22, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        j = 0;
                        break;
                        k = 0;
                        break label49;
                        m = 0;
                        break label65;
                        n = 0;
                        break label81;
                        i1 = 0;
                        break label97;
                    }
                    label215: i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void clearTestProviderEnabled(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(27, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void clearTestProviderLocation(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void clearTestProviderStatus(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean geocoderIsPresent()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<String> getAllProviders()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createStringArrayList();
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getBestProvider(Criteria paramCriteria, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationManager");
                        if (paramCriteria != null)
                        {
                            localParcel1.writeInt(1);
                            paramCriteria.writeToParcel(localParcel1, 0);
                            break label115;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(3, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            String str = localParcel2.readString();
                            return str;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label115: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public String getFromLocation(double paramDouble1, double paramDouble2, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeDouble(paramDouble1);
                    localParcel1.writeDouble(paramDouble2);
                    localParcel1.writeInt(paramInt);
                    if (paramGeocoderParams != null)
                    {
                        localParcel1.writeInt(1);
                        paramGeocoderParams.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(20, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        String str = localParcel2.readString();
                        localParcel2.readTypedList(paramList, Address.CREATOR);
                        return str;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getFromLocationName(String paramString, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeDouble(paramDouble1);
                    localParcel1.writeDouble(paramDouble2);
                    localParcel1.writeDouble(paramDouble3);
                    localParcel1.writeDouble(paramDouble4);
                    localParcel1.writeInt(paramInt);
                    if (paramGeocoderParams != null)
                    {
                        localParcel1.writeInt(1);
                        paramGeocoderParams.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(21, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        String str = localParcel2.readString();
                        localParcel2.readTypedList(paramList, Address.CREATOR);
                        return str;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.ILocationManager";
            }

            public Location getLastKnownLocation(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localLocation = (Location)Location.CREATOR.createFromParcel(localParcel2);
                        return localLocation;
                    }
                    Location localLocation = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Bundle getProviderInfo(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localBundle = (Bundle)Bundle.CREATOR.createFromParcel(localParcel2);
                        return localBundle;
                    }
                    Bundle localBundle = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<String> getProviders(Criteria paramCriteria, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationManager");
                        if (paramCriteria != null)
                        {
                            localParcel1.writeInt(1);
                            paramCriteria.writeToParcel(localParcel1, 0);
                            break label115;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(2, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            ArrayList localArrayList = localParcel2.createStringArrayList();
                            return localArrayList;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label115: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public boolean isProviderEnabled(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void locationCallbackFinished(ILocationListener paramILocationListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    if (paramILocationListener != null)
                    {
                        localIBinder = paramILocationListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean providerMeetsCriteria(String paramString, Criteria paramCriteria)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationManager");
                        localParcel1.writeString(paramString);
                        if (paramCriteria != null)
                        {
                            localParcel1.writeInt(1);
                            paramCriteria.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(4, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void removeGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    if (paramIGpsStatusListener != null)
                    {
                        localIBinder = paramIGpsStatusListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(10, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeProximityAlert(PendingIntent paramPendingIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    if (paramPendingIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramPendingIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(14, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeTestProvider(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeUpdates(ILocationListener paramILocationListener, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    if (paramILocationListener != null)
                    {
                        localIBinder = paramILocationListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeUpdatesPI(PendingIntent paramPendingIntent, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    if (paramPendingIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramPendingIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(8, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void reportLocation(Location paramLocation, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationManager");
                        if (paramLocation != null)
                        {
                            localParcel1.writeInt(1);
                            paramLocation.writeToParcel(localParcel1, 0);
                            break label107;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(18, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label107: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public void requestLocationUpdates(String paramString1, Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean, ILocationListener paramILocationListener, String paramString2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationManager");
                        localParcel1.writeString(paramString1);
                        if (paramCriteria != null)
                        {
                            localParcel1.writeInt(1);
                            paramCriteria.writeToParcel(localParcel1, 0);
                            localParcel1.writeLong(paramLong);
                            localParcel1.writeFloat(paramFloat);
                            if (paramBoolean)
                            {
                                localParcel1.writeInt(i);
                                if (paramILocationListener == null)
                                    break label158;
                                localIBinder = paramILocationListener.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                localParcel1.writeString(paramString2);
                                this.mRemote.transact(5, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    i = 0;
                    continue;
                    label158: IBinder localIBinder = null;
                }
            }

            public void requestLocationUpdatesPI(String paramString1, Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean, PendingIntent paramPendingIntent, String paramString2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationManager");
                        localParcel1.writeString(paramString1);
                        if (paramCriteria != null)
                        {
                            localParcel1.writeInt(1);
                            paramCriteria.writeToParcel(localParcel1, 0);
                            localParcel1.writeLong(paramLong);
                            localParcel1.writeFloat(paramFloat);
                            if (paramBoolean)
                            {
                                localParcel1.writeInt(i);
                                if (paramPendingIntent == null)
                                    break label157;
                                localParcel1.writeInt(1);
                                paramPendingIntent.writeToParcel(localParcel1, 0);
                                localParcel1.writeString(paramString2);
                                this.mRemote.transact(6, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    i = 0;
                    continue;
                    label157: localParcel1.writeInt(0);
                }
            }

            public boolean sendExtraCommand(String paramString1, String paramString2, Bundle paramBundle)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationManager");
                        localParcel1.writeString(paramString1);
                        localParcel1.writeString(paramString2);
                        if (paramBundle != null)
                        {
                            localParcel1.writeInt(1);
                            paramBundle.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(12, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                if (localParcel2.readInt() != 0)
                                    paramBundle.readFromParcel(localParcel2);
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean sendNiResponse(int paramInt1, int paramInt2)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setTestProviderEnabled(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setTestProviderLocation(String paramString, Location paramLocation)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    if (paramLocation != null)
                    {
                        localParcel1.writeInt(1);
                        paramLocation.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(24, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setTestProviderStatus(String paramString, int paramInt, Bundle paramBundle, long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    if (paramBundle != null)
                    {
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeLong(paramLong);
                        this.mRemote.transact(28, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.ILocationManager
 * JD-Core Version:        0.6.2
 */