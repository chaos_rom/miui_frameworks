package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ICountryListener extends IInterface
{
    public abstract void onCountryDetected(Country paramCountry)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ICountryListener
    {
        private static final String DESCRIPTOR = "android.location.ICountryListener";
        static final int TRANSACTION_onCountryDetected = 1;

        public Stub()
        {
            attachInterface(this, "android.location.ICountryListener");
        }

        public static ICountryListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.ICountryListener");
                if ((localIInterface != null) && ((localIInterface instanceof ICountryListener)))
                    localObject = (ICountryListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.location.ICountryListener");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.location.ICountryListener");
            if (paramParcel1.readInt() != 0);
            for (Country localCountry = (Country)Country.CREATOR.createFromParcel(paramParcel1); ; localCountry = null)
            {
                onCountryDetected(localCountry);
                break;
            }
        }

        private static class Proxy
            implements ICountryListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.ICountryListener";
            }

            public void onCountryDetected(Country paramCountry)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.ICountryListener");
                    if (paramCountry != null)
                    {
                        localParcel.writeInt(1);
                        paramCountry.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.ICountryListener
 * JD-Core Version:        0.6.2
 */