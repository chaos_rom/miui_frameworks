package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ICountryDetector extends IInterface
{
    public abstract void addCountryListener(ICountryListener paramICountryListener)
        throws RemoteException;

    public abstract Country detectCountry()
        throws RemoteException;

    public abstract void removeCountryListener(ICountryListener paramICountryListener)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ICountryDetector
    {
        private static final String DESCRIPTOR = "android.location.ICountryDetector";
        static final int TRANSACTION_addCountryListener = 2;
        static final int TRANSACTION_detectCountry = 1;
        static final int TRANSACTION_removeCountryListener = 3;

        public Stub()
        {
            attachInterface(this, "android.location.ICountryDetector");
        }

        public static ICountryDetector asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.ICountryDetector");
                if ((localIInterface != null) && ((localIInterface instanceof ICountryDetector)))
                    localObject = (ICountryDetector)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.location.ICountryDetector");
                continue;
                paramParcel1.enforceInterface("android.location.ICountryDetector");
                Country localCountry = detectCountry();
                paramParcel2.writeNoException();
                if (localCountry != null)
                {
                    paramParcel2.writeInt(i);
                    localCountry.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.location.ICountryDetector");
                    addCountryListener(ICountryListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.location.ICountryDetector");
                    removeCountryListener(ICountryListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                }
            }
        }

        private static class Proxy
            implements ICountryDetector
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addCountryListener(ICountryListener paramICountryListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ICountryDetector");
                    if (paramICountryListener != null)
                    {
                        localIBinder = paramICountryListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public Country detectCountry()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ICountryDetector");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localCountry = (Country)Country.CREATOR.createFromParcel(localParcel2);
                        return localCountry;
                    }
                    Country localCountry = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.ICountryDetector";
            }

            public void removeCountryListener(ICountryListener paramICountryListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ICountryDetector");
                    if (paramICountryListener != null)
                    {
                        localIBinder = paramICountryListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.ICountryDetector
 * JD-Core Version:        0.6.2
 */