package android.location;

public final class GpsSatellite
{
    float mAzimuth;
    float mElevation;
    boolean mHasAlmanac;
    boolean mHasEphemeris;
    int mPrn;
    float mSnr;
    boolean mUsedInFix;
    boolean mValid;

    GpsSatellite(int paramInt)
    {
        this.mPrn = paramInt;
    }

    public float getAzimuth()
    {
        return this.mAzimuth;
    }

    public float getElevation()
    {
        return this.mElevation;
    }

    public int getPrn()
    {
        return this.mPrn;
    }

    public float getSnr()
    {
        return this.mSnr;
    }

    public boolean hasAlmanac()
    {
        return this.mHasAlmanac;
    }

    public boolean hasEphemeris()
    {
        return this.mHasEphemeris;
    }

    void setStatus(GpsSatellite paramGpsSatellite)
    {
        this.mValid = paramGpsSatellite.mValid;
        this.mHasEphemeris = paramGpsSatellite.mHasEphemeris;
        this.mHasAlmanac = paramGpsSatellite.mHasAlmanac;
        this.mUsedInFix = paramGpsSatellite.mUsedInFix;
        this.mSnr = paramGpsSatellite.mSnr;
        this.mElevation = paramGpsSatellite.mElevation;
        this.mAzimuth = paramGpsSatellite.mAzimuth;
    }

    public boolean usedInFix()
    {
        return this.mUsedInFix;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.GpsSatellite
 * JD-Core Version:        0.6.2
 */