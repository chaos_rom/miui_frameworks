package android.location;

import android.os.RemoteException;
import android.util.Log;

public abstract class LocationProvider
{
    public static final int AVAILABLE = 2;
    static final String BAD_CHARS_REGEX = "[^a-zA-Z0-9]";
    public static final int OUT_OF_SERVICE = 0;
    private static final String TAG = "LocationProvider";
    public static final int TEMPORARILY_UNAVAILABLE = 1;
    private final String mName;
    private final ILocationManager mService;

    public LocationProvider(String paramString, ILocationManager paramILocationManager)
    {
        if (paramString.matches("[^a-zA-Z0-9]"))
            throw new IllegalArgumentException("name " + paramString + " contains an illegal character");
        this.mName = paramString;
        this.mService = paramILocationManager;
    }

    public abstract int getAccuracy();

    public String getName()
    {
        return this.mName;
    }

    public abstract int getPowerRequirement();

    public abstract boolean hasMonetaryCost();

    public boolean meetsCriteria(Criteria paramCriteria)
    {
        try
        {
            boolean bool2 = this.mService.providerMeetsCriteria(this.mName, paramCriteria);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("LocationProvider", "meetsCriteria: RemoteException", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public abstract boolean requiresCell();

    public abstract boolean requiresNetwork();

    public abstract boolean requiresSatellite();

    public abstract boolean supportsAltitude();

    public abstract boolean supportsBearing();

    public abstract boolean supportsSpeed();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.LocationProvider
 * JD-Core Version:        0.6.2
 */