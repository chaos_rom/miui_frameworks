package android.location;

import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import java.util.HashMap;

public class CountryDetector
{
    private static final String TAG = "CountryDetector";
    private final HashMap<CountryListener, ListenerTransport> mListeners;
    private final ICountryDetector mService;

    public CountryDetector(ICountryDetector paramICountryDetector)
    {
        this.mService = paramICountryDetector;
        this.mListeners = new HashMap();
    }

    public void addCountryListener(CountryListener paramCountryListener, Looper paramLooper)
    {
        synchronized (this.mListeners)
        {
            ListenerTransport localListenerTransport;
            if (!this.mListeners.containsKey(paramCountryListener))
                localListenerTransport = new ListenerTransport(paramCountryListener, paramLooper);
            try
            {
                this.mService.addCountryListener(localListenerTransport);
                this.mListeners.put(paramCountryListener, localListenerTransport);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("CountryDetector", "addCountryListener: RemoteException", localRemoteException);
            }
        }
    }

    public Country detectCountry()
    {
        try
        {
            Country localCountry2 = this.mService.detectCountry();
            localCountry1 = localCountry2;
            return localCountry1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("CountryDetector", "detectCountry: RemoteException", localRemoteException);
                Country localCountry1 = null;
            }
        }
    }

    public void removeCountryListener(CountryListener paramCountryListener)
    {
        synchronized (this.mListeners)
        {
            ListenerTransport localListenerTransport = (ListenerTransport)this.mListeners.get(paramCountryListener);
            if (localListenerTransport != null);
            try
            {
                this.mListeners.remove(paramCountryListener);
                this.mService.removeCountryListener(localListenerTransport);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("CountryDetector", "removeCountryListener: RemoteException", localRemoteException);
            }
        }
    }

    private static final class ListenerTransport extends ICountryListener.Stub
    {
        private final Handler mHandler;
        private final CountryListener mListener;

        public ListenerTransport(CountryListener paramCountryListener, Looper paramLooper)
        {
            this.mListener = paramCountryListener;
            if (paramLooper != null);
            for (this.mHandler = new Handler(paramLooper); ; this.mHandler = new Handler())
                return;
        }

        public void onCountryDetected(final Country paramCountry)
        {
            this.mHandler.post(new Runnable()
            {
                public void run()
                {
                    CountryDetector.ListenerTransport.this.mListener.onCountryDetected(paramCountry);
                }
            });
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.CountryDetector
 * JD-Core Version:        0.6.2
 */