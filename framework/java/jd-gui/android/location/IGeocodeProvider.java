package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IGeocodeProvider extends IInterface
{
    public abstract String getFromLocation(double paramDouble1, double paramDouble2, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
        throws RemoteException;

    public abstract String getFromLocationName(String paramString, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IGeocodeProvider
    {
        private static final String DESCRIPTOR = "android.location.IGeocodeProvider";
        static final int TRANSACTION_getFromLocation = 1;
        static final int TRANSACTION_getFromLocationName = 2;

        public Stub()
        {
            attachInterface(this, "android.location.IGeocodeProvider");
        }

        public static IGeocodeProvider asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.IGeocodeProvider");
                if ((localIInterface != null) && ((localIInterface instanceof IGeocodeProvider)))
                    localObject = (IGeocodeProvider)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
            case 1598968902:
                for (bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2); ; bool = true)
                {
                    return bool;
                    paramParcel2.writeString("android.location.IGeocodeProvider");
                }
            case 1:
                paramParcel1.enforceInterface("android.location.IGeocodeProvider");
                double d5 = paramParcel1.readDouble();
                double d6 = paramParcel1.readDouble();
                int j = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (GeocoderParams localGeocoderParams2 = (GeocoderParams)GeocoderParams.CREATOR.createFromParcel(paramParcel1); ; localGeocoderParams2 = null)
                {
                    ArrayList localArrayList2 = new ArrayList();
                    String str3 = getFromLocation(d5, d6, j, localGeocoderParams2, localArrayList2);
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str3);
                    paramParcel2.writeTypedList(localArrayList2);
                    bool = true;
                    break;
                }
            case 2:
            }
            paramParcel1.enforceInterface("android.location.IGeocodeProvider");
            String str1 = paramParcel1.readString();
            double d1 = paramParcel1.readDouble();
            double d2 = paramParcel1.readDouble();
            double d3 = paramParcel1.readDouble();
            double d4 = paramParcel1.readDouble();
            int i = paramParcel1.readInt();
            if (paramParcel1.readInt() != 0);
            for (GeocoderParams localGeocoderParams1 = (GeocoderParams)GeocoderParams.CREATOR.createFromParcel(paramParcel1); ; localGeocoderParams1 = null)
            {
                ArrayList localArrayList1 = new ArrayList();
                String str2 = getFromLocationName(str1, d1, d2, d3, d4, i, localGeocoderParams1, localArrayList1);
                paramParcel2.writeNoException();
                paramParcel2.writeString(str2);
                paramParcel2.writeTypedList(localArrayList1);
                bool = true;
                break;
            }
        }

        private static class Proxy
            implements IGeocodeProvider
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getFromLocation(double paramDouble1, double paramDouble2, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.IGeocodeProvider");
                    localParcel1.writeDouble(paramDouble1);
                    localParcel1.writeDouble(paramDouble2);
                    localParcel1.writeInt(paramInt);
                    if (paramGeocoderParams != null)
                    {
                        localParcel1.writeInt(1);
                        paramGeocoderParams.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        String str = localParcel2.readString();
                        localParcel2.readTypedList(paramList, Address.CREATOR);
                        return str;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getFromLocationName(String paramString, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.IGeocodeProvider");
                    localParcel1.writeString(paramString);
                    localParcel1.writeDouble(paramDouble1);
                    localParcel1.writeDouble(paramDouble2);
                    localParcel1.writeDouble(paramDouble3);
                    localParcel1.writeDouble(paramDouble4);
                    localParcel1.writeInt(paramInt);
                    if (paramGeocoderParams != null)
                    {
                        localParcel1.writeInt(1);
                        paramGeocoderParams.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        String str = localParcel2.readString();
                        localParcel2.readTypedList(paramList, Address.CREATOR);
                        return str;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.IGeocodeProvider";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.IGeocodeProvider
 * JD-Core Version:        0.6.2
 */