package android.location;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Printer;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

public class Location
    implements Parcelable
{
    public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator()
    {
        public Location createFromParcel(Parcel paramAnonymousParcel)
        {
            boolean bool1 = true;
            Location localLocation = new Location(paramAnonymousParcel.readString());
            Location.access$002(localLocation, paramAnonymousParcel.readLong());
            Location.access$102(localLocation, paramAnonymousParcel.readDouble());
            Location.access$202(localLocation, paramAnonymousParcel.readDouble());
            boolean bool2;
            boolean bool3;
            label77: boolean bool4;
            if (paramAnonymousParcel.readInt() != 0)
            {
                bool2 = bool1;
                Location.access$302(localLocation, bool2);
                Location.access$402(localLocation, paramAnonymousParcel.readDouble());
                if (paramAnonymousParcel.readInt() == 0)
                    break label158;
                bool3 = bool1;
                Location.access$502(localLocation, bool3);
                Location.access$602(localLocation, paramAnonymousParcel.readFloat());
                if (paramAnonymousParcel.readInt() == 0)
                    break label164;
                bool4 = bool1;
                label103: Location.access$702(localLocation, bool4);
                Location.access$802(localLocation, paramAnonymousParcel.readFloat());
                if (paramAnonymousParcel.readInt() == 0)
                    break label170;
            }
            while (true)
            {
                Location.access$902(localLocation, bool1);
                Location.access$1002(localLocation, paramAnonymousParcel.readFloat());
                Location.access$1102(localLocation, paramAnonymousParcel.readBundle());
                return localLocation;
                bool2 = false;
                break;
                label158: bool3 = false;
                break label77;
                label164: bool4 = false;
                break label103;
                label170: bool1 = false;
            }
        }

        public Location[] newArray(int paramAnonymousInt)
        {
            return new Location[paramAnonymousInt];
        }
    };
    public static final int FORMAT_DEGREES = 0;
    public static final int FORMAT_MINUTES = 1;
    public static final int FORMAT_SECONDS = 2;
    private float mAccuracy = 0.0F;
    private double mAltitude = 0.0D;
    private float mBearing = 0.0F;
    private float mDistance = 0.0F;
    private Bundle mExtras = null;
    private boolean mHasAccuracy = false;
    private boolean mHasAltitude = false;
    private boolean mHasBearing = false;
    private boolean mHasSpeed = false;
    private float mInitialBearing = 0.0F;
    private double mLat1 = 0.0D;
    private double mLat2 = 0.0D;
    private double mLatitude = 0.0D;
    private double mLon1 = 0.0D;
    private double mLon2 = 0.0D;
    private double mLongitude = 0.0D;
    private String mProvider;
    private float[] mResults = new float[2];
    private float mSpeed = 0.0F;
    private long mTime = 0L;

    public Location(Location paramLocation)
    {
        set(paramLocation);
    }

    public Location(String paramString)
    {
        this.mProvider = paramString;
    }

    private static void computeDistanceAndBearing(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, float[] paramArrayOfFloat)
    {
        double d1 = paramDouble1 * 0.0174532925199433D;
        double d2 = paramDouble3 * 0.0174532925199433D;
        double d3 = paramDouble2 * 0.0174532925199433D;
        double d4 = paramDouble4 * 0.0174532925199433D;
        double d5 = (6378137.0D - 6356752.3141999999D) / 6378137.0D;
        double d6 = (6378137.0D * 6378137.0D - 6356752.3141999999D * 6356752.3141999999D) / (6356752.3141999999D * 6356752.3141999999D);
        double d7 = d4 - d3;
        double d8 = 0.0D;
        double d9 = Math.atan((1.0D - d5) * Math.tan(d1));
        double d10 = Math.atan((1.0D - d5) * Math.tan(d2));
        double d11 = Math.cos(d9);
        double d12 = Math.cos(d10);
        double d13 = Math.sin(d9);
        double d14 = Math.sin(d10);
        double d15 = d11 * d12;
        double d16 = d13 * d14;
        double d17 = 0.0D;
        double d18 = 0.0D;
        double d19 = 0.0D;
        double d20 = 0.0D;
        double d21 = d7;
        label652: for (int i = 0; ; i++)
        {
            double d22;
            double d25;
            double d26;
            double d27;
            double d28;
            if (i < 20)
            {
                d22 = d21;
                d19 = Math.cos(d21);
                d20 = Math.sin(d21);
                double d23 = d12 * d20;
                double d24 = d11 * d14 - d19 * (d13 * d12);
                d25 = Math.sqrt(d23 * d23 + d24 * d24);
                d26 = d16 + d15 * d19;
                d17 = Math.atan2(d25, d26);
                if (d25 != 0.0D)
                    break label622;
                d27 = 0.0D;
                d28 = 1.0D - d27 * d27;
                if (d28 != 0.0D)
                    break label635;
            }
            label622: label635: for (double d29 = 0.0D; ; d29 = d26 - 2.0D * d16 / d28)
            {
                double d30 = d28 * d6;
                d8 = 1.0D + d30 / 16384.0D * (4096.0D + d30 * (-768.0D + d30 * (320.0D - 175.0D * d30)));
                double d31 = d30 / 1024.0D * (256.0D + d30 * (-128.0D + d30 * (74.0D - 47.0D * d30)));
                double d32 = d28 * (d5 / 16.0D) * (4.0D + d5 * (4.0D - 3.0D * d28));
                double d33 = d29 * d29;
                d18 = d31 * d25 * (d29 + d31 / 4.0D * (d26 * (-1.0D + 2.0D * d33) - d29 * (d31 / 6.0D) * (-3.0D + d25 * (4.0D * d25)) * (-3.0D + 4.0D * d33)));
                d21 = d7 + d27 * (d5 * (1.0D - d32)) * (d17 + d32 * d25 * (d29 + d32 * d26 * (-1.0D + d29 * (2.0D * d29))));
                if (Math.abs((d21 - d22) / d21) >= 1.0E-12D)
                    break label652;
                paramArrayOfFloat[0] = ((float)(6356752.3141999999D * d8 * (d17 - d18)));
                if (paramArrayOfFloat.length > 1)
                {
                    paramArrayOfFloat[1] = ((float)(57.295779513082323D * (float)Math.atan2(d12 * d20, d11 * d14 - d19 * (d13 * d12))));
                    if (paramArrayOfFloat.length > 2)
                        paramArrayOfFloat[2] = ((float)(57.295779513082323D * (float)Math.atan2(d11 * d20, d12 * -d13 + d19 * (d11 * d14))));
                }
                return;
                d27 = d15 * d20 / d25;
                break;
            }
        }
    }

    public static double convert(String paramString)
    {
        if (paramString == null)
            throw new NullPointerException("coordinate");
        int i = 0;
        if (paramString.charAt(0) == '-')
        {
            paramString = paramString.substring(1);
            i = 1;
        }
        StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ":");
        int j = localStringTokenizer.countTokens();
        if (j < 1)
            throw new IllegalArgumentException("coordinate=" + paramString);
        double d5;
        double d4;
        String str2;
        int k;
        double d1;
        try
        {
            String str1 = localStringTokenizer.nextToken();
            if (j == 1)
            {
                d5 = Double.parseDouble(str1);
                if (i == 0)
                    break label317;
                d4 = -d5;
                break label314;
            }
            str2 = localStringTokenizer.nextToken();
            k = Integer.parseInt(str1);
            d1 = 0.0D;
            if (localStringTokenizer.hasMoreTokens())
            {
                d2 = Integer.parseInt(str2);
                d1 = Double.parseDouble(localStringTokenizer.nextToken());
                break label324;
                throw new IllegalArgumentException("coordinate=" + paramString);
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            throw new IllegalArgumentException("coordinate=" + paramString);
        }
        double d2 = Double.parseDouble(str2);
        break label324;
        label221: throw new IllegalArgumentException("coordinate=" + paramString);
        while (true)
        {
            label248: throw new IllegalArgumentException("coordinate=" + paramString);
            label314: label317: label324: label381: label383: 
            do
            {
                double d3 = (d1 + (3600.0D * k + 60.0D * d2)) / 3600.0D;
                if (i != 0)
                    d4 = -d3;
                for (d4 = d3; ; d4 = d5)
                    return d4;
                if ((i != 0) && (k == 180) && (d2 == 0.0D) && (d1 == 0.0D));
                for (int m = 1; ; m = 0)
                {
                    if (k < 0.0D)
                        break label381;
                    if ((k <= 179) || (m != 0))
                        break label383;
                    break;
                }
                break;
                if ((d2 < 0.0D) || (d2 > 59.0D))
                    break label221;
                if (d1 < 0.0D)
                    break label248;
            }
            while (d1 <= 59.0D);
        }
    }

    public static String convert(double paramDouble, int paramInt)
    {
        if ((paramDouble < -180.0D) || (paramDouble > 180.0D) || (Double.isNaN(paramDouble)))
            throw new IllegalArgumentException("coordinate=" + paramDouble);
        if ((paramInt != 0) && (paramInt != 1) && (paramInt != 2))
            throw new IllegalArgumentException("outputType=" + paramInt);
        StringBuilder localStringBuilder = new StringBuilder();
        if (paramDouble < 0.0D)
        {
            localStringBuilder.append('-');
            paramDouble = -paramDouble;
        }
        DecimalFormat localDecimalFormat = new DecimalFormat("###.#####");
        if ((paramInt == 1) || (paramInt == 2))
        {
            int i = (int)Math.floor(paramDouble);
            localStringBuilder.append(i);
            localStringBuilder.append(':');
            paramDouble = 60.0D * (paramDouble - i);
            if (paramInt == 2)
            {
                int j = (int)Math.floor(paramDouble);
                localStringBuilder.append(j);
                localStringBuilder.append(':');
                paramDouble = 60.0D * (paramDouble - j);
            }
        }
        localStringBuilder.append(localDecimalFormat.format(paramDouble));
        return localStringBuilder.toString();
    }

    public static void distanceBetween(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, float[] paramArrayOfFloat)
    {
        if ((paramArrayOfFloat == null) || (paramArrayOfFloat.length < 1))
            throw new IllegalArgumentException("results is null or has length < 1");
        computeDistanceAndBearing(paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramArrayOfFloat);
    }

    public float bearingTo(Location paramLocation)
    {
        synchronized (this.mResults)
        {
            if ((this.mLatitude != this.mLat1) || (this.mLongitude != this.mLon1) || (paramLocation.mLatitude != this.mLat2) || (paramLocation.mLongitude != this.mLon2))
            {
                computeDistanceAndBearing(this.mLatitude, this.mLongitude, paramLocation.mLatitude, paramLocation.mLongitude, this.mResults);
                this.mLat1 = this.mLatitude;
                this.mLon1 = this.mLongitude;
                this.mLat2 = paramLocation.mLatitude;
                this.mLon2 = paramLocation.mLongitude;
                this.mDistance = this.mResults[0];
                this.mInitialBearing = this.mResults[1];
            }
            float f = this.mInitialBearing;
            return f;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public float distanceTo(Location paramLocation)
    {
        synchronized (this.mResults)
        {
            if ((this.mLatitude != this.mLat1) || (this.mLongitude != this.mLon1) || (paramLocation.mLatitude != this.mLat2) || (paramLocation.mLongitude != this.mLon2))
            {
                computeDistanceAndBearing(this.mLatitude, this.mLongitude, paramLocation.mLatitude, paramLocation.mLongitude, this.mResults);
                this.mLat1 = this.mLatitude;
                this.mLon1 = this.mLongitude;
                this.mLat2 = paramLocation.mLatitude;
                this.mLon2 = paramLocation.mLongitude;
                this.mDistance = this.mResults[0];
                this.mInitialBearing = this.mResults[1];
            }
            float f = this.mDistance;
            return f;
        }
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        paramPrinter.println(paramString + "mProvider=" + this.mProvider + " mTime=" + this.mTime);
        paramPrinter.println(paramString + "mLatitude=" + this.mLatitude + " mLongitude=" + this.mLongitude);
        paramPrinter.println(paramString + "mHasAltitude=" + this.mHasAltitude + " mAltitude=" + this.mAltitude);
        paramPrinter.println(paramString + "mHasSpeed=" + this.mHasSpeed + " mSpeed=" + this.mSpeed);
        paramPrinter.println(paramString + "mHasBearing=" + this.mHasBearing + " mBearing=" + this.mBearing);
        paramPrinter.println(paramString + "mHasAccuracy=" + this.mHasAccuracy + " mAccuracy=" + this.mAccuracy);
        paramPrinter.println(paramString + "mExtras=" + this.mExtras);
    }

    public float getAccuracy()
    {
        return this.mAccuracy;
    }

    public double getAltitude()
    {
        return this.mAltitude;
    }

    public float getBearing()
    {
        return this.mBearing;
    }

    public Bundle getExtras()
    {
        return this.mExtras;
    }

    public double getLatitude()
    {
        return this.mLatitude;
    }

    public double getLongitude()
    {
        return this.mLongitude;
    }

    public String getProvider()
    {
        return this.mProvider;
    }

    public float getSpeed()
    {
        return this.mSpeed;
    }

    public long getTime()
    {
        return this.mTime;
    }

    public boolean hasAccuracy()
    {
        return this.mHasAccuracy;
    }

    public boolean hasAltitude()
    {
        return this.mHasAltitude;
    }

    public boolean hasBearing()
    {
        return this.mHasBearing;
    }

    public boolean hasSpeed()
    {
        return this.mHasSpeed;
    }

    public void removeAccuracy()
    {
        this.mAccuracy = 0.0F;
        this.mHasAccuracy = false;
    }

    public void removeAltitude()
    {
        this.mAltitude = 0.0D;
        this.mHasAltitude = false;
    }

    public void removeBearing()
    {
        this.mBearing = 0.0F;
        this.mHasBearing = false;
    }

    public void removeSpeed()
    {
        this.mSpeed = 0.0F;
        this.mHasSpeed = false;
    }

    public void reset()
    {
        this.mProvider = null;
        this.mTime = 0L;
        this.mLatitude = 0.0D;
        this.mLongitude = 0.0D;
        this.mHasAltitude = false;
        this.mAltitude = 0.0D;
        this.mHasSpeed = false;
        this.mSpeed = 0.0F;
        this.mHasBearing = false;
        this.mBearing = 0.0F;
        this.mHasAccuracy = false;
        this.mAccuracy = 0.0F;
        this.mExtras = null;
    }

    public void set(Location paramLocation)
    {
        this.mProvider = paramLocation.mProvider;
        this.mTime = paramLocation.mTime;
        this.mLatitude = paramLocation.mLatitude;
        this.mLongitude = paramLocation.mLongitude;
        this.mHasAltitude = paramLocation.mHasAltitude;
        this.mAltitude = paramLocation.mAltitude;
        this.mHasSpeed = paramLocation.mHasSpeed;
        this.mSpeed = paramLocation.mSpeed;
        this.mHasBearing = paramLocation.mHasBearing;
        this.mBearing = paramLocation.mBearing;
        this.mHasAccuracy = paramLocation.mHasAccuracy;
        this.mAccuracy = paramLocation.mAccuracy;
        if (paramLocation.mExtras == null);
        for (Bundle localBundle = null; ; localBundle = new Bundle(paramLocation.mExtras))
        {
            this.mExtras = localBundle;
            return;
        }
    }

    public void setAccuracy(float paramFloat)
    {
        this.mAccuracy = paramFloat;
        this.mHasAccuracy = true;
    }

    public void setAltitude(double paramDouble)
    {
        this.mAltitude = paramDouble;
        this.mHasAltitude = true;
    }

    public void setBearing(float paramFloat)
    {
        while (paramFloat < 0.0F)
            paramFloat += 360.0F;
        while (paramFloat >= 360.0F)
            paramFloat -= 360.0F;
        this.mBearing = paramFloat;
        this.mHasBearing = true;
    }

    public void setExtras(Bundle paramBundle)
    {
        if (paramBundle == null);
        for (Bundle localBundle = null; ; localBundle = new Bundle(paramBundle))
        {
            this.mExtras = localBundle;
            return;
        }
    }

    public void setLatitude(double paramDouble)
    {
        this.mLatitude = paramDouble;
    }

    public void setLongitude(double paramDouble)
    {
        this.mLongitude = paramDouble;
    }

    public void setProvider(String paramString)
    {
        this.mProvider = paramString;
    }

    public void setSpeed(float paramFloat)
    {
        this.mSpeed = paramFloat;
        this.mHasSpeed = true;
    }

    public void setTime(long paramLong)
    {
        this.mTime = paramLong;
    }

    public String toString()
    {
        return "Location[mProvider=" + this.mProvider + ",mTime=" + this.mTime + ",mLatitude=" + this.mLatitude + ",mLongitude=" + this.mLongitude + ",mHasAltitude=" + this.mHasAltitude + ",mAltitude=" + this.mAltitude + ",mHasSpeed=" + this.mHasSpeed + ",mSpeed=" + this.mSpeed + ",mHasBearing=" + this.mHasBearing + ",mBearing=" + this.mBearing + ",mHasAccuracy=" + this.mHasAccuracy + ",mAccuracy=" + this.mAccuracy + ",mExtras=" + this.mExtras + "]";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeString(this.mProvider);
        paramParcel.writeLong(this.mTime);
        paramParcel.writeDouble(this.mLatitude);
        paramParcel.writeDouble(this.mLongitude);
        int j;
        int k;
        label68: int m;
        if (this.mHasAltitude)
        {
            j = i;
            paramParcel.writeInt(j);
            paramParcel.writeDouble(this.mAltitude);
            if (!this.mHasSpeed)
                break label141;
            k = i;
            paramParcel.writeInt(k);
            paramParcel.writeFloat(this.mSpeed);
            if (!this.mHasBearing)
                break label147;
            m = i;
            label92: paramParcel.writeInt(m);
            paramParcel.writeFloat(this.mBearing);
            if (!this.mHasAccuracy)
                break label153;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeFloat(this.mAccuracy);
            paramParcel.writeBundle(this.mExtras);
            return;
            j = 0;
            break;
            label141: k = 0;
            break label68;
            label147: m = 0;
            break label92;
            label153: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.Location
 * JD-Core Version:        0.6.2
 */