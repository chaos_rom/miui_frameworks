package android.location;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Set;

public class Address
    implements Parcelable
{
    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator()
    {
        public Address createFromParcel(Parcel paramAnonymousParcel)
        {
            boolean bool1 = false;
            String str1 = paramAnonymousParcel.readString();
            String str2 = paramAnonymousParcel.readString();
            if (str2.length() > 0);
            Address localAddress;
            for (Locale localLocale = new Locale(str1, str2); ; localLocale = new Locale(str1))
            {
                localAddress = new Address(localLocale);
                int i = paramAnonymousParcel.readInt();
                if (i <= 0)
                    break;
                Address.access$002(localAddress, new HashMap(i));
                for (int j = 0; j < i; j++)
                {
                    int k = paramAnonymousParcel.readInt();
                    String str3 = paramAnonymousParcel.readString();
                    localAddress.mAddressLines.put(Integer.valueOf(k), str3);
                    Address.access$102(localAddress, Math.max(localAddress.mMaxAddressLineIndex, k));
                }
            }
            Address.access$002(localAddress, null);
            Address.access$102(localAddress, -1);
            Address.access$202(localAddress, paramAnonymousParcel.readString());
            Address.access$302(localAddress, paramAnonymousParcel.readString());
            Address.access$402(localAddress, paramAnonymousParcel.readString());
            Address.access$502(localAddress, paramAnonymousParcel.readString());
            Address.access$602(localAddress, paramAnonymousParcel.readString());
            Address.access$702(localAddress, paramAnonymousParcel.readString());
            Address.access$802(localAddress, paramAnonymousParcel.readString());
            Address.access$902(localAddress, paramAnonymousParcel.readString());
            Address.access$1002(localAddress, paramAnonymousParcel.readString());
            Address.access$1102(localAddress, paramAnonymousParcel.readString());
            Address.access$1202(localAddress, paramAnonymousParcel.readString());
            boolean bool2;
            if (paramAnonymousParcel.readInt() == 0)
            {
                bool2 = false;
                Address.access$1302(localAddress, bool2);
                if (localAddress.mHasLatitude)
                    Address.access$1402(localAddress, paramAnonymousParcel.readDouble());
                if (paramAnonymousParcel.readInt() != 0)
                    break label375;
            }
            while (true)
            {
                Address.access$1502(localAddress, bool1);
                if (localAddress.mHasLongitude)
                    Address.access$1602(localAddress, paramAnonymousParcel.readDouble());
                Address.access$1702(localAddress, paramAnonymousParcel.readString());
                Address.access$1802(localAddress, paramAnonymousParcel.readString());
                Address.access$1902(localAddress, paramAnonymousParcel.readBundle());
                return localAddress;
                bool2 = true;
                break;
                label375: bool1 = true;
            }
        }

        public Address[] newArray(int paramAnonymousInt)
        {
            return new Address[paramAnonymousInt];
        }
    };
    private HashMap<Integer, String> mAddressLines;
    private String mAdminArea;
    private String mCountryCode;
    private String mCountryName;
    private Bundle mExtras = null;
    private String mFeatureName;
    private boolean mHasLatitude = false;
    private boolean mHasLongitude = false;
    private double mLatitude;
    private Locale mLocale;
    private String mLocality;
    private double mLongitude;
    private int mMaxAddressLineIndex = -1;
    private String mPhone;
    private String mPostalCode;
    private String mPremises;
    private String mSubAdminArea;
    private String mSubLocality;
    private String mSubThoroughfare;
    private String mThoroughfare;
    private String mUrl;

    public Address(Locale paramLocale)
    {
        this.mLocale = paramLocale;
    }

    public void clearLatitude()
    {
        this.mHasLatitude = false;
    }

    public void clearLongitude()
    {
        this.mHasLongitude = false;
    }

    public int describeContents()
    {
        if (this.mExtras != null);
        for (int i = this.mExtras.describeContents(); ; i = 0)
            return i;
    }

    public String getAddressLine(int paramInt)
    {
        if (paramInt < 0)
            throw new IllegalArgumentException("index = " + paramInt + " < 0");
        if (this.mAddressLines == null);
        for (String str = null; ; str = (String)this.mAddressLines.get(Integer.valueOf(paramInt)))
            return str;
    }

    public String getAdminArea()
    {
        return this.mAdminArea;
    }

    public String getCountryCode()
    {
        return this.mCountryCode;
    }

    public String getCountryName()
    {
        return this.mCountryName;
    }

    public Bundle getExtras()
    {
        return this.mExtras;
    }

    public String getFeatureName()
    {
        return this.mFeatureName;
    }

    public double getLatitude()
    {
        if (this.mHasLatitude)
            return this.mLatitude;
        throw new IllegalStateException();
    }

    public Locale getLocale()
    {
        return this.mLocale;
    }

    public String getLocality()
    {
        return this.mLocality;
    }

    public double getLongitude()
    {
        if (this.mHasLongitude)
            return this.mLongitude;
        throw new IllegalStateException();
    }

    public int getMaxAddressLineIndex()
    {
        return this.mMaxAddressLineIndex;
    }

    public String getPhone()
    {
        return this.mPhone;
    }

    public String getPostalCode()
    {
        return this.mPostalCode;
    }

    public String getPremises()
    {
        return this.mPremises;
    }

    public String getSubAdminArea()
    {
        return this.mSubAdminArea;
    }

    public String getSubLocality()
    {
        return this.mSubLocality;
    }

    public String getSubThoroughfare()
    {
        return this.mSubThoroughfare;
    }

    public String getThoroughfare()
    {
        return this.mThoroughfare;
    }

    public String getUrl()
    {
        return this.mUrl;
    }

    public boolean hasLatitude()
    {
        return this.mHasLatitude;
    }

    public boolean hasLongitude()
    {
        return this.mHasLongitude;
    }

    public void setAddressLine(int paramInt, String paramString)
    {
        if (paramInt < 0)
            throw new IllegalArgumentException("index = " + paramInt + " < 0");
        if (this.mAddressLines == null)
            this.mAddressLines = new HashMap();
        this.mAddressLines.put(Integer.valueOf(paramInt), paramString);
        if (paramString == null)
        {
            this.mMaxAddressLineIndex = -1;
            Iterator localIterator = this.mAddressLines.keySet().iterator();
            while (localIterator.hasNext())
            {
                Integer localInteger = (Integer)localIterator.next();
                this.mMaxAddressLineIndex = Math.max(this.mMaxAddressLineIndex, localInteger.intValue());
            }
        }
        this.mMaxAddressLineIndex = Math.max(this.mMaxAddressLineIndex, paramInt);
    }

    public void setAdminArea(String paramString)
    {
        this.mAdminArea = paramString;
    }

    public void setCountryCode(String paramString)
    {
        this.mCountryCode = paramString;
    }

    public void setCountryName(String paramString)
    {
        this.mCountryName = paramString;
    }

    public void setExtras(Bundle paramBundle)
    {
        if (paramBundle == null);
        for (Bundle localBundle = null; ; localBundle = new Bundle(paramBundle))
        {
            this.mExtras = localBundle;
            return;
        }
    }

    public void setFeatureName(String paramString)
    {
        this.mFeatureName = paramString;
    }

    public void setLatitude(double paramDouble)
    {
        this.mLatitude = paramDouble;
        this.mHasLatitude = true;
    }

    public void setLocality(String paramString)
    {
        this.mLocality = paramString;
    }

    public void setLongitude(double paramDouble)
    {
        this.mLongitude = paramDouble;
        this.mHasLongitude = true;
    }

    public void setPhone(String paramString)
    {
        this.mPhone = paramString;
    }

    public void setPostalCode(String paramString)
    {
        this.mPostalCode = paramString;
    }

    public void setPremises(String paramString)
    {
        this.mPremises = paramString;
    }

    public void setSubAdminArea(String paramString)
    {
        this.mSubAdminArea = paramString;
    }

    public void setSubLocality(String paramString)
    {
        this.mSubLocality = paramString;
    }

    public void setSubThoroughfare(String paramString)
    {
        this.mSubThoroughfare = paramString;
    }

    public void setThoroughfare(String paramString)
    {
        this.mThoroughfare = paramString;
    }

    public void setUrl(String paramString)
    {
        this.mUrl = paramString;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Address[addressLines=[");
        int i = 0;
        if (i <= this.mMaxAddressLineIndex)
        {
            if (i > 0)
                localStringBuilder.append(',');
            localStringBuilder.append(i);
            localStringBuilder.append(':');
            String str = (String)this.mAddressLines.get(Integer.valueOf(i));
            if (str == null)
                localStringBuilder.append("null");
            while (true)
            {
                i++;
                break;
                localStringBuilder.append('"');
                localStringBuilder.append(str);
                localStringBuilder.append('"');
            }
        }
        localStringBuilder.append(']');
        localStringBuilder.append(",feature=");
        localStringBuilder.append(this.mFeatureName);
        localStringBuilder.append(",admin=");
        localStringBuilder.append(this.mAdminArea);
        localStringBuilder.append(",sub-admin=");
        localStringBuilder.append(this.mSubAdminArea);
        localStringBuilder.append(",locality=");
        localStringBuilder.append(this.mLocality);
        localStringBuilder.append(",thoroughfare=");
        localStringBuilder.append(this.mThoroughfare);
        localStringBuilder.append(",postalCode=");
        localStringBuilder.append(this.mPostalCode);
        localStringBuilder.append(",countryCode=");
        localStringBuilder.append(this.mCountryCode);
        localStringBuilder.append(",countryName=");
        localStringBuilder.append(this.mCountryName);
        localStringBuilder.append(",hasLatitude=");
        localStringBuilder.append(this.mHasLatitude);
        localStringBuilder.append(",latitude=");
        localStringBuilder.append(this.mLatitude);
        localStringBuilder.append(",hasLongitude=");
        localStringBuilder.append(this.mHasLongitude);
        localStringBuilder.append(",longitude=");
        localStringBuilder.append(this.mLongitude);
        localStringBuilder.append(",phone=");
        localStringBuilder.append(this.mPhone);
        localStringBuilder.append(",url=");
        localStringBuilder.append(this.mUrl);
        localStringBuilder.append(",extras=");
        localStringBuilder.append(this.mExtras);
        localStringBuilder.append(']');
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeString(this.mLocale.getLanguage());
        paramParcel.writeString(this.mLocale.getCountry());
        int j;
        if (this.mAddressLines == null)
        {
            paramParcel.writeInt(0);
            paramParcel.writeString(this.mFeatureName);
            paramParcel.writeString(this.mAdminArea);
            paramParcel.writeString(this.mSubAdminArea);
            paramParcel.writeString(this.mLocality);
            paramParcel.writeString(this.mSubLocality);
            paramParcel.writeString(this.mThoroughfare);
            paramParcel.writeString(this.mSubThoroughfare);
            paramParcel.writeString(this.mPremises);
            paramParcel.writeString(this.mPostalCode);
            paramParcel.writeString(this.mCountryCode);
            paramParcel.writeString(this.mCountryName);
            if (!this.mHasLatitude)
                break label292;
            j = i;
            label134: paramParcel.writeInt(j);
            if (this.mHasLatitude)
                paramParcel.writeDouble(this.mLatitude);
            if (!this.mHasLongitude)
                break label298;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            if (this.mHasLongitude)
                paramParcel.writeDouble(this.mLongitude);
            paramParcel.writeString(this.mPhone);
            paramParcel.writeString(this.mUrl);
            paramParcel.writeBundle(this.mExtras);
            return;
            Set localSet = this.mAddressLines.entrySet();
            paramParcel.writeInt(localSet.size());
            Iterator localIterator = localSet.iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                paramParcel.writeInt(((Integer)localEntry.getKey()).intValue());
                paramParcel.writeString((String)localEntry.getValue());
            }
            break;
            label292: j = 0;
            break label134;
            label298: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.Address
 * JD-Core Version:        0.6.2
 */