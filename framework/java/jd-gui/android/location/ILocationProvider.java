package android.location;

import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.WorkSource;

public abstract interface ILocationProvider extends IInterface
{
    public abstract void addListener(int paramInt)
        throws RemoteException;

    public abstract void disable()
        throws RemoteException;

    public abstract void enable()
        throws RemoteException;

    public abstract void enableLocationTracking(boolean paramBoolean)
        throws RemoteException;

    public abstract int getAccuracy()
        throws RemoteException;

    public abstract String getInternalState()
        throws RemoteException;

    public abstract int getPowerRequirement()
        throws RemoteException;

    public abstract int getStatus(Bundle paramBundle)
        throws RemoteException;

    public abstract long getStatusUpdateTime()
        throws RemoteException;

    public abstract boolean hasMonetaryCost()
        throws RemoteException;

    public abstract boolean meetsCriteria(Criteria paramCriteria)
        throws RemoteException;

    public abstract void removeListener(int paramInt)
        throws RemoteException;

    public abstract boolean requiresCell()
        throws RemoteException;

    public abstract boolean requiresNetwork()
        throws RemoteException;

    public abstract boolean requiresSatellite()
        throws RemoteException;

    public abstract boolean sendExtraCommand(String paramString, Bundle paramBundle)
        throws RemoteException;

    public abstract void setMinTime(long paramLong, WorkSource paramWorkSource)
        throws RemoteException;

    public abstract boolean supportsAltitude()
        throws RemoteException;

    public abstract boolean supportsBearing()
        throws RemoteException;

    public abstract boolean supportsSpeed()
        throws RemoteException;

    public abstract void updateLocation(Location paramLocation)
        throws RemoteException;

    public abstract void updateNetworkState(int paramInt, NetworkInfo paramNetworkInfo)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ILocationProvider
    {
        private static final String DESCRIPTOR = "android.location.ILocationProvider";
        static final int TRANSACTION_addListener = 21;
        static final int TRANSACTION_disable = 12;
        static final int TRANSACTION_enable = 11;
        static final int TRANSACTION_enableLocationTracking = 16;
        static final int TRANSACTION_getAccuracy = 10;
        static final int TRANSACTION_getInternalState = 15;
        static final int TRANSACTION_getPowerRequirement = 8;
        static final int TRANSACTION_getStatus = 13;
        static final int TRANSACTION_getStatusUpdateTime = 14;
        static final int TRANSACTION_hasMonetaryCost = 4;
        static final int TRANSACTION_meetsCriteria = 9;
        static final int TRANSACTION_removeListener = 22;
        static final int TRANSACTION_requiresCell = 3;
        static final int TRANSACTION_requiresNetwork = 1;
        static final int TRANSACTION_requiresSatellite = 2;
        static final int TRANSACTION_sendExtraCommand = 20;
        static final int TRANSACTION_setMinTime = 17;
        static final int TRANSACTION_supportsAltitude = 5;
        static final int TRANSACTION_supportsBearing = 7;
        static final int TRANSACTION_supportsSpeed = 6;
        static final int TRANSACTION_updateLocation = 19;
        static final int TRANSACTION_updateNetworkState = 18;

        public Stub()
        {
            attachInterface(this, "android.location.ILocationProvider");
        }

        public static ILocationProvider asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.ILocationProvider");
                if ((localIInterface != null) && ((localIInterface instanceof ILocationProvider)))
                    localObject = (ILocationProvider)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.location.ILocationProvider");
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                boolean bool9 = requiresNetwork();
                paramParcel2.writeNoException();
                if (bool9)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                boolean bool8 = requiresSatellite();
                paramParcel2.writeNoException();
                if (bool8)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                boolean bool7 = requiresCell();
                paramParcel2.writeNoException();
                if (bool7)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                boolean bool6 = hasMonetaryCost();
                paramParcel2.writeNoException();
                if (bool6)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                boolean bool5 = supportsAltitude();
                paramParcel2.writeNoException();
                if (bool5)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                boolean bool4 = supportsSpeed();
                paramParcel2.writeNoException();
                if (bool4)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                boolean bool3 = supportsBearing();
                paramParcel2.writeNoException();
                if (bool3)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                int i5 = getPowerRequirement();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i5);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                if (paramParcel1.readInt() != 0);
                for (Criteria localCriteria = (Criteria)Criteria.CREATOR.createFromParcel(paramParcel1); ; localCriteria = null)
                {
                    boolean bool2 = meetsCriteria(localCriteria);
                    paramParcel2.writeNoException();
                    if (bool2)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                int i4 = getAccuracy();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i4);
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                enable();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                disable();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.location.ILocationProvider");
                Bundle localBundle2 = new Bundle();
                int i3 = getStatus(localBundle2);
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i3);
                if (localBundle2 != null)
                {
                    paramParcel2.writeInt(j);
                    localBundle2.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    long l2 = getStatusUpdateTime();
                    paramParcel2.writeNoException();
                    paramParcel2.writeLong(l2);
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    String str2 = getInternalState();
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str2);
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    if (paramParcel1.readInt() != 0);
                    int i2;
                    for (int i1 = j; ; i2 = 0)
                    {
                        enableLocationTracking(i1);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    long l1 = paramParcel1.readLong();
                    if (paramParcel1.readInt() != 0);
                    for (WorkSource localWorkSource = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource = null)
                    {
                        setMinTime(l1, localWorkSource);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    int n = paramParcel1.readInt();
                    if (paramParcel1.readInt() != 0);
                    for (NetworkInfo localNetworkInfo = (NetworkInfo)NetworkInfo.CREATOR.createFromParcel(paramParcel1); ; localNetworkInfo = null)
                    {
                        updateNetworkState(n, localNetworkInfo);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    if (paramParcel1.readInt() != 0);
                    for (Location localLocation = (Location)Location.CREATOR.createFromParcel(paramParcel1); ; localLocation = null)
                    {
                        updateLocation(localLocation);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    String str1 = paramParcel1.readString();
                    Bundle localBundle1;
                    if (paramParcel1.readInt() != 0)
                    {
                        localBundle1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                        label944: boolean bool1 = sendExtraCommand(str1, localBundle1);
                        paramParcel2.writeNoException();
                        if (!bool1)
                            break label1001;
                    }
                    label1001: int m;
                    for (int k = j; ; m = 0)
                    {
                        paramParcel2.writeInt(k);
                        if (localBundle1 == null)
                            break label1007;
                        paramParcel2.writeInt(j);
                        localBundle1.writeToParcel(paramParcel2, j);
                        break;
                        localBundle1 = null;
                        break label944;
                    }
                    label1007: paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    addListener(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.location.ILocationProvider");
                    removeListener(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                }
            }
        }

        private static class Proxy
            implements ILocationProvider
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addListener(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void disable()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enable()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enableLocationTracking(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getAccuracy()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.ILocationProvider";
            }

            public String getInternalState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPowerRequirement()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getStatus(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (localParcel2.readInt() != 0)
                        paramBundle.readFromParcel(localParcel2);
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getStatusUpdateTime()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasMonetaryCost()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean meetsCriteria(Criteria paramCriteria)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                        if (paramCriteria != null)
                        {
                            localParcel1.writeInt(1);
                            paramCriteria.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(9, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void removeListener(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean requiresCell()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean requiresNetwork()
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        return bool;
                    bool = false;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean requiresSatellite()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean sendExtraCommand(String paramString, Bundle paramBundle)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                        localParcel1.writeString(paramString);
                        if (paramBundle != null)
                        {
                            localParcel1.writeInt(1);
                            paramBundle.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(20, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                if (localParcel2.readInt() != 0)
                                    paramBundle.readFromParcel(localParcel2);
                                return bool;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void setMinTime(long paramLong, WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    localParcel1.writeLong(paramLong);
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(17, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean supportsAltitude()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean supportsBearing()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean supportsSpeed()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateLocation(Location paramLocation)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    if (paramLocation != null)
                    {
                        localParcel1.writeInt(1);
                        paramLocation.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(19, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateNetworkState(int paramInt, NetworkInfo paramNetworkInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.ILocationProvider");
                    localParcel1.writeInt(paramInt);
                    if (paramNetworkInfo != null)
                    {
                        localParcel1.writeInt(1);
                        paramNetworkInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(18, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.ILocationProvider
 * JD-Core Version:        0.6.2
 */