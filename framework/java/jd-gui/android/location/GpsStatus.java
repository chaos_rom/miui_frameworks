package android.location;

import java.util.Iterator;
import java.util.NoSuchElementException;

public final class GpsStatus
{
    public static final int GPS_EVENT_FIRST_FIX = 3;
    public static final int GPS_EVENT_SATELLITE_STATUS = 4;
    public static final int GPS_EVENT_STARTED = 1;
    public static final int GPS_EVENT_STOPPED = 2;
    private static final int NUM_SATELLITES = 255;
    private Iterable<GpsSatellite> mSatelliteList = new Iterable()
    {
        public Iterator<GpsSatellite> iterator()
        {
            return new GpsStatus.SatelliteIterator(GpsStatus.this, GpsStatus.this.mSatellites);
        }
    };
    private GpsSatellite[] mSatellites = new GpsSatellite['ÿ'];
    private int mTimeToFirstFix;

    GpsStatus()
    {
        for (int i = 0; i < this.mSatellites.length; i++)
            this.mSatellites[i] = new GpsSatellite(i + 1);
    }

    public int getMaxSatellites()
    {
        return 255;
    }

    public Iterable<GpsSatellite> getSatellites()
    {
        return this.mSatelliteList;
    }

    public int getTimeToFirstFix()
    {
        return this.mTimeToFirstFix;
    }

    /** @deprecated */
    void setStatus(int paramInt1, int[] paramArrayOfInt, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, float[] paramArrayOfFloat3, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = 0;
        while (true)
        {
            try
            {
                if (i < this.mSatellites.length)
                {
                    this.mSatellites[i].mValid = false;
                    i++;
                    continue;
                    if (j < paramInt1)
                    {
                        int k = -1 + paramArrayOfInt[j];
                        int m = 1 << k;
                        if ((k >= 0) && (k < this.mSatellites.length))
                        {
                            GpsSatellite localGpsSatellite = this.mSatellites[k];
                            localGpsSatellite.mValid = true;
                            localGpsSatellite.mSnr = paramArrayOfFloat1[j];
                            localGpsSatellite.mElevation = paramArrayOfFloat2[j];
                            localGpsSatellite.mAzimuth = paramArrayOfFloat3[j];
                            if ((paramInt2 & m) != 0)
                            {
                                bool1 = true;
                                localGpsSatellite.mHasEphemeris = bool1;
                                if ((paramInt3 & m) == 0)
                                    continue;
                                bool2 = true;
                                localGpsSatellite.mHasAlmanac = bool2;
                                if ((paramInt4 & m) == 0)
                                    continue;
                                bool3 = true;
                                localGpsSatellite.mUsedInFix = bool3;
                            }
                        }
                        else
                        {
                            j++;
                            continue;
                        }
                        boolean bool1 = false;
                        continue;
                        boolean bool2 = false;
                        continue;
                        boolean bool3 = false;
                        continue;
                    }
                    return;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            int j = 0;
        }
    }

    void setStatus(GpsStatus paramGpsStatus)
    {
        this.mTimeToFirstFix = paramGpsStatus.getTimeToFirstFix();
        for (int i = 0; i < this.mSatellites.length; i++)
            this.mSatellites[i].setStatus(paramGpsStatus.mSatellites[i]);
    }

    void setTimeToFirstFix(int paramInt)
    {
        this.mTimeToFirstFix = paramInt;
    }

    public static abstract interface NmeaListener
    {
        public abstract void onNmeaReceived(long paramLong, String paramString);
    }

    public static abstract interface Listener
    {
        public abstract void onGpsStatusChanged(int paramInt);
    }

    private final class SatelliteIterator
        implements Iterator<GpsSatellite>
    {
        int mIndex = 0;
        private GpsSatellite[] mSatellites;

        SatelliteIterator(GpsSatellite[] arg2)
        {
            Object localObject;
            this.mSatellites = localObject;
        }

        public boolean hasNext()
        {
            int i = this.mIndex;
            if (i < this.mSatellites.length)
                if (!this.mSatellites[i].mValid);
            for (boolean bool = true; ; bool = false)
            {
                return bool;
                i++;
                break;
            }
        }

        public GpsSatellite next()
        {
            while (this.mIndex < this.mSatellites.length)
            {
                GpsSatellite[] arrayOfGpsSatellite = this.mSatellites;
                int i = this.mIndex;
                this.mIndex = (i + 1);
                GpsSatellite localGpsSatellite = arrayOfGpsSatellite[i];
                if (localGpsSatellite.mValid)
                    return localGpsSatellite;
            }
            throw new NoSuchElementException();
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.GpsStatus
 * JD-Core Version:        0.6.2
 */