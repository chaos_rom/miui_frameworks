package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IGpsStatusProvider extends IInterface
{
    public abstract void addGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
        throws RemoteException;

    public abstract void removeGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IGpsStatusProvider
    {
        private static final String DESCRIPTOR = "android.location.IGpsStatusProvider";
        static final int TRANSACTION_addGpsStatusListener = 1;
        static final int TRANSACTION_removeGpsStatusListener = 2;

        public Stub()
        {
            attachInterface(this, "android.location.IGpsStatusProvider");
        }

        public static IGpsStatusProvider asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.IGpsStatusProvider");
                if ((localIInterface != null) && ((localIInterface instanceof IGpsStatusProvider)))
                    localObject = (IGpsStatusProvider)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.location.IGpsStatusProvider");
                continue;
                paramParcel1.enforceInterface("android.location.IGpsStatusProvider");
                addGpsStatusListener(IGpsStatusListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.location.IGpsStatusProvider");
                removeGpsStatusListener(IGpsStatusListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IGpsStatusProvider
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.IGpsStatusProvider");
                    if (paramIGpsStatusListener != null)
                    {
                        localIBinder = paramIGpsStatusListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.IGpsStatusProvider";
            }

            public void removeGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.IGpsStatusProvider");
                    if (paramIGpsStatusListener != null)
                    {
                        localIBinder = paramIGpsStatusListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.IGpsStatusProvider
 * JD-Core Version:        0.6.2
 */