package android.location;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ILocationListener extends IInterface
{
    public abstract void onLocationChanged(Location paramLocation)
        throws RemoteException;

    public abstract void onProviderDisabled(String paramString)
        throws RemoteException;

    public abstract void onProviderEnabled(String paramString)
        throws RemoteException;

    public abstract void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ILocationListener
    {
        private static final String DESCRIPTOR = "android.location.ILocationListener";
        static final int TRANSACTION_onLocationChanged = 1;
        static final int TRANSACTION_onProviderDisabled = 4;
        static final int TRANSACTION_onProviderEnabled = 3;
        static final int TRANSACTION_onStatusChanged = 2;

        public Stub()
        {
            attachInterface(this, "android.location.ILocationListener");
        }

        public static ILocationListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.ILocationListener");
                if ((localIInterface != null) && ((localIInterface instanceof ILocationListener)))
                    localObject = (ILocationListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.location.ILocationListener");
                continue;
                paramParcel1.enforceInterface("android.location.ILocationListener");
                if (paramParcel1.readInt() != 0);
                for (Location localLocation = (Location)Location.CREATOR.createFromParcel(paramParcel1); ; localLocation = null)
                {
                    onLocationChanged(localLocation);
                    break;
                }
                paramParcel1.enforceInterface("android.location.ILocationListener");
                String str = paramParcel1.readString();
                int i = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                {
                    onStatusChanged(str, i, localBundle);
                    break;
                }
                paramParcel1.enforceInterface("android.location.ILocationListener");
                onProviderEnabled(paramParcel1.readString());
                continue;
                paramParcel1.enforceInterface("android.location.ILocationListener");
                onProviderDisabled(paramParcel1.readString());
            }
        }

        private static class Proxy
            implements ILocationListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.ILocationListener";
            }

            public void onLocationChanged(Location paramLocation)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.ILocationListener");
                    if (paramLocation != null)
                    {
                        localParcel.writeInt(1);
                        paramLocation.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onProviderDisabled(String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.ILocationListener");
                    localParcel.writeString(paramString);
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onProviderEnabled(String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.ILocationListener");
                    localParcel.writeString(paramString);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.location.ILocationListener");
                    localParcel.writeString(paramString);
                    localParcel.writeInt(paramInt);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.ILocationListener
 * JD-Core Version:        0.6.2
 */