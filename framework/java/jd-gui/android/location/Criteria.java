package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Criteria
    implements Parcelable
{
    public static final int ACCURACY_COARSE = 2;
    public static final int ACCURACY_FINE = 1;
    public static final int ACCURACY_HIGH = 3;
    public static final int ACCURACY_LOW = 1;
    public static final int ACCURACY_MEDIUM = 2;
    public static final Parcelable.Creator<Criteria> CREATOR = new Parcelable.Creator()
    {
        public Criteria createFromParcel(Parcel paramAnonymousParcel)
        {
            boolean bool1 = true;
            Criteria localCriteria = new Criteria();
            Criteria.access$002(localCriteria, paramAnonymousParcel.readInt());
            Criteria.access$102(localCriteria, paramAnonymousParcel.readInt());
            Criteria.access$202(localCriteria, paramAnonymousParcel.readInt());
            Criteria.access$302(localCriteria, paramAnonymousParcel.readInt());
            Criteria.access$402(localCriteria, paramAnonymousParcel.readInt());
            boolean bool2;
            boolean bool3;
            label82: boolean bool4;
            if (paramAnonymousParcel.readInt() != 0)
            {
                bool2 = bool1;
                Criteria.access$502(localCriteria, bool2);
                if (paramAnonymousParcel.readInt() == 0)
                    break label127;
                bool3 = bool1;
                Criteria.access$602(localCriteria, bool3);
                if (paramAnonymousParcel.readInt() == 0)
                    break label133;
                bool4 = bool1;
                label99: Criteria.access$702(localCriteria, bool4);
                if (paramAnonymousParcel.readInt() == 0)
                    break label139;
            }
            while (true)
            {
                Criteria.access$802(localCriteria, bool1);
                return localCriteria;
                bool2 = false;
                break;
                label127: bool3 = false;
                break label82;
                label133: bool4 = false;
                break label99;
                label139: bool1 = false;
            }
        }

        public Criteria[] newArray(int paramAnonymousInt)
        {
            return new Criteria[paramAnonymousInt];
        }
    };
    public static final int NO_REQUIREMENT = 0;
    public static final int POWER_HIGH = 3;
    public static final int POWER_LOW = 1;
    public static final int POWER_MEDIUM = 2;
    private boolean mAltitudeRequired = false;
    private int mBearingAccuracy = 0;
    private boolean mBearingRequired = false;
    private boolean mCostAllowed = false;
    private int mHorizontalAccuracy = 0;
    private int mPowerRequirement = 0;
    private int mSpeedAccuracy = 0;
    private boolean mSpeedRequired = false;
    private int mVerticalAccuracy = 0;

    public Criteria()
    {
    }

    public Criteria(Criteria paramCriteria)
    {
        this.mHorizontalAccuracy = paramCriteria.mHorizontalAccuracy;
        this.mVerticalAccuracy = paramCriteria.mVerticalAccuracy;
        this.mSpeedAccuracy = paramCriteria.mSpeedAccuracy;
        this.mBearingAccuracy = paramCriteria.mBearingAccuracy;
        this.mPowerRequirement = paramCriteria.mPowerRequirement;
        this.mAltitudeRequired = paramCriteria.mAltitudeRequired;
        this.mBearingRequired = paramCriteria.mBearingRequired;
        this.mSpeedRequired = paramCriteria.mSpeedRequired;
        this.mCostAllowed = paramCriteria.mCostAllowed;
    }

    public int describeContents()
    {
        return 0;
    }

    public int getAccuracy()
    {
        if (this.mHorizontalAccuracy >= 3);
        for (int i = 1; ; i = 2)
            return i;
    }

    public int getBearingAccuracy()
    {
        return this.mBearingAccuracy;
    }

    public int getHorizontalAccuracy()
    {
        return this.mHorizontalAccuracy;
    }

    public int getPowerRequirement()
    {
        return this.mPowerRequirement;
    }

    public int getSpeedAccuracy()
    {
        return this.mSpeedAccuracy;
    }

    public int getVerticalAccuracy()
    {
        return this.mVerticalAccuracy;
    }

    public boolean isAltitudeRequired()
    {
        return this.mAltitudeRequired;
    }

    public boolean isBearingRequired()
    {
        return this.mBearingRequired;
    }

    public boolean isCostAllowed()
    {
        return this.mCostAllowed;
    }

    public boolean isSpeedRequired()
    {
        return this.mSpeedRequired;
    }

    public void setAccuracy(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 2))
            throw new IllegalArgumentException("accuracy=" + paramInt);
        if (paramInt == 1);
        for (this.mHorizontalAccuracy = 3; ; this.mHorizontalAccuracy = 1)
            return;
    }

    public void setAltitudeRequired(boolean paramBoolean)
    {
        this.mAltitudeRequired = paramBoolean;
    }

    public void setBearingAccuracy(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException("accuracy=" + paramInt);
        this.mBearingAccuracy = paramInt;
    }

    public void setBearingRequired(boolean paramBoolean)
    {
        this.mBearingRequired = paramBoolean;
    }

    public void setCostAllowed(boolean paramBoolean)
    {
        this.mCostAllowed = paramBoolean;
    }

    public void setHorizontalAccuracy(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException("accuracy=" + paramInt);
        this.mHorizontalAccuracy = paramInt;
    }

    public void setPowerRequirement(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException("level=" + paramInt);
        this.mPowerRequirement = paramInt;
    }

    public void setSpeedAccuracy(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException("accuracy=" + paramInt);
        this.mSpeedAccuracy = paramInt;
    }

    public void setSpeedRequired(boolean paramBoolean)
    {
        this.mSpeedRequired = paramBoolean;
    }

    public void setVerticalAccuracy(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException("accuracy=" + paramInt);
        this.mVerticalAccuracy = paramInt;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeInt(this.mHorizontalAccuracy);
        paramParcel.writeInt(this.mVerticalAccuracy);
        paramParcel.writeInt(this.mSpeedAccuracy);
        paramParcel.writeInt(this.mBearingAccuracy);
        paramParcel.writeInt(this.mPowerRequirement);
        int j;
        int k;
        label68: int m;
        if (this.mAltitudeRequired)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.mBearingRequired)
                break label109;
            k = i;
            paramParcel.writeInt(k);
            if (!this.mSpeedRequired)
                break label115;
            m = i;
            label84: paramParcel.writeInt(m);
            if (!this.mCostAllowed)
                break label121;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            return;
            j = 0;
            break;
            label109: k = 0;
            break label68;
            label115: m = 0;
            break label84;
            label121: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.Criteria
 * JD-Core Version:        0.6.2
 */