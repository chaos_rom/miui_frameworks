package android.location;

public abstract interface CountryListener
{
    public abstract void onCountryDetected(Country paramCountry);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.CountryListener
 * JD-Core Version:        0.6.2
 */