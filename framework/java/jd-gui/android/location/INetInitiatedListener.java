package android.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface INetInitiatedListener extends IInterface
{
    public abstract boolean sendNiResponse(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INetInitiatedListener
    {
        private static final String DESCRIPTOR = "android.location.INetInitiatedListener";
        static final int TRANSACTION_sendNiResponse = 1;

        public Stub()
        {
            attachInterface(this, "android.location.INetInitiatedListener");
        }

        public static INetInitiatedListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.location.INetInitiatedListener");
                if ((localIInterface != null) && ((localIInterface instanceof INetInitiatedListener)))
                    localObject = (INetInitiatedListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return i;
                    paramParcel2.writeString("android.location.INetInitiatedListener");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.location.INetInitiatedListener");
            boolean bool = sendNiResponse(paramParcel1.readInt(), paramParcel1.readInt());
            paramParcel2.writeNoException();
            if (bool);
            int k;
            for (int j = i; ; k = 0)
            {
                paramParcel2.writeInt(j);
                break;
            }
        }

        private static class Proxy
            implements INetInitiatedListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.location.INetInitiatedListener";
            }

            public boolean sendNiResponse(int paramInt1, int paramInt2)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.location.INetInitiatedListener");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        return bool;
                    bool = false;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.INetInitiatedListener
 * JD-Core Version:        0.6.2
 */