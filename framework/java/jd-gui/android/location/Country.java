package android.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import java.util.Locale;

public class Country
    implements Parcelable
{
    public static final int COUNTRY_SOURCE_LOCALE = 3;
    public static final int COUNTRY_SOURCE_LOCATION = 1;
    public static final int COUNTRY_SOURCE_NETWORK = 0;
    public static final int COUNTRY_SOURCE_SIM = 2;
    public static final Parcelable.Creator<Country> CREATOR = new Parcelable.Creator()
    {
        public Country createFromParcel(Parcel paramAnonymousParcel)
        {
            return new Country(paramAnonymousParcel.readString(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readLong(), null);
        }

        public Country[] newArray(int paramAnonymousInt)
        {
            return new Country[paramAnonymousInt];
        }
    };
    private final String mCountryIso;
    private int mHashCode;
    private final int mSource;
    private final long mTimestamp;

    public Country(Country paramCountry)
    {
        this.mCountryIso = paramCountry.mCountryIso;
        this.mSource = paramCountry.mSource;
        this.mTimestamp = paramCountry.mTimestamp;
    }

    public Country(String paramString, int paramInt)
    {
        if ((paramString == null) || (paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException();
        this.mCountryIso = paramString.toUpperCase(Locale.US);
        this.mSource = paramInt;
        this.mTimestamp = SystemClock.elapsedRealtime();
    }

    private Country(String paramString, int paramInt, long paramLong)
    {
        if ((paramString == null) || (paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException();
        this.mCountryIso = paramString.toUpperCase(Locale.US);
        this.mSource = paramInt;
        this.mTimestamp = paramLong;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if ((paramObject instanceof Country))
            {
                Country localCountry = (Country)paramObject;
                if ((!this.mCountryIso.equals(localCountry.getCountryIso())) || (this.mSource != localCountry.getSource()))
                    bool = false;
            }
            else
            {
                bool = false;
            }
        }
    }

    public boolean equalsIgnoreSource(Country paramCountry)
    {
        if ((paramCountry != null) && (this.mCountryIso.equals(paramCountry.getCountryIso())));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public final String getCountryIso()
    {
        return this.mCountryIso;
    }

    public final int getSource()
    {
        return this.mSource;
    }

    public final long getTimestamp()
    {
        return this.mTimestamp;
    }

    public int hashCode()
    {
        if (this.mHashCode == 0)
            this.mHashCode = (13 * (221 + this.mCountryIso.hashCode()) + this.mSource);
        return this.mHashCode;
    }

    public String toString()
    {
        return "Country {ISO=" + this.mCountryIso + ", source=" + this.mSource + ", time=" + this.mTimestamp + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mCountryIso);
        paramParcel.writeInt(this.mSource);
        paramParcel.writeLong(this.mTimestamp);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.location.Country
 * JD-Core Version:        0.6.2
 */