package android.debug;

import java.io.PrintStream;

public class JNITest
{
    private native int part1(int paramInt, double paramDouble, String paramString, int[] paramArrayOfInt);

    private int part2(double paramDouble, int paramInt, String paramString)
    {
        System.out.println(paramString + " : " + (float)paramDouble + " : " + paramInt);
        return 6 + part3(paramString);
    }

    private static native int part3(String paramString);

    public int test(int paramInt, double paramDouble, String paramString)
    {
        int[] arrayOfInt = new int[4];
        arrayOfInt[0] = 42;
        arrayOfInt[1] = 53;
        arrayOfInt[2] = 65;
        arrayOfInt[3] = 127;
        return part1(paramInt, paramDouble, paramString, arrayOfInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.debug.JNITest
 * JD-Core Version:        0.6.2
 */