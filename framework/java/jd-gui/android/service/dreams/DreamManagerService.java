package android.service.dreams;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings.Secure;
import android.util.Slog;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class DreamManagerService extends IDreamManager.Stub
    implements ServiceConnection
{
    private static final boolean DEBUG = true;
    private static final String TAG = "DreamManagerService";
    private Context mContext;
    private IDreamService mCurrentDream;
    private ComponentName mCurrentDreamComponent;
    private Binder mCurrentDreamToken;
    private IWindowManager mIWindowManager;
    final Object mLock = new Object[0];

    public DreamManagerService(Context paramContext)
    {
        Slog.v("DreamManagerService", "DreamManagerService startup");
        this.mContext = paramContext;
        this.mIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
    }

    private void checkPermission(String paramString)
    {
        if (this.mContext.checkCallingOrSelfPermission(paramString) != 0)
            throw new SecurityException("Access denied to process: " + Binder.getCallingPid() + ", must have permission " + paramString);
    }

    public void awaken()
    {
        Slog.v("DreamManagerService", "awaken()");
        synchronized (this.mLock)
        {
            if (this.mCurrentDream != null)
                this.mContext.unbindService(this);
            return;
        }
    }

    public void bindDreamComponentL(ComponentName paramComponentName, boolean paramBoolean)
    {
        Slog.v("DreamManagerService", "bindDreamComponent: componentName=" + paramComponentName + " pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        Intent localIntent = new Intent("android.intent.action.MAIN").setComponent(paramComponentName).addFlags(8388608).putExtra("android.dreams.TEST", paramBoolean);
        if (!this.mContext.bindService(localIntent, this, 1))
            Slog.w("DreamManagerService", "unable to bind service: " + paramComponentName);
        while (true)
        {
            return;
            this.mCurrentDreamComponent = paramComponentName;
            this.mCurrentDreamToken = new Binder();
            try
            {
                Slog.v("DreamManagerService", "Adding window token: " + this.mCurrentDreamToken + " for window type: " + 2023);
                this.mIWindowManager.addWindowToken(this.mCurrentDreamToken, 2023);
            }
            catch (RemoteException localRemoteException)
            {
                Slog.w("DreamManagerService", "Unable to add window token. Proceed at your own risk.");
            }
        }
    }

    public void dream()
    {
        ComponentName localComponentName = getDreamComponent();
        if (localComponentName != null)
            synchronized (this.mLock)
            {
                long l = Binder.clearCallingIdentity();
                try
                {
                    bindDreamComponentL(localComponentName, false);
                    Binder.restoreCallingIdentity(l);
                }
                finally
                {
                    localObject3 = finally;
                    Binder.restoreCallingIdentity(l);
                    throw localObject3;
                }
            }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "DreamManagerService");
        paramPrintWriter.println("Dreamland:");
        paramPrintWriter.print("    component=");
        paramPrintWriter.println(this.mCurrentDreamComponent);
        paramPrintWriter.print("    token=");
        paramPrintWriter.println(this.mCurrentDreamToken);
        paramPrintWriter.print("    dream=");
        paramPrintWriter.println(this.mCurrentDream);
    }

    public ComponentName getDreamComponent()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "screensaver_component");
        if (str == null)
            str = this.mContext.getResources().getString(17039404);
        if (str != null);
        for (ComponentName localComponentName = ComponentName.unflattenFromString(str); ; localComponentName = null)
            return localComponentName;
    }

    public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
    {
        Slog.v("DreamManagerService", "connected to dream: " + paramComponentName + " binder=" + paramIBinder + " thread=" + Thread.currentThread().getId());
        this.mCurrentDream = IDreamService.Stub.asInterface(paramIBinder);
        try
        {
            Slog.v("DreamManagerService", "attaching with token:" + this.mCurrentDreamToken);
            this.mCurrentDream.attach(this.mCurrentDreamToken);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Slog.w("DreamManagerService", "Unable to send window token to dream:" + localRemoteException);
        }
    }

    public void onServiceDisconnected(ComponentName paramComponentName)
    {
        Slog.v("DreamManagerService", "disconnected: " + paramComponentName + " service: " + this.mCurrentDream);
        this.mCurrentDream = null;
        this.mCurrentDreamToken = null;
    }

    public void setDreamComponent(ComponentName paramComponentName)
    {
        Settings.Secure.putString(this.mContext.getContentResolver(), "screensaver_component", paramComponentName.flattenToString());
    }

    public void systemReady()
    {
        Slog.v("DreamManagerService", "ready to dream!");
    }

    public void testDream(ComponentName paramComponentName)
    {
        Slog.v("DreamManagerService", "startDream name=" + paramComponentName + " pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        synchronized (this.mLock)
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                bindDreamComponentL(paramComponentName, true);
                Binder.restoreCallingIdentity(l);
                return;
            }
            finally
            {
                localObject3 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject3;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.dreams.DreamManagerService
 * JD-Core Version:        0.6.2
 */