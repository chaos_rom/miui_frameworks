package android.service.dreams;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IDreamService extends IInterface
{
    public abstract void attach(IBinder paramIBinder)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IDreamService
    {
        private static final String DESCRIPTOR = "android.service.dreams.IDreamService";
        static final int TRANSACTION_attach = 1;

        public Stub()
        {
            attachInterface(this, "android.service.dreams.IDreamService");
        }

        public static IDreamService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.service.dreams.IDreamService");
                if ((localIInterface != null) && ((localIInterface instanceof IDreamService)))
                    localObject = (IDreamService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.service.dreams.IDreamService");
                continue;
                paramParcel1.enforceInterface("android.service.dreams.IDreamService");
                attach(paramParcel1.readStrongBinder());
            }
        }

        private static class Proxy
            implements IDreamService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void attach(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.service.dreams.IDreamService");
                    localParcel.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.service.dreams.IDreamService";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.dreams.IDreamService
 * JD-Core Version:        0.6.2
 */