package android.service.dreams;

import android.app.Service;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Slog;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.policy.PolicyManager;

public class Dream extends Service
    implements Window.Callback
{
    private static final boolean DEBUG = true;
    public static final String SERVICE_INTERFACE = "android.service.dreams.Dream";
    private static final String TAG = "Dream";
    boolean mFinished = false;
    final Handler mHandler = new Handler();
    private boolean mInteractive;
    private IDreamManager mSandman;
    private Window mWindow;
    private WindowManager mWindowManager;

    public void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        getWindow().addContentView(paramView, paramLayoutParams);
    }

    final void attach(IBinder paramIBinder)
    {
        Slog.v("Dream", "Dream attached on thread " + Thread.currentThread().getId());
        this.mWindow = PolicyManager.makeNewWindow(this);
        this.mWindow.setCallback(this);
        this.mWindow.requestFeature(1);
        this.mWindow.setBackgroundDrawable(new ColorDrawable(-16777216));
        Slog.v("Dream", "attaching window token: " + paramIBinder + " to window of type " + 2023);
        WindowManager.LayoutParams localLayoutParams = this.mWindow.getAttributes();
        localLayoutParams.type = 2023;
        localLayoutParams.token = paramIBinder;
        localLayoutParams.windowAnimations = 16974317;
        Slog.v("Dream", "created and attached window: " + this.mWindow);
        this.mWindow.setWindowManager(null, paramIBinder, "dream", true);
        this.mWindowManager = this.mWindow.getWindowManager();
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                Slog.v("Dream", "Dream window added on thread " + Thread.currentThread().getId());
                Dream.this.getWindowManager().addView(Dream.this.mWindow.getDecorView(), Dream.this.mWindow.getAttributes());
            }
        });
    }

    public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        if (!this.mInteractive)
            finish();
        for (boolean bool = true; ; bool = this.mWindow.superDispatchGenericMotionEvent(paramMotionEvent))
            return bool;
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        if (!this.mInteractive)
            finish();
        for (boolean bool = true; ; bool = this.mWindow.superDispatchKeyEvent(paramKeyEvent))
            return bool;
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
    {
        if (!this.mInteractive)
            finish();
        for (boolean bool = true; ; bool = this.mWindow.superDispatchKeyShortcutEvent(paramKeyEvent))
            return bool;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        return false;
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        if (!this.mInteractive)
            finish();
        for (boolean bool = true; ; bool = this.mWindow.superDispatchTouchEvent(paramMotionEvent))
            return bool;
    }

    public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent)
    {
        if (!this.mInteractive)
            finish();
        for (boolean bool = true; ; bool = this.mWindow.superDispatchTrackballEvent(paramMotionEvent))
            return bool;
    }

    public View findViewById(int paramInt)
    {
        return getWindow().findViewById(paramInt);
    }

    public void finish()
    {
        if (this.mFinished);
        while (true)
        {
            return;
            try
            {
                this.mSandman.awaken();
                stopSelf();
                this.mFinished = true;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public Window getWindow()
    {
        return this.mWindow;
    }

    public WindowManager getWindowManager()
    {
        return this.mWindowManager;
    }

    public boolean isInteractive()
    {
        return this.mInteractive;
    }

    protected void lightsOut()
    {
        View localView = this.mWindow.getDecorView();
        if (localView != null)
            localView.setSystemUiVisibility(1);
    }

    public void onActionModeFinished(ActionMode paramActionMode)
    {
    }

    public void onActionModeStarted(ActionMode paramActionMode)
    {
    }

    public void onAttachedToWindow()
    {
        this.mWindow.addFlags(524289);
        lightsOut();
    }

    public final IBinder onBind(Intent paramIntent)
    {
        return new IDreamServiceWrapper();
    }

    public void onContentChanged()
    {
    }

    public void onCreate()
    {
        super.onCreate();
        Slog.v("Dream", "Dream created on thread " + Thread.currentThread().getId());
        this.mSandman = IDreamManager.Stub.asInterface(ServiceManager.getService("dreams"));
    }

    public boolean onCreatePanelMenu(int paramInt, Menu paramMenu)
    {
        return false;
    }

    public View onCreatePanelView(int paramInt)
    {
        return null;
    }

    public void onDestroy()
    {
        super.onDestroy();
        this.mWindowManager.removeView(this.mWindow.getDecorView());
    }

    public void onDetachedFromWindow()
    {
    }

    public boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem)
    {
        return false;
    }

    public boolean onMenuOpened(int paramInt, Menu paramMenu)
    {
        return false;
    }

    public void onPanelClosed(int paramInt, Menu paramMenu)
    {
    }

    public boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu)
    {
        return false;
    }

    public boolean onSearchRequested()
    {
        return false;
    }

    public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
    {
        return super.onStartCommand(paramIntent, paramInt1, paramInt2);
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams)
    {
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
    }

    public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback)
    {
        return null;
    }

    public void setContentView(int paramInt)
    {
        getWindow().setContentView(paramInt);
    }

    public void setContentView(View paramView)
    {
        getWindow().setContentView(paramView);
    }

    public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        getWindow().setContentView(paramView, paramLayoutParams);
    }

    public void setInteractive(boolean paramBoolean)
    {
        this.mInteractive = paramBoolean;
    }

    class IDreamServiceWrapper extends IDreamService.Stub
    {
        public IDreamServiceWrapper()
        {
        }

        public void attach(IBinder paramIBinder)
        {
            Dream.this.attach(paramIBinder);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.dreams.Dream
 * JD-Core Version:        0.6.2
 */