package android.service.dreams;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IDreamManager extends IInterface
{
    public abstract void awaken()
        throws RemoteException;

    public abstract void dream()
        throws RemoteException;

    public abstract ComponentName getDreamComponent()
        throws RemoteException;

    public abstract void setDreamComponent(ComponentName paramComponentName)
        throws RemoteException;

    public abstract void testDream(ComponentName paramComponentName)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IDreamManager
    {
        private static final String DESCRIPTOR = "android.service.dreams.IDreamManager";
        static final int TRANSACTION_awaken = 2;
        static final int TRANSACTION_dream = 1;
        static final int TRANSACTION_getDreamComponent = 4;
        static final int TRANSACTION_setDreamComponent = 3;
        static final int TRANSACTION_testDream = 5;

        public Stub()
        {
            attachInterface(this, "android.service.dreams.IDreamManager");
        }

        public static IDreamManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.service.dreams.IDreamManager");
                if ((localIInterface != null) && ((localIInterface instanceof IDreamManager)))
                    localObject = (IDreamManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
                while (true)
                {
                    return i;
                    paramParcel2.writeString("android.service.dreams.IDreamManager");
                    continue;
                    paramParcel1.enforceInterface("android.service.dreams.IDreamManager");
                    dream();
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.service.dreams.IDreamManager");
                    awaken();
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.service.dreams.IDreamManager");
                    if (paramParcel1.readInt() != 0);
                    for (ComponentName localComponentName3 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName3 = null)
                    {
                        setDreamComponent(localComponentName3);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.service.dreams.IDreamManager");
                    ComponentName localComponentName2 = getDreamComponent();
                    paramParcel2.writeNoException();
                    if (localComponentName2 != null)
                    {
                        paramParcel2.writeInt(i);
                        localComponentName2.writeToParcel(paramParcel2, i);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                    }
                }
            case 5:
            }
            paramParcel1.enforceInterface("android.service.dreams.IDreamManager");
            if (paramParcel1.readInt() != 0);
            for (ComponentName localComponentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName1 = null)
            {
                testDream(localComponentName1);
                paramParcel2.writeNoException();
                break;
            }
        }

        private static class Proxy
            implements IDreamManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void awaken()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void dream()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ComponentName getDreamComponent()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localComponentName = (ComponentName)ComponentName.CREATOR.createFromParcel(localParcel2);
                        return localComponentName;
                    }
                    ComponentName localComponentName = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.service.dreams.IDreamManager";
            }

            public void setDreamComponent(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void testDream(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.service.dreams.IDreamManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.dreams.IDreamManager
 * JD-Core Version:        0.6.2
 */