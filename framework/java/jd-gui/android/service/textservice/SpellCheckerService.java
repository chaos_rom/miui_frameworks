package android.service.textservice;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.text.TextUtils;
import android.text.method.WordIterator;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import com.android.internal.textservice.ISpellCheckerService.Stub;
import com.android.internal.textservice.ISpellCheckerSession;
import com.android.internal.textservice.ISpellCheckerSession.Stub;
import com.android.internal.textservice.ISpellCheckerSessionListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;

public abstract class SpellCheckerService extends Service
{
    private static final boolean DBG = false;
    public static final String SERVICE_INTERFACE = "android.service.textservice.SpellCheckerService";
    private static final String TAG = SpellCheckerService.class.getSimpleName();
    private final SpellCheckerServiceBinder mBinder = new SpellCheckerServiceBinder(this);

    public abstract Session createSession();

    public final IBinder onBind(Intent paramIntent)
    {
        return this.mBinder;
    }

    private static class SentenceLevelAdapter
    {
        public static final SentenceSuggestionsInfo[] EMPTY_SENTENCE_SUGGESTIONS_INFOS = new SentenceSuggestionsInfo[0];
        private static final SuggestionsInfo EMPTY_SUGGESTIONS_INFO = new SuggestionsInfo(0, null);
        private final WordIterator mWordIterator;

        public SentenceLevelAdapter(Locale paramLocale)
        {
            this.mWordIterator = new WordIterator(paramLocale);
        }

        private SentenceTextInfoParams getSplitWords(TextInfo paramTextInfo)
        {
            WordIterator localWordIterator = this.mWordIterator;
            String str1 = paramTextInfo.getText();
            int i = paramTextInfo.getCookie();
            int j = str1.length();
            ArrayList localArrayList = new ArrayList();
            localWordIterator.setCharSequence(str1, 0, str1.length());
            int k = localWordIterator.following(0);
            for (int m = localWordIterator.getBeginning(k); ; m = localWordIterator.getBeginning(k))
                if ((m <= j) && (k != -1) && (m != -1))
                {
                    if ((k >= 0) && (k > m))
                    {
                        String str2 = str1.subSequence(m, k).toString();
                        localArrayList.add(new SentenceWordItem(new TextInfo(str2, i, str2.hashCode()), m, k));
                    }
                    k = localWordIterator.following(k);
                    if (k != -1);
                }
                else
                {
                    return new SentenceTextInfoParams(paramTextInfo, localArrayList);
                }
        }

        public static SentenceSuggestionsInfo reconstructSuggestions(SentenceTextInfoParams paramSentenceTextInfoParams, SuggestionsInfo[] paramArrayOfSuggestionsInfo)
        {
            SentenceSuggestionsInfo localSentenceSuggestionsInfo = null;
            if ((paramArrayOfSuggestionsInfo == null) || (paramArrayOfSuggestionsInfo.length == 0));
            while (true)
            {
                return localSentenceSuggestionsInfo;
                if (paramSentenceTextInfoParams != null)
                {
                    int i = paramSentenceTextInfoParams.mOriginalTextInfo.getCookie();
                    int j = paramSentenceTextInfoParams.mOriginalTextInfo.getSequence();
                    int k = paramSentenceTextInfoParams.mSize;
                    int[] arrayOfInt1 = new int[k];
                    int[] arrayOfInt2 = new int[k];
                    SuggestionsInfo[] arrayOfSuggestionsInfo = new SuggestionsInfo[k];
                    int m = 0;
                    if (m < k)
                    {
                        SentenceWordItem localSentenceWordItem = (SentenceWordItem)paramSentenceTextInfoParams.mItems.get(m);
                        Object localObject = null;
                        int n = 0;
                        label89: if (n < paramArrayOfSuggestionsInfo.length)
                        {
                            SuggestionsInfo localSuggestionsInfo = paramArrayOfSuggestionsInfo[n];
                            if ((localSuggestionsInfo != null) && (localSuggestionsInfo.getSequence() == localSentenceWordItem.mTextInfo.getSequence()))
                            {
                                localObject = localSuggestionsInfo;
                                ((SuggestionsInfo)localObject).setCookieAndSequence(i, j);
                            }
                        }
                        else
                        {
                            arrayOfInt1[m] = localSentenceWordItem.mStart;
                            arrayOfInt2[m] = localSentenceWordItem.mLength;
                            if (localObject == null)
                                break label179;
                        }
                        while (true)
                        {
                            arrayOfSuggestionsInfo[m] = localObject;
                            m++;
                            break;
                            n++;
                            break label89;
                            label179: localObject = EMPTY_SUGGESTIONS_INFO;
                        }
                    }
                    localSentenceSuggestionsInfo = new SentenceSuggestionsInfo(arrayOfSuggestionsInfo, arrayOfInt1, arrayOfInt2);
                }
            }
        }

        public static class SentenceTextInfoParams
        {
            final ArrayList<SpellCheckerService.SentenceLevelAdapter.SentenceWordItem> mItems;
            final TextInfo mOriginalTextInfo;
            final int mSize;

            public SentenceTextInfoParams(TextInfo paramTextInfo, ArrayList<SpellCheckerService.SentenceLevelAdapter.SentenceWordItem> paramArrayList)
            {
                this.mOriginalTextInfo = paramTextInfo;
                this.mItems = paramArrayList;
                this.mSize = paramArrayList.size();
            }
        }

        public static class SentenceWordItem
        {
            public final int mLength;
            public final int mStart;
            public final TextInfo mTextInfo;

            public SentenceWordItem(TextInfo paramTextInfo, int paramInt1, int paramInt2)
            {
                this.mTextInfo = paramTextInfo;
                this.mStart = paramInt1;
                this.mLength = (paramInt2 - paramInt1);
            }
        }
    }

    private static class SpellCheckerServiceBinder extends ISpellCheckerService.Stub
    {
        private final WeakReference<SpellCheckerService> mInternalServiceRef;

        public SpellCheckerServiceBinder(SpellCheckerService paramSpellCheckerService)
        {
            this.mInternalServiceRef = new WeakReference(paramSpellCheckerService);
        }

        public ISpellCheckerSession getISpellCheckerSession(String paramString, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle)
        {
            SpellCheckerService localSpellCheckerService = (SpellCheckerService)this.mInternalServiceRef.get();
            Object localObject;
            if (localSpellCheckerService == null)
                localObject = null;
            while (true)
            {
                return localObject;
                SpellCheckerService.Session localSession = localSpellCheckerService.createSession();
                localObject = new SpellCheckerService.InternalISpellCheckerSession(paramString, paramISpellCheckerSessionListener, paramBundle, localSession);
                localSession.onCreate();
            }
        }
    }

    private static class InternalISpellCheckerSession extends ISpellCheckerSession.Stub
    {
        private final Bundle mBundle;
        private ISpellCheckerSessionListener mListener;
        private final String mLocale;
        private final SpellCheckerService.Session mSession;

        public InternalISpellCheckerSession(String paramString, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle, SpellCheckerService.Session paramSession)
        {
            this.mListener = paramISpellCheckerSessionListener;
            this.mSession = paramSession;
            this.mLocale = paramString;
            this.mBundle = paramBundle;
            paramSession.setInternalISpellCheckerSession(this);
        }

        public Bundle getBundle()
        {
            return this.mBundle;
        }

        public String getLocale()
        {
            return this.mLocale;
        }

        public void onCancel()
        {
            int i = Process.getThreadPriority(Process.myTid());
            try
            {
                Process.setThreadPriority(10);
                this.mSession.onCancel();
                return;
            }
            finally
            {
                Process.setThreadPriority(i);
            }
        }

        public void onClose()
        {
            int i = Process.getThreadPriority(Process.myTid());
            try
            {
                Process.setThreadPriority(10);
                this.mSession.onClose();
                return;
            }
            finally
            {
                Process.setThreadPriority(i);
                this.mListener = null;
            }
        }

        public void onGetSentenceSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt)
        {
            try
            {
                this.mListener.onGetSentenceSuggestions(this.mSession.onGetSentenceSuggestionsMultiple(paramArrayOfTextInfo, paramInt));
                label18: return;
            }
            catch (RemoteException localRemoteException)
            {
                break label18;
            }
        }

        // ERROR //
        public void onGetSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt, boolean paramBoolean)
        {
            // Byte code:
            //     0: invokestatic 45	android/os/Process:myTid	()I
            //     3: invokestatic 49	android/os/Process:getThreadPriority	(I)I
            //     6: istore 4
            //     8: bipush 10
            //     10: invokestatic 53	android/os/Process:setThreadPriority	(I)V
            //     13: aload_0
            //     14: getfield 22	android/service/textservice/SpellCheckerService$InternalISpellCheckerSession:mListener	Lcom/android/internal/textservice/ISpellCheckerSessionListener;
            //     17: aload_0
            //     18: getfield 24	android/service/textservice/SpellCheckerService$InternalISpellCheckerSession:mSession	Landroid/service/textservice/SpellCheckerService$Session;
            //     21: aload_1
            //     22: iload_2
            //     23: iload_3
            //     24: invokevirtual 76	android/service/textservice/SpellCheckerService$Session:onGetSuggestionsMultiple	([Landroid/view/textservice/TextInfo;IZ)[Landroid/view/textservice/SuggestionsInfo;
            //     27: invokeinterface 80 2 0
            //     32: iload 4
            //     34: invokestatic 53	android/os/Process:setThreadPriority	(I)V
            //     37: return
            //     38: astore 6
            //     40: iload 4
            //     42: invokestatic 53	android/os/Process:setThreadPriority	(I)V
            //     45: aload 6
            //     47: athrow
            //     48: astore 5
            //     50: goto -18 -> 32
            //
            // Exception table:
            //     from	to	target	type
            //     8	32	38	finally
            //     8	32	48	android/os/RemoteException
        }
    }

    public static abstract class Session
    {
        private SpellCheckerService.InternalISpellCheckerSession mInternalSession;
        private volatile SpellCheckerService.SentenceLevelAdapter mSentenceLevelAdapter;

        public Bundle getBundle()
        {
            return this.mInternalSession.getBundle();
        }

        public String getLocale()
        {
            return this.mInternalSession.getLocale();
        }

        public void onCancel()
        {
        }

        public void onClose()
        {
        }

        public abstract void onCreate();

        public SentenceSuggestionsInfo[] onGetSentenceSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt)
        {
            SentenceSuggestionsInfo[] arrayOfSentenceSuggestionsInfo;
            if ((paramArrayOfTextInfo == null) || (paramArrayOfTextInfo.length == 0))
                arrayOfSentenceSuggestionsInfo = SpellCheckerService.SentenceLevelAdapter.EMPTY_SENTENCE_SUGGESTIONS_INFOS;
            while (true)
            {
                return arrayOfSentenceSuggestionsInfo;
                if (this.mSentenceLevelAdapter == null);
                try
                {
                    if (this.mSentenceLevelAdapter == null)
                    {
                        String str = getLocale();
                        if (!TextUtils.isEmpty(str))
                            this.mSentenceLevelAdapter = new SpellCheckerService.SentenceLevelAdapter(new Locale(str));
                    }
                    if (this.mSentenceLevelAdapter == null)
                    {
                        arrayOfSentenceSuggestionsInfo = SpellCheckerService.SentenceLevelAdapter.EMPTY_SENTENCE_SUGGESTIONS_INFOS;
                        continue;
                    }
                }
                finally
                {
                }
                int i = paramArrayOfTextInfo.length;
                arrayOfSentenceSuggestionsInfo = new SentenceSuggestionsInfo[i];
                for (int j = 0; j < i; j++)
                {
                    SpellCheckerService.SentenceLevelAdapter.SentenceTextInfoParams localSentenceTextInfoParams = this.mSentenceLevelAdapter.getSplitWords(paramArrayOfTextInfo[j]);
                    ArrayList localArrayList = localSentenceTextInfoParams.mItems;
                    int k = localArrayList.size();
                    TextInfo[] arrayOfTextInfo = new TextInfo[k];
                    for (int m = 0; m < k; m++)
                        arrayOfTextInfo[m] = ((SpellCheckerService.SentenceLevelAdapter.SentenceWordItem)localArrayList.get(m)).mTextInfo;
                    arrayOfSentenceSuggestionsInfo[j] = SpellCheckerService.SentenceLevelAdapter.reconstructSuggestions(localSentenceTextInfoParams, onGetSuggestionsMultiple(arrayOfTextInfo, paramInt, true));
                }
            }
        }

        public abstract SuggestionsInfo onGetSuggestions(TextInfo paramTextInfo, int paramInt);

        public SuggestionsInfo[] onGetSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt, boolean paramBoolean)
        {
            int i = paramArrayOfTextInfo.length;
            SuggestionsInfo[] arrayOfSuggestionsInfo = new SuggestionsInfo[i];
            for (int j = 0; j < i; j++)
            {
                arrayOfSuggestionsInfo[j] = onGetSuggestions(paramArrayOfTextInfo[j], paramInt);
                arrayOfSuggestionsInfo[j].setCookieAndSequence(paramArrayOfTextInfo[j].getCookie(), paramArrayOfTextInfo[j].getSequence());
            }
            return arrayOfSuggestionsInfo;
        }

        public final void setInternalISpellCheckerSession(SpellCheckerService.InternalISpellCheckerSession paramInternalISpellCheckerSession)
        {
            this.mInternalSession = paramInternalISpellCheckerSession;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.textservice.SpellCheckerService
 * JD-Core Version:        0.6.2
 */