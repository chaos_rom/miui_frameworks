package android.service.wallpaper;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;
import android.view.IWindowSession;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceHolder.Callback2;
import android.view.ViewRootImpl;
import android.view.WindowManager.LayoutParams;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.HandlerCaller.Callback;
import com.android.internal.view.BaseIWindow;
import com.android.internal.view.BaseSurfaceHolder;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public abstract class WallpaperService extends Service
{
    static final boolean DEBUG = false;
    private static final int DO_ATTACH = 10;
    private static final int DO_DETACH = 20;
    private static final int DO_SET_DESIRED_SIZE = 30;
    private static final int MSG_TOUCH_EVENT = 10040;
    private static final int MSG_UPDATE_SURFACE = 10000;
    private static final int MSG_VISIBILITY_CHANGED = 10010;
    private static final int MSG_WALLPAPER_COMMAND = 10025;
    private static final int MSG_WALLPAPER_OFFSETS = 10020;
    private static final int MSG_WINDOW_RESIZED = 10030;
    public static final String SERVICE_INTERFACE = "android.service.wallpaper.WallpaperService";
    public static final String SERVICE_META_DATA = "android.service.wallpaper";
    static final String TAG = "WallpaperService";
    private final ArrayList<Engine> mActiveEngines = new ArrayList();
    private Looper mCallbackLooper;

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.print("State of wallpaper ");
        paramPrintWriter.print(this);
        paramPrintWriter.println(":");
        for (int i = 0; i < this.mActiveEngines.size(); i++)
        {
            Engine localEngine = (Engine)this.mActiveEngines.get(i);
            paramPrintWriter.print("    Engine ");
            paramPrintWriter.print(localEngine);
            paramPrintWriter.println(":");
            localEngine.dump("        ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        }
    }

    public final IBinder onBind(Intent paramIntent)
    {
        return new IWallpaperServiceWrapper(this);
    }

    public void onCreate()
    {
        super.onCreate();
    }

    public abstract Engine onCreateEngine();

    public void onDestroy()
    {
        super.onDestroy();
        for (int i = 0; i < this.mActiveEngines.size(); i++)
            ((Engine)this.mActiveEngines.get(i)).detach();
        this.mActiveEngines.clear();
    }

    public void setCallbackLooper(Looper paramLooper)
    {
        this.mCallbackLooper = paramLooper;
    }

    class IWallpaperServiceWrapper extends IWallpaperService.Stub
    {
        private final WallpaperService mTarget;

        public IWallpaperServiceWrapper(WallpaperService arg2)
        {
            Object localObject;
            this.mTarget = localObject;
        }

        public void attach(IWallpaperConnection paramIWallpaperConnection, IBinder paramIBinder, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
        {
            new WallpaperService.IWallpaperEngineWrapper(WallpaperService.this, this.mTarget, paramIWallpaperConnection, paramIBinder, paramInt1, paramBoolean, paramInt2, paramInt3);
        }
    }

    class IWallpaperEngineWrapper extends IWallpaperEngine.Stub
        implements HandlerCaller.Callback
    {
        private final HandlerCaller mCaller;
        final IWallpaperConnection mConnection;
        WallpaperService.Engine mEngine;
        final boolean mIsPreview;
        int mReqHeight;
        int mReqWidth;
        final IBinder mWindowToken;
        final int mWindowType;

        IWallpaperEngineWrapper(WallpaperService paramIWallpaperConnection, IWallpaperConnection paramIBinder, IBinder paramInt1, int paramBoolean, boolean paramInt2, int paramInt3, int arg8)
        {
            if (WallpaperService.this.mCallbackLooper != null);
            for (Looper localLooper = WallpaperService.this.mCallbackLooper; ; localLooper = paramIWallpaperConnection.getMainLooper())
            {
                this.mCaller = new HandlerCaller(paramIWallpaperConnection, localLooper, this);
                this.mConnection = paramIBinder;
                this.mWindowToken = paramInt1;
                this.mWindowType = paramBoolean;
                this.mIsPreview = paramInt2;
                this.mReqWidth = paramInt3;
                int i;
                this.mReqHeight = i;
                Message localMessage = this.mCaller.obtainMessage(10);
                this.mCaller.sendMessage(localMessage);
                return;
            }
        }

        public void destroy()
        {
            Message localMessage = this.mCaller.obtainMessage(20);
            this.mCaller.sendMessage(localMessage);
        }

        public void dispatchPointer(MotionEvent paramMotionEvent)
        {
            if (this.mEngine != null)
                WallpaperService.Engine.access$000(this.mEngine, paramMotionEvent);
            while (true)
            {
                return;
                paramMotionEvent.recycle();
            }
        }

        public void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle)
        {
            if (this.mEngine != null)
                this.mEngine.mWindow.dispatchWallpaperCommand(paramString, paramInt1, paramInt2, paramInt3, paramBundle, false);
        }

        public void executeMessage(Message paramMessage)
        {
            boolean bool1 = true;
            switch (paramMessage.what)
            {
            default:
                Log.w("WallpaperService", "Unknown message type " + paramMessage.what);
            case 10:
            case 20:
            case 30:
            case 10000:
            case 10010:
            case 10020:
            case 10025:
            case 10030:
            case 10040:
            }
            while (true)
            {
                return;
                try
                {
                    this.mConnection.attachEngine(this);
                    WallpaperService.Engine localEngine2 = WallpaperService.this.onCreateEngine();
                    this.mEngine = localEngine2;
                    WallpaperService.this.mActiveEngines.add(localEngine2);
                    localEngine2.attach(this);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.w("WallpaperService", "Wallpaper host disappeared", localRemoteException);
                }
                continue;
                WallpaperService.this.mActiveEngines.remove(this.mEngine);
                this.mEngine.detach();
                continue;
                this.mEngine.doDesiredSizeChanged(paramMessage.arg1, paramMessage.arg2);
                continue;
                this.mEngine.updateSurface(bool1, false, false);
                continue;
                WallpaperService.Engine localEngine1 = this.mEngine;
                if (paramMessage.arg1 != 0);
                while (true)
                {
                    localEngine1.doVisibilityChanged(bool1);
                    break;
                    bool1 = false;
                }
                this.mEngine.doOffsetsChanged(bool1);
                continue;
                WallpaperService.WallpaperCommand localWallpaperCommand = (WallpaperService.WallpaperCommand)paramMessage.obj;
                this.mEngine.doCommand(localWallpaperCommand);
                continue;
                if (paramMessage.arg1 != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    this.mEngine.updateSurface(bool1, false, bool2);
                    this.mEngine.doOffsetsChanged(bool1);
                    break;
                }
                int i = 0;
                MotionEvent localMotionEvent = (MotionEvent)paramMessage.obj;
                if (localMotionEvent.getAction() == 2);
                synchronized (this.mEngine.mLock)
                {
                    if (this.mEngine.mPendingMove == localMotionEvent)
                    {
                        this.mEngine.mPendingMove = null;
                        if (i == 0)
                            this.mEngine.onTouchEvent(localMotionEvent);
                        localMotionEvent.recycle();
                        continue;
                    }
                    i = 1;
                }
            }
        }

        public void setDesiredSize(int paramInt1, int paramInt2)
        {
            Message localMessage = this.mCaller.obtainMessageII(30, paramInt1, paramInt2);
            this.mCaller.sendMessage(localMessage);
        }

        public void setVisibility(boolean paramBoolean)
        {
            HandlerCaller localHandlerCaller = this.mCaller;
            if (paramBoolean);
            for (int i = 1; ; i = 0)
            {
                Message localMessage = localHandlerCaller.obtainMessageI(10010, i);
                this.mCaller.sendMessage(localMessage);
                return;
            }
        }
    }

    public class Engine
    {
        HandlerCaller mCaller;
        final Configuration mConfiguration = new Configuration();
        IWallpaperConnection mConnection;
        final Rect mContentInsets = new Rect();
        boolean mCreated;
        int mCurHeight;
        int mCurWidth;
        int mCurWindowFlags = this.mWindowFlags;
        int mCurWindowPrivateFlags = this.mWindowPrivateFlags;
        boolean mDestroyed;
        boolean mDrawingAllowed;
        boolean mFixedSizeAllowed;
        int mFormat;
        int mHeight;
        WallpaperService.IWallpaperEngineWrapper mIWallpaperEngine;
        boolean mInitializing = true;
        InputChannel mInputChannel;
        WallpaperInputEventReceiver mInputEventReceiver;
        boolean mIsCreating;
        final WindowManager.LayoutParams mLayout = new WindowManager.LayoutParams();
        final Object mLock = new Object();
        boolean mOffsetMessageEnqueued;
        boolean mOffsetsChanged;
        MotionEvent mPendingMove;
        boolean mPendingSync;
        float mPendingXOffset;
        float mPendingXOffsetStep;
        float mPendingYOffset;
        float mPendingYOffsetStep;
        final BroadcastReceiver mReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                if ("android.intent.action.SCREEN_ON".equals(paramAnonymousIntent.getAction()))
                {
                    WallpaperService.Engine.this.mScreenOn = true;
                    WallpaperService.Engine.this.reportVisibility();
                }
                while (true)
                {
                    return;
                    if ("android.intent.action.SCREEN_OFF".equals(paramAnonymousIntent.getAction()))
                    {
                        WallpaperService.Engine.this.mScreenOn = false;
                        WallpaperService.Engine.this.reportVisibility();
                    }
                }
            }
        };
        boolean mReportedVisible;
        boolean mScreenOn = true;
        IWindowSession mSession;
        boolean mSurfaceCreated;
        final BaseSurfaceHolder mSurfaceHolder = new BaseSurfaceHolder()
        {
            public boolean isCreating()
            {
                return WallpaperService.Engine.this.mIsCreating;
            }

            public boolean onAllowLockCanvas()
            {
                return WallpaperService.Engine.this.mDrawingAllowed;
            }

            public void onRelayoutContainer()
            {
                Message localMessage = WallpaperService.Engine.this.mCaller.obtainMessage(10000);
                WallpaperService.Engine.this.mCaller.sendMessage(localMessage);
            }

            public void onUpdateSurface()
            {
                Message localMessage = WallpaperService.Engine.this.mCaller.obtainMessage(10000);
                WallpaperService.Engine.this.mCaller.sendMessage(localMessage);
            }

            public void setFixedSize(int paramAnonymousInt1, int paramAnonymousInt2)
            {
                if (!WallpaperService.Engine.this.mFixedSizeAllowed)
                    throw new UnsupportedOperationException("Wallpapers currently only support sizing from layout");
                super.setFixedSize(paramAnonymousInt1, paramAnonymousInt2);
            }

            public void setKeepScreenOn(boolean paramAnonymousBoolean)
            {
                throw new UnsupportedOperationException("Wallpapers do not support keep screen on");
            }
        };
        int mType;
        boolean mVisible;
        final Rect mVisibleInsets = new Rect();
        int mWidth;
        final Rect mWinFrame = new Rect();
        final BaseIWindow mWindow = new BaseIWindow()
        {
            public void dispatchAppVisibility(boolean paramAnonymousBoolean)
            {
                HandlerCaller localHandlerCaller;
                if (!WallpaperService.Engine.this.mIWallpaperEngine.mIsPreview)
                {
                    localHandlerCaller = WallpaperService.Engine.this.mCaller;
                    if (!paramAnonymousBoolean)
                        break label50;
                }
                label50: for (int i = 1; ; i = 0)
                {
                    Message localMessage = localHandlerCaller.obtainMessageI(10010, i);
                    WallpaperService.Engine.this.mCaller.sendMessage(localMessage);
                    return;
                }
            }

            public void dispatchWallpaperCommand(String paramAnonymousString, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, Bundle paramAnonymousBundle, boolean paramAnonymousBoolean)
            {
                synchronized (WallpaperService.Engine.this.mLock)
                {
                    WallpaperService.WallpaperCommand localWallpaperCommand = new WallpaperService.WallpaperCommand();
                    localWallpaperCommand.action = paramAnonymousString;
                    localWallpaperCommand.x = paramAnonymousInt1;
                    localWallpaperCommand.y = paramAnonymousInt2;
                    localWallpaperCommand.z = paramAnonymousInt3;
                    localWallpaperCommand.extras = paramAnonymousBundle;
                    localWallpaperCommand.sync = paramAnonymousBoolean;
                    Message localMessage = WallpaperService.Engine.this.mCaller.obtainMessage(10025);
                    localMessage.obj = localWallpaperCommand;
                    WallpaperService.Engine.this.mCaller.sendMessage(localMessage);
                    return;
                }
            }

            public void dispatchWallpaperOffsets(float paramAnonymousFloat1, float paramAnonymousFloat2, float paramAnonymousFloat3, float paramAnonymousFloat4, boolean paramAnonymousBoolean)
            {
                synchronized (WallpaperService.Engine.this.mLock)
                {
                    WallpaperService.Engine.this.mPendingXOffset = paramAnonymousFloat1;
                    WallpaperService.Engine.this.mPendingYOffset = paramAnonymousFloat2;
                    WallpaperService.Engine.this.mPendingXOffsetStep = paramAnonymousFloat3;
                    WallpaperService.Engine.this.mPendingYOffsetStep = paramAnonymousFloat4;
                    if (paramAnonymousBoolean)
                        WallpaperService.Engine.this.mPendingSync = true;
                    if (!WallpaperService.Engine.this.mOffsetMessageEnqueued)
                    {
                        WallpaperService.Engine.this.mOffsetMessageEnqueued = true;
                        Message localMessage = WallpaperService.Engine.this.mCaller.obtainMessage(10020);
                        WallpaperService.Engine.this.mCaller.sendMessage(localMessage);
                    }
                    return;
                }
            }

            public void resized(int paramAnonymousInt1, int paramAnonymousInt2, Rect paramAnonymousRect1, Rect paramAnonymousRect2, boolean paramAnonymousBoolean, Configuration paramAnonymousConfiguration)
            {
                HandlerCaller localHandlerCaller = WallpaperService.Engine.this.mCaller;
                if (paramAnonymousBoolean);
                for (int i = 1; ; i = 0)
                {
                    Message localMessage = localHandlerCaller.obtainMessageI(10030, i);
                    WallpaperService.Engine.this.mCaller.sendMessage(localMessage);
                    return;
                }
            }
        };
        int mWindowFlags = 16;
        int mWindowPrivateFlags = 4;
        IBinder mWindowToken;

        public Engine()
        {
        }

        private void dispatchPointer(MotionEvent paramMotionEvent)
        {
            if (paramMotionEvent.isTouchEvent());
            while (true)
            {
                synchronized (this.mLock)
                {
                    if (paramMotionEvent.getAction() == 2)
                    {
                        this.mPendingMove = paramMotionEvent;
                        Message localMessage = this.mCaller.obtainMessageO(10040, paramMotionEvent);
                        this.mCaller.sendMessage(localMessage);
                        return;
                    }
                    this.mPendingMove = null;
                }
                paramMotionEvent.recycle();
            }
        }

        void attach(WallpaperService.IWallpaperEngineWrapper paramIWallpaperEngineWrapper)
        {
            if (this.mDestroyed);
            while (true)
            {
                return;
                this.mIWallpaperEngine = paramIWallpaperEngineWrapper;
                this.mCaller = paramIWallpaperEngineWrapper.mCaller;
                this.mConnection = paramIWallpaperEngineWrapper.mConnection;
                this.mWindowToken = paramIWallpaperEngineWrapper.mWindowToken;
                this.mSurfaceHolder.setSizeFromLayout();
                this.mInitializing = true;
                this.mSession = ViewRootImpl.getWindowSession(WallpaperService.this.getMainLooper());
                this.mWindow.setSession(this.mSession);
                this.mScreenOn = ((PowerManager)WallpaperService.this.getSystemService("power")).isScreenOn();
                IntentFilter localIntentFilter = new IntentFilter();
                localIntentFilter.addAction("android.intent.action.SCREEN_ON");
                localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
                WallpaperService.this.registerReceiver(this.mReceiver, localIntentFilter);
                onCreate(this.mSurfaceHolder);
                this.mInitializing = false;
                this.mReportedVisible = false;
                updateSurface(false, false, false);
            }
        }

        void detach()
        {
            if (this.mDestroyed);
            while (true)
            {
                return;
                this.mDestroyed = true;
                if (this.mVisible)
                {
                    this.mVisible = false;
                    onVisibilityChanged(false);
                }
                reportSurfaceDestroyed();
                onDestroy();
                WallpaperService.this.unregisterReceiver(this.mReceiver);
                if (!this.mCreated)
                    continue;
                try
                {
                    if (this.mInputEventReceiver != null)
                    {
                        this.mInputEventReceiver.dispose();
                        this.mInputEventReceiver = null;
                    }
                    this.mSession.remove(this.mWindow);
                    label88: this.mSurfaceHolder.mSurface.release();
                    this.mCreated = false;
                    if (this.mInputChannel == null)
                        continue;
                    this.mInputChannel.dispose();
                    this.mInputChannel = null;
                }
                catch (RemoteException localRemoteException)
                {
                    break label88;
                }
            }
        }

        void doCommand(WallpaperService.WallpaperCommand paramWallpaperCommand)
        {
            Bundle localBundle;
            if (!this.mDestroyed)
                localBundle = onCommand(paramWallpaperCommand.action, paramWallpaperCommand.x, paramWallpaperCommand.y, paramWallpaperCommand.z, paramWallpaperCommand.extras, paramWallpaperCommand.sync);
            while (true)
            {
                if (paramWallpaperCommand.sync);
                try
                {
                    this.mSession.wallpaperCommandComplete(this.mWindow.asBinder(), localBundle);
                    label60: return;
                    localBundle = null;
                }
                catch (RemoteException localRemoteException)
                {
                    break label60;
                }
            }
        }

        void doDesiredSizeChanged(int paramInt1, int paramInt2)
        {
            if (!this.mDestroyed)
            {
                this.mIWallpaperEngine.mReqWidth = paramInt1;
                this.mIWallpaperEngine.mReqHeight = paramInt2;
                onDesiredSizeChanged(paramInt1, paramInt2);
                doOffsetsChanged(true);
            }
        }

        void doOffsetsChanged(boolean paramBoolean)
        {
            if (this.mDestroyed);
            while ((!paramBoolean) && (!this.mOffsetsChanged))
                return;
            while (true)
            {
                synchronized (this.mLock)
                {
                    while (true)
                    {
                        float f1 = this.mPendingXOffset;
                        float f2 = this.mPendingYOffset;
                        float f3 = this.mPendingXOffsetStep;
                        float f4 = this.mPendingYOffsetStep;
                        boolean bool = this.mPendingSync;
                        this.mPendingSync = false;
                        this.mOffsetMessageEnqueued = false;
                        if (this.mSurfaceCreated)
                        {
                            if (!this.mReportedVisible)
                                break label210;
                            int i = this.mIWallpaperEngine.mReqWidth - this.mCurWidth;
                            if (i > 0)
                            {
                                j = -(int)(0.5F + f1 * i);
                                int k = this.mIWallpaperEngine.mReqHeight - this.mCurHeight;
                                if (k <= 0)
                                    break label204;
                                m = -(int)(0.5F + f2 * k);
                                onOffsetsChanged(f1, f2, f3, f4, j, m);
                            }
                        }
                        else
                        {
                            if (!bool)
                                break;
                            try
                            {
                                this.mSession.wallpaperOffsetsComplete(this.mWindow.asBinder());
                            }
                            catch (RemoteException localRemoteException)
                            {
                            }
                        }
                    }
                }
                int j = 0;
                continue;
                label204: int m = 0;
                continue;
                label210: this.mOffsetsChanged = true;
            }
        }

        void doVisibilityChanged(boolean paramBoolean)
        {
            if (!this.mDestroyed)
            {
                this.mVisible = paramBoolean;
                reportVisibility();
            }
        }

        protected void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mInitializing=");
            paramPrintWriter.print(this.mInitializing);
            paramPrintWriter.print(" mDestroyed=");
            paramPrintWriter.println(this.mDestroyed);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mVisible=");
            paramPrintWriter.print(this.mVisible);
            paramPrintWriter.print(" mScreenOn=");
            paramPrintWriter.print(this.mScreenOn);
            paramPrintWriter.print(" mReportedVisible=");
            paramPrintWriter.println(this.mReportedVisible);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mCreated=");
            paramPrintWriter.print(this.mCreated);
            paramPrintWriter.print(" mSurfaceCreated=");
            paramPrintWriter.print(this.mSurfaceCreated);
            paramPrintWriter.print(" mIsCreating=");
            paramPrintWriter.print(this.mIsCreating);
            paramPrintWriter.print(" mDrawingAllowed=");
            paramPrintWriter.println(this.mDrawingAllowed);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mWidth=");
            paramPrintWriter.print(this.mWidth);
            paramPrintWriter.print(" mCurWidth=");
            paramPrintWriter.print(this.mCurWidth);
            paramPrintWriter.print(" mHeight=");
            paramPrintWriter.print(this.mHeight);
            paramPrintWriter.print(" mCurHeight=");
            paramPrintWriter.println(this.mCurHeight);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mType=");
            paramPrintWriter.print(this.mType);
            paramPrintWriter.print(" mWindowFlags=");
            paramPrintWriter.print(this.mWindowFlags);
            paramPrintWriter.print(" mCurWindowFlags=");
            paramPrintWriter.println(this.mCurWindowFlags);
            paramPrintWriter.print(" mWindowPrivateFlags=");
            paramPrintWriter.print(this.mWindowPrivateFlags);
            paramPrintWriter.print(" mCurWindowPrivateFlags=");
            paramPrintWriter.println(this.mCurWindowPrivateFlags);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mVisibleInsets=");
            paramPrintWriter.print(this.mVisibleInsets.toShortString());
            paramPrintWriter.print(" mWinFrame=");
            paramPrintWriter.print(this.mWinFrame.toShortString());
            paramPrintWriter.print(" mContentInsets=");
            paramPrintWriter.println(this.mContentInsets.toShortString());
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mConfiguration=");
            paramPrintWriter.println(this.mConfiguration);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mLayout=");
            paramPrintWriter.println(this.mLayout);
            synchronized (this.mLock)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mPendingXOffset=");
                paramPrintWriter.print(this.mPendingXOffset);
                paramPrintWriter.print(" mPendingXOffset=");
                paramPrintWriter.println(this.mPendingXOffset);
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mPendingXOffsetStep=");
                paramPrintWriter.print(this.mPendingXOffsetStep);
                paramPrintWriter.print(" mPendingXOffsetStep=");
                paramPrintWriter.println(this.mPendingXOffsetStep);
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mOffsetMessageEnqueued=");
                paramPrintWriter.print(this.mOffsetMessageEnqueued);
                paramPrintWriter.print(" mPendingSync=");
                paramPrintWriter.println(this.mPendingSync);
                if (this.mPendingMove != null)
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("mPendingMove=");
                    paramPrintWriter.println(this.mPendingMove);
                }
                return;
            }
        }

        public int getDesiredMinimumHeight()
        {
            return this.mIWallpaperEngine.mReqHeight;
        }

        public int getDesiredMinimumWidth()
        {
            return this.mIWallpaperEngine.mReqWidth;
        }

        public SurfaceHolder getSurfaceHolder()
        {
            return this.mSurfaceHolder;
        }

        public boolean isPreview()
        {
            return this.mIWallpaperEngine.mIsPreview;
        }

        public boolean isVisible()
        {
            return this.mReportedVisible;
        }

        public Bundle onCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean)
        {
            return null;
        }

        public void onCreate(SurfaceHolder paramSurfaceHolder)
        {
        }

        public void onDesiredSizeChanged(int paramInt1, int paramInt2)
        {
        }

        public void onDestroy()
        {
        }

        public void onOffsetsChanged(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt1, int paramInt2)
        {
        }

        public void onSurfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
        {
        }

        public void onSurfaceCreated(SurfaceHolder paramSurfaceHolder)
        {
        }

        public void onSurfaceDestroyed(SurfaceHolder paramSurfaceHolder)
        {
        }

        public void onSurfaceRedrawNeeded(SurfaceHolder paramSurfaceHolder)
        {
        }

        public void onTouchEvent(MotionEvent paramMotionEvent)
        {
        }

        public void onVisibilityChanged(boolean paramBoolean)
        {
        }

        void reportSurfaceDestroyed()
        {
            if (this.mSurfaceCreated)
            {
                this.mSurfaceCreated = false;
                this.mSurfaceHolder.ungetCallbacks();
                SurfaceHolder.Callback[] arrayOfCallback = this.mSurfaceHolder.getCallbacks();
                if (arrayOfCallback != null)
                {
                    int i = arrayOfCallback.length;
                    for (int j = 0; j < i; j++)
                        arrayOfCallback[j].surfaceDestroyed(this.mSurfaceHolder);
                }
                onSurfaceDestroyed(this.mSurfaceHolder);
            }
        }

        void reportVisibility()
        {
            if (!this.mDestroyed)
                if ((!this.mVisible) || (!this.mScreenOn))
                    break label58;
            label58: for (boolean bool = true; ; bool = false)
            {
                if (this.mReportedVisible != bool)
                {
                    this.mReportedVisible = bool;
                    if (bool)
                    {
                        doOffsetsChanged(false);
                        updateSurface(false, false, false);
                    }
                    onVisibilityChanged(bool);
                }
                return;
            }
        }

        public void setFixedSizeAllowed(boolean paramBoolean)
        {
            this.mFixedSizeAllowed = paramBoolean;
        }

        public void setOffsetNotificationsEnabled(boolean paramBoolean)
        {
            if (paramBoolean);
            for (int i = 0x4 | this.mWindowPrivateFlags; ; i = 0xFFFFFFFB & this.mWindowPrivateFlags)
            {
                this.mWindowPrivateFlags = i;
                if (this.mCreated)
                    updateSurface(false, false, false);
                return;
            }
        }

        public void setTouchEventsEnabled(boolean paramBoolean)
        {
            if (paramBoolean);
            for (int i = 0xFFFFFFEF & this.mWindowFlags; ; i = 0x10 | this.mWindowFlags)
            {
                this.mWindowFlags = i;
                if (this.mCreated)
                    updateSurface(false, false, false);
                return;
            }
        }

        void updateSurface(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
        {
            if (this.mDestroyed)
                Log.w("WallpaperService", "Ignoring updateSurface: destroyed");
            int i = this.mSurfaceHolder.getRequestedWidth();
            if (i <= 0)
                i = -1;
            int j = this.mSurfaceHolder.getRequestedHeight();
            if (j <= 0)
                j = -1;
            int k;
            int m;
            int n;
            label90: int i1;
            label111: int i2;
            label128: int i3;
            label153: int i4;
            int i7;
            if (!this.mCreated)
            {
                k = 1;
                if (this.mSurfaceCreated)
                    break label991;
                m = 1;
                if (this.mFormat == this.mSurfaceHolder.getRequestedFormat())
                    break label997;
                n = 1;
                if ((this.mWidth == i) && (this.mHeight == j))
                    break label1003;
                i1 = 1;
                if (this.mType == this.mSurfaceHolder.getRequestedType())
                    break label1009;
                i2 = 1;
                if ((this.mCurWindowFlags == this.mWindowFlags) && (this.mCurWindowPrivateFlags == this.mWindowPrivateFlags))
                    break label1015;
                i3 = 1;
                if ((paramBoolean1) || (k != 0) || (m != 0) || (n != 0) || (i1 != 0) || (i2 != 0) || (i3 != 0) || (paramBoolean3))
                {
                    try
                    {
                        this.mWidth = i;
                        this.mHeight = j;
                        this.mFormat = this.mSurfaceHolder.getRequestedFormat();
                        this.mType = this.mSurfaceHolder.getRequestedType();
                        this.mLayout.x = 0;
                        this.mLayout.y = 0;
                        this.mLayout.width = i;
                        this.mLayout.height = j;
                        this.mLayout.format = this.mFormat;
                        this.mCurWindowFlags = this.mWindowFlags;
                        this.mLayout.flags = (0x8 | (0x100 | (0x200 | this.mWindowFlags)));
                        this.mCurWindowPrivateFlags = this.mWindowPrivateFlags;
                        this.mLayout.privateFlags = this.mWindowPrivateFlags;
                        this.mLayout.memoryType = this.mType;
                        this.mLayout.token = this.mWindowToken;
                        if (!this.mCreated)
                        {
                            this.mLayout.type = this.mIWallpaperEngine.mWindowType;
                            this.mLayout.gravity = 51;
                            this.mLayout.setTitle(WallpaperService.this.getClass().getName());
                            this.mLayout.windowAnimations = 16974312;
                            this.mInputChannel = new InputChannel();
                            if (this.mSession.add(this.mWindow, this.mWindow.mSeq, this.mLayout, 0, this.mContentInsets, this.mInputChannel) < 0)
                            {
                                Log.w("WallpaperService", "Failed to add window while updating wallpaper surface.");
                            }
                            else
                            {
                                this.mCreated = true;
                                this.mInputEventReceiver = new WallpaperInputEventReceiver(this.mInputChannel, Looper.myLooper());
                            }
                        }
                        else
                        {
                            this.mSurfaceHolder.mSurfaceLock.lock();
                            this.mDrawingAllowed = true;
                            i4 = this.mSession.relayout(this.mWindow, this.mWindow.mSeq, this.mLayout, this.mWidth, this.mHeight, 0, 0, this.mWinFrame, this.mContentInsets, this.mVisibleInsets, this.mConfiguration, this.mSurfaceHolder.mSurface);
                            int i5 = this.mWinFrame.width();
                            if (this.mCurWidth != i5)
                            {
                                i1 = 1;
                                this.mCurWidth = i5;
                            }
                            int i6 = this.mWinFrame.height();
                            if (this.mCurHeight != i6)
                            {
                                i1 = 1;
                                this.mCurHeight = i6;
                            }
                            this.mSurfaceHolder.setSurfaceFrameSize(i5, i6);
                            this.mSurfaceHolder.mSurfaceLock.unlock();
                            if (!this.mSurfaceHolder.mSurface.isValid())
                                reportSurfaceDestroyed();
                        }
                    }
                    catch (RemoteException localRemoteException)
                    {
                    }
                    i7 = 0;
                }
            }
            while (true)
            {
                label820: int i9;
                label991: 
                try
                {
                    this.mSurfaceHolder.ungetCallbacks();
                    if (m == 0)
                        break label1021;
                    this.mIsCreating = true;
                    i7 = 1;
                    onSurfaceCreated(this.mSurfaceHolder);
                    SurfaceHolder.Callback[] arrayOfCallback3 = this.mSurfaceHolder.getCallbacks();
                    if (arrayOfCallback3 == null)
                        break label1021;
                    int i12 = arrayOfCallback3.length;
                    int i13 = 0;
                    if (i13 >= i12)
                        break label1021;
                    arrayOfCallback3[i13].surfaceCreated(this.mSurfaceHolder);
                    i13++;
                    continue;
                    i7 = 1;
                    onSurfaceChanged(this.mSurfaceHolder, this.mFormat, this.mCurWidth, this.mCurHeight);
                    SurfaceHolder.Callback[] arrayOfCallback1 = this.mSurfaceHolder.getCallbacks();
                    if (arrayOfCallback1 != null)
                    {
                        int i10 = arrayOfCallback1.length;
                        int i11 = 0;
                        if (i11 < i10)
                        {
                            arrayOfCallback1[i11].surfaceChanged(this.mSurfaceHolder, this.mFormat, this.mCurWidth, this.mCurHeight);
                            i11++;
                            continue;
                        }
                    }
                    if (paramBoolean3)
                    {
                        onSurfaceRedrawNeeded(this.mSurfaceHolder);
                        SurfaceHolder.Callback[] arrayOfCallback2 = this.mSurfaceHolder.getCallbacks();
                        if (arrayOfCallback2 != null)
                        {
                            int i8 = arrayOfCallback2.length;
                            i9 = 0;
                            if (i9 < i8)
                            {
                                SurfaceHolder.Callback localCallback = arrayOfCallback2[i9];
                                if (!(localCallback instanceof SurfaceHolder.Callback2))
                                    break label1074;
                                ((SurfaceHolder.Callback2)localCallback).surfaceRedrawNeeded(this.mSurfaceHolder);
                                break label1074;
                            }
                        }
                    }
                    if ((i7 != 0) && (!this.mReportedVisible))
                    {
                        if (this.mIsCreating)
                            onVisibilityChanged(true);
                        onVisibilityChanged(false);
                    }
                    this.mIsCreating = false;
                    this.mSurfaceCreated = true;
                    if (paramBoolean3)
                        this.mSession.finishDrawing(this.mWindow);
                }
                finally
                {
                    this.mIsCreating = false;
                    this.mSurfaceCreated = true;
                    if (paramBoolean3)
                        this.mSession.finishDrawing(this.mWindow);
                }
                break;
                label997: n = 0;
                break label90;
                label1003: i1 = 0;
                break label111;
                label1009: i2 = 0;
                break label128;
                label1015: i3 = 0;
                break label153;
                label1021: if ((k != 0) || ((i4 & 0x2) != 0));
                for (boolean bool = true; ; bool = false)
                {
                    paramBoolean3 |= bool;
                    if ((paramBoolean2) || (k != 0) || (m != 0) || (n != 0))
                        break;
                    if (i1 == 0)
                        break label820;
                    break;
                }
                label1074: i9++;
            }
        }

        final class WallpaperInputEventReceiver extends InputEventReceiver
        {
            public WallpaperInputEventReceiver(InputChannel paramLooper, Looper arg3)
            {
                super(localLooper);
            }

            public void onInputEvent(InputEvent paramInputEvent)
            {
                boolean bool = false;
                try
                {
                    if (((paramInputEvent instanceof MotionEvent)) && ((0x2 & paramInputEvent.getSource()) != 0))
                    {
                        MotionEvent localMotionEvent = MotionEvent.obtainNoHistory((MotionEvent)paramInputEvent);
                        WallpaperService.Engine.this.dispatchPointer(localMotionEvent);
                        bool = true;
                    }
                    finishInputEvent(paramInputEvent, bool);
                    return;
                }
                finally
                {
                    finishInputEvent(paramInputEvent, false);
                }
            }
        }
    }

    static final class WallpaperCommand
    {
        String action;
        Bundle extras;
        boolean sync;
        int x;
        int y;
        int z;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.wallpaper.WallpaperService
 * JD-Core Version:        0.6.2
 */