package android.service.wallpaper;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IWallpaperConnection extends IInterface
{
    public abstract void attachEngine(IWallpaperEngine paramIWallpaperEngine)
        throws RemoteException;

    public abstract ParcelFileDescriptor setWallpaper(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWallpaperConnection
    {
        private static final String DESCRIPTOR = "android.service.wallpaper.IWallpaperConnection";
        static final int TRANSACTION_attachEngine = 1;
        static final int TRANSACTION_setWallpaper = 2;

        public Stub()
        {
            attachInterface(this, "android.service.wallpaper.IWallpaperConnection");
        }

        public static IWallpaperConnection asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.service.wallpaper.IWallpaperConnection");
                if ((localIInterface != null) && ((localIInterface instanceof IWallpaperConnection)))
                    localObject = (IWallpaperConnection)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.service.wallpaper.IWallpaperConnection");
                continue;
                paramParcel1.enforceInterface("android.service.wallpaper.IWallpaperConnection");
                attachEngine(IWallpaperEngine.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.service.wallpaper.IWallpaperConnection");
                ParcelFileDescriptor localParcelFileDescriptor = setWallpaper(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localParcelFileDescriptor != null)
                {
                    paramParcel2.writeInt(i);
                    localParcelFileDescriptor.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                }
            }
        }

        private static class Proxy
            implements IWallpaperConnection
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void attachEngine(IWallpaperEngine paramIWallpaperEngine)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.service.wallpaper.IWallpaperConnection");
                    if (paramIWallpaperEngine != null)
                    {
                        localIBinder = paramIWallpaperEngine.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.service.wallpaper.IWallpaperConnection";
            }

            public ParcelFileDescriptor setWallpaper(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.service.wallpaper.IWallpaperConnection");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
                        return localParcelFileDescriptor;
                    }
                    ParcelFileDescriptor localParcelFileDescriptor = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.wallpaper.IWallpaperConnection
 * JD-Core Version:        0.6.2
 */