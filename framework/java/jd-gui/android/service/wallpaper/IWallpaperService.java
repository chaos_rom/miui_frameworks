package android.service.wallpaper;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IWallpaperService extends IInterface
{
    public abstract void attach(IWallpaperConnection paramIWallpaperConnection, IBinder paramIBinder, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWallpaperService
    {
        private static final String DESCRIPTOR = "android.service.wallpaper.IWallpaperService";
        static final int TRANSACTION_attach = 1;

        public Stub()
        {
            attachInterface(this, "android.service.wallpaper.IWallpaperService");
        }

        public static IWallpaperService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.service.wallpaper.IWallpaperService");
                if ((localIInterface != null) && ((localIInterface instanceof IWallpaperService)))
                    localObject = (IWallpaperService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.service.wallpaper.IWallpaperService");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.service.wallpaper.IWallpaperService");
            IWallpaperConnection localIWallpaperConnection = IWallpaperConnection.Stub.asInterface(paramParcel1.readStrongBinder());
            IBinder localIBinder = paramParcel1.readStrongBinder();
            int i = paramParcel1.readInt();
            if (paramParcel1.readInt() != 0);
            for (boolean bool2 = bool1; ; bool2 = false)
            {
                attach(localIWallpaperConnection, localIBinder, i, bool2, paramParcel1.readInt(), paramParcel1.readInt());
                break;
            }
        }

        private static class Proxy
            implements IWallpaperService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void attach(IWallpaperConnection paramIWallpaperConnection, IBinder paramIBinder, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
                throws RemoteException
            {
                IBinder localIBinder = null;
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.service.wallpaper.IWallpaperService");
                    if (paramIWallpaperConnection != null)
                        localIBinder = paramIWallpaperConnection.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    localParcel.writeStrongBinder(paramIBinder);
                    localParcel.writeInt(paramInt1);
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        localParcel.writeInt(paramInt2);
                        localParcel.writeInt(paramInt3);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.service.wallpaper.IWallpaperService";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.wallpaper.IWallpaperService
 * JD-Core Version:        0.6.2
 */