package android.service.wallpaper;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.view.MotionEvent;

public abstract interface IWallpaperEngine extends IInterface
{
    public abstract void destroy()
        throws RemoteException;

    public abstract void dispatchPointer(MotionEvent paramMotionEvent)
        throws RemoteException;

    public abstract void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle)
        throws RemoteException;

    public abstract void setDesiredSize(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setVisibility(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWallpaperEngine
    {
        private static final String DESCRIPTOR = "android.service.wallpaper.IWallpaperEngine";
        static final int TRANSACTION_destroy = 5;
        static final int TRANSACTION_dispatchPointer = 3;
        static final int TRANSACTION_dispatchWallpaperCommand = 4;
        static final int TRANSACTION_setDesiredSize = 1;
        static final int TRANSACTION_setVisibility = 2;

        public Stub()
        {
            attachInterface(this, "android.service.wallpaper.IWallpaperEngine");
        }

        public static IWallpaperEngine asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.service.wallpaper.IWallpaperEngine");
                if ((localIInterface != null) && ((localIInterface instanceof IWallpaperEngine)))
                    localObject = (IWallpaperEngine)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("android.service.wallpaper.IWallpaperEngine");
                continue;
                paramParcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
                setDesiredSize(paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    setVisibility(bool2);
                    break;
                }
                paramParcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
                if (paramParcel1.readInt() != 0);
                for (MotionEvent localMotionEvent = (MotionEvent)MotionEvent.CREATOR.createFromParcel(paramParcel1); ; localMotionEvent = null)
                {
                    dispatchPointer(localMotionEvent);
                    break;
                }
                paramParcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
                String str = paramParcel1.readString();
                int i = paramParcel1.readInt();
                int j = paramParcel1.readInt();
                int k = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                {
                    dispatchWallpaperCommand(str, i, j, k, localBundle);
                    break;
                }
                paramParcel1.enforceInterface("android.service.wallpaper.IWallpaperEngine");
                destroy();
            }
        }

        private static class Proxy
            implements IWallpaperEngine
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void destroy()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
                    this.mRemote.transact(5, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchPointer(MotionEvent paramMotionEvent)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
                    if (paramMotionEvent != null)
                    {
                        localParcel.writeInt(1);
                        paramMotionEvent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
                    localParcel.writeString(paramString);
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.service.wallpaper.IWallpaperEngine";
            }

            public void setDesiredSize(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setVisibility(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.service.wallpaper.IWallpaperEngine");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.wallpaper.IWallpaperEngine
 * JD-Core Version:        0.6.2
 */