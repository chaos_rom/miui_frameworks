package android.service.wallpaper;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class WallpaperSettingsActivity extends PreferenceActivity
{
    public static final String EXTRA_PREVIEW_MODE = "android.service.wallpaper.PREVIEW_MODE";

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.service.wallpaper.WallpaperSettingsActivity
 * JD-Core Version:        0.6.2
 */