package android.inputmethodservice;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.InputMethodSession;
import android.view.inputmethod.InputMethodSession.EventCallback;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.HandlerCaller.Callback;
import com.android.internal.os.HandlerCaller.SomeArgs;
import com.android.internal.view.IInputMethodCallback;
import com.android.internal.view.IInputMethodSession.Stub;

class IInputMethodSessionWrapper extends IInputMethodSession.Stub
    implements HandlerCaller.Callback
{
    private static final boolean DEBUG = false;
    private static final int DO_APP_PRIVATE_COMMAND = 100;
    private static final int DO_DISPATCH_KEY_EVENT = 70;
    private static final int DO_DISPATCH_TRACKBALL_EVENT = 80;
    private static final int DO_DISPLAY_COMPLETIONS = 65;
    private static final int DO_FINISH_INPUT = 60;
    private static final int DO_FINISH_SESSION = 110;
    private static final int DO_TOGGLE_SOFT_INPUT = 105;
    private static final int DO_UPDATE_CURSOR = 95;
    private static final int DO_UPDATE_EXTRACTED_TEXT = 67;
    private static final int DO_UPDATE_SELECTION = 90;
    private static final int DO_VIEW_CLICKED = 115;
    private static final String TAG = "InputMethodWrapper";
    HandlerCaller mCaller = new HandlerCaller(paramContext, this);
    InputMethodSession mInputMethodSession;

    public IInputMethodSessionWrapper(Context paramContext, InputMethodSession paramInputMethodSession)
    {
        this.mInputMethodSession = paramInputMethodSession;
    }

    public void appPrivateCommand(String paramString, Bundle paramBundle)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageOO(100, paramString, paramBundle));
    }

    public void dispatchKeyEvent(int paramInt, KeyEvent paramKeyEvent, IInputMethodCallback paramIInputMethodCallback)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageIOO(70, paramInt, paramKeyEvent, paramIInputMethodCallback));
    }

    public void dispatchTrackballEvent(int paramInt, MotionEvent paramMotionEvent, IInputMethodCallback paramIInputMethodCallback)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageIOO(80, paramInt, paramMotionEvent, paramIInputMethodCallback));
    }

    public void displayCompletions(CompletionInfo[] paramArrayOfCompletionInfo)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageO(65, paramArrayOfCompletionInfo));
    }

    public void executeMessage(Message paramMessage)
    {
        int i = 1;
        if (this.mInputMethodSession == null);
        while (true)
        {
            return;
            switch (paramMessage.what)
            {
            default:
                Log.w("InputMethodWrapper", "Unhandled message code: " + paramMessage.what);
                break;
            case 60:
                this.mInputMethodSession.finishInput();
                break;
            case 65:
                this.mInputMethodSession.displayCompletions((CompletionInfo[])paramMessage.obj);
                break;
            case 67:
                this.mInputMethodSession.updateExtractedText(paramMessage.arg1, (ExtractedText)paramMessage.obj);
                break;
            case 70:
                HandlerCaller.SomeArgs localSomeArgs4 = (HandlerCaller.SomeArgs)paramMessage.obj;
                this.mInputMethodSession.dispatchKeyEvent(paramMessage.arg1, (KeyEvent)localSomeArgs4.arg1, new InputMethodEventCallbackWrapper((IInputMethodCallback)localSomeArgs4.arg2));
                this.mCaller.recycleArgs(localSomeArgs4);
                break;
            case 80:
                HandlerCaller.SomeArgs localSomeArgs3 = (HandlerCaller.SomeArgs)paramMessage.obj;
                this.mInputMethodSession.dispatchTrackballEvent(paramMessage.arg1, (MotionEvent)localSomeArgs3.arg1, new InputMethodEventCallbackWrapper((IInputMethodCallback)localSomeArgs3.arg2));
                this.mCaller.recycleArgs(localSomeArgs3);
                break;
            case 90:
                HandlerCaller.SomeArgs localSomeArgs2 = (HandlerCaller.SomeArgs)paramMessage.obj;
                this.mInputMethodSession.updateSelection(localSomeArgs2.argi1, localSomeArgs2.argi2, localSomeArgs2.argi3, localSomeArgs2.argi4, localSomeArgs2.argi5, localSomeArgs2.argi6);
                this.mCaller.recycleArgs(localSomeArgs2);
                break;
            case 95:
                this.mInputMethodSession.updateCursor((Rect)paramMessage.obj);
                break;
            case 100:
                HandlerCaller.SomeArgs localSomeArgs1 = (HandlerCaller.SomeArgs)paramMessage.obj;
                this.mInputMethodSession.appPrivateCommand((String)localSomeArgs1.arg1, (Bundle)localSomeArgs1.arg2);
                this.mCaller.recycleArgs(localSomeArgs1);
                break;
            case 105:
                this.mInputMethodSession.toggleSoftInput(paramMessage.arg1, paramMessage.arg2);
                break;
            case 110:
                this.mInputMethodSession = null;
            case 115:
            }
        }
        InputMethodSession localInputMethodSession = this.mInputMethodSession;
        if (paramMessage.arg1 == i);
        while (true)
        {
            localInputMethodSession.viewClicked(i);
            break;
            int j = 0;
        }
    }

    public void finishInput()
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessage(60));
    }

    public void finishSession()
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessage(110));
    }

    public InputMethodSession getInternalInputMethodSession()
    {
        return this.mInputMethodSession;
    }

    public void toggleSoftInput(int paramInt1, int paramInt2)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageII(105, paramInt1, paramInt2));
    }

    public void updateCursor(Rect paramRect)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageO(95, paramRect));
    }

    public void updateExtractedText(int paramInt, ExtractedText paramExtractedText)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageIO(67, paramInt, paramExtractedText));
    }

    public void updateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageIIIIII(90, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6));
    }

    public void viewClicked(boolean paramBoolean)
    {
        HandlerCaller localHandlerCaller1 = this.mCaller;
        HandlerCaller localHandlerCaller2 = this.mCaller;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localHandlerCaller1.executeOrSendMessage(localHandlerCaller2.obtainMessageI(115, i));
            return;
        }
    }

    static class InputMethodEventCallbackWrapper
        implements InputMethodSession.EventCallback
    {
        final IInputMethodCallback mCb;

        InputMethodEventCallbackWrapper(IInputMethodCallback paramIInputMethodCallback)
        {
            this.mCb = paramIInputMethodCallback;
        }

        public void finishedEvent(int paramInt, boolean paramBoolean)
        {
            try
            {
                this.mCb.finishedEvent(paramInt, paramBoolean);
                label11: return;
            }
            catch (RemoteException localRemoteException)
            {
                break label11;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.inputmethodservice.IInputMethodSessionWrapper
 * JD-Core Version:        0.6.2
 */