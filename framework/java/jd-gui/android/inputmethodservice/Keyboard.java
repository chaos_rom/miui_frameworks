package android.inputmethodservice;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import com.android.internal.R.styleable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.xmlpull.v1.XmlPullParserException;

public class Keyboard
{
    public static final int EDGE_BOTTOM = 8;
    public static final int EDGE_LEFT = 1;
    public static final int EDGE_RIGHT = 2;
    public static final int EDGE_TOP = 4;
    private static final int GRID_HEIGHT = 5;
    private static final int GRID_SIZE = 50;
    private static final int GRID_WIDTH = 10;
    public static final int KEYCODE_ALT = -6;
    public static final int KEYCODE_CANCEL = -3;
    public static final int KEYCODE_DELETE = -5;
    public static final int KEYCODE_DONE = -4;
    public static final int KEYCODE_MODE_CHANGE = -2;
    public static final int KEYCODE_SHIFT = -1;
    private static float SEARCH_DISTANCE = 0.0F;
    static final String TAG = "Keyboard";
    private static final String TAG_KEY = "Key";
    private static final String TAG_KEYBOARD = "Keyboard";
    private static final String TAG_ROW = "Row";
    private int mCellHeight;
    private int mCellWidth;
    private int mDefaultHeight;
    private int mDefaultHorizontalGap;
    private int mDefaultVerticalGap;
    private int mDefaultWidth;
    private int mDisplayHeight;
    private int mDisplayWidth;
    private int[][] mGridNeighbors;
    private int mKeyHeight;
    private int mKeyWidth;
    private int mKeyboardMode;
    private List<Key> mKeys;
    private CharSequence mLabel;
    private List<Key> mModifierKeys;
    private int mProximityThreshold;
    private int[] mShiftKeyIndices;
    private Key[] mShiftKeys;
    private boolean mShifted;
    private int mTotalHeight;
    private int mTotalWidth;
    private ArrayList<Row> rows;

    public Keyboard(Context paramContext, int paramInt)
    {
        this(paramContext, paramInt, 0);
    }

    public Keyboard(Context paramContext, int paramInt1, int paramInt2)
    {
        Key[] arrayOfKey = new Key[2];
        arrayOfKey[0] = null;
        arrayOfKey[1] = null;
        this.mShiftKeys = arrayOfKey;
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = -1;
        arrayOfInt[1] = -1;
        this.mShiftKeyIndices = arrayOfInt;
        this.rows = new ArrayList();
        DisplayMetrics localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
        this.mDisplayWidth = localDisplayMetrics.widthPixels;
        this.mDisplayHeight = localDisplayMetrics.heightPixels;
        this.mDefaultHorizontalGap = 0;
        this.mDefaultWidth = (this.mDisplayWidth / 10);
        this.mDefaultVerticalGap = 0;
        this.mDefaultHeight = this.mDefaultWidth;
        this.mKeys = new ArrayList();
        this.mModifierKeys = new ArrayList();
        this.mKeyboardMode = paramInt2;
        loadKeyboard(paramContext, paramContext.getResources().getXml(paramInt1));
    }

    public Keyboard(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Key[] arrayOfKey = new Key[2];
        arrayOfKey[0] = null;
        arrayOfKey[1] = null;
        this.mShiftKeys = arrayOfKey;
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = -1;
        arrayOfInt[1] = -1;
        this.mShiftKeyIndices = arrayOfInt;
        this.rows = new ArrayList();
        this.mDisplayWidth = paramInt3;
        this.mDisplayHeight = paramInt4;
        this.mDefaultHorizontalGap = 0;
        this.mDefaultWidth = (this.mDisplayWidth / 10);
        this.mDefaultVerticalGap = 0;
        this.mDefaultHeight = this.mDefaultWidth;
        this.mKeys = new ArrayList();
        this.mModifierKeys = new ArrayList();
        this.mKeyboardMode = paramInt2;
        loadKeyboard(paramContext, paramContext.getResources().getXml(paramInt1));
    }

    public Keyboard(Context paramContext, int paramInt1, CharSequence paramCharSequence, int paramInt2, int paramInt3)
    {
        this(paramContext, paramInt1);
        int i = 0;
        int j = 0;
        int k = 0;
        this.mTotalWidth = 0;
        Row localRow = new Row(this);
        localRow.defaultHeight = this.mDefaultHeight;
        localRow.defaultWidth = this.mDefaultWidth;
        localRow.defaultHorizontalGap = this.mDefaultHorizontalGap;
        localRow.verticalGap = this.mDefaultVerticalGap;
        localRow.rowEdgeFlags = 12;
        if (paramInt2 == -1);
        for (int m = 2147483647; ; m = paramInt2)
            for (int n = 0; n < paramCharSequence.length(); n++)
            {
                char c = paramCharSequence.charAt(n);
                if ((k >= m) || (paramInt3 + (i + this.mDefaultWidth) > this.mDisplayWidth))
                {
                    i = 0;
                    j += this.mDefaultVerticalGap + this.mDefaultHeight;
                    k = 0;
                }
                Key localKey = new Key(localRow);
                localKey.x = i;
                localKey.y = j;
                localKey.label = String.valueOf(c);
                int[] arrayOfInt = new int[1];
                arrayOfInt[0] = c;
                localKey.codes = arrayOfInt;
                k++;
                i += localKey.width + localKey.gap;
                this.mKeys.add(localKey);
                localRow.mKeys.add(localKey);
                if (i > this.mTotalWidth)
                    this.mTotalWidth = i;
            }
        this.mTotalHeight = (j + this.mDefaultHeight);
        this.rows.add(localRow);
    }

    private void computeNearestNeighbors()
    {
        this.mCellWidth = ((-1 + (10 + getMinWidth())) / 10);
        this.mCellHeight = ((-1 + (5 + getHeight())) / 5);
        this.mGridNeighbors = new int[50][];
        int[] arrayOfInt1 = new int[this.mKeys.size()];
        int i = 10 * this.mCellWidth;
        int j = 5 * this.mCellHeight;
        int k = 0;
        while (k < i)
        {
            int m = 0;
            while (m < j)
            {
                int n = 0;
                for (int i1 = 0; i1 < this.mKeys.size(); i1++)
                {
                    Key localKey = (Key)this.mKeys.get(i1);
                    if ((localKey.squaredDistanceFrom(k, m) < this.mProximityThreshold) || (localKey.squaredDistanceFrom(-1 + (k + this.mCellWidth), m) < this.mProximityThreshold) || (localKey.squaredDistanceFrom(-1 + (k + this.mCellWidth), -1 + (m + this.mCellHeight)) < this.mProximityThreshold) || (localKey.squaredDistanceFrom(k, -1 + (m + this.mCellHeight)) < this.mProximityThreshold))
                    {
                        int i2 = n + 1;
                        arrayOfInt1[n] = i1;
                        n = i2;
                    }
                }
                int[] arrayOfInt2 = new int[n];
                System.arraycopy(arrayOfInt1, 0, arrayOfInt2, 0, n);
                this.mGridNeighbors[(10 * (m / this.mCellHeight) + k / this.mCellWidth)] = arrayOfInt2;
                m += this.mCellHeight;
            }
            k += this.mCellWidth;
        }
    }

    static int getDimensionOrFraction(TypedArray paramTypedArray, int paramInt1, int paramInt2, int paramInt3)
    {
        TypedValue localTypedValue = paramTypedArray.peekValue(paramInt1);
        if (localTypedValue == null);
        while (true)
        {
            return paramInt3;
            if (localTypedValue.type == 5)
                paramInt3 = paramTypedArray.getDimensionPixelOffset(paramInt1, paramInt3);
            else if (localTypedValue.type == 6)
                paramInt3 = Math.round(paramTypedArray.getFraction(paramInt1, paramInt2, paramInt2, paramInt3));
        }
    }

    private void loadKeyboard(Context paramContext, XmlResourceParser paramXmlResourceParser)
    {
        int i = 0;
        int j = 0;
        int k = 0;
        int m = 0;
        int n = 0;
        Key localKey = null;
        Row localRow = null;
        Resources localResources = paramContext.getResources();
        while (true)
        {
            int i1;
            String str;
            int i4;
            int i3;
            try
            {
                i1 = paramXmlResourceParser.next();
                if (i1 != 1)
                {
                    if (i1 != 2)
                        break label355;
                    str = paramXmlResourceParser.getName();
                    if ("Row".equals(str))
                    {
                        j = 1;
                        m = 0;
                        localRow = createRowFromXml(localResources, paramXmlResourceParser);
                        this.rows.add(localRow);
                        if ((localRow.mode == 0) || (localRow.mode == this.mKeyboardMode))
                            break label439;
                        i4 = 1;
                        if (i4 == 0)
                            continue;
                        skipToEndOfRow(paramXmlResourceParser);
                        j = 0;
                        continue;
                    }
                    if (!"Key".equals(str))
                        break label335;
                    i = 1;
                    localKey = createKeyFromXml(localResources, localRow, m, n, paramXmlResourceParser);
                    this.mKeys.add(localKey);
                    if (localKey.codes[0] != -1)
                        break label308;
                    i3 = 0;
                    if (i3 < this.mShiftKeys.length)
                    {
                        if (this.mShiftKeys[i3] != null)
                            break label302;
                        this.mShiftKeys[i3] = localKey;
                        this.mShiftKeyIndices[i3] = (-1 + this.mKeys.size());
                    }
                    this.mModifierKeys.add(localKey);
                    localRow.mKeys.add(localKey);
                    continue;
                }
            }
            catch (Exception localException)
            {
                Log.e("Keyboard", "Parse error:" + localException);
                localException.printStackTrace();
                this.mTotalHeight = (n - this.mDefaultVerticalGap);
                return;
            }
            label302: i3++;
            continue;
            label308: label439: if (localKey.codes[0] == -6)
            {
                this.mModifierKeys.add(localKey);
                continue;
                label335: if ("Keyboard".equals(str))
                {
                    parseKeyboardAttributes(localResources, paramXmlResourceParser);
                    continue;
                    label355: if (i1 == 3)
                        if (i != 0)
                        {
                            i = 0;
                            m += localKey.gap + localKey.width;
                            if (m > this.mTotalWidth)
                                this.mTotalWidth = m;
                        }
                        else if (j != 0)
                        {
                            j = 0;
                            n += localRow.verticalGap;
                            int i2 = localRow.defaultHeight;
                            n += i2;
                            k++;
                            continue;
                            i4 = 0;
                        }
                }
            }
        }
    }

    private void parseKeyboardAttributes(Resources paramResources, XmlResourceParser paramXmlResourceParser)
    {
        TypedArray localTypedArray = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard);
        this.mDefaultWidth = getDimensionOrFraction(localTypedArray, 0, this.mDisplayWidth, this.mDisplayWidth / 10);
        this.mDefaultHeight = getDimensionOrFraction(localTypedArray, 1, this.mDisplayHeight, 50);
        this.mDefaultHorizontalGap = getDimensionOrFraction(localTypedArray, 2, this.mDisplayWidth, 0);
        this.mDefaultVerticalGap = getDimensionOrFraction(localTypedArray, 3, this.mDisplayHeight, 0);
        this.mProximityThreshold = ((int)(this.mDefaultWidth * SEARCH_DISTANCE));
        this.mProximityThreshold *= this.mProximityThreshold;
        localTypedArray.recycle();
    }

    private void skipToEndOfRow(XmlResourceParser paramXmlResourceParser)
        throws XmlPullParserException, IOException
    {
        int i;
        do
            i = paramXmlResourceParser.next();
        while ((i != 1) && ((i != 3) || (!paramXmlResourceParser.getName().equals("Row"))));
    }

    protected Key createKeyFromXml(Resources paramResources, Row paramRow, int paramInt1, int paramInt2, XmlResourceParser paramXmlResourceParser)
    {
        return new Key(paramResources, paramRow, paramInt1, paramInt2, paramXmlResourceParser);
    }

    protected Row createRowFromXml(Resources paramResources, XmlResourceParser paramXmlResourceParser)
    {
        return new Row(paramResources, this, paramXmlResourceParser);
    }

    public int getHeight()
    {
        return this.mTotalHeight;
    }

    protected int getHorizontalGap()
    {
        return this.mDefaultHorizontalGap;
    }

    protected int getKeyHeight()
    {
        return this.mDefaultHeight;
    }

    protected int getKeyWidth()
    {
        return this.mDefaultWidth;
    }

    public List<Key> getKeys()
    {
        return this.mKeys;
    }

    public int getMinWidth()
    {
        return this.mTotalWidth;
    }

    public List<Key> getModifierKeys()
    {
        return this.mModifierKeys;
    }

    public int[] getNearestKeys(int paramInt1, int paramInt2)
    {
        if (this.mGridNeighbors == null)
            computeNearestNeighbors();
        int i;
        if ((paramInt1 >= 0) && (paramInt1 < getMinWidth()) && (paramInt2 >= 0) && (paramInt2 < getHeight()))
        {
            i = 10 * (paramInt2 / this.mCellHeight) + paramInt1 / this.mCellWidth;
            if (i >= 50);
        }
        for (int[] arrayOfInt = this.mGridNeighbors[i]; ; arrayOfInt = new int[0])
            return arrayOfInt;
    }

    public int getShiftKeyIndex()
    {
        return this.mShiftKeyIndices[0];
    }

    public int[] getShiftKeyIndices()
    {
        return this.mShiftKeyIndices;
    }

    protected int getVerticalGap()
    {
        return this.mDefaultVerticalGap;
    }

    public boolean isShifted()
    {
        return this.mShifted;
    }

    final void resize(int paramInt1, int paramInt2)
    {
        int i = this.rows.size();
        for (int j = 0; j < i; j++)
        {
            Row localRow = (Row)this.rows.get(j);
            int k = localRow.mKeys.size();
            int m = 0;
            int n = 0;
            for (int i1 = 0; i1 < k; i1++)
            {
                Key localKey2 = (Key)localRow.mKeys.get(i1);
                if (i1 > 0)
                    m += localKey2.gap;
                n += localKey2.width;
            }
            if (m + n > paramInt1)
            {
                int i2 = 0;
                float f = (paramInt1 - m) / n;
                for (int i3 = 0; i3 < k; i3++)
                {
                    Key localKey1 = (Key)localRow.mKeys.get(i3);
                    localKey1.width = ((int)(f * localKey1.width));
                    localKey1.x = i2;
                    i2 += localKey1.width + localKey1.gap;
                }
            }
        }
        this.mTotalWidth = paramInt1;
    }

    protected void setHorizontalGap(int paramInt)
    {
        this.mDefaultHorizontalGap = paramInt;
    }

    protected void setKeyHeight(int paramInt)
    {
        this.mDefaultHeight = paramInt;
    }

    protected void setKeyWidth(int paramInt)
    {
        this.mDefaultWidth = paramInt;
    }

    public boolean setShifted(boolean paramBoolean)
    {
        for (Key localKey : this.mShiftKeys)
            if (localKey != null)
                localKey.on = paramBoolean;
        if (this.mShifted != paramBoolean)
            this.mShifted = paramBoolean;
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void setVerticalGap(int paramInt)
    {
        this.mDefaultVerticalGap = paramInt;
    }

    public static class Key
    {
        private static final int[] KEY_STATE_NORMAL;
        private static final int[] KEY_STATE_NORMAL_OFF;
        private static final int[] KEY_STATE_NORMAL_ON;
        private static final int[] KEY_STATE_PRESSED = arrayOfInt5;
        private static final int[] KEY_STATE_PRESSED_OFF;
        private static final int[] KEY_STATE_PRESSED_ON;
        public int[] codes;
        public int edgeFlags;
        public int gap;
        public int height;
        public Drawable icon;
        public Drawable iconPreview;
        private Keyboard keyboard;
        public CharSequence label;
        public boolean modifier;
        public boolean on;
        public CharSequence popupCharacters;
        public int popupResId;
        public boolean pressed;
        public boolean repeatable;
        public boolean sticky;
        public CharSequence text;
        public int width;
        public int x;
        public int y;

        static
        {
            int[] arrayOfInt1 = new int[2];
            arrayOfInt1[0] = 16842911;
            arrayOfInt1[1] = 16842912;
            KEY_STATE_NORMAL_ON = arrayOfInt1;
            int[] arrayOfInt2 = new int[3];
            arrayOfInt2[0] = 16842919;
            arrayOfInt2[1] = 16842911;
            arrayOfInt2[2] = 16842912;
            KEY_STATE_PRESSED_ON = arrayOfInt2;
            int[] arrayOfInt3 = new int[1];
            arrayOfInt3[0] = 16842911;
            KEY_STATE_NORMAL_OFF = arrayOfInt3;
            int[] arrayOfInt4 = new int[2];
            arrayOfInt4[0] = 16842919;
            arrayOfInt4[1] = 16842911;
            KEY_STATE_PRESSED_OFF = arrayOfInt4;
            KEY_STATE_NORMAL = new int[0];
            int[] arrayOfInt5 = new int[1];
            arrayOfInt5[0] = 16842919;
        }

        public Key(Resources paramResources, Keyboard.Row paramRow, int paramInt1, int paramInt2, XmlResourceParser paramXmlResourceParser)
        {
            this(paramRow);
            this.x = paramInt1;
            this.y = paramInt2;
            TypedArray localTypedArray1 = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard);
            this.width = Keyboard.getDimensionOrFraction(localTypedArray1, 0, this.keyboard.mDisplayWidth, paramRow.defaultWidth);
            this.height = Keyboard.getDimensionOrFraction(localTypedArray1, 1, this.keyboard.mDisplayHeight, paramRow.defaultHeight);
            this.gap = Keyboard.getDimensionOrFraction(localTypedArray1, 2, this.keyboard.mDisplayWidth, paramRow.defaultHorizontalGap);
            localTypedArray1.recycle();
            TypedArray localTypedArray2 = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard_Key);
            this.x += this.gap;
            TypedValue localTypedValue = new TypedValue();
            localTypedArray2.getValue(0, localTypedValue);
            if ((localTypedValue.type == 16) || (localTypedValue.type == 17))
            {
                int[] arrayOfInt1 = new int[1];
                arrayOfInt1[0] = localTypedValue.data;
                this.codes = arrayOfInt1;
            }
            while (true)
            {
                this.iconPreview = localTypedArray2.getDrawable(7);
                if (this.iconPreview != null)
                    this.iconPreview.setBounds(0, 0, this.iconPreview.getIntrinsicWidth(), this.iconPreview.getIntrinsicHeight());
                this.popupCharacters = localTypedArray2.getText(2);
                this.popupResId = localTypedArray2.getResourceId(1, 0);
                this.repeatable = localTypedArray2.getBoolean(6, false);
                this.modifier = localTypedArray2.getBoolean(4, false);
                this.sticky = localTypedArray2.getBoolean(5, false);
                this.edgeFlags = localTypedArray2.getInt(3, 0);
                this.edgeFlags |= paramRow.rowEdgeFlags;
                this.icon = localTypedArray2.getDrawable(10);
                if (this.icon != null)
                    this.icon.setBounds(0, 0, this.icon.getIntrinsicWidth(), this.icon.getIntrinsicHeight());
                this.label = localTypedArray2.getText(9);
                this.text = localTypedArray2.getText(8);
                if ((this.codes == null) && (!TextUtils.isEmpty(this.label)))
                {
                    int[] arrayOfInt2 = new int[1];
                    arrayOfInt2[0] = this.label.charAt(0);
                    this.codes = arrayOfInt2;
                }
                localTypedArray2.recycle();
                return;
                if (localTypedValue.type == 3)
                    this.codes = parseCSV(localTypedValue.string.toString());
            }
        }

        public Key(Keyboard.Row paramRow)
        {
            this.keyboard = Keyboard.Row.access$600(paramRow);
            this.height = paramRow.defaultHeight;
            this.width = paramRow.defaultWidth;
            this.gap = paramRow.defaultHorizontalGap;
            this.edgeFlags = paramRow.rowEdgeFlags;
        }

        public int[] getCurrentDrawableState()
        {
            int[] arrayOfInt = KEY_STATE_NORMAL;
            if (this.on)
                if (this.pressed)
                    arrayOfInt = KEY_STATE_PRESSED_ON;
            while (true)
            {
                return arrayOfInt;
                arrayOfInt = KEY_STATE_NORMAL_ON;
                continue;
                if (this.sticky)
                {
                    if (this.pressed)
                        arrayOfInt = KEY_STATE_PRESSED_OFF;
                    else
                        arrayOfInt = KEY_STATE_NORMAL_OFF;
                }
                else if (this.pressed)
                    arrayOfInt = KEY_STATE_PRESSED;
            }
        }

        public boolean isInside(int paramInt1, int paramInt2)
        {
            boolean bool1 = true;
            boolean bool2;
            boolean bool3;
            label26: boolean bool4;
            label38: boolean bool5;
            if ((0x1 & this.edgeFlags) > 0)
            {
                bool2 = bool1;
                if ((0x2 & this.edgeFlags) <= 0)
                    break label163;
                bool3 = bool1;
                if ((0x4 & this.edgeFlags) <= 0)
                    break label169;
                bool4 = bool1;
                if ((0x8 & this.edgeFlags) <= 0)
                    break label175;
                bool5 = bool1;
                label51: if (((paramInt1 < this.x) && ((!bool2) || (paramInt1 > this.x + this.width))) || ((paramInt1 >= this.x + this.width) && ((!bool3) || (paramInt1 < this.x))) || ((paramInt2 < this.y) && ((!bool4) || (paramInt2 > this.y + this.height))) || ((paramInt2 >= this.y + this.height) && ((!bool5) || (paramInt2 < this.y))))
                    break label181;
            }
            while (true)
            {
                return bool1;
                bool2 = false;
                break;
                label163: bool3 = false;
                break label26;
                label169: bool4 = false;
                break label38;
                label175: bool5 = false;
                break label51;
                label181: bool1 = false;
            }
        }

        public void onPressed()
        {
            if (!this.pressed);
            for (boolean bool = true; ; bool = false)
            {
                this.pressed = bool;
                return;
            }
        }

        public void onReleased(boolean paramBoolean)
        {
            boolean bool1 = true;
            boolean bool2;
            if (!this.pressed)
            {
                bool2 = bool1;
                this.pressed = bool2;
                if (this.sticky)
                    if (this.on)
                        break label41;
            }
            while (true)
            {
                this.on = bool1;
                return;
                bool2 = false;
                break;
                label41: bool1 = false;
            }
        }

        int[] parseCSV(String paramString)
        {
            int i = 0;
            int j = 0;
            if (paramString.length() > 0)
                for (i = 0 + 1; ; i++)
                {
                    j = paramString.indexOf(",", j + 1);
                    if (j <= 0)
                        break;
                }
            int[] arrayOfInt = new int[i];
            int k = 0;
            StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
            while (localStringTokenizer.hasMoreTokens())
            {
                int m = k + 1;
                try
                {
                    arrayOfInt[k] = Integer.parseInt(localStringTokenizer.nextToken());
                    k = m;
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    Log.e("Keyboard", "Error parsing keycodes " + paramString);
                    k = m;
                }
            }
            return arrayOfInt;
        }

        public int squaredDistanceFrom(int paramInt1, int paramInt2)
        {
            int i = this.x + this.width / 2 - paramInt1;
            int j = this.y + this.height / 2 - paramInt2;
            return i * i + j * j;
        }
    }

    public static class Row
    {
        public int defaultHeight;
        public int defaultHorizontalGap;
        public int defaultWidth;
        ArrayList<Keyboard.Key> mKeys = new ArrayList();
        public int mode;
        private Keyboard parent;
        public int rowEdgeFlags;
        public int verticalGap;

        public Row(Resources paramResources, Keyboard paramKeyboard, XmlResourceParser paramXmlResourceParser)
        {
            this.parent = paramKeyboard;
            TypedArray localTypedArray1 = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard);
            this.defaultWidth = Keyboard.getDimensionOrFraction(localTypedArray1, 0, paramKeyboard.mDisplayWidth, paramKeyboard.mDefaultWidth);
            this.defaultHeight = Keyboard.getDimensionOrFraction(localTypedArray1, 1, paramKeyboard.mDisplayHeight, paramKeyboard.mDefaultHeight);
            this.defaultHorizontalGap = Keyboard.getDimensionOrFraction(localTypedArray1, 2, paramKeyboard.mDisplayWidth, paramKeyboard.mDefaultHorizontalGap);
            this.verticalGap = Keyboard.getDimensionOrFraction(localTypedArray1, 3, paramKeyboard.mDisplayHeight, paramKeyboard.mDefaultVerticalGap);
            localTypedArray1.recycle();
            TypedArray localTypedArray2 = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard_Row);
            this.rowEdgeFlags = localTypedArray2.getInt(0, 0);
            this.mode = localTypedArray2.getResourceId(1, 0);
        }

        public Row(Keyboard paramKeyboard)
        {
            this.parent = paramKeyboard;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.inputmethodservice.Keyboard
 * JD-Core Version:        0.6.2
 */