package android.inputmethodservice;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region.Op;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KeyboardView extends View
    implements View.OnClickListener
{
    private static final int DEBOUNCE_TIME = 70;
    private static final boolean DEBUG = false;
    private static final int DELAY_AFTER_PREVIEW = 70;
    private static final int DELAY_BEFORE_PREVIEW = 0;
    private static final int[] KEY_DELETE;
    private static final int LONGPRESS_TIMEOUT = 0;
    private static final int[] LONG_PRESSABLE_STATE_SET;
    private static int MAX_NEARBY_KEYS = 0;
    private static final int MSG_LONGPRESS = 4;
    private static final int MSG_REMOVE_PREVIEW = 2;
    private static final int MSG_REPEAT = 3;
    private static final int MSG_SHOW_PREVIEW = 1;
    private static final int MULTITAP_INTERVAL = 800;
    private static final int NOT_A_KEY = -1;
    private static final int REPEAT_INTERVAL = 50;
    private static final int REPEAT_START_DELAY = 400;
    private boolean mAbortKey;
    private AccessibilityManager mAccessibilityManager;
    private AudioManager mAudioManager;
    private float mBackgroundDimAmount;
    private Bitmap mBuffer;
    private Canvas mCanvas;
    private Rect mClipRegion = new Rect(0, 0, 0, 0);
    private final int[] mCoordinates = new int[2];
    private int mCurrentKey = -1;
    private int mCurrentKeyIndex = -1;
    private long mCurrentKeyTime;
    private Rect mDirtyRect = new Rect();
    private boolean mDisambiguateSwipe;
    private int[] mDistances = new int[MAX_NEARBY_KEYS];
    private int mDownKey = -1;
    private long mDownTime;
    private boolean mDrawPending;
    private GestureDetector mGestureDetector;
    Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return;
                KeyboardView.this.showKey(paramAnonymousMessage.arg1);
                continue;
                KeyboardView.this.mPreviewText.setVisibility(4);
                continue;
                if (KeyboardView.this.repeatKey())
                {
                    sendMessageDelayed(Message.obtain(this, 3), 50L);
                    continue;
                    KeyboardView.this.openPopupIfRequired((MotionEvent)paramAnonymousMessage.obj);
                }
            }
        }
    };
    private boolean mHeadsetRequiredToHearPasswordsAnnounced;
    private boolean mInMultiTap;
    private Keyboard.Key mInvalidatedKey;
    private Drawable mKeyBackground;
    private int[] mKeyIndices = new int[12];
    private int mKeyTextColor;
    private int mKeyTextSize;
    private Keyboard mKeyboard;
    private OnKeyboardActionListener mKeyboardActionListener;
    private boolean mKeyboardChanged;
    private Keyboard.Key[] mKeys;
    private int mLabelTextSize;
    private int mLastCodeX;
    private int mLastCodeY;
    private int mLastKey;
    private long mLastKeyTime;
    private long mLastMoveTime;
    private int mLastSentIndex;
    private long mLastTapTime;
    private int mLastX;
    private int mLastY;
    private KeyboardView mMiniKeyboard;
    private Map<Keyboard.Key, View> mMiniKeyboardCache;
    private View mMiniKeyboardContainer;
    private int mMiniKeyboardOffsetX;
    private int mMiniKeyboardOffsetY;
    private boolean mMiniKeyboardOnScreen;
    private int mOldPointerCount = 1;
    private float mOldPointerX;
    private float mOldPointerY;
    private Rect mPadding;
    private Paint mPaint;
    private PopupWindow mPopupKeyboard;
    private int mPopupLayout;
    private View mPopupParent;
    private int mPopupPreviewX;
    private int mPopupPreviewY;
    private int mPopupX;
    private int mPopupY;
    private boolean mPossiblePoly;
    private boolean mPreviewCentered = false;
    private int mPreviewHeight;
    private StringBuilder mPreviewLabel = new StringBuilder(1);
    private int mPreviewOffset;
    private PopupWindow mPreviewPopup;
    private TextView mPreviewText;
    private int mPreviewTextSizeLarge;
    private boolean mProximityCorrectOn;
    private int mProximityThreshold;
    private int mRepeatKeyIndex = -1;
    private int mShadowColor;
    private float mShadowRadius;
    private boolean mShowPreview = true;
    private boolean mShowTouchPoints = true;
    private int mStartX;
    private int mStartY;
    private int mSwipeThreshold;
    private SwipeTracker mSwipeTracker = new SwipeTracker(null);
    private int mTapCount;
    private int mVerticalCorrection;

    static
    {
        int[] arrayOfInt1 = new int[1];
        arrayOfInt1[0] = -5;
        KEY_DELETE = arrayOfInt1;
        int[] arrayOfInt2 = new int[1];
        arrayOfInt2[0] = 16843324;
        LONG_PRESSABLE_STATE_SET = arrayOfInt2;
    }

    public KeyboardView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16843794);
    }

    public KeyboardView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, android.R.styleable.KeyboardView, paramInt, 0);
        LayoutInflater localLayoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
        int i = 0;
        int j = localTypedArray.getIndexCount();
        int k = 0;
        if (k < j)
        {
            int m = localTypedArray.getIndex(k);
            switch (m)
            {
            default:
            case 2:
            case 9:
            case 6:
            case 7:
            case 8:
            case 3:
            case 5:
            case 4:
            case 10:
            case 0:
            case 1:
            }
            while (true)
            {
                k++;
                break;
                this.mKeyBackground = localTypedArray.getDrawable(m);
                continue;
                this.mVerticalCorrection = localTypedArray.getDimensionPixelOffset(m, 0);
                continue;
                i = localTypedArray.getResourceId(m, 0);
                continue;
                this.mPreviewOffset = localTypedArray.getDimensionPixelOffset(m, 0);
                continue;
                this.mPreviewHeight = localTypedArray.getDimensionPixelSize(m, 80);
                continue;
                this.mKeyTextSize = localTypedArray.getDimensionPixelSize(m, 18);
                continue;
                this.mKeyTextColor = localTypedArray.getColor(m, -16777216);
                continue;
                this.mLabelTextSize = localTypedArray.getDimensionPixelSize(m, 14);
                continue;
                this.mPopupLayout = localTypedArray.getResourceId(m, 0);
                continue;
                this.mShadowColor = localTypedArray.getColor(m, 0);
                continue;
                this.mShadowRadius = localTypedArray.getFloat(m, 0.0F);
            }
        }
        this.mBackgroundDimAmount = this.mContext.obtainStyledAttributes(com.android.internal.R.styleable.Theme).getFloat(2, 0.5F);
        this.mPreviewPopup = new PopupWindow(paramContext);
        if (i != 0)
        {
            this.mPreviewText = ((TextView)localLayoutInflater.inflate(i, null));
            this.mPreviewTextSizeLarge = ((int)this.mPreviewText.getTextSize());
            this.mPreviewPopup.setContentView(this.mPreviewText);
            this.mPreviewPopup.setBackgroundDrawable(null);
        }
        while (true)
        {
            this.mPreviewPopup.setTouchable(false);
            this.mPopupKeyboard = new PopupWindow(paramContext);
            this.mPopupKeyboard.setBackgroundDrawable(null);
            this.mPopupParent = this;
            this.mPaint = new Paint();
            this.mPaint.setAntiAlias(true);
            this.mPaint.setTextSize(0);
            this.mPaint.setTextAlign(Paint.Align.CENTER);
            this.mPaint.setAlpha(255);
            this.mPadding = new Rect(0, 0, 0, 0);
            this.mMiniKeyboardCache = new HashMap();
            this.mKeyBackground.getPadding(this.mPadding);
            this.mSwipeThreshold = ((int)(500.0F * getResources().getDisplayMetrics().density));
            this.mDisambiguateSwipe = getResources().getBoolean(17891363);
            this.mAccessibilityManager = AccessibilityManager.getInstance(paramContext);
            this.mAudioManager = ((AudioManager)paramContext.getSystemService("audio"));
            resetMultiTap();
            initGestureDetector();
            return;
            this.mShowPreview = false;
        }
    }

    private CharSequence adjustCase(CharSequence paramCharSequence)
    {
        if ((this.mKeyboard.isShifted()) && (paramCharSequence != null) && (paramCharSequence.length() < 3) && (Character.isLowerCase(paramCharSequence.charAt(0))))
            paramCharSequence = paramCharSequence.toString().toUpperCase();
        return paramCharSequence;
    }

    private void checkMultiTap(long paramLong, int paramInt)
    {
        if (paramInt == -1);
        while (true)
        {
            return;
            Keyboard.Key localKey = this.mKeys[paramInt];
            if (localKey.codes.length > 1)
            {
                this.mInMultiTap = true;
                if ((paramLong < 800L + this.mLastTapTime) && (paramInt == this.mLastSentIndex))
                    this.mTapCount = ((1 + this.mTapCount) % localKey.codes.length);
                else
                    this.mTapCount = -1;
            }
            else if ((paramLong > 800L + this.mLastTapTime) || (paramInt != this.mLastSentIndex))
            {
                resetMultiTap();
            }
        }
    }

    private void computeProximityThreshold(Keyboard paramKeyboard)
    {
        if (paramKeyboard == null);
        while (true)
        {
            return;
            Keyboard.Key[] arrayOfKey = this.mKeys;
            if (arrayOfKey != null)
            {
                int i = arrayOfKey.length;
                int j = 0;
                for (int k = 0; k < i; k++)
                {
                    Keyboard.Key localKey = arrayOfKey[k];
                    j += Math.min(localKey.width, localKey.height) + localKey.gap;
                }
                if ((j >= 0) && (i != 0))
                {
                    this.mProximityThreshold = ((int)(1.4F * j / i));
                    this.mProximityThreshold *= this.mProximityThreshold;
                }
            }
        }
    }

    private void detectAndSendKey(int paramInt1, int paramInt2, int paramInt3, long paramLong)
    {
        Keyboard.Key localKey;
        if ((paramInt1 != -1) && (paramInt1 < this.mKeys.length))
        {
            localKey = this.mKeys[paramInt1];
            if (localKey.text != null)
            {
                this.mKeyboardActionListener.onText(localKey.text);
                this.mKeyboardActionListener.onRelease(-1);
                this.mLastSentIndex = paramInt1;
                this.mLastTapTime = paramLong;
            }
        }
        else
        {
            return;
        }
        int i = localKey.codes[0];
        int[] arrayOfInt = new int[MAX_NEARBY_KEYS];
        Arrays.fill(arrayOfInt, -1);
        getKeyIndices(paramInt2, paramInt3, arrayOfInt);
        if (this.mInMultiTap)
        {
            if (this.mTapCount == -1)
                break label169;
            this.mKeyboardActionListener.onKey(-5, KEY_DELETE);
        }
        while (true)
        {
            i = localKey.codes[this.mTapCount];
            this.mKeyboardActionListener.onKey(i, arrayOfInt);
            this.mKeyboardActionListener.onRelease(i);
            break;
            label169: this.mTapCount = 0;
        }
    }

    private void dismissPopupKeyboard()
    {
        if (this.mPopupKeyboard.isShowing())
        {
            this.mPopupKeyboard.dismiss();
            this.mMiniKeyboardOnScreen = false;
            invalidateAllKeys();
        }
    }

    private int getKeyIndices(int paramInt1, int paramInt2, int[] paramArrayOfInt)
    {
        Keyboard.Key[] arrayOfKey = this.mKeys;
        int i = -1;
        int j = -1;
        int k = 1 + this.mProximityThreshold;
        Arrays.fill(this.mDistances, 2147483647);
        int[] arrayOfInt = this.mKeyboard.getNearestKeys(paramInt1, paramInt2);
        int m = arrayOfInt.length;
        int n = 0;
        if (n < m)
        {
            Keyboard.Key localKey = arrayOfKey[arrayOfInt[n]];
            int i1 = 0;
            boolean bool = localKey.isInside(paramInt1, paramInt2);
            if (bool)
                i = arrayOfInt[n];
            if (this.mProximityCorrectOn)
            {
                i1 = localKey.squaredDistanceFrom(paramInt1, paramInt2);
                if (i1 < this.mProximityThreshold)
                    break label122;
            }
            label122: int i2;
            if ((bool) && (localKey.codes[0] > 32))
            {
                i2 = localKey.codes.length;
                if (i1 < k)
                {
                    k = i1;
                    j = arrayOfInt[n];
                }
                if (paramArrayOfInt != null)
                    break label170;
            }
            label291: 
            while (true)
            {
                n++;
                break;
                label170: for (int i3 = 0; ; i3++)
                {
                    if (i3 >= this.mDistances.length)
                        break label291;
                    if (this.mDistances[i3] > i1)
                    {
                        System.arraycopy(this.mDistances, i3, this.mDistances, i3 + i2, this.mDistances.length - i3 - i2);
                        System.arraycopy(paramArrayOfInt, i3, paramArrayOfInt, i3 + i2, paramArrayOfInt.length - i3 - i2);
                        for (int i4 = 0; i4 < i2; i4++)
                        {
                            paramArrayOfInt[(i3 + i4)] = localKey.codes[i4];
                            this.mDistances[(i3 + i4)] = i1;
                        }
                        break;
                    }
                }
            }
        }
        if (i == -1)
            i = j;
        return i;
    }

    private CharSequence getPreviewText(Keyboard.Key paramKey)
    {
        int i = 0;
        if (this.mInMultiTap)
        {
            this.mPreviewLabel.setLength(0);
            StringBuilder localStringBuilder = this.mPreviewLabel;
            int[] arrayOfInt = paramKey.codes;
            if (this.mTapCount < 0)
                localStringBuilder.append((char)arrayOfInt[i]);
        }
        for (CharSequence localCharSequence = adjustCase(this.mPreviewLabel); ; localCharSequence = adjustCase(paramKey.label))
        {
            return localCharSequence;
            i = this.mTapCount;
            break;
        }
    }

    private void initGestureDetector()
    {
        this.mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener()
        {
            public boolean onFling(MotionEvent paramAnonymousMotionEvent1, MotionEvent paramAnonymousMotionEvent2, float paramAnonymousFloat1, float paramAnonymousFloat2)
            {
                boolean bool;
                if (KeyboardView.this.mPossiblePoly)
                    bool = false;
                while (true)
                {
                    return bool;
                    float f1 = Math.abs(paramAnonymousFloat1);
                    float f2 = Math.abs(paramAnonymousFloat2);
                    float f3 = paramAnonymousMotionEvent2.getX() - paramAnonymousMotionEvent1.getX();
                    float f4 = paramAnonymousMotionEvent2.getY() - paramAnonymousMotionEvent1.getY();
                    int i = KeyboardView.this.getWidth() / 2;
                    int j = KeyboardView.this.getHeight() / 2;
                    KeyboardView.this.mSwipeTracker.computeCurrentVelocity(1000);
                    float f5 = KeyboardView.this.mSwipeTracker.getXVelocity();
                    float f6 = KeyboardView.this.mSwipeTracker.getYVelocity();
                    int k = 0;
                    if ((paramAnonymousFloat1 > KeyboardView.this.mSwipeThreshold) && (f2 < f1) && (f3 > i))
                        if ((KeyboardView.this.mDisambiguateSwipe) && (f5 < paramAnonymousFloat1 / 4.0F))
                            k = 1;
                    while (true)
                    {
                        if (k != 0)
                            KeyboardView.this.detectAndSendKey(KeyboardView.this.mDownKey, KeyboardView.this.mStartX, KeyboardView.this.mStartY, paramAnonymousMotionEvent1.getEventTime());
                        bool = false;
                        break;
                        KeyboardView.this.swipeRight();
                        bool = true;
                        break;
                        if ((paramAnonymousFloat1 < -KeyboardView.this.mSwipeThreshold) && (f2 < f1) && (f3 < -i))
                        {
                            if ((KeyboardView.this.mDisambiguateSwipe) && (f5 > paramAnonymousFloat1 / 4.0F))
                            {
                                k = 1;
                                continue;
                            }
                            KeyboardView.this.swipeLeft();
                            bool = true;
                            break;
                        }
                        if ((paramAnonymousFloat2 < -KeyboardView.this.mSwipeThreshold) && (f1 < f2) && (f4 < -j))
                        {
                            if ((KeyboardView.this.mDisambiguateSwipe) && (f6 > paramAnonymousFloat2 / 4.0F))
                            {
                                k = 1;
                                continue;
                            }
                            KeyboardView.this.swipeUp();
                            bool = true;
                            break;
                        }
                        if ((paramAnonymousFloat2 > KeyboardView.this.mSwipeThreshold) && (f1 < f2 / 2.0F) && (f4 > j))
                        {
                            if ((!KeyboardView.this.mDisambiguateSwipe) || (f6 >= paramAnonymousFloat2 / 4.0F))
                                break label426;
                            k = 1;
                        }
                    }
                    label426: KeyboardView.this.swipeDown();
                    bool = true;
                }
            }
        });
        this.mGestureDetector.setIsLongpressEnabled(false);
    }

    private void onBufferDraw()
    {
        if ((this.mBuffer == null) || (this.mKeyboardChanged))
        {
            if ((this.mBuffer == null) || ((this.mKeyboardChanged) && ((this.mBuffer.getWidth() != getWidth()) || (this.mBuffer.getHeight() != getHeight()))))
            {
                this.mBuffer = Bitmap.createBitmap(Math.max(1, getWidth()), Math.max(1, getHeight()), Bitmap.Config.ARGB_8888);
                this.mCanvas = new Canvas(this.mBuffer);
            }
            invalidateAllKeys();
            this.mKeyboardChanged = false;
        }
        Canvas localCanvas = this.mCanvas;
        localCanvas.clipRect(this.mDirtyRect, Region.Op.REPLACE);
        if (this.mKeyboard == null);
        while (true)
        {
            return;
            Paint localPaint = this.mPaint;
            Drawable localDrawable = this.mKeyBackground;
            Rect localRect1 = this.mClipRegion;
            Rect localRect2 = this.mPadding;
            int i = this.mPaddingLeft;
            int j = this.mPaddingTop;
            Keyboard.Key[] arrayOfKey = this.mKeys;
            Keyboard.Key localKey1 = this.mInvalidatedKey;
            localPaint.setColor(this.mKeyTextColor);
            int k = 0;
            if ((localKey1 != null) && (localCanvas.getClipBounds(localRect1)) && (-1 + (i + localKey1.x) <= localRect1.left) && (-1 + (j + localKey1.y) <= localRect1.top) && (1 + (i + (localKey1.x + localKey1.width)) >= localRect1.right) && (1 + (j + (localKey1.y + localKey1.height)) >= localRect1.bottom))
                k = 1;
            localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
            int m = arrayOfKey.length;
            int n = 0;
            while (n < m)
            {
                Keyboard.Key localKey2 = arrayOfKey[n];
                if ((k != 0) && (localKey1 != localKey2))
                {
                    n++;
                }
                else
                {
                    localDrawable.setState(localKey2.getCurrentDrawableState());
                    String str;
                    if (localKey2.label == null)
                    {
                        str = null;
                        label362: Rect localRect3 = localDrawable.getBounds();
                        if ((localKey2.width != localRect3.right) || (localKey2.height != localRect3.bottom))
                            localDrawable.setBounds(0, 0, localKey2.width, localKey2.height);
                        localCanvas.translate(i + localKey2.x, j + localKey2.y);
                        localDrawable.draw(localCanvas);
                        if (str == null)
                            break label647;
                        if ((str.length() <= 1) || (localKey2.codes.length >= 2))
                            break label627;
                        localPaint.setTextSize(this.mLabelTextSize);
                        localPaint.setTypeface(Typeface.DEFAULT_BOLD);
                        label481: localPaint.setShadowLayer(this.mShadowRadius, 0.0F, 0.0F, this.mShadowColor);
                        float f1 = (localKey2.width - localRect2.left - localRect2.right) / 2 + localRect2.left;
                        float f2 = (localKey2.height - localRect2.top - localRect2.bottom) / 2 + (localPaint.getTextSize() - localPaint.descent()) / 2.0F + localRect2.top;
                        localCanvas.drawText(str, f1, f2, localPaint);
                        localPaint.setShadowLayer(0.0F, 0.0F, 0.0F, 0);
                    }
                    while (true)
                    {
                        localCanvas.translate(-localKey2.x - i, -localKey2.y - j);
                        break;
                        str = adjustCase(localKey2.label).toString();
                        break label362;
                        label627: localPaint.setTextSize(this.mKeyTextSize);
                        localPaint.setTypeface(Typeface.DEFAULT);
                        break label481;
                        label647: if (localKey2.icon != null)
                        {
                            int i1 = (localKey2.width - localRect2.left - localRect2.right - localKey2.icon.getIntrinsicWidth()) / 2 + localRect2.left;
                            int i2 = (localKey2.height - localRect2.top - localRect2.bottom - localKey2.icon.getIntrinsicHeight()) / 2 + localRect2.top;
                            localCanvas.translate(i1, i2);
                            localKey2.icon.setBounds(0, 0, localKey2.icon.getIntrinsicWidth(), localKey2.icon.getIntrinsicHeight());
                            localKey2.icon.draw(localCanvas);
                            localCanvas.translate(-i1, -i2);
                        }
                    }
                }
            }
            this.mInvalidatedKey = null;
            if (this.mMiniKeyboardOnScreen)
            {
                localPaint.setColor((int)(255.0F * this.mBackgroundDimAmount) << 24);
                localCanvas.drawRect(0.0F, 0.0F, getWidth(), getHeight(), localPaint);
            }
            this.mDrawPending = false;
            this.mDirtyRect.setEmpty();
        }
    }

    private boolean onModifiedTouchEvent(MotionEvent paramMotionEvent, boolean paramBoolean)
    {
        int i = (int)paramMotionEvent.getX() - this.mPaddingLeft;
        int j = (int)paramMotionEvent.getY() - this.mPaddingTop;
        if (j >= -this.mVerticalCorrection)
            j += this.mVerticalCorrection;
        int k = paramMotionEvent.getAction();
        long l = paramMotionEvent.getEventTime();
        int m = getKeyIndices(i, j, null);
        this.mPossiblePoly = paramBoolean;
        if (k == 0)
            this.mSwipeTracker.clear();
        this.mSwipeTracker.addMovement(paramMotionEvent);
        boolean bool;
        if ((this.mAbortKey) && (k != 0) && (k != 3))
            bool = true;
        while (true)
        {
            return bool;
            if (this.mGestureDetector.onTouchEvent(paramMotionEvent))
            {
                showPreview(-1);
                this.mHandler.removeMessages(3);
                this.mHandler.removeMessages(4);
                bool = true;
            }
            else
            {
                if ((!this.mMiniKeyboardOnScreen) || (k == 3))
                    break;
                bool = true;
            }
        }
        switch (k)
        {
        default:
        case 0:
        case 2:
        case 1:
        case 3:
        }
        while (true)
        {
            this.mLastX = i;
            this.mLastY = j;
            bool = true;
            break;
            this.mAbortKey = false;
            this.mStartX = i;
            this.mStartY = j;
            this.mLastCodeX = i;
            this.mLastCodeY = j;
            this.mLastKeyTime = 0L;
            this.mCurrentKeyTime = 0L;
            this.mLastKey = -1;
            this.mCurrentKey = m;
            this.mDownKey = m;
            this.mDownTime = paramMotionEvent.getEventTime();
            this.mLastMoveTime = this.mDownTime;
            checkMultiTap(l, m);
            OnKeyboardActionListener localOnKeyboardActionListener = this.mKeyboardActionListener;
            if (m != -1);
            for (int i1 = this.mKeys[m].codes[0]; ; i1 = 0)
            {
                localOnKeyboardActionListener.onPress(i1);
                if ((this.mCurrentKey < 0) || (!this.mKeys[this.mCurrentKey].repeatable))
                    break label416;
                this.mRepeatKeyIndex = this.mCurrentKey;
                Message localMessage3 = this.mHandler.obtainMessage(3);
                this.mHandler.sendMessageDelayed(localMessage3, 400L);
                repeatKey();
                if (!this.mAbortKey)
                    break label416;
                this.mRepeatKeyIndex = -1;
                break;
            }
            label416: if (this.mCurrentKey != -1)
            {
                Message localMessage2 = this.mHandler.obtainMessage(4, paramMotionEvent);
                this.mHandler.sendMessageDelayed(localMessage2, LONGPRESS_TIMEOUT);
            }
            showPreview(m);
            continue;
            int n = 0;
            if (m != -1)
            {
                if (this.mCurrentKey != -1)
                    break label557;
                this.mCurrentKey = m;
                this.mCurrentKeyTime = (l - this.mDownTime);
            }
            while (true)
            {
                if (n == 0)
                {
                    this.mHandler.removeMessages(4);
                    if (m != -1)
                    {
                        Message localMessage1 = this.mHandler.obtainMessage(4, paramMotionEvent);
                        this.mHandler.sendMessageDelayed(localMessage1, LONGPRESS_TIMEOUT);
                    }
                }
                showPreview(this.mCurrentKey);
                this.mLastMoveTime = l;
                break;
                label557: if (m == this.mCurrentKey)
                {
                    this.mCurrentKeyTime += l - this.mLastMoveTime;
                    n = 1;
                }
                else if (this.mRepeatKeyIndex == -1)
                {
                    resetMultiTap();
                    this.mLastKey = this.mCurrentKey;
                    this.mLastCodeX = this.mLastX;
                    this.mLastCodeY = this.mLastY;
                    this.mLastKeyTime = (l + this.mCurrentKeyTime - this.mLastMoveTime);
                    this.mCurrentKey = m;
                    this.mCurrentKeyTime = 0L;
                }
            }
            removeMessages();
            if (m == this.mCurrentKey);
            for (this.mCurrentKeyTime += l - this.mLastMoveTime; ; this.mCurrentKeyTime = 0L)
            {
                if ((this.mCurrentKeyTime < this.mLastKeyTime) && (this.mCurrentKeyTime < 70L) && (this.mLastKey != -1))
                {
                    this.mCurrentKey = this.mLastKey;
                    i = this.mLastCodeX;
                    j = this.mLastCodeY;
                }
                showPreview(-1);
                Arrays.fill(this.mKeyIndices, -1);
                if ((this.mRepeatKeyIndex == -1) && (!this.mMiniKeyboardOnScreen) && (!this.mAbortKey))
                    detectAndSendKey(this.mCurrentKey, i, j, l);
                invalidateKey(m);
                this.mRepeatKeyIndex = -1;
                break;
                resetMultiTap();
                this.mLastKey = this.mCurrentKey;
                this.mLastKeyTime = (l + this.mCurrentKeyTime - this.mLastMoveTime);
                this.mCurrentKey = m;
            }
            removeMessages();
            dismissPopupKeyboard();
            this.mAbortKey = true;
            showPreview(-1);
            invalidateKey(this.mCurrentKey);
        }
    }

    private boolean openPopupIfRequired(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        if (this.mPopupLayout == 0);
        while (true)
        {
            return bool;
            if ((this.mCurrentKey >= 0) && (this.mCurrentKey < this.mKeys.length))
            {
                bool = onLongPress(this.mKeys[this.mCurrentKey]);
                if (bool)
                {
                    this.mAbortKey = true;
                    showPreview(-1);
                }
            }
        }
    }

    private void removeMessages()
    {
        this.mHandler.removeMessages(3);
        this.mHandler.removeMessages(4);
        this.mHandler.removeMessages(1);
    }

    private boolean repeatKey()
    {
        Keyboard.Key localKey = this.mKeys[this.mRepeatKeyIndex];
        detectAndSendKey(this.mCurrentKey, localKey.x, localKey.y, this.mLastTapTime);
        return true;
    }

    private void resetMultiTap()
    {
        this.mLastSentIndex = -1;
        this.mTapCount = 0;
        this.mLastTapTime = -1L;
        this.mInMultiTap = false;
    }

    private void sendAccessibilityEventForUnicodeCharacter(int paramInt1, int paramInt2)
    {
        int i = 0;
        AccessibilityEvent localAccessibilityEvent;
        String str;
        if (this.mAccessibilityManager.isEnabled())
        {
            localAccessibilityEvent = AccessibilityEvent.obtain(paramInt1);
            onInitializeAccessibilityEvent(localAccessibilityEvent);
            if (Settings.Secure.getInt(this.mContext.getContentResolver(), "speak_password", 0) != 0)
                i = 1;
            if ((i == 0) && (!this.mAudioManager.isBluetoothA2dpOn()) && (!this.mAudioManager.isWiredHeadsetOn()))
                break label271;
            switch (paramInt2)
            {
            default:
                str = String.valueOf((char)paramInt2);
            case -6:
            case -3:
            case -5:
            case -4:
            case -2:
            case -1:
            case 10:
            }
        }
        while (true)
        {
            localAccessibilityEvent.getText().add(str);
            this.mAccessibilityManager.sendAccessibilityEvent(localAccessibilityEvent);
            return;
            str = this.mContext.getString(17040569);
            continue;
            str = this.mContext.getString(17040570);
            continue;
            str = this.mContext.getString(17040571);
            continue;
            str = this.mContext.getString(17040572);
            continue;
            str = this.mContext.getString(17040573);
            continue;
            str = this.mContext.getString(17040574);
            continue;
            str = this.mContext.getString(17040575);
            continue;
            label271: if (!this.mHeadsetRequiredToHearPasswordsAnnounced)
            {
                if (paramInt1 == 256)
                    this.mHeadsetRequiredToHearPasswordsAnnounced = true;
                str = this.mContext.getString(17040590);
            }
            else
            {
                str = this.mContext.getString(17040591);
            }
        }
    }

    private void showKey(int paramInt)
    {
        PopupWindow localPopupWindow = this.mPreviewPopup;
        Keyboard.Key[] arrayOfKey = this.mKeys;
        if ((paramInt < 0) || (paramInt >= this.mKeys.length))
            return;
        Keyboard.Key localKey = arrayOfKey[paramInt];
        Drawable localDrawable2;
        label58: label76: int i;
        int j;
        label206: int[] arrayOfInt3;
        if (localKey.icon != null)
        {
            TextView localTextView = this.mPreviewText;
            if (localKey.iconPreview != null)
            {
                localDrawable2 = localKey.iconPreview;
                localTextView.setCompoundDrawables(null, null, null, localDrawable2);
                this.mPreviewText.setText(null);
                this.mPreviewText.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                i = Math.max(this.mPreviewText.getMeasuredWidth(), localKey.width + this.mPreviewText.getPaddingLeft() + this.mPreviewText.getPaddingRight());
                j = this.mPreviewHeight;
                ViewGroup.LayoutParams localLayoutParams = this.mPreviewText.getLayoutParams();
                if (localLayoutParams != null)
                {
                    localLayoutParams.width = i;
                    localLayoutParams.height = j;
                }
                if (this.mPreviewCentered)
                    break label537;
                this.mPopupPreviewX = (localKey.x - this.mPreviewText.getPaddingLeft() + this.mPaddingLeft);
                this.mPopupPreviewY = (localKey.y - j + this.mPreviewOffset);
                this.mHandler.removeMessages(2);
                getLocationInWindow(this.mCoordinates);
                int[] arrayOfInt1 = this.mCoordinates;
                arrayOfInt1[0] += this.mMiniKeyboardOffsetX;
                int[] arrayOfInt2 = this.mCoordinates;
                arrayOfInt2[1] += this.mMiniKeyboardOffsetY;
                Drawable localDrawable1 = this.mPreviewText.getBackground();
                if (localKey.popupResId == 0)
                    break label569;
                arrayOfInt3 = LONG_PRESSABLE_STATE_SET;
                label282: localDrawable1.setState(arrayOfInt3);
                this.mPopupPreviewX += this.mCoordinates[0];
                this.mPopupPreviewY += this.mCoordinates[1];
                getLocationOnScreen(this.mCoordinates);
                if (this.mPopupPreviewY + this.mCoordinates[1] < 0)
                {
                    if (localKey.x + localKey.width > getWidth() / 2)
                        break label577;
                    this.mPopupPreviewX += (int)(2.5D * localKey.width);
                    label382: this.mPopupPreviewY = (j + this.mPopupPreviewY);
                }
                if (!localPopupWindow.isShowing())
                    break label600;
                localPopupWindow.update(this.mPopupPreviewX, this.mPopupPreviewY, i, j);
            }
        }
        while (true)
        {
            this.mPreviewText.setVisibility(0);
            break;
            localDrawable2 = localKey.icon;
            break label58;
            this.mPreviewText.setCompoundDrawables(null, null, null, null);
            this.mPreviewText.setText(getPreviewText(localKey));
            if ((localKey.label.length() > 1) && (localKey.codes.length < 2))
            {
                this.mPreviewText.setTextSize(0, this.mKeyTextSize);
                this.mPreviewText.setTypeface(Typeface.DEFAULT_BOLD);
                break label76;
            }
            this.mPreviewText.setTextSize(0, this.mPreviewTextSizeLarge);
            this.mPreviewText.setTypeface(Typeface.DEFAULT);
            break label76;
            label537: this.mPopupPreviewX = (160 - this.mPreviewText.getMeasuredWidth() / 2);
            this.mPopupPreviewY = (-this.mPreviewText.getMeasuredHeight());
            break label206;
            label569: arrayOfInt3 = EMPTY_STATE_SET;
            break label282;
            label577: this.mPopupPreviewX -= (int)(2.5D * localKey.width);
            break label382;
            label600: localPopupWindow.setWidth(i);
            localPopupWindow.setHeight(j);
            localPopupWindow.showAtLocation(this.mPopupParent, 0, this.mPopupPreviewX, this.mPopupPreviewY);
        }
    }

    private void showPreview(int paramInt)
    {
        int i = this.mCurrentKeyIndex;
        PopupWindow localPopupWindow = this.mPreviewPopup;
        this.mCurrentKeyIndex = paramInt;
        Keyboard.Key[] arrayOfKey = this.mKeys;
        boolean bool;
        if (i != this.mCurrentKeyIndex)
        {
            if ((i != -1) && (arrayOfKey.length > i))
            {
                Keyboard.Key localKey2 = arrayOfKey[i];
                if (this.mCurrentKeyIndex != -1)
                    break label251;
                bool = true;
                localKey2.onReleased(bool);
                invalidateKey(i);
                int k = localKey2.codes[0];
                sendAccessibilityEventForUnicodeCharacter(256, k);
                sendAccessibilityEventForUnicodeCharacter(65536, k);
            }
            if ((this.mCurrentKeyIndex != -1) && (arrayOfKey.length > this.mCurrentKeyIndex))
            {
                Keyboard.Key localKey1 = arrayOfKey[this.mCurrentKeyIndex];
                localKey1.onPressed();
                invalidateKey(this.mCurrentKeyIndex);
                int j = localKey1.codes[0];
                sendAccessibilityEventForUnicodeCharacter(128, j);
                sendAccessibilityEventForUnicodeCharacter(32768, j);
            }
        }
        if ((i != this.mCurrentKeyIndex) && (this.mShowPreview))
        {
            this.mHandler.removeMessages(1);
            if ((localPopupWindow.isShowing()) && (paramInt == -1))
                this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(2), 70L);
            if (paramInt != -1)
            {
                if ((!localPopupWindow.isShowing()) || (this.mPreviewText.getVisibility() != 0))
                    break label257;
                showKey(paramInt);
            }
        }
        while (true)
        {
            return;
            label251: bool = false;
            break;
            label257: this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1, paramInt, 0), 0L);
        }
    }

    public void closing()
    {
        if (this.mPreviewPopup.isShowing())
            this.mPreviewPopup.dismiss();
        removeMessages();
        dismissPopupKeyboard();
        this.mBuffer = null;
        this.mCanvas = null;
        this.mMiniKeyboardCache.clear();
    }

    public Keyboard getKeyboard()
    {
        return this.mKeyboard;
    }

    protected OnKeyboardActionListener getOnKeyboardActionListener()
    {
        return this.mKeyboardActionListener;
    }

    public boolean handleBack()
    {
        if (this.mPopupKeyboard.isShowing())
            dismissPopupKeyboard();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void invalidateAllKeys()
    {
        this.mDirtyRect.union(0, 0, getWidth(), getHeight());
        this.mDrawPending = true;
        invalidate();
    }

    public void invalidateKey(int paramInt)
    {
        if (this.mKeys == null);
        while (true)
        {
            return;
            if ((paramInt >= 0) && (paramInt < this.mKeys.length))
            {
                Keyboard.Key localKey = this.mKeys[paramInt];
                this.mInvalidatedKey = localKey;
                this.mDirtyRect.union(localKey.x + this.mPaddingLeft, localKey.y + this.mPaddingTop, localKey.x + localKey.width + this.mPaddingLeft, localKey.y + localKey.height + this.mPaddingTop);
                onBufferDraw();
                invalidate(localKey.x + this.mPaddingLeft, localKey.y + this.mPaddingTop, localKey.x + localKey.width + this.mPaddingLeft, localKey.y + localKey.height + this.mPaddingTop);
            }
        }
    }

    public boolean isPreviewEnabled()
    {
        return this.mShowPreview;
    }

    public boolean isProximityCorrectionEnabled()
    {
        return this.mProximityCorrectOn;
    }

    public boolean isShifted()
    {
        if (this.mKeyboard != null);
        for (boolean bool = this.mKeyboard.isShifted(); ; bool = false)
            return bool;
    }

    public void onClick(View paramView)
    {
        dismissPopupKeyboard();
    }

    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        closing();
    }

    public void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        if ((this.mDrawPending) || (this.mBuffer == null) || (this.mKeyboardChanged))
            onBufferDraw();
        paramCanvas.drawBitmap(this.mBuffer, 0.0F, 0.0F, null);
    }

    public boolean onHoverEvent(MotionEvent paramMotionEvent)
    {
        int i = 1;
        if ((this.mAccessibilityManager.isTouchExplorationEnabled()) && (paramMotionEvent.getPointerCount() == i))
            switch (paramMotionEvent.getAction())
            {
            case 8:
            default:
            case 9:
            case 7:
            case 10:
            }
        while (true)
        {
            int j = onTouchEvent(paramMotionEvent);
            return j;
            paramMotionEvent.setAction(0);
            continue;
            paramMotionEvent.setAction(2);
            continue;
            paramMotionEvent.setAction(j);
        }
    }

    protected boolean onLongPress(Keyboard.Key paramKey)
    {
        boolean bool = false;
        int i = paramKey.popupResId;
        Keyboard localKeyboard;
        label209: int j;
        int k;
        KeyboardView localKeyboardView;
        if (i != 0)
        {
            this.mMiniKeyboardContainer = ((View)this.mMiniKeyboardCache.get(paramKey));
            if (this.mMiniKeyboardContainer != null)
                break label439;
            this.mMiniKeyboardContainer = ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(this.mPopupLayout, null);
            this.mMiniKeyboard = ((KeyboardView)this.mMiniKeyboardContainer.findViewById(16908326));
            View localView = this.mMiniKeyboardContainer.findViewById(16908327);
            if (localView != null)
                localView.setOnClickListener(this);
            this.mMiniKeyboard.setOnKeyboardActionListener(new OnKeyboardActionListener()
            {
                public void onKey(int paramAnonymousInt, int[] paramAnonymousArrayOfInt)
                {
                    KeyboardView.this.mKeyboardActionListener.onKey(paramAnonymousInt, paramAnonymousArrayOfInt);
                    KeyboardView.this.dismissPopupKeyboard();
                }

                public void onPress(int paramAnonymousInt)
                {
                    KeyboardView.this.mKeyboardActionListener.onPress(paramAnonymousInt);
                }

                public void onRelease(int paramAnonymousInt)
                {
                    KeyboardView.this.mKeyboardActionListener.onRelease(paramAnonymousInt);
                }

                public void onText(CharSequence paramAnonymousCharSequence)
                {
                    KeyboardView.this.mKeyboardActionListener.onText(paramAnonymousCharSequence);
                    KeyboardView.this.dismissPopupKeyboard();
                }

                public void swipeDown()
                {
                }

                public void swipeLeft()
                {
                }

                public void swipeRight()
                {
                }

                public void swipeUp()
                {
                }
            });
            if (paramKey.popupCharacters == null)
                break label422;
            localKeyboard = new Keyboard(getContext(), i, paramKey.popupCharacters, -1, getPaddingLeft() + getPaddingRight());
            this.mMiniKeyboard.setKeyboard(localKeyboard);
            this.mMiniKeyboard.setPopupParent(this);
            this.mMiniKeyboardContainer.measure(View.MeasureSpec.makeMeasureSpec(getWidth(), -2147483648), View.MeasureSpec.makeMeasureSpec(getHeight(), -2147483648));
            this.mMiniKeyboardCache.put(paramKey, this.mMiniKeyboardContainer);
            getLocationInWindow(this.mCoordinates);
            this.mPopupX = (paramKey.x + this.mPaddingLeft);
            this.mPopupY = (paramKey.y + this.mPaddingTop);
            this.mPopupX = (this.mPopupX + paramKey.width - this.mMiniKeyboardContainer.getMeasuredWidth());
            this.mPopupY -= this.mMiniKeyboardContainer.getMeasuredHeight();
            j = this.mPopupX + this.mMiniKeyboardContainer.getPaddingRight() + this.mCoordinates[0];
            k = this.mPopupY + this.mMiniKeyboardContainer.getPaddingBottom() + this.mCoordinates[1];
            localKeyboardView = this.mMiniKeyboard;
            if (j >= 0)
                break label459;
        }
        label422: label439: label459: for (int m = 0; ; m = j)
        {
            localKeyboardView.setPopupOffset(m, k);
            this.mMiniKeyboard.setShifted(isShifted());
            this.mPopupKeyboard.setContentView(this.mMiniKeyboardContainer);
            this.mPopupKeyboard.setWidth(this.mMiniKeyboardContainer.getMeasuredWidth());
            this.mPopupKeyboard.setHeight(this.mMiniKeyboardContainer.getMeasuredHeight());
            this.mPopupKeyboard.showAtLocation(this, 0, j, k);
            this.mMiniKeyboardOnScreen = true;
            invalidateAllKeys();
            bool = true;
            return bool;
            localKeyboard = new Keyboard(getContext(), i);
            break;
            this.mMiniKeyboard = ((KeyboardView)this.mMiniKeyboardContainer.findViewById(16908326));
            break label209;
        }
    }

    public void onMeasure(int paramInt1, int paramInt2)
    {
        if (this.mKeyboard == null)
            setMeasuredDimension(this.mPaddingLeft + this.mPaddingRight, this.mPaddingTop + this.mPaddingBottom);
        while (true)
        {
            return;
            int i = this.mKeyboard.getMinWidth() + this.mPaddingLeft + this.mPaddingRight;
            if (View.MeasureSpec.getSize(paramInt1) < i + 10)
                i = View.MeasureSpec.getSize(paramInt1);
            setMeasuredDimension(i, this.mKeyboard.getHeight() + this.mPaddingTop + this.mPaddingBottom);
        }
    }

    public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        if (this.mKeyboard != null)
            this.mKeyboard.resize(paramInt1, paramInt2);
        this.mBuffer = null;
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getPointerCount();
        int j = paramMotionEvent.getAction();
        long l = paramMotionEvent.getEventTime();
        boolean bool;
        if (i != this.mOldPointerCount)
            if (i == 1)
            {
                MotionEvent localMotionEvent2 = MotionEvent.obtain(l, l, 0, paramMotionEvent.getX(), paramMotionEvent.getY(), paramMotionEvent.getMetaState());
                bool = onModifiedTouchEvent(localMotionEvent2, false);
                localMotionEvent2.recycle();
                if (j == 1)
                    bool = onModifiedTouchEvent(paramMotionEvent, true);
            }
        while (true)
        {
            this.mOldPointerCount = i;
            return bool;
            MotionEvent localMotionEvent1 = MotionEvent.obtain(l, l, 1, this.mOldPointerX, this.mOldPointerY, paramMotionEvent.getMetaState());
            bool = onModifiedTouchEvent(localMotionEvent1, true);
            localMotionEvent1.recycle();
            continue;
            if (i == 1)
            {
                bool = onModifiedTouchEvent(paramMotionEvent, false);
                this.mOldPointerX = paramMotionEvent.getX();
                this.mOldPointerY = paramMotionEvent.getY();
            }
            else
            {
                bool = true;
            }
        }
    }

    public void setKeyboard(Keyboard paramKeyboard)
    {
        if (this.mKeyboard != null)
            showPreview(-1);
        removeMessages();
        this.mKeyboard = paramKeyboard;
        List localList = this.mKeyboard.getKeys();
        this.mKeys = ((Keyboard.Key[])localList.toArray(new Keyboard.Key[localList.size()]));
        requestLayout();
        this.mKeyboardChanged = true;
        invalidateAllKeys();
        computeProximityThreshold(paramKeyboard);
        this.mMiniKeyboardCache.clear();
        this.mAbortKey = true;
    }

    public void setOnKeyboardActionListener(OnKeyboardActionListener paramOnKeyboardActionListener)
    {
        this.mKeyboardActionListener = paramOnKeyboardActionListener;
    }

    public void setPopupOffset(int paramInt1, int paramInt2)
    {
        this.mMiniKeyboardOffsetX = paramInt1;
        this.mMiniKeyboardOffsetY = paramInt2;
        if (this.mPreviewPopup.isShowing())
            this.mPreviewPopup.dismiss();
    }

    public void setPopupParent(View paramView)
    {
        this.mPopupParent = paramView;
    }

    public void setPreviewEnabled(boolean paramBoolean)
    {
        this.mShowPreview = paramBoolean;
    }

    public void setProximityCorrectionEnabled(boolean paramBoolean)
    {
        this.mProximityCorrectOn = paramBoolean;
    }

    public boolean setShifted(boolean paramBoolean)
    {
        if ((this.mKeyboard != null) && (this.mKeyboard.setShifted(paramBoolean)))
            invalidateAllKeys();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setVerticalCorrection(int paramInt)
    {
    }

    protected void swipeDown()
    {
        this.mKeyboardActionListener.swipeDown();
    }

    protected void swipeLeft()
    {
        this.mKeyboardActionListener.swipeLeft();
    }

    protected void swipeRight()
    {
        this.mKeyboardActionListener.swipeRight();
    }

    protected void swipeUp()
    {
        this.mKeyboardActionListener.swipeUp();
    }

    private static class SwipeTracker
    {
        static final int LONGEST_PAST_TIME = 200;
        static final int NUM_PAST = 4;
        final long[] mPastTime = new long[4];
        final float[] mPastX = new float[4];
        final float[] mPastY = new float[4];
        float mXVelocity;
        float mYVelocity;

        private void addPoint(float paramFloat1, float paramFloat2, long paramLong)
        {
            int i = -1;
            long[] arrayOfLong = this.mPastTime;
            for (int j = 0; ; j++)
            {
                if ((j >= 4) || (arrayOfLong[j] == 0L))
                {
                    if ((j == 4) && (i < 0))
                        i = 0;
                    if (i == j)
                        i--;
                    float[] arrayOfFloat1 = this.mPastX;
                    float[] arrayOfFloat2 = this.mPastY;
                    if (i >= 0)
                    {
                        int m = i + 1;
                        int n = -1 + (4 - i);
                        System.arraycopy(arrayOfFloat1, m, arrayOfFloat1, 0, n);
                        System.arraycopy(arrayOfFloat2, m, arrayOfFloat2, 0, n);
                        System.arraycopy(arrayOfLong, m, arrayOfLong, 0, n);
                        j -= i + 1;
                    }
                    arrayOfFloat1[j] = paramFloat1;
                    arrayOfFloat2[j] = paramFloat2;
                    arrayOfLong[j] = paramLong;
                    int k = j + 1;
                    if (k < 4)
                        arrayOfLong[k] = 0L;
                    return;
                }
                if (arrayOfLong[j] < paramLong - 200L)
                    i = j;
            }
        }

        public void addMovement(MotionEvent paramMotionEvent)
        {
            long l = paramMotionEvent.getEventTime();
            int i = paramMotionEvent.getHistorySize();
            for (int j = 0; j < i; j++)
                addPoint(paramMotionEvent.getHistoricalX(j), paramMotionEvent.getHistoricalY(j), paramMotionEvent.getHistoricalEventTime(j));
            addPoint(paramMotionEvent.getX(), paramMotionEvent.getY(), l);
        }

        public void clear()
        {
            this.mPastTime[0] = 0L;
        }

        public void computeCurrentVelocity(int paramInt)
        {
            computeCurrentVelocity(paramInt, 3.4028235E+38F);
        }

        public void computeCurrentVelocity(int paramInt, float paramFloat)
        {
            float[] arrayOfFloat1 = this.mPastX;
            float[] arrayOfFloat2 = this.mPastY;
            long[] arrayOfLong = this.mPastTime;
            float f1 = arrayOfFloat1[0];
            float f2 = arrayOfFloat2[0];
            long l = arrayOfLong[0];
            float f3 = 0.0F;
            float f4 = 0.0F;
            int i = 0;
            int j;
            label62: int k;
            if ((i >= 4) || (arrayOfLong[i] == 0L))
            {
                j = 1;
                if (j >= i)
                    break label181;
                k = (int)(arrayOfLong[j] - l);
                if (k != 0)
                    break label97;
            }
            while (true)
            {
                j++;
                break label62;
                i++;
                break;
                label97: float f7 = (arrayOfFloat1[j] - f1) / k * paramInt;
                if (f3 == 0.0F);
                float f8;
                for (f3 = f7; ; f3 = 0.5F * (f3 + f7))
                {
                    f8 = (arrayOfFloat2[j] - f2) / k * paramInt;
                    if (f4 != 0.0F)
                        break label168;
                    f4 = f8;
                    break;
                }
                label168: f4 = 0.5F * (f4 + f8);
            }
            label181: float f5;
            if (f3 < 0.0F)
            {
                f5 = Math.max(f3, -paramFloat);
                this.mXVelocity = f5;
                if (f4 >= 0.0F)
                    break label237;
            }
            label237: for (float f6 = Math.max(f4, -paramFloat); ; f6 = Math.min(f4, paramFloat))
            {
                this.mYVelocity = f6;
                return;
                f5 = Math.min(f3, paramFloat);
                break;
            }
        }

        public float getXVelocity()
        {
            return this.mXVelocity;
        }

        public float getYVelocity()
        {
            return this.mYVelocity;
        }
    }

    public static abstract interface OnKeyboardActionListener
    {
        public abstract void onKey(int paramInt, int[] paramArrayOfInt);

        public abstract void onPress(int paramInt);

        public abstract void onRelease(int paramInt);

        public abstract void onText(CharSequence paramCharSequence);

        public abstract void swipeDown();

        public abstract void swipeLeft();

        public abstract void swipeRight();

        public abstract void swipeUp();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.inputmethodservice.KeyboardView
 * JD-Core Version:        0.6.2
 */