package android.inputmethodservice;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class ExtractEditText extends EditText
{
    private InputMethodService mIME;
    private int mSettingExtractedText;

    public ExtractEditText(Context paramContext)
    {
        super(paramContext, null);
    }

    public ExtractEditText(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet, 16842862);
    }

    public ExtractEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    protected void deleteText_internal(int paramInt1, int paramInt2)
    {
        this.mIME.onExtractedDeleteText(paramInt1, paramInt2);
    }

    public void finishInternalChanges()
    {
        this.mSettingExtractedText = (-1 + this.mSettingExtractedText);
    }

    public boolean hasFocus()
    {
        return isEnabled();
    }

    public boolean hasVerticalScrollBar()
    {
        if (computeVerticalScrollRange() > computeVerticalScrollExtent());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasWindowFocus()
    {
        return isEnabled();
    }

    public boolean isFocused()
    {
        return isEnabled();
    }

    public boolean isInputMethodTarget()
    {
        return true;
    }

    protected void onSelectionChanged(int paramInt1, int paramInt2)
    {
        if ((this.mSettingExtractedText == 0) && (this.mIME != null) && (paramInt1 >= 0) && (paramInt2 >= 0))
            this.mIME.onExtractedSelectionChanged(paramInt1, paramInt2);
    }

    public boolean onTextContextMenuItem(int paramInt)
    {
        if ((this.mIME != null) && (this.mIME.onExtractTextContextMenuItem(paramInt)))
            if (paramInt == 16908321)
                stopSelectionActionMode();
        for (boolean bool = true; ; bool = super.onTextContextMenuItem(paramInt))
            return bool;
    }

    public boolean performClick()
    {
        if ((!super.performClick()) && (this.mIME != null))
            this.mIME.onExtractedTextClicked();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void replaceText_internal(int paramInt1, int paramInt2, CharSequence paramCharSequence)
    {
        this.mIME.onExtractedReplaceText(paramInt1, paramInt2, paramCharSequence);
    }

    protected void setCursorPosition_internal(int paramInt1, int paramInt2)
    {
        this.mIME.onExtractedSelectionChanged(paramInt1, paramInt2);
    }

    public void setExtractedText(ExtractedText paramExtractedText)
    {
        try
        {
            this.mSettingExtractedText = (1 + this.mSettingExtractedText);
            super.setExtractedText(paramExtractedText);
            return;
        }
        finally
        {
            this.mSettingExtractedText = (-1 + this.mSettingExtractedText);
        }
    }

    void setIME(InputMethodService paramInputMethodService)
    {
        this.mIME = paramInputMethodService;
    }

    protected void setSpan_internal(Object paramObject, int paramInt1, int paramInt2, int paramInt3)
    {
        this.mIME.onExtractedSetSpan(paramObject, paramInt1, paramInt2, paramInt3);
    }

    public void startInternalChanges()
    {
        this.mSettingExtractedText = (1 + this.mSettingExtractedText);
    }

    protected void viewClicked(InputMethodManager paramInputMethodManager)
    {
        if (this.mIME != null)
            this.mIME.onViewClicked(false);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.inputmethodservice.ExtractEditText
 * JD-Core Version:        0.6.2
 */