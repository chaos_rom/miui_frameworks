package android.inputmethodservice;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.IBinder;
import android.view.KeyEvent.DispatcherState;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

class SoftInputWindow extends Dialog
{
    private final Rect mBounds = new Rect();
    final KeyEvent.DispatcherState mDispatcherState;

    public SoftInputWindow(Context paramContext, int paramInt, KeyEvent.DispatcherState paramDispatcherState)
    {
        super(paramContext, paramInt);
        this.mDispatcherState = paramDispatcherState;
        initDockWindow();
    }

    private void initDockWindow()
    {
        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        localLayoutParams.type = 2011;
        localLayoutParams.setTitle("InputMethod");
        localLayoutParams.gravity = 80;
        localLayoutParams.width = -1;
        getWindow().setAttributes(localLayoutParams);
        getWindow().setFlags(264, 266);
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        getWindow().getDecorView().getHitRect(this.mBounds);
        boolean bool;
        if (paramMotionEvent.isWithinBoundsNoHistory(this.mBounds.left, this.mBounds.top, -1 + this.mBounds.right, -1 + this.mBounds.bottom))
            bool = super.dispatchTouchEvent(paramMotionEvent);
        while (true)
        {
            return bool;
            MotionEvent localMotionEvent = paramMotionEvent.clampNoHistory(this.mBounds.left, this.mBounds.top, -1 + this.mBounds.right, -1 + this.mBounds.bottom);
            bool = super.dispatchTouchEvent(localMotionEvent);
            localMotionEvent.recycle();
        }
    }

    public int getSize()
    {
        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        if ((localLayoutParams.gravity == 48) || (localLayoutParams.gravity == 80));
        for (int i = localLayoutParams.height; ; i = localLayoutParams.width)
            return i;
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        super.onWindowFocusChanged(paramBoolean);
        this.mDispatcherState.reset();
    }

    public void setGravity(int paramInt)
    {
        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        int i;
        if ((localLayoutParams.gravity == 48) || (localLayoutParams.gravity == 80))
        {
            i = 1;
            localLayoutParams.gravity = paramInt;
            if ((localLayoutParams.gravity != 48) && (localLayoutParams.gravity != 80))
                break label94;
        }
        label94: for (int j = 1; ; j = 0)
        {
            if (i != j)
            {
                int k = localLayoutParams.width;
                localLayoutParams.width = localLayoutParams.height;
                localLayoutParams.height = k;
                getWindow().setAttributes(localLayoutParams);
            }
            return;
            i = 0;
            break;
        }
    }

    public void setSize(int paramInt)
    {
        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        if ((localLayoutParams.gravity == 48) || (localLayoutParams.gravity == 80))
            localLayoutParams.width = -1;
        for (localLayoutParams.height = paramInt; ; localLayoutParams.height = -1)
        {
            getWindow().setAttributes(localLayoutParams);
            return;
            localLayoutParams.width = paramInt;
        }
    }

    public void setToken(IBinder paramIBinder)
    {
        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        localLayoutParams.token = paramIBinder;
        getWindow().setAttributes(localLayoutParams);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.inputmethodservice.SoftInputWindow
 * JD-Core Version:        0.6.2
 */