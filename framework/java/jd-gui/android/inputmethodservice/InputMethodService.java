package android.inputmethodservice;

import android.R.styleable;
import android.app.Dialog;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.provider.Settings.System;
import android.text.Editable;
import android.text.Layout;
import android.text.Spannable;
import android.text.method.MovementMethod;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.util.Printer;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.InternalInsetsInfo;
import android.view.ViewTreeObserver.OnComputeInternalInsetsListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout.LayoutParams;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class InputMethodService extends AbstractInputMethodService
{
    public static final int BACK_DISPOSITION_DEFAULT = 0;
    public static final int BACK_DISPOSITION_WILL_DISMISS = 2;
    public static final int BACK_DISPOSITION_WILL_NOT_DISMISS = 1;
    static final boolean DEBUG = false;
    public static final int IME_ACTIVE = 1;
    public static final int IME_VISIBLE = 2;
    static final int MOVEMENT_DOWN = -1;
    static final int MOVEMENT_UP = -2;
    static final String TAG = "InputMethodService";
    final View.OnClickListener mActionClickListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            EditorInfo localEditorInfo = InputMethodService.this.getCurrentInputEditorInfo();
            InputConnection localInputConnection = InputMethodService.this.getCurrentInputConnection();
            if ((localEditorInfo != null) && (localInputConnection != null))
            {
                if (localEditorInfo.actionId == 0)
                    break label43;
                localInputConnection.performEditorAction(localEditorInfo.actionId);
            }
            while (true)
            {
                return;
                label43: if ((0xFF & localEditorInfo.imeOptions) != 1)
                    localInputConnection.performEditorAction(0xFF & localEditorInfo.imeOptions);
            }
        }
    };
    int mBackDisposition;
    FrameLayout mCandidatesFrame;
    boolean mCandidatesViewStarted;
    int mCandidatesVisibility;
    CompletionInfo[] mCurCompletions;
    ViewGroup mExtractAccessories;
    Button mExtractAction;
    ExtractEditText mExtractEditText;
    FrameLayout mExtractFrame;
    View mExtractView;
    boolean mExtractViewHidden;
    ExtractedText mExtractedText;
    int mExtractedToken;
    boolean mFullscreenApplied;
    ViewGroup mFullscreenArea;
    InputMethodManager mImm;
    boolean mInShowWindow;
    LayoutInflater mInflater;
    boolean mInitialized;
    InputBinding mInputBinding;
    InputConnection mInputConnection;
    EditorInfo mInputEditorInfo;
    FrameLayout mInputFrame;
    boolean mInputStarted;
    View mInputView;
    boolean mInputViewStarted;
    final ViewTreeObserver.OnComputeInternalInsetsListener mInsetsComputer = new ViewTreeObserver.OnComputeInternalInsetsListener()
    {
        public void onComputeInternalInsets(ViewTreeObserver.InternalInsetsInfo paramAnonymousInternalInsetsInfo)
        {
            if (InputMethodService.this.isExtractViewShown())
            {
                View localView = InputMethodService.this.getWindow().getWindow().getDecorView();
                Rect localRect1 = paramAnonymousInternalInsetsInfo.contentInsets;
                Rect localRect2 = paramAnonymousInternalInsetsInfo.visibleInsets;
                int i = localView.getHeight();
                localRect2.top = i;
                localRect1.top = i;
                paramAnonymousInternalInsetsInfo.touchableRegion.setEmpty();
                paramAnonymousInternalInsetsInfo.setTouchableInsets(0);
            }
            while (true)
            {
                return;
                InputMethodService.this.onComputeInsets(InputMethodService.this.mTmpInsets);
                paramAnonymousInternalInsetsInfo.contentInsets.top = InputMethodService.this.mTmpInsets.contentTopInsets;
                paramAnonymousInternalInsetsInfo.visibleInsets.top = InputMethodService.this.mTmpInsets.visibleTopInsets;
                paramAnonymousInternalInsetsInfo.touchableRegion.set(InputMethodService.this.mTmpInsets.touchableRegion);
                paramAnonymousInternalInsetsInfo.setTouchableInsets(InputMethodService.this.mTmpInsets.touchableInsets);
            }
        }
    };
    boolean mIsFullscreen;
    boolean mIsInputViewShown;
    boolean mLastShowInputRequested;
    View mRootView;
    int mShowInputFlags;
    boolean mShowInputForced;
    boolean mShowInputRequested;
    InputConnection mStartedInputConnection;
    int mStatusIcon;
    int mTheme = 0;
    TypedArray mThemeAttrs;
    final Insets mTmpInsets = new Insets();
    final int[] mTmpLocation = new int[2];
    IBinder mToken;
    SoftInputWindow mWindow;
    boolean mWindowAdded;
    boolean mWindowCreated;
    boolean mWindowVisible;
    boolean mWindowWasVisible;

    private void doHideWindow()
    {
        this.mImm.setImeWindowStatus(this.mToken, 0, this.mBackDisposition);
        hideWindow();
    }

    private void finishViews()
    {
        if (this.mInputViewStarted)
            onFinishInputView(false);
        while (true)
        {
            this.mInputViewStarted = false;
            this.mCandidatesViewStarted = false;
            return;
            if (this.mCandidatesViewStarted)
                onFinishCandidatesView(false);
        }
    }

    private boolean handleBack(boolean paramBoolean)
    {
        boolean bool = true;
        if (this.mShowInputRequested)
            if ((isExtractViewShown()) && ((this.mExtractView instanceof ExtractEditLayout)))
            {
                ExtractEditLayout localExtractEditLayout = (ExtractEditLayout)this.mExtractView;
                if (localExtractEditLayout.isActionModeStarted())
                    if (paramBoolean)
                        localExtractEditLayout.finishActionMode();
            }
        while (true)
        {
            return bool;
            if (paramBoolean)
            {
                requestHideSelf(0);
                continue;
                if (this.mWindowVisible)
                {
                    if (this.mCandidatesVisibility == 0)
                    {
                        if (paramBoolean)
                            setCandidatesViewShown(false);
                    }
                    else if (paramBoolean)
                        doHideWindow();
                }
                else
                    bool = false;
            }
        }
    }

    private void onToggleSoftInput(int paramInt1, int paramInt2)
    {
        if (isInputViewShown())
            requestHideSelf(paramInt2);
        while (true)
        {
            return;
            requestShowSelf(paramInt1);
        }
    }

    private void requestShowSelf(int paramInt)
    {
        this.mImm.showSoftInputFromInputMethod(this.mToken, paramInt);
    }

    void doFinishInput()
    {
        if (this.mInputViewStarted)
            onFinishInputView(true);
        while (true)
        {
            this.mInputViewStarted = false;
            this.mCandidatesViewStarted = false;
            if (this.mInputStarted)
                onFinishInput();
            this.mInputStarted = false;
            this.mStartedInputConnection = null;
            this.mCurCompletions = null;
            return;
            if (this.mCandidatesViewStarted)
                onFinishCandidatesView(true);
        }
    }

    boolean doMovementKey(int paramInt1, KeyEvent paramKeyEvent, int paramInt2)
    {
        int i = 1;
        ExtractEditText localExtractEditText = this.mExtractEditText;
        MovementMethod localMovementMethod;
        if ((isExtractViewShown()) && (isInputViewShown()) && (localExtractEditText != null))
        {
            localMovementMethod = localExtractEditText.getMovementMethod();
            Layout localLayout = localExtractEditText.getLayout();
            if ((localMovementMethod != null) && (localLayout != null))
                if (paramInt2 == -1)
                {
                    if (localMovementMethod.onKeyDown(localExtractEditText, localExtractEditText.getText(), paramInt1, paramKeyEvent))
                        reportExtractedMovement(paramInt1, i);
                }
                else
                    do
                    {
                        return i;
                        if (paramInt2 != -2)
                            break;
                    }
                    while (localMovementMethod.onKeyUp(localExtractEditText, localExtractEditText.getText(), paramInt1, paramKeyEvent));
        }
        while (true)
        {
            switch (paramInt1)
            {
            case 19:
            case 20:
            case 21:
            case 22:
            }
            int j = 0;
            break;
            if (localMovementMethod.onKeyOther(localExtractEditText, localExtractEditText.getText(), paramKeyEvent))
            {
                reportExtractedMovement(paramInt1, paramInt2);
            }
            else
            {
                KeyEvent localKeyEvent1 = KeyEvent.changeAction(paramKeyEvent, 0);
                if (localMovementMethod.onKeyDown(localExtractEditText, localExtractEditText.getText(), paramInt1, localKeyEvent1))
                {
                    KeyEvent localKeyEvent2 = KeyEvent.changeAction(paramKeyEvent, j);
                    localMovementMethod.onKeyUp(localExtractEditText, localExtractEditText.getText(), paramInt1, localKeyEvent2);
                    while (true)
                    {
                        paramInt2--;
                        if (paramInt2 <= 0)
                            break;
                        localMovementMethod.onKeyDown(localExtractEditText, localExtractEditText.getText(), paramInt1, localKeyEvent1);
                        localMovementMethod.onKeyUp(localExtractEditText, localExtractEditText.getText(), paramInt1, localKeyEvent2);
                    }
                    reportExtractedMovement(paramInt1, paramInt2);
                }
            }
        }
    }

    void doStartInput(InputConnection paramInputConnection, EditorInfo paramEditorInfo, boolean paramBoolean)
    {
        if (!paramBoolean)
            doFinishInput();
        this.mInputStarted = true;
        this.mStartedInputConnection = paramInputConnection;
        this.mInputEditorInfo = paramEditorInfo;
        initialize();
        onStartInput(paramEditorInfo, paramBoolean);
        if (this.mWindowVisible)
        {
            if (!this.mShowInputRequested)
                break label67;
            this.mInputViewStarted = true;
            onStartInputView(this.mInputEditorInfo, paramBoolean);
            startExtractingText(true);
        }
        while (true)
        {
            return;
            label67: if (this.mCandidatesVisibility == 0)
            {
                this.mCandidatesViewStarted = true;
                onStartCandidatesView(this.mInputEditorInfo, paramBoolean);
            }
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        PrintWriterPrinter localPrintWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
        localPrintWriterPrinter.println("Input method service state for " + this + ":");
        localPrintWriterPrinter.println("    mWindowCreated=" + this.mWindowCreated + " mWindowAdded=" + this.mWindowAdded);
        localPrintWriterPrinter.println("    mWindowVisible=" + this.mWindowVisible + " mWindowWasVisible=" + this.mWindowWasVisible + " mInShowWindow=" + this.mInShowWindow);
        localPrintWriterPrinter.println("    Configuration=" + getResources().getConfiguration());
        localPrintWriterPrinter.println("    mToken=" + this.mToken);
        localPrintWriterPrinter.println("    mInputBinding=" + this.mInputBinding);
        localPrintWriterPrinter.println("    mInputConnection=" + this.mInputConnection);
        localPrintWriterPrinter.println("    mStartedInputConnection=" + this.mStartedInputConnection);
        localPrintWriterPrinter.println("    mInputStarted=" + this.mInputStarted + " mInputViewStarted=" + this.mInputViewStarted + " mCandidatesViewStarted=" + this.mCandidatesViewStarted);
        if (this.mInputEditorInfo != null)
        {
            localPrintWriterPrinter.println("    mInputEditorInfo:");
            this.mInputEditorInfo.dump(localPrintWriterPrinter, "        ");
            localPrintWriterPrinter.println("    mShowInputRequested=" + this.mShowInputRequested + " mLastShowInputRequested=" + this.mLastShowInputRequested + " mShowInputForced=" + this.mShowInputForced + " mShowInputFlags=0x" + Integer.toHexString(this.mShowInputFlags));
            localPrintWriterPrinter.println("    mCandidatesVisibility=" + this.mCandidatesVisibility + " mFullscreenApplied=" + this.mFullscreenApplied + " mIsFullscreen=" + this.mIsFullscreen + " mExtractViewHidden=" + this.mExtractViewHidden);
            if (this.mExtractedText == null)
                break label844;
            localPrintWriterPrinter.println("    mExtractedText:");
            localPrintWriterPrinter.println("        text=" + this.mExtractedText.text.length() + " chars" + " startOffset=" + this.mExtractedText.startOffset);
            localPrintWriterPrinter.println("        selectionStart=" + this.mExtractedText.selectionStart + " selectionEnd=" + this.mExtractedText.selectionEnd + " flags=0x" + Integer.toHexString(this.mExtractedText.flags));
        }
        while (true)
        {
            localPrintWriterPrinter.println("    mExtractedToken=" + this.mExtractedToken);
            localPrintWriterPrinter.println("    mIsInputViewShown=" + this.mIsInputViewShown + " mStatusIcon=" + this.mStatusIcon);
            localPrintWriterPrinter.println("Last computed insets:");
            localPrintWriterPrinter.println("    contentTopInsets=" + this.mTmpInsets.contentTopInsets + " visibleTopInsets=" + this.mTmpInsets.visibleTopInsets + " touchableInsets=" + this.mTmpInsets.touchableInsets + " touchableRegion=" + this.mTmpInsets.touchableRegion);
            return;
            localPrintWriterPrinter.println("    mInputEditorInfo: null");
            break;
            label844: localPrintWriterPrinter.println("    mExtractedText: null");
        }
    }

    public int getBackDisposition()
    {
        return this.mBackDisposition;
    }

    public int getCandidatesHiddenVisibility()
    {
        if (isExtractViewShown());
        for (int i = 8; ; i = 4)
            return i;
    }

    public InputBinding getCurrentInputBinding()
    {
        return this.mInputBinding;
    }

    public InputConnection getCurrentInputConnection()
    {
        InputConnection localInputConnection = this.mStartedInputConnection;
        if (localInputConnection != null);
        while (true)
        {
            return localInputConnection;
            localInputConnection = this.mInputConnection;
        }
    }

    public EditorInfo getCurrentInputEditorInfo()
    {
        return this.mInputEditorInfo;
    }

    public boolean getCurrentInputStarted()
    {
        return this.mInputStarted;
    }

    public LayoutInflater getLayoutInflater()
    {
        return this.mInflater;
    }

    public int getMaxWidth()
    {
        return ((WindowManager)getSystemService("window")).getDefaultDisplay().getWidth();
    }

    public CharSequence getTextForImeAction(int paramInt)
    {
        CharSequence localCharSequence;
        switch (paramInt & 0xFF)
        {
        default:
            localCharSequence = getText(17040491);
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            return localCharSequence;
            localCharSequence = null;
            continue;
            localCharSequence = getText(17040485);
            continue;
            localCharSequence = getText(17040486);
            continue;
            localCharSequence = getText(17040487);
            continue;
            localCharSequence = getText(17040488);
            continue;
            localCharSequence = getText(17040489);
            continue;
            localCharSequence = getText(17040490);
        }
    }

    public Dialog getWindow()
    {
        return this.mWindow;
    }

    public void hideStatusIcon()
    {
        this.mStatusIcon = 0;
        this.mImm.hideStatusIcon(this.mToken);
    }

    public void hideWindow()
    {
        finishViews();
        if (this.mWindowVisible)
        {
            this.mWindow.hide();
            this.mWindowVisible = false;
            onWindowHidden();
            this.mWindowWasVisible = false;
        }
    }

    void initViews()
    {
        this.mInitialized = false;
        this.mWindowCreated = false;
        this.mShowInputRequested = false;
        this.mShowInputForced = false;
        this.mThemeAttrs = obtainStyledAttributes(R.styleable.InputMethodService);
        this.mRootView = this.mInflater.inflate(17367111, null);
        this.mWindow.setContentView(this.mRootView);
        this.mRootView.getViewTreeObserver().addOnComputeInternalInsetsListener(this.mInsetsComputer);
        if (Settings.System.getInt(getContentResolver(), "fancy_ime_animations", 0) != 0)
            this.mWindow.getWindow().setWindowAnimations(16974309);
        this.mFullscreenArea = ((ViewGroup)this.mRootView.findViewById(16908940));
        this.mExtractViewHidden = false;
        this.mExtractFrame = ((FrameLayout)this.mRootView.findViewById(16908316));
        this.mExtractView = null;
        this.mExtractEditText = null;
        this.mExtractAccessories = null;
        this.mExtractAction = null;
        this.mFullscreenApplied = false;
        this.mCandidatesFrame = ((FrameLayout)this.mRootView.findViewById(16908317));
        this.mInputFrame = ((FrameLayout)this.mRootView.findViewById(16908318));
        this.mInputView = null;
        this.mIsInputViewShown = false;
        this.mExtractFrame.setVisibility(8);
        this.mCandidatesVisibility = getCandidatesHiddenVisibility();
        this.mCandidatesFrame.setVisibility(this.mCandidatesVisibility);
        this.mInputFrame.setVisibility(8);
    }

    void initialize()
    {
        if (!this.mInitialized)
        {
            this.mInitialized = true;
            onInitializeInterface();
        }
    }

    public boolean isExtractViewShown()
    {
        if ((this.mIsFullscreen) && (!this.mExtractViewHidden));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isFullscreenMode()
    {
        return this.mIsFullscreen;
    }

    public boolean isInputViewShown()
    {
        if ((this.mIsInputViewShown) && (this.mWindowVisible));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isShowInputRequested()
    {
        return this.mShowInputRequested;
    }

    public void onAppPrivateCommand(String paramString, Bundle paramBundle)
    {
    }

    public void onBindInput()
    {
    }

    public void onComputeInsets(Insets paramInsets)
    {
        int[] arrayOfInt = this.mTmpLocation;
        if (this.mInputFrame.getVisibility() == 0)
        {
            this.mInputFrame.getLocationInWindow(arrayOfInt);
            if (!isFullscreenMode())
                break label104;
        }
        label104: for (paramInsets.contentTopInsets = getWindow().getWindow().getDecorView().getHeight(); ; paramInsets.contentTopInsets = arrayOfInt[1])
        {
            if (this.mCandidatesFrame.getVisibility() == 0)
                this.mCandidatesFrame.getLocationInWindow(arrayOfInt);
            paramInsets.visibleTopInsets = arrayOfInt[1];
            paramInsets.touchableInsets = 2;
            paramInsets.touchableRegion.setEmpty();
            return;
            arrayOfInt[1] = getWindow().getWindow().getDecorView().getHeight();
            break;
        }
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
        int i = 0;
        super.onConfigurationChanged(paramConfiguration);
        boolean bool1 = this.mWindowVisible;
        int j = this.mShowInputFlags;
        boolean bool2 = this.mShowInputRequested;
        CompletionInfo[] arrayOfCompletionInfo = this.mCurCompletions;
        initViews();
        this.mInputViewStarted = false;
        this.mCandidatesViewStarted = false;
        if (this.mInputStarted)
            doStartInput(getCurrentInputConnection(), getCurrentInputEditorInfo(), true);
        if (bool1)
        {
            if (!bool2)
                break label152;
            if (!onShowInputRequested(j, true))
                break label145;
            showWindow(true);
            if (arrayOfCompletionInfo != null)
            {
                this.mCurCompletions = arrayOfCompletionInfo;
                onDisplayCompletions(arrayOfCompletionInfo);
            }
        }
        while (true)
        {
            boolean bool3 = onEvaluateInputViewShown();
            InputMethodManager localInputMethodManager = this.mImm;
            IBinder localIBinder = this.mToken;
            if (bool3)
                i = 2;
            localInputMethodManager.setImeWindowStatus(localIBinder, i | 0x1, this.mBackDisposition);
            return;
            label145: doHideWindow();
            continue;
            label152: if (this.mCandidatesVisibility == 0)
                showWindow(false);
            else
                doHideWindow();
        }
    }

    public void onConfigureWindow(Window paramWindow, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (paramBoolean1)
            this.mWindow.getWindow().setLayout(-1, -1);
        while (true)
        {
            return;
            this.mWindow.getWindow().setLayout(-1, -2);
        }
    }

    public void onCreate()
    {
        this.mTheme = Resources.selectSystemTheme(this.mTheme, getApplicationInfo().targetSdkVersion, 16973908, 16973951, 16974142);
        super.setTheme(this.mTheme);
        super.onCreate();
        this.mImm = ((InputMethodManager)getSystemService("input_method"));
        this.mInflater = ((LayoutInflater)getSystemService("layout_inflater"));
        this.mWindow = new SoftInputWindow(this, this.mTheme, this.mDispatcherState);
        initViews();
        this.mWindow.getWindow().setLayout(-1, -2);
    }

    public View onCreateCandidatesView()
    {
        return null;
    }

    public View onCreateExtractTextView()
    {
        return this.mInflater.inflate(17367112, null);
    }

    public AbstractInputMethodService.AbstractInputMethodImpl onCreateInputMethodInterface()
    {
        return new InputMethodImpl();
    }

    public AbstractInputMethodService.AbstractInputMethodSessionImpl onCreateInputMethodSessionInterface()
    {
        return new InputMethodSessionImpl();
    }

    public View onCreateInputView()
    {
        return null;
    }

    protected void onCurrentInputMethodSubtypeChanged(InputMethodSubtype paramInputMethodSubtype)
    {
    }

    public void onDestroy()
    {
        super.onDestroy();
        this.mRootView.getViewTreeObserver().removeOnComputeInternalInsetsListener(this.mInsetsComputer);
        finishViews();
        if (this.mWindowAdded)
        {
            this.mWindow.getWindow().setWindowAnimations(0);
            this.mWindow.dismiss();
        }
    }

    public void onDisplayCompletions(CompletionInfo[] paramArrayOfCompletionInfo)
    {
    }

    public boolean onEvaluateFullscreenMode()
    {
        boolean bool = false;
        if (getResources().getConfiguration().orientation != 2);
        while (true)
        {
            return bool;
            if ((this.mInputEditorInfo == null) || ((0x2000000 & this.mInputEditorInfo.imeOptions) == 0))
                bool = true;
        }
    }

    public boolean onEvaluateInputViewShown()
    {
        int i = 1;
        Configuration localConfiguration = getResources().getConfiguration();
        if ((localConfiguration.keyboard == i) || (localConfiguration.hardKeyboardHidden == 2));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean onExtractTextContextMenuItem(int paramInt)
    {
        InputConnection localInputConnection = getCurrentInputConnection();
        if (localInputConnection != null)
            localInputConnection.performContextMenuAction(paramInt);
        return true;
    }

    public void onExtractedCursorMovement(int paramInt1, int paramInt2)
    {
        if ((this.mExtractEditText == null) || (paramInt2 == 0));
        while (true)
        {
            return;
            if (this.mExtractEditText.hasVerticalScrollBar())
                setCandidatesViewShown(false);
        }
    }

    public void onExtractedDeleteText(int paramInt1, int paramInt2)
    {
        InputConnection localInputConnection = getCurrentInputConnection();
        if (localInputConnection != null)
        {
            localInputConnection.setSelection(paramInt1, paramInt1);
            localInputConnection.deleteSurroundingText(0, paramInt2 - paramInt1);
        }
    }

    public void onExtractedReplaceText(int paramInt1, int paramInt2, CharSequence paramCharSequence)
    {
        InputConnection localInputConnection = getCurrentInputConnection();
        if (localInputConnection != null)
        {
            localInputConnection.setComposingRegion(paramInt1, paramInt2);
            localInputConnection.commitText(paramCharSequence, 1);
        }
    }

    public void onExtractedSelectionChanged(int paramInt1, int paramInt2)
    {
        InputConnection localInputConnection = getCurrentInputConnection();
        if (localInputConnection != null)
            localInputConnection.setSelection(paramInt1, paramInt2);
    }

    public void onExtractedSetSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3)
    {
        InputConnection localInputConnection = getCurrentInputConnection();
        if ((localInputConnection == null) || (!localInputConnection.setSelection(paramInt1, paramInt2)));
        while (true)
        {
            return;
            CharSequence localCharSequence = localInputConnection.getSelectedText(1);
            if ((localCharSequence instanceof Spannable))
            {
                ((Spannable)localCharSequence).setSpan(paramObject, 0, localCharSequence.length(), paramInt3);
                localInputConnection.setComposingRegion(paramInt1, paramInt2);
                localInputConnection.commitText(localCharSequence, 1);
            }
        }
    }

    public void onExtractedTextClicked()
    {
        if (this.mExtractEditText == null);
        while (true)
        {
            return;
            if (this.mExtractEditText.hasVerticalScrollBar())
                setCandidatesViewShown(false);
        }
    }

    public void onExtractingInputChanged(EditorInfo paramEditorInfo)
    {
        if (paramEditorInfo.inputType == 0)
            requestHideSelf(2);
    }

    public void onFinishCandidatesView(boolean paramBoolean)
    {
        if (!paramBoolean)
        {
            InputConnection localInputConnection = getCurrentInputConnection();
            if (localInputConnection != null)
                localInputConnection.finishComposingText();
        }
    }

    public void onFinishInput()
    {
        InputConnection localInputConnection = getCurrentInputConnection();
        if (localInputConnection != null)
            localInputConnection.finishComposingText();
    }

    public void onFinishInputView(boolean paramBoolean)
    {
        if (!paramBoolean)
        {
            InputConnection localInputConnection = getCurrentInputConnection();
            if (localInputConnection != null)
                localInputConnection.finishComposingText();
        }
    }

    public void onInitializeInterface()
    {
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        if (paramKeyEvent.getKeyCode() == 4)
            if (handleBack(false))
                paramKeyEvent.startTracking();
        for (bool = true; ; bool = doMovementKey(paramInt, paramKeyEvent, -1))
            return bool;
    }

    public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent)
    {
        return false;
    }

    public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        return doMovementKey(paramInt1, paramKeyEvent, paramInt2);
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        if ((paramKeyEvent.getKeyCode() == 4) && (paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()));
        for (boolean bool = handleBack(true); ; bool = doMovementKey(paramInt, paramKeyEvent, -2))
            return bool;
    }

    public boolean onShowInputRequested(int paramInt, boolean paramBoolean)
    {
        boolean bool = false;
        if (!onEvaluateInputViewShown());
        while (true)
        {
            return bool;
            if (((paramInt & 0x1) != 0) || (((paramBoolean) || (!onEvaluateFullscreenMode())) && (getResources().getConfiguration().keyboard == 1)))
            {
                if ((paramInt & 0x2) != 0)
                    this.mShowInputForced = true;
                bool = true;
            }
        }
    }

    public void onStartCandidatesView(EditorInfo paramEditorInfo, boolean paramBoolean)
    {
    }

    public void onStartInput(EditorInfo paramEditorInfo, boolean paramBoolean)
    {
    }

    public void onStartInputView(EditorInfo paramEditorInfo, boolean paramBoolean)
    {
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        return false;
    }

    public void onUnbindInput()
    {
    }

    public void onUpdateCursor(Rect paramRect)
    {
    }

    public void onUpdateExtractedText(int paramInt, ExtractedText paramExtractedText)
    {
        if (this.mExtractedToken != paramInt);
        while (true)
        {
            return;
            if ((paramExtractedText != null) && (this.mExtractEditText != null))
            {
                this.mExtractedText = paramExtractedText;
                this.mExtractEditText.setExtractedText(paramExtractedText);
            }
        }
    }

    public void onUpdateExtractingViews(EditorInfo paramEditorInfo)
    {
        int i = 1;
        if (!isExtractViewShown());
        while (true)
        {
            return;
            if (this.mExtractAccessories != null)
                if ((paramEditorInfo.actionLabel != null) || (((0xFF & paramEditorInfo.imeOptions) != i) && ((0x20000000 & paramEditorInfo.imeOptions) == 0) && (paramEditorInfo.inputType != 0)))
                {
                    label54: if (i != 0)
                    {
                        this.mExtractAccessories.setVisibility(0);
                        if (this.mExtractAction == null)
                            continue;
                        if (paramEditorInfo.actionLabel != null)
                            this.mExtractAction.setText(paramEditorInfo.actionLabel);
                    }
                }
                else
                {
                    while (true)
                    {
                        this.mExtractAction.setOnClickListener(this.mActionClickListener);
                        break;
                        i = 0;
                        break label54;
                        this.mExtractAction.setText(getTextForImeAction(paramEditorInfo.imeOptions));
                    }
                    this.mExtractAccessories.setVisibility(8);
                    if (this.mExtractAction != null)
                        this.mExtractAction.setOnClickListener(null);
                }
        }
    }

    public void onUpdateExtractingVisibility(EditorInfo paramEditorInfo)
    {
        if ((paramEditorInfo.inputType == 0) || ((0x10000000 & paramEditorInfo.imeOptions) != 0))
            setExtractViewShown(false);
        while (true)
        {
            return;
            setExtractViewShown(true);
        }
    }

    public void onUpdateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        ExtractEditText localExtractEditText = this.mExtractEditText;
        int j;
        int k;
        int m;
        if ((localExtractEditText != null) && (isFullscreenMode()) && (this.mExtractedText != null))
        {
            int i = this.mExtractedText.startOffset;
            localExtractEditText.startInternalChanges();
            j = paramInt3 - i;
            k = paramInt4 - i;
            m = localExtractEditText.getText().length();
            if (j >= 0)
                break label95;
            j = 0;
            if (k >= 0)
                break label109;
            k = 0;
        }
        while (true)
        {
            localExtractEditText.setSelection(j, k);
            localExtractEditText.finishInternalChanges();
            return;
            label95: if (j <= m)
                break;
            j = m;
            break;
            label109: if (k > m)
                k = m;
        }
    }

    public void onViewClicked(boolean paramBoolean)
    {
    }

    public void onWindowHidden()
    {
    }

    public void onWindowShown()
    {
    }

    void reportExtractedMovement(int paramInt1, int paramInt2)
    {
        int i = 0;
        int j = 0;
        switch (paramInt1)
        {
        default:
        case 21:
        case 22:
        case 19:
        case 20:
        }
        while (true)
        {
            onExtractedCursorMovement(i, j);
            return;
            i = -paramInt2;
            continue;
            i = paramInt2;
            continue;
            j = -paramInt2;
            continue;
            j = paramInt2;
        }
    }

    public void requestHideSelf(int paramInt)
    {
        this.mImm.hideSoftInputFromInputMethod(this.mToken, paramInt);
    }

    public boolean sendDefaultEditorAction(boolean paramBoolean)
    {
        int i = 1;
        EditorInfo localEditorInfo = getCurrentInputEditorInfo();
        if ((localEditorInfo != null) && ((!paramBoolean) || ((0x40000000 & localEditorInfo.imeOptions) == 0)) && ((0xFF & localEditorInfo.imeOptions) != i))
        {
            InputConnection localInputConnection = getCurrentInputConnection();
            if (localInputConnection != null)
                localInputConnection.performEditorAction(0xFF & localEditorInfo.imeOptions);
        }
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public void sendDownUpKeyEvents(int paramInt)
    {
        InputConnection localInputConnection = getCurrentInputConnection();
        if (localInputConnection == null);
        while (true)
        {
            return;
            long l = SystemClock.uptimeMillis();
            localInputConnection.sendKeyEvent(new KeyEvent(l, l, 0, paramInt, 0, 0, -1, 0, 6));
            localInputConnection.sendKeyEvent(new KeyEvent(SystemClock.uptimeMillis(), l, 1, paramInt, 0, 0, -1, 0, 6));
        }
    }

    public void sendKeyChar(char paramChar)
    {
        switch (paramChar)
        {
        default:
            if ((paramChar >= '0') && (paramChar <= '9'))
                sendDownUpKeyEvents(7 + (paramChar + '\0*0'));
            break;
        case '\n':
        }
        while (true)
        {
            return;
            if (!sendDefaultEditorAction(true))
            {
                sendDownUpKeyEvents(66);
                continue;
                InputConnection localInputConnection = getCurrentInputConnection();
                if (localInputConnection != null)
                    localInputConnection.commitText(String.valueOf(paramChar), 1);
            }
        }
    }

    public void setBackDisposition(int paramInt)
    {
        this.mBackDisposition = paramInt;
    }

    public void setCandidatesView(View paramView)
    {
        this.mCandidatesFrame.removeAllViews();
        this.mCandidatesFrame.addView(paramView, new FrameLayout.LayoutParams(-1, -2));
    }

    public void setCandidatesViewShown(boolean paramBoolean)
    {
        updateCandidatesVisibility(paramBoolean);
        if ((!this.mShowInputRequested) && (this.mWindowVisible != paramBoolean))
        {
            if (!paramBoolean)
                break label30;
            showWindow(false);
        }
        while (true)
        {
            return;
            label30: doHideWindow();
        }
    }

    public void setExtractView(View paramView)
    {
        this.mExtractFrame.removeAllViews();
        this.mExtractFrame.addView(paramView, new FrameLayout.LayoutParams(-1, -1));
        this.mExtractView = paramView;
        if (paramView != null)
        {
            this.mExtractEditText = ((ExtractEditText)paramView.findViewById(16908325));
            this.mExtractEditText.setIME(this);
            this.mExtractAction = ((Button)paramView.findViewById(16908942));
            if (this.mExtractAction != null)
                this.mExtractAccessories = ((ViewGroup)paramView.findViewById(16908941));
            startExtractingText(false);
        }
        while (true)
        {
            return;
            this.mExtractEditText = null;
            this.mExtractAccessories = null;
            this.mExtractAction = null;
        }
    }

    public void setExtractViewShown(boolean paramBoolean)
    {
        if (this.mExtractViewHidden == paramBoolean)
            if (paramBoolean)
                break label24;
        label24: for (boolean bool = true; ; bool = false)
        {
            this.mExtractViewHidden = bool;
            updateExtractFrameVisibility();
            return;
        }
    }

    public void setInputView(View paramView)
    {
        this.mInputFrame.removeAllViews();
        this.mInputFrame.addView(paramView, new FrameLayout.LayoutParams(-1, -2));
        this.mInputView = paramView;
    }

    public void setTheme(int paramInt)
    {
        if (this.mWindow != null)
            throw new IllegalStateException("Must be called before onCreate()");
        this.mTheme = paramInt;
    }

    public void showStatusIcon(int paramInt)
    {
        this.mStatusIcon = paramInt;
        this.mImm.showStatusIcon(this.mToken, getPackageName(), paramInt);
    }

    public void showWindow(boolean paramBoolean)
    {
        if (this.mInShowWindow)
            Log.w("InputMethodService", "Re-entrance in to showWindow");
        while (true)
        {
            return;
            try
            {
                this.mWindowWasVisible = this.mWindowVisible;
                this.mInShowWindow = true;
                showWindowInner(paramBoolean);
                this.mWindowWasVisible = true;
                this.mInShowWindow = false;
            }
            finally
            {
                this.mWindowWasVisible = true;
                this.mInShowWindow = false;
            }
        }
    }

    void showWindowInner(boolean paramBoolean)
    {
        int i = 0;
        boolean bool = this.mWindowVisible;
        this.mWindowVisible = true;
        if (!this.mShowInputRequested)
        {
            if ((this.mInputStarted) && (paramBoolean))
            {
                i = 1;
                this.mShowInputRequested = true;
            }
            initialize();
            updateFullscreenMode();
            updateInputViewShown();
            if ((!this.mWindowAdded) || (!this.mWindowCreated))
            {
                this.mWindowAdded = true;
                this.mWindowCreated = true;
                initialize();
                View localView = onCreateCandidatesView();
                if (localView != null)
                    setCandidatesView(localView);
            }
            if (!this.mShowInputRequested)
                break label166;
            if (!this.mInputViewStarted)
            {
                this.mInputViewStarted = true;
                onStartInputView(this.mInputEditorInfo, false);
            }
        }
        while (true)
        {
            if (i != 0)
                startExtractingText(false);
            if (!bool)
            {
                this.mImm.setImeWindowStatus(this.mToken, 1, this.mBackDisposition);
                onWindowShown();
                this.mWindow.show();
            }
            return;
            break;
            label166: if (!this.mCandidatesViewStarted)
            {
                this.mCandidatesViewStarted = true;
                onStartCandidatesView(this.mInputEditorInfo, false);
            }
        }
    }

    void startExtractingText(boolean paramBoolean)
    {
        ExtractEditText localExtractEditText = this.mExtractEditText;
        ExtractedTextRequest localExtractedTextRequest;
        InputConnection localInputConnection;
        ExtractedText localExtractedText;
        if ((localExtractEditText != null) && (getCurrentInputStarted()) && (isFullscreenMode()))
        {
            this.mExtractedToken = (1 + this.mExtractedToken);
            localExtractedTextRequest = new ExtractedTextRequest();
            localExtractedTextRequest.token = this.mExtractedToken;
            localExtractedTextRequest.flags = 1;
            localExtractedTextRequest.hintMaxLines = 10;
            localExtractedTextRequest.hintMaxChars = 10000;
            localInputConnection = getCurrentInputConnection();
            if (localInputConnection != null)
                break label244;
            localExtractedText = null;
        }
        while (true)
        {
            this.mExtractedText = localExtractedText;
            if ((this.mExtractedText == null) || (localInputConnection == null))
                Log.e("InputMethodService", "Unexpected null in startExtractingText : mExtractedText = " + this.mExtractedText + ", input connection = " + localInputConnection);
            EditorInfo localEditorInfo = getCurrentInputEditorInfo();
            try
            {
                localExtractEditText.startInternalChanges();
                onUpdateExtractingVisibility(localEditorInfo);
                onUpdateExtractingViews(localEditorInfo);
                int i = localEditorInfo.inputType;
                if (((i & 0xF) == 1) && ((0x40000 & i) != 0))
                    i |= 131072;
                localExtractEditText.setInputType(i);
                localExtractEditText.setHint(localEditorInfo.hintText);
                if (this.mExtractedText != null)
                {
                    localExtractEditText.setEnabled(true);
                    localExtractEditText.setExtractedText(this.mExtractedText);
                }
                while (true)
                {
                    localExtractEditText.finishInternalChanges();
                    if (paramBoolean)
                        onExtractingInputChanged(localEditorInfo);
                    return;
                    label244: localExtractedText = localInputConnection.getExtractedText(localExtractedTextRequest, 1);
                    break;
                    localExtractEditText.setEnabled(false);
                    localExtractEditText.setText("");
                }
            }
            finally
            {
                localExtractEditText.finishInternalChanges();
            }
        }
    }

    public void switchInputMethod(String paramString)
    {
        this.mImm.setInputMethod(this.mToken, paramString);
    }

    void updateCandidatesVisibility(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 0; ; i = getCandidatesHiddenVisibility())
        {
            if (this.mCandidatesVisibility != i)
            {
                this.mCandidatesFrame.setVisibility(i);
                this.mCandidatesVisibility = i;
            }
            return;
        }
    }

    void updateExtractFrameVisibility()
    {
        int i = 1;
        int j;
        label26: label35: TypedArray localTypedArray;
        if (isFullscreenMode())
            if (this.mExtractViewHidden)
            {
                j = 4;
                this.mExtractFrame.setVisibility(0);
                if (this.mCandidatesVisibility != 0)
                    break label123;
                int k = i;
                updateCandidatesVisibility(k);
                if ((this.mWindowWasVisible) && (this.mFullscreenArea.getVisibility() != j))
                {
                    localTypedArray = this.mThemeAttrs;
                    if (j != 0)
                        break label128;
                }
            }
        while (true)
        {
            int n = localTypedArray.getResourceId(i, 0);
            if (n != 0)
                this.mFullscreenArea.startAnimation(AnimationUtils.loadAnimation(this, n));
            this.mFullscreenArea.setVisibility(j);
            return;
            j = 0;
            break;
            j = 0;
            this.mExtractFrame.setVisibility(8);
            break label26;
            label123: int m = 0;
            break label35;
            label128: i = 2;
        }
    }

    public void updateFullscreenMode()
    {
        boolean bool1 = true;
        boolean bool2;
        boolean bool3;
        label31: LinearLayout.LayoutParams localLayoutParams;
        label125: Window localWindow;
        if ((this.mShowInputRequested) && (onEvaluateFullscreenMode()))
        {
            bool2 = bool1;
            if (this.mLastShowInputRequested == this.mShowInputRequested)
                break label223;
            bool3 = bool1;
            if ((this.mIsFullscreen != bool2) || (!this.mFullscreenApplied))
            {
                bool3 = true;
                this.mIsFullscreen = bool2;
                InputConnection localInputConnection = getCurrentInputConnection();
                if (localInputConnection != null)
                    localInputConnection.reportFullscreenMode(bool2);
                this.mFullscreenApplied = bool1;
                initialize();
                localLayoutParams = (LinearLayout.LayoutParams)this.mFullscreenArea.getLayoutParams();
                if (!bool2)
                    break label228;
                this.mFullscreenArea.setBackgroundDrawable(this.mThemeAttrs.getDrawable(0));
                localLayoutParams.height = 0;
                localLayoutParams.weight = 1.0F;
                ((ViewGroup)this.mFullscreenArea.getParent()).updateViewLayout(this.mFullscreenArea, localLayoutParams);
                if (bool2)
                {
                    if (this.mExtractView == null)
                    {
                        View localView = onCreateExtractTextView();
                        if (localView != null)
                            setExtractView(localView);
                    }
                    startExtractingText(false);
                }
                updateExtractFrameVisibility();
            }
            if (bool3)
            {
                localWindow = this.mWindow.getWindow();
                if (this.mShowInputRequested)
                    break label252;
            }
        }
        while (true)
        {
            onConfigureWindow(localWindow, bool2, bool1);
            this.mLastShowInputRequested = this.mShowInputRequested;
            return;
            bool2 = false;
            break;
            label223: bool3 = false;
            break label31;
            label228: this.mFullscreenArea.setBackgroundDrawable(null);
            localLayoutParams.height = -2;
            localLayoutParams.weight = 0.0F;
            break label125;
            label252: bool1 = false;
        }
    }

    public void updateInputViewShown()
    {
        int i = 0;
        boolean bool;
        FrameLayout localFrameLayout;
        if ((this.mShowInputRequested) && (onEvaluateInputViewShown()))
        {
            bool = true;
            if ((this.mIsInputViewShown != bool) && (this.mWindowVisible))
            {
                this.mIsInputViewShown = bool;
                localFrameLayout = this.mInputFrame;
                if (!bool)
                    break label86;
            }
        }
        while (true)
        {
            localFrameLayout.setVisibility(i);
            if (this.mInputView == null)
            {
                initialize();
                View localView = onCreateInputView();
                if (localView != null)
                    setInputView(localView);
            }
            return;
            bool = false;
            break;
            label86: i = 8;
        }
    }

    public static final class Insets
    {
        public static final int TOUCHABLE_INSETS_CONTENT = 1;
        public static final int TOUCHABLE_INSETS_FRAME = 0;
        public static final int TOUCHABLE_INSETS_REGION = 3;
        public static final int TOUCHABLE_INSETS_VISIBLE = 2;
        public int contentTopInsets;
        public int touchableInsets;
        public final Region touchableRegion = new Region();
        public int visibleTopInsets;
    }

    public class InputMethodSessionImpl extends AbstractInputMethodService.AbstractInputMethodSessionImpl
    {
        public InputMethodSessionImpl()
        {
            super();
        }

        public void appPrivateCommand(String paramString, Bundle paramBundle)
        {
            if (!isEnabled());
            while (true)
            {
                return;
                InputMethodService.this.onAppPrivateCommand(paramString, paramBundle);
            }
        }

        public void displayCompletions(CompletionInfo[] paramArrayOfCompletionInfo)
        {
            if (!isEnabled());
            while (true)
            {
                return;
                InputMethodService.this.mCurCompletions = paramArrayOfCompletionInfo;
                InputMethodService.this.onDisplayCompletions(paramArrayOfCompletionInfo);
            }
        }

        public void finishInput()
        {
            if (!isEnabled());
            while (true)
            {
                return;
                InputMethodService.this.doFinishInput();
            }
        }

        public void toggleSoftInput(int paramInt1, int paramInt2)
        {
            InputMethodService.this.onToggleSoftInput(paramInt1, paramInt2);
        }

        public void updateCursor(Rect paramRect)
        {
            if (!isEnabled());
            while (true)
            {
                return;
                InputMethodService.this.onUpdateCursor(paramRect);
            }
        }

        public void updateExtractedText(int paramInt, ExtractedText paramExtractedText)
        {
            if (!isEnabled());
            while (true)
            {
                return;
                InputMethodService.this.onUpdateExtractedText(paramInt, paramExtractedText);
            }
        }

        public void updateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
        {
            if (!isEnabled());
            while (true)
            {
                return;
                InputMethodService.this.onUpdateSelection(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
            }
        }

        public void viewClicked(boolean paramBoolean)
        {
            if (!isEnabled());
            while (true)
            {
                return;
                InputMethodService.this.onViewClicked(paramBoolean);
            }
        }
    }

    public class InputMethodImpl extends AbstractInputMethodService.AbstractInputMethodImpl
    {
        public InputMethodImpl()
        {
            super();
        }

        public void attachToken(IBinder paramIBinder)
        {
            if (InputMethodService.this.mToken == null)
            {
                InputMethodService.this.mToken = paramIBinder;
                InputMethodService.this.mWindow.setToken(paramIBinder);
            }
        }

        public void bindInput(InputBinding paramInputBinding)
        {
            InputMethodService.this.mInputBinding = paramInputBinding;
            InputMethodService.this.mInputConnection = paramInputBinding.getConnection();
            InputConnection localInputConnection = InputMethodService.this.getCurrentInputConnection();
            if (localInputConnection != null)
                localInputConnection.reportFullscreenMode(InputMethodService.this.mIsFullscreen);
            InputMethodService.this.initialize();
            InputMethodService.this.onBindInput();
        }

        public void changeInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype)
        {
            InputMethodService.this.onCurrentInputMethodSubtypeChanged(paramInputMethodSubtype);
        }

        public void hideSoftInput(int paramInt, ResultReceiver paramResultReceiver)
        {
            int i = 0;
            boolean bool = InputMethodService.this.isInputViewShown();
            InputMethodService.this.mShowInputFlags = 0;
            InputMethodService.this.mShowInputRequested = false;
            InputMethodService.this.mShowInputForced = false;
            InputMethodService.this.doHideWindow();
            if (paramResultReceiver != null)
            {
                if (bool == InputMethodService.this.isInputViewShown())
                    break label67;
                i = 3;
            }
            while (true)
            {
                paramResultReceiver.send(i, null);
                return;
                label67: if (!bool)
                    i = 1;
            }
        }

        public void restartInput(InputConnection paramInputConnection, EditorInfo paramEditorInfo)
        {
            InputMethodService.this.doStartInput(paramInputConnection, paramEditorInfo, true);
        }

        public void showSoftInput(int paramInt, ResultReceiver paramResultReceiver)
        {
            int i = 2;
            boolean bool1 = InputMethodService.this.isInputViewShown();
            InputMethodService.this.mShowInputFlags = 0;
            if (InputMethodService.this.onShowInputRequested(paramInt, false))
                InputMethodService.this.showWindow(true);
            boolean bool2 = InputMethodService.this.onEvaluateInputViewShown();
            InputMethodManager localInputMethodManager = InputMethodService.this.mImm;
            IBinder localIBinder = InputMethodService.this.mToken;
            int j;
            if (bool2)
            {
                j = i;
                localInputMethodManager.setImeWindowStatus(localIBinder, j | 0x1, InputMethodService.this.mBackDisposition);
                if (paramResultReceiver != null)
                    if (bool1 == InputMethodService.this.isInputViewShown())
                        break label121;
            }
            while (true)
            {
                paramResultReceiver.send(i, null);
                return;
                j = 0;
                break;
                label121: if (bool1)
                    i = 0;
                else
                    i = 1;
            }
        }

        public void startInput(InputConnection paramInputConnection, EditorInfo paramEditorInfo)
        {
            InputMethodService.this.doStartInput(paramInputConnection, paramEditorInfo, false);
        }

        public void unbindInput()
        {
            InputMethodService.this.onUnbindInput();
            InputMethodService.this.mInputStarted = false;
            InputMethodService.this.mInputBinding = null;
            InputMethodService.this.mInputConnection = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.inputmethodservice.InputMethodService
 * JD-Core Version:        0.6.2
 */