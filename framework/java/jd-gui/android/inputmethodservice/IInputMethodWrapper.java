package android.inputmethodservice;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethod.SessionCallback;
import android.view.inputmethod.InputMethodSession;
import android.view.inputmethod.InputMethodSubtype;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.HandlerCaller.Callback;
import com.android.internal.os.HandlerCaller.SomeArgs;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputContext.Stub;
import com.android.internal.view.IInputMethod.Stub;
import com.android.internal.view.IInputMethodCallback;
import com.android.internal.view.IInputMethodSession;
import com.android.internal.view.InputConnectionWrapper;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

class IInputMethodWrapper extends IInputMethod.Stub
    implements HandlerCaller.Callback
{
    private static final boolean DEBUG = false;
    private static final int DO_ATTACH_TOKEN = 10;
    private static final int DO_CHANGE_INPUTMETHOD_SUBTYPE = 80;
    private static final int DO_CREATE_SESSION = 40;
    private static final int DO_DUMP = 1;
    private static final int DO_HIDE_SOFT_INPUT = 70;
    private static final int DO_RESTART_INPUT = 34;
    private static final int DO_REVOKE_SESSION = 50;
    private static final int DO_SET_INPUT_CONTEXT = 20;
    private static final int DO_SET_SESSION_ENABLED = 45;
    private static final int DO_SHOW_SOFT_INPUT = 60;
    private static final int DO_START_INPUT = 32;
    private static final int DO_UNSET_INPUT_CONTEXT = 30;
    private static final String TAG = "InputMethodWrapper";
    final HandlerCaller mCaller;
    final WeakReference<InputMethod> mInputMethod;
    final WeakReference<AbstractInputMethodService> mTarget;
    final int mTargetSdkVersion;

    public IInputMethodWrapper(AbstractInputMethodService paramAbstractInputMethodService, InputMethod paramInputMethod)
    {
        this.mTarget = new WeakReference(paramAbstractInputMethodService);
        this.mCaller = new HandlerCaller(paramAbstractInputMethodService.getApplicationContext(), this);
        this.mInputMethod = new WeakReference(paramInputMethod);
        this.mTargetSdkVersion = paramAbstractInputMethodService.getApplicationInfo().targetSdkVersion;
    }

    public void attachToken(IBinder paramIBinder)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageO(10, paramIBinder));
    }

    public void bindInput(InputBinding paramInputBinding)
    {
        InputBinding localInputBinding = new InputBinding(new InputConnectionWrapper(IInputContext.Stub.asInterface(paramInputBinding.getConnectionToken())), paramInputBinding);
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageO(20, localInputBinding));
    }

    public void changeInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageO(80, paramInputMethodSubtype));
    }

    public void createSession(IInputMethodCallback paramIInputMethodCallback)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageO(40, paramIInputMethodCallback));
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        AbstractInputMethodService localAbstractInputMethodService = (AbstractInputMethodService)this.mTarget.get();
        if (localAbstractInputMethodService == null);
        while (true)
        {
            return;
            if (localAbstractInputMethodService.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            {
                paramPrintWriter.println("Permission Denial: can't dump InputMethodManager from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
            }
            else
            {
                CountDownLatch localCountDownLatch = new CountDownLatch(1);
                this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageOOOO(1, paramFileDescriptor, paramPrintWriter, paramArrayOfString, localCountDownLatch));
                try
                {
                    if (!localCountDownLatch.await(5L, TimeUnit.SECONDS))
                        paramPrintWriter.println("Timeout waiting for dump");
                }
                catch (InterruptedException localInterruptedException)
                {
                    paramPrintWriter.println("Interrupted waiting for dump");
                }
            }
        }
    }

    public void executeMessage(Message paramMessage)
    {
        InputConnectionWrapper localInputConnectionWrapper = null;
        int i = 1;
        InputMethod localInputMethod = (InputMethod)this.mInputMethod.get();
        if ((localInputMethod == null) && (paramMessage.what != i))
            Log.w("InputMethodWrapper", "Input method reference was null, ignoring message: " + paramMessage.what);
        while (true)
        {
            return;
            switch (paramMessage.what)
            {
            default:
                Log.w("InputMethodWrapper", "Unhandled message code: " + paramMessage.what);
                break;
            case 1:
                AbstractInputMethodService localAbstractInputMethodService = (AbstractInputMethodService)this.mTarget.get();
                if (localAbstractInputMethodService != null)
                {
                    HandlerCaller.SomeArgs localSomeArgs3 = (HandlerCaller.SomeArgs)paramMessage.obj;
                    try
                    {
                        localAbstractInputMethodService.dump((FileDescriptor)localSomeArgs3.arg1, (PrintWriter)localSomeArgs3.arg2, (String[])localSomeArgs3.arg3);
                        synchronized (localSomeArgs3.arg4)
                        {
                            ((CountDownLatch)localSomeArgs3.arg4).countDown();
                        }
                    }
                    catch (RuntimeException localRuntimeException)
                    {
                        while (true)
                            ((PrintWriter)localSomeArgs3.arg2).println("Exception: " + localRuntimeException);
                    }
                }
                break;
            case 10:
                localInputMethod.attachToken((IBinder)paramMessage.obj);
                break;
            case 20:
                localInputMethod.bindInput((InputBinding)paramMessage.obj);
                break;
            case 30:
                localInputMethod.unbindInput();
                break;
            case 32:
                HandlerCaller.SomeArgs localSomeArgs2 = (HandlerCaller.SomeArgs)paramMessage.obj;
                IInputContext localIInputContext2 = (IInputContext)localSomeArgs2.arg1;
                if (localIInputContext2 != null)
                    localInputConnectionWrapper = new InputConnectionWrapper(localIInputContext2);
                EditorInfo localEditorInfo2 = (EditorInfo)localSomeArgs2.arg2;
                localEditorInfo2.makeCompatible(this.mTargetSdkVersion);
                localInputMethod.startInput(localInputConnectionWrapper, localEditorInfo2);
                break;
            case 34:
                HandlerCaller.SomeArgs localSomeArgs1 = (HandlerCaller.SomeArgs)paramMessage.obj;
                IInputContext localIInputContext1 = (IInputContext)localSomeArgs1.arg1;
                if (localIInputContext1 != null)
                    localInputConnectionWrapper = new InputConnectionWrapper(localIInputContext1);
                EditorInfo localEditorInfo1 = (EditorInfo)localSomeArgs1.arg2;
                localEditorInfo1.makeCompatible(this.mTargetSdkVersion);
                localInputMethod.restartInput(localInputConnectionWrapper, localEditorInfo1);
                break;
            case 40:
                localInputMethod.createSession(new InputMethodSessionCallbackWrapper(this.mCaller.mContext, (IInputMethodCallback)paramMessage.obj));
                break;
            case 45:
                InputMethodSession localInputMethodSession = (InputMethodSession)paramMessage.obj;
                if (paramMessage.arg1 != 0);
                while (true)
                {
                    localInputMethod.setSessionEnabled(localInputMethodSession, i);
                    break;
                    int j = 0;
                }
            case 50:
                localInputMethod.revokeSession((InputMethodSession)paramMessage.obj);
                break;
            case 60:
                localInputMethod.showSoftInput(paramMessage.arg1, (ResultReceiver)paramMessage.obj);
                break;
            case 70:
                localInputMethod.hideSoftInput(paramMessage.arg1, (ResultReceiver)paramMessage.obj);
                break;
            case 80:
                localInputMethod.changeInputMethodSubtype((InputMethodSubtype)paramMessage.obj);
            }
        }
    }

    public InputMethod getInternalInputMethod()
    {
        return (InputMethod)this.mInputMethod.get();
    }

    public void hideSoftInput(int paramInt, ResultReceiver paramResultReceiver)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageIO(70, paramInt, paramResultReceiver));
    }

    public void restartInput(IInputContext paramIInputContext, EditorInfo paramEditorInfo)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageOO(34, paramIInputContext, paramEditorInfo));
    }

    public void revokeSession(IInputMethodSession paramIInputMethodSession)
    {
        try
        {
            InputMethodSession localInputMethodSession = ((IInputMethodSessionWrapper)paramIInputMethodSession).getInternalInputMethodSession();
            this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageO(50, localInputMethodSession));
            return;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                Log.w("InputMethodWrapper", "Incoming session not of correct type: " + paramIInputMethodSession, localClassCastException);
        }
    }

    public void setSessionEnabled(IInputMethodSession paramIInputMethodSession, boolean paramBoolean)
    {
        try
        {
            InputMethodSession localInputMethodSession = ((IInputMethodSessionWrapper)paramIInputMethodSession).getInternalInputMethodSession();
            HandlerCaller localHandlerCaller1 = this.mCaller;
            HandlerCaller localHandlerCaller2 = this.mCaller;
            if (paramBoolean);
            for (int i = 1; ; i = 0)
            {
                localHandlerCaller1.executeOrSendMessage(localHandlerCaller2.obtainMessageIO(45, i, localInputMethodSession));
                return;
            }
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                Log.w("InputMethodWrapper", "Incoming session not of correct type: " + paramIInputMethodSession, localClassCastException);
        }
    }

    public void showSoftInput(int paramInt, ResultReceiver paramResultReceiver)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageIO(60, paramInt, paramResultReceiver));
    }

    public void startInput(IInputContext paramIInputContext, EditorInfo paramEditorInfo)
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessageOO(32, paramIInputContext, paramEditorInfo));
    }

    public void unbindInput()
    {
        this.mCaller.executeOrSendMessage(this.mCaller.obtainMessage(30));
    }

    static class InputMethodSessionCallbackWrapper
        implements InputMethod.SessionCallback
    {
        final IInputMethodCallback mCb;
        final Context mContext;

        InputMethodSessionCallbackWrapper(Context paramContext, IInputMethodCallback paramIInputMethodCallback)
        {
            this.mContext = paramContext;
            this.mCb = paramIInputMethodCallback;
        }

        public void sessionCreated(InputMethodSession paramInputMethodSession)
        {
            if (paramInputMethodSession != null);
            try
            {
                IInputMethodSessionWrapper localIInputMethodSessionWrapper = new IInputMethodSessionWrapper(this.mContext, paramInputMethodSession);
                this.mCb.sessionCreated(localIInputMethodSessionWrapper);
                return;
                this.mCb.sessionCreated(null);
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    static class Notifier
    {
        boolean notified;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.inputmethodservice.IInputMethodWrapper
 * JD-Core Version:        0.6.2
 */