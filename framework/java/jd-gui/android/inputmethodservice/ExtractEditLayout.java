package android.inputmethodservice;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuBuilder.Callback;
import com.android.internal.view.menu.MenuPopupHelper;

public class ExtractEditLayout extends LinearLayout
{
    ExtractActionMode mActionMode;
    Button mEditButton;
    Button mExtractActionButton;

    public ExtractEditLayout(Context paramContext)
    {
        super(paramContext);
    }

    public ExtractEditLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public void finishActionMode()
    {
        if (this.mActionMode != null)
            this.mActionMode.finish();
    }

    public boolean isActionModeStarted()
    {
        if (this.mActionMode != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onFinishInflate()
    {
        super.onFinishInflate();
        this.mExtractActionButton = ((Button)findViewById(16908942));
        this.mEditButton = ((Button)findViewById(16908943));
        this.mEditButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                if (ExtractEditLayout.this.mActionMode != null)
                    new MenuPopupHelper(ExtractEditLayout.this.getContext(), ExtractEditLayout.this.mActionMode.mMenu, paramAnonymousView).show();
            }
        });
    }

    public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback)
    {
        ExtractActionMode localExtractActionMode = new ExtractActionMode(paramCallback);
        if (localExtractActionMode.dispatchOnCreate())
        {
            localExtractActionMode.invalidate();
            this.mExtractActionButton.setVisibility(4);
            this.mEditButton.setVisibility(0);
            this.mActionMode = localExtractActionMode;
            sendAccessibilityEvent(32);
        }
        while (true)
        {
            return localExtractActionMode;
            localExtractActionMode = null;
        }
    }

    private class ExtractActionMode extends ActionMode
        implements MenuBuilder.Callback
    {
        private ActionMode.Callback mCallback;
        MenuBuilder mMenu = new MenuBuilder(ExtractEditLayout.this.getContext());

        public ExtractActionMode(ActionMode.Callback arg2)
        {
            this.mMenu.setCallback(this);
            Object localObject;
            this.mCallback = localObject;
        }

        public boolean dispatchOnCreate()
        {
            this.mMenu.stopDispatchingItemsChanged();
            try
            {
                boolean bool = this.mCallback.onCreateActionMode(this, this.mMenu);
                return bool;
            }
            finally
            {
                this.mMenu.startDispatchingItemsChanged();
            }
        }

        public void finish()
        {
            if (ExtractEditLayout.this.mActionMode != this);
            while (true)
            {
                return;
                this.mCallback.onDestroyActionMode(this);
                this.mCallback = null;
                ExtractEditLayout.this.mExtractActionButton.setVisibility(0);
                ExtractEditLayout.this.mEditButton.setVisibility(4);
                ExtractEditLayout.this.sendAccessibilityEvent(32);
                ExtractEditLayout.this.mActionMode = null;
            }
        }

        public View getCustomView()
        {
            return null;
        }

        public Menu getMenu()
        {
            return this.mMenu;
        }

        public MenuInflater getMenuInflater()
        {
            return new MenuInflater(ExtractEditLayout.this.getContext());
        }

        public CharSequence getSubtitle()
        {
            return null;
        }

        public CharSequence getTitle()
        {
            return null;
        }

        public void invalidate()
        {
            this.mMenu.stopDispatchingItemsChanged();
            try
            {
                this.mCallback.onPrepareActionMode(this, this.mMenu);
                return;
            }
            finally
            {
                this.mMenu.startDispatchingItemsChanged();
            }
        }

        public boolean isTitleOptional()
        {
            return true;
        }

        public boolean onMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
        {
            if (this.mCallback != null);
            for (boolean bool = this.mCallback.onActionItemClicked(this, paramMenuItem); ; bool = false)
                return bool;
        }

        public void onMenuModeChange(MenuBuilder paramMenuBuilder)
        {
        }

        public void setCustomView(View paramView)
        {
        }

        public void setSubtitle(int paramInt)
        {
        }

        public void setSubtitle(CharSequence paramCharSequence)
        {
        }

        public void setTitle(int paramInt)
        {
        }

        public void setTitle(CharSequence paramCharSequence)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.inputmethodservice.ExtractEditLayout
 * JD-Core Version:        0.6.2
 */