package android.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.IContentProvider;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserId;
import android.text.TextUtils;
import android.util.AndroidException;
import android.util.Log;
import com.android.internal.widget.ILockSettings;
import com.android.internal.widget.ILockSettings.Stub;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;

public final class Settings
{
    public static final String ACTION_ACCESSIBILITY_SETTINGS = "android.settings.ACCESSIBILITY_SETTINGS";
    public static final String ACTION_ADD_ACCOUNT = "android.settings.ADD_ACCOUNT_SETTINGS";
    public static final String ACTION_AIRPLANE_MODE_SETTINGS = "android.settings.AIRPLANE_MODE_SETTINGS";
    public static final String ACTION_APN_SETTINGS = "android.settings.APN_SETTINGS";
    public static final String ACTION_APPLICATION_DETAILS_SETTINGS = "android.settings.APPLICATION_DETAILS_SETTINGS";
    public static final String ACTION_APPLICATION_DEVELOPMENT_SETTINGS = "android.settings.APPLICATION_DEVELOPMENT_SETTINGS";
    public static final String ACTION_APPLICATION_SETTINGS = "android.settings.APPLICATION_SETTINGS";
    public static final String ACTION_BLUETOOTH_SETTINGS = "android.settings.BLUETOOTH_SETTINGS";
    public static final String ACTION_DATA_ROAMING_SETTINGS = "android.settings.DATA_ROAMING_SETTINGS";
    public static final String ACTION_DATE_SETTINGS = "android.settings.DATE_SETTINGS";
    public static final String ACTION_DEVICE_INFO_SETTINGS = "android.settings.DEVICE_INFO_SETTINGS";
    public static final String ACTION_DISPLAY_SETTINGS = "android.settings.DISPLAY_SETTINGS";
    public static final String ACTION_INPUT_METHOD_SETTINGS = "android.settings.INPUT_METHOD_SETTINGS";
    public static final String ACTION_INPUT_METHOD_SUBTYPE_SETTINGS = "android.settings.INPUT_METHOD_SUBTYPE_SETTINGS";
    public static final String ACTION_INTERNAL_STORAGE_SETTINGS = "android.settings.INTERNAL_STORAGE_SETTINGS";
    public static final String ACTION_LOCALE_SETTINGS = "android.settings.LOCALE_SETTINGS";
    public static final String ACTION_LOCATION_SOURCE_SETTINGS = "android.settings.LOCATION_SOURCE_SETTINGS";
    public static final String ACTION_MANAGE_ALL_APPLICATIONS_SETTINGS = "android.settings.MANAGE_ALL_APPLICATIONS_SETTINGS";
    public static final String ACTION_MANAGE_APPLICATIONS_SETTINGS = "android.settings.MANAGE_APPLICATIONS_SETTINGS";
    public static final String ACTION_MEMORY_CARD_SETTINGS = "android.settings.MEMORY_CARD_SETTINGS";
    public static final String ACTION_NETWORK_OPERATOR_SETTINGS = "android.settings.NETWORK_OPERATOR_SETTINGS";
    public static final String ACTION_NFCSHARING_SETTINGS = "android.settings.NFCSHARING_SETTINGS";
    public static final String ACTION_NFC_SETTINGS = "android.settings.NFC_SETTINGS";
    public static final String ACTION_PRIVACY_SETTINGS = "android.settings.PRIVACY_SETTINGS";
    public static final String ACTION_QUICK_LAUNCH_SETTINGS = "android.settings.QUICK_LAUNCH_SETTINGS";
    public static final String ACTION_SEARCH_SETTINGS = "android.search.action.SEARCH_SETTINGS";
    public static final String ACTION_SECURITY_SETTINGS = "android.settings.SECURITY_SETTINGS";
    public static final String ACTION_SETTINGS = "android.settings.SETTINGS";
    public static final String ACTION_SHOW_INPUT_METHOD_PICKER = "android.settings.SHOW_INPUT_METHOD_PICKER";
    public static final String ACTION_SOUND_SETTINGS = "android.settings.SOUND_SETTINGS";
    public static final String ACTION_SYNC_SETTINGS = "android.settings.SYNC_SETTINGS";
    public static final String ACTION_SYSTEM_UPDATE_SETTINGS = "android.settings.SYSTEM_UPDATE_SETTINGS";
    public static final String ACTION_USER_DICTIONARY_INSERT = "com.android.settings.USER_DICTIONARY_INSERT";
    public static final String ACTION_USER_DICTIONARY_SETTINGS = "android.settings.USER_DICTIONARY_SETTINGS";
    public static final String ACTION_WIFI_IP_SETTINGS = "android.settings.WIFI_IP_SETTINGS";
    public static final String ACTION_WIFI_SETTINGS = "android.settings.WIFI_SETTINGS";
    public static final String ACTION_WIRELESS_SETTINGS = "android.settings.WIRELESS_SETTINGS";
    public static final String AUTHORITY = "settings";
    public static final String CALL_METHOD_GET_SECURE = "GET_secure";
    public static final String CALL_METHOD_GET_SYSTEM = "GET_system";
    public static final String EXTRA_AUTHORITIES = "authorities";
    public static final String EXTRA_INPUT_METHOD_ID = "input_method_id";
    private static final String JID_RESOURCE_PREFIX = "android";
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "Settings";

    public static String getGTalkDeviceId(long paramLong)
    {
        return "android-" + Long.toHexString(paramLong);
    }

    public static final class Bookmarks
        implements BaseColumns
    {
        public static final Uri CONTENT_URI = Uri.parse("content://settings/bookmarks");
        public static final String FOLDER = "folder";
        public static final String ID = "_id";
        public static final String INTENT = "intent";
        public static final String ORDERING = "ordering";
        public static final String SHORTCUT = "shortcut";
        private static final String TAG = "Bookmarks";
        public static final String TITLE = "title";
        private static final String[] sIntentProjection;
        private static final String[] sShortcutProjection = arrayOfString2;
        private static final String sShortcutSelection = "shortcut=?";

        static
        {
            String[] arrayOfString1 = new String[1];
            arrayOfString1[0] = "intent";
            sIntentProjection = arrayOfString1;
            String[] arrayOfString2 = new String[2];
            arrayOfString2[0] = "_id";
            arrayOfString2[1] = "shortcut";
        }

        public static Uri add(ContentResolver paramContentResolver, Intent paramIntent, String paramString1, String paramString2, char paramChar, int paramInt)
        {
            if (paramChar != 0)
            {
                Uri localUri = CONTENT_URI;
                String[] arrayOfString = new String[1];
                arrayOfString[0] = String.valueOf(paramChar);
                paramContentResolver.delete(localUri, "shortcut=?", arrayOfString);
            }
            ContentValues localContentValues = new ContentValues();
            if (paramString1 != null)
                localContentValues.put("title", paramString1);
            if (paramString2 != null)
                localContentValues.put("folder", paramString2);
            localContentValues.put("intent", paramIntent.toUri(0));
            if (paramChar != 0)
                localContentValues.put("shortcut", Integer.valueOf(paramChar));
            localContentValues.put("ordering", Integer.valueOf(paramInt));
            return paramContentResolver.insert(CONTENT_URI, localContentValues);
        }

        // ERROR //
        public static Intent getIntentForShortcut(ContentResolver paramContentResolver, char paramChar)
        {
            // Byte code:
            //     0: aconst_null
            //     1: astore_2
            //     2: getstatic 50	android/provider/Settings$Bookmarks:CONTENT_URI	Landroid/net/Uri;
            //     5: astore_3
            //     6: getstatic 54	android/provider/Settings$Bookmarks:sIntentProjection	[Ljava/lang/String;
            //     9: astore 4
            //     11: iconst_1
            //     12: anewarray 52	java/lang/String
            //     15: astore 5
            //     17: aload 5
            //     19: iconst_0
            //     20: iload_1
            //     21: invokestatic 65	java/lang/String:valueOf	(I)Ljava/lang/String;
            //     24: aastore
            //     25: aload_0
            //     26: aload_3
            //     27: aload 4
            //     29: ldc 38
            //     31: aload 5
            //     33: ldc 24
            //     35: invokevirtual 105	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
            //     38: astore 6
            //     40: aload_2
            //     41: ifnonnull +77 -> 118
            //     44: aload 6
            //     46: invokeinterface 111 1 0
            //     51: istore 8
            //     53: iload 8
            //     55: ifeq +63 -> 118
            //     58: aload 6
            //     60: aload 6
            //     62: ldc 21
            //     64: invokeinterface 115 2 0
            //     69: invokeinterface 118 2 0
            //     74: iconst_0
            //     75: invokestatic 122	android/content/Intent:parseUri	(Ljava/lang/String;I)Landroid/content/Intent;
            //     78: astore 12
            //     80: aload 12
            //     82: astore_2
            //     83: goto -43 -> 40
            //     86: astore 10
            //     88: ldc 29
            //     90: ldc 124
            //     92: aload 10
            //     94: invokestatic 130	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     97: pop
            //     98: goto -58 -> 40
            //     101: astore 7
            //     103: aload 6
            //     105: ifnull +10 -> 115
            //     108: aload 6
            //     110: invokeinterface 133 1 0
            //     115: aload 7
            //     117: athrow
            //     118: aload 6
            //     120: ifnull +10 -> 130
            //     123: aload 6
            //     125: invokeinterface 133 1 0
            //     130: aload_2
            //     131: areturn
            //     132: astore 9
            //     134: goto -94 -> 40
            //
            // Exception table:
            //     from	to	target	type
            //     58	80	86	java/lang/IllegalArgumentException
            //     44	53	101	finally
            //     58	80	101	finally
            //     88	98	101	finally
            //     58	80	132	java/net/URISyntaxException
        }

        public static CharSequence getLabelForFolder(Resources paramResources, String paramString)
        {
            return paramString;
        }

        public static CharSequence getTitle(Context paramContext, Cursor paramCursor)
        {
            int i = paramCursor.getColumnIndex("title");
            int j = paramCursor.getColumnIndex("intent");
            if ((i == -1) || (j == -1))
                throw new IllegalArgumentException("The cursor must contain the TITLE and INTENT columns.");
            Object localObject1 = paramCursor.getString(i);
            if (!TextUtils.isEmpty((CharSequence)localObject1));
            String str;
            while (true)
            {
                return localObject1;
                str = paramCursor.getString(j);
                if (!TextUtils.isEmpty(str))
                    break;
                localObject1 = "";
            }
            while (true)
            {
                try
                {
                    Intent localIntent = Intent.parseUri(str, 0);
                    PackageManager localPackageManager = paramContext.getPackageManager();
                    ResolveInfo localResolveInfo = localPackageManager.resolveActivity(localIntent, 0);
                    if (localResolveInfo == null)
                        break label138;
                    localObject2 = localResolveInfo.loadLabel(localPackageManager);
                    localObject1 = localObject2;
                }
                catch (URISyntaxException localURISyntaxException)
                {
                    localObject1 = "";
                }
                break;
                label138: Object localObject2 = "";
            }
        }
    }

    public static final class Secure extends Settings.NameValueTable
    {
        public static final String ACCESSIBILITY_ENABLED = "accessibility_enabled";
        public static final String ACCESSIBILITY_SCREEN_READER_URL = "accessibility_script_injection_url";
        public static final String ACCESSIBILITY_SCRIPT_INJECTION = "accessibility_script_injection";
        public static final String ACCESSIBILITY_SPEAK_PASSWORD = "speak_password";
        public static final String ACCESSIBILITY_WEB_CONTENT_KEY_BINDINGS = "accessibility_web_content_key_bindings";
        public static final String ADB_ENABLED = "adb_enabled";
        public static final String ALLOWED_GEOLOCATION_ORIGINS = "allowed_geolocation_origins";
        public static final String ALLOW_MOCK_LOCATION = "mock_location";
        public static final String ANDROID_ID = "android_id";
        public static final String ANR_SHOW_BACKGROUND = "anr_show_background";
        public static final String ASSISTED_GPS_ENABLED = "assisted_gps_enabled";

        @Deprecated
        public static final String BACKGROUND_DATA = "background_data";
        public static final String BACKUP_AUTO_RESTORE = "backup_auto_restore";
        public static final String BACKUP_ENABLED = "backup_enabled";
        public static final String BACKUP_PROVISIONED = "backup_provisioned";
        public static final String BACKUP_TRANSPORT = "backup_transport";
        public static final String BATTERY_DISCHARGE_DURATION_THRESHOLD = "battery_discharge_duration_threshold";
        public static final String BATTERY_DISCHARGE_THRESHOLD = "battery_discharge_threshold";
        public static final String BLUETOOTH_ON = "bluetooth_on";
        public static final String CDMA_CELL_BROADCAST_SMS = "cdma_cell_broadcast_sms";
        public static final String CDMA_ROAMING_MODE = "roaming_settings";
        public static final String CDMA_SUBSCRIPTION_MODE = "subscription_mode";
        public static final String CONNECTIVITY_CHANGE_DELAY = "connectivity_change_delay";
        public static final int CONNECTIVITY_CHANGE_DELAY_DEFAULT = 3000;
        public static final String CONTACTS_PREAUTH_URI_EXPIRATION = "contacts_preauth_uri_expiration";
        public static final Uri CONTENT_URI;
        public static final String DATA_ROAMING = "data_roaming";
        public static final String DATA_STALL_ALARM_AGGRESSIVE_DELAY_IN_MS = "data_stall_alarm_aggressive_delay_in_ms";
        public static final String DATA_STALL_ALARM_NON_AGGRESSIVE_DELAY_IN_MS = "data_stall_alarm_non_aggressive_delay_in_ms";
        public static final String DEFAULT_DNS_SERVER = "default_dns_server";
        public static final String DEFAULT_INPUT_METHOD = "default_input_method";
        public static final String DEFAULT_INSTALL_LOCATION = "default_install_location";
        public static final String DEVELOPMENT_SETTINGS_ENABLED = "development_settings_enabled";
        public static final String DEVICE_PROVISIONED = "device_provisioned";
        public static final String DISABLED_SYSTEM_INPUT_METHODS = "disabled_system_input_methods";
        public static final String DISK_FREE_CHANGE_REPORTING_THRESHOLD = "disk_free_change_reporting_threshold";
        public static final String DISPLAY_SIZE_FORCED = "display_size_forced";
        public static final String DOWNLOAD_MAX_BYTES_OVER_MOBILE = "download_manager_max_bytes_over_mobile";
        public static final String DOWNLOAD_RECOMMENDED_MAX_BYTES_OVER_MOBILE = "download_manager_recommended_max_bytes_over_mobile";
        public static final String DROPBOX_AGE_SECONDS = "dropbox_age_seconds";
        public static final String DROPBOX_MAX_FILES = "dropbox_max_files";
        public static final String DROPBOX_QUOTA_KB = "dropbox_quota_kb";
        public static final String DROPBOX_QUOTA_PERCENT = "dropbox_quota_percent";
        public static final String DROPBOX_RESERVE_PERCENT = "dropbox_reserve_percent";
        public static final String DROPBOX_TAG_PREFIX = "dropbox:";
        public static final String ENABLED_ACCESSIBILITY_SERVICES = "enabled_accessibility_services";
        public static final String ENABLED_INPUT_METHODS = "enabled_input_methods";
        public static final String ENHANCED_VOICE_PRIVACY_ENABLED = "enhanced_voice_privacy_enabled";
        public static final String ERROR_LOGCAT_PREFIX = "logcat_for_";
        public static final String GLOBAL_HTTP_PROXY_EXCLUSION_LIST = "global_http_proxy_exclusion_list";
        public static final String GLOBAL_HTTP_PROXY_HOST = "global_http_proxy_host";
        public static final String GLOBAL_HTTP_PROXY_PORT = "global_http_proxy_port";
        public static final String GPRS_REGISTER_CHECK_PERIOD_MS = "gprs_register_check_period_ms";
        public static final String HTTP_PROXY = "http_proxy";
        public static final String INCALL_POWER_BUTTON_BEHAVIOR = "incall_power_button_behavior";
        public static final int INCALL_POWER_BUTTON_BEHAVIOR_DEFAULT = 1;
        public static final int INCALL_POWER_BUTTON_BEHAVIOR_HANGUP = 2;
        public static final int INCALL_POWER_BUTTON_BEHAVIOR_SCREEN_OFF = 1;
        public static final String INET_CONDITION_DEBOUNCE_DOWN_DELAY = "inet_condition_debounce_down_delay";
        public static final String INET_CONDITION_DEBOUNCE_UP_DELAY = "inet_condition_debounce_up_delay";
        public static final String INPUT_METHODS_SUBTYPE_HISTORY = "input_methods_subtype_history";
        public static final String INPUT_METHOD_SELECTOR_VISIBILITY = "input_method_selector_visibility";
        public static final String INSTALL_NON_MARKET_APPS = "install_non_market_apps";
        public static final String LAST_SETUP_SHOWN = "last_setup_shown";
        public static final String LOCATION_PROVIDERS_ALLOWED = "location_providers_allowed";
        public static final String LOCK_BIOMETRIC_WEAK_FLAGS = "lock_biometric_weak_flags";
        public static final String LOCK_PATTERN_ENABLED = "lock_pattern_autolock";
        public static final String LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED = "lock_pattern_tactile_feedback_enabled";
        public static final String LOCK_PATTERN_VISIBLE = "lock_pattern_visible_pattern";
        public static final String LOCK_SCREEN_LOCK_AFTER_TIMEOUT = "lock_screen_lock_after_timeout";
        public static final String LOCK_SCREEN_OWNER_INFO = "lock_screen_owner_info";
        public static final String LOCK_SCREEN_OWNER_INFO_ENABLED = "lock_screen_owner_info_enabled";

        @Deprecated
        public static final String LOGGING_ID = "logging_id";
        public static final String LONG_PRESS_TIMEOUT = "long_press_timeout";
        public static final String MEMCHECK_EXEC_END_TIME = "memcheck_exec_end_time";
        public static final String MEMCHECK_EXEC_START_TIME = "memcheck_exec_start_time";
        public static final String MEMCHECK_INTERVAL = "memcheck_interval";
        public static final String MEMCHECK_LOG_REALTIME_INTERVAL = "memcheck_log_realtime_interval";
        public static final String MEMCHECK_MIN_ALARM = "memcheck_min_alarm";
        public static final String MEMCHECK_MIN_SCREEN_OFF = "memcheck_min_screen_off";
        public static final String MEMCHECK_PHONE_ENABLED = "memcheck_phone_enabled";
        public static final String MEMCHECK_PHONE_HARD_THRESHOLD = "memcheck_phone_hard";
        public static final String MEMCHECK_PHONE_SOFT_THRESHOLD = "memcheck_phone_soft";
        public static final String MEMCHECK_RECHECK_INTERVAL = "memcheck_recheck_interval";
        public static final String MEMCHECK_SYSTEM_ENABLED = "memcheck_system_enabled";
        public static final String MEMCHECK_SYSTEM_HARD_THRESHOLD = "memcheck_system_hard";
        public static final String MEMCHECK_SYSTEM_SOFT_THRESHOLD = "memcheck_system_soft";
        public static final String MOBILE_DATA = "mobile_data";
        public static final String MOUNT_PLAY_NOTIFICATION_SND = "mount_play_not_snd";
        public static final String MOUNT_UMS_AUTOSTART = "mount_ums_autostart";
        public static final String MOUNT_UMS_NOTIFY_ENABLED = "mount_ums_notify_enabled";
        public static final String MOUNT_UMS_PROMPT = "mount_ums_prompt";
        private static final HashSet<String> MOVED_TO_LOCK_SETTINGS;
        public static final String NETSTATS_DEV_BUCKET_DURATION = "netstats_dev_bucket_duration";
        public static final String NETSTATS_DEV_DELETE_AGE = "netstats_dev_delete_age";
        public static final String NETSTATS_DEV_PERSIST_BYTES = "netstats_dev_persist_bytes";
        public static final String NETSTATS_DEV_ROTATE_AGE = "netstats_dev_rotate_age";
        public static final String NETSTATS_ENABLED = "netstats_enabled";
        public static final String NETSTATS_GLOBAL_ALERT_BYTES = "netstats_global_alert_bytes";
        public static final String NETSTATS_POLL_INTERVAL = "netstats_poll_interval";
        public static final String NETSTATS_REPORT_XT_OVER_DEV = "netstats_report_xt_over_dev";
        public static final String NETSTATS_SAMPLE_ENABLED = "netstats_sample_enabled";
        public static final String NETSTATS_TIME_CACHE_MAX_AGE = "netstats_time_cache_max_age";
        public static final String NETSTATS_UID_BUCKET_DURATION = "netstats_uid_bucket_duration";
        public static final String NETSTATS_UID_DELETE_AGE = "netstats_uid_delete_age";
        public static final String NETSTATS_UID_PERSIST_BYTES = "netstats_uid_persist_bytes";
        public static final String NETSTATS_UID_ROTATE_AGE = "netstats_uid_rotate_age";
        public static final String NETSTATS_UID_TAG_BUCKET_DURATION = "netstats_uid_tag_bucket_duration";
        public static final String NETSTATS_UID_TAG_DELETE_AGE = "netstats_uid_tag_delete_age";
        public static final String NETSTATS_UID_TAG_PERSIST_BYTES = "netstats_uid_tag_persist_bytes";
        public static final String NETSTATS_UID_TAG_ROTATE_AGE = "netstats_uid_tag_rotate_age";
        public static final String NETWORK_PREFERENCE = "network_preference";
        public static final String NITZ_UPDATE_DIFF = "nitz_update_diff";
        public static final String NITZ_UPDATE_SPACING = "nitz_update_spacing";
        public static final String NSD_ON = "nsd_on";
        public static final String NTP_SERVER = "ntp_server";
        public static final String NTP_TIMEOUT = "ntp_timeout";
        public static final String PACKAGE_VERIFIER_ENABLE = "verifier_enable";
        public static final String PACKAGE_VERIFIER_TIMEOUT = "verifier_timeout";
        public static final String PARENTAL_CONTROL_ENABLED = "parental_control_enabled";
        public static final String PARENTAL_CONTROL_LAST_UPDATE = "parental_control_last_update";
        public static final String PARENTAL_CONTROL_REDIRECT_URL = "parental_control_redirect_url";
        public static final String PDP_WATCHDOG_ERROR_POLL_COUNT = "pdp_watchdog_error_poll_count";
        public static final String PDP_WATCHDOG_ERROR_POLL_INTERVAL_MS = "pdp_watchdog_error_poll_interval_ms";
        public static final String PDP_WATCHDOG_LONG_POLL_INTERVAL_MS = "pdp_watchdog_long_poll_interval_ms";
        public static final String PDP_WATCHDOG_MAX_PDP_RESET_FAIL_COUNT = "pdp_watchdog_max_pdp_reset_fail_count";
        public static final String PDP_WATCHDOG_POLL_INTERVAL_MS = "pdp_watchdog_poll_interval_ms";
        public static final String PDP_WATCHDOG_TRIGGER_PACKET_COUNT = "pdp_watchdog_trigger_packet_count";
        public static final String PREFERRED_CDMA_SUBSCRIPTION = "preferred_cdma_subscription";
        public static final String PREFERRED_NETWORK_MODE = "preferred_network_mode";
        public static final String PREFERRED_TTY_MODE = "preferred_tty_mode";
        public static final String READ_EXTERNAL_STORAGE_ENFORCED_DEFAULT = "read_external_storage_enforced_default";
        public static final String REBOOT_INTERVAL = "reboot_interval";
        public static final String REBOOT_START_TIME = "reboot_start_time";
        public static final String REBOOT_WINDOW = "reboot_window";
        public static final String SAMPLING_PROFILER_MS = "sampling_profiler_ms";
        public static final String SCREENSAVER_ACTIVATE_ON_DOCK = "screensaver_activate_on_dock";
        public static final String SCREENSAVER_COMPONENT = "screensaver_component";
        public static final String SCREENSAVER_ENABLED = "screensaver_enabled";
        public static final String SEARCH_GLOBAL_SEARCH_ACTIVITY = "search_global_search_activity";
        public static final String SEARCH_MAX_RESULTS_PER_SOURCE = "search_max_results_per_source";
        public static final String SEARCH_MAX_RESULTS_TO_DISPLAY = "search_max_results_to_display";
        public static final String SEARCH_MAX_SHORTCUTS_RETURNED = "search_max_shortcuts_returned";
        public static final String SEARCH_MAX_SOURCE_EVENT_AGE_MILLIS = "search_max_source_event_age_millis";
        public static final String SEARCH_MAX_STAT_AGE_MILLIS = "search_max_stat_age_millis";
        public static final String SEARCH_MIN_CLICKS_FOR_SOURCE_RANKING = "search_min_clicks_for_source_ranking";
        public static final String SEARCH_MIN_IMPRESSIONS_FOR_SOURCE_RANKING = "search_min_impressions_for_source_ranking";
        public static final String SEARCH_NUM_PROMOTED_SOURCES = "search_num_promoted_sources";
        public static final String SEARCH_PER_SOURCE_CONCURRENT_QUERY_LIMIT = "search_per_source_concurrent_query_limit";
        public static final String SEARCH_PREFILL_MILLIS = "search_prefill_millis";
        public static final String SEARCH_PROMOTED_SOURCE_DEADLINE_MILLIS = "search_promoted_source_deadline_millis";
        public static final String SEARCH_QUERY_THREAD_CORE_POOL_SIZE = "search_query_thread_core_pool_size";
        public static final String SEARCH_QUERY_THREAD_MAX_POOL_SIZE = "search_query_thread_max_pool_size";
        public static final String SEARCH_SHORTCUT_REFRESH_CORE_POOL_SIZE = "search_shortcut_refresh_core_pool_size";
        public static final String SEARCH_SHORTCUT_REFRESH_MAX_POOL_SIZE = "search_shortcut_refresh_max_pool_size";
        public static final String SEARCH_SOURCE_TIMEOUT_MILLIS = "search_source_timeout_millis";
        public static final String SEARCH_THREAD_KEEPALIVE_SECONDS = "search_thread_keepalive_seconds";
        public static final String SEARCH_WEB_RESULTS_OVERRIDE_LIMIT = "search_web_results_override_limit";
        public static final String SELECTED_INPUT_METHOD_SUBTYPE = "selected_input_method_subtype";
        public static final String SELECTED_SPELL_CHECKER = "selected_spell_checker";
        public static final String SELECTED_SPELL_CHECKER_SUBTYPE = "selected_spell_checker_subtype";
        public static final String SEND_ACTION_APP_ERROR = "send_action_app_error";
        public static final String SETTINGS_CLASSNAME = "settings_classname";
        public static final String[] SETTINGS_TO_BACKUP = arrayOfString;
        public static final String SETUP_PREPAID_DATA_SERVICE_URL = "setup_prepaid_data_service_url";
        public static final String SETUP_PREPAID_DETECTION_REDIR_HOST = "setup_prepaid_detection_redir_host";
        public static final String SETUP_PREPAID_DETECTION_TARGET_URL = "setup_prepaid_detection_target_url";
        public static final String SET_GLOBAL_HTTP_PROXY = "set_global_http_proxy";
        public static final String SET_INSTALL_LOCATION = "set_install_location";
        public static final String SHORT_KEYLIGHT_DELAY_MS = "short_keylight_delay_ms";
        public static final String SMS_OUTGOING_CHECK_INTERVAL_MS = "sms_outgoing_check_interval_ms";
        public static final String SMS_OUTGOING_CHECK_MAX_COUNT = "sms_outgoing_check_max_count";
        public static final String SMS_SHORT_CODES_PREFIX = "sms_short_codes_";
        public static final String SPELL_CHECKER_ENABLED = "spell_checker_enabled";
        public static final String SYNC_MAX_RETRY_DELAY_IN_SECONDS = "sync_max_retry_delay_in_seconds";
        public static final String SYS_FREE_STORAGE_LOG_INTERVAL = "sys_free_storage_log_interval";
        public static final String SYS_PROP_SETTING_VERSION = "sys.settings_secure_version";
        public static final String SYS_STORAGE_FULL_THRESHOLD_BYTES = "sys_storage_full_threshold_bytes";
        public static final String SYS_STORAGE_THRESHOLD_MAX_BYTES = "sys_storage_threshold_max_bytes";
        public static final String SYS_STORAGE_THRESHOLD_PERCENTAGE = "sys_storage_threshold_percentage";
        public static final String TETHER_DUN_APN = "tether_dun_apn";
        public static final String TETHER_DUN_REQUIRED = "tether_dun_required";
        public static final String TETHER_SUPPORTED = "tether_supported";
        public static final String THROTTLE_HELP_URI = "throttle_help_uri";
        public static final String THROTTLE_MAX_NTP_CACHE_AGE_SEC = "throttle_max_ntp_cache_age_sec";
        public static final String THROTTLE_NOTIFICATION_TYPE = "throttle_notification_type";
        public static final String THROTTLE_POLLING_SEC = "throttle_polling_sec";
        public static final String THROTTLE_RESET_DAY = "throttle_reset_day";
        public static final String THROTTLE_THRESHOLD_BYTES = "throttle_threshold_bytes";
        public static final String THROTTLE_VALUE_KBITSPS = "throttle_value_kbitsps";
        public static final String TOUCH_EXPLORATION_ENABLED = "touch_exploration_enabled";
        public static final String TOUCH_EXPLORATION_GRANTED_ACCESSIBILITY_SERVICES = "touch_exploration_granted_accessibility_services";

        @Deprecated
        public static final String TTS_DEFAULT_COUNTRY = "tts_default_country";

        @Deprecated
        public static final String TTS_DEFAULT_LANG = "tts_default_lang";
        public static final String TTS_DEFAULT_LOCALE = "tts_default_locale";
        public static final String TTS_DEFAULT_PITCH = "tts_default_pitch";
        public static final String TTS_DEFAULT_RATE = "tts_default_rate";
        public static final String TTS_DEFAULT_SYNTH = "tts_default_synth";

        @Deprecated
        public static final String TTS_DEFAULT_VARIANT = "tts_default_variant";
        public static final String TTS_ENABLED_PLUGINS = "tts_enabled_plugins";

        @Deprecated
        public static final String TTS_USE_DEFAULTS = "tts_use_defaults";
        public static final String TTY_MODE_ENABLED = "tty_mode_enabled";
        public static final String UI_NIGHT_MODE = "ui_night_mode";
        public static final String USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled";
        public static final String USE_GOOGLE_MAIL = "use_google_mail";
        public static final String VOICE_RECOGNITION_SERVICE = "voice_recognition_service";
        public static final String WEB_AUTOFILL_QUERY_URL = "web_autofill_query_url";
        public static final String WIFI_AP_PASSWD = "wifi_ap_passwd";
        public static final String WIFI_AP_SECURITY = "wifi_ap_security";
        public static final String WIFI_AP_SSID = "wifi_ap_ssid";
        public static final String WIFI_COUNTRY_CODE = "wifi_country_code";
        public static final String WIFI_FRAMEWORK_SCAN_INTERVAL_MS = "wifi_framework_scan_interval_ms";
        public static final String WIFI_FREQUENCY_BAND = "wifi_frequency_band";
        public static final String WIFI_IDLE_MS = "wifi_idle_ms";
        public static final String WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count";
        public static final String WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms";
        public static final String WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on";
        public static final String WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay";
        public static final String WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept";
        public static final String WIFI_ON = "wifi_on";
        public static final String WIFI_P2P_DEVICE_NAME = "wifi_p2p_device_name";
        public static final String WIFI_SAVED_STATE = "wifi_saved_state";
        public static final String WIFI_SUPPLICANT_SCAN_INTERVAL_MS = "wifi_supplicant_scan_interval_ms";

        @Deprecated
        public static final String WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE = "wifi_watchdog_acceptable_packet_loss_percentage";

        @Deprecated
        public static final String WIFI_WATCHDOG_AP_COUNT = "wifi_watchdog_ap_count";
        public static final String WIFI_WATCHDOG_ARP_CHECK_INTERVAL_MS = "wifi_watchdog_arp_interval_ms";
        public static final String WIFI_WATCHDOG_ARP_PING_TIMEOUT_MS = "wifi_watchdog_arp_ping_timeout_ms";

        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS = "wifi_watchdog_background_check_delay_ms";

        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED = "wifi_watchdog_background_check_enabled";

        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS = "wifi_watchdog_background_check_timeout_ms";

        @Deprecated
        public static final String WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT = "wifi_watchdog_initial_ignored_ping_count";

        @Deprecated
        public static final String WIFI_WATCHDOG_MAX_AP_CHECKS = "wifi_watchdog_max_ap_checks";
        public static final String WIFI_WATCHDOG_MIN_ARP_RESPONSES = "wifi_watchdog_min_arp_responses";
        public static final String WIFI_WATCHDOG_NUM_ARP_PINGS = "wifi_watchdog_num_arp_pings";
        public static final String WIFI_WATCHDOG_ON = "wifi_watchdog_on";

        @Deprecated
        public static final String WIFI_WATCHDOG_PING_COUNT = "wifi_watchdog_ping_count";

        @Deprecated
        public static final String WIFI_WATCHDOG_PING_DELAY_MS = "wifi_watchdog_ping_delay_ms";

        @Deprecated
        public static final String WIFI_WATCHDOG_PING_TIMEOUT_MS = "wifi_watchdog_ping_timeout_ms";
        public static final String WIFI_WATCHDOG_POOR_NETWORK_TEST_ENABLED = "wifi_watchdog_poor_network_test_enabled";
        public static final String WIFI_WATCHDOG_RSSI_FETCH_INTERVAL_MS = "wifi_watchdog_rssi_fetch_interval_ms";
        public static final String WIFI_WATCHDOG_WALLED_GARDEN_INTERVAL_MS = "wifi_watchdog_walled_garden_interval_ms";
        public static final String WIFI_WATCHDOG_WALLED_GARDEN_TEST_ENABLED = "wifi_watchdog_walled_garden_test_enabled";
        public static final String WIFI_WATCHDOG_WALLED_GARDEN_URL = "wifi_watchdog_walled_garden_url";

        @Deprecated
        public static final String WIFI_WATCHDOG_WATCH_LIST = "wifi_watchdog_watch_list";
        public static final String WIMAX_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wimax_networks_available_notification_on";
        public static final String WTF_IS_FATAL = "wtf_is_fatal";
        private static boolean sIsSystemProcess;
        private static ILockSettings sLockSettings;
        private static Settings.NameValueCache sNameValueCache = null;

        static
        {
            sLockSettings = null;
            MOVED_TO_LOCK_SETTINGS = new HashSet(3);
            MOVED_TO_LOCK_SETTINGS.add("lock_pattern_autolock");
            MOVED_TO_LOCK_SETTINGS.add("lock_pattern_visible_pattern");
            MOVED_TO_LOCK_SETTINGS.add("lock_pattern_tactile_feedback_enabled");
            CONTENT_URI = Uri.parse("content://settings/secure");
            String[] arrayOfString = new String[30];
            arrayOfString[0] = "adb_enabled";
            arrayOfString[1] = "mock_location";
            arrayOfString[2] = "parental_control_enabled";
            arrayOfString[3] = "parental_control_redirect_url";
            arrayOfString[4] = "usb_mass_storage_enabled";
            arrayOfString[5] = "accessibility_script_injection";
            arrayOfString[6] = "backup_auto_restore";
            arrayOfString[7] = "enabled_accessibility_services";
            arrayOfString[8] = "touch_exploration_granted_accessibility_services";
            arrayOfString[9] = "touch_exploration_enabled";
            arrayOfString[10] = "accessibility_enabled";
            arrayOfString[11] = "speak_password";
            arrayOfString[12] = "tts_use_defaults";
            arrayOfString[13] = "tts_default_rate";
            arrayOfString[14] = "tts_default_pitch";
            arrayOfString[15] = "tts_default_synth";
            arrayOfString[16] = "tts_default_lang";
            arrayOfString[17] = "tts_default_country";
            arrayOfString[18] = "tts_enabled_plugins";
            arrayOfString[19] = "tts_default_locale";
            arrayOfString[20] = "wifi_networks_available_notification_on";
            arrayOfString[21] = "wifi_networks_available_repeat_delay";
            arrayOfString[22] = "wifi_num_open_networks_kept";
            arrayOfString[23] = "mount_play_not_snd";
            arrayOfString[24] = "mount_ums_autostart";
            arrayOfString[25] = "mount_ums_prompt";
            arrayOfString[26] = "mount_ums_notify_enabled";
            arrayOfString[27] = "ui_night_mode";
            arrayOfString[28] = "lock_screen_owner_info";
            arrayOfString[29] = "lock_screen_owner_info_enabled";
        }

        public static final String getBluetoothA2dpSinkPriorityKey(String paramString)
        {
            return "bluetooth_a2dp_sink_priority_" + paramString.toUpperCase();
        }

        public static final String getBluetoothHeadsetPriorityKey(String paramString)
        {
            return "bluetooth_headset_priority_" + paramString.toUpperCase();
        }

        public static final String getBluetoothInputDevicePriorityKey(String paramString)
        {
            return "bluetooth_input_device_priority_" + paramString.toUpperCase();
        }

        public static float getFloat(ContentResolver paramContentResolver, String paramString)
            throws Settings.SettingNotFoundException
        {
            String str = getString(paramContentResolver, paramString);
            if (str == null)
                throw new Settings.SettingNotFoundException(paramString);
            try
            {
                float f = Float.parseFloat(str);
                return f;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            throw new Settings.SettingNotFoundException(paramString);
        }

        public static float getFloat(ContentResolver paramContentResolver, String paramString, float paramFloat)
        {
            String str = getString(paramContentResolver, paramString);
            if (str != null);
            try
            {
                float f = Float.parseFloat(str);
                paramFloat = f;
                label19: return paramFloat;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label19;
            }
        }

        public static int getInt(ContentResolver paramContentResolver, String paramString)
            throws Settings.SettingNotFoundException
        {
            String str = getString(paramContentResolver, paramString);
            try
            {
                int i = Integer.parseInt(str);
                return i;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            throw new Settings.SettingNotFoundException(paramString);
        }

        public static int getInt(ContentResolver paramContentResolver, String paramString, int paramInt)
        {
            String str = getString(paramContentResolver, paramString);
            if (str != null);
            try
            {
                int i = Integer.parseInt(str);
                paramInt = i;
                label19: return paramInt;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label19;
            }
        }

        public static long getLong(ContentResolver paramContentResolver, String paramString)
            throws Settings.SettingNotFoundException
        {
            String str = getString(paramContentResolver, paramString);
            try
            {
                long l = Long.parseLong(str);
                return l;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            throw new Settings.SettingNotFoundException(paramString);
        }

        public static long getLong(ContentResolver paramContentResolver, String paramString, long paramLong)
        {
            String str = getString(paramContentResolver, paramString);
            if (str != null);
            try
            {
                long l2 = Long.parseLong(str);
                for (l1 = l2; ; l1 = paramLong)
                    return l1;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                while (true)
                    long l1 = paramLong;
            }
        }

        /** @deprecated */
        public static String getString(ContentResolver paramContentResolver, String paramString)
        {
            try
            {
                if (sNameValueCache == null)
                    sNameValueCache = new Settings.NameValueCache("sys.settings_secure_version", CONTENT_URI, "GET_secure");
                boolean bool2;
                if (sLockSettings == null)
                {
                    sLockSettings = ILockSettings.Stub.asInterface(ServiceManager.getService("lock_settings"));
                    if (Process.myUid() == 1000)
                    {
                        bool2 = true;
                        sIsSystemProcess = bool2;
                    }
                }
                else
                {
                    if ((sLockSettings == null) || (sIsSystemProcess))
                        break label124;
                    boolean bool1 = MOVED_TO_LOCK_SETTINGS.contains(paramString);
                    if (!bool1)
                        break label124;
                }
                while (true)
                {
                    try
                    {
                        String str2 = sLockSettings.getString(paramString, "0", UserId.getCallingUserId());
                        localObject2 = str2;
                        return localObject2;
                        bool2 = false;
                    }
                    catch (RemoteException localRemoteException)
                    {
                    }
                    label124: String str1 = sNameValueCache.getString(paramContentResolver, paramString);
                    Object localObject2 = str1;
                }
            }
            finally
            {
            }
        }

        public static Uri getUriFor(String paramString)
        {
            return getUriFor(CONTENT_URI, paramString);
        }

        public static final boolean isLocationProviderEnabled(ContentResolver paramContentResolver, String paramString)
        {
            return TextUtils.delimitedStringContains(getString(paramContentResolver, "location_providers_allowed"), ',', paramString);
        }

        public static boolean putFloat(ContentResolver paramContentResolver, String paramString, float paramFloat)
        {
            return putString(paramContentResolver, paramString, Float.toString(paramFloat));
        }

        public static boolean putInt(ContentResolver paramContentResolver, String paramString, int paramInt)
        {
            return putString(paramContentResolver, paramString, Integer.toString(paramInt));
        }

        public static boolean putLong(ContentResolver paramContentResolver, String paramString, long paramLong)
        {
            return putString(paramContentResolver, paramString, Long.toString(paramLong));
        }

        public static boolean putString(ContentResolver paramContentResolver, String paramString1, String paramString2)
        {
            return putString(paramContentResolver, CONTENT_URI, paramString1, paramString2);
        }

        public static final void setLocationProviderEnabled(ContentResolver paramContentResolver, String paramString, boolean paramBoolean)
        {
            if (paramBoolean);
            for (String str = "+" + paramString; ; str = "-" + paramString)
            {
                putString(paramContentResolver, "location_providers_allowed", str);
                return;
            }
        }
    }

    public static final class System extends Settings.NameValueTable
    {
        public static final String ACCELEROMETER_ROTATION = "accelerometer_rotation";

        @Deprecated
        public static final String ADB_ENABLED = "adb_enabled";
        public static final String ADVANCED_SETTINGS = "advanced_settings";
        public static final int ADVANCED_SETTINGS_DEFAULT = 0;
        public static final String AIRPLANE_MODE_ON = "airplane_mode_on";
        public static final String AIRPLANE_MODE_RADIOS = "airplane_mode_radios";
        public static final String AIRPLANE_MODE_TOGGLEABLE_RADIOS = "airplane_mode_toggleable_radios";
        public static final String ALARM_ALERT = "alarm_alert";
        public static final String ALWAYS_FINISH_ACTIVITIES = "always_finish_activities";

        @Deprecated
        public static final String ANDROID_ID = "android_id";
        public static final String ANIMATOR_DURATION_SCALE = "animator_duration_scale";
        public static final String APPEND_FOR_LAST_AUDIBLE = "_last_audible";
        public static final String AUTO_TIME = "auto_time";
        public static final String AUTO_TIME_ZONE = "auto_time_zone";
        public static final String BLUETOOTH_DISCOVERABILITY = "bluetooth_discoverability";
        public static final String BLUETOOTH_DISCOVERABILITY_TIMEOUT = "bluetooth_discoverability_timeout";

        @Deprecated
        public static final String BLUETOOTH_ON = "bluetooth_on";
        public static final String CALL_AUTO_RETRY = "call_auto_retry";
        public static final String CAR_DOCK_SOUND = "car_dock_sound";
        public static final String CAR_UNDOCK_SOUND = "car_undock_sound";
        public static final String COMPATIBILITY_MODE = "compatibility_mode";
        public static final Uri CONTENT_URI;

        @Deprecated
        public static final String DATA_ROAMING = "data_roaming";
        public static final String DATE_FORMAT = "date_format";
        public static final String DEBUG_APP = "debug_app";
        public static final Uri DEFAULT_ALARM_ALERT_URI;
        public static final Uri DEFAULT_NOTIFICATION_URI;
        public static final Uri DEFAULT_RINGTONE_URI;
        public static final String DESK_DOCK_SOUND = "desk_dock_sound";
        public static final String DESK_UNDOCK_SOUND = "desk_undock_sound";

        @Deprecated
        public static final String DEVICE_PROVISIONED = "device_provisioned";
        public static final String DIM_SCREEN = "dim_screen";
        public static final String DOCK_SOUNDS_ENABLED = "dock_sounds_enabled";
        public static final String DTMF_TONE_TYPE_WHEN_DIALING = "dtmf_tone_type";
        public static final String DTMF_TONE_WHEN_DIALING = "dtmf_tone";
        public static final String EMERGENCY_TONE = "emergency_tone";
        public static final String END_BUTTON_BEHAVIOR = "end_button_behavior";
        public static final int END_BUTTON_BEHAVIOR_DEFAULT = 2;
        public static final int END_BUTTON_BEHAVIOR_HOME = 1;
        public static final int END_BUTTON_BEHAVIOR_SLEEP = 2;
        public static final String FANCY_IME_ANIMATIONS = "fancy_ime_animations";
        public static final String FONT_SCALE = "font_scale";
        public static final String HAPTIC_FEEDBACK_ENABLED = "haptic_feedback_enabled";
        public static final String HEARING_AID = "hearing_aid";
        public static final String HIDE_ROTATION_LOCK_TOGGLE_FOR_ACCESSIBILITY = "hide_rotation_lock_toggle_for_accessibility";

        @Deprecated
        public static final String HTTP_PROXY = "http_proxy";

        @Deprecated
        public static final String INSTALL_NON_MARKET_APPS = "install_non_market_apps";

        @Deprecated
        public static final String LOCATION_PROVIDERS_ALLOWED = "location_providers_allowed";
        public static final String LOCKSCREEN_DISABLED = "lockscreen.disabled";
        public static final String LOCKSCREEN_SOUNDS_ENABLED = "lockscreen_sounds_enabled";

        @Deprecated
        public static final String LOCK_PATTERN_ENABLED = "lock_pattern_autolock";

        @Deprecated
        public static final String LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED = "lock_pattern_tactile_feedback_enabled";

        @Deprecated
        public static final String LOCK_PATTERN_VISIBLE = "lock_pattern_visible_pattern";
        public static final String LOCK_SOUND = "lock_sound";

        @Deprecated
        public static final String LOGGING_ID = "logging_id";
        public static final String LOW_BATTERY_SOUND = "low_battery_sound";
        public static final String MEDIA_BUTTON_RECEIVER = "media_button_receiver";
        public static final String MODE_RINGER = "mode_ringer";
        public static final String MODE_RINGER_STREAMS_AFFECTED = "mode_ringer_streams_affected";
        private static final HashSet<String> MOVED_TO_SECURE;
        public static final String MUTE_STREAMS_AFFECTED = "mute_streams_affected";

        @Deprecated
        public static final String NETWORK_PREFERENCE = "network_preference";
        public static final String NEXT_ALARM_FORMATTED = "next_alarm_formatted";

        @Deprecated
        public static final String NOTIFICATIONS_USE_RING_VOLUME = "notifications_use_ring_volume";
        public static final String NOTIFICATION_LIGHT_PULSE = "notification_light_pulse";
        public static final String NOTIFICATION_SOUND = "notification_sound";

        @Deprecated
        public static final String PARENTAL_CONTROL_ENABLED = "parental_control_enabled";

        @Deprecated
        public static final String PARENTAL_CONTROL_LAST_UPDATE = "parental_control_last_update";

        @Deprecated
        public static final String PARENTAL_CONTROL_REDIRECT_URL = "parental_control_redirect_url";
        public static final String POINTER_LOCATION = "pointer_location";
        public static final String POINTER_SPEED = "pointer_speed";
        public static final String POWER_SOUNDS_ENABLED = "power_sounds_enabled";
        public static final String RADIO_BLUETOOTH = "bluetooth";
        public static final String RADIO_CELL = "cell";
        public static final String RADIO_NFC = "nfc";
        public static final String RADIO_WIFI = "wifi";
        public static final String RADIO_WIMAX = "wimax";
        public static final String RINGTONE = "ringtone";
        public static final String SCREEN_AUTO_BRIGHTNESS_ADJ = "screen_auto_brightness_adj";
        public static final String SCREEN_BRIGHTNESS = "screen_brightness";
        public static final String SCREEN_BRIGHTNESS_MODE = "screen_brightness_mode";
        public static final int SCREEN_BRIGHTNESS_MODE_AUTOMATIC = 1;
        public static final int SCREEN_BRIGHTNESS_MODE_MANUAL = 0;
        public static final String SCREEN_OFF_TIMEOUT = "screen_off_timeout";

        @Deprecated
        public static final String SETTINGS_CLASSNAME = "settings_classname";
        public static final String[] SETTINGS_TO_BACKUP = arrayOfString2;
        public static final String SETUP_WIZARD_HAS_RUN = "setup_wizard_has_run";
        public static final String SHOW_GTALK_SERVICE_STATUS = "SHOW_GTALK_SERVICE_STATUS";
        public static final String SHOW_PROCESSES = "show_processes";
        public static final String SHOW_TOUCHES = "show_touches";

        @Deprecated
        public static final String SHOW_WEB_SUGGESTIONS = "show_web_suggestions";
        public static final String SIP_ADDRESS_ONLY = "SIP_ADDRESS_ONLY";
        public static final String SIP_ALWAYS = "SIP_ALWAYS";
        public static final String SIP_ASK_ME_EACH_TIME = "SIP_ASK_ME_EACH_TIME";
        public static final String SIP_CALL_OPTIONS = "sip_call_options";
        public static final String SIP_RECEIVE_CALLS = "sip_receive_calls";
        public static final String SOUND_EFFECTS_ENABLED = "sound_effects_enabled";
        public static final String STAY_ON_WHILE_PLUGGED_IN = "stay_on_while_plugged_in";
        public static final String SYS_PROP_SETTING_VERSION = "sys.settings_system_version";
        public static final String TEXT_AUTO_CAPS = "auto_caps";
        public static final String TEXT_AUTO_PUNCTUATE = "auto_punctuate";
        public static final String TEXT_AUTO_REPLACE = "auto_replace";
        public static final String TEXT_SHOW_PASSWORD = "show_password";
        public static final String TIME_12_24 = "time_12_24";
        public static final String TRANSITION_ANIMATION_SCALE = "transition_animation_scale";
        public static final String TTY_MODE = "tty_mode";
        public static final String UNLOCK_SOUND = "unlock_sound";

        @Deprecated
        public static final String USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled";
        public static final String USER_ROTATION = "user_rotation";

        @Deprecated
        public static final String USE_GOOGLE_MAIL = "use_google_mail";
        public static final String VIBRATE_INPUT_DEVICES = "vibrate_input_devices";
        public static final String VIBRATE_IN_SILENT = "vibrate_in_silent";
        public static final String VIBRATE_ON = "vibrate_on";
        public static final String VIBRATE_WHEN_RINGING = "vibrate_when_ringing";
        public static final String VOLUME_ALARM = "volume_alarm";
        public static final String VOLUME_BLUETOOTH_SCO = "volume_bluetooth_sco";
        public static final String VOLUME_MASTER = "volume_master";
        public static final String VOLUME_MASTER_MUTE = "volume_master_mute";
        public static final String VOLUME_MUSIC = "volume_music";
        public static final String VOLUME_NOTIFICATION = "volume_notification";
        public static final String VOLUME_RING = "volume_ring";
        public static final String[] VOLUME_SETTINGS;
        public static final String VOLUME_SYSTEM = "volume_system";
        public static final String VOLUME_VOICE = "volume_voice";
        public static final String WAIT_FOR_DEBUGGER = "wait_for_debugger";
        public static final String WALLPAPER_ACTIVITY = "wallpaper_activity";

        @Deprecated
        public static final String WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count";

        @Deprecated
        public static final String WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms";

        @Deprecated
        public static final String WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on";

        @Deprecated
        public static final String WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay";

        @Deprecated
        public static final String WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept";

        @Deprecated
        public static final String WIFI_ON = "wifi_on";
        public static final String WIFI_SLEEP_POLICY = "wifi_sleep_policy";
        public static final int WIFI_SLEEP_POLICY_DEFAULT = 0;
        public static final int WIFI_SLEEP_POLICY_NEVER = 2;
        public static final int WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED = 1;
        public static final String WIFI_STATIC_DNS1 = "wifi_static_dns1";
        public static final String WIFI_STATIC_DNS2 = "wifi_static_dns2";
        public static final String WIFI_STATIC_GATEWAY = "wifi_static_gateway";
        public static final String WIFI_STATIC_IP = "wifi_static_ip";
        public static final String WIFI_STATIC_NETMASK = "wifi_static_netmask";
        public static final String WIFI_USE_STATIC_IP = "wifi_use_static_ip";

        @Deprecated
        public static final String WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE = "wifi_watchdog_acceptable_packet_loss_percentage";

        @Deprecated
        public static final String WIFI_WATCHDOG_AP_COUNT = "wifi_watchdog_ap_count";

        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS = "wifi_watchdog_background_check_delay_ms";

        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED = "wifi_watchdog_background_check_enabled";

        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS = "wifi_watchdog_background_check_timeout_ms";

        @Deprecated
        public static final String WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT = "wifi_watchdog_initial_ignored_ping_count";

        @Deprecated
        public static final String WIFI_WATCHDOG_MAX_AP_CHECKS = "wifi_watchdog_max_ap_checks";

        @Deprecated
        public static final String WIFI_WATCHDOG_ON = "wifi_watchdog_on";

        @Deprecated
        public static final String WIFI_WATCHDOG_PING_COUNT = "wifi_watchdog_ping_count";

        @Deprecated
        public static final String WIFI_WATCHDOG_PING_DELAY_MS = "wifi_watchdog_ping_delay_ms";

        @Deprecated
        public static final String WIFI_WATCHDOG_PING_TIMEOUT_MS = "wifi_watchdog_ping_timeout_ms";
        public static final String WINDOW_ANIMATION_SCALE = "window_animation_scale";
        public static final String WINDOW_ORIENTATION_LISTENER_LOG = "window_orientation_listener_log";
        private static Settings.NameValueCache sNameValueCache = null;

        static
        {
            MOVED_TO_SECURE = new HashSet(30);
            MOVED_TO_SECURE.add("adb_enabled");
            MOVED_TO_SECURE.add("android_id");
            MOVED_TO_SECURE.add("bluetooth_on");
            MOVED_TO_SECURE.add("data_roaming");
            MOVED_TO_SECURE.add("device_provisioned");
            MOVED_TO_SECURE.add("http_proxy");
            MOVED_TO_SECURE.add("install_non_market_apps");
            MOVED_TO_SECURE.add("location_providers_allowed");
            MOVED_TO_SECURE.add("lock_biometric_weak_flags");
            MOVED_TO_SECURE.add("lock_pattern_autolock");
            MOVED_TO_SECURE.add("lock_pattern_visible_pattern");
            MOVED_TO_SECURE.add("lock_pattern_tactile_feedback_enabled");
            MOVED_TO_SECURE.add("logging_id");
            MOVED_TO_SECURE.add("parental_control_enabled");
            MOVED_TO_SECURE.add("parental_control_last_update");
            MOVED_TO_SECURE.add("parental_control_redirect_url");
            MOVED_TO_SECURE.add("settings_classname");
            MOVED_TO_SECURE.add("usb_mass_storage_enabled");
            MOVED_TO_SECURE.add("use_google_mail");
            MOVED_TO_SECURE.add("wifi_networks_available_notification_on");
            MOVED_TO_SECURE.add("wifi_networks_available_repeat_delay");
            MOVED_TO_SECURE.add("wifi_num_open_networks_kept");
            MOVED_TO_SECURE.add("wifi_on");
            MOVED_TO_SECURE.add("wifi_watchdog_acceptable_packet_loss_percentage");
            MOVED_TO_SECURE.add("wifi_watchdog_ap_count");
            MOVED_TO_SECURE.add("wifi_watchdog_background_check_delay_ms");
            MOVED_TO_SECURE.add("wifi_watchdog_background_check_enabled");
            MOVED_TO_SECURE.add("wifi_watchdog_background_check_timeout_ms");
            MOVED_TO_SECURE.add("wifi_watchdog_initial_ignored_ping_count");
            MOVED_TO_SECURE.add("wifi_watchdog_max_ap_checks");
            MOVED_TO_SECURE.add("wifi_watchdog_on");
            MOVED_TO_SECURE.add("wifi_watchdog_ping_count");
            MOVED_TO_SECURE.add("wifi_watchdog_ping_delay_ms");
            MOVED_TO_SECURE.add("wifi_watchdog_ping_timeout_ms");
            CONTENT_URI = Uri.parse("content://settings/system");
            String[] arrayOfString1 = new String[7];
            arrayOfString1[0] = "volume_voice";
            arrayOfString1[1] = "volume_system";
            arrayOfString1[2] = "volume_ring";
            arrayOfString1[3] = "volume_music";
            arrayOfString1[4] = "volume_alarm";
            arrayOfString1[5] = "volume_notification";
            arrayOfString1[6] = "volume_bluetooth_sco";
            VOLUME_SETTINGS = arrayOfString1;
            DEFAULT_RINGTONE_URI = getUriFor("ringtone");
            DEFAULT_NOTIFICATION_URI = getUriFor("notification_sound");
            DEFAULT_ALARM_ALERT_URI = getUriFor("alarm_alert");
            String[] arrayOfString2 = new String[57];
            arrayOfString2[0] = "stay_on_while_plugged_in";
            arrayOfString2[1] = "wifi_use_static_ip";
            arrayOfString2[2] = "wifi_static_ip";
            arrayOfString2[3] = "wifi_static_gateway";
            arrayOfString2[4] = "wifi_static_netmask";
            arrayOfString2[5] = "wifi_static_dns1";
            arrayOfString2[6] = "wifi_static_dns2";
            arrayOfString2[7] = "bluetooth_discoverability";
            arrayOfString2[8] = "bluetooth_discoverability_timeout";
            arrayOfString2[9] = "dim_screen";
            arrayOfString2[10] = "screen_off_timeout";
            arrayOfString2[11] = "screen_brightness";
            arrayOfString2[12] = "screen_brightness_mode";
            arrayOfString2[13] = "screen_auto_brightness_adj";
            arrayOfString2[14] = "vibrate_input_devices";
            arrayOfString2[15] = "mode_ringer";
            arrayOfString2[16] = "mode_ringer_streams_affected";
            arrayOfString2[17] = "mute_streams_affected";
            arrayOfString2[18] = "volume_voice";
            arrayOfString2[19] = "volume_system";
            arrayOfString2[20] = "volume_ring";
            arrayOfString2[21] = "volume_music";
            arrayOfString2[22] = "volume_alarm";
            arrayOfString2[23] = "volume_notification";
            arrayOfString2[24] = "volume_bluetooth_sco";
            arrayOfString2[25] = "volume_voice_last_audible";
            arrayOfString2[26] = "volume_system_last_audible";
            arrayOfString2[27] = "volume_ring_last_audible";
            arrayOfString2[28] = "volume_music_last_audible";
            arrayOfString2[29] = "volume_alarm_last_audible";
            arrayOfString2[30] = "volume_notification_last_audible";
            arrayOfString2[31] = "volume_bluetooth_sco_last_audible";
            arrayOfString2[32] = "auto_replace";
            arrayOfString2[33] = "auto_caps";
            arrayOfString2[34] = "auto_punctuate";
            arrayOfString2[35] = "show_password";
            arrayOfString2[36] = "auto_time";
            arrayOfString2[37] = "auto_time_zone";
            arrayOfString2[38] = "time_12_24";
            arrayOfString2[39] = "date_format";
            arrayOfString2[40] = "dtmf_tone";
            arrayOfString2[41] = "dtmf_tone_type";
            arrayOfString2[42] = "emergency_tone";
            arrayOfString2[43] = "call_auto_retry";
            arrayOfString2[44] = "hearing_aid";
            arrayOfString2[45] = "tty_mode";
            arrayOfString2[46] = "sound_effects_enabled";
            arrayOfString2[47] = "haptic_feedback_enabled";
            arrayOfString2[48] = "power_sounds_enabled";
            arrayOfString2[49] = "dock_sounds_enabled";
            arrayOfString2[50] = "lockscreen_sounds_enabled";
            arrayOfString2[51] = "show_web_suggestions";
            arrayOfString2[52] = "notification_light_pulse";
            arrayOfString2[53] = "sip_call_options";
            arrayOfString2[54] = "sip_receive_calls";
            arrayOfString2[55] = "pointer_speed";
            arrayOfString2[56] = "vibrate_when_ringing";
        }

        public static void clearConfiguration(Configuration paramConfiguration)
        {
            paramConfiguration.fontScale = 0.0F;
        }

        public static void getConfiguration(ContentResolver paramContentResolver, Configuration paramConfiguration)
        {
            paramConfiguration.fontScale = getFloat(paramContentResolver, "font_scale", paramConfiguration.fontScale);
            if (paramConfiguration.fontScale < 0.0F)
                paramConfiguration.fontScale = 1.0F;
        }

        public static float getFloat(ContentResolver paramContentResolver, String paramString)
            throws Settings.SettingNotFoundException
        {
            String str = getString(paramContentResolver, paramString);
            if (str == null)
                throw new Settings.SettingNotFoundException(paramString);
            try
            {
                float f = Float.parseFloat(str);
                return f;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            throw new Settings.SettingNotFoundException(paramString);
        }

        public static float getFloat(ContentResolver paramContentResolver, String paramString, float paramFloat)
        {
            String str = getString(paramContentResolver, paramString);
            if (str != null);
            try
            {
                float f = Float.parseFloat(str);
                paramFloat = f;
                label19: return paramFloat;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label19;
            }
        }

        public static int getInt(ContentResolver paramContentResolver, String paramString)
            throws Settings.SettingNotFoundException
        {
            String str = getString(paramContentResolver, paramString);
            try
            {
                int i = Integer.parseInt(str);
                return i;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            throw new Settings.SettingNotFoundException(paramString);
        }

        public static int getInt(ContentResolver paramContentResolver, String paramString, int paramInt)
        {
            String str = getString(paramContentResolver, paramString);
            if (str != null);
            try
            {
                int i = Integer.parseInt(str);
                paramInt = i;
                label19: return paramInt;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label19;
            }
        }

        public static long getLong(ContentResolver paramContentResolver, String paramString)
            throws Settings.SettingNotFoundException
        {
            String str = getString(paramContentResolver, paramString);
            try
            {
                long l = Long.parseLong(str);
                return l;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            throw new Settings.SettingNotFoundException(paramString);
        }

        public static long getLong(ContentResolver paramContentResolver, String paramString, long paramLong)
        {
            String str = getString(paramContentResolver, paramString);
            if (str != null);
            try
            {
                long l2 = Long.parseLong(str);
                for (l1 = l2; ; l1 = paramLong)
                    return l1;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                while (true)
                    long l1 = paramLong;
            }
        }

        public static boolean getShowGTalkServiceStatus(ContentResolver paramContentResolver)
        {
            boolean bool = false;
            if (getInt(paramContentResolver, "SHOW_GTALK_SERVICE_STATUS", 0) != 0)
                bool = true;
            return bool;
        }

        /** @deprecated */
        public static String getString(ContentResolver paramContentResolver, String paramString)
        {
            try
            {
                String str2;
                if (MOVED_TO_SECURE.contains(paramString))
                {
                    Log.w("Settings", "Setting " + paramString + " has moved from android.provider.Settings.System" + " to android.provider.Settings.Secure, returning read-only value.");
                    str2 = Settings.Secure.getString(paramContentResolver, paramString);
                }
                String str1;
                for (Object localObject2 = str2; ; localObject2 = str1)
                {
                    return localObject2;
                    if (sNameValueCache == null)
                        sNameValueCache = new Settings.NameValueCache("sys.settings_system_version", CONTENT_URI, "GET_system");
                    str1 = sNameValueCache.getString(paramContentResolver, paramString);
                }
            }
            finally
            {
            }
        }

        public static Uri getUriFor(String paramString)
        {
            if (MOVED_TO_SECURE.contains(paramString))
                Log.w("Settings", "Setting " + paramString + " has moved from android.provider.Settings.System" + " to android.provider.Settings.Secure, returning Secure URI.");
            for (Uri localUri = Settings.Secure.getUriFor(Settings.Secure.CONTENT_URI, paramString); ; localUri = getUriFor(CONTENT_URI, paramString))
                return localUri;
        }

        public static boolean hasInterestingConfigurationChanges(int paramInt)
        {
            if ((0x40000000 & paramInt) != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public static boolean putConfiguration(ContentResolver paramContentResolver, Configuration paramConfiguration)
        {
            return putFloat(paramContentResolver, "font_scale", paramConfiguration.fontScale);
        }

        public static boolean putFloat(ContentResolver paramContentResolver, String paramString, float paramFloat)
        {
            return putString(paramContentResolver, paramString, Float.toString(paramFloat));
        }

        public static boolean putInt(ContentResolver paramContentResolver, String paramString, int paramInt)
        {
            return putString(paramContentResolver, paramString, Integer.toString(paramInt));
        }

        public static boolean putLong(ContentResolver paramContentResolver, String paramString, long paramLong)
        {
            return putString(paramContentResolver, paramString, Long.toString(paramLong));
        }

        public static boolean putString(ContentResolver paramContentResolver, String paramString1, String paramString2)
        {
            if (MOVED_TO_SECURE.contains(paramString1))
                Log.w("Settings", "Setting " + paramString1 + " has moved from android.provider.Settings.System" + " to android.provider.Settings.Secure, value is unchanged.");
            for (boolean bool = false; ; bool = putString(paramContentResolver, CONTENT_URI, paramString1, paramString2))
                return bool;
        }

        public static void setShowGTalkServiceStatus(ContentResolver paramContentResolver, boolean paramBoolean)
        {
            if (paramBoolean);
            for (int i = 1; ; i = 0)
            {
                putInt(paramContentResolver, "SHOW_GTALK_SERVICE_STATUS", i);
                return;
            }
        }
    }

    private static class NameValueCache
    {
        private static final String NAME_EQ_PLACEHOLDER = "name=?";
        private static final String[] SELECT_VALUE = arrayOfString;
        private final String mCallCommand;
        private IContentProvider mContentProvider = null;
        private final Uri mUri;
        private final HashMap<String, String> mValues = new HashMap();
        private long mValuesVersion = 0L;
        private final String mVersionSystemProperty;

        static
        {
            String[] arrayOfString = new String[1];
            arrayOfString[0] = "value";
        }

        public NameValueCache(String paramString1, Uri paramUri, String paramString2)
        {
            this.mVersionSystemProperty = paramString1;
            this.mUri = paramUri;
            this.mCallCommand = paramString2;
        }

        // ERROR //
        public String getString(ContentResolver paramContentResolver, String paramString)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 47	android/provider/Settings$NameValueCache:mVersionSystemProperty	Ljava/lang/String;
            //     4: lconst_0
            //     5: invokestatic 61	android/os/SystemProperties:getLong	(Ljava/lang/String;J)J
            //     8: lstore_3
            //     9: aload_0
            //     10: monitorenter
            //     11: aload_0
            //     12: getfield 43	android/provider/Settings$NameValueCache:mValuesVersion	J
            //     15: lload_3
            //     16: lcmp
            //     17: ifeq +15 -> 32
            //     20: aload_0
            //     21: getfield 41	android/provider/Settings$NameValueCache:mValues	Ljava/util/HashMap;
            //     24: invokevirtual 64	java/util/HashMap:clear	()V
            //     27: aload_0
            //     28: lload_3
            //     29: putfield 43	android/provider/Settings$NameValueCache:mValuesVersion	J
            //     32: aload_0
            //     33: getfield 41	android/provider/Settings$NameValueCache:mValues	Ljava/util/HashMap;
            //     36: aload_2
            //     37: invokevirtual 68	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
            //     40: ifeq +25 -> 65
            //     43: aload_0
            //     44: getfield 41	android/provider/Settings$NameValueCache:mValues	Ljava/util/HashMap;
            //     47: aload_2
            //     48: invokevirtual 72	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     51: checkcast 28	java/lang/String
            //     54: astore 24
            //     56: aload_0
            //     57: monitorexit
            //     58: aload 24
            //     60: astore 12
            //     62: goto +361 -> 423
            //     65: aload_0
            //     66: monitorexit
            //     67: aload_0
            //     68: monitorenter
            //     69: aload_0
            //     70: getfield 45	android/provider/Settings$NameValueCache:mContentProvider	Landroid/content/IContentProvider;
            //     73: astore 7
            //     75: aload 7
            //     77: ifnonnull +26 -> 103
            //     80: aload_1
            //     81: aload_0
            //     82: getfield 49	android/provider/Settings$NameValueCache:mUri	Landroid/net/Uri;
            //     85: invokevirtual 78	android/net/Uri:getAuthority	()Ljava/lang/String;
            //     88: invokevirtual 84	android/content/ContentResolver:acquireProvider	(Ljava/lang/String;)Landroid/content/IContentProvider;
            //     91: astore 23
            //     93: aload_0
            //     94: aload 23
            //     96: putfield 45	android/provider/Settings$NameValueCache:mContentProvider	Landroid/content/IContentProvider;
            //     99: aload 23
            //     101: astore 7
            //     103: aload_0
            //     104: monitorexit
            //     105: aload_0
            //     106: getfield 51	android/provider/Settings$NameValueCache:mCallCommand	Ljava/lang/String;
            //     109: ifnull +57 -> 166
            //     112: aload 7
            //     114: aload_0
            //     115: getfield 51	android/provider/Settings$NameValueCache:mCallCommand	Ljava/lang/String;
            //     118: aload_2
            //     119: aconst_null
            //     120: invokeinterface 90 4 0
            //     125: astore 20
            //     127: aload 20
            //     129: ifnull +37 -> 166
            //     132: aload 20
            //     134: invokevirtual 95	android/os/Bundle:getPairValue	()Ljava/lang/String;
            //     137: astore 12
            //     139: aload_0
            //     140: monitorenter
            //     141: aload_0
            //     142: getfield 41	android/provider/Settings$NameValueCache:mValues	Ljava/util/HashMap;
            //     145: aload_2
            //     146: aload 12
            //     148: invokevirtual 99	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     151: pop
            //     152: aload_0
            //     153: monitorexit
            //     154: goto +269 -> 423
            //     157: astore 21
            //     159: aload_0
            //     160: monitorexit
            //     161: aload 21
            //     163: athrow
            //     164: astore 19
            //     166: aconst_null
            //     167: astore 8
            //     169: aload_0
            //     170: getfield 49	android/provider/Settings$NameValueCache:mUri	Landroid/net/Uri;
            //     173: astore 13
            //     175: getstatic 32	android/provider/Settings$NameValueCache:SELECT_VALUE	[Ljava/lang/String;
            //     178: astore 14
            //     180: iconst_1
            //     181: anewarray 28	java/lang/String
            //     184: astore 15
            //     186: aload 15
            //     188: iconst_0
            //     189: aload_2
            //     190: aastore
            //     191: aload 7
            //     193: aload 13
            //     195: aload 14
            //     197: ldc 11
            //     199: aload 15
            //     201: aconst_null
            //     202: aconst_null
            //     203: invokeinterface 103 7 0
            //     208: astore 8
            //     210: aload 8
            //     212: ifnonnull +72 -> 284
            //     215: ldc 105
            //     217: new 107	java/lang/StringBuilder
            //     220: dup
            //     221: invokespecial 108	java/lang/StringBuilder:<init>	()V
            //     224: ldc 110
            //     226: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     229: aload_2
            //     230: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     233: ldc 116
            //     235: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     238: aload_0
            //     239: getfield 49	android/provider/Settings$NameValueCache:mUri	Landroid/net/Uri;
            //     242: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     245: invokevirtual 122	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     248: invokestatic 128	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     251: pop
            //     252: aconst_null
            //     253: astore 12
            //     255: aload 8
            //     257: ifnull +166 -> 423
            //     260: aload 8
            //     262: invokeinterface 133 1 0
            //     267: goto +156 -> 423
            //     270: astore 5
            //     272: aload_0
            //     273: monitorexit
            //     274: aload 5
            //     276: athrow
            //     277: astore 6
            //     279: aload_0
            //     280: monitorexit
            //     281: aload 6
            //     283: athrow
            //     284: aload 8
            //     286: invokeinterface 137 1 0
            //     291: ifeq +43 -> 334
            //     294: aload 8
            //     296: iconst_0
            //     297: invokeinterface 140 2 0
            //     302: astore 12
            //     304: aload_0
            //     305: monitorenter
            //     306: aload_0
            //     307: getfield 41	android/provider/Settings$NameValueCache:mValues	Ljava/util/HashMap;
            //     310: aload_2
            //     311: aload 12
            //     313: invokevirtual 99	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     316: pop
            //     317: aload_0
            //     318: monitorexit
            //     319: aload 8
            //     321: ifnull +102 -> 423
            //     324: aload 8
            //     326: invokeinterface 133 1 0
            //     331: goto +92 -> 423
            //     334: aconst_null
            //     335: astore 12
            //     337: goto -33 -> 304
            //     340: astore 16
            //     342: aload_0
            //     343: monitorexit
            //     344: aload 16
            //     346: athrow
            //     347: astore 10
            //     349: ldc 105
            //     351: new 107	java/lang/StringBuilder
            //     354: dup
            //     355: invokespecial 108	java/lang/StringBuilder:<init>	()V
            //     358: ldc 110
            //     360: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     363: aload_2
            //     364: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     367: ldc 116
            //     369: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     372: aload_0
            //     373: getfield 49	android/provider/Settings$NameValueCache:mUri	Landroid/net/Uri;
            //     376: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     379: invokevirtual 122	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     382: aload 10
            //     384: invokestatic 143	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     387: pop
            //     388: aconst_null
            //     389: astore 12
            //     391: aload 8
            //     393: ifnull +30 -> 423
            //     396: aload 8
            //     398: invokeinterface 133 1 0
            //     403: goto +20 -> 423
            //     406: astore 9
            //     408: aload 8
            //     410: ifnull +10 -> 420
            //     413: aload 8
            //     415: invokeinterface 133 1 0
            //     420: aload 9
            //     422: athrow
            //     423: aload 12
            //     425: areturn
            //
            // Exception table:
            //     from	to	target	type
            //     141	161	157	finally
            //     112	141	164	android/os/RemoteException
            //     161	164	164	android/os/RemoteException
            //     11	67	270	finally
            //     272	274	270	finally
            //     69	105	277	finally
            //     279	281	277	finally
            //     306	319	340	finally
            //     342	344	340	finally
            //     169	252	347	android/os/RemoteException
            //     284	306	347	android/os/RemoteException
            //     344	347	347	android/os/RemoteException
            //     169	252	406	finally
            //     284	306	406	finally
            //     344	347	406	finally
            //     349	388	406	finally
        }
    }

    public static class NameValueTable
        implements BaseColumns
    {
        public static final String NAME = "name";
        public static final String VALUE = "value";

        public static Uri getUriFor(Uri paramUri, String paramString)
        {
            return Uri.withAppendedPath(paramUri, paramString);
        }

        protected static boolean putString(ContentResolver paramContentResolver, Uri paramUri, String paramString1, String paramString2)
        {
            try
            {
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("name", paramString1);
                localContentValues.put("value", paramString2);
                paramContentResolver.insert(paramUri, localContentValues);
                bool = true;
                return bool;
            }
            catch (SQLException localSQLException)
            {
                while (true)
                {
                    Log.w("Settings", "Can't set key " + paramString1 + " in " + paramUri, localSQLException);
                    boolean bool = false;
                }
            }
        }
    }

    public static class SettingNotFoundException extends AndroidException
    {
        public SettingNotFoundException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.Settings
 * JD-Core Version:        0.6.2
 */