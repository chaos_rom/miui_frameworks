package android.provider;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebIconDatabase;
import android.webkit.WebIconDatabase.IconListener;

public class Browser
{
    public static final Uri BOOKMARKS_URI = Uri.parse("content://browser/bookmarks");
    public static final String EXTRA_APPLICATION_ID = "com.android.browser.application_id";
    public static final String EXTRA_CREATE_NEW_TAB = "create_new_tab";
    public static final String EXTRA_HEADERS = "com.android.browser.headers";
    public static final String EXTRA_SHARE_FAVICON = "share_favicon";
    public static final String EXTRA_SHARE_SCREENSHOT = "share_screenshot";
    public static final String[] HISTORY_PROJECTION;
    public static final int HISTORY_PROJECTION_BOOKMARK_INDEX = 4;
    public static final int HISTORY_PROJECTION_DATE_INDEX = 3;
    public static final int HISTORY_PROJECTION_FAVICON_INDEX = 6;
    public static final int HISTORY_PROJECTION_ID_INDEX = 0;
    public static final int HISTORY_PROJECTION_THUMBNAIL_INDEX = 7;
    public static final int HISTORY_PROJECTION_TITLE_INDEX = 5;
    public static final int HISTORY_PROJECTION_TOUCH_ICON_INDEX = 8;
    public static final int HISTORY_PROJECTION_URL_INDEX = 1;
    public static final int HISTORY_PROJECTION_VISITS_INDEX = 2;
    public static final String INITIAL_ZOOM_LEVEL = "browser.initialZoomLevel";
    private static final String LOGTAG = "browser";
    private static final int MAX_HISTORY_COUNT = 250;
    public static final String[] SEARCHES_PROJECTION = arrayOfString3;
    public static final int SEARCHES_PROJECTION_DATE_INDEX = 2;
    public static final int SEARCHES_PROJECTION_SEARCH_INDEX = 1;
    public static final Uri SEARCHES_URI;
    public static final String[] TRUNCATE_HISTORY_PROJECTION;
    public static final int TRUNCATE_HISTORY_PROJECTION_ID_INDEX = 0;
    public static final int TRUNCATE_N_OLDEST = 5;

    static
    {
        String[] arrayOfString1 = new String[10];
        arrayOfString1[0] = "_id";
        arrayOfString1[1] = "url";
        arrayOfString1[2] = "visits";
        arrayOfString1[3] = "date";
        arrayOfString1[4] = "bookmark";
        arrayOfString1[5] = "title";
        arrayOfString1[6] = "favicon";
        arrayOfString1[7] = "thumbnail";
        arrayOfString1[8] = "touch_icon";
        arrayOfString1[9] = "user_entered";
        HISTORY_PROJECTION = arrayOfString1;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "_id";
        arrayOfString2[1] = "date";
        TRUNCATE_HISTORY_PROJECTION = arrayOfString2;
        SEARCHES_URI = Uri.parse("content://browser/searches");
        String[] arrayOfString3 = new String[3];
        arrayOfString3[0] = "_id";
        arrayOfString3[1] = "search";
        arrayOfString3[2] = "date";
    }

    private static final void addOrUrlEquals(StringBuilder paramStringBuilder)
    {
        paramStringBuilder.append(" OR url = ");
    }

    public static final void addSearchUrl(ContentResolver paramContentResolver, String paramString)
    {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("search", paramString);
        localContentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        paramContentResolver.insert(BrowserContract.Searches.CONTENT_URI, localContentValues);
    }

    public static final boolean canClearHistory(ContentResolver paramContentResolver)
    {
        Cursor localCursor = null;
        boolean bool = false;
        try
        {
            Uri localUri = BrowserContract.History.CONTENT_URI;
            String[] arrayOfString = new String[2];
            arrayOfString[0] = "_id";
            arrayOfString[1] = "visits";
            localCursor = paramContentResolver.query(localUri, arrayOfString, null, null, null);
            int i = localCursor.getCount();
            if (i > 0);
            for (bool = true; ; bool = false)
                return bool;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
            {
                Log.e("browser", "canClearHistory", localIllegalStateException);
                if (localCursor != null)
                    localCursor.close();
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    public static final void clearHistory(ContentResolver paramContentResolver)
    {
        deleteHistoryWhere(paramContentResolver, null);
    }

    public static final void clearSearches(ContentResolver paramContentResolver)
    {
        try
        {
            paramContentResolver.delete(BrowserContract.Searches.CONTENT_URI, null, null);
            return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Log.e("browser", "clearSearches", localIllegalStateException);
        }
    }

    public static final void deleteFromHistory(ContentResolver paramContentResolver, String paramString)
    {
        Uri localUri = BrowserContract.History.CONTENT_URI;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = paramString;
        paramContentResolver.delete(localUri, "url=?", arrayOfString);
    }

    public static final void deleteHistoryTimeFrame(ContentResolver paramContentResolver, long paramLong1, long paramLong2)
    {
        String str;
        if (-1L == paramLong1)
        {
            if (-1L == paramLong2)
            {
                clearHistory(paramContentResolver);
                return;
            }
            str = "date" + " < " + Long.toString(paramLong2);
        }
        while (true)
        {
            deleteHistoryWhere(paramContentResolver, str);
            break;
            if (-1L == paramLong2)
                str = "date" + " >= " + Long.toString(paramLong1);
            else
                str = "date" + " >= " + Long.toString(paramLong1) + " AND " + "date" + " < " + Long.toString(paramLong2);
        }
    }

    private static final void deleteHistoryWhere(ContentResolver paramContentResolver, String paramString)
    {
        Cursor localCursor = null;
        try
        {
            Uri localUri = BrowserContract.History.CONTENT_URI;
            String[] arrayOfString = new String[1];
            arrayOfString[0] = "url";
            localCursor = paramContentResolver.query(localUri, arrayOfString, paramString, null, null);
            if (localCursor.moveToFirst())
            {
                WebIconDatabase localWebIconDatabase = WebIconDatabase.getInstance();
                do
                    localWebIconDatabase.releaseIconForPageUrl(localCursor.getString(0));
                while (localCursor.moveToNext());
                paramContentResolver.delete(BrowserContract.History.CONTENT_URI, paramString, null);
            }
            return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
            {
                Log.e("browser", "deleteHistoryWhere", localIllegalStateException);
                if (localCursor != null)
                    localCursor.close();
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    public static final Cursor getAllBookmarks(ContentResolver paramContentResolver)
        throws IllegalStateException
    {
        Uri localUri = BrowserContract.Bookmarks.CONTENT_URI;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "url";
        return paramContentResolver.query(localUri, arrayOfString, "folder = 0", null, null);
    }

    public static final Cursor getAllVisitedUrls(ContentResolver paramContentResolver)
        throws IllegalStateException
    {
        Uri localUri = BrowserContract.Combined.CONTENT_URI;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "url";
        return paramContentResolver.query(localUri, arrayOfString, null, null, "created ASC");
    }

    public static final String[] getVisitedHistory(ContentResolver paramContentResolver)
    {
        Cursor localCursor = null;
        try
        {
            String[] arrayOfString2 = new String[1];
            arrayOfString2[0] = "url";
            localCursor = paramContentResolver.query(BrowserContract.History.CONTENT_URI, arrayOfString2, "visits > 0", null, null);
            if (localCursor == null);
            for (Object localObject2 = new String[0]; ; localObject2 = arrayOfString1)
            {
                return localObject2;
                arrayOfString1 = new String[localCursor.getCount()];
                for (int i = 0; localCursor.moveToNext(); i++)
                    arrayOfString1[i] = localCursor.getString(0);
                if (localCursor != null)
                    localCursor.close();
            }
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
            {
                Log.e("browser", "getVisitedHistory", localIllegalStateException);
                String[] arrayOfString1 = new String[0];
                if (localCursor != null)
                    localCursor.close();
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    private static final Cursor getVisitedLike(ContentResolver paramContentResolver, String paramString)
    {
        int i = 0;
        String str1 = paramString;
        StringBuilder localStringBuilder;
        if (str1.startsWith("http://"))
        {
            str1 = str1.substring(7);
            if (str1.startsWith("www."))
                str1 = str1.substring(4);
            if (i == 0)
                break label169;
            localStringBuilder = new StringBuilder("url = ");
            DatabaseUtils.appendEscapedSQLString(localStringBuilder, "https://" + str1);
            addOrUrlEquals(localStringBuilder);
            DatabaseUtils.appendEscapedSQLString(localStringBuilder, "https://www." + str1);
        }
        while (true)
        {
            Uri localUri = BrowserContract.History.CONTENT_URI;
            String[] arrayOfString = new String[2];
            arrayOfString[0] = "_id";
            arrayOfString[1] = "visits";
            return paramContentResolver.query(localUri, arrayOfString, localStringBuilder.toString(), null, null);
            if (!str1.startsWith("https://"))
                break;
            str1 = str1.substring(8);
            i = 1;
            break;
            label169: localStringBuilder = new StringBuilder("url = ");
            DatabaseUtils.appendEscapedSQLString(localStringBuilder, str1);
            addOrUrlEquals(localStringBuilder);
            String str2 = "www." + str1;
            DatabaseUtils.appendEscapedSQLString(localStringBuilder, str2);
            addOrUrlEquals(localStringBuilder);
            DatabaseUtils.appendEscapedSQLString(localStringBuilder, "http://" + str1);
            addOrUrlEquals(localStringBuilder);
            DatabaseUtils.appendEscapedSQLString(localStringBuilder, "http://" + str2);
        }
    }

    public static final void requestAllIcons(ContentResolver paramContentResolver, String paramString, WebIconDatabase.IconListener paramIconListener)
    {
        WebIconDatabase.getInstance().bulkRequestIconForPageUrl(paramContentResolver, paramString, paramIconListener);
    }

    public static final void saveBookmark(Context paramContext, String paramString1, String paramString2)
    {
        Intent localIntent = new Intent("android.intent.action.INSERT", BOOKMARKS_URI);
        localIntent.putExtra("title", paramString1);
        localIntent.putExtra("url", paramString2);
        paramContext.startActivity(localIntent);
    }

    public static final void sendString(Context paramContext, String paramString)
    {
        sendString(paramContext, paramString, paramContext.getString(17040367));
    }

    public static final void sendString(Context paramContext, String paramString1, String paramString2)
    {
        Intent localIntent1 = new Intent("android.intent.action.SEND");
        localIntent1.setType("text/plain");
        localIntent1.putExtra("android.intent.extra.TEXT", paramString1);
        try
        {
            Intent localIntent2 = Intent.createChooser(localIntent1, paramString2);
            localIntent2.setFlags(268435456);
            paramContext.startActivity(localIntent2);
            label50: return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            break label50;
        }
    }

    public static final void truncateHistory(ContentResolver paramContentResolver)
    {
        Cursor localCursor = null;
        try
        {
            Uri localUri = BrowserContract.History.CONTENT_URI;
            String[] arrayOfString = new String[3];
            arrayOfString[0] = "_id";
            arrayOfString[1] = "url";
            arrayOfString[2] = "date";
            localCursor = paramContentResolver.query(localUri, arrayOfString, null, null, "date ASC");
            WebIconDatabase localWebIconDatabase;
            if ((localCursor.moveToFirst()) && (localCursor.getCount() >= 250))
                localWebIconDatabase = WebIconDatabase.getInstance();
            for (int i = 0; ; i++)
                if (i < 5)
                {
                    paramContentResolver.delete(ContentUris.withAppendedId(BrowserContract.History.CONTENT_URI, localCursor.getLong(0)), null, null);
                    localWebIconDatabase.releaseIconForPageUrl(localCursor.getString(1));
                    boolean bool = localCursor.moveToNext();
                    if (bool);
                }
                else
                {
                    return;
                }
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
            {
                Log.e("browser", "truncateHistory", localIllegalStateException);
                if (localCursor != null)
                    localCursor.close();
            }
        }
        finally
        {
            if (localCursor != null)
                localCursor.close();
        }
    }

    public static final void updateVisitedHistory(ContentResolver paramContentResolver, String paramString, boolean paramBoolean)
    {
        long l = System.currentTimeMillis();
        Cursor localCursor = null;
        while (true)
        {
            try
            {
                localCursor = getVisitedLike(paramContentResolver, paramString);
                if (localCursor.moveToFirst())
                {
                    ContentValues localContentValues1 = new ContentValues();
                    if (paramBoolean)
                    {
                        localContentValues1.put("visits", Integer.valueOf(1 + localCursor.getInt(1)));
                        localContentValues1.put("date", Long.valueOf(l));
                        paramContentResolver.update(ContentUris.withAppendedId(BrowserContract.History.CONTENT_URI, localCursor.getLong(0)), localContentValues1, null, null);
                        return;
                    }
                    localContentValues1.put("user_entered", Integer.valueOf(1));
                    continue;
                }
            }
            catch (IllegalStateException localIllegalStateException)
            {
                Log.e("browser", "updateVisitedHistory", localIllegalStateException);
                if (localCursor == null)
                    continue;
                localCursor.close();
                continue;
                truncateHistory(paramContentResolver);
                ContentValues localContentValues2 = new ContentValues();
                if (paramBoolean)
                {
                    i = 1;
                    j = 0;
                    localContentValues2.put("url", paramString);
                    localContentValues2.put("visits", Integer.valueOf(i));
                    localContentValues2.put("date", Long.valueOf(l));
                    localContentValues2.put("title", paramString);
                    localContentValues2.put("created", Integer.valueOf(0));
                    localContentValues2.put("user_entered", Integer.valueOf(j));
                    paramContentResolver.insert(BrowserContract.History.CONTENT_URI, localContentValues2);
                    continue;
                }
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            int i = 0;
            int j = 1;
        }
    }

    public static class SearchColumns
        implements BaseColumns
    {
        public static final String DATE = "date";
        public static final String SEARCH = "search";

        @Deprecated
        public static final String URL = "url";
    }

    public static class BookmarkColumns
        implements BaseColumns
    {
        public static final String BOOKMARK = "bookmark";
        public static final String CREATED = "created";
        public static final String DATE = "date";
        public static final String FAVICON = "favicon";
        public static final String THUMBNAIL = "thumbnail";
        public static final String TITLE = "title";
        public static final String TOUCH_ICON = "touch_icon";
        public static final String URL = "url";
        public static final String USER_ENTERED = "user_entered";
        public static final String VISITS = "visits";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.Browser
 * JD-Core Version:        0.6.2
 */