package android.provider;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import java.util.List;

public class Applications
{
    public static final String APPLICATION_DIR_TYPE = "vnd.android.cursor.dir/vnd.android.application";
    public static final String APPLICATION_ITEM_TYPE = "vnd.android.cursor.item/vnd.android.application";
    public static final String APPLICATION_PATH = "applications";
    private static final String APPLICATION_SUB_TYPE = "vnd.android.application";
    public static final String AUTHORITY = "applications";
    public static final Uri CONTENT_URI = Uri.parse("content://applications");
    public static final String SEARCH_PATH = "search";

    public static Uri componentNameToUri(String paramString1, String paramString2)
    {
        return CONTENT_URI.buildUpon().appendEncodedPath("applications").appendPath(paramString1).appendPath(paramString2).build();
    }

    public static Cursor search(ContentResolver paramContentResolver, String paramString)
    {
        return paramContentResolver.query(CONTENT_URI.buildUpon().appendPath("search").appendPath(paramString).build(), null, null, null, null);
    }

    public static ComponentName uriToComponentName(Uri paramUri)
    {
        ComponentName localComponentName = null;
        if (paramUri == null);
        while (true)
        {
            return localComponentName;
            if (("content".equals(paramUri.getScheme())) && ("applications".equals(paramUri.getAuthority())))
            {
                List localList = paramUri.getPathSegments();
                if ((localList.size() == 3) && ("applications".equals(localList.get(0))))
                    localComponentName = new ComponentName((String)localList.get(1), (String)localList.get(2));
            }
        }
    }

    public static abstract interface ApplicationColumns extends BaseColumns
    {
        public static final String ICON = "icon";
        public static final String NAME = "name";
        public static final String URI = "uri";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.Applications
 * JD-Core Version:        0.6.2
 */