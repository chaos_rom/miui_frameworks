package android.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.MiniThumbFile;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class MediaStore
{
    public static final String ACTION_IMAGE_CAPTURE = "android.media.action.IMAGE_CAPTURE";
    public static final String ACTION_MTP_SESSION_END = "android.provider.action.MTP_SESSION_END";
    public static final String ACTION_VIDEO_CAPTURE = "android.media.action.VIDEO_CAPTURE";
    public static final String AUTHORITY = "media";
    private static final String CONTENT_AUTHORITY_SLASH = "content://media/";
    public static final String EXTRA_DURATION_LIMIT = "android.intent.extra.durationLimit";
    public static final String EXTRA_FINISH_ON_COMPLETION = "android.intent.extra.finishOnCompletion";
    public static final String EXTRA_FULL_SCREEN = "android.intent.extra.fullScreen";
    public static final String EXTRA_MEDIA_ALBUM = "android.intent.extra.album";
    public static final String EXTRA_MEDIA_ARTIST = "android.intent.extra.artist";
    public static final String EXTRA_MEDIA_FOCUS = "android.intent.extra.focus";
    public static final String EXTRA_MEDIA_TITLE = "android.intent.extra.title";
    public static final String EXTRA_OUTPUT = "output";
    public static final String EXTRA_SCREEN_ORIENTATION = "android.intent.extra.screenOrientation";
    public static final String EXTRA_SHOW_ACTION_ICONS = "android.intent.extra.showActionIcons";
    public static final String EXTRA_SIZE_LIMIT = "android.intent.extra.sizeLimit";
    public static final String EXTRA_VIDEO_QUALITY = "android.intent.extra.videoQuality";
    public static final String INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH = "android.media.action.MEDIA_PLAY_FROM_SEARCH";
    public static final String INTENT_ACTION_MEDIA_SEARCH = "android.intent.action.MEDIA_SEARCH";

    @Deprecated
    public static final String INTENT_ACTION_MUSIC_PLAYER = "android.intent.action.MUSIC_PLAYER";
    public static final String INTENT_ACTION_STILL_IMAGE_CAMERA = "android.media.action.STILL_IMAGE_CAMERA";
    public static final String INTENT_ACTION_VIDEO_CAMERA = "android.media.action.VIDEO_CAMERA";
    public static final String MEDIA_IGNORE_FILENAME = ".nomedia";
    public static final String MEDIA_SCANNER_VOLUME = "volume";
    public static final String PARAM_DELETE_DATA = "deletedata";
    private static final String TAG = "MediaStore";
    public static final String UNHIDE_CALL = "unhide";
    public static final String UNKNOWN_STRING = "<unknown>";

    public static Uri getMediaScannerUri()
    {
        return Uri.parse("content://media/none/media_scanner");
    }

    public static String getVersion(Context paramContext)
    {
        Object localObject1 = null;
        Cursor localCursor = paramContext.getContentResolver().query(Uri.parse("content://media/none/version"), null, null, null, null);
        if (localCursor != null);
        try
        {
            if (localCursor.moveToFirst())
            {
                String str = localCursor.getString(0);
                localObject1 = str;
                return localObject1;
            }
            localCursor.close();
        }
        finally
        {
            localCursor.close();
        }
    }

    public static final class Video
    {
        public static final String DEFAULT_SORT_ORDER = "_display_name";

        public static final Cursor query(ContentResolver paramContentResolver, Uri paramUri, String[] paramArrayOfString)
        {
            return paramContentResolver.query(paramUri, paramArrayOfString, null, null, "_display_name");
        }

        public static class Thumbnails
            implements BaseColumns
        {
            public static final String DATA = "_data";
            public static final String DEFAULT_SORT_ORDER = "video_id ASC";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final int FULL_SCREEN_KIND = 2;
            public static final String HEIGHT = "height";
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");
            public static final String KIND = "kind";
            public static final int MICRO_KIND = 3;
            public static final int MINI_KIND = 1;
            public static final String VIDEO_ID = "video_id";
            public static final String WIDTH = "width";

            public static void cancelThumbnailRequest(ContentResolver paramContentResolver, long paramLong)
            {
                MediaStore.InternalThumbnails.cancelThumbnailRequest(paramContentResolver, paramLong, EXTERNAL_CONTENT_URI, 0L);
            }

            public static void cancelThumbnailRequest(ContentResolver paramContentResolver, long paramLong1, long paramLong2)
            {
                MediaStore.InternalThumbnails.cancelThumbnailRequest(paramContentResolver, paramLong1, EXTERNAL_CONTENT_URI, paramLong2);
            }

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/video/thumbnails");
            }

            public static Bitmap getThumbnail(ContentResolver paramContentResolver, long paramLong, int paramInt, BitmapFactory.Options paramOptions)
            {
                return MediaStore.InternalThumbnails.getThumbnail(paramContentResolver, paramLong, 0L, paramInt, paramOptions, EXTERNAL_CONTENT_URI, true);
            }

            public static Bitmap getThumbnail(ContentResolver paramContentResolver, long paramLong1, long paramLong2, int paramInt, BitmapFactory.Options paramOptions)
            {
                return MediaStore.InternalThumbnails.getThumbnail(paramContentResolver, paramLong1, paramLong2, paramInt, paramOptions, EXTERNAL_CONTENT_URI, true);
            }
        }

        public static final class Media
            implements MediaStore.Video.VideoColumns
        {
            public static final String CONTENT_TYPE = "vnd.android.cursor.dir/video";
            public static final String DEFAULT_SORT_ORDER = "title";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/video/media");
            }
        }

        public static abstract interface VideoColumns extends MediaStore.MediaColumns
        {
            public static final String ALBUM = "album";
            public static final String ARTIST = "artist";
            public static final String BOOKMARK = "bookmark";
            public static final String BUCKET_DISPLAY_NAME = "bucket_display_name";
            public static final String BUCKET_ID = "bucket_id";
            public static final String CATEGORY = "category";
            public static final String DATE_TAKEN = "datetaken";
            public static final String DESCRIPTION = "description";
            public static final String DURATION = "duration";
            public static final String IS_PRIVATE = "isprivate";
            public static final String LANGUAGE = "language";
            public static final String LATITUDE = "latitude";
            public static final String LONGITUDE = "longitude";
            public static final String MINI_THUMB_MAGIC = "mini_thumb_magic";
            public static final String RESOLUTION = "resolution";
            public static final String TAGS = "tags";
        }
    }

    public static final class Audio
    {
        public static String keyFor(String paramString)
        {
            int i;
            String str1;
            if (paramString != null)
            {
                i = 0;
                if (paramString.equals("<unknown>"))
                    str1 = "\001";
            }
            while (true)
            {
                return str1;
                if (paramString.startsWith("\001"))
                    i = 1;
                String str2 = paramString.trim().toLowerCase();
                if (str2.startsWith("the "))
                    str2 = str2.substring(4);
                if (str2.startsWith("an "))
                    str2 = str2.substring(3);
                if (str2.startsWith("a "))
                    str2 = str2.substring(2);
                if ((str2.endsWith(", the")) || (str2.endsWith(",the")) || (str2.endsWith(", an")) || (str2.endsWith(",an")) || (str2.endsWith(", a")) || (str2.endsWith(",a")))
                    str2 = str2.substring(0, str2.lastIndexOf(','));
                String str3 = str2.replaceAll("[\\[\\]\\(\\)\"'.,?!]", "").trim();
                if (str3.length() > 0)
                {
                    StringBuilder localStringBuilder = new StringBuilder();
                    localStringBuilder.append('.');
                    int j = str3.length();
                    for (int k = 0; k < j; k++)
                    {
                        localStringBuilder.append(str3.charAt(k));
                        localStringBuilder.append('.');
                    }
                    str1 = DatabaseUtils.getCollationKey(localStringBuilder.toString());
                    if (i != 0)
                        str1 = "\001" + str1;
                }
                else
                {
                    str1 = "";
                    continue;
                    str1 = null;
                }
            }
        }

        public static final class Albums
            implements BaseColumns, MediaStore.Audio.AlbumColumns
        {
            public static final String CONTENT_TYPE = "vnd.android.cursor.dir/albums";
            public static final String DEFAULT_SORT_ORDER = "album_key";
            public static final String ENTRY_CONTENT_TYPE = "vnd.android.cursor.item/album";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/audio/albums");
            }
        }

        public static abstract interface AlbumColumns
        {
            public static final String ALBUM = "album";
            public static final String ALBUM_ART = "album_art";
            public static final String ALBUM_ID = "album_id";
            public static final String ALBUM_KEY = "album_key";
            public static final String ARTIST = "artist";
            public static final String FIRST_YEAR = "minyear";
            public static final String LAST_YEAR = "maxyear";
            public static final String NUMBER_OF_SONGS = "numsongs";
            public static final String NUMBER_OF_SONGS_FOR_ARTIST = "numsongs_by_artist";
        }

        public static final class Artists
            implements BaseColumns, MediaStore.Audio.ArtistColumns
        {
            public static final String CONTENT_TYPE = "vnd.android.cursor.dir/artists";
            public static final String DEFAULT_SORT_ORDER = "artist_key";
            public static final String ENTRY_CONTENT_TYPE = "vnd.android.cursor.item/artist";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/audio/artists");
            }

            public static final class Albums
                implements MediaStore.Audio.AlbumColumns
            {
                public static final Uri getContentUri(String paramString, long paramLong)
                {
                    return Uri.parse("content://media/" + paramString + "/audio/artists/" + paramLong + "/albums");
                }
            }
        }

        public static abstract interface ArtistColumns
        {
            public static final String ARTIST = "artist";
            public static final String ARTIST_KEY = "artist_key";
            public static final String NUMBER_OF_ALBUMS = "number_of_albums";
            public static final String NUMBER_OF_TRACKS = "number_of_tracks";
        }

        public static final class Playlists
            implements BaseColumns, MediaStore.Audio.PlaylistsColumns
        {
            public static final String CONTENT_TYPE = "vnd.android.cursor.dir/playlist";
            public static final String DEFAULT_SORT_ORDER = "name";
            public static final String ENTRY_CONTENT_TYPE = "vnd.android.cursor.item/playlist";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/audio/playlists");
            }

            public static final class Members
                implements MediaStore.Audio.AudioColumns
            {
                public static final String AUDIO_ID = "audio_id";
                public static final String CONTENT_DIRECTORY = "members";
                public static final String DEFAULT_SORT_ORDER = "play_order";
                public static final String PLAYLIST_ID = "playlist_id";
                public static final String PLAY_ORDER = "play_order";
                public static final String _ID = "_id";

                public static final Uri getContentUri(String paramString, long paramLong)
                {
                    return Uri.parse("content://media/" + paramString + "/audio/playlists/" + paramLong + "/members");
                }

                public static final boolean moveItem(ContentResolver paramContentResolver, long paramLong, int paramInt1, int paramInt2)
                {
                    Uri localUri = getContentUri("external", paramLong).buildUpon().appendEncodedPath(String.valueOf(paramInt1)).appendQueryParameter("move", "true").build();
                    ContentValues localContentValues = new ContentValues();
                    localContentValues.put("play_order", Integer.valueOf(paramInt2));
                    if (paramContentResolver.update(localUri, localContentValues, null, null) != 0);
                    for (boolean bool = true; ; bool = false)
                        return bool;
                }
            }
        }

        public static abstract interface PlaylistsColumns
        {
            public static final String DATA = "_data";
            public static final String DATE_ADDED = "date_added";
            public static final String DATE_MODIFIED = "date_modified";
            public static final String NAME = "name";
        }

        public static final class Genres
            implements BaseColumns, MediaStore.Audio.GenresColumns
        {
            public static final String CONTENT_TYPE = "vnd.android.cursor.dir/genre";
            public static final String DEFAULT_SORT_ORDER = "name";
            public static final String ENTRY_CONTENT_TYPE = "vnd.android.cursor.item/genre";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/audio/genres");
            }

            public static Uri getContentUriForAudioId(String paramString, int paramInt)
            {
                return Uri.parse("content://media/" + paramString + "/audio/media/" + paramInt + "/genres");
            }

            public static final class Members
                implements MediaStore.Audio.AudioColumns
            {
                public static final String AUDIO_ID = "audio_id";
                public static final String CONTENT_DIRECTORY = "members";
                public static final String DEFAULT_SORT_ORDER = "title_key";
                public static final String GENRE_ID = "genre_id";

                public static final Uri getContentUri(String paramString, long paramLong)
                {
                    return Uri.parse("content://media/" + paramString + "/audio/genres/" + paramLong + "/members");
                }
            }
        }

        public static abstract interface GenresColumns
        {
            public static final String NAME = "name";
        }

        public static final class Media
            implements MediaStore.Audio.AudioColumns
        {
            public static final String CONTENT_TYPE = "vnd.android.cursor.dir/audio";
            public static final String DEFAULT_SORT_ORDER = "title_key";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final String EXTRA_MAX_BYTES = "android.provider.MediaStore.extra.MAX_BYTES";
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");
            public static final String RECORD_SOUND_ACTION = "android.provider.MediaStore.RECORD_SOUND";

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/audio/media");
            }

            public static Uri getContentUriForPath(String paramString)
            {
                if (paramString.startsWith(Environment.getExternalStorageDirectory().getPath()));
                for (Uri localUri = EXTERNAL_CONTENT_URI; ; localUri = INTERNAL_CONTENT_URI)
                    return localUri;
            }
        }

        public static abstract interface AudioColumns extends MediaStore.MediaColumns
        {
            public static final String ALBUM = "album";
            public static final String ALBUM_ARTIST = "album_artist";
            public static final String ALBUM_ID = "album_id";
            public static final String ALBUM_KEY = "album_key";
            public static final String ARTIST = "artist";
            public static final String ARTIST_ID = "artist_id";
            public static final String ARTIST_KEY = "artist_key";
            public static final String BOOKMARK = "bookmark";
            public static final String COMPILATION = "compilation";
            public static final String COMPOSER = "composer";
            public static final String DURATION = "duration";
            public static final String GENRE = "genre";
            public static final String IS_ALARM = "is_alarm";
            public static final String IS_MUSIC = "is_music";
            public static final String IS_NOTIFICATION = "is_notification";
            public static final String IS_PODCAST = "is_podcast";
            public static final String IS_RINGTONE = "is_ringtone";
            public static final String TITLE_KEY = "title_key";
            public static final String TRACK = "track";
            public static final String YEAR = "year";
        }
    }

    public static final class Images
    {
        public static class Thumbnails
            implements BaseColumns
        {
            public static final String DATA = "_data";
            public static final String DEFAULT_SORT_ORDER = "image_id ASC";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final int FULL_SCREEN_KIND = 2;
            public static final String HEIGHT = "height";
            public static final String IMAGE_ID = "image_id";
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");
            public static final String KIND = "kind";
            public static final int MICRO_KIND = 3;
            public static final int MINI_KIND = 1;
            public static final String THUMB_DATA = "thumb_data";
            public static final String WIDTH = "width";

            public static void cancelThumbnailRequest(ContentResolver paramContentResolver, long paramLong)
            {
                MediaStore.InternalThumbnails.cancelThumbnailRequest(paramContentResolver, paramLong, EXTERNAL_CONTENT_URI, 0L);
            }

            public static void cancelThumbnailRequest(ContentResolver paramContentResolver, long paramLong1, long paramLong2)
            {
                MediaStore.InternalThumbnails.cancelThumbnailRequest(paramContentResolver, paramLong1, EXTERNAL_CONTENT_URI, paramLong2);
            }

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/images/thumbnails");
            }

            public static Bitmap getThumbnail(ContentResolver paramContentResolver, long paramLong, int paramInt, BitmapFactory.Options paramOptions)
            {
                return MediaStore.InternalThumbnails.getThumbnail(paramContentResolver, paramLong, 0L, paramInt, paramOptions, EXTERNAL_CONTENT_URI, false);
            }

            public static Bitmap getThumbnail(ContentResolver paramContentResolver, long paramLong1, long paramLong2, int paramInt, BitmapFactory.Options paramOptions)
            {
                return MediaStore.InternalThumbnails.getThumbnail(paramContentResolver, paramLong1, paramLong2, paramInt, paramOptions, EXTERNAL_CONTENT_URI, false);
            }

            public static final Cursor query(ContentResolver paramContentResolver, Uri paramUri, String[] paramArrayOfString)
            {
                return paramContentResolver.query(paramUri, paramArrayOfString, null, null, "image_id ASC");
            }

            public static final Cursor queryMiniThumbnail(ContentResolver paramContentResolver, long paramLong, int paramInt, String[] paramArrayOfString)
            {
                return paramContentResolver.query(EXTERNAL_CONTENT_URI, paramArrayOfString, "image_id = " + paramLong + " AND " + "kind" + " = " + paramInt, null, null);
            }

            public static final Cursor queryMiniThumbnails(ContentResolver paramContentResolver, Uri paramUri, int paramInt, String[] paramArrayOfString)
            {
                return paramContentResolver.query(paramUri, paramArrayOfString, "kind = " + paramInt, null, "image_id ASC");
            }
        }

        public static final class Media
            implements MediaStore.Images.ImageColumns
        {
            public static final String CONTENT_TYPE = "vnd.android.cursor.dir/image";
            public static final String DEFAULT_SORT_ORDER = "bucket_display_name";
            public static final Uri EXTERNAL_CONTENT_URI = getContentUri("external");
            public static final Uri INTERNAL_CONTENT_URI = getContentUri("internal");

            private static final Bitmap StoreThumbnail(ContentResolver paramContentResolver, Bitmap paramBitmap, long paramLong, float paramFloat1, float paramFloat2, int paramInt)
            {
                Matrix localMatrix = new Matrix();
                localMatrix.setScale(paramFloat1 / paramBitmap.getWidth(), paramFloat2 / paramBitmap.getHeight());
                Bitmap localBitmap = Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), localMatrix, true);
                ContentValues localContentValues = new ContentValues(4);
                localContentValues.put("kind", Integer.valueOf(paramInt));
                localContentValues.put("image_id", Integer.valueOf((int)paramLong));
                localContentValues.put("height", Integer.valueOf(localBitmap.getHeight()));
                localContentValues.put("width", Integer.valueOf(localBitmap.getWidth()));
                Uri localUri = paramContentResolver.insert(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, localContentValues);
                try
                {
                    OutputStream localOutputStream = paramContentResolver.openOutputStream(localUri);
                    localBitmap.compress(Bitmap.CompressFormat.JPEG, 100, localOutputStream);
                    localOutputStream.close();
                    return localBitmap;
                }
                catch (FileNotFoundException localFileNotFoundException)
                {
                    while (true)
                        localBitmap = null;
                }
                catch (IOException localIOException)
                {
                    while (true)
                        localBitmap = null;
                }
            }

            public static final Bitmap getBitmap(ContentResolver paramContentResolver, Uri paramUri)
                throws FileNotFoundException, IOException
            {
                InputStream localInputStream = paramContentResolver.openInputStream(paramUri);
                Bitmap localBitmap = BitmapFactory.decodeStream(localInputStream);
                localInputStream.close();
                return localBitmap;
            }

            public static Uri getContentUri(String paramString)
            {
                return Uri.parse("content://media/" + paramString + "/images/media");
            }

            public static final String insertImage(ContentResolver paramContentResolver, Bitmap paramBitmap, String paramString1, String paramString2)
            {
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("title", paramString1);
                localContentValues.put("description", paramString2);
                localContentValues.put("mime_type", "image/jpeg");
                Uri localUri = null;
                String str = null;
                try
                {
                    localUri = paramContentResolver.insert(EXTERNAL_CONTENT_URI, localContentValues);
                    if (paramBitmap != null)
                    {
                        OutputStream localOutputStream = paramContentResolver.openOutputStream(localUri);
                        try
                        {
                            paramBitmap.compress(Bitmap.CompressFormat.JPEG, 50, localOutputStream);
                            localOutputStream.close();
                            long l = ContentUris.parseId(localUri);
                            StoreThumbnail(paramContentResolver, MediaStore.Images.Thumbnails.getThumbnail(paramContentResolver, l, 1, null), l, 50.0F, 50.0F, 3);
                            if (localUri != null)
                                str = localUri.toString();
                            return str;
                        }
                        finally
                        {
                            localOutputStream.close();
                        }
                    }
                }
                catch (Exception localException)
                {
                    while (true)
                    {
                        Log.e("MediaStore", "Failed to insert image", localException);
                        if (localUri != null)
                        {
                            paramContentResolver.delete(localUri, null, null);
                            localUri = null;
                            continue;
                            Log.e("MediaStore", "Failed to create thumbnail, removing original");
                            paramContentResolver.delete(localUri, null, null);
                            localUri = null;
                        }
                    }
                }
            }

            // ERROR //
            public static final String insertImage(ContentResolver paramContentResolver, String paramString1, String paramString2, String paramString3)
                throws FileNotFoundException
            {
                // Byte code:
                //     0: new 200	java/io/FileInputStream
                //     3: dup
                //     4: aload_1
                //     5: invokespecial 203	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
                //     8: astore 4
                //     10: aload_1
                //     11: invokestatic 207	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;)Landroid/graphics/Bitmap;
                //     14: astore 7
                //     16: aload_0
                //     17: aload 7
                //     19: aload_2
                //     20: aload_3
                //     21: invokestatic 209	android/provider/MediaStore$Images$Media:insertImage	(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
                //     24: astore 8
                //     26: aload 7
                //     28: invokevirtual 212	android/graphics/Bitmap:recycle	()V
                //     31: aload 4
                //     33: invokevirtual 213	java/io/FileInputStream:close	()V
                //     36: aload 8
                //     38: areturn
                //     39: astore 5
                //     41: aload 4
                //     43: invokevirtual 213	java/io/FileInputStream:close	()V
                //     46: aload 5
                //     48: athrow
                //     49: astore 9
                //     51: goto -15 -> 36
                //     54: astore 6
                //     56: goto -10 -> 46
                //
                // Exception table:
                //     from	to	target	type
                //     10	31	39	finally
                //     31	36	49	java/io/IOException
                //     41	46	54	java/io/IOException
            }

            public static final Cursor query(ContentResolver paramContentResolver, Uri paramUri, String[] paramArrayOfString)
            {
                return paramContentResolver.query(paramUri, paramArrayOfString, null, null, "bucket_display_name");
            }

            public static final Cursor query(ContentResolver paramContentResolver, Uri paramUri, String[] paramArrayOfString, String paramString1, String paramString2)
            {
                if (paramString2 == null);
                for (String str = "bucket_display_name"; ; str = paramString2)
                    return paramContentResolver.query(paramUri, paramArrayOfString, paramString1, null, str);
            }

            public static final Cursor query(ContentResolver paramContentResolver, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
            {
                if (paramString2 == null);
                for (String str = "bucket_display_name"; ; str = paramString2)
                    return paramContentResolver.query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, str);
            }
        }

        public static abstract interface ImageColumns extends MediaStore.MediaColumns
        {
            public static final String BUCKET_DISPLAY_NAME = "bucket_display_name";
            public static final String BUCKET_ID = "bucket_id";
            public static final String DATE_TAKEN = "datetaken";
            public static final String DESCRIPTION = "description";
            public static final String IS_PRIVATE = "isprivate";
            public static final String LATITUDE = "latitude";
            public static final String LONGITUDE = "longitude";
            public static final String MINI_THUMB_MAGIC = "mini_thumb_magic";
            public static final String ORIENTATION = "orientation";
            public static final String PICASA_ID = "picasa_id";
        }
    }

    private static class InternalThumbnails
        implements BaseColumns
    {
        static final int DEFAULT_GROUP_ID = 0;
        private static final int FULL_SCREEN_KIND = 2;
        private static final int MICRO_KIND = 3;
        private static final int MINI_KIND = 1;
        private static final String[] PROJECTION = arrayOfString;
        private static byte[] sThumbBuf;
        private static final Object sThumbBufLock = new Object();

        static
        {
            String[] arrayOfString = new String[2];
            arrayOfString[0] = "_id";
            arrayOfString[1] = "_data";
        }

        static void cancelThumbnailRequest(ContentResolver paramContentResolver, long paramLong1, Uri paramUri, long paramLong2)
        {
            Uri localUri = paramUri.buildUpon().appendQueryParameter("cancel", "1").appendQueryParameter("orig_id", String.valueOf(paramLong1)).appendQueryParameter("group_id", String.valueOf(paramLong2)).build();
            try
            {
                Cursor localCursor = paramContentResolver.query(localUri, PROJECTION, null, null, null);
                if (localCursor != null)
                    localCursor.close();
                return;
            }
            finally
            {
                if (0 != 0)
                    null.close();
            }
        }

        private static Bitmap getMiniThumbFromFile(Cursor paramCursor, Uri paramUri, ContentResolver paramContentResolver, BitmapFactory.Options paramOptions)
        {
            Bitmap localBitmap = null;
            Uri localUri = null;
            try
            {
                long l = paramCursor.getLong(0);
                paramCursor.getString(1);
                localUri = ContentUris.withAppendedId(paramUri, l);
                ParcelFileDescriptor localParcelFileDescriptor = paramContentResolver.openFileDescriptor(localUri, "r");
                localBitmap = BitmapFactory.decodeFileDescriptor(localParcelFileDescriptor.getFileDescriptor(), null, paramOptions);
                localParcelFileDescriptor.close();
                return localBitmap;
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                while (true)
                    Log.e("MediaStore", "couldn't open thumbnail " + localUri + "; " + localFileNotFoundException);
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.e("MediaStore", "couldn't open thumbnail " + localUri + "; " + localIOException);
            }
            catch (OutOfMemoryError localOutOfMemoryError)
            {
                while (true)
                    Log.e("MediaStore", "failed to allocate memory for thumbnail " + localUri + "; " + localOutOfMemoryError);
            }
        }

        static Bitmap getThumbnail(ContentResolver paramContentResolver, long paramLong1, long paramLong2, int paramInt, BitmapFactory.Options paramOptions, Uri paramUri, boolean paramBoolean)
        {
            Object localObject1 = null;
            String str1 = null;
            Uri localUri1;
            MiniThumbFile localMiniThumbFile;
            Object localObject2;
            if (paramBoolean)
            {
                localUri1 = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                localMiniThumbFile = new MiniThumbFile(localUri1);
                localObject2 = null;
            }
            while (true)
            {
                try
                {
                    while (true)
                        if (localMiniThumbFile.getMagic(paramLong1) != 0L)
                            if (paramInt == 3)
                                synchronized (sThumbBufLock)
                                {
                                    if (sThumbBuf == null)
                                        sThumbBuf = new byte[10000];
                                    if (localMiniThumbFile.getMiniThumbFromFile(paramLong1, sThumbBuf) != null)
                                    {
                                        localObject1 = BitmapFactory.decodeByteArray(sThumbBuf, 0, sThumbBuf.length);
                                        if (localObject1 == null)
                                            Log.w("MediaStore", "couldn't decode byte array.");
                                    }
                                    if (0 != 0)
                                        null.close();
                                    localMiniThumbFile.deactivate();
                                    localObject4 = localObject1;
                                    return localObject4;
                                    localUri1 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                                }
                }
                catch (SQLiteException localSQLiteException)
                {
                    Log.w("MediaStore", localSQLiteException);
                    if (localObject2 != null)
                        ((Cursor)localObject2).close();
                    localMiniThumbFile.deactivate();
                    Object localObject4 = localObject1;
                    continue;
                    if (paramInt == 1)
                    {
                        if (paramBoolean)
                        {
                            str2 = "video_id=";
                            localObject2 = paramContentResolver.query(paramUri, PROJECTION, str2 + paramLong1, null, null);
                            if ((localObject2 == null) || (!((Cursor)localObject2).moveToFirst()))
                                continue;
                            Bitmap localBitmap3 = getMiniThumbFromFile((Cursor)localObject2, paramUri, paramContentResolver, paramOptions);
                            localObject1 = localBitmap3;
                            if (localObject1 == null)
                                continue;
                            if (localObject2 != null)
                                ((Cursor)localObject2).close();
                            localMiniThumbFile.deactivate();
                            localObject4 = localObject1;
                            continue;
                        }
                        String str2 = "image_id=";
                        continue;
                    }
                    Uri localUri2 = paramUri.buildUpon().appendQueryParameter("blocking", "1").appendQueryParameter("orig_id", String.valueOf(paramLong1)).appendQueryParameter("group_id", String.valueOf(paramLong2)).build();
                    if (localObject2 != null)
                        ((Cursor)localObject2).close();
                    Cursor localCursor = paramContentResolver.query(localUri2, PROJECTION, null, null, null);
                    localObject2 = localCursor;
                    if (localObject2 == null)
                    {
                        localObject4 = null;
                        if (localObject2 != null)
                            ((Cursor)localObject2).close();
                        localMiniThumbFile.deactivate();
                        continue;
                    }
                    if (paramInt == 3)
                        synchronized (sThumbBufLock)
                        {
                            if (sThumbBuf == null)
                                sThumbBuf = new byte[10000];
                            if (localMiniThumbFile.getMiniThumbFromFile(paramLong1, sThumbBuf) != null)
                            {
                                localObject1 = BitmapFactory.decodeByteArray(sThumbBuf, 0, sThumbBuf.length);
                                if (localObject1 == null)
                                    Log.w("MediaStore", "couldn't decode byte array.");
                            }
                            if (localObject1 != null)
                                break label724;
                            Log.v("MediaStore", "Create the thumbnail in memory: origId=" + paramLong1 + ", kind=" + paramInt + ", isVideo=" + paramBoolean);
                            Uri localUri3 = Uri.parse(paramUri.buildUpon().appendPath(String.valueOf(paramLong1)).toString().replaceFirst("thumbnails", "media"));
                            if (0 != 0)
                                break label706;
                            if (localObject2 != null)
                                ((Cursor)localObject2).close();
                            localObject2 = paramContentResolver.query(localUri3, PROJECTION, null, null, null);
                            if (localObject2 != null)
                            {
                                boolean bool = ((Cursor)localObject2).moveToFirst();
                                if (bool)
                                    break;
                            }
                            localObject4 = null;
                        }
                }
                finally
                {
                    if (localObject2 != null)
                        ((Cursor)localObject2).close();
                    localMiniThumbFile.deactivate();
                }
                if (paramInt == 1)
                {
                    if (((Cursor)localObject2).moveToFirst())
                        localObject1 = getMiniThumbFromFile((Cursor)localObject2, paramUri, paramContentResolver, paramOptions);
                }
                else
                    throw new IllegalArgumentException("Unsupported kind: " + paramInt);
            }
            str1 = ((Cursor)localObject2).getString(1);
            label706: Bitmap localBitmap2;
            if (paramBoolean)
                localBitmap2 = ThumbnailUtils.createVideoThumbnail(str1, paramInt);
            label724: Bitmap localBitmap1;
            for (localObject1 = localBitmap2; ; localObject1 = localBitmap1)
            {
                if (localObject2 != null)
                    ((Cursor)localObject2).close();
                localMiniThumbFile.deactivate();
                break;
                localBitmap1 = ThumbnailUtils.createImageThumbnail(str1, paramInt);
            }
        }
    }

    public static final class Files
    {
        public static Uri getContentUri(String paramString)
        {
            return Uri.parse("content://media/" + paramString + "/file");
        }

        public static final Uri getContentUri(String paramString, long paramLong)
        {
            return Uri.parse("content://media/" + paramString + "/file/" + paramLong);
        }

        public static Uri getMtpObjectsUri(String paramString)
        {
            return Uri.parse("content://media/" + paramString + "/object");
        }

        public static final Uri getMtpObjectsUri(String paramString, long paramLong)
        {
            return Uri.parse("content://media/" + paramString + "/object/" + paramLong);
        }

        public static final Uri getMtpReferencesUri(String paramString, long paramLong)
        {
            return Uri.parse("content://media/" + paramString + "/object/" + paramLong + "/references");
        }

        public static abstract interface FileColumns extends MediaStore.MediaColumns
        {
            public static final String FORMAT = "format";
            public static final String MEDIA_TYPE = "media_type";
            public static final int MEDIA_TYPE_AUDIO = 2;
            public static final int MEDIA_TYPE_IMAGE = 1;
            public static final int MEDIA_TYPE_NONE = 0;
            public static final int MEDIA_TYPE_PLAYLIST = 4;
            public static final int MEDIA_TYPE_VIDEO = 3;
            public static final String MIME_TYPE = "mime_type";
            public static final String PARENT = "parent";
            public static final String STORAGE_ID = "storage_id";
            public static final String TITLE = "title";
        }
    }

    public static abstract interface MediaColumns extends BaseColumns
    {
        public static final String DATA = "_data";
        public static final String DATE_ADDED = "date_added";
        public static final String DATE_MODIFIED = "date_modified";
        public static final String DISPLAY_NAME = "_display_name";
        public static final String HEIGHT = "height";
        public static final String IS_DRM = "is_drm";
        public static final String MEDIA_SCANNER_NEW_OBJECT_ID = "media_scanner_new_object_id";
        public static final String MIME_TYPE = "mime_type";
        public static final String SIZE = "_size";
        public static final String TITLE = "title";
        public static final String WIDTH = "width";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.MediaStore
 * JD-Core Version:        0.6.2
 */