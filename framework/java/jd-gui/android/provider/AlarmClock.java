package android.provider;

public final class AlarmClock
{
    public static final String ACTION_SET_ALARM = "android.intent.action.SET_ALARM";
    public static final String EXTRA_HOUR = "android.intent.extra.alarm.HOUR";
    public static final String EXTRA_MESSAGE = "android.intent.extra.alarm.MESSAGE";
    public static final String EXTRA_MINUTES = "android.intent.extra.alarm.MINUTES";
    public static final String EXTRA_SKIP_UI = "android.intent.extra.alarm.SKIP_UI";
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.AlarmClock
 * JD-Core Version:        0.6.2
 */