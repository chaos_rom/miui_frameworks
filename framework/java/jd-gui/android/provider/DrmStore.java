package android.provider;

import android.content.Context;
import android.net.Uri;

public final class DrmStore
{
    private static final String ACCESS_DRM_PERMISSION = "android.permission.ACCESS_DRM";
    public static final String AUTHORITY = "drm";
    private static final String TAG = "DrmStore";

    // ERROR //
    public static final android.content.Intent addDrmFile(android.content.ContentResolver paramContentResolver, java.io.File paramFile, String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aconst_null
        //     3: astore 4
        //     5: new 35	java/io/FileInputStream
        //     8: dup
        //     9: aload_1
        //     10: invokespecial 38	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     13: astore 5
        //     15: aload_2
        //     16: ifnonnull +29 -> 45
        //     19: aload_1
        //     20: invokevirtual 44	java/io/File:getName	()Ljava/lang/String;
        //     23: astore_2
        //     24: aload_2
        //     25: bipush 46
        //     27: invokevirtual 50	java/lang/String:lastIndexOf	(I)I
        //     30: istore 16
        //     32: iload 16
        //     34: ifle +11 -> 45
        //     37: aload_2
        //     38: iconst_0
        //     39: iload 16
        //     41: invokevirtual 54	java/lang/String:substring	(II)Ljava/lang/String;
        //     44: astore_2
        //     45: aload_0
        //     46: aload 5
        //     48: aload_2
        //     49: invokestatic 57	android/provider/DrmStore:addDrmFile	(Landroid/content/ContentResolver;Ljava/io/FileInputStream;Ljava/lang/String;)Landroid/content/Intent;
        //     52: astore 13
        //     54: aload 13
        //     56: astore 4
        //     58: aload 5
        //     60: ifnull +8 -> 68
        //     63: aload 5
        //     65: invokevirtual 60	java/io/FileInputStream:close	()V
        //     68: aload 4
        //     70: areturn
        //     71: astore 14
        //     73: ldc 23
        //     75: ldc 62
        //     77: aload 14
        //     79: invokestatic 68	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     82: pop
        //     83: goto -15 -> 68
        //     86: astore 6
        //     88: ldc 23
        //     90: ldc 70
        //     92: aload 6
        //     94: invokestatic 68	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     97: pop
        //     98: aload_3
        //     99: ifnull -31 -> 68
        //     102: aload_3
        //     103: invokevirtual 60	java/io/FileInputStream:close	()V
        //     106: goto -38 -> 68
        //     109: astore 11
        //     111: ldc 23
        //     113: ldc 62
        //     115: aload 11
        //     117: invokestatic 68	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     120: pop
        //     121: goto -53 -> 68
        //     124: astore 7
        //     126: aload_3
        //     127: ifnull +7 -> 134
        //     130: aload_3
        //     131: invokevirtual 60	java/io/FileInputStream:close	()V
        //     134: aload 7
        //     136: athrow
        //     137: astore 8
        //     139: ldc 23
        //     141: ldc 62
        //     143: aload 8
        //     145: invokestatic 68	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     148: pop
        //     149: goto -15 -> 134
        //     152: astore 7
        //     154: aload 5
        //     156: astore_3
        //     157: goto -31 -> 126
        //     160: astore 6
        //     162: aload 5
        //     164: astore_3
        //     165: goto -77 -> 88
        //
        // Exception table:
        //     from	to	target	type
        //     63	68	71	java/io/IOException
        //     5	15	86	java/lang/Exception
        //     102	106	109	java/io/IOException
        //     5	15	124	finally
        //     88	98	124	finally
        //     130	134	137	java/io/IOException
        //     19	54	152	finally
        //     19	54	160	java/lang/Exception
    }

    // ERROR //
    public static final android.content.Intent addDrmFile(android.content.ContentResolver paramContentResolver, java.io.FileInputStream paramFileInputStream, String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aconst_null
        //     3: astore 4
        //     5: new 72	android/drm/mobile1/DrmRawContent
        //     8: dup
        //     9: aload_1
        //     10: aload_1
        //     11: invokevirtual 76	java/io/FileInputStream:available	()I
        //     14: ldc 78
        //     16: invokespecial 81	android/drm/mobile1/DrmRawContent:<init>	(Ljava/io/InputStream;ILjava/lang/String;)V
        //     19: astore 5
        //     21: aload 5
        //     23: invokevirtual 84	android/drm/mobile1/DrmRawContent:getContentType	()Ljava/lang/String;
        //     26: astore 13
        //     28: aload_1
        //     29: invokevirtual 88	java/io/FileInputStream:getChannel	()Ljava/nio/channels/FileChannel;
        //     32: invokevirtual 94	java/nio/channels/FileChannel:size	()J
        //     35: lstore 14
        //     37: aload 5
        //     39: invokestatic 100	android/drm/mobile1/DrmRightsManager:getInstance	()Landroid/drm/mobile1/DrmRightsManager;
        //     42: aload 5
        //     44: invokevirtual 104	android/drm/mobile1/DrmRightsManager:queryRights	(Landroid/drm/mobile1/DrmRawContent;)Landroid/drm/mobile1/DrmRights;
        //     47: invokevirtual 108	android/drm/mobile1/DrmRawContent:getContentInputStream	(Landroid/drm/mobile1/DrmRights;)Ljava/io/InputStream;
        //     50: astore 16
        //     52: aconst_null
        //     53: astore 17
        //     55: aload 13
        //     57: ldc 110
        //     59: invokevirtual 114	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     62: ifeq +140 -> 202
        //     65: getstatic 118	android/provider/DrmStore$Audio:CONTENT_URI	Landroid/net/Uri;
        //     68: astore 17
        //     70: aload 17
        //     72: ifnull +221 -> 293
        //     75: new 120	android/content/ContentValues
        //     78: dup
        //     79: iconst_3
        //     80: invokespecial 123	android/content/ContentValues:<init>	(I)V
        //     83: astore 19
        //     85: aload 19
        //     87: ldc 125
        //     89: aload_2
        //     90: invokevirtual 129	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
        //     93: aload 19
        //     95: ldc 131
        //     97: lload 14
        //     99: invokestatic 137	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     102: invokevirtual 140	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
        //     105: aload 19
        //     107: ldc 142
        //     109: aload 13
        //     111: invokevirtual 129	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
        //     114: aload_0
        //     115: aload 17
        //     117: aload 19
        //     119: invokevirtual 148	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
        //     122: astore 20
        //     124: aload 20
        //     126: ifnull +167 -> 293
        //     129: aload_0
        //     130: aload 20
        //     132: invokevirtual 152	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
        //     135: astore_3
        //     136: sipush 1000
        //     139: newarray byte
        //     141: astore 23
        //     143: aload 16
        //     145: aload 23
        //     147: invokevirtual 158	java/io/InputStream:read	([B)I
        //     150: istore 24
        //     152: iload 24
        //     154: bipush 255
        //     156: if_icmpeq +114 -> 270
        //     159: aload_3
        //     160: aload 23
        //     162: iconst_0
        //     163: iload 24
        //     165: invokevirtual 164	java/io/OutputStream:write	([BII)V
        //     168: goto -25 -> 143
        //     171: astore 9
        //     173: ldc 23
        //     175: ldc 70
        //     177: aload 9
        //     179: invokestatic 68	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     182: pop
        //     183: aload_1
        //     184: ifnull +7 -> 191
        //     187: aload_1
        //     188: invokevirtual 60	java/io/FileInputStream:close	()V
        //     191: aload_3
        //     192: ifnull +7 -> 199
        //     195: aload_3
        //     196: invokevirtual 165	java/io/OutputStream:close	()V
        //     199: aload 4
        //     201: areturn
        //     202: aload 13
        //     204: ldc 167
        //     206: invokevirtual 114	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     209: ifeq +11 -> 220
        //     212: getstatic 168	android/provider/DrmStore$Images:CONTENT_URI	Landroid/net/Uri;
        //     215: astore 17
        //     217: goto -147 -> 70
        //     220: ldc 23
        //     222: new 170	java/lang/StringBuilder
        //     225: dup
        //     226: invokespecial 171	java/lang/StringBuilder:<init>	()V
        //     229: ldc 173
        //     231: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     234: aload 13
        //     236: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     239: invokevirtual 180	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     242: invokestatic 184	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     245: pop
        //     246: goto -176 -> 70
        //     249: astore 6
        //     251: aload_1
        //     252: ifnull +7 -> 259
        //     255: aload_1
        //     256: invokevirtual 60	java/io/FileInputStream:close	()V
        //     259: aload_3
        //     260: ifnull +7 -> 267
        //     263: aload_3
        //     264: invokevirtual 165	java/io/OutputStream:close	()V
        //     267: aload 6
        //     269: athrow
        //     270: new 186	android/content/Intent
        //     273: dup
        //     274: invokespecial 187	android/content/Intent:<init>	()V
        //     277: astore 25
        //     279: aload 25
        //     281: aload 20
        //     283: aload 13
        //     285: invokevirtual 191	android/content/Intent:setDataAndType	(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
        //     288: pop
        //     289: aload 25
        //     291: astore 4
        //     293: aload_1
        //     294: ifnull +7 -> 301
        //     297: aload_1
        //     298: invokevirtual 60	java/io/FileInputStream:close	()V
        //     301: aload_3
        //     302: ifnull -103 -> 199
        //     305: aload_3
        //     306: invokevirtual 165	java/io/OutputStream:close	()V
        //     309: goto -110 -> 199
        //     312: astore 21
        //     314: ldc 23
        //     316: ldc 62
        //     318: aload 21
        //     320: invokestatic 68	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     323: pop
        //     324: goto -125 -> 199
        //     327: astore 11
        //     329: ldc 23
        //     331: ldc 62
        //     333: aload 11
        //     335: invokestatic 68	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     338: pop
        //     339: goto -140 -> 199
        //     342: astore 7
        //     344: ldc 23
        //     346: ldc 62
        //     348: aload 7
        //     350: invokestatic 68	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     353: pop
        //     354: goto -87 -> 267
        //     357: astore 6
        //     359: goto -108 -> 251
        //     362: astore 9
        //     364: aload 25
        //     366: astore 4
        //     368: goto -195 -> 173
        //
        // Exception table:
        //     from	to	target	type
        //     5	168	171	java/lang/Exception
        //     202	246	171	java/lang/Exception
        //     270	279	171	java/lang/Exception
        //     5	168	249	finally
        //     173	183	249	finally
        //     202	246	249	finally
        //     270	279	249	finally
        //     297	309	312	java/io/IOException
        //     187	199	327	java/io/IOException
        //     255	267	342	java/io/IOException
        //     279	289	357	finally
        //     279	289	362	java/lang/Exception
    }

    public static void enforceAccessDrmPermission(Context paramContext)
    {
        if (paramContext.checkCallingOrSelfPermission("android.permission.ACCESS_DRM") != 0)
            throw new SecurityException("Requires DRM permission");
    }

    public static abstract interface Audio extends DrmStore.Columns
    {
        public static final Uri CONTENT_URI = Uri.parse("content://drm/audio");
    }

    public static abstract interface Images extends DrmStore.Columns
    {
        public static final Uri CONTENT_URI = Uri.parse("content://drm/images");
    }

    public static abstract interface Columns extends BaseColumns
    {
        public static final String DATA = "_data";
        public static final String MIME_TYPE = "mime_type";
        public static final String SIZE = "_size";
        public static final String TITLE = "title";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.DrmStore
 * JD-Core Version:        0.6.2
 */