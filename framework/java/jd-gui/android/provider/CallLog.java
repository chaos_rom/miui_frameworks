package android.provider;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.android.internal.telephony.CallerInfo;
import com.android.internal.telephony.Connection;

public class CallLog
{
    public static final String AUTHORITY = "call_log";
    public static final Uri CONTENT_URI = Uri.parse("content://call_log");

    public static class Calls
        implements BaseColumns
    {
        public static final String ALLOW_VOICEMAILS_PARAM_KEY = "allow_voicemails";
        public static final String CACHED_FORMATTED_NUMBER = "formatted_number";
        public static final String CACHED_LOOKUP_URI = "lookup_uri";
        public static final String CACHED_MATCHED_NUMBER = "matched_number";
        public static final String CACHED_NAME = "name";
        public static final String CACHED_NORMALIZED_NUMBER = "normalized_number";
        public static final String CACHED_NUMBER_LABEL = "numberlabel";
        public static final String CACHED_NUMBER_TYPE = "numbertype";
        public static final String CACHED_PHOTO_ID = "photo_id";
        public static final Uri CONTENT_FILTER_URI = Uri.parse("content://call_log/calls/filter");
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/calls";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/calls";
        public static final Uri CONTENT_URI;
        public static final Uri CONTENT_URI_WITH_VOICEMAIL = CONTENT_URI.buildUpon().appendQueryParameter("allow_voicemails", "true").build();
        public static final String COUNTRY_ISO = "countryiso";
        public static final String DATE = "date";
        public static final String DEFAULT_SORT_ORDER = "date DESC";
        public static final String DURATION = "duration";
        public static final String GEOCODED_LOCATION = "geocoded_location";
        public static final int INCOMING_TYPE = 1;
        public static final String IS_READ = "is_read";
        public static final int MISSED_TYPE = 3;
        public static final String NEW = "new";
        public static final String NUMBER = "number";
        public static final int OUTGOING_TYPE = 2;
        public static final String TYPE = "type";
        public static final int VOICEMAIL_TYPE = 4;
        public static final String VOICEMAIL_URI = "voicemail_uri";

        @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
        static ContentValues sExtraCallLogValues = null;

        static
        {
            CONTENT_URI = Uri.parse("content://call_log/calls");
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        public static Uri addCall(CallerInfo paramCallerInfo, Context paramContext, String paramString, int paramInt1, int paramInt2, long paramLong, int paramInt3)
        {
            ContentResolver localContentResolver = paramContext.getContentResolver();
            if (paramInt1 == Connection.PRESENTATION_RESTRICTED)
            {
                paramString = "-2";
                if (paramCallerInfo != null)
                    paramCallerInfo.name = "";
            }
            while (true)
            {
                ContentValues localContentValues = CallLog.Injector.getExtraCallLogValues(new ContentValues(5));
                localContentValues.put("number", paramString);
                localContentValues.put("type", Integer.valueOf(paramInt2));
                localContentValues.put("date", Long.valueOf(paramLong));
                localContentValues.put("duration", Long.valueOf(paramInt3));
                localContentValues.put("new", Integer.valueOf(1));
                if (paramInt2 == 3)
                    localContentValues.put("is_read", Integer.valueOf(0));
                if (paramCallerInfo != null)
                {
                    localContentValues.put("name", paramCallerInfo.name);
                    localContentValues.put("numbertype", Integer.valueOf(paramCallerInfo.numberType));
                    localContentValues.put("numberlabel", paramCallerInfo.numberLabel);
                }
                Cursor localCursor;
                if ((paramCallerInfo != null) && (paramCallerInfo.person_id > 0L))
                {
                    if (paramCallerInfo.normalizedNumber == null)
                        break label382;
                    String str2 = paramCallerInfo.normalizedNumber;
                    Uri localUri3 = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                    String[] arrayOfString3 = new String[1];
                    arrayOfString3[0] = "_id";
                    String[] arrayOfString4 = new String[2];
                    arrayOfString4[0] = String.valueOf(paramCallerInfo.person_id);
                    arrayOfString4[1] = str2;
                    localCursor = localContentResolver.query(localUri3, arrayOfString3, "contact_id =? AND data4 =?", arrayOfString4, null);
                    label234: if (localCursor == null);
                }
                try
                {
                    if ((localCursor.getCount() > 0) && (localCursor.moveToFirst()))
                        localContentResolver.update(ContactsContract.DataUsageFeedback.FEEDBACK_URI.buildUpon().appendPath(localCursor.getString(0)).appendQueryParameter("type", "call").build(), new ContentValues(), null, null);
                    localCursor.close();
                    Uri localUri1 = localContentResolver.insert(CONTENT_URI, localContentValues);
                    return localUri1;
                    if (paramInt1 == Connection.PRESENTATION_PAYPHONE)
                    {
                        paramString = "-3";
                        if (paramCallerInfo == null)
                            continue;
                        paramCallerInfo.name = "";
                        continue;
                    }
                    if ((!TextUtils.isEmpty(paramString)) && (paramInt1 != Connection.PRESENTATION_UNKNOWN))
                        continue;
                    paramString = "-1";
                    if (paramCallerInfo == null)
                        continue;
                    paramCallerInfo.name = "";
                    continue;
                    label382: if (paramCallerInfo.phoneNumber != null)
                    {
                        str1 = paramCallerInfo.phoneNumber;
                        Uri localUri2 = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Callable.CONTENT_FILTER_URI, Uri.encode(str1));
                        String[] arrayOfString1 = new String[1];
                        arrayOfString1[0] = "_id";
                        String[] arrayOfString2 = new String[1];
                        arrayOfString2[0] = String.valueOf(paramCallerInfo.person_id);
                        localCursor = localContentResolver.query(localUri2, arrayOfString1, "contact_id =?", arrayOfString2, null);
                        break label234;
                    }
                    String str1 = paramString;
                }
                finally
                {
                    localCursor.close();
                }
            }
        }

        public static String getLastOutgoingCall(Context paramContext)
        {
            ContentResolver localContentResolver = paramContext.getContentResolver();
            Cursor localCursor = null;
            try
            {
                Uri localUri = CONTENT_URI;
                String[] arrayOfString = new String[1];
                arrayOfString[0] = "number";
                localCursor = localContentResolver.query(localUri, arrayOfString, "type = 2", null, "date DESC LIMIT 1");
                Object localObject2;
                if ((localCursor == null) || (!localCursor.moveToFirst()))
                    localObject2 = "";
                while (true)
                {
                    return localObject2;
                    String str = localCursor.getString(0);
                    localObject2 = str;
                    if (localCursor != null)
                        localCursor.close();
                }
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
        }

        private static void removeExpiredEntries(Context paramContext)
        {
            paramContext.getContentResolver().delete(CONTENT_URI, "_id IN (SELECT _id FROM calls ORDER BY date DESC LIMIT -1 OFFSET 500)", null);
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        public static void setExtraCallLogValues(ContentValues paramContentValues)
        {
            sExtraCallLogValues = paramContentValues;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static ContentValues getExtraCallLogValues(ContentValues paramContentValues)
        {
            ContentValues localContentValues;
            if (CallLog.Calls.sExtraCallLogValues == null)
                localContentValues = paramContentValues;
            while (true)
            {
                return localContentValues;
                localContentValues = CallLog.Calls.sExtraCallLogValues;
                CallLog.Calls.sExtraCallLogValues = null;
            }
        }

        static void removeExpiredEntries(Context paramContext)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.CallLog
 * JD-Core Version:        0.6.2
 */