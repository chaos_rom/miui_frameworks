package android.provider;

import android.accounts.Account;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Pair;

public class SyncStateContract
{
    public static final class Helpers
    {
        private static final String[] DATA_PROJECTION = arrayOfString;
        private static final String SELECT_BY_ACCOUNT = "account_name=? AND account_type=?";

        static
        {
            String[] arrayOfString = new String[2];
            arrayOfString[0] = "data";
            arrayOfString[1] = "_id";
        }

        public static byte[] get(ContentProviderClient paramContentProviderClient, Uri paramUri, Account paramAccount)
            throws RemoteException
        {
            Object localObject1 = null;
            String[] arrayOfString1 = DATA_PROJECTION;
            String[] arrayOfString2 = new String[2];
            arrayOfString2[0] = paramAccount.name;
            arrayOfString2[1] = paramAccount.type;
            Cursor localCursor = paramContentProviderClient.query(paramUri, arrayOfString1, "account_name=? AND account_type=?", arrayOfString2, null);
            if (localCursor == null)
                throw new RemoteException();
            try
            {
                if (localCursor.moveToNext())
                {
                    byte[] arrayOfByte = localCursor.getBlob(localCursor.getColumnIndexOrThrow("data"));
                    localObject1 = arrayOfByte;
                    return localObject1;
                }
                localCursor.close();
            }
            finally
            {
                localCursor.close();
            }
        }

        public static Pair<Uri, byte[]> getWithUri(ContentProviderClient paramContentProviderClient, Uri paramUri, Account paramAccount)
            throws RemoteException
        {
            Object localObject1 = null;
            String[] arrayOfString1 = DATA_PROJECTION;
            String[] arrayOfString2 = new String[2];
            arrayOfString2[0] = paramAccount.name;
            arrayOfString2[1] = paramAccount.type;
            Cursor localCursor = paramContentProviderClient.query(paramUri, arrayOfString1, "account_name=? AND account_type=?", arrayOfString2, null);
            if (localCursor == null)
                throw new RemoteException();
            try
            {
                if (localCursor.moveToNext())
                {
                    long l = localCursor.getLong(1);
                    byte[] arrayOfByte = localCursor.getBlob(localCursor.getColumnIndexOrThrow("data"));
                    Pair localPair = Pair.create(ContentUris.withAppendedId(paramUri, l), arrayOfByte);
                    localObject1 = localPair;
                    return localObject1;
                }
                localCursor.close();
            }
            finally
            {
                localCursor.close();
            }
        }

        public static Uri insert(ContentProviderClient paramContentProviderClient, Uri paramUri, Account paramAccount, byte[] paramArrayOfByte)
            throws RemoteException
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("data", paramArrayOfByte);
            localContentValues.put("account_name", paramAccount.name);
            localContentValues.put("account_type", paramAccount.type);
            return paramContentProviderClient.insert(paramUri, localContentValues);
        }

        public static ContentProviderOperation newSetOperation(Uri paramUri, Account paramAccount, byte[] paramArrayOfByte)
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("data", paramArrayOfByte);
            return ContentProviderOperation.newInsert(paramUri).withValue("account_name", paramAccount.name).withValue("account_type", paramAccount.type).withValues(localContentValues).build();
        }

        public static ContentProviderOperation newUpdateOperation(Uri paramUri, byte[] paramArrayOfByte)
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("data", paramArrayOfByte);
            return ContentProviderOperation.newUpdate(paramUri).withValues(localContentValues).build();
        }

        public static void set(ContentProviderClient paramContentProviderClient, Uri paramUri, Account paramAccount, byte[] paramArrayOfByte)
            throws RemoteException
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("data", paramArrayOfByte);
            localContentValues.put("account_name", paramAccount.name);
            localContentValues.put("account_type", paramAccount.type);
            paramContentProviderClient.insert(paramUri, localContentValues);
        }

        public static void update(ContentProviderClient paramContentProviderClient, Uri paramUri, byte[] paramArrayOfByte)
            throws RemoteException
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("data", paramArrayOfByte);
            paramContentProviderClient.update(paramUri, localContentValues, null, null);
        }
    }

    public static class Constants
        implements SyncStateContract.Columns
    {
        public static final String CONTENT_DIRECTORY = "syncstate";
    }

    public static abstract interface Columns extends BaseColumns
    {
        public static final String ACCOUNT_NAME = "account_name";
        public static final String ACCOUNT_TYPE = "account_type";
        public static final String DATA = "data";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.SyncStateContract
 * JD-Core Version:        0.6.2
 */