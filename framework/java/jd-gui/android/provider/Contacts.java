package android.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Deprecated
public class Contacts
{

    @Deprecated
    public static final String AUTHORITY = "contacts";

    @Deprecated
    public static final Uri CONTENT_URI = Uri.parse("content://contacts");

    @Deprecated
    public static final int KIND_EMAIL = 1;

    @Deprecated
    public static final int KIND_IM = 3;

    @Deprecated
    public static final int KIND_ORGANIZATION = 4;

    @Deprecated
    public static final int KIND_PHONE = 5;

    @Deprecated
    public static final int KIND_POSTAL = 2;
    private static final String TAG = "Contacts";

    @Deprecated
    public static final class Intents
    {

        @Deprecated
        public static final String ATTACH_IMAGE = "com.android.contacts.action.ATTACH_IMAGE";

        @Deprecated
        public static final String EXTRA_CREATE_DESCRIPTION = "com.android.contacts.action.CREATE_DESCRIPTION";

        @Deprecated
        public static final String EXTRA_FORCE_CREATE = "com.android.contacts.action.FORCE_CREATE";

        @Deprecated
        public static final String EXTRA_TARGET_RECT = "target_rect";

        @Deprecated
        public static final String SEARCH_SUGGESTION_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_CLICKED";

        @Deprecated
        public static final String SEARCH_SUGGESTION_CREATE_CONTACT_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_CREATE_CONTACT_CLICKED";

        @Deprecated
        public static final String SEARCH_SUGGESTION_DIAL_NUMBER_CLICKED = "android.provider.Contacts.SEARCH_SUGGESTION_DIAL_NUMBER_CLICKED";

        @Deprecated
        public static final String SHOW_OR_CREATE_CONTACT = "com.android.contacts.action.SHOW_OR_CREATE_CONTACT";

        @Deprecated
        public static final class Insert
        {

            @Deprecated
            public static final String ACTION = "android.intent.action.INSERT";

            @Deprecated
            public static final String COMPANY = "company";

            @Deprecated
            public static final String EMAIL = "email";

            @Deprecated
            public static final String EMAIL_ISPRIMARY = "email_isprimary";

            @Deprecated
            public static final String EMAIL_TYPE = "email_type";

            @Deprecated
            public static final String FULL_MODE = "full_mode";

            @Deprecated
            public static final String IM_HANDLE = "im_handle";

            @Deprecated
            public static final String IM_ISPRIMARY = "im_isprimary";

            @Deprecated
            public static final String IM_PROTOCOL = "im_protocol";

            @Deprecated
            public static final String JOB_TITLE = "job_title";

            @Deprecated
            public static final String NAME = "name";

            @Deprecated
            public static final String NOTES = "notes";

            @Deprecated
            public static final String PHONE = "phone";

            @Deprecated
            public static final String PHONETIC_NAME = "phonetic_name";

            @Deprecated
            public static final String PHONE_ISPRIMARY = "phone_isprimary";

            @Deprecated
            public static final String PHONE_TYPE = "phone_type";

            @Deprecated
            public static final String POSTAL = "postal";

            @Deprecated
            public static final String POSTAL_ISPRIMARY = "postal_isprimary";

            @Deprecated
            public static final String POSTAL_TYPE = "postal_type";

            @Deprecated
            public static final String SECONDARY_EMAIL = "secondary_email";

            @Deprecated
            public static final String SECONDARY_EMAIL_TYPE = "secondary_email_type";

            @Deprecated
            public static final String SECONDARY_PHONE = "secondary_phone";

            @Deprecated
            public static final String SECONDARY_PHONE_TYPE = "secondary_phone_type";

            @Deprecated
            public static final String TERTIARY_EMAIL = "tertiary_email";

            @Deprecated
            public static final String TERTIARY_EMAIL_TYPE = "tertiary_email_type";

            @Deprecated
            public static final String TERTIARY_PHONE = "tertiary_phone";

            @Deprecated
            public static final String TERTIARY_PHONE_TYPE = "tertiary_phone_type";
        }

        @Deprecated
        public static final class UI
        {

            @Deprecated
            public static final String FILTER_CONTACTS_ACTION = "com.android.contacts.action.FILTER_CONTACTS";

            @Deprecated
            public static final String FILTER_TEXT_EXTRA_KEY = "com.android.contacts.extra.FILTER_TEXT";

            @Deprecated
            public static final String GROUP_NAME_EXTRA_KEY = "com.android.contacts.extra.GROUP";

            @Deprecated
            public static final String LIST_ALL_CONTACTS_ACTION = "com.android.contacts.action.LIST_ALL_CONTACTS";

            @Deprecated
            public static final String LIST_CONTACTS_WITH_PHONES_ACTION = "com.android.contacts.action.LIST_CONTACTS_WITH_PHONES";

            @Deprecated
            public static final String LIST_DEFAULT = "com.android.contacts.action.LIST_DEFAULT";

            @Deprecated
            public static final String LIST_FREQUENT_ACTION = "com.android.contacts.action.LIST_FREQUENT";

            @Deprecated
            public static final String LIST_GROUP_ACTION = "com.android.contacts.action.LIST_GROUP";

            @Deprecated
            public static final String LIST_STARRED_ACTION = "com.android.contacts.action.LIST_STARRED";

            @Deprecated
            public static final String LIST_STREQUENT_ACTION = "com.android.contacts.action.LIST_STREQUENT";

            @Deprecated
            public static final String TITLE_EXTRA_KEY = "com.android.contacts.extra.TITLE_EXTRA";
        }
    }

    @Deprecated
    public static final class Extensions
        implements BaseColumns, Contacts.ExtensionsColumns
    {

        @Deprecated
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contact_extensions";

        @Deprecated
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contact_extensions";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/extensions");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "person, name ASC";

        @Deprecated
        public static final String PERSON_ID = "person";
    }

    @Deprecated
    public static abstract interface ExtensionsColumns
    {

        @Deprecated
        public static final String NAME = "name";

        @Deprecated
        public static final String VALUE = "value";
    }

    @Deprecated
    public static final class Photos
        implements BaseColumns, Contacts.PhotosColumns, SyncConstValue
    {

        @Deprecated
        public static final String CONTENT_DIRECTORY = "photo";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/photos");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "person ASC";
    }

    @Deprecated
    public static abstract interface PhotosColumns
    {

        @Deprecated
        public static final String DATA = "data";

        @Deprecated
        public static final String DOWNLOAD_REQUIRED = "download_required";

        @Deprecated
        public static final String EXISTS_ON_SERVER = "exists_on_server";

        @Deprecated
        public static final String LOCAL_VERSION = "local_version";

        @Deprecated
        public static final String PERSON_ID = "person";

        @Deprecated
        public static final String SYNC_ERROR = "sync_error";
    }

    @Deprecated
    public static final class Organizations
        implements BaseColumns, Contacts.OrganizationColumns
    {

        @Deprecated
        public static final String CONTENT_DIRECTORY = "organizations";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/organizations");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "company, title, isprimary ASC";

        @Deprecated
        public static final CharSequence getDisplayLabel(Context paramContext, int paramInt, CharSequence paramCharSequence)
        {
            Object localObject = "";
            CharSequence[] arrayOfCharSequence;
            int i;
            if (paramInt != 0)
            {
                arrayOfCharSequence = paramContext.getResources().getTextArray(17235970);
                i = paramInt - 1;
            }
            while (true)
            {
                try
                {
                    localObject = arrayOfCharSequence[i];
                    return localObject;
                }
                catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
                {
                    localObject = arrayOfCharSequence[0];
                    continue;
                }
                if (!TextUtils.isEmpty(paramCharSequence))
                    localObject = paramCharSequence;
            }
        }
    }

    @Deprecated
    public static abstract interface OrganizationColumns
    {

        @Deprecated
        public static final String COMPANY = "company";

        @Deprecated
        public static final String ISPRIMARY = "isprimary";

        @Deprecated
        public static final String LABEL = "label";

        @Deprecated
        public static final String PERSON_ID = "person";

        @Deprecated
        public static final String TITLE = "title";

        @Deprecated
        public static final String TYPE = "type";

        @Deprecated
        public static final int TYPE_CUSTOM = 0;

        @Deprecated
        public static final int TYPE_OTHER = 2;

        @Deprecated
        public static final int TYPE_WORK = 1;
    }

    @Deprecated
    public static final class Presence
        implements BaseColumns, Contacts.PresenceColumns, Contacts.PeopleColumns
    {

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/presence");

        @Deprecated
        public static final String PERSON_ID = "person";

        @Deprecated
        public static final int getPresenceIconResourceId(int paramInt)
        {
            int i;
            switch (paramInt)
            {
            default:
                i = 17301610;
            case 5:
            case 2:
            case 3:
            case 4:
            case 1:
            }
            while (true)
            {
                return i;
                i = 17301611;
                continue;
                i = 17301607;
                continue;
                i = 17301608;
                continue;
                i = 17301609;
            }
        }

        @Deprecated
        public static final void setPresenceIcon(ImageView paramImageView, int paramInt)
        {
            paramImageView.setImageResource(getPresenceIconResourceId(paramInt));
        }
    }

    @Deprecated
    public static abstract interface PresenceColumns
    {
        public static final int AVAILABLE = 5;
        public static final int AWAY = 2;
        public static final int DO_NOT_DISTURB = 4;
        public static final int IDLE = 3;

        @Deprecated
        public static final String IM_ACCOUNT = "im_account";

        @Deprecated
        public static final String IM_HANDLE = "im_handle";

        @Deprecated
        public static final String IM_PROTOCOL = "im_protocol";
        public static final int INVISIBLE = 1;
        public static final int OFFLINE = 0;
        public static final String PRESENCE_CUSTOM_STATUS = "status";
        public static final String PRESENCE_STATUS = "mode";
        public static final String PRIORITY = "priority";
    }

    @Deprecated
    public static final class ContactMethods
        implements BaseColumns, Contacts.ContactMethodsColumns, Contacts.PeopleColumns
    {

        @Deprecated
        public static final String CONTENT_EMAIL_ITEM_TYPE = "vnd.android.cursor.item/email";

        @Deprecated
        public static final String CONTENT_EMAIL_TYPE = "vnd.android.cursor.dir/email";

        @Deprecated
        public static final Uri CONTENT_EMAIL_URI = Uri.parse("content://contacts/contact_methods/email");

        @Deprecated
        public static final String CONTENT_IM_ITEM_TYPE = "vnd.android.cursor.item/jabber-im";

        @Deprecated
        public static final String CONTENT_POSTAL_ITEM_TYPE = "vnd.android.cursor.item/postal-address";

        @Deprecated
        public static final String CONTENT_POSTAL_TYPE = "vnd.android.cursor.dir/postal-address";

        @Deprecated
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contact-methods";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/contact_methods");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "name ASC";

        @Deprecated
        public static final String PERSON_ID = "person";

        @Deprecated
        public static final String POSTAL_LOCATION_LATITUDE = "data";

        @Deprecated
        public static final String POSTAL_LOCATION_LONGITUDE = "aux_data";

        @Deprecated
        public static final int PROTOCOL_AIM = 0;

        @Deprecated
        public static final int PROTOCOL_GOOGLE_TALK = 5;

        @Deprecated
        public static final int PROTOCOL_ICQ = 6;

        @Deprecated
        public static final int PROTOCOL_JABBER = 7;

        @Deprecated
        public static final int PROTOCOL_MSN = 1;

        @Deprecated
        public static final int PROTOCOL_QQ = 4;

        @Deprecated
        public static final int PROTOCOL_SKYPE = 3;

        @Deprecated
        public static final int PROTOCOL_YAHOO = 2;

        @Deprecated
        public static Object decodeImProtocol(String paramString)
        {
            Object localObject;
            if (paramString == null)
                localObject = null;
            while (true)
            {
                return localObject;
                if (paramString.startsWith("pre:"))
                {
                    localObject = Integer.valueOf(Integer.parseInt(paramString.substring(4)));
                }
                else
                {
                    if (!paramString.startsWith("custom:"))
                        break;
                    localObject = paramString.substring(7);
                }
            }
            throw new IllegalArgumentException("the value is not a valid encoded protocol, " + paramString);
        }

        @Deprecated
        public static String encodeCustomImProtocol(String paramString)
        {
            return "custom:" + paramString;
        }

        @Deprecated
        public static String encodePredefinedImProtocol(int paramInt)
        {
            return "pre:" + paramInt;
        }

        @Deprecated
        public static final CharSequence getDisplayLabel(Context paramContext, int paramInt1, int paramInt2, CharSequence paramCharSequence)
        {
            Object localObject = "";
            switch (paramInt1)
            {
            default:
                localObject = paramContext.getString(17039375);
            case 1:
            case 2:
            }
            while (true)
            {
                return localObject;
                if (paramInt2 != 0)
                {
                    CharSequence[] arrayOfCharSequence2 = paramContext.getResources().getTextArray(17235968);
                    int j = paramInt2 - 1;
                    try
                    {
                        localObject = arrayOfCharSequence2[j];
                    }
                    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException2)
                    {
                        localObject = arrayOfCharSequence2[0];
                    }
                }
                else if (!TextUtils.isEmpty(paramCharSequence))
                {
                    localObject = paramCharSequence;
                    continue;
                    if (paramInt2 != 0)
                    {
                        CharSequence[] arrayOfCharSequence1 = paramContext.getResources().getTextArray(17235972);
                        int i = paramInt2 - 1;
                        try
                        {
                            localObject = arrayOfCharSequence1[i];
                        }
                        catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException1)
                        {
                            localObject = arrayOfCharSequence1[0];
                        }
                    }
                    else if (!TextUtils.isEmpty(paramCharSequence))
                    {
                        localObject = paramCharSequence;
                    }
                }
            }
        }

        @Deprecated
        public static String lookupProviderNameFromId(int paramInt)
        {
            String str;
            switch (paramInt)
            {
            default:
                str = null;
            case 5:
            case 0:
            case 1:
            case 2:
            case 6:
            case 7:
            case 3:
            case 4:
            }
            while (true)
            {
                return str;
                str = "GTalk";
                continue;
                str = "AIM";
                continue;
                str = "MSN";
                continue;
                str = "Yahoo";
                continue;
                str = "ICQ";
                continue;
                str = "JABBER";
                continue;
                str = "SKYPE";
                continue;
                str = "QQ";
            }
        }

        @Deprecated
        public void addPostalLocation(Context paramContext, long paramLong, double paramDouble1, double paramDouble2)
        {
            ContentResolver localContentResolver = paramContext.getContentResolver();
            ContentValues localContentValues = new ContentValues(2);
            localContentValues.put("data", Double.valueOf(paramDouble1));
            localContentValues.put("aux_data", Double.valueOf(paramDouble2));
            long l = ContentUris.parseId(localContentResolver.insert(CONTENT_URI, localContentValues));
            localContentValues.clear();
            localContentValues.put("aux_data", Long.valueOf(l));
            localContentResolver.update(ContentUris.withAppendedId(CONTENT_URI, paramLong), localContentValues, null, null);
        }

        static abstract interface ProviderNames
        {
            public static final String AIM = "AIM";
            public static final String GTALK = "GTalk";
            public static final String ICQ = "ICQ";
            public static final String JABBER = "JABBER";
            public static final String MSN = "MSN";
            public static final String QQ = "QQ";
            public static final String SKYPE = "SKYPE";
            public static final String XMPP = "XMPP";
            public static final String YAHOO = "Yahoo";
        }
    }

    @Deprecated
    public static abstract interface ContactMethodsColumns
    {

        @Deprecated
        public static final String AUX_DATA = "aux_data";

        @Deprecated
        public static final String DATA = "data";

        @Deprecated
        public static final String ISPRIMARY = "isprimary";

        @Deprecated
        public static final String KIND = "kind";

        @Deprecated
        public static final String LABEL = "label";

        @Deprecated
        public static final int MOBILE_EMAIL_TYPE_INDEX = 2;

        @Deprecated
        public static final String MOBILE_EMAIL_TYPE_NAME = "_AUTO_CELL";

        @Deprecated
        public static final String TYPE = "type";

        @Deprecated
        public static final int TYPE_CUSTOM = 0;

        @Deprecated
        public static final int TYPE_HOME = 1;

        @Deprecated
        public static final int TYPE_OTHER = 3;

        @Deprecated
        public static final int TYPE_WORK = 2;
    }

    @Deprecated
    public static final class GroupMembership
        implements BaseColumns, Contacts.GroupsColumns
    {

        @Deprecated
        public static final String CONTENT_DIRECTORY = "groupmembership";

        @Deprecated
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contactsgroupmembership";

        @Deprecated
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contactsgroupmembership";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/groupmembership");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "group_id ASC";

        @Deprecated
        public static final String GROUP_ID = "group_id";

        @Deprecated
        public static final String GROUP_SYNC_ACCOUNT = "group_sync_account";

        @Deprecated
        public static final String GROUP_SYNC_ACCOUNT_TYPE = "group_sync_account_type";

        @Deprecated
        public static final String GROUP_SYNC_ID = "group_sync_id";

        @Deprecated
        public static final String PERSON_ID = "person";

        @Deprecated
        public static final Uri RAW_CONTENT_URI = Uri.parse("content://contacts/groupmembershipraw");
    }

    @Deprecated
    public static final class Phones
        implements BaseColumns, Contacts.PhonesColumns, Contacts.PeopleColumns
    {

        @Deprecated
        public static final Uri CONTENT_FILTER_URL = Uri.parse("content://contacts/phones/filter");

        @Deprecated
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/phone";

        @Deprecated
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/phone";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/phones");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "name ASC";

        @Deprecated
        public static final String PERSON_ID = "person";

        @Deprecated
        public static final CharSequence getDisplayLabel(Context paramContext, int paramInt, CharSequence paramCharSequence)
        {
            return getDisplayLabel(paramContext, paramInt, paramCharSequence, null);
        }

        @Deprecated
        public static final CharSequence getDisplayLabel(Context paramContext, int paramInt, CharSequence paramCharSequence, CharSequence[] paramArrayOfCharSequence)
        {
            Object localObject = "";
            CharSequence[] arrayOfCharSequence;
            int i;
            if (paramInt != 0)
                if (paramArrayOfCharSequence != null)
                {
                    arrayOfCharSequence = paramArrayOfCharSequence;
                    i = paramInt - 1;
                }
            while (true)
            {
                try
                {
                    localObject = arrayOfCharSequence[i];
                    return localObject;
                    arrayOfCharSequence = paramContext.getResources().getTextArray(17235971);
                }
                catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
                {
                    localObject = arrayOfCharSequence[0];
                    continue;
                }
                if (!TextUtils.isEmpty(paramCharSequence))
                    localObject = paramCharSequence;
            }
        }
    }

    @Deprecated
    public static abstract interface PhonesColumns
    {

        @Deprecated
        public static final String ISPRIMARY = "isprimary";

        @Deprecated
        public static final String LABEL = "label";

        @Deprecated
        public static final String NUMBER = "number";

        @Deprecated
        public static final String NUMBER_KEY = "number_key";

        @Deprecated
        public static final String TYPE = "type";

        @Deprecated
        public static final int TYPE_CUSTOM = 0;

        @Deprecated
        public static final int TYPE_FAX_HOME = 5;

        @Deprecated
        public static final int TYPE_FAX_WORK = 4;

        @Deprecated
        public static final int TYPE_HOME = 1;

        @Deprecated
        public static final int TYPE_MOBILE = 2;

        @Deprecated
        public static final int TYPE_OTHER = 7;

        @Deprecated
        public static final int TYPE_PAGER = 6;

        @Deprecated
        public static final int TYPE_WORK = 3;
    }

    @Deprecated
    public static final class Groups
        implements BaseColumns, SyncConstValue, Contacts.GroupsColumns
    {

        @Deprecated
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/contactsgroup";

        @Deprecated
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/contactsgroup";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/groups");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "name ASC";

        @Deprecated
        public static final Uri DELETED_CONTENT_URI = Uri.parse("content://contacts/deleted_groups");

        @Deprecated
        public static final String GROUP_ANDROID_STARRED = "Starred in Android";

        @Deprecated
        public static final String GROUP_MY_CONTACTS = "Contacts";
    }

    @Deprecated
    public static abstract interface GroupsColumns
    {

        @Deprecated
        public static final String NAME = "name";

        @Deprecated
        public static final String NOTES = "notes";

        @Deprecated
        public static final String SHOULD_SYNC = "should_sync";

        @Deprecated
        public static final String SYSTEM_ID = "system_id";
    }

    @Deprecated
    public static final class People
        implements BaseColumns, SyncConstValue, Contacts.PeopleColumns, Contacts.PhonesColumns, Contacts.PresenceColumns
    {

        @Deprecated
        public static final Uri CONTENT_FILTER_URI;

        @Deprecated
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/person";

        @Deprecated
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/person";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/people");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "name ASC";

        @Deprecated
        public static final Uri DELETED_CONTENT_URI;
        private static final String[] GROUPS_PROJECTION = arrayOfString;

        @Deprecated
        public static final String PRIMARY_EMAIL_ID = "primary_email";

        @Deprecated
        public static final String PRIMARY_ORGANIZATION_ID = "primary_organization";

        @Deprecated
        public static final String PRIMARY_PHONE_ID = "primary_phone";

        @Deprecated
        public static final Uri WITH_EMAIL_OR_IM_FILTER_URI;

        static
        {
            CONTENT_FILTER_URI = Uri.parse("content://contacts/people/filter");
            DELETED_CONTENT_URI = Uri.parse("content://contacts/deleted_people");
            WITH_EMAIL_OR_IM_FILTER_URI = Uri.parse("content://contacts/people/with_email_or_im_filter");
            String[] arrayOfString = new String[1];
            arrayOfString[0] = "_id";
        }

        @Deprecated
        public static Uri addToGroup(ContentResolver paramContentResolver, long paramLong1, long paramLong2)
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("person", Long.valueOf(paramLong1));
            localContentValues.put("group_id", Long.valueOf(paramLong2));
            return paramContentResolver.insert(Contacts.GroupMembership.CONTENT_URI, localContentValues);
        }

        @Deprecated
        public static Uri addToGroup(ContentResolver paramContentResolver, long paramLong, String paramString)
        {
            long l1 = 0L;
            Uri localUri = Contacts.Groups.CONTENT_URI;
            String[] arrayOfString1 = GROUPS_PROJECTION;
            String[] arrayOfString2 = new String[1];
            arrayOfString2[0] = paramString;
            Cursor localCursor = paramContentResolver.query(localUri, arrayOfString1, "name=?", arrayOfString2, null);
            if (localCursor != null);
            try
            {
                if (localCursor.moveToFirst())
                {
                    long l2 = localCursor.getLong(0);
                    l1 = l2;
                }
                localCursor.close();
                if (l1 == 0L)
                    throw new IllegalStateException("Failed to find the My Contacts group");
            }
            finally
            {
                localCursor.close();
            }
            return addToGroup(paramContentResolver, paramLong, l1);
        }

        @Deprecated
        public static Uri addToMyContactsGroup(ContentResolver paramContentResolver, long paramLong)
        {
            long l = tryGetMyContactsGroupId(paramContentResolver);
            if (l == 0L)
                throw new IllegalStateException("Failed to find the My Contacts group");
            return addToGroup(paramContentResolver, paramLong, l);
        }

        @Deprecated
        public static Uri createPersonInMyContactsGroup(ContentResolver paramContentResolver, ContentValues paramContentValues)
        {
            Uri localUri = paramContentResolver.insert(CONTENT_URI, paramContentValues);
            if (localUri == null)
            {
                Log.e("Contacts", "Failed to create the contact");
                localUri = null;
            }
            while (true)
            {
                return localUri;
                if (addToMyContactsGroup(paramContentResolver, ContentUris.parseId(localUri)) == null)
                {
                    paramContentResolver.delete(localUri, null, null);
                    localUri = null;
                }
            }
        }

        @Deprecated
        public static Bitmap loadContactPhoto(Context paramContext, Uri paramUri, int paramInt, BitmapFactory.Options paramOptions)
        {
            Bitmap localBitmap = null;
            if (paramUri == null)
                localBitmap = loadPlaceholderPhoto(paramInt, paramContext, paramOptions);
            while (true)
            {
                return localBitmap;
                InputStream localInputStream = openContactPhotoInputStream(paramContext.getContentResolver(), paramUri);
                if (localInputStream != null)
                    localBitmap = BitmapFactory.decodeStream(localInputStream, null, paramOptions);
                if (localBitmap == null)
                    localBitmap = loadPlaceholderPhoto(paramInt, paramContext, paramOptions);
            }
        }

        private static Bitmap loadPlaceholderPhoto(int paramInt, Context paramContext, BitmapFactory.Options paramOptions)
        {
            if (paramInt == 0);
            for (Bitmap localBitmap = null; ; localBitmap = BitmapFactory.decodeResource(paramContext.getResources(), paramInt, paramOptions))
                return localBitmap;
        }

        @Deprecated
        public static void markAsContacted(ContentResolver paramContentResolver, long paramLong)
        {
            Uri localUri = Uri.withAppendedPath(ContentUris.withAppendedId(CONTENT_URI, paramLong), "update_contact_time");
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("last_time_contacted", Long.valueOf(System.currentTimeMillis()));
            paramContentResolver.update(localUri, localContentValues, null, null);
        }

        @Deprecated
        public static InputStream openContactPhotoInputStream(ContentResolver paramContentResolver, Uri paramUri)
        {
            Object localObject1 = null;
            Uri localUri = Uri.withAppendedPath(paramUri, "photo");
            String[] arrayOfString = new String[1];
            arrayOfString[0] = "data";
            Cursor localCursor = paramContentResolver.query(localUri, arrayOfString, null, null, null);
            if (localCursor != null);
            try
            {
                boolean bool = localCursor.moveToNext();
                if (!bool);
                while (true)
                {
                    return localObject1;
                    byte[] arrayOfByte = localCursor.getBlob(0);
                    if (arrayOfByte == null)
                    {
                        if (localCursor != null)
                            localCursor.close();
                    }
                    else
                    {
                        localObject1 = new ByteArrayInputStream(arrayOfByte);
                        if (localCursor != null)
                            localCursor.close();
                    }
                }
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
        }

        @Deprecated
        public static Cursor queryGroups(ContentResolver paramContentResolver, long paramLong)
        {
            Uri localUri = Contacts.GroupMembership.CONTENT_URI;
            String[] arrayOfString = new String[1];
            arrayOfString[0] = String.valueOf(paramLong);
            return paramContentResolver.query(localUri, null, "person=?", arrayOfString, "name ASC");
        }

        @Deprecated
        public static void setPhotoData(ContentResolver paramContentResolver, Uri paramUri, byte[] paramArrayOfByte)
        {
            Uri localUri = Uri.withAppendedPath(paramUri, "photo");
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("data", paramArrayOfByte);
            paramContentResolver.update(localUri, localContentValues, null, null);
        }

        @Deprecated
        public static long tryGetMyContactsGroupId(ContentResolver paramContentResolver)
        {
            Cursor localCursor = paramContentResolver.query(Contacts.Groups.CONTENT_URI, GROUPS_PROJECTION, "system_id='Contacts'", null, null);
            if (localCursor != null);
            try
            {
                if (localCursor.moveToFirst())
                {
                    long l2 = localCursor.getLong(0);
                    l1 = l2;
                    return l1;
                }
                localCursor.close();
                long l1 = 0L;
            }
            finally
            {
                localCursor.close();
            }
        }

        @Deprecated
        public static class Extensions
            implements BaseColumns, Contacts.ExtensionsColumns
        {

            @Deprecated
            public static final String CONTENT_DIRECTORY = "extensions";

            @Deprecated
            public static final String DEFAULT_SORT_ORDER = "name ASC";

            @Deprecated
            public static final String PERSON_ID = "person";
        }

        @Deprecated
        public static final class ContactMethods
            implements BaseColumns, Contacts.ContactMethodsColumns, Contacts.PeopleColumns
        {

            @Deprecated
            public static final String CONTENT_DIRECTORY = "contact_methods";

            @Deprecated
            public static final String DEFAULT_SORT_ORDER = "data ASC";
        }

        @Deprecated
        public static final class Phones
            implements BaseColumns, Contacts.PhonesColumns, Contacts.PeopleColumns
        {

            @Deprecated
            public static final String CONTENT_DIRECTORY = "phones";

            @Deprecated
            public static final String DEFAULT_SORT_ORDER = "number ASC";
        }
    }

    @Deprecated
    public static abstract interface PeopleColumns
    {

        @Deprecated
        public static final String CUSTOM_RINGTONE = "custom_ringtone";

        @Deprecated
        public static final String DISPLAY_NAME = "display_name";

        @Deprecated
        public static final String LAST_TIME_CONTACTED = "last_time_contacted";

        @Deprecated
        public static final String NAME = "name";

        @Deprecated
        public static final String NOTES = "notes";

        @Deprecated
        public static final String PHONETIC_NAME = "phonetic_name";

        @Deprecated
        public static final String PHOTO_VERSION = "photo_version";

        @Deprecated
        public static final String SEND_TO_VOICEMAIL = "send_to_voicemail";

        @Deprecated
        public static final String SORT_STRING = "sort_string";

        @Deprecated
        public static final String STARRED = "starred";

        @Deprecated
        public static final String TIMES_CONTACTED = "times_contacted";
    }

    @Deprecated
    public static final class Settings
        implements BaseColumns, Contacts.SettingsColumns
    {

        @Deprecated
        public static final String CONTENT_DIRECTORY = "settings";

        @Deprecated
        public static final Uri CONTENT_URI = Uri.parse("content://contacts/settings");

        @Deprecated
        public static final String DEFAULT_SORT_ORDER = "key ASC";

        @Deprecated
        public static final String SYNC_EVERYTHING = "syncEverything";

        @Deprecated
        public static String getSetting(ContentResolver paramContentResolver, String paramString1, String paramString2)
        {
            Object localObject1 = null;
            String[] arrayOfString1 = new String[1];
            arrayOfString1[0] = paramString2;
            Uri localUri = CONTENT_URI;
            String[] arrayOfString2 = new String[1];
            arrayOfString2[0] = "value";
            Cursor localCursor = paramContentResolver.query(localUri, arrayOfString2, "key=?", arrayOfString1, null);
            try
            {
                boolean bool = localCursor.moveToNext();
                if (!bool);
                while (true)
                {
                    return localObject1;
                    String str = localCursor.getString(0);
                    localObject1 = str;
                    localCursor.close();
                }
            }
            finally
            {
                localCursor.close();
            }
        }

        @Deprecated
        public static void setSetting(ContentResolver paramContentResolver, String paramString1, String paramString2, String paramString3)
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("key", paramString2);
            localContentValues.put("value", paramString3);
            paramContentResolver.update(CONTENT_URI, localContentValues, null, null);
        }
    }

    @Deprecated
    public static abstract interface SettingsColumns
    {

        @Deprecated
        public static final String KEY = "key";

        @Deprecated
        public static final String VALUE = "value";

        @Deprecated
        public static final String _SYNC_ACCOUNT = "_sync_account";

        @Deprecated
        public static final String _SYNC_ACCOUNT_TYPE = "_sync_account_type";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.Contacts
 * JD-Core Version:        0.6.2
 */