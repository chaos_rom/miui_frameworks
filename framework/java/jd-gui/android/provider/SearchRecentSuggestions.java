package android.provider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.Semaphore;

public class SearchRecentSuggestions
{
    private static final String LOG_TAG = "SearchSuggestions";
    private static final int MAX_HISTORY_COUNT = 250;
    public static final String[] QUERIES_PROJECTION_1LINE;
    public static final String[] QUERIES_PROJECTION_2LINE = arrayOfString2;
    public static final int QUERIES_PROJECTION_DATE_INDEX = 1;
    public static final int QUERIES_PROJECTION_DISPLAY1_INDEX = 3;
    public static final int QUERIES_PROJECTION_DISPLAY2_INDEX = 4;
    public static final int QUERIES_PROJECTION_QUERY_INDEX = 2;
    private static final Semaphore sWritesInProgress = new Semaphore(0);
    private final String mAuthority;
    private final Context mContext;
    private final Uri mSuggestionsUri;
    private final boolean mTwoLineDisplay;

    static
    {
        String[] arrayOfString1 = new String[4];
        arrayOfString1[0] = "_id";
        arrayOfString1[1] = "date";
        arrayOfString1[2] = "query";
        arrayOfString1[3] = "display1";
        QUERIES_PROJECTION_1LINE = arrayOfString1;
        String[] arrayOfString2 = new String[5];
        arrayOfString2[0] = "_id";
        arrayOfString2[1] = "date";
        arrayOfString2[2] = "query";
        arrayOfString2[3] = "display1";
        arrayOfString2[4] = "display2";
    }

    public SearchRecentSuggestions(Context paramContext, String paramString, int paramInt)
    {
        if ((TextUtils.isEmpty(paramString)) || ((paramInt & 0x1) == 0))
            throw new IllegalArgumentException();
        if ((paramInt & 0x2) != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mTwoLineDisplay = bool;
            this.mContext = paramContext;
            this.mAuthority = new String(paramString);
            this.mSuggestionsUri = Uri.parse("content://" + this.mAuthority + "/suggestions");
            return;
        }
    }

    private void saveRecentQueryBlocking(String paramString1, String paramString2)
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        long l = System.currentTimeMillis();
        try
        {
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("display1", paramString1);
            if (this.mTwoLineDisplay)
                localContentValues.put("display2", paramString2);
            localContentValues.put("query", paramString1);
            localContentValues.put("date", Long.valueOf(l));
            localContentResolver.insert(this.mSuggestionsUri, localContentValues);
            truncateHistory(localContentResolver, 250);
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Log.e("SearchSuggestions", "saveRecentQuery", localRuntimeException);
        }
    }

    public void clearHistory()
    {
        truncateHistory(this.mContext.getContentResolver(), 0);
    }

    public void saveRecentQuery(final String paramString1, final String paramString2)
    {
        if (TextUtils.isEmpty(paramString1));
        while (true)
        {
            return;
            if ((!this.mTwoLineDisplay) && (!TextUtils.isEmpty(paramString2)))
                throw new IllegalArgumentException();
            new Thread("saveRecentQuery")
            {
                public void run()
                {
                    SearchRecentSuggestions.this.saveRecentQueryBlocking(paramString1, paramString2);
                    SearchRecentSuggestions.sWritesInProgress.release();
                }
            }
            .start();
        }
    }

    protected void truncateHistory(ContentResolver paramContentResolver, int paramInt)
    {
        if (paramInt < 0)
            throw new IllegalArgumentException();
        String str = null;
        if (paramInt > 0);
        try
        {
            str = "_id IN (SELECT _id FROM suggestions ORDER BY date DESC LIMIT -1 OFFSET " + String.valueOf(paramInt) + ")";
            paramContentResolver.delete(this.mSuggestionsUri, str, null);
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Log.e("SearchSuggestions", "truncateHistory", localRuntimeException);
        }
    }

    void waitForSave()
    {
        do
            sWritesInProgress.acquireUninterruptibly();
        while (sWritesInProgress.availablePermits() > 0);
    }

    private static class SuggestionColumns
        implements BaseColumns
    {
        public static final String DATE = "date";
        public static final String DISPLAY1 = "display1";
        public static final String DISPLAY2 = "display2";
        public static final String QUERY = "query";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.SearchRecentSuggestions
 * JD-Core Version:        0.6.2
 */