package android.provider;

public abstract interface OpenableColumns
{
    public static final String DISPLAY_NAME = "_display_name";
    public static final String SIZE = "_size";
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.provider.OpenableColumns
 * JD-Core Version:        0.6.2
 */