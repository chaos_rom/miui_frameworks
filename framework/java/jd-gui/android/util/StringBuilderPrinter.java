package android.util;

public class StringBuilderPrinter
    implements Printer
{
    private final StringBuilder mBuilder;

    public StringBuilderPrinter(StringBuilder paramStringBuilder)
    {
        this.mBuilder = paramStringBuilder;
    }

    public void println(String paramString)
    {
        this.mBuilder.append(paramString);
        int i = paramString.length();
        if ((i <= 0) || (paramString.charAt(i - 1) != '\n'))
            this.mBuilder.append('\n');
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.StringBuilderPrinter
 * JD-Core Version:        0.6.2
 */