package android.util;

import java.util.AbstractSet;
import java.util.Iterator;

public final class FastImmutableArraySet<T> extends AbstractSet<T>
{
    T[] mContents;
    FastIterator<T> mIterator;

    public FastImmutableArraySet(T[] paramArrayOfT)
    {
        this.mContents = paramArrayOfT;
    }

    public Iterator<T> iterator()
    {
        FastIterator localFastIterator = this.mIterator;
        if (localFastIterator == null)
        {
            localFastIterator = new FastIterator(this.mContents);
            this.mIterator = localFastIterator;
        }
        while (true)
        {
            return localFastIterator;
            localFastIterator.mIndex = 0;
        }
    }

    public int size()
    {
        return this.mContents.length;
    }

    private static final class FastIterator<T>
        implements Iterator<T>
    {
        private final T[] mContents;
        int mIndex;

        public FastIterator(T[] paramArrayOfT)
        {
            this.mContents = paramArrayOfT;
        }

        public boolean hasNext()
        {
            if (this.mIndex != this.mContents.length);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public T next()
        {
            Object[] arrayOfObject = this.mContents;
            int i = this.mIndex;
            this.mIndex = (i + 1);
            return arrayOfObject[i];
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.FastImmutableArraySet
 * JD-Core Version:        0.6.2
 */