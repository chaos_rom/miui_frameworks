package android.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.net.SntpClient;
import android.os.SystemClock;
import android.provider.Settings.Secure;

public class NtpTrustedTime
    implements TrustedTime
{
    private static final boolean LOGD = false;
    private static final String TAG = "NtpTrustedTime";
    private static NtpTrustedTime sSingleton;
    private long mCachedNtpCertainty;
    private long mCachedNtpElapsedRealtime;
    private long mCachedNtpTime;
    private boolean mHasCache;
    private final String mServer;
    private final long mTimeout;

    private NtpTrustedTime(String paramString, long paramLong)
    {
        this.mServer = paramString;
        this.mTimeout = paramLong;
    }

    /** @deprecated */
    public static NtpTrustedTime getInstance(Context paramContext)
    {
        try
        {
            String str1;
            if (sSingleton == null)
            {
                Resources localResources = paramContext.getResources();
                ContentResolver localContentResolver = paramContext.getContentResolver();
                str1 = localResources.getString(17039398);
                long l1 = localResources.getInteger(17694768);
                String str2 = Settings.Secure.getString(localContentResolver, "ntp_server");
                long l2 = Settings.Secure.getLong(localContentResolver, "ntp_timeout", l1);
                if (str2 != null)
                {
                    str3 = str2;
                    sSingleton = new NtpTrustedTime(str3, l2);
                }
            }
            else
            {
                NtpTrustedTime localNtpTrustedTime = sSingleton;
                return localNtpTrustedTime;
            }
            String str3 = str1;
        }
        finally
        {
        }
    }

    public long currentTimeMillis()
    {
        if (!this.mHasCache)
            throw new IllegalStateException("Missing authoritative time source");
        return this.mCachedNtpTime + getCacheAge();
    }

    public boolean forceRefresh()
    {
        boolean bool = false;
        if (this.mServer == null);
        while (true)
        {
            return bool;
            SntpClient localSntpClient = new SntpClient();
            if (localSntpClient.requestTime(this.mServer, (int)this.mTimeout))
            {
                this.mHasCache = true;
                this.mCachedNtpTime = localSntpClient.getNtpTime();
                this.mCachedNtpElapsedRealtime = localSntpClient.getNtpTimeReference();
                this.mCachedNtpCertainty = (localSntpClient.getRoundTripTime() / 2L);
                bool = true;
            }
        }
    }

    public long getCacheAge()
    {
        if (this.mHasCache);
        for (long l = SystemClock.elapsedRealtime() - this.mCachedNtpElapsedRealtime; ; l = 9223372036854775807L)
            return l;
    }

    public long getCacheCertainty()
    {
        if (this.mHasCache);
        for (long l = this.mCachedNtpCertainty; ; l = 9223372036854775807L)
            return l;
    }

    public long getCachedNtpTime()
    {
        return this.mCachedNtpTime;
    }

    public long getCachedNtpTimeReference()
    {
        return this.mCachedNtpElapsedRealtime;
    }

    public boolean hasCache()
    {
        return this.mHasCache;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.NtpTrustedTime
 * JD-Core Version:        0.6.2
 */