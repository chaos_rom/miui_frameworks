package android.util;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import libcore.internal.StringPool;

public final class JsonReader
    implements Closeable
{
    private static final String FALSE = "false";
    private static final String TRUE = "true";
    private final char[] buffer = new char[1024];
    private int bufferStartColumn = 1;
    private int bufferStartLine = 1;
    private final Reader in;
    private boolean lenient = false;
    private int limit = 0;
    private String name;
    private int pos = 0;
    private boolean skipping;
    private final List<JsonScope> stack = new ArrayList();
    private final StringPool stringPool = new StringPool();
    private JsonToken token;
    private String value;
    private int valueLength;
    private int valuePos;

    public JsonReader(Reader paramReader)
    {
        push(JsonScope.EMPTY_DOCUMENT);
        this.skipping = false;
        if (paramReader == null)
            throw new NullPointerException("in == null");
        this.in = paramReader;
    }

    private JsonToken advance()
        throws IOException
    {
        peek();
        JsonToken localJsonToken = this.token;
        this.token = null;
        this.value = null;
        this.name = null;
        return localJsonToken;
    }

    private void checkLenient()
        throws IOException
    {
        if (!this.lenient)
            throw syntaxError("Use JsonReader.setLenient(true) to accept malformed JSON");
    }

    private JsonToken decodeLiteral()
        throws IOException
    {
        JsonToken localJsonToken;
        if (this.valuePos == -1)
            localJsonToken = JsonToken.STRING;
        while (true)
        {
            return localJsonToken;
            if ((this.valueLength == 4) && (('n' == this.buffer[this.valuePos]) || ('N' == this.buffer[this.valuePos])) && (('u' == this.buffer[(1 + this.valuePos)]) || ('U' == this.buffer[(1 + this.valuePos)])) && (('l' == this.buffer[(2 + this.valuePos)]) || ('L' == this.buffer[(2 + this.valuePos)])) && (('l' == this.buffer[(3 + this.valuePos)]) || ('L' == this.buffer[(3 + this.valuePos)])))
            {
                this.value = "null";
                localJsonToken = JsonToken.NULL;
            }
            else if ((this.valueLength == 4) && (('t' == this.buffer[this.valuePos]) || ('T' == this.buffer[this.valuePos])) && (('r' == this.buffer[(1 + this.valuePos)]) || ('R' == this.buffer[(1 + this.valuePos)])) && (('u' == this.buffer[(2 + this.valuePos)]) || ('U' == this.buffer[(2 + this.valuePos)])) && (('e' == this.buffer[(3 + this.valuePos)]) || ('E' == this.buffer[(3 + this.valuePos)])))
            {
                this.value = "true";
                localJsonToken = JsonToken.BOOLEAN;
            }
            else if ((this.valueLength == 5) && (('f' == this.buffer[this.valuePos]) || ('F' == this.buffer[this.valuePos])) && (('a' == this.buffer[(1 + this.valuePos)]) || ('A' == this.buffer[(1 + this.valuePos)])) && (('l' == this.buffer[(2 + this.valuePos)]) || ('L' == this.buffer[(2 + this.valuePos)])) && (('s' == this.buffer[(3 + this.valuePos)]) || ('S' == this.buffer[(3 + this.valuePos)])) && (('e' == this.buffer[(4 + this.valuePos)]) || ('E' == this.buffer[(4 + this.valuePos)])))
            {
                this.value = "false";
                localJsonToken = JsonToken.BOOLEAN;
            }
            else
            {
                this.value = this.stringPool.get(this.buffer, this.valuePos, this.valueLength);
                localJsonToken = decodeNumber(this.buffer, this.valuePos, this.valueLength);
            }
        }
    }

    private JsonToken decodeNumber(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = paramInt1;
        int j = paramArrayOfChar[i];
        if (j == 45)
        {
            i++;
            j = paramArrayOfChar[i];
        }
        int k;
        int m;
        if (j == 48)
        {
            k = i + 1;
            m = paramArrayOfChar[k];
        }
        JsonToken localJsonToken;
        while (m == 46)
        {
            k++;
            m = paramArrayOfChar[k];
            while (true)
                if ((m >= 48) && (m <= 57))
                {
                    k++;
                    m = paramArrayOfChar[k];
                    continue;
                    if ((j >= 49) && (j <= 57))
                    {
                        k = i + 1;
                        for (m = paramArrayOfChar[k]; (m >= 48) && (m <= 57); m = paramArrayOfChar[k])
                            k++;
                        break;
                    }
                    localJsonToken = JsonToken.STRING;
                }
        }
        while (true)
        {
            return localJsonToken;
            if ((m == 101) || (m == 69))
            {
                int n = k + 1;
                int i1 = paramArrayOfChar[n];
                if ((i1 == 43) || (i1 == 45))
                {
                    n++;
                    i1 = paramArrayOfChar[n];
                }
                int i2;
                if ((i1 >= 48) && (i1 <= 57))
                {
                    k = n + 1;
                    i2 = paramArrayOfChar[k];
                }
                while (true)
                    if ((i2 >= 48) && (i2 <= 57))
                    {
                        k++;
                        i2 = paramArrayOfChar[k];
                        continue;
                        localJsonToken = JsonToken.STRING;
                        break;
                    }
            }
            if (k == paramInt1 + paramInt2)
                localJsonToken = JsonToken.NUMBER;
            else
                localJsonToken = JsonToken.STRING;
        }
    }

    private void expect(JsonToken paramJsonToken)
        throws IOException
    {
        peek();
        if (this.token != paramJsonToken)
            throw new IllegalStateException("Expected " + paramJsonToken + " but was " + peek());
        advance();
    }

    private boolean fillBuffer(int paramInt)
        throws IOException
    {
        int i = 1;
        int k = 0;
        if (k < this.pos)
        {
            if (this.buffer[k] == '\n')
                this.bufferStartLine = (1 + this.bufferStartLine);
            for (this.bufferStartColumn = i; ; this.bufferStartColumn = (1 + this.bufferStartColumn))
            {
                k++;
                break;
            }
        }
        if (this.limit != this.pos)
        {
            this.limit -= this.pos;
            System.arraycopy(this.buffer, this.pos, this.buffer, 0, this.limit);
            this.pos = 0;
            do
            {
                int m = this.in.read(this.buffer, this.limit, this.buffer.length - this.limit);
                if (m == -1)
                    break;
                this.limit = (m + this.limit);
                if ((this.bufferStartLine == i) && (this.bufferStartColumn == i) && (this.limit > 0) && (this.buffer[0] == 65279))
                {
                    this.pos = (1 + this.pos);
                    this.bufferStartColumn = (-1 + this.bufferStartColumn);
                }
            }
            while (this.limit < paramInt);
        }
        while (true)
        {
            return i;
            this.limit = 0;
            break;
            int j = 0;
        }
    }

    private int getColumnNumber()
    {
        int i = this.bufferStartColumn;
        int j = 0;
        if (j < this.pos)
        {
            if (this.buffer[j] == '\n');
            for (i = 1; ; i++)
            {
                j++;
                break;
            }
        }
        return i;
    }

    private int getLineNumber()
    {
        int i = this.bufferStartLine;
        for (int j = 0; j < this.pos; j++)
            if (this.buffer[j] == '\n')
                i++;
        return i;
    }

    private CharSequence getSnippet()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = Math.min(this.pos, 20);
        localStringBuilder.append(this.buffer, this.pos - i, i);
        int j = Math.min(this.limit - this.pos, 20);
        localStringBuilder.append(this.buffer, this.pos, j);
        return localStringBuilder;
    }

    private JsonToken nextInArray(boolean paramBoolean)
        throws IOException
    {
        label11: JsonToken localJsonToken;
        if (paramBoolean)
        {
            replaceTop(JsonScope.NONEMPTY_ARRAY);
            switch (nextNonWhitespace())
            {
            default:
                this.pos = (-1 + this.pos);
                localJsonToken = nextValue();
            case 93:
            case 44:
            case 59:
            }
        }
        while (true)
        {
            return localJsonToken;
            switch (nextNonWhitespace())
            {
            case 44:
            default:
                throw syntaxError("Unterminated array");
            case 93:
                pop();
                localJsonToken = JsonToken.END_ARRAY;
                this.token = localJsonToken;
                break;
            case 59:
                checkLenient();
                break label11;
                if (paramBoolean)
                {
                    pop();
                    localJsonToken = JsonToken.END_ARRAY;
                    this.token = localJsonToken;
                }
                else
                {
                    checkLenient();
                    this.pos = (-1 + this.pos);
                    this.value = "null";
                    localJsonToken = JsonToken.NULL;
                    this.token = localJsonToken;
                }
                break;
            }
        }
    }

    private JsonToken nextInObject(boolean paramBoolean)
        throws IOException
    {
        int i;
        JsonToken localJsonToken;
        if (paramBoolean)
            switch (nextNonWhitespace())
            {
            default:
                this.pos = (-1 + this.pos);
                i = nextNonWhitespace();
                switch (i)
                {
                default:
                    checkLenient();
                    this.pos = (-1 + this.pos);
                    this.name = nextLiteral(false);
                    if (this.name.isEmpty())
                        throw syntaxError("Expected name");
                    break;
                case 39:
                case 34:
                }
            case 125:
                pop();
                localJsonToken = JsonToken.END_OBJECT;
                this.token = localJsonToken;
            }
        while (true)
        {
            return localJsonToken;
            switch (nextNonWhitespace())
            {
            case 44:
            case 59:
            default:
                throw syntaxError("Unterminated object");
            case 125:
            }
            pop();
            localJsonToken = JsonToken.END_OBJECT;
            this.token = localJsonToken;
            continue;
            checkLenient();
            this.name = nextString((char)i);
            replaceTop(JsonScope.DANGLING_NAME);
            localJsonToken = JsonToken.NAME;
            this.token = localJsonToken;
        }
    }

    private String nextLiteral(boolean paramBoolean)
        throws IOException
    {
        StringBuilder localStringBuilder = null;
        this.valuePos = -1;
        this.valueLength = 0;
        int i = 0;
        label186: String str;
        while (i + this.pos < this.limit)
            switch (this.buffer[(i + this.pos)])
            {
            default:
                i++;
                break;
            case '#':
            case '/':
            case ';':
            case '=':
            case '\\':
                checkLenient();
            case '\t':
            case '\n':
            case '\f':
            case '\r':
            case ' ':
            case ',':
            case ':':
            case '[':
            case ']':
            case '{':
            case '}':
                if ((!paramBoolean) || (localStringBuilder != null))
                    break label319;
                this.valuePos = this.pos;
                str = null;
            }
        while (true)
        {
            this.valueLength = (i + this.valueLength);
            this.pos = (i + this.pos);
            return str;
            if (i < this.buffer.length)
            {
                if (fillBuffer(i + 1))
                    break;
                this.buffer[this.limit] = '\000';
                break label186;
            }
            if (localStringBuilder == null)
                localStringBuilder = new StringBuilder();
            localStringBuilder.append(this.buffer, this.pos, i);
            this.valueLength = (i + this.valueLength);
            this.pos = (i + this.pos);
            i = 0;
            if (fillBuffer(1))
                break;
            break label186;
            label319: if (this.skipping)
            {
                str = "skipped!";
            }
            else if (localStringBuilder == null)
            {
                str = this.stringPool.get(this.buffer, this.pos, i);
            }
            else
            {
                localStringBuilder.append(this.buffer, this.pos, i);
                str = localStringBuilder.toString();
            }
        }
    }

    private int nextNonWhitespace()
        throws IOException
    {
        while ((this.pos < this.limit) || (fillBuffer(1)))
        {
            char[] arrayOfChar = this.buffer;
            int i = this.pos;
            this.pos = (i + 1);
            int j = arrayOfChar[i];
            switch (j)
            {
            case 9:
            case 10:
            case 13:
            case 32:
            default:
            case 47:
                while (true)
                {
                    return j;
                    if ((this.pos != this.limit) || (fillBuffer(1)))
                    {
                        checkLenient();
                        switch (this.buffer[this.pos])
                        {
                        default:
                        case '*':
                        case '/':
                        }
                    }
                }
                this.pos = (1 + this.pos);
                if (!skipTo("*/"))
                    throw syntaxError("Unterminated comment");
                this.pos = (2 + this.pos);
                continue;
                this.pos = (1 + this.pos);
                skipToEndOfLine();
                break;
            case 35:
            }
            checkLenient();
            skipToEndOfLine();
        }
        throw new EOFException("End of input");
    }

    private String nextString(char paramChar)
        throws IOException
    {
        StringBuilder localStringBuilder = null;
        do
        {
            int i = this.pos;
            while (this.pos < this.limit)
            {
                char[] arrayOfChar = this.buffer;
                int j = this.pos;
                this.pos = (j + 1);
                char c = arrayOfChar[j];
                if (c == paramChar)
                {
                    String str;
                    if (this.skipping)
                        str = "skipped!";
                    while (true)
                    {
                        return str;
                        if (localStringBuilder == null)
                        {
                            str = this.stringPool.get(this.buffer, i, -1 + (this.pos - i));
                        }
                        else
                        {
                            localStringBuilder.append(this.buffer, i, -1 + (this.pos - i));
                            str = localStringBuilder.toString();
                        }
                    }
                }
                if (c == '\\')
                {
                    if (localStringBuilder == null)
                        localStringBuilder = new StringBuilder();
                    localStringBuilder.append(this.buffer, i, -1 + (this.pos - i));
                    localStringBuilder.append(readEscapeCharacter());
                    i = this.pos;
                }
            }
            if (localStringBuilder == null)
                localStringBuilder = new StringBuilder();
            localStringBuilder.append(this.buffer, i, this.pos - i);
        }
        while (fillBuffer(1));
        throw syntaxError("Unterminated string");
    }

    private JsonToken nextValue()
        throws IOException
    {
        int i = nextNonWhitespace();
        JsonToken localJsonToken;
        switch (i)
        {
        default:
            this.pos = (-1 + this.pos);
            localJsonToken = readLiteral();
        case 123:
        case 91:
        case 39:
        case 34:
        }
        while (true)
        {
            return localJsonToken;
            push(JsonScope.EMPTY_OBJECT);
            localJsonToken = JsonToken.BEGIN_OBJECT;
            this.token = localJsonToken;
            continue;
            push(JsonScope.EMPTY_ARRAY);
            localJsonToken = JsonToken.BEGIN_ARRAY;
            this.token = localJsonToken;
            continue;
            checkLenient();
            this.value = nextString((char)i);
            localJsonToken = JsonToken.STRING;
            this.token = localJsonToken;
        }
    }

    private JsonToken objectValue()
        throws IOException
    {
        switch (nextNonWhitespace())
        {
        case 59:
        case 60:
        default:
            throw syntaxError("Expected ':'");
        case 61:
            checkLenient();
            if (((this.pos < this.limit) || (fillBuffer(1))) && (this.buffer[this.pos] == '>'))
                this.pos = (1 + this.pos);
            break;
        case 58:
        }
        replaceTop(JsonScope.NONEMPTY_OBJECT);
        return nextValue();
    }

    private JsonScope peekStack()
    {
        return (JsonScope)this.stack.get(-1 + this.stack.size());
    }

    private JsonScope pop()
    {
        return (JsonScope)this.stack.remove(-1 + this.stack.size());
    }

    private void push(JsonScope paramJsonScope)
    {
        this.stack.add(paramJsonScope);
    }

    private char readEscapeCharacter()
        throws IOException
    {
        if ((this.pos == this.limit) && (!fillBuffer(1)))
            throw syntaxError("Unterminated escape sequence");
        char[] arrayOfChar = this.buffer;
        int i = this.pos;
        this.pos = (i + 1);
        char c = arrayOfChar[i];
        switch (c)
        {
        default:
        case 'u':
        case 't':
        case 'b':
        case 'n':
        case 'r':
        case 'f':
        }
        while (true)
        {
            return c;
            if ((4 + this.pos > this.limit) && (!fillBuffer(4)))
                throw syntaxError("Unterminated escape sequence");
            String str = this.stringPool.get(this.buffer, this.pos, 4);
            this.pos = (4 + this.pos);
            c = (char)Integer.parseInt(str, 16);
            continue;
            c = '\t';
            continue;
            c = '\b';
            continue;
            c = '\n';
            continue;
            c = '\r';
            continue;
            c = '\f';
        }
    }

    private JsonToken readLiteral()
        throws IOException
    {
        this.value = nextLiteral(true);
        if (this.valueLength == 0)
            throw syntaxError("Expected literal value");
        this.token = decodeLiteral();
        if (this.token == JsonToken.STRING)
            checkLenient();
        return this.token;
    }

    private void replaceTop(JsonScope paramJsonScope)
    {
        this.stack.set(-1 + this.stack.size(), paramJsonScope);
    }

    private boolean skipTo(String paramString)
        throws IOException
    {
        if ((this.pos + paramString.length() <= this.limit) || (fillBuffer(paramString.length())))
            for (int i = 0; ; i++)
            {
                if (i >= paramString.length())
                    break label75;
                if (this.buffer[(i + this.pos)] != paramString.charAt(i))
                {
                    this.pos = (1 + this.pos);
                    break;
                }
            }
        label75: for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void skipToEndOfLine()
        throws IOException
    {
        int j;
        do
        {
            if ((this.pos >= this.limit) && (!fillBuffer(1)))
                break;
            char[] arrayOfChar = this.buffer;
            int i = this.pos;
            this.pos = (i + 1);
            j = arrayOfChar[i];
        }
        while ((j != 13) && (j != 10));
    }

    private IOException syntaxError(String paramString)
        throws IOException
    {
        throw new MalformedJsonException(paramString + " at line " + getLineNumber() + " column " + getColumnNumber());
    }

    public void beginArray()
        throws IOException
    {
        expect(JsonToken.BEGIN_ARRAY);
    }

    public void beginObject()
        throws IOException
    {
        expect(JsonToken.BEGIN_OBJECT);
    }

    public void close()
        throws IOException
    {
        this.value = null;
        this.token = null;
        this.stack.clear();
        this.stack.add(JsonScope.CLOSED);
        this.in.close();
    }

    public void endArray()
        throws IOException
    {
        expect(JsonToken.END_ARRAY);
    }

    public void endObject()
        throws IOException
    {
        expect(JsonToken.END_OBJECT);
    }

    public boolean hasNext()
        throws IOException
    {
        peek();
        if ((this.token != JsonToken.END_OBJECT) && (this.token != JsonToken.END_ARRAY));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isLenient()
    {
        return this.lenient;
    }

    public boolean nextBoolean()
        throws IOException
    {
        peek();
        if (this.token != JsonToken.BOOLEAN)
            throw new IllegalStateException("Expected a boolean but was " + this.token);
        if (this.value == "true");
        for (boolean bool = true; ; bool = false)
        {
            advance();
            return bool;
        }
    }

    public double nextDouble()
        throws IOException
    {
        peek();
        if ((this.token != JsonToken.STRING) && (this.token != JsonToken.NUMBER))
            throw new IllegalStateException("Expected a double but was " + this.token);
        double d = Double.parseDouble(this.value);
        advance();
        return d;
    }

    public int nextInt()
        throws IOException
    {
        peek();
        if ((this.token != JsonToken.STRING) && (this.token != JsonToken.NUMBER))
            throw new IllegalStateException("Expected an int but was " + this.token);
        try
        {
            int j = Integer.parseInt(this.value);
            i = j;
            advance();
            return i;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            int i;
            double d;
            do
            {
                d = Double.parseDouble(this.value);
                i = (int)d;
            }
            while (i == d);
        }
        throw new NumberFormatException(this.value);
    }

    public long nextLong()
        throws IOException
    {
        peek();
        if ((this.token != JsonToken.STRING) && (this.token != JsonToken.NUMBER))
            throw new IllegalStateException("Expected a long but was " + this.token);
        try
        {
            long l2 = Long.parseLong(this.value);
            l1 = l2;
            advance();
            return l1;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            long l1;
            double d;
            do
            {
                d = Double.parseDouble(this.value);
                l1 = ()d;
            }
            while (l1 == d);
        }
        throw new NumberFormatException(this.value);
    }

    public String nextName()
        throws IOException
    {
        peek();
        if (this.token != JsonToken.NAME)
            throw new IllegalStateException("Expected a name but was " + peek());
        String str = this.name;
        advance();
        return str;
    }

    public void nextNull()
        throws IOException
    {
        peek();
        if (this.token != JsonToken.NULL)
            throw new IllegalStateException("Expected null but was " + this.token);
        advance();
    }

    public String nextString()
        throws IOException
    {
        peek();
        if ((this.token != JsonToken.STRING) && (this.token != JsonToken.NUMBER))
            throw new IllegalStateException("Expected a string but was " + peek());
        String str = this.value;
        advance();
        return str;
    }

    public JsonToken peek()
        throws IOException
    {
        Object localObject;
        if (this.token != null)
            localObject = this.token;
        while (true)
        {
            return localObject;
            switch (1.$SwitchMap$android$util$JsonScope[peekStack().ordinal()])
            {
            default:
                throw new AssertionError();
            case 1:
                replaceTop(JsonScope.NONEMPTY_DOCUMENT);
                localObject = nextValue();
                if ((!this.lenient) && (this.token != JsonToken.BEGIN_ARRAY) && (this.token != JsonToken.BEGIN_OBJECT))
                    throw new IOException("Expected JSON document to start with '[' or '{' but was " + this.token);
                break;
            case 2:
                localObject = nextInArray(true);
                break;
            case 3:
                localObject = nextInArray(false);
                break;
            case 4:
                localObject = nextInObject(true);
                break;
            case 5:
                localObject = objectValue();
                break;
            case 6:
                localObject = nextInObject(false);
                break;
            case 7:
                try
                {
                    JsonToken localJsonToken = nextValue();
                    if (this.lenient)
                        localObject = localJsonToken;
                    else
                        throw syntaxError("Expected EOF");
                }
                catch (EOFException localEOFException)
                {
                    localObject = JsonToken.END_DOCUMENT;
                    this.token = ((JsonToken)localObject);
                }
            case 8:
            }
        }
        throw new IllegalStateException("JsonReader is closed");
    }

    public void setLenient(boolean paramBoolean)
    {
        this.lenient = paramBoolean;
    }

    public void skipValue()
        throws IOException
    {
        this.skipping = true;
        int i = 0;
        try
        {
            JsonToken localJsonToken1 = advance();
            if (localJsonToken1 != JsonToken.BEGIN_ARRAY)
            {
                JsonToken localJsonToken2 = JsonToken.BEGIN_OBJECT;
                if (localJsonToken1 != localJsonToken2);
            }
            else
            {
                i++;
            }
            while (i == 0)
            {
                return;
                if (localJsonToken1 != JsonToken.END_ARRAY)
                {
                    JsonToken localJsonToken3 = JsonToken.END_OBJECT;
                    if (localJsonToken1 != localJsonToken3);
                }
                else
                {
                    i--;
                }
            }
        }
        finally
        {
            this.skipping = false;
        }
    }

    public String toString()
    {
        return getClass().getSimpleName() + " near " + getSnippet();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.JsonReader
 * JD-Core Version:        0.6.2
 */