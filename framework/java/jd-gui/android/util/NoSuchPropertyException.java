package android.util;

public class NoSuchPropertyException extends RuntimeException
{
    public NoSuchPropertyException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.NoSuchPropertyException
 * JD-Core Version:        0.6.2
 */