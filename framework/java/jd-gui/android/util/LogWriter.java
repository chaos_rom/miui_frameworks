package android.util;

import java.io.Writer;

public class LogWriter extends Writer
{
    private final int mBuffer;
    private StringBuilder mBuilder = new StringBuilder(128);
    private final int mPriority;
    private final String mTag;

    public LogWriter(int paramInt, String paramString)
    {
        this.mPriority = paramInt;
        this.mTag = paramString;
        this.mBuffer = 0;
    }

    public LogWriter(int paramInt1, String paramString, int paramInt2)
    {
        this.mPriority = paramInt1;
        this.mTag = paramString;
        this.mBuffer = paramInt2;
    }

    private void flushBuilder()
    {
        if (this.mBuilder.length() > 0)
        {
            Log.println_native(this.mBuffer, this.mPriority, this.mTag, this.mBuilder.toString());
            this.mBuilder.delete(0, this.mBuilder.length());
        }
    }

    public void close()
    {
        flushBuilder();
    }

    public void flush()
    {
        flushBuilder();
    }

    public void write(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        int i = 0;
        if (i < paramInt2)
        {
            char c = paramArrayOfChar[(paramInt1 + i)];
            if (c == '\n')
                flushBuilder();
            while (true)
            {
                i++;
                break;
                this.mBuilder.append(c);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.LogWriter
 * JD-Core Version:        0.6.2
 */