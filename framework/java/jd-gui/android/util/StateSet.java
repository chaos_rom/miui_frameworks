package android.util;

public class StateSet
{
    public static final int[] NOTHING = arrayOfInt;
    public static final int[] WILD_CARD = new int[0];

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 0;
    }

    public static String dump(int[] paramArrayOfInt)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = paramArrayOfInt.length;
        int j = 0;
        if (j < i)
        {
            switch (paramArrayOfInt[j])
            {
            default:
            case 16842909:
            case 16842919:
            case 16842913:
            case 16842908:
            case 16842910:
            }
            while (true)
            {
                j++;
                break;
                localStringBuilder.append("W ");
                continue;
                localStringBuilder.append("P ");
                continue;
                localStringBuilder.append("S ");
                continue;
                localStringBuilder.append("F ");
                continue;
                localStringBuilder.append("E ");
            }
        }
        return localStringBuilder.toString();
    }

    public static boolean isWildCard(int[] paramArrayOfInt)
    {
        boolean bool = false;
        if ((paramArrayOfInt.length == 0) || (paramArrayOfInt[0] == 0))
            bool = true;
        return bool;
    }

    public static boolean stateSetMatches(int[] paramArrayOfInt, int paramInt)
    {
        boolean bool = true;
        int i = paramArrayOfInt.length;
        for (int j = 0; ; j++)
        {
            int k;
            if (j < i)
            {
                k = paramArrayOfInt[j];
                if (k != 0)
                    break label27;
            }
            while (true)
            {
                return bool;
                label27: if (k > 0)
                {
                    if (paramInt == k)
                        break;
                    bool = false;
                }
                else
                {
                    if (paramInt != -k)
                        break;
                    bool = false;
                }
            }
        }
    }

    public static boolean stateSetMatches(int[] paramArrayOfInt1, int[] paramArrayOfInt2)
    {
        boolean bool = false;
        if (paramArrayOfInt2 == null)
            if ((paramArrayOfInt1 == null) || (isWildCard(paramArrayOfInt1)))
                bool = true;
        label133: label135: 
        while (true)
        {
            return bool;
            int i = paramArrayOfInt1.length;
            int j = paramArrayOfInt2.length;
            int k = 0;
            label31: if (k < i)
            {
                int m = paramArrayOfInt1[k];
                if (m == 0)
                {
                    bool = true;
                }
                else
                {
                    int n;
                    label61: int i1;
                    if (m > 0)
                    {
                        n = 1;
                        i1 = 0;
                    }
                    for (int i2 = 0; ; i2++)
                    {
                        int i3;
                        if (i2 < j)
                        {
                            i3 = paramArrayOfInt2[i2];
                            if (i3 != 0)
                                break label117;
                            if (n != 0)
                                break;
                        }
                        while (true)
                        {
                            if ((n != 0) && (i1 == 0))
                                break label133;
                            k++;
                            break label31;
                            n = 0;
                            m = -m;
                            break label61;
                            label117: if (i3 != m)
                                break label135;
                            if (n == 0)
                                break;
                            i1 = 1;
                        }
                        break;
                    }
                }
            }
            else
            {
                bool = true;
            }
        }
    }

    public static int[] trimStateSet(int[] paramArrayOfInt, int paramInt)
    {
        if (paramArrayOfInt.length == paramInt);
        while (true)
        {
            return paramArrayOfInt;
            int[] arrayOfInt = new int[paramInt];
            System.arraycopy(paramArrayOfInt, 0, arrayOfInt, 0, paramInt);
            paramArrayOfInt = arrayOfInt;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.StateSet
 * JD-Core Version:        0.6.2
 */