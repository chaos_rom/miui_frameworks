package android.util;

public class LogPrinter
    implements Printer
{
    private final int mBuffer;
    private final int mPriority;
    private final String mTag;

    public LogPrinter(int paramInt, String paramString)
    {
        this.mPriority = paramInt;
        this.mTag = paramString;
        this.mBuffer = 0;
    }

    public LogPrinter(int paramInt1, String paramString, int paramInt2)
    {
        this.mPriority = paramInt1;
        this.mTag = paramString;
        this.mBuffer = paramInt2;
    }

    public void println(String paramString)
    {
        Log.println_native(this.mBuffer, this.mPriority, this.mTag, paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.LogPrinter
 * JD-Core Version:        0.6.2
 */