package android.util;

class SynchronizedPool<T extends Poolable<T>>
    implements Pool<T>
{
    private final Object mLock;
    private final Pool<T> mPool;

    public SynchronizedPool(Pool<T> paramPool)
    {
        this.mPool = paramPool;
        this.mLock = this;
    }

    public SynchronizedPool(Pool<T> paramPool, Object paramObject)
    {
        this.mPool = paramPool;
        this.mLock = paramObject;
    }

    public T acquire()
    {
        synchronized (this.mLock)
        {
            Poolable localPoolable = this.mPool.acquire();
            return localPoolable;
        }
    }

    public void release(T paramT)
    {
        synchronized (this.mLock)
        {
            this.mPool.release(paramT);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.SynchronizedPool
 * JD-Core Version:        0.6.2
 */