package android.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DebugUtils
{
    public static void buildShortClassTag(Object paramObject, StringBuilder paramStringBuilder)
    {
        if (paramObject == null)
            paramStringBuilder.append("null");
        while (true)
        {
            return;
            String str = paramObject.getClass().getSimpleName();
            if ((str == null) || (str.isEmpty()))
            {
                str = paramObject.getClass().getName();
                int i = str.lastIndexOf('.');
                if (i > 0)
                    str = str.substring(i + 1);
            }
            paramStringBuilder.append(str);
            paramStringBuilder.append('{');
            paramStringBuilder.append(Integer.toHexString(System.identityHashCode(paramObject)));
        }
    }

    public static boolean isObjectSelected(Object paramObject)
    {
        boolean bool = false;
        String str1 = System.getenv("ANDROID_OBJECT_FILTER");
        String[] arrayOfString1;
        if ((str1 != null) && (str1.length() > 0))
        {
            arrayOfString1 = str1.split("@");
            if (!paramObject.getClass().getSimpleName().matches(arrayOfString1[0]));
        }
        for (int i = 1; ; i++)
        {
            if (i < arrayOfString1.length)
            {
                String[] arrayOfString2 = arrayOfString1[i].split("=");
                Class localClass1 = paramObject.getClass();
                Class localClass2 = localClass1;
                try
                {
                    Method localMethod;
                    do
                    {
                        localMethod = localClass2.getDeclaredMethod("get" + arrayOfString2[0].substring(0, 1).toUpperCase() + arrayOfString2[0].substring(1), (Class[])null);
                        localClass2 = localClass1.getSuperclass();
                    }
                    while ((localClass2 != null) && (localMethod == null));
                    if (localMethod == null)
                        continue;
                    Object localObject = localMethod.invoke(paramObject, (Object[])null);
                    if (localObject != null);
                    for (String str2 = localObject.toString(); ; str2 = "null")
                    {
                        bool |= str2.matches(arrayOfString2[1]);
                        break;
                    }
                }
                catch (NoSuchMethodException localNoSuchMethodException)
                {
                    localNoSuchMethodException.printStackTrace();
                }
                catch (IllegalAccessException localIllegalAccessException)
                {
                    localIllegalAccessException.printStackTrace();
                }
                catch (InvocationTargetException localInvocationTargetException)
                {
                    localInvocationTargetException.printStackTrace();
                }
            }
            return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.DebugUtils
 * JD-Core Version:        0.6.2
 */