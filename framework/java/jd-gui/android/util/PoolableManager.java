package android.util;

public abstract interface PoolableManager<T extends Poolable<T>>
{
    public abstract T newInstance();

    public abstract void onAcquired(T paramT);

    public abstract void onReleased(T paramT);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.PoolableManager
 * JD-Core Version:        0.6.2
 */