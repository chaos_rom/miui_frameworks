package android.util;

import java.io.PrintStream;

public class PrintStreamPrinter
    implements Printer
{
    private final PrintStream mPS;

    public PrintStreamPrinter(PrintStream paramPrintStream)
    {
        this.mPS = paramPrintStream;
    }

    public void println(String paramString)
    {
        this.mPS.println(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.PrintStreamPrinter
 * JD-Core Version:        0.6.2
 */