package android.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import org.apache.harmony.xml.ExpatReader;
import org.kxml2.io.KXmlParser;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

public class Xml
{
    public static String FEATURE_RELAXED = "http://xmlpull.org/v1/doc/features.html#relaxed";

    public static AttributeSet asAttributeSet(XmlPullParser paramXmlPullParser)
    {
        if ((paramXmlPullParser instanceof AttributeSet));
        for (Object localObject = (AttributeSet)paramXmlPullParser; ; localObject = new XmlPullAttributes(paramXmlPullParser))
            return localObject;
    }

    public static Encoding findEncodingByName(String paramString)
        throws UnsupportedEncodingException
    {
        Encoding localEncoding;
        if (paramString == null)
        {
            localEncoding = Encoding.UTF_8;
            return localEncoding;
        }
        Encoding[] arrayOfEncoding = Encoding.values();
        int i = arrayOfEncoding.length;
        for (int j = 0; ; j++)
        {
            if (j >= i)
                break label49;
            localEncoding = arrayOfEncoding[j];
            if (localEncoding.expatName.equalsIgnoreCase(paramString))
                break;
        }
        label49: throw new UnsupportedEncodingException(paramString);
    }

    public static XmlPullParser newPullParser()
    {
        try
        {
            KXmlParser localKXmlParser = new KXmlParser();
            localKXmlParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-docdecl", true);
            localKXmlParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
            return localKXmlParser;
        }
        catch (XmlPullParserException localXmlPullParserException)
        {
        }
        throw new AssertionError();
    }

    public static XmlSerializer newSerializer()
    {
        try
        {
            XmlSerializer localXmlSerializer = XmlSerializerFactory.instance.newSerializer();
            return localXmlSerializer;
        }
        catch (XmlPullParserException localXmlPullParserException)
        {
            throw new AssertionError(localXmlPullParserException);
        }
    }

    public static void parse(InputStream paramInputStream, Encoding paramEncoding, ContentHandler paramContentHandler)
        throws IOException, SAXException
    {
        ExpatReader localExpatReader = new ExpatReader();
        localExpatReader.setContentHandler(paramContentHandler);
        InputSource localInputSource = new InputSource(paramInputStream);
        localInputSource.setEncoding(paramEncoding.expatName);
        localExpatReader.parse(localInputSource);
    }

    public static void parse(Reader paramReader, ContentHandler paramContentHandler)
        throws IOException, SAXException
    {
        ExpatReader localExpatReader = new ExpatReader();
        localExpatReader.setContentHandler(paramContentHandler);
        localExpatReader.parse(new InputSource(paramReader));
    }

    public static void parse(String paramString, ContentHandler paramContentHandler)
        throws SAXException
    {
        try
        {
            ExpatReader localExpatReader = new ExpatReader();
            localExpatReader.setContentHandler(paramContentHandler);
            localExpatReader.parse(new InputSource(new StringReader(paramString)));
            return;
        }
        catch (IOException localIOException)
        {
            throw new AssertionError(localIOException);
        }
    }

    public static enum Encoding
    {
        final String expatName;

        static
        {
            UTF_16 = new Encoding("UTF_16", 2, "UTF-16");
            ISO_8859_1 = new Encoding("ISO_8859_1", 3, "ISO-8859-1");
            Encoding[] arrayOfEncoding = new Encoding[4];
            arrayOfEncoding[0] = US_ASCII;
            arrayOfEncoding[1] = UTF_8;
            arrayOfEncoding[2] = UTF_16;
            arrayOfEncoding[3] = ISO_8859_1;
        }

        private Encoding(String paramString)
        {
            this.expatName = paramString;
        }
    }

    static class XmlSerializerFactory
    {
        static final String TYPE = "org.kxml2.io.KXmlParser,org.kxml2.io.KXmlSerializer";
        static final XmlPullParserFactory instance;

        static
        {
            try
            {
                instance = XmlPullParserFactory.newInstance("org.kxml2.io.KXmlParser,org.kxml2.io.KXmlSerializer", null);
                return;
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                throw new AssertionError(localXmlPullParserException);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Xml
 * JD-Core Version:        0.6.2
 */