package android.util;

public class AndroidRuntimeException extends RuntimeException
{
    public AndroidRuntimeException()
    {
    }

    public AndroidRuntimeException(Exception paramException)
    {
        super(paramException);
    }

    public AndroidRuntimeException(String paramString)
    {
        super(paramString);
    }

    public AndroidRuntimeException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.AndroidRuntimeException
 * JD-Core Version:        0.6.2
 */