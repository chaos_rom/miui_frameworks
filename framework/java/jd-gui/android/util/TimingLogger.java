package android.util;

import android.os.SystemClock;
import java.util.ArrayList;

public class TimingLogger
{
    private boolean mDisabled;
    private String mLabel;
    ArrayList<String> mSplitLabels;
    ArrayList<Long> mSplits;
    private String mTag;

    public TimingLogger(String paramString1, String paramString2)
    {
        reset(paramString1, paramString2);
    }

    public void addSplit(String paramString)
    {
        if (this.mDisabled);
        while (true)
        {
            return;
            long l = SystemClock.elapsedRealtime();
            this.mSplits.add(Long.valueOf(l));
            this.mSplitLabels.add(paramString);
        }
    }

    public void dumpToLog()
    {
        if (this.mDisabled);
        while (true)
        {
            return;
            Log.d(this.mTag, this.mLabel + ": begin");
            long l1 = ((Long)this.mSplits.get(0)).longValue();
            long l2 = l1;
            for (int i = 1; i < this.mSplits.size(); i++)
            {
                l2 = ((Long)this.mSplits.get(i)).longValue();
                String str = (String)this.mSplitLabels.get(i);
                long l3 = ((Long)this.mSplits.get(i - 1)).longValue();
                Log.d(this.mTag, this.mLabel + ":            " + (l2 - l3) + " ms, " + str);
            }
            Log.d(this.mTag, this.mLabel + ": end, " + (l2 - l1) + " ms");
        }
    }

    public void reset()
    {
        if (!Log.isLoggable(this.mTag, 2));
        for (boolean bool = true; ; bool = false)
        {
            this.mDisabled = bool;
            if (!this.mDisabled)
                break;
            return;
        }
        if (this.mSplits == null)
        {
            this.mSplits = new ArrayList();
            this.mSplitLabels = new ArrayList();
        }
        while (true)
        {
            addSplit(null);
            break;
            this.mSplits.clear();
            this.mSplitLabels.clear();
        }
    }

    public void reset(String paramString1, String paramString2)
    {
        this.mTag = paramString1;
        this.mLabel = paramString2;
        reset();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.TimingLogger
 * JD-Core Version:        0.6.2
 */