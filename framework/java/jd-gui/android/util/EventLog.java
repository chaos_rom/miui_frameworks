package android.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Collection;
import java.util.HashMap;

public class EventLog
{
    private static final String COMMENT_PATTERN = "^\\s*(#.*)?$";
    private static final String TAG = "EventLog";
    private static final String TAGS_FILE = "/system/etc/event-log-tags";
    private static final String TAG_PATTERN = "^\\s*(\\d+)\\s+(\\w+)\\s*(\\(.*\\))?\\s*$";
    private static HashMap<String, Integer> sTagCodes = null;
    private static HashMap<Integer, String> sTagNames = null;

    public static int getTagCode(String paramString)
    {
        readTagsFile();
        Integer localInteger = (Integer)sTagCodes.get(paramString);
        if (localInteger != null);
        for (int i = localInteger.intValue(); ; i = -1)
            return i;
    }

    public static String getTagName(int paramInt)
    {
        readTagsFile();
        return (String)sTagNames.get(Integer.valueOf(paramInt));
    }

    public static native void readEvents(int[] paramArrayOfInt, Collection<Event> paramCollection)
        throws IOException;

    /** @deprecated */
    // ERROR //
    private static void readTagsFile()
    {
        // Byte code:
        //     0: ldc 2
        //     2: monitorenter
        //     3: getstatic 29	android/util/EventLog:sTagCodes	Ljava/util/HashMap;
        //     6: ifnull +17 -> 23
        //     9: getstatic 31	android/util/EventLog:sTagNames	Ljava/util/HashMap;
        //     12: astore 20
        //     14: aload 20
        //     16: ifnull +7 -> 23
        //     19: ldc 2
        //     21: monitorexit
        //     22: return
        //     23: new 41	java/util/HashMap
        //     26: dup
        //     27: invokespecial 66	java/util/HashMap:<init>	()V
        //     30: putstatic 29	android/util/EventLog:sTagCodes	Ljava/util/HashMap;
        //     33: new 41	java/util/HashMap
        //     36: dup
        //     37: invokespecial 66	java/util/HashMap:<init>	()V
        //     40: putstatic 31	android/util/EventLog:sTagNames	Ljava/util/HashMap;
        //     43: ldc 11
        //     45: invokestatic 72	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
        //     48: astore_1
        //     49: ldc 20
        //     51: invokestatic 72	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
        //     54: astore_2
        //     55: aconst_null
        //     56: astore_3
        //     57: new 74	java/io/BufferedReader
        //     60: dup
        //     61: new 76	java/io/FileReader
        //     64: dup
        //     65: ldc 17
        //     67: invokespecial 79	java/io/FileReader:<init>	(Ljava/lang/String;)V
        //     70: sipush 256
        //     73: invokespecial 82	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
        //     76: astore 4
        //     78: aload 4
        //     80: invokevirtual 86	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     83: astore 10
        //     85: aload 10
        //     87: ifnull +196 -> 283
        //     90: aload_1
        //     91: aload 10
        //     93: invokevirtual 90	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
        //     96: invokevirtual 96	java/util/regex/Matcher:matches	()Z
        //     99: ifne -21 -> 78
        //     102: aload_2
        //     103: aload 10
        //     105: invokevirtual 90	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
        //     108: astore 12
        //     110: aload 12
        //     112: invokevirtual 96	java/util/regex/Matcher:matches	()Z
        //     115: ifne +63 -> 178
        //     118: ldc 14
        //     120: new 98	java/lang/StringBuilder
        //     123: dup
        //     124: invokespecial 99	java/lang/StringBuilder:<init>	()V
        //     127: ldc 101
        //     129: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     132: aload 10
        //     134: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     137: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     140: invokestatic 114	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;)I
        //     143: pop
        //     144: goto -66 -> 78
        //     147: astore 7
        //     149: aload 4
        //     151: astore_3
        //     152: ldc 14
        //     154: ldc 116
        //     156: aload 7
        //     158: invokestatic 119	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     161: pop
        //     162: aload_3
        //     163: ifnull -144 -> 19
        //     166: aload_3
        //     167: invokevirtual 122	java/io/BufferedReader:close	()V
        //     170: goto -151 -> 19
        //     173: astore 9
        //     175: goto -156 -> 19
        //     178: aload 12
        //     180: iconst_1
        //     181: invokevirtual 125	java/util/regex/Matcher:group	(I)Ljava/lang/String;
        //     184: invokestatic 128	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     187: istore 15
        //     189: aload 12
        //     191: iconst_2
        //     192: invokevirtual 125	java/util/regex/Matcher:group	(I)Ljava/lang/String;
        //     195: astore 16
        //     197: getstatic 29	android/util/EventLog:sTagCodes	Ljava/util/HashMap;
        //     200: aload 16
        //     202: iload 15
        //     204: invokestatic 57	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     207: invokevirtual 132	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     210: pop
        //     211: getstatic 31	android/util/EventLog:sTagNames	Ljava/util/HashMap;
        //     214: iload 15
        //     216: invokestatic 57	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     219: aload 16
        //     221: invokevirtual 132	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     224: pop
        //     225: goto -147 -> 78
        //     228: astore 13
        //     230: ldc 14
        //     232: new 98	java/lang/StringBuilder
        //     235: dup
        //     236: invokespecial 99	java/lang/StringBuilder:<init>	()V
        //     239: ldc 134
        //     241: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     244: aload 10
        //     246: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     249: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     252: aload 13
        //     254: invokestatic 119	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     257: pop
        //     258: goto -180 -> 78
        //     261: astore 5
        //     263: aload 4
        //     265: astore_3
        //     266: aload_3
        //     267: ifnull +7 -> 274
        //     270: aload_3
        //     271: invokevirtual 122	java/io/BufferedReader:close	()V
        //     274: aload 5
        //     276: athrow
        //     277: astore_0
        //     278: ldc 2
        //     280: monitorexit
        //     281: aload_0
        //     282: athrow
        //     283: aload 4
        //     285: ifnull +8 -> 293
        //     288: aload 4
        //     290: invokevirtual 122	java/io/BufferedReader:close	()V
        //     293: goto -274 -> 19
        //     296: astore 11
        //     298: goto -279 -> 19
        //     301: astore 6
        //     303: goto -29 -> 274
        //     306: astore 5
        //     308: goto -42 -> 266
        //     311: astore 7
        //     313: goto -161 -> 152
        //
        // Exception table:
        //     from	to	target	type
        //     78	144	147	java/io/IOException
        //     178	225	147	java/io/IOException
        //     230	258	147	java/io/IOException
        //     166	170	173	java/io/IOException
        //     178	225	228	java/lang/NumberFormatException
        //     78	144	261	finally
        //     178	225	261	finally
        //     230	258	261	finally
        //     3	14	277	finally
        //     23	55	277	finally
        //     166	170	277	finally
        //     270	274	277	finally
        //     274	277	277	finally
        //     288	293	277	finally
        //     288	293	296	java/io/IOException
        //     270	274	301	java/io/IOException
        //     57	78	306	finally
        //     152	162	306	finally
        //     57	78	311	java/io/IOException
    }

    public static native int writeEvent(int paramInt1, int paramInt2);

    public static native int writeEvent(int paramInt, long paramLong);

    public static native int writeEvent(int paramInt, String paramString);

    public static native int writeEvent(int paramInt, Object[] paramArrayOfObject);

    public static final class Event
    {
        private static final int DATA_START = 24;
        private static final byte INT_TYPE = 0;
        private static final int LENGTH_OFFSET = 0;
        private static final byte LIST_TYPE = 3;
        private static final byte LONG_TYPE = 1;
        private static final int NANOSECONDS_OFFSET = 16;
        private static final int PAYLOAD_START = 20;
        private static final int PROCESS_OFFSET = 4;
        private static final int SECONDS_OFFSET = 12;
        private static final byte STRING_TYPE = 2;
        private static final int TAG_OFFSET = 20;
        private static final int THREAD_OFFSET = 8;
        private final ByteBuffer mBuffer;

        Event(byte[] paramArrayOfByte)
        {
            this.mBuffer = ByteBuffer.wrap(paramArrayOfByte);
            this.mBuffer.order(ByteOrder.nativeOrder());
        }

        private Object decodeObject()
        {
            int i = this.mBuffer.get();
            Object localObject;
            switch (i)
            {
            default:
                throw new IllegalArgumentException("Unknown entry type: " + i);
            case 0:
                localObject = Integer.valueOf(this.mBuffer.getInt());
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return localObject;
                localObject = Long.valueOf(this.mBuffer.getLong());
                continue;
                try
                {
                    int m = this.mBuffer.getInt();
                    int n = this.mBuffer.position();
                    this.mBuffer.position(n + m);
                    localObject = new String(this.mBuffer.array(), n, m, "UTF-8");
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException)
                {
                    Log.wtf("EventLog", "UTF-8 is not supported", localUnsupportedEncodingException);
                    localObject = null;
                }
                continue;
                int j = this.mBuffer.get();
                if (j < 0)
                    j += 256;
                localObject = new Object[j];
                int k = 0;
                while (k < j)
                {
                    localObject[k] = decodeObject();
                    k += 1;
                }
            }
        }

        /** @deprecated */
        // ERROR //
        public Object getData()
        {
            // Byte code:
            //     0: aconst_null
            //     1: astore_1
            //     2: aload_0
            //     3: monitorenter
            //     4: aload_0
            //     5: getfield 46	android/util/EventLog$Event:mBuffer	Ljava/nio/ByteBuffer;
            //     8: bipush 20
            //     10: aload_0
            //     11: getfield 46	android/util/EventLog$Event:mBuffer	Ljava/nio/ByteBuffer;
            //     14: iconst_0
            //     15: invokevirtual 140	java/nio/ByteBuffer:getShort	(I)S
            //     18: iadd
            //     19: invokevirtual 143	java/nio/ByteBuffer:limit	(I)Ljava/nio/Buffer;
            //     22: pop
            //     23: aload_0
            //     24: getfield 46	android/util/EventLog$Event:mBuffer	Ljava/nio/ByteBuffer;
            //     27: bipush 24
            //     29: invokevirtual 110	java/nio/ByteBuffer:position	(I)Ljava/nio/Buffer;
            //     32: pop
            //     33: aload_0
            //     34: invokespecial 133	android/util/EventLog$Event:decodeObject	()Ljava/lang/Object;
            //     37: astore 9
            //     39: aload 9
            //     41: astore_1
            //     42: aload_0
            //     43: monitorexit
            //     44: aload_1
            //     45: areturn
            //     46: astore 5
            //     48: ldc 123
            //     50: new 68	java/lang/StringBuilder
            //     53: dup
            //     54: invokespecial 69	java/lang/StringBuilder:<init>	()V
            //     57: ldc 145
            //     59: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     62: aload_0
            //     63: invokevirtual 148	android/util/EventLog$Event:getTag	()I
            //     66: invokevirtual 78	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     69: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     72: aload 5
            //     74: invokestatic 131	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     77: pop
            //     78: goto -36 -> 42
            //     81: astore 4
            //     83: aload_0
            //     84: monitorexit
            //     85: aload 4
            //     87: athrow
            //     88: astore_2
            //     89: ldc 123
            //     91: new 68	java/lang/StringBuilder
            //     94: dup
            //     95: invokespecial 69	java/lang/StringBuilder:<init>	()V
            //     98: ldc 150
            //     100: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     103: aload_0
            //     104: invokevirtual 148	android/util/EventLog$Event:getTag	()I
            //     107: invokevirtual 78	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     110: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     113: aload_2
            //     114: invokestatic 131	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     117: pop
            //     118: goto -76 -> 42
            //
            // Exception table:
            //     from	to	target	type
            //     4	39	46	java/lang/IllegalArgumentException
            //     4	39	81	finally
            //     48	78	81	finally
            //     89	118	81	finally
            //     4	39	88	java/nio/BufferUnderflowException
        }

        public int getProcessId()
        {
            return this.mBuffer.getInt(4);
        }

        public int getTag()
        {
            return this.mBuffer.getInt(20);
        }

        public int getThreadId()
        {
            return this.mBuffer.getInt(8);
        }

        public long getTimeNanos()
        {
            return 1000000000L * this.mBuffer.getInt(12) + this.mBuffer.getInt(16);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.EventLog
 * JD-Core Version:        0.6.2
 */