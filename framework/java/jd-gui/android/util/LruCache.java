package android.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<K, V>
{
    private int createCount;
    private int evictionCount;
    private int hitCount;
    private final LinkedHashMap<K, V> map;
    private int maxSize;
    private int missCount;
    private int putCount;
    private int size;

    public LruCache(int paramInt)
    {
        if (paramInt <= 0)
            throw new IllegalArgumentException("maxSize <= 0");
        this.maxSize = paramInt;
        this.map = new LinkedHashMap(0, 0.75F, true);
    }

    private int safeSizeOf(K paramK, V paramV)
    {
        int i = sizeOf(paramK, paramV);
        if (i < 0)
            throw new IllegalStateException("Negative size: " + paramK + "=" + paramV);
        return i;
    }

    // ERROR //
    private void trimToSize(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 67	android/util/LruCache:size	I
        //     6: iflt +20 -> 26
        //     9: aload_0
        //     10: getfield 38	android/util/LruCache:map	Ljava/util/LinkedHashMap;
        //     13: invokevirtual 71	java/util/LinkedHashMap:isEmpty	()Z
        //     16: ifeq +48 -> 64
        //     19: aload_0
        //     20: getfield 67	android/util/LruCache:size	I
        //     23: ifeq +41 -> 64
        //     26: new 45	java/lang/IllegalStateException
        //     29: dup
        //     30: new 47	java/lang/StringBuilder
        //     33: dup
        //     34: invokespecial 48	java/lang/StringBuilder:<init>	()V
        //     37: aload_0
        //     38: invokevirtual 75	java/lang/Object:getClass	()Ljava/lang/Class;
        //     41: invokevirtual 80	java/lang/Class:getName	()Ljava/lang/String;
        //     44: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     47: ldc 82
        //     49: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     52: invokevirtual 63	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     55: invokespecial 64	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     58: athrow
        //     59: astore_2
        //     60: aload_0
        //     61: monitorexit
        //     62: aload_2
        //     63: athrow
        //     64: aload_0
        //     65: getfield 67	android/util/LruCache:size	I
        //     68: iload_1
        //     69: if_icmpgt +8 -> 77
        //     72: aload_0
        //     73: monitorexit
        //     74: goto +88 -> 162
        //     77: aload_0
        //     78: getfield 38	android/util/LruCache:map	Ljava/util/LinkedHashMap;
        //     81: invokevirtual 86	java/util/LinkedHashMap:eldest	()Ljava/util/Map$Entry;
        //     84: astore_3
        //     85: aload_3
        //     86: ifnonnull +8 -> 94
        //     89: aload_0
        //     90: monitorexit
        //     91: goto +71 -> 162
        //     94: aload_3
        //     95: invokeinterface 92 1 0
        //     100: astore 4
        //     102: aload_3
        //     103: invokeinterface 95 1 0
        //     108: astore 5
        //     110: aload_0
        //     111: getfield 38	android/util/LruCache:map	Ljava/util/LinkedHashMap;
        //     114: aload 4
        //     116: invokevirtual 99	java/util/LinkedHashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     119: pop
        //     120: aload_0
        //     121: aload_0
        //     122: getfield 67	android/util/LruCache:size	I
        //     125: aload_0
        //     126: aload 4
        //     128: aload 5
        //     130: invokespecial 101	android/util/LruCache:safeSizeOf	(Ljava/lang/Object;Ljava/lang/Object;)I
        //     133: isub
        //     134: putfield 67	android/util/LruCache:size	I
        //     137: aload_0
        //     138: iconst_1
        //     139: aload_0
        //     140: getfield 103	android/util/LruCache:evictionCount	I
        //     143: iadd
        //     144: putfield 103	android/util/LruCache:evictionCount	I
        //     147: aload_0
        //     148: monitorexit
        //     149: aload_0
        //     150: iconst_1
        //     151: aload 4
        //     153: aload 5
        //     155: aconst_null
        //     156: invokevirtual 107	android/util/LruCache:entryRemoved	(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
        //     159: goto -159 -> 0
        //     162: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	62	59	finally
        //     64	149	59	finally
    }

    protected V create(K paramK)
    {
        return null;
    }

    /** @deprecated */
    public final int createCount()
    {
        try
        {
            int i = this.createCount;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void entryRemoved(boolean paramBoolean, K paramK, V paramV1, V paramV2)
    {
    }

    public final void evictAll()
    {
        trimToSize(-1);
    }

    /** @deprecated */
    public final int evictionCount()
    {
        try
        {
            int i = this.evictionCount;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final V get(K paramK)
    {
        if (paramK == null)
            throw new NullPointerException("key == null");
        Object localObject3;
        try
        {
            Object localObject2 = this.map.get(paramK);
            if (localObject2 != null)
            {
                this.hitCount = (1 + this.hitCount);
                localObject3 = localObject2;
            }
            else
            {
                this.missCount = (1 + this.missCount);
                localObject3 = create(paramK);
                if (localObject3 == null)
                    localObject3 = null;
            }
        }
        finally
        {
        }
        try
        {
            this.createCount = (1 + this.createCount);
            Object localObject5 = this.map.put(paramK, localObject3);
            if (localObject5 != null)
                this.map.put(paramK, localObject5);
            while (true)
            {
                if (localObject5 == null)
                    break;
                entryRemoved(false, paramK, localObject3, localObject5);
                localObject3 = localObject5;
                break label180;
                this.size += safeSizeOf(paramK, localObject3);
            }
        }
        finally
        {
        }
        trimToSize(this.maxSize);
        label180: return localObject3;
    }

    /** @deprecated */
    public final int hitCount()
    {
        try
        {
            int i = this.hitCount;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public final int maxSize()
    {
        try
        {
            int i = this.maxSize;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public final int missCount()
    {
        try
        {
            int i = this.missCount;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final V put(K paramK, V paramV)
    {
        if ((paramK == null) || (paramV == null))
            throw new NullPointerException("key == null || value == null");
        try
        {
            this.putCount = (1 + this.putCount);
            this.size += safeSizeOf(paramK, paramV);
            Object localObject2 = this.map.put(paramK, paramV);
            if (localObject2 != null)
                this.size -= safeSizeOf(paramK, localObject2);
            if (localObject2 != null)
                entryRemoved(false, paramK, localObject2, paramV);
            trimToSize(this.maxSize);
            return localObject2;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public final int putCount()
    {
        try
        {
            int i = this.putCount;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public final V remove(K paramK)
    {
        if (paramK == null)
            throw new NullPointerException("key == null");
        try
        {
            Object localObject2 = this.map.remove(paramK);
            if (localObject2 != null)
                this.size -= safeSizeOf(paramK, localObject2);
            if (localObject2 != null)
                entryRemoved(false, paramK, localObject2, null);
            return localObject2;
        }
        finally
        {
        }
    }

    public void resize(int paramInt)
    {
        if (paramInt <= 0)
            throw new IllegalArgumentException("maxSize <= 0");
        try
        {
            this.maxSize = paramInt;
            trimToSize(paramInt);
            return;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public final int size()
    {
        try
        {
            int i = this.size;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected int sizeOf(K paramK, V paramV)
    {
        return 1;
    }

    /** @deprecated */
    public final Map<K, V> snapshot()
    {
        try
        {
            LinkedHashMap localLinkedHashMap = new LinkedHashMap(this.map);
            return localLinkedHashMap;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public final String toString()
    {
        int i = 0;
        try
        {
            int j = this.hitCount + this.missCount;
            if (j != 0)
                i = 100 * this.hitCount / j;
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = Integer.valueOf(this.maxSize);
            arrayOfObject[1] = Integer.valueOf(this.hitCount);
            arrayOfObject[2] = Integer.valueOf(this.missCount);
            arrayOfObject[3] = Integer.valueOf(i);
            String str = String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", arrayOfObject);
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.LruCache
 * JD-Core Version:        0.6.2
 */