package android.util;

public class Pair<F, S>
{
    public final F first;
    public final S second;

    public Pair(F paramF, S paramS)
    {
        this.first = paramF;
        this.second = paramS;
    }

    public static <A, B> Pair<A, B> create(A paramA, B paramB)
    {
        return new Pair(paramA, paramB);
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof Pair))
                bool = false;
            else
                try
                {
                    Pair localPair = (Pair)paramObject;
                    if ((!this.first.equals(localPair.first)) || (!this.second.equals(localPair.second)))
                        bool = false;
                }
                catch (ClassCastException localClassCastException)
                {
                    bool = false;
                }
        }
    }

    public int hashCode()
    {
        return 31 * (527 + this.first.hashCode()) + this.second.hashCode();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Pair
 * JD-Core Version:        0.6.2
 */