package android.util;

public class AndroidException extends Exception
{
    public AndroidException()
    {
    }

    public AndroidException(Exception paramException)
    {
        super(paramException);
    }

    public AndroidException(String paramString)
    {
        super(paramString);
    }

    public AndroidException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.AndroidException
 * JD-Core Version:        0.6.2
 */