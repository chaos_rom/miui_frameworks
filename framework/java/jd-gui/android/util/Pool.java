package android.util;

public abstract interface Pool<T extends Poolable<T>>
{
    public abstract T acquire();

    public abstract void release(T paramT);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Pool
 * JD-Core Version:        0.6.2
 */