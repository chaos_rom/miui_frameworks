package android.util;

import java.util.Calendar;

public class MonthDisplayHelper
{
    private Calendar mCalendar;
    private int mNumDaysInMonth;
    private int mNumDaysInPrevMonth;
    private int mOffset;
    private final int mWeekStartDay;

    public MonthDisplayHelper(int paramInt1, int paramInt2)
    {
        this(paramInt1, paramInt2, 1);
    }

    public MonthDisplayHelper(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((paramInt3 < 1) || (paramInt3 > 7))
            throw new IllegalArgumentException();
        this.mWeekStartDay = paramInt3;
        this.mCalendar = Calendar.getInstance();
        this.mCalendar.set(1, paramInt1);
        this.mCalendar.set(2, paramInt2);
        this.mCalendar.set(5, 1);
        this.mCalendar.set(11, 0);
        this.mCalendar.set(12, 0);
        this.mCalendar.set(13, 0);
        this.mCalendar.getTimeInMillis();
        recalculate();
    }

    private void recalculate()
    {
        this.mNumDaysInMonth = this.mCalendar.getActualMaximum(5);
        this.mCalendar.add(2, -1);
        this.mNumDaysInPrevMonth = this.mCalendar.getActualMaximum(5);
        this.mCalendar.add(2, 1);
        int i = getFirstDayOfMonth() - this.mWeekStartDay;
        if (i < 0)
            i += 7;
        this.mOffset = i;
    }

    public int getColumnOf(int paramInt)
    {
        return (-1 + (paramInt + this.mOffset)) % 7;
    }

    public int getDayAt(int paramInt1, int paramInt2)
    {
        int i;
        if ((paramInt1 == 0) && (paramInt2 < this.mOffset))
            i = 1 + (paramInt2 + this.mNumDaysInPrevMonth - this.mOffset);
        while (true)
        {
            return i;
            i = 1 + (paramInt2 + paramInt1 * 7 - this.mOffset);
            if (i > this.mNumDaysInMonth)
                i -= this.mNumDaysInMonth;
        }
    }

    public int[] getDigitsForRow(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 5))
            throw new IllegalArgumentException("row " + paramInt + " out of range (0-5)");
        int[] arrayOfInt = new int[7];
        for (int i = 0; i < 7; i++)
            arrayOfInt[i] = getDayAt(paramInt, i);
        return arrayOfInt;
    }

    public int getFirstDayOfMonth()
    {
        return this.mCalendar.get(7);
    }

    public int getMonth()
    {
        return this.mCalendar.get(2);
    }

    public int getNumberOfDaysInMonth()
    {
        return this.mNumDaysInMonth;
    }

    public int getOffset()
    {
        return this.mOffset;
    }

    public int getRowOf(int paramInt)
    {
        return (-1 + (paramInt + this.mOffset)) / 7;
    }

    public int getWeekStartDay()
    {
        return this.mWeekStartDay;
    }

    public int getYear()
    {
        return this.mCalendar.get(1);
    }

    public boolean isWithinCurrentMonth(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 > 5) || (paramInt2 > 6));
        while (true)
        {
            return bool;
            if (((paramInt1 != 0) || (paramInt2 >= this.mOffset)) && (1 + (paramInt2 + paramInt1 * 7 - this.mOffset) <= this.mNumDaysInMonth))
                bool = true;
        }
    }

    public void nextMonth()
    {
        this.mCalendar.add(2, 1);
        recalculate();
    }

    public void previousMonth()
    {
        this.mCalendar.add(2, -1);
        recalculate();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.MonthDisplayHelper
 * JD-Core Version:        0.6.2
 */