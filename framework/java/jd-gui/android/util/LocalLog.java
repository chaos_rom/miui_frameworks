package android.util;

import android.text.format.Time;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public final class LocalLog
{
    private LinkedList<String> mLog = new LinkedList();
    private int mMaxLines;
    private Time mNow;

    public LocalLog(int paramInt)
    {
        this.mMaxLines = paramInt;
        this.mNow = new Time();
    }

    /** @deprecated */
    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        try
        {
            ListIterator localListIterator = this.mLog.listIterator(0);
            if (localListIterator.hasNext())
                paramPrintWriter.println((String)localListIterator.next());
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void log(String paramString)
    {
        label85: 
        try
        {
            if (this.mMaxLines > 0)
            {
                this.mNow.setToNow();
                this.mLog.add(this.mNow.format("%H:%M:%S") + " - " + paramString);
                if (this.mLog.size() <= this.mMaxLines)
                    break label85;
                this.mLog.remove();
            }
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.LocalLog
 * JD-Core Version:        0.6.2
 */