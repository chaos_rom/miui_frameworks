package android.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ReflectiveProperty<T, V> extends Property<T, V>
{
    private static final String PREFIX_GET = "get";
    private static final String PREFIX_IS = "is";
    private static final String PREFIX_SET = "set";
    private Field mField;
    private Method mGetter;
    private Method mSetter;

    public ReflectiveProperty(Class<T> paramClass, Class<V> paramClass1, String paramString)
    {
        super(paramClass1, paramString);
        char c = Character.toUpperCase(paramString.charAt(0));
        String str1 = paramString.substring(1);
        String str2 = c + str1;
        String str3 = "get" + str2;
        Class localClass2;
        String str5;
        try
        {
            this.mGetter = paramClass.getMethod(str3, (Class[])null);
            localClass2 = this.mGetter.getReturnType();
            if (!typesMatch(paramClass1, localClass2))
                throw new NoSuchPropertyException("Underlying type (" + localClass2 + ") " + "does not match Property type (" + paramClass1 + ")");
        }
        catch (NoSuchMethodException localNoSuchMethodException1)
        {
            while (true)
            {
                String str4 = "is" + str2;
                try
                {
                    this.mGetter = paramClass.getMethod(str4, (Class[])null);
                }
                catch (NoSuchMethodException localNoSuchMethodException2)
                {
                    try
                    {
                        this.mField = paramClass.getField(paramString);
                        Class localClass1 = this.mField.getType();
                        if (typesMatch(paramClass1, localClass1))
                            break label340;
                        throw new NoSuchPropertyException("Underlying type (" + localClass1 + ") " + "does not match Property type (" + paramClass1 + ")");
                    }
                    catch (NoSuchFieldException localNoSuchFieldException)
                    {
                        throw new NoSuchPropertyException("No accessor method or field found for property with name " + paramString);
                    }
                }
            }
            str5 = "set" + str2;
        }
        try
        {
            Class[] arrayOfClass = new Class[1];
            arrayOfClass[0] = localClass2;
            this.mSetter = paramClass.getMethod(str5, arrayOfClass);
            label340: return;
        }
        catch (NoSuchMethodException localNoSuchMethodException3)
        {
            break label340;
        }
    }

    private boolean typesMatch(Class<V> paramClass, Class paramClass1)
    {
        boolean bool = false;
        if (paramClass1 != paramClass)
            if ((!paramClass1.isPrimitive()) || (((paramClass1 != Float.TYPE) || (paramClass != Float.class)) && ((paramClass1 != Integer.TYPE) || (paramClass != Integer.class)) && ((paramClass1 != Boolean.TYPE) || (paramClass != Boolean.class)) && ((paramClass1 != Long.TYPE) || (paramClass != Long.class)) && ((paramClass1 != Double.TYPE) || (paramClass != Double.class)) && ((paramClass1 != Short.TYPE) || (paramClass != Short.class)) && ((paramClass1 != Byte.TYPE) || (paramClass != Byte.class)) && ((paramClass1 != Character.TYPE) || (paramClass != Character.class))));
        for (bool = true; ; bool = true)
            return bool;
    }

    public V get(T paramT)
    {
        if (this.mGetter != null);
        while (true)
        {
            Object localObject2;
            try
            {
                Object localObject3 = this.mGetter.invoke(paramT, (Object[])null);
                localObject2 = localObject3;
                return localObject2;
            }
            catch (IllegalAccessException localIllegalAccessException2)
            {
                throw new AssertionError();
            }
            catch (InvocationTargetException localInvocationTargetException)
            {
                throw new RuntimeException(localInvocationTargetException.getCause());
            }
            if (this.mField != null)
                try
                {
                    Object localObject1 = this.mField.get(paramT);
                    localObject2 = localObject1;
                }
                catch (IllegalAccessException localIllegalAccessException1)
                {
                    throw new AssertionError();
                }
        }
        throw new AssertionError();
    }

    public boolean isReadOnly()
    {
        if ((this.mSetter == null) && (this.mField == null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void set(T paramT, V paramV)
    {
        if (this.mSetter != null);
        while (true)
        {
            try
            {
                Method localMethod = this.mSetter;
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = paramV;
                localMethod.invoke(paramT, arrayOfObject);
                return;
            }
            catch (IllegalAccessException localIllegalAccessException2)
            {
                throw new AssertionError();
            }
            catch (InvocationTargetException localInvocationTargetException)
            {
                throw new RuntimeException(localInvocationTargetException.getCause());
            }
            if (this.mField != null)
                try
                {
                    this.mField.set(paramT, paramV);
                }
                catch (IllegalAccessException localIllegalAccessException1)
                {
                    throw new AssertionError();
                }
        }
        throw new UnsupportedOperationException("Property " + getName() + " is read-only");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.ReflectiveProperty
 * JD-Core Version:        0.6.2
 */