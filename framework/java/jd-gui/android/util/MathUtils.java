package android.util;

import java.util.Random;

public final class MathUtils
{
    private static final float DEG_TO_RAD = 0.01745329F;
    private static final float RAD_TO_DEG = 57.295784F;
    private static final Random sRandom = new Random();

    public static float abs(float paramFloat)
    {
        if (paramFloat > 0.0F);
        while (true)
        {
            return paramFloat;
            paramFloat = -paramFloat;
        }
    }

    public static float acos(float paramFloat)
    {
        return (float)Math.acos(paramFloat);
    }

    public static float asin(float paramFloat)
    {
        return (float)Math.asin(paramFloat);
    }

    public static float atan(float paramFloat)
    {
        return (float)Math.atan(paramFloat);
    }

    public static float atan2(float paramFloat1, float paramFloat2)
    {
        return (float)Math.atan2(paramFloat1, paramFloat2);
    }

    public static float constrain(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        if (paramFloat1 < paramFloat2);
        while (true)
        {
            return paramFloat2;
            if (paramFloat1 > paramFloat3)
                paramFloat2 = paramFloat3;
            else
                paramFloat2 = paramFloat1;
        }
    }

    public static int constrain(int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt1 < paramInt2);
        while (true)
        {
            return paramInt2;
            if (paramInt1 > paramInt3)
                paramInt2 = paramInt3;
            else
                paramInt2 = paramInt1;
        }
    }

    public static long constrain(long paramLong1, long paramLong2, long paramLong3)
    {
        if (paramLong1 < paramLong2);
        while (true)
        {
            return paramLong2;
            if (paramLong1 > paramLong3)
                paramLong2 = paramLong3;
            else
                paramLong2 = paramLong1;
        }
    }

    public static float degrees(float paramFloat)
    {
        return 57.295784F * paramFloat;
    }

    public static float dist(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        float f1 = paramFloat3 - paramFloat1;
        float f2 = paramFloat4 - paramFloat2;
        return (float)Math.sqrt(f1 * f1 + f2 * f2);
    }

    public static float dist(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
    {
        float f1 = paramFloat4 - paramFloat1;
        float f2 = paramFloat5 - paramFloat2;
        float f3 = paramFloat6 - paramFloat3;
        return (float)Math.sqrt(f1 * f1 + f2 * f2 + f3 * f3);
    }

    public static float exp(float paramFloat)
    {
        return (float)Math.exp(paramFloat);
    }

    public static float lerp(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        return paramFloat1 + paramFloat3 * (paramFloat2 - paramFloat1);
    }

    public static float log(float paramFloat)
    {
        return (float)Math.log(paramFloat);
    }

    public static float mag(float paramFloat1, float paramFloat2)
    {
        return (float)Math.sqrt(paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2);
    }

    public static float mag(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        return (float)Math.sqrt(paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2 + paramFloat3 * paramFloat3);
    }

    public static float map(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5)
    {
        return paramFloat3 + (paramFloat3 - paramFloat4) * ((paramFloat5 - paramFloat1) / (paramFloat2 - paramFloat1));
    }

    public static float max(float paramFloat1, float paramFloat2)
    {
        if (paramFloat1 > paramFloat2);
        while (true)
        {
            return paramFloat1;
            paramFloat1 = paramFloat2;
        }
    }

    public static float max(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        if (paramFloat1 > paramFloat2)
            if (paramFloat1 > paramFloat3)
                paramFloat3 = paramFloat1;
        while (true)
        {
            return paramFloat3;
            if (paramFloat2 > paramFloat3)
                paramFloat3 = paramFloat2;
        }
    }

    public static float max(int paramInt1, int paramInt2)
    {
        if (paramInt1 > paramInt2);
        for (float f = paramInt1; ; f = paramInt2)
            return f;
    }

    public static float max(int paramInt1, int paramInt2, int paramInt3)
    {
        float f;
        if (paramInt1 > paramInt2)
        {
            if (paramInt1 > paramInt3);
            while (true)
            {
                f = paramInt1;
                return f;
                paramInt1 = paramInt3;
            }
        }
        if (paramInt2 > paramInt3);
        while (true)
        {
            f = paramInt2;
            break;
            paramInt2 = paramInt3;
        }
    }

    public static float min(float paramFloat1, float paramFloat2)
    {
        if (paramFloat1 < paramFloat2);
        while (true)
        {
            return paramFloat1;
            paramFloat1 = paramFloat2;
        }
    }

    public static float min(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        if (paramFloat1 < paramFloat2)
            if (paramFloat1 < paramFloat3)
                paramFloat3 = paramFloat1;
        while (true)
        {
            return paramFloat3;
            if (paramFloat2 < paramFloat3)
                paramFloat3 = paramFloat2;
        }
    }

    public static float min(int paramInt1, int paramInt2)
    {
        if (paramInt1 < paramInt2);
        for (float f = paramInt1; ; f = paramInt2)
            return f;
    }

    public static float min(int paramInt1, int paramInt2, int paramInt3)
    {
        float f;
        if (paramInt1 < paramInt2)
        {
            if (paramInt1 < paramInt3);
            while (true)
            {
                f = paramInt1;
                return f;
                paramInt1 = paramInt3;
            }
        }
        if (paramInt2 < paramInt3);
        while (true)
        {
            f = paramInt2;
            break;
            paramInt2 = paramInt3;
        }
    }

    public static float norm(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        return (paramFloat3 - paramFloat1) / (paramFloat2 - paramFloat1);
    }

    public static float pow(float paramFloat1, float paramFloat2)
    {
        return (float)Math.pow(paramFloat1, paramFloat2);
    }

    public static float radians(float paramFloat)
    {
        return 0.01745329F * paramFloat;
    }

    public static float random(float paramFloat)
    {
        return paramFloat * sRandom.nextFloat();
    }

    public static float random(float paramFloat1, float paramFloat2)
    {
        if (paramFloat1 >= paramFloat2);
        while (true)
        {
            return paramFloat1;
            paramFloat1 += sRandom.nextFloat() * (paramFloat2 - paramFloat1);
        }
    }

    public static int random(int paramInt)
    {
        return (int)(sRandom.nextFloat() * paramInt);
    }

    public static int random(int paramInt1, int paramInt2)
    {
        if (paramInt1 >= paramInt2);
        while (true)
        {
            return paramInt1;
            paramInt1 = (int)(sRandom.nextFloat() * (paramInt2 - paramInt1) + paramInt1);
        }
    }

    public static void randomSeed(long paramLong)
    {
        sRandom.setSeed(paramLong);
    }

    public static float sq(float paramFloat)
    {
        return paramFloat * paramFloat;
    }

    public static float tan(float paramFloat)
    {
        return (float)Math.tan(paramFloat);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.MathUtils
 * JD-Core Version:        0.6.2
 */