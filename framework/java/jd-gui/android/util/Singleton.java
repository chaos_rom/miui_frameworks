package android.util;

public abstract class Singleton<T>
{
    private T mInstance;

    protected abstract T create();

    public final T get()
    {
        try
        {
            if (this.mInstance == null)
                this.mInstance = create();
            Object localObject2 = this.mInstance;
            return localObject2;
        }
        finally
        {
            localObject1 = finally;
            throw localObject1;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Singleton
 * JD-Core Version:        0.6.2
 */