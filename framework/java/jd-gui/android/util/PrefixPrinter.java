package android.util;

public class PrefixPrinter
    implements Printer
{
    private final String mPrefix;
    private final Printer mPrinter;

    private PrefixPrinter(Printer paramPrinter, String paramString)
    {
        this.mPrinter = paramPrinter;
        this.mPrefix = paramString;
    }

    public static Printer create(Printer paramPrinter, String paramString)
    {
        if ((paramString == null) || (paramString.equals("")));
        while (true)
        {
            return paramPrinter;
            paramPrinter = new PrefixPrinter(paramPrinter, paramString);
        }
    }

    public void println(String paramString)
    {
        this.mPrinter.println(this.mPrefix + paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.PrefixPrinter
 * JD-Core Version:        0.6.2
 */