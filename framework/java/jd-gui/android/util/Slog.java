package android.util;

public final class Slog
{
    public static int d(String paramString1, String paramString2)
    {
        return Log.println_native(3, 3, paramString1, paramString2);
    }

    public static int d(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return Log.println_native(3, 3, paramString1, paramString2 + '\n' + Log.getStackTraceString(paramThrowable));
    }

    public static int e(String paramString1, String paramString2)
    {
        return Log.println_native(3, 6, paramString1, paramString2);
    }

    public static int e(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return Log.println_native(3, 6, paramString1, paramString2 + '\n' + Log.getStackTraceString(paramThrowable));
    }

    public static int i(String paramString1, String paramString2)
    {
        return Log.println_native(3, 4, paramString1, paramString2);
    }

    public static int i(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return Log.println_native(3, 4, paramString1, paramString2 + '\n' + Log.getStackTraceString(paramThrowable));
    }

    public static int println(int paramInt, String paramString1, String paramString2)
    {
        return Log.println_native(3, paramInt, paramString1, paramString2);
    }

    public static int v(String paramString1, String paramString2)
    {
        return Log.println_native(3, 2, paramString1, paramString2);
    }

    public static int v(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return Log.println_native(3, 2, paramString1, paramString2 + '\n' + Log.getStackTraceString(paramThrowable));
    }

    public static int w(String paramString1, String paramString2)
    {
        return Log.println_native(3, 5, paramString1, paramString2);
    }

    public static int w(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return Log.println_native(3, 5, paramString1, paramString2 + '\n' + Log.getStackTraceString(paramThrowable));
    }

    public static int w(String paramString, Throwable paramThrowable)
    {
        return Log.println_native(3, 5, paramString, Log.getStackTraceString(paramThrowable));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Slog
 * JD-Core Version:        0.6.2
 */