package android.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Base64InputStream extends FilterInputStream
{
    private static final int BUFFER_SIZE = 2048;
    private static byte[] EMPTY = new byte[0];
    private final Base64.Coder coder;
    private boolean eof = false;
    private byte[] inputBuffer = new byte[2048];
    private int outputEnd;
    private int outputStart;

    public Base64InputStream(InputStream paramInputStream, int paramInt)
    {
        this(paramInputStream, paramInt, false);
    }

    public Base64InputStream(InputStream paramInputStream, int paramInt, boolean paramBoolean)
    {
        super(paramInputStream);
        if (paramBoolean);
        for (this.coder = new Base64.Encoder(paramInt, null); ; this.coder = new Base64.Decoder(paramInt, null))
        {
            this.coder.output = new byte[this.coder.maxOutputSize(2048)];
            this.outputStart = 0;
            this.outputEnd = 0;
            return;
        }
    }

    private void refill()
        throws IOException
    {
        if (this.eof);
        while (true)
        {
            return;
            int i = this.in.read(this.inputBuffer);
            if (i == -1)
                this.eof = true;
            for (boolean bool = this.coder.process(EMPTY, 0, 0, true); !bool; bool = this.coder.process(this.inputBuffer, 0, i, false))
                throw new Base64DataException("bad base-64");
            this.outputEnd = this.coder.op;
            this.outputStart = 0;
        }
    }

    public int available()
    {
        return this.outputEnd - this.outputStart;
    }

    public void close()
        throws IOException
    {
        this.in.close();
        this.inputBuffer = null;
    }

    public void mark(int paramInt)
    {
        throw new UnsupportedOperationException();
    }

    public boolean markSupported()
    {
        return false;
    }

    public int read()
        throws IOException
    {
        if (this.outputStart >= this.outputEnd)
            refill();
        if (this.outputStart >= this.outputEnd);
        byte[] arrayOfByte;
        int i;
        for (int j = -1; ; j = 0xFF & arrayOfByte[i])
        {
            return j;
            arrayOfByte = this.coder.output;
            i = this.outputStart;
            this.outputStart = (i + 1);
        }
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        if (this.outputStart >= this.outputEnd)
            refill();
        int i;
        if (this.outputStart >= this.outputEnd)
            i = -1;
        while (true)
        {
            return i;
            i = Math.min(paramInt2, this.outputEnd - this.outputStart);
            System.arraycopy(this.coder.output, this.outputStart, paramArrayOfByte, paramInt1, i);
            this.outputStart = (i + this.outputStart);
        }
    }

    public void reset()
    {
        throw new UnsupportedOperationException();
    }

    public long skip(long paramLong)
        throws IOException
    {
        if (this.outputStart >= this.outputEnd)
            refill();
        long l;
        if (this.outputStart >= this.outputEnd)
            l = 0L;
        while (true)
        {
            return l;
            l = Math.min(paramLong, this.outputEnd - this.outputStart);
            this.outputStart = ((int)(l + this.outputStart));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Base64InputStream
 * JD-Core Version:        0.6.2
 */