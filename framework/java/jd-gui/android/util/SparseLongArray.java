package android.util;

import com.android.internal.util.ArrayUtils;

public class SparseLongArray
    implements Cloneable
{
    private int[] mKeys;
    private int mSize;
    private long[] mValues;

    public SparseLongArray()
    {
        this(10);
    }

    public SparseLongArray(int paramInt)
    {
        int i = ArrayUtils.idealLongArraySize(paramInt);
        this.mKeys = new int[i];
        this.mValues = new long[i];
        this.mSize = 0;
    }

    private static int binarySearch(int[] paramArrayOfInt, int paramInt1, int paramInt2, long paramLong)
    {
        int i = paramInt1 + paramInt2;
        int j = paramInt1 - 1;
        while (i - j > 1)
        {
            int k = (i + j) / 2;
            if (paramArrayOfInt[k] < paramLong)
                j = k;
            else
                i = k;
        }
        if (i == paramInt1 + paramInt2)
            i = 0xFFFFFFFF ^ paramInt1 + paramInt2;
        while (true)
        {
            return i;
            if (paramArrayOfInt[i] != paramLong)
                i ^= -1;
        }
    }

    private void growKeyAndValueArrays(int paramInt)
    {
        int i = ArrayUtils.idealLongArraySize(paramInt);
        int[] arrayOfInt = new int[i];
        long[] arrayOfLong = new long[i];
        System.arraycopy(this.mKeys, 0, arrayOfInt, 0, this.mKeys.length);
        System.arraycopy(this.mValues, 0, arrayOfLong, 0, this.mValues.length);
        this.mKeys = arrayOfInt;
        this.mValues = arrayOfLong;
    }

    public void append(int paramInt, long paramLong)
    {
        if ((this.mSize != 0) && (paramInt <= this.mKeys[(-1 + this.mSize)]))
            put(paramInt, paramLong);
        while (true)
        {
            return;
            int i = this.mSize;
            if (i >= this.mKeys.length)
                growKeyAndValueArrays(i + 1);
            this.mKeys[i] = paramInt;
            this.mValues[i] = paramLong;
            this.mSize = (i + 1);
        }
    }

    public void clear()
    {
        this.mSize = 0;
    }

    public SparseLongArray clone()
    {
        SparseLongArray localSparseLongArray = null;
        try
        {
            localSparseLongArray = (SparseLongArray)super.clone();
            localSparseLongArray.mKeys = ((int[])this.mKeys.clone());
            localSparseLongArray.mValues = ((long[])this.mValues.clone());
            label38: return localSparseLongArray;
        }
        catch (CloneNotSupportedException localCloneNotSupportedException)
        {
            break label38;
        }
    }

    public void delete(int paramInt)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt);
        if (i >= 0)
            removeAt(i);
    }

    public long get(int paramInt)
    {
        return get(paramInt, 0L);
    }

    public long get(int paramInt, long paramLong)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt);
        if (i < 0);
        while (true)
        {
            return paramLong;
            paramLong = this.mValues[i];
        }
    }

    public int indexOfKey(int paramInt)
    {
        return binarySearch(this.mKeys, 0, this.mSize, paramInt);
    }

    public int indexOfValue(long paramLong)
    {
        int i = 0;
        if (i < this.mSize)
            if (this.mValues[i] != paramLong);
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    public int keyAt(int paramInt)
    {
        return this.mKeys[paramInt];
    }

    public void put(int paramInt, long paramLong)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt);
        if (i >= 0)
            this.mValues[i] = paramLong;
        while (true)
        {
            return;
            int j = i ^ 0xFFFFFFFF;
            if (this.mSize >= this.mKeys.length)
                growKeyAndValueArrays(1 + this.mSize);
            if (this.mSize - j != 0)
            {
                System.arraycopy(this.mKeys, j, this.mKeys, j + 1, this.mSize - j);
                System.arraycopy(this.mValues, j, this.mValues, j + 1, this.mSize - j);
            }
            this.mKeys[j] = paramInt;
            this.mValues[j] = paramLong;
            this.mSize = (1 + this.mSize);
        }
    }

    public void removeAt(int paramInt)
    {
        System.arraycopy(this.mKeys, paramInt + 1, this.mKeys, paramInt, this.mSize - (paramInt + 1));
        System.arraycopy(this.mValues, paramInt + 1, this.mValues, paramInt, this.mSize - (paramInt + 1));
        this.mSize = (-1 + this.mSize);
    }

    public int size()
    {
        return this.mSize;
    }

    public long valueAt(int paramInt)
    {
        return this.mValues[paramInt];
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.SparseLongArray
 * JD-Core Version:        0.6.2
 */