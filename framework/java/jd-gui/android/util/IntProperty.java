package android.util;

public abstract class IntProperty<T> extends Property<T, Integer>
{
    public IntProperty(String paramString)
    {
        super(Integer.class, paramString);
    }

    public final void set(T paramT, Integer paramInteger)
    {
        set(paramT, Integer.valueOf(paramInteger.intValue()));
    }

    public abstract void setValue(T paramT, int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.IntProperty
 * JD-Core Version:        0.6.2
 */