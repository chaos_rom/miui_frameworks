package android.util;

public abstract interface Poolable<T>
{
    public abstract T getNextPoolable();

    public abstract boolean isPooled();

    public abstract void setNextPoolable(T paramT);

    public abstract void setPooled(boolean paramBoolean);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Poolable
 * JD-Core Version:        0.6.2
 */