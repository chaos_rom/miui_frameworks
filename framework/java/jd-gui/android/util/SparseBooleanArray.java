package android.util;

import com.android.internal.util.ArrayUtils;

public class SparseBooleanArray
    implements Cloneable
{
    private int[] mKeys;
    private int mSize;
    private boolean[] mValues;

    public SparseBooleanArray()
    {
        this(10);
    }

    public SparseBooleanArray(int paramInt)
    {
        int i = ArrayUtils.idealIntArraySize(paramInt);
        this.mKeys = new int[i];
        this.mValues = new boolean[i];
        this.mSize = 0;
    }

    private static int binarySearch(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramInt1 + paramInt2;
        int j = paramInt1 - 1;
        while (i - j > 1)
        {
            int k = (i + j) / 2;
            if (paramArrayOfInt[k] < paramInt3)
                j = k;
            else
                i = k;
        }
        if (i == paramInt1 + paramInt2)
            i = 0xFFFFFFFF ^ paramInt1 + paramInt2;
        while (true)
        {
            return i;
            if (paramArrayOfInt[i] != paramInt3)
                i ^= -1;
        }
    }

    public void append(int paramInt, boolean paramBoolean)
    {
        if ((this.mSize != 0) && (paramInt <= this.mKeys[(-1 + this.mSize)]))
            put(paramInt, paramBoolean);
        while (true)
        {
            return;
            int i = this.mSize;
            if (i >= this.mKeys.length)
            {
                int j = ArrayUtils.idealIntArraySize(i + 1);
                int[] arrayOfInt = new int[j];
                boolean[] arrayOfBoolean = new boolean[j];
                System.arraycopy(this.mKeys, 0, arrayOfInt, 0, this.mKeys.length);
                System.arraycopy(this.mValues, 0, arrayOfBoolean, 0, this.mValues.length);
                this.mKeys = arrayOfInt;
                this.mValues = arrayOfBoolean;
            }
            this.mKeys[i] = paramInt;
            this.mValues[i] = paramBoolean;
            this.mSize = (i + 1);
        }
    }

    public void clear()
    {
        this.mSize = 0;
    }

    public SparseBooleanArray clone()
    {
        SparseBooleanArray localSparseBooleanArray = null;
        try
        {
            localSparseBooleanArray = (SparseBooleanArray)super.clone();
            localSparseBooleanArray.mKeys = ((int[])this.mKeys.clone());
            localSparseBooleanArray.mValues = ((boolean[])this.mValues.clone());
            label38: return localSparseBooleanArray;
        }
        catch (CloneNotSupportedException localCloneNotSupportedException)
        {
            break label38;
        }
    }

    public void delete(int paramInt)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt);
        if (i >= 0)
        {
            System.arraycopy(this.mKeys, i + 1, this.mKeys, i, this.mSize - (i + 1));
            System.arraycopy(this.mValues, i + 1, this.mValues, i, this.mSize - (i + 1));
            this.mSize = (-1 + this.mSize);
        }
    }

    public boolean get(int paramInt)
    {
        return get(paramInt, false);
    }

    public boolean get(int paramInt, boolean paramBoolean)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt);
        if (i < 0);
        while (true)
        {
            return paramBoolean;
            paramBoolean = this.mValues[i];
        }
    }

    public int indexOfKey(int paramInt)
    {
        return binarySearch(this.mKeys, 0, this.mSize, paramInt);
    }

    public int indexOfValue(boolean paramBoolean)
    {
        int i = 0;
        if (i < this.mSize)
            if (this.mValues[i] != paramBoolean);
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    public int keyAt(int paramInt)
    {
        return this.mKeys[paramInt];
    }

    public void put(int paramInt, boolean paramBoolean)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt);
        if (i >= 0)
            this.mValues[i] = paramBoolean;
        while (true)
        {
            return;
            int j = i ^ 0xFFFFFFFF;
            if (this.mSize >= this.mKeys.length)
            {
                int k = ArrayUtils.idealIntArraySize(1 + this.mSize);
                int[] arrayOfInt = new int[k];
                boolean[] arrayOfBoolean = new boolean[k];
                System.arraycopy(this.mKeys, 0, arrayOfInt, 0, this.mKeys.length);
                System.arraycopy(this.mValues, 0, arrayOfBoolean, 0, this.mValues.length);
                this.mKeys = arrayOfInt;
                this.mValues = arrayOfBoolean;
            }
            if (this.mSize - j != 0)
            {
                System.arraycopy(this.mKeys, j, this.mKeys, j + 1, this.mSize - j);
                System.arraycopy(this.mValues, j, this.mValues, j + 1, this.mSize - j);
            }
            this.mKeys[j] = paramInt;
            this.mValues[j] = paramBoolean;
            this.mSize = (1 + this.mSize);
        }
    }

    public int size()
    {
        return this.mSize;
    }

    public boolean valueAt(int paramInt)
    {
        return this.mValues[paramInt];
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.SparseBooleanArray
 * JD-Core Version:        0.6.2
 */