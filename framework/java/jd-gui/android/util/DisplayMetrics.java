package android.util;

import android.os.SystemProperties;

public class DisplayMetrics
{
    public static final int DENSITY_DEFAULT = 160;
    public static final int DENSITY_DEVICE = 0;
    public static final int DENSITY_HIGH = 240;
    public static final int DENSITY_LOW = 120;
    public static final int DENSITY_MEDIUM = 160;
    public static final int DENSITY_TV = 213;
    public static final int DENSITY_XHIGH = 320;
    public static final int DENSITY_XXHIGH = 480;
    public float density;
    public int densityDpi;
    public int heightPixels;
    public float noncompatDensity;
    public int noncompatHeightPixels;
    public float noncompatScaledDensity;
    public int noncompatWidthPixels;
    public float noncompatXdpi;
    public float noncompatYdpi;
    public float scaledDensity;
    public int widthPixels;
    public float xdpi;
    public float ydpi;

    private static int getDeviceDensity()
    {
        return SystemProperties.getInt("qemu.sf.lcd_density", SystemProperties.getInt("ro.sf.lcd_density", 160));
    }

    public void setTo(DisplayMetrics paramDisplayMetrics)
    {
        this.widthPixels = paramDisplayMetrics.widthPixels;
        this.heightPixels = paramDisplayMetrics.heightPixels;
        this.density = paramDisplayMetrics.density;
        this.densityDpi = paramDisplayMetrics.densityDpi;
        this.scaledDensity = paramDisplayMetrics.scaledDensity;
        this.xdpi = paramDisplayMetrics.xdpi;
        this.ydpi = paramDisplayMetrics.ydpi;
        this.noncompatWidthPixels = paramDisplayMetrics.noncompatWidthPixels;
        this.noncompatHeightPixels = paramDisplayMetrics.noncompatHeightPixels;
        this.noncompatDensity = paramDisplayMetrics.noncompatDensity;
        this.noncompatScaledDensity = paramDisplayMetrics.noncompatScaledDensity;
        this.noncompatXdpi = paramDisplayMetrics.noncompatXdpi;
        this.noncompatYdpi = paramDisplayMetrics.noncompatYdpi;
    }

    public void setToDefaults()
    {
        this.widthPixels = 0;
        this.heightPixels = 0;
        this.density = (DENSITY_DEVICE / 160.0F);
        this.densityDpi = DENSITY_DEVICE;
        this.scaledDensity = this.density;
        this.xdpi = DENSITY_DEVICE;
        this.ydpi = DENSITY_DEVICE;
        this.noncompatWidthPixels = 0;
        this.noncompatHeightPixels = 0;
    }

    public String toString()
    {
        return "DisplayMetrics{density=" + this.density + ", width=" + this.widthPixels + ", height=" + this.heightPixels + ", scaledDensity=" + this.scaledDensity + ", xdpi=" + this.xdpi + ", ydpi=" + this.ydpi + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.DisplayMetrics
 * JD-Core Version:        0.6.2
 */