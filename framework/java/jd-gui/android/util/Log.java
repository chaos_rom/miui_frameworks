package android.util;

import com.android.internal.os.RuntimeInit;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;

public final class Log
{
    public static final int ASSERT = 7;
    public static final int DEBUG = 3;
    public static final int ERROR = 6;
    public static final int INFO = 4;
    public static final int LOG_ID_EVENTS = 2;
    public static final int LOG_ID_MAIN = 0;
    public static final int LOG_ID_RADIO = 1;
    public static final int LOG_ID_SYSTEM = 3;
    public static final int VERBOSE = 2;
    public static final int WARN = 5;
    private static TerribleFailureHandler sWtfHandler = new TerribleFailureHandler()
    {
        public void onTerribleFailure(String paramAnonymousString, Log.TerribleFailure paramAnonymousTerribleFailure)
        {
            RuntimeInit.wtf(paramAnonymousString, paramAnonymousTerribleFailure);
        }
    };

    public static int d(String paramString1, String paramString2)
    {
        return println_native(0, 3, paramString1, paramString2);
    }

    public static int d(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return println_native(0, 3, paramString1, paramString2 + '\n' + getStackTraceString(paramThrowable));
    }

    public static int e(String paramString1, String paramString2)
    {
        return println_native(0, 6, paramString1, paramString2);
    }

    public static int e(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return println_native(0, 6, paramString1, paramString2 + '\n' + getStackTraceString(paramThrowable));
    }

    public static String getStackTraceString(Throwable paramThrowable)
    {
        String str;
        if (paramThrowable == null)
            str = "";
        while (true)
        {
            return str;
            for (Throwable localThrowable = paramThrowable; ; localThrowable = localThrowable.getCause())
            {
                if (localThrowable == null)
                    break label36;
                if ((localThrowable instanceof UnknownHostException))
                {
                    str = "";
                    break;
                }
            }
            label36: StringWriter localStringWriter = new StringWriter();
            paramThrowable.printStackTrace(new PrintWriter(localStringWriter));
            str = localStringWriter.toString();
        }
    }

    public static int i(String paramString1, String paramString2)
    {
        return println_native(0, 4, paramString1, paramString2);
    }

    public static int i(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return println_native(0, 4, paramString1, paramString2 + '\n' + getStackTraceString(paramThrowable));
    }

    public static native boolean isLoggable(String paramString, int paramInt);

    public static int println(int paramInt, String paramString1, String paramString2)
    {
        return println_native(0, paramInt, paramString1, paramString2);
    }

    public static native int println_native(int paramInt1, int paramInt2, String paramString1, String paramString2);

    public static TerribleFailureHandler setWtfHandler(TerribleFailureHandler paramTerribleFailureHandler)
    {
        if (paramTerribleFailureHandler == null)
            throw new NullPointerException("handler == null");
        TerribleFailureHandler localTerribleFailureHandler = sWtfHandler;
        sWtfHandler = paramTerribleFailureHandler;
        return localTerribleFailureHandler;
    }

    public static int v(String paramString1, String paramString2)
    {
        return println_native(0, 2, paramString1, paramString2);
    }

    public static int v(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return println_native(0, 2, paramString1, paramString2 + '\n' + getStackTraceString(paramThrowable));
    }

    public static int w(String paramString1, String paramString2)
    {
        return println_native(0, 5, paramString1, paramString2);
    }

    public static int w(String paramString1, String paramString2, Throwable paramThrowable)
    {
        return println_native(0, 5, paramString1, paramString2 + '\n' + getStackTraceString(paramThrowable));
    }

    public static int w(String paramString, Throwable paramThrowable)
    {
        return println_native(0, 5, paramString, getStackTraceString(paramThrowable));
    }

    public static int wtf(String paramString1, String paramString2)
    {
        return wtf(paramString1, paramString2, null);
    }

    public static int wtf(String paramString1, String paramString2, Throwable paramThrowable)
    {
        TerribleFailure localTerribleFailure = new TerribleFailure(paramString2, paramThrowable);
        int i = println_native(0, 7, paramString1, paramString2 + '\n' + getStackTraceString(paramThrowable));
        sWtfHandler.onTerribleFailure(paramString1, localTerribleFailure);
        return i;
    }

    public static int wtf(String paramString, Throwable paramThrowable)
    {
        return wtf(paramString, paramThrowable.getMessage(), paramThrowable);
    }

    public static abstract interface TerribleFailureHandler
    {
        public abstract void onTerribleFailure(String paramString, Log.TerribleFailure paramTerribleFailure);
    }

    private static class TerribleFailure extends Exception
    {
        TerribleFailure(String paramString, Throwable paramThrowable)
        {
            super(paramThrowable);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Log
 * JD-Core Version:        0.6.2
 */