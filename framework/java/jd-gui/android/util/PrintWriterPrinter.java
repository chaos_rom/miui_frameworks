package android.util;

import java.io.PrintWriter;

public class PrintWriterPrinter
    implements Printer
{
    private final PrintWriter mPW;

    public PrintWriterPrinter(PrintWriter paramPrintWriter)
    {
        this.mPW = paramPrintWriter;
    }

    public void println(String paramString)
    {
        this.mPW.println(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.PrintWriterPrinter
 * JD-Core Version:        0.6.2
 */