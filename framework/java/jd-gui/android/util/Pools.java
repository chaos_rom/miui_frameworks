package android.util;

public class Pools
{
    public static <T extends Poolable<T>> Pool<T> finitePool(PoolableManager<T> paramPoolableManager, int paramInt)
    {
        return new FinitePool(paramPoolableManager, paramInt);
    }

    public static <T extends Poolable<T>> Pool<T> simplePool(PoolableManager<T> paramPoolableManager)
    {
        return new FinitePool(paramPoolableManager);
    }

    public static <T extends Poolable<T>> Pool<T> synchronizedPool(Pool<T> paramPool)
    {
        return new SynchronizedPool(paramPool);
    }

    public static <T extends Poolable<T>> Pool<T> synchronizedPool(Pool<T> paramPool, Object paramObject)
    {
        return new SynchronizedPool(paramPool, paramObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.Pools
 * JD-Core Version:        0.6.2
 */