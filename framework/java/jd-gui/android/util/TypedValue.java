package android.util;

import java.io.PrintStream;

public class TypedValue
{
    public static final int COMPLEX_MANTISSA_MASK = 16777215;
    public static final int COMPLEX_MANTISSA_SHIFT = 8;
    public static final int COMPLEX_RADIX_0p23 = 3;
    public static final int COMPLEX_RADIX_16p7 = 1;
    public static final int COMPLEX_RADIX_23p0 = 0;
    public static final int COMPLEX_RADIX_8p15 = 2;
    public static final int COMPLEX_RADIX_MASK = 3;
    public static final int COMPLEX_RADIX_SHIFT = 4;
    public static final int COMPLEX_UNIT_DIP = 1;
    public static final int COMPLEX_UNIT_FRACTION = 0;
    public static final int COMPLEX_UNIT_FRACTION_PARENT = 1;
    public static final int COMPLEX_UNIT_IN = 4;
    public static final int COMPLEX_UNIT_MASK = 15;
    public static final int COMPLEX_UNIT_MM = 5;
    public static final int COMPLEX_UNIT_PT = 3;
    public static final int COMPLEX_UNIT_PX = 0;
    public static final int COMPLEX_UNIT_SHIFT = 0;
    public static final int COMPLEX_UNIT_SP = 2;
    public static final int DENSITY_DEFAULT = 0;
    public static final int DENSITY_NONE = 65535;
    private static final String[] DIMENSION_UNIT_STRS;
    private static final String[] FRACTION_UNIT_STRS = arrayOfString2;
    private static final float MANTISSA_MULT = 0.0039063F;
    private static final float[] RADIX_MULTS;
    public static final int TYPE_ATTRIBUTE = 2;
    public static final int TYPE_DIMENSION = 5;
    public static final int TYPE_FIRST_COLOR_INT = 28;
    public static final int TYPE_FIRST_INT = 16;
    public static final int TYPE_FLOAT = 4;
    public static final int TYPE_FRACTION = 6;
    public static final int TYPE_INT_BOOLEAN = 18;
    public static final int TYPE_INT_COLOR_ARGB4 = 30;
    public static final int TYPE_INT_COLOR_ARGB8 = 28;
    public static final int TYPE_INT_COLOR_RGB4 = 31;
    public static final int TYPE_INT_COLOR_RGB8 = 29;
    public static final int TYPE_INT_DEC = 16;
    public static final int TYPE_INT_HEX = 17;
    public static final int TYPE_LAST_COLOR_INT = 31;
    public static final int TYPE_LAST_INT = 31;
    public static final int TYPE_NULL = 0;
    public static final int TYPE_REFERENCE = 1;
    public static final int TYPE_STRING = 3;
    public int assetCookie;
    public int changingConfigurations = -1;
    public int data;
    public int density;
    public int resourceId;
    public CharSequence string;
    public int type;

    static
    {
        float[] arrayOfFloat = new float[4];
        arrayOfFloat[0] = 0.0039063F;
        arrayOfFloat[1] = 3.051758E-05F;
        arrayOfFloat[2] = 1.192093E-07F;
        arrayOfFloat[3] = 4.656613E-10F;
        RADIX_MULTS = arrayOfFloat;
        String[] arrayOfString1 = new String[6];
        arrayOfString1[0] = "px";
        arrayOfString1[1] = "dip";
        arrayOfString1[2] = "sp";
        arrayOfString1[3] = "pt";
        arrayOfString1[4] = "in";
        arrayOfString1[5] = "mm";
        DIMENSION_UNIT_STRS = arrayOfString1;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "%";
        arrayOfString2[1] = "%p";
    }

    public static float applyDimension(int paramInt, float paramFloat, DisplayMetrics paramDisplayMetrics)
    {
        switch (paramInt)
        {
        default:
            paramFloat = 0.0F;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return paramFloat;
            paramFloat *= paramDisplayMetrics.density;
            continue;
            paramFloat *= paramDisplayMetrics.scaledDensity;
            continue;
            paramFloat = 0.01388889F * (paramFloat * paramDisplayMetrics.xdpi);
            continue;
            paramFloat *= paramDisplayMetrics.xdpi;
            continue;
            paramFloat = 0.03937008F * (paramFloat * paramDisplayMetrics.xdpi);
        }
    }

    public static final String coerceToString(int paramInt1, int paramInt2)
    {
        String str = null;
        switch (paramInt1)
        {
        case 3:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        default:
            if ((paramInt1 >= 28) && (paramInt1 <= 31))
                str = "#" + Integer.toHexString(paramInt2);
            break;
        case 0:
        case 1:
        case 2:
        case 4:
        case 5:
        case 6:
        case 17:
        case 18:
        }
        while (true)
        {
            return str;
            str = "@" + paramInt2;
            continue;
            str = "?" + paramInt2;
            continue;
            str = Float.toString(Float.intBitsToFloat(paramInt2));
            continue;
            str = Float.toString(complexToFloat(paramInt2)) + DIMENSION_UNIT_STRS[(0xF & paramInt2 >> 0)];
            continue;
            str = Float.toString(100.0F * complexToFloat(paramInt2)) + FRACTION_UNIT_STRS[(0xF & paramInt2 >> 0)];
            continue;
            str = "0x" + Integer.toHexString(paramInt2);
            continue;
            if (paramInt2 != 0)
            {
                str = "true";
            }
            else
            {
                str = "false";
                continue;
                if ((paramInt1 >= 16) && (paramInt1 <= 31))
                    str = Integer.toString(paramInt2);
            }
        }
    }

    public static float complexToDimension(int paramInt, DisplayMetrics paramDisplayMetrics)
    {
        return applyDimension(0xF & paramInt >> 0, complexToFloat(paramInt), paramDisplayMetrics);
    }

    public static float complexToDimensionNoisy(int paramInt, DisplayMetrics paramDisplayMetrics)
    {
        float f = complexToDimension(paramInt, paramDisplayMetrics);
        System.out.println("Dimension (0x" + (0xFFFFFF & paramInt >> 8) + "*" + RADIX_MULTS[(0x3 & paramInt >> 4)] / 0.0039063F + ")" + DIMENSION_UNIT_STRS[(0xF & paramInt >> 0)] + " = " + f);
        return f;
    }

    public static int complexToDimensionPixelOffset(int paramInt, DisplayMetrics paramDisplayMetrics)
    {
        return (int)applyDimension(0xF & paramInt >> 0, complexToFloat(paramInt), paramDisplayMetrics);
    }

    public static int complexToDimensionPixelSize(int paramInt, DisplayMetrics paramDisplayMetrics)
    {
        float f = complexToFloat(paramInt);
        int i = (int)(0.5F + applyDimension(0xF & paramInt >> 0, f, paramDisplayMetrics));
        if (i != 0);
        while (true)
        {
            return i;
            if (f == 0.0F)
                i = 0;
            else if (f > 0.0F)
                i = 1;
            else
                i = -1;
        }
    }

    public static float complexToFloat(int paramInt)
    {
        return (paramInt & 0xFFFFFF00) * RADIX_MULTS[(0x3 & paramInt >> 4)];
    }

    public static float complexToFraction(int paramInt, float paramFloat1, float paramFloat2)
    {
        float f;
        switch (0xF & paramInt >> 0)
        {
        default:
            f = 0.0F;
        case 0:
        case 1:
        }
        while (true)
        {
            return f;
            f = paramFloat1 * complexToFloat(paramInt);
            continue;
            f = paramFloat2 * complexToFloat(paramInt);
        }
    }

    public final CharSequence coerceToString()
    {
        int i = this.type;
        if (i == 3);
        for (Object localObject = this.string; ; localObject = coerceToString(i, this.data))
            return localObject;
    }

    public float getDimension(DisplayMetrics paramDisplayMetrics)
    {
        return complexToDimension(this.data, paramDisplayMetrics);
    }

    public final float getFloat()
    {
        return Float.intBitsToFloat(this.data);
    }

    public float getFraction(float paramFloat1, float paramFloat2)
    {
        return complexToFraction(this.data, paramFloat1, paramFloat2);
    }

    public void setTo(TypedValue paramTypedValue)
    {
        this.type = paramTypedValue.type;
        this.string = paramTypedValue.string;
        this.data = paramTypedValue.data;
        this.assetCookie = paramTypedValue.assetCookie;
        this.resourceId = paramTypedValue.resourceId;
        this.density = paramTypedValue.density;
    }

    public String toString()
    {
        StringBuilder localStringBuilder1 = new StringBuilder();
        localStringBuilder1.append("TypedValue{t=0x").append(Integer.toHexString(this.type));
        localStringBuilder1.append("/d=0x").append(Integer.toHexString(this.data));
        StringBuilder localStringBuilder2;
        if (this.type == 3)
        {
            localStringBuilder2 = localStringBuilder1.append(" \"");
            if (this.string == null)
                break label141;
        }
        label141: for (Object localObject = this.string; ; localObject = "<null>")
        {
            localStringBuilder2.append((CharSequence)localObject).append("\"");
            if (this.assetCookie != 0)
                localStringBuilder1.append(" a=").append(this.assetCookie);
            if (this.resourceId != 0)
                localStringBuilder1.append(" r=0x").append(Integer.toHexString(this.resourceId));
            localStringBuilder1.append("}");
            return localStringBuilder1.toString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.TypedValue
 * JD-Core Version:        0.6.2
 */