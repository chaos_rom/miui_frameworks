package android.util;

public abstract class FloatProperty<T> extends Property<T, Float>
{
    public FloatProperty(String paramString)
    {
        super(Float.class, paramString);
    }

    public final void set(T paramT, Float paramFloat)
    {
        setValue(paramT, paramFloat.floatValue());
    }

    public abstract void setValue(T paramT, float paramFloat);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.FloatProperty
 * JD-Core Version:        0.6.2
 */