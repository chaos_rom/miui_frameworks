package android.util;

class FinitePool<T extends Poolable<T>>
    implements Pool<T>
{
    private static final String LOG_TAG = "FinitePool";
    private final boolean mInfinite;
    private final int mLimit;
    private final PoolableManager<T> mManager;
    private int mPoolCount;
    private T mRoot;

    FinitePool(PoolableManager<T> paramPoolableManager)
    {
        this.mManager = paramPoolableManager;
        this.mLimit = 0;
        this.mInfinite = true;
    }

    FinitePool(PoolableManager<T> paramPoolableManager, int paramInt)
    {
        if (paramInt <= 0)
            throw new IllegalArgumentException("The pool limit must be > 0");
        this.mManager = paramPoolableManager;
        this.mLimit = paramInt;
        this.mInfinite = false;
    }

    public T acquire()
    {
        Poolable localPoolable;
        if (this.mRoot != null)
        {
            localPoolable = this.mRoot;
            this.mRoot = ((Poolable)localPoolable.getNextPoolable());
            this.mPoolCount = (-1 + this.mPoolCount);
        }
        while (true)
        {
            if (localPoolable != null)
            {
                localPoolable.setNextPoolable(null);
                localPoolable.setPooled(false);
                this.mManager.onAcquired(localPoolable);
            }
            return localPoolable;
            localPoolable = this.mManager.newInstance();
        }
    }

    public void release(T paramT)
    {
        if (!paramT.isPooled())
        {
            if ((this.mInfinite) || (this.mPoolCount < this.mLimit))
            {
                this.mPoolCount = (1 + this.mPoolCount);
                paramT.setNextPoolable(this.mRoot);
                paramT.setPooled(true);
                this.mRoot = paramT;
            }
            this.mManager.onReleased(paramT);
        }
        while (true)
        {
            return;
            Log.w("FinitePool", "Element is already in pool: " + paramT);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.FinitePool
 * JD-Core Version:        0.6.2
 */