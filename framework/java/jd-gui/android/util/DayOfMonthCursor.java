package android.util;

public class DayOfMonthCursor extends MonthDisplayHelper
{
    private int mColumn;
    private int mRow;

    public DayOfMonthCursor(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super(paramInt1, paramInt2, paramInt4);
        this.mRow = getRowOf(paramInt3);
        this.mColumn = getColumnOf(paramInt3);
    }

    public boolean down()
    {
        boolean bool = false;
        if (isWithinCurrentMonth(1 + this.mRow, this.mColumn))
            this.mRow = (1 + this.mRow);
        while (true)
        {
            return bool;
            nextMonth();
            for (this.mRow = 0; !isWithinCurrentMonth(this.mRow, this.mColumn); this.mRow = (1 + this.mRow));
            bool = true;
        }
    }

    public int getSelectedColumn()
    {
        return this.mColumn;
    }

    public int getSelectedDayOfMonth()
    {
        return getDayAt(this.mRow, this.mColumn);
    }

    public int getSelectedMonthOffset()
    {
        int i;
        if (isWithinCurrentMonth(this.mRow, this.mColumn))
            i = 0;
        while (true)
        {
            return i;
            if (this.mRow == 0)
                i = -1;
            else
                i = 1;
        }
    }

    public int getSelectedRow()
    {
        return this.mRow;
    }

    public boolean isSelected(int paramInt1, int paramInt2)
    {
        if ((this.mRow == paramInt1) && (this.mColumn == paramInt2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean left()
    {
        if (this.mColumn == 0)
        {
            this.mRow = (-1 + this.mRow);
            this.mColumn = 6;
            if (!isWithinCurrentMonth(this.mRow, this.mColumn))
                break label57;
        }
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.mColumn = (-1 + this.mColumn);
            break;
            label57: previousMonth();
            int i = getNumberOfDaysInMonth();
            this.mRow = getRowOf(i);
            this.mColumn = getColumnOf(i);
        }
    }

    public boolean right()
    {
        boolean bool = false;
        if (this.mColumn == 6)
        {
            this.mRow = (1 + this.mRow);
            this.mColumn = 0;
            if (!isWithinCurrentMonth(this.mRow, this.mColumn))
                break label56;
        }
        while (true)
        {
            return bool;
            this.mColumn = (1 + this.mColumn);
            break;
            label56: nextMonth();
            this.mRow = 0;
            for (this.mColumn = 0; !isWithinCurrentMonth(this.mRow, this.mColumn); this.mColumn = (1 + this.mColumn));
            bool = true;
        }
    }

    public void setSelectedDayOfMonth(int paramInt)
    {
        this.mRow = getRowOf(paramInt);
        this.mColumn = getColumnOf(paramInt);
    }

    public void setSelectedRowColumn(int paramInt1, int paramInt2)
    {
        this.mRow = paramInt1;
        this.mColumn = paramInt2;
    }

    public boolean up()
    {
        if (isWithinCurrentMonth(-1 + this.mRow, this.mColumn))
            this.mRow = (-1 + this.mRow);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            previousMonth();
            for (this.mRow = 5; !isWithinCurrentMonth(this.mRow, this.mColumn); this.mRow = (-1 + this.mRow));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.DayOfMonthCursor
 * JD-Core Version:        0.6.2
 */