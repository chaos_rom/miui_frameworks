package android.util;

import com.android.internal.util.ArrayUtils;

public class SparseIntArray
    implements Cloneable
{
    private int[] mKeys;
    private int mSize;
    private int[] mValues;

    public SparseIntArray()
    {
        this(10);
    }

    public SparseIntArray(int paramInt)
    {
        int i = ArrayUtils.idealIntArraySize(paramInt);
        this.mKeys = new int[i];
        this.mValues = new int[i];
        this.mSize = 0;
    }

    private static int binarySearch(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramInt1 + paramInt2;
        int j = paramInt1 - 1;
        while (i - j > 1)
        {
            int k = (i + j) / 2;
            if (paramArrayOfInt[k] < paramInt3)
                j = k;
            else
                i = k;
        }
        if (i == paramInt1 + paramInt2)
            i = 0xFFFFFFFF ^ paramInt1 + paramInt2;
        while (true)
        {
            return i;
            if (paramArrayOfInt[i] != paramInt3)
                i ^= -1;
        }
    }

    public void append(int paramInt1, int paramInt2)
    {
        if ((this.mSize != 0) && (paramInt1 <= this.mKeys[(-1 + this.mSize)]))
            put(paramInt1, paramInt2);
        while (true)
        {
            return;
            int i = this.mSize;
            if (i >= this.mKeys.length)
            {
                int j = ArrayUtils.idealIntArraySize(i + 1);
                int[] arrayOfInt1 = new int[j];
                int[] arrayOfInt2 = new int[j];
                System.arraycopy(this.mKeys, 0, arrayOfInt1, 0, this.mKeys.length);
                System.arraycopy(this.mValues, 0, arrayOfInt2, 0, this.mValues.length);
                this.mKeys = arrayOfInt1;
                this.mValues = arrayOfInt2;
            }
            this.mKeys[i] = paramInt1;
            this.mValues[i] = paramInt2;
            this.mSize = (i + 1);
        }
    }

    public void clear()
    {
        this.mSize = 0;
    }

    public SparseIntArray clone()
    {
        SparseIntArray localSparseIntArray = null;
        try
        {
            localSparseIntArray = (SparseIntArray)super.clone();
            localSparseIntArray.mKeys = ((int[])this.mKeys.clone());
            localSparseIntArray.mValues = ((int[])this.mValues.clone());
            label38: return localSparseIntArray;
        }
        catch (CloneNotSupportedException localCloneNotSupportedException)
        {
            break label38;
        }
    }

    public void delete(int paramInt)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt);
        if (i >= 0)
            removeAt(i);
    }

    public int get(int paramInt)
    {
        return get(paramInt, 0);
    }

    public int get(int paramInt1, int paramInt2)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt1);
        if (i < 0);
        while (true)
        {
            return paramInt2;
            paramInt2 = this.mValues[i];
        }
    }

    public int indexOfKey(int paramInt)
    {
        return binarySearch(this.mKeys, 0, this.mSize, paramInt);
    }

    public int indexOfValue(int paramInt)
    {
        int i = 0;
        if (i < this.mSize)
            if (this.mValues[i] != paramInt);
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    public int keyAt(int paramInt)
    {
        return this.mKeys[paramInt];
    }

    public void put(int paramInt1, int paramInt2)
    {
        int i = binarySearch(this.mKeys, 0, this.mSize, paramInt1);
        if (i >= 0)
            this.mValues[i] = paramInt2;
        while (true)
        {
            return;
            int j = i ^ 0xFFFFFFFF;
            if (this.mSize >= this.mKeys.length)
            {
                int k = ArrayUtils.idealIntArraySize(1 + this.mSize);
                int[] arrayOfInt1 = new int[k];
                int[] arrayOfInt2 = new int[k];
                System.arraycopy(this.mKeys, 0, arrayOfInt1, 0, this.mKeys.length);
                System.arraycopy(this.mValues, 0, arrayOfInt2, 0, this.mValues.length);
                this.mKeys = arrayOfInt1;
                this.mValues = arrayOfInt2;
            }
            if (this.mSize - j != 0)
            {
                System.arraycopy(this.mKeys, j, this.mKeys, j + 1, this.mSize - j);
                System.arraycopy(this.mValues, j, this.mValues, j + 1, this.mSize - j);
            }
            this.mKeys[j] = paramInt1;
            this.mValues[j] = paramInt2;
            this.mSize = (1 + this.mSize);
        }
    }

    public void removeAt(int paramInt)
    {
        System.arraycopy(this.mKeys, paramInt + 1, this.mKeys, paramInt, this.mSize - (paramInt + 1));
        System.arraycopy(this.mValues, paramInt + 1, this.mValues, paramInt, this.mSize - (paramInt + 1));
        this.mSize = (-1 + this.mSize);
    }

    public int size()
    {
        return this.mSize;
    }

    public int valueAt(int paramInt)
    {
        return this.mValues[paramInt];
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.SparseIntArray
 * JD-Core Version:        0.6.2
 */