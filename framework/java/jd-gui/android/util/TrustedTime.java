package android.util;

public abstract interface TrustedTime
{
    public abstract long currentTimeMillis();

    public abstract boolean forceRefresh();

    public abstract long getCacheAge();

    public abstract long getCacheCertainty();

    public abstract boolean hasCache();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.TrustedTime
 * JD-Core Version:        0.6.2
 */