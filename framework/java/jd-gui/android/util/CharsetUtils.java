package android.util;

import android.os.Build;
import android.text.TextUtils;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.HashMap;
import java.util.Map;

public final class CharsetUtils
{
    private static final String VENDOR_DOCOMO = "docomo";
    private static final String VENDOR_KDDI = "kddi";
    private static final String VENDOR_SOFTBANK = "softbank";
    private static final Map<String, String> sVendorShiftJisMap = new HashMap();

    static
    {
        sVendorShiftJisMap.put("docomo", "docomo-shift_jis-2007");
        sVendorShiftJisMap.put("kddi", "kddi-shift_jis-2007");
        sVendorShiftJisMap.put("softbank", "softbank-shift_jis-2007");
    }

    public static Charset charsetForVendor(String paramString)
        throws UnsupportedCharsetException, IllegalCharsetNameException
    {
        return charsetForVendor(paramString, getDefaultVendor());
    }

    public static Charset charsetForVendor(String paramString1, String paramString2)
        throws UnsupportedCharsetException, IllegalCharsetNameException
    {
        return Charset.forName(nameForVendor(paramString1, paramString2));
    }

    private static String getDefaultVendor()
    {
        return Build.BRAND;
    }

    private static boolean isShiftJis(String paramString)
    {
        boolean bool = false;
        if (paramString == null);
        while (true)
        {
            return bool;
            int i = paramString.length();
            if (((i == 4) || (i == 9)) && ((paramString.equalsIgnoreCase("shift_jis")) || (paramString.equalsIgnoreCase("shift-jis")) || (paramString.equalsIgnoreCase("sjis"))))
                bool = true;
        }
    }

    public static String nameForDefaultVendor(String paramString)
    {
        return nameForVendor(paramString, getDefaultVendor());
    }

    public static String nameForVendor(String paramString1, String paramString2)
    {
        String str;
        if ((!TextUtils.isEmpty(paramString1)) && (!TextUtils.isEmpty(paramString2)) && (isShiftJis(paramString1)))
        {
            str = (String)sVendorShiftJisMap.get(paramString2);
            if (str == null);
        }
        while (true)
        {
            return str;
            str = paramString1;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.util.CharsetUtils
 * JD-Core Version:        0.6.2
 */