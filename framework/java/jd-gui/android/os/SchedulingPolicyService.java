package android.os;

public class SchedulingPolicyService extends ISchedulingPolicyService.Stub
{
    private static final int PRIORITY_MAX = 2;
    private static final int PRIORITY_MIN = 1;
    private static final String TAG = "SchedulingPolicyService";

    public int requestPriority(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = -1;
        if ((Binder.getCallingUid() != 1013) || (paramInt3 < 1) || (paramInt3 > 2) || (Process.getThreadGroupLeader(paramInt2) != paramInt1));
        while (true)
        {
            return i;
            try
            {
                if (Binder.getCallingPid() == paramInt1);
                for (int j = 4; ; j = 3)
                {
                    Process.setThreadGroup(paramInt2, j);
                    Process.setThreadScheduler(paramInt2, 1, paramInt3);
                    i = 0;
                    break;
                }
            }
            catch (RuntimeException localRuntimeException)
            {
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.SchedulingPolicyService
 * JD-Core Version:        0.6.2
 */