package android.os;

public abstract interface IHardwareService extends IInterface
{
    public abstract boolean getFlashlightEnabled()
        throws RemoteException;

    public abstract void setFlashlightEnabled(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IHardwareService
    {
        private static final String DESCRIPTOR = "android.os.IHardwareService";
        static final int TRANSACTION_getFlashlightEnabled = 1;
        static final int TRANSACTION_setFlashlightEnabled = 2;

        public Stub()
        {
            attachInterface(this, "android.os.IHardwareService");
        }

        public static IHardwareService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.os.IHardwareService");
                if ((localIInterface != null) && ((localIInterface instanceof IHardwareService)))
                    localObject = (IHardwareService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
                while (true)
                {
                    return j;
                    paramParcel2.writeString("android.os.IHardwareService");
                    continue;
                    paramParcel1.enforceInterface("android.os.IHardwareService");
                    boolean bool2 = getFlashlightEnabled();
                    paramParcel2.writeNoException();
                    if (bool2)
                        i = j;
                    paramParcel2.writeInt(i);
                }
            case 2:
            }
            paramParcel1.enforceInterface("android.os.IHardwareService");
            if (paramParcel1.readInt() != 0);
            for (boolean bool1 = j; ; bool1 = false)
            {
                setFlashlightEnabled(bool1);
                paramParcel2.writeNoException();
                break;
            }
        }

        private static class Proxy
            implements IHardwareService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean getFlashlightEnabled()
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IHardwareService");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        return bool;
                    bool = false;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.os.IHardwareService";
            }

            public void setFlashlightEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IHardwareService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.IHardwareService
 * JD-Core Version:        0.6.2
 */