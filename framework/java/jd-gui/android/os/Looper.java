package android.os;

import android.util.Log;
import android.util.PrefixPrinter;
import android.util.Printer;

public class Looper
{
    private static final String TAG = "Looper";
    private static Looper sMainLooper;
    static final ThreadLocal<Looper> sThreadLocal = new ThreadLocal();
    private Printer mLogging;
    final MessageQueue mQueue;
    volatile boolean mRun;
    final Thread mThread;

    private Looper(boolean paramBoolean)
    {
        this.mQueue = new MessageQueue(paramBoolean);
        this.mRun = true;
        this.mThread = Thread.currentThread();
    }

    public static Looper getMainLooper()
    {
        try
        {
            Looper localLooper = sMainLooper;
            return localLooper;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static void loop()
    {
        Looper localLooper = myLooper();
        if (localLooper == null)
            throw new RuntimeException("No Looper; Looper.prepare() wasn't called on this thread.");
        MessageQueue localMessageQueue = localLooper.mQueue;
        Binder.clearCallingIdentity();
        long l1 = Binder.clearCallingIdentity();
        while (true)
        {
            Message localMessage = localMessageQueue.next();
            if (localMessage == null)
                return;
            Printer localPrinter = localLooper.mLogging;
            if (localPrinter != null)
                localPrinter.println(">>>>> Dispatching to " + localMessage.target + " " + localMessage.callback + ": " + localMessage.what);
            localMessage.target.dispatchMessage(localMessage);
            if (localPrinter != null)
                localPrinter.println("<<<<< Finished to " + localMessage.target + " " + localMessage.callback);
            long l2 = Binder.clearCallingIdentity();
            if (l1 != l2)
                Log.wtf("Looper", "Thread identity changed from 0x" + Long.toHexString(l1) + " to 0x" + Long.toHexString(l2) + " while dispatching to " + localMessage.target.getClass().getName() + " " + localMessage.callback + " what=" + localMessage.what);
            localMessage.recycle();
        }
    }

    public static Looper myLooper()
    {
        return (Looper)sThreadLocal.get();
    }

    public static MessageQueue myQueue()
    {
        return myLooper().mQueue;
    }

    public static void prepare()
    {
        prepare(true);
    }

    private static void prepare(boolean paramBoolean)
    {
        if (sThreadLocal.get() != null)
            throw new RuntimeException("Only one Looper may be created per thread");
        sThreadLocal.set(new Looper(paramBoolean));
    }

    // ERROR //
    public static void prepareMainLooper()
    {
        // Byte code:
        //     0: iconst_0
        //     1: invokestatic 166	android/os/Looper:prepare	(Z)V
        //     4: ldc 2
        //     6: monitorenter
        //     7: getstatic 52	android/os/Looper:sMainLooper	Landroid/os/Looper;
        //     10: ifnull +19 -> 29
        //     13: new 176	java/lang/IllegalStateException
        //     16: dup
        //     17: ldc 178
        //     19: invokespecial 179	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     22: athrow
        //     23: astore_0
        //     24: ldc 2
        //     26: monitorexit
        //     27: aload_0
        //     28: athrow
        //     29: invokestatic 56	android/os/Looper:myLooper	()Landroid/os/Looper;
        //     32: putstatic 52	android/os/Looper:sMainLooper	Landroid/os/Looper;
        //     35: ldc 2
        //     37: monitorexit
        //     38: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	27	23	finally
        //     29	38	23	finally
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        Printer localPrinter = PrefixPrinter.create(paramPrinter, paramString);
        localPrinter.println(toString());
        localPrinter.println("mRun=" + this.mRun);
        localPrinter.println("mThread=" + this.mThread);
        StringBuilder localStringBuilder = new StringBuilder().append("mQueue=");
        Object localObject1;
        if (this.mQueue != null)
            localObject1 = this.mQueue;
        while (true)
        {
            localPrinter.println(localObject1);
            if (this.mQueue != null)
                synchronized (this.mQueue)
                {
                    long l = SystemClock.uptimeMillis();
                    Message localMessage = this.mQueue.mMessages;
                    int i = 0;
                    while (true)
                        if (localMessage != null)
                        {
                            localPrinter.println("    Message " + i + ": " + localMessage.toString(l));
                            i++;
                            localMessage = localMessage.next;
                            continue;
                            localObject1 = "(null";
                            break;
                        }
                    localPrinter.println("(Total messages: " + i + ")");
                }
        }
    }

    public MessageQueue getQueue()
    {
        return this.mQueue;
    }

    public Thread getThread()
    {
        return this.mThread;
    }

    public final int postSyncBarrier()
    {
        return this.mQueue.enqueueSyncBarrier(SystemClock.uptimeMillis());
    }

    public void quit()
    {
        this.mQueue.quit();
    }

    public final void removeSyncBarrier(int paramInt)
    {
        this.mQueue.removeSyncBarrier(paramInt);
    }

    public void setMessageLogging(Printer paramPrinter)
    {
        this.mLogging = paramPrinter;
    }

    public String toString()
    {
        return "Looper{" + Integer.toHexString(System.identityHashCode(this)) + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Looper
 * JD-Core Version:        0.6.2
 */