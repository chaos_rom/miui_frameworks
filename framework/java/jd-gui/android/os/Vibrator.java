package android.os;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;

public abstract class Vibrator
{
    public abstract void cancel();

    public abstract boolean hasVibrator();

    public abstract void vibrate(long paramLong);

    public abstract void vibrate(long[] paramArrayOfLong, int paramInt);

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void vibrateEx(byte[] paramArrayOfByte)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Vibrator
 * JD-Core Version:        0.6.2
 */