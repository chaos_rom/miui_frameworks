package android.os;

public class Build
{
    public static final String BOARD;
    public static final String BOOTLOADER;
    public static final String BRAND;
    public static final String CPU_ABI;
    public static final String CPU_ABI2;
    public static final String DEVICE;
    public static final String DISPLAY;
    public static final String FINGERPRINT;
    public static final String HARDWARE;
    public static final String HOST;
    public static final String ID;
    public static final boolean IS_DEBUGGABLE = false;
    public static final String MANUFACTURER;
    public static final String MODEL;
    public static final String PRODUCT;

    @Deprecated
    public static final String RADIO;
    public static final String SERIAL;
    public static final String TAGS;
    public static final long TIME = 0L;
    public static final String TYPE;
    public static final String UNKNOWN = "unknown";
    public static final String USER;

    static
    {
        int i = 1;
        ID = getString("ro.build.id");
        DISPLAY = getString("ro.build.display.id");
        PRODUCT = getString("ro.product.name");
        DEVICE = getString("ro.product.device");
        BOARD = getString("ro.product.board");
        CPU_ABI = getString("ro.product.cpu.abi");
        CPU_ABI2 = getString("ro.product.cpu.abi2");
        MANUFACTURER = getString("ro.product.manufacturer");
        BRAND = getString("ro.product.brand");
        MODEL = getString("ro.product.model");
        BOOTLOADER = getString("ro.bootloader");
        RADIO = getString("gsm.version.baseband");
        HARDWARE = getString("ro.hardware");
        SERIAL = getString("ro.serialno");
        TYPE = getString("ro.build.type");
        TAGS = getString("ro.build.tags");
        FINGERPRINT = getString("ro.build.fingerprint");
        TIME = 1000L * getLong("ro.build.date.utc");
        USER = getString("ro.build.user");
        HOST = getString("ro.build.host");
        if (SystemProperties.getInt("ro.debuggable", 0) == i);
        while (true)
        {
            IS_DEBUGGABLE = i;
            return;
            i = 0;
        }
    }

    private static long getLong(String paramString)
    {
        try
        {
            long l2 = Long.parseLong(SystemProperties.get(paramString));
            l1 = l2;
            return l1;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                long l1 = -1L;
        }
    }

    public static String getRadioVersion()
    {
        return SystemProperties.get("gsm.version.baseband", null);
    }

    private static String getString(String paramString)
    {
        return SystemProperties.get(paramString, "unknown");
    }

    public static class VERSION_CODES
    {
        public static final int BASE = 1;
        public static final int BASE_1_1 = 2;
        public static final int CUPCAKE = 3;
        public static final int CUR_DEVELOPMENT = 10000;
        public static final int DONUT = 4;
        public static final int ECLAIR = 5;
        public static final int ECLAIR_0_1 = 6;
        public static final int ECLAIR_MR1 = 7;
        public static final int FROYO = 8;
        public static final int GINGERBREAD = 9;
        public static final int GINGERBREAD_MR1 = 10;
        public static final int HONEYCOMB = 11;
        public static final int HONEYCOMB_MR1 = 12;
        public static final int HONEYCOMB_MR2 = 13;
        public static final int ICE_CREAM_SANDWICH = 14;
        public static final int ICE_CREAM_SANDWICH_MR1 = 15;
        public static final int JELLY_BEAN = 16;
    }

    public static class VERSION
    {
        public static final String CODENAME;
        public static final String INCREMENTAL;
        public static final String RELEASE;
        public static final int RESOURCES_SDK_INT;

        @Deprecated
        public static final String SDK;
        public static final int SDK_INT;

        static
        {
            int i = 0;
            INCREMENTAL = Build.getString("ro.build.version.incremental");
            RELEASE = Build.getString("ro.build.version.release");
            SDK = Build.getString("ro.build.version.sdk");
            SDK_INT = SystemProperties.getInt("ro.build.version.sdk", 0);
            CODENAME = Build.getString("ro.build.version.codename");
            int j = SDK_INT;
            if ("REL".equals(CODENAME));
            while (true)
            {
                RESOURCES_SDK_INT = i + j;
                return;
                i = 1;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Build
 * JD-Core Version:        0.6.2
 */