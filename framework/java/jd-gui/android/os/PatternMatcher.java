package android.os;

public class PatternMatcher
    implements Parcelable
{
    public static final Parcelable.Creator<PatternMatcher> CREATOR = new Parcelable.Creator()
    {
        public PatternMatcher createFromParcel(Parcel paramAnonymousParcel)
        {
            return new PatternMatcher(paramAnonymousParcel);
        }

        public PatternMatcher[] newArray(int paramAnonymousInt)
        {
            return new PatternMatcher[paramAnonymousInt];
        }
    };
    public static final int PATTERN_LITERAL = 0;
    public static final int PATTERN_PREFIX = 1;
    public static final int PATTERN_SIMPLE_GLOB = 2;
    private final String mPattern;
    private final int mType;

    public PatternMatcher(Parcel paramParcel)
    {
        this.mPattern = paramParcel.readString();
        this.mType = paramParcel.readInt();
    }

    public PatternMatcher(String paramString, int paramInt)
    {
        this.mPattern = paramString;
        this.mType = paramInt;
    }

    static boolean matchPattern(String paramString1, String paramString2, int paramInt)
    {
        int i = 1;
        boolean bool = false;
        if (paramString2 == null);
        while (true)
        {
            return bool;
            if (paramInt == 0)
            {
                bool = paramString1.equals(paramString2);
            }
            else if (paramInt == i)
            {
                bool = paramString2.startsWith(paramString1);
            }
            else if (paramInt == 2)
            {
                int k = paramString1.length();
                int j;
                if (k <= 0)
                {
                    if (paramString2.length() <= 0);
                    while (true)
                    {
                        j = i;
                        break;
                        i = 0;
                    }
                }
                int m = paramString2.length();
                int n = 0;
                int i1 = 0;
                int i2 = paramString1.charAt(0);
                while (true)
                {
                    label94: if ((n >= k) || (i1 >= m))
                        break label403;
                    int i3 = i2;
                    n++;
                    label130: int i4;
                    if (n < k)
                    {
                        i2 = paramString1.charAt(n);
                        if (i3 != 92)
                            break label207;
                        i4 = i;
                        label140: if (i4 != 0)
                        {
                            i3 = i2;
                            n++;
                            if (n >= k)
                                break label213;
                            i2 = paramString1.charAt(n);
                        }
                    }
                    while (true)
                        if (i2 == 42)
                        {
                            if ((i4 == 0) && (i3 == 46))
                            {
                                if (n >= k - 1)
                                {
                                    j = i;
                                    break;
                                    i2 = 0;
                                    break label130;
                                    label207: i4 = 0;
                                    break label140;
                                    label213: i2 = 0;
                                    continue;
                                }
                                int i5 = n + 1;
                                int i6 = paramString1.charAt(i5);
                                if (i6 == 92)
                                {
                                    i5++;
                                    if (i5 < k)
                                        i6 = paramString1.charAt(i5);
                                }
                                else
                                {
                                    label258: if (paramString2.charAt(i1) != i6)
                                        break label309;
                                    label269: if (i1 == m)
                                        break label320;
                                    n = i5 + 1;
                                    if (n >= k)
                                        break label322;
                                }
                                label309: label320: label322: for (i2 = paramString1.charAt(n); ; i2 = 0)
                                {
                                    i1++;
                                    break label94;
                                    i6 = 0;
                                    break label258;
                                    i1++;
                                    if (i1 < m)
                                        break label258;
                                    break label269;
                                    break;
                                }
                            }
                            label328: if (paramString2.charAt(i1) != i3)
                            {
                                label339: n++;
                                if (n >= k)
                                    break label373;
                            }
                            label373: for (i2 = paramString1.charAt(n); ; i2 = 0)
                            {
                                break;
                                i1++;
                                if (i1 < m)
                                    break label328;
                                break label339;
                            }
                        }
                    if ((i3 != 46) && (paramString2.charAt(i1) != i3))
                        break;
                    i1++;
                }
                label403: if ((n >= k) && (i1 >= m))
                    j = i;
                else if ((n == k - 2) && (paramString1.charAt(n) == '.') && (paramString1.charAt(n + 1) == '*'))
                    j = i;
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public final String getPath()
    {
        return this.mPattern;
    }

    public final int getType()
    {
        return this.mType;
    }

    public boolean match(String paramString)
    {
        return matchPattern(this.mPattern, paramString, this.mType);
    }

    public String toString()
    {
        String str = "? ";
        switch (this.mType)
        {
        default:
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            return "PatternMatcher{" + str + this.mPattern + "}";
            str = "LITERAL: ";
            continue;
            str = "PREFIX: ";
            continue;
            str = "GLOB: ";
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mPattern);
        paramParcel.writeInt(this.mType);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.PatternMatcher
 * JD-Core Version:        0.6.2
 */