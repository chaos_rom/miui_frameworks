package android.os;

public abstract interface LocalPowerManager
{
    public static final int BUTTON_EVENT = 1;
    public static final int OTHER_EVENT = 0;
    public static final int POKE_LOCK_IGNORE_TOUCH_EVENTS = 1;
    public static final int POKE_LOCK_MEDIUM_TIMEOUT = 4;
    public static final int POKE_LOCK_SHORT_TIMEOUT = 2;
    public static final int POKE_LOCK_TIMEOUT_MASK = 6;
    public static final int TOUCH_EVENT = 2;

    public abstract void enableUserActivity(boolean paramBoolean);

    public abstract void goToSleep(long paramLong);

    public abstract boolean isScreenOn();

    public abstract void setButtonBrightnessOverride(int paramInt);

    public abstract void setKeyboardVisibility(boolean paramBoolean);

    public abstract void setScreenBrightnessOverride(int paramInt);

    public abstract void userActivity(long paramLong, boolean paramBoolean, int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.LocalPowerManager
 * JD-Core Version:        0.6.2
 */