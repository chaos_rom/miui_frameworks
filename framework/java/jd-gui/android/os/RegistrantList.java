package android.os;

import java.util.ArrayList;

public class RegistrantList
{
    ArrayList registrants = new ArrayList();

    /** @deprecated */
    private void internalNotifyRegistrants(Object paramObject, Throwable paramThrowable)
    {
        int i = 0;
        try
        {
            int j = this.registrants.size();
            while (i < j)
            {
                ((Registrant)this.registrants.get(i)).internalNotifyRegistrant(paramObject, paramThrowable);
                i++;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void add(Handler paramHandler, int paramInt, Object paramObject)
    {
        try
        {
            add(new Registrant(paramHandler, paramInt, paramObject));
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void add(Registrant paramRegistrant)
    {
        try
        {
            removeCleared();
            this.registrants.add(paramRegistrant);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void addUnique(Handler paramHandler, int paramInt, Object paramObject)
    {
        try
        {
            remove(paramHandler);
            add(new Registrant(paramHandler, paramInt, paramObject));
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public Object get(int paramInt)
    {
        try
        {
            Object localObject2 = this.registrants.get(paramInt);
            return localObject2;
        }
        finally
        {
            localObject1 = finally;
            throw localObject1;
        }
    }

    public void notifyException(Throwable paramThrowable)
    {
        internalNotifyRegistrants(null, paramThrowable);
    }

    public void notifyRegistrants()
    {
        internalNotifyRegistrants(null, null);
    }

    public void notifyRegistrants(AsyncResult paramAsyncResult)
    {
        internalNotifyRegistrants(paramAsyncResult.result, paramAsyncResult.exception);
    }

    public void notifyResult(Object paramObject)
    {
        internalNotifyRegistrants(paramObject, null);
    }

    /** @deprecated */
    public void remove(Handler paramHandler)
    {
        for (int i = 0; ; i++)
            try
            {
                int j = this.registrants.size();
                if (i < j)
                {
                    Registrant localRegistrant = (Registrant)this.registrants.get(i);
                    Handler localHandler = localRegistrant.getHandler();
                    if ((localHandler == null) || (localHandler == paramHandler))
                        localRegistrant.clear();
                }
                else
                {
                    removeCleared();
                    return;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
    }

    /** @deprecated */
    public void removeCleared()
    {
        try
        {
            for (int i = -1 + this.registrants.size(); i >= 0; i--)
                if (((Registrant)this.registrants.get(i)).refH == null)
                    this.registrants.remove(i);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int size()
    {
        try
        {
            int i = this.registrants.size();
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.RegistrantList
 * JD-Core Version:        0.6.2
 */