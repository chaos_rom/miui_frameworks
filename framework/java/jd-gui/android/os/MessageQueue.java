package android.os;

import android.util.AndroidRuntimeException;
import android.util.Log;
import java.util.ArrayList;

public class MessageQueue
{
    private boolean mBlocked;
    private final ArrayList<IdleHandler> mIdleHandlers = new ArrayList();
    Message mMessages;
    private int mNextBarrierToken;
    private IdleHandler[] mPendingIdleHandlers;
    private int mPtr;
    private final boolean mQuitAllowed;
    private boolean mQuiting;

    MessageQueue(boolean paramBoolean)
    {
        this.mQuitAllowed = paramBoolean;
        nativeInit();
    }

    private native void nativeDestroy();

    private native void nativeInit();

    private native void nativePollOnce(int paramInt1, int paramInt2);

    private native void nativeWake(int paramInt);

    public final void addIdleHandler(IdleHandler paramIdleHandler)
    {
        if (paramIdleHandler == null)
            throw new NullPointerException("Can't add a null IdleHandler");
        try
        {
            this.mIdleHandlers.add(paramIdleHandler);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    final boolean enqueueMessage(Message paramMessage, long paramLong)
    {
        boolean bool1 = false;
        if (paramMessage.isInUse())
            throw new AndroidRuntimeException(paramMessage + " This message is already in use.");
        if (paramMessage.target == null)
            throw new AndroidRuntimeException("Message must have a target.");
        while (true)
        {
            Message localMessage1;
            try
            {
                if (this.mQuiting)
                {
                    RuntimeException localRuntimeException = new RuntimeException(paramMessage.target + " sending message to a Handler on a dead thread");
                    Log.w("MessageQueue", localRuntimeException.getMessage(), localRuntimeException);
                    break;
                }
                paramMessage.when = paramLong;
                localMessage1 = this.mMessages;
                if ((localMessage1 == null) || (paramLong == 0L) || (paramLong < localMessage1.when))
                {
                    paramMessage.next = localMessage1;
                    this.mMessages = paramMessage;
                    bool1 = this.mBlocked;
                    if (bool1)
                        nativeWake(this.mPtr);
                    bool1 = true;
                    break;
                }
                if ((this.mBlocked) && (localMessage1.target == null) && (paramMessage.isAsynchronous()))
                    bool1 = true;
                Message localMessage2 = localMessage1;
                localMessage1 = localMessage1.next;
                if ((localMessage1 == null) || (paramLong < localMessage1.when))
                {
                    paramMessage.next = localMessage1;
                    localMessage2.next = paramMessage;
                    continue;
                }
            }
            finally
            {
            }
            if (bool1)
            {
                boolean bool2 = localMessage1.isAsynchronous();
                if (bool2)
                    bool1 = false;
            }
        }
        return bool1;
    }

    final int enqueueSyncBarrier(long paramLong)
    {
        try
        {
            int i = this.mNextBarrierToken;
            this.mNextBarrierToken = (i + 1);
            Message localMessage1 = Message.obtain();
            localMessage1.arg1 = i;
            Object localObject2 = null;
            Message localMessage2 = this.mMessages;
            if (paramLong != 0L)
                while ((localMessage2 != null) && (localMessage2.when <= paramLong))
                {
                    localObject2 = localMessage2;
                    localMessage2 = localMessage2.next;
                }
            if (localObject2 != null)
            {
                localMessage1.next = localMessage2;
                localObject2.next = localMessage1;
                return i;
            }
            localMessage1.next = localMessage2;
            this.mMessages = localMessage1;
        }
        finally
        {
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDestroy();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    // ERROR //
    final boolean hasMessages(Handler paramHandler, int paramInt, Object paramObject)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 4
        //     3: aload_1
        //     4: ifnonnull +6 -> 10
        //     7: iload 4
        //     9: ireturn
        //     10: aload_0
        //     11: monitorenter
        //     12: aload_0
        //     13: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     16: astore 6
        //     18: aload 6
        //     20: ifnull +59 -> 79
        //     23: aload 6
        //     25: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     28: aload_1
        //     29: if_acmpne +40 -> 69
        //     32: aload 6
        //     34: getfield 146	android/os/Message:what	I
        //     37: iload_2
        //     38: if_icmpne +31 -> 69
        //     41: aload_3
        //     42: ifnull +12 -> 54
        //     45: aload 6
        //     47: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     50: aload_3
        //     51: if_acmpne +18 -> 69
        //     54: iconst_1
        //     55: istore 4
        //     57: aload_0
        //     58: monitorexit
        //     59: goto -52 -> 7
        //     62: astore 5
        //     64: aload_0
        //     65: monitorexit
        //     66: aload 5
        //     68: athrow
        //     69: aload 6
        //     71: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     74: astore 6
        //     76: goto -58 -> 18
        //     79: aload_0
        //     80: monitorexit
        //     81: goto -74 -> 7
        //
        // Exception table:
        //     from	to	target	type
        //     12	66	62	finally
        //     69	81	62	finally
    }

    // ERROR //
    final boolean hasMessages(Handler paramHandler, java.lang.Runnable paramRunnable, Object paramObject)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 4
        //     3: aload_1
        //     4: ifnonnull +6 -> 10
        //     7: iload 4
        //     9: ireturn
        //     10: aload_0
        //     11: monitorenter
        //     12: aload_0
        //     13: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     16: astore 6
        //     18: aload 6
        //     20: ifnull +59 -> 79
        //     23: aload 6
        //     25: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     28: aload_1
        //     29: if_acmpne +40 -> 69
        //     32: aload 6
        //     34: getfield 155	android/os/Message:callback	Ljava/lang/Runnable;
        //     37: aload_2
        //     38: if_acmpne +31 -> 69
        //     41: aload_3
        //     42: ifnull +12 -> 54
        //     45: aload 6
        //     47: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     50: aload_3
        //     51: if_acmpne +18 -> 69
        //     54: iconst_1
        //     55: istore 4
        //     57: aload_0
        //     58: monitorexit
        //     59: goto -52 -> 7
        //     62: astore 5
        //     64: aload_0
        //     65: monitorexit
        //     66: aload 5
        //     68: athrow
        //     69: aload 6
        //     71: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     74: astore 6
        //     76: goto -58 -> 18
        //     79: aload_0
        //     80: monitorexit
        //     81: goto -74 -> 7
        //
        // Exception table:
        //     from	to	target	type
        //     12	66	62	finally
        //     69	81	62	finally
    }

    // ERROR //
    final Message next()
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore_1
        //     3: iconst_0
        //     4: istore_2
        //     5: iload_2
        //     6: ifeq +6 -> 12
        //     9: invokestatic 160	android/os/Binder:flushPendingCommands	()V
        //     12: aload_0
        //     13: aload_0
        //     14: getfield 118	android/os/MessageQueue:mPtr	I
        //     17: iload_2
        //     18: invokespecial 162	android/os/MessageQueue:nativePollOnce	(II)V
        //     21: aload_0
        //     22: monitorenter
        //     23: aload_0
        //     24: getfield 89	android/os/MessageQueue:mQuiting	Z
        //     27: ifeq +11 -> 38
        //     30: aconst_null
        //     31: astore 7
        //     33: aload_0
        //     34: monitorexit
        //     35: goto +320 -> 355
        //     38: invokestatic 168	android/os/SystemClock:uptimeMillis	()J
        //     41: lstore 4
        //     43: aconst_null
        //     44: astore 6
        //     46: aload_0
        //     47: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     50: astore 7
        //     52: aload 7
        //     54: ifnull +35 -> 89
        //     57: aload 7
        //     59: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     62: ifnonnull +27 -> 89
        //     65: aload 7
        //     67: astore 6
        //     69: aload 7
        //     71: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     74: astore 7
        //     76: aload 7
        //     78: ifnull +11 -> 89
        //     81: aload 7
        //     83: invokevirtual 123	android/os/Message:isAsynchronous	()Z
        //     86: ifeq -21 -> 65
        //     89: aload 7
        //     91: ifnull +267 -> 358
        //     94: lload 4
        //     96: aload 7
        //     98: getfield 109	android/os/Message:when	J
        //     101: lcmp
        //     102: ifge +70 -> 172
        //     105: aload 7
        //     107: getfield 109	android/os/Message:when	J
        //     110: lload 4
        //     112: lsub
        //     113: ldc2_w 169
        //     116: invokestatic 176	java/lang/Math:min	(JJ)J
        //     119: l2i
        //     120: istore_2
        //     121: iload_1
        //     122: ifge +31 -> 153
        //     125: aload_0
        //     126: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     129: ifnull +16 -> 145
        //     132: lload 4
        //     134: aload_0
        //     135: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     138: getfield 109	android/os/Message:when	J
        //     141: lcmp
        //     142: ifge +11 -> 153
        //     145: aload_0
        //     146: getfield 31	android/os/MessageQueue:mIdleHandlers	Ljava/util/ArrayList;
        //     149: invokevirtual 180	java/util/ArrayList:size	()I
        //     152: istore_1
        //     153: iload_1
        //     154: ifgt +66 -> 220
        //     157: aload_0
        //     158: iconst_1
        //     159: putfield 116	android/os/MessageQueue:mBlocked	Z
        //     162: aload_0
        //     163: monitorexit
        //     164: goto -159 -> 5
        //     167: astore_3
        //     168: aload_0
        //     169: monitorexit
        //     170: aload_3
        //     171: athrow
        //     172: aload_0
        //     173: iconst_0
        //     174: putfield 116	android/os/MessageQueue:mBlocked	Z
        //     177: aload 6
        //     179: ifnull +29 -> 208
        //     182: aload 6
        //     184: aload 7
        //     186: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     189: putfield 114	android/os/Message:next	Landroid/os/Message;
        //     192: aload 7
        //     194: aconst_null
        //     195: putfield 114	android/os/Message:next	Landroid/os/Message;
        //     198: aload 7
        //     200: invokevirtual 183	android/os/Message:markInUse	()V
        //     203: aload_0
        //     204: monitorexit
        //     205: goto +150 -> 355
        //     208: aload_0
        //     209: aload 7
        //     211: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     214: putfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     217: goto -25 -> 192
        //     220: aload_0
        //     221: getfield 185	android/os/MessageQueue:mPendingIdleHandlers	[Landroid/os/MessageQueue$IdleHandler;
        //     224: ifnonnull +15 -> 239
        //     227: aload_0
        //     228: iload_1
        //     229: iconst_4
        //     230: invokestatic 189	java/lang/Math:max	(II)I
        //     233: anewarray 6	android/os/MessageQueue$IdleHandler
        //     236: putfield 185	android/os/MessageQueue:mPendingIdleHandlers	[Landroid/os/MessageQueue$IdleHandler;
        //     239: aload_0
        //     240: aload_0
        //     241: getfield 31	android/os/MessageQueue:mIdleHandlers	Ljava/util/ArrayList;
        //     244: aload_0
        //     245: getfield 185	android/os/MessageQueue:mPendingIdleHandlers	[Landroid/os/MessageQueue$IdleHandler;
        //     248: invokevirtual 193	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     251: checkcast 194	[Landroid/os/MessageQueue$IdleHandler;
        //     254: putfield 185	android/os/MessageQueue:mPendingIdleHandlers	[Landroid/os/MessageQueue$IdleHandler;
        //     257: aload_0
        //     258: monitorexit
        //     259: iconst_0
        //     260: istore 8
        //     262: iload 8
        //     264: iload_1
        //     265: if_icmpge +83 -> 348
        //     268: aload_0
        //     269: getfield 185	android/os/MessageQueue:mPendingIdleHandlers	[Landroid/os/MessageQueue$IdleHandler;
        //     272: iload 8
        //     274: aaload
        //     275: astore 9
        //     277: aload_0
        //     278: getfield 185	android/os/MessageQueue:mPendingIdleHandlers	[Landroid/os/MessageQueue$IdleHandler;
        //     281: iload 8
        //     283: aconst_null
        //     284: aastore
        //     285: iconst_0
        //     286: istore 10
        //     288: aload 9
        //     290: invokeinterface 197 1 0
        //     295: istore 15
        //     297: iload 15
        //     299: istore 10
        //     301: iload 10
        //     303: ifne +17 -> 320
        //     306: aload_0
        //     307: monitorenter
        //     308: aload_0
        //     309: getfield 31	android/os/MessageQueue:mIdleHandlers	Ljava/util/ArrayList;
        //     312: aload 9
        //     314: invokevirtual 200	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     317: pop
        //     318: aload_0
        //     319: monitorexit
        //     320: iinc 8 1
        //     323: goto -61 -> 262
        //     326: astore 11
        //     328: ldc 96
        //     330: ldc 202
        //     332: aload 11
        //     334: invokestatic 205	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     337: pop
        //     338: goto -37 -> 301
        //     341: astore 13
        //     343: aload_0
        //     344: monitorexit
        //     345: aload 13
        //     347: athrow
        //     348: iconst_0
        //     349: istore_1
        //     350: iconst_0
        //     351: istore_2
        //     352: goto -347 -> 5
        //     355: aload 7
        //     357: areturn
        //     358: bipush 255
        //     360: istore_2
        //     361: goto -240 -> 121
        //
        // Exception table:
        //     from	to	target	type
        //     23	170	167	finally
        //     172	259	167	finally
        //     288	297	326	java/lang/Throwable
        //     308	320	341	finally
        //     343	345	341	finally
    }

    final void quit()
    {
        if (!this.mQuitAllowed)
            throw new RuntimeException("Main thread not allowed to quit.");
        try
        {
            if (this.mQuiting)
                return;
            this.mQuiting = true;
            nativeWake(this.mPtr);
        }
        finally
        {
        }
    }

    // ERROR //
    final void removeCallbacksAndMessages(Handler paramHandler, Object paramObject)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +4 -> 5
        //     4: return
        //     5: aload_0
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     11: astore 4
        //     13: aload 4
        //     15: ifnull +50 -> 65
        //     18: aload 4
        //     20: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     23: aload_1
        //     24: if_acmpne +41 -> 65
        //     27: aload_2
        //     28: ifnull +12 -> 40
        //     31: aload 4
        //     33: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     36: aload_2
        //     37: if_acmpne +28 -> 65
        //     40: aload 4
        //     42: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     45: astore 7
        //     47: aload_0
        //     48: aload 7
        //     50: putfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     53: aload 4
        //     55: invokevirtual 213	android/os/Message:recycle	()V
        //     58: aload 7
        //     60: astore 4
        //     62: goto -49 -> 13
        //     65: aload 4
        //     67: ifnull +71 -> 138
        //     70: aload 4
        //     72: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     75: astore 5
        //     77: aload 5
        //     79: ifnull +52 -> 131
        //     82: aload 5
        //     84: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     87: aload_1
        //     88: if_acmpne +43 -> 131
        //     91: aload_2
        //     92: ifnull +12 -> 104
        //     95: aload 5
        //     97: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     100: aload_2
        //     101: if_acmpne +30 -> 131
        //     104: aload 5
        //     106: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     109: astore 6
        //     111: aload 5
        //     113: invokevirtual 213	android/os/Message:recycle	()V
        //     116: aload 4
        //     118: aload 6
        //     120: putfield 114	android/os/Message:next	Landroid/os/Message;
        //     123: goto -58 -> 65
        //     126: astore_3
        //     127: aload_0
        //     128: monitorexit
        //     129: aload_3
        //     130: athrow
        //     131: aload 5
        //     133: astore 4
        //     135: goto -70 -> 65
        //     138: aload_0
        //     139: monitorexit
        //     140: goto -136 -> 4
        //
        // Exception table:
        //     from	to	target	type
        //     7	129	126	finally
        //     138	140	126	finally
    }

    public final void removeIdleHandler(IdleHandler paramIdleHandler)
    {
        try
        {
            this.mIdleHandlers.remove(paramIdleHandler);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    final void removeMessages(Handler paramHandler, int paramInt, Object paramObject)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +4 -> 5
        //     4: return
        //     5: aload_0
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     11: astore 5
        //     13: aload 5
        //     15: ifnull +59 -> 74
        //     18: aload 5
        //     20: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     23: aload_1
        //     24: if_acmpne +50 -> 74
        //     27: aload 5
        //     29: getfield 146	android/os/Message:what	I
        //     32: iload_2
        //     33: if_icmpne +41 -> 74
        //     36: aload_3
        //     37: ifnull +12 -> 49
        //     40: aload 5
        //     42: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     45: aload_3
        //     46: if_acmpne +28 -> 74
        //     49: aload 5
        //     51: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     54: astore 8
        //     56: aload_0
        //     57: aload 8
        //     59: putfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     62: aload 5
        //     64: invokevirtual 213	android/os/Message:recycle	()V
        //     67: aload 8
        //     69: astore 5
        //     71: goto -58 -> 13
        //     74: aload 5
        //     76: ifnull +82 -> 158
        //     79: aload 5
        //     81: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     84: astore 6
        //     86: aload 6
        //     88: ifnull +63 -> 151
        //     91: aload 6
        //     93: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     96: aload_1
        //     97: if_acmpne +54 -> 151
        //     100: aload 6
        //     102: getfield 146	android/os/Message:what	I
        //     105: iload_2
        //     106: if_icmpne +45 -> 151
        //     109: aload_3
        //     110: ifnull +12 -> 122
        //     113: aload 6
        //     115: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     118: aload_3
        //     119: if_acmpne +32 -> 151
        //     122: aload 6
        //     124: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     127: astore 7
        //     129: aload 6
        //     131: invokevirtual 213	android/os/Message:recycle	()V
        //     134: aload 5
        //     136: aload 7
        //     138: putfield 114	android/os/Message:next	Landroid/os/Message;
        //     141: goto -67 -> 74
        //     144: astore 4
        //     146: aload_0
        //     147: monitorexit
        //     148: aload 4
        //     150: athrow
        //     151: aload 6
        //     153: astore 5
        //     155: goto -81 -> 74
        //     158: aload_0
        //     159: monitorexit
        //     160: goto -156 -> 4
        //
        // Exception table:
        //     from	to	target	type
        //     7	148	144	finally
        //     158	160	144	finally
    }

    // ERROR //
    final void removeMessages(Handler paramHandler, java.lang.Runnable paramRunnable, Object paramObject)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnull +7 -> 8
        //     4: aload_2
        //     5: ifnonnull +4 -> 9
        //     8: return
        //     9: aload_0
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     15: astore 5
        //     17: aload 5
        //     19: ifnull +59 -> 78
        //     22: aload 5
        //     24: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     27: aload_1
        //     28: if_acmpne +50 -> 78
        //     31: aload 5
        //     33: getfield 155	android/os/Message:callback	Ljava/lang/Runnable;
        //     36: aload_2
        //     37: if_acmpne +41 -> 78
        //     40: aload_3
        //     41: ifnull +12 -> 53
        //     44: aload 5
        //     46: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     49: aload_3
        //     50: if_acmpne +28 -> 78
        //     53: aload 5
        //     55: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     58: astore 8
        //     60: aload_0
        //     61: aload 8
        //     63: putfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     66: aload 5
        //     68: invokevirtual 213	android/os/Message:recycle	()V
        //     71: aload 8
        //     73: astore 5
        //     75: goto -58 -> 17
        //     78: aload 5
        //     80: ifnull +82 -> 162
        //     83: aload 5
        //     85: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     88: astore 6
        //     90: aload 6
        //     92: ifnull +63 -> 155
        //     95: aload 6
        //     97: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     100: aload_1
        //     101: if_acmpne +54 -> 155
        //     104: aload 6
        //     106: getfield 155	android/os/Message:callback	Ljava/lang/Runnable;
        //     109: aload_2
        //     110: if_acmpne +45 -> 155
        //     113: aload_3
        //     114: ifnull +12 -> 126
        //     117: aload 6
        //     119: getfield 150	android/os/Message:obj	Ljava/lang/Object;
        //     122: aload_3
        //     123: if_acmpne +32 -> 155
        //     126: aload 6
        //     128: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     131: astore 7
        //     133: aload 6
        //     135: invokevirtual 213	android/os/Message:recycle	()V
        //     138: aload 5
        //     140: aload 7
        //     142: putfield 114	android/os/Message:next	Landroid/os/Message;
        //     145: goto -67 -> 78
        //     148: astore 4
        //     150: aload_0
        //     151: monitorexit
        //     152: aload 4
        //     154: athrow
        //     155: aload 6
        //     157: astore 5
        //     159: goto -81 -> 78
        //     162: aload_0
        //     163: monitorexit
        //     164: goto -156 -> 8
        //
        // Exception table:
        //     from	to	target	type
        //     11	152	148	finally
        //     162	164	148	finally
    }

    // ERROR //
    final void removeSyncBarrier(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aconst_null
        //     3: astore_2
        //     4: aload_0
        //     5: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     8: astore 4
        //     10: aload 4
        //     12: ifnull +33 -> 45
        //     15: aload 4
        //     17: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     20: ifnonnull +12 -> 32
        //     23: aload 4
        //     25: getfield 134	android/os/Message:arg1	I
        //     28: iload_1
        //     29: if_icmpeq +16 -> 45
        //     32: aload 4
        //     34: astore_2
        //     35: aload 4
        //     37: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     40: astore 4
        //     42: goto -32 -> 10
        //     45: aload 4
        //     47: ifnonnull +18 -> 65
        //     50: new 220	java/lang/IllegalStateException
        //     53: dup
        //     54: ldc 222
        //     56: invokespecial 223	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     59: athrow
        //     60: astore_3
        //     61: aload_0
        //     62: monitorexit
        //     63: aload_3
        //     64: athrow
        //     65: aload_2
        //     66: ifnull +36 -> 102
        //     69: aload_2
        //     70: aload 4
        //     72: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     75: putfield 114	android/os/Message:next	Landroid/os/Message;
        //     78: iconst_0
        //     79: istore 5
        //     81: aload 4
        //     83: invokevirtual 213	android/os/Message:recycle	()V
        //     86: aload_0
        //     87: monitorexit
        //     88: iload 5
        //     90: ifeq +11 -> 101
        //     93: aload_0
        //     94: aload_0
        //     95: getfield 118	android/os/MessageQueue:mPtr	I
        //     98: invokespecial 120	android/os/MessageQueue:nativeWake	(I)V
        //     101: return
        //     102: aload_0
        //     103: aload 4
        //     105: getfield 114	android/os/Message:next	Landroid/os/Message;
        //     108: putfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     111: aload_0
        //     112: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     115: ifnull +17 -> 132
        //     118: aload_0
        //     119: getfield 111	android/os/MessageQueue:mMessages	Landroid/os/Message;
        //     122: getfield 85	android/os/Message:target	Landroid/os/Handler;
        //     125: astore 6
        //     127: aload 6
        //     129: ifnull +9 -> 138
        //     132: iconst_1
        //     133: istore 5
        //     135: goto -54 -> 81
        //     138: iconst_0
        //     139: istore 5
        //     141: goto -6 -> 135
        //
        // Exception table:
        //     from	to	target	type
        //     4	63	60	finally
        //     69	88	60	finally
        //     102	127	60	finally
    }

    public static abstract interface IdleHandler
    {
        public abstract boolean queueIdle();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.MessageQueue
 * JD-Core Version:        0.6.2
 */