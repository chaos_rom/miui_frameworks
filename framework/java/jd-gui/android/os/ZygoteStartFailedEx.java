package android.os;

class ZygoteStartFailedEx extends Exception
{
    ZygoteStartFailedEx()
    {
    }

    ZygoteStartFailedEx(String paramString)
    {
        super(paramString);
    }

    ZygoteStartFailedEx(Throwable paramThrowable)
    {
        super(paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ZygoteStartFailedEx
 * JD-Core Version:        0.6.2
 */