package android.os;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import libcore.io.OsConstants;

class CommonTimeUtils
{
    public static final int ERROR = -1;
    public static final int ERROR_BAD_VALUE = -4;
    public static final int ERROR_DEAD_OBJECT = -7;
    public static final int SUCCESS;
    private String mInterfaceDesc;
    private IBinder mRemote;

    public CommonTimeUtils(IBinder paramIBinder, String paramString)
    {
        this.mRemote = paramIBinder;
        this.mInterfaceDesc = paramString;
    }

    public int transactGetInt(int paramInt1, int paramInt2)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken(this.mInterfaceDesc);
            this.mRemote.transact(paramInt1, localParcel1, localParcel2, 0);
            if (localParcel2.readInt() == 0)
            {
                int j = localParcel2.readInt();
                i = j;
                return i;
            }
            int i = paramInt2;
        }
        finally
        {
            localParcel2.recycle();
            localParcel1.recycle();
        }
    }

    public long transactGetLong(int paramInt, long paramLong)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken(this.mInterfaceDesc);
            this.mRemote.transact(paramInt, localParcel1, localParcel2, 0);
            if (localParcel2.readInt() == 0)
            {
                long l2 = localParcel2.readLong();
                l1 = l2;
                return l1;
            }
            long l1 = paramLong;
        }
        finally
        {
            localParcel2.recycle();
            localParcel1.recycle();
        }
    }

    public InetSocketAddress transactGetSockaddr(int paramInt)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        InetSocketAddress localInetSocketAddress = null;
        try
        {
            localParcel1.writeInterfaceToken(this.mInterfaceDesc);
            this.mRemote.transact(paramInt, localParcel1, localParcel2, 0);
            int i;
            Object localObject2;
            int j;
            if (localParcel2.readInt() == 0)
            {
                i = 0;
                localObject2 = null;
                j = localParcel2.readInt();
                if (OsConstants.AF_INET != j)
                    break label177;
                int i2 = localParcel2.readInt();
                i = localParcel2.readInt();
                Object[] arrayOfObject2 = new Object[4];
                arrayOfObject2[0] = Integer.valueOf(0xFF & i2 >> 24);
                arrayOfObject2[1] = Integer.valueOf(0xFF & i2 >> 16);
                arrayOfObject2[2] = Integer.valueOf(0xFF & i2 >> 8);
                arrayOfObject2[3] = Integer.valueOf(i2 & 0xFF);
                localObject2 = String.format("%d.%d.%d.%d", arrayOfObject2);
            }
            while (true)
            {
                if (localObject2 != null)
                    localInetSocketAddress = new InetSocketAddress((String)localObject2, i);
                return localInetSocketAddress;
                label177: if (OsConstants.AF_INET6 == j)
                {
                    int k = localParcel2.readInt();
                    int m = localParcel2.readInt();
                    int n = localParcel2.readInt();
                    int i1 = localParcel2.readInt();
                    i = localParcel2.readInt();
                    localParcel2.readInt();
                    localParcel2.readInt();
                    Object[] arrayOfObject1 = new Object[8];
                    arrayOfObject1[0] = Integer.valueOf(0xFFFF & k >> 16);
                    arrayOfObject1[1] = Integer.valueOf(0xFFFF & k);
                    arrayOfObject1[2] = Integer.valueOf(0xFFFF & m >> 16);
                    arrayOfObject1[3] = Integer.valueOf(0xFFFF & m);
                    arrayOfObject1[4] = Integer.valueOf(0xFFFF & n >> 16);
                    arrayOfObject1[5] = Integer.valueOf(0xFFFF & n);
                    arrayOfObject1[6] = Integer.valueOf(0xFFFF & i1 >> 16);
                    arrayOfObject1[7] = Integer.valueOf(0xFFFF & i1);
                    String str = String.format("[%04X:%04X:%04X:%04X:%04X:%04X:%04X:%04X]", arrayOfObject1);
                    localObject2 = str;
                }
            }
        }
        finally
        {
            localParcel2.recycle();
            localParcel1.recycle();
        }
    }

    public String transactGetString(int paramInt, String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
            localParcel1.writeInterfaceToken(this.mInterfaceDesc);
            this.mRemote.transact(paramInt, localParcel1, localParcel2, 0);
            if (localParcel2.readInt() == 0)
            {
                String str2 = localParcel2.readString();
                str1 = str2;
                return str1;
            }
            String str1 = paramString;
        }
        finally
        {
            localParcel2.recycle();
            localParcel1.recycle();
        }
    }

    // ERROR //
    public int transactSetInt(int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: invokestatic 35	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     3: astore_3
        //     4: invokestatic 35	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     7: astore 4
        //     9: aload_3
        //     10: aload_0
        //     11: getfield 25	android/os/CommonTimeUtils:mInterfaceDesc	Ljava/lang/String;
        //     14: invokevirtual 39	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //     17: aload_3
        //     18: iload_2
        //     19: invokevirtual 101	android/os/Parcel:writeInt	(I)V
        //     22: aload_0
        //     23: getfield 23	android/os/CommonTimeUtils:mRemote	Landroid/os/IBinder;
        //     26: iload_1
        //     27: aload_3
        //     28: aload 4
        //     30: iconst_0
        //     31: invokeinterface 45 5 0
        //     36: pop
        //     37: aload 4
        //     39: invokevirtual 49	android/os/Parcel:readInt	()I
        //     42: istore 9
        //     44: iload 9
        //     46: istore 7
        //     48: aload 4
        //     50: invokevirtual 52	android/os/Parcel:recycle	()V
        //     53: aload_3
        //     54: invokevirtual 52	android/os/Parcel:recycle	()V
        //     57: iload 7
        //     59: ireturn
        //     60: astore 6
        //     62: bipush 249
        //     64: istore 7
        //     66: aload 4
        //     68: invokevirtual 52	android/os/Parcel:recycle	()V
        //     71: aload_3
        //     72: invokevirtual 52	android/os/Parcel:recycle	()V
        //     75: goto -18 -> 57
        //     78: astore 5
        //     80: aload 4
        //     82: invokevirtual 52	android/os/Parcel:recycle	()V
        //     85: aload_3
        //     86: invokevirtual 52	android/os/Parcel:recycle	()V
        //     89: aload 5
        //     91: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     9	44	60	android/os/RemoteException
        //     9	44	78	finally
    }

    // ERROR //
    public int transactSetLong(int paramInt, long paramLong)
    {
        // Byte code:
        //     0: invokestatic 35	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     3: astore 4
        //     5: invokestatic 35	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     8: astore 5
        //     10: aload 4
        //     12: aload_0
        //     13: getfield 25	android/os/CommonTimeUtils:mInterfaceDesc	Ljava/lang/String;
        //     16: invokevirtual 39	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //     19: aload 4
        //     21: lload_2
        //     22: invokevirtual 107	android/os/Parcel:writeLong	(J)V
        //     25: aload_0
        //     26: getfield 23	android/os/CommonTimeUtils:mRemote	Landroid/os/IBinder;
        //     29: iload_1
        //     30: aload 4
        //     32: aload 5
        //     34: iconst_0
        //     35: invokeinterface 45 5 0
        //     40: pop
        //     41: aload 5
        //     43: invokevirtual 49	android/os/Parcel:readInt	()I
        //     46: istore 10
        //     48: iload 10
        //     50: istore 8
        //     52: aload 5
        //     54: invokevirtual 52	android/os/Parcel:recycle	()V
        //     57: aload 4
        //     59: invokevirtual 52	android/os/Parcel:recycle	()V
        //     62: iload 8
        //     64: ireturn
        //     65: astore 7
        //     67: bipush 249
        //     69: istore 8
        //     71: aload 5
        //     73: invokevirtual 52	android/os/Parcel:recycle	()V
        //     76: aload 4
        //     78: invokevirtual 52	android/os/Parcel:recycle	()V
        //     81: goto -19 -> 62
        //     84: astore 6
        //     86: aload 5
        //     88: invokevirtual 52	android/os/Parcel:recycle	()V
        //     91: aload 4
        //     93: invokevirtual 52	android/os/Parcel:recycle	()V
        //     96: aload 6
        //     98: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     10	48	65	android/os/RemoteException
        //     10	48	84	finally
    }

    public int transactSetSockaddr(int paramInt, InetSocketAddress paramInetSocketAddress)
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        while (true)
        {
            try
            {
                localParcel1.writeInterfaceToken(this.mInterfaceDesc);
                if (paramInetSocketAddress == null)
                {
                    localParcel1.writeInt(0);
                    this.mRemote.transact(paramInt, localParcel1, localParcel2, 0);
                    int n = localParcel2.readInt();
                    i = n;
                    localParcel2.recycle();
                    localParcel1.recycle();
                    j = i;
                    return j;
                }
                localParcel1.writeInt(1);
                localInetAddress = paramInetSocketAddress.getAddress();
                arrayOfByte = localInetAddress.getAddress();
                k = paramInetSocketAddress.getPort();
                if ((localInetAddress instanceof Inet4Address))
                {
                    int i1 = (0xFF & arrayOfByte[0]) << 24 | (0xFF & arrayOfByte[1]) << 16 | (0xFF & arrayOfByte[2]) << 8 | 0xFF & arrayOfByte[3];
                    localParcel1.writeInt(OsConstants.AF_INET);
                    localParcel1.writeInt(i1);
                    localParcel1.writeInt(k);
                    continue;
                }
            }
            catch (RemoteException localRemoteException)
            {
                InetAddress localInetAddress;
                byte[] arrayOfByte;
                int k;
                int i = -7;
                localParcel2.recycle();
                localParcel1.recycle();
                continue;
                if ((localInetAddress instanceof Inet6Address))
                {
                    Inet6Address localInet6Address = (Inet6Address)localInetAddress;
                    localParcel1.writeInt(OsConstants.AF_INET6);
                    int m = 0;
                    if (m < 4)
                    {
                        localParcel1.writeInt((0xFF & arrayOfByte[(0 + m * 4)]) << 24 | (0xFF & arrayOfByte[(1 + m * 4)]) << 16 | (0xFF & arrayOfByte[(2 + m * 4)]) << 8 | 0xFF & arrayOfByte[(3 + m * 4)]);
                        m++;
                        continue;
                    }
                    localParcel1.writeInt(k);
                    localParcel1.writeInt(0);
                    localParcel1.writeInt(localInet6Address.getScopeId());
                    continue;
                }
            }
            finally
            {
                localParcel2.recycle();
                localParcel1.recycle();
            }
            int j = -4;
            localParcel2.recycle();
            localParcel1.recycle();
        }
    }

    // ERROR //
    public int transactSetString(int paramInt, String paramString)
    {
        // Byte code:
        //     0: invokestatic 35	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     3: astore_3
        //     4: invokestatic 35	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     7: astore 4
        //     9: aload_3
        //     10: aload_0
        //     11: getfield 25	android/os/CommonTimeUtils:mInterfaceDesc	Ljava/lang/String;
        //     14: invokevirtual 39	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //     17: aload_3
        //     18: aload_2
        //     19: invokevirtual 133	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     22: aload_0
        //     23: getfield 23	android/os/CommonTimeUtils:mRemote	Landroid/os/IBinder;
        //     26: iload_1
        //     27: aload_3
        //     28: aload 4
        //     30: iconst_0
        //     31: invokeinterface 45 5 0
        //     36: pop
        //     37: aload 4
        //     39: invokevirtual 49	android/os/Parcel:readInt	()I
        //     42: istore 9
        //     44: iload 9
        //     46: istore 7
        //     48: aload 4
        //     50: invokevirtual 52	android/os/Parcel:recycle	()V
        //     53: aload_3
        //     54: invokevirtual 52	android/os/Parcel:recycle	()V
        //     57: iload 7
        //     59: ireturn
        //     60: astore 6
        //     62: bipush 249
        //     64: istore 7
        //     66: aload 4
        //     68: invokevirtual 52	android/os/Parcel:recycle	()V
        //     71: aload_3
        //     72: invokevirtual 52	android/os/Parcel:recycle	()V
        //     75: goto -18 -> 57
        //     78: astore 5
        //     80: aload 4
        //     82: invokevirtual 52	android/os/Parcel:recycle	()V
        //     85: aload_3
        //     86: invokevirtual 52	android/os/Parcel:recycle	()V
        //     89: aload 5
        //     91: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     9	44	60	android/os/RemoteException
        //     9	44	78	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.CommonTimeUtils
 * JD-Core Version:        0.6.2
 */