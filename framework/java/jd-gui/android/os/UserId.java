package android.os;

public final class UserId
{
    public static final boolean MU_ENABLED = true;
    public static final int PER_USER_RANGE = 100000;
    public static final int USER_ALL = -1;

    public static final int getAppId(int paramInt)
    {
        return paramInt % 100000;
    }

    public static final int getCallingUserId()
    {
        return getUserId(Binder.getCallingUid());
    }

    public static final int getUid(int paramInt1, int paramInt2)
    {
        return paramInt1 * 100000 + paramInt2 % 100000;
    }

    public static final int getUserId(int paramInt)
    {
        return paramInt / 100000;
    }

    public static boolean isApp(int paramInt)
    {
        boolean bool = false;
        if (paramInt > 0)
        {
            int i = getAppId(paramInt);
            if ((i >= 10000) && (i <= 19999))
                bool = true;
        }
        return bool;
    }

    public static final boolean isIsolated(int paramInt)
    {
        int i = getAppId(paramInt);
        if ((i >= 99000) && (i <= 99999));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static final boolean isSameApp(int paramInt1, int paramInt2)
    {
        if (getAppId(paramInt1) == getAppId(paramInt2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static final boolean isSameUser(int paramInt1, int paramInt2)
    {
        if (getUserId(paramInt1) == getUserId(paramInt2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static final int myUserId()
    {
        return getUserId(Process.myUid());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.UserId
 * JD-Core Version:        0.6.2
 */