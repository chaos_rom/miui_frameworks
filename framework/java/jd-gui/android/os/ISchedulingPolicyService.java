package android.os;

public abstract interface ISchedulingPolicyService extends IInterface
{
    public abstract int requestPriority(int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISchedulingPolicyService
    {
        private static final String DESCRIPTOR = "android.os.ISchedulingPolicyService";
        static final int TRANSACTION_requestPriority = 1;

        public Stub()
        {
            attachInterface(this, "android.os.ISchedulingPolicyService");
        }

        public static ISchedulingPolicyService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.os.ISchedulingPolicyService");
                if ((localIInterface != null) && ((localIInterface instanceof ISchedulingPolicyService)))
                    localObject = (ISchedulingPolicyService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.os.ISchedulingPolicyService");
                continue;
                paramParcel1.enforceInterface("android.os.ISchedulingPolicyService");
                int i = requestPriority(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i);
            }
        }

        private static class Proxy
            implements ISchedulingPolicyService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.os.ISchedulingPolicyService";
            }

            public int requestPriority(int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.ISchedulingPolicyService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ISchedulingPolicyService
 * JD-Core Version:        0.6.2
 */