package android.os;

import java.net.InetSocketAddress;
import java.util.NoSuchElementException;

public class CommonTimeConfig
{
    public static final int ERROR = -1;
    public static final int ERROR_BAD_VALUE = -4;
    public static final int ERROR_DEAD_OBJECT = -7;
    public static final long INVALID_GROUP_ID = -1L;
    private static final int METHOD_FORCE_NETWORKLESS_MASTER_MODE = 17;
    private static final int METHOD_GET_AUTO_DISABLE = 15;
    private static final int METHOD_GET_CLIENT_SYNC_INTERVAL = 11;
    private static final int METHOD_GET_INTERFACE_BINDING = 7;
    private static final int METHOD_GET_MASTER_ANNOUNCE_INTERVAL = 9;
    private static final int METHOD_GET_MASTER_ELECTION_ENDPOINT = 3;
    private static final int METHOD_GET_MASTER_ELECTION_GROUP_ID = 5;
    private static final int METHOD_GET_MASTER_ELECTION_PRIORITY = 1;
    private static final int METHOD_GET_PANIC_THRESHOLD = 13;
    private static final int METHOD_SET_AUTO_DISABLE = 16;
    private static final int METHOD_SET_CLIENT_SYNC_INTERVAL = 12;
    private static final int METHOD_SET_INTERFACE_BINDING = 8;
    private static final int METHOD_SET_MASTER_ANNOUNCE_INTERVAL = 10;
    private static final int METHOD_SET_MASTER_ELECTION_ENDPOINT = 4;
    private static final int METHOD_SET_MASTER_ELECTION_GROUP_ID = 6;
    private static final int METHOD_SET_MASTER_ELECTION_PRIORITY = 2;
    private static final int METHOD_SET_PANIC_THRESHOLD = 14;
    public static final String SERVICE_NAME = "common_time.config";
    public static final int SUCCESS;
    private IBinder.DeathRecipient mDeathHandler = new IBinder.DeathRecipient()
    {
        public void binderDied()
        {
            synchronized (CommonTimeConfig.this.mListenerLock)
            {
                if (CommonTimeConfig.this.mServerDiedListener != null)
                    CommonTimeConfig.this.mServerDiedListener.onServerDied();
                return;
            }
        }
    };
    private String mInterfaceDesc = "";
    private final Object mListenerLock = new Object();
    private IBinder mRemote = null;
    private OnServerDiedListener mServerDiedListener = null;
    private CommonTimeUtils mUtils;

    public CommonTimeConfig()
        throws RemoteException
    {
        if (this.mRemote == null)
            throw new RemoteException();
        this.mInterfaceDesc = this.mRemote.getInterfaceDescriptor();
        this.mUtils = new CommonTimeUtils(this.mRemote, this.mInterfaceDesc);
        this.mRemote.linkToDeath(this.mDeathHandler, 0);
    }

    private boolean checkDeadServer()
    {
        if ((this.mRemote == null) || (this.mUtils == null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static CommonTimeConfig create()
    {
        try
        {
            localCommonTimeConfig = new CommonTimeConfig();
            return localCommonTimeConfig;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                CommonTimeConfig localCommonTimeConfig = null;
        }
    }

    private void throwOnDeadServer()
        throws RemoteException
    {
        if (checkDeadServer())
            throw new RemoteException();
    }

    protected void finalize()
        throws Throwable
    {
        release();
    }

    // ERROR //
    public int forceNetworklessMasterMode()
    {
        // Byte code:
        //     0: invokestatic 141	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     3: astore_1
        //     4: invokestatic 141	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     7: astore_2
        //     8: aload_1
        //     9: aload_0
        //     10: getfield 86	android/os/CommonTimeConfig:mInterfaceDesc	Ljava/lang/String;
        //     13: invokevirtual 145	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //     16: aload_0
        //     17: getfield 82	android/os/CommonTimeConfig:mRemote	Landroid/os/IBinder;
        //     20: bipush 17
        //     22: aload_1
        //     23: aload_2
        //     24: iconst_0
        //     25: invokeinterface 149 5 0
        //     30: pop
        //     31: aload_2
        //     32: invokevirtual 152	android/os/Parcel:readInt	()I
        //     35: istore 7
        //     37: iload 7
        //     39: istore 5
        //     41: aload_2
        //     42: invokevirtual 155	android/os/Parcel:recycle	()V
        //     45: aload_1
        //     46: invokevirtual 155	android/os/Parcel:recycle	()V
        //     49: iload 5
        //     51: ireturn
        //     52: astore 4
        //     54: bipush 249
        //     56: istore 5
        //     58: aload_2
        //     59: invokevirtual 155	android/os/Parcel:recycle	()V
        //     62: aload_1
        //     63: invokevirtual 155	android/os/Parcel:recycle	()V
        //     66: goto -17 -> 49
        //     69: astore_3
        //     70: aload_2
        //     71: invokevirtual 155	android/os/Parcel:recycle	()V
        //     74: aload_1
        //     75: invokevirtual 155	android/os/Parcel:recycle	()V
        //     78: aload_3
        //     79: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     8	37	52	android/os/RemoteException
        //     8	37	69	finally
    }

    public boolean getAutoDisable()
        throws RemoteException
    {
        int i = 1;
        throwOnDeadServer();
        if (i == this.mUtils.transactGetInt(15, i));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public int getClientSyncInterval()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetInt(11, -1);
    }

    public String getInterfaceBinding()
        throws RemoteException
    {
        throwOnDeadServer();
        String str = this.mUtils.transactGetString(7, null);
        if ((str != null) && (str.length() == 0))
            str = null;
        return str;
    }

    public int getMasterAnnounceInterval()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetInt(9, -1);
    }

    public InetSocketAddress getMasterElectionEndpoint()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetSockaddr(3);
    }

    public long getMasterElectionGroupId()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetLong(5, -1L);
    }

    public byte getMasterElectionPriority()
        throws RemoteException
    {
        throwOnDeadServer();
        return (byte)this.mUtils.transactGetInt(1, -1);
    }

    public int getPanicThreshold()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetInt(13, -1);
    }

    public void release()
    {
        if (this.mRemote != null);
        try
        {
            this.mRemote.unlinkToDeath(this.mDeathHandler, 0);
            label22: this.mRemote = null;
            this.mUtils = null;
            return;
        }
        catch (NoSuchElementException localNoSuchElementException)
        {
            break label22;
        }
    }

    public int setAutoDisable(boolean paramBoolean)
    {
        int j;
        if (checkDeadServer())
        {
            j = -7;
            return j;
        }
        CommonTimeUtils localCommonTimeUtils = this.mUtils;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            j = localCommonTimeUtils.transactSetInt(16, i);
            break;
        }
    }

    public int setClientSyncInterval(int paramInt)
    {
        if (checkDeadServer());
        for (int i = -7; ; i = this.mUtils.transactSetInt(12, paramInt))
            return i;
    }

    public int setMasterAnnounceInterval(int paramInt)
    {
        if (checkDeadServer());
        for (int i = -7; ; i = this.mUtils.transactSetInt(10, paramInt))
            return i;
    }

    public int setMasterElectionEndpoint(InetSocketAddress paramInetSocketAddress)
    {
        if (checkDeadServer());
        for (int i = -7; ; i = this.mUtils.transactSetSockaddr(4, paramInetSocketAddress))
            return i;
    }

    public int setMasterElectionGroupId(long paramLong)
    {
        if (checkDeadServer());
        for (int i = -7; ; i = this.mUtils.transactSetLong(6, paramLong))
            return i;
    }

    public int setMasterElectionPriority(byte paramByte)
    {
        if (checkDeadServer());
        for (int i = -7; ; i = this.mUtils.transactSetInt(2, paramByte))
            return i;
    }

    public int setNetworkBinding(String paramString)
    {
        if (checkDeadServer());
        CommonTimeUtils localCommonTimeUtils;
        for (int i = -7; ; i = localCommonTimeUtils.transactSetString(8, paramString))
        {
            return i;
            localCommonTimeUtils = this.mUtils;
            if (paramString == null)
                paramString = "";
        }
    }

    public int setPanicThreshold(int paramInt)
    {
        if (checkDeadServer());
        for (int i = -7; ; i = this.mUtils.transactSetInt(14, paramInt))
            return i;
    }

    public void setServerDiedListener(OnServerDiedListener paramOnServerDiedListener)
    {
        synchronized (this.mListenerLock)
        {
            this.mServerDiedListener = paramOnServerDiedListener;
            return;
        }
    }

    public static abstract interface OnServerDiedListener
    {
        public abstract void onServerDied();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.CommonTimeConfig
 * JD-Core Version:        0.6.2
 */