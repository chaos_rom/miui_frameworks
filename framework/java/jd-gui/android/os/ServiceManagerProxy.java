package android.os;

import java.util.ArrayList;

class ServiceManagerProxy
    implements IServiceManager
{
    private IBinder mRemote;

    public ServiceManagerProxy(IBinder paramIBinder)
    {
        this.mRemote = paramIBinder;
    }

    public void addService(String paramString, IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.os.IServiceManager");
        localParcel1.writeString(paramString);
        localParcel1.writeStrongBinder(paramIBinder);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel1.writeInt(i);
            this.mRemote.transact(3, localParcel1, localParcel2, 0);
            localParcel2.recycle();
            localParcel1.recycle();
            return;
        }
    }

    public IBinder asBinder()
    {
        return this.mRemote;
    }

    public IBinder checkService(String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.os.IServiceManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(2, localParcel1, localParcel2, 0);
        IBinder localIBinder = localParcel2.readStrongBinder();
        localParcel2.recycle();
        localParcel1.recycle();
        return localIBinder;
    }

    public IBinder getService(String paramString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.os.IServiceManager");
        localParcel1.writeString(paramString);
        this.mRemote.transact(1, localParcel1, localParcel2, 0);
        IBinder localIBinder = localParcel2.readStrongBinder();
        localParcel2.recycle();
        localParcel1.recycle();
        return localIBinder;
    }

    public String[] listServices()
        throws RemoteException
    {
        ArrayList localArrayList = new ArrayList();
        int i = 0;
        while (true)
        {
            Parcel localParcel1 = Parcel.obtain();
            Parcel localParcel2 = Parcel.obtain();
            localParcel1.writeInterfaceToken("android.os.IServiceManager");
            localParcel1.writeInt(i);
            i++;
            try
            {
                boolean bool = this.mRemote.transact(4, localParcel1, localParcel2, 0);
                if (!bool)
                {
                    label54: String[] arrayOfString = new String[localArrayList.size()];
                    localArrayList.toArray(arrayOfString);
                    return arrayOfString;
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                break label54;
                localArrayList.add(localParcel2.readString());
                localParcel2.recycle();
                localParcel1.recycle();
            }
        }
    }

    public void setPermissionController(IPermissionController paramIPermissionController)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeInterfaceToken("android.os.IServiceManager");
        localParcel1.writeStrongBinder(paramIPermissionController.asBinder());
        this.mRemote.transact(6, localParcel1, localParcel2, 0);
        localParcel2.recycle();
        localParcel1.recycle();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ServiceManagerProxy
 * JD-Core Version:        0.6.2
 */