package android.os;

import java.net.InetSocketAddress;
import java.util.NoSuchElementException;

public class CommonClock
{
    public static final int ERROR_ESTIMATE_UNKNOWN = 2147483647;
    public static final long INVALID_TIMELINE_ID = 0L;
    private static final int METHOD_CBK_ON_TIMELINE_CHANGED = 1;
    private static final int METHOD_COMMON_TIME_TO_LOCAL_TIME = 2;
    private static final int METHOD_GET_COMMON_FREQ = 5;
    private static final int METHOD_GET_COMMON_TIME = 4;
    private static final int METHOD_GET_ESTIMATED_ERROR = 8;
    private static final int METHOD_GET_LOCAL_FREQ = 7;
    private static final int METHOD_GET_LOCAL_TIME = 6;
    private static final int METHOD_GET_MASTER_ADDRESS = 11;
    private static final int METHOD_GET_STATE = 10;
    private static final int METHOD_GET_TIMELINE_ID = 9;
    private static final int METHOD_IS_COMMON_TIME_VALID = 1;
    private static final int METHOD_LOCAL_TIME_TO_COMMON_TIME = 3;
    private static final int METHOD_REGISTER_LISTENER = 12;
    private static final int METHOD_UNREGISTER_LISTENER = 13;
    public static final String SERVICE_NAME = "common_time.clock";
    public static final int STATE_CLIENT = 1;
    public static final int STATE_INITIAL = 0;
    public static final int STATE_INVALID = -1;
    public static final int STATE_MASTER = 2;
    public static final int STATE_RONIN = 3;
    public static final int STATE_WAIT_FOR_ELECTION = 4;
    public static final long TIME_NOT_SYNCED = -1L;
    private TimelineChangedListener mCallbackTgt = null;
    private IBinder.DeathRecipient mDeathHandler = new IBinder.DeathRecipient()
    {
        public void binderDied()
        {
            synchronized (CommonClock.this.mListenerLock)
            {
                if (CommonClock.this.mServerDiedListener != null)
                    CommonClock.this.mServerDiedListener.onServerDied();
                return;
            }
        }
    };
    private String mInterfaceDesc = "";
    private final Object mListenerLock = new Object();
    private IBinder mRemote = null;
    private OnServerDiedListener mServerDiedListener = null;
    private OnTimelineChangedListener mTimelineChangedListener = null;
    private CommonTimeUtils mUtils;

    public CommonClock()
        throws RemoteException
    {
        if (this.mRemote == null)
            throw new RemoteException();
        this.mInterfaceDesc = this.mRemote.getInterfaceDescriptor();
        this.mUtils = new CommonTimeUtils(this.mRemote, this.mInterfaceDesc);
        this.mRemote.linkToDeath(this.mDeathHandler, 0);
        registerTimelineChangeListener();
    }

    public static CommonClock create()
    {
        try
        {
            localCommonClock = new CommonClock();
            return localCommonClock;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                CommonClock localCommonClock = null;
        }
    }

    // ERROR //
    private void registerTimelineChangeListener()
        throws RemoteException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     4: ifnull +4 -> 8
        //     7: return
        //     8: invokestatic 146	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     11: astore_1
        //     12: invokestatic 146	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     15: astore_2
        //     16: aload_0
        //     17: new 8	android/os/CommonClock$TimelineChangedListener
        //     20: dup
        //     21: aload_0
        //     22: aconst_null
        //     23: invokespecial 149	android/os/CommonClock$TimelineChangedListener:<init>	(Landroid/os/CommonClock;Landroid/os/CommonClock$1;)V
        //     26: putfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     29: aload_1
        //     30: aload_0
        //     31: getfield 97	android/os/CommonClock:mInterfaceDesc	Ljava/lang/String;
        //     34: invokevirtual 153	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //     37: aload_1
        //     38: aload_0
        //     39: getfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     42: invokevirtual 157	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
        //     45: aload_0
        //     46: getfield 93	android/os/CommonClock:mRemote	Landroid/os/IBinder;
        //     49: bipush 12
        //     51: aload_1
        //     52: aload_2
        //     53: iconst_0
        //     54: invokeinterface 161 5 0
        //     59: pop
        //     60: aload_2
        //     61: invokevirtual 165	android/os/Parcel:readInt	()I
        //     64: istore 7
        //     66: iload 7
        //     68: ifne +37 -> 105
        //     71: iconst_1
        //     72: istore 5
        //     74: aload_2
        //     75: invokevirtual 168	android/os/Parcel:recycle	()V
        //     78: aload_1
        //     79: invokevirtual 168	android/os/Parcel:recycle	()V
        //     82: iload 5
        //     84: ifne -77 -> 7
        //     87: aload_0
        //     88: aconst_null
        //     89: putfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     92: aload_0
        //     93: aconst_null
        //     94: putfield 93	android/os/CommonClock:mRemote	Landroid/os/IBinder;
        //     97: aload_0
        //     98: aconst_null
        //     99: putfield 124	android/os/CommonClock:mUtils	Landroid/os/CommonTimeUtils;
        //     102: goto -95 -> 7
        //     105: iconst_0
        //     106: istore 5
        //     108: goto -34 -> 74
        //     111: astore 4
        //     113: iconst_0
        //     114: istore 5
        //     116: aload_2
        //     117: invokevirtual 168	android/os/Parcel:recycle	()V
        //     120: aload_1
        //     121: invokevirtual 168	android/os/Parcel:recycle	()V
        //     124: goto -42 -> 82
        //     127: astore_3
        //     128: aload_2
        //     129: invokevirtual 168	android/os/Parcel:recycle	()V
        //     132: aload_1
        //     133: invokevirtual 168	android/os/Parcel:recycle	()V
        //     136: aload_3
        //     137: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     29	66	111	android/os/RemoteException
        //     29	66	127	finally
    }

    private void throwOnDeadServer()
        throws RemoteException
    {
        if ((this.mRemote == null) || (this.mUtils == null))
            throw new RemoteException();
    }

    // ERROR //
    private void unregisterTimelineChangeListener()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     4: ifnonnull +4 -> 8
        //     7: return
        //     8: invokestatic 146	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     11: astore_1
        //     12: invokestatic 146	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     15: astore_2
        //     16: aload_1
        //     17: aload_0
        //     18: getfield 97	android/os/CommonClock:mInterfaceDesc	Ljava/lang/String;
        //     21: invokevirtual 153	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //     24: aload_1
        //     25: aload_0
        //     26: getfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     29: invokevirtual 157	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
        //     32: aload_0
        //     33: getfield 93	android/os/CommonClock:mRemote	Landroid/os/IBinder;
        //     36: bipush 13
        //     38: aload_1
        //     39: aload_2
        //     40: iconst_0
        //     41: invokeinterface 161 5 0
        //     46: pop
        //     47: aload_2
        //     48: invokevirtual 168	android/os/Parcel:recycle	()V
        //     51: aload_1
        //     52: invokevirtual 168	android/os/Parcel:recycle	()V
        //     55: aload_0
        //     56: aconst_null
        //     57: putfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     60: goto -53 -> 7
        //     63: astore 4
        //     65: aload_2
        //     66: invokevirtual 168	android/os/Parcel:recycle	()V
        //     69: aload_1
        //     70: invokevirtual 168	android/os/Parcel:recycle	()V
        //     73: aload_0
        //     74: aconst_null
        //     75: putfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     78: goto -71 -> 7
        //     81: astore_3
        //     82: aload_2
        //     83: invokevirtual 168	android/os/Parcel:recycle	()V
        //     86: aload_1
        //     87: invokevirtual 168	android/os/Parcel:recycle	()V
        //     90: aload_0
        //     91: aconst_null
        //     92: putfield 104	android/os/CommonClock:mCallbackTgt	Landroid/os/CommonClock$TimelineChangedListener;
        //     95: aload_3
        //     96: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     16	47	63	android/os/RemoteException
        //     16	47	81	finally
    }

    protected void finalize()
        throws Throwable
    {
        release();
    }

    public int getEstimatedError()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetInt(8, 2147483647);
    }

    public InetSocketAddress getMasterAddr()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetSockaddr(11);
    }

    public int getState()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetInt(10, -1);
    }

    public long getTime()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetLong(4, -1L);
    }

    public long getTimelineId()
        throws RemoteException
    {
        throwOnDeadServer();
        return this.mUtils.transactGetLong(9, 0L);
    }

    public void release()
    {
        unregisterTimelineChangeListener();
        if (this.mRemote != null);
        try
        {
            this.mRemote.unlinkToDeath(this.mDeathHandler, 0);
            label26: this.mRemote = null;
            this.mUtils = null;
            return;
        }
        catch (NoSuchElementException localNoSuchElementException)
        {
            break label26;
        }
    }

    public void setServerDiedListener(OnServerDiedListener paramOnServerDiedListener)
    {
        synchronized (this.mListenerLock)
        {
            this.mServerDiedListener = paramOnServerDiedListener;
            return;
        }
    }

    public void setTimelineChangedListener(OnTimelineChangedListener paramOnTimelineChangedListener)
    {
        synchronized (this.mListenerLock)
        {
            this.mTimelineChangedListener = paramOnTimelineChangedListener;
            return;
        }
    }

    private class TimelineChangedListener extends Binder
    {
        private static final String DESCRIPTOR = "android.os.ICommonClockListener";

        private TimelineChangedListener()
        {
        }

        protected boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel1.enforceInterface("android.os.ICommonClockListener");
                long l = paramParcel1.readLong();
                synchronized (CommonClock.this.mListenerLock)
                {
                    if (CommonClock.this.mTimelineChangedListener != null)
                        CommonClock.this.mTimelineChangedListener.onTimelineChanged(l);
                    bool = true;
                }
            }
        }
    }

    public static abstract interface OnServerDiedListener
    {
        public abstract void onServerDied();
    }

    public static abstract interface OnTimelineChangedListener
    {
        public abstract void onTimelineChanged(long paramLong);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.CommonClock
 * JD-Core Version:        0.6.2
 */