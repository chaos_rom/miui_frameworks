package android.os;

import android.util.AndroidRuntimeException;

public class BadParcelableException extends AndroidRuntimeException
{
    public BadParcelableException(Exception paramException)
    {
        super(paramException);
    }

    public BadParcelableException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.BadParcelableException
 * JD-Core Version:        0.6.2
 */