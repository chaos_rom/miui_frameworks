package android.os;

import android.net.INetworkManagementEventObserver;
import android.net.INetworkManagementEventObserver.Stub;
import android.net.InterfaceConfiguration;
import android.net.NetworkStats;
import android.net.RouteInfo;
import android.net.wifi.WifiConfiguration;

public abstract interface INetworkManagementService extends IInterface
{
    public abstract void addRoute(String paramString, RouteInfo paramRouteInfo)
        throws RemoteException;

    public abstract void addSecondaryRoute(String paramString, RouteInfo paramRouteInfo)
        throws RemoteException;

    public abstract void attachPppd(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
        throws RemoteException;

    public abstract void clearInterfaceAddresses(String paramString)
        throws RemoteException;

    public abstract void detachPppd(String paramString)
        throws RemoteException;

    public abstract void disableIpv6(String paramString)
        throws RemoteException;

    public abstract void disableNat(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void enableIpv6(String paramString)
        throws RemoteException;

    public abstract void enableNat(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void flushDefaultDnsCache()
        throws RemoteException;

    public abstract void flushInterfaceDnsCache(String paramString)
        throws RemoteException;

    public abstract String[] getDnsForwarders()
        throws RemoteException;

    public abstract InterfaceConfiguration getInterfaceConfig(String paramString)
        throws RemoteException;

    public abstract int getInterfaceRxThrottle(String paramString)
        throws RemoteException;

    public abstract int getInterfaceTxThrottle(String paramString)
        throws RemoteException;

    public abstract boolean getIpForwardingEnabled()
        throws RemoteException;

    public abstract NetworkStats getNetworkStatsDetail()
        throws RemoteException;

    public abstract NetworkStats getNetworkStatsSummaryDev()
        throws RemoteException;

    public abstract NetworkStats getNetworkStatsSummaryXt()
        throws RemoteException;

    public abstract NetworkStats getNetworkStatsTethering(String[] paramArrayOfString)
        throws RemoteException;

    public abstract NetworkStats getNetworkStatsUidDetail(int paramInt)
        throws RemoteException;

    public abstract RouteInfo[] getRoutes(String paramString)
        throws RemoteException;

    public abstract boolean isBandwidthControlEnabled()
        throws RemoteException;

    public abstract boolean isTetheringStarted()
        throws RemoteException;

    public abstract String[] listInterfaces()
        throws RemoteException;

    public abstract String[] listTetheredInterfaces()
        throws RemoteException;

    public abstract String[] listTtys()
        throws RemoteException;

    public abstract void registerObserver(INetworkManagementEventObserver paramINetworkManagementEventObserver)
        throws RemoteException;

    public abstract void removeInterfaceAlert(String paramString)
        throws RemoteException;

    public abstract void removeInterfaceQuota(String paramString)
        throws RemoteException;

    public abstract void removeRoute(String paramString, RouteInfo paramRouteInfo)
        throws RemoteException;

    public abstract void removeSecondaryRoute(String paramString, RouteInfo paramRouteInfo)
        throws RemoteException;

    public abstract void setAccessPoint(WifiConfiguration paramWifiConfiguration, String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setDefaultInterfaceForDns(String paramString)
        throws RemoteException;

    public abstract void setDnsForwarders(String[] paramArrayOfString)
        throws RemoteException;

    public abstract void setDnsServersForInterface(String paramString, String[] paramArrayOfString)
        throws RemoteException;

    public abstract void setGlobalAlert(long paramLong)
        throws RemoteException;

    public abstract void setInterfaceAlert(String paramString, long paramLong)
        throws RemoteException;

    public abstract void setInterfaceConfig(String paramString, InterfaceConfiguration paramInterfaceConfiguration)
        throws RemoteException;

    public abstract void setInterfaceDown(String paramString)
        throws RemoteException;

    public abstract void setInterfaceIpv6PrivacyExtensions(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void setInterfaceQuota(String paramString, long paramLong)
        throws RemoteException;

    public abstract void setInterfaceThrottle(String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setInterfaceUp(String paramString)
        throws RemoteException;

    public abstract void setIpForwardingEnabled(boolean paramBoolean)
        throws RemoteException;

    public abstract void setUidNetworkRules(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract void shutdown()
        throws RemoteException;

    public abstract void startAccessPoint(WifiConfiguration paramWifiConfiguration, String paramString1, String paramString2)
        throws RemoteException;

    public abstract void startTethering(String[] paramArrayOfString)
        throws RemoteException;

    public abstract void stopAccessPoint(String paramString)
        throws RemoteException;

    public abstract void stopTethering()
        throws RemoteException;

    public abstract void tetherInterface(String paramString)
        throws RemoteException;

    public abstract void unregisterObserver(INetworkManagementEventObserver paramINetworkManagementEventObserver)
        throws RemoteException;

    public abstract void untetherInterface(String paramString)
        throws RemoteException;

    public abstract void wifiFirmwareReload(String paramString1, String paramString2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INetworkManagementService
    {
        private static final String DESCRIPTOR = "android.os.INetworkManagementService";
        static final int TRANSACTION_addRoute = 13;
        static final int TRANSACTION_addSecondaryRoute = 15;
        static final int TRANSACTION_attachPppd = 31;
        static final int TRANSACTION_clearInterfaceAddresses = 6;
        static final int TRANSACTION_detachPppd = 32;
        static final int TRANSACTION_disableIpv6 = 10;
        static final int TRANSACTION_disableNat = 29;
        static final int TRANSACTION_enableIpv6 = 11;
        static final int TRANSACTION_enableNat = 28;
        static final int TRANSACTION_flushDefaultDnsCache = 54;
        static final int TRANSACTION_flushInterfaceDnsCache = 55;
        static final int TRANSACTION_getDnsForwarders = 27;
        static final int TRANSACTION_getInterfaceConfig = 4;
        static final int TRANSACTION_getInterfaceRxThrottle = 50;
        static final int TRANSACTION_getInterfaceTxThrottle = 51;
        static final int TRANSACTION_getIpForwardingEnabled = 18;
        static final int TRANSACTION_getNetworkStatsDetail = 39;
        static final int TRANSACTION_getNetworkStatsSummaryDev = 37;
        static final int TRANSACTION_getNetworkStatsSummaryXt = 38;
        static final int TRANSACTION_getNetworkStatsTethering = 41;
        static final int TRANSACTION_getNetworkStatsUidDetail = 40;
        static final int TRANSACTION_getRoutes = 12;
        static final int TRANSACTION_isBandwidthControlEnabled = 48;
        static final int TRANSACTION_isTetheringStarted = 22;
        static final int TRANSACTION_listInterfaces = 3;
        static final int TRANSACTION_listTetheredInterfaces = 25;
        static final int TRANSACTION_listTtys = 30;
        static final int TRANSACTION_registerObserver = 1;
        static final int TRANSACTION_removeInterfaceAlert = 45;
        static final int TRANSACTION_removeInterfaceQuota = 43;
        static final int TRANSACTION_removeRoute = 14;
        static final int TRANSACTION_removeSecondaryRoute = 16;
        static final int TRANSACTION_setAccessPoint = 36;
        static final int TRANSACTION_setDefaultInterfaceForDns = 52;
        static final int TRANSACTION_setDnsForwarders = 26;
        static final int TRANSACTION_setDnsServersForInterface = 53;
        static final int TRANSACTION_setGlobalAlert = 46;
        static final int TRANSACTION_setInterfaceAlert = 44;
        static final int TRANSACTION_setInterfaceConfig = 5;
        static final int TRANSACTION_setInterfaceDown = 7;
        static final int TRANSACTION_setInterfaceIpv6PrivacyExtensions = 9;
        static final int TRANSACTION_setInterfaceQuota = 42;
        static final int TRANSACTION_setInterfaceThrottle = 49;
        static final int TRANSACTION_setInterfaceUp = 8;
        static final int TRANSACTION_setIpForwardingEnabled = 19;
        static final int TRANSACTION_setUidNetworkRules = 47;
        static final int TRANSACTION_shutdown = 17;
        static final int TRANSACTION_startAccessPoint = 34;
        static final int TRANSACTION_startTethering = 20;
        static final int TRANSACTION_stopAccessPoint = 35;
        static final int TRANSACTION_stopTethering = 21;
        static final int TRANSACTION_tetherInterface = 23;
        static final int TRANSACTION_unregisterObserver = 2;
        static final int TRANSACTION_untetherInterface = 24;
        static final int TRANSACTION_wifiFirmwareReload = 33;

        public Stub()
        {
            attachInterface(this, "android.os.INetworkManagementService");
        }

        public static INetworkManagementService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.os.INetworkManagementService");
                if ((localIInterface != null) && ((localIInterface instanceof INetworkManagementService)))
                    localObject = (INetworkManagementService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("android.os.INetworkManagementService");
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                registerObserver(INetworkManagementEventObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                unregisterObserver(INetworkManagementEventObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String[] arrayOfString4 = listInterfaces();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString4);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                InterfaceConfiguration localInterfaceConfiguration2 = getInterfaceConfig(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localInterfaceConfiguration2 != null)
                {
                    paramParcel2.writeInt(1);
                    localInterfaceConfiguration2.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String str6 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (InterfaceConfiguration localInterfaceConfiguration1 = (InterfaceConfiguration)InterfaceConfiguration.CREATOR.createFromParcel(paramParcel1); ; localInterfaceConfiguration1 = null)
                {
                    setInterfaceConfig(str6, localInterfaceConfiguration1);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                clearInterfaceAddresses(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setInterfaceDown(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setInterfaceUp(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String str5 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (boolean bool7 = true; ; bool7 = false)
                {
                    setInterfaceIpv6PrivacyExtensions(str5, bool7);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                disableIpv6(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                enableIpv6(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                RouteInfo[] arrayOfRouteInfo = getRoutes(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeTypedArray(arrayOfRouteInfo, 1);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String str4 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (RouteInfo localRouteInfo4 = (RouteInfo)RouteInfo.CREATOR.createFromParcel(paramParcel1); ; localRouteInfo4 = null)
                {
                    addRoute(str4, localRouteInfo4);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String str3 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (RouteInfo localRouteInfo3 = (RouteInfo)RouteInfo.CREATOR.createFromParcel(paramParcel1); ; localRouteInfo3 = null)
                {
                    removeRoute(str3, localRouteInfo3);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String str2 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (RouteInfo localRouteInfo2 = (RouteInfo)RouteInfo.CREATOR.createFromParcel(paramParcel1); ; localRouteInfo2 = null)
                {
                    addSecondaryRoute(str2, localRouteInfo2);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String str1 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (RouteInfo localRouteInfo1 = (RouteInfo)RouteInfo.CREATOR.createFromParcel(paramParcel1); ; localRouteInfo1 = null)
                {
                    removeSecondaryRoute(str1, localRouteInfo1);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                shutdown();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                boolean bool6 = getIpForwardingEnabled();
                paramParcel2.writeNoException();
                if (bool6);
                for (int i1 = 1; ; i1 = 0)
                {
                    paramParcel2.writeInt(i1);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                if (paramParcel1.readInt() != 0);
                for (boolean bool5 = true; ; bool5 = false)
                {
                    setIpForwardingEnabled(bool5);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                startTethering(paramParcel1.createStringArray());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                stopTethering();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                boolean bool4 = isTetheringStarted();
                paramParcel2.writeNoException();
                if (bool4);
                for (int n = 1; ; n = 0)
                {
                    paramParcel2.writeInt(n);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                tetherInterface(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                untetherInterface(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String[] arrayOfString3 = listTetheredInterfaces();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString3);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setDnsForwarders(paramParcel1.createStringArray());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String[] arrayOfString2 = getDnsForwarders();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString2);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                enableNat(paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                disableNat(paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                String[] arrayOfString1 = listTtys();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString1);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                attachPppd(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                detachPppd(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                wifiFirmwareReload(paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                if (paramParcel1.readInt() != 0);
                for (WifiConfiguration localWifiConfiguration2 = (WifiConfiguration)WifiConfiguration.CREATOR.createFromParcel(paramParcel1); ; localWifiConfiguration2 = null)
                {
                    startAccessPoint(localWifiConfiguration2, paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                stopAccessPoint(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                if (paramParcel1.readInt() != 0);
                for (WifiConfiguration localWifiConfiguration1 = (WifiConfiguration)WifiConfiguration.CREATOR.createFromParcel(paramParcel1); ; localWifiConfiguration1 = null)
                {
                    setAccessPoint(localWifiConfiguration1, paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                NetworkStats localNetworkStats5 = getNetworkStatsSummaryDev();
                paramParcel2.writeNoException();
                if (localNetworkStats5 != null)
                {
                    paramParcel2.writeInt(1);
                    localNetworkStats5.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                NetworkStats localNetworkStats4 = getNetworkStatsSummaryXt();
                paramParcel2.writeNoException();
                if (localNetworkStats4 != null)
                {
                    paramParcel2.writeInt(1);
                    localNetworkStats4.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                NetworkStats localNetworkStats3 = getNetworkStatsDetail();
                paramParcel2.writeNoException();
                if (localNetworkStats3 != null)
                {
                    paramParcel2.writeInt(1);
                    localNetworkStats3.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                NetworkStats localNetworkStats2 = getNetworkStatsUidDetail(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localNetworkStats2 != null)
                {
                    paramParcel2.writeInt(1);
                    localNetworkStats2.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                NetworkStats localNetworkStats1 = getNetworkStatsTethering(paramParcel1.createStringArray());
                paramParcel2.writeNoException();
                if (localNetworkStats1 != null)
                {
                    paramParcel2.writeInt(1);
                    localNetworkStats1.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setInterfaceQuota(paramParcel1.readString(), paramParcel1.readLong());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                removeInterfaceQuota(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setInterfaceAlert(paramParcel1.readString(), paramParcel1.readLong());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                removeInterfaceAlert(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setGlobalAlert(paramParcel1.readLong());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                int m = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool3 = true; ; bool3 = false)
                {
                    setUidNetworkRules(m, bool3);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                boolean bool2 = isBandwidthControlEnabled();
                paramParcel2.writeNoException();
                if (bool2);
                for (int k = 1; ; k = 0)
                {
                    paramParcel2.writeInt(k);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setInterfaceThrottle(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                int j = getInterfaceRxThrottle(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(j);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                int i = getInterfaceTxThrottle(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setDefaultInterfaceForDns(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                setDnsServersForInterface(paramParcel1.readString(), paramParcel1.createStringArray());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                flushDefaultDnsCache();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.os.INetworkManagementService");
                flushInterfaceDnsCache(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
            }
        }

        private static class Proxy
            implements INetworkManagementService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addRoute(String paramString, RouteInfo paramRouteInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    if (paramRouteInfo != null)
                    {
                        localParcel1.writeInt(1);
                        paramRouteInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void addSecondaryRoute(String paramString, RouteInfo paramRouteInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    if (paramRouteInfo != null)
                    {
                        localParcel1.writeInt(1);
                        paramRouteInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(15, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void attachPppd(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    localParcel1.writeString(paramString4);
                    localParcel1.writeString(paramString5);
                    this.mRemote.transact(31, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void clearInterfaceAddresses(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void detachPppd(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void disableIpv6(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void disableNat(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enableIpv6(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enableNat(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(28, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void flushDefaultDnsCache()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(54, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void flushInterfaceDnsCache(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(55, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getDnsForwarders()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(27, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public InterfaceConfiguration getInterfaceConfig(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localInterfaceConfiguration = (InterfaceConfiguration)InterfaceConfiguration.CREATOR.createFromParcel(localParcel2);
                        return localInterfaceConfiguration;
                    }
                    InterfaceConfiguration localInterfaceConfiguration = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.os.INetworkManagementService";
            }

            public int getInterfaceRxThrottle(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(50, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getInterfaceTxThrottle(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(51, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getIpForwardingEnabled()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkStats getNetworkStatsDetail()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(39, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkStats = (NetworkStats)NetworkStats.CREATOR.createFromParcel(localParcel2);
                        return localNetworkStats;
                    }
                    NetworkStats localNetworkStats = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkStats getNetworkStatsSummaryDev()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(37, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkStats = (NetworkStats)NetworkStats.CREATOR.createFromParcel(localParcel2);
                        return localNetworkStats;
                    }
                    NetworkStats localNetworkStats = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkStats getNetworkStatsSummaryXt()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(38, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkStats = (NetworkStats)NetworkStats.CREATOR.createFromParcel(localParcel2);
                        return localNetworkStats;
                    }
                    NetworkStats localNetworkStats = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkStats getNetworkStatsTethering(String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeStringArray(paramArrayOfString);
                    this.mRemote.transact(41, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkStats = (NetworkStats)NetworkStats.CREATOR.createFromParcel(localParcel2);
                        return localNetworkStats;
                    }
                    NetworkStats localNetworkStats = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkStats getNetworkStatsUidDetail(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(40, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkStats = (NetworkStats)NetworkStats.CREATOR.createFromParcel(localParcel2);
                        return localNetworkStats;
                    }
                    NetworkStats localNetworkStats = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public RouteInfo[] getRoutes(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    RouteInfo[] arrayOfRouteInfo = (RouteInfo[])localParcel2.createTypedArray(RouteInfo.CREATOR);
                    return arrayOfRouteInfo;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isBandwidthControlEnabled()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(48, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isTetheringStarted()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] listInterfaces()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] listTetheredInterfaces()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] listTtys()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void registerObserver(INetworkManagementEventObserver paramINetworkManagementEventObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    if (paramINetworkManagementEventObserver != null)
                    {
                        localIBinder = paramINetworkManagementEventObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeInterfaceAlert(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(45, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeInterfaceQuota(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(43, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeRoute(String paramString, RouteInfo paramRouteInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    if (paramRouteInfo != null)
                    {
                        localParcel1.writeInt(1);
                        paramRouteInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(14, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeSecondaryRoute(String paramString, RouteInfo paramRouteInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    if (paramRouteInfo != null)
                    {
                        localParcel1.writeInt(1);
                        paramRouteInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(16, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAccessPoint(WifiConfiguration paramWifiConfiguration, String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    if (paramWifiConfiguration != null)
                    {
                        localParcel1.writeInt(1);
                        paramWifiConfiguration.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString1);
                        localParcel1.writeString(paramString2);
                        this.mRemote.transact(36, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setDefaultInterfaceForDns(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(52, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setDnsForwarders(String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeStringArray(paramArrayOfString);
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setDnsServersForInterface(String paramString, String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    localParcel1.writeStringArray(paramArrayOfString);
                    this.mRemote.transact(53, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setGlobalAlert(long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeLong(paramLong);
                    this.mRemote.transact(46, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInterfaceAlert(String paramString, long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    localParcel1.writeLong(paramLong);
                    this.mRemote.transact(44, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInterfaceConfig(String paramString, InterfaceConfiguration paramInterfaceConfiguration)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    if (paramInterfaceConfiguration != null)
                    {
                        localParcel1.writeInt(1);
                        paramInterfaceConfiguration.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInterfaceDown(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInterfaceIpv6PrivacyExtensions(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInterfaceQuota(String paramString, long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    localParcel1.writeLong(paramLong);
                    this.mRemote.transact(42, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInterfaceThrottle(String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(49, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInterfaceUp(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setIpForwardingEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setUidNetworkRules(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(47, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void shutdown()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void startAccessPoint(WifiConfiguration paramWifiConfiguration, String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    if (paramWifiConfiguration != null)
                    {
                        localParcel1.writeInt(1);
                        paramWifiConfiguration.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString1);
                        localParcel1.writeString(paramString2);
                        this.mRemote.transact(34, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void startTethering(String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeStringArray(paramArrayOfString);
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void stopAccessPoint(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(35, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void stopTethering()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void tetherInterface(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unregisterObserver(INetworkManagementEventObserver paramINetworkManagementEventObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    if (paramINetworkManagementEventObserver != null)
                    {
                        localIBinder = paramINetworkManagementEventObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void untetherInterface(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(24, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void wifiFirmwareReload(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.INetworkManagementService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(33, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.INetworkManagementService
 * JD-Core Version:        0.6.2
 */