package android.os;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class Process
{
    public static final String ANDROID_SHARED_MEDIA = "com.android.process.media";
    public static final int BLUETOOTH_GID = 2000;
    public static final int DRM_UID = 1019;
    public static final int FIRST_APPLICATION_UID = 10000;
    public static final int FIRST_ISOLATED_UID = 99000;
    public static final String GOOGLE_SHARED_APP_CONTENT = "com.google.process.content";
    public static final int LAST_APPLICATION_UID = 19999;
    public static final int LAST_ISOLATED_UID = 99999;
    private static final String LOG_TAG = "Process";
    public static final int LOG_UID = 1007;
    public static final int MEDIA_RW_GID = 1023;
    public static final int MEDIA_UID = 1013;
    public static final int NFC_UID = 1027;
    public static final int PHONE_UID = 1001;
    public static final int PROC_COMBINE = 256;
    public static final int PROC_OUT_FLOAT = 16384;
    public static final int PROC_OUT_LONG = 8192;
    public static final int PROC_OUT_STRING = 4096;
    public static final int PROC_PARENS = 512;
    public static final int PROC_SPACE_TERM = 32;
    public static final int PROC_TAB_TERM = 9;
    public static final int PROC_TERM_MASK = 255;
    public static final int PROC_ZERO_TERM = 0;
    public static final int SCHED_BATCH = 3;
    public static final int SCHED_FIFO = 1;
    public static final int SCHED_IDLE = 5;
    public static final int SCHED_OTHER = 0;
    public static final int SCHED_RR = 2;
    public static final int SDCARD_RW_GID = 1015;
    public static final int SHELL_UID = 2000;
    public static final int SIGNAL_KILL = 9;
    public static final int SIGNAL_QUIT = 3;
    public static final int SIGNAL_USR1 = 10;
    public static final int SYSTEM_UID = 1000;
    public static final int THREAD_GROUP_AUDIO_APP = 3;
    public static final int THREAD_GROUP_AUDIO_SYS = 4;
    public static final int THREAD_GROUP_BG_NONINTERACTIVE = 0;
    public static final int THREAD_GROUP_DEFAULT = -1;
    private static final int THREAD_GROUP_FOREGROUND = 1;
    public static final int THREAD_GROUP_SYSTEM = 2;
    public static final int THREAD_PRIORITY_AUDIO = -16;
    public static final int THREAD_PRIORITY_BACKGROUND = 10;
    public static final int THREAD_PRIORITY_DEFAULT = 0;
    public static final int THREAD_PRIORITY_DISPLAY = -4;
    public static final int THREAD_PRIORITY_FOREGROUND = -2;
    public static final int THREAD_PRIORITY_LESS_FAVORABLE = 1;
    public static final int THREAD_PRIORITY_LOWEST = 19;
    public static final int THREAD_PRIORITY_MORE_FAVORABLE = -1;
    public static final int THREAD_PRIORITY_URGENT_AUDIO = -19;
    public static final int THREAD_PRIORITY_URGENT_DISPLAY = -8;
    public static final int VPN_UID = 1016;
    public static final int WIFI_UID = 1010;
    static final int ZYGOTE_RETRY_MILLIS = 500;
    private static final String ZYGOTE_SOCKET = "zygote";
    static boolean sPreviousZygoteOpenFailed;
    static DataInputStream sZygoteInputStream;
    static LocalSocket sZygoteSocket;
    static BufferedWriter sZygoteWriter;

    public static final native long getElapsedCpuTime();

    public static final native long getFreeMemory();

    public static final native int getGidForName(String paramString);

    public static final int getParentPid(int paramInt)
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "PPid:";
        long[] arrayOfLong = new long[1];
        arrayOfLong[0] = -1L;
        readProcLines("/proc/" + paramInt + "/status", arrayOfString, arrayOfLong);
        return (int)arrayOfLong[0];
    }

    public static final native int[] getPids(String paramString, int[] paramArrayOfInt);

    public static final native int[] getPidsForCommands(String[] paramArrayOfString);

    public static final native long getPss(int paramInt);

    public static final int getThreadGroupLeader(int paramInt)
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "Tgid:";
        long[] arrayOfLong = new long[1];
        arrayOfLong[0] = -1L;
        readProcLines("/proc/" + paramInt + "/status", arrayOfString, arrayOfLong);
        return (int)arrayOfLong[0];
    }

    public static final native int getThreadPriority(int paramInt)
        throws IllegalArgumentException;

    public static final native long getTotalMemory();

    public static final native int getUidForName(String paramString);

    public static final int getUidForPid(int paramInt)
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "Uid:";
        long[] arrayOfLong = new long[1];
        arrayOfLong[0] = -1L;
        readProcLines("/proc/" + paramInt + "/status", arrayOfString, arrayOfLong);
        return (int)arrayOfLong[0];
    }

    public static final boolean isIsolated()
    {
        int i = UserId.getAppId(myUid());
        if ((i >= 99000) && (i <= 99999));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static final void killProcess(int paramInt)
    {
        sendSignal(paramInt, 9);
    }

    public static final void killProcessQuiet(int paramInt)
    {
        sendSignalQuiet(paramInt, 9);
    }

    public static final native int myPid();

    public static final native int myTid();

    public static final native int myUid();

    private static void openZygoteSocketIfNeeded()
        throws ZygoteStartFailedEx
    {
        int i;
        if (sPreviousZygoteOpenFailed)
            i = 0;
        while (true)
        {
            int j = 0;
            if ((sZygoteSocket != null) || (j >= i + 1) || (j > 0));
            try
            {
                Log.i("Zygote", "Zygote not up yet, sleeping...");
                Thread.sleep(500L);
                try
                {
                    label41: sZygoteSocket = new LocalSocket();
                    sZygoteSocket.connect(new LocalSocketAddress("zygote", LocalSocketAddress.Namespace.RESERVED));
                    sZygoteInputStream = new DataInputStream(sZygoteSocket.getInputStream());
                    sZygoteWriter = new BufferedWriter(new OutputStreamWriter(sZygoteSocket.getOutputStream()), 256);
                    Log.i("Zygote", "Process: zygote socket opened");
                    sPreviousZygoteOpenFailed = false;
                    if (sZygoteSocket == null)
                    {
                        sPreviousZygoteOpenFailed = true;
                        throw new ZygoteStartFailedEx("connect failed");
                        i = 10;
                    }
                }
                catch (IOException localIOException1)
                {
                    while (true)
                    {
                        if (sZygoteSocket != null);
                        try
                        {
                            sZygoteSocket.close();
                            sZygoteSocket = null;
                            j++;
                        }
                        catch (IOException localIOException2)
                        {
                            while (true)
                                Log.e("Process", "I/O exception on close after exception", localIOException2);
                        }
                    }
                }
                return;
            }
            catch (InterruptedException localInterruptedException)
            {
                break label41;
            }
        }
    }

    public static final native boolean parseProcLine(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int[] paramArrayOfInt, String[] paramArrayOfString, long[] paramArrayOfLong, float[] paramArrayOfFloat);

    public static final native boolean readProcFile(String paramString, int[] paramArrayOfInt, String[] paramArrayOfString, long[] paramArrayOfLong, float[] paramArrayOfFloat);

    public static final native void readProcLines(String paramString, String[] paramArrayOfString, long[] paramArrayOfLong);

    public static final native void sendSignal(int paramInt1, int paramInt2);

    public static final native void sendSignalQuiet(int paramInt1, int paramInt2);

    public static final native void setArgV0(String paramString);

    public static final native void setCanSelfBackground(boolean paramBoolean);

    public static final native int setGid(int paramInt);

    public static final native boolean setOomAdj(int paramInt1, int paramInt2);

    public static final native void setProcessGroup(int paramInt1, int paramInt2)
        throws IllegalArgumentException, SecurityException;

    public static final native void setThreadGroup(int paramInt1, int paramInt2)
        throws IllegalArgumentException, SecurityException;

    public static final native void setThreadPriority(int paramInt)
        throws IllegalArgumentException, SecurityException;

    public static final native void setThreadPriority(int paramInt1, int paramInt2)
        throws IllegalArgumentException, SecurityException;

    public static final native void setThreadScheduler(int paramInt1, int paramInt2, int paramInt3)
        throws IllegalArgumentException;

    public static final native int setUid(int paramInt);

    public static final ProcessStartResult start(String paramString1, String paramString2, int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3, int paramInt4, String[] paramArrayOfString)
    {
        try
        {
            ProcessStartResult localProcessStartResult = startViaZygote(paramString1, paramString2, paramInt1, paramInt2, paramArrayOfInt, paramInt3, paramInt4, paramArrayOfString);
            return localProcessStartResult;
        }
        catch (ZygoteStartFailedEx localZygoteStartFailedEx)
        {
            Log.e("Process", "Starting VM process through Zygote failed");
            throw new RuntimeException("Starting VM process through Zygote failed", localZygoteStartFailedEx);
        }
    }

    private static ProcessStartResult startViaZygote(String paramString1, String paramString2, int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3, int paramInt4, String[] paramArrayOfString)
        throws ZygoteStartFailedEx
    {
        try
        {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add("--runtime-init");
            localArrayList.add("--setuid=" + paramInt1);
            localArrayList.add("--setgid=" + paramInt2);
            if ((paramInt3 & 0x10) != 0)
                localArrayList.add("--enable-jni-logging");
            if ((paramInt3 & 0x8) != 0)
                localArrayList.add("--enable-safemode");
            if ((paramInt3 & 0x1) != 0)
                localArrayList.add("--enable-debugger");
            if ((paramInt3 & 0x2) != 0)
                localArrayList.add("--enable-checkjni");
            if ((paramInt3 & 0x4) != 0)
                localArrayList.add("--enable-assert");
            localArrayList.add("--target-sdk-version=" + paramInt4);
            if ((paramArrayOfInt != null) && (paramArrayOfInt.length > 0))
            {
                StringBuilder localStringBuilder = new StringBuilder();
                localStringBuilder.append("--setgroups=");
                int k = paramArrayOfInt.length;
                for (int m = 0; m < k; m++)
                {
                    if (m != 0)
                        localStringBuilder.append(',');
                    localStringBuilder.append(paramArrayOfInt[m]);
                }
                localArrayList.add(localStringBuilder.toString());
            }
            if (paramString2 != null)
                localArrayList.add("--nice-name=" + paramString2);
            localArrayList.add(paramString1);
            if (paramArrayOfString != null)
            {
                int i = paramArrayOfString.length;
                for (int j = 0; j < i; j++)
                    localArrayList.add(paramArrayOfString[j]);
            }
            ProcessStartResult localProcessStartResult = zygoteSendArgsAndGetResult(localArrayList);
            return localProcessStartResult;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @Deprecated
    public static final boolean supportsProcesses()
    {
        return true;
    }

    private static ProcessStartResult zygoteSendArgsAndGetResult(ArrayList<String> paramArrayList)
        throws ZygoteStartFailedEx
    {
        openZygoteSocketIfNeeded();
        while (true)
        {
            int j;
            String str;
            try
            {
                sZygoteWriter.write(Integer.toString(paramArrayList.size()));
                sZygoteWriter.newLine();
                int i = paramArrayList.size();
                j = 0;
                if (j < i)
                {
                    str = (String)paramArrayList.get(j);
                    if (str.indexOf('\n') >= 0)
                        throw new ZygoteStartFailedEx("embedded newlines not allowed");
                }
            }
            catch (IOException localIOException1)
            {
            }
            try
            {
                if (sZygoteSocket != null)
                    sZygoteSocket.close();
                sZygoteSocket = null;
                throw new ZygoteStartFailedEx(localIOException1);
                sZygoteWriter.write(str);
                sZygoteWriter.newLine();
                j++;
                continue;
                sZygoteWriter.flush();
                ProcessStartResult localProcessStartResult = new ProcessStartResult();
                localProcessStartResult.pid = sZygoteInputStream.readInt();
                if (localProcessStartResult.pid < 0)
                    throw new ZygoteStartFailedEx("fork() failed");
                localProcessStartResult.usingWrapper = sZygoteInputStream.readBoolean();
                return localProcessStartResult;
            }
            catch (IOException localIOException2)
            {
                while (true)
                    Log.e("Process", "I/O exception on routine close", localIOException2);
            }
        }
    }

    public static final class ProcessStartResult
    {
        public int pid;
        public boolean usingWrapper;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Process
 * JD-Core Version:        0.6.2
 */