package android.os;

import android.util.Log;
import android.util.SparseArray;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Bundle
    implements Parcelable, Cloneable
{
    public static final Parcelable.Creator<Bundle> CREATOR = new Parcelable.Creator()
    {
        public Bundle createFromParcel(Parcel paramAnonymousParcel)
        {
            return paramAnonymousParcel.readBundle();
        }

        public Bundle[] newArray(int paramAnonymousInt)
        {
            return new Bundle[paramAnonymousInt];
        }
    };
    public static final Bundle EMPTY = new Bundle();
    private static final String LOG_TAG = "Bundle";
    private boolean mAllowFds = true;
    private ClassLoader mClassLoader;
    private boolean mFdsKnown = true;
    private boolean mHasFds = false;
    Map<String, Object> mMap = null;
    Parcel mParcelledData = null;

    static
    {
        EMPTY.mMap = Collections.unmodifiableMap(new HashMap());
    }

    public Bundle()
    {
        this.mMap = new HashMap();
        this.mClassLoader = getClass().getClassLoader();
    }

    public Bundle(int paramInt)
    {
        this.mMap = new HashMap(paramInt);
        this.mClassLoader = getClass().getClassLoader();
    }

    public Bundle(Bundle paramBundle)
    {
        if (paramBundle.mParcelledData != null)
        {
            this.mParcelledData = Parcel.obtain();
            this.mParcelledData.appendFrom(paramBundle.mParcelledData, 0, paramBundle.mParcelledData.dataSize());
            this.mParcelledData.setDataPosition(0);
            if (paramBundle.mMap == null)
                break label125;
        }
        label125: for (this.mMap = new HashMap(paramBundle.mMap); ; this.mMap = null)
        {
            this.mHasFds = paramBundle.mHasFds;
            this.mFdsKnown = paramBundle.mFdsKnown;
            this.mClassLoader = paramBundle.mClassLoader;
            return;
            this.mParcelledData = null;
            break;
        }
    }

    Bundle(Parcel paramParcel)
    {
        readFromParcel(paramParcel);
    }

    Bundle(Parcel paramParcel, int paramInt)
    {
        readFromParcelInner(paramParcel, paramInt);
    }

    public Bundle(ClassLoader paramClassLoader)
    {
        this.mMap = new HashMap();
        this.mClassLoader = paramClassLoader;
    }

    public static Bundle forPair(String paramString1, String paramString2)
    {
        Bundle localBundle = new Bundle(1);
        localBundle.putString(paramString1, paramString2);
        return localBundle;
    }

    private void typeWarning(String paramString1, Object paramObject, String paramString2, ClassCastException paramClassCastException)
    {
        typeWarning(paramString1, paramObject, paramString2, "<null>", paramClassCastException);
    }

    private void typeWarning(String paramString1, Object paramObject1, String paramString2, Object paramObject2, ClassCastException paramClassCastException)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Key ");
        localStringBuilder.append(paramString1);
        localStringBuilder.append(" expected ");
        localStringBuilder.append(paramString2);
        localStringBuilder.append(" but value was a ");
        localStringBuilder.append(paramObject1.getClass().getName());
        localStringBuilder.append(".    The default value ");
        localStringBuilder.append(paramObject2);
        localStringBuilder.append(" was returned.");
        Log.w("Bundle", localStringBuilder.toString());
        Log.w("Bundle", "Attempt to cast generated internal exception:", paramClassCastException);
    }

    public void clear()
    {
        unparcel();
        this.mMap.clear();
        this.mHasFds = false;
        this.mFdsKnown = true;
    }

    public Object clone()
    {
        return new Bundle(this);
    }

    public boolean containsKey(String paramString)
    {
        unparcel();
        return this.mMap.containsKey(paramString);
    }

    public int describeContents()
    {
        int i = 0;
        if (hasFileDescriptors())
            i = 0x0 | 0x1;
        return i;
    }

    public Object get(String paramString)
    {
        unparcel();
        return this.mMap.get(paramString);
    }

    public boolean getBoolean(String paramString)
    {
        unparcel();
        return getBoolean(paramString, false);
    }

    public boolean getBoolean(String paramString, boolean paramBoolean)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramBoolean;
            try
            {
                boolean bool = ((Boolean)localObject).booleanValue();
                paramBoolean = bool;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Boolean", Boolean.valueOf(paramBoolean), localClassCastException);
            }
        }
    }

    public boolean[] getBooleanArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        boolean[] arrayOfBoolean;
        if (localObject == null)
            arrayOfBoolean = null;
        while (true)
        {
            return arrayOfBoolean;
            try
            {
                arrayOfBoolean = (boolean[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "byte[]", localClassCastException);
                arrayOfBoolean = null;
            }
        }
    }

    public Bundle getBundle(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        Bundle localBundle;
        if (localObject == null)
            localBundle = null;
        while (true)
        {
            return localBundle;
            try
            {
                localBundle = (Bundle)localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Bundle", localClassCastException);
                localBundle = null;
            }
        }
    }

    public byte getByte(String paramString)
    {
        unparcel();
        return getByte(paramString, (byte)0).byteValue();
    }

    public Byte getByte(String paramString, byte paramByte)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        Byte localByte;
        if (localObject == null)
            localByte = Byte.valueOf(paramByte);
        while (true)
        {
            return localByte;
            try
            {
                localByte = (Byte)localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Byte", Byte.valueOf(paramByte), localClassCastException);
                localByte = Byte.valueOf(paramByte);
            }
        }
    }

    public byte[] getByteArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        byte[] arrayOfByte;
        if (localObject == null)
            arrayOfByte = null;
        while (true)
        {
            return arrayOfByte;
            try
            {
                arrayOfByte = (byte[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "byte[]", localClassCastException);
                arrayOfByte = null;
            }
        }
    }

    public char getChar(String paramString)
    {
        unparcel();
        return getChar(paramString, '\000');
    }

    public char getChar(String paramString, char paramChar)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramChar;
            try
            {
                char c = ((Character)localObject).charValue();
                paramChar = c;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Character", Character.valueOf(paramChar), localClassCastException);
            }
        }
    }

    public char[] getCharArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        char[] arrayOfChar;
        if (localObject == null)
            arrayOfChar = null;
        while (true)
        {
            return arrayOfChar;
            try
            {
                arrayOfChar = (char[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "char[]", localClassCastException);
                arrayOfChar = null;
            }
        }
    }

    public CharSequence getCharSequence(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        CharSequence localCharSequence;
        if (localObject == null)
            localCharSequence = null;
        while (true)
        {
            return localCharSequence;
            try
            {
                localCharSequence = (CharSequence)localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "CharSequence", localClassCastException);
                localCharSequence = null;
            }
        }
    }

    public CharSequence getCharSequence(String paramString, CharSequence paramCharSequence)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramCharSequence;
            try
            {
                CharSequence localCharSequence = (CharSequence)localObject;
                paramCharSequence = localCharSequence;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "CharSequence", localClassCastException);
            }
        }
    }

    public CharSequence[] getCharSequenceArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        CharSequence[] arrayOfCharSequence;
        if (localObject == null)
            arrayOfCharSequence = null;
        while (true)
        {
            return arrayOfCharSequence;
            try
            {
                arrayOfCharSequence = (CharSequence[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "CharSequence[]", localClassCastException);
                arrayOfCharSequence = null;
            }
        }
    }

    public ArrayList<CharSequence> getCharSequenceArrayList(String paramString)
    {
        unparcel();
        Object localObject1 = this.mMap.get(paramString);
        Object localObject2;
        if (localObject1 == null)
            localObject2 = null;
        while (true)
        {
            return localObject2;
            try
            {
                localObject2 = (ArrayList)localObject1;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject1, "ArrayList<CharSequence>", localClassCastException);
                localObject2 = null;
            }
        }
    }

    public ClassLoader getClassLoader()
    {
        return this.mClassLoader;
    }

    public double getDouble(String paramString)
    {
        unparcel();
        return getDouble(paramString, 0.0D);
    }

    public double getDouble(String paramString, double paramDouble)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramDouble;
            try
            {
                double d = ((Double)localObject).doubleValue();
                paramDouble = d;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Double", Double.valueOf(paramDouble), localClassCastException);
            }
        }
    }

    public double[] getDoubleArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        double[] arrayOfDouble;
        if (localObject == null)
            arrayOfDouble = null;
        while (true)
        {
            return arrayOfDouble;
            try
            {
                arrayOfDouble = (double[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "double[]", localClassCastException);
                arrayOfDouble = null;
            }
        }
    }

    public float getFloat(String paramString)
    {
        unparcel();
        return getFloat(paramString, 0.0F);
    }

    public float getFloat(String paramString, float paramFloat)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramFloat;
            try
            {
                float f = ((Float)localObject).floatValue();
                paramFloat = f;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Float", Float.valueOf(paramFloat), localClassCastException);
            }
        }
    }

    public float[] getFloatArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        float[] arrayOfFloat;
        if (localObject == null)
            arrayOfFloat = null;
        while (true)
        {
            return arrayOfFloat;
            try
            {
                arrayOfFloat = (float[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "float[]", localClassCastException);
                arrayOfFloat = null;
            }
        }
    }

    @Deprecated
    public IBinder getIBinder(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        IBinder localIBinder;
        if (localObject == null)
            localIBinder = null;
        while (true)
        {
            return localIBinder;
            try
            {
                localIBinder = (IBinder)localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "IBinder", localClassCastException);
                localIBinder = null;
            }
        }
    }

    public int getInt(String paramString)
    {
        unparcel();
        return getInt(paramString, 0);
    }

    public int getInt(String paramString, int paramInt)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramInt;
            try
            {
                int i = ((Integer)localObject).intValue();
                paramInt = i;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Integer", Integer.valueOf(paramInt), localClassCastException);
            }
        }
    }

    public int[] getIntArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        int[] arrayOfInt;
        if (localObject == null)
            arrayOfInt = null;
        while (true)
        {
            return arrayOfInt;
            try
            {
                arrayOfInt = (int[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "int[]", localClassCastException);
                arrayOfInt = null;
            }
        }
    }

    public ArrayList<Integer> getIntegerArrayList(String paramString)
    {
        unparcel();
        Object localObject1 = this.mMap.get(paramString);
        Object localObject2;
        if (localObject1 == null)
            localObject2 = null;
        while (true)
        {
            return localObject2;
            try
            {
                localObject2 = (ArrayList)localObject1;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject1, "ArrayList<Integer>", localClassCastException);
                localObject2 = null;
            }
        }
    }

    public long getLong(String paramString)
    {
        unparcel();
        return getLong(paramString, 0L);
    }

    public long getLong(String paramString, long paramLong)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramLong;
            try
            {
                long l = ((Long)localObject).longValue();
                paramLong = l;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Long", Long.valueOf(paramLong), localClassCastException);
            }
        }
    }

    public long[] getLongArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        long[] arrayOfLong;
        if (localObject == null)
            arrayOfLong = null;
        while (true)
        {
            return arrayOfLong;
            try
            {
                arrayOfLong = (long[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "long[]", localClassCastException);
                arrayOfLong = null;
            }
        }
    }

    public String getPairValue()
    {
        unparcel();
        int i = this.mMap.size();
        if (i > 1)
            Log.w("Bundle", "getPairValue() used on Bundle with multiple pairs.");
        String str;
        if (i == 0)
            str = null;
        while (true)
        {
            return str;
            Object localObject = this.mMap.values().iterator().next();
            try
            {
                str = (String)localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning("getPairValue()", localObject, "String", localClassCastException);
                str = null;
            }
        }
    }

    public <T extends Parcelable> T getParcelable(String paramString)
    {
        unparcel();
        Object localObject1 = this.mMap.get(paramString);
        Object localObject2;
        if (localObject1 == null)
            localObject2 = null;
        while (true)
        {
            return localObject2;
            try
            {
                localObject2 = (Cloneable)localObject1;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject1, "Parcelable", localClassCastException);
                localObject2 = null;
            }
        }
    }

    public Parcelable[] getParcelableArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        Parcelable[] arrayOfParcelable;
        if (localObject == null)
            arrayOfParcelable = null;
        while (true)
        {
            return arrayOfParcelable;
            try
            {
                arrayOfParcelable = (Parcelable[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Parcelable[]", localClassCastException);
                arrayOfParcelable = null;
            }
        }
    }

    public <T extends Parcelable> ArrayList<T> getParcelableArrayList(String paramString)
    {
        unparcel();
        Object localObject1 = this.mMap.get(paramString);
        Object localObject2;
        if (localObject1 == null)
            localObject2 = null;
        while (true)
        {
            return localObject2;
            try
            {
                localObject2 = (ArrayList)localObject1;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject1, "ArrayList", localClassCastException);
                localObject2 = null;
            }
        }
    }

    public Serializable getSerializable(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        Serializable localSerializable;
        if (localObject == null)
            localSerializable = null;
        while (true)
        {
            return localSerializable;
            try
            {
                localSerializable = (Serializable)localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Serializable", localClassCastException);
                localSerializable = null;
            }
        }
    }

    public short getShort(String paramString)
    {
        unparcel();
        return getShort(paramString, (short)0);
    }

    public short getShort(String paramString, short paramShort)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramShort;
            try
            {
                short s = ((Short)localObject).shortValue();
                paramShort = s;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "Short", Short.valueOf(paramShort), localClassCastException);
            }
        }
    }

    public short[] getShortArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        short[] arrayOfShort;
        if (localObject == null)
            arrayOfShort = null;
        while (true)
        {
            return arrayOfShort;
            try
            {
                arrayOfShort = (short[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "short[]", localClassCastException);
                arrayOfShort = null;
            }
        }
    }

    public <T extends Parcelable> SparseArray<T> getSparseParcelableArray(String paramString)
    {
        unparcel();
        Object localObject1 = this.mMap.get(paramString);
        Object localObject2;
        if (localObject1 == null)
            localObject2 = null;
        while (true)
        {
            return localObject2;
            try
            {
                localObject2 = (SparseArray)localObject1;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject1, "SparseArray", localClassCastException);
                localObject2 = null;
            }
        }
    }

    public String getString(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        String str;
        if (localObject == null)
            str = null;
        while (true)
        {
            return str;
            try
            {
                str = (String)localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "String", localClassCastException);
                str = null;
            }
        }
    }

    public String getString(String paramString1, String paramString2)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString1);
        if (localObject == null);
        while (true)
        {
            return paramString2;
            try
            {
                String str = (String)localObject;
                paramString2 = str;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString1, localObject, "String", localClassCastException);
            }
        }
    }

    public String[] getStringArray(String paramString)
    {
        unparcel();
        Object localObject = this.mMap.get(paramString);
        String[] arrayOfString;
        if (localObject == null)
            arrayOfString = null;
        while (true)
        {
            return arrayOfString;
            try
            {
                arrayOfString = (String[])localObject;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject, "String[]", localClassCastException);
                arrayOfString = null;
            }
        }
    }

    public ArrayList<String> getStringArrayList(String paramString)
    {
        unparcel();
        Object localObject1 = this.mMap.get(paramString);
        Object localObject2;
        if (localObject1 == null)
            localObject2 = null;
        while (true)
        {
            return localObject2;
            try
            {
                localObject2 = (ArrayList)localObject1;
            }
            catch (ClassCastException localClassCastException)
            {
                typeWarning(paramString, localObject1, "ArrayList<String>", localClassCastException);
                localObject2 = null;
            }
        }
    }

    public boolean hasFileDescriptors()
    {
        boolean bool;
        if (!this.mFdsKnown)
        {
            bool = false;
            if (this.mParcelledData != null)
            {
                if (this.mParcelledData.hasFileDescriptors())
                    bool = true;
                this.mHasFds = bool;
                this.mFdsKnown = true;
            }
        }
        else
        {
            return this.mHasFds;
        }
        Iterator localIterator = this.mMap.entrySet().iterator();
        label165: label312: 
        while (true)
        {
            label58: label224: ArrayList localArrayList;
            if ((!bool) && (localIterator.hasNext()))
            {
                Object localObject = ((Map.Entry)localIterator.next()).getValue();
                if ((localObject instanceof Cloneable))
                {
                    if ((0x1 & ((Cloneable)localObject).describeContents()) == 0)
                        continue;
                    bool = true;
                    break;
                }
                if ((localObject instanceof Parcelable[]))
                {
                    Parcelable[] arrayOfParcelable = (Parcelable[])localObject;
                    for (int k = -1 + arrayOfParcelable.length; ; k--)
                    {
                        if (k < 0)
                            break label165;
                        if ((0x1 & arrayOfParcelable[k].describeContents()) != 0)
                        {
                            bool = true;
                            break;
                        }
                    }
                    continue;
                }
                if ((localObject instanceof SparseArray))
                {
                    SparseArray localSparseArray = (SparseArray)localObject;
                    for (int j = -1 + localSparseArray.size(); ; j--)
                    {
                        if (j < 0)
                            break label224;
                        if ((0x1 & ((Cloneable)localSparseArray.get(j)).describeContents()) != 0)
                        {
                            bool = true;
                            break;
                        }
                    }
                    continue;
                }
                if (!(localObject instanceof ArrayList))
                    continue;
                localArrayList = (ArrayList)localObject;
                if ((localArrayList.size() <= 0) || (!(localArrayList.get(0) instanceof Cloneable)))
                    continue;
            }
            for (int i = -1 + localArrayList.size(); ; i--)
            {
                if (i < 0)
                    break label312;
                Parcelable localParcelable = (Cloneable)localArrayList.get(i);
                if ((localParcelable != null) && ((0x1 & localParcelable.describeContents()) != 0))
                {
                    bool = true;
                    break label58;
                    break;
                }
            }
        }
    }

    public boolean isEmpty()
    {
        unparcel();
        return this.mMap.isEmpty();
    }

    public boolean isParcelled()
    {
        if (this.mParcelledData != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public Set<String> keySet()
    {
        unparcel();
        return this.mMap.keySet();
    }

    public void putAll(Bundle paramBundle)
    {
        unparcel();
        paramBundle.unparcel();
        this.mMap.putAll(paramBundle.mMap);
        this.mHasFds |= paramBundle.mHasFds;
        if ((this.mFdsKnown) && (paramBundle.mFdsKnown));
        for (boolean bool = true; ; bool = false)
        {
            this.mFdsKnown = bool;
            return;
        }
    }

    public void putBoolean(String paramString, boolean paramBoolean)
    {
        unparcel();
        this.mMap.put(paramString, Boolean.valueOf(paramBoolean));
    }

    public void putBooleanArray(String paramString, boolean[] paramArrayOfBoolean)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfBoolean);
    }

    public void putBundle(String paramString, Bundle paramBundle)
    {
        unparcel();
        this.mMap.put(paramString, paramBundle);
    }

    public void putByte(String paramString, byte paramByte)
    {
        unparcel();
        this.mMap.put(paramString, Byte.valueOf(paramByte));
    }

    public void putByteArray(String paramString, byte[] paramArrayOfByte)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfByte);
    }

    public void putChar(String paramString, char paramChar)
    {
        unparcel();
        this.mMap.put(paramString, Character.valueOf(paramChar));
    }

    public void putCharArray(String paramString, char[] paramArrayOfChar)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfChar);
    }

    public void putCharSequence(String paramString, CharSequence paramCharSequence)
    {
        unparcel();
        this.mMap.put(paramString, paramCharSequence);
    }

    public void putCharSequenceArray(String paramString, CharSequence[] paramArrayOfCharSequence)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfCharSequence);
    }

    public void putCharSequenceArrayList(String paramString, ArrayList<CharSequence> paramArrayList)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayList);
    }

    public void putDouble(String paramString, double paramDouble)
    {
        unparcel();
        this.mMap.put(paramString, Double.valueOf(paramDouble));
    }

    public void putDoubleArray(String paramString, double[] paramArrayOfDouble)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfDouble);
    }

    public void putFloat(String paramString, float paramFloat)
    {
        unparcel();
        this.mMap.put(paramString, Float.valueOf(paramFloat));
    }

    public void putFloatArray(String paramString, float[] paramArrayOfFloat)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfFloat);
    }

    @Deprecated
    public void putIBinder(String paramString, IBinder paramIBinder)
    {
        unparcel();
        this.mMap.put(paramString, paramIBinder);
    }

    public void putInt(String paramString, int paramInt)
    {
        unparcel();
        this.mMap.put(paramString, Integer.valueOf(paramInt));
    }

    public void putIntArray(String paramString, int[] paramArrayOfInt)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfInt);
    }

    public void putIntegerArrayList(String paramString, ArrayList<Integer> paramArrayList)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayList);
    }

    public void putLong(String paramString, long paramLong)
    {
        unparcel();
        this.mMap.put(paramString, Long.valueOf(paramLong));
    }

    public void putLongArray(String paramString, long[] paramArrayOfLong)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfLong);
    }

    public void putParcelable(String paramString, Parcelable paramParcelable)
    {
        unparcel();
        this.mMap.put(paramString, paramParcelable);
        this.mFdsKnown = false;
    }

    public void putParcelableArray(String paramString, Parcelable[] paramArrayOfParcelable)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfParcelable);
        this.mFdsKnown = false;
    }

    public void putParcelableArrayList(String paramString, ArrayList<? extends Parcelable> paramArrayList)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayList);
        this.mFdsKnown = false;
    }

    public void putSerializable(String paramString, Serializable paramSerializable)
    {
        unparcel();
        this.mMap.put(paramString, paramSerializable);
    }

    public void putShort(String paramString, short paramShort)
    {
        unparcel();
        this.mMap.put(paramString, Short.valueOf(paramShort));
    }

    public void putShortArray(String paramString, short[] paramArrayOfShort)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfShort);
    }

    public void putSparseParcelableArray(String paramString, SparseArray<? extends Parcelable> paramSparseArray)
    {
        unparcel();
        this.mMap.put(paramString, paramSparseArray);
        this.mFdsKnown = false;
    }

    public void putString(String paramString1, String paramString2)
    {
        unparcel();
        this.mMap.put(paramString1, paramString2);
    }

    public void putStringArray(String paramString, String[] paramArrayOfString)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayOfString);
    }

    public void putStringArrayList(String paramString, ArrayList<String> paramArrayList)
    {
        unparcel();
        this.mMap.put(paramString, paramArrayList);
    }

    public void readFromParcel(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        if (i < 0)
            throw new RuntimeException("Bad length in parcel: " + i);
        readFromParcelInner(paramParcel, i);
    }

    void readFromParcelInner(Parcel paramParcel, int paramInt)
    {
        if (paramParcel.readInt() != 1279544898)
        {
            String str = Log.getStackTraceString(new RuntimeException());
            Log.e("Bundle", "readBundle: bad magic number");
            Log.e("Bundle", "readBundle: trace = " + str);
        }
        int i = paramParcel.dataPosition();
        paramParcel.setDataPosition(i + paramInt);
        Parcel localParcel = Parcel.obtain();
        localParcel.setDataPosition(0);
        localParcel.appendFrom(paramParcel, i, paramInt);
        localParcel.setDataPosition(0);
        this.mParcelledData = localParcel;
        this.mHasFds = localParcel.hasFileDescriptors();
        this.mFdsKnown = true;
    }

    public void remove(String paramString)
    {
        unparcel();
        this.mMap.remove(paramString);
    }

    public boolean setAllowFds(boolean paramBoolean)
    {
        boolean bool = this.mAllowFds;
        this.mAllowFds = paramBoolean;
        return bool;
    }

    public void setClassLoader(ClassLoader paramClassLoader)
    {
        this.mClassLoader = paramClassLoader;
    }

    public int size()
    {
        unparcel();
        return this.mMap.size();
    }

    /** @deprecated */
    public String toString()
    {
        try
        {
            String str2;
            if (this.mParcelledData != null)
                str2 = "Bundle[mParcelledData.dataSize=" + this.mParcelledData.dataSize() + "]";
            String str1;
            for (Object localObject2 = str2; ; localObject2 = str1)
            {
                return localObject2;
                str1 = "Bundle[" + this.mMap.toString() + "]";
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    void unparcel()
    {
        try
        {
            Parcel localParcel = this.mParcelledData;
            if (localParcel == null);
            while (true)
            {
                return;
                int i = this.mParcelledData.readInt();
                if (i >= 0)
                {
                    if (this.mMap == null)
                        this.mMap = new HashMap();
                    this.mParcelledData.readMapInternal(this.mMap, i, this.mClassLoader);
                    this.mParcelledData.recycle();
                    this.mParcelledData = null;
                }
            }
        }
        finally
        {
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        boolean bool = paramParcel.pushAllowFds(this.mAllowFds);
        try
        {
            if (this.mParcelledData != null)
            {
                int k = this.mParcelledData.dataSize();
                paramParcel.writeInt(k);
                paramParcel.writeInt(1279544898);
                paramParcel.appendFrom(this.mParcelledData, 0, k);
            }
            while (true)
            {
                return;
                paramParcel.writeInt(-1);
                paramParcel.writeInt(1279544898);
                int i = paramParcel.dataPosition();
                paramParcel.writeMapInternal(this.mMap);
                int j = paramParcel.dataPosition();
                paramParcel.setDataPosition(i - 8);
                paramParcel.writeInt(j - i);
                paramParcel.setDataPosition(j);
            }
        }
        finally
        {
            paramParcel.restoreAllowFds(bool);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Bundle
 * JD-Core Version:        0.6.2
 */