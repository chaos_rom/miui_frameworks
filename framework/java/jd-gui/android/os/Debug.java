package android.os;

import android.util.Log;
import com.android.internal.util.TypedProperties;
import dalvik.bytecode.OpcodeInfo;
import dalvik.system.VMDebug;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import org.apache.harmony.dalvik.ddmc.Chunk;
import org.apache.harmony.dalvik.ddmc.ChunkHandler;
import org.apache.harmony.dalvik.ddmc.DdmServer;

public final class Debug
{
    private static final String DEFAULT_TRACE_BODY = "dmtrace";
    private static final String DEFAULT_TRACE_EXTENSION = ".trace";
    private static final String DEFAULT_TRACE_FILE_PATH = DEFAULT_TRACE_PATH_PREFIX + "dmtrace" + ".trace";
    private static final String DEFAULT_TRACE_PATH_PREFIX;
    private static final int MIN_DEBUGGER_IDLE = 1300;
    public static final int SHOW_CLASSLOADER = 2;
    public static final int SHOW_FULL_DETAIL = 1;
    public static final int SHOW_INITIALIZED = 4;
    private static final int SPIN_DELAY = 200;
    private static final String SYSFS_QEMU_TRACE_STATE = "/sys/qemu_trace/state";
    private static final String TAG = "Debug";
    public static final int TRACE_COUNT_ALLOCS = 1;
    private static final TypedProperties debugProperties = null;
    private static volatile boolean mWaiting = false;

    static
    {
        DEFAULT_TRACE_PATH_PREFIX = Environment.getExternalStorageDirectory().getPath() + "/";
    }

    public static final boolean cacheRegisterMap(String paramString)
    {
        return VMDebug.cacheRegisterMap(paramString);
    }

    @Deprecated
    public static void changeDebugPort(int paramInt)
    {
    }

    public static long countInstancesOfClass(Class paramClass)
    {
        return VMDebug.countInstancesOfClass(paramClass, true);
    }

    public static void dumpHprofData(String paramString)
        throws IOException
    {
        VMDebug.dumpHprofData(paramString);
    }

    public static void dumpHprofData(String paramString, FileDescriptor paramFileDescriptor)
        throws IOException
    {
        VMDebug.dumpHprofData(paramString, paramFileDescriptor);
    }

    public static void dumpHprofDataDdms()
    {
        VMDebug.dumpHprofDataDdms();
    }

    public static native void dumpNativeBacktraceToFile(int paramInt, String paramString);

    public static native void dumpNativeHeap(FileDescriptor paramFileDescriptor);

    public static final void dumpReferenceTables()
    {
        VMDebug.dumpReferenceTables();
    }

    public static boolean dumpService(String paramString, FileDescriptor paramFileDescriptor, String[] paramArrayOfString)
    {
        boolean bool = false;
        IBinder localIBinder = ServiceManager.getService(paramString);
        if (localIBinder == null)
            Log.e("Debug", "Can't find service to dump: " + paramString);
        while (true)
        {
            return bool;
            try
            {
                localIBinder.dump(paramFileDescriptor, paramArrayOfString);
                bool = true;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("Debug", "Can't dump service: " + paramString, localRemoteException);
            }
        }
    }

    public static void enableEmulatorTraceOutput()
    {
        VMDebug.startEmulatorTracing();
    }

    // ERROR //
    private static boolean fieldTypeMatches(Field paramField, Class<?> paramClass)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: aload_0
        //     3: invokevirtual 161	java/lang/reflect/Field:getType	()Ljava/lang/Class;
        //     6: astore_3
        //     7: aload_3
        //     8: aload_1
        //     9: if_acmpne +7 -> 16
        //     12: iconst_1
        //     13: istore_2
        //     14: iload_2
        //     15: ireturn
        //     16: aload_1
        //     17: ldc 163
        //     19: invokevirtual 169	java/lang/Class:getField	(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //     22: astore 5
        //     24: aload 5
        //     26: aconst_null
        //     27: invokevirtual 173	java/lang/reflect/Field:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     30: checkcast 165	java/lang/Class
        //     33: astore 7
        //     35: aload_3
        //     36: aload 7
        //     38: if_acmpne +17 -> 55
        //     41: iconst_1
        //     42: istore 8
        //     44: iload 8
        //     46: istore_2
        //     47: goto -33 -> 14
        //     50: astore 4
        //     52: goto -38 -> 14
        //     55: iconst_0
        //     56: istore 8
        //     58: goto -14 -> 44
        //     61: astore 6
        //     63: goto -49 -> 14
        //
        // Exception table:
        //     from	to	target	type
        //     16	24	50	java/lang/NoSuchFieldException
        //     24	35	61	java/lang/IllegalAccessException
    }

    public static final native int getBinderDeathObjectCount();

    public static final native int getBinderLocalObjectCount();

    public static final native int getBinderProxyObjectCount();

    public static native int getBinderReceivedTransactions();

    public static native int getBinderSentTransactions();

    public static String getCaller()
    {
        return getCaller(Thread.currentThread().getStackTrace(), 0);
    }

    private static String getCaller(StackTraceElement[] paramArrayOfStackTraceElement, int paramInt)
    {
        if (paramInt + 4 >= paramArrayOfStackTraceElement.length);
        StackTraceElement localStackTraceElement;
        for (String str = "<bottom of call stack>"; ; str = localStackTraceElement.getClassName() + "." + localStackTraceElement.getMethodName() + ":" + localStackTraceElement.getLineNumber())
        {
            return str;
            localStackTraceElement = paramArrayOfStackTraceElement[(paramInt + 4)];
        }
    }

    public static String getCallers(int paramInt)
    {
        StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
        StringBuffer localStringBuffer = new StringBuffer();
        for (int i = 0; i < paramInt; i++)
            localStringBuffer.append(getCaller(arrayOfStackTraceElement, i)).append(" ");
        return localStringBuffer.toString();
    }

    public static int getGlobalAllocCount()
    {
        return VMDebug.getAllocCount(1);
    }

    public static int getGlobalAllocSize()
    {
        return VMDebug.getAllocCount(2);
    }

    public static int getGlobalClassInitCount()
    {
        return VMDebug.getAllocCount(32);
    }

    public static int getGlobalClassInitTime()
    {
        return VMDebug.getAllocCount(64);
    }

    @Deprecated
    public static int getGlobalExternalAllocCount()
    {
        return 0;
    }

    @Deprecated
    public static int getGlobalExternalAllocSize()
    {
        return 0;
    }

    @Deprecated
    public static int getGlobalExternalFreedCount()
    {
        return 0;
    }

    @Deprecated
    public static int getGlobalExternalFreedSize()
    {
        return 0;
    }

    public static int getGlobalFreedCount()
    {
        return VMDebug.getAllocCount(4);
    }

    public static int getGlobalFreedSize()
    {
        return VMDebug.getAllocCount(8);
    }

    public static int getGlobalGcInvocationCount()
    {
        return VMDebug.getAllocCount(16);
    }

    public static int getLoadedClassCount()
    {
        return VMDebug.getLoadedClassCount();
    }

    public static native void getMemoryInfo(int paramInt, MemoryInfo paramMemoryInfo);

    public static native void getMemoryInfo(MemoryInfo paramMemoryInfo);

    public static native long getNativeHeapAllocatedSize();

    public static native long getNativeHeapFreeSize();

    public static native long getNativeHeapSize();

    public static native long getPss();

    public static native long getPss(int paramInt);

    public static int getThreadAllocCount()
    {
        return VMDebug.getAllocCount(65536);
    }

    public static int getThreadAllocSize()
    {
        return VMDebug.getAllocCount(131072);
    }

    @Deprecated
    public static int getThreadExternalAllocCount()
    {
        return 0;
    }

    @Deprecated
    public static int getThreadExternalAllocSize()
    {
        return 0;
    }

    public static int getThreadGcInvocationCount()
    {
        return VMDebug.getAllocCount(1048576);
    }

    public static String[] getVmFeatureList()
    {
        return VMDebug.getVmFeatureList();
    }

    public static boolean isDebuggerConnected()
    {
        return VMDebug.isDebuggerConnected();
    }

    public static boolean isMethodTracingActive()
    {
        return VMDebug.isMethodTracingActive();
    }

    private static void modifyFieldIfSet(Field paramField, TypedProperties paramTypedProperties, String paramString)
    {
        if (paramField.getType() == String.class)
        {
            int i = paramTypedProperties.getStringInfo(paramString);
            switch (i)
            {
            default:
                throw new IllegalStateException("Unexpected getStringInfo(" + paramString + ") return value " + i);
            case 0:
            case -1:
            case -2:
            case 1:
            }
        }
        while (true)
        {
            try
            {
                paramField.set(null, null);
                return;
            }
            catch (IllegalAccessException localIllegalAccessException2)
            {
                throw new IllegalArgumentException("Cannot set field for " + paramString, localIllegalAccessException2);
            }
            throw new IllegalArgumentException("Type of " + paramString + " " + " does not match field type (" + paramField.getType() + ")");
            Object localObject = paramTypedProperties.get(paramString);
            if (localObject == null)
                continue;
            if (!fieldTypeMatches(paramField, localObject.getClass()))
                throw new IllegalArgumentException("Type of " + paramString + " (" + localObject.getClass() + ") " + " does not match field type (" + paramField.getType() + ")");
            try
            {
                paramField.set(null, localObject);
            }
            catch (IllegalAccessException localIllegalAccessException1)
            {
                throw new IllegalArgumentException("Cannot set field for " + paramString, localIllegalAccessException1);
            }
        }
    }

    public static void printLoadedClasses(int paramInt)
    {
        VMDebug.printLoadedClasses(paramInt);
    }

    public static void resetAllCounts()
    {
        VMDebug.resetAllocCount(-1);
    }

    public static void resetGlobalAllocCount()
    {
        VMDebug.resetAllocCount(1);
    }

    public static void resetGlobalAllocSize()
    {
        VMDebug.resetAllocCount(2);
    }

    public static void resetGlobalClassInitCount()
    {
        VMDebug.resetAllocCount(32);
    }

    public static void resetGlobalClassInitTime()
    {
        VMDebug.resetAllocCount(64);
    }

    @Deprecated
    public static void resetGlobalExternalAllocCount()
    {
    }

    @Deprecated
    public static void resetGlobalExternalAllocSize()
    {
    }

    @Deprecated
    public static void resetGlobalExternalFreedCount()
    {
    }

    @Deprecated
    public static void resetGlobalExternalFreedSize()
    {
    }

    public static void resetGlobalFreedCount()
    {
        VMDebug.resetAllocCount(4);
    }

    public static void resetGlobalFreedSize()
    {
        VMDebug.resetAllocCount(8);
    }

    public static void resetGlobalGcInvocationCount()
    {
        VMDebug.resetAllocCount(16);
    }

    public static void resetThreadAllocCount()
    {
        VMDebug.resetAllocCount(65536);
    }

    public static void resetThreadAllocSize()
    {
        VMDebug.resetAllocCount(131072);
    }

    @Deprecated
    public static void resetThreadExternalAllocCount()
    {
    }

    @Deprecated
    public static void resetThreadExternalAllocSize()
    {
    }

    public static void resetThreadGcInvocationCount()
    {
        VMDebug.resetAllocCount(1048576);
    }

    @Deprecated
    public static int setAllocationLimit(int paramInt)
    {
        return -1;
    }

    public static void setFieldsOn(Class<?> paramClass)
    {
        setFieldsOn(paramClass, false);
    }

    public static void setFieldsOn(Class<?> paramClass, boolean paramBoolean)
    {
        StringBuilder localStringBuilder = new StringBuilder().append("setFieldsOn(");
        if (paramClass == null);
        for (String str = "null"; ; str = paramClass.getName())
        {
            Log.wtf("Debug", str + ") called in non-DEBUG build");
            return;
        }
    }

    @Deprecated
    public static int setGlobalAllocationLimit(int paramInt)
    {
        return -1;
    }

    public static void startAllocCounting()
    {
        VMDebug.startAllocCounting();
    }

    public static void startMethodTracing()
    {
        VMDebug.startMethodTracing(DEFAULT_TRACE_FILE_PATH, 0, 0);
    }

    public static void startMethodTracing(String paramString)
    {
        startMethodTracing(paramString, 0, 0);
    }

    public static void startMethodTracing(String paramString, int paramInt)
    {
        startMethodTracing(paramString, paramInt, 0);
    }

    public static void startMethodTracing(String paramString, int paramInt1, int paramInt2)
    {
        String str = paramString;
        if (str.charAt(0) != '/')
            str = DEFAULT_TRACE_PATH_PREFIX + str;
        if (!str.endsWith(".trace"))
            str = str + ".trace";
        VMDebug.startMethodTracing(str, paramInt1, paramInt2);
    }

    public static void startMethodTracing(String paramString, FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2)
    {
        VMDebug.startMethodTracing(paramString, paramFileDescriptor, paramInt1, paramInt2);
    }

    public static void startMethodTracingDdms(int paramInt1, int paramInt2)
    {
        VMDebug.startMethodTracingDdms(paramInt1, paramInt2);
    }

    // ERROR //
    public static void startNativeTracing()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_0
        //     2: new 389	java/io/PrintWriter
        //     5: dup
        //     6: new 391	java/io/OutputStreamWriter
        //     9: dup
        //     10: new 393	java/io/FileOutputStream
        //     13: dup
        //     14: ldc 38
        //     16: invokespecial 394	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
        //     19: invokespecial 397	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
        //     22: invokespecial 400	java/io/PrintWriter:<init>	(Ljava/io/Writer;)V
        //     25: astore_1
        //     26: aload_1
        //     27: ldc_w 402
        //     30: invokevirtual 405	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     33: aload_1
        //     34: ifnull +47 -> 81
        //     37: aload_1
        //     38: invokevirtual 408	java/io/PrintWriter:close	()V
        //     41: invokestatic 149	dalvik/system/VMDebug:startEmulatorTracing	()V
        //     44: return
        //     45: astore 4
        //     47: aload_0
        //     48: ifnull -7 -> 41
        //     51: aload_0
        //     52: invokevirtual 408	java/io/PrintWriter:close	()V
        //     55: goto -14 -> 41
        //     58: astore_3
        //     59: aload_0
        //     60: ifnull +7 -> 67
        //     63: aload_0
        //     64: invokevirtual 408	java/io/PrintWriter:close	()V
        //     67: aload_3
        //     68: athrow
        //     69: astore_3
        //     70: aload_1
        //     71: astore_0
        //     72: goto -13 -> 59
        //     75: astore_2
        //     76: aload_1
        //     77: astore_0
        //     78: goto -31 -> 47
        //     81: goto -40 -> 41
        //
        // Exception table:
        //     from	to	target	type
        //     2	26	45	java/lang/Exception
        //     2	26	58	finally
        //     26	33	69	finally
        //     26	33	75	java/lang/Exception
    }

    public static void stopAllocCounting()
    {
        VMDebug.stopAllocCounting();
    }

    public static void stopMethodTracing()
    {
        VMDebug.stopMethodTracing();
    }

    // ERROR //
    public static void stopNativeTracing()
    {
        // Byte code:
        //     0: invokestatic 418	dalvik/system/VMDebug:stopEmulatorTracing	()V
        //     3: aconst_null
        //     4: astore_0
        //     5: new 389	java/io/PrintWriter
        //     8: dup
        //     9: new 391	java/io/OutputStreamWriter
        //     12: dup
        //     13: new 393	java/io/FileOutputStream
        //     16: dup
        //     17: ldc 38
        //     19: invokespecial 394	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
        //     22: invokespecial 397	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
        //     25: invokespecial 400	java/io/PrintWriter:<init>	(Ljava/io/Writer;)V
        //     28: astore_1
        //     29: aload_1
        //     30: ldc_w 420
        //     33: invokevirtual 405	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     36: aload_1
        //     37: ifnull +44 -> 81
        //     40: aload_1
        //     41: invokevirtual 408	java/io/PrintWriter:close	()V
        //     44: return
        //     45: astore 4
        //     47: aload_0
        //     48: ifnull -4 -> 44
        //     51: aload_0
        //     52: invokevirtual 408	java/io/PrintWriter:close	()V
        //     55: goto -11 -> 44
        //     58: astore_3
        //     59: aload_0
        //     60: ifnull +7 -> 67
        //     63: aload_0
        //     64: invokevirtual 408	java/io/PrintWriter:close	()V
        //     67: aload_3
        //     68: athrow
        //     69: astore_3
        //     70: aload_1
        //     71: astore_0
        //     72: goto -13 -> 59
        //     75: astore_2
        //     76: aload_1
        //     77: astore_0
        //     78: goto -31 -> 47
        //     81: goto -37 -> 44
        //
        // Exception table:
        //     from	to	target	type
        //     5	29	45	java/lang/Exception
        //     5	29	58	finally
        //     29	36	69	finally
        //     29	36	75	java/lang/Exception
    }

    public static long threadCpuTimeNanos()
    {
        return VMDebug.threadCpuTimeNanos();
    }

    public static void waitForDebugger()
    {
        if (!VMDebug.isDebuggingEnabled());
        while (true)
        {
            return;
            if (!isDebuggerConnected())
            {
                System.out.println("Sending WAIT chunk");
                byte[] arrayOfByte = new byte[1];
                arrayOfByte[0] = 0;
                DdmServer.sendChunk(new Chunk(ChunkHandler.type("WAIT"), arrayOfByte, 0, 1));
                mWaiting = true;
                while (!isDebuggerConnected())
                    try
                    {
                        Thread.sleep(200L);
                    }
                    catch (InterruptedException localInterruptedException2)
                    {
                    }
                mWaiting = false;
                System.out.println("Debugger has connected");
                long l;
                while (true)
                {
                    l = VMDebug.lastDebuggerActivity();
                    if (l < 0L)
                    {
                        System.out.println("debugger detached?");
                        break;
                    }
                    if (l >= 1300L)
                        break label138;
                    System.out.println("waiting for debugger to settle...");
                    try
                    {
                        Thread.sleep(200L);
                    }
                    catch (InterruptedException localInterruptedException1)
                    {
                    }
                }
                label138: System.out.println("debugger has settled (" + l + ")");
            }
        }
    }

    public static boolean waitingForDebugger()
    {
        return mWaiting;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({java.lang.annotation.ElementType.FIELD})
    public static @interface DebugProperty
    {
    }

    public static class InstructionCount
    {
        private static final int NUM_INSTR = 1 + OpcodeInfo.MAXIMUM_PACKED_VALUE;
        private int[] mCounts = new int[NUM_INSTR];

        public boolean collect()
        {
            try
            {
                VMDebug.stopInstructionCounting();
                VMDebug.getInstructionCount(this.mCounts);
                bool = true;
                return bool;
            }
            catch (UnsupportedOperationException localUnsupportedOperationException)
            {
                while (true)
                    boolean bool = false;
            }
        }

        public int globalMethodInvocations()
        {
            int i = 0;
            for (int j = 0; j < NUM_INSTR; j++)
                if (OpcodeInfo.isInvoke(j))
                    i += this.mCounts[j];
            return i;
        }

        public int globalTotal()
        {
            int i = 0;
            for (int j = 0; j < NUM_INSTR; j++)
                i += this.mCounts[j];
            return i;
        }

        public boolean resetAndStart()
        {
            try
            {
                VMDebug.startInstructionCounting();
                VMDebug.resetInstructionCount();
                bool = true;
                return bool;
            }
            catch (UnsupportedOperationException localUnsupportedOperationException)
            {
                while (true)
                    boolean bool = false;
            }
        }
    }

    public static class MemoryInfo
        implements Parcelable
    {
        public static final Parcelable.Creator<MemoryInfo> CREATOR = new Parcelable.Creator()
        {
            public Debug.MemoryInfo createFromParcel(Parcel paramAnonymousParcel)
            {
                return new Debug.MemoryInfo(paramAnonymousParcel, null);
            }

            public Debug.MemoryInfo[] newArray(int paramAnonymousInt)
            {
                return new Debug.MemoryInfo[paramAnonymousInt];
            }
        };
        public static final int NUM_OTHER_STATS = 9;
        public int dalvikPrivateDirty;
        public int dalvikPss;
        public int dalvikSharedDirty;
        public int nativePrivateDirty;
        public int nativePss;
        public int nativeSharedDirty;
        public int otherPrivateDirty;
        public int otherPss;
        public int otherSharedDirty;
        private int[] otherStats = new int[27];

        public MemoryInfo()
        {
        }

        private MemoryInfo(Parcel paramParcel)
        {
            readFromParcel(paramParcel);
        }

        public static String getOtherLabel(int paramInt)
        {
            String str;
            switch (paramInt)
            {
            default:
                str = "????";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            }
            while (true)
            {
                return str;
                str = "Cursor";
                continue;
                str = "Ashmem";
                continue;
                str = "Other dev";
                continue;
                str = ".so mmap";
                continue;
                str = ".jar mmap";
                continue;
                str = ".apk mmap";
                continue;
                str = ".ttf mmap";
                continue;
                str = ".dex mmap";
                continue;
                str = "Other mmap";
            }
        }

        public int describeContents()
        {
            return 0;
        }

        public int getOtherPrivateDirty(int paramInt)
        {
            return this.otherStats[(1 + paramInt * 3)];
        }

        public int getOtherPss(int paramInt)
        {
            return this.otherStats[(paramInt * 3)];
        }

        public int getOtherSharedDirty(int paramInt)
        {
            return this.otherStats[(2 + paramInt * 3)];
        }

        public int getTotalPrivateDirty()
        {
            return this.dalvikPrivateDirty + this.nativePrivateDirty + this.otherPrivateDirty;
        }

        public int getTotalPss()
        {
            return this.dalvikPss + this.nativePss + this.otherPss;
        }

        public int getTotalSharedDirty()
        {
            return this.dalvikSharedDirty + this.nativeSharedDirty + this.otherSharedDirty;
        }

        public void readFromParcel(Parcel paramParcel)
        {
            this.dalvikPss = paramParcel.readInt();
            this.dalvikPrivateDirty = paramParcel.readInt();
            this.dalvikSharedDirty = paramParcel.readInt();
            this.nativePss = paramParcel.readInt();
            this.nativePrivateDirty = paramParcel.readInt();
            this.nativeSharedDirty = paramParcel.readInt();
            this.otherPss = paramParcel.readInt();
            this.otherPrivateDirty = paramParcel.readInt();
            this.otherSharedDirty = paramParcel.readInt();
            this.otherStats = paramParcel.createIntArray();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(this.dalvikPss);
            paramParcel.writeInt(this.dalvikPrivateDirty);
            paramParcel.writeInt(this.dalvikSharedDirty);
            paramParcel.writeInt(this.nativePss);
            paramParcel.writeInt(this.nativePrivateDirty);
            paramParcel.writeInt(this.nativeSharedDirty);
            paramParcel.writeInt(this.otherPss);
            paramParcel.writeInt(this.otherPrivateDirty);
            paramParcel.writeInt(this.otherSharedDirty);
            paramParcel.writeIntArray(this.otherStats);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Debug
 * JD-Core Version:        0.6.2
 */