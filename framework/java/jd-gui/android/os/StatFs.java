package android.os;

public class StatFs
{
    private int mNativeContext;

    public StatFs(String paramString)
    {
        native_setup(paramString);
    }

    private native void native_finalize();

    private native void native_restat(String paramString);

    private native void native_setup(String paramString);

    protected void finalize()
    {
        native_finalize();
    }

    public native int getAvailableBlocks();

    public native int getBlockCount();

    public native int getBlockSize();

    public native int getFreeBlocks();

    public void restat(String paramString)
    {
        native_restat(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.StatFs
 * JD-Core Version:        0.6.2
 */