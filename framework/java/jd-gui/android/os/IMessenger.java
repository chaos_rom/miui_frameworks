package android.os;

public abstract interface IMessenger extends IInterface
{
    public abstract void send(Message paramMessage)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IMessenger
    {
        private static final String DESCRIPTOR = "android.os.IMessenger";
        static final int TRANSACTION_send = 1;

        public Stub()
        {
            attachInterface(this, "android.os.IMessenger");
        }

        public static IMessenger asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.os.IMessenger");
                if ((localIInterface != null) && ((localIInterface instanceof IMessenger)))
                    localObject = (IMessenger)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.os.IMessenger");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.os.IMessenger");
            if (paramParcel1.readInt() != 0);
            for (Message localMessage = (Message)Message.CREATOR.createFromParcel(paramParcel1); ; localMessage = null)
            {
                send(localMessage);
                break;
            }
        }

        private static class Proxy
            implements IMessenger
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.os.IMessenger";
            }

            public void send(Message paramMessage)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.os.IMessenger");
                    if (paramMessage != null)
                    {
                        localParcel.writeInt(1);
                        paramMessage.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.IMessenger
 * JD-Core Version:        0.6.2
 */