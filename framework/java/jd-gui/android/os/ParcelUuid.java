package android.os;

import java.util.UUID;

public final class ParcelUuid
    implements Parcelable
{
    public static final Parcelable.Creator<ParcelUuid> CREATOR = new Parcelable.Creator()
    {
        public ParcelUuid createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ParcelUuid(new UUID(paramAnonymousParcel.readLong(), paramAnonymousParcel.readLong()));
        }

        public ParcelUuid[] newArray(int paramAnonymousInt)
        {
            return new ParcelUuid[paramAnonymousInt];
        }
    };
    private final UUID mUuid;

    public ParcelUuid(UUID paramUUID)
    {
        this.mUuid = paramUUID;
    }

    public static ParcelUuid fromString(String paramString)
    {
        return new ParcelUuid(UUID.fromString(paramString));
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (paramObject == null);
        while (true)
        {
            return bool;
            if (this == paramObject)
            {
                bool = true;
            }
            else if ((paramObject instanceof ParcelUuid))
            {
                ParcelUuid localParcelUuid = (ParcelUuid)paramObject;
                bool = this.mUuid.equals(localParcelUuid.mUuid);
            }
        }
    }

    public UUID getUuid()
    {
        return this.mUuid;
    }

    public int hashCode()
    {
        return this.mUuid.hashCode();
    }

    public String toString()
    {
        return this.mUuid.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeLong(this.mUuid.getMostSignificantBits());
        paramParcel.writeLong(this.mUuid.getLeastSignificantBits());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ParcelUuid
 * JD-Core Version:        0.6.2
 */