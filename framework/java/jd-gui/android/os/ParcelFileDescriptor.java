package android.os;

import java.io.Closeable;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.Socket;

public class ParcelFileDescriptor
    implements Parcelable, Closeable
{
    public static final Parcelable.Creator<ParcelFileDescriptor> CREATOR = new Parcelable.Creator()
    {
        public ParcelFileDescriptor createFromParcel(Parcel paramAnonymousParcel)
        {
            return paramAnonymousParcel.readFileDescriptor();
        }

        public ParcelFileDescriptor[] newArray(int paramAnonymousInt)
        {
            return new ParcelFileDescriptor[paramAnonymousInt];
        }
    };
    public static final int MODE_APPEND = 33554432;
    public static final int MODE_CREATE = 134217728;
    public static final int MODE_READ_ONLY = 268435456;
    public static final int MODE_READ_WRITE = 805306368;
    public static final int MODE_TRUNCATE = 67108864;
    public static final int MODE_WORLD_READABLE = 1;
    public static final int MODE_WORLD_WRITEABLE = 2;
    public static final int MODE_WRITE_ONLY = 536870912;
    private boolean mClosed;
    private final FileDescriptor mFileDescriptor;
    private final ParcelFileDescriptor mParcelDescriptor;

    public ParcelFileDescriptor(ParcelFileDescriptor paramParcelFileDescriptor)
    {
        this.mParcelDescriptor = paramParcelFileDescriptor;
        this.mFileDescriptor = this.mParcelDescriptor.mFileDescriptor;
    }

    ParcelFileDescriptor(FileDescriptor paramFileDescriptor)
    {
        if (paramFileDescriptor == null)
            throw new NullPointerException("descriptor must not be null");
        this.mFileDescriptor = paramFileDescriptor;
        this.mParcelDescriptor = null;
    }

    public static ParcelFileDescriptor adoptFd(int paramInt)
    {
        return new ParcelFileDescriptor(getFileDescriptorFromFdNoDup(paramInt));
    }

    public static ParcelFileDescriptor[] createPipe()
        throws IOException
    {
        FileDescriptor[] arrayOfFileDescriptor = new FileDescriptor[2];
        createPipeNative(arrayOfFileDescriptor);
        ParcelFileDescriptor[] arrayOfParcelFileDescriptor = new ParcelFileDescriptor[2];
        arrayOfParcelFileDescriptor[0] = new ParcelFileDescriptor(arrayOfFileDescriptor[0]);
        arrayOfParcelFileDescriptor[1] = new ParcelFileDescriptor(arrayOfFileDescriptor[1]);
        return arrayOfParcelFileDescriptor;
    }

    private static native void createPipeNative(FileDescriptor[] paramArrayOfFileDescriptor)
        throws IOException;

    public static ParcelFileDescriptor dup(FileDescriptor paramFileDescriptor)
        throws IOException
    {
        FileDescriptor localFileDescriptor = Parcel.dupFileDescriptor(paramFileDescriptor);
        if (localFileDescriptor != null);
        for (ParcelFileDescriptor localParcelFileDescriptor = new ParcelFileDescriptor(localFileDescriptor); ; localParcelFileDescriptor = null)
            return localParcelFileDescriptor;
    }

    @Deprecated
    public static ParcelFileDescriptor fromData(byte[] paramArrayOfByte, String paramString)
        throws IOException
    {
        ParcelFileDescriptor localParcelFileDescriptor = null;
        if (paramArrayOfByte == null);
        while (true)
        {
            return localParcelFileDescriptor;
            MemoryFile localMemoryFile = new MemoryFile(paramString, paramArrayOfByte.length);
            if (paramArrayOfByte.length > 0)
                localMemoryFile.writeBytes(paramArrayOfByte, 0, 0, paramArrayOfByte.length);
            localMemoryFile.deactivate();
            FileDescriptor localFileDescriptor = localMemoryFile.getFileDescriptor();
            if (localFileDescriptor != null)
                localParcelFileDescriptor = new ParcelFileDescriptor(localFileDescriptor);
        }
    }

    public static ParcelFileDescriptor fromDatagramSocket(DatagramSocket paramDatagramSocket)
    {
        FileDescriptor localFileDescriptor = paramDatagramSocket.getFileDescriptor$();
        if (localFileDescriptor != null);
        for (ParcelFileDescriptor localParcelFileDescriptor = new ParcelFileDescriptor(localFileDescriptor); ; localParcelFileDescriptor = null)
            return localParcelFileDescriptor;
    }

    public static ParcelFileDescriptor fromFd(int paramInt)
        throws IOException
    {
        return new ParcelFileDescriptor(getFileDescriptorFromFd(paramInt));
    }

    public static ParcelFileDescriptor fromSocket(Socket paramSocket)
    {
        FileDescriptor localFileDescriptor = paramSocket.getFileDescriptor$();
        if (localFileDescriptor != null);
        for (ParcelFileDescriptor localParcelFileDescriptor = new ParcelFileDescriptor(localFileDescriptor); ; localParcelFileDescriptor = null)
            return localParcelFileDescriptor;
    }

    private native int getFdNative();

    private static native FileDescriptor getFileDescriptorFromFd(int paramInt)
        throws IOException;

    private static native FileDescriptor getFileDescriptorFromFdNoDup(int paramInt);

    public static ParcelFileDescriptor open(File paramFile, int paramInt)
        throws FileNotFoundException
    {
        String str = paramFile.getPath();
        SecurityManager localSecurityManager = System.getSecurityManager();
        if (localSecurityManager != null)
        {
            localSecurityManager.checkRead(str);
            if ((0x20000000 & paramInt) != 0)
                localSecurityManager.checkWrite(str);
        }
        if ((0x30000000 & paramInt) == 0)
            throw new IllegalArgumentException("Must specify MODE_READ_ONLY, MODE_WRITE_ONLY, or MODE_READ_WRITE");
        FileDescriptor localFileDescriptor = Parcel.openFileDescriptor(str, paramInt);
        if (localFileDescriptor != null);
        for (ParcelFileDescriptor localParcelFileDescriptor = new ParcelFileDescriptor(localFileDescriptor); ; localParcelFileDescriptor = null)
            return localParcelFileDescriptor;
    }

    public void close()
        throws IOException
    {
        try
        {
            if (this.mClosed)
                return;
            this.mClosed = true;
            if (this.mParcelDescriptor != null)
                this.mParcelDescriptor.close();
        }
        finally
        {
        }
        Parcel.closeFileDescriptor(this.mFileDescriptor);
    }

    public int describeContents()
    {
        return 1;
    }

    public int detachFd()
    {
        if (this.mClosed)
            throw new IllegalStateException("Already closed");
        int k;
        if (this.mParcelDescriptor != null)
        {
            k = this.mParcelDescriptor.detachFd();
            this.mClosed = true;
        }
        int i;
        for (int j = k; ; j = i)
        {
            return j;
            i = getFd();
            this.mClosed = true;
            Parcel.clearFileDescriptor(this.mFileDescriptor);
        }
    }

    public ParcelFileDescriptor dup()
        throws IOException
    {
        return dup(getFileDescriptor());
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if (!this.mClosed)
                close();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getFd()
    {
        if (this.mClosed)
            throw new IllegalStateException("Already closed");
        return getFdNative();
    }

    public FileDescriptor getFileDescriptor()
    {
        return this.mFileDescriptor;
    }

    public native long getStatSize();

    public native long seekTo(long paramLong);

    public String toString()
    {
        return "{ParcelFileDescriptor: " + this.mFileDescriptor + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeFileDescriptor(this.mFileDescriptor);
        if (((paramInt & 0x1) != 0) && (!this.mClosed));
        try
        {
            close();
            label25: return;
        }
        catch (IOException localIOException)
        {
            break label25;
        }
    }

    public static class AutoCloseOutputStream extends FileOutputStream
    {
        private final ParcelFileDescriptor mFd;

        public AutoCloseOutputStream(ParcelFileDescriptor paramParcelFileDescriptor)
        {
            super();
            this.mFd = paramParcelFileDescriptor;
        }

        public void close()
            throws IOException
        {
            try
            {
                this.mFd.close();
                return;
            }
            finally
            {
                super.close();
            }
        }
    }

    public static class AutoCloseInputStream extends FileInputStream
    {
        private final ParcelFileDescriptor mFd;

        public AutoCloseInputStream(ParcelFileDescriptor paramParcelFileDescriptor)
        {
            super();
            this.mFd = paramParcelFileDescriptor;
        }

        public void close()
            throws IOException
        {
            try
            {
                this.mFd.close();
                return;
            }
            finally
            {
                super.close();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ParcelFileDescriptor
 * JD-Core Version:        0.6.2
 */