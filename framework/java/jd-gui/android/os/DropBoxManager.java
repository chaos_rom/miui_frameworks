package android.os;

import com.android.internal.os.IDropBoxManagerService;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class DropBoxManager
{
    public static final String ACTION_DROPBOX_ENTRY_ADDED = "android.intent.action.DROPBOX_ENTRY_ADDED";
    public static final String EXTRA_TAG = "tag";
    public static final String EXTRA_TIME = "time";
    private static final int HAS_BYTE_ARRAY = 8;
    public static final int IS_EMPTY = 1;
    public static final int IS_GZIPPED = 4;
    public static final int IS_TEXT = 2;
    private static final String TAG = "DropBoxManager";
    private final IDropBoxManagerService mService;

    protected DropBoxManager()
    {
        this.mService = null;
    }

    public DropBoxManager(IDropBoxManagerService paramIDropBoxManagerService)
    {
        this.mService = paramIDropBoxManagerService;
    }

    public void addData(String paramString, byte[] paramArrayOfByte, int paramInt)
    {
        if (paramArrayOfByte == null)
            throw new NullPointerException("data == null");
        try
        {
            this.mService.add(new Entry(paramString, 0L, paramArrayOfByte, paramInt));
            label34: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label34;
        }
    }

    // ERROR //
    public void addFile(String paramString, File paramFile, int paramInt)
        throws IOException
    {
        // Byte code:
        //     0: aload_2
        //     1: ifnonnull +13 -> 14
        //     4: new 44	java/lang/NullPointerException
        //     7: dup
        //     8: ldc 64
        //     10: invokespecial 49	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     13: athrow
        //     14: new 6	android/os/DropBoxManager$Entry
        //     17: dup
        //     18: aload_1
        //     19: lconst_0
        //     20: aload_2
        //     21: iload_3
        //     22: invokespecial 67	android/os/DropBoxManager$Entry:<init>	(Ljava/lang/String;JLjava/io/File;I)V
        //     25: astore 4
        //     27: aload_0
        //     28: getfield 37	android/os/DropBoxManager:mService	Lcom/android/internal/os/IDropBoxManagerService;
        //     31: aload 4
        //     33: invokeinterface 58 2 0
        //     38: aload 4
        //     40: invokevirtual 70	android/os/DropBoxManager$Entry:close	()V
        //     43: return
        //     44: astore 6
        //     46: aload 4
        //     48: invokevirtual 70	android/os/DropBoxManager$Entry:close	()V
        //     51: goto -8 -> 43
        //     54: astore 5
        //     56: aload 4
        //     58: invokevirtual 70	android/os/DropBoxManager$Entry:close	()V
        //     61: aload 5
        //     63: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     27	38	44	android/os/RemoteException
        //     27	38	54	finally
    }

    public void addText(String paramString1, String paramString2)
    {
        try
        {
            this.mService.add(new Entry(paramString1, 0L, paramString2));
            label19: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label19;
        }
    }

    public Entry getNextEntry(String paramString, long paramLong)
    {
        try
        {
            Entry localEntry2 = this.mService.getNextEntry(paramString, paramLong);
            localEntry1 = localEntry2;
            return localEntry1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Entry localEntry1 = null;
        }
    }

    public boolean isTagEnabled(String paramString)
    {
        try
        {
            boolean bool2 = this.mService.isTagEnabled(paramString);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public static class Entry
        implements Parcelable, Closeable
    {
        public static final Parcelable.Creator<Entry> CREATOR = new Parcelable.Creator()
        {
            public DropBoxManager.Entry createFromParcel(Parcel paramAnonymousParcel)
            {
                String str = paramAnonymousParcel.readString();
                long l = paramAnonymousParcel.readLong();
                int i = paramAnonymousParcel.readInt();
                if ((i & 0x8) != 0);
                for (DropBoxManager.Entry localEntry = new DropBoxManager.Entry(str, l, paramAnonymousParcel.createByteArray(), i & 0xFFFFFFF7); ; localEntry = new DropBoxManager.Entry(str, l, paramAnonymousParcel.readFileDescriptor(), i))
                    return localEntry;
            }

            public DropBoxManager.Entry[] newArray(int paramAnonymousInt)
            {
                return new DropBoxManager.Entry[paramAnonymousInt];
            }
        };
        private final byte[] mData;
        private final ParcelFileDescriptor mFileDescriptor;
        private final int mFlags;
        private final String mTag;
        private final long mTimeMillis;

        public Entry(String paramString, long paramLong)
        {
            if (paramString == null)
                throw new NullPointerException("tag == null");
            this.mTag = paramString;
            this.mTimeMillis = paramLong;
            this.mData = null;
            this.mFileDescriptor = null;
            this.mFlags = 1;
        }

        public Entry(String paramString, long paramLong, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt)
        {
            if (paramString == null)
                throw new NullPointerException("tag == null");
            int j;
            if ((paramInt & 0x1) != 0)
            {
                j = i;
                if (paramParcelFileDescriptor != null)
                    break label78;
            }
            while (true)
            {
                if (j == i)
                    break label84;
                throw new IllegalArgumentException("Bad flags: " + paramInt);
                j = 0;
                break;
                label78: i = 0;
            }
            label84: this.mTag = paramString;
            this.mTimeMillis = paramLong;
            this.mData = null;
            this.mFileDescriptor = paramParcelFileDescriptor;
            this.mFlags = paramInt;
        }

        public Entry(String paramString, long paramLong, File paramFile, int paramInt)
            throws IOException
        {
            if (paramString == null)
                throw new NullPointerException("tag == null");
            if ((paramInt & 0x1) != 0)
                throw new IllegalArgumentException("Bad flags: " + paramInt);
            this.mTag = paramString;
            this.mTimeMillis = paramLong;
            this.mData = null;
            this.mFileDescriptor = ParcelFileDescriptor.open(paramFile, 268435456);
            this.mFlags = paramInt;
        }

        public Entry(String paramString1, long paramLong, String paramString2)
        {
            if (paramString1 == null)
                throw new NullPointerException("tag == null");
            if (paramString2 == null)
                throw new NullPointerException("text == null");
            this.mTag = paramString1;
            this.mTimeMillis = paramLong;
            this.mData = paramString2.getBytes();
            this.mFileDescriptor = null;
            this.mFlags = 2;
        }

        public Entry(String paramString, long paramLong, byte[] paramArrayOfByte, int paramInt)
        {
            if (paramString == null)
                throw new NullPointerException("tag == null");
            int j;
            if ((paramInt & 0x1) != 0)
            {
                j = i;
                if (paramArrayOfByte != null)
                    break label78;
            }
            while (true)
            {
                if (j == i)
                    break label84;
                throw new IllegalArgumentException("Bad flags: " + paramInt);
                j = 0;
                break;
                label78: i = 0;
            }
            label84: this.mTag = paramString;
            this.mTimeMillis = paramLong;
            this.mData = paramArrayOfByte;
            this.mFileDescriptor = null;
            this.mFlags = paramInt;
        }

        public void close()
        {
            try
            {
                if (this.mFileDescriptor != null)
                    this.mFileDescriptor.close();
                label14: return;
            }
            catch (IOException localIOException)
            {
                break label14;
            }
        }

        public int describeContents()
        {
            if (this.mFileDescriptor != null);
            for (int i = 1; ; i = 0)
                return i;
        }

        public int getFlags()
        {
            return 0xFFFFFFFB & this.mFlags;
        }

        public InputStream getInputStream()
            throws IOException
        {
            if (this.mData != null)
            {
                localObject = new ByteArrayInputStream(this.mData);
                if ((0x4 & this.mFlags) == 0);
            }
            for (Object localObject = new GZIPInputStream((InputStream)localObject); ; localObject = null)
            {
                return localObject;
                if (this.mFileDescriptor != null)
                {
                    localObject = new ParcelFileDescriptor.AutoCloseInputStream(this.mFileDescriptor);
                    break;
                }
            }
        }

        public String getTag()
        {
            return this.mTag;
        }

        // ERROR //
        public String getText(int paramInt)
        {
            // Byte code:
            //     0: aconst_null
            //     1: astore_2
            //     2: iconst_2
            //     3: aload_0
            //     4: getfield 52	android/os/DropBoxManager$Entry:mFlags	I
            //     7: iand
            //     8: ifne +5 -> 13
            //     11: aload_2
            //     12: areturn
            //     13: aload_0
            //     14: getfield 48	android/os/DropBoxManager$Entry:mData	[B
            //     17: ifnull +28 -> 45
            //     20: new 87	java/lang/String
            //     23: dup
            //     24: aload_0
            //     25: getfield 48	android/os/DropBoxManager$Entry:mData	[B
            //     28: iconst_0
            //     29: iload_1
            //     30: aload_0
            //     31: getfield 48	android/os/DropBoxManager$Entry:mData	[B
            //     34: arraylength
            //     35: invokestatic 124	java/lang/Math:min	(II)I
            //     38: invokespecial 127	java/lang/String:<init>	([BII)V
            //     41: astore_2
            //     42: goto -31 -> 11
            //     45: aconst_null
            //     46: astore_3
            //     47: aload_0
            //     48: invokevirtual 129	android/os/DropBoxManager$Entry:getInputStream	()Ljava/io/InputStream;
            //     51: astore 8
            //     53: aload 8
            //     55: astore_3
            //     56: aload_3
            //     57: ifnonnull +19 -> 76
            //     60: aload_3
            //     61: ifnull -50 -> 11
            //     64: aload_3
            //     65: invokevirtual 132	java/io/InputStream:close	()V
            //     68: goto -57 -> 11
            //     71: astore 14
            //     73: goto -62 -> 11
            //     76: iload_1
            //     77: newarray byte
            //     79: astore 9
            //     81: iconst_0
            //     82: istore 10
            //     84: iconst_0
            //     85: istore 11
            //     87: iload 11
            //     89: iflt +33 -> 122
            //     92: iload 10
            //     94: iload 11
            //     96: iadd
            //     97: istore 10
            //     99: iload 10
            //     101: iload_1
            //     102: if_icmpge +20 -> 122
            //     105: aload_3
            //     106: aload 9
            //     108: iload 10
            //     110: iload_1
            //     111: iload 10
            //     113: isub
            //     114: invokevirtual 136	java/io/InputStream:read	([BII)I
            //     117: istore 11
            //     119: goto -32 -> 87
            //     122: new 87	java/lang/String
            //     125: dup
            //     126: aload 9
            //     128: iconst_0
            //     129: iload 10
            //     131: invokespecial 127	java/lang/String:<init>	([BII)V
            //     134: astore 12
            //     136: aload_3
            //     137: ifnull +7 -> 144
            //     140: aload_3
            //     141: invokevirtual 132	java/io/InputStream:close	()V
            //     144: aload 12
            //     146: astore_2
            //     147: goto -136 -> 11
            //     150: astore 6
            //     152: aload_3
            //     153: ifnull -142 -> 11
            //     156: aload_3
            //     157: invokevirtual 132	java/io/InputStream:close	()V
            //     160: goto -149 -> 11
            //     163: astore 7
            //     165: goto -154 -> 11
            //     168: astore 4
            //     170: aload_3
            //     171: ifnull +7 -> 178
            //     174: aload_3
            //     175: invokevirtual 132	java/io/InputStream:close	()V
            //     178: aload 4
            //     180: athrow
            //     181: astore 13
            //     183: goto -39 -> 144
            //     186: astore 5
            //     188: goto -10 -> 178
            //
            // Exception table:
            //     from	to	target	type
            //     64	68	71	java/io/IOException
            //     47	53	150	java/io/IOException
            //     76	136	150	java/io/IOException
            //     156	160	163	java/io/IOException
            //     47	53	168	finally
            //     76	136	168	finally
            //     140	144	181	java/io/IOException
            //     174	178	186	java/io/IOException
        }

        public long getTimeMillis()
        {
            return this.mTimeMillis;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeString(this.mTag);
            paramParcel.writeLong(this.mTimeMillis);
            if (this.mFileDescriptor != null)
            {
                paramParcel.writeInt(0xFFFFFFF7 & this.mFlags);
                this.mFileDescriptor.writeToParcel(paramParcel, paramInt);
            }
            while (true)
            {
                return;
                paramParcel.writeInt(0x8 | this.mFlags);
                paramParcel.writeByteArray(this.mData);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.DropBoxManager
 * JD-Core Version:        0.6.2
 */