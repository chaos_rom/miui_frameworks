package android.os;

public class WorkSource
    implements Parcelable
{
    public static final Parcelable.Creator<WorkSource> CREATOR = new Parcelable.Creator()
    {
        public WorkSource createFromParcel(Parcel paramAnonymousParcel)
        {
            return new WorkSource(paramAnonymousParcel);
        }

        public WorkSource[] newArray(int paramAnonymousInt)
        {
            return new WorkSource[paramAnonymousInt];
        }
    };
    static WorkSource sGoneWork;
    static WorkSource sNewbWork;
    static final WorkSource sTmpWorkSource = new WorkSource(0);
    int mNum;
    int[] mUids;

    public WorkSource()
    {
        this.mNum = 0;
    }

    public WorkSource(int paramInt)
    {
        this.mNum = 1;
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = paramInt;
        arrayOfInt[1] = 0;
        this.mUids = arrayOfInt;
    }

    WorkSource(Parcel paramParcel)
    {
        this.mNum = paramParcel.readInt();
        this.mUids = paramParcel.createIntArray();
    }

    public WorkSource(WorkSource paramWorkSource)
    {
        if (paramWorkSource == null)
            this.mNum = 0;
        while (true)
        {
            return;
            this.mNum = paramWorkSource.mNum;
            if (paramWorkSource.mUids != null)
                this.mUids = ((int[])paramWorkSource.mUids.clone());
            else
                this.mUids = null;
        }
    }

    private void addLocked(int paramInt)
    {
        if (this.mUids == null)
        {
            this.mUids = new int[4];
            this.mUids[0] = paramInt;
        }
        for (this.mNum = 1; ; this.mNum = (1 + this.mNum))
        {
            return;
            if (this.mNum >= this.mUids.length)
            {
                int[] arrayOfInt = new int[3 * this.mNum / 2];
                System.arraycopy(this.mUids, 0, arrayOfInt, 0, this.mNum);
                this.mUids = arrayOfInt;
            }
            this.mUids[this.mNum] = paramInt;
        }
    }

    private boolean updateLocked(WorkSource paramWorkSource, boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = this.mNum;
        Object localObject = this.mUids;
        int j = paramWorkSource.mNum;
        int[] arrayOfInt1 = paramWorkSource.mUids;
        boolean bool = false;
        int k = 0;
        int m = 0;
        if (m < j)
        {
            if ((k >= i) || (arrayOfInt1[m] < localObject[k]))
            {
                bool = true;
                if (localObject == null)
                {
                    localObject = new int[4];
                    localObject[0] = arrayOfInt1[m];
                    label82: if (paramBoolean2)
                    {
                        if (sNewbWork != null)
                            break label234;
                        sNewbWork = new WorkSource(arrayOfInt1[m]);
                    }
                    label107: i++;
                    k++;
                    label112: m++;
                    break label252;
                }
            }
            while (true)
            {
                break;
                if (k >= localObject.length)
                {
                    int[] arrayOfInt2 = new int[3 * localObject.length / 2];
                    if (k > 0)
                        System.arraycopy(localObject, 0, arrayOfInt2, 0, k);
                    if (k < i)
                        System.arraycopy(localObject, k, arrayOfInt2, k + 1, i - k);
                    localObject = arrayOfInt2;
                    localObject[k] = arrayOfInt1[m];
                    break label82;
                }
                if (k < i)
                    System.arraycopy(localObject, k, localObject, k + 1, i - k);
                localObject[k] = arrayOfInt1[m];
                break label82;
                label234: sNewbWork.addLocked(arrayOfInt1[m]);
                break label107;
                if (!paramBoolean1)
                {
                    label252: k++;
                    if (k >= i)
                        continue;
                    if (arrayOfInt1[m] >= localObject[k])
                        break label112;
                    continue;
                }
                int n = k;
                if ((k < i) && (arrayOfInt1[m] > localObject[k]))
                {
                    if (sGoneWork == null)
                        sGoneWork = new WorkSource(localObject[k]);
                    while (true)
                    {
                        k++;
                        break;
                        sGoneWork.addLocked(localObject[k]);
                    }
                }
                if (n < k)
                {
                    System.arraycopy(localObject, k, localObject, n, k - n);
                    i -= k - n;
                    k = n;
                }
                if ((k < i) && (arrayOfInt1[k] == localObject[k]))
                    k++;
            }
        }
        this.mNum = i;
        this.mUids = ((int[])localObject);
        return bool;
    }

    public boolean add(int paramInt)
    {
        synchronized (sTmpWorkSource)
        {
            sTmpWorkSource.mUids[0] = paramInt;
            boolean bool = updateLocked(sTmpWorkSource, false, false);
            return bool;
        }
    }

    public boolean add(WorkSource paramWorkSource)
    {
        synchronized (sTmpWorkSource)
        {
            boolean bool = updateLocked(paramWorkSource, false, false);
            return bool;
        }
    }

    public WorkSource addReturningNewbs(int paramInt)
    {
        synchronized (sTmpWorkSource)
        {
            sNewbWork = null;
            sTmpWorkSource.mUids[0] = paramInt;
            updateLocked(sTmpWorkSource, false, true);
            WorkSource localWorkSource2 = sNewbWork;
            return localWorkSource2;
        }
    }

    public WorkSource addReturningNewbs(WorkSource paramWorkSource)
    {
        synchronized (sTmpWorkSource)
        {
            sNewbWork = null;
            updateLocked(paramWorkSource, false, true);
            WorkSource localWorkSource2 = sNewbWork;
            return localWorkSource2;
        }
    }

    public void clear()
    {
        this.mNum = 0;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean diff(WorkSource paramWorkSource)
    {
        boolean bool = true;
        int i = this.mNum;
        if (i != paramWorkSource.mNum);
        while (true)
        {
            return bool;
            int[] arrayOfInt1 = this.mUids;
            int[] arrayOfInt2 = paramWorkSource.mUids;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label57;
                if (arrayOfInt1[j] != arrayOfInt2[j])
                    break;
            }
            label57: bool = false;
        }
    }

    public int get(int paramInt)
    {
        return this.mUids[paramInt];
    }

    public boolean remove(WorkSource paramWorkSource)
    {
        int i = this.mNum;
        int[] arrayOfInt1 = this.mUids;
        int j = paramWorkSource.mNum;
        int[] arrayOfInt2 = paramWorkSource.mUids;
        int k = 0;
        for (int m = 0; (m < j) && (k < i); m++)
        {
            if (arrayOfInt2[m] == arrayOfInt1[k])
            {
                i--;
                if (k < i)
                    System.arraycopy(arrayOfInt1, k + 1, arrayOfInt1, k, i - k);
            }
            while ((k < i) && (arrayOfInt2[m] > arrayOfInt1[k]))
                k++;
        }
        this.mNum = i;
        return false;
    }

    public void set(int paramInt)
    {
        this.mNum = 1;
        if (this.mUids == null)
            this.mUids = new int[2];
        this.mUids[0] = paramInt;
    }

    public void set(WorkSource paramWorkSource)
    {
        if (paramWorkSource == null)
            this.mNum = 0;
        while (true)
        {
            return;
            this.mNum = paramWorkSource.mNum;
            if (paramWorkSource.mUids != null)
            {
                if ((this.mUids != null) && (this.mUids.length >= this.mNum))
                    System.arraycopy(paramWorkSource.mUids, 0, this.mUids, 0, this.mNum);
                else
                    this.mUids = ((int[])paramWorkSource.mUids.clone());
            }
            else
                this.mUids = null;
        }
    }

    public WorkSource[] setReturningDiffs(WorkSource paramWorkSource)
    {
        WorkSource[] arrayOfWorkSource = null;
        synchronized (sTmpWorkSource)
        {
            sNewbWork = null;
            sGoneWork = null;
            updateLocked(paramWorkSource, true, true);
            if ((sNewbWork != null) || (sGoneWork != null))
            {
                arrayOfWorkSource = new WorkSource[2];
                arrayOfWorkSource[0] = sNewbWork;
                arrayOfWorkSource[1] = sGoneWork;
            }
        }
        return arrayOfWorkSource;
    }

    public int size()
    {
        return this.mNum;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mNum);
        paramParcel.writeIntArray(this.mUids);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.WorkSource
 * JD-Core Version:        0.6.2
 */