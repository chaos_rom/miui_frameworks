package android.os;

public abstract interface ICancellationSignal extends IInterface
{
    public abstract void cancel()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ICancellationSignal
    {
        private static final String DESCRIPTOR = "android.os.ICancellationSignal";
        static final int TRANSACTION_cancel = 1;

        public Stub()
        {
            attachInterface(this, "android.os.ICancellationSignal");
        }

        public static ICancellationSignal asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.os.ICancellationSignal");
                if ((localIInterface != null) && ((localIInterface instanceof ICancellationSignal)))
                    localObject = (ICancellationSignal)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.os.ICancellationSignal");
                continue;
                paramParcel1.enforceInterface("android.os.ICancellationSignal");
                cancel();
            }
        }

        private static class Proxy
            implements ICancellationSignal
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancel()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.os.ICancellationSignal");
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.os.ICancellationSignal";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ICancellationSignal
 * JD-Core Version:        0.6.2
 */