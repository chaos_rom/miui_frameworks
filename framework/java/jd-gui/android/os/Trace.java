package android.os;

public final class Trace
{
    public static final String PROPERTY_TRACE_TAG_ENABLEFLAGS = "debug.atrace.tags.enableflags";
    public static final int TRACE_FLAGS_START_BIT = 1;
    public static final String[] TRACE_TAGS;
    public static final long TRACE_TAG_ACTIVITY_MANAGER = 64L;
    public static final long TRACE_TAG_ALWAYS = 1L;
    public static final long TRACE_TAG_AUDIO = 256L;
    public static final long TRACE_TAG_GRAPHICS = 2L;
    public static final long TRACE_TAG_INPUT = 4L;
    public static final long TRACE_TAG_NEVER = 0L;
    public static final long TRACE_TAG_SYNC_MANAGER = 128L;
    public static final long TRACE_TAG_VIDEO = 512L;
    public static final long TRACE_TAG_VIEW = 8L;
    public static final long TRACE_TAG_WEBVIEW = 16L;
    public static final long TRACE_TAG_WINDOW_MANAGER = 32L;
    private static long sEnabledTags;

    static
    {
        String[] arrayOfString = new String[9];
        arrayOfString[0] = "Graphics";
        arrayOfString[1] = "Input";
        arrayOfString[2] = "View";
        arrayOfString[3] = "WebView";
        arrayOfString[4] = "Window Manager";
        arrayOfString[5] = "Activity Manager";
        arrayOfString[6] = "Sync Manager";
        arrayOfString[7] = "Audio";
        arrayOfString[8] = "Video";
        TRACE_TAGS = arrayOfString;
        sEnabledTags = nativeGetEnabledTags();
        SystemProperties.addChangeCallback(new Runnable()
        {
            public void run()
            {
                Trace.access$002(Trace.access$100());
            }
        });
    }

    public static boolean isTagEnabled(long paramLong)
    {
        if ((paramLong & sEnabledTags) != 0L);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static native long nativeGetEnabledTags();

    private static native void nativeTraceBegin(long paramLong, String paramString);

    private static native void nativeTraceCounter(long paramLong, String paramString, int paramInt);

    private static native void nativeTraceEnd(long paramLong);

    public static void traceBegin(long paramLong, String paramString)
    {
        if ((paramLong & sEnabledTags) != 0L)
            nativeTraceBegin(paramLong, paramString);
    }

    public static void traceCounter(long paramLong, String paramString, int paramInt)
    {
        if ((paramLong & sEnabledTags) != 0L)
            nativeTraceCounter(paramLong, paramString, paramInt);
    }

    public static void traceEnd(long paramLong)
    {
        if ((paramLong & sEnabledTags) != 0L)
            nativeTraceEnd(paramLong);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Trace
 * JD-Core Version:        0.6.2
 */