package android.os;

import android.util.Log;
import java.util.HashMap;

public final class LatencyTimer
{
    final String TAG = "LatencyTimer";
    final int mSampleSize;
    final int mScaleFactor;
    volatile HashMap<String, long[]> store = new HashMap();

    public LatencyTimer(int paramInt1, int paramInt2)
    {
        if (paramInt2 == 0)
            paramInt2 = 1;
        this.mScaleFactor = paramInt2;
        this.mSampleSize = paramInt1;
    }

    private long[] getArray(String paramString)
    {
        long[] arrayOfLong = (long[])this.store.get(paramString);
        if (arrayOfLong == null)
            synchronized (this.store)
            {
                arrayOfLong = (long[])this.store.get(paramString);
                if (arrayOfLong == null)
                {
                    arrayOfLong = new long[1 + this.mSampleSize];
                    this.store.put(paramString, arrayOfLong);
                    arrayOfLong[this.mSampleSize] = 0L;
                }
            }
        return arrayOfLong;
    }

    public void sample(String paramString, long paramLong)
    {
        long[] arrayOfLong = getArray(paramString);
        int i = this.mSampleSize;
        long l1 = arrayOfLong[i];
        arrayOfLong[i] = (1L + l1);
        arrayOfLong[((int)l1)] = paramLong;
        if (arrayOfLong[this.mSampleSize] == this.mSampleSize)
        {
            long l2 = 0L;
            int j = arrayOfLong.length;
            for (int k = 0; k < j; k++)
                l2 += arrayOfLong[k] / this.mScaleFactor;
            arrayOfLong[this.mSampleSize] = 0L;
            Log.i("LatencyTimer", paramString + " average = " + l2 / this.mSampleSize);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.LatencyTimer
 * JD-Core Version:        0.6.2
 */