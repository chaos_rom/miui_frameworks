package android.os;

import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MemoryFile
{
    private static final int PROT_READ = 1;
    private static final int PROT_WRITE = 2;
    private static String TAG = "MemoryFile";
    private int mAddress;
    private boolean mAllowPurging = false;
    private FileDescriptor mFD;
    private int mLength;

    public MemoryFile(String paramString, int paramInt)
        throws IOException
    {
        this.mLength = paramInt;
        this.mFD = native_open(paramString, paramInt);
        if (paramInt > 0);
        for (this.mAddress = native_mmap(this.mFD, paramInt, 3); ; this.mAddress = 0)
            return;
    }

    public static int getSize(FileDescriptor paramFileDescriptor)
        throws IOException
    {
        return native_get_size(paramFileDescriptor);
    }

    private boolean isClosed()
    {
        if (!this.mFD.valid());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isDeactivated()
    {
        if (this.mAddress == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static native void native_close(FileDescriptor paramFileDescriptor);

    private static native int native_get_size(FileDescriptor paramFileDescriptor)
        throws IOException;

    private static native int native_mmap(FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2)
        throws IOException;

    private static native void native_munmap(int paramInt1, int paramInt2)
        throws IOException;

    private static native FileDescriptor native_open(String paramString, int paramInt)
        throws IOException;

    private static native void native_pin(FileDescriptor paramFileDescriptor, boolean paramBoolean)
        throws IOException;

    private static native int native_read(FileDescriptor paramFileDescriptor, int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
        throws IOException;

    private static native void native_write(FileDescriptor paramFileDescriptor, int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
        throws IOException;

    /** @deprecated */
    public boolean allowPurging(boolean paramBoolean)
        throws IOException
    {
        try
        {
            boolean bool1 = this.mAllowPurging;
            if (bool1 != paramBoolean)
            {
                FileDescriptor localFileDescriptor = this.mFD;
                if (!paramBoolean)
                {
                    bool2 = true;
                    native_pin(localFileDescriptor, bool2);
                    this.mAllowPurging = paramBoolean;
                }
            }
            else
            {
                return bool1;
            }
            boolean bool2 = false;
        }
        finally
        {
        }
    }

    public void close()
    {
        deactivate();
        if (!isClosed())
            native_close(this.mFD);
    }

    void deactivate()
    {
        if (!isDeactivated());
        try
        {
            native_munmap(this.mAddress, this.mLength);
            this.mAddress = 0;
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.e(TAG, localIOException.toString());
        }
    }

    protected void finalize()
    {
        if (!isClosed())
        {
            Log.e(TAG, "MemoryFile.finalize() called while ashmem still open");
            close();
        }
    }

    public FileDescriptor getFileDescriptor()
        throws IOException
    {
        return this.mFD;
    }

    public InputStream getInputStream()
    {
        return new MemoryInputStream(null);
    }

    public OutputStream getOutputStream()
    {
        return new MemoryOutputStream(null);
    }

    public boolean isPurgingAllowed()
    {
        return this.mAllowPurging;
    }

    public int length()
    {
        return this.mLength;
    }

    public int readBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
        throws IOException
    {
        if (isDeactivated())
            throw new IOException("Can't read from deactivated memory file.");
        if ((paramInt2 < 0) || (paramInt2 > paramArrayOfByte.length) || (paramInt3 < 0) || (paramInt3 > paramArrayOfByte.length - paramInt2) || (paramInt1 < 0) || (paramInt1 > this.mLength) || (paramInt3 > this.mLength - paramInt1))
            throw new IndexOutOfBoundsException();
        return native_read(this.mFD, this.mAddress, paramArrayOfByte, paramInt1, paramInt2, paramInt3, this.mAllowPurging);
    }

    public void writeBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
        throws IOException
    {
        if (isDeactivated())
            throw new IOException("Can't write to deactivated memory file.");
        if ((paramInt1 < 0) || (paramInt1 > paramArrayOfByte.length) || (paramInt3 < 0) || (paramInt3 > paramArrayOfByte.length - paramInt1) || (paramInt2 < 0) || (paramInt2 > this.mLength) || (paramInt3 > this.mLength - paramInt2))
            throw new IndexOutOfBoundsException();
        native_write(this.mFD, this.mAddress, paramArrayOfByte, paramInt1, paramInt2, paramInt3, this.mAllowPurging);
    }

    private class MemoryOutputStream extends OutputStream
    {
        private int mOffset = 0;
        private byte[] mSingleByte;

        private MemoryOutputStream()
        {
        }

        public void write(int paramInt)
            throws IOException
        {
            if (this.mSingleByte == null)
                this.mSingleByte = new byte[1];
            this.mSingleByte[0] = ((byte)paramInt);
            write(this.mSingleByte, 0, 1);
        }

        public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            MemoryFile.this.writeBytes(paramArrayOfByte, paramInt1, this.mOffset, paramInt2);
            this.mOffset = (paramInt2 + this.mOffset);
        }
    }

    private class MemoryInputStream extends InputStream
    {
        private int mMark = 0;
        private int mOffset = 0;
        private byte[] mSingleByte;

        private MemoryInputStream()
        {
        }

        public int available()
            throws IOException
        {
            if (this.mOffset >= MemoryFile.this.mLength);
            for (int i = 0; ; i = MemoryFile.this.mLength - this.mOffset)
                return i;
        }

        public void mark(int paramInt)
        {
            this.mMark = this.mOffset;
        }

        public boolean markSupported()
        {
            return true;
        }

        public int read()
            throws IOException
        {
            if (this.mSingleByte == null)
                this.mSingleByte = new byte[1];
            if (read(this.mSingleByte, 0, 1) != 1);
            for (int i = -1; ; i = this.mSingleByte[0])
                return i;
        }

        public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length))
                throw new IndexOutOfBoundsException();
            int i = Math.min(paramInt2, available());
            int j;
            if (i < 1)
                j = -1;
            while (true)
            {
                return j;
                j = MemoryFile.this.readBytes(paramArrayOfByte, this.mOffset, paramInt1, i);
                if (j > 0)
                    this.mOffset = (j + this.mOffset);
            }
        }

        public void reset()
            throws IOException
        {
            this.mOffset = this.mMark;
        }

        public long skip(long paramLong)
            throws IOException
        {
            if (paramLong + this.mOffset > MemoryFile.this.mLength)
                paramLong = MemoryFile.this.mLength - this.mOffset;
            this.mOffset = ((int)(paramLong + this.mOffset));
            return paramLong;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.MemoryFile
 * JD-Core Version:        0.6.2
 */