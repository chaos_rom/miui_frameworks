package android.os;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Binder
    implements IBinder
{
    private static final boolean FIND_POTENTIAL_LEAKS = false;
    private static final String TAG = "Binder";
    private String mDescriptor;
    private int mObject;
    private IInterface mOwner;

    public Binder()
    {
        init();
    }

    public static final native long clearCallingIdentity();

    private final native void destroy();

    private boolean execTransact(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Parcel localParcel1 = Parcel.obtain(paramInt2);
        Parcel localParcel2 = Parcel.obtain(paramInt3);
        try
        {
            boolean bool2 = onTransact(paramInt1, localParcel1, localParcel2, paramInt4);
            bool1 = bool2;
            localParcel2.recycle();
            localParcel1.recycle();
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                localParcel2.setDataPosition(0);
                localParcel2.writeException(localRemoteException);
                bool1 = true;
            }
        }
        catch (RuntimeException localRuntimeException2)
        {
            while (true)
            {
                localParcel2.setDataPosition(0);
                localParcel2.writeException(localRuntimeException2);
                bool1 = true;
            }
        }
        catch (OutOfMemoryError localOutOfMemoryError)
        {
            while (true)
            {
                RuntimeException localRuntimeException1 = new RuntimeException("Out of memory", localOutOfMemoryError);
                localParcel2.setDataPosition(0);
                localParcel2.writeException(localRuntimeException1);
                boolean bool1 = true;
            }
        }
    }

    public static final native void flushPendingCommands();

    public static final native int getCallingPid();

    public static final native int getCallingUid();

    public static final int getOrigCallingUid()
    {
        return getOrigCallingUidNative();
    }

    private static final native int getOrigCallingUidNative();

    public static final int getOrigCallingUser()
    {
        return UserId.getUserId(getOrigCallingUid());
    }

    public static final native int getThreadStrictModePolicy();

    private final native void init();

    public static final native void joinThreadPool();

    public static final native void restoreCallingIdentity(long paramLong);

    public static final native void setThreadStrictModePolicy(int paramInt);

    public void attachInterface(IInterface paramIInterface, String paramString)
    {
        this.mOwner = paramIInterface;
        this.mDescriptor = paramString;
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
    }

    public void dump(FileDescriptor paramFileDescriptor, String[] paramArrayOfString)
    {
        PrintWriter localPrintWriter = new PrintWriter(new FileOutputStream(paramFileDescriptor));
        try
        {
            dump(paramFileDescriptor, localPrintWriter, paramArrayOfString);
            return;
        }
        finally
        {
            localPrintWriter.flush();
        }
    }

    public void dumpAsync(final FileDescriptor paramFileDescriptor, final String[] paramArrayOfString)
    {
        new Thread("Binder.dumpAsync")
        {
            public void run()
            {
                try
                {
                    Binder.this.dump(paramFileDescriptor, this.val$pw, paramArrayOfString);
                    return;
                }
                finally
                {
                    this.val$pw.flush();
                }
            }
        }
        .start();
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            destroy();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public String getInterfaceDescriptor()
    {
        return this.mDescriptor;
    }

    public boolean isBinderAlive()
    {
        return true;
    }

    public void linkToDeath(IBinder.DeathRecipient paramDeathRecipient, int paramInt)
    {
    }

    // ERROR //
    protected boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore 5
        //     3: iload_1
        //     4: ldc 137
        //     6: if_icmpne +14 -> 20
        //     9: aload_3
        //     10: aload_0
        //     11: invokevirtual 139	android/os/Binder:getInterfaceDescriptor	()Ljava/lang/String;
        //     14: invokevirtual 143	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     17: iload 5
        //     19: ireturn
        //     20: iload_1
        //     21: ldc 144
        //     23: if_icmpne +63 -> 86
        //     26: aload_2
        //     27: invokevirtual 148	android/os/Parcel:readFileDescriptor	()Landroid/os/ParcelFileDescriptor;
        //     30: astore 6
        //     32: aload_2
        //     33: invokevirtual 152	android/os/Parcel:readStringArray	()[Ljava/lang/String;
        //     36: astore 7
        //     38: aload 6
        //     40: ifnull +19 -> 59
        //     43: aload_0
        //     44: aload 6
        //     46: invokevirtual 158	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     49: aload 7
        //     51: invokevirtual 160	android/os/Binder:dump	(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
        //     54: aload 6
        //     56: invokevirtual 163	android/os/ParcelFileDescriptor:close	()V
        //     59: aload_3
        //     60: ifnull +20 -> 80
        //     63: aload_3
        //     64: invokevirtual 166	android/os/Parcel:writeNoException	()V
        //     67: goto -50 -> 17
        //     70: astore 8
        //     72: aload 6
        //     74: invokevirtual 163	android/os/ParcelFileDescriptor:close	()V
        //     77: aload 8
        //     79: athrow
        //     80: invokestatic 171	android/os/StrictMode:clearGatheredViolations	()V
        //     83: goto -66 -> 17
        //     86: iconst_0
        //     87: istore 5
        //     89: goto -72 -> 17
        //     92: astore 10
        //     94: goto -35 -> 59
        //     97: astore 9
        //     99: goto -22 -> 77
        //
        // Exception table:
        //     from	to	target	type
        //     43	54	70	finally
        //     54	59	92	java/io/IOException
        //     72	77	97	java/io/IOException
    }

    public boolean pingBinder()
    {
        return true;
    }

    public IInterface queryLocalInterface(String paramString)
    {
        if (this.mDescriptor.equals(paramString));
        for (IInterface localIInterface = this.mOwner; ; localIInterface = null)
            return localIInterface;
    }

    public final boolean transact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        if (paramParcel1 != null)
            paramParcel1.setDataPosition(0);
        boolean bool = onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
        if (paramParcel2 != null)
            paramParcel2.setDataPosition(0);
        return bool;
    }

    public boolean unlinkToDeath(IBinder.DeathRecipient paramDeathRecipient, int paramInt)
    {
        return true;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Binder
 * JD-Core Version:        0.6.2
 */