package android.os;

public class NullVibrator extends Vibrator
{
    private static final NullVibrator sInstance = new NullVibrator();

    public static NullVibrator getInstance()
    {
        return sInstance;
    }

    public void cancel()
    {
    }

    public boolean hasVibrator()
    {
        return false;
    }

    public void vibrate(long paramLong)
    {
    }

    public void vibrate(long[] paramArrayOfLong, int paramInt)
    {
        if (paramInt >= paramArrayOfLong.length)
            throw new ArrayIndexOutOfBoundsException();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.NullVibrator
 * JD-Core Version:        0.6.2
 */