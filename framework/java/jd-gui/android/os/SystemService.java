package android.os;

public class SystemService
{
    public static void restart(String paramString)
    {
        SystemProperties.set("ctl.restart", paramString);
    }

    public static void start(String paramString)
    {
        SystemProperties.set("ctl.start", paramString);
    }

    public static void stop(String paramString)
    {
        SystemProperties.set("ctl.stop", paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.SystemService
 * JD-Core Version:        0.6.2
 */