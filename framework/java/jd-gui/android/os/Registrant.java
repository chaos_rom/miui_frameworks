package android.os;

import java.lang.ref.WeakReference;

public class Registrant
{
    WeakReference refH;
    Object userObj;
    int what;

    public Registrant(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.refH = new WeakReference(paramHandler);
        this.what = paramInt;
        this.userObj = paramObject;
    }

    public void clear()
    {
        this.refH = null;
        this.userObj = null;
    }

    public Handler getHandler()
    {
        if (this.refH == null);
        for (Handler localHandler = null; ; localHandler = (Handler)this.refH.get())
            return localHandler;
    }

    void internalNotifyRegistrant(Object paramObject, Throwable paramThrowable)
    {
        Handler localHandler = getHandler();
        if (localHandler == null)
            clear();
        while (true)
        {
            return;
            Message localMessage = Message.obtain();
            localMessage.what = this.what;
            localMessage.obj = new AsyncResult(this.userObj, paramObject, paramThrowable);
            localHandler.sendMessage(localMessage);
        }
    }

    public Message messageForRegistrant()
    {
        Handler localHandler = getHandler();
        Message localMessage;
        if (localHandler == null)
        {
            clear();
            localMessage = null;
        }
        while (true)
        {
            return localMessage;
            localMessage = localHandler.obtainMessage();
            localMessage.what = this.what;
            localMessage.obj = this.userObj;
        }
    }

    public void notifyException(Throwable paramThrowable)
    {
        internalNotifyRegistrant(null, paramThrowable);
    }

    public void notifyRegistrant()
    {
        internalNotifyRegistrant(null, null);
    }

    public void notifyRegistrant(AsyncResult paramAsyncResult)
    {
        internalNotifyRegistrant(paramAsyncResult.result, paramAsyncResult.exception);
    }

    public void notifyResult(Object paramObject)
    {
        internalNotifyRegistrant(paramObject, null);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Registrant
 * JD-Core Version:        0.6.2
 */