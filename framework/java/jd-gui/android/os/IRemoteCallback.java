package android.os;

public abstract interface IRemoteCallback extends IInterface
{
    public abstract void sendResult(Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRemoteCallback
    {
        private static final String DESCRIPTOR = "android.os.IRemoteCallback";
        static final int TRANSACTION_sendResult = 1;

        public Stub()
        {
            attachInterface(this, "android.os.IRemoteCallback");
        }

        public static IRemoteCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.os.IRemoteCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IRemoteCallback)))
                    localObject = (IRemoteCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.os.IRemoteCallback");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.os.IRemoteCallback");
            if (paramParcel1.readInt() != 0);
            for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
            {
                sendResult(localBundle);
                break;
            }
        }

        private static class Proxy
            implements IRemoteCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.os.IRemoteCallback";
            }

            public void sendResult(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.os.IRemoteCallback");
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.IRemoteCallback
 * JD-Core Version:        0.6.2
 */