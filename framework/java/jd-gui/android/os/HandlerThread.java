package android.os;

public class HandlerThread extends Thread
{
    Looper mLooper;
    int mPriority;
    int mTid = -1;

    public HandlerThread(String paramString)
    {
        super(paramString);
        this.mPriority = 0;
    }

    public HandlerThread(String paramString, int paramInt)
    {
        super(paramString);
        this.mPriority = paramInt;
    }

    public Looper getLooper()
    {
        Looper localLooper1;
        if (!isAlive())
            localLooper1 = null;
        while (true)
        {
            return localLooper1;
            try
            {
                while (isAlive())
                {
                    Looper localLooper2 = this.mLooper;
                    if (localLooper2 != null)
                        break;
                    try
                    {
                        wait();
                    }
                    catch (InterruptedException localInterruptedException)
                    {
                    }
                }
                localLooper1 = this.mLooper;
            }
            finally
            {
            }
        }
    }

    public int getThreadId()
    {
        return this.mTid;
    }

    protected void onLooperPrepared()
    {
    }

    public boolean quit()
    {
        Looper localLooper = getLooper();
        if (localLooper != null)
            localLooper.quit();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void run()
    {
        this.mTid = Process.myTid();
        Looper.prepare();
        try
        {
            this.mLooper = Looper.myLooper();
            notifyAll();
            Process.setThreadPriority(this.mPriority);
            onLooperPrepared();
            Looper.loop();
            this.mTid = -1;
            return;
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.HandlerThread
 * JD-Core Version:        0.6.2
 */