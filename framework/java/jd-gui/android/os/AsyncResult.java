package android.os;

public class AsyncResult
{
    public Throwable exception;
    public Object result;
    public Object userObj;

    public AsyncResult(Object paramObject1, Object paramObject2, Throwable paramThrowable)
    {
        this.userObj = paramObject1;
        this.result = paramObject2;
        this.exception = paramThrowable;
    }

    public static AsyncResult forMessage(Message paramMessage)
    {
        AsyncResult localAsyncResult = new AsyncResult(paramMessage.obj, null, null);
        paramMessage.obj = localAsyncResult;
        return localAsyncResult;
    }

    public static AsyncResult forMessage(Message paramMessage, Object paramObject, Throwable paramThrowable)
    {
        AsyncResult localAsyncResult = new AsyncResult(paramMessage.obj, paramObject, paramThrowable);
        paramMessage.obj = localAsyncResult;
        return localAsyncResult;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.AsyncResult
 * JD-Core Version:        0.6.2
 */