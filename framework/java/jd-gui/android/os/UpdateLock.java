package android.os;

import android.util.Log;

public class UpdateLock
{
    private static final boolean DEBUG = false;
    public static final String NOW_IS_CONVENIENT = "nowisconvenient";
    private static final String TAG = "UpdateLock";
    public static final String TIMESTAMP = "timestamp";
    public static final String UPDATE_LOCK_CHANGED = "android.os.UpdateLock.UPDATE_LOCK_CHANGED";
    private static IUpdateLock sService;
    int mCount = 0;
    boolean mHeld = false;
    boolean mRefCounted = true;
    final String mTag;
    IBinder mToken;

    public UpdateLock(String paramString)
    {
        this.mTag = paramString;
        this.mToken = new Binder();
    }

    private void acquireLocked()
    {
        if (this.mRefCounted)
        {
            int i = this.mCount;
            this.mCount = (i + 1);
            if (i != 0);
        }
        else if (sService == null);
        try
        {
            sService.acquireUpdateLock(this.mToken, this.mTag);
            this.mHeld = true;
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UpdateLock", "Unable to contact service to acquire");
        }
    }

    private static void checkService()
    {
        if (sService == null)
            sService = IUpdateLock.Stub.asInterface(ServiceManager.getService("updatelock"));
    }

    private void releaseLocked()
    {
        if (this.mRefCounted)
        {
            int i = -1 + this.mCount;
            this.mCount = i;
            if (i != 0);
        }
        else if (sService == null);
        try
        {
            sService.releaseUpdateLock(this.mToken);
            this.mHeld = false;
            if (this.mCount < 0)
                throw new RuntimeException("UpdateLock under-locked");
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UpdateLock", "Unable to contact service to release");
        }
    }

    public void acquire()
    {
        checkService();
        synchronized (this.mToken)
        {
            acquireLocked();
            return;
        }
    }

    protected void finalize()
        throws Throwable
    {
        synchronized (this.mToken)
        {
            if (this.mHeld)
                Log.wtf("UpdateLock", "UpdateLock finalized while still held");
            try
            {
                sService.releaseUpdateLock(this.mToken);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("UpdateLock", "Unable to contact service to release");
            }
        }
    }

    public boolean isHeld()
    {
        synchronized (this.mToken)
        {
            boolean bool = this.mHeld;
            return bool;
        }
    }

    public void release()
    {
        checkService();
        synchronized (this.mToken)
        {
            releaseLocked();
            return;
        }
    }

    public void setReferenceCounted(boolean paramBoolean)
    {
        this.mRefCounted = paramBoolean;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.UpdateLock
 * JD-Core Version:        0.6.2
 */