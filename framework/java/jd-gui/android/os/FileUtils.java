package android.os;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileUtils
{
    private static final Pattern SAFE_FILENAME_PATTERN = Pattern.compile("[\\w%+,./=_-]+");
    public static final int S_IRGRP = 32;
    public static final int S_IROTH = 4;
    public static final int S_IRUSR = 256;
    public static final int S_IRWXG = 56;
    public static final int S_IRWXO = 7;
    public static final int S_IRWXU = 448;
    public static final int S_IWGRP = 16;
    public static final int S_IWOTH = 2;
    public static final int S_IWUSR = 128;
    public static final int S_IXGRP = 8;
    public static final int S_IXOTH = 1;
    public static final int S_IXUSR = 64;

    // ERROR //
    public static long checksumCrc32(File paramFile)
        throws java.io.FileNotFoundException, IOException
    {
        // Byte code:
        //     0: new 57	java/util/zip/CRC32
        //     3: dup
        //     4: invokespecial 58	java/util/zip/CRC32:<init>	()V
        //     7: astore_1
        //     8: aconst_null
        //     9: astore_2
        //     10: new 60	java/util/zip/CheckedInputStream
        //     13: dup
        //     14: new 62	java/io/FileInputStream
        //     17: dup
        //     18: aload_0
        //     19: invokespecial 65	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     22: aload_1
        //     23: invokespecial 68	java/util/zip/CheckedInputStream:<init>	(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V
        //     26: astore_3
        //     27: sipush 128
        //     30: newarray byte
        //     32: astore 6
        //     34: aload_3
        //     35: aload 6
        //     37: invokevirtual 72	java/util/zip/CheckedInputStream:read	([B)I
        //     40: ifge -6 -> 34
        //     43: aload_1
        //     44: invokevirtual 76	java/util/zip/CRC32:getValue	()J
        //     47: lstore 7
        //     49: aload_3
        //     50: ifnull +7 -> 57
        //     53: aload_3
        //     54: invokevirtual 79	java/util/zip/CheckedInputStream:close	()V
        //     57: lload 7
        //     59: lreturn
        //     60: astore 4
        //     62: aload_2
        //     63: ifnull +7 -> 70
        //     66: aload_2
        //     67: invokevirtual 79	java/util/zip/CheckedInputStream:close	()V
        //     70: aload 4
        //     72: athrow
        //     73: astore 9
        //     75: goto -18 -> 57
        //     78: astore 5
        //     80: goto -10 -> 70
        //     83: astore 4
        //     85: aload_3
        //     86: astore_2
        //     87: goto -25 -> 62
        //
        // Exception table:
        //     from	to	target	type
        //     10	27	60	finally
        //     53	57	73	java/io/IOException
        //     66	70	78	java/io/IOException
        //     27	49	83	finally
    }

    // ERROR //
    public static boolean copyFile(File paramFile1, File paramFile2)
    {
        // Byte code:
        //     0: new 62	java/io/FileInputStream
        //     3: dup
        //     4: aload_0
        //     5: invokespecial 65	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     8: astore_2
        //     9: aload_2
        //     10: aload_1
        //     11: invokestatic 85	android/os/FileUtils:copyToFile	(Ljava/io/InputStream;Ljava/io/File;)Z
        //     14: istore 6
        //     16: iload 6
        //     18: istore 5
        //     20: aload_2
        //     21: invokevirtual 86	java/io/FileInputStream:close	()V
        //     24: goto +15 -> 39
        //     27: astore_3
        //     28: aload_2
        //     29: invokevirtual 86	java/io/FileInputStream:close	()V
        //     32: aload_3
        //     33: athrow
        //     34: astore 4
        //     36: iconst_0
        //     37: istore 5
        //     39: iload 5
        //     41: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     9	16	27	finally
        //     0	9	34	java/io/IOException
        //     20	34	34	java/io/IOException
    }

    // ERROR //
    public static boolean copyToFile(java.io.InputStream paramInputStream, File paramFile)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: aload_1
        //     3: invokevirtual 92	java/io/File:exists	()Z
        //     6: ifeq +8 -> 14
        //     9: aload_1
        //     10: invokevirtual 95	java/io/File:delete	()Z
        //     13: pop
        //     14: new 97	java/io/FileOutputStream
        //     17: dup
        //     18: aload_1
        //     19: invokespecial 98	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     22: astore 4
        //     24: sipush 4096
        //     27: newarray byte
        //     29: astore 7
        //     31: aload_0
        //     32: aload 7
        //     34: invokevirtual 101	java/io/InputStream:read	([B)I
        //     37: istore 8
        //     39: iload 8
        //     41: iflt +39 -> 80
        //     44: aload 4
        //     46: aload 7
        //     48: iconst_0
        //     49: iload 8
        //     51: invokevirtual 105	java/io/FileOutputStream:write	([BII)V
        //     54: goto -23 -> 31
        //     57: astore 5
        //     59: aload 4
        //     61: invokevirtual 108	java/io/FileOutputStream:flush	()V
        //     64: aload 4
        //     66: invokevirtual 112	java/io/FileOutputStream:getFD	()Ljava/io/FileDescriptor;
        //     69: invokevirtual 117	java/io/FileDescriptor:sync	()V
        //     72: aload 4
        //     74: invokevirtual 118	java/io/FileOutputStream:close	()V
        //     77: aload 5
        //     79: athrow
        //     80: aload 4
        //     82: invokevirtual 108	java/io/FileOutputStream:flush	()V
        //     85: aload 4
        //     87: invokevirtual 112	java/io/FileOutputStream:getFD	()Ljava/io/FileDescriptor;
        //     90: invokevirtual 117	java/io/FileDescriptor:sync	()V
        //     93: aload 4
        //     95: invokevirtual 118	java/io/FileOutputStream:close	()V
        //     98: iconst_1
        //     99: istore_2
        //     100: goto +14 -> 114
        //     103: astore 6
        //     105: goto -33 -> 72
        //     108: astore 9
        //     110: goto -17 -> 93
        //     113: astore_3
        //     114: iload_2
        //     115: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     24	54	57	finally
        //     64	72	103	java/io/IOException
        //     85	93	108	java/io/IOException
        //     2	24	113	java/io/IOException
        //     59	64	113	java/io/IOException
        //     72	85	113	java/io/IOException
        //     93	98	113	java/io/IOException
    }

    public static native int getFatVolumeId(String paramString);

    @Deprecated
    public static boolean getFileStatus(String paramString, FileStatus paramFileStatus)
    {
        StrictMode.noteDiskRead();
        return getFileStatusNative(paramString, paramFileStatus);
    }

    private static native boolean getFileStatusNative(String paramString, FileStatus paramFileStatus);

    @Deprecated
    public static native int getPermissions(String paramString, int[] paramArrayOfInt);

    public static boolean isFilenameSafe(File paramFile)
    {
        return SAFE_FILENAME_PATTERN.matcher(paramFile.getPath()).matches();
    }

    public static String readTextFile(File paramFile, int paramInt, String paramString)
        throws IOException
    {
        FileInputStream localFileInputStream = new FileInputStream(paramFile);
        while (true)
        {
            long l;
            int k;
            try
            {
                l = paramFile.length();
                if (paramInt <= 0)
                {
                    Object localObject2;
                    if ((l > 0L) && (paramInt == 0))
                    {
                        break label441;
                        byte[] arrayOfByte1 = new byte[paramInt + 1];
                        int i = localFileInputStream.read(arrayOfByte1);
                        if (i <= 0)
                        {
                            localObject2 = "";
                            return localObject2;
                        }
                        if (i <= paramInt)
                        {
                            localObject2 = new String(arrayOfByte1, 0, i);
                            localFileInputStream.close();
                            continue;
                        }
                        if (paramString == null)
                        {
                            localObject2 = new String(arrayOfByte1, 0, paramInt);
                            localFileInputStream.close();
                            continue;
                        }
                        String str1 = new String(arrayOfByte1, 0, paramInt) + paramString;
                        localObject2 = str1;
                        localFileInputStream.close();
                        continue;
                    }
                    else
                    {
                        if (paramInt < 0)
                        {
                            k = 0;
                            Object localObject3 = null;
                            Object localObject4 = null;
                            if (localObject3 != null)
                                k = 1;
                            Object localObject5 = localObject3;
                            localObject3 = localObject4;
                            localObject4 = localObject5;
                            if (localObject4 == null)
                            {
                                int n = -paramInt;
                                localObject4 = new byte[n];
                            }
                            int m = localFileInputStream.read((byte[])localObject4);
                            if (m == localObject4.length)
                                continue;
                            if ((localObject3 == null) && (m <= 0))
                            {
                                localObject2 = "";
                                localFileInputStream.close();
                                continue;
                            }
                            if (localObject3 == null)
                            {
                                localObject2 = new String((byte[])localObject4, 0, m);
                                localFileInputStream.close();
                                continue;
                            }
                            if (m <= 0)
                                break label467;
                            k = 1;
                            System.arraycopy(localObject3, m, localObject3, 0, localObject3.length - m);
                            System.arraycopy(localObject4, 0, localObject3, localObject3.length - m, m);
                            break label467;
                            localObject2 = new String(localObject3);
                            localFileInputStream.close();
                            continue;
                            String str3 = paramString + new String(localObject3);
                            localObject2 = str3;
                            localFileInputStream.close();
                            continue;
                        }
                        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
                        byte[] arrayOfByte2 = new byte[1024];
                        int j = localFileInputStream.read(arrayOfByte2);
                        if (j > 0)
                            localByteArrayOutputStream.write(arrayOfByte2, 0, j);
                        if (j == arrayOfByte2.length)
                            continue;
                        String str2 = localByteArrayOutputStream.toString();
                        localObject2 = str2;
                        localFileInputStream.close();
                        continue;
                    }
                }
            }
            finally
            {
                localFileInputStream.close();
            }
            label441: if ((l > 0L) && ((paramInt == 0) || (l < paramInt)))
            {
                paramInt = (int)l;
                continue;
                label467: if (paramString != null)
                    if (k != 0);
            }
        }
    }

    public static native int setPermissions(String paramString, int paramInt1, int paramInt2, int paramInt3);

    public static native int setUMask(int paramInt);

    public static void stringToFile(String paramString1, String paramString2)
        throws IOException
    {
        FileWriter localFileWriter = new FileWriter(paramString1);
        try
        {
            localFileWriter.write(paramString2);
            return;
        }
        finally
        {
            localFileWriter.close();
        }
    }

    public static boolean sync(FileOutputStream paramFileOutputStream)
    {
        if (paramFileOutputStream != null);
        try
        {
            paramFileOutputStream.getFD().sync();
            bool = true;
            return bool;
        }
        catch (IOException localIOException)
        {
            while (true)
                boolean bool = false;
        }
    }

    @Deprecated
    public static final class FileStatus
    {
        public long atime;
        public int blksize;
        public long blocks;
        public long ctime;
        public int dev;
        public int gid;
        public int ino;
        public int mode;
        public long mtime;
        public int nlink;
        public int rdev;
        public long size;
        public int uid;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.FileUtils
 * JD-Core Version:        0.6.2
 */