package android.os;

public final class CancellationSignal
{
    private boolean mCancelInProgress;
    private boolean mIsCanceled;
    private OnCancelListener mOnCancelListener;
    private ICancellationSignal mRemote;

    public static ICancellationSignal createTransport()
    {
        return new Transport(null);
    }

    public static CancellationSignal fromTransport(ICancellationSignal paramICancellationSignal)
    {
        if ((paramICancellationSignal instanceof Transport));
        for (CancellationSignal localCancellationSignal = ((Transport)paramICancellationSignal).mCancellationSignal; ; localCancellationSignal = null)
            return localCancellationSignal;
    }

    private void waitForCancelFinishedLocked()
    {
        while (this.mCancelInProgress)
            try
            {
                wait();
            }
            catch (InterruptedException localInterruptedException)
            {
            }
    }

    // ERROR //
    public void cancel()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 47	android/os/CancellationSignal:mIsCanceled	Z
        //     6: ifeq +8 -> 14
        //     9: aload_0
        //     10: monitorexit
        //     11: goto +103 -> 114
        //     14: aload_0
        //     15: iconst_1
        //     16: putfield 47	android/os/CancellationSignal:mIsCanceled	Z
        //     19: aload_0
        //     20: iconst_1
        //     21: putfield 39	android/os/CancellationSignal:mCancelInProgress	Z
        //     24: aload_0
        //     25: getfield 49	android/os/CancellationSignal:mOnCancelListener	Landroid/os/CancellationSignal$OnCancelListener;
        //     28: astore_2
        //     29: aload_0
        //     30: getfield 51	android/os/CancellationSignal:mRemote	Landroid/os/ICancellationSignal;
        //     33: astore_3
        //     34: aload_0
        //     35: monitorexit
        //     36: aload_2
        //     37: ifnull +9 -> 46
        //     40: aload_2
        //     41: invokeinterface 54 1 0
        //     46: aload_3
        //     47: ifnull +9 -> 56
        //     50: aload_3
        //     51: invokeinterface 58 1 0
        //     56: aload_0
        //     57: monitorenter
        //     58: aload_0
        //     59: iconst_0
        //     60: putfield 39	android/os/CancellationSignal:mCancelInProgress	Z
        //     63: aload_0
        //     64: invokevirtual 61	java/lang/Object:notifyAll	()V
        //     67: aload_0
        //     68: monitorexit
        //     69: goto +45 -> 114
        //     72: astore 4
        //     74: aload_0
        //     75: monitorexit
        //     76: aload 4
        //     78: athrow
        //     79: astore_1
        //     80: aload_0
        //     81: monitorexit
        //     82: aload_1
        //     83: athrow
        //     84: astore 6
        //     86: aload_0
        //     87: monitorenter
        //     88: aload_0
        //     89: iconst_0
        //     90: putfield 39	android/os/CancellationSignal:mCancelInProgress	Z
        //     93: aload_0
        //     94: invokevirtual 61	java/lang/Object:notifyAll	()V
        //     97: aload_0
        //     98: monitorexit
        //     99: aload 6
        //     101: athrow
        //     102: astore 5
        //     104: goto -48 -> 56
        //     107: astore 7
        //     109: aload_0
        //     110: monitorexit
        //     111: aload 7
        //     113: athrow
        //     114: return
        //
        // Exception table:
        //     from	to	target	type
        //     58	76	72	finally
        //     2	36	79	finally
        //     80	82	79	finally
        //     40	46	84	finally
        //     50	56	84	finally
        //     50	56	102	android/os/RemoteException
        //     88	99	107	finally
        //     109	111	107	finally
    }

    public boolean isCanceled()
    {
        try
        {
            boolean bool = this.mIsCanceled;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public void setOnCancelListener(OnCancelListener paramOnCancelListener)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: invokespecial 67	android/os/CancellationSignal:waitForCancelFinishedLocked	()V
        //     6: aload_0
        //     7: getfield 49	android/os/CancellationSignal:mOnCancelListener	Landroid/os/CancellationSignal$OnCancelListener;
        //     10: aload_1
        //     11: if_acmpne +8 -> 19
        //     14: aload_0
        //     15: monitorexit
        //     16: goto +37 -> 53
        //     19: aload_0
        //     20: aload_1
        //     21: putfield 49	android/os/CancellationSignal:mOnCancelListener	Landroid/os/CancellationSignal$OnCancelListener;
        //     24: aload_0
        //     25: getfield 47	android/os/CancellationSignal:mIsCanceled	Z
        //     28: ifeq +7 -> 35
        //     31: aload_1
        //     32: ifnonnull +13 -> 45
        //     35: aload_0
        //     36: monitorexit
        //     37: goto +16 -> 53
        //     40: astore_2
        //     41: aload_0
        //     42: monitorexit
        //     43: aload_2
        //     44: athrow
        //     45: aload_0
        //     46: monitorexit
        //     47: aload_1
        //     48: invokeinterface 54 1 0
        //     53: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	43	40	finally
        //     45	47	40	finally
    }

    // ERROR //
    public void setRemote(ICancellationSignal paramICancellationSignal)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: invokespecial 67	android/os/CancellationSignal:waitForCancelFinishedLocked	()V
        //     6: aload_0
        //     7: getfield 51	android/os/CancellationSignal:mRemote	Landroid/os/ICancellationSignal;
        //     10: aload_1
        //     11: if_acmpne +8 -> 19
        //     14: aload_0
        //     15: monitorexit
        //     16: goto +41 -> 57
        //     19: aload_0
        //     20: aload_1
        //     21: putfield 51	android/os/CancellationSignal:mRemote	Landroid/os/ICancellationSignal;
        //     24: aload_0
        //     25: getfield 47	android/os/CancellationSignal:mIsCanceled	Z
        //     28: ifeq +7 -> 35
        //     31: aload_1
        //     32: ifnonnull +13 -> 45
        //     35: aload_0
        //     36: monitorexit
        //     37: goto +20 -> 57
        //     40: astore_2
        //     41: aload_0
        //     42: monitorexit
        //     43: aload_2
        //     44: athrow
        //     45: aload_0
        //     46: monitorexit
        //     47: aload_1
        //     48: invokeinterface 58 1 0
        //     53: goto +4 -> 57
        //     56: astore_3
        //     57: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	43	40	finally
        //     45	47	40	finally
        //     47	53	56	android/os/RemoteException
    }

    public void throwIfCanceled()
    {
        if (isCanceled())
            throw new OperationCanceledException();
    }

    private static final class Transport extends ICancellationSignal.Stub
    {
        final CancellationSignal mCancellationSignal = new CancellationSignal();

        public void cancel()
            throws RemoteException
        {
            this.mCancellationSignal.cancel();
        }
    }

    public static abstract interface OnCancelListener
    {
        public abstract void onCancel();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.CancellationSignal
 * JD-Core Version:        0.6.2
 */