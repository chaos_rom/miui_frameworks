package android.os;

import android.util.Log;

public class PowerManager
{
    public static final int ACQUIRE_CAUSES_WAKEUP = 268435456;
    public static final int BRIGHTNESS_DIM = 20;
    public static final int BRIGHTNESS_LOW_BATTERY = 10;
    public static final int BRIGHTNESS_OFF = 0;
    public static final int BRIGHTNESS_ON = 255;
    public static final int FULL_WAKE_LOCK = 26;
    private static final int LOCK_MASK = 63;
    public static final int ON_AFTER_RELEASE = 536870912;
    public static final int PARTIAL_WAKE_LOCK = 1;
    public static final int PROXIMITY_SCREEN_OFF_WAKE_LOCK = 32;

    @Deprecated
    public static final int SCREEN_BRIGHT_WAKE_LOCK = 10;
    public static final int SCREEN_DIM_WAKE_LOCK = 6;
    private static final String TAG = "PowerManager";
    public static final int WAIT_FOR_PROXIMITY_NEGATIVE = 1;
    private static final int WAKE_BIT_CPU_STRONG = 1;
    private static final int WAKE_BIT_CPU_WEAK = 2;
    private static final int WAKE_BIT_KEYBOARD_BRIGHT = 16;
    private static final int WAKE_BIT_PROXIMITY_SCREEN_OFF = 32;
    private static final int WAKE_BIT_SCREEN_BRIGHT = 8;
    private static final int WAKE_BIT_SCREEN_DIM = 4;
    Handler mHandler;
    IPowerManager mService;

    private PowerManager()
    {
    }

    public PowerManager(IPowerManager paramIPowerManager, Handler paramHandler)
    {
        this.mService = paramIPowerManager;
        this.mHandler = paramHandler;
    }

    public int getSupportedWakeLockFlags()
    {
        try
        {
            int j = this.mService.getSupportedWakeLockFlags();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 0;
        }
    }

    public void goToSleep(long paramLong)
    {
        try
        {
            this.mService.goToSleep(paramLong);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public boolean isScreenOn()
    {
        try
        {
            boolean bool2 = this.mService.isScreenOn();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public WakeLock newWakeLock(int paramInt, String paramString)
    {
        if (paramString == null)
            throw new NullPointerException("tag is null in PowerManager.newWakeLock");
        return new WakeLock(paramInt, paramString);
    }

    public void reboot(String paramString)
    {
        try
        {
            this.mService.reboot(paramString);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void setBacklightBrightness(int paramInt)
    {
        try
        {
            this.mService.setBacklightBrightness(paramInt);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void userActivity(long paramLong, boolean paramBoolean)
    {
        try
        {
            this.mService.userActivity(paramLong, paramBoolean);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public class WakeLock
    {
        static final int RELEASE_WAKE_LOCK = 1;
        int mCount = 0;
        int mFlags;
        boolean mHeld = false;
        boolean mRefCounted = true;
        Runnable mReleaser = new Runnable()
        {
            public void run()
            {
                PowerManager.WakeLock.this.release();
            }
        };
        String mTag;
        IBinder mToken;
        WorkSource mWorkSource;

        WakeLock(int paramString, String arg3)
        {
            switch (paramString & 0x3F)
            {
            default:
                throw new IllegalArgumentException();
            case 1:
            case 6:
            case 10:
            case 26:
            case 32:
            }
            this.mFlags = paramString;
            Object localObject;
            this.mTag = localObject;
            this.mToken = new Binder();
        }

        private void acquireLocked()
        {
            if (this.mRefCounted)
            {
                int i = this.mCount;
                this.mCount = (i + 1);
                if (i != 0);
            }
            else
            {
                PowerManager.this.mHandler.removeCallbacks(this.mReleaser);
            }
            try
            {
                PowerManager.this.mService.acquireWakeLock(this.mFlags, this.mToken, this.mTag, this.mWorkSource);
                label65: this.mHeld = true;
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label65;
            }
        }

        public void acquire()
        {
            synchronized (this.mToken)
            {
                acquireLocked();
                return;
            }
        }

        public void acquire(long paramLong)
        {
            synchronized (this.mToken)
            {
                acquireLocked();
                PowerManager.this.mHandler.postDelayed(this.mReleaser, paramLong);
                return;
            }
        }

        protected void finalize()
            throws Throwable
        {
            synchronized (this.mToken)
            {
                if (this.mHeld)
                    Log.wtf("PowerManager", "WakeLock finalized while still held: " + this.mTag);
            }
            try
            {
                PowerManager.this.mService.releaseWakeLock(this.mToken, 0);
                label59: return;
                localObject = finally;
                throw localObject;
            }
            catch (RemoteException localRemoteException)
            {
                break label59;
            }
        }

        public boolean isHeld()
        {
            synchronized (this.mToken)
            {
                boolean bool = this.mHeld;
                return bool;
            }
        }

        public void release()
        {
            release(0);
        }

        public void release(int paramInt)
        {
            synchronized (this.mToken)
            {
                if (this.mRefCounted)
                {
                    int i = -1 + this.mCount;
                    this.mCount = i;
                    if (i != 0);
                }
                else
                {
                    PowerManager.this.mHandler.removeCallbacks(this.mReleaser);
                }
            }
            try
            {
                PowerManager.this.mService.releaseWakeLock(this.mToken, paramInt);
                label65: this.mHeld = false;
                if (this.mCount < 0)
                {
                    throw new RuntimeException("WakeLock under-locked " + this.mTag);
                    localObject = finally;
                    throw localObject;
                }
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label65;
            }
        }

        public void setReferenceCounted(boolean paramBoolean)
        {
            this.mRefCounted = paramBoolean;
        }

        // ERROR //
        public void setWorkSource(WorkSource paramWorkSource)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 57	android/os/PowerManager$WakeLock:mToken	Landroid/os/IBinder;
            //     4: astore_2
            //     5: aload_2
            //     6: monitorenter
            //     7: aload_1
            //     8: ifnull +12 -> 20
            //     11: aload_1
            //     12: invokevirtual 141	android/os/WorkSource:size	()I
            //     15: ifne +5 -> 20
            //     18: aconst_null
            //     19: astore_1
            //     20: iconst_1
            //     21: istore_3
            //     22: aload_1
            //     23: ifnonnull +46 -> 69
            //     26: aload_0
            //     27: aconst_null
            //     28: putfield 76	android/os/PowerManager$WakeLock:mWorkSource	Landroid/os/WorkSource;
            //     31: iload_3
            //     32: ifeq +34 -> 66
            //     35: aload_0
            //     36: getfield 45	android/os/PowerManager$WakeLock:mHeld	Z
            //     39: istore 5
            //     41: iload 5
            //     43: ifeq +23 -> 66
            //     46: aload_0
            //     47: getfield 31	android/os/PowerManager$WakeLock:this$0	Landroid/os/PowerManager;
            //     50: getfield 74	android/os/PowerManager:mService	Landroid/os/IPowerManager;
            //     53: aload_0
            //     54: getfield 57	android/os/PowerManager$WakeLock:mToken	Landroid/os/IBinder;
            //     57: aload_0
            //     58: getfield 76	android/os/PowerManager$WakeLock:mWorkSource	Landroid/os/WorkSource;
            //     61: invokeinterface 145 3 0
            //     66: aload_2
            //     67: monitorexit
            //     68: return
            //     69: aload_0
            //     70: getfield 76	android/os/PowerManager$WakeLock:mWorkSource	Landroid/os/WorkSource;
            //     73: ifnonnull +39 -> 112
            //     76: aload_0
            //     77: getfield 76	android/os/PowerManager$WakeLock:mWorkSource	Landroid/os/WorkSource;
            //     80: ifnull +27 -> 107
            //     83: iconst_1
            //     84: istore_3
            //     85: aload_0
            //     86: new 137	android/os/WorkSource
            //     89: dup
            //     90: aload_1
            //     91: invokespecial 147	android/os/WorkSource:<init>	(Landroid/os/WorkSource;)V
            //     94: putfield 76	android/os/PowerManager$WakeLock:mWorkSource	Landroid/os/WorkSource;
            //     97: goto -66 -> 31
            //     100: astore 4
            //     102: aload_2
            //     103: monitorexit
            //     104: aload 4
            //     106: athrow
            //     107: iconst_0
            //     108: istore_3
            //     109: goto -24 -> 85
            //     112: aload_0
            //     113: getfield 76	android/os/PowerManager$WakeLock:mWorkSource	Landroid/os/WorkSource;
            //     116: aload_1
            //     117: invokevirtual 151	android/os/WorkSource:diff	(Landroid/os/WorkSource;)Z
            //     120: istore_3
            //     121: iload_3
            //     122: ifeq -91 -> 31
            //     125: aload_0
            //     126: getfield 76	android/os/PowerManager$WakeLock:mWorkSource	Landroid/os/WorkSource;
            //     129: aload_1
            //     130: invokevirtual 154	android/os/WorkSource:set	(Landroid/os/WorkSource;)V
            //     133: goto -102 -> 31
            //     136: astore 6
            //     138: goto -72 -> 66
            //
            // Exception table:
            //     from	to	target	type
            //     11	41	100	finally
            //     46	66	100	finally
            //     66	104	100	finally
            //     112	133	100	finally
            //     46	66	136	android/os/RemoteException
        }

        public String toString()
        {
            synchronized (this.mToken)
            {
                String str = "WakeLock{" + Integer.toHexString(System.identityHashCode(this)) + " held=" + this.mHeld + ", refCount=" + this.mCount + "}";
                return str;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.PowerManager
 * JD-Core Version:        0.6.2
 */