package android.os;

public abstract class RemoteCallback
    implements Parcelable
{
    public static final Parcelable.Creator<RemoteCallback> CREATOR = new Parcelable.Creator()
    {
        public RemoteCallback createFromParcel(Parcel paramAnonymousParcel)
        {
            IBinder localIBinder = paramAnonymousParcel.readStrongBinder();
            if (localIBinder != null);
            for (RemoteCallback.RemoteCallbackProxy localRemoteCallbackProxy = new RemoteCallback.RemoteCallbackProxy(IRemoteCallback.Stub.asInterface(localIBinder)); ; localRemoteCallbackProxy = null)
                return localRemoteCallbackProxy;
        }

        public RemoteCallback[] newArray(int paramAnonymousInt)
        {
            return new RemoteCallback[paramAnonymousInt];
        }
    };
    final Handler mHandler;
    final IRemoteCallback mTarget;

    public RemoteCallback(Handler paramHandler)
    {
        this.mHandler = paramHandler;
        this.mTarget = new LocalCallback();
    }

    RemoteCallback(IRemoteCallback paramIRemoteCallback)
    {
        this.mHandler = null;
        this.mTarget = paramIRemoteCallback;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool1 = false;
        if (paramObject == null);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mTarget.asBinder().equals(((RemoteCallback)paramObject).mTarget.asBinder());
                bool1 = bool2;
            }
            catch (ClassCastException localClassCastException)
            {
            }
        }
    }

    public int hashCode()
    {
        return this.mTarget.asBinder().hashCode();
    }

    protected abstract void onResult(Bundle paramBundle);

    public void sendResult(Bundle paramBundle)
        throws RemoteException
    {
        this.mTarget.sendResult(paramBundle);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStrongBinder(this.mTarget.asBinder());
    }

    static class RemoteCallbackProxy extends RemoteCallback
    {
        RemoteCallbackProxy(IRemoteCallback paramIRemoteCallback)
        {
            super();
        }

        protected void onResult(Bundle paramBundle)
        {
        }
    }

    class LocalCallback extends IRemoteCallback.Stub
    {
        LocalCallback()
        {
        }

        public void sendResult(Bundle paramBundle)
        {
            RemoteCallback.this.mHandler.post(new RemoteCallback.DeliverResult(RemoteCallback.this, paramBundle));
        }
    }

    class DeliverResult
        implements Runnable
    {
        final Bundle mResult;

        DeliverResult(Bundle arg2)
        {
            Object localObject;
            this.mResult = localObject;
        }

        public void run()
        {
            RemoteCallback.this.onResult(this.mResult);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.RemoteCallback
 * JD-Core Version:        0.6.2
 */