package android.os;

import java.util.HashMap;

public class RemoteCallbackList<E extends IInterface>
{
    private Object[] mActiveBroadcast;
    private int mBroadcastCount = -1;
    HashMap<IBinder, RemoteCallbackList<E>.Callback> mCallbacks = new HashMap();
    private boolean mKilled = false;

    // ERROR //
    public int beginBroadcast()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 26	android/os/RemoteCallbackList:mCallbacks	Ljava/util/HashMap;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 28	android/os/RemoteCallbackList:mBroadcastCount	I
        //     11: ifle +18 -> 29
        //     14: new 34	java/lang/IllegalStateException
        //     17: dup
        //     18: ldc 36
        //     20: invokespecial 39	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: astore_2
        //     25: aload_1
        //     26: monitorexit
        //     27: aload_2
        //     28: athrow
        //     29: aload_0
        //     30: getfield 26	android/os/RemoteCallbackList:mCallbacks	Ljava/util/HashMap;
        //     33: invokevirtual 42	java/util/HashMap:size	()I
        //     36: istore_3
        //     37: aload_0
        //     38: iload_3
        //     39: putfield 28	android/os/RemoteCallbackList:mBroadcastCount	I
        //     42: iload_3
        //     43: ifgt +11 -> 54
        //     46: iconst_0
        //     47: istore 6
        //     49: aload_1
        //     50: monitorexit
        //     51: goto +94 -> 145
        //     54: aload_0
        //     55: getfield 44	android/os/RemoteCallbackList:mActiveBroadcast	[Ljava/lang/Object;
        //     58: astore 4
        //     60: aload 4
        //     62: ifnull +10 -> 72
        //     65: aload 4
        //     67: arraylength
        //     68: iload_3
        //     69: if_icmpge +15 -> 84
        //     72: iload_3
        //     73: anewarray 5	java/lang/Object
        //     76: astore 4
        //     78: aload_0
        //     79: aload 4
        //     81: putfield 44	android/os/RemoteCallbackList:mActiveBroadcast	[Ljava/lang/Object;
        //     84: aload_0
        //     85: getfield 26	android/os/RemoteCallbackList:mCallbacks	Ljava/util/HashMap;
        //     88: invokevirtual 48	java/util/HashMap:values	()Ljava/util/Collection;
        //     91: invokeinterface 54 1 0
        //     96: astore 5
        //     98: iconst_0
        //     99: istore 6
        //     101: aload 5
        //     103: invokeinterface 60 1 0
        //     108: ifeq +35 -> 143
        //     111: aload 5
        //     113: invokeinterface 64 1 0
        //     118: checkcast 7	android/os/RemoteCallbackList$Callback
        //     121: astore 7
        //     123: iload 6
        //     125: iconst_1
        //     126: iadd
        //     127: istore 8
        //     129: aload 4
        //     131: iload 6
        //     133: aload 7
        //     135: aastore
        //     136: iload 8
        //     138: istore 6
        //     140: goto -39 -> 101
        //     143: aload_1
        //     144: monitorexit
        //     145: iload 6
        //     147: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	27	24	finally
        //     29	145	24	finally
    }

    public void finishBroadcast()
    {
        if (this.mBroadcastCount < 0)
            throw new IllegalStateException("finishBroadcast() called outside of a broadcast");
        Object[] arrayOfObject = this.mActiveBroadcast;
        if (arrayOfObject != null)
        {
            int i = this.mBroadcastCount;
            for (int j = 0; j < i; j++)
                arrayOfObject[j] = null;
        }
        this.mBroadcastCount = -1;
    }

    public Object getBroadcastCookie(int paramInt)
    {
        return ((Callback)this.mActiveBroadcast[paramInt]).mCookie;
    }

    public E getBroadcastItem(int paramInt)
    {
        return ((Callback)this.mActiveBroadcast[paramInt]).mCallback;
    }

    // ERROR //
    public void kill()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 26	android/os/RemoteCallbackList:mCallbacks	Ljava/util/HashMap;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 26	android/os/RemoteCallbackList:mCallbacks	Ljava/util/HashMap;
        //     11: invokevirtual 48	java/util/HashMap:values	()Ljava/util/Collection;
        //     14: invokeinterface 54 1 0
        //     19: astore_3
        //     20: aload_3
        //     21: invokeinterface 60 1 0
        //     26: ifeq +41 -> 67
        //     29: aload_3
        //     30: invokeinterface 64 1 0
        //     35: checkcast 7	android/os/RemoteCallbackList$Callback
        //     38: astore 4
        //     40: aload 4
        //     42: getfield 79	android/os/RemoteCallbackList$Callback:mCallback	Landroid/os/IInterface;
        //     45: invokeinterface 86 1 0
        //     50: aload 4
        //     52: iconst_0
        //     53: invokeinterface 92 3 0
        //     58: pop
        //     59: goto -39 -> 20
        //     62: astore_2
        //     63: aload_1
        //     64: monitorexit
        //     65: aload_2
        //     66: athrow
        //     67: aload_0
        //     68: getfield 26	android/os/RemoteCallbackList:mCallbacks	Ljava/util/HashMap;
        //     71: invokevirtual 95	java/util/HashMap:clear	()V
        //     74: aload_0
        //     75: iconst_1
        //     76: putfield 30	android/os/RemoteCallbackList:mKilled	Z
        //     79: aload_1
        //     80: monitorexit
        //     81: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	65	62	finally
        //     67	81	62	finally
    }

    public void onCallbackDied(E paramE)
    {
    }

    public void onCallbackDied(E paramE, Object paramObject)
    {
        onCallbackDied(paramE);
    }

    public boolean register(E paramE)
    {
        return register(paramE, null);
    }

    public boolean register(E paramE, Object paramObject)
    {
        boolean bool = false;
        IBinder localIBinder;
        synchronized (this.mCallbacks)
        {
            if (this.mKilled)
                break label87;
            localIBinder = paramE.asBinder();
        }
        try
        {
            Callback localCallback = new Callback(paramE, paramObject);
            localIBinder.linkToDeath(localCallback, 0);
            this.mCallbacks.put(localIBinder, localCallback);
            bool = true;
            break label87;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
        }
        label87: return bool;
    }

    public boolean unregister(E paramE)
    {
        boolean bool = false;
        synchronized (this.mCallbacks)
        {
            Callback localCallback = (Callback)this.mCallbacks.remove(paramE.asBinder());
            if (localCallback != null)
            {
                localCallback.mCallback.asBinder().unlinkToDeath(localCallback, 0);
                bool = true;
            }
        }
        return bool;
    }

    private final class Callback
        implements IBinder.DeathRecipient
    {
        final E mCallback;
        final Object mCookie;

        Callback(Object arg2)
        {
            Object localObject1;
            this.mCallback = localObject1;
            Object localObject2;
            this.mCookie = localObject2;
        }

        public void binderDied()
        {
            synchronized (RemoteCallbackList.this.mCallbacks)
            {
                RemoteCallbackList.this.mCallbacks.remove(this.mCallback.asBinder());
                RemoteCallbackList.this.onCallbackDied(this.mCallback, this.mCookie);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.RemoteCallbackList
 * JD-Core Version:        0.6.2
 */