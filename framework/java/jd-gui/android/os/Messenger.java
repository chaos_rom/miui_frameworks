package android.os;

public final class Messenger
    implements Parcelable
{
    public static final Parcelable.Creator<Messenger> CREATOR = new Parcelable.Creator()
    {
        public Messenger createFromParcel(Parcel paramAnonymousParcel)
        {
            IBinder localIBinder = paramAnonymousParcel.readStrongBinder();
            if (localIBinder != null);
            for (Messenger localMessenger = new Messenger(localIBinder); ; localMessenger = null)
                return localMessenger;
        }

        public Messenger[] newArray(int paramAnonymousInt)
        {
            return new Messenger[paramAnonymousInt];
        }
    };
    private final IMessenger mTarget;

    public Messenger(Handler paramHandler)
    {
        this.mTarget = paramHandler.getIMessenger();
    }

    public Messenger(IBinder paramIBinder)
    {
        this.mTarget = IMessenger.Stub.asInterface(paramIBinder);
    }

    public static Messenger readMessengerOrNullFromParcel(Parcel paramParcel)
    {
        IBinder localIBinder = paramParcel.readStrongBinder();
        if (localIBinder != null);
        for (Messenger localMessenger = new Messenger(localIBinder); ; localMessenger = null)
            return localMessenger;
    }

    public static void writeMessengerOrNullToParcel(Messenger paramMessenger, Parcel paramParcel)
    {
        if (paramMessenger != null);
        for (IBinder localIBinder = paramMessenger.mTarget.asBinder(); ; localIBinder = null)
        {
            paramParcel.writeStrongBinder(localIBinder);
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool1 = false;
        if (paramObject == null);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mTarget.asBinder().equals(((Messenger)paramObject).mTarget.asBinder());
                bool1 = bool2;
            }
            catch (ClassCastException localClassCastException)
            {
            }
        }
    }

    public IBinder getBinder()
    {
        return this.mTarget.asBinder();
    }

    public int hashCode()
    {
        return this.mTarget.asBinder().hashCode();
    }

    public void send(Message paramMessage)
        throws RemoteException
    {
        this.mTarget.send(paramMessage);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStrongBinder(this.mTarget.asBinder());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Messenger
 * JD-Core Version:        0.6.2
 */