package android.os;

import android.util.Log;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

public abstract class TokenWatcher
{
    private volatile boolean mAcquired = false;
    private Handler mHandler;
    private int mNotificationQueue = -1;
    private Runnable mNotificationTask = new Runnable()
    {
        public void run()
        {
            while (true)
            {
                int i;
                synchronized (TokenWatcher.this.mTokens)
                {
                    i = TokenWatcher.this.mNotificationQueue;
                    TokenWatcher.access$102(TokenWatcher.this, -1);
                    if (i == 1)
                    {
                        TokenWatcher.this.acquired();
                        return;
                    }
                }
                if (i == 0)
                    TokenWatcher.this.released();
            }
        }
    };
    private String mTag;
    private WeakHashMap<IBinder, Death> mTokens = new WeakHashMap();

    public TokenWatcher(Handler paramHandler, String paramString)
    {
        this.mHandler = paramHandler;
        if (paramString != null);
        while (true)
        {
            this.mTag = paramString;
            return;
            paramString = "TokenWatcher";
        }
    }

    private ArrayList<String> dumpInternal()
    {
        ArrayList localArrayList = new ArrayList();
        synchronized (this.mTokens)
        {
            Set localSet = this.mTokens.keySet();
            localArrayList.add("Token count: " + this.mTokens.size());
            int i = 0;
            Iterator localIterator = localSet.iterator();
            while (localIterator.hasNext())
            {
                IBinder localIBinder = (IBinder)localIterator.next();
                localArrayList.add("[" + i + "] " + ((Death)this.mTokens.get(localIBinder)).tag + " - " + localIBinder);
                i++;
            }
            return localArrayList;
        }
    }

    private void sendNotificationLocked(boolean paramBoolean)
    {
        int i;
        if (paramBoolean)
        {
            i = 1;
            if (this.mNotificationQueue != -1)
                break label38;
            this.mNotificationQueue = i;
            this.mHandler.post(this.mNotificationTask);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label38: if (this.mNotificationQueue != i)
            {
                this.mNotificationQueue = -1;
                this.mHandler.removeCallbacks(this.mNotificationTask);
            }
        }
    }

    public void acquire(IBinder paramIBinder, String paramString)
    {
        synchronized (this.mTokens)
        {
            int i = this.mTokens.size();
            Death localDeath = new Death(paramIBinder, paramString);
            try
            {
                paramIBinder.linkToDeath(localDeath, 0);
                this.mTokens.put(paramIBinder, localDeath);
                if ((i == 0) && (!this.mAcquired))
                {
                    sendNotificationLocked(true);
                    this.mAcquired = true;
                }
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public abstract void acquired();

    public void cleanup(IBinder paramIBinder, boolean paramBoolean)
    {
        synchronized (this.mTokens)
        {
            Death localDeath = (Death)this.mTokens.remove(paramIBinder);
            if ((paramBoolean) && (localDeath != null))
            {
                localDeath.token.unlinkToDeath(localDeath, 0);
                localDeath.token = null;
            }
            if ((this.mTokens.size() == 0) && (this.mAcquired))
            {
                sendNotificationLocked(false);
                this.mAcquired = false;
            }
            return;
        }
    }

    public void dump()
    {
        Iterator localIterator = dumpInternal().iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            Log.i(this.mTag, str);
        }
    }

    public void dump(PrintWriter paramPrintWriter)
    {
        Iterator localIterator = dumpInternal().iterator();
        while (localIterator.hasNext())
            paramPrintWriter.println((String)localIterator.next());
    }

    public boolean isAcquired()
    {
        synchronized (this.mTokens)
        {
            boolean bool = this.mAcquired;
            return bool;
        }
    }

    public void release(IBinder paramIBinder)
    {
        cleanup(paramIBinder, true);
    }

    public abstract void released();

    private class Death
        implements IBinder.DeathRecipient
    {
        String tag;
        IBinder token;

        Death(IBinder paramString, String arg3)
        {
            this.token = paramString;
            Object localObject;
            this.tag = localObject;
        }

        public void binderDied()
        {
            TokenWatcher.this.cleanup(this.token, false);
        }

        protected void finalize()
            throws Throwable
        {
            try
            {
                if (this.token != null)
                {
                    Log.w(TokenWatcher.this.mTag, "cleaning up leaked reference: " + this.tag);
                    TokenWatcher.this.release(this.token);
                }
                return;
            }
            finally
            {
                super.finalize();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.TokenWatcher
 * JD-Core Version:        0.6.2
 */