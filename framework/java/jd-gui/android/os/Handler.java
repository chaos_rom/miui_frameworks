package android.os;

import android.util.Log;
import android.util.Printer;

public class Handler
{
    private static final boolean FIND_POTENTIAL_LEAKS = false;
    private static final String TAG = "Handler";
    final Callback mCallback;
    final Looper mLooper;
    IMessenger mMessenger;
    final MessageQueue mQueue;

    public Handler()
    {
        this.mLooper = Looper.myLooper();
        if (this.mLooper == null)
            throw new RuntimeException("Can't create handler inside thread that has not called Looper.prepare()");
        this.mQueue = this.mLooper.mQueue;
        this.mCallback = null;
    }

    public Handler(Callback paramCallback)
    {
        this.mLooper = Looper.myLooper();
        if (this.mLooper == null)
            throw new RuntimeException("Can't create handler inside thread that has not called Looper.prepare()");
        this.mQueue = this.mLooper.mQueue;
        this.mCallback = paramCallback;
    }

    public Handler(Looper paramLooper)
    {
        this.mLooper = paramLooper;
        this.mQueue = paramLooper.mQueue;
        this.mCallback = null;
    }

    public Handler(Looper paramLooper, Callback paramCallback)
    {
        this.mLooper = paramLooper;
        this.mQueue = paramLooper.mQueue;
        this.mCallback = paramCallback;
    }

    private static Message getPostMessage(Runnable paramRunnable)
    {
        Message localMessage = Message.obtain();
        localMessage.callback = paramRunnable;
        return localMessage;
    }

    private static Message getPostMessage(Runnable paramRunnable, Object paramObject)
    {
        Message localMessage = Message.obtain();
        localMessage.obj = paramObject;
        localMessage.callback = paramRunnable;
        return localMessage;
    }

    private static void handleCallback(Message paramMessage)
    {
        paramMessage.callback.run();
    }

    public void dispatchMessage(Message paramMessage)
    {
        if (paramMessage.callback != null)
            handleCallback(paramMessage);
        while (true)
        {
            return;
            if ((this.mCallback == null) || (!this.mCallback.handleMessage(paramMessage)))
                handleMessage(paramMessage);
        }
    }

    public final void dump(Printer paramPrinter, String paramString)
    {
        paramPrinter.println(paramString + this + " @ " + SystemClock.uptimeMillis());
        if (this.mLooper == null)
            paramPrinter.println(paramString + "looper uninitialized");
        while (true)
        {
            return;
            this.mLooper.dump(paramPrinter, paramString + "    ");
        }
    }

    final IMessenger getIMessenger()
    {
        IMessenger localIMessenger;
        synchronized (this.mQueue)
        {
            if (this.mMessenger != null)
            {
                localIMessenger = this.mMessenger;
            }
            else
            {
                this.mMessenger = new MessengerImpl(null);
                localIMessenger = this.mMessenger;
            }
        }
        return localIMessenger;
    }

    public final Looper getLooper()
    {
        return this.mLooper;
    }

    public String getMessageName(Message paramMessage)
    {
        if (paramMessage.callback != null);
        for (String str = paramMessage.callback.getClass().getName(); ; str = "0x" + Integer.toHexString(paramMessage.what))
            return str;
    }

    public void handleMessage(Message paramMessage)
    {
    }

    public final boolean hasCallbacks(Runnable paramRunnable)
    {
        return this.mQueue.hasMessages(this, paramRunnable, null);
    }

    public final boolean hasMessages(int paramInt)
    {
        return this.mQueue.hasMessages(this, paramInt, null);
    }

    public final boolean hasMessages(int paramInt, Object paramObject)
    {
        return this.mQueue.hasMessages(this, paramInt, paramObject);
    }

    public final Message obtainMessage()
    {
        return Message.obtain(this);
    }

    public final Message obtainMessage(int paramInt)
    {
        return Message.obtain(this, paramInt);
    }

    public final Message obtainMessage(int paramInt1, int paramInt2, int paramInt3)
    {
        return Message.obtain(this, paramInt1, paramInt2, paramInt3);
    }

    public final Message obtainMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        return Message.obtain(this, paramInt1, paramInt2, paramInt3, paramObject);
    }

    public final Message obtainMessage(int paramInt, Object paramObject)
    {
        return Message.obtain(this, paramInt, paramObject);
    }

    public final boolean post(Runnable paramRunnable)
    {
        return sendMessageDelayed(getPostMessage(paramRunnable), 0L);
    }

    public final boolean postAtFrontOfQueue(Runnable paramRunnable)
    {
        return sendMessageAtFrontOfQueue(getPostMessage(paramRunnable));
    }

    public final boolean postAtTime(Runnable paramRunnable, long paramLong)
    {
        return sendMessageAtTime(getPostMessage(paramRunnable), paramLong);
    }

    public final boolean postAtTime(Runnable paramRunnable, Object paramObject, long paramLong)
    {
        return sendMessageAtTime(getPostMessage(paramRunnable, paramObject), paramLong);
    }

    public final boolean postDelayed(Runnable paramRunnable, long paramLong)
    {
        return sendMessageDelayed(getPostMessage(paramRunnable), paramLong);
    }

    public final void removeCallbacks(Runnable paramRunnable)
    {
        this.mQueue.removeMessages(this, paramRunnable, null);
    }

    public final void removeCallbacks(Runnable paramRunnable, Object paramObject)
    {
        this.mQueue.removeMessages(this, paramRunnable, paramObject);
    }

    public final void removeCallbacksAndMessages(Object paramObject)
    {
        this.mQueue.removeCallbacksAndMessages(this, paramObject);
    }

    public final void removeMessages(int paramInt)
    {
        this.mQueue.removeMessages(this, paramInt, null);
    }

    public final void removeMessages(int paramInt, Object paramObject)
    {
        this.mQueue.removeMessages(this, paramInt, paramObject);
    }

    public final boolean sendEmptyMessage(int paramInt)
    {
        return sendEmptyMessageDelayed(paramInt, 0L);
    }

    public final boolean sendEmptyMessageAtTime(int paramInt, long paramLong)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt;
        return sendMessageAtTime(localMessage, paramLong);
    }

    public final boolean sendEmptyMessageDelayed(int paramInt, long paramLong)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt;
        return sendMessageDelayed(localMessage, paramLong);
    }

    public final boolean sendMessage(Message paramMessage)
    {
        return sendMessageDelayed(paramMessage, 0L);
    }

    public final boolean sendMessageAtFrontOfQueue(Message paramMessage)
    {
        boolean bool = false;
        MessageQueue localMessageQueue = this.mQueue;
        if (localMessageQueue != null)
        {
            paramMessage.target = this;
            bool = localMessageQueue.enqueueMessage(paramMessage, 0L);
        }
        while (true)
        {
            return bool;
            RuntimeException localRuntimeException = new RuntimeException(this + " sendMessageAtTime() called with no mQueue");
            Log.w("Looper", localRuntimeException.getMessage(), localRuntimeException);
        }
    }

    public boolean sendMessageAtTime(Message paramMessage, long paramLong)
    {
        boolean bool = false;
        MessageQueue localMessageQueue = this.mQueue;
        if (localMessageQueue != null)
        {
            paramMessage.target = this;
            bool = localMessageQueue.enqueueMessage(paramMessage, paramLong);
        }
        while (true)
        {
            return bool;
            RuntimeException localRuntimeException = new RuntimeException(this + " sendMessageAtTime() called with no mQueue");
            Log.w("Looper", localRuntimeException.getMessage(), localRuntimeException);
        }
    }

    public final boolean sendMessageDelayed(Message paramMessage, long paramLong)
    {
        if (paramLong < 0L)
            paramLong = 0L;
        return sendMessageAtTime(paramMessage, paramLong + SystemClock.uptimeMillis());
    }

    public String toString()
    {
        return "Handler (" + getClass().getName() + ") {" + Integer.toHexString(System.identityHashCode(this)) + "}";
    }

    private final class MessengerImpl extends IMessenger.Stub
    {
        private MessengerImpl()
        {
        }

        public void send(Message paramMessage)
        {
            Handler.this.sendMessage(paramMessage);
        }
    }

    public static abstract interface Callback
    {
        public abstract boolean handleMessage(Message paramMessage);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Handler
 * JD-Core Version:        0.6.2
 */