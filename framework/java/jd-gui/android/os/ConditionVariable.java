package android.os;

public class ConditionVariable
{
    private volatile boolean mCondition;

    public ConditionVariable()
    {
        this.mCondition = false;
    }

    public ConditionVariable(boolean paramBoolean)
    {
        this.mCondition = paramBoolean;
    }

    public void block()
    {
        try
        {
            while (true)
            {
                boolean bool = this.mCondition;
                if (bool)
                    break;
                try
                {
                    wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
            return;
        }
        finally
        {
        }
    }

    // ERROR //
    public boolean block(long paramLong)
    {
        // Byte code:
        //     0: lload_1
        //     1: lconst_0
        //     2: lcmp
        //     3: ifeq +73 -> 76
        //     6: aload_0
        //     7: monitorenter
        //     8: invokestatic 26	java/lang/System:currentTimeMillis	()J
        //     11: lstore 5
        //     13: lload 5
        //     15: lload_1
        //     16: ladd
        //     17: lstore 7
        //     19: aload_0
        //     20: getfield 12	android/os/ConditionVariable:mCondition	Z
        //     23: istore 9
        //     25: iload 9
        //     27: ifne +32 -> 59
        //     30: lload 5
        //     32: lload 7
        //     34: lcmp
        //     35: ifge +24 -> 59
        //     38: lload 7
        //     40: lload 5
        //     42: lsub
        //     43: lstore 10
        //     45: aload_0
        //     46: lload 10
        //     48: invokevirtual 29	java/lang/Object:wait	(J)V
        //     51: invokestatic 26	java/lang/System:currentTimeMillis	()J
        //     54: lstore 5
        //     56: goto -37 -> 19
        //     59: aload_0
        //     60: getfield 12	android/os/ConditionVariable:mCondition	Z
        //     63: istore_3
        //     64: aload_0
        //     65: monitorexit
        //     66: goto +24 -> 90
        //     69: astore 4
        //     71: aload_0
        //     72: monitorexit
        //     73: aload 4
        //     75: athrow
        //     76: aload_0
        //     77: invokevirtual 31	android/os/ConditionVariable:block	()V
        //     80: iconst_1
        //     81: istore_3
        //     82: goto +8 -> 90
        //     85: astore 12
        //     87: goto -36 -> 51
        //     90: iload_3
        //     91: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     8	25	69	finally
        //     45	51	69	finally
        //     51	73	69	finally
        //     45	51	85	java/lang/InterruptedException
    }

    public void close()
    {
        try
        {
            this.mCondition = false;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void open()
    {
        try
        {
            boolean bool = this.mCondition;
            this.mCondition = true;
            if (!bool)
                notifyAll();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ConditionVariable
 * JD-Core Version:        0.6.2
 */