package android.os;

public abstract class ServiceManagerNative extends Binder
    implements IServiceManager
{
    public ServiceManagerNative()
    {
        attachInterface(this, "android.os.IServiceManager");
    }

    public static IServiceManager asInterface(IBinder paramIBinder)
    {
        Object localObject;
        if (paramIBinder == null)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = (IServiceManager)paramIBinder.queryLocalInterface("android.os.IServiceManager");
            if (localObject == null)
                localObject = new ServiceManagerProxy(paramIBinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
    {
        boolean bool1 = true;
        switch (paramInt1)
        {
        case 5:
        default:
            bool1 = false;
            label47: return bool1;
        case 1:
        case 2:
        case 3:
        case 4:
        case 6:
        }
        while (true)
        {
            try
            {
                paramParcel1.enforceInterface("android.os.IServiceManager");
                paramParcel2.writeStrongBinder(getService(paramParcel1.readString()));
                break label47;
                paramParcel1.enforceInterface("android.os.IServiceManager");
                paramParcel2.writeStrongBinder(checkService(paramParcel1.readString()));
                break label47;
                paramParcel1.enforceInterface("android.os.IServiceManager");
                String str = paramParcel1.readString();
                IBinder localIBinder = paramParcel1.readStrongBinder();
                if (paramParcel1.readInt() == 0)
                    break label176;
                bool2 = bool1;
                addService(str, localIBinder, bool2);
                break label47;
                paramParcel1.enforceInterface("android.os.IServiceManager");
                paramParcel2.writeStringArray(listServices());
                break label47;
                paramParcel1.enforceInterface("android.os.IServiceManager");
                setPermissionController(IPermissionController.Stub.asInterface(paramParcel1.readStrongBinder()));
            }
            catch (RemoteException localRemoteException)
            {
            }
            break;
            label176: boolean bool2 = false;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ServiceManagerNative
 * JD-Core Version:        0.6.2
 */