package android.os;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageItemInfo;
import android.telephony.SignalStrength;
import android.util.Printer;
import android.util.SparseArray;
import android.util.TimeUtils;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public abstract class BatteryStats
    implements Parcelable
{
    private static final String APK_DATA = "apk";
    public static final int AUDIO_TURNED_ON = 7;
    private static final String BATTERY_DATA = "bt";
    private static final String BATTERY_DISCHARGE_DATA = "dc";
    private static final String BATTERY_LEVEL_DATA = "lv";
    private static final int BATTERY_STATS_CHECKIN_VERSION = 5;
    private static final long BYTES_PER_GB = 1073741824L;
    private static final long BYTES_PER_KB = 1024L;
    private static final long BYTES_PER_MB = 1048576L;
    public static final int DATA_CONNECTION_1xRTT = 7;
    public static final int DATA_CONNECTION_CDMA = 4;
    private static final String DATA_CONNECTION_COUNT_DATA = "dcc";
    public static final int DATA_CONNECTION_EDGE = 2;
    public static final int DATA_CONNECTION_EHRPD = 14;
    public static final int DATA_CONNECTION_EVDO_0 = 5;
    public static final int DATA_CONNECTION_EVDO_A = 6;
    public static final int DATA_CONNECTION_EVDO_B = 12;
    public static final int DATA_CONNECTION_GPRS = 1;
    public static final int DATA_CONNECTION_HSDPA = 8;
    public static final int DATA_CONNECTION_HSPA = 10;
    public static final int DATA_CONNECTION_HSUPA = 9;
    public static final int DATA_CONNECTION_IDEN = 11;
    public static final int DATA_CONNECTION_LTE = 13;
    static final String[] DATA_CONNECTION_NAMES;
    public static final int DATA_CONNECTION_NONE = 0;
    public static final int DATA_CONNECTION_OTHER = 15;
    private static final String DATA_CONNECTION_TIME_DATA = "dct";
    public static final int DATA_CONNECTION_UMTS = 3;
    public static final int FULL_WIFI_LOCK = 5;
    public static final BitDescription[] HISTORY_STATE_DESCRIPTIONS = arrayOfBitDescription;
    private static final String KERNEL_WAKELOCK_DATA = "kwl";
    private static final boolean LOCAL_LOGV = false;
    private static final String MISC_DATA = "m";
    private static final String NETWORK_DATA = "nt";
    public static final int NUM_DATA_CONNECTION_TYPES = 16;
    public static final int NUM_SCREEN_BRIGHTNESS_BINS = 5;
    private static final String PROCESS_DATA = "pr";
    public static final int SCAN_WIFI_LOCK = 6;
    public static final int SCREEN_BRIGHTNESS_BRIGHT = 4;
    public static final int SCREEN_BRIGHTNESS_DARK = 0;
    private static final String SCREEN_BRIGHTNESS_DATA = "br";
    public static final int SCREEN_BRIGHTNESS_DIM = 1;
    public static final int SCREEN_BRIGHTNESS_LIGHT = 3;
    public static final int SCREEN_BRIGHTNESS_MEDIUM = 2;
    static final String[] SCREEN_BRIGHTNESS_NAMES;
    public static final int SENSOR = 3;
    private static final String SENSOR_DATA = "sr";
    private static final String SIGNAL_SCANNING_TIME_DATA = "sst";
    private static final String SIGNAL_STRENGTH_COUNT_DATA = "sgc";
    private static final String SIGNAL_STRENGTH_TIME_DATA = "sgt";
    public static final int STATS_CURRENT = 2;
    public static final int STATS_LAST = 1;
    public static final int STATS_SINCE_CHARGED = 0;
    public static final int STATS_SINCE_UNPLUGGED = 3;
    private static final String[] STAT_NAMES;
    private static final String UID_DATA = "uid";
    private static final String USER_ACTIVITY_DATA = "ua";
    public static final int VIDEO_TURNED_ON = 8;
    private static final String WAKELOCK_DATA = "wl";
    public static final int WAKE_TYPE_FULL = 1;
    public static final int WAKE_TYPE_PARTIAL = 0;
    public static final int WAKE_TYPE_WINDOW = 2;
    private static final String WIFI_LOCK_DATA = "wfl";
    public static final int WIFI_MULTICAST_ENABLED = 7;
    public static final int WIFI_RUNNING = 4;
    private final StringBuilder mFormatBuilder = new StringBuilder(32);
    private final Formatter mFormatter = new Formatter(this.mFormatBuilder);

    static
    {
        String[] arrayOfString1 = new String[4];
        arrayOfString1[0] = "t";
        arrayOfString1[1] = "l";
        arrayOfString1[2] = "c";
        arrayOfString1[3] = "u";
        STAT_NAMES = arrayOfString1;
        String[] arrayOfString2 = new String[5];
        arrayOfString2[0] = "dark";
        arrayOfString2[1] = "dim";
        arrayOfString2[2] = "medium";
        arrayOfString2[3] = "light";
        arrayOfString2[4] = "bright";
        SCREEN_BRIGHTNESS_NAMES = arrayOfString2;
        String[] arrayOfString3 = new String[16];
        arrayOfString3[0] = "none";
        arrayOfString3[1] = "gprs";
        arrayOfString3[2] = "edge";
        arrayOfString3[3] = "umts";
        arrayOfString3[4] = "cdma";
        arrayOfString3[5] = "evdo_0";
        arrayOfString3[6] = "evdo_A";
        arrayOfString3[7] = "1xrtt";
        arrayOfString3[8] = "hsdpa";
        arrayOfString3[9] = "hsupa";
        arrayOfString3[10] = "hspa";
        arrayOfString3[11] = "iden";
        arrayOfString3[12] = "evdo_b";
        arrayOfString3[13] = "lte";
        arrayOfString3[14] = "ehrpd";
        arrayOfString3[15] = "other";
        DATA_CONNECTION_NAMES = arrayOfString3;
        BitDescription[] arrayOfBitDescription = new BitDescription[19];
        arrayOfBitDescription[0] = new BitDescription(524288, "plugged");
        arrayOfBitDescription[1] = new BitDescription(1048576, "screen");
        arrayOfBitDescription[2] = new BitDescription(268435456, "gps");
        arrayOfBitDescription[3] = new BitDescription(262144, "phone_in_call");
        arrayOfBitDescription[4] = new BitDescription(134217728, "phone_scanning");
        arrayOfBitDescription[5] = new BitDescription(131072, "wifi");
        arrayOfBitDescription[6] = new BitDescription(67108864, "wifi_running");
        arrayOfBitDescription[7] = new BitDescription(33554432, "wifi_full_lock");
        arrayOfBitDescription[8] = new BitDescription(16777216, "wifi_scan_lock");
        arrayOfBitDescription[9] = new BitDescription(8388608, "wifi_multicast");
        arrayOfBitDescription[10] = new BitDescription(65536, "bluetooth");
        arrayOfBitDescription[11] = new BitDescription(4194304, "audio");
        arrayOfBitDescription[12] = new BitDescription(2097152, "video");
        arrayOfBitDescription[13] = new BitDescription(1073741824, "wake_lock");
        arrayOfBitDescription[14] = new BitDescription(536870912, "sensor");
        arrayOfBitDescription[15] = new BitDescription(15, 0, "brightness", SCREEN_BRIGHTNESS_NAMES);
        arrayOfBitDescription[16] = new BitDescription(240, 4, "signal_strength", SignalStrength.SIGNAL_STRENGTH_NAMES);
        String[] arrayOfString4 = new String[4];
        arrayOfString4[0] = "in";
        arrayOfString4[1] = "out";
        arrayOfString4[2] = "emergency";
        arrayOfString4[3] = "off";
        arrayOfBitDescription[17] = new BitDescription(3840, 8, "phone_state", arrayOfString4);
        arrayOfBitDescription[18] = new BitDescription(61440, 12, "data_conn", DATA_CONNECTION_NAMES);
    }

    private static long computeWakeLock(Timer paramTimer, long paramLong, int paramInt)
    {
        if (paramTimer != null);
        for (long l = (500L + paramTimer.getTotalTimeLocked(paramLong, paramInt)) / 1000L; ; l = 0L)
            return l;
    }

    private static final void dumpLine(PrintWriter paramPrintWriter, int paramInt, String paramString1, String paramString2, Object[] paramArrayOfObject)
    {
        paramPrintWriter.print(5);
        paramPrintWriter.print(',');
        paramPrintWriter.print(paramInt);
        paramPrintWriter.print(',');
        paramPrintWriter.print(paramString1);
        paramPrintWriter.print(',');
        paramPrintWriter.print(paramString2);
        int i = paramArrayOfObject.length;
        for (int j = 0; j < i; j++)
        {
            Object localObject = paramArrayOfObject[j];
            paramPrintWriter.print(',');
            paramPrintWriter.print(localObject);
        }
        paramPrintWriter.print('\n');
    }

    private final String formatBytesLocked(long paramLong)
    {
        this.mFormatBuilder.setLength(0);
        String str;
        if (paramLong < 1024L)
            str = paramLong + "B";
        while (true)
        {
            return str;
            if (paramLong < 1048576L)
            {
                Formatter localFormatter3 = this.mFormatter;
                Object[] arrayOfObject3 = new Object[1];
                arrayOfObject3[0] = Double.valueOf(paramLong / 1024.0D);
                localFormatter3.format("%.2fKB", arrayOfObject3);
                str = this.mFormatBuilder.toString();
            }
            else if (paramLong < 1073741824L)
            {
                Formatter localFormatter2 = this.mFormatter;
                Object[] arrayOfObject2 = new Object[1];
                arrayOfObject2[0] = Double.valueOf(paramLong / 1048576.0D);
                localFormatter2.format("%.2fMB", arrayOfObject2);
                str = this.mFormatBuilder.toString();
            }
            else
            {
                Formatter localFormatter1 = this.mFormatter;
                Object[] arrayOfObject1 = new Object[1];
                arrayOfObject1[0] = Double.valueOf(paramLong / 1073741824.0D);
                localFormatter1.format("%.2fGB", arrayOfObject1);
                str = this.mFormatBuilder.toString();
            }
        }
    }

    private final String formatRatioLocked(long paramLong1, long paramLong2)
    {
        if (paramLong2 == 0L);
        for (String str = "---%"; ; str = this.mFormatBuilder.toString())
        {
            return str;
            float f = 100.0F * ((float)paramLong1 / (float)paramLong2);
            this.mFormatBuilder.setLength(0);
            Formatter localFormatter = this.mFormatter;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Float.valueOf(f);
            localFormatter.format("%.1f%%", arrayOfObject);
        }
    }

    private static final void formatTime(StringBuilder paramStringBuilder, long paramLong)
    {
        long l = paramLong / 100L;
        formatTimeRaw(paramStringBuilder, l);
        paramStringBuilder.append(10L * (paramLong - 100L * l));
        paramStringBuilder.append("ms ");
    }

    private static final void formatTimeMs(StringBuilder paramStringBuilder, long paramLong)
    {
        long l = paramLong / 1000L;
        formatTimeRaw(paramStringBuilder, l);
        paramStringBuilder.append(paramLong - 1000L * l);
        paramStringBuilder.append("ms ");
    }

    private static final void formatTimeRaw(StringBuilder paramStringBuilder, long paramLong)
    {
        long l1 = paramLong / 86400L;
        if (l1 != 0L)
        {
            paramStringBuilder.append(l1);
            paramStringBuilder.append("d ");
        }
        long l2 = 24L * (60L * (60L * l1));
        long l3 = (paramLong - l2) / 3600L;
        if ((l3 != 0L) || (l2 != 0L))
        {
            paramStringBuilder.append(l3);
            paramStringBuilder.append("h ");
        }
        long l4 = l2 + 60L * (60L * l3);
        long l5 = (paramLong - l4) / 60L;
        if ((l5 != 0L) || (l4 != 0L))
        {
            paramStringBuilder.append(l5);
            paramStringBuilder.append("m ");
        }
        long l6 = l4 + 60L * l5;
        if ((paramLong != 0L) || (l6 != 0L))
        {
            paramStringBuilder.append(paramLong - l6);
            paramStringBuilder.append("s ");
        }
    }

    static void printBitDescriptions(PrintWriter paramPrintWriter, int paramInt1, int paramInt2, BitDescription[] paramArrayOfBitDescription)
    {
        int i = paramInt1 ^ paramInt2;
        if (i == 0)
            return;
        int j = 0;
        label14: BitDescription localBitDescription;
        String str;
        if (j < paramArrayOfBitDescription.length)
        {
            localBitDescription = paramArrayOfBitDescription[j];
            if ((i & localBitDescription.mask) != 0)
            {
                if (localBitDescription.shift >= 0)
                    break label90;
                if ((paramInt2 & localBitDescription.mask) == 0)
                    break label82;
                str = " +";
                label61: paramPrintWriter.print(str);
                paramPrintWriter.print(localBitDescription.name);
            }
        }
        while (true)
        {
            j++;
            break label14;
            break;
            label82: str = " -";
            break label61;
            label90: paramPrintWriter.print(" ");
            paramPrintWriter.print(localBitDescription.name);
            paramPrintWriter.print("=");
            int k = (paramInt2 & localBitDescription.mask) >> localBitDescription.shift;
            if ((localBitDescription.values != null) && (k >= 0) && (k < localBitDescription.values.length))
                paramPrintWriter.print(localBitDescription.values[k]);
            else
                paramPrintWriter.print(k);
        }
    }

    private static final String printWakeLock(StringBuilder paramStringBuilder, Timer paramTimer, long paramLong, String paramString1, int paramInt, String paramString2)
    {
        if (paramTimer != null)
        {
            long l = computeWakeLock(paramTimer, paramLong, paramInt);
            int i = paramTimer.getCountLocked(paramInt);
            if (l != 0L)
            {
                paramStringBuilder.append(paramString2);
                formatTimeMs(paramStringBuilder, l);
                if (paramString1 != null)
                    paramStringBuilder.append(paramString1);
                paramStringBuilder.append(' ');
                paramStringBuilder.append('(');
                paramStringBuilder.append(i);
                paramStringBuilder.append(" times)");
                paramString2 = ", ";
            }
        }
        return paramString2;
    }

    private static final String printWakeLockCheckin(StringBuilder paramStringBuilder, Timer paramTimer, long paramLong, String paramString1, int paramInt, String paramString2)
    {
        long l = 0L;
        int i = 0;
        if (paramTimer != null)
        {
            l = paramTimer.getTotalTimeLocked(paramLong, paramInt);
            i = paramTimer.getCountLocked(paramInt);
        }
        paramStringBuilder.append(paramString2);
        paramStringBuilder.append((500L + l) / 1000L);
        paramStringBuilder.append(',');
        if (paramString1 != null);
        for (String str = paramString1 + ","; ; str = "")
        {
            paramStringBuilder.append(str);
            paramStringBuilder.append(i);
            return ",";
        }
    }

    public abstract long computeBatteryRealtime(long paramLong, int paramInt);

    public abstract long computeBatteryUptime(long paramLong, int paramInt);

    public abstract long computeRealtime(long paramLong, int paramInt);

    public abstract long computeUptime(long paramLong, int paramInt);

    public final void dumpCheckinLocked(PrintWriter paramPrintWriter, int paramInt1, int paramInt2)
    {
        long l1 = 1000L * SystemClock.uptimeMillis();
        long l2 = 1000L * SystemClock.elapsedRealtime();
        long l3 = getBatteryUptime(l1);
        long l4 = getBatteryRealtime(l2);
        long l5 = computeBatteryUptime(l1, paramInt1);
        long l6 = computeBatteryRealtime(l2, paramInt1);
        long l7 = computeRealtime(l2, paramInt1);
        long l8 = computeUptime(l1, paramInt1);
        long l9 = getScreenOnTime(l4, paramInt1);
        long l10 = getPhoneOnTime(l4, paramInt1);
        long l11 = getWifiOnTime(l4, paramInt1);
        long l12 = getGlobalWifiRunningTime(l4, paramInt1);
        long l13 = getBluetoothOnTime(l4, paramInt1);
        StringBuilder localStringBuilder = new StringBuilder(128);
        SparseArray localSparseArray = getUidStats();
        int i = localSparseArray.size();
        String str1 = STAT_NAMES[paramInt1];
        Object[] arrayOfObject1 = new Object[5];
        Object localObject;
        long l14;
        long l15;
        long l16;
        long l17;
        if (paramInt1 == 0)
        {
            localObject = Integer.valueOf(getStartCount());
            arrayOfObject1[0] = localObject;
            arrayOfObject1[1] = Long.valueOf(l6 / 1000L);
            arrayOfObject1[2] = Long.valueOf(l5 / 1000L);
            arrayOfObject1[3] = Long.valueOf(l7 / 1000L);
            arrayOfObject1[4] = Long.valueOf(l8 / 1000L);
            dumpLine(paramPrintWriter, 0, str1, "bt", arrayOfObject1);
            l14 = 0L;
            l15 = 0L;
            l16 = 0L;
            l17 = 0L;
        }
        for (int j = 0; ; j++)
        {
            if (j >= i)
                break label421;
            Uid localUid2 = (Uid)localSparseArray.valueAt(j);
            l14 += localUid2.getTcpBytesReceived(paramInt1);
            l15 += localUid2.getTcpBytesSent(paramInt1);
            Map localMap6 = localUid2.getWakelockStats();
            if (localMap6.size() > 0)
            {
                Iterator localIterator7 = localMap6.entrySet().iterator();
                while (true)
                {
                    Object[] arrayOfObject2;
                    Object[] arrayOfObject3;
                    int k;
                    Object[] arrayOfObject4;
                    int m;
                    Object[] arrayOfObject5;
                    int n;
                    Object[] arrayOfObject6;
                    int i1;
                    int i2;
                    Object[] arrayOfObject17;
                    Object[] arrayOfObject16;
                    int i3;
                    int i4;
                    Map localMap4;
                    Iterator localIterator1;
                    if (localIterator7.hasNext())
                    {
                        BatteryStats.Uid.Wakelock localWakelock2 = (BatteryStats.Uid.Wakelock)((Map.Entry)localIterator7.next()).getValue();
                        Timer localTimer2 = localWakelock2.getWakeTime(1);
                        if (localTimer2 != null)
                            l16 += localTimer2.getTotalTimeLocked(l4, paramInt1);
                        Timer localTimer3 = localWakelock2.getWakeTime(0);
                        if (localTimer3 != null)
                        {
                            l17 += localTimer3.getTotalTimeLocked(l4, paramInt1);
                            continue;
                            localObject = "N/A";
                            break;
                        }
                    }
                }
            }
        }
        label421: arrayOfObject2 = new Object[10];
        arrayOfObject2[0] = Long.valueOf(l9 / 1000L);
        arrayOfObject2[1] = Long.valueOf(l10 / 1000L);
        arrayOfObject2[2] = Long.valueOf(l11 / 1000L);
        arrayOfObject2[3] = Long.valueOf(l12 / 1000L);
        arrayOfObject2[4] = Long.valueOf(l13 / 1000L);
        arrayOfObject2[5] = Long.valueOf(l14);
        arrayOfObject2[6] = Long.valueOf(l15);
        arrayOfObject2[7] = Long.valueOf(l16);
        arrayOfObject2[8] = Long.valueOf(l17);
        arrayOfObject2[9] = Integer.valueOf(getInputEventCount(paramInt1));
        dumpLine(paramPrintWriter, 0, str1, "m", arrayOfObject2);
        arrayOfObject3 = new Object[5];
        for (k = 0; k < 5; k++)
            arrayOfObject3[k] = Long.valueOf(getScreenBrightnessTime(k, l4, paramInt1) / 1000L);
        dumpLine(paramPrintWriter, 0, str1, "br", arrayOfObject3);
        arrayOfObject4 = new Object[5];
        for (m = 0; m < 5; m++)
            arrayOfObject4[m] = Long.valueOf(getPhoneSignalStrengthTime(m, l4, paramInt1) / 1000L);
        dumpLine(paramPrintWriter, 0, str1, "sgt", arrayOfObject4);
        arrayOfObject5 = new Object[1];
        arrayOfObject5[0] = Long.valueOf(getPhoneSignalScanningTime(l4, paramInt1) / 1000L);
        dumpLine(paramPrintWriter, 0, str1, "sst", arrayOfObject5);
        for (n = 0; n < 5; n++)
            arrayOfObject4[n] = Integer.valueOf(getPhoneSignalStrengthCount(n, paramInt1));
        dumpLine(paramPrintWriter, 0, str1, "sgc", arrayOfObject4);
        arrayOfObject6 = new Object[16];
        for (i1 = 0; i1 < 16; i1++)
            arrayOfObject6[i1] = Long.valueOf(getPhoneDataConnectionTime(i1, l4, paramInt1) / 1000L);
        dumpLine(paramPrintWriter, 0, str1, "dct", arrayOfObject6);
        for (i2 = 0; i2 < 16; i2++)
            arrayOfObject6[i2] = Integer.valueOf(getPhoneDataConnectionCount(i2, paramInt1));
        dumpLine(paramPrintWriter, 0, str1, "dcc", arrayOfObject6);
        if (paramInt1 == 3)
        {
            arrayOfObject17 = new Object[2];
            arrayOfObject17[0] = Integer.valueOf(getDischargeStartLevel());
            arrayOfObject17[1] = Integer.valueOf(getDischargeCurrentLevel());
            dumpLine(paramPrintWriter, 0, str1, "lv", arrayOfObject17);
        }
        if (paramInt1 == 3)
        {
            arrayOfObject16 = new Object[4];
            arrayOfObject16[0] = Integer.valueOf(getDischargeStartLevel() - getDischargeCurrentLevel());
            arrayOfObject16[1] = Integer.valueOf(getDischargeStartLevel() - getDischargeCurrentLevel());
            arrayOfObject16[2] = Integer.valueOf(getDischargeAmountScreenOn());
            arrayOfObject16[3] = Integer.valueOf(getDischargeAmountScreenOff());
            dumpLine(paramPrintWriter, 0, str1, "dc", arrayOfObject16);
        }
        while (paramInt2 < 0)
        {
            Map localMap5 = getKernelWakelockStats();
            if (localMap5.size() <= 0)
                break;
            Iterator localIterator6 = localMap5.entrySet().iterator();
            while (localIterator6.hasNext())
            {
                Map.Entry localEntry6 = (Map.Entry)localIterator6.next();
                localStringBuilder.setLength(0);
                printWakeLockCheckin(localStringBuilder, (Timer)localEntry6.getValue(), l4, null, paramInt1, "");
                Object[] arrayOfObject15 = new Object[2];
                arrayOfObject15[0] = localEntry6.getKey();
                arrayOfObject15[1] = localStringBuilder.toString();
                dumpLine(paramPrintWriter, 0, str1, "kwl", arrayOfObject15);
            }
            Object[] arrayOfObject7 = new Object[4];
            arrayOfObject7[0] = Integer.valueOf(getLowDischargeAmountSinceCharge());
            arrayOfObject7[1] = Integer.valueOf(getHighDischargeAmountSinceCharge());
            arrayOfObject7[2] = Integer.valueOf(getDischargeAmountScreenOn());
            arrayOfObject7[3] = Integer.valueOf(getDischargeAmountScreenOff());
            dumpLine(paramPrintWriter, 0, str1, "dc", arrayOfObject7);
        }
        i3 = 0;
        if (i3 < i)
        {
            i4 = localSparseArray.keyAt(i3);
            if ((paramInt2 >= 0) && (i4 != paramInt2));
            do
            {
                i3++;
                break;
                Uid localUid1 = (Uid)localSparseArray.valueAt(i3);
                long l18 = localUid1.getTcpBytesReceived(paramInt1);
                long l19 = localUid1.getTcpBytesSent(paramInt1);
                long l20 = localUid1.getFullWifiLockTime(l4, paramInt1);
                long l21 = localUid1.getScanWifiLockTime(l4, paramInt1);
                long l22 = localUid1.getWifiRunningTime(l4, paramInt1);
                if ((l18 > 0L) || (l19 > 0L))
                {
                    Object[] arrayOfObject8 = new Object[2];
                    arrayOfObject8[0] = Long.valueOf(l18);
                    arrayOfObject8[1] = Long.valueOf(l19);
                    dumpLine(paramPrintWriter, i4, str1, "nt", arrayOfObject8);
                }
                if ((l20 != 0L) || (l21 != 0L) || (l22 != 0L))
                {
                    Object[] arrayOfObject9 = new Object[3];
                    arrayOfObject9[0] = Long.valueOf(l20);
                    arrayOfObject9[1] = Long.valueOf(l21);
                    arrayOfObject9[2] = Long.valueOf(l22);
                    dumpLine(paramPrintWriter, i4, str1, "wfl", arrayOfObject9);
                }
                if (localUid1.hasUserActivity())
                {
                    Object[] arrayOfObject14 = new Object[7];
                    int i11 = 0;
                    for (int i12 = 0; i12 < 7; i12++)
                    {
                        int i13 = localUid1.getUserActivityCount(i12, paramInt1);
                        arrayOfObject14[i12] = Integer.valueOf(i13);
                        if (i13 != 0)
                            i11 = 1;
                    }
                    if (i11 != 0)
                        dumpLine(paramPrintWriter, 0, str1, "ua", arrayOfObject14);
                }
                Map localMap1 = localUid1.getWakelockStats();
                if (localMap1.size() > 0)
                {
                    Iterator localIterator5 = localMap1.entrySet().iterator();
                    while (localIterator5.hasNext())
                    {
                        Map.Entry localEntry5 = (Map.Entry)localIterator5.next();
                        BatteryStats.Uid.Wakelock localWakelock1 = (BatteryStats.Uid.Wakelock)localEntry5.getValue();
                        localStringBuilder.setLength(0);
                        String str2 = printWakeLockCheckin(localStringBuilder, localWakelock1.getWakeTime(1), l4, "f", paramInt1, "");
                        String str3 = printWakeLockCheckin(localStringBuilder, localWakelock1.getWakeTime(0), l4, "p", paramInt1, str2);
                        printWakeLockCheckin(localStringBuilder, localWakelock1.getWakeTime(2), l4, "w", paramInt1, str3);
                        if (localStringBuilder.length() > 0)
                        {
                            Object[] arrayOfObject13 = new Object[2];
                            arrayOfObject13[0] = localEntry5.getKey();
                            arrayOfObject13[1] = localStringBuilder.toString();
                            dumpLine(paramPrintWriter, i4, str1, "wl", arrayOfObject13);
                        }
                    }
                }
                Map localMap2 = localUid1.getSensorStats();
                if (localMap2.size() > 0)
                {
                    Iterator localIterator4 = localMap2.entrySet().iterator();
                    while (localIterator4.hasNext())
                    {
                        Map.Entry localEntry4 = (Map.Entry)localIterator4.next();
                        BatteryStats.Uid.Sensor localSensor = (BatteryStats.Uid.Sensor)localEntry4.getValue();
                        int i9 = ((Integer)localEntry4.getKey()).intValue();
                        Timer localTimer1 = localSensor.getSensorTime();
                        if (localTimer1 != null)
                        {
                            long l26 = (500L + localTimer1.getTotalTimeLocked(l4, paramInt1)) / 1000L;
                            int i10 = localTimer1.getCountLocked(paramInt1);
                            if (l26 != 0L)
                            {
                                Object[] arrayOfObject12 = new Object[3];
                                arrayOfObject12[0] = Integer.valueOf(i9);
                                arrayOfObject12[1] = Long.valueOf(l26);
                                arrayOfObject12[2] = Integer.valueOf(i10);
                                dumpLine(paramPrintWriter, i4, str1, "sr", arrayOfObject12);
                            }
                        }
                    }
                }
                Map localMap3 = localUid1.getProcessStats();
                if (localMap3.size() > 0)
                {
                    Iterator localIterator3 = localMap3.entrySet().iterator();
                    while (localIterator3.hasNext())
                    {
                        Map.Entry localEntry3 = (Map.Entry)localIterator3.next();
                        BatteryStats.Uid.Proc localProc = (BatteryStats.Uid.Proc)localEntry3.getValue();
                        long l24 = localProc.getUserTime(paramInt1);
                        long l25 = localProc.getSystemTime(paramInt1);
                        int i8 = localProc.getStarts(paramInt1);
                        if ((l24 != 0L) || (l25 != 0L) || (i8 != 0))
                        {
                            Object[] arrayOfObject11 = new Object[4];
                            arrayOfObject11[0] = localEntry3.getKey();
                            arrayOfObject11[1] = Long.valueOf(10L * l24);
                            arrayOfObject11[2] = Long.valueOf(10L * l25);
                            arrayOfObject11[3] = Integer.valueOf(i8);
                            dumpLine(paramPrintWriter, i4, str1, "pr", arrayOfObject11);
                        }
                    }
                }
                localMap4 = localUid1.getPackageStats();
            }
            while (localMap4.size() <= 0);
            localIterator1 = localMap4.entrySet().iterator();
            while (localIterator1.hasNext())
            {
                Map.Entry localEntry1 = (Map.Entry)localIterator1.next();
                BatteryStats.Uid.Pkg localPkg = (BatteryStats.Uid.Pkg)localEntry1.getValue();
                int i5 = localPkg.getWakeups(paramInt1);
                Iterator localIterator2 = localPkg.getServiceStats().entrySet().iterator();
                while (localIterator2.hasNext())
                {
                    Map.Entry localEntry2 = (Map.Entry)localIterator2.next();
                    BatteryStats.Uid.Pkg.Serv localServ = (BatteryStats.Uid.Pkg.Serv)localEntry2.getValue();
                    long l23 = localServ.getStartTime(l3, paramInt1);
                    int i6 = localServ.getStarts(paramInt1);
                    int i7 = localServ.getLaunches(paramInt1);
                    if ((l23 != 0L) || (i6 != 0) || (i7 != 0))
                    {
                        Object[] arrayOfObject10 = new Object[6];
                        arrayOfObject10[0] = Integer.valueOf(i5);
                        arrayOfObject10[1] = localEntry1.getKey();
                        arrayOfObject10[2] = localEntry2.getKey();
                        arrayOfObject10[3] = Long.valueOf(l23 / 1000L);
                        arrayOfObject10[4] = Integer.valueOf(i6);
                        arrayOfObject10[5] = Integer.valueOf(i7);
                        dumpLine(paramPrintWriter, i4, str1, "apk", arrayOfObject10);
                    }
                }
            }
        }
    }

    public void dumpCheckinLocked(PrintWriter paramPrintWriter, String[] paramArrayOfString, List<ApplicationInfo> paramList)
    {
        prepareForDumpLocked();
        int i = 0;
        int j = paramArrayOfString.length;
        for (int k = 0; k < j; k++)
            if ("-u".equals(paramArrayOfString[k]))
                i = 1;
        if (paramList != null)
        {
            SparseArray localSparseArray1 = new SparseArray();
            for (int m = 0; m < paramList.size(); m++)
            {
                ApplicationInfo localApplicationInfo = (ApplicationInfo)paramList.get(m);
                ArrayList localArrayList2 = (ArrayList)localSparseArray1.get(localApplicationInfo.uid);
                if (localArrayList2 == null)
                {
                    localArrayList2 = new ArrayList();
                    localSparseArray1.put(localApplicationInfo.uid, localArrayList2);
                }
                localArrayList2.add(localApplicationInfo.packageName);
            }
            SparseArray localSparseArray2 = getUidStats();
            int n = localSparseArray2.size();
            String[] arrayOfString = new String[2];
            for (int i1 = 0; i1 < n; i1++)
            {
                int i2 = localSparseArray2.keyAt(i1);
                ArrayList localArrayList1 = (ArrayList)localSparseArray1.get(i2);
                if (localArrayList1 != null)
                    for (int i3 = 0; i3 < localArrayList1.size(); i3++)
                    {
                        arrayOfString[0] = Integer.toString(i2);
                        arrayOfString[1] = ((String)localArrayList1.get(i3));
                        dumpLine(paramPrintWriter, 0, "i", "uid", (Object[])arrayOfString);
                    }
            }
        }
        if (i != 0)
            dumpCheckinLocked(paramPrintWriter, 3, -1);
        while (true)
        {
            return;
            dumpCheckinLocked(paramPrintWriter, 0, -1);
            dumpCheckinLocked(paramPrintWriter, 3, -1);
        }
    }

    public void dumpLocked(PrintWriter paramPrintWriter)
    {
        prepareForDumpLocked();
        long l1 = getHistoryBaseTime() + SystemClock.elapsedRealtime();
        HistoryItem localHistoryItem = new HistoryItem();
        if (startIteratingHistoryLocked())
        {
            paramPrintWriter.println("Battery History:");
            HistoryPrinter localHistoryPrinter2 = new HistoryPrinter();
            while (getNextHistoryLocked(localHistoryItem))
                localHistoryPrinter2.printNextItem(paramPrintWriter, localHistoryItem, l1);
            finishIteratingHistoryLocked();
            paramPrintWriter.println("");
        }
        if (startIteratingOldHistoryLocked())
        {
            paramPrintWriter.println("Old battery History:");
            HistoryPrinter localHistoryPrinter1 = new HistoryPrinter();
            while (getNextOldHistoryLocked(localHistoryItem))
                localHistoryPrinter1.printNextItem(paramPrintWriter, localHistoryItem, l1);
            finishIteratingOldHistoryLocked();
            paramPrintWriter.println("");
        }
        SparseArray localSparseArray1 = getUidStats();
        int i = localSparseArray1.size();
        int j = 0;
        long l2 = SystemClock.elapsedRealtime();
        for (int k = 0; k < i; k++)
        {
            SparseArray localSparseArray2 = ((Uid)localSparseArray1.valueAt(k)).getPidStats();
            if (localSparseArray2 != null)
            {
                int m = 0;
                if (m < localSparseArray2.size())
                {
                    BatteryStats.Uid.Pid localPid = (BatteryStats.Uid.Pid)localSparseArray2.valueAt(m);
                    if (j == 0)
                    {
                        paramPrintWriter.println("Per-PID Stats:");
                        j = 1;
                    }
                    long l3 = localPid.mWakeSum;
                    if (localPid.mWakeStart != 0L);
                    for (long l4 = l2 - localPid.mWakeStart; ; l4 = 0L)
                    {
                        long l5 = l3 + l4;
                        paramPrintWriter.print("    PID ");
                        paramPrintWriter.print(localSparseArray2.keyAt(m));
                        paramPrintWriter.print(" wake time: ");
                        TimeUtils.formatDuration(l5, paramPrintWriter);
                        paramPrintWriter.println("");
                        m++;
                        break;
                    }
                }
            }
        }
        if (j != 0)
            paramPrintWriter.println("");
        paramPrintWriter.println("Statistics since last charge:");
        paramPrintWriter.println("    System starts: " + getStartCount() + ", currently on battery: " + getIsOnBattery());
        dumpLocked(paramPrintWriter, "", 0, -1);
        paramPrintWriter.println("");
        paramPrintWriter.println("Statistics since last unplugged:");
        dumpLocked(paramPrintWriter, "", 3, -1);
    }

    public final void dumpLocked(PrintWriter paramPrintWriter, String paramString, int paramInt1, int paramInt2)
    {
        long l1 = 1000L * SystemClock.uptimeMillis();
        long l2 = 1000L * SystemClock.elapsedRealtime();
        long l3 = getBatteryUptime(l1);
        long l4 = getBatteryRealtime(l2);
        long l5 = computeBatteryUptime(l1, paramInt1);
        long l6 = computeBatteryRealtime(l2, paramInt1);
        long l7 = computeRealtime(l2, paramInt1);
        long l8 = computeUptime(l1, paramInt1);
        StringBuilder localStringBuilder = new StringBuilder(128);
        SparseArray localSparseArray = getUidStats();
        int i = localSparseArray.size();
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Time on battery: ");
        formatTimeMs(localStringBuilder, l6 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l6, l7));
        localStringBuilder.append(") realtime, ");
        formatTimeMs(localStringBuilder, l5 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l5, l7));
        localStringBuilder.append(") uptime");
        paramPrintWriter.println(localStringBuilder.toString());
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Total run time: ");
        formatTimeMs(localStringBuilder, l7 / 1000L);
        localStringBuilder.append("realtime, ");
        formatTimeMs(localStringBuilder, l8 / 1000L);
        localStringBuilder.append("uptime, ");
        paramPrintWriter.println(localStringBuilder.toString());
        long l9 = getScreenOnTime(l4, paramInt1);
        long l10 = getPhoneOnTime(l4, paramInt1);
        long l11 = getGlobalWifiRunningTime(l4, paramInt1);
        long l12 = getWifiOnTime(l4, paramInt1);
        long l13 = getBluetoothOnTime(l4, paramInt1);
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Screen on: ");
        formatTimeMs(localStringBuilder, l9 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l9, l6));
        localStringBuilder.append("), Input events: ");
        localStringBuilder.append(getInputEventCount(paramInt1));
        localStringBuilder.append(", Active phone call: ");
        formatTimeMs(localStringBuilder, l10 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l10, l6));
        localStringBuilder.append(")");
        paramPrintWriter.println(localStringBuilder.toString());
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Screen brightnesses: ");
        int j = 0;
        int k = 0;
        long l32 = getScreenBrightnessTime(k, l4, paramInt1);
        k++;
        if (j != 0)
            localStringBuilder.append(", ");
        j = 1;
        localStringBuilder.append(SCREEN_BRIGHTNESS_NAMES[k]);
        localStringBuilder.append(" ");
        formatTimeMs(l32, 0.0F / 1000L);
        localStringBuilder.append("(");
        append(l32.formatRatioLocked(0.0F, l9));
        localStringBuilder.append(")");
        if (j == 0)
            localStringBuilder.append("No activity");
        paramPrintWriter.println(localStringBuilder.toString());
        long l14 = 0L;
        long l15 = 0L;
        long l16 = 0L;
        long l17 = 0L;
        if (paramInt2 < 0)
        {
            Map localMap7 = getKernelWakelockStats();
            if (0.size() > 0)
            {
                Iterator localIterator7 = 0.entrySet().iterator();
                Map.Entry localEntry6;
                do
                {
                    localEntry6 = (Map.Entry)1.next();
                    localStringBuilder.setLength(0);
                    localStringBuilder.append(paramString);
                    localStringBuilder.append("    Kernel Wake lock ");
                    localEntry6.append((String)2.getKey());
                }
                while (printWakeLock(localEntry6, (Timer)2.getValue(), l4, null, paramInt1, ": ").equals(": "));
                localStringBuilder.append(" realtime");
                paramPrintWriter.println(localStringBuilder.toString());
            }
        }
        int m = 0;
        Uid localUid2 = (Uid)localSparseArray.valueAt(m);
        l14 += localUid2.getTcpBytesReceived(paramInt1);
        l15 += localUid2.getTcpBytesSent(paramInt1);
        Map localMap6 = localUid2.getWakelockStats();
        if (localMap6.size() > 0)
        {
            Iterator localIterator6 = localMap6.entrySet().iterator();
            Timer localTimer3;
            do
            {
                BatteryStats.Uid.Wakelock localWakelock2 = (BatteryStats.Uid.Wakelock)((Map.Entry)localIterator6.next()).getValue();
                Timer localTimer2 = localWakelock2.getWakeTime(1);
                if (null != null)
                    l16 = localTimer2 + null.getTotalTimeLocked(l4, paramInt1);
                localTimer3 = localWakelock2.getWakeTime(0);
            }
            while (-1 == null);
            l17 = localTimer3 + -1.getTotalTimeLocked(l4, paramInt1);
        }
        m++;
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("    Total received: ");
        paramPrintWriter.print(formatBytesLocked(l14));
        paramPrintWriter.print(", Total sent: ");
        paramPrintWriter.println(formatBytesLocked(l15));
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Total full wakelock time: ");
        formatTimeMs(localStringBuilder, (500L + l16) / 1000L);
        localStringBuilder.append(", Total partial waklock time: ");
        formatTimeMs(localStringBuilder, (500L + l17) / 1000L);
        paramPrintWriter.println(localStringBuilder.toString());
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Signal levels: ");
        int n = 0;
        int i1 = 0;
        long l31 = getPhoneSignalStrengthTime(i1, l4, paramInt1);
        i1++;
        if (n != 0)
            localStringBuilder.append(", ");
        n = 1;
        localStringBuilder.append(SignalStrength.SIGNAL_STRENGTH_NAMES[i1]);
        localStringBuilder.append(" ");
        formatTimeMs(localStringBuilder, l31 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l31, l6));
        localStringBuilder.append(") ");
        localStringBuilder.append(getPhoneSignalStrengthCount(i1, paramInt1));
        localStringBuilder.append("x");
        if (n == 0)
            localStringBuilder.append("No activity");
        paramPrintWriter.println(localStringBuilder.toString());
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Signal scanning time: ");
        formatTimeMs(localStringBuilder, getPhoneSignalScanningTime(l4, paramInt1) / 1000L);
        paramPrintWriter.println(localStringBuilder.toString());
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Radio types: ");
        int i2 = 0;
        int i3 = 0;
        long l30 = getPhoneDataConnectionTime(i3, l4, paramInt1);
        i3++;
        if (i2 != 0)
            localStringBuilder.append(", ");
        i2 = 1;
        localStringBuilder.append(DATA_CONNECTION_NAMES[i3]);
        localStringBuilder.append(" ");
        formatTimeMs(localStringBuilder, l30 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l30, l6));
        localStringBuilder.append(") ");
        localStringBuilder.append(getPhoneDataConnectionCount(i3, paramInt1));
        localStringBuilder.append("x");
        if (i2 == 0)
            localStringBuilder.append("No activity");
        paramPrintWriter.println(localStringBuilder.toString());
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Radio data uptime when unplugged: ");
        localStringBuilder.append(getRadioDataUptime() / 1000L);
        localStringBuilder.append(" ms");
        paramPrintWriter.println(localStringBuilder.toString());
        localStringBuilder.setLength(0);
        localStringBuilder.append(paramString);
        localStringBuilder.append("    Wifi on: ");
        formatTimeMs(localStringBuilder, l12 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l12, l6));
        localStringBuilder.append("), Wifi running: ");
        formatTimeMs(localStringBuilder, l11 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l11, l6));
        localStringBuilder.append("), Bluetooth on: ");
        formatTimeMs(localStringBuilder, l13 / 1000L);
        localStringBuilder.append("(");
        localStringBuilder.append(formatRatioLocked(l13, l6));
        localStringBuilder.append(")");
        paramPrintWriter.println(localStringBuilder.toString());
        paramPrintWriter.println(" ");
        paramPrintWriter.print(paramString);
        paramPrintWriter.println("    Device is currently unplugged");
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("        Discharge cycle start level: ");
        paramPrintWriter.println(getDischargeStartLevel());
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("        Discharge cycle current level: ");
        paramPrintWriter.println(getDischargeCurrentLevel());
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("        Amount discharged while screen on: ");
        paramPrintWriter.println(getDischargeAmountScreenOn());
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("        Amount discharged while screen off: ");
        paramPrintWriter.println(getDischargeAmountScreenOff());
        paramPrintWriter.println(" ");
        int i4 = 0;
        if (i4 < i)
        {
            int i5 = localSparseArray.keyAt(i4);
            while (true)
            {
                i4++;
                paramPrintWriter.print(paramString);
                paramPrintWriter.println("    Device is currently plugged into power");
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        Last discharge cycle start level: ");
                paramPrintWriter.println(getDischargeStartLevel());
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        Last discharge cycle end level: ");
                paramPrintWriter.println(getDischargeCurrentLevel());
                paramPrintWriter.print(paramString);
                paramPrintWriter.println("    Device battery use since last full charge");
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        Amount discharged (lower bound): ");
                paramPrintWriter.println(getLowDischargeAmountSinceCharge());
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        Amount discharged (upper bound): ");
                paramPrintWriter.println(getHighDischargeAmountSinceCharge());
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        Amount discharged while screen on: ");
                paramPrintWriter.println(getDischargeAmountScreenOnSinceCharge());
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        Amount discharged while screen off: ");
                paramPrintWriter.println(getDischargeAmountScreenOffSinceCharge());
                paramPrintWriter.println(" ");
                Uid localUid1 = (Uid)localSparseArray.valueAt(i4);
                paramPrintWriter.println(paramString + "    #" + i5 + ":");
                int i6 = 0;
                long l18 = localUid1.getTcpBytesReceived(paramInt1);
                long l19 = localUid1.getTcpBytesSent(paramInt1);
                long l20 = localUid1.getFullWifiLockTime(l4, paramInt1);
                long l21 = localUid1.getScanWifiLockTime(l4, paramInt1);
                long l22 = localUid1.getWifiRunningTime(l4, paramInt1);
                if ((l18 != 0L) || (l19 != 0L))
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("        Network: ");
                    paramPrintWriter.print(formatBytesLocked(l18));
                    paramPrintWriter.print(" received, ");
                    paramPrintWriter.print(formatBytesLocked(l19));
                    paramPrintWriter.println(" sent");
                }
                if (localUid1.hasUserActivity())
                {
                    int i18 = 0;
                    int i19 = 0;
                    int i20 = localUid1.getUserActivityCount(i19, paramInt1);
                    if (i20 != 0)
                    {
                        localStringBuilder.setLength(0);
                        localStringBuilder.append("        User activity: ");
                        i18 = 1;
                        localStringBuilder.append(i20);
                        localStringBuilder.append(" ");
                        localStringBuilder.append(Uid.USER_ACTIVITY_TYPES[i19]);
                    }
                    i19++;
                    localStringBuilder.append(", ");
                    if (i18 != 0)
                        paramPrintWriter.println(localStringBuilder.toString());
                }
                if ((l20 != 0L) || (l21 != 0L) || (l22 != 0L))
                {
                    localStringBuilder.setLength(0);
                    localStringBuilder.append(paramString);
                    localStringBuilder.append("        Wifi Running: ");
                    formatTimeMs(localStringBuilder, l22 / 1000L);
                    localStringBuilder.append("(");
                    localStringBuilder.append(formatRatioLocked(l22, l6));
                    localStringBuilder.append(")\n");
                    localStringBuilder.append(paramString);
                    localStringBuilder.append("        Full Wifi Lock: ");
                    formatTimeMs(localStringBuilder, l20 / 1000L);
                    localStringBuilder.append("(");
                    localStringBuilder.append(formatRatioLocked(l20, l6));
                    localStringBuilder.append(")\n");
                    localStringBuilder.append(paramString);
                    localStringBuilder.append("        Scan Wifi Lock: ");
                    formatTimeMs(localStringBuilder, l21 / 1000L);
                    localStringBuilder.append("(");
                    localStringBuilder.append(formatRatioLocked(l21, l6));
                    localStringBuilder.append(")");
                    paramPrintWriter.println(localStringBuilder.toString());
                }
                Map localMap1 = localUid1.getWakelockStats();
                if (localMap1.size() > 0)
                {
                    long l27 = 0L;
                    long l28 = 0L;
                    long l29 = 0L;
                    int i16 = 0;
                    Iterator localIterator5 = localMap1.entrySet().iterator();
                    Map.Entry localEntry5 = (Map.Entry)localIterator5.next();
                    BatteryStats.Uid.Wakelock localWakelock1 = (BatteryStats.Uid.Wakelock)localEntry5.getValue();
                    localStringBuilder.setLength(0);
                    localStringBuilder.append(paramString);
                    localStringBuilder.append("        Wake lock ");
                    localStringBuilder.append((String)localEntry5.getKey());
                    String str1 = printWakeLock(localStringBuilder, localWakelock1.getWakeTime(1), l4, "full", paramInt1, ": ");
                    String str2 = printWakeLock(localStringBuilder, localWakelock1.getWakeTime(0), l4, "partial", paramInt1, str1);
                    if (!printWakeLock(localStringBuilder, localWakelock1.getWakeTime(2), l4, "window", paramInt1, str2).equals(": "))
                    {
                        localStringBuilder.append(" realtime");
                        paramPrintWriter.println(localStringBuilder.toString());
                        i6 = 1;
                    }
                    l27 += computeWakeLock(localWakelock1.getWakeTime(1), l4, paramInt1);
                    l28 += computeWakeLock(localWakelock1.getWakeTime(0), l4, paramInt1);
                    l29 += computeWakeLock(localWakelock1.getWakeTime(2), l4, paramInt1);
                    if ((i16 > 1) && ((l27 != 0L) || (l28 != 0L) || (l29 != 0L)))
                    {
                        localStringBuilder.setLength(0);
                        localStringBuilder.append(paramString);
                        localStringBuilder.append("        TOTAL wake: ");
                        int i17 = 0;
                        if (l27 != 0L)
                        {
                            i17 = 1;
                            formatTimeMs(localStringBuilder, l27);
                            localStringBuilder.append("full");
                        }
                        if (l28 != 0L)
                        {
                            if (i17 != 0)
                                localStringBuilder.append(", ");
                            i17 = 1;
                            formatTimeMs(localStringBuilder, l28);
                            localStringBuilder.append("partial");
                        }
                        if (l29 != 0L)
                        {
                            if (i17 != 0)
                                localStringBuilder.append(", ");
                            formatTimeMs(localStringBuilder, l29);
                            localStringBuilder.append("window");
                        }
                        localStringBuilder.append(" realtime");
                        paramPrintWriter.println(localStringBuilder.toString());
                    }
                }
                Map localMap2 = localUid1.getSensorStats();
                if (localMap2.size() > 0)
                {
                    Iterator localIterator4 = localMap2.entrySet().iterator();
                    Map.Entry localEntry4 = (Map.Entry)localIterator4.next();
                    BatteryStats.Uid.Sensor localSensor = (BatteryStats.Uid.Sensor)localEntry4.getValue();
                    ((Integer)localEntry4.getKey()).intValue();
                    localStringBuilder.setLength(0);
                    localStringBuilder.append(paramString);
                    localStringBuilder.append("        Sensor ");
                    int i14 = localSensor.getHandle();
                    localStringBuilder.append("GPS");
                    localStringBuilder.append(": ");
                    Timer localTimer1 = localSensor.getSensorTime();
                    long l26 = (500L + localTimer1.getTotalTimeLocked(l4, paramInt1)) / 1000L;
                    int i15 = localTimer1.getCountLocked(paramInt1);
                    formatTimeMs(localStringBuilder, l26);
                    localStringBuilder.append("realtime (");
                    localStringBuilder.append(i15);
                    localStringBuilder.append(" times)");
                    paramPrintWriter.println(localStringBuilder.toString());
                    i6 = 1;
                    localStringBuilder.append(i14);
                    localStringBuilder.append("(not used)");
                    localStringBuilder.append("(not used)");
                }
                Map localMap3 = localUid1.getProcessStats();
                if (localMap3.size() > 0)
                {
                    Iterator localIterator3 = localMap3.entrySet().iterator();
                    while (localIterator3.hasNext())
                    {
                        Map.Entry localEntry3 = (Map.Entry)localIterator3.next();
                        BatteryStats.Uid.Proc localProc = (BatteryStats.Uid.Proc)localEntry3.getValue();
                        long l24 = localProc.getUserTime(paramInt1);
                        long l25 = localProc.getSystemTime(paramInt1);
                        int i11 = localProc.getStarts(paramInt1);
                        int i12 = localProc.countExcessivePowers();
                        label3351: int i13;
                        BatteryStats.Uid.Proc.ExcessivePower localExcessivePower;
                        if ((l24 != 0L) || (l25 != 0L) || (i11 != 0) || (i12 != 0))
                        {
                            localStringBuilder.setLength(0);
                            localStringBuilder.append(paramString);
                            localStringBuilder.append("        Proc ");
                            localStringBuilder.append((String)localEntry3.getKey());
                            localStringBuilder.append(":\n");
                            localStringBuilder.append(paramString);
                            localStringBuilder.append("            CPU: ");
                            formatTime(localStringBuilder, l24);
                            localStringBuilder.append("usr + ");
                            formatTime(localStringBuilder, l25);
                            localStringBuilder.append("krn");
                            if (i11 != 0)
                            {
                                localStringBuilder.append("\n");
                                localStringBuilder.append(paramString);
                                localStringBuilder.append("            ");
                                localStringBuilder.append(i11);
                                localStringBuilder.append(" proc starts");
                            }
                            paramPrintWriter.println(localStringBuilder.toString());
                            i13 = 0;
                            if (i13 >= i12)
                                break label3684;
                            localExcessivePower = localProc.getExcessivePower(i13);
                            if (localExcessivePower != null)
                            {
                                paramPrintWriter.print(paramString);
                                paramPrintWriter.print("            * Killed for ");
                                if (localExcessivePower.type != 1)
                                    break label3655;
                                paramPrintWriter.print("wake lock");
                            }
                        }
                        while (true)
                        {
                            paramPrintWriter.print(" use: ");
                            TimeUtils.formatDuration(localExcessivePower.usedTime, paramPrintWriter);
                            paramPrintWriter.print(" over ");
                            TimeUtils.formatDuration(localExcessivePower.overTime, paramPrintWriter);
                            paramPrintWriter.print(" (");
                            paramPrintWriter.print(100L * localExcessivePower.usedTime / localExcessivePower.overTime);
                            paramPrintWriter.println("%)");
                            i13++;
                            i12 = 0;
                            break label3351;
                            break;
                            label3655: if (localExcessivePower.type == 2)
                                paramPrintWriter.print("cpu");
                            else
                                paramPrintWriter.print("unknown");
                        }
                        label3684: i6 = 1;
                    }
                }
                Map localMap4 = localUid1.getPackageStats();
                if (localMap4.size() > 0)
                {
                    Iterator localIterator1 = localMap4.entrySet().iterator();
                    while (localIterator1.hasNext())
                    {
                        Map.Entry localEntry1 = (Map.Entry)localIterator1.next();
                        paramPrintWriter.print(paramString);
                        paramPrintWriter.print("        Apk ");
                        paramPrintWriter.print((String)localEntry1.getKey());
                        paramPrintWriter.println(":");
                        int i7 = 0;
                        BatteryStats.Uid.Pkg localPkg = (BatteryStats.Uid.Pkg)localEntry1.getValue();
                        int i8 = localPkg.getWakeups(paramInt1);
                        if (i8 != 0)
                        {
                            paramPrintWriter.print(paramString);
                            paramPrintWriter.print("            ");
                            paramPrintWriter.print(i8);
                            paramPrintWriter.println(" wakeup alarms");
                            i7 = 1;
                        }
                        Map localMap5 = localPkg.getServiceStats();
                        if (localMap5.size() > 0)
                        {
                            Iterator localIterator2 = localMap5.entrySet().iterator();
                            while (localIterator2.hasNext())
                            {
                                Map.Entry localEntry2 = (Map.Entry)localIterator2.next();
                                BatteryStats.Uid.Pkg.Serv localServ = (BatteryStats.Uid.Pkg.Serv)localEntry2.getValue();
                                long l23 = localServ.getStartTime(l3, paramInt1);
                                int i9 = localServ.getStarts(paramInt1);
                                int i10 = localServ.getLaunches(paramInt1);
                                if ((l23 != 0L) || (i9 != 0) || (i10 != 0))
                                {
                                    localStringBuilder.setLength(0);
                                    localStringBuilder.append(paramString);
                                    localStringBuilder.append("            Service ");
                                    localStringBuilder.append((String)localEntry2.getKey());
                                    localStringBuilder.append(":\n");
                                    localStringBuilder.append(paramString);
                                    localStringBuilder.append("                Created for: ");
                                    formatTimeMs(localStringBuilder, l23 / 1000L);
                                    localStringBuilder.append(" uptime\n");
                                    localStringBuilder.append(paramString);
                                    localStringBuilder.append("                Starts: ");
                                    localStringBuilder.append(i9);
                                    localStringBuilder.append(", launches: ");
                                    localStringBuilder.append(i10);
                                    paramPrintWriter.println(localStringBuilder.toString());
                                    i7 = 1;
                                }
                            }
                        }
                        if (i7 == 0)
                        {
                            paramPrintWriter.print(paramString);
                            paramPrintWriter.println("            (nothing executed)");
                        }
                        i6 = 1;
                    }
                }
                if (i6 == 0)
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.println("        (nothing executed)");
                }
            }
        }
    }

    public abstract void finishIteratingHistoryLocked();

    public abstract void finishIteratingOldHistoryLocked();

    public abstract long getBatteryRealtime(long paramLong);

    public abstract long getBatteryUptime(long paramLong);

    public abstract long getBluetoothOnTime(long paramLong, int paramInt);

    public abstract int getCpuSpeedSteps();

    public abstract int getDischargeAmountScreenOff();

    public abstract int getDischargeAmountScreenOffSinceCharge();

    public abstract int getDischargeAmountScreenOn();

    public abstract int getDischargeAmountScreenOnSinceCharge();

    public abstract int getDischargeCurrentLevel();

    public abstract int getDischargeStartLevel();

    public abstract long getGlobalWifiRunningTime(long paramLong, int paramInt);

    public abstract int getHighDischargeAmountSinceCharge();

    public abstract long getHistoryBaseTime();

    public abstract int getInputEventCount(int paramInt);

    public abstract boolean getIsOnBattery();

    public abstract Map<String, ? extends Timer> getKernelWakelockStats();

    public abstract int getLowDischargeAmountSinceCharge();

    public abstract boolean getNextHistoryLocked(HistoryItem paramHistoryItem);

    public abstract boolean getNextOldHistoryLocked(HistoryItem paramHistoryItem);

    public abstract int getPhoneDataConnectionCount(int paramInt1, int paramInt2);

    public abstract long getPhoneDataConnectionTime(int paramInt1, long paramLong, int paramInt2);

    public abstract long getPhoneOnTime(long paramLong, int paramInt);

    public abstract long getPhoneSignalScanningTime(long paramLong, int paramInt);

    public abstract int getPhoneSignalStrengthCount(int paramInt1, int paramInt2);

    public abstract long getPhoneSignalStrengthTime(int paramInt1, long paramLong, int paramInt2);

    public abstract long getRadioDataUptime();

    public long getRadioDataUptimeMs()
    {
        return getRadioDataUptime() / 1000L;
    }

    public abstract long getScreenBrightnessTime(int paramInt1, long paramLong, int paramInt2);

    public abstract long getScreenOnTime(long paramLong, int paramInt);

    public abstract int getStartCount();

    public abstract SparseArray<? extends Uid> getUidStats();

    public abstract long getWifiOnTime(long paramLong, int paramInt);

    public void prepareForDumpLocked()
    {
    }

    public abstract boolean startIteratingHistoryLocked();

    public abstract boolean startIteratingOldHistoryLocked();

    public static class HistoryPrinter
    {
        int oldHealth = -1;
        int oldPlug = -1;
        int oldState = 0;
        int oldStatus = -1;
        int oldTemp = -1;
        int oldVolt = -1;

        public void printNextItem(PrintWriter paramPrintWriter, BatteryStats.HistoryItem paramHistoryItem, long paramLong)
        {
            paramPrintWriter.print("    ");
            TimeUtils.formatDuration(paramHistoryItem.time - paramLong, paramPrintWriter, 19);
            paramPrintWriter.print(" ");
            if (paramHistoryItem.cmd == 2)
                paramPrintWriter.println(" START");
            while (true)
            {
                this.oldState = paramHistoryItem.states;
                return;
                if (paramHistoryItem.cmd != 3)
                    break;
                paramPrintWriter.println(" *OVERFLOW*");
            }
            if (paramHistoryItem.batteryLevel < 10)
            {
                paramPrintWriter.print("00");
                label79: paramPrintWriter.print(paramHistoryItem.batteryLevel);
                paramPrintWriter.print(" ");
                if (paramHistoryItem.states >= 16)
                    break label438;
                paramPrintWriter.print("0000000");
                label108: paramPrintWriter.print(Integer.toHexString(paramHistoryItem.states));
                if (this.oldStatus != paramHistoryItem.batteryStatus)
                {
                    this.oldStatus = paramHistoryItem.batteryStatus;
                    paramPrintWriter.print(" status=");
                }
                switch (this.oldStatus)
                {
                default:
                    paramPrintWriter.print(this.oldStatus);
                    label192: if (this.oldHealth != paramHistoryItem.batteryHealth)
                    {
                        this.oldHealth = paramHistoryItem.batteryHealth;
                        paramPrintWriter.print(" health=");
                    }
                    switch (this.oldHealth)
                    {
                    default:
                        paramPrintWriter.print(this.oldHealth);
                        label268: if (this.oldPlug != paramHistoryItem.batteryPlugType)
                        {
                            this.oldPlug = paramHistoryItem.batteryPlugType;
                            paramPrintWriter.print(" plug=");
                            switch (this.oldPlug)
                            {
                            default:
                                paramPrintWriter.print(this.oldPlug);
                            case 0:
                            case 1:
                            case 2:
                            }
                        }
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    }
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                }
            }
            while (true)
            {
                if (this.oldTemp != paramHistoryItem.batteryTemperature)
                {
                    this.oldTemp = paramHistoryItem.batteryTemperature;
                    paramPrintWriter.print(" temp=");
                    paramPrintWriter.print(this.oldTemp);
                }
                if (this.oldVolt != paramHistoryItem.batteryVoltage)
                {
                    this.oldVolt = paramHistoryItem.batteryVoltage;
                    paramPrintWriter.print(" volt=");
                    paramPrintWriter.print(this.oldVolt);
                }
                BatteryStats.printBitDescriptions(paramPrintWriter, this.oldState, paramHistoryItem.states, BatteryStats.HISTORY_STATE_DESCRIPTIONS);
                paramPrintWriter.println();
                break;
                if (paramHistoryItem.batteryLevel >= 100)
                    break label79;
                paramPrintWriter.print("0");
                break label79;
                label438: if (paramHistoryItem.states < 256)
                {
                    paramPrintWriter.print("000000");
                    break label108;
                }
                if (paramHistoryItem.states < 4096)
                {
                    paramPrintWriter.print("00000");
                    break label108;
                }
                if (paramHistoryItem.states < 65536)
                {
                    paramPrintWriter.print("0000");
                    break label108;
                }
                if (paramHistoryItem.states < 1048576)
                {
                    paramPrintWriter.print("000");
                    break label108;
                }
                if (paramHistoryItem.states < 16777216)
                {
                    paramPrintWriter.print("00");
                    break label108;
                }
                if (paramHistoryItem.states >= 268435456)
                    break label108;
                paramPrintWriter.print("0");
                break label108;
                paramPrintWriter.print("unknown");
                break label192;
                paramPrintWriter.print("charging");
                break label192;
                paramPrintWriter.print("discharging");
                break label192;
                paramPrintWriter.print("not-charging");
                break label192;
                paramPrintWriter.print("full");
                break label192;
                paramPrintWriter.print("unknown");
                break label268;
                paramPrintWriter.print("good");
                break label268;
                paramPrintWriter.print("overheat");
                break label268;
                paramPrintWriter.print("dead");
                break label268;
                paramPrintWriter.print("over-voltage");
                break label268;
                paramPrintWriter.print("failure");
                break label268;
                paramPrintWriter.print("none");
                continue;
                paramPrintWriter.print("ac");
                continue;
                paramPrintWriter.print("usb");
            }
        }
    }

    public static final class BitDescription
    {
        public final int mask;
        public final String name;
        public final int shift;
        public final String[] values;

        public BitDescription(int paramInt1, int paramInt2, String paramString, String[] paramArrayOfString)
        {
            this.mask = paramInt1;
            this.shift = paramInt2;
            this.name = paramString;
            this.values = paramArrayOfString;
        }

        public BitDescription(int paramInt, String paramString)
        {
            this.mask = paramInt;
            this.shift = -1;
            this.name = paramString;
            this.values = null;
        }
    }

    public static final class HistoryItem
        implements Parcelable
    {
        public static final byte CMD_NULL = 0;
        public static final byte CMD_OVERFLOW = 3;
        public static final byte CMD_START = 2;
        public static final byte CMD_UPDATE = 1;
        static final boolean DEBUG = false;
        static final int DELTA_BATTERY_LEVEL_FLAG = 1048576;
        static final int DELTA_CMD_MASK = 3;
        static final int DELTA_CMD_SHIFT = 18;
        static final int DELTA_STATE_FLAG = 2097152;
        static final int DELTA_STATE_MASK = -4194304;
        static final int DELTA_TIME_ABS = 262141;
        static final int DELTA_TIME_INT = 262142;
        static final int DELTA_TIME_LONG = 262143;
        static final int DELTA_TIME_MASK = 262143;
        public static final int MOST_INTERESTING_STATES = 270270464;
        public static final int STATE_AUDIO_ON_FLAG = 4194304;
        public static final int STATE_BATTERY_PLUGGED_FLAG = 524288;
        public static final int STATE_BLUETOOTH_ON_FLAG = 65536;
        public static final int STATE_BRIGHTNESS_MASK = 15;
        public static final int STATE_BRIGHTNESS_SHIFT = 0;
        public static final int STATE_DATA_CONNECTION_MASK = 61440;
        public static final int STATE_DATA_CONNECTION_SHIFT = 12;
        public static final int STATE_GPS_ON_FLAG = 268435456;
        public static final int STATE_PHONE_IN_CALL_FLAG = 262144;
        public static final int STATE_PHONE_SCANNING_FLAG = 134217728;
        public static final int STATE_PHONE_STATE_MASK = 3840;
        public static final int STATE_PHONE_STATE_SHIFT = 8;
        public static final int STATE_SCREEN_ON_FLAG = 1048576;
        public static final int STATE_SENSOR_ON_FLAG = 536870912;
        public static final int STATE_SIGNAL_STRENGTH_MASK = 240;
        public static final int STATE_SIGNAL_STRENGTH_SHIFT = 4;
        public static final int STATE_VIDEO_ON_FLAG = 2097152;
        public static final int STATE_WAKE_LOCK_FLAG = 1073741824;
        public static final int STATE_WIFI_FULL_LOCK_FLAG = 33554432;
        public static final int STATE_WIFI_MULTICAST_ON_FLAG = 8388608;
        public static final int STATE_WIFI_ON_FLAG = 131072;
        public static final int STATE_WIFI_RUNNING_FLAG = 67108864;
        public static final int STATE_WIFI_SCAN_LOCK_FLAG = 16777216;
        static final String TAG = "HistoryItem";
        public byte batteryHealth;
        public byte batteryLevel;
        public byte batteryPlugType;
        public byte batteryStatus;
        public char batteryTemperature;
        public char batteryVoltage;
        public byte cmd = 0;
        public HistoryItem next;
        public int states;
        public long time;

        public HistoryItem()
        {
        }

        public HistoryItem(long paramLong, Parcel paramParcel)
        {
            this.time = paramLong;
            readFromParcel(paramParcel);
        }

        private int buildBatteryLevelInt()
        {
            return 0xFF000000 & this.batteryLevel << 24 | 0xFFC000 & this.batteryTemperature << '\016' | 0x3FFF & this.batteryVoltage;
        }

        private int buildStateInt()
        {
            return 0xF0000000 & this.batteryStatus << 28 | 0xF000000 & this.batteryHealth << 24 | 0xC00000 & this.batteryPlugType << 22 | 0x3FFFFF & this.states;
        }

        private void readFromParcel(Parcel paramParcel)
        {
            int i = paramParcel.readInt();
            this.cmd = ((byte)(i & 0xFF));
            this.batteryLevel = ((byte)(0xFF & i >> 8));
            this.batteryStatus = ((byte)(0xF & i >> 16));
            this.batteryHealth = ((byte)(0xF & i >> 20));
            this.batteryPlugType = ((byte)(0xF & i >> 24));
            int j = paramParcel.readInt();
            this.batteryTemperature = ((char)(j & 0xFFFF));
            this.batteryVoltage = ((char)(0xFFFF & j >> 16));
            this.states = paramParcel.readInt();
        }

        public void clear()
        {
            this.time = 0L;
            this.cmd = 0;
            this.batteryLevel = 0;
            this.batteryStatus = 0;
            this.batteryHealth = 0;
            this.batteryPlugType = 0;
            this.batteryTemperature = '\000';
            this.batteryVoltage = '\000';
            this.states = 0;
        }

        public int describeContents()
        {
            return 0;
        }

        public void readDelta(Parcel paramParcel)
        {
            int i = paramParcel.readInt();
            int j = i & 0x3FFFF;
            this.cmd = ((byte)(0x3 & i >> 18));
            if (j < 262141)
            {
                this.time += j;
                if ((0x100000 & i) != 0)
                {
                    int m = paramParcel.readInt();
                    this.batteryLevel = ((byte)(0xFF & m >> 24));
                    this.batteryTemperature = ((char)(0x3FF & m >> 14));
                    this.batteryVoltage = ((char)(m & 0x3FFF));
                }
                if ((0x200000 & i) == 0)
                    break label221;
                int k = paramParcel.readInt();
                this.states = (i & 0xFFC00000 | k & 0x3FFFFF);
                this.batteryStatus = ((byte)(0xF & k >> 28));
                this.batteryHealth = ((byte)(0xF & k >> 24));
                this.batteryPlugType = ((byte)(0x3 & k >> 22));
            }
            while (true)
            {
                return;
                if (j == 262141)
                {
                    this.time = paramParcel.readLong();
                    readFromParcel(paramParcel);
                }
                else
                {
                    if (j == 262142)
                    {
                        int n = paramParcel.readInt();
                        this.time += n;
                        break;
                    }
                    this.time = (paramParcel.readLong() + this.time);
                    break;
                    label221: this.states = (i & 0xFFC00000 | 0x3FFFFF & this.states);
                }
            }
        }

        public boolean same(HistoryItem paramHistoryItem)
        {
            if ((this.batteryLevel == paramHistoryItem.batteryLevel) && (this.batteryStatus == paramHistoryItem.batteryStatus) && (this.batteryHealth == paramHistoryItem.batteryHealth) && (this.batteryPlugType == paramHistoryItem.batteryPlugType) && (this.batteryTemperature == paramHistoryItem.batteryTemperature) && (this.batteryVoltage == paramHistoryItem.batteryVoltage) && (this.states == paramHistoryItem.states));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void setTo(long paramLong, byte paramByte, HistoryItem paramHistoryItem)
        {
            this.time = paramLong;
            this.cmd = paramByte;
            this.batteryLevel = paramHistoryItem.batteryLevel;
            this.batteryStatus = paramHistoryItem.batteryStatus;
            this.batteryHealth = paramHistoryItem.batteryHealth;
            this.batteryPlugType = paramHistoryItem.batteryPlugType;
            this.batteryTemperature = paramHistoryItem.batteryTemperature;
            this.batteryVoltage = paramHistoryItem.batteryVoltage;
            this.states = paramHistoryItem.states;
        }

        public void setTo(HistoryItem paramHistoryItem)
        {
            this.time = paramHistoryItem.time;
            this.cmd = paramHistoryItem.cmd;
            this.batteryLevel = paramHistoryItem.batteryLevel;
            this.batteryStatus = paramHistoryItem.batteryStatus;
            this.batteryHealth = paramHistoryItem.batteryHealth;
            this.batteryPlugType = paramHistoryItem.batteryPlugType;
            this.batteryTemperature = paramHistoryItem.batteryTemperature;
            this.batteryVoltage = paramHistoryItem.batteryVoltage;
            this.states = paramHistoryItem.states;
        }

        public void writeDelta(Parcel paramParcel, HistoryItem paramHistoryItem)
        {
            if ((paramHistoryItem == null) || (paramHistoryItem.cmd != 1))
            {
                paramParcel.writeInt(262141);
                writeToParcel(paramParcel, 0);
                return;
            }
            long l = this.time - paramHistoryItem.time;
            int i = paramHistoryItem.buildBatteryLevelInt();
            int j = paramHistoryItem.buildStateInt();
            int k;
            label65: int n;
            int i1;
            label101: int i2;
            int i3;
            if ((l < 0L) || (l > 2147483647L))
            {
                k = 262143;
                int m = k | this.cmd << 18 | 0xFFC00000 & this.states;
                n = buildBatteryLevelInt();
                if (n == i)
                    break label214;
                i1 = 1;
                if (i1 != 0)
                    m |= 1048576;
                i2 = buildStateInt();
                if (i2 == j)
                    break label220;
                i3 = 1;
                label129: if (i3 != 0)
                    m |= 2097152;
                paramParcel.writeInt(m);
                if (k >= 262142)
                {
                    if (k != 262142)
                        break label226;
                    paramParcel.writeInt((int)l);
                }
            }
            while (true)
            {
                if (i1 != 0)
                    paramParcel.writeInt(n);
                if (i3 == 0)
                    break;
                paramParcel.writeInt(i2);
                break;
                if (l >= 262141L)
                {
                    k = 262142;
                    break label65;
                }
                k = (int)l;
                break label65;
                label214: i1 = 0;
                break label101;
                label220: i3 = 0;
                break label129;
                label226: paramParcel.writeLong(l);
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeLong(this.time);
            paramParcel.writeInt(0xFF & this.cmd | 0xFF00 & this.batteryLevel << 8 | 0xF0000 & this.batteryStatus << 16 | 0xF00000 & this.batteryHealth << 20 | 0xF000000 & this.batteryPlugType << 24);
            paramParcel.writeInt(0xFFFF & this.batteryTemperature | 0xFFFF0000 & this.batteryVoltage << '\020');
            paramParcel.writeInt(this.states);
        }
    }

    public static abstract class Uid
    {
        public static final int NUM_USER_ACTIVITY_TYPES = 7;
        static final String[] USER_ACTIVITY_TYPES = arrayOfString;

        static
        {
            String[] arrayOfString = new String[7];
            arrayOfString[0] = "other";
            arrayOfString[1] = "cheek";
            arrayOfString[2] = "touch";
            arrayOfString[3] = "long_touch";
            arrayOfString[4] = "touch_up";
            arrayOfString[5] = "button";
            arrayOfString[6] = "unknown";
        }

        public abstract long getAudioTurnedOnTime(long paramLong, int paramInt);

        public abstract long getFullWifiLockTime(long paramLong, int paramInt);

        public abstract Map<String, ? extends Pkg> getPackageStats();

        public abstract SparseArray<? extends Pid> getPidStats();

        public abstract Map<String, ? extends Proc> getProcessStats();

        public abstract long getScanWifiLockTime(long paramLong, int paramInt);

        public abstract Map<Integer, ? extends Sensor> getSensorStats();

        public abstract long getTcpBytesReceived(int paramInt);

        public abstract long getTcpBytesSent(int paramInt);

        public abstract int getUid();

        public abstract int getUserActivityCount(int paramInt1, int paramInt2);

        public abstract long getVideoTurnedOnTime(long paramLong, int paramInt);

        public abstract Map<String, ? extends Wakelock> getWakelockStats();

        public abstract long getWifiMulticastTime(long paramLong, int paramInt);

        public abstract long getWifiRunningTime(long paramLong, int paramInt);

        public abstract boolean hasUserActivity();

        public abstract void noteAudioTurnedOffLocked();

        public abstract void noteAudioTurnedOnLocked();

        public abstract void noteFullWifiLockAcquiredLocked();

        public abstract void noteFullWifiLockReleasedLocked();

        public abstract void noteScanWifiLockAcquiredLocked();

        public abstract void noteScanWifiLockReleasedLocked();

        public abstract void noteUserActivityLocked(int paramInt);

        public abstract void noteVideoTurnedOffLocked();

        public abstract void noteVideoTurnedOnLocked();

        public abstract void noteWifiMulticastDisabledLocked();

        public abstract void noteWifiMulticastEnabledLocked();

        public abstract void noteWifiRunningLocked();

        public abstract void noteWifiStoppedLocked();

        public static abstract class Pkg
        {
            public abstract Map<String, ? extends Serv> getServiceStats();

            public abstract int getWakeups(int paramInt);

            public abstract class Serv
            {
                public Serv()
                {
                }

                public abstract int getLaunches(int paramInt);

                public abstract long getStartTime(long paramLong, int paramInt);

                public abstract int getStarts(int paramInt);
            }
        }

        public static abstract class Proc
        {
            public abstract int countExcessivePowers();

            public abstract ExcessivePower getExcessivePower(int paramInt);

            public abstract long getForegroundTime(int paramInt);

            public abstract int getStarts(int paramInt);

            public abstract long getSystemTime(int paramInt);

            public abstract long getTimeAtCpuSpeedStep(int paramInt1, int paramInt2);

            public abstract long getUserTime(int paramInt);

            public static class ExcessivePower
            {
                public static final int TYPE_CPU = 2;
                public static final int TYPE_WAKE = 1;
                public long overTime;
                public int type;
                public long usedTime;
            }
        }

        public class Pid
        {
            public long mWakeStart;
            public long mWakeSum;

            public Pid()
            {
            }
        }

        public static abstract class Sensor
        {
            public static final int GPS = -10000;

            public abstract int getHandle();

            public abstract BatteryStats.Timer getSensorTime();
        }

        public static abstract class Wakelock
        {
            public abstract BatteryStats.Timer getWakeTime(int paramInt);
        }
    }

    public static abstract class Timer
    {
        public abstract int getCountLocked(int paramInt);

        public abstract long getTotalTimeLocked(long paramLong, int paramInt);

        public abstract void logState(Printer paramPrinter, String paramString);
    }

    public static abstract class Counter
    {
        public abstract int getCountLocked(int paramInt);

        public abstract void logState(Printer paramPrinter, String paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.BatteryStats
 * JD-Core Version:        0.6.2
 */