package android.os;

public abstract class CountDownTimer
{
    private static final int MSG = 1;
    private final long mCountdownInterval;
    private Handler mHandler = new Handler()
    {
        // ERROR //
        public void handleMessage(Message paramAnonymousMessage)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 12	android/os/CountDownTimer$1:this$0	Landroid/os/CountDownTimer;
            //     4: astore_2
            //     5: aload_2
            //     6: monitorenter
            //     7: aload_0
            //     8: getfield 12	android/os/CountDownTimer$1:this$0	Landroid/os/CountDownTimer;
            //     11: invokestatic 21	android/os/CountDownTimer:access$000	(Landroid/os/CountDownTimer;)J
            //     14: invokestatic 27	android/os/SystemClock:elapsedRealtime	()J
            //     17: lsub
            //     18: lstore 4
            //     20: lload 4
            //     22: lconst_0
            //     23: lcmp
            //     24: ifgt +13 -> 37
            //     27: aload_0
            //     28: getfield 12	android/os/CountDownTimer$1:this$0	Landroid/os/CountDownTimer;
            //     31: invokevirtual 30	android/os/CountDownTimer:onFinish	()V
            //     34: aload_2
            //     35: monitorexit
            //     36: return
            //     37: lload 4
            //     39: aload_0
            //     40: getfield 12	android/os/CountDownTimer$1:this$0	Landroid/os/CountDownTimer;
            //     43: invokestatic 33	android/os/CountDownTimer:access$100	(Landroid/os/CountDownTimer;)J
            //     46: lcmp
            //     47: ifge +23 -> 70
            //     50: aload_0
            //     51: aload_0
            //     52: iconst_1
            //     53: invokevirtual 37	android/os/CountDownTimer$1:obtainMessage	(I)Landroid/os/Message;
            //     56: lload 4
            //     58: invokevirtual 41	android/os/CountDownTimer$1:sendMessageDelayed	(Landroid/os/Message;J)Z
            //     61: pop
            //     62: goto -28 -> 34
            //     65: astore_3
            //     66: aload_2
            //     67: monitorexit
            //     68: aload_3
            //     69: athrow
            //     70: invokestatic 27	android/os/SystemClock:elapsedRealtime	()J
            //     73: lstore 6
            //     75: aload_0
            //     76: getfield 12	android/os/CountDownTimer$1:this$0	Landroid/os/CountDownTimer;
            //     79: lload 4
            //     81: invokevirtual 45	android/os/CountDownTimer:onTick	(J)V
            //     84: lload 6
            //     86: aload_0
            //     87: getfield 12	android/os/CountDownTimer$1:this$0	Landroid/os/CountDownTimer;
            //     90: invokestatic 33	android/os/CountDownTimer:access$100	(Landroid/os/CountDownTimer;)J
            //     93: ladd
            //     94: invokestatic 27	android/os/SystemClock:elapsedRealtime	()J
            //     97: lsub
            //     98: lstore 8
            //     100: lload 8
            //     102: lconst_0
            //     103: lcmp
            //     104: ifge +18 -> 122
            //     107: lload 8
            //     109: aload_0
            //     110: getfield 12	android/os/CountDownTimer$1:this$0	Landroid/os/CountDownTimer;
            //     113: invokestatic 33	android/os/CountDownTimer:access$100	(Landroid/os/CountDownTimer;)J
            //     116: ladd
            //     117: lstore 8
            //     119: goto -19 -> 100
            //     122: aload_0
            //     123: aload_0
            //     124: iconst_1
            //     125: invokevirtual 37	android/os/CountDownTimer$1:obtainMessage	(I)Landroid/os/Message;
            //     128: lload 8
            //     130: invokevirtual 41	android/os/CountDownTimer$1:sendMessageDelayed	(Landroid/os/Message;J)Z
            //     133: pop
            //     134: goto -100 -> 34
            //
            // Exception table:
            //     from	to	target	type
            //     7	68	65	finally
            //     70	134	65	finally
        }
    };
    private final long mMillisInFuture;
    private long mStopTimeInFuture;

    public CountDownTimer(long paramLong1, long paramLong2)
    {
        this.mMillisInFuture = paramLong1;
        this.mCountdownInterval = paramLong2;
    }

    public final void cancel()
    {
        this.mHandler.removeMessages(1);
    }

    public abstract void onFinish();

    public abstract void onTick(long paramLong);

    /** @deprecated */
    public final CountDownTimer start()
    {
        try
        {
            if (this.mMillisInFuture <= 0L)
                onFinish();
            for (CountDownTimer localCountDownTimer = this; ; localCountDownTimer = this)
            {
                return localCountDownTimer;
                this.mStopTimeInFuture = (SystemClock.elapsedRealtime() + this.mMillisInFuture);
                this.mHandler.sendMessage(this.mHandler.obtainMessage(1));
            }
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.CountDownTimer
 * JD-Core Version:        0.6.2
 */