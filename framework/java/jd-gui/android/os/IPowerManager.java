package android.os;

public abstract interface IPowerManager extends IInterface
{
    public abstract void acquireWakeLock(int paramInt, IBinder paramIBinder, String paramString, WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void clearUserActivityTimeout(long paramLong1, long paramLong2)
        throws RemoteException;

    public abstract void crash(String paramString)
        throws RemoteException;

    public abstract int getSupportedWakeLockFlags()
        throws RemoteException;

    public abstract void goToSleep(long paramLong)
        throws RemoteException;

    public abstract void goToSleepWithReason(long paramLong, int paramInt)
        throws RemoteException;

    public abstract boolean isScreenOn()
        throws RemoteException;

    public abstract void preventScreenOn(boolean paramBoolean)
        throws RemoteException;

    public abstract void reboot(String paramString)
        throws RemoteException;

    public abstract void releaseWakeLock(IBinder paramIBinder, int paramInt)
        throws RemoteException;

    public abstract void setAttentionLight(boolean paramBoolean, int paramInt)
        throws RemoteException;

    public abstract void setAutoBrightnessAdjustment(float paramFloat)
        throws RemoteException;

    public abstract void setBacklightBrightness(int paramInt)
        throws RemoteException;

    public abstract void setMaximumScreenOffTimeount(int paramInt)
        throws RemoteException;

    public abstract void setPokeLock(int paramInt, IBinder paramIBinder, String paramString)
        throws RemoteException;

    public abstract void setStayOnSetting(int paramInt)
        throws RemoteException;

    public abstract void updateWakeLockWorkSource(IBinder paramIBinder, WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void userActivity(long paramLong, boolean paramBoolean)
        throws RemoteException;

    public abstract void userActivityWithForce(long paramLong, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IPowerManager
    {
        private static final String DESCRIPTOR = "android.os.IPowerManager";
        static final int TRANSACTION_acquireWakeLock = 1;
        static final int TRANSACTION_clearUserActivityTimeout = 8;
        static final int TRANSACTION_crash = 16;
        static final int TRANSACTION_getSupportedWakeLockFlags = 10;
        static final int TRANSACTION_goToSleep = 3;
        static final int TRANSACTION_goToSleepWithReason = 4;
        static final int TRANSACTION_isScreenOn = 14;
        static final int TRANSACTION_preventScreenOn = 13;
        static final int TRANSACTION_reboot = 15;
        static final int TRANSACTION_releaseWakeLock = 5;
        static final int TRANSACTION_setAttentionLight = 18;
        static final int TRANSACTION_setAutoBrightnessAdjustment = 19;
        static final int TRANSACTION_setBacklightBrightness = 17;
        static final int TRANSACTION_setMaximumScreenOffTimeount = 12;
        static final int TRANSACTION_setPokeLock = 9;
        static final int TRANSACTION_setStayOnSetting = 11;
        static final int TRANSACTION_updateWakeLockWorkSource = 2;
        static final int TRANSACTION_userActivity = 6;
        static final int TRANSACTION_userActivityWithForce = 7;

        public Stub()
        {
            attachInterface(this, "android.os.IPowerManager");
        }

        public static IPowerManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.os.IPowerManager");
                if ((localIInterface != null) && ((localIInterface instanceof IPowerManager)))
                    localObject = (IPowerManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.os.IPowerManager");
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                int m = paramParcel1.readInt();
                IBinder localIBinder2 = paramParcel1.readStrongBinder();
                String str = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource2 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource2 = null)
                {
                    acquireWakeLock(m, localIBinder2, str, localWorkSource2);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.os.IPowerManager");
                IBinder localIBinder1 = paramParcel1.readStrongBinder();
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource1 = null)
                {
                    updateWakeLockWorkSource(localIBinder1, localWorkSource1);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.os.IPowerManager");
                goToSleep(paramParcel1.readLong());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                goToSleepWithReason(paramParcel1.readLong(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                releaseWakeLock(paramParcel1.readStrongBinder(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                long l2 = paramParcel1.readLong();
                if (paramParcel1.readInt() != 0);
                for (boolean bool6 = j; ; bool6 = false)
                {
                    userActivity(l2, bool6);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.os.IPowerManager");
                long l1 = paramParcel1.readLong();
                boolean bool4;
                if (paramParcel1.readInt() != 0)
                {
                    bool4 = j;
                    label461: if (paramParcel1.readInt() == 0)
                        break label495;
                }
                label495: for (boolean bool5 = j; ; bool5 = false)
                {
                    userActivityWithForce(l1, bool4, bool5);
                    paramParcel2.writeNoException();
                    break;
                    bool4 = false;
                    break label461;
                }
                paramParcel1.enforceInterface("android.os.IPowerManager");
                clearUserActivityTimeout(paramParcel1.readLong(), paramParcel1.readLong());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                setPokeLock(paramParcel1.readInt(), paramParcel1.readStrongBinder(), paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                int k = getSupportedWakeLockFlags();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(k);
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                setStayOnSetting(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                setMaximumScreenOffTimeount(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                if (paramParcel1.readInt() != 0);
                for (boolean bool3 = j; ; bool3 = false)
                {
                    preventScreenOn(bool3);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.os.IPowerManager");
                boolean bool2 = isScreenOn();
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                reboot(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                crash(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                setBacklightBrightness(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IPowerManager");
                if (paramParcel1.readInt() != 0);
                for (boolean bool1 = j; ; bool1 = false)
                {
                    setAttentionLight(bool1, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.os.IPowerManager");
                setAutoBrightnessAdjustment(paramParcel1.readFloat());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IPowerManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void acquireWakeLock(int paramInt, IBinder paramIBinder, String paramString, WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void clearUserActivityTimeout(long paramLong1, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeLong(paramLong1);
                    localParcel1.writeLong(paramLong2);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void crash(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.os.IPowerManager";
            }

            public int getSupportedWakeLockFlags()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void goToSleep(long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeLong(paramLong);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void goToSleepWithReason(long paramLong, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeLong(paramLong);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isScreenOn()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void preventScreenOn(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void reboot(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void releaseWakeLock(IBinder paramIBinder, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAttentionLight(boolean paramBoolean, int paramInt)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAutoBrightnessAdjustment(float paramFloat)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeFloat(paramFloat);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBacklightBrightness(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setMaximumScreenOffTimeount(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPokeLock(int paramInt, IBinder paramIBinder, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setStayOnSetting(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateWakeLockWorkSource(IBinder paramIBinder, WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void userActivity(long paramLong, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeLong(paramLong);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void userActivityWithForce(long paramLong, boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IPowerManager");
                    localParcel1.writeLong(paramLong);
                    if (paramBoolean1);
                    for (int j = i; ; j = 0)
                    {
                        localParcel1.writeInt(j);
                        if (!paramBoolean2)
                            break;
                        localParcel1.writeInt(i);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.IPowerManager
 * JD-Core Version:        0.6.2
 */