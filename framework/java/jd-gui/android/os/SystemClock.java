package android.os;

public final class SystemClock
{
    public static native long currentThreadTimeMicro();

    public static native long currentThreadTimeMillis();

    public static native long currentTimeMicro();

    public static native long elapsedRealtime();

    public static native boolean setCurrentTimeMillis(long paramLong);

    public static void sleep(long paramLong)
    {
        long l1 = uptimeMillis();
        long l2 = paramLong;
        int i = 0;
        try
        {
            do
            {
                Thread.sleep(l2);
                l2 = l1 + paramLong - uptimeMillis();
            }
            while (l2 > 0L);
            if (i != 0)
                Thread.currentThread().interrupt();
            return;
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
                i = 1;
        }
    }

    public static native long uptimeMillis();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.SystemClock
 * JD-Core Version:        0.6.2
 */