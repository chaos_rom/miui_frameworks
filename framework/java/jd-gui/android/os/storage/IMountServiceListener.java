package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IMountServiceListener extends IInterface
{
    public abstract void onStorageStateChanged(String paramString1, String paramString2, String paramString3)
        throws RemoteException;

    public abstract void onUsbMassStorageConnectionChanged(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IMountServiceListener
    {
        private static final String DESCRIPTOR = "IMountServiceListener";
        static final int TRANSACTION_onStorageStateChanged = 2;
        static final int TRANSACTION_onUsbMassStorageConnectionChanged = 1;

        public Stub()
        {
            attachInterface(this, "IMountServiceListener");
        }

        public static IMountServiceListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("IMountServiceListener");
                if ((localIInterface != null) && ((localIInterface instanceof IMountServiceListener)))
                    localObject = (IMountServiceListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("IMountServiceListener");
                continue;
                paramParcel1.enforceInterface("IMountServiceListener");
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    onUsbMassStorageConnectionChanged(bool2);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("IMountServiceListener");
                onStorageStateChanged(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IMountServiceListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "IMountServiceListener";
            }

            public void onStorageStateChanged(String paramString1, String paramString2, String paramString3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountServiceListener");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onUsbMassStorageConnectionChanged(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountServiceListener");
                    if (paramBoolean)
                    {
                        localParcel1.writeInt(i);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.storage.IMountServiceListener
 * JD-Core Version:        0.6.2
 */