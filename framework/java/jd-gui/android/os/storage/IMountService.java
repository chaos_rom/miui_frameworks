package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public abstract interface IMountService extends IInterface
{
    public static final int ENCRYPTION_STATE_ERROR_INCOMPLETE = -2;
    public static final int ENCRYPTION_STATE_ERROR_UNKNOWN = -1;
    public static final int ENCRYPTION_STATE_NONE = 1;
    public static final int ENCRYPTION_STATE_OK;

    public abstract int changeEncryptionPassword(String paramString)
        throws RemoteException;

    public abstract int createSecureContainer(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2, boolean paramBoolean)
        throws RemoteException;

    public abstract int decryptStorage(String paramString)
        throws RemoteException;

    public abstract int destroySecureContainer(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract int encryptStorage(String paramString)
        throws RemoteException;

    public abstract int finalizeSecureContainer(String paramString)
        throws RemoteException;

    public abstract void finishMediaUpdate()
        throws RemoteException;

    public abstract int fixPermissionsSecureContainer(String paramString1, int paramInt, String paramString2)
        throws RemoteException;

    public abstract int formatVolume(String paramString)
        throws RemoteException;

    public abstract int getEncryptionState()
        throws RemoteException;

    public abstract String getMountedObbPath(String paramString)
        throws RemoteException;

    public abstract String getSecureContainerFilesystemPath(String paramString)
        throws RemoteException;

    public abstract String[] getSecureContainerList()
        throws RemoteException;

    public abstract String getSecureContainerPath(String paramString)
        throws RemoteException;

    public abstract int[] getStorageUsers(String paramString)
        throws RemoteException;

    public abstract Parcelable[] getVolumeList()
        throws RemoteException;

    public abstract String getVolumeState(String paramString)
        throws RemoteException;

    public abstract boolean isExternalStorageEmulated()
        throws RemoteException;

    public abstract boolean isObbMounted(String paramString)
        throws RemoteException;

    public abstract boolean isSecureContainerMounted(String paramString)
        throws RemoteException;

    public abstract boolean isUsbMassStorageConnected()
        throws RemoteException;

    public abstract boolean isUsbMassStorageEnabled()
        throws RemoteException;

    public abstract void mountObb(String paramString1, String paramString2, IObbActionListener paramIObbActionListener, int paramInt)
        throws RemoteException;

    public abstract int mountSecureContainer(String paramString1, String paramString2, int paramInt)
        throws RemoteException;

    public abstract int mountVolume(String paramString)
        throws RemoteException;

    public abstract void registerListener(IMountServiceListener paramIMountServiceListener)
        throws RemoteException;

    public abstract int renameSecureContainer(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setUsbMassStorageEnabled(boolean paramBoolean)
        throws RemoteException;

    public abstract void shutdown(IMountShutdownObserver paramIMountShutdownObserver)
        throws RemoteException;

    public abstract void unmountObb(String paramString, boolean paramBoolean, IObbActionListener paramIObbActionListener, int paramInt)
        throws RemoteException;

    public abstract int unmountSecureContainer(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void unmountVolume(String paramString, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public abstract void unregisterListener(IMountServiceListener paramIMountServiceListener)
        throws RemoteException;

    public abstract int verifyEncryptionPassword(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IMountService
    {
        private static final String DESCRIPTOR = "IMountService";
        static final int TRANSACTION_changeEncryptionPassword = 29;
        static final int TRANSACTION_createSecureContainer = 11;
        static final int TRANSACTION_decryptStorage = 27;
        static final int TRANSACTION_destroySecureContainer = 13;
        static final int TRANSACTION_encryptStorage = 28;
        static final int TRANSACTION_finalizeSecureContainer = 12;
        static final int TRANSACTION_finishMediaUpdate = 21;
        static final int TRANSACTION_fixPermissionsSecureContainer = 34;
        static final int TRANSACTION_formatVolume = 8;
        static final int TRANSACTION_getEncryptionState = 32;
        static final int TRANSACTION_getMountedObbPath = 25;
        static final int TRANSACTION_getSecureContainerFilesystemPath = 31;
        static final int TRANSACTION_getSecureContainerList = 19;
        static final int TRANSACTION_getSecureContainerPath = 18;
        static final int TRANSACTION_getStorageUsers = 9;
        static final int TRANSACTION_getVolumeList = 30;
        static final int TRANSACTION_getVolumeState = 10;
        static final int TRANSACTION_isExternalStorageEmulated = 26;
        static final int TRANSACTION_isObbMounted = 24;
        static final int TRANSACTION_isSecureContainerMounted = 16;
        static final int TRANSACTION_isUsbMassStorageConnected = 3;
        static final int TRANSACTION_isUsbMassStorageEnabled = 5;
        static final int TRANSACTION_mountObb = 22;
        static final int TRANSACTION_mountSecureContainer = 14;
        static final int TRANSACTION_mountVolume = 6;
        static final int TRANSACTION_registerListener = 1;
        static final int TRANSACTION_renameSecureContainer = 17;
        static final int TRANSACTION_setUsbMassStorageEnabled = 4;
        static final int TRANSACTION_shutdown = 20;
        static final int TRANSACTION_unmountObb = 23;
        static final int TRANSACTION_unmountSecureContainer = 15;
        static final int TRANSACTION_unmountVolume = 7;
        static final int TRANSACTION_unregisterListener = 2;
        static final int TRANSACTION_verifyEncryptionPassword = 33;

        public Stub()
        {
            attachInterface(this, "IMountService");
        }

        public static IMountService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("IMountService");
                if ((localIInterface != null) && ((localIInterface instanceof IMountService)))
                    localObject = (IMountService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 34:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("IMountService");
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                registerListener(IMountServiceListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                unregisterListener(IMountServiceListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                boolean bool13 = isUsbMassStorageConnected();
                paramParcel2.writeNoException();
                if (bool13);
                for (int i15 = 1; ; i15 = 0)
                {
                    paramParcel2.writeInt(i15);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                if (paramParcel1.readInt() != 0);
                for (boolean bool12 = true; ; bool12 = false)
                {
                    setUsbMassStorageEnabled(bool12);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                boolean bool11 = isUsbMassStorageEnabled();
                paramParcel2.writeNoException();
                if (bool11);
                for (int i14 = 1; ; i14 = 0)
                {
                    paramParcel2.writeInt(i14);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                int i13 = mountVolume(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i13);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String str11 = paramParcel1.readString();
                boolean bool9;
                if (paramParcel1.readInt() != 0)
                {
                    bool9 = true;
                    label540: if (paramParcel1.readInt() == 0)
                        break label576;
                }
                label576: for (boolean bool10 = true; ; bool10 = false)
                {
                    unmountVolume(str11, bool9, bool10);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                    bool9 = false;
                    break label540;
                }
                paramParcel1.enforceInterface("IMountService");
                int i12 = formatVolume(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i12);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                int[] arrayOfInt = getStorageUsers(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeIntArray(arrayOfInt);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String str10 = getVolumeState(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeString(str10);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String str7 = paramParcel1.readString();
                int i9 = paramParcel1.readInt();
                String str8 = paramParcel1.readString();
                String str9 = paramParcel1.readString();
                int i10 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool8 = true; ; bool8 = false)
                {
                    int i11 = createSecureContainer(str7, i9, str8, str9, i10, bool8);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i11);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                int i8 = finalizeSecureContainer(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i8);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String str6 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (boolean bool7 = true; ; bool7 = false)
                {
                    int i7 = destroySecureContainer(str6, bool7);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i7);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                int i6 = mountSecureContainer(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i6);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String str5 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (boolean bool6 = true; ; bool6 = false)
                {
                    int i5 = unmountSecureContainer(str5, bool6);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i5);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                boolean bool5 = isSecureContainerMounted(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool5);
                for (int i4 = 1; ; i4 = 0)
                {
                    paramParcel2.writeInt(i4);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                int i3 = renameSecureContainer(paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i3);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String str4 = getSecureContainerPath(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeString(str4);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String[] arrayOfString = getSecureContainerList();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                shutdown(IMountShutdownObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                finishMediaUpdate();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                mountObb(paramParcel1.readString(), paramParcel1.readString(), IObbActionListener.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String str3 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (boolean bool4 = true; ; bool4 = false)
                {
                    unmountObb(str3, bool4, IObbActionListener.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                boolean bool3 = isObbMounted(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool3);
                for (int i2 = 1; ; i2 = 0)
                {
                    paramParcel2.writeInt(i2);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                String str2 = getMountedObbPath(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeString(str2);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                boolean bool2 = isExternalStorageEmulated();
                paramParcel2.writeNoException();
                if (bool2);
                for (int i1 = 1; ; i1 = 0)
                {
                    paramParcel2.writeInt(i1);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("IMountService");
                int n = decryptStorage(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(n);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                int m = encryptStorage(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(m);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                int k = changeEncryptionPassword(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(k);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                Parcelable[] arrayOfParcelable = getVolumeList();
                paramParcel2.writeNoException();
                paramParcel2.writeParcelableArray(arrayOfParcelable, 0);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                String str1 = getSecureContainerFilesystemPath(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeString(str1);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                int j = getEncryptionState();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(j);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("IMountService");
                int i = fixPermissionsSecureContainer(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i);
                bool1 = true;
            }
        }

        private static class Proxy
            implements IMountService
        {
            private final IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public int changeEncryptionPassword(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int createSecureContainer(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeString(paramString3);
                    localParcel1.writeInt(paramInt2);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int j = localParcel2.readInt();
                    return j;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int decryptStorage(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(27, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int destroySecureContainer(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int j = localParcel2.readInt();
                    return j;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int encryptStorage(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(28, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int finalizeSecureContainer(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void finishMediaUpdate()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int fixPermissionsSecureContainer(String paramString1, int paramInt, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(34, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int formatVolume(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getEncryptionState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "IMountService";
            }

            public String getMountedObbPath(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getSecureContainerFilesystemPath(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(31, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getSecureContainerList()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getSecureContainerPath(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int[] getStorageUsers(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int[] arrayOfInt = localParcel2.createIntArray();
                    return arrayOfInt;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Parcelable[] getVolumeList()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    Parcelable[] arrayOfParcelable = localParcel2.readParcelableArray(StorageVolume.class.getClassLoader());
                    return arrayOfParcelable;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getVolumeState(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isExternalStorageEmulated()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isObbMounted(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(24, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isSecureContainerMounted(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isUsbMassStorageConnected()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isUsbMassStorageEnabled()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void mountObb(String paramString1, String paramString2, IObbActionListener paramIObbActionListener, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    if (paramIObbActionListener != null)
                    {
                        localIBinder = paramIObbActionListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(22, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int mountSecureContainer(String paramString1, String paramString2, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int mountVolume(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void registerListener(IMountServiceListener paramIMountServiceListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    if (paramIMountServiceListener != null)
                    {
                        localIBinder = paramIMountServiceListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int renameSecureContainer(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setUsbMassStorageEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void shutdown(IMountShutdownObserver paramIMountShutdownObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    if (paramIMountShutdownObserver != null)
                    {
                        localIBinder = paramIMountShutdownObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(20, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unmountObb(String paramString, boolean paramBoolean, IObbActionListener paramIObbActionListener, int paramInt)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    if (paramIObbActionListener != null)
                    {
                        localIBinder = paramIObbActionListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(23, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int unmountSecureContainer(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int j = localParcel2.readInt();
                    return j;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unmountVolume(String paramString, boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    if (paramBoolean1);
                    for (int j = i; ; j = 0)
                    {
                        localParcel1.writeInt(j);
                        if (!paramBoolean2)
                            break;
                        localParcel1.writeInt(i);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unregisterListener(IMountServiceListener paramIMountServiceListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    if (paramIMountServiceListener != null)
                    {
                        localIBinder = paramIMountServiceListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int verifyEncryptionPassword(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(33, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.storage.IMountService
 * JD-Core Version:        0.6.2
 */