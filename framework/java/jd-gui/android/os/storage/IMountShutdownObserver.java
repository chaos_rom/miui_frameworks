package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IMountShutdownObserver extends IInterface
{
    public abstract void onShutDownComplete(int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IMountShutdownObserver
    {
        private static final String DESCRIPTOR = "IMountShutdownObserver";
        static final int TRANSACTION_onShutDownComplete = 1;

        public Stub()
        {
            attachInterface(this, "IMountShutdownObserver");
        }

        public static IMountShutdownObserver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("IMountShutdownObserver");
                if ((localIInterface != null) && ((localIInterface instanceof IMountShutdownObserver)))
                    localObject = (IMountShutdownObserver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("IMountShutdownObserver");
                continue;
                paramParcel1.enforceInterface("IMountShutdownObserver");
                onShutDownComplete(paramParcel1.readInt());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IMountShutdownObserver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "IMountShutdownObserver";
            }

            public void onShutDownComplete(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IMountShutdownObserver");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.storage.IMountShutdownObserver
 * JD-Core Version:        0.6.2
 */