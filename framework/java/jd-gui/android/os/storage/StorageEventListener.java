package android.os.storage;

public abstract class StorageEventListener
{
    public void onStorageStateChanged(String paramString1, String paramString2, String paramString3)
    {
    }

    public void onUsbMassStorageConnectionChanged(boolean paramBoolean)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.storage.StorageEventListener
 * JD-Core Version:        0.6.2
 */