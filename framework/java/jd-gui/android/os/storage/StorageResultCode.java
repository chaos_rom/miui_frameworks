package android.os.storage;

public class StorageResultCode
{
    public static final int OperationFailedInternalError = -1;
    public static final int OperationFailedMediaBlank = -3;
    public static final int OperationFailedMediaCorrupt = -4;
    public static final int OperationFailedNoMedia = -2;
    public static final int OperationFailedStorageBusy = -7;
    public static final int OperationFailedStorageMounted = -6;
    public static final int OperationFailedStorageNotMounted = -5;
    public static final int OperationSucceeded;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.storage.StorageResultCode
 * JD-Core Version:        0.6.2
 */