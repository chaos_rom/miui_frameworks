package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IObbActionListener extends IInterface
{
    public abstract void onObbResult(String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IObbActionListener
    {
        private static final String DESCRIPTOR = "IObbActionListener";
        static final int TRANSACTION_onObbResult = 1;

        public Stub()
        {
            attachInterface(this, "IObbActionListener");
        }

        public static IObbActionListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("IObbActionListener");
                if ((localIInterface != null) && ((localIInterface instanceof IObbActionListener)))
                    localObject = (IObbActionListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("IObbActionListener");
                continue;
                paramParcel1.enforceInterface("IObbActionListener");
                onObbResult(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IObbActionListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "IObbActionListener";
            }

            public void onObbResult(String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("IObbActionListener");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(1, localParcel1, localParcel2, 1);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.storage.IObbActionListener
 * JD-Core Version:        0.6.2
 */