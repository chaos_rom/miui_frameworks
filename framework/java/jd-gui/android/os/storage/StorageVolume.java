package android.os.storage;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class StorageVolume
    implements Parcelable
{
    public static final Parcelable.Creator<StorageVolume> CREATOR = new Parcelable.Creator()
    {
        public StorageVolume createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = 1;
            String str = paramAnonymousParcel.readString();
            int j = paramAnonymousParcel.readInt();
            int k = paramAnonymousParcel.readInt();
            int m = paramAnonymousParcel.readInt();
            int n = paramAnonymousParcel.readInt();
            int i1 = paramAnonymousParcel.readInt();
            int i2 = paramAnonymousParcel.readInt();
            long l = paramAnonymousParcel.readLong();
            int i3;
            int i5;
            if (k == i)
            {
                i3 = i;
                if (m != i)
                    break label102;
                i5 = i;
                label67: if (i2 != i)
                    break label108;
            }
            while (true)
            {
                return new StorageVolume(str, j, i3, i5, i1, n, i, l, null);
                int i4 = 0;
                break;
                label102: int i6 = 0;
                break label67;
                label108: i = 0;
            }
        }

        public StorageVolume[] newArray(int paramAnonymousInt)
        {
            return new StorageVolume[paramAnonymousInt];
        }
    };
    public static final String EXTRA_STORAGE_VOLUME = "storage_volume";
    private final boolean mAllowMassStorage;
    private final int mDescriptionId;
    private final boolean mEmulated;
    private final long mMaxFileSize;
    private final int mMtpReserveSpace;
    private final String mPath;
    private final boolean mRemovable;
    private int mStorageId;

    private StorageVolume(String paramString, int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2, int paramInt3, boolean paramBoolean3, long paramLong)
    {
        this.mPath = paramString;
        this.mDescriptionId = paramInt1;
        this.mRemovable = paramBoolean1;
        this.mEmulated = paramBoolean2;
        this.mMtpReserveSpace = paramInt2;
        this.mAllowMassStorage = paramBoolean3;
        this.mStorageId = paramInt3;
        this.mMaxFileSize = paramLong;
    }

    public StorageVolume(String paramString, int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2, boolean paramBoolean3, long paramLong)
    {
        this.mPath = paramString;
        this.mDescriptionId = paramInt1;
        this.mRemovable = paramBoolean1;
        this.mEmulated = paramBoolean2;
        this.mMtpReserveSpace = paramInt2;
        this.mAllowMassStorage = paramBoolean3;
        this.mMaxFileSize = paramLong;
    }

    public boolean allowMassStorage()
    {
        return this.mAllowMassStorage;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        StorageVolume localStorageVolume;
        if (((paramObject instanceof StorageVolume)) && (this.mPath != null))
            localStorageVolume = (StorageVolume)paramObject;
        for (boolean bool = this.mPath.equals(localStorageVolume.mPath); ; bool = false)
            return bool;
    }

    public String getDescription(Context paramContext)
    {
        return paramContext.getResources().getString(this.mDescriptionId);
    }

    public int getDescriptionId()
    {
        return this.mDescriptionId;
    }

    public long getMaxFileSize()
    {
        return this.mMaxFileSize;
    }

    public int getMtpReserveSpace()
    {
        return this.mMtpReserveSpace;
    }

    public String getPath()
    {
        return this.mPath;
    }

    public int getStorageId()
    {
        return this.mStorageId;
    }

    public int hashCode()
    {
        return this.mPath.hashCode();
    }

    public boolean isEmulated()
    {
        return this.mEmulated;
    }

    public boolean isRemovable()
    {
        return this.mRemovable;
    }

    public void setStorageId(int paramInt)
    {
        this.mStorageId = (1 + (paramInt + 1 << 16));
    }

    public String toString()
    {
        return "StorageVolume [mAllowMassStorage=" + this.mAllowMassStorage + ", mDescriptionId=" + this.mDescriptionId + ", mEmulated=" + this.mEmulated + ", mMaxFileSize=" + this.mMaxFileSize + ", mMtpReserveSpace=" + this.mMtpReserveSpace + ", mPath=" + this.mPath + ", mRemovable=" + this.mRemovable + ", mStorageId=" + this.mStorageId + "]";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeString(this.mPath);
        paramParcel.writeInt(this.mDescriptionId);
        int j;
        int k;
        if (this.mRemovable)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.mEmulated)
                break label93;
            k = i;
            label44: paramParcel.writeInt(k);
            paramParcel.writeInt(this.mStorageId);
            paramParcel.writeInt(this.mMtpReserveSpace);
            if (!this.mAllowMassStorage)
                break label99;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeLong(this.mMaxFileSize);
            return;
            j = 0;
            break;
            label93: k = 0;
            break label44;
            label99: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.storage.StorageVolume
 * JD-Core Version:        0.6.2
 */