package android.os.storage;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.util.SparseArray;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class StorageManager
{
    private static final String TAG = "StorageManager";
    private MountServiceBinderListener mBinderListener;
    private List<ListenerDelegate> mListeners = new ArrayList();
    private IMountService mMountService = IMountService.Stub.asInterface(ServiceManager.getService("mount"));
    private final AtomicInteger mNextNonce = new AtomicInteger(0);
    private final ObbActionListener mObbActionListener = new ObbActionListener(null);
    Looper mTgtLooper;

    public StorageManager(Looper paramLooper)
        throws RemoteException
    {
        if (this.mMountService == null)
            Log.e("StorageManager", "Unable to connect to mount service! - is it running yet?");
        while (true)
        {
            return;
            this.mTgtLooper = paramLooper;
            this.mBinderListener = new MountServiceBinderListener(null);
            this.mMountService.registerListener(this.mBinderListener);
        }
    }

    private int getNextNonce()
    {
        return this.mNextNonce.getAndIncrement();
    }

    public void disableUsbMassStorage()
    {
        try
        {
            this.mMountService.setUsbMassStorageEnabled(false);
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Log.e("StorageManager", "Failed to disable UMS", localException);
        }
    }

    public void enableUsbMassStorage()
    {
        try
        {
            this.mMountService.setUsbMassStorageEnabled(true);
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Log.e("StorageManager", "Failed to enable UMS", localException);
        }
    }

    public String getMountedObbPath(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("filename cannot be null");
        try
        {
            String str2 = this.mMountService.getMountedObbPath(paramString);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("StorageManager", "Failed to find mounted path for OBB", localRemoteException);
                String str1 = null;
            }
        }
    }

    public StorageVolume[] getVolumeList()
    {
        StorageVolume[] arrayOfStorageVolume;
        if (this.mMountService == null)
            arrayOfStorageVolume = new StorageVolume[0];
        while (true)
        {
            return arrayOfStorageVolume;
            try
            {
                Parcelable[] arrayOfParcelable = this.mMountService.getVolumeList();
                if (arrayOfParcelable == null)
                {
                    arrayOfStorageVolume = new StorageVolume[0];
                }
                else
                {
                    int i = arrayOfParcelable.length;
                    arrayOfStorageVolume = new StorageVolume[i];
                    for (int j = 0; j < i; j++)
                        arrayOfStorageVolume[j] = ((StorageVolume)arrayOfParcelable[j]);
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("StorageManager", "Failed to get volume list", localRemoteException);
                arrayOfStorageVolume = null;
            }
        }
    }

    public String[] getVolumePaths()
    {
        StorageVolume[] arrayOfStorageVolume = getVolumeList();
        String[] arrayOfString;
        if (arrayOfStorageVolume == null)
            arrayOfString = null;
        while (true)
        {
            return arrayOfString;
            int i = arrayOfStorageVolume.length;
            arrayOfString = new String[i];
            for (int j = 0; j < i; j++)
                arrayOfString[j] = arrayOfStorageVolume[j].getPath();
        }
    }

    public String getVolumeState(String paramString)
    {
        Object localObject;
        if (this.mMountService == null)
            localObject = "removed";
        while (true)
        {
            return localObject;
            try
            {
                String str = this.mMountService.getVolumeState(paramString);
                localObject = str;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("StorageManager", "Failed to get volume state", localRemoteException);
                localObject = null;
            }
        }
    }

    public boolean isObbMounted(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("filename cannot be null");
        try
        {
            boolean bool2 = this.mMountService.isObbMounted(paramString);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("StorageManager", "Failed to check if OBB is mounted", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean isUsbMassStorageConnected()
    {
        try
        {
            boolean bool2 = this.mMountService.isUsbMassStorageConnected();
            bool1 = bool2;
            return bool1;
        }
        catch (Exception localException)
        {
            while (true)
            {
                Log.e("StorageManager", "Failed to get UMS connection state", localException);
                boolean bool1 = false;
            }
        }
    }

    public boolean isUsbMassStorageEnabled()
    {
        try
        {
            boolean bool2 = this.mMountService.isUsbMassStorageEnabled();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("StorageManager", "Failed to get UMS enable state", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean mountObb(String paramString1, String paramString2, OnObbStateChangeListener paramOnObbStateChangeListener)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("filename cannot be null");
        if (paramOnObbStateChangeListener == null)
            throw new IllegalArgumentException("listener cannot be null");
        try
        {
            int i = this.mObbActionListener.addListener(paramOnObbStateChangeListener);
            this.mMountService.mountObb(paramString1, paramString2, this.mObbActionListener, i);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("StorageManager", "Failed to mount OBB", localRemoteException);
                boolean bool = false;
            }
        }
    }

    public void registerListener(StorageEventListener paramStorageEventListener)
    {
        if (paramStorageEventListener == null);
        while (true)
        {
            return;
            synchronized (this.mListeners)
            {
                this.mListeners.add(new ListenerDelegate(paramStorageEventListener));
            }
        }
    }

    public boolean unmountObb(String paramString, boolean paramBoolean, OnObbStateChangeListener paramOnObbStateChangeListener)
    {
        if (paramString == null)
            throw new IllegalArgumentException("filename cannot be null");
        if (paramOnObbStateChangeListener == null)
            throw new IllegalArgumentException("listener cannot be null");
        try
        {
            int i = this.mObbActionListener.addListener(paramOnObbStateChangeListener);
            this.mMountService.unmountObb(paramString, paramBoolean, this.mObbActionListener, i);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("StorageManager", "Failed to mount OBB", localRemoteException);
                boolean bool = false;
            }
        }
    }

    public void unregisterListener(StorageEventListener paramStorageEventListener)
    {
        if (paramStorageEventListener == null)
            return;
        while (true)
        {
            int j;
            synchronized (this.mListeners)
            {
                int i = this.mListeners.size();
                j = 0;
                if (j < i)
                {
                    if (((ListenerDelegate)this.mListeners.get(j)).getListener() != paramStorageEventListener)
                        break label76;
                    this.mListeners.remove(j);
                }
            }
            label76: j++;
        }
    }

    private class ListenerDelegate
    {
        private final Handler mHandler;
        final StorageEventListener mStorageEventListener;

        ListenerDelegate(StorageEventListener arg2)
        {
            Object localObject;
            this.mStorageEventListener = localObject;
            this.mHandler = new Handler(StorageManager.this.mTgtLooper)
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    StorageManager.StorageEvent localStorageEvent = (StorageManager.StorageEvent)paramAnonymousMessage.obj;
                    if (paramAnonymousMessage.what == 1)
                    {
                        StorageManager.UmsConnectionChangedStorageEvent localUmsConnectionChangedStorageEvent = (StorageManager.UmsConnectionChangedStorageEvent)localStorageEvent;
                        StorageManager.ListenerDelegate.this.mStorageEventListener.onUsbMassStorageConnectionChanged(localUmsConnectionChangedStorageEvent.available);
                    }
                    while (true)
                    {
                        return;
                        if (paramAnonymousMessage.what == 2)
                        {
                            StorageManager.StorageStateChangedStorageEvent localStorageStateChangedStorageEvent = (StorageManager.StorageStateChangedStorageEvent)localStorageEvent;
                            StorageManager.ListenerDelegate.this.mStorageEventListener.onStorageStateChanged(localStorageStateChangedStorageEvent.path, localStorageStateChangedStorageEvent.oldState, localStorageStateChangedStorageEvent.newState);
                        }
                        else
                        {
                            Log.e("StorageManager", "Unsupported event " + paramAnonymousMessage.what);
                        }
                    }
                }
            };
        }

        StorageEventListener getListener()
        {
            return this.mStorageEventListener;
        }

        void sendShareAvailabilityChanged(boolean paramBoolean)
        {
            StorageManager.UmsConnectionChangedStorageEvent localUmsConnectionChangedStorageEvent = new StorageManager.UmsConnectionChangedStorageEvent(StorageManager.this, paramBoolean);
            this.mHandler.sendMessage(localUmsConnectionChangedStorageEvent.getMessage());
        }

        void sendStorageStateChanged(String paramString1, String paramString2, String paramString3)
        {
            StorageManager.StorageStateChangedStorageEvent localStorageStateChangedStorageEvent = new StorageManager.StorageStateChangedStorageEvent(StorageManager.this, paramString1, paramString2, paramString3);
            this.mHandler.sendMessage(localStorageStateChangedStorageEvent.getMessage());
        }
    }

    private class StorageStateChangedStorageEvent extends StorageManager.StorageEvent
    {
        public String newState;
        public String oldState;
        public String path;

        public StorageStateChangedStorageEvent(String paramString1, String paramString2, String arg4)
        {
            super(2);
            this.path = paramString1;
            this.oldState = paramString2;
            Object localObject;
            this.newState = localObject;
        }
    }

    private class UmsConnectionChangedStorageEvent extends StorageManager.StorageEvent
    {
        public boolean available;

        public UmsConnectionChangedStorageEvent(boolean arg2)
        {
            super(1);
            boolean bool;
            this.available = bool;
        }
    }

    private class StorageEvent
    {
        static final int EVENT_OBB_STATE_CHANGED = 3;
        static final int EVENT_STORAGE_STATE_CHANGED = 2;
        static final int EVENT_UMS_CONNECTION_CHANGED = 1;
        private Message mMessage = Message.obtain();

        public StorageEvent(int arg2)
        {
            int i;
            this.mMessage.what = i;
            this.mMessage.obj = this;
        }

        public Message getMessage()
        {
            return this.mMessage;
        }
    }

    private class ObbStateChangedStorageEvent extends StorageManager.StorageEvent
    {
        public final String path;
        public final int state;

        public ObbStateChangedStorageEvent(String paramInt, int arg3)
        {
            super(3);
            this.path = paramInt;
            int i;
            this.state = i;
        }
    }

    private class ObbListenerDelegate
    {
        private final Handler mHandler;
        private final WeakReference<OnObbStateChangeListener> mObbEventListenerRef;
        private final int nonce = StorageManager.this.getNextNonce();

        ObbListenerDelegate(OnObbStateChangeListener arg2)
        {
            Object localObject;
            this.mObbEventListenerRef = new WeakReference(localObject);
            this.mHandler = new Handler(StorageManager.this.mTgtLooper)
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    OnObbStateChangeListener localOnObbStateChangeListener = StorageManager.ObbListenerDelegate.this.getListener();
                    if (localOnObbStateChangeListener == null);
                    while (true)
                    {
                        return;
                        StorageManager.StorageEvent localStorageEvent = (StorageManager.StorageEvent)paramAnonymousMessage.obj;
                        if (paramAnonymousMessage.what == 3)
                        {
                            StorageManager.ObbStateChangedStorageEvent localObbStateChangedStorageEvent = (StorageManager.ObbStateChangedStorageEvent)localStorageEvent;
                            localOnObbStateChangeListener.onObbStateChange(localObbStateChangedStorageEvent.path, localObbStateChangedStorageEvent.state);
                        }
                        else
                        {
                            Log.e("StorageManager", "Unsupported event " + paramAnonymousMessage.what);
                        }
                    }
                }
            };
        }

        OnObbStateChangeListener getListener()
        {
            if (this.mObbEventListenerRef == null);
            for (OnObbStateChangeListener localOnObbStateChangeListener = null; ; localOnObbStateChangeListener = (OnObbStateChangeListener)this.mObbEventListenerRef.get())
                return localOnObbStateChangeListener;
        }

        void sendObbStateChanged(String paramString, int paramInt)
        {
            StorageManager.ObbStateChangedStorageEvent localObbStateChangedStorageEvent = new StorageManager.ObbStateChangedStorageEvent(StorageManager.this, paramString, paramInt);
            this.mHandler.sendMessage(localObbStateChangedStorageEvent.getMessage());
        }
    }

    private class ObbActionListener extends IObbActionListener.Stub
    {
        private SparseArray<StorageManager.ObbListenerDelegate> mListeners = new SparseArray();

        private ObbActionListener()
        {
        }

        public int addListener(OnObbStateChangeListener paramOnObbStateChangeListener)
        {
            StorageManager.ObbListenerDelegate localObbListenerDelegate = new StorageManager.ObbListenerDelegate(StorageManager.this, paramOnObbStateChangeListener);
            synchronized (this.mListeners)
            {
                this.mListeners.put(localObbListenerDelegate.nonce, localObbListenerDelegate);
                return localObbListenerDelegate.nonce;
            }
        }

        public void onObbResult(String paramString, int paramInt1, int paramInt2)
        {
            synchronized (this.mListeners)
            {
                StorageManager.ObbListenerDelegate localObbListenerDelegate = (StorageManager.ObbListenerDelegate)this.mListeners.get(paramInt1);
                if (localObbListenerDelegate != null)
                    this.mListeners.remove(paramInt1);
                if (localObbListenerDelegate != null)
                    localObbListenerDelegate.sendObbStateChanged(paramString, paramInt2);
                return;
            }
        }
    }

    private class MountServiceBinderListener extends IMountServiceListener.Stub
    {
        private MountServiceBinderListener()
        {
        }

        public void onStorageStateChanged(String paramString1, String paramString2, String paramString3)
        {
            int i = StorageManager.this.mListeners.size();
            for (int j = 0; j < i; j++)
                ((StorageManager.ListenerDelegate)StorageManager.this.mListeners.get(j)).sendStorageStateChanged(paramString1, paramString2, paramString3);
        }

        public void onUsbMassStorageConnectionChanged(boolean paramBoolean)
        {
            int i = StorageManager.this.mListeners.size();
            for (int j = 0; j < i; j++)
                ((StorageManager.ListenerDelegate)StorageManager.this.mListeners.get(j)).sendShareAvailabilityChanged(paramBoolean);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.storage.StorageManager
 * JD-Core Version:        0.6.2
 */