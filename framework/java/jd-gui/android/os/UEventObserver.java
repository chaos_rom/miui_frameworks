package android.os;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class UEventObserver
{
    private static final String TAG = UEventObserver.class.getSimpleName();
    private static UEventThread sThread;
    private static boolean sThreadStarted = false;

    /** @deprecated */
    private static final void ensureThreadStarted()
    {
        try
        {
            if (!sThreadStarted)
            {
                sThread = new UEventThread();
                sThread.start();
                sThreadStarted = true;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private static native void native_setup();

    private static native int next_event(byte[] paramArrayOfByte);

    protected void finalize()
        throws Throwable
    {
        try
        {
            stopObserving();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public abstract void onUEvent(UEvent paramUEvent);

    /** @deprecated */
    public final void startObserving(String paramString)
    {
        try
        {
            ensureThreadStarted();
            sThread.addObserver(paramString, this);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public final void stopObserving()
    {
        try
        {
            sThread.removeObserver(this);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private static class UEventThread extends Thread
    {
        private ArrayList<Object> mObservers = new ArrayList();

        UEventThread()
        {
            super();
        }

        public void addObserver(String paramString, UEventObserver paramUEventObserver)
        {
            synchronized (this.mObservers)
            {
                this.mObservers.add(paramString);
                this.mObservers.add(paramUEventObserver);
                return;
            }
        }

        public void removeObserver(UEventObserver paramUEventObserver)
        {
            ArrayList localArrayList = this.mObservers;
            int i = 1;
            if (i != 0)
                i = 0;
            for (int j = 0; ; j += 2)
                try
                {
                    if (j >= this.mObservers.size())
                        break;
                    if (this.mObservers.get(j + 1) == paramUEventObserver)
                    {
                        this.mObservers.remove(j + 1);
                        this.mObservers.remove(j);
                        i = 1;
                        break;
                        return;
                    }
                }
                finally
                {
                    localObject = finally;
                    throw localObject;
                }
        }

        public void run()
        {
            UEventObserver.access$000();
            byte[] arrayOfByte = new byte[1024];
            int i;
            do
                i = UEventObserver.next_event(arrayOfByte);
            while (i <= 0);
            String str = new String(arrayOfByte, 0, i);
            ArrayList localArrayList = this.mObservers;
            for (int j = 0; ; j += 2)
                try
                {
                    if (j < this.mObservers.size())
                    {
                        if (str.indexOf((String)this.mObservers.get(j)) != -1)
                            ((UEventObserver)this.mObservers.get(j + 1)).onUEvent(new UEventObserver.UEvent(str));
                    }
                    else
                        break;
                }
                finally
                {
                    localObject = finally;
                    throw localObject;
                }
        }
    }

    public static class UEvent
    {
        public HashMap<String, String> mMap = new HashMap();

        public UEvent(String paramString)
        {
            int i = 0;
            int j = paramString.length();
            while (true)
            {
                int k;
                int m;
                if (i < j)
                {
                    k = paramString.indexOf('=', i);
                    m = paramString.indexOf(0, i);
                    if (m >= 0);
                }
                else
                {
                    return;
                }
                if ((k > i) && (k < m))
                    this.mMap.put(paramString.substring(i, k), paramString.substring(k + 1, m));
                i = m + 1;
            }
        }

        public String get(String paramString)
        {
            return (String)this.mMap.get(paramString);
        }

        public String get(String paramString1, String paramString2)
        {
            String str = (String)this.mMap.get(paramString1);
            if (str == null);
            while (true)
            {
                return paramString2;
                paramString2 = str;
            }
        }

        public String toString()
        {
            return this.mMap.toString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.UEventObserver
 * JD-Core Version:        0.6.2
 */