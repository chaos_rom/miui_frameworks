package android.os;

public abstract interface IVibratorService extends IInterface
{
    public abstract void cancelVibrate(IBinder paramIBinder)
        throws RemoteException;

    public abstract boolean hasVibrator()
        throws RemoteException;

    public abstract void vibrate(long paramLong, IBinder paramIBinder)
        throws RemoteException;

    public abstract void vibratePattern(long[] paramArrayOfLong, int paramInt, IBinder paramIBinder)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IVibratorService
    {
        private static final String DESCRIPTOR = "android.os.IVibratorService";
        static final int TRANSACTION_cancelVibrate = 4;
        static final int TRANSACTION_hasVibrator = 1;
        static final int TRANSACTION_vibrate = 2;
        static final int TRANSACTION_vibratePattern = 3;

        public Stub()
        {
            attachInterface(this, "android.os.IVibratorService");
        }

        public static IVibratorService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.os.IVibratorService");
                if ((localIInterface != null) && ((localIInterface instanceof IVibratorService)))
                    localObject = (IVibratorService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.os.IVibratorService");
                continue;
                paramParcel1.enforceInterface("android.os.IVibratorService");
                boolean bool = hasVibrator();
                paramParcel2.writeNoException();
                if (bool);
                int k;
                for (int j = i; ; k = 0)
                {
                    paramParcel2.writeInt(j);
                    break;
                }
                paramParcel1.enforceInterface("android.os.IVibratorService");
                vibrate(paramParcel1.readLong(), paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IVibratorService");
                vibratePattern(paramParcel1.createLongArray(), paramParcel1.readInt(), paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.os.IVibratorService");
                cancelVibrate(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IVibratorService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancelVibrate(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IVibratorService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.os.IVibratorService";
            }

            public boolean hasVibrator()
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IVibratorService");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        return bool;
                    bool = false;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void vibrate(long paramLong, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IVibratorService");
                    localParcel1.writeLong(paramLong);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void vibratePattern(long[] paramArrayOfLong, int paramInt, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.os.IVibratorService");
                    localParcel1.writeLongArray(paramArrayOfLong);
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.IVibratorService
 * JD-Core Version:        0.6.2
 */