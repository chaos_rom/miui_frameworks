package android.os;

import android.util.Log;
import com.android.internal.os.BinderInternal;
import java.util.HashMap;
import java.util.Map;

public final class ServiceManager
{
    private static final String TAG = "ServiceManager";
    private static HashMap<String, IBinder> sCache = new HashMap();
    private static IServiceManager sServiceManager;

    public static void addService(String paramString, IBinder paramIBinder)
    {
        try
        {
            getIServiceManager().addService(paramString, paramIBinder, false);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("ServiceManager", "error in addService", localRemoteException);
        }
    }

    public static void addService(String paramString, IBinder paramIBinder, boolean paramBoolean)
    {
        try
        {
            getIServiceManager().addService(paramString, paramIBinder, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("ServiceManager", "error in addService", localRemoteException);
        }
    }

    public static IBinder checkService(String paramString)
    {
        Object localObject;
        try
        {
            localObject = (IBinder)sCache.get(paramString);
            if (localObject == null)
            {
                IBinder localIBinder = getIServiceManager().checkService(paramString);
                localObject = localIBinder;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("ServiceManager", "error in checkService", localRemoteException);
            localObject = null;
        }
        return localObject;
    }

    private static IServiceManager getIServiceManager()
    {
        if (sServiceManager != null);
        for (IServiceManager localIServiceManager = sServiceManager; ; localIServiceManager = sServiceManager)
        {
            return localIServiceManager;
            sServiceManager = ServiceManagerNative.asInterface(BinderInternal.getContextObject());
        }
    }

    public static IBinder getService(String paramString)
    {
        Object localObject;
        try
        {
            localObject = (IBinder)sCache.get(paramString);
            if (localObject == null)
            {
                IBinder localIBinder = getIServiceManager().getService(paramString);
                localObject = localIBinder;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("ServiceManager", "error in getService", localRemoteException);
            localObject = null;
        }
        return localObject;
    }

    public static void initServiceCache(Map<String, IBinder> paramMap)
    {
        if (sCache.size() != 0)
            throw new IllegalStateException("setServiceCache may only be called once");
        sCache.putAll(paramMap);
    }

    public static String[] listServices()
        throws RemoteException
    {
        try
        {
            String[] arrayOfString2 = getIServiceManager().listServices();
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("ServiceManager", "error in listServices", localRemoteException);
                String[] arrayOfString1 = null;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.ServiceManager
 * JD-Core Version:        0.6.2
 */