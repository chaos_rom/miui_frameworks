package android.os;

import java.io.PrintStream;

public class Broadcaster
{
    private Registration mReg;

    public void broadcast(Message paramMessage)
    {
        try
        {
            if (this.mReg == null)
                return;
            int i = paramMessage.what;
            Registration localRegistration1 = this.mReg;
            Registration localRegistration2 = localRegistration1;
            if (localRegistration2.senderWhat >= i);
            while (true)
            {
                if (localRegistration2.senderWhat != i)
                    break label135;
                Handler[] arrayOfHandler = localRegistration2.targets;
                int[] arrayOfInt = localRegistration2.targetWhats;
                int j = arrayOfHandler.length;
                for (int k = 0; k < j; k++)
                {
                    Handler localHandler = arrayOfHandler[k];
                    Message localMessage = Message.obtain();
                    localMessage.copyFrom(paramMessage);
                    localMessage.what = arrayOfInt[k];
                    localHandler.sendMessage(localMessage);
                }
                localRegistration2 = localRegistration2.next;
                if (localRegistration2 != localRegistration1)
                    break;
            }
        }
        finally
        {
            label135: localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public void cancelRequest(int paramInt1, Handler paramHandler, int paramInt2)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 19	android/os/Broadcaster:mReg	Landroid/os/Broadcaster$Registration;
        //     6: astore 5
        //     8: aload 5
        //     10: astore 6
        //     12: aload 6
        //     14: ifnonnull +8 -> 22
        //     17: aload_0
        //     18: monitorexit
        //     19: goto +210 -> 229
        //     22: aload 6
        //     24: getfield 28	android/os/Broadcaster$Registration:senderWhat	I
        //     27: iload_1
        //     28: if_icmplt +178 -> 206
        //     31: aload 6
        //     33: getfield 28	android/os/Broadcaster$Registration:senderWhat	I
        //     36: iload_1
        //     37: if_icmpne +157 -> 194
        //     40: aload 6
        //     42: getfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     45: astore 7
        //     47: aload 6
        //     49: getfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     52: astore 8
        //     54: aload 7
        //     56: arraylength
        //     57: istore 9
        //     59: iconst_0
        //     60: istore 10
        //     62: iload 10
        //     64: iload 9
        //     66: if_icmpge +128 -> 194
        //     69: aload 7
        //     71: iload 10
        //     73: aaload
        //     74: aload_2
        //     75: if_acmpne +148 -> 223
        //     78: aload 8
        //     80: iload 10
        //     82: iaload
        //     83: iload_3
        //     84: if_icmpne +139 -> 223
        //     87: aload 6
        //     89: iload 9
        //     91: iconst_1
        //     92: isub
        //     93: anewarray 45	android/os/Handler
        //     96: putfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     99: aload 6
        //     101: iload 9
        //     103: iconst_1
        //     104: isub
        //     105: newarray int
        //     107: putfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     110: iload 10
        //     112: ifle +31 -> 143
        //     115: aload 7
        //     117: iconst_0
        //     118: aload 6
        //     120: getfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     123: iconst_0
        //     124: iload 10
        //     126: invokestatic 60	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     129: aload 8
        //     131: iconst_0
        //     132: aload 6
        //     134: getfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     137: iconst_0
        //     138: iload 10
        //     140: invokestatic 60	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     143: bipush 255
        //     145: iload 9
        //     147: iload 10
        //     149: isub
        //     150: iadd
        //     151: istore 11
        //     153: iload 11
        //     155: ifeq +39 -> 194
        //     158: aload 7
        //     160: iload 10
        //     162: iconst_1
        //     163: iadd
        //     164: aload 6
        //     166: getfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     169: iload 10
        //     171: iload 11
        //     173: invokestatic 60	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     176: aload 8
        //     178: iload 10
        //     180: iconst_1
        //     181: iadd
        //     182: aload 6
        //     184: getfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     187: iload 10
        //     189: iload 11
        //     191: invokestatic 60	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     194: aload_0
        //     195: monitorexit
        //     196: goto +33 -> 229
        //     199: astore 4
        //     201: aload_0
        //     202: monitorexit
        //     203: aload 4
        //     205: athrow
        //     206: aload 6
        //     208: getfield 52	android/os/Broadcaster$Registration:next	Landroid/os/Broadcaster$Registration;
        //     211: astore 6
        //     213: aload 6
        //     215: aload 5
        //     217: if_acmpne -195 -> 22
        //     220: goto -189 -> 31
        //     223: iinc 10 1
        //     226: goto -164 -> 62
        //     229: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	203	199	finally
        //     206	213	199	finally
    }

    public void dumpRegistrations()
    {
        try
        {
            Registration localRegistration1 = this.mReg;
            System.out.println("Broadcaster " + this + " {");
            if (localRegistration1 != null)
            {
                Registration localRegistration2 = localRegistration1;
                do
                {
                    System.out.println("        senderWhat=" + localRegistration2.senderWhat);
                    int i = localRegistration2.targets.length;
                    for (int j = 0; j < i; j++)
                        System.out.println("                [" + localRegistration2.targetWhats[j] + "] " + localRegistration2.targets[j]);
                    localRegistration2 = localRegistration2.next;
                }
                while (localRegistration2 != localRegistration1);
            }
            System.out.println("}");
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public void request(int paramInt1, Handler paramHandler, int paramInt2)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 19	android/os/Broadcaster:mReg	Landroid/os/Broadcaster$Registration;
        //     6: ifnonnull +78 -> 84
        //     9: new 8	android/os/Broadcaster$Registration
        //     12: dup
        //     13: aload_0
        //     14: aconst_null
        //     15: invokespecial 104	android/os/Broadcaster$Registration:<init>	(Landroid/os/Broadcaster;Landroid/os/Broadcaster$1;)V
        //     18: astore 5
        //     20: aload 5
        //     22: iload_1
        //     23: putfield 28	android/os/Broadcaster$Registration:senderWhat	I
        //     26: aload 5
        //     28: iconst_1
        //     29: anewarray 45	android/os/Handler
        //     32: putfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     35: aload 5
        //     37: iconst_1
        //     38: newarray int
        //     40: putfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     43: aload 5
        //     45: getfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     48: iconst_0
        //     49: aload_2
        //     50: aastore
        //     51: aload 5
        //     53: getfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     56: iconst_0
        //     57: iload_3
        //     58: iastore
        //     59: aload_0
        //     60: aload 5
        //     62: putfield 19	android/os/Broadcaster:mReg	Landroid/os/Broadcaster$Registration;
        //     65: aload 5
        //     67: aload 5
        //     69: putfield 52	android/os/Broadcaster$Registration:next	Landroid/os/Broadcaster$Registration;
        //     72: aload 5
        //     74: aload 5
        //     76: putfield 107	android/os/Broadcaster$Registration:prev	Landroid/os/Broadcaster$Registration;
        //     79: aload_0
        //     80: monitorexit
        //     81: goto +287 -> 368
        //     84: aload_0
        //     85: getfield 19	android/os/Broadcaster:mReg	Landroid/os/Broadcaster$Registration;
        //     88: astore 6
        //     90: aload 6
        //     92: astore 7
        //     94: aload 7
        //     96: getfield 28	android/os/Broadcaster$Registration:senderWhat	I
        //     99: iload_1
        //     100: if_icmplt +137 -> 237
        //     103: aload 7
        //     105: getfield 28	android/os/Broadcaster$Registration:senderWhat	I
        //     108: iload_1
        //     109: if_icmpeq +145 -> 254
        //     112: new 8	android/os/Broadcaster$Registration
        //     115: dup
        //     116: aload_0
        //     117: aconst_null
        //     118: invokespecial 104	android/os/Broadcaster$Registration:<init>	(Landroid/os/Broadcaster;Landroid/os/Broadcaster$1;)V
        //     121: astore 8
        //     123: aload 8
        //     125: iload_1
        //     126: putfield 28	android/os/Broadcaster$Registration:senderWhat	I
        //     129: aload 8
        //     131: iconst_1
        //     132: anewarray 45	android/os/Handler
        //     135: putfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     138: aload 8
        //     140: iconst_1
        //     141: newarray int
        //     143: putfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     146: aload 8
        //     148: aload 7
        //     150: putfield 52	android/os/Broadcaster$Registration:next	Landroid/os/Broadcaster$Registration;
        //     153: aload 8
        //     155: aload 7
        //     157: getfield 107	android/os/Broadcaster$Registration:prev	Landroid/os/Broadcaster$Registration;
        //     160: putfield 107	android/os/Broadcaster$Registration:prev	Landroid/os/Broadcaster$Registration;
        //     163: aload 7
        //     165: getfield 107	android/os/Broadcaster$Registration:prev	Landroid/os/Broadcaster$Registration;
        //     168: aload 8
        //     170: putfield 52	android/os/Broadcaster$Registration:next	Landroid/os/Broadcaster$Registration;
        //     173: aload 7
        //     175: aload 8
        //     177: putfield 107	android/os/Broadcaster$Registration:prev	Landroid/os/Broadcaster$Registration;
        //     180: aload 7
        //     182: aload_0
        //     183: getfield 19	android/os/Broadcaster:mReg	Landroid/os/Broadcaster$Registration;
        //     186: if_acmpne +183 -> 369
        //     189: aload 7
        //     191: getfield 28	android/os/Broadcaster$Registration:senderWhat	I
        //     194: aload 8
        //     196: getfield 28	android/os/Broadcaster$Registration:senderWhat	I
        //     199: if_icmple +170 -> 369
        //     202: aload_0
        //     203: aload 8
        //     205: putfield 19	android/os/Broadcaster:mReg	Landroid/os/Broadcaster$Registration;
        //     208: goto +161 -> 369
        //     211: aload 7
        //     213: getfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     216: iload 9
        //     218: aload_2
        //     219: aastore
        //     220: aload 7
        //     222: getfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     225: iload 9
        //     227: iload_3
        //     228: iastore
        //     229: goto -150 -> 79
        //     232: aload_0
        //     233: monitorexit
        //     234: aload 4
        //     236: athrow
        //     237: aload 7
        //     239: getfield 52	android/os/Broadcaster$Registration:next	Landroid/os/Broadcaster$Registration;
        //     242: astore 7
        //     244: aload 7
        //     246: aload 6
        //     248: if_acmpne -154 -> 94
        //     251: goto -148 -> 103
        //     254: aload 7
        //     256: getfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     259: arraylength
        //     260: istore 9
        //     262: aload 7
        //     264: getfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     267: astore 10
        //     269: aload 7
        //     271: getfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     274: astore 11
        //     276: iconst_0
        //     277: istore 12
        //     279: iload 12
        //     281: iload 9
        //     283: if_icmpge +26 -> 309
        //     286: aload 10
        //     288: iload 12
        //     290: aaload
        //     291: aload_2
        //     292: if_acmpne +92 -> 384
        //     295: aload 11
        //     297: iload 12
        //     299: iaload
        //     300: iload_3
        //     301: if_icmpne +83 -> 384
        //     304: aload_0
        //     305: monitorexit
        //     306: goto +62 -> 368
        //     309: aload 7
        //     311: iload 9
        //     313: iconst_1
        //     314: iadd
        //     315: anewarray 45	android/os/Handler
        //     318: putfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     321: aload 10
        //     323: iconst_0
        //     324: aload 7
        //     326: getfield 32	android/os/Broadcaster$Registration:targets	[Landroid/os/Handler;
        //     329: iconst_0
        //     330: iload 9
        //     332: invokestatic 60	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     335: aload 7
        //     337: iload 9
        //     339: iconst_1
        //     340: iadd
        //     341: newarray int
        //     343: putfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     346: aload 11
        //     348: iconst_0
        //     349: aload 7
        //     351: getfield 36	android/os/Broadcaster$Registration:targetWhats	[I
        //     354: iconst_0
        //     355: iload 9
        //     357: invokestatic 60	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     360: goto -149 -> 211
        //     363: astore 4
        //     365: goto -133 -> 232
        //     368: return
        //     369: aload 8
        //     371: astore 7
        //     373: iconst_0
        //     374: istore 9
        //     376: goto -165 -> 211
        //     379: astore 4
        //     381: goto -149 -> 232
        //     384: iinc 12 1
        //     387: goto -108 -> 279
        //
        // Exception table:
        //     from	to	target	type
        //     20	79	363	finally
        //     2	20	379	finally
        //     79	234	379	finally
        //     237	360	379	finally
    }

    private class Registration
    {
        Registration next;
        Registration prev;
        int senderWhat;
        int[] targetWhats;
        Handler[] targets;

        private Registration()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Broadcaster
 * JD-Core Version:        0.6.2
 */