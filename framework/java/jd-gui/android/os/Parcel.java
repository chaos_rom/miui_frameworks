package android.os;

import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Parcel
{
    private static final boolean DEBUG_RECYCLE = false;
    private static final int EX_BAD_PARCELABLE = -2;
    private static final int EX_HAS_REPLY_HEADER = -128;
    private static final int EX_ILLEGAL_ARGUMENT = -3;
    private static final int EX_ILLEGAL_STATE = -5;
    private static final int EX_NULL_POINTER = -4;
    private static final int EX_SECURITY = -1;
    private static final int POOL_SIZE = 6;
    public static final Parcelable.Creator<String> STRING_CREATOR = new Parcelable.Creator()
    {
        public String createFromParcel(Parcel paramAnonymousParcel)
        {
            return paramAnonymousParcel.readString();
        }

        public String[] newArray(int paramAnonymousInt)
        {
            return new String[paramAnonymousInt];
        }
    };
    private static final String TAG = "Parcel";
    private static final int VAL_BOOLEAN = 9;
    private static final int VAL_BOOLEANARRAY = 23;
    private static final int VAL_BUNDLE = 3;
    private static final int VAL_BYTE = 20;
    private static final int VAL_BYTEARRAY = 13;
    private static final int VAL_CHARSEQUENCE = 10;
    private static final int VAL_CHARSEQUENCEARRAY = 24;
    private static final int VAL_DOUBLE = 8;
    private static final int VAL_FLOAT = 7;
    private static final int VAL_IBINDER = 15;
    private static final int VAL_INTARRAY = 18;
    private static final int VAL_INTEGER = 1;
    private static final int VAL_LIST = 11;
    private static final int VAL_LONG = 6;
    private static final int VAL_LONGARRAY = 19;
    private static final int VAL_MAP = 2;
    private static final int VAL_NULL = -1;
    private static final int VAL_OBJECTARRAY = 17;
    private static final int VAL_PARCELABLE = 4;
    private static final int VAL_PARCELABLEARRAY = 16;
    private static final int VAL_SERIALIZABLE = 21;
    private static final int VAL_SHORT = 5;
    private static final int VAL_SPARSEARRAY = 12;
    private static final int VAL_SPARSEBOOLEANARRAY = 22;
    private static final int VAL_STRING = 0;
    private static final int VAL_STRINGARRAY = 14;
    private static final HashMap<ClassLoader, HashMap<String, Parcelable.Creator>> mCreators = new HashMap();
    private static final Parcel[] sHolderPool;
    private static final Parcel[] sOwnedPool = new Parcel[6];
    private int mNativePtr;
    private boolean mOwnsNativeParcelObject;
    private RuntimeException mStack;

    static
    {
        sHolderPool = new Parcel[6];
    }

    private Parcel(int paramInt)
    {
        init(paramInt);
    }

    static native void clearFileDescriptor(FileDescriptor paramFileDescriptor);

    static native void closeFileDescriptor(FileDescriptor paramFileDescriptor)
        throws IOException;

    private void destroy()
    {
        if (this.mNativePtr != 0)
        {
            if (this.mOwnsNativeParcelObject)
                nativeDestroy(this.mNativePtr);
            this.mNativePtr = 0;
        }
    }

    static native FileDescriptor dupFileDescriptor(FileDescriptor paramFileDescriptor)
        throws IOException;

    private void freeBuffer()
    {
        if (this.mOwnsNativeParcelObject)
            nativeFreeBuffer(this.mNativePtr);
    }

    private void init(int paramInt)
    {
        if (paramInt != 0)
            this.mNativePtr = paramInt;
        for (this.mOwnsNativeParcelObject = false; ; this.mOwnsNativeParcelObject = true)
        {
            return;
            this.mNativePtr = nativeCreate();
        }
    }

    private static native void nativeAppendFrom(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native int nativeCreate();

    private static native byte[] nativeCreateByteArray(int paramInt);

    private static native int nativeDataAvail(int paramInt);

    private static native int nativeDataCapacity(int paramInt);

    private static native int nativeDataPosition(int paramInt);

    private static native int nativeDataSize(int paramInt);

    private static native void nativeDestroy(int paramInt);

    private static native void nativeEnforceInterface(int paramInt, String paramString);

    private static native void nativeFreeBuffer(int paramInt);

    private static native boolean nativeHasFileDescriptors(int paramInt);

    private static native byte[] nativeMarshall(int paramInt);

    private static native boolean nativePushAllowFds(int paramInt, boolean paramBoolean);

    private static native double nativeReadDouble(int paramInt);

    private static native FileDescriptor nativeReadFileDescriptor(int paramInt);

    private static native float nativeReadFloat(int paramInt);

    private static native int nativeReadInt(int paramInt);

    private static native long nativeReadLong(int paramInt);

    private static native String nativeReadString(int paramInt);

    private static native IBinder nativeReadStrongBinder(int paramInt);

    private static native void nativeRestoreAllowFds(int paramInt, boolean paramBoolean);

    private static native void nativeSetDataCapacity(int paramInt1, int paramInt2);

    private static native void nativeSetDataPosition(int paramInt1, int paramInt2);

    private static native void nativeSetDataSize(int paramInt1, int paramInt2);

    private static native void nativeUnmarshall(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3);

    private static native void nativeWriteByteArray(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3);

    private static native void nativeWriteDouble(int paramInt, double paramDouble);

    private static native void nativeWriteFileDescriptor(int paramInt, FileDescriptor paramFileDescriptor);

    private static native void nativeWriteFloat(int paramInt, float paramFloat);

    private static native void nativeWriteInt(int paramInt1, int paramInt2);

    private static native void nativeWriteInterfaceToken(int paramInt, String paramString);

    private static native void nativeWriteLong(int paramInt, long paramLong);

    private static native void nativeWriteString(int paramInt, String paramString);

    private static native void nativeWriteStrongBinder(int paramInt, IBinder paramIBinder);

    public static Parcel obtain()
    {
        Parcel[] arrayOfParcel = sOwnedPool;
        for (int i = 0; ; i++)
        {
            if (i < 6);
            Parcel localParcel;
            try
            {
                localParcel = arrayOfParcel[i];
                if (localParcel == null)
                    continue;
                arrayOfParcel[i] = null;
                break label50;
                localParcel = new Parcel(0);
            }
            finally
            {
            }
            label50: return localParcel;
        }
    }

    protected static final Parcel obtain(int paramInt)
    {
        Parcel[] arrayOfParcel = sHolderPool;
        for (int i = 0; ; i++)
        {
            if (i < 6);
            Parcel localParcel;
            try
            {
                localParcel = arrayOfParcel[i];
                if (localParcel == null)
                    continue;
                arrayOfParcel[i] = null;
                localParcel.init(paramInt);
                break label59;
                localParcel = new Parcel(paramInt);
            }
            finally
            {
            }
            label59: return localParcel;
        }
    }

    static native FileDescriptor openFileDescriptor(String paramString, int paramInt)
        throws FileNotFoundException;

    private void readArrayInternal(Object[] paramArrayOfObject, int paramInt, ClassLoader paramClassLoader)
    {
        for (int i = 0; i < paramInt; i++)
            paramArrayOfObject[i] = readValue(paramClassLoader);
    }

    private void readListInternal(List paramList, int paramInt, ClassLoader paramClassLoader)
    {
        while (paramInt > 0)
        {
            paramList.add(readValue(paramClassLoader));
            paramInt--;
        }
    }

    private void readSparseArrayInternal(SparseArray paramSparseArray, int paramInt, ClassLoader paramClassLoader)
    {
        while (paramInt > 0)
        {
            paramSparseArray.append(readInt(), readValue(paramClassLoader));
            paramInt--;
        }
    }

    private void readSparseBooleanArrayInternal(SparseBooleanArray paramSparseBooleanArray, int paramInt)
    {
        if (paramInt > 0)
        {
            int i = readInt();
            if (readByte() == 1);
            for (boolean bool = true; ; bool = false)
            {
                paramSparseBooleanArray.append(i, bool);
                paramInt--;
                break;
            }
        }
    }

    public final void appendFrom(Parcel paramParcel, int paramInt1, int paramInt2)
    {
        nativeAppendFrom(this.mNativePtr, paramParcel.mNativePtr, paramInt1, paramInt2);
    }

    public final IBinder[] createBinderArray()
    {
        int i = readInt();
        if (i >= 0)
        {
            arrayOfIBinder = new IBinder[i];
            for (int j = 0; j < i; j++)
                arrayOfIBinder[j] = readStrongBinder();
        }
        IBinder[] arrayOfIBinder = null;
        return arrayOfIBinder;
    }

    public final ArrayList<IBinder> createBinderArrayList()
    {
        int i = readInt();
        Object localObject;
        if (i < 0)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = new ArrayList(i);
            while (i > 0)
            {
                ((ArrayList)localObject).add(readStrongBinder());
                i--;
            }
        }
    }

    public final boolean[] createBooleanArray()
    {
        int i = readInt();
        boolean[] arrayOfBoolean;
        if ((i >= 0) && (i <= dataAvail() >> 2))
        {
            arrayOfBoolean = new boolean[i];
            int j = 0;
            if (j < i)
            {
                if (readInt() != 0);
                for (int k = 1; ; k = 0)
                {
                    arrayOfBoolean[j] = k;
                    j++;
                    break;
                }
            }
        }
        else
        {
            arrayOfBoolean = null;
        }
        return arrayOfBoolean;
    }

    public final byte[] createByteArray()
    {
        return nativeCreateByteArray(this.mNativePtr);
    }

    public final char[] createCharArray()
    {
        int i = readInt();
        char[] arrayOfChar;
        int j;
        if ((i >= 0) && (i <= dataAvail() >> 2))
        {
            arrayOfChar = new char[i];
            j = 0;
        }
        while (j < i)
        {
            arrayOfChar[j] = ((char)readInt());
            j++;
            continue;
            arrayOfChar = null;
        }
        return arrayOfChar;
    }

    public final double[] createDoubleArray()
    {
        int i = readInt();
        double[] arrayOfDouble;
        int j;
        if ((i >= 0) && (i <= dataAvail() >> 3))
        {
            arrayOfDouble = new double[i];
            j = 0;
        }
        while (j < i)
        {
            arrayOfDouble[j] = readDouble();
            j++;
            continue;
            arrayOfDouble = null;
        }
        return arrayOfDouble;
    }

    public final float[] createFloatArray()
    {
        int i = readInt();
        float[] arrayOfFloat;
        int j;
        if ((i >= 0) && (i <= dataAvail() >> 2))
        {
            arrayOfFloat = new float[i];
            j = 0;
        }
        while (j < i)
        {
            arrayOfFloat[j] = readFloat();
            j++;
            continue;
            arrayOfFloat = null;
        }
        return arrayOfFloat;
    }

    public final int[] createIntArray()
    {
        int i = readInt();
        int[] arrayOfInt;
        int j;
        if ((i >= 0) && (i <= dataAvail() >> 2))
        {
            arrayOfInt = new int[i];
            j = 0;
        }
        while (j < i)
        {
            arrayOfInt[j] = readInt();
            j++;
            continue;
            arrayOfInt = null;
        }
        return arrayOfInt;
    }

    public final long[] createLongArray()
    {
        int i = readInt();
        long[] arrayOfLong;
        int j;
        if ((i >= 0) && (i <= dataAvail() >> 3))
        {
            arrayOfLong = new long[i];
            j = 0;
        }
        while (j < i)
        {
            arrayOfLong[j] = readLong();
            j++;
            continue;
            arrayOfLong = null;
        }
        return arrayOfLong;
    }

    public final String[] createStringArray()
    {
        int i = readInt();
        if (i >= 0)
        {
            arrayOfString = new String[i];
            for (int j = 0; j < i; j++)
                arrayOfString[j] = readString();
        }
        String[] arrayOfString = null;
        return arrayOfString;
    }

    public final ArrayList<String> createStringArrayList()
    {
        int i = readInt();
        Object localObject;
        if (i < 0)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = new ArrayList(i);
            while (i > 0)
            {
                ((ArrayList)localObject).add(readString());
                i--;
            }
        }
    }

    public final <T> T[] createTypedArray(Parcelable.Creator<T> paramCreator)
    {
        int i = readInt();
        Object localObject;
        if (i < 0)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = paramCreator.newArray(i);
            for (int j = 0; j < i; j++)
                if (readInt() != 0)
                    localObject[j] = paramCreator.createFromParcel(this);
        }
    }

    public final <T> ArrayList<T> createTypedArrayList(Parcelable.Creator<T> paramCreator)
    {
        int i = readInt();
        if (i < 0)
        {
            localObject = null;
            return localObject;
        }
        Object localObject = new ArrayList(i);
        label22: if (i > 0)
        {
            if (readInt() == 0)
                break label51;
            ((ArrayList)localObject).add(paramCreator.createFromParcel(this));
        }
        while (true)
        {
            i--;
            break label22;
            break;
            label51: ((ArrayList)localObject).add(null);
        }
    }

    public final int dataAvail()
    {
        return nativeDataAvail(this.mNativePtr);
    }

    public final int dataCapacity()
    {
        return nativeDataCapacity(this.mNativePtr);
    }

    public final int dataPosition()
    {
        return nativeDataPosition(this.mNativePtr);
    }

    public final int dataSize()
    {
        return nativeDataSize(this.mNativePtr);
    }

    public final void enforceInterface(String paramString)
    {
        nativeEnforceInterface(this.mNativePtr, paramString);
    }

    protected void finalize()
        throws Throwable
    {
        destroy();
    }

    public final boolean hasFileDescriptors()
    {
        return nativeHasFileDescriptors(this.mNativePtr);
    }

    public final byte[] marshall()
    {
        return nativeMarshall(this.mNativePtr);
    }

    public final boolean pushAllowFds(boolean paramBoolean)
    {
        return nativePushAllowFds(this.mNativePtr, paramBoolean);
    }

    public final Object[] readArray(ClassLoader paramClassLoader)
    {
        int i = readInt();
        Object[] arrayOfObject;
        if (i < 0)
            arrayOfObject = null;
        while (true)
        {
            return arrayOfObject;
            arrayOfObject = new Object[i];
            readArrayInternal(arrayOfObject, i, paramClassLoader);
        }
    }

    public final ArrayList readArrayList(ClassLoader paramClassLoader)
    {
        int i = readInt();
        ArrayList localArrayList;
        if (i < 0)
            localArrayList = null;
        while (true)
        {
            return localArrayList;
            localArrayList = new ArrayList(i);
            readListInternal(localArrayList, i, paramClassLoader);
        }
    }

    public final void readBinderArray(IBinder[] paramArrayOfIBinder)
    {
        int i = readInt();
        if (i == paramArrayOfIBinder.length)
            for (int j = 0; j < i; j++)
                paramArrayOfIBinder[j] = readStrongBinder();
        throw new RuntimeException("bad array lengths");
    }

    public final void readBinderList(List<IBinder> paramList)
    {
        int i = paramList.size();
        int j = readInt();
        for (int k = 0; (k < i) && (k < j); k++)
            paramList.set(k, readStrongBinder());
        while (k < j)
        {
            paramList.add(readStrongBinder());
            k++;
        }
        while (k < i)
        {
            paramList.remove(j);
            k++;
        }
    }

    public final void readBooleanArray(boolean[] paramArrayOfBoolean)
    {
        int i = readInt();
        if (i == paramArrayOfBoolean.length)
        {
            int j = 0;
            if (j < i)
            {
                if (readInt() != 0);
                for (int k = 1; ; k = 0)
                {
                    paramArrayOfBoolean[j] = k;
                    j++;
                    break;
                }
            }
        }
        else
        {
            throw new RuntimeException("bad array lengths");
        }
    }

    public final Bundle readBundle()
    {
        return readBundle(null);
    }

    public final Bundle readBundle(ClassLoader paramClassLoader)
    {
        int i = readInt();
        Bundle localBundle;
        if (i < 0)
            localBundle = null;
        while (true)
        {
            return localBundle;
            localBundle = new Bundle(this, i);
            if (paramClassLoader != null)
                localBundle.setClassLoader(paramClassLoader);
        }
    }

    public final byte readByte()
    {
        return (byte)(0xFF & readInt());
    }

    public final void readByteArray(byte[] paramArrayOfByte)
    {
        byte[] arrayOfByte = createByteArray();
        if (arrayOfByte.length == paramArrayOfByte.length)
        {
            System.arraycopy(arrayOfByte, 0, paramArrayOfByte, 0, arrayOfByte.length);
            return;
        }
        throw new RuntimeException("bad array lengths");
    }

    public final void readCharArray(char[] paramArrayOfChar)
    {
        int i = readInt();
        if (i == paramArrayOfChar.length)
            for (int j = 0; j < i; j++)
                paramArrayOfChar[j] = ((char)readInt());
        throw new RuntimeException("bad array lengths");
    }

    public final CharSequence readCharSequence()
    {
        return (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(this);
    }

    public final CharSequence[] readCharSequenceArray()
    {
        CharSequence[] arrayOfCharSequence = null;
        int i = readInt();
        if (i >= 0)
        {
            arrayOfCharSequence = new CharSequence[i];
            for (int j = 0; j < i; j++)
                arrayOfCharSequence[j] = readCharSequence();
        }
        return arrayOfCharSequence;
    }

    public final double readDouble()
    {
        return nativeReadDouble(this.mNativePtr);
    }

    public final void readDoubleArray(double[] paramArrayOfDouble)
    {
        int i = readInt();
        if (i == paramArrayOfDouble.length)
            for (int j = 0; j < i; j++)
                paramArrayOfDouble[j] = readDouble();
        throw new RuntimeException("bad array lengths");
    }

    public final void readException()
    {
        int i = readExceptionCode();
        if (i != 0)
            readException(i, readString());
    }

    public final void readException(int paramInt, String paramString)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Unknown exception code: " + paramInt + " msg " + paramString);
        case -1:
            throw new SecurityException(paramString);
        case -2:
            throw new BadParcelableException(paramString);
        case -3:
            throw new IllegalArgumentException(paramString);
        case -4:
            throw new NullPointerException(paramString);
        case -5:
        }
        throw new IllegalStateException(paramString);
    }

    public final int readExceptionCode()
    {
        int i = readInt();
        if (i == -128)
        {
            if (readInt() != 0)
                break label31;
            Log.e("Parcel", "Unexpected zero-sized Parcel reply header.");
        }
        while (true)
        {
            i = 0;
            return i;
            label31: StrictMode.readAndHandleBinderCallViolations(this);
        }
    }

    public final ParcelFileDescriptor readFileDescriptor()
    {
        FileDescriptor localFileDescriptor = nativeReadFileDescriptor(this.mNativePtr);
        if (localFileDescriptor != null);
        for (ParcelFileDescriptor localParcelFileDescriptor = new ParcelFileDescriptor(localFileDescriptor); ; localParcelFileDescriptor = null)
            return localParcelFileDescriptor;
    }

    public final float readFloat()
    {
        return nativeReadFloat(this.mNativePtr);
    }

    public final void readFloatArray(float[] paramArrayOfFloat)
    {
        int i = readInt();
        if (i == paramArrayOfFloat.length)
            for (int j = 0; j < i; j++)
                paramArrayOfFloat[j] = readFloat();
        throw new RuntimeException("bad array lengths");
    }

    public final HashMap readHashMap(ClassLoader paramClassLoader)
    {
        int i = readInt();
        HashMap localHashMap;
        if (i < 0)
            localHashMap = null;
        while (true)
        {
            return localHashMap;
            localHashMap = new HashMap(i);
            readMapInternal(localHashMap, i, paramClassLoader);
        }
    }

    public final int readInt()
    {
        return nativeReadInt(this.mNativePtr);
    }

    public final void readIntArray(int[] paramArrayOfInt)
    {
        int i = readInt();
        if (i == paramArrayOfInt.length)
            for (int j = 0; j < i; j++)
                paramArrayOfInt[j] = readInt();
        throw new RuntimeException("bad array lengths");
    }

    public final void readList(List paramList, ClassLoader paramClassLoader)
    {
        readListInternal(paramList, readInt(), paramClassLoader);
    }

    public final long readLong()
    {
        return nativeReadLong(this.mNativePtr);
    }

    public final void readLongArray(long[] paramArrayOfLong)
    {
        int i = readInt();
        if (i == paramArrayOfLong.length)
            for (int j = 0; j < i; j++)
                paramArrayOfLong[j] = readLong();
        throw new RuntimeException("bad array lengths");
    }

    public final void readMap(Map paramMap, ClassLoader paramClassLoader)
    {
        readMapInternal(paramMap, readInt(), paramClassLoader);
    }

    void readMapInternal(Map paramMap, int paramInt, ClassLoader paramClassLoader)
    {
        while (paramInt > 0)
        {
            paramMap.put(readValue(paramClassLoader), readValue(paramClassLoader));
            paramInt--;
        }
    }

    public final <T extends Parcelable> T readParcelable(ClassLoader paramClassLoader)
    {
        Object localObject1 = null;
        String str = readString();
        if (str == null);
        while (true)
        {
            return localObject1;
            HashMap localHashMap2;
            Parcelable.Creator localCreator;
            synchronized (mCreators)
            {
                localHashMap2 = (HashMap)mCreators.get(paramClassLoader);
                if (localHashMap2 == null)
                {
                    localHashMap2 = new HashMap();
                    mCreators.put(paramClassLoader, localHashMap2);
                }
                localCreator = (Parcelable.Creator)localHashMap2.get(str);
                if (localCreator != null)
                    break label359;
                if (paramClassLoader != null);
            }
            try
            {
                Class localClass;
                for (Object localObject3 = Class.forName(str); ; localObject3 = localClass)
                {
                    localCreator = (Parcelable.Creator)((Class)localObject3).getField("CREATOR").get(null);
                    if (localCreator != null)
                        break;
                    throw new BadParcelableException("Parcelable protocol requires a Parcelable.Creator object called    CREATOR on class " + str);
                    localObject2 = finally;
                    throw localObject2;
                    localClass = Class.forName(str, true, paramClassLoader);
                }
            }
            catch (IllegalAccessException localIllegalAccessException)
            {
                Log.e("Parcel", "Class not found when unmarshalling: " + str + ", e: " + localIllegalAccessException);
                throw new BadParcelableException("IllegalAccessException when unmarshalling: " + str);
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
                Log.e("Parcel", "Class not found when unmarshalling: " + str + ", e: " + localClassNotFoundException);
                throw new BadParcelableException("ClassNotFoundException when unmarshalling: " + str);
            }
            catch (ClassCastException localClassCastException)
            {
                throw new BadParcelableException("Parcelable protocol requires a Parcelable.Creator object called    CREATOR on class " + str);
            }
            catch (NoSuchFieldException localNoSuchFieldException)
            {
                throw new BadParcelableException("Parcelable protocol requires a Parcelable.Creator object called    CREATOR on class " + str);
            }
            localHashMap2.put(str, localCreator);
            label359: if ((localCreator instanceof Parcelable.ClassLoaderCreator))
                localObject1 = (Parcelable)((Parcelable.ClassLoaderCreator)localCreator).createFromParcel(this, paramClassLoader);
            else
                localObject1 = (Parcelable)localCreator.createFromParcel(this);
        }
    }

    public final Parcelable[] readParcelableArray(ClassLoader paramClassLoader)
    {
        int i = readInt();
        Parcelable[] arrayOfParcelable;
        if (i < 0)
            arrayOfParcelable = null;
        while (true)
        {
            return arrayOfParcelable;
            arrayOfParcelable = new Parcelable[i];
            for (int j = 0; j < i; j++)
                arrayOfParcelable[j] = readParcelable(paramClassLoader);
        }
    }

    public final Serializable readSerializable()
    {
        String str = readString();
        Serializable localSerializable;
        if (str == null)
            localSerializable = null;
        while (true)
        {
            return localSerializable;
            ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(createByteArray());
            try
            {
                localSerializable = (Serializable)new ObjectInputStream(localByteArrayInputStream).readObject();
            }
            catch (IOException localIOException)
            {
                throw new RuntimeException("Parcelable encountered IOException reading a Serializable object (name = " + str + ")", localIOException);
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
                throw new RuntimeException("Parcelable encounteredClassNotFoundException reading a Serializable object (name = " + str + ")", localClassNotFoundException);
            }
        }
    }

    public final SparseArray readSparseArray(ClassLoader paramClassLoader)
    {
        int i = readInt();
        SparseArray localSparseArray;
        if (i < 0)
            localSparseArray = null;
        while (true)
        {
            return localSparseArray;
            localSparseArray = new SparseArray(i);
            readSparseArrayInternal(localSparseArray, i, paramClassLoader);
        }
    }

    public final SparseBooleanArray readSparseBooleanArray()
    {
        int i = readInt();
        SparseBooleanArray localSparseBooleanArray;
        if (i < 0)
            localSparseBooleanArray = null;
        while (true)
        {
            return localSparseBooleanArray;
            localSparseBooleanArray = new SparseBooleanArray(i);
            readSparseBooleanArrayInternal(localSparseBooleanArray, i);
        }
    }

    public final String readString()
    {
        return nativeReadString(this.mNativePtr);
    }

    public final void readStringArray(String[] paramArrayOfString)
    {
        int i = readInt();
        if (i == paramArrayOfString.length)
            for (int j = 0; j < i; j++)
                paramArrayOfString[j] = readString();
        throw new RuntimeException("bad array lengths");
    }

    public final String[] readStringArray()
    {
        String[] arrayOfString = null;
        int i = readInt();
        if (i >= 0)
        {
            arrayOfString = new String[i];
            for (int j = 0; j < i; j++)
                arrayOfString[j] = readString();
        }
        return arrayOfString;
    }

    public final void readStringList(List<String> paramList)
    {
        int i = paramList.size();
        int j = readInt();
        for (int k = 0; (k < i) && (k < j); k++)
            paramList.set(k, readString());
        while (k < j)
        {
            paramList.add(readString());
            k++;
        }
        while (k < i)
        {
            paramList.remove(j);
            k++;
        }
    }

    public final IBinder readStrongBinder()
    {
        return nativeReadStrongBinder(this.mNativePtr);
    }

    public final <T> void readTypedArray(T[] paramArrayOfT, Parcelable.Creator<T> paramCreator)
    {
        int i = readInt();
        if (i == paramArrayOfT.length)
        {
            int j = 0;
            if (j < i)
            {
                if (readInt() != 0)
                    paramArrayOfT[j] = paramCreator.createFromParcel(this);
                while (true)
                {
                    j++;
                    break;
                    paramArrayOfT[j] = null;
                }
            }
        }
        else
        {
            throw new RuntimeException("bad array lengths");
        }
    }

    @Deprecated
    public final <T> T[] readTypedArray(Parcelable.Creator<T> paramCreator)
    {
        return createTypedArray(paramCreator);
    }

    public final <T> void readTypedList(List<T> paramList, Parcelable.Creator<T> paramCreator)
    {
        int i = paramList.size();
        int j = readInt();
        int k = 0;
        if ((k < i) && (k < j))
        {
            if (readInt() != 0)
                paramList.set(k, paramCreator.createFromParcel(this));
            while (true)
            {
                k++;
                break;
                paramList.set(k, null);
            }
        }
        if (k < j)
        {
            if (readInt() != 0)
                paramList.add(paramCreator.createFromParcel(this));
            while (true)
            {
                k++;
                break;
                paramList.add(null);
            }
        }
        while (k < i)
        {
            paramList.remove(j);
            k++;
        }
    }

    public final Object readValue(ClassLoader paramClassLoader)
    {
        int i = 1;
        int k = readInt();
        Object localObject;
        switch (k)
        {
        default:
            int m = -4 + dataPosition();
            throw new RuntimeException("Parcel " + this + ": Unmarshalling unknown type code " + k + " at offset " + m);
        case -1:
            localObject = null;
        case 0:
        case 1:
        case 2:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 23:
        case 13:
        case 14:
        case 24:
        case 15:
        case 17:
        case 18:
        case 19:
        case 20:
        case 21:
        case 16:
        case 12:
        case 22:
        case 3:
        }
        while (true)
        {
            return localObject;
            localObject = readString();
            continue;
            localObject = Integer.valueOf(readInt());
            continue;
            localObject = readHashMap(paramClassLoader);
            continue;
            localObject = readParcelable(paramClassLoader);
            continue;
            localObject = Short.valueOf((short)readInt());
            continue;
            localObject = Long.valueOf(readLong());
            continue;
            localObject = Float.valueOf(readFloat());
            continue;
            localObject = Double.valueOf(readDouble());
            continue;
            if (readInt() == i);
            while (true)
            {
                localObject = Boolean.valueOf(i);
                break;
                int j = 0;
            }
            localObject = readCharSequence();
            continue;
            localObject = readArrayList(paramClassLoader);
            continue;
            localObject = createBooleanArray();
            continue;
            localObject = createByteArray();
            continue;
            localObject = readStringArray();
            continue;
            localObject = readCharSequenceArray();
            continue;
            localObject = readStrongBinder();
            continue;
            localObject = readArray(paramClassLoader);
            continue;
            localObject = createIntArray();
            continue;
            localObject = createLongArray();
            continue;
            localObject = Byte.valueOf(readByte());
            continue;
            localObject = readSerializable();
            continue;
            localObject = readParcelableArray(paramClassLoader);
            continue;
            localObject = readSparseArray(paramClassLoader);
            continue;
            localObject = readSparseBooleanArray();
            continue;
            localObject = readBundle(paramClassLoader);
        }
    }

    public final void recycle()
    {
        freeBuffer();
        Parcel[] arrayOfParcel;
        if (this.mOwnsNativeParcelObject)
            arrayOfParcel = sOwnedPool;
        while (true)
        {
            int i = 0;
            label19: if (i < 6);
            try
            {
                if (arrayOfParcel[i] == null)
                    arrayOfParcel[i] = this;
                while (true)
                {
                    return;
                    this.mNativePtr = 0;
                    arrayOfParcel = sHolderPool;
                    break;
                    i++;
                    break label19;
                }
            }
            finally
            {
            }
        }
    }

    public final void restoreAllowFds(boolean paramBoolean)
    {
        nativeRestoreAllowFds(this.mNativePtr, paramBoolean);
    }

    public final void setDataCapacity(int paramInt)
    {
        nativeSetDataCapacity(this.mNativePtr, paramInt);
    }

    public final void setDataPosition(int paramInt)
    {
        nativeSetDataPosition(this.mNativePtr, paramInt);
    }

    public final void setDataSize(int paramInt)
    {
        nativeSetDataSize(this.mNativePtr, paramInt);
    }

    public final void unmarshall(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        nativeUnmarshall(this.mNativePtr, paramArrayOfByte, paramInt1, paramInt2);
    }

    public final void writeArray(Object[] paramArrayOfObject)
    {
        if (paramArrayOfObject == null)
            writeInt(-1);
        while (true)
        {
            return;
            int i = paramArrayOfObject.length;
            int j = 0;
            writeInt(i);
            while (j < i)
            {
                writeValue(paramArrayOfObject[j]);
                j++;
            }
        }
    }

    public final void writeBinderArray(IBinder[] paramArrayOfIBinder)
    {
        if (paramArrayOfIBinder != null)
        {
            int i = paramArrayOfIBinder.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeStrongBinder(paramArrayOfIBinder[j]);
        }
        writeInt(-1);
    }

    public final void writeBinderList(List<IBinder> paramList)
    {
        if (paramList == null)
            writeInt(-1);
        while (true)
        {
            return;
            int i = paramList.size();
            int j = 0;
            writeInt(i);
            while (j < i)
            {
                writeStrongBinder((IBinder)paramList.get(j));
                j++;
            }
        }
    }

    public final void writeBooleanArray(boolean[] paramArrayOfBoolean)
    {
        if (paramArrayOfBoolean != null)
        {
            int i = paramArrayOfBoolean.length;
            writeInt(i);
            int j = 0;
            if (j < i)
            {
                if (paramArrayOfBoolean[j] != 0);
                for (int k = 1; ; k = 0)
                {
                    writeInt(k);
                    j++;
                    break;
                }
            }
        }
        else
        {
            writeInt(-1);
        }
    }

    public final void writeBundle(Bundle paramBundle)
    {
        if (paramBundle == null)
            writeInt(-1);
        while (true)
        {
            return;
            paramBundle.writeToParcel(this, 0);
        }
    }

    public final void writeByte(byte paramByte)
    {
        writeInt(paramByte);
    }

    public final void writeByteArray(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte != null);
        for (int i = paramArrayOfByte.length; ; i = 0)
        {
            writeByteArray(paramArrayOfByte, 0, i);
            return;
        }
    }

    public final void writeByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        if (paramArrayOfByte == null)
            writeInt(-1);
        while (true)
        {
            return;
            Arrays.checkOffsetAndCount(paramArrayOfByte.length, paramInt1, paramInt2);
            nativeWriteByteArray(this.mNativePtr, paramArrayOfByte, paramInt1, paramInt2);
        }
    }

    public final void writeCharArray(char[] paramArrayOfChar)
    {
        if (paramArrayOfChar != null)
        {
            int i = paramArrayOfChar.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeInt(paramArrayOfChar[j]);
        }
        writeInt(-1);
    }

    public final void writeCharSequence(CharSequence paramCharSequence)
    {
        TextUtils.writeToParcel(paramCharSequence, this, 0);
    }

    public final void writeCharSequenceArray(CharSequence[] paramArrayOfCharSequence)
    {
        if (paramArrayOfCharSequence != null)
        {
            int i = paramArrayOfCharSequence.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeCharSequence(paramArrayOfCharSequence[j]);
        }
        writeInt(-1);
    }

    public final void writeDouble(double paramDouble)
    {
        nativeWriteDouble(this.mNativePtr, paramDouble);
    }

    public final void writeDoubleArray(double[] paramArrayOfDouble)
    {
        if (paramArrayOfDouble != null)
        {
            int i = paramArrayOfDouble.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeDouble(paramArrayOfDouble[j]);
        }
        writeInt(-1);
    }

    public final void writeException(Exception paramException)
    {
        int i = 0;
        if ((paramException instanceof SecurityException))
            i = -1;
        while (true)
        {
            writeInt(i);
            StrictMode.clearGatheredViolations();
            if (i != 0)
                break label97;
            if (!(paramException instanceof RuntimeException))
                break;
            throw ((RuntimeException)paramException);
            if ((paramException instanceof BadParcelableException))
                i = -2;
            else if ((paramException instanceof IllegalArgumentException))
                i = -3;
            else if ((paramException instanceof NullPointerException))
                i = -4;
            else if ((paramException instanceof IllegalStateException))
                i = -5;
        }
        throw new RuntimeException(paramException);
        label97: writeString(paramException.getMessage());
    }

    public final void writeFileDescriptor(FileDescriptor paramFileDescriptor)
    {
        nativeWriteFileDescriptor(this.mNativePtr, paramFileDescriptor);
    }

    public final void writeFloat(float paramFloat)
    {
        nativeWriteFloat(this.mNativePtr, paramFloat);
    }

    public final void writeFloatArray(float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat != null)
        {
            int i = paramArrayOfFloat.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeFloat(paramArrayOfFloat[j]);
        }
        writeInt(-1);
    }

    public final void writeInt(int paramInt)
    {
        nativeWriteInt(this.mNativePtr, paramInt);
    }

    public final void writeIntArray(int[] paramArrayOfInt)
    {
        if (paramArrayOfInt != null)
        {
            int i = paramArrayOfInt.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeInt(paramArrayOfInt[j]);
        }
        writeInt(-1);
    }

    public final void writeInterfaceToken(String paramString)
    {
        nativeWriteInterfaceToken(this.mNativePtr, paramString);
    }

    public final void writeList(List paramList)
    {
        if (paramList == null)
            writeInt(-1);
        while (true)
        {
            return;
            int i = paramList.size();
            int j = 0;
            writeInt(i);
            while (j < i)
            {
                writeValue(paramList.get(j));
                j++;
            }
        }
    }

    public final void writeLong(long paramLong)
    {
        nativeWriteLong(this.mNativePtr, paramLong);
    }

    public final void writeLongArray(long[] paramArrayOfLong)
    {
        if (paramArrayOfLong != null)
        {
            int i = paramArrayOfLong.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeLong(paramArrayOfLong[j]);
        }
        writeInt(-1);
    }

    public final void writeMap(Map paramMap)
    {
        writeMapInternal(paramMap);
    }

    void writeMapInternal(Map<String, Object> paramMap)
    {
        if (paramMap == null)
            writeInt(-1);
        while (true)
        {
            return;
            Set localSet = paramMap.entrySet();
            writeInt(localSet.size());
            Iterator localIterator = localSet.iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                writeValue(localEntry.getKey());
                writeValue(localEntry.getValue());
            }
        }
    }

    public final void writeNoException()
    {
        if (StrictMode.hasGatheredViolations())
        {
            writeInt(-128);
            int i = dataPosition();
            writeInt(0);
            StrictMode.writeGatheredViolationsToParcel(this);
            int j = dataPosition();
            setDataPosition(i);
            writeInt(j - i);
            setDataPosition(j);
        }
        while (true)
        {
            return;
            writeInt(0);
        }
    }

    public final void writeParcelable(Parcelable paramParcelable, int paramInt)
    {
        if (paramParcelable == null)
            writeString(null);
        while (true)
        {
            return;
            writeString(paramParcelable.getClass().getName());
            paramParcelable.writeToParcel(this, paramInt);
        }
    }

    public final <T extends Parcelable> void writeParcelableArray(T[] paramArrayOfT, int paramInt)
    {
        if (paramArrayOfT != null)
        {
            int i = paramArrayOfT.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeParcelable(paramArrayOfT[j], paramInt);
        }
        writeInt(-1);
    }

    public final void writeSerializable(Serializable paramSerializable)
    {
        if (paramSerializable == null)
            writeString(null);
        while (true)
        {
            return;
            String str = paramSerializable.getClass().getName();
            writeString(str);
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
            try
            {
                ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(localByteArrayOutputStream);
                localObjectOutputStream.writeObject(paramSerializable);
                localObjectOutputStream.close();
                writeByteArray(localByteArrayOutputStream.toByteArray());
            }
            catch (IOException localIOException)
            {
                throw new RuntimeException("Parcelable encountered IOException writing serializable object (name = " + str + ")", localIOException);
            }
        }
    }

    public final void writeSparseArray(SparseArray<Object> paramSparseArray)
    {
        if (paramSparseArray == null)
            writeInt(-1);
        while (true)
        {
            return;
            int i = paramSparseArray.size();
            writeInt(i);
            for (int j = 0; j < i; j++)
            {
                writeInt(paramSparseArray.keyAt(j));
                writeValue(paramSparseArray.valueAt(j));
            }
        }
    }

    public final void writeSparseBooleanArray(SparseBooleanArray paramSparseBooleanArray)
    {
        if (paramSparseBooleanArray == null)
        {
            writeInt(-1);
            return;
        }
        int i = paramSparseBooleanArray.size();
        writeInt(i);
        int j = 0;
        label23: if (j < i)
        {
            writeInt(paramSparseBooleanArray.keyAt(j));
            if (!paramSparseBooleanArray.valueAt(j))
                break label61;
        }
        label61: for (int k = 1; ; k = 0)
        {
            writeByte((byte)k);
            j++;
            break label23;
            break;
        }
    }

    public final void writeString(String paramString)
    {
        nativeWriteString(this.mNativePtr, paramString);
    }

    public final void writeStringArray(String[] paramArrayOfString)
    {
        if (paramArrayOfString != null)
        {
            int i = paramArrayOfString.length;
            writeInt(i);
            for (int j = 0; j < i; j++)
                writeString(paramArrayOfString[j]);
        }
        writeInt(-1);
    }

    public final void writeStringList(List<String> paramList)
    {
        if (paramList == null)
            writeInt(-1);
        while (true)
        {
            return;
            int i = paramList.size();
            int j = 0;
            writeInt(i);
            while (j < i)
            {
                writeString((String)paramList.get(j));
                j++;
            }
        }
    }

    public final void writeStrongBinder(IBinder paramIBinder)
    {
        nativeWriteStrongBinder(this.mNativePtr, paramIBinder);
    }

    public final void writeStrongInterface(IInterface paramIInterface)
    {
        if (paramIInterface == null);
        for (IBinder localIBinder = null; ; localIBinder = paramIInterface.asBinder())
        {
            writeStrongBinder(localIBinder);
            return;
        }
    }

    public final <T extends Parcelable> void writeTypedArray(T[] paramArrayOfT, int paramInt)
    {
        if (paramArrayOfT != null)
        {
            int i = paramArrayOfT.length;
            writeInt(i);
            int j = 0;
            if (j < i)
            {
                T ? = paramArrayOfT[j];
                if (? != null)
                {
                    writeInt(1);
                    ?.writeToParcel(this, paramInt);
                }
                while (true)
                {
                    j++;
                    break;
                    writeInt(0);
                }
            }
        }
        else
        {
            writeInt(-1);
        }
    }

    public final <T extends Parcelable> void writeTypedList(List<T> paramList)
    {
        if (paramList == null)
        {
            writeInt(-1);
            return;
        }
        int i = paramList.size();
        int j = 0;
        writeInt(i);
        label25: if (j < i)
        {
            Parcelable localParcelable = (Parcelable)paramList.get(j);
            if (localParcelable == null)
                break label67;
            writeInt(1);
            localParcelable.writeToParcel(this, 0);
        }
        while (true)
        {
            j++;
            break label25;
            break;
            label67: writeInt(0);
        }
    }

    public final void writeValue(Object paramObject)
    {
        int i = 1;
        if (paramObject == null)
            writeInt(-1);
        while (true)
        {
            return;
            if ((paramObject instanceof String))
            {
                writeInt(0);
                writeString((String)paramObject);
            }
            else if ((paramObject instanceof Integer))
            {
                writeInt(i);
                writeInt(((Integer)paramObject).intValue());
            }
            else if ((paramObject instanceof Map))
            {
                writeInt(2);
                writeMap((Map)paramObject);
            }
            else if ((paramObject instanceof Bundle))
            {
                writeInt(3);
                writeBundle((Bundle)paramObject);
            }
            else if ((paramObject instanceof Parcelable))
            {
                writeInt(4);
                writeParcelable((Parcelable)paramObject, 0);
            }
            else if ((paramObject instanceof Short))
            {
                writeInt(5);
                writeInt(((Short)paramObject).intValue());
            }
            else if ((paramObject instanceof Long))
            {
                writeInt(6);
                writeLong(((Long)paramObject).longValue());
            }
            else if ((paramObject instanceof Float))
            {
                writeInt(7);
                writeFloat(((Float)paramObject).floatValue());
            }
            else if ((paramObject instanceof Double))
            {
                writeInt(8);
                writeDouble(((Double)paramObject).doubleValue());
            }
            else
            {
                if ((paramObject instanceof Boolean))
                {
                    writeInt(9);
                    if (((Boolean)paramObject).booleanValue());
                    while (true)
                    {
                        writeInt(i);
                        break;
                        i = 0;
                    }
                }
                if ((paramObject instanceof CharSequence))
                {
                    writeInt(10);
                    writeCharSequence((CharSequence)paramObject);
                }
                else if ((paramObject instanceof List))
                {
                    writeInt(11);
                    writeList((List)paramObject);
                }
                else if ((paramObject instanceof SparseArray))
                {
                    writeInt(12);
                    writeSparseArray((SparseArray)paramObject);
                }
                else if ((paramObject instanceof boolean[]))
                {
                    writeInt(23);
                    writeBooleanArray((boolean[])paramObject);
                }
                else if ((paramObject instanceof byte[]))
                {
                    writeInt(13);
                    writeByteArray((byte[])paramObject);
                }
                else if ((paramObject instanceof String[]))
                {
                    writeInt(14);
                    writeStringArray((String[])paramObject);
                }
                else if ((paramObject instanceof CharSequence[]))
                {
                    writeInt(24);
                    writeCharSequenceArray((CharSequence[])paramObject);
                }
                else if ((paramObject instanceof IBinder))
                {
                    writeInt(15);
                    writeStrongBinder((IBinder)paramObject);
                }
                else if ((paramObject instanceof Parcelable[]))
                {
                    writeInt(16);
                    writeParcelableArray((Parcelable[])paramObject, 0);
                }
                else if ((paramObject instanceof Object[]))
                {
                    writeInt(17);
                    writeArray((Object[])paramObject);
                }
                else if ((paramObject instanceof int[]))
                {
                    writeInt(18);
                    writeIntArray((int[])paramObject);
                }
                else if ((paramObject instanceof long[]))
                {
                    writeInt(19);
                    writeLongArray((long[])paramObject);
                }
                else if ((paramObject instanceof Byte))
                {
                    writeInt(20);
                    writeInt(((Byte)paramObject).byteValue());
                }
                else
                {
                    if (!(paramObject instanceof Serializable))
                        break;
                    writeInt(21);
                    writeSerializable((Serializable)paramObject);
                }
            }
        }
        throw new RuntimeException("Parcel: unable to marshal value " + paramObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.Parcel
 * JD-Core Version:        0.6.2
 */