package android.os;

import android.util.Log;
import java.io.FileDescriptor;
import java.lang.ref.WeakReference;

final class BinderProxy
    implements IBinder
{
    private int mObject;
    private int mOrgue;
    private final WeakReference mSelf = new WeakReference(this);

    private final native void destroy();

    private static final void sendDeathNotice(IBinder.DeathRecipient paramDeathRecipient)
    {
        try
        {
            paramDeathRecipient.binderDied();
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Log.w("BinderNative", "Uncaught exception from death notification", localRuntimeException);
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeFileDescriptor(paramFileDescriptor);
        localParcel1.writeStringArray(paramArrayOfString);
        try
        {
            transact(1598311760, localParcel1, localParcel2, 0);
            localParcel2.readException();
            return;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    public void dumpAsync(FileDescriptor paramFileDescriptor, String[] paramArrayOfString)
        throws RemoteException
    {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        localParcel1.writeFileDescriptor(paramFileDescriptor);
        localParcel1.writeStringArray(paramArrayOfString);
        try
        {
            transact(1598311760, localParcel1, localParcel2, 1);
            localParcel2.readException();
            return;
        }
        finally
        {
            localParcel1.recycle();
            localParcel2.recycle();
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            destroy();
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public native String getInterfaceDescriptor()
        throws RemoteException;

    public native boolean isBinderAlive();

    public native void linkToDeath(IBinder.DeathRecipient paramDeathRecipient, int paramInt)
        throws RemoteException;

    public native boolean pingBinder();

    public IInterface queryLocalInterface(String paramString)
    {
        return null;
    }

    public native boolean transact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException;

    public native boolean unlinkToDeath(IBinder.DeathRecipient paramDeathRecipient, int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.BinderProxy
 * JD-Core Version:        0.6.2
 */