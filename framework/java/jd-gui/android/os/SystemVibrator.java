package android.os;

import android.util.Log;

public class SystemVibrator extends Vibrator
{
    private static final String TAG = "Vibrator";
    private final IVibratorService mService = IVibratorService.Stub.asInterface(ServiceManager.getService("vibrator"));
    private final Binder mToken = new Binder();

    public void cancel()
    {
        if (this.mService == null);
        while (true)
        {
            return;
            try
            {
                this.mService.cancelVibrate(this.mToken);
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("Vibrator", "Failed to cancel vibration.", localRemoteException);
            }
        }
    }

    public boolean hasVibrator()
    {
        boolean bool1 = false;
        if (this.mService == null)
            Log.w("Vibrator", "Failed to vibrate; no vibrator service.");
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.hasVibrator();
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public void vibrate(long paramLong)
    {
        if (this.mService == null)
            Log.w("Vibrator", "Failed to vibrate; no vibrator service.");
        while (true)
        {
            return;
            try
            {
                this.mService.vibrate(paramLong, this.mToken);
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("Vibrator", "Failed to vibrate.", localRemoteException);
            }
        }
    }

    public void vibrate(long[] paramArrayOfLong, int paramInt)
    {
        if (this.mService == null)
            Log.w("Vibrator", "Failed to vibrate; no vibrator service.");
        while (true)
        {
            return;
            if (paramInt >= paramArrayOfLong.length)
                break;
            try
            {
                this.mService.vibratePattern(paramArrayOfLong, paramInt, this.mToken);
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("Vibrator", "Failed to vibrate.", localRemoteException);
            }
        }
        throw new ArrayIndexOutOfBoundsException();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.os.SystemVibrator
 * JD-Core Version:        0.6.2
 */