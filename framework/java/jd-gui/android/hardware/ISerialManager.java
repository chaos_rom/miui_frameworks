package android.hardware;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ISerialManager extends IInterface
{
    public abstract String[] getSerialPorts()
        throws RemoteException;

    public abstract ParcelFileDescriptor openSerialPort(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISerialManager
    {
        private static final String DESCRIPTOR = "android.hardware.ISerialManager";
        static final int TRANSACTION_getSerialPorts = 1;
        static final int TRANSACTION_openSerialPort = 2;

        public Stub()
        {
            attachInterface(this, "android.hardware.ISerialManager");
        }

        public static ISerialManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.hardware.ISerialManager");
                if ((localIInterface != null) && ((localIInterface instanceof ISerialManager)))
                    localObject = (ISerialManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.hardware.ISerialManager");
                continue;
                paramParcel1.enforceInterface("android.hardware.ISerialManager");
                String[] arrayOfString = getSerialPorts();
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString);
                continue;
                paramParcel1.enforceInterface("android.hardware.ISerialManager");
                ParcelFileDescriptor localParcelFileDescriptor = openSerialPort(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localParcelFileDescriptor != null)
                {
                    paramParcel2.writeInt(i);
                    localParcelFileDescriptor.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                }
            }
        }

        private static class Proxy
            implements ISerialManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.hardware.ISerialManager";
            }

            public String[] getSerialPorts()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.ISerialManager");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ParcelFileDescriptor openSerialPort(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.ISerialManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
                        return localParcelFileDescriptor;
                    }
                    ParcelFileDescriptor localParcelFileDescriptor = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.ISerialManager
 * JD-Core Version:        0.6.2
 */