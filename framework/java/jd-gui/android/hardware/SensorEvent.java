package android.hardware;

public class SensorEvent
{
    public int accuracy;
    public Sensor sensor;
    public long timestamp;
    public final float[] values;

    SensorEvent(int paramInt)
    {
        this.values = new float[paramInt];
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.SensorEvent
 * JD-Core Version:        0.6.2
 */