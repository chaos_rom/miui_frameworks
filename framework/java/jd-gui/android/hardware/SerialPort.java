package android.hardware;

import android.os.ParcelFileDescriptor;
import java.io.FileDescriptor;
import java.io.IOException;
import java.nio.ByteBuffer;

public class SerialPort
{
    private static final String TAG = "SerialPort";
    private ParcelFileDescriptor mFileDescriptor;
    private final String mName;
    private int mNativeContext;

    public SerialPort(String paramString)
    {
        this.mName = paramString;
    }

    private native void native_close();

    private native void native_open(FileDescriptor paramFileDescriptor, int paramInt)
        throws IOException;

    private native int native_read_array(byte[] paramArrayOfByte, int paramInt)
        throws IOException;

    private native int native_read_direct(ByteBuffer paramByteBuffer, int paramInt)
        throws IOException;

    private native void native_send_break();

    private native void native_write_array(byte[] paramArrayOfByte, int paramInt)
        throws IOException;

    private native void native_write_direct(ByteBuffer paramByteBuffer, int paramInt)
        throws IOException;

    public void close()
        throws IOException
    {
        if (this.mFileDescriptor != null)
        {
            this.mFileDescriptor.close();
            this.mFileDescriptor = null;
        }
        native_close();
    }

    public String getName()
    {
        return this.mName;
    }

    public void open(ParcelFileDescriptor paramParcelFileDescriptor, int paramInt)
        throws IOException
    {
        native_open(paramParcelFileDescriptor.getFileDescriptor(), paramInt);
        this.mFileDescriptor = paramParcelFileDescriptor;
    }

    public int read(ByteBuffer paramByteBuffer)
        throws IOException
    {
        if (paramByteBuffer.isDirect());
        for (int i = native_read_direct(paramByteBuffer, paramByteBuffer.remaining()); ; i = native_read_array(paramByteBuffer.array(), paramByteBuffer.remaining()))
        {
            return i;
            if (!paramByteBuffer.hasArray())
                break;
        }
        throw new IllegalArgumentException("buffer is not direct and has no array");
    }

    public void sendBreak()
    {
        native_send_break();
    }

    public void write(ByteBuffer paramByteBuffer, int paramInt)
        throws IOException
    {
        if (paramByteBuffer.isDirect())
            native_write_direct(paramByteBuffer, paramInt);
        while (true)
        {
            return;
            if (!paramByteBuffer.hasArray())
                break;
            native_write_array(paramByteBuffer.array(), paramInt);
        }
        throw new IllegalArgumentException("buffer is not direct and has no array");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.SerialPort
 * JD-Core Version:        0.6.2
 */