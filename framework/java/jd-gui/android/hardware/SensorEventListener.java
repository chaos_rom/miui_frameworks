package android.hardware;

public abstract interface SensorEventListener
{
    public abstract void onAccuracyChanged(Sensor paramSensor, int paramInt);

    public abstract void onSensorChanged(SensorEvent paramSensorEvent);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.SensorEventListener
 * JD-Core Version:        0.6.2
 */