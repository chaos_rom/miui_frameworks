package android.hardware.input;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.view.InputDevice;
import android.view.InputEvent;

public abstract interface IInputManager extends IInterface
{
    public abstract void addKeyboardLayoutForInputDevice(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void cancelVibrate(int paramInt, IBinder paramIBinder)
        throws RemoteException;

    public abstract String getCurrentKeyboardLayoutForInputDevice(String paramString)
        throws RemoteException;

    public abstract InputDevice getInputDevice(int paramInt)
        throws RemoteException;

    public abstract int[] getInputDeviceIds()
        throws RemoteException;

    public abstract KeyboardLayout getKeyboardLayout(String paramString)
        throws RemoteException;

    public abstract KeyboardLayout[] getKeyboardLayouts()
        throws RemoteException;

    public abstract String[] getKeyboardLayoutsForInputDevice(String paramString)
        throws RemoteException;

    public abstract boolean hasKeys(int paramInt1, int paramInt2, int[] paramArrayOfInt, boolean[] paramArrayOfBoolean)
        throws RemoteException;

    public abstract boolean injectInputEvent(InputEvent paramInputEvent, int paramInt)
        throws RemoteException;

    public abstract void registerInputDevicesChangedListener(IInputDevicesChangedListener paramIInputDevicesChangedListener)
        throws RemoteException;

    public abstract void removeKeyboardLayoutForInputDevice(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setCurrentKeyboardLayoutForInputDevice(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void tryPointerSpeed(int paramInt)
        throws RemoteException;

    public abstract void vibrate(int paramInt1, long[] paramArrayOfLong, int paramInt2, IBinder paramIBinder)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputManager
    {
        private static final String DESCRIPTOR = "android.hardware.input.IInputManager";
        static final int TRANSACTION_addKeyboardLayoutForInputDevice = 11;
        static final int TRANSACTION_cancelVibrate = 15;
        static final int TRANSACTION_getCurrentKeyboardLayoutForInputDevice = 8;
        static final int TRANSACTION_getInputDevice = 1;
        static final int TRANSACTION_getInputDeviceIds = 2;
        static final int TRANSACTION_getKeyboardLayout = 7;
        static final int TRANSACTION_getKeyboardLayouts = 6;
        static final int TRANSACTION_getKeyboardLayoutsForInputDevice = 10;
        static final int TRANSACTION_hasKeys = 3;
        static final int TRANSACTION_injectInputEvent = 5;
        static final int TRANSACTION_registerInputDevicesChangedListener = 13;
        static final int TRANSACTION_removeKeyboardLayoutForInputDevice = 12;
        static final int TRANSACTION_setCurrentKeyboardLayoutForInputDevice = 9;
        static final int TRANSACTION_tryPointerSpeed = 4;
        static final int TRANSACTION_vibrate = 14;

        public Stub()
        {
            attachInterface(this, "android.hardware.input.IInputManager");
        }

        public static IInputManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.hardware.input.IInputManager");
                if ((localIInterface != null) && ((localIInterface instanceof IInputManager)))
                    localObject = (IInputManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.hardware.input.IInputManager");
                continue;
                paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                InputDevice localInputDevice = getInputDevice(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localInputDevice != null)
                {
                    paramParcel2.writeInt(j);
                    localInputDevice.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                    int[] arrayOfInt2 = getInputDeviceIds();
                    paramParcel2.writeNoException();
                    paramParcel2.writeIntArray(arrayOfInt2);
                    continue;
                    paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                    int k = paramParcel1.readInt();
                    int m = paramParcel1.readInt();
                    int[] arrayOfInt1 = paramParcel1.createIntArray();
                    int n = paramParcel1.readInt();
                    if (n < 0);
                    for (boolean[] arrayOfBoolean = null; ; arrayOfBoolean = new boolean[n])
                    {
                        boolean bool2 = hasKeys(k, m, arrayOfInt1, arrayOfBoolean);
                        paramParcel2.writeNoException();
                        if (bool2)
                            i = j;
                        paramParcel2.writeInt(i);
                        paramParcel2.writeBooleanArray(arrayOfBoolean);
                        break;
                    }
                    paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                    tryPointerSpeed(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                    if (paramParcel1.readInt() != 0);
                    for (InputEvent localInputEvent = (InputEvent)InputEvent.CREATOR.createFromParcel(paramParcel1); ; localInputEvent = null)
                    {
                        boolean bool1 = injectInputEvent(localInputEvent, paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (bool1)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                    KeyboardLayout[] arrayOfKeyboardLayout = getKeyboardLayouts();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedArray(arrayOfKeyboardLayout, j);
                    continue;
                    paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                    KeyboardLayout localKeyboardLayout = getKeyboardLayout(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (localKeyboardLayout != null)
                    {
                        paramParcel2.writeInt(j);
                        localKeyboardLayout.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                        String str = getCurrentKeyboardLayoutForInputDevice(paramParcel1.readString());
                        paramParcel2.writeNoException();
                        paramParcel2.writeString(str);
                        continue;
                        paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                        setCurrentKeyboardLayoutForInputDevice(paramParcel1.readString(), paramParcel1.readString());
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                        String[] arrayOfString = getKeyboardLayoutsForInputDevice(paramParcel1.readString());
                        paramParcel2.writeNoException();
                        paramParcel2.writeStringArray(arrayOfString);
                        continue;
                        paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                        addKeyboardLayoutForInputDevice(paramParcel1.readString(), paramParcel1.readString());
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                        removeKeyboardLayoutForInputDevice(paramParcel1.readString(), paramParcel1.readString());
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                        registerInputDevicesChangedListener(IInputDevicesChangedListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                        vibrate(paramParcel1.readInt(), paramParcel1.createLongArray(), paramParcel1.readInt(), paramParcel1.readStrongBinder());
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.hardware.input.IInputManager");
                        cancelVibrate(paramParcel1.readInt(), paramParcel1.readStrongBinder());
                        paramParcel2.writeNoException();
                    }
                }
            }
        }

        private static class Proxy
            implements IInputManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addKeyboardLayoutForInputDevice(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancelVibrate(int paramInt, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getCurrentKeyboardLayoutForInputDevice(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public InputDevice getInputDevice(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localInputDevice = (InputDevice)InputDevice.CREATOR.createFromParcel(localParcel2);
                        return localInputDevice;
                    }
                    InputDevice localInputDevice = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int[] getInputDeviceIds()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int[] arrayOfInt = localParcel2.createIntArray();
                    return arrayOfInt;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.hardware.input.IInputManager";
            }

            public KeyboardLayout getKeyboardLayout(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localKeyboardLayout = (KeyboardLayout)KeyboardLayout.CREATOR.createFromParcel(localParcel2);
                        return localKeyboardLayout;
                    }
                    KeyboardLayout localKeyboardLayout = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public KeyboardLayout[] getKeyboardLayouts()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    KeyboardLayout[] arrayOfKeyboardLayout = (KeyboardLayout[])localParcel2.createTypedArray(KeyboardLayout.CREATOR);
                    return arrayOfKeyboardLayout;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getKeyboardLayoutsForInputDevice(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasKeys(int paramInt1, int paramInt2, int[] paramArrayOfInt, boolean[] paramArrayOfBoolean)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeIntArray(paramArrayOfInt);
                    if (paramArrayOfBoolean == null)
                        localParcel1.writeInt(-1);
                    while (true)
                    {
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        if (localParcel2.readInt() != 0)
                            bool = true;
                        localParcel2.readBooleanArray(paramArrayOfBoolean);
                        return bool;
                        localParcel1.writeInt(paramArrayOfBoolean.length);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean injectInputEvent(InputEvent paramInputEvent, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                        if (paramInputEvent != null)
                        {
                            localParcel1.writeInt(1);
                            paramInputEvent.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(5, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void registerInputDevicesChangedListener(IInputDevicesChangedListener paramIInputDevicesChangedListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    if (paramIInputDevicesChangedListener != null)
                    {
                        localIBinder = paramIInputDevicesChangedListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeKeyboardLayoutForInputDevice(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setCurrentKeyboardLayoutForInputDevice(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void tryPointerSpeed(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void vibrate(int paramInt1, long[] paramArrayOfLong, int paramInt2, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.input.IInputManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeLongArray(paramArrayOfLong);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.input.IInputManager
 * JD-Core Version:        0.6.2
 */