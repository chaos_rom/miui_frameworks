package android.hardware.input;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class KeyboardLayout
    implements Parcelable, Comparable<KeyboardLayout>
{
    public static final Parcelable.Creator<KeyboardLayout> CREATOR = new Parcelable.Creator()
    {
        public KeyboardLayout createFromParcel(Parcel paramAnonymousParcel)
        {
            return new KeyboardLayout(paramAnonymousParcel, null);
        }

        public KeyboardLayout[] newArray(int paramAnonymousInt)
        {
            return new KeyboardLayout[paramAnonymousInt];
        }
    };
    private final String mCollection;
    private final String mDescriptor;
    private final String mLabel;

    private KeyboardLayout(Parcel paramParcel)
    {
        this.mDescriptor = paramParcel.readString();
        this.mLabel = paramParcel.readString();
        this.mCollection = paramParcel.readString();
    }

    public KeyboardLayout(String paramString1, String paramString2, String paramString3)
    {
        this.mDescriptor = paramString1;
        this.mLabel = paramString2;
        this.mCollection = paramString3;
    }

    public int compareTo(KeyboardLayout paramKeyboardLayout)
    {
        int i = this.mLabel.compareToIgnoreCase(paramKeyboardLayout.mLabel);
        if (i == 0)
            i = this.mCollection.compareToIgnoreCase(paramKeyboardLayout.mCollection);
        return i;
    }

    public int describeContents()
    {
        return 0;
    }

    public String getCollection()
    {
        return this.mCollection;
    }

    public String getDescriptor()
    {
        return this.mDescriptor;
    }

    public String getLabel()
    {
        return this.mLabel;
    }

    public String toString()
    {
        if (this.mCollection.isEmpty());
        for (String str = this.mLabel; ; str = this.mLabel + " - " + this.mCollection)
            return str;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mDescriptor);
        paramParcel.writeString(this.mLabel);
        paramParcel.writeString(this.mCollection);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.input.KeyboardLayout
 * JD-Core Version:        0.6.2
 */