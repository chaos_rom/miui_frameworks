package android.hardware.input;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IInputDevicesChangedListener extends IInterface
{
    public abstract void onInputDevicesChanged(int[] paramArrayOfInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputDevicesChangedListener
    {
        private static final String DESCRIPTOR = "android.hardware.input.IInputDevicesChangedListener";
        static final int TRANSACTION_onInputDevicesChanged = 1;

        public Stub()
        {
            attachInterface(this, "android.hardware.input.IInputDevicesChangedListener");
        }

        public static IInputDevicesChangedListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.hardware.input.IInputDevicesChangedListener");
                if ((localIInterface != null) && ((localIInterface instanceof IInputDevicesChangedListener)))
                    localObject = (IInputDevicesChangedListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.hardware.input.IInputDevicesChangedListener");
                continue;
                paramParcel1.enforceInterface("android.hardware.input.IInputDevicesChangedListener");
                onInputDevicesChanged(paramParcel1.createIntArray());
            }
        }

        private static class Proxy
            implements IInputDevicesChangedListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.hardware.input.IInputDevicesChangedListener";
            }

            public void onInputDevicesChanged(int[] paramArrayOfInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.hardware.input.IInputDevicesChangedListener");
                    localParcel.writeIntArray(paramArrayOfInt);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.input.IInputDevicesChangedListener
 * JD-Core Version:        0.6.2
 */