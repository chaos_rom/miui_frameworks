package android.hardware.input;

import android.content.Context;
import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.Vibrator;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.util.Log;
import android.util.SparseArray;
import android.view.InputDevice;
import android.view.InputEvent;
import com.android.internal.util.ArrayUtils;
import java.util.ArrayList;

public final class InputManager
{
    public static final String ACTION_QUERY_KEYBOARD_LAYOUTS = "android.hardware.input.action.QUERY_KEYBOARD_LAYOUTS";
    private static final boolean DEBUG = false;
    public static final int DEFAULT_POINTER_SPEED = 0;
    public static final int INJECT_INPUT_EVENT_MODE_ASYNC = 0;
    public static final int INJECT_INPUT_EVENT_MODE_WAIT_FOR_FINISH = 2;
    public static final int INJECT_INPUT_EVENT_MODE_WAIT_FOR_RESULT = 1;
    public static final int MAX_POINTER_SPEED = 7;
    public static final String META_DATA_KEYBOARD_LAYOUTS = "android.hardware.input.metadata.KEYBOARD_LAYOUTS";
    public static final int MIN_POINTER_SPEED = -7;
    private static final int MSG_DEVICE_ADDED = 1;
    private static final int MSG_DEVICE_CHANGED = 3;
    private static final int MSG_DEVICE_REMOVED = 2;
    private static final String TAG = "InputManager";
    private static InputManager sInstance;
    private final IInputManager mIm;
    private final ArrayList<InputDeviceListenerDelegate> mInputDeviceListeners = new ArrayList();
    private SparseArray<InputDevice> mInputDevices;
    private InputDevicesChangedListener mInputDevicesChangedListener;
    private final Object mInputDevicesLock = new Object();

    private InputManager(IInputManager paramIInputManager)
    {
        this.mIm = paramIInputManager;
    }

    private static boolean containsDeviceId(int[] paramArrayOfInt, int paramInt)
    {
        int i = 0;
        if (i < paramArrayOfInt.length)
            if (paramArrayOfInt[i] != paramInt);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i += 2;
            break;
        }
    }

    private int findInputDeviceListenerLocked(InputDeviceListener paramInputDeviceListener)
    {
        int i = this.mInputDeviceListeners.size();
        int j = 0;
        if (j < i)
            if (((InputDeviceListenerDelegate)this.mInputDeviceListeners.get(j)).mListener != paramInputDeviceListener);
        while (true)
        {
            return j;
            j++;
            break;
            j = -1;
        }
    }

    public static InputManager getInstance()
    {
        try
        {
            if (sInstance == null)
                sInstance = new InputManager(IInputManager.Stub.asInterface(ServiceManager.getService("input")));
            InputManager localInputManager = sInstance;
            return localInputManager;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    private void onInputDevicesChanged(int[] paramArrayOfInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 67	android/hardware/input/InputManager:mInputDevicesLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     11: invokevirtual 123	android/util/SparseArray:size	()I
        //     14: istore 4
        //     16: iinc 4 255
        //     19: iload 4
        //     21: ifle +47 -> 68
        //     24: aload_0
        //     25: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     28: iload 4
        //     30: invokevirtual 127	android/util/SparseArray:keyAt	(I)I
        //     33: istore 10
        //     35: aload_1
        //     36: iload 10
        //     38: invokestatic 129	android/hardware/input/InputManager:containsDeviceId	([II)Z
        //     41: ifne -25 -> 16
        //     44: aload_0
        //     45: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     48: iload 4
        //     50: invokevirtual 133	android/util/SparseArray:removeAt	(I)V
        //     53: aload_0
        //     54: iconst_2
        //     55: iload 10
        //     57: invokespecial 137	android/hardware/input/InputManager:sendMessageToInputDeviceListenersLocked	(II)V
        //     60: goto -44 -> 16
        //     63: astore_3
        //     64: aload_2
        //     65: monitorexit
        //     66: aload_3
        //     67: athrow
        //     68: iconst_0
        //     69: istore 5
        //     71: iload 5
        //     73: aload_1
        //     74: arraylength
        //     75: if_icmpge +102 -> 177
        //     78: aload_1
        //     79: iload 5
        //     81: iaload
        //     82: istore 6
        //     84: aload_0
        //     85: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     88: iload 6
        //     90: invokevirtual 140	android/util/SparseArray:indexOfKey	(I)I
        //     93: istore 7
        //     95: iload 7
        //     97: iflt +60 -> 157
        //     100: aload_0
        //     101: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     104: iload 7
        //     106: invokevirtual 143	android/util/SparseArray:valueAt	(I)Ljava/lang/Object;
        //     109: checkcast 145	android/view/InputDevice
        //     112: astore 8
        //     114: aload 8
        //     116: ifnull +64 -> 180
        //     119: aload_1
        //     120: iload 5
        //     122: iconst_1
        //     123: iadd
        //     124: iaload
        //     125: istore 9
        //     127: aload 8
        //     129: invokevirtual 148	android/view/InputDevice:getGeneration	()I
        //     132: iload 9
        //     134: if_icmpeq +46 -> 180
        //     137: aload_0
        //     138: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     141: iload 7
        //     143: aconst_null
        //     144: invokevirtual 152	android/util/SparseArray:setValueAt	(ILjava/lang/Object;)V
        //     147: aload_0
        //     148: iconst_3
        //     149: iload 6
        //     151: invokespecial 137	android/hardware/input/InputManager:sendMessageToInputDeviceListenersLocked	(II)V
        //     154: goto +26 -> 180
        //     157: aload_0
        //     158: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     161: iload 6
        //     163: aconst_null
        //     164: invokevirtual 155	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     167: aload_0
        //     168: iconst_1
        //     169: iload 6
        //     171: invokespecial 137	android/hardware/input/InputManager:sendMessageToInputDeviceListenersLocked	(II)V
        //     174: goto +6 -> 180
        //     177: aload_2
        //     178: monitorexit
        //     179: return
        //     180: iinc 5 2
        //     183: goto -112 -> 71
        //
        // Exception table:
        //     from	to	target	type
        //     7	66	63	finally
        //     71	179	63	finally
    }

    // ERROR //
    private void populateInputDevicesLocked()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 160	android/hardware/input/InputManager:mInputDevicesChangedListener	Landroid/hardware/input/InputManager$InputDevicesChangedListener;
        //     4: ifnonnull +28 -> 32
        //     7: new 14	android/hardware/input/InputManager$InputDevicesChangedListener
        //     10: dup
        //     11: aload_0
        //     12: aconst_null
        //     13: invokespecial 163	android/hardware/input/InputManager$InputDevicesChangedListener:<init>	(Landroid/hardware/input/InputManager;Landroid/hardware/input/InputManager$1;)V
        //     16: astore_1
        //     17: aload_0
        //     18: getfield 74	android/hardware/input/InputManager:mIm	Landroid/hardware/input/IInputManager;
        //     21: aload_1
        //     22: invokeinterface 169 2 0
        //     27: aload_0
        //     28: aload_1
        //     29: putfield 160	android/hardware/input/InputManager:mInputDevicesChangedListener	Landroid/hardware/input/InputManager$InputDevicesChangedListener;
        //     32: aload_0
        //     33: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     36: ifnonnull +79 -> 115
        //     39: aload_0
        //     40: getfield 74	android/hardware/input/InputManager:mIm	Landroid/hardware/input/IInputManager;
        //     43: invokeinterface 173 1 0
        //     48: astore 4
        //     50: aload_0
        //     51: new 122	android/util/SparseArray
        //     54: dup
        //     55: invokespecial 174	android/util/SparseArray:<init>	()V
        //     58: putfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     61: iconst_0
        //     62: istore 5
        //     64: iload 5
        //     66: aload 4
        //     68: arraylength
        //     69: if_icmpge +46 -> 115
        //     72: aload_0
        //     73: getfield 120	android/hardware/input/InputManager:mInputDevices	Landroid/util/SparseArray;
        //     76: aload 4
        //     78: iload 5
        //     80: iaload
        //     81: aconst_null
        //     82: invokevirtual 155	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     85: iinc 5 1
        //     88: goto -24 -> 64
        //     91: astore_2
        //     92: new 176	java/lang/RuntimeException
        //     95: dup
        //     96: ldc 178
        //     98: aload_2
        //     99: invokespecial 181	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     102: athrow
        //     103: astore_3
        //     104: new 176	java/lang/RuntimeException
        //     107: dup
        //     108: ldc 183
        //     110: aload_3
        //     111: invokespecial 181	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     114: athrow
        //     115: return
        //
        // Exception table:
        //     from	to	target	type
        //     17	27	91	android/os/RemoteException
        //     39	50	103	android/os/RemoteException
    }

    private void sendMessageToInputDeviceListenersLocked(int paramInt1, int paramInt2)
    {
        int i = this.mInputDeviceListeners.size();
        for (int j = 0; j < i; j++)
        {
            InputDeviceListenerDelegate localInputDeviceListenerDelegate = (InputDeviceListenerDelegate)this.mInputDeviceListeners.get(j);
            localInputDeviceListenerDelegate.sendMessage(localInputDeviceListenerDelegate.obtainMessage(paramInt1, paramInt2, 0));
        }
    }

    public void addKeyboardLayoutForInputDevice(String paramString1, String paramString2)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("inputDeviceDescriptor must not be null");
        if (paramString2 == null)
            throw new IllegalArgumentException("keyboardLayoutDescriptor must not be null");
        try
        {
            this.mIm.addKeyboardLayoutForInputDevice(paramString1, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("InputManager", "Could not add keyboard layout for input device.", localRemoteException);
        }
    }

    public boolean[] deviceHasKeys(int[] paramArrayOfInt)
    {
        boolean[] arrayOfBoolean = new boolean[paramArrayOfInt.length];
        try
        {
            this.mIm.hasKeys(-1, -256, paramArrayOfInt, arrayOfBoolean);
            label22: return arrayOfBoolean;
        }
        catch (RemoteException localRemoteException)
        {
            break label22;
        }
    }

    public String getCurrentKeyboardLayoutForInputDevice(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("inputDeviceDescriptor must not be null");
        try
        {
            String str2 = this.mIm.getCurrentKeyboardLayoutForInputDevice(paramString);
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.w("InputManager", "Could not get current keyboard layout for input device.", localRemoteException);
                String str1 = null;
            }
        }
    }

    public InputDevice getInputDevice(int paramInt)
    {
        int i;
        Object localObject3;
        synchronized (this.mInputDevicesLock)
        {
            populateInputDevicesLocked();
            i = this.mInputDevices.indexOfKey(paramInt);
            if (i < 0)
            {
                localObject3 = null;
                break label104;
            }
            localObject3 = (InputDevice)this.mInputDevices.valueAt(i);
            if (localObject3 != null);
        }
        try
        {
            InputDevice localInputDevice = this.mIm.getInputDevice(paramInt);
            localObject3 = localInputDevice;
            this.mInputDevices.setValueAt(i, localObject3);
            break label104;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException("Could not get input device information.", localRemoteException);
        }
        label104: return localObject3;
    }

    public InputDevice getInputDeviceByDescriptor(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("descriptor must not be null.");
        while (true)
        {
            int j;
            Object localObject3;
            int k;
            synchronized (this.mInputDevicesLock)
            {
                populateInputDevicesLocked();
                int i = this.mInputDevices.size();
                j = 0;
                if (j < i)
                {
                    localObject3 = (InputDevice)this.mInputDevices.valueAt(j);
                    if (localObject3 == null)
                        k = this.mInputDevices.keyAt(j);
                }
            }
            try
            {
                InputDevice localInputDevice = this.mIm.getInputDevice(k);
                localObject3 = localInputDevice;
                this.mInputDevices.setValueAt(j, localObject3);
                if (paramString.equals(((InputDevice)localObject3).getDescriptor()))
                {
                    break label132;
                    localObject3 = null;
                    break label132;
                    localObject2 = finally;
                    throw localObject2;
                    label132: return localObject3;
                }
            }
            catch (RemoteException localRemoteException)
            {
                j++;
            }
        }
    }

    public int[] getInputDeviceIds()
    {
        synchronized (this.mInputDevicesLock)
        {
            populateInputDevicesLocked();
            int i = this.mInputDevices.size();
            int[] arrayOfInt = new int[i];
            for (int j = 0; j < i; j++)
                arrayOfInt[j] = this.mInputDevices.keyAt(j);
            return arrayOfInt;
        }
    }

    public Vibrator getInputDeviceVibrator(int paramInt)
    {
        return new InputDeviceVibrator(paramInt);
    }

    public KeyboardLayout getKeyboardLayout(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("keyboardLayoutDescriptor must not be null");
        try
        {
            KeyboardLayout localKeyboardLayout2 = this.mIm.getKeyboardLayout(paramString);
            localKeyboardLayout1 = localKeyboardLayout2;
            return localKeyboardLayout1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.w("InputManager", "Could not get keyboard layout information.", localRemoteException);
                KeyboardLayout localKeyboardLayout1 = null;
            }
        }
    }

    public KeyboardLayout[] getKeyboardLayouts()
    {
        try
        {
            KeyboardLayout[] arrayOfKeyboardLayout2 = this.mIm.getKeyboardLayouts();
            arrayOfKeyboardLayout1 = arrayOfKeyboardLayout2;
            return arrayOfKeyboardLayout1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.w("InputManager", "Could not get list of keyboard layout informations.", localRemoteException);
                KeyboardLayout[] arrayOfKeyboardLayout1 = new KeyboardLayout[0];
            }
        }
    }

    public String[] getKeyboardLayoutsForInputDevice(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("inputDeviceDescriptor must not be null");
        try
        {
            String[] arrayOfString2 = this.mIm.getKeyboardLayoutsForInputDevice(paramString);
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.w("InputManager", "Could not get keyboard layouts for input device.", localRemoteException);
                String[] arrayOfString1 = (String[])ArrayUtils.emptyArray(String.class);
            }
        }
    }

    public int getPointerSpeed(Context paramContext)
    {
        int i = 0;
        try
        {
            int j = Settings.System.getInt(paramContext.getContentResolver(), "pointer_speed");
            i = j;
            label17: return i;
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label17;
        }
    }

    public boolean injectInputEvent(InputEvent paramInputEvent, int paramInt)
    {
        if (paramInputEvent == null)
            throw new IllegalArgumentException("event must not be null");
        if ((paramInt != 0) && (paramInt != 2) && (paramInt != 1))
            throw new IllegalArgumentException("mode is invalid");
        try
        {
            boolean bool2 = this.mIm.injectInputEvent(paramInputEvent, paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void registerInputDeviceListener(InputDeviceListener paramInputDeviceListener, Handler paramHandler)
    {
        if (paramInputDeviceListener == null)
            throw new IllegalArgumentException("listener must not be null");
        synchronized (this.mInputDevicesLock)
        {
            if (findInputDeviceListenerLocked(paramInputDeviceListener) < 0)
                this.mInputDeviceListeners.add(new InputDeviceListenerDelegate(paramInputDeviceListener, paramHandler));
            return;
        }
    }

    public void removeKeyboardLayoutForInputDevice(String paramString1, String paramString2)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("inputDeviceDescriptor must not be null");
        if (paramString2 == null)
            throw new IllegalArgumentException("keyboardLayoutDescriptor must not be null");
        try
        {
            this.mIm.removeKeyboardLayoutForInputDevice(paramString1, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("InputManager", "Could not remove keyboard layout for input device.", localRemoteException);
        }
    }

    public void setCurrentKeyboardLayoutForInputDevice(String paramString1, String paramString2)
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("inputDeviceDescriptor must not be null");
        if (paramString2 == null)
            throw new IllegalArgumentException("keyboardLayoutDescriptor must not be null");
        try
        {
            this.mIm.setCurrentKeyboardLayoutForInputDevice(paramString1, paramString2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("InputManager", "Could not set current keyboard layout for input device.", localRemoteException);
        }
    }

    public void setPointerSpeed(Context paramContext, int paramInt)
    {
        if ((paramInt < -7) || (paramInt > 7))
            throw new IllegalArgumentException("speed out of range");
        Settings.System.putInt(paramContext.getContentResolver(), "pointer_speed", paramInt);
    }

    public void tryPointerSpeed(int paramInt)
    {
        if ((paramInt < -7) || (paramInt > 7))
            throw new IllegalArgumentException("speed out of range");
        try
        {
            this.mIm.tryPointerSpeed(paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("InputManager", "Could not set temporary pointer speed.", localRemoteException);
        }
    }

    public void unregisterInputDeviceListener(InputDeviceListener paramInputDeviceListener)
    {
        if (paramInputDeviceListener == null)
            throw new IllegalArgumentException("listener must not be null");
        synchronized (this.mInputDevicesLock)
        {
            int i = findInputDeviceListenerLocked(paramInputDeviceListener);
            if (i >= 0)
            {
                ((InputDeviceListenerDelegate)this.mInputDeviceListeners.get(i)).removeCallbacksAndMessages(null);
                this.mInputDeviceListeners.remove(i);
            }
            return;
        }
    }

    private final class InputDeviceVibrator extends Vibrator
    {
        private final int mDeviceId;
        private final Binder mToken;

        public InputDeviceVibrator(int arg2)
        {
            int i;
            this.mDeviceId = i;
            this.mToken = new Binder();
        }

        public void cancel()
        {
            try
            {
                InputManager.this.mIm.cancelVibrate(this.mDeviceId, this.mToken);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.w("InputManager", "Failed to cancel vibration.", localRemoteException);
            }
        }

        public boolean hasVibrator()
        {
            return true;
        }

        public void vibrate(long paramLong)
        {
            long[] arrayOfLong = new long[2];
            arrayOfLong[0] = 0L;
            arrayOfLong[1] = paramLong;
            vibrate(arrayOfLong, -1);
        }

        public void vibrate(long[] paramArrayOfLong, int paramInt)
        {
            if (paramInt >= paramArrayOfLong.length)
                throw new ArrayIndexOutOfBoundsException();
            try
            {
                InputManager.this.mIm.vibrate(this.mDeviceId, paramArrayOfLong, paramInt, this.mToken);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.w("InputManager", "Failed to vibrate.", localRemoteException);
            }
        }
    }

    private static final class InputDeviceListenerDelegate extends Handler
    {
        public final InputManager.InputDeviceListener mListener;

        public InputDeviceListenerDelegate(InputManager.InputDeviceListener paramInputDeviceListener, Handler paramHandler)
        {
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                this.mListener.onInputDeviceAdded(paramMessage.arg1);
                continue;
                this.mListener.onInputDeviceRemoved(paramMessage.arg1);
                continue;
                this.mListener.onInputDeviceChanged(paramMessage.arg1);
            }
        }
    }

    private final class InputDevicesChangedListener extends IInputDevicesChangedListener.Stub
    {
        private InputDevicesChangedListener()
        {
        }

        public void onInputDevicesChanged(int[] paramArrayOfInt)
            throws RemoteException
        {
            InputManager.this.onInputDevicesChanged(paramArrayOfInt);
        }
    }

    public static abstract interface InputDeviceListener
    {
        public abstract void onInputDeviceAdded(int paramInt);

        public abstract void onInputDeviceChanged(int paramInt);

        public abstract void onInputDeviceRemoved(int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.input.InputManager
 * JD-Core Version:        0.6.2
 */