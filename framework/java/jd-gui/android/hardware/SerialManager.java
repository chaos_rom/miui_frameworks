package android.hardware;

import android.content.Context;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;

public class SerialManager
{
    private static final String TAG = "SerialManager";
    private final Context mContext;
    private final ISerialManager mService;

    public SerialManager(Context paramContext, ISerialManager paramISerialManager)
    {
        this.mContext = paramContext;
        this.mService = paramISerialManager;
    }

    public String[] getSerialPorts()
    {
        try
        {
            String[] arrayOfString2 = this.mService.getSerialPorts();
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SerialManager", "RemoteException in getSerialPorts", localRemoteException);
                String[] arrayOfString1 = null;
            }
        }
    }

    public SerialPort openSerialPort(String paramString, int paramInt)
        throws IOException
    {
        SerialPort localSerialPort;
        try
        {
            ParcelFileDescriptor localParcelFileDescriptor = this.mService.openSerialPort(paramString);
            if (localParcelFileDescriptor != null)
            {
                localSerialPort = new SerialPort(paramString);
                localSerialPort.open(localParcelFileDescriptor, paramInt);
            }
            else
            {
                throw new IOException("Could not open serial port " + paramString);
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("SerialManager", "exception in UsbManager.openDevice", localRemoteException);
            localSerialPort = null;
        }
        return localSerialPort;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.SerialManager
 * JD-Core Version:        0.6.2
 */