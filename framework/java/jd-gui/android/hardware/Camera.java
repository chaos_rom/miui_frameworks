package android.hardware;

import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class Camera
{
    public static final String ACTION_NEW_PICTURE = "android.hardware.action.NEW_PICTURE";
    public static final String ACTION_NEW_VIDEO = "android.hardware.action.NEW_VIDEO";
    public static final int CAMERA_ERROR_SERVER_DIED = 100;
    public static final int CAMERA_ERROR_UNKNOWN = 1;
    private static final int CAMERA_FACE_DETECTION_HW = 0;
    private static final int CAMERA_FACE_DETECTION_SW = 1;
    private static final int CAMERA_MSG_COMPRESSED_IMAGE = 256;
    private static final int CAMERA_MSG_ERROR = 1;
    private static final int CAMERA_MSG_FOCUS = 4;
    private static final int CAMERA_MSG_FOCUS_MOVE = 2048;
    private static final int CAMERA_MSG_POSTVIEW_FRAME = 64;
    private static final int CAMERA_MSG_PREVIEW_FRAME = 16;
    private static final int CAMERA_MSG_PREVIEW_METADATA = 1024;
    private static final int CAMERA_MSG_RAW_IMAGE = 128;
    private static final int CAMERA_MSG_RAW_IMAGE_NOTIFY = 512;
    private static final int CAMERA_MSG_SHUTTER = 2;
    private static final int CAMERA_MSG_VIDEO_FRAME = 32;
    private static final int CAMERA_MSG_ZOOM = 8;
    private static final String TAG = "Camera";
    private AutoFocusCallback mAutoFocusCallback;
    private Object mAutoFocusCallbackLock = new Object();
    private AutoFocusMoveCallback mAutoFocusMoveCallback;
    private ErrorCallback mErrorCallback;
    private EventHandler mEventHandler;
    private boolean mFaceDetectionRunning = false;
    private FaceDetectionListener mFaceListener;
    private PictureCallback mJpegCallback;
    private int mNativeContext;
    private boolean mOneShot;
    private PictureCallback mPostviewCallback;
    private PreviewCallback mPreviewCallback;
    private PictureCallback mRawImageCallback;
    private ShutterCallback mShutterCallback;
    private boolean mWithBuffer;
    private OnZoomChangeListener mZoomListener;

    Camera()
    {
    }

    Camera(int paramInt)
    {
        this.mShutterCallback = null;
        this.mRawImageCallback = null;
        this.mJpegCallback = null;
        this.mPreviewCallback = null;
        this.mPostviewCallback = null;
        this.mZoomListener = null;
        Looper localLooper1 = Looper.myLooper();
        if (localLooper1 != null)
            this.mEventHandler = new EventHandler(this, localLooper1);
        while (true)
        {
            native_setup(new WeakReference(this), paramInt);
            return;
            Looper localLooper2 = Looper.getMainLooper();
            if (localLooper2 != null)
                this.mEventHandler = new EventHandler(this, localLooper2);
            else
                this.mEventHandler = null;
        }
    }

    private final native void _addCallbackBuffer(byte[] paramArrayOfByte, int paramInt);

    private final native void _startFaceDetection(int paramInt);

    private final native void _stopFaceDetection();

    private final native void _stopPreview();

    private final void addCallbackBuffer(byte[] paramArrayOfByte, int paramInt)
    {
        if ((paramInt != 16) && (paramInt != 128))
            throw new IllegalArgumentException("Unsupported message type: " + paramInt);
        _addCallbackBuffer(paramArrayOfByte, paramInt);
    }

    private native void enableFocusMoveCallback(int paramInt);

    public static native void getCameraInfo(int paramInt, CameraInfo paramCameraInfo);

    public static Parameters getEmptyParameters()
    {
        Camera localCamera = new Camera();
        localCamera.getClass();
        return new Parameters(null);
    }

    public static native int getNumberOfCameras();

    private final native void native_autoFocus();

    private final native void native_cancelAutoFocus();

    private final native String native_getParameters();

    private final native void native_release();

    private final native void native_setParameters(String paramString);

    private final native void native_setup(Object paramObject, int paramInt);

    private final native void native_takePicture(int paramInt);

    public static Camera open()
    {
        int i = getNumberOfCameras();
        CameraInfo localCameraInfo = new CameraInfo();
        int j = 0;
        if (j < i)
        {
            getCameraInfo(j, localCameraInfo);
            if (localCameraInfo.facing != 0);
        }
        for (Camera localCamera = new Camera(j); ; localCamera = null)
        {
            return localCamera;
            j++;
            break;
        }
    }

    public static Camera open(int paramInt)
    {
        return new Camera(paramInt);
    }

    private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2)
    {
        Camera localCamera = (Camera)((WeakReference)paramObject1).get();
        if (localCamera == null);
        while (true)
        {
            return;
            if (localCamera.mEventHandler != null)
            {
                Message localMessage = localCamera.mEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
                localCamera.mEventHandler.sendMessage(localMessage);
            }
        }
    }

    private final native void setHasPreviewCallback(boolean paramBoolean1, boolean paramBoolean2);

    private final native void setPreviewDisplay(Surface paramSurface)
        throws IOException;

    public final void addCallbackBuffer(byte[] paramArrayOfByte)
    {
        _addCallbackBuffer(paramArrayOfByte, 16);
    }

    public final void addRawImageCallbackBuffer(byte[] paramArrayOfByte)
    {
        addCallbackBuffer(paramArrayOfByte, 128);
    }

    public final void autoFocus(AutoFocusCallback paramAutoFocusCallback)
    {
        synchronized (this.mAutoFocusCallbackLock)
        {
            this.mAutoFocusCallback = paramAutoFocusCallback;
            native_autoFocus();
            return;
        }
    }

    public final void cancelAutoFocus()
    {
        synchronized (this.mAutoFocusCallbackLock)
        {
            this.mAutoFocusCallback = null;
            native_cancelAutoFocus();
            this.mEventHandler.removeMessages(4);
            return;
        }
    }

    protected void finalize()
    {
        release();
    }

    public Parameters getParameters()
    {
        Parameters localParameters = new Parameters(null);
        localParameters.unflatten(native_getParameters());
        return localParameters;
    }

    public final native void lock();

    public final native boolean previewEnabled();

    public final native void reconnect()
        throws IOException;

    public final void release()
    {
        native_release();
        this.mFaceDetectionRunning = false;
    }

    public void setAutoFocusMoveCallback(AutoFocusMoveCallback paramAutoFocusMoveCallback)
    {
        this.mAutoFocusMoveCallback = paramAutoFocusMoveCallback;
        if (this.mAutoFocusMoveCallback != null);
        for (int i = 1; ; i = 0)
        {
            enableFocusMoveCallback(i);
            return;
        }
    }

    public final native void setDisplayOrientation(int paramInt);

    public final void setErrorCallback(ErrorCallback paramErrorCallback)
    {
        this.mErrorCallback = paramErrorCallback;
    }

    public final void setFaceDetectionListener(FaceDetectionListener paramFaceDetectionListener)
    {
        this.mFaceListener = paramFaceDetectionListener;
    }

    public final void setOneShotPreviewCallback(PreviewCallback paramPreviewCallback)
    {
        boolean bool = true;
        this.mPreviewCallback = paramPreviewCallback;
        this.mOneShot = bool;
        this.mWithBuffer = false;
        if (paramPreviewCallback != null);
        while (true)
        {
            setHasPreviewCallback(bool, false);
            return;
            bool = false;
        }
    }

    public void setParameters(Parameters paramParameters)
    {
        native_setParameters(paramParameters.flatten());
    }

    public final void setPreviewCallback(PreviewCallback paramPreviewCallback)
    {
        this.mPreviewCallback = paramPreviewCallback;
        this.mOneShot = false;
        this.mWithBuffer = false;
        if (paramPreviewCallback != null);
        for (boolean bool = true; ; bool = false)
        {
            setHasPreviewCallback(bool, false);
            return;
        }
    }

    public final void setPreviewCallbackWithBuffer(PreviewCallback paramPreviewCallback)
    {
        boolean bool = false;
        this.mPreviewCallback = paramPreviewCallback;
        this.mOneShot = false;
        this.mWithBuffer = true;
        if (paramPreviewCallback != null)
            bool = true;
        setHasPreviewCallback(bool, true);
    }

    public final void setPreviewDisplay(SurfaceHolder paramSurfaceHolder)
        throws IOException
    {
        if (paramSurfaceHolder != null)
            setPreviewDisplay(paramSurfaceHolder.getSurface());
        while (true)
        {
            return;
            setPreviewDisplay((Surface)null);
        }
    }

    public final native void setPreviewTexture(SurfaceTexture paramSurfaceTexture)
        throws IOException;

    public final void setZoomChangeListener(OnZoomChangeListener paramOnZoomChangeListener)
    {
        this.mZoomListener = paramOnZoomChangeListener;
    }

    public final void startFaceDetection()
    {
        if (this.mFaceDetectionRunning)
            throw new RuntimeException("Face detection is already running");
        _startFaceDetection(0);
        this.mFaceDetectionRunning = true;
    }

    public final native void startPreview();

    public final native void startSmoothZoom(int paramInt);

    public final void stopFaceDetection()
    {
        _stopFaceDetection();
        this.mFaceDetectionRunning = false;
    }

    public final void stopPreview()
    {
        _stopPreview();
        this.mFaceDetectionRunning = false;
        this.mShutterCallback = null;
        this.mRawImageCallback = null;
        this.mPostviewCallback = null;
        this.mJpegCallback = null;
        synchronized (this.mAutoFocusCallbackLock)
        {
            this.mAutoFocusCallback = null;
            this.mAutoFocusMoveCallback = null;
            return;
        }
    }

    public final native void stopSmoothZoom();

    public final void takePicture(ShutterCallback paramShutterCallback, PictureCallback paramPictureCallback1, PictureCallback paramPictureCallback2)
    {
        takePicture(paramShutterCallback, paramPictureCallback1, null, paramPictureCallback2);
    }

    public final void takePicture(ShutterCallback paramShutterCallback, PictureCallback paramPictureCallback1, PictureCallback paramPictureCallback2, PictureCallback paramPictureCallback3)
    {
        this.mShutterCallback = paramShutterCallback;
        this.mRawImageCallback = paramPictureCallback1;
        this.mPostviewCallback = paramPictureCallback2;
        this.mJpegCallback = paramPictureCallback3;
        int i = 0;
        if (this.mShutterCallback != null)
            i = 0x0 | 0x2;
        if (this.mRawImageCallback != null)
            i |= 128;
        if (this.mPostviewCallback != null)
            i |= 64;
        if (this.mJpegCallback != null)
            i |= 256;
        native_takePicture(i);
        this.mFaceDetectionRunning = false;
    }

    public final native void unlock();

    public class Parameters
    {
        public static final String ANTIBANDING_50HZ = "50hz";
        public static final String ANTIBANDING_60HZ = "60hz";
        public static final String ANTIBANDING_AUTO = "auto";
        public static final String ANTIBANDING_OFF = "off";
        public static final String EFFECT_AQUA = "aqua";
        public static final String EFFECT_BLACKBOARD = "blackboard";
        public static final String EFFECT_MONO = "mono";
        public static final String EFFECT_NEGATIVE = "negative";
        public static final String EFFECT_NONE = "none";
        public static final String EFFECT_POSTERIZE = "posterize";
        public static final String EFFECT_SEPIA = "sepia";
        public static final String EFFECT_SOLARIZE = "solarize";
        public static final String EFFECT_WHITEBOARD = "whiteboard";
        private static final String FALSE = "false";
        public static final String FLASH_MODE_AUTO = "auto";
        public static final String FLASH_MODE_OFF = "off";
        public static final String FLASH_MODE_ON = "on";
        public static final String FLASH_MODE_RED_EYE = "red-eye";
        public static final String FLASH_MODE_TORCH = "torch";
        public static final int FOCUS_DISTANCE_FAR_INDEX = 2;
        public static final int FOCUS_DISTANCE_NEAR_INDEX = 0;
        public static final int FOCUS_DISTANCE_OPTIMAL_INDEX = 1;
        public static final String FOCUS_MODE_AUTO = "auto";
        public static final String FOCUS_MODE_CONTINUOUS_PICTURE = "continuous-picture";
        public static final String FOCUS_MODE_CONTINUOUS_VIDEO = "continuous-video";
        public static final String FOCUS_MODE_EDOF = "edof";
        public static final String FOCUS_MODE_FIXED = "fixed";
        public static final String FOCUS_MODE_INFINITY = "infinity";
        public static final String FOCUS_MODE_MACRO = "macro";
        private static final String KEY_ANTIBANDING = "antibanding";
        private static final String KEY_AUTO_EXPOSURE_LOCK = "auto-exposure-lock";
        private static final String KEY_AUTO_EXPOSURE_LOCK_SUPPORTED = "auto-exposure-lock-supported";
        private static final String KEY_AUTO_WHITEBALANCE_LOCK = "auto-whitebalance-lock";
        private static final String KEY_AUTO_WHITEBALANCE_LOCK_SUPPORTED = "auto-whitebalance-lock-supported";
        private static final String KEY_EFFECT = "effect";
        private static final String KEY_EXPOSURE_COMPENSATION = "exposure-compensation";
        private static final String KEY_EXPOSURE_COMPENSATION_STEP = "exposure-compensation-step";
        private static final String KEY_FLASH_MODE = "flash-mode";
        private static final String KEY_FOCAL_LENGTH = "focal-length";
        private static final String KEY_FOCUS_AREAS = "focus-areas";
        private static final String KEY_FOCUS_DISTANCES = "focus-distances";
        private static final String KEY_FOCUS_MODE = "focus-mode";
        private static final String KEY_GPS_ALTITUDE = "gps-altitude";
        private static final String KEY_GPS_LATITUDE = "gps-latitude";
        private static final String KEY_GPS_LONGITUDE = "gps-longitude";
        private static final String KEY_GPS_PROCESSING_METHOD = "gps-processing-method";
        private static final String KEY_GPS_TIMESTAMP = "gps-timestamp";
        private static final String KEY_HORIZONTAL_VIEW_ANGLE = "horizontal-view-angle";
        private static final String KEY_JPEG_QUALITY = "jpeg-quality";
        private static final String KEY_JPEG_THUMBNAIL_HEIGHT = "jpeg-thumbnail-height";
        private static final String KEY_JPEG_THUMBNAIL_QUALITY = "jpeg-thumbnail-quality";
        private static final String KEY_JPEG_THUMBNAIL_SIZE = "jpeg-thumbnail-size";
        private static final String KEY_JPEG_THUMBNAIL_WIDTH = "jpeg-thumbnail-width";
        private static final String KEY_MAX_EXPOSURE_COMPENSATION = "max-exposure-compensation";
        private static final String KEY_MAX_NUM_DETECTED_FACES_HW = "max-num-detected-faces-hw";
        private static final String KEY_MAX_NUM_DETECTED_FACES_SW = "max-num-detected-faces-sw";
        private static final String KEY_MAX_NUM_FOCUS_AREAS = "max-num-focus-areas";
        private static final String KEY_MAX_NUM_METERING_AREAS = "max-num-metering-areas";
        private static final String KEY_MAX_ZOOM = "max-zoom";
        private static final String KEY_METERING_AREAS = "metering-areas";
        private static final String KEY_MIN_EXPOSURE_COMPENSATION = "min-exposure-compensation";
        private static final String KEY_PICTURE_FORMAT = "picture-format";
        private static final String KEY_PICTURE_SIZE = "picture-size";
        private static final String KEY_PREFERRED_PREVIEW_SIZE_FOR_VIDEO = "preferred-preview-size-for-video";
        private static final String KEY_PREVIEW_FORMAT = "preview-format";
        private static final String KEY_PREVIEW_FPS_RANGE = "preview-fps-range";
        private static final String KEY_PREVIEW_FRAME_RATE = "preview-frame-rate";
        private static final String KEY_PREVIEW_SIZE = "preview-size";
        private static final String KEY_RECORDING_HINT = "recording-hint";
        private static final String KEY_ROTATION = "rotation";
        private static final String KEY_SCENE_MODE = "scene-mode";
        private static final String KEY_SMOOTH_ZOOM_SUPPORTED = "smooth-zoom-supported";
        private static final String KEY_VERTICAL_VIEW_ANGLE = "vertical-view-angle";
        private static final String KEY_VIDEO_SIZE = "video-size";
        private static final String KEY_VIDEO_SNAPSHOT_SUPPORTED = "video-snapshot-supported";
        private static final String KEY_VIDEO_STABILIZATION = "video-stabilization";
        private static final String KEY_VIDEO_STABILIZATION_SUPPORTED = "video-stabilization-supported";
        private static final String KEY_WHITE_BALANCE = "whitebalance";
        private static final String KEY_ZOOM = "zoom";
        private static final String KEY_ZOOM_RATIOS = "zoom-ratios";
        private static final String KEY_ZOOM_SUPPORTED = "zoom-supported";
        private static final String PIXEL_FORMAT_BAYER_RGGB = "bayer-rggb";
        private static final String PIXEL_FORMAT_JPEG = "jpeg";
        private static final String PIXEL_FORMAT_RGB565 = "rgb565";
        private static final String PIXEL_FORMAT_YUV420P = "yuv420p";
        private static final String PIXEL_FORMAT_YUV420SP = "yuv420sp";
        private static final String PIXEL_FORMAT_YUV422I = "yuv422i-yuyv";
        private static final String PIXEL_FORMAT_YUV422SP = "yuv422sp";
        public static final int PREVIEW_FPS_MAX_INDEX = 1;
        public static final int PREVIEW_FPS_MIN_INDEX = 0;
        public static final String SCENE_MODE_ACTION = "action";
        public static final String SCENE_MODE_AUTO = "auto";
        public static final String SCENE_MODE_BARCODE = "barcode";
        public static final String SCENE_MODE_BEACH = "beach";
        public static final String SCENE_MODE_CANDLELIGHT = "candlelight";
        public static final String SCENE_MODE_FIREWORKS = "fireworks";
        public static final String SCENE_MODE_LANDSCAPE = "landscape";
        public static final String SCENE_MODE_NIGHT = "night";
        public static final String SCENE_MODE_NIGHT_PORTRAIT = "night-portrait";
        public static final String SCENE_MODE_PARTY = "party";
        public static final String SCENE_MODE_PORTRAIT = "portrait";
        public static final String SCENE_MODE_SNOW = "snow";
        public static final String SCENE_MODE_SPORTS = "sports";
        public static final String SCENE_MODE_STEADYPHOTO = "steadyphoto";
        public static final String SCENE_MODE_SUNSET = "sunset";
        public static final String SCENE_MODE_THEATRE = "theatre";
        private static final String SUPPORTED_VALUES_SUFFIX = "-values";
        private static final String TRUE = "true";
        public static final String WHITE_BALANCE_AUTO = "auto";
        public static final String WHITE_BALANCE_CLOUDY_DAYLIGHT = "cloudy-daylight";
        public static final String WHITE_BALANCE_DAYLIGHT = "daylight";
        public static final String WHITE_BALANCE_FLUORESCENT = "fluorescent";
        public static final String WHITE_BALANCE_INCANDESCENT = "incandescent";
        public static final String WHITE_BALANCE_SHADE = "shade";
        public static final String WHITE_BALANCE_TWILIGHT = "twilight";
        public static final String WHITE_BALANCE_WARM_FLUORESCENT = "warm-fluorescent";
        private HashMap<String, String> mMap = new HashMap();

        private Parameters()
        {
        }

        private String cameraFormatForPixelFormat(int paramInt)
        {
            String str;
            switch (paramInt)
            {
            default:
                str = null;
            case 16:
            case 17:
            case 20:
            case 842094169:
            case 4:
            case 256:
            case 512:
            }
            while (true)
            {
                return str;
                str = "yuv422sp";
                continue;
                str = "yuv420sp";
                continue;
                str = "yuv422i-yuyv";
                continue;
                str = "yuv420p";
                continue;
                str = "rgb565";
                continue;
                str = "jpeg";
                continue;
                str = "bayer-rggb";
            }
        }

        private float getFloat(String paramString, float paramFloat)
        {
            try
            {
                float f = Float.parseFloat((String)this.mMap.get(paramString));
                paramFloat = f;
                label19: return paramFloat;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label19;
            }
        }

        private int getInt(String paramString, int paramInt)
        {
            try
            {
                int i = Integer.parseInt((String)this.mMap.get(paramString));
                paramInt = i;
                label19: return paramInt;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label19;
            }
        }

        private int pixelFormatForCameraFormat(String paramString)
        {
            int i = 0;
            if (paramString == null);
            while (true)
            {
                return i;
                if (paramString.equals("yuv422sp"))
                    i = 16;
                else if (paramString.equals("yuv420sp"))
                    i = 17;
                else if (paramString.equals("yuv422i-yuyv"))
                    i = 20;
                else if (paramString.equals("yuv420p"))
                    i = 842094169;
                else if (paramString.equals("rgb565"))
                    i = 4;
                else if (paramString.equals("jpeg"))
                    i = 256;
            }
        }

        private boolean same(String paramString1, String paramString2)
        {
            boolean bool = true;
            if ((paramString1 == null) && (paramString2 == null));
            while (true)
            {
                return bool;
                if ((paramString1 == null) || (!paramString1.equals(paramString2)))
                    bool = false;
            }
        }

        private void set(String paramString, List<Camera.Area> paramList)
        {
            if (paramList == null)
                set(paramString, "(0,0,0,0,0)");
            while (true)
            {
                return;
                StringBuilder localStringBuilder = new StringBuilder();
                for (int i = 0; i < paramList.size(); i++)
                {
                    Camera.Area localArea = (Camera.Area)paramList.get(i);
                    Rect localRect = localArea.rect;
                    localStringBuilder.append('(');
                    localStringBuilder.append(localRect.left);
                    localStringBuilder.append(',');
                    localStringBuilder.append(localRect.top);
                    localStringBuilder.append(',');
                    localStringBuilder.append(localRect.right);
                    localStringBuilder.append(',');
                    localStringBuilder.append(localRect.bottom);
                    localStringBuilder.append(',');
                    localStringBuilder.append(localArea.weight);
                    localStringBuilder.append(')');
                    if (i != -1 + paramList.size())
                        localStringBuilder.append(',');
                }
                set(paramString, localStringBuilder.toString());
            }
        }

        private ArrayList<String> split(String paramString)
        {
            Object localObject;
            if (paramString == null)
                localObject = null;
            while (true)
            {
                return localObject;
                StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
                localObject = new ArrayList();
                while (localStringTokenizer.hasMoreElements())
                    ((ArrayList)localObject).add(localStringTokenizer.nextToken());
            }
        }

        private ArrayList<Camera.Area> splitArea(String paramString)
        {
            Object localObject;
            if ((paramString == null) || (paramString.charAt(0) != '(') || (paramString.charAt(-1 + paramString.length()) != ')'))
            {
                Log.e("Camera", "Invalid area string=" + paramString);
                localObject = null;
            }
            while (true)
            {
                return localObject;
                localObject = new ArrayList();
                int i = 1;
                int[] arrayOfInt = new int[5];
                int j;
                do
                {
                    j = paramString.indexOf("),(", i);
                    if (j == -1)
                        j = -1 + paramString.length();
                    splitInt(paramString.substring(i, j), arrayOfInt);
                    ((ArrayList)localObject).add(new Camera.Area(new Rect(arrayOfInt[0], arrayOfInt[1], arrayOfInt[2], arrayOfInt[3]), arrayOfInt[4]));
                    i = j + 3;
                }
                while (j != -1 + paramString.length());
                if (((ArrayList)localObject).size() == 0)
                {
                    localObject = null;
                }
                else if (((ArrayList)localObject).size() == 1)
                {
                    Camera.Area localArea = (Camera.Area)((ArrayList)localObject).get(0);
                    Rect localRect = localArea.rect;
                    if ((localRect.left == 0) && (localRect.top == 0) && (localRect.right == 0) && (localRect.bottom == 0) && (localArea.weight == 0))
                        localObject = null;
                }
            }
        }

        private void splitFloat(String paramString, float[] paramArrayOfFloat)
        {
            if (paramString == null);
            while (true)
            {
                return;
                StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
                int j;
                for (int i = 0; localStringTokenizer.hasMoreElements(); i = j)
                {
                    String str = localStringTokenizer.nextToken();
                    j = i + 1;
                    paramArrayOfFloat[i] = Float.parseFloat(str);
                }
            }
        }

        private ArrayList<Integer> splitInt(String paramString)
        {
            Object localObject;
            if (paramString == null)
                localObject = null;
            while (true)
            {
                return localObject;
                StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
                localObject = new ArrayList();
                while (localStringTokenizer.hasMoreElements())
                    ((ArrayList)localObject).add(Integer.valueOf(Integer.parseInt(localStringTokenizer.nextToken())));
                if (((ArrayList)localObject).size() == 0)
                    localObject = null;
            }
        }

        private void splitInt(String paramString, int[] paramArrayOfInt)
        {
            if (paramString == null);
            while (true)
            {
                return;
                StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
                int j;
                for (int i = 0; localStringTokenizer.hasMoreElements(); i = j)
                {
                    String str = localStringTokenizer.nextToken();
                    j = i + 1;
                    paramArrayOfInt[i] = Integer.parseInt(str);
                }
            }
        }

        private ArrayList<int[]> splitRange(String paramString)
        {
            Object localObject;
            if ((paramString == null) || (paramString.charAt(0) != '(') || (paramString.charAt(-1 + paramString.length()) != ')'))
            {
                Log.e("Camera", "Invalid range list string=" + paramString);
                localObject = null;
            }
            while (true)
            {
                return localObject;
                localObject = new ArrayList();
                int i = 1;
                int j;
                do
                {
                    int[] arrayOfInt = new int[2];
                    j = paramString.indexOf("),(", i);
                    if (j == -1)
                        j = -1 + paramString.length();
                    splitInt(paramString.substring(i, j), arrayOfInt);
                    ((ArrayList)localObject).add(arrayOfInt);
                    i = j + 3;
                }
                while (j != -1 + paramString.length());
                if (((ArrayList)localObject).size() == 0)
                    localObject = null;
            }
        }

        private ArrayList<Camera.Size> splitSize(String paramString)
        {
            Object localObject;
            if (paramString == null)
                localObject = null;
            while (true)
            {
                return localObject;
                StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
                localObject = new ArrayList();
                while (localStringTokenizer.hasMoreElements())
                {
                    Camera.Size localSize = strToSize(localStringTokenizer.nextToken());
                    if (localSize != null)
                        ((ArrayList)localObject).add(localSize);
                }
                if (((ArrayList)localObject).size() == 0)
                    localObject = null;
            }
        }

        private Camera.Size strToSize(String paramString)
        {
            Camera.Size localSize = null;
            if (paramString == null);
            while (true)
            {
                return localSize;
                int i = paramString.indexOf('x');
                if (i != -1)
                {
                    String str1 = paramString.substring(0, i);
                    String str2 = paramString.substring(i + 1);
                    localSize = new Camera.Size(Camera.this, Integer.parseInt(str1), Integer.parseInt(str2));
                }
                else
                {
                    Log.e("Camera", "Invalid size parameter string=" + paramString);
                }
            }
        }

        public void dump()
        {
            Log.e("Camera", "dump: size=" + this.mMap.size());
            Iterator localIterator = this.mMap.keySet().iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                Log.e("Camera", "dump: " + str + "=" + (String)this.mMap.get(str));
            }
        }

        public String flatten()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            Iterator localIterator = this.mMap.keySet().iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                localStringBuilder.append(str);
                localStringBuilder.append("=");
                localStringBuilder.append((String)this.mMap.get(str));
                localStringBuilder.append(";");
            }
            localStringBuilder.deleteCharAt(-1 + localStringBuilder.length());
            return localStringBuilder.toString();
        }

        public String get(String paramString)
        {
            return (String)this.mMap.get(paramString);
        }

        public String getAntibanding()
        {
            return get("antibanding");
        }

        public boolean getAutoExposureLock()
        {
            return "true".equals(get("auto-exposure-lock"));
        }

        public boolean getAutoWhiteBalanceLock()
        {
            return "true".equals(get("auto-whitebalance-lock"));
        }

        public String getColorEffect()
        {
            return get("effect");
        }

        public int getExposureCompensation()
        {
            return getInt("exposure-compensation", 0);
        }

        public float getExposureCompensationStep()
        {
            return getFloat("exposure-compensation-step", 0.0F);
        }

        public String getFlashMode()
        {
            return get("flash-mode");
        }

        public float getFocalLength()
        {
            return Float.parseFloat(get("focal-length"));
        }

        public List<Camera.Area> getFocusAreas()
        {
            return splitArea(get("focus-areas"));
        }

        public void getFocusDistances(float[] paramArrayOfFloat)
        {
            if ((paramArrayOfFloat == null) || (paramArrayOfFloat.length != 3))
                throw new IllegalArgumentException("output must be a float array with three elements.");
            splitFloat(get("focus-distances"), paramArrayOfFloat);
        }

        public String getFocusMode()
        {
            return get("focus-mode");
        }

        public float getHorizontalViewAngle()
        {
            return Float.parseFloat(get("horizontal-view-angle"));
        }

        public int getInt(String paramString)
        {
            return Integer.parseInt((String)this.mMap.get(paramString));
        }

        public int getJpegQuality()
        {
            return getInt("jpeg-quality");
        }

        public int getJpegThumbnailQuality()
        {
            return getInt("jpeg-thumbnail-quality");
        }

        public Camera.Size getJpegThumbnailSize()
        {
            return new Camera.Size(Camera.this, getInt("jpeg-thumbnail-width"), getInt("jpeg-thumbnail-height"));
        }

        public int getMaxExposureCompensation()
        {
            return getInt("max-exposure-compensation", 0);
        }

        public int getMaxNumDetectedFaces()
        {
            return getInt("max-num-detected-faces-hw", 0);
        }

        public int getMaxNumFocusAreas()
        {
            return getInt("max-num-focus-areas", 0);
        }

        public int getMaxNumMeteringAreas()
        {
            return getInt("max-num-metering-areas", 0);
        }

        public int getMaxZoom()
        {
            return getInt("max-zoom", 0);
        }

        public List<Camera.Area> getMeteringAreas()
        {
            return splitArea(get("metering-areas"));
        }

        public int getMinExposureCompensation()
        {
            return getInt("min-exposure-compensation", 0);
        }

        public int getPictureFormat()
        {
            return pixelFormatForCameraFormat(get("picture-format"));
        }

        public Camera.Size getPictureSize()
        {
            return strToSize(get("picture-size"));
        }

        public Camera.Size getPreferredPreviewSizeForVideo()
        {
            return strToSize(get("preferred-preview-size-for-video"));
        }

        public int getPreviewFormat()
        {
            return pixelFormatForCameraFormat(get("preview-format"));
        }

        public void getPreviewFpsRange(int[] paramArrayOfInt)
        {
            if ((paramArrayOfInt == null) || (paramArrayOfInt.length != 2))
                throw new IllegalArgumentException("range must be an array with two elements.");
            splitInt(get("preview-fps-range"), paramArrayOfInt);
        }

        @Deprecated
        public int getPreviewFrameRate()
        {
            return getInt("preview-frame-rate");
        }

        public Camera.Size getPreviewSize()
        {
            return strToSize(get("preview-size"));
        }

        public String getSceneMode()
        {
            return get("scene-mode");
        }

        public List<String> getSupportedAntibanding()
        {
            return split(get("antibanding-values"));
        }

        public List<String> getSupportedColorEffects()
        {
            return split(get("effect-values"));
        }

        public List<String> getSupportedFlashModes()
        {
            return split(get("flash-mode-values"));
        }

        public List<String> getSupportedFocusModes()
        {
            return split(get("focus-mode-values"));
        }

        public List<Camera.Size> getSupportedJpegThumbnailSizes()
        {
            return splitSize(get("jpeg-thumbnail-size-values"));
        }

        public List<Integer> getSupportedPictureFormats()
        {
            String str = get("picture-format-values");
            ArrayList localArrayList = new ArrayList();
            Iterator localIterator = split(str).iterator();
            while (localIterator.hasNext())
            {
                int i = pixelFormatForCameraFormat((String)localIterator.next());
                if (i != 0)
                    localArrayList.add(Integer.valueOf(i));
            }
            return localArrayList;
        }

        public List<Camera.Size> getSupportedPictureSizes()
        {
            return splitSize(get("picture-size-values"));
        }

        public List<Integer> getSupportedPreviewFormats()
        {
            String str = get("preview-format-values");
            ArrayList localArrayList = new ArrayList();
            Iterator localIterator = split(str).iterator();
            while (localIterator.hasNext())
            {
                int i = pixelFormatForCameraFormat((String)localIterator.next());
                if (i != 0)
                    localArrayList.add(Integer.valueOf(i));
            }
            return localArrayList;
        }

        public List<int[]> getSupportedPreviewFpsRange()
        {
            return splitRange(get("preview-fps-range-values"));
        }

        @Deprecated
        public List<Integer> getSupportedPreviewFrameRates()
        {
            return splitInt(get("preview-frame-rate-values"));
        }

        public List<Camera.Size> getSupportedPreviewSizes()
        {
            return splitSize(get("preview-size-values"));
        }

        public List<String> getSupportedSceneModes()
        {
            return split(get("scene-mode-values"));
        }

        public List<Camera.Size> getSupportedVideoSizes()
        {
            return splitSize(get("video-size-values"));
        }

        public List<String> getSupportedWhiteBalance()
        {
            return split(get("whitebalance-values"));
        }

        public float getVerticalViewAngle()
        {
            return Float.parseFloat(get("vertical-view-angle"));
        }

        public boolean getVideoStabilization()
        {
            return "true".equals(get("video-stabilization"));
        }

        public String getWhiteBalance()
        {
            return get("whitebalance");
        }

        public int getZoom()
        {
            return getInt("zoom", 0);
        }

        public List<Integer> getZoomRatios()
        {
            return splitInt(get("zoom-ratios"));
        }

        public boolean isAutoExposureLockSupported()
        {
            return "true".equals(get("auto-exposure-lock-supported"));
        }

        public boolean isAutoWhiteBalanceLockSupported()
        {
            return "true".equals(get("auto-whitebalance-lock-supported"));
        }

        public boolean isSmoothZoomSupported()
        {
            return "true".equals(get("smooth-zoom-supported"));
        }

        public boolean isVideoSnapshotSupported()
        {
            return "true".equals(get("video-snapshot-supported"));
        }

        public boolean isVideoStabilizationSupported()
        {
            return "true".equals(get("video-stabilization-supported"));
        }

        public boolean isZoomSupported()
        {
            return "true".equals(get("zoom-supported"));
        }

        public void remove(String paramString)
        {
            this.mMap.remove(paramString);
        }

        public void removeGpsData()
        {
            remove("gps-latitude");
            remove("gps-longitude");
            remove("gps-altitude");
            remove("gps-timestamp");
            remove("gps-processing-method");
        }

        public void set(String paramString, int paramInt)
        {
            this.mMap.put(paramString, Integer.toString(paramInt));
        }

        public void set(String paramString1, String paramString2)
        {
            if ((paramString1.indexOf('=') != -1) || (paramString1.indexOf(';') != -1) || (paramString1.indexOf(0) != -1))
                Log.e("Camera", "Key \"" + paramString1 + "\" contains invalid character (= or ; or \\0)");
            while (true)
            {
                return;
                if ((paramString2.indexOf('=') != -1) || (paramString2.indexOf(';') != -1) || (paramString2.indexOf(0) != -1))
                    Log.e("Camera", "Value \"" + paramString2 + "\" contains invalid character (= or ; or \\0)");
                else
                    this.mMap.put(paramString1, paramString2);
            }
        }

        public void setAntibanding(String paramString)
        {
            set("antibanding", paramString);
        }

        public void setAutoExposureLock(boolean paramBoolean)
        {
            if (paramBoolean);
            for (String str = "true"; ; str = "false")
            {
                set("auto-exposure-lock", str);
                return;
            }
        }

        public void setAutoWhiteBalanceLock(boolean paramBoolean)
        {
            if (paramBoolean);
            for (String str = "true"; ; str = "false")
            {
                set("auto-whitebalance-lock", str);
                return;
            }
        }

        public void setColorEffect(String paramString)
        {
            set("effect", paramString);
        }

        public void setExposureCompensation(int paramInt)
        {
            set("exposure-compensation", paramInt);
        }

        public void setFlashMode(String paramString)
        {
            set("flash-mode", paramString);
        }

        public void setFocusAreas(List<Camera.Area> paramList)
        {
            set("focus-areas", paramList);
        }

        public void setFocusMode(String paramString)
        {
            set("focus-mode", paramString);
        }

        public void setGpsAltitude(double paramDouble)
        {
            set("gps-altitude", Double.toString(paramDouble));
        }

        public void setGpsLatitude(double paramDouble)
        {
            set("gps-latitude", Double.toString(paramDouble));
        }

        public void setGpsLongitude(double paramDouble)
        {
            set("gps-longitude", Double.toString(paramDouble));
        }

        public void setGpsProcessingMethod(String paramString)
        {
            set("gps-processing-method", paramString);
        }

        public void setGpsTimestamp(long paramLong)
        {
            set("gps-timestamp", Long.toString(paramLong));
        }

        public void setJpegQuality(int paramInt)
        {
            set("jpeg-quality", paramInt);
        }

        public void setJpegThumbnailQuality(int paramInt)
        {
            set("jpeg-thumbnail-quality", paramInt);
        }

        public void setJpegThumbnailSize(int paramInt1, int paramInt2)
        {
            set("jpeg-thumbnail-width", paramInt1);
            set("jpeg-thumbnail-height", paramInt2);
        }

        public void setMeteringAreas(List<Camera.Area> paramList)
        {
            set("metering-areas", paramList);
        }

        public void setPictureFormat(int paramInt)
        {
            String str = cameraFormatForPixelFormat(paramInt);
            if (str == null)
                throw new IllegalArgumentException("Invalid pixel_format=" + paramInt);
            set("picture-format", str);
        }

        public void setPictureSize(int paramInt1, int paramInt2)
        {
            set("picture-size", Integer.toString(paramInt1) + "x" + Integer.toString(paramInt2));
        }

        public void setPreviewFormat(int paramInt)
        {
            String str = cameraFormatForPixelFormat(paramInt);
            if (str == null)
                throw new IllegalArgumentException("Invalid pixel_format=" + paramInt);
            set("preview-format", str);
        }

        public void setPreviewFpsRange(int paramInt1, int paramInt2)
        {
            set("preview-fps-range", "" + paramInt1 + "," + paramInt2);
        }

        @Deprecated
        public void setPreviewFrameRate(int paramInt)
        {
            set("preview-frame-rate", paramInt);
        }

        public void setPreviewSize(int paramInt1, int paramInt2)
        {
            set("preview-size", Integer.toString(paramInt1) + "x" + Integer.toString(paramInt2));
        }

        public void setRecordingHint(boolean paramBoolean)
        {
            if (paramBoolean);
            for (String str = "true"; ; str = "false")
            {
                set("recording-hint", str);
                return;
            }
        }

        public void setRotation(int paramInt)
        {
            if ((paramInt == 0) || (paramInt == 90) || (paramInt == 180) || (paramInt == 270))
            {
                set("rotation", Integer.toString(paramInt));
                return;
            }
            throw new IllegalArgumentException("Invalid rotation=" + paramInt);
        }

        public void setSceneMode(String paramString)
        {
            set("scene-mode", paramString);
        }

        public void setVideoStabilization(boolean paramBoolean)
        {
            if (paramBoolean);
            for (String str = "true"; ; str = "false")
            {
                set("video-stabilization", str);
                return;
            }
        }

        public void setWhiteBalance(String paramString)
        {
            if (same(paramString, get("whitebalance")));
            while (true)
            {
                return;
                set("whitebalance", paramString);
                set("auto-whitebalance-lock", "false");
            }
        }

        public void setZoom(int paramInt)
        {
            set("zoom", paramInt);
        }

        public void unflatten(String paramString)
        {
            this.mMap.clear();
            StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ";");
            while (localStringTokenizer.hasMoreElements())
            {
                String str1 = localStringTokenizer.nextToken();
                int i = str1.indexOf('=');
                if (i != -1)
                {
                    String str2 = str1.substring(0, i);
                    String str3 = str1.substring(i + 1);
                    this.mMap.put(str2, str3);
                }
            }
        }
    }

    public static class Area
    {
        public Rect rect;
        public int weight;

        public Area(Rect paramRect, int paramInt)
        {
            this.rect = paramRect;
            this.weight = paramInt;
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = false;
            if (!(paramObject instanceof Area))
                break label30;
            while (true)
            {
                return bool;
                Area localArea = (Area)paramObject;
                if (this.rect == null)
                {
                    if (localArea.rect == null)
                        label30: if (this.weight == localArea.weight)
                            bool = true;
                }
                else
                    if (this.rect.equals(localArea.rect))
                        break;
            }
        }
    }

    public class Size
    {
        public int height;
        public int width;

        public Size(int paramInt1, int arg3)
        {
            this.width = paramInt1;
            int i;
            this.height = i;
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = false;
            if (!(paramObject instanceof Size));
            while (true)
            {
                return bool;
                Size localSize = (Size)paramObject;
                if ((this.width == localSize.width) && (this.height == localSize.height))
                    bool = true;
            }
        }

        public int hashCode()
        {
            return 32713 * this.width + this.height;
        }
    }

    public static abstract interface ErrorCallback
    {
        public abstract void onError(int paramInt, Camera paramCamera);
    }

    public static class Face
    {
        public int id = -1;
        public Point leftEye = null;
        public Point mouth = null;
        public Rect rect;
        public Point rightEye = null;
        public int score;
    }

    public static abstract interface FaceDetectionListener
    {
        public abstract void onFaceDetection(Camera.Face[] paramArrayOfFace, Camera paramCamera);
    }

    public static abstract interface OnZoomChangeListener
    {
        public abstract void onZoomChange(int paramInt, boolean paramBoolean, Camera paramCamera);
    }

    public static abstract interface PictureCallback
    {
        public abstract void onPictureTaken(byte[] paramArrayOfByte, Camera paramCamera);
    }

    public static abstract interface ShutterCallback
    {
        public abstract void onShutter();
    }

    public static abstract interface AutoFocusMoveCallback
    {
        public abstract void onAutoFocusMoving(boolean paramBoolean, Camera paramCamera);
    }

    public static abstract interface AutoFocusCallback
    {
        public abstract void onAutoFocus(boolean paramBoolean, Camera paramCamera);
    }

    private class EventHandler extends Handler
    {
        private Camera mCamera;

        public EventHandler(Camera paramLooper, Looper arg3)
        {
            super();
            this.mCamera = paramLooper;
        }

        public void handleMessage(Message paramMessage)
        {
            boolean bool1 = true;
            boolean bool2 = false;
            switch (paramMessage.what)
            {
            default:
                Log.e("Camera", "Unknown message type " + paramMessage.what);
            case 2:
            case 128:
            case 256:
            case 16:
            case 64:
            case 4:
            case 8:
            case 1024:
            case 1:
            case 2048:
            }
            do
                while (true)
                {
                    return;
                    if (Camera.this.mShutterCallback != null)
                    {
                        Camera.this.mShutterCallback.onShutter();
                        continue;
                        if (Camera.this.mRawImageCallback != null)
                        {
                            Camera.this.mRawImageCallback.onPictureTaken((byte[])paramMessage.obj, this.mCamera);
                            continue;
                            if (Camera.this.mJpegCallback != null)
                            {
                                Camera.this.mJpegCallback.onPictureTaken((byte[])paramMessage.obj, this.mCamera);
                                continue;
                                Camera.PreviewCallback localPreviewCallback = Camera.this.mPreviewCallback;
                                if (localPreviewCallback != null)
                                {
                                    if (Camera.this.mOneShot)
                                        Camera.access$302(Camera.this, null);
                                    while (true)
                                    {
                                        localPreviewCallback.onPreviewFrame((byte[])paramMessage.obj, this.mCamera);
                                        break;
                                        if (!Camera.this.mWithBuffer)
                                            Camera.this.setHasPreviewCallback(bool1, false);
                                    }
                                    if (Camera.this.mPostviewCallback != null)
                                    {
                                        Camera.this.mPostviewCallback.onPictureTaken((byte[])paramMessage.obj, this.mCamera);
                                        continue;
                                        while (true)
                                        {
                                            synchronized (Camera.this.mAutoFocusCallbackLock)
                                            {
                                                Camera.AutoFocusCallback localAutoFocusCallback = Camera.this.mAutoFocusCallback;
                                                if (localAutoFocusCallback == null)
                                                    break;
                                                if (paramMessage.arg1 == 0)
                                                    localAutoFocusCallback.onAutoFocus(bool2, this.mCamera);
                                            }
                                            bool2 = bool1;
                                        }
                                        if (Camera.this.mZoomListener != null)
                                        {
                                            Camera.OnZoomChangeListener localOnZoomChangeListener = Camera.this.mZoomListener;
                                            int i = paramMessage.arg1;
                                            if (paramMessage.arg2 != 0);
                                            while (true)
                                            {
                                                localOnZoomChangeListener.onZoomChange(i, bool1, this.mCamera);
                                                break;
                                                bool1 = false;
                                            }
                                            if (Camera.this.mFaceListener != null)
                                            {
                                                Camera.this.mFaceListener.onFaceDetection((Camera.Face[])paramMessage.obj, this.mCamera);
                                                continue;
                                                Log.e("Camera", "Error " + paramMessage.arg1);
                                                if (Camera.this.mErrorCallback != null)
                                                    Camera.this.mErrorCallback.onError(paramMessage.arg1, this.mCamera);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            while (Camera.this.mAutoFocusMoveCallback == null);
            Camera.AutoFocusMoveCallback localAutoFocusMoveCallback = Camera.this.mAutoFocusMoveCallback;
            if (paramMessage.arg1 == 0);
            while (true)
            {
                localAutoFocusMoveCallback.onAutoFocusMoving(bool2, this.mCamera);
                break;
                bool2 = bool1;
            }
        }
    }

    public static abstract interface PreviewCallback
    {
        public abstract void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera);
    }

    public static class CameraInfo
    {
        public static final int CAMERA_FACING_BACK = 0;
        public static final int CAMERA_FACING_FRONT = 1;
        public int facing;
        public int orientation;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.Camera
 * JD-Core Version:        0.6.2
 */