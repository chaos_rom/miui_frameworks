package android.hardware;

import android.view.IRotationWatcher.Stub;
import android.view.IWindowManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

final class LegacySensorManager
{
    private static boolean sInitialized;
    private static int sRotation = 0;
    private static IWindowManager sWindowManager;
    private final HashMap<SensorListener, LegacyListener> mLegacyListenersMap;
    private final SensorManager mSensorManager;

    // ERROR //
    public LegacySensorManager(SensorManager paramSensorManager)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 33	java/lang/Object:<init>	()V
        //     4: aload_0
        //     5: new 35	java/util/HashMap
        //     8: dup
        //     9: invokespecial 36	java/util/HashMap:<init>	()V
        //     12: putfield 38	android/hardware/LegacySensorManager:mLegacyListenersMap	Ljava/util/HashMap;
        //     15: aload_0
        //     16: aload_1
        //     17: putfield 40	android/hardware/LegacySensorManager:mSensorManager	Landroid/hardware/SensorManager;
        //     20: ldc 42
        //     22: monitorenter
        //     23: getstatic 44	android/hardware/LegacySensorManager:sInitialized	Z
        //     26: ifne +41 -> 67
        //     29: ldc 46
        //     31: invokestatic 52	android/os/ServiceManager:getService	(Ljava/lang/String;)Landroid/os/IBinder;
        //     34: invokestatic 58	android/view/IWindowManager$Stub:asInterface	(Landroid/os/IBinder;)Landroid/view/IWindowManager;
        //     37: putstatic 60	android/hardware/LegacySensorManager:sWindowManager	Landroid/view/IWindowManager;
        //     40: getstatic 60	android/hardware/LegacySensorManager:sWindowManager	Landroid/view/IWindowManager;
        //     43: astore_3
        //     44: aload_3
        //     45: ifnull +22 -> 67
        //     48: getstatic 60	android/hardware/LegacySensorManager:sWindowManager	Landroid/view/IWindowManager;
        //     51: new 6	android/hardware/LegacySensorManager$1
        //     54: dup
        //     55: aload_0
        //     56: invokespecial 63	android/hardware/LegacySensorManager$1:<init>	(Landroid/hardware/LegacySensorManager;)V
        //     59: invokeinterface 69 2 0
        //     64: putstatic 27	android/hardware/LegacySensorManager:sRotation	I
        //     67: ldc 42
        //     69: monitorexit
        //     70: return
        //     71: astore_2
        //     72: ldc 42
        //     74: monitorexit
        //     75: aload_2
        //     76: athrow
        //     77: astore 4
        //     79: goto -12 -> 67
        //
        // Exception table:
        //     from	to	target	type
        //     23	44	71	finally
        //     48	67	71	finally
        //     67	75	71	finally
        //     48	67	77	android/os/RemoteException
    }

    static int getRotation()
    {
        try
        {
            int i = sRotation;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    static void onRotationChanged(int paramInt)
    {
        try
        {
            sRotation = paramInt;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private boolean registerLegacyListener(int paramInt1, int paramInt2, SensorListener paramSensorListener, int paramInt3, int paramInt4)
    {
        boolean bool = false;
        Sensor localSensor;
        if ((paramInt3 & paramInt1) != 0)
        {
            localSensor = this.mSensorManager.getDefaultSensor(paramInt2);
            if (localSensor == null);
        }
        while (true)
        {
            synchronized (this.mLegacyListenersMap)
            {
                LegacyListener localLegacyListener = (LegacyListener)this.mLegacyListenersMap.get(paramSensorListener);
                if (localLegacyListener == null)
                {
                    localLegacyListener = new LegacyListener(paramSensorListener);
                    this.mLegacyListenersMap.put(paramSensorListener, localLegacyListener);
                }
                if (!localLegacyListener.registerSensor(paramInt1))
                    break label114;
                bool = this.mSensorManager.registerListener(localLegacyListener, localSensor, paramInt4);
            }
            return bool;
            label114: bool = true;
        }
    }

    private void unregisterLegacyListener(int paramInt1, int paramInt2, SensorListener paramSensorListener, int paramInt3)
    {
        if ((paramInt3 & paramInt1) != 0)
        {
            Sensor localSensor = this.mSensorManager.getDefaultSensor(paramInt2);
            if (localSensor != null)
                synchronized (this.mLegacyListenersMap)
                {
                    LegacyListener localLegacyListener = (LegacyListener)this.mLegacyListenersMap.get(paramSensorListener);
                    if ((localLegacyListener != null) && (localLegacyListener.unregisterSensor(paramInt1)))
                    {
                        this.mSensorManager.unregisterListener(localLegacyListener, localSensor);
                        if (!localLegacyListener.hasSensors())
                            this.mLegacyListenersMap.remove(paramSensorListener);
                    }
                }
        }
    }

    public int getSensors()
    {
        int i = 0;
        Iterator localIterator = this.mSensorManager.getFullSensorList().iterator();
        while (localIterator.hasNext())
            switch (((Sensor)localIterator.next()).getType())
            {
            default:
                break;
            case 1:
                i |= 2;
                break;
            case 2:
                i |= 8;
                break;
            case 3:
                i |= 129;
            }
        return i;
    }

    public boolean registerListener(SensorListener paramSensorListener, int paramInt1, int paramInt2)
    {
        boolean bool1 = false;
        if (paramSensorListener == null)
            return bool1;
        int i;
        label29: int j;
        label50: int k;
        label72: int m;
        if ((registerLegacyListener(2, 1, paramSensorListener, paramInt1, paramInt2)) || (0 != 0))
        {
            i = 1;
            if ((!registerLegacyListener(8, 2, paramSensorListener, paramInt1, paramInt2)) && (i == 0))
                break label126;
            j = 1;
            if ((!registerLegacyListener(128, 3, paramSensorListener, paramInt1, paramInt2)) && (j == 0))
                break label132;
            k = 1;
            if ((!registerLegacyListener(1, 3, paramSensorListener, paramInt1, paramInt2)) && (k == 0))
                break label138;
            m = 1;
            label92: if ((!registerLegacyListener(4, 7, paramSensorListener, paramInt1, paramInt2)) && (m == 0))
                break label144;
        }
        label132: label138: label144: for (boolean bool2 = true; ; bool2 = false)
        {
            bool1 = bool2;
            break;
            i = 0;
            break label29;
            label126: j = 0;
            break label50;
            k = 0;
            break label72;
            m = 0;
            break label92;
        }
    }

    public void unregisterListener(SensorListener paramSensorListener, int paramInt)
    {
        if (paramSensorListener == null);
        while (true)
        {
            return;
            unregisterLegacyListener(2, 1, paramSensorListener, paramInt);
            unregisterLegacyListener(8, 2, paramSensorListener, paramInt);
            unregisterLegacyListener(128, 3, paramSensorListener, paramInt);
            unregisterLegacyListener(1, 3, paramSensorListener, paramInt);
            unregisterLegacyListener(4, 7, paramSensorListener, paramInt);
        }
    }

    private static final class LmsFilter
    {
        private static final int COUNT = 12;
        private static final float PREDICTION_RATIO = 0.3333333F;
        private static final float PREDICTION_TIME = 0.08F;
        private static final int SENSORS_RATE_MS = 20;
        private int mIndex = 12;
        private float[] mT = new float[24];
        private float[] mV = new float[24];

        public float filter(long paramLong, float paramFloat)
        {
            float f1 = paramFloat;
            float f2 = 1.0E-09F * (float)paramLong;
            float f3 = this.mV[this.mIndex];
            if (f1 - f3 > 180.0F)
                f1 -= 360.0F;
            float f4;
            float f5;
            float f6;
            float f7;
            float f8;
            while (true)
            {
                this.mIndex = (1 + this.mIndex);
                if (this.mIndex >= 24)
                    this.mIndex = 12;
                this.mV[this.mIndex] = f1;
                this.mT[this.mIndex] = f2;
                this.mV[(-12 + this.mIndex)] = f1;
                this.mT[(-12 + this.mIndex)] = f2;
                f4 = 0.0F;
                f5 = 0.0F;
                f6 = 0.0F;
                f7 = 0.0F;
                f8 = 0.0F;
                for (int i = 0; i < 11; i++)
                {
                    int j = -1 + this.mIndex - i;
                    float f12 = this.mV[j];
                    float f13 = 0.5F * (this.mT[j] + this.mT[(j + 1)]) - f2;
                    float f14 = this.mT[j] - this.mT[(j + 1)];
                    float f15 = f14 * f14;
                    f8 += f12 * f15;
                    f7 += f13 * (f13 * f15);
                    f6 += f13 * f15;
                    f5 += f12 * (f13 * f15);
                    f4 += f15;
                }
                if (f3 - f1 > 180.0F)
                    f1 += 360.0F;
            }
            float f9 = (f8 * f7 + f6 * f5) / (f4 * f7 + f6 * f6);
            float f10 = 0.002777778F * (f9 + 0.08F * ((f4 * f9 - f8) / f6));
            if (f10 >= 0.0F);
            for (float f11 = f10; ; f11 = -f10)
            {
                if (f11 >= 0.5F)
                    f10 = 1.0F + (f10 - (float)Math.ceil(0.5F + f10));
                if (f10 < 0.0F)
                    f10 += 1.0F;
                return f10 * 360.0F;
            }
        }
    }

    private static final class LegacyListener
        implements SensorEventListener
    {
        private int mSensors;
        private SensorListener mTarget;
        private float[] mValues = new float[6];
        private final LegacySensorManager.LmsFilter mYawfilter = new LegacySensorManager.LmsFilter();

        LegacyListener(SensorListener paramSensorListener)
        {
            this.mTarget = paramSensorListener;
            this.mSensors = 0;
        }

        private static int getLegacySensorType(int paramInt)
        {
            int i;
            switch (paramInt)
            {
            case 4:
            case 5:
            case 6:
            default:
                i = 0;
            case 1:
            case 2:
            case 3:
            case 7:
            }
            while (true)
            {
                return i;
                i = 2;
                continue;
                i = 8;
                continue;
                i = 128;
                continue;
                i = 4;
            }
        }

        private static boolean hasOrientationSensor(int paramInt)
        {
            if ((paramInt & 0x81) != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private void mapSensorDataToWindow(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
        {
            float f1 = paramArrayOfFloat[0];
            float f2 = paramArrayOfFloat[1];
            float f3 = paramArrayOfFloat[2];
            label140: float f4;
            float f5;
            float f6;
            switch (paramInt1)
            {
            default:
                paramArrayOfFloat[0] = f1;
                paramArrayOfFloat[1] = f2;
                paramArrayOfFloat[2] = f3;
                paramArrayOfFloat[3] = f1;
                paramArrayOfFloat[4] = f2;
                paramArrayOfFloat[5] = f3;
                if ((paramInt2 & 0x1) != 0);
                switch (paramInt1)
                {
                default:
                    if ((paramInt2 & 0x2) != 0)
                    {
                        f4 = paramArrayOfFloat[0];
                        f5 = paramArrayOfFloat[1];
                        f6 = paramArrayOfFloat[2];
                        switch (paramInt1)
                        {
                        default:
                        case 2:
                        case 8:
                        case 1:
                        case 128:
                        }
                    }
                    break;
                case 2:
                case 8:
                case 1:
                case 128:
                }
                break;
            case 1:
            case 128:
            case 2:
            case 8:
            }
            while (true)
            {
                return;
                f3 = -f3;
                break;
                f1 = -f1;
                f2 = -f2;
                f3 = -f3;
                break;
                f1 = -f1;
                f2 = -f2;
                break;
                paramArrayOfFloat[0] = (-f2);
                paramArrayOfFloat[1] = f1;
                paramArrayOfFloat[2] = f3;
                break label140;
                if (f1 < 270.0F);
                for (int i = 90; ; i = -270)
                {
                    paramArrayOfFloat[0] = (f1 + i);
                    paramArrayOfFloat[1] = f3;
                    paramArrayOfFloat[2] = f2;
                    break;
                }
                paramArrayOfFloat[0] = (-f4);
                paramArrayOfFloat[1] = (-f5);
                paramArrayOfFloat[2] = f6;
            }
            if (f4 >= 180.0F);
            for (float f7 = f4 - 180.0F; ; f7 = f4 + 180.0F)
            {
                paramArrayOfFloat[0] = f7;
                paramArrayOfFloat[1] = (-f5);
                paramArrayOfFloat[2] = (-f6);
                break;
            }
        }

        boolean hasSensors()
        {
            if (this.mSensors != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onAccuracyChanged(Sensor paramSensor, int paramInt)
        {
            try
            {
                this.mTarget.onAccuracyChanged(getLegacySensorType(paramSensor.getType()), paramInt);
                label17: return;
            }
            catch (AbstractMethodError localAbstractMethodError)
            {
                break label17;
            }
        }

        public void onSensorChanged(SensorEvent paramSensorEvent)
        {
            float[] arrayOfFloat = this.mValues;
            arrayOfFloat[0] = paramSensorEvent.values[0];
            arrayOfFloat[1] = paramSensorEvent.values[1];
            arrayOfFloat[2] = paramSensorEvent.values[2];
            int i = paramSensorEvent.sensor.getType();
            int j = getLegacySensorType(i);
            mapSensorDataToWindow(j, arrayOfFloat, LegacySensorManager.getRotation());
            if (i == 3)
            {
                if ((0x80 & this.mSensors) != 0)
                    this.mTarget.onSensorChanged(128, arrayOfFloat);
                if ((0x1 & this.mSensors) != 0)
                {
                    arrayOfFloat[0] = this.mYawfilter.filter(paramSensorEvent.timestamp, arrayOfFloat[0]);
                    this.mTarget.onSensorChanged(1, arrayOfFloat);
                }
            }
            while (true)
            {
                return;
                this.mTarget.onSensorChanged(j, arrayOfFloat);
            }
        }

        boolean registerSensor(int paramInt)
        {
            boolean bool1 = false;
            if ((paramInt & this.mSensors) != 0);
            while (true)
            {
                return bool1;
                boolean bool2 = hasOrientationSensor(this.mSensors);
                this.mSensors = (paramInt | this.mSensors);
                if ((!bool2) || (!hasOrientationSensor(paramInt)))
                    bool1 = true;
            }
        }

        boolean unregisterSensor(int paramInt)
        {
            boolean bool = false;
            if ((paramInt & this.mSensors) == 0);
            while (true)
            {
                return bool;
                this.mSensors &= (paramInt ^ 0xFFFFFFFF);
                if ((!hasOrientationSensor(paramInt)) || (!hasOrientationSensor(this.mSensors)))
                    bool = true;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.LegacySensorManager
 * JD-Core Version:        0.6.2
 */