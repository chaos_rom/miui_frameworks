package android.hardware;

public class Sensor
{
    public static final int TYPE_ACCELEROMETER = 1;
    public static final int TYPE_ALL = -1;
    public static final int TYPE_AMBIENT_TEMPERATURE = 13;
    public static final int TYPE_GRAVITY = 9;
    public static final int TYPE_GYROSCOPE = 4;
    public static final int TYPE_LIGHT = 5;
    public static final int TYPE_LINEAR_ACCELERATION = 10;
    public static final int TYPE_MAGNETIC_FIELD = 2;

    @Deprecated
    public static final int TYPE_ORIENTATION = 3;
    public static final int TYPE_PRESSURE = 6;
    public static final int TYPE_PROXIMITY = 8;
    public static final int TYPE_RELATIVE_HUMIDITY = 12;
    public static final int TYPE_ROTATION_VECTOR = 11;

    @Deprecated
    public static final int TYPE_TEMPERATURE = 7;
    private int mHandle;
    private float mMaxRange;
    private int mMinDelay;
    private String mName;
    private float mPower;
    private float mResolution;
    private int mType;
    private String mVendor;
    private int mVersion;

    int getHandle()
    {
        return this.mHandle;
    }

    public float getMaximumRange()
    {
        return this.mMaxRange;
    }

    public int getMinDelay()
    {
        return this.mMinDelay;
    }

    public String getName()
    {
        return this.mName;
    }

    public float getPower()
    {
        return this.mPower;
    }

    public float getResolution()
    {
        return this.mResolution;
    }

    public int getType()
    {
        return this.mType;
    }

    public String getVendor()
    {
        return this.mVendor;
    }

    public int getVersion()
    {
        return this.mVersion;
    }

    void setRange(float paramFloat1, float paramFloat2)
    {
        this.mMaxRange = paramFloat1;
        this.mResolution = paramFloat2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.Sensor
 * JD-Core Version:        0.6.2
 */