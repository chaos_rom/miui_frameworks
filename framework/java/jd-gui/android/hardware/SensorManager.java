package android.hardware;

import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class SensorManager
{
    public static final int AXIS_MINUS_X = 129;
    public static final int AXIS_MINUS_Y = 130;
    public static final int AXIS_MINUS_Z = 131;
    public static final int AXIS_X = 1;
    public static final int AXIS_Y = 2;
    public static final int AXIS_Z = 3;

    @Deprecated
    public static final int DATA_X = 0;

    @Deprecated
    public static final int DATA_Y = 1;

    @Deprecated
    public static final int DATA_Z = 2;
    public static final float GRAVITY_DEATH_STAR_I = 3.530361E-07F;
    public static final float GRAVITY_EARTH = 9.80665F;
    public static final float GRAVITY_JUPITER = 23.120001F;
    public static final float GRAVITY_MARS = 3.71F;
    public static final float GRAVITY_MERCURY = 3.7F;
    public static final float GRAVITY_MOON = 1.6F;
    public static final float GRAVITY_NEPTUNE = 11.0F;
    public static final float GRAVITY_PLUTO = 0.6F;
    public static final float GRAVITY_SATURN = 8.96F;
    public static final float GRAVITY_SUN = 275.0F;
    public static final float GRAVITY_THE_ISLAND = 4.815162F;
    public static final float GRAVITY_URANUS = 8.69F;
    public static final float GRAVITY_VENUS = 8.87F;
    public static final float LIGHT_CLOUDY = 100.0F;
    public static final float LIGHT_FULLMOON = 0.25F;
    public static final float LIGHT_NO_MOON = 0.001F;
    public static final float LIGHT_OVERCAST = 10000.0F;
    public static final float LIGHT_SHADE = 20000.0F;
    public static final float LIGHT_SUNLIGHT = 110000.0F;
    public static final float LIGHT_SUNLIGHT_MAX = 120000.0F;
    public static final float LIGHT_SUNRISE = 400.0F;
    public static final float MAGNETIC_FIELD_EARTH_MAX = 60.0F;
    public static final float MAGNETIC_FIELD_EARTH_MIN = 30.0F;
    public static final float PRESSURE_STANDARD_ATMOSPHERE = 1013.25F;

    @Deprecated
    public static final int RAW_DATA_INDEX = 3;

    @Deprecated
    public static final int RAW_DATA_X = 3;

    @Deprecated
    public static final int RAW_DATA_Y = 4;

    @Deprecated
    public static final int RAW_DATA_Z = 5;

    @Deprecated
    public static final int SENSOR_ACCELEROMETER = 2;

    @Deprecated
    public static final int SENSOR_ALL = 127;
    public static final int SENSOR_DELAY_FASTEST = 0;
    public static final int SENSOR_DELAY_GAME = 1;
    public static final int SENSOR_DELAY_NORMAL = 3;
    public static final int SENSOR_DELAY_UI = 2;

    @Deprecated
    public static final int SENSOR_LIGHT = 16;

    @Deprecated
    public static final int SENSOR_MAGNETIC_FIELD = 8;

    @Deprecated
    public static final int SENSOR_MAX = 64;

    @Deprecated
    public static final int SENSOR_MIN = 1;

    @Deprecated
    public static final int SENSOR_ORIENTATION = 1;

    @Deprecated
    public static final int SENSOR_ORIENTATION_RAW = 128;

    @Deprecated
    public static final int SENSOR_PROXIMITY = 32;
    public static final int SENSOR_STATUS_ACCURACY_HIGH = 3;
    public static final int SENSOR_STATUS_ACCURACY_LOW = 1;
    public static final int SENSOR_STATUS_ACCURACY_MEDIUM = 2;
    public static final int SENSOR_STATUS_UNRELIABLE = 0;

    @Deprecated
    public static final int SENSOR_TEMPERATURE = 4;

    @Deprecated
    public static final int SENSOR_TRICORDER = 64;
    public static final float STANDARD_GRAVITY = 9.80665F;
    protected static final String TAG = "SensorManager";
    private static final float[] mTempMatrix = new float[16];
    private LegacySensorManager mLegacySensorManager;
    private final SparseArray<List<Sensor>> mSensorListByType = new SparseArray();

    public static float getAltitude(float paramFloat1, float paramFloat2)
    {
        return 44330.0F * (1.0F - (float)Math.pow(paramFloat2 / paramFloat1, 0.1902949512004852D));
    }

    public static void getAngleChange(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, float[] paramArrayOfFloat3)
    {
        float f1 = 0.0F;
        float f2 = 0.0F;
        float f3 = 0.0F;
        float f4 = 0.0F;
        float f5 = 0.0F;
        float f6 = 0.0F;
        float f7 = 0.0F;
        float f8 = 0.0F;
        float f9 = 0.0F;
        float f10 = 0.0F;
        float f11 = 0.0F;
        float f12 = 0.0F;
        float f13 = 0.0F;
        float f14 = 0.0F;
        float f15 = 0.0F;
        float f16 = 0.0F;
        float f17 = 0.0F;
        float f18 = 0.0F;
        if (paramArrayOfFloat2.length == 9)
        {
            f1 = paramArrayOfFloat2[0];
            f2 = paramArrayOfFloat2[1];
            f3 = paramArrayOfFloat2[2];
            f4 = paramArrayOfFloat2[3];
            f5 = paramArrayOfFloat2[4];
            f6 = paramArrayOfFloat2[5];
            f7 = paramArrayOfFloat2[6];
            f8 = paramArrayOfFloat2[7];
            f9 = paramArrayOfFloat2[8];
            if (paramArrayOfFloat3.length != 9)
                break label353;
            f10 = paramArrayOfFloat3[0];
            f11 = paramArrayOfFloat3[1];
            f12 = paramArrayOfFloat3[2];
            f13 = paramArrayOfFloat3[3];
            f14 = paramArrayOfFloat3[4];
            f15 = paramArrayOfFloat3[5];
            f16 = paramArrayOfFloat3[6];
            f17 = paramArrayOfFloat3[7];
            f18 = paramArrayOfFloat3[8];
        }
        while (true)
        {
            float f19 = f10 * f2 + f13 * f5 + f16 * f8;
            float f20 = f11 * f2 + f14 * f5 + f17 * f8;
            float f21 = f12 * f1 + f15 * f4 + f18 * f7;
            float f22 = f12 * f2 + f15 * f5 + f18 * f8;
            float f23 = f12 * f3 + f15 * f6 + f18 * f9;
            paramArrayOfFloat1[0] = ((float)Math.atan2(f19, f20));
            paramArrayOfFloat1[1] = ((float)Math.asin(-f22));
            paramArrayOfFloat1[2] = ((float)Math.atan2(-f21, f23));
            return;
            if (paramArrayOfFloat2.length != 16)
                break;
            f1 = paramArrayOfFloat2[0];
            f2 = paramArrayOfFloat2[1];
            f3 = paramArrayOfFloat2[2];
            f4 = paramArrayOfFloat2[4];
            f5 = paramArrayOfFloat2[5];
            f6 = paramArrayOfFloat2[6];
            f7 = paramArrayOfFloat2[8];
            f8 = paramArrayOfFloat2[9];
            f9 = paramArrayOfFloat2[10];
            break;
            label353: if (paramArrayOfFloat3.length == 16)
            {
                f10 = paramArrayOfFloat3[0];
                f11 = paramArrayOfFloat3[1];
                f12 = paramArrayOfFloat3[2];
                f13 = paramArrayOfFloat3[4];
                f14 = paramArrayOfFloat3[5];
                f15 = paramArrayOfFloat3[6];
                f16 = paramArrayOfFloat3[8];
                f17 = paramArrayOfFloat3[9];
                f18 = paramArrayOfFloat3[10];
            }
        }
    }

    public static float getInclination(float[] paramArrayOfFloat)
    {
        if (paramArrayOfFloat.length == 9);
        for (float f = (float)Math.atan2(paramArrayOfFloat[5], paramArrayOfFloat[4]); ; f = (float)Math.atan2(paramArrayOfFloat[6], paramArrayOfFloat[5]))
            return f;
    }

    private LegacySensorManager getLegacySensorManager()
    {
        synchronized (this.mSensorListByType)
        {
            if (this.mLegacySensorManager == null)
            {
                Log.i("SensorManager", "This application is using deprecated SensorManager API which will be removed someday.    Please consider switching to the new API.");
                this.mLegacySensorManager = new LegacySensorManager(this);
            }
            LegacySensorManager localLegacySensorManager = this.mLegacySensorManager;
            return localLegacySensorManager;
        }
    }

    public static float[] getOrientation(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        if (paramArrayOfFloat1.length == 9)
        {
            paramArrayOfFloat2[0] = ((float)Math.atan2(paramArrayOfFloat1[1], paramArrayOfFloat1[4]));
            paramArrayOfFloat2[1] = ((float)Math.asin(-paramArrayOfFloat1[7]));
            paramArrayOfFloat2[2] = ((float)Math.atan2(-paramArrayOfFloat1[6], paramArrayOfFloat1[8]));
        }
        while (true)
        {
            return paramArrayOfFloat2;
            paramArrayOfFloat2[0] = ((float)Math.atan2(paramArrayOfFloat1[1], paramArrayOfFloat1[5]));
            paramArrayOfFloat2[1] = ((float)Math.asin(-paramArrayOfFloat1[9]));
            paramArrayOfFloat2[2] = ((float)Math.atan2(-paramArrayOfFloat1[8], paramArrayOfFloat1[10]));
        }
    }

    public static void getQuaternionFromVector(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        float f = 0.0F;
        if (paramArrayOfFloat2.length == 4)
            paramArrayOfFloat1[0] = paramArrayOfFloat2[3];
        while (true)
        {
            paramArrayOfFloat1[1] = paramArrayOfFloat2[0];
            paramArrayOfFloat1[2] = paramArrayOfFloat2[1];
            paramArrayOfFloat1[3] = paramArrayOfFloat2[2];
            return;
            paramArrayOfFloat1[0] = (1.0F - paramArrayOfFloat2[0] * paramArrayOfFloat2[0] - paramArrayOfFloat2[1] * paramArrayOfFloat2[1] - paramArrayOfFloat2[2] * paramArrayOfFloat2[2]);
            if (paramArrayOfFloat1[0] > 0.0F)
                f = (float)Math.sqrt(paramArrayOfFloat1[0]);
            paramArrayOfFloat1[0] = f;
        }
    }

    public static boolean getRotationMatrix(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, float[] paramArrayOfFloat3, float[] paramArrayOfFloat4)
    {
        float f1 = paramArrayOfFloat3[0];
        float f2 = paramArrayOfFloat3[1];
        float f3 = paramArrayOfFloat3[2];
        float f4 = paramArrayOfFloat4[0];
        float f5 = paramArrayOfFloat4[1];
        float f6 = paramArrayOfFloat4[2];
        float f7 = f5 * f3 - f6 * f2;
        float f8 = f6 * f1 - f4 * f3;
        float f9 = f4 * f2 - f5 * f1;
        float f10 = (float)Math.sqrt(f7 * f7 + f8 * f8 + f9 * f9);
        boolean bool;
        if (f10 < 0.1F)
        {
            bool = false;
            return bool;
        }
        float f11 = 1.0F / f10;
        float f12 = f7 * f11;
        float f13 = f8 * f11;
        float f14 = f9 * f11;
        float f15 = 1.0F / (float)Math.sqrt(f1 * f1 + f2 * f2 + f3 * f3);
        float f16 = f1 * f15;
        float f17 = f2 * f15;
        float f18 = f3 * f15;
        float f19 = f17 * f14 - f18 * f13;
        float f20 = f18 * f12 - f16 * f14;
        float f21 = f16 * f13 - f17 * f12;
        label279: float f23;
        float f24;
        if (paramArrayOfFloat1 != null)
        {
            if (paramArrayOfFloat1.length == 9)
            {
                paramArrayOfFloat1[0] = f12;
                paramArrayOfFloat1[1] = f13;
                paramArrayOfFloat1[2] = f14;
                paramArrayOfFloat1[3] = f19;
                paramArrayOfFloat1[4] = f20;
                paramArrayOfFloat1[5] = f21;
                paramArrayOfFloat1[6] = f16;
                paramArrayOfFloat1[7] = f17;
                paramArrayOfFloat1[8] = f18;
            }
        }
        else if (paramArrayOfFloat2 != null)
        {
            float f22 = 1.0F / (float)Math.sqrt(f4 * f4 + f5 * f5 + f6 * f6);
            f23 = f22 * (f4 * f19 + f5 * f20 + f6 * f21);
            f24 = f22 * (f4 * f16 + f5 * f17 + f6 * f18);
            if (paramArrayOfFloat2.length != 9)
                break label503;
            paramArrayOfFloat2[0] = 1.0F;
            paramArrayOfFloat2[1] = 0.0F;
            paramArrayOfFloat2[2] = 0.0F;
            paramArrayOfFloat2[3] = 0.0F;
            paramArrayOfFloat2[4] = f23;
            paramArrayOfFloat2[5] = f24;
            paramArrayOfFloat2[6] = 0.0F;
            paramArrayOfFloat2[7] = (-f24);
            paramArrayOfFloat2[8] = f23;
        }
        while (true)
        {
            bool = true;
            break;
            if (paramArrayOfFloat1.length != 16)
                break label279;
            paramArrayOfFloat1[0] = f12;
            paramArrayOfFloat1[1] = f13;
            paramArrayOfFloat1[2] = f14;
            paramArrayOfFloat1[3] = 0.0F;
            paramArrayOfFloat1[4] = f19;
            paramArrayOfFloat1[5] = f20;
            paramArrayOfFloat1[6] = f21;
            paramArrayOfFloat1[7] = 0.0F;
            paramArrayOfFloat1[8] = f16;
            paramArrayOfFloat1[9] = f17;
            paramArrayOfFloat1[10] = f18;
            paramArrayOfFloat1[11] = 0.0F;
            paramArrayOfFloat1[12] = 0.0F;
            paramArrayOfFloat1[13] = 0.0F;
            paramArrayOfFloat1[14] = 0.0F;
            paramArrayOfFloat1[15] = 1.0F;
            break label279;
            label503: if (paramArrayOfFloat2.length == 16)
            {
                paramArrayOfFloat2[0] = 1.0F;
                paramArrayOfFloat2[1] = 0.0F;
                paramArrayOfFloat2[2] = 0.0F;
                paramArrayOfFloat2[4] = 0.0F;
                paramArrayOfFloat2[5] = f23;
                paramArrayOfFloat2[6] = f24;
                paramArrayOfFloat2[8] = 0.0F;
                paramArrayOfFloat2[9] = (-f24);
                paramArrayOfFloat2[10] = f23;
                paramArrayOfFloat2[14] = 0.0F;
                paramArrayOfFloat2[13] = 0.0F;
                paramArrayOfFloat2[12] = 0.0F;
                paramArrayOfFloat2[11] = 0.0F;
                paramArrayOfFloat2[7] = 0.0F;
                paramArrayOfFloat2[3] = 0.0F;
                paramArrayOfFloat2[15] = 1.0F;
            }
        }
    }

    public static void getRotationMatrixFromVector(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
    {
        float f1 = paramArrayOfFloat2[0];
        float f2 = paramArrayOfFloat2[1];
        float f3 = paramArrayOfFloat2[2];
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        float f11;
        float f12;
        float f13;
        float f14;
        if (paramArrayOfFloat2.length == 4)
        {
            f5 = paramArrayOfFloat2[3];
            f6 = f1 * (2.0F * f1);
            f7 = f2 * (2.0F * f2);
            f8 = f3 * (2.0F * f3);
            f9 = f2 * (2.0F * f1);
            f10 = f5 * (2.0F * f3);
            f11 = f3 * (2.0F * f1);
            f12 = f5 * (2.0F * f2);
            f13 = f3 * (2.0F * f2);
            f14 = f5 * (2.0F * f1);
            if (paramArrayOfFloat1.length != 9)
                break label226;
            paramArrayOfFloat1[0] = (1.0F - f7 - f8);
            paramArrayOfFloat1[1] = (f9 - f10);
            paramArrayOfFloat1[2] = (f11 + f12);
            paramArrayOfFloat1[3] = (f9 + f10);
            paramArrayOfFloat1[4] = (1.0F - f6 - f8);
            paramArrayOfFloat1[5] = (f13 - f14);
            paramArrayOfFloat1[6] = (f11 - f12);
            paramArrayOfFloat1[7] = (f13 + f14);
            paramArrayOfFloat1[8] = (1.0F - f6 - f7);
        }
        while (true)
        {
            return;
            float f4 = 1.0F - f1 * f1 - f2 * f2 - f3 * f3;
            if (f4 > 0.0F);
            for (f5 = (float)Math.sqrt(f4); ; f5 = 0.0F)
                break;
            label226: if (paramArrayOfFloat1.length == 16)
            {
                paramArrayOfFloat1[0] = (1.0F - f7 - f8);
                paramArrayOfFloat1[1] = (f9 - f10);
                paramArrayOfFloat1[2] = (f11 + f12);
                paramArrayOfFloat1[3] = 0.0F;
                paramArrayOfFloat1[4] = (f9 + f10);
                paramArrayOfFloat1[5] = (1.0F - f6 - f8);
                paramArrayOfFloat1[6] = (f13 - f14);
                paramArrayOfFloat1[7] = 0.0F;
                paramArrayOfFloat1[8] = (f11 - f12);
                paramArrayOfFloat1[9] = (f13 + f14);
                paramArrayOfFloat1[10] = (1.0F - f6 - f7);
                paramArrayOfFloat1[11] = 0.0F;
                paramArrayOfFloat1[14] = 0.0F;
                paramArrayOfFloat1[13] = 0.0F;
                paramArrayOfFloat1[12] = 0.0F;
                paramArrayOfFloat1[15] = 1.0F;
            }
        }
    }

    public static boolean remapCoordinateSystem(float[] paramArrayOfFloat1, int paramInt1, int paramInt2, float[] paramArrayOfFloat2)
    {
        if (paramArrayOfFloat1 == paramArrayOfFloat2);
        boolean bool;
        synchronized (mTempMatrix)
        {
            if (remapCoordinateSystemImpl(paramArrayOfFloat1, paramInt1, paramInt2, ???))
            {
                int i = paramArrayOfFloat2.length;
                for (int j = 0; j < i; j++)
                    paramArrayOfFloat2[j] = ???[j];
                bool = true;
            }
            else
            {
                bool = remapCoordinateSystemImpl(paramArrayOfFloat1, paramInt1, paramInt2, paramArrayOfFloat2);
            }
        }
        return bool;
    }

    private static boolean remapCoordinateSystemImpl(float[] paramArrayOfFloat1, int paramInt1, int paramInt2, float[] paramArrayOfFloat2)
    {
        int i = paramArrayOfFloat2.length;
        boolean bool;
        if (paramArrayOfFloat1.length != i)
            bool = false;
        while (true)
        {
            return bool;
            if (((paramInt1 & 0x7C) != 0) || ((paramInt2 & 0x7C) != 0))
            {
                bool = false;
            }
            else if (((paramInt1 & 0x3) == 0) || ((paramInt2 & 0x3) == 0))
            {
                bool = false;
            }
            else if ((paramInt1 & 0x3) == (paramInt2 & 0x3))
            {
                bool = false;
            }
            else
            {
                int j = paramInt1 ^ paramInt2;
                int k = -1 + (paramInt1 & 0x3);
                int m = -1 + (paramInt2 & 0x3);
                int n = -1 + (j & 0x3);
                int i1 = (n + 1) % 3;
                int i2 = (n + 2) % 3;
                if ((k ^ i1 | m ^ i2) != 0)
                    j ^= 128;
                int i3;
                int i4;
                label158: int i5;
                label169: int i6;
                if (paramInt1 >= 128)
                {
                    i3 = 1;
                    if (paramInt2 < 128)
                        break label318;
                    i4 = 1;
                    if (j < 128)
                        break label324;
                    i5 = 1;
                    if (i != 16)
                        break label330;
                    i6 = 4;
                }
                label179: label318: label324: for (int i7 = 0; ; i7++)
                {
                    if (i7 >= 3)
                        break label375;
                    int i8 = i7 * i6;
                    int i9 = 0;
                    label198: if (i9 < 3)
                    {
                        float f3;
                        label232: float f2;
                        int i10;
                        if (k == i9)
                        {
                            int i12 = i8 + i9;
                            if (i3 != 0)
                            {
                                f3 = -paramArrayOfFloat1[(i8 + 0)];
                                paramArrayOfFloat2[i12] = f3;
                            }
                        }
                        else
                        {
                            if (m == i9)
                            {
                                int i11 = i8 + i9;
                                if (i4 == 0)
                                    break label347;
                                f2 = -paramArrayOfFloat1[(i8 + 1)];
                                paramArrayOfFloat2[i11] = f2;
                            }
                            if (n == i9)
                            {
                                i10 = i8 + i9;
                                if (i5 == 0)
                                    break label358;
                            }
                        }
                        label330: label347: label358: for (float f1 = -paramArrayOfFloat1[(i8 + 2)]; ; f1 = paramArrayOfFloat1[(i8 + 2)])
                        {
                            paramArrayOfFloat2[i10] = f1;
                            i9++;
                            break label198;
                            i3 = 0;
                            break;
                            i4 = 0;
                            break label158;
                            i5 = 0;
                            break label169;
                            i6 = 3;
                            break label179;
                            f3 = paramArrayOfFloat1[(i8 + 0)];
                            break label232;
                            f2 = paramArrayOfFloat1[(i8 + 1)];
                            break label266;
                        }
                    }
                }
                label266: label375: if (i == 16)
                {
                    paramArrayOfFloat2[14] = 0.0F;
                    paramArrayOfFloat2[13] = 0.0F;
                    paramArrayOfFloat2[12] = 0.0F;
                    paramArrayOfFloat2[11] = 0.0F;
                    paramArrayOfFloat2[7] = 0.0F;
                    paramArrayOfFloat2[3] = 0.0F;
                    paramArrayOfFloat2[15] = 1.0F;
                }
                bool = true;
            }
        }
    }

    public Sensor getDefaultSensor(int paramInt)
    {
        List localList = getSensorList(paramInt);
        if (localList.isEmpty());
        for (Sensor localSensor = null; ; localSensor = (Sensor)localList.get(0))
            return localSensor;
    }

    protected abstract List<Sensor> getFullSensorList();

    public List<Sensor> getSensorList(int paramInt)
    {
        List localList1 = getFullSensorList();
        synchronized (this.mSensorListByType)
        {
            List localList2 = (List)this.mSensorListByType.get(paramInt);
            Object localObject2;
            if (localList2 == null)
            {
                if (paramInt != -1)
                    break label61;
                localObject2 = localList1;
            }
            label61: Sensor localSensor;
            do
            {
                Iterator localIterator;
                while (!localIterator.hasNext())
                {
                    localList2 = Collections.unmodifiableList((List)localObject2);
                    this.mSensorListByType.append(paramInt, localList2);
                    return localList2;
                    localObject2 = new ArrayList();
                    localIterator = localList1.iterator();
                }
                localSensor = (Sensor)localIterator.next();
            }
            while (localSensor.getType() != paramInt);
            ((List)localObject2).add(localSensor);
        }
    }

    @Deprecated
    public int getSensors()
    {
        return getLegacySensorManager().getSensors();
    }

    public boolean registerListener(SensorEventListener paramSensorEventListener, Sensor paramSensor, int paramInt)
    {
        return registerListener(paramSensorEventListener, paramSensor, paramInt, null);
    }

    public boolean registerListener(SensorEventListener paramSensorEventListener, Sensor paramSensor, int paramInt, Handler paramHandler)
    {
        boolean bool;
        if ((paramSensorEventListener == null) || (paramSensor == null))
        {
            bool = false;
            return bool;
        }
        int i;
        switch (paramInt)
        {
        default:
            i = paramInt;
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            bool = registerListenerImpl(paramSensorEventListener, paramSensor, i, paramHandler);
            break;
            i = 0;
            continue;
            i = 20000;
            continue;
            i = 66667;
            continue;
            i = 200000;
        }
    }

    @Deprecated
    public boolean registerListener(SensorListener paramSensorListener, int paramInt)
    {
        return registerListener(paramSensorListener, paramInt, 3);
    }

    @Deprecated
    public boolean registerListener(SensorListener paramSensorListener, int paramInt1, int paramInt2)
    {
        return getLegacySensorManager().registerListener(paramSensorListener, paramInt1, paramInt2);
    }

    protected abstract boolean registerListenerImpl(SensorEventListener paramSensorEventListener, Sensor paramSensor, int paramInt, Handler paramHandler);

    public void unregisterListener(SensorEventListener paramSensorEventListener)
    {
        if (paramSensorEventListener == null);
        while (true)
        {
            return;
            unregisterListenerImpl(paramSensorEventListener, null);
        }
    }

    public void unregisterListener(SensorEventListener paramSensorEventListener, Sensor paramSensor)
    {
        if ((paramSensorEventListener == null) || (paramSensor == null));
        while (true)
        {
            return;
            unregisterListenerImpl(paramSensorEventListener, paramSensor);
        }
    }

    @Deprecated
    public void unregisterListener(SensorListener paramSensorListener)
    {
        unregisterListener(paramSensorListener, 255);
    }

    @Deprecated
    public void unregisterListener(SensorListener paramSensorListener, int paramInt)
    {
        getLegacySensorManager().unregisterListener(paramSensorListener, paramInt);
    }

    protected abstract void unregisterListenerImpl(SensorEventListener paramSensorEventListener, Sensor paramSensor);

    protected static final class SensorEventPool
    {
        private int mNumItemsInPool;
        private final SensorEvent[] mPool;
        private final int mPoolSize;

        SensorEventPool(int paramInt)
        {
            this.mPoolSize = paramInt;
            this.mNumItemsInPool = paramInt;
            this.mPool = new SensorEvent[paramInt];
        }

        private SensorEvent createSensorEvent()
        {
            return new SensorEvent(3);
        }

        SensorEvent getFromPool()
        {
            SensorEvent localSensorEvent = null;
            try
            {
                if (this.mNumItemsInPool > 0)
                {
                    int i = this.mPoolSize - this.mNumItemsInPool;
                    localSensorEvent = this.mPool[i];
                    this.mPool[i] = null;
                    this.mNumItemsInPool = (-1 + this.mNumItemsInPool);
                }
                if (localSensorEvent == null)
                    localSensorEvent = createSensorEvent();
                return localSensorEvent;
            }
            finally
            {
            }
        }

        void returnToPool(SensorEvent paramSensorEvent)
        {
            try
            {
                if (this.mNumItemsInPool < this.mPoolSize)
                {
                    this.mNumItemsInPool = (1 + this.mNumItemsInPool);
                    int i = this.mPoolSize - this.mNumItemsInPool;
                    this.mPool[i] = paramSensorEvent;
                }
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.SensorManager
 * JD-Core Version:        0.6.2
 */