package android.hardware.usb;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class UsbAccessory
    implements Parcelable
{
    public static final Parcelable.Creator<UsbAccessory> CREATOR = new Parcelable.Creator()
    {
        public UsbAccessory createFromParcel(Parcel paramAnonymousParcel)
        {
            return new UsbAccessory(paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), paramAnonymousParcel.readString());
        }

        public UsbAccessory[] newArray(int paramAnonymousInt)
        {
            return new UsbAccessory[paramAnonymousInt];
        }
    };
    public static final int DESCRIPTION_STRING = 2;
    public static final int MANUFACTURER_STRING = 0;
    public static final int MODEL_STRING = 1;
    public static final int SERIAL_STRING = 5;
    private static final String TAG = "UsbAccessory";
    public static final int URI_STRING = 4;
    public static final int VERSION_STRING = 3;
    private final String mDescription;
    private final String mManufacturer;
    private final String mModel;
    private final String mSerial;
    private final String mUri;
    private final String mVersion;

    public UsbAccessory(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
    {
        this.mManufacturer = paramString1;
        this.mModel = paramString2;
        this.mDescription = paramString3;
        this.mVersion = paramString4;
        this.mUri = paramString5;
        this.mSerial = paramString6;
    }

    public UsbAccessory(String[] paramArrayOfString)
    {
        this.mManufacturer = paramArrayOfString[0];
        this.mModel = paramArrayOfString[1];
        this.mDescription = paramArrayOfString[2];
        this.mVersion = paramArrayOfString[3];
        this.mUri = paramArrayOfString[4];
        this.mSerial = paramArrayOfString[5];
    }

    private static boolean compare(String paramString1, String paramString2)
    {
        boolean bool;
        if (paramString1 == null)
            if (paramString2 == null)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = paramString1.equals(paramString2);
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if ((paramObject instanceof UsbAccessory))
        {
            UsbAccessory localUsbAccessory = (UsbAccessory)paramObject;
            if ((compare(this.mManufacturer, localUsbAccessory.getManufacturer())) && (compare(this.mModel, localUsbAccessory.getModel())) && (compare(this.mDescription, localUsbAccessory.getDescription())) && (compare(this.mVersion, localUsbAccessory.getVersion())) && (compare(this.mUri, localUsbAccessory.getUri())) && (compare(this.mSerial, localUsbAccessory.getSerial())))
                bool = true;
        }
        return bool;
    }

    public String getDescription()
    {
        return this.mDescription;
    }

    public String getManufacturer()
    {
        return this.mManufacturer;
    }

    public String getModel()
    {
        return this.mModel;
    }

    public String getSerial()
    {
        return this.mSerial;
    }

    public String getUri()
    {
        return this.mUri;
    }

    public String getVersion()
    {
        return this.mVersion;
    }

    public int hashCode()
    {
        int i = 0;
        int j;
        int k;
        label20: int n;
        label35: int i2;
        label52: int i4;
        label69: int i5;
        if (this.mManufacturer == null)
        {
            j = 0;
            if (this.mModel != null)
                break label99;
            k = 0;
            int m = k ^ j;
            if (this.mDescription != null)
                break label110;
            n = 0;
            int i1 = m ^ n;
            if (this.mVersion != null)
                break label122;
            i2 = 0;
            int i3 = i1 ^ i2;
            if (this.mUri != null)
                break label134;
            i4 = 0;
            i5 = i4 ^ i3;
            if (this.mSerial != null)
                break label146;
        }
        while (true)
        {
            return i5 ^ i;
            j = this.mManufacturer.hashCode();
            break;
            label99: k = this.mModel.hashCode();
            break label20;
            label110: n = this.mDescription.hashCode();
            break label35;
            label122: i2 = this.mVersion.hashCode();
            break label52;
            label134: i4 = this.mUri.hashCode();
            break label69;
            label146: i = this.mSerial.hashCode();
        }
    }

    public String toString()
    {
        return "UsbAccessory[mManufacturer=" + this.mManufacturer + ", mModel=" + this.mModel + ", mDescription=" + this.mDescription + ", mVersion=" + this.mVersion + ", mUri=" + this.mUri + ", mSerial=" + this.mSerial + "]";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mManufacturer);
        paramParcel.writeString(this.mModel);
        paramParcel.writeString(this.mDescription);
        paramParcel.writeString(this.mVersion);
        paramParcel.writeString(this.mUri);
        paramParcel.writeString(this.mSerial);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.usb.UsbAccessory
 * JD-Core Version:        0.6.2
 */