package android.hardware.usb;

import android.app.PendingIntent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IUsbManager extends IInterface
{
    public abstract void clearDefaults(String paramString)
        throws RemoteException;

    public abstract UsbAccessory getCurrentAccessory()
        throws RemoteException;

    public abstract void getDeviceList(Bundle paramBundle)
        throws RemoteException;

    public abstract void grantAccessoryPermission(UsbAccessory paramUsbAccessory, int paramInt)
        throws RemoteException;

    public abstract void grantDevicePermission(UsbDevice paramUsbDevice, int paramInt)
        throws RemoteException;

    public abstract boolean hasAccessoryPermission(UsbAccessory paramUsbAccessory)
        throws RemoteException;

    public abstract boolean hasDefaults(String paramString)
        throws RemoteException;

    public abstract boolean hasDevicePermission(UsbDevice paramUsbDevice)
        throws RemoteException;

    public abstract ParcelFileDescriptor openAccessory(UsbAccessory paramUsbAccessory)
        throws RemoteException;

    public abstract ParcelFileDescriptor openDevice(String paramString)
        throws RemoteException;

    public abstract void requestAccessoryPermission(UsbAccessory paramUsbAccessory, String paramString, PendingIntent paramPendingIntent)
        throws RemoteException;

    public abstract void requestDevicePermission(UsbDevice paramUsbDevice, String paramString, PendingIntent paramPendingIntent)
        throws RemoteException;

    public abstract void setAccessoryPackage(UsbAccessory paramUsbAccessory, String paramString)
        throws RemoteException;

    public abstract void setCurrentFunction(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void setDevicePackage(UsbDevice paramUsbDevice, String paramString)
        throws RemoteException;

    public abstract void setMassStorageBackingFile(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IUsbManager
    {
        private static final String DESCRIPTOR = "android.hardware.usb.IUsbManager";
        static final int TRANSACTION_clearDefaults = 14;
        static final int TRANSACTION_getCurrentAccessory = 3;
        static final int TRANSACTION_getDeviceList = 1;
        static final int TRANSACTION_grantAccessoryPermission = 12;
        static final int TRANSACTION_grantDevicePermission = 11;
        static final int TRANSACTION_hasAccessoryPermission = 8;
        static final int TRANSACTION_hasDefaults = 13;
        static final int TRANSACTION_hasDevicePermission = 7;
        static final int TRANSACTION_openAccessory = 4;
        static final int TRANSACTION_openDevice = 2;
        static final int TRANSACTION_requestAccessoryPermission = 10;
        static final int TRANSACTION_requestDevicePermission = 9;
        static final int TRANSACTION_setAccessoryPackage = 6;
        static final int TRANSACTION_setCurrentFunction = 15;
        static final int TRANSACTION_setDevicePackage = 5;
        static final int TRANSACTION_setMassStorageBackingFile = 16;

        public Stub()
        {
            attachInterface(this, "android.hardware.usb.IUsbManager");
        }

        public static IUsbManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.hardware.usb.IUsbManager");
                if ((localIInterface != null) && ((localIInterface instanceof IUsbManager)))
                    localObject = (IUsbManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.hardware.usb.IUsbManager");
                continue;
                paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                Bundle localBundle = new Bundle();
                getDeviceList(localBundle);
                paramParcel2.writeNoException();
                if (localBundle != null)
                {
                    paramParcel2.writeInt(j);
                    localBundle.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                    ParcelFileDescriptor localParcelFileDescriptor2 = openDevice(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (localParcelFileDescriptor2 != null)
                    {
                        paramParcel2.writeInt(j);
                        localParcelFileDescriptor2.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                        UsbAccessory localUsbAccessory6 = getCurrentAccessory();
                        paramParcel2.writeNoException();
                        if (localUsbAccessory6 != null)
                        {
                            paramParcel2.writeInt(j);
                            localUsbAccessory6.writeToParcel(paramParcel2, j);
                        }
                        else
                        {
                            paramParcel2.writeInt(0);
                            continue;
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            if (paramParcel1.readInt() != 0);
                            for (UsbAccessory localUsbAccessory5 = (UsbAccessory)UsbAccessory.CREATOR.createFromParcel(paramParcel1); ; localUsbAccessory5 = null)
                            {
                                ParcelFileDescriptor localParcelFileDescriptor1 = openAccessory(localUsbAccessory5);
                                paramParcel2.writeNoException();
                                if (localParcelFileDescriptor1 == null)
                                    break label393;
                                paramParcel2.writeInt(j);
                                localParcelFileDescriptor1.writeToParcel(paramParcel2, j);
                                break;
                            }
                            label393: paramParcel2.writeInt(0);
                            continue;
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            if (paramParcel1.readInt() != 0);
                            for (UsbDevice localUsbDevice4 = (UsbDevice)UsbDevice.CREATOR.createFromParcel(paramParcel1); ; localUsbDevice4 = null)
                            {
                                setDevicePackage(localUsbDevice4, paramParcel1.readString());
                                paramParcel2.writeNoException();
                                break;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            if (paramParcel1.readInt() != 0);
                            for (UsbAccessory localUsbAccessory4 = (UsbAccessory)UsbAccessory.CREATOR.createFromParcel(paramParcel1); ; localUsbAccessory4 = null)
                            {
                                setAccessoryPackage(localUsbAccessory4, paramParcel1.readString());
                                paramParcel2.writeNoException();
                                break;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            if (paramParcel1.readInt() != 0);
                            for (UsbDevice localUsbDevice3 = (UsbDevice)UsbDevice.CREATOR.createFromParcel(paramParcel1); ; localUsbDevice3 = null)
                            {
                                boolean bool3 = hasDevicePermission(localUsbDevice3);
                                paramParcel2.writeNoException();
                                if (bool3)
                                    i = j;
                                paramParcel2.writeInt(i);
                                break;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            if (paramParcel1.readInt() != 0);
                            for (UsbAccessory localUsbAccessory3 = (UsbAccessory)UsbAccessory.CREATOR.createFromParcel(paramParcel1); ; localUsbAccessory3 = null)
                            {
                                boolean bool2 = hasAccessoryPermission(localUsbAccessory3);
                                paramParcel2.writeNoException();
                                if (bool2)
                                    i = j;
                                paramParcel2.writeInt(i);
                                break;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            UsbDevice localUsbDevice2;
                            label654: String str3;
                            if (paramParcel1.readInt() != 0)
                            {
                                localUsbDevice2 = (UsbDevice)UsbDevice.CREATOR.createFromParcel(paramParcel1);
                                str3 = paramParcel1.readString();
                                if (paramParcel1.readInt() == 0)
                                    break label704;
                            }
                            label704: for (PendingIntent localPendingIntent2 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent2 = null)
                            {
                                requestDevicePermission(localUsbDevice2, str3, localPendingIntent2);
                                paramParcel2.writeNoException();
                                break;
                                localUsbDevice2 = null;
                                break label654;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            UsbAccessory localUsbAccessory2;
                            label737: String str2;
                            if (paramParcel1.readInt() != 0)
                            {
                                localUsbAccessory2 = (UsbAccessory)UsbAccessory.CREATOR.createFromParcel(paramParcel1);
                                str2 = paramParcel1.readString();
                                if (paramParcel1.readInt() == 0)
                                    break label787;
                            }
                            label787: for (PendingIntent localPendingIntent1 = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent1 = null)
                            {
                                requestAccessoryPermission(localUsbAccessory2, str2, localPendingIntent1);
                                paramParcel2.writeNoException();
                                break;
                                localUsbAccessory2 = null;
                                break label737;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            if (paramParcel1.readInt() != 0);
                            for (UsbDevice localUsbDevice1 = (UsbDevice)UsbDevice.CREATOR.createFromParcel(paramParcel1); ; localUsbDevice1 = null)
                            {
                                grantDevicePermission(localUsbDevice1, paramParcel1.readInt());
                                paramParcel2.writeNoException();
                                break;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            if (paramParcel1.readInt() != 0);
                            for (UsbAccessory localUsbAccessory1 = (UsbAccessory)UsbAccessory.CREATOR.createFromParcel(paramParcel1); ; localUsbAccessory1 = null)
                            {
                                grantAccessoryPermission(localUsbAccessory1, paramParcel1.readInt());
                                paramParcel2.writeNoException();
                                break;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            boolean bool1 = hasDefaults(paramParcel1.readString());
                            paramParcel2.writeNoException();
                            if (bool1)
                                i = j;
                            paramParcel2.writeInt(i);
                            continue;
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            clearDefaults(paramParcel1.readString());
                            paramParcel2.writeNoException();
                            continue;
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            String str1 = paramParcel1.readString();
                            if (paramParcel1.readInt() != 0);
                            int m;
                            for (int k = j; ; m = 0)
                            {
                                setCurrentFunction(str1, k);
                                paramParcel2.writeNoException();
                                break;
                            }
                            paramParcel1.enforceInterface("android.hardware.usb.IUsbManager");
                            setMassStorageBackingFile(paramParcel1.readString());
                            paramParcel2.writeNoException();
                        }
                    }
                }
            }
        }

        private static class Proxy
            implements IUsbManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void clearDefaults(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public UsbAccessory getCurrentAccessory()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localUsbAccessory = (UsbAccessory)UsbAccessory.CREATOR.createFromParcel(localParcel2);
                        return localUsbAccessory;
                    }
                    UsbAccessory localUsbAccessory = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getDeviceList(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                        paramBundle.readFromParcel(localParcel2);
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.hardware.usb.IUsbManager";
            }

            public void grantAccessoryPermission(UsbAccessory paramUsbAccessory, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    if (paramUsbAccessory != null)
                    {
                        localParcel1.writeInt(1);
                        paramUsbAccessory.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(12, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void grantDevicePermission(UsbDevice paramUsbDevice, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    if (paramUsbDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramUsbDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasAccessoryPermission(UsbAccessory paramUsbAccessory)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                        if (paramUsbAccessory != null)
                        {
                            localParcel1.writeInt(1);
                            paramUsbAccessory.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(8, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean hasDefaults(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasDevicePermission(UsbDevice paramUsbDevice)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                        if (paramUsbDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramUsbDevice.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(7, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public ParcelFileDescriptor openAccessory(UsbAccessory paramUsbAccessory)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                        if (paramUsbAccessory != null)
                        {
                            localParcel1.writeInt(1);
                            paramUsbAccessory.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(4, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
                                return localParcelFileDescriptor;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ParcelFileDescriptor localParcelFileDescriptor = null;
                }
            }

            public ParcelFileDescriptor openDevice(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
                        return localParcelFileDescriptor;
                    }
                    ParcelFileDescriptor localParcelFileDescriptor = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void requestAccessoryPermission(UsbAccessory paramUsbAccessory, String paramString, PendingIntent paramPendingIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                        if (paramUsbAccessory != null)
                        {
                            localParcel1.writeInt(1);
                            paramUsbAccessory.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            if (paramPendingIntent != null)
                            {
                                localParcel1.writeInt(1);
                                paramPendingIntent.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(10, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public void requestDevicePermission(UsbDevice paramUsbDevice, String paramString, PendingIntent paramPendingIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                        if (paramUsbDevice != null)
                        {
                            localParcel1.writeInt(1);
                            paramUsbDevice.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            if (paramPendingIntent != null)
                            {
                                localParcel1.writeInt(1);
                                paramPendingIntent.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(9, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public void setAccessoryPackage(UsbAccessory paramUsbAccessory, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    if (paramUsbAccessory != null)
                    {
                        localParcel1.writeInt(1);
                        paramUsbAccessory.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setCurrentFunction(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setDevicePackage(UsbDevice paramUsbDevice, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    if (paramUsbDevice != null)
                    {
                        localParcel1.writeInt(1);
                        paramUsbDevice.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setMassStorageBackingFile(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.usb.IUsbManager
 * JD-Core Version:        0.6.2
 */