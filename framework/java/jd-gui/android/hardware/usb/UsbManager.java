package android.hardware.usb;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class UsbManager
{
    public static final String ACTION_USB_ACCESSORY_ATTACHED = "android.hardware.usb.action.USB_ACCESSORY_ATTACHED";
    public static final String ACTION_USB_ACCESSORY_DETACHED = "android.hardware.usb.action.USB_ACCESSORY_DETACHED";
    public static final String ACTION_USB_DEVICE_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED";
    public static final String ACTION_USB_DEVICE_DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED";
    public static final String ACTION_USB_STATE = "android.hardware.usb.action.USB_STATE";
    public static final String EXTRA_ACCESSORY = "accessory";
    public static final String EXTRA_DEVICE = "device";
    public static final String EXTRA_PERMISSION_GRANTED = "permission";
    private static final String TAG = "UsbManager";
    public static final String USB_CONFIGURED = "configured";
    public static final String USB_CONNECTED = "connected";
    public static final String USB_FUNCTION_ACCESSORY = "accessory";
    public static final String USB_FUNCTION_ADB = "adb";
    public static final String USB_FUNCTION_AUDIO_SOURCE = "audio_source";
    public static final String USB_FUNCTION_MASS_STORAGE = "mass_storage";
    public static final String USB_FUNCTION_MTP = "mtp";
    public static final String USB_FUNCTION_PTP = "ptp";
    public static final String USB_FUNCTION_RNDIS = "rndis";
    private final Context mContext;
    private final IUsbManager mService;

    public UsbManager(Context paramContext, IUsbManager paramIUsbManager)
    {
        this.mContext = paramContext;
        this.mService = paramIUsbManager;
    }

    private static boolean propertyContainsFunction(String paramString1, String paramString2)
    {
        boolean bool = false;
        String str = SystemProperties.get(paramString1, "");
        int i = str.indexOf(paramString2);
        if (i < 0);
        while (true)
        {
            return bool;
            if ((i <= 0) || (str.charAt(i - 1) == ','))
            {
                int j = i + paramString2.length();
                if ((j >= str.length()) || (str.charAt(j) == ','))
                    bool = true;
            }
        }
    }

    public UsbAccessory[] getAccessoryList()
    {
        Object localObject = null;
        try
        {
            UsbAccessory localUsbAccessory = this.mService.getCurrentAccessory();
            if (localUsbAccessory != null)
            {
                UsbAccessory[] arrayOfUsbAccessory = new UsbAccessory[1];
                arrayOfUsbAccessory[0] = localUsbAccessory;
                localObject = arrayOfUsbAccessory;
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("UsbManager", "RemoteException in getAccessoryList", localRemoteException);
        }
        return localObject;
    }

    public String getDefaultFunction()
    {
        String str = SystemProperties.get("persist.sys.usb.config", "");
        int i = str.indexOf(',');
        if (i > 0)
            str = str.substring(0, i);
        return str;
    }

    public HashMap<String, UsbDevice> getDeviceList()
    {
        Bundle localBundle = new Bundle();
        HashMap localHashMap;
        try
        {
            this.mService.getDeviceList(localBundle);
            localHashMap = new HashMap();
            Iterator localIterator = localBundle.keySet().iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                localHashMap.put(str, (UsbDevice)localBundle.get(str));
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("UsbManager", "RemoteException in getDeviceList", localRemoteException);
            localHashMap = null;
        }
        return localHashMap;
    }

    public boolean hasPermission(UsbAccessory paramUsbAccessory)
    {
        try
        {
            boolean bool2 = this.mService.hasAccessoryPermission(paramUsbAccessory);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("UsbManager", "RemoteException in hasPermission", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean hasPermission(UsbDevice paramUsbDevice)
    {
        try
        {
            boolean bool2 = this.mService.hasDevicePermission(paramUsbDevice);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("UsbManager", "RemoteException in hasPermission", localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public boolean isFunctionEnabled(String paramString)
    {
        return propertyContainsFunction("sys.usb.config", paramString);
    }

    public ParcelFileDescriptor openAccessory(UsbAccessory paramUsbAccessory)
    {
        try
        {
            ParcelFileDescriptor localParcelFileDescriptor2 = this.mService.openAccessory(paramUsbAccessory);
            localParcelFileDescriptor1 = localParcelFileDescriptor2;
            return localParcelFileDescriptor1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("UsbManager", "RemoteException in openAccessory", localRemoteException);
                ParcelFileDescriptor localParcelFileDescriptor1 = null;
            }
        }
    }

    public UsbDeviceConnection openDevice(UsbDevice paramUsbDevice)
    {
        try
        {
            String str = paramUsbDevice.getDeviceName();
            ParcelFileDescriptor localParcelFileDescriptor = this.mService.openDevice(str);
            if (localParcelFileDescriptor != null)
            {
                localUsbDeviceConnection = new UsbDeviceConnection(paramUsbDevice);
                boolean bool = localUsbDeviceConnection.open(str, localParcelFileDescriptor);
                localParcelFileDescriptor.close();
                if (bool)
                    return localUsbDeviceConnection;
            }
        }
        catch (Exception localException)
        {
            while (true)
            {
                Log.e("UsbManager", "exception in UsbManager.openDevice", localException);
                UsbDeviceConnection localUsbDeviceConnection = null;
            }
        }
    }

    public void requestPermission(UsbAccessory paramUsbAccessory, PendingIntent paramPendingIntent)
    {
        try
        {
            this.mService.requestAccessoryPermission(paramUsbAccessory, this.mContext.getPackageName(), paramPendingIntent);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UsbManager", "RemoteException in requestPermission", localRemoteException);
        }
    }

    public void requestPermission(UsbDevice paramUsbDevice, PendingIntent paramPendingIntent)
    {
        try
        {
            this.mService.requestDevicePermission(paramUsbDevice, this.mContext.getPackageName(), paramPendingIntent);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UsbManager", "RemoteException in requestPermission", localRemoteException);
        }
    }

    public void setCurrentFunction(String paramString, boolean paramBoolean)
    {
        try
        {
            this.mService.setCurrentFunction(paramString, paramBoolean);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UsbManager", "RemoteException in setCurrentFunction", localRemoteException);
        }
    }

    public void setMassStorageBackingFile(String paramString)
    {
        try
        {
            this.mService.setMassStorageBackingFile(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("UsbManager", "RemoteException in setDefaultFunction", localRemoteException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.usb.UsbManager
 * JD-Core Version:        0.6.2
 */