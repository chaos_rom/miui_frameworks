package android.hardware.usb;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class UsbDevice
    implements Parcelable
{
    public static final Parcelable.Creator<UsbDevice> CREATOR = new Parcelable.Creator()
    {
        public UsbDevice createFromParcel(Parcel paramAnonymousParcel)
        {
            return new UsbDevice(paramAnonymousParcel.readString(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readParcelableArray(UsbInterface.class.getClassLoader()));
        }

        public UsbDevice[] newArray(int paramAnonymousInt)
        {
            return new UsbDevice[paramAnonymousInt];
        }
    };
    private static final String TAG = "UsbDevice";
    private final int mClass;
    private final Parcelable[] mInterfaces;
    private final String mName;
    private final int mProductId;
    private final int mProtocol;
    private final int mSubclass;
    private final int mVendorId;

    public UsbDevice(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Parcelable[] paramArrayOfParcelable)
    {
        this.mName = paramString;
        this.mVendorId = paramInt1;
        this.mProductId = paramInt2;
        this.mClass = paramInt3;
        this.mSubclass = paramInt4;
        this.mProtocol = paramInt5;
        this.mInterfaces = paramArrayOfParcelable;
    }

    public static int getDeviceId(String paramString)
    {
        return native_get_device_id(paramString);
    }

    public static String getDeviceName(int paramInt)
    {
        return native_get_device_name(paramInt);
    }

    private static native int native_get_device_id(String paramString);

    private static native String native_get_device_name(int paramInt);

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool;
        if ((paramObject instanceof UsbDevice))
            bool = ((UsbDevice)paramObject).mName.equals(this.mName);
        while (true)
        {
            return bool;
            if ((paramObject instanceof String))
                bool = ((String)paramObject).equals(this.mName);
            else
                bool = false;
        }
    }

    public int getDeviceClass()
    {
        return this.mClass;
    }

    public int getDeviceId()
    {
        return getDeviceId(this.mName);
    }

    public String getDeviceName()
    {
        return this.mName;
    }

    public int getDeviceProtocol()
    {
        return this.mProtocol;
    }

    public int getDeviceSubclass()
    {
        return this.mSubclass;
    }

    public UsbInterface getInterface(int paramInt)
    {
        return (UsbInterface)this.mInterfaces[paramInt];
    }

    public int getInterfaceCount()
    {
        return this.mInterfaces.length;
    }

    public int getProductId()
    {
        return this.mProductId;
    }

    public int getVendorId()
    {
        return this.mVendorId;
    }

    public int hashCode()
    {
        return this.mName.hashCode();
    }

    public String toString()
    {
        return "UsbDevice[mName=" + this.mName + ",mVendorId=" + this.mVendorId + ",mProductId=" + this.mProductId + ",mClass=" + this.mClass + ",mSubclass=" + this.mSubclass + ",mProtocol=" + this.mProtocol + ",mInterfaces=" + this.mInterfaces + "]";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mName);
        paramParcel.writeInt(this.mVendorId);
        paramParcel.writeInt(this.mProductId);
        paramParcel.writeInt(this.mClass);
        paramParcel.writeInt(this.mSubclass);
        paramParcel.writeInt(this.mProtocol);
        paramParcel.writeParcelableArray(this.mInterfaces, 0);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.usb.UsbDevice
 * JD-Core Version:        0.6.2
 */