package android.hardware.usb;

import android.os.ParcelFileDescriptor;
import java.io.FileDescriptor;

public class UsbDeviceConnection
{
    private static final String TAG = "UsbDeviceConnection";
    private final UsbDevice mDevice;
    private int mNativeContext;

    public UsbDeviceConnection(UsbDevice paramUsbDevice)
    {
        this.mDevice = paramUsbDevice;
    }

    private native int native_bulk_request(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3);

    private native boolean native_claim_interface(int paramInt, boolean paramBoolean);

    private native void native_close();

    private native int native_control_request(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte, int paramInt5, int paramInt6);

    private native byte[] native_get_desc();

    private native int native_get_fd();

    private native String native_get_serial();

    private native boolean native_open(String paramString, FileDescriptor paramFileDescriptor);

    private native boolean native_release_interface(int paramInt);

    private native UsbRequest native_request_wait();

    public int bulkTransfer(UsbEndpoint paramUsbEndpoint, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        return native_bulk_request(paramUsbEndpoint.getAddress(), paramArrayOfByte, paramInt1, paramInt2);
    }

    public boolean claimInterface(UsbInterface paramUsbInterface, boolean paramBoolean)
    {
        return native_claim_interface(paramUsbInterface.getId(), paramBoolean);
    }

    public void close()
    {
        native_close();
    }

    public int controlTransfer(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte, int paramInt5, int paramInt6)
    {
        return native_control_request(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfByte, paramInt5, paramInt6);
    }

    public int getFileDescriptor()
    {
        return native_get_fd();
    }

    public byte[] getRawDescriptors()
    {
        return native_get_desc();
    }

    public String getSerial()
    {
        return native_get_serial();
    }

    boolean open(String paramString, ParcelFileDescriptor paramParcelFileDescriptor)
    {
        return native_open(paramString, paramParcelFileDescriptor.getFileDescriptor());
    }

    public boolean releaseInterface(UsbInterface paramUsbInterface)
    {
        return native_release_interface(paramUsbInterface.getId());
    }

    public UsbRequest requestWait()
    {
        UsbRequest localUsbRequest = native_request_wait();
        if (localUsbRequest != null)
            localUsbRequest.dequeue();
        return localUsbRequest;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.usb.UsbDeviceConnection
 * JD-Core Version:        0.6.2
 */