package android.hardware.usb;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class UsbInterface
    implements Parcelable
{
    public static final Parcelable.Creator<UsbInterface> CREATOR = new Parcelable.Creator()
    {
        public UsbInterface createFromParcel(Parcel paramAnonymousParcel)
        {
            return new UsbInterface(paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readParcelableArray(UsbEndpoint.class.getClassLoader()));
        }

        public UsbInterface[] newArray(int paramAnonymousInt)
        {
            return new UsbInterface[paramAnonymousInt];
        }
    };
    private final int mClass;
    private final Parcelable[] mEndpoints;
    private final int mId;
    private final int mProtocol;
    private final int mSubclass;

    public UsbInterface(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Parcelable[] paramArrayOfParcelable)
    {
        this.mId = paramInt1;
        this.mClass = paramInt2;
        this.mSubclass = paramInt3;
        this.mProtocol = paramInt4;
        this.mEndpoints = paramArrayOfParcelable;
    }

    public int describeContents()
    {
        return 0;
    }

    public UsbEndpoint getEndpoint(int paramInt)
    {
        return (UsbEndpoint)this.mEndpoints[paramInt];
    }

    public int getEndpointCount()
    {
        return this.mEndpoints.length;
    }

    public int getId()
    {
        return this.mId;
    }

    public int getInterfaceClass()
    {
        return this.mClass;
    }

    public int getInterfaceProtocol()
    {
        return this.mProtocol;
    }

    public int getInterfaceSubclass()
    {
        return this.mSubclass;
    }

    public String toString()
    {
        return "UsbInterface[mId=" + this.mId + ",mClass=" + this.mClass + ",mSubclass=" + this.mSubclass + ",mProtocol=" + this.mProtocol + ",mEndpoints=" + this.mEndpoints + "]";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mId);
        paramParcel.writeInt(this.mClass);
        paramParcel.writeInt(this.mSubclass);
        paramParcel.writeInt(this.mProtocol);
        paramParcel.writeParcelableArray(this.mEndpoints, 0);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.usb.UsbInterface
 * JD-Core Version:        0.6.2
 */