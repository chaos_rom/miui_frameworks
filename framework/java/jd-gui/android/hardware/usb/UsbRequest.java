package android.hardware.usb;

import android.util.Log;
import java.nio.ByteBuffer;

public class UsbRequest
{
    private static final String TAG = "UsbRequest";
    private ByteBuffer mBuffer;
    private Object mClientData;
    private UsbEndpoint mEndpoint;
    private int mLength;
    private int mNativeContext;

    private native boolean native_cancel();

    private native void native_close();

    private native void native_dequeue_array(byte[] paramArrayOfByte, int paramInt, boolean paramBoolean);

    private native void native_dequeue_direct();

    private native boolean native_init(UsbDeviceConnection paramUsbDeviceConnection, int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private native boolean native_queue_array(byte[] paramArrayOfByte, int paramInt, boolean paramBoolean);

    private native boolean native_queue_direct(ByteBuffer paramByteBuffer, int paramInt, boolean paramBoolean);

    public boolean cancel()
    {
        return native_cancel();
    }

    public void close()
    {
        this.mEndpoint = null;
        native_close();
    }

    void dequeue()
    {
        boolean bool;
        if (this.mEndpoint.getDirection() == 0)
        {
            bool = true;
            if (!this.mBuffer.isDirect())
                break label42;
            native_dequeue_direct();
        }
        while (true)
        {
            this.mBuffer = null;
            this.mLength = 0;
            return;
            bool = false;
            break;
            label42: native_dequeue_array(this.mBuffer.array(), this.mLength, bool);
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if (this.mEndpoint != null)
            {
                Log.v("UsbRequest", "endpoint still open in finalize(): " + this);
                close();
            }
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public Object getClientData()
    {
        return this.mClientData;
    }

    public UsbEndpoint getEndpoint()
    {
        return this.mEndpoint;
    }

    public boolean initialize(UsbDeviceConnection paramUsbDeviceConnection, UsbEndpoint paramUsbEndpoint)
    {
        this.mEndpoint = paramUsbEndpoint;
        return native_init(paramUsbDeviceConnection, paramUsbEndpoint.getAddress(), paramUsbEndpoint.getAttributes(), paramUsbEndpoint.getMaxPacketSize(), paramUsbEndpoint.getInterval());
    }

    public boolean queue(ByteBuffer paramByteBuffer, int paramInt)
    {
        boolean bool1;
        if (this.mEndpoint.getDirection() == 0)
        {
            bool1 = true;
            if (!paramByteBuffer.isDirect())
                break label51;
        }
        for (boolean bool2 = native_queue_direct(paramByteBuffer, paramInt, bool1); ; bool2 = native_queue_array(paramByteBuffer.array(), paramInt, bool1))
        {
            if (bool2)
            {
                this.mBuffer = paramByteBuffer;
                this.mLength = paramInt;
            }
            return bool2;
            bool1 = false;
            break;
            label51: if (!paramByteBuffer.hasArray())
                break label73;
        }
        label73: throw new IllegalArgumentException("buffer is not direct and has no array");
    }

    public void setClientData(Object paramObject)
    {
        this.mClientData = paramObject;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.usb.UsbRequest
 * JD-Core Version:        0.6.2
 */