package android.hardware.usb;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class UsbEndpoint
    implements Parcelable
{
    public static final Parcelable.Creator<UsbEndpoint> CREATOR = new Parcelable.Creator()
    {
        public UsbEndpoint createFromParcel(Parcel paramAnonymousParcel)
        {
            return new UsbEndpoint(paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt());
        }

        public UsbEndpoint[] newArray(int paramAnonymousInt)
        {
            return new UsbEndpoint[paramAnonymousInt];
        }
    };
    private final int mAddress;
    private final int mAttributes;
    private final int mInterval;
    private final int mMaxPacketSize;

    public UsbEndpoint(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mAddress = paramInt1;
        this.mAttributes = paramInt2;
        this.mMaxPacketSize = paramInt3;
        this.mInterval = paramInt4;
    }

    public int describeContents()
    {
        return 0;
    }

    public int getAddress()
    {
        return this.mAddress;
    }

    public int getAttributes()
    {
        return this.mAttributes;
    }

    public int getDirection()
    {
        return 0x80 & this.mAddress;
    }

    public int getEndpointNumber()
    {
        return 0xF & this.mAddress;
    }

    public int getInterval()
    {
        return this.mInterval;
    }

    public int getMaxPacketSize()
    {
        return this.mMaxPacketSize;
    }

    public int getType()
    {
        return 0x3 & this.mAttributes;
    }

    public String toString()
    {
        return "UsbEndpoint[mAddress=" + this.mAddress + ",mAttributes=" + this.mAttributes + ",mMaxPacketSize=" + this.mMaxPacketSize + ",mInterval=" + this.mInterval + "]";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mAddress);
        paramParcel.writeInt(this.mAttributes);
        paramParcel.writeInt(this.mMaxPacketSize);
        paramParcel.writeInt(this.mInterval);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.usb.UsbEndpoint
 * JD-Core Version:        0.6.2
 */