package android.hardware;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SystemSensorManager extends SensorManager
{
    private static final int SENSOR_DISABLE = -1;
    private static ArrayList<Sensor> sFullSensorsList = new ArrayList();
    static SparseArray<Sensor> sHandleToSensor = new SparseArray();
    static final ArrayList<ListenerDelegate> sListeners = new ArrayList();
    static SensorManager.SensorEventPool sPool;
    private static int sQueue;
    private static boolean sSensorModuleInitialized = false;
    private static SensorThread sSensorThread;
    final Looper mMainLooper;

    public SystemSensorManager(Looper paramLooper)
    {
        this.mMainLooper = paramLooper;
        synchronized (sListeners)
        {
            if (!sSensorModuleInitialized)
            {
                sSensorModuleInitialized = true;
                nativeClassInit();
                sensors_module_init();
                ArrayList localArrayList2 = sFullSensorsList;
                int i = 0;
                do
                {
                    Sensor localSensor = new Sensor();
                    i = sensors_module_get_next_sensor(localSensor, i);
                    if (i >= 0)
                    {
                        localArrayList2.add(localSensor);
                        sHandleToSensor.append(localSensor.getHandle(), localSensor);
                    }
                }
                while (i > 0);
                sPool = new SensorManager.SensorEventPool(2 * sFullSensorsList.size());
                sSensorThread = new SensorThread();
            }
            return;
        }
    }

    private boolean disableSensorLocked(Sensor paramSensor)
    {
        Iterator localIterator = sListeners.iterator();
        do
            if (!localIterator.hasNext())
                break;
        while (!((ListenerDelegate)localIterator.next()).hasSensor(paramSensor));
        String str;
        int i;
        for (boolean bool = true; ; bool = sensors_enable_sensor(sQueue, str, i, -1))
        {
            return bool;
            str = paramSensor.getName();
            i = paramSensor.getHandle();
        }
    }

    private boolean enableSensorLocked(Sensor paramSensor, int paramInt)
    {
        boolean bool = false;
        Iterator localIterator = sListeners.iterator();
        while (localIterator.hasNext())
            if (((ListenerDelegate)localIterator.next()).hasSensor(paramSensor))
            {
                String str = paramSensor.getName();
                int i = paramSensor.getHandle();
                bool = sensors_enable_sensor(sQueue, str, i, paramInt);
            }
        return bool;
    }

    private static native void nativeClassInit();

    static native int sensors_create_queue();

    static native int sensors_data_poll(int paramInt, float[] paramArrayOfFloat, int[] paramArrayOfInt, long[] paramArrayOfLong);

    static native void sensors_destroy_queue(int paramInt);

    static native boolean sensors_enable_sensor(int paramInt1, String paramString, int paramInt2, int paramInt3);

    private static native int sensors_module_get_next_sensor(Sensor paramSensor, int paramInt);

    private static native int sensors_module_init();

    protected List<Sensor> getFullSensorList()
    {
        return sFullSensorsList;
    }

    protected boolean registerListenerImpl(SensorEventListener paramSensorEventListener, Sensor paramSensor, int paramInt, Handler paramHandler)
    {
        boolean bool = true;
        while (true)
        {
            synchronized (sListeners)
            {
                Iterator localIterator = sListeners.iterator();
                if (!localIterator.hasNext())
                    break label214;
                ListenerDelegate localListenerDelegate3 = (ListenerDelegate)localIterator.next();
                Object localObject3 = localListenerDelegate3.getListener();
                if (localObject3 != paramSensorEventListener)
                    continue;
                localListenerDelegate1 = localListenerDelegate3;
                if (localListenerDelegate1 != null);
            }
            try
            {
                ListenerDelegate localListenerDelegate2 = new ListenerDelegate(paramSensorEventListener, paramSensor, paramHandler);
                sListeners.add(localListenerDelegate2);
                if (!sListeners.isEmpty())
                    if (sSensorThread.startLocked())
                        if (!enableSensorLocked(paramSensor, paramInt))
                        {
                            sListeners.remove(localListenerDelegate2);
                            bool = false;
                        }
                while (true)
                {
                    return bool;
                    sListeners.remove(localListenerDelegate2);
                    bool = false;
                    continue;
                    bool = false;
                    continue;
                    if (localListenerDelegate1.hasSensor(paramSensor))
                        break;
                    localListenerDelegate1.addSensor(paramSensor);
                    if (enableSensorLocked(paramSensor, paramInt))
                        break;
                    localListenerDelegate1.removeSensor(paramSensor);
                    bool = false;
                }
                localObject1 = finally;
                throw localObject1;
            }
            finally
            {
                while (true);
            }
            label214: ListenerDelegate localListenerDelegate1 = null;
        }
    }

    // ERROR //
    protected void unregisterListenerImpl(SensorEventListener paramSensorEventListener, Sensor paramSensor)
    {
        // Byte code:
        //     0: getstatic 48	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
        //     3: astore_3
        //     4: aload_3
        //     5: monitorenter
        //     6: getstatic 48	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
        //     9: invokevirtual 82	java/util/ArrayList:size	()I
        //     12: istore 5
        //     14: iconst_0
        //     15: istore 6
        //     17: iload 6
        //     19: iload 5
        //     21: if_icmpge +109 -> 130
        //     24: getstatic 48	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
        //     27: iload 6
        //     29: invokevirtual 163	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     32: checkcast 6	android/hardware/SystemSensorManager$ListenerDelegate
        //     35: astore 7
        //     37: aload 7
        //     39: invokevirtual 135	android/hardware/SystemSensorManager$ListenerDelegate:getListener	()Ljava/lang/Object;
        //     42: aload_1
        //     43: if_acmpne +90 -> 133
        //     46: aload_2
        //     47: ifnonnull +59 -> 106
        //     50: getstatic 48	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
        //     53: iload 6
        //     55: invokevirtual 165	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     58: pop
        //     59: aload 7
        //     61: invokevirtual 168	android/hardware/SystemSensorManager$ListenerDelegate:getSensors	()Ljava/util/List;
        //     64: invokeinterface 171 1 0
        //     69: astore 11
        //     71: aload 11
        //     73: invokeinterface 107 1 0
        //     78: ifeq +52 -> 130
        //     81: aload_0
        //     82: aload 11
        //     84: invokeinterface 111 1 0
        //     89: checkcast 61	android/hardware/Sensor
        //     92: invokespecial 173	android/hardware/SystemSensorManager:disableSensorLocked	(Landroid/hardware/Sensor;)Z
        //     95: pop
        //     96: goto -25 -> 71
        //     99: astore 4
        //     101: aload_3
        //     102: monitorexit
        //     103: aload 4
        //     105: athrow
        //     106: aload 7
        //     108: aload_2
        //     109: invokevirtual 157	android/hardware/SystemSensorManager$ListenerDelegate:removeSensor	(Landroid/hardware/Sensor;)I
        //     112: ifne +18 -> 130
        //     115: getstatic 48	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
        //     118: iload 6
        //     120: invokevirtual 165	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     123: pop
        //     124: aload_0
        //     125: aload_2
        //     126: invokespecial 173	android/hardware/SystemSensorManager:disableSensorLocked	(Landroid/hardware/Sensor;)Z
        //     129: pop
        //     130: aload_3
        //     131: monitorexit
        //     132: return
        //     133: iinc 6 1
        //     136: goto -119 -> 17
        //
        // Exception table:
        //     from	to	target	type
        //     6	103	99	finally
        //     106	132	99	finally
    }

    private class ListenerDelegate
    {
        public SparseBooleanArray mFirstEvent = new SparseBooleanArray();
        private final Handler mHandler;
        public SparseIntArray mSensorAccuracies = new SparseIntArray();
        private final SensorEventListener mSensorEventListener;
        private final ArrayList<Sensor> mSensorList = new ArrayList();
        public SparseBooleanArray mSensors = new SparseBooleanArray();

        ListenerDelegate(SensorEventListener paramSensor, Sensor paramHandler, Handler arg4)
        {
            this.mSensorEventListener = paramSensor;
            Object localObject;
            if (localObject != null);
            for (Looper localLooper = localObject.getLooper(); ; localLooper = SystemSensorManager.this.mMainLooper)
            {
                this.mHandler = new Handler(localLooper)
                {
                    public void handleMessage(Message paramAnonymousMessage)
                    {
                        SensorEvent localSensorEvent = (SensorEvent)paramAnonymousMessage.obj;
                        int i = localSensorEvent.sensor.getHandle();
                        switch (localSensorEvent.sensor.getType())
                        {
                        default:
                            if (!SystemSensorManager.ListenerDelegate.this.mFirstEvent.get(i))
                            {
                                SystemSensorManager.ListenerDelegate.this.mFirstEvent.put(i, true);
                                SystemSensorManager.ListenerDelegate.this.mSensorEventListener.onAccuracyChanged(localSensorEvent.sensor, 3);
                            }
                            break;
                        case 2:
                        case 3:
                        }
                        while (true)
                        {
                            SystemSensorManager.ListenerDelegate.this.mSensorEventListener.onSensorChanged(localSensorEvent);
                            SystemSensorManager.sPool.returnToPool(localSensorEvent);
                            return;
                            int j = SystemSensorManager.ListenerDelegate.this.mSensorAccuracies.get(i);
                            if ((localSensorEvent.accuracy >= 0) && (j != localSensorEvent.accuracy))
                            {
                                SystemSensorManager.ListenerDelegate.this.mSensorAccuracies.put(i, localSensorEvent.accuracy);
                                SystemSensorManager.ListenerDelegate.this.mSensorEventListener.onAccuracyChanged(localSensorEvent.sensor, localSensorEvent.accuracy);
                            }
                        }
                    }
                };
                addSensor(paramHandler);
                return;
            }
        }

        void addSensor(Sensor paramSensor)
        {
            this.mSensors.put(paramSensor.getHandle(), true);
            this.mSensorList.add(paramSensor);
        }

        Object getListener()
        {
            return this.mSensorEventListener;
        }

        List<Sensor> getSensors()
        {
            return this.mSensorList;
        }

        boolean hasSensor(Sensor paramSensor)
        {
            return this.mSensors.get(paramSensor.getHandle());
        }

        void onSensorChangedLocked(Sensor paramSensor, float[] paramArrayOfFloat, long[] paramArrayOfLong, int paramInt)
        {
            SensorEvent localSensorEvent = SystemSensorManager.sPool.getFromPool();
            float[] arrayOfFloat = localSensorEvent.values;
            arrayOfFloat[0] = paramArrayOfFloat[0];
            arrayOfFloat[1] = paramArrayOfFloat[1];
            arrayOfFloat[2] = paramArrayOfFloat[2];
            localSensorEvent.timestamp = paramArrayOfLong[0];
            localSensorEvent.accuracy = paramInt;
            localSensorEvent.sensor = paramSensor;
            Message localMessage = Message.obtain();
            localMessage.what = 0;
            localMessage.obj = localSensorEvent;
            localMessage.setAsynchronous(true);
            this.mHandler.sendMessage(localMessage);
        }

        int removeSensor(Sensor paramSensor)
        {
            this.mSensors.delete(paramSensor.getHandle());
            this.mSensorList.remove(paramSensor);
            return this.mSensors.size();
        }
    }

    private static class SensorThread
    {
        boolean mSensorsReady;
        Thread mThread;

        protected void finalize()
        {
        }

        // ERROR //
        boolean startLocked()
        {
            // Byte code:
            //     0: iconst_0
            //     1: istore_1
            //     2: aload_0
            //     3: getfield 25	android/hardware/SystemSensorManager$SensorThread:mThread	Ljava/lang/Thread;
            //     6: ifnonnull +61 -> 67
            //     9: aload_0
            //     10: iconst_0
            //     11: putfield 27	android/hardware/SystemSensorManager$SensorThread:mSensorsReady	Z
            //     14: new 9	android/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable
            //     17: dup
            //     18: aload_0
            //     19: invokespecial 30	android/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable:<init>	(Landroid/hardware/SystemSensorManager$SensorThread;)V
            //     22: astore_3
            //     23: new 32	java/lang/Thread
            //     26: dup
            //     27: aload_3
            //     28: ldc 2
            //     30: invokevirtual 38	java/lang/Class:getName	()Ljava/lang/String;
            //     33: invokespecial 41	java/lang/Thread:<init>	(Ljava/lang/Runnable;Ljava/lang/String;)V
            //     36: astore 4
            //     38: aload 4
            //     40: invokevirtual 44	java/lang/Thread:start	()V
            //     43: aload_3
            //     44: monitorenter
            //     45: aload_0
            //     46: getfield 27	android/hardware/SystemSensorManager$SensorThread:mSensorsReady	Z
            //     49: ifne +27 -> 76
            //     52: aload_3
            //     53: invokevirtual 47	java/lang/Object:wait	()V
            //     56: goto -11 -> 45
            //     59: astore 5
            //     61: aload_3
            //     62: monitorexit
            //     63: aload 5
            //     65: athrow
            //     66: astore_2
            //     67: aload_0
            //     68: getfield 25	android/hardware/SystemSensorManager$SensorThread:mThread	Ljava/lang/Thread;
            //     71: ifnonnull +16 -> 87
            //     74: iload_1
            //     75: ireturn
            //     76: aload_3
            //     77: monitorexit
            //     78: aload_0
            //     79: aload 4
            //     81: putfield 25	android/hardware/SystemSensorManager$SensorThread:mThread	Ljava/lang/Thread;
            //     84: goto -17 -> 67
            //     87: iconst_1
            //     88: istore_1
            //     89: goto -15 -> 74
            //
            // Exception table:
            //     from	to	target	type
            //     45	63	59	finally
            //     76	78	59	finally
            //     2	45	66	java/lang/InterruptedException
            //     63	66	66	java/lang/InterruptedException
            //     78	84	66	java/lang/InterruptedException
        }

        private class SensorThreadRunnable
            implements Runnable
        {
            SensorThreadRunnable()
            {
            }

            private boolean open()
            {
                SystemSensorManager.access$002(SystemSensorManager.sensors_create_queue());
                return true;
            }

            // ERROR //
            public void run()
            {
                // Byte code:
                //     0: iconst_3
                //     1: newarray float
                //     3: astore_1
                //     4: iconst_1
                //     5: newarray int
                //     7: astore_2
                //     8: iconst_1
                //     9: newarray long
                //     11: astore_3
                //     12: bipush 248
                //     14: invokestatic 37	android/os/Process:setThreadPriority	(I)V
                //     17: aload_0
                //     18: invokespecial 39	android/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable:open	()Z
                //     21: ifne +4 -> 25
                //     24: return
                //     25: aload_0
                //     26: monitorenter
                //     27: aload_0
                //     28: getfield 15	android/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable:this$0	Landroid/hardware/SystemSensorManager$SensorThread;
                //     31: iconst_1
                //     32: putfield 43	android/hardware/SystemSensorManager$SensorThread:mSensorsReady	Z
                //     35: aload_0
                //     36: invokevirtual 46	java/lang/Object:notify	()V
                //     39: aload_0
                //     40: monitorexit
                //     41: invokestatic 49	android/hardware/SystemSensorManager:access$000	()I
                //     44: aload_1
                //     45: aload_2
                //     46: aload_3
                //     47: invokestatic 53	android/hardware/SystemSensorManager:sensors_data_poll	(I[F[I[J)I
                //     50: istore 5
                //     52: aload_2
                //     53: iconst_0
                //     54: iaload
                //     55: istore 6
                //     57: getstatic 57	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
                //     60: astore 7
                //     62: aload 7
                //     64: monitorenter
                //     65: iload 5
                //     67: bipush 255
                //     69: if_icmpeq +12 -> 81
                //     72: getstatic 57	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
                //     75: invokevirtual 62	java/util/ArrayList:isEmpty	()Z
                //     78: ifeq +85 -> 163
                //     81: iload 5
                //     83: bipush 255
                //     85: if_icmpne +38 -> 123
                //     88: getstatic 57	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
                //     91: invokevirtual 62	java/util/ArrayList:isEmpty	()Z
                //     94: ifne +29 -> 123
                //     97: ldc 64
                //     99: new 66	java/lang/StringBuilder
                //     102: dup
                //     103: invokespecial 67	java/lang/StringBuilder:<init>	()V
                //     106: ldc 69
                //     108: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //     111: iload 5
                //     113: invokevirtual 76	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
                //     116: invokevirtual 80	java/lang/StringBuilder:toString	()Ljava/lang/String;
                //     119: invokestatic 86	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
                //     122: pop
                //     123: invokestatic 49	android/hardware/SystemSensorManager:access$000	()I
                //     126: invokestatic 89	android/hardware/SystemSensorManager:sensors_destroy_queue	(I)V
                //     129: iconst_0
                //     130: invokestatic 30	android/hardware/SystemSensorManager:access$002	(I)I
                //     133: pop
                //     134: aload_0
                //     135: getfield 15	android/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable:this$0	Landroid/hardware/SystemSensorManager$SensorThread;
                //     138: aconst_null
                //     139: putfield 93	android/hardware/SystemSensorManager$SensorThread:mThread	Ljava/lang/Thread;
                //     142: aload 7
                //     144: monitorexit
                //     145: goto -121 -> 24
                //     148: astore 8
                //     150: aload 7
                //     152: monitorexit
                //     153: aload 8
                //     155: athrow
                //     156: astore 4
                //     158: aload_0
                //     159: monitorexit
                //     160: aload 4
                //     162: athrow
                //     163: getstatic 97	android/hardware/SystemSensorManager:sHandleToSensor	Landroid/util/SparseArray;
                //     166: iload 5
                //     168: invokevirtual 103	android/util/SparseArray:get	(I)Ljava/lang/Object;
                //     171: checkcast 105	android/hardware/Sensor
                //     174: astore 11
                //     176: aload 11
                //     178: ifnull +58 -> 236
                //     181: getstatic 57	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
                //     184: invokevirtual 108	java/util/ArrayList:size	()I
                //     187: istore 12
                //     189: iconst_0
                //     190: istore 13
                //     192: iload 13
                //     194: iload 12
                //     196: if_icmpge +40 -> 236
                //     199: getstatic 57	android/hardware/SystemSensorManager:sListeners	Ljava/util/ArrayList;
                //     202: iload 13
                //     204: invokevirtual 109	java/util/ArrayList:get	(I)Ljava/lang/Object;
                //     207: checkcast 111	android/hardware/SystemSensorManager$ListenerDelegate
                //     210: astore 14
                //     212: aload 14
                //     214: aload 11
                //     216: invokevirtual 115	android/hardware/SystemSensorManager$ListenerDelegate:hasSensor	(Landroid/hardware/Sensor;)Z
                //     219: ifeq +23 -> 242
                //     222: aload 14
                //     224: aload 11
                //     226: aload_1
                //     227: aload_3
                //     228: iload 6
                //     230: invokevirtual 119	android/hardware/SystemSensorManager$ListenerDelegate:onSensorChangedLocked	(Landroid/hardware/Sensor;[F[JI)V
                //     233: goto +9 -> 242
                //     236: aload 7
                //     238: monitorexit
                //     239: goto -198 -> 41
                //     242: iinc 13 1
                //     245: goto -53 -> 192
                //
                // Exception table:
                //     from	to	target	type
                //     72	153	148	finally
                //     163	239	148	finally
                //     27	41	156	finally
                //     158	160	156	finally
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.SystemSensorManager
 * JD-Core Version:        0.6.2
 */