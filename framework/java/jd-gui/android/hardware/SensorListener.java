package android.hardware;

@Deprecated
public abstract interface SensorListener
{
    public abstract void onAccuracyChanged(int paramInt1, int paramInt2);

    public abstract void onSensorChanged(int paramInt, float[] paramArrayOfFloat);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.SensorListener
 * JD-Core Version:        0.6.2
 */