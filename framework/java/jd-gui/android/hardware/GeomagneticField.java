package android.hardware;

import [F;
import java.util.GregorianCalendar;

public class GeomagneticField
{
    private static final long BASE_TIME = 0L;
    private static final float[][] DELTA_G;
    private static final float[][] DELTA_H;
    private static final float EARTH_REFERENCE_RADIUS_KM = 6371.2002F;
    private static final float EARTH_SEMI_MAJOR_AXIS_KM = 6378.1372F;
    private static final float EARTH_SEMI_MINOR_AXIS_KM = 6356.7524F;
    private static final float[][] G_COEFF;
    private static final float[][] H_COEFF;
    private static final float[][] SCHMIDT_QUASI_NORM_FACTORS;
    private float mGcLatitudeRad;
    private float mGcLongitudeRad;
    private float mGcRadiusKm;
    private float mX;
    private float mY;
    private float mZ;

    static
    {
        if (!GeomagneticField.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            float[][] arrayOfFloat1 = new float[13][];
            float[] arrayOfFloat2 = new float[1];
            arrayOfFloat2[0] = 0.0F;
            arrayOfFloat1[0] = arrayOfFloat2;
            float[] arrayOfFloat3 = new float[2];
            arrayOfFloat3[0] = -29496.6F;
            arrayOfFloat3[1] = -1586.3F;
            arrayOfFloat1[1] = arrayOfFloat3;
            float[] arrayOfFloat4 = new float[3];
            arrayOfFloat4[0] = -2396.6001F;
            arrayOfFloat4[1] = 3026.1001F;
            arrayOfFloat4[2] = 1668.6F;
            arrayOfFloat1[2] = arrayOfFloat4;
            float[] arrayOfFloat5 = new float[4];
            arrayOfFloat5[0] = 1340.1F;
            arrayOfFloat5[1] = -2326.2F;
            arrayOfFloat5[2] = 1231.9F;
            arrayOfFloat5[3] = 634.0F;
            arrayOfFloat1[3] = arrayOfFloat5;
            float[] arrayOfFloat6 = new float[5];
            arrayOfFloat6[0] = 912.59998F;
            arrayOfFloat6[1] = 808.90002F;
            arrayOfFloat6[2] = 166.7F;
            arrayOfFloat6[3] = -357.10001F;
            arrayOfFloat6[4] = 89.400002F;
            arrayOfFloat1[4] = arrayOfFloat6;
            float[] arrayOfFloat7 = new float[6];
            arrayOfFloat7[0] = -230.89999F;
            arrayOfFloat7[1] = 357.20001F;
            arrayOfFloat7[2] = 200.3F;
            arrayOfFloat7[3] = -141.10001F;
            arrayOfFloat7[4] = -163.0F;
            arrayOfFloat7[5] = -7.8F;
            arrayOfFloat1[5] = arrayOfFloat7;
            float[] arrayOfFloat8 = new float[7];
            arrayOfFloat8[0] = 72.800003F;
            arrayOfFloat8[1] = 68.599998F;
            arrayOfFloat8[2] = 76.0F;
            arrayOfFloat8[3] = -141.39999F;
            arrayOfFloat8[4] = -22.799999F;
            arrayOfFloat8[5] = 13.2F;
            arrayOfFloat8[6] = -77.900002F;
            arrayOfFloat1[6] = arrayOfFloat8;
            float[] arrayOfFloat9 = new float[8];
            arrayOfFloat9[0] = 80.5F;
            arrayOfFloat9[1] = -75.099998F;
            arrayOfFloat9[2] = -4.7F;
            arrayOfFloat9[3] = 45.299999F;
            arrayOfFloat9[4] = 13.9F;
            arrayOfFloat9[5] = 10.4F;
            arrayOfFloat9[6] = 1.7F;
            arrayOfFloat9[7] = 4.9F;
            arrayOfFloat1[7] = arrayOfFloat9;
            float[] arrayOfFloat10 = new float[9];
            arrayOfFloat10[0] = 24.4F;
            arrayOfFloat10[1] = 8.1F;
            arrayOfFloat10[2] = -14.5F;
            arrayOfFloat10[3] = -5.6F;
            arrayOfFloat10[4] = -19.299999F;
            arrayOfFloat10[5] = 11.5F;
            arrayOfFloat10[6] = 10.9F;
            arrayOfFloat10[7] = -14.1F;
            arrayOfFloat10[8] = -3.7F;
            arrayOfFloat1[8] = arrayOfFloat10;
            float[] arrayOfFloat11 = new float[10];
            arrayOfFloat11[0] = 5.4F;
            arrayOfFloat11[1] = 9.4F;
            arrayOfFloat11[2] = 3.4F;
            arrayOfFloat11[3] = -5.2F;
            arrayOfFloat11[4] = 3.1F;
            arrayOfFloat11[5] = -12.4F;
            arrayOfFloat11[6] = -0.7F;
            arrayOfFloat11[7] = 8.4F;
            arrayOfFloat11[8] = -8.5F;
            arrayOfFloat11[9] = -10.1F;
            arrayOfFloat1[9] = arrayOfFloat11;
            float[] arrayOfFloat12 = new float[11];
            arrayOfFloat12[0] = -2.0F;
            arrayOfFloat12[1] = -6.3F;
            arrayOfFloat12[2] = 0.9F;
            arrayOfFloat12[3] = -1.1F;
            arrayOfFloat12[4] = -0.2F;
            arrayOfFloat12[5] = 2.5F;
            arrayOfFloat12[6] = -0.3F;
            arrayOfFloat12[7] = 2.2F;
            arrayOfFloat12[8] = 3.1F;
            arrayOfFloat12[9] = -1.0F;
            arrayOfFloat12[10] = -2.8F;
            arrayOfFloat1[10] = arrayOfFloat12;
            float[] arrayOfFloat13 = new float[12];
            arrayOfFloat13[0] = 3.0F;
            arrayOfFloat13[1] = -1.5F;
            arrayOfFloat13[2] = -2.1F;
            arrayOfFloat13[3] = 1.7F;
            arrayOfFloat13[4] = -0.5F;
            arrayOfFloat13[5] = 0.5F;
            arrayOfFloat13[6] = -0.8F;
            arrayOfFloat13[7] = 0.4F;
            arrayOfFloat13[8] = 1.8F;
            arrayOfFloat13[9] = 0.1F;
            arrayOfFloat13[10] = 0.7F;
            arrayOfFloat13[11] = 3.8F;
            arrayOfFloat1[11] = arrayOfFloat13;
            float[] arrayOfFloat14 = new float[13];
            arrayOfFloat14[0] = -2.2F;
            arrayOfFloat14[1] = -0.2F;
            arrayOfFloat14[2] = 0.3F;
            arrayOfFloat14[3] = 1.0F;
            arrayOfFloat14[4] = -0.6F;
            arrayOfFloat14[5] = 0.9F;
            arrayOfFloat14[6] = -0.1F;
            arrayOfFloat14[7] = 0.5F;
            arrayOfFloat14[8] = -0.4F;
            arrayOfFloat14[9] = -0.4F;
            arrayOfFloat14[10] = 0.2F;
            arrayOfFloat14[11] = -0.8F;
            arrayOfFloat14[12] = 0.0F;
            arrayOfFloat1[12] = arrayOfFloat14;
            G_COEFF = arrayOfFloat1;
            float[][] arrayOfFloat15 = new float[13][];
            float[] arrayOfFloat16 = new float[1];
            arrayOfFloat16[0] = 0.0F;
            arrayOfFloat15[0] = arrayOfFloat16;
            float[] arrayOfFloat17 = new float[2];
            arrayOfFloat17[0] = 0.0F;
            arrayOfFloat17[1] = 4944.3999F;
            arrayOfFloat15[1] = arrayOfFloat17;
            float[] arrayOfFloat18 = new float[3];
            arrayOfFloat18[0] = 0.0F;
            arrayOfFloat18[1] = -2707.7F;
            arrayOfFloat18[2] = -576.09998F;
            arrayOfFloat15[2] = arrayOfFloat18;
            float[] arrayOfFloat19 = new float[4];
            arrayOfFloat19[0] = 0.0F;
            arrayOfFloat19[1] = -160.2F;
            arrayOfFloat19[2] = 251.89999F;
            arrayOfFloat19[3] = -536.59998F;
            arrayOfFloat15[3] = arrayOfFloat19;
            float[] arrayOfFloat20 = new float[5];
            arrayOfFloat20[0] = 0.0F;
            arrayOfFloat20[1] = 286.39999F;
            arrayOfFloat20[2] = -211.2F;
            arrayOfFloat20[3] = 164.3F;
            arrayOfFloat20[4] = -309.10001F;
            arrayOfFloat15[4] = arrayOfFloat20;
            float[] arrayOfFloat21 = new float[6];
            arrayOfFloat21[0] = 0.0F;
            arrayOfFloat21[1] = 44.599998F;
            arrayOfFloat21[2] = 188.89999F;
            arrayOfFloat21[3] = -118.2F;
            arrayOfFloat21[4] = 0.0F;
            arrayOfFloat21[5] = 100.9F;
            arrayOfFloat15[5] = arrayOfFloat21;
            float[] arrayOfFloat22 = new float[7];
            arrayOfFloat22[0] = 0.0F;
            arrayOfFloat22[1] = -20.799999F;
            arrayOfFloat22[2] = 44.099998F;
            arrayOfFloat22[3] = 61.5F;
            arrayOfFloat22[4] = -66.300003F;
            arrayOfFloat22[5] = 3.1F;
            arrayOfFloat22[6] = 55.0F;
            arrayOfFloat15[6] = arrayOfFloat22;
            float[] arrayOfFloat23 = new float[8];
            arrayOfFloat23[0] = 0.0F;
            arrayOfFloat23[1] = -57.900002F;
            arrayOfFloat23[2] = -21.1F;
            arrayOfFloat23[3] = 6.5F;
            arrayOfFloat23[4] = 24.9F;
            arrayOfFloat23[5] = 7.0F;
            arrayOfFloat23[6] = -27.700001F;
            arrayOfFloat23[7] = -3.3F;
            arrayOfFloat15[7] = arrayOfFloat23;
            float[] arrayOfFloat24 = new float[9];
            arrayOfFloat24[0] = 0.0F;
            arrayOfFloat24[1] = 11.0F;
            arrayOfFloat24[2] = -20.0F;
            arrayOfFloat24[3] = 11.9F;
            arrayOfFloat24[4] = -17.4F;
            arrayOfFloat24[5] = 16.700001F;
            arrayOfFloat24[6] = 7.0F;
            arrayOfFloat24[7] = -10.8F;
            arrayOfFloat24[8] = 1.7F;
            arrayOfFloat15[8] = arrayOfFloat24;
            float[] arrayOfFloat25 = new float[10];
            arrayOfFloat25[0] = 0.0F;
            arrayOfFloat25[1] = -20.5F;
            arrayOfFloat25[2] = 11.5F;
            arrayOfFloat25[3] = 12.8F;
            arrayOfFloat25[4] = -7.2F;
            arrayOfFloat25[5] = -7.4F;
            arrayOfFloat25[6] = 8.0F;
            arrayOfFloat25[7] = 2.1F;
            arrayOfFloat25[8] = -6.1F;
            arrayOfFloat25[9] = 7.0F;
            arrayOfFloat15[9] = arrayOfFloat25;
            float[] arrayOfFloat26 = new float[11];
            arrayOfFloat26[0] = 0.0F;
            arrayOfFloat26[1] = 2.8F;
            arrayOfFloat26[2] = -0.1F;
            arrayOfFloat26[3] = 4.7F;
            arrayOfFloat26[4] = 4.4F;
            arrayOfFloat26[5] = -7.2F;
            arrayOfFloat26[6] = -1.0F;
            arrayOfFloat26[7] = -3.9F;
            arrayOfFloat26[8] = -2.0F;
            arrayOfFloat26[9] = -2.0F;
            arrayOfFloat26[10] = -8.3F;
            arrayOfFloat15[10] = arrayOfFloat26;
            float[] arrayOfFloat27 = new float[12];
            arrayOfFloat27[0] = 0.0F;
            arrayOfFloat27[1] = 0.2F;
            arrayOfFloat27[2] = 1.7F;
            arrayOfFloat27[3] = -0.6F;
            arrayOfFloat27[4] = -1.8F;
            arrayOfFloat27[5] = 0.9F;
            arrayOfFloat27[6] = -0.4F;
            arrayOfFloat27[7] = -2.5F;
            arrayOfFloat27[8] = -1.3F;
            arrayOfFloat27[9] = -2.1F;
            arrayOfFloat27[10] = -1.9F;
            arrayOfFloat27[11] = -1.8F;
            arrayOfFloat15[11] = arrayOfFloat27;
            float[] arrayOfFloat28 = new float[13];
            arrayOfFloat28[0] = 0.0F;
            arrayOfFloat28[1] = -0.9F;
            arrayOfFloat28[2] = 0.3F;
            arrayOfFloat28[3] = 2.1F;
            arrayOfFloat28[4] = -2.5F;
            arrayOfFloat28[5] = 0.5F;
            arrayOfFloat28[6] = 0.6F;
            arrayOfFloat28[7] = 0.0F;
            arrayOfFloat28[8] = 0.1F;
            arrayOfFloat28[9] = 0.3F;
            arrayOfFloat28[10] = -0.9F;
            arrayOfFloat28[11] = -0.2F;
            arrayOfFloat28[12] = 0.9F;
            arrayOfFloat15[12] = arrayOfFloat28;
            H_COEFF = arrayOfFloat15;
            float[][] arrayOfFloat29 = new float[13][];
            float[] arrayOfFloat30 = new float[1];
            arrayOfFloat30[0] = 0.0F;
            arrayOfFloat29[0] = arrayOfFloat30;
            float[] arrayOfFloat31 = new float[2];
            arrayOfFloat31[0] = 11.6F;
            arrayOfFloat31[1] = 16.5F;
            arrayOfFloat29[1] = arrayOfFloat31;
            float[] arrayOfFloat32 = new float[3];
            arrayOfFloat32[0] = -12.1F;
            arrayOfFloat32[1] = -4.4F;
            arrayOfFloat32[2] = 1.9F;
            arrayOfFloat29[2] = arrayOfFloat32;
            float[] arrayOfFloat33 = new float[4];
            arrayOfFloat33[0] = 0.4F;
            arrayOfFloat33[1] = -4.1F;
            arrayOfFloat33[2] = -2.9F;
            arrayOfFloat33[3] = -7.7F;
            arrayOfFloat29[3] = arrayOfFloat33;
            float[] arrayOfFloat34 = new float[5];
            arrayOfFloat34[0] = -1.8F;
            arrayOfFloat34[1] = 2.3F;
            arrayOfFloat34[2] = -8.7F;
            arrayOfFloat34[3] = 4.6F;
            arrayOfFloat34[4] = -2.1F;
            arrayOfFloat29[4] = arrayOfFloat34;
            float[] arrayOfFloat35 = new float[6];
            arrayOfFloat35[0] = -1.0F;
            arrayOfFloat35[1] = 0.6F;
            arrayOfFloat35[2] = -1.8F;
            arrayOfFloat35[3] = -1.0F;
            arrayOfFloat35[4] = 0.9F;
            arrayOfFloat35[5] = 1.0F;
            arrayOfFloat29[5] = arrayOfFloat35;
            float[] arrayOfFloat36 = new float[7];
            arrayOfFloat36[0] = -0.2F;
            arrayOfFloat36[1] = -0.2F;
            arrayOfFloat36[2] = -0.1F;
            arrayOfFloat36[3] = 2.0F;
            arrayOfFloat36[4] = -1.7F;
            arrayOfFloat36[5] = -0.3F;
            arrayOfFloat36[6] = 1.7F;
            arrayOfFloat29[6] = arrayOfFloat36;
            float[] arrayOfFloat37 = new float[8];
            arrayOfFloat37[0] = 0.1F;
            arrayOfFloat37[1] = -0.1F;
            arrayOfFloat37[2] = -0.6F;
            arrayOfFloat37[3] = 1.3F;
            arrayOfFloat37[4] = 0.4F;
            arrayOfFloat37[5] = 0.3F;
            arrayOfFloat37[6] = -0.7F;
            arrayOfFloat37[7] = 0.6F;
            arrayOfFloat29[7] = arrayOfFloat37;
            float[] arrayOfFloat38 = new float[9];
            arrayOfFloat38[0] = -0.1F;
            arrayOfFloat38[1] = 0.1F;
            arrayOfFloat38[2] = -0.6F;
            arrayOfFloat38[3] = 0.2F;
            arrayOfFloat38[4] = -0.2F;
            arrayOfFloat38[5] = 0.3F;
            arrayOfFloat38[6] = 0.3F;
            arrayOfFloat38[7] = -0.6F;
            arrayOfFloat38[8] = 0.2F;
            arrayOfFloat29[8] = arrayOfFloat38;
            float[] arrayOfFloat39 = new float[10];
            arrayOfFloat39[0] = 0.0F;
            arrayOfFloat39[1] = -0.1F;
            arrayOfFloat39[2] = 0.0F;
            arrayOfFloat39[3] = 0.3F;
            arrayOfFloat39[4] = -0.4F;
            arrayOfFloat39[5] = -0.3F;
            arrayOfFloat39[6] = 0.1F;
            arrayOfFloat39[7] = -0.1F;
            arrayOfFloat39[8] = -0.4F;
            arrayOfFloat39[9] = -0.2F;
            arrayOfFloat29[9] = arrayOfFloat39;
            float[] arrayOfFloat40 = new float[11];
            arrayOfFloat40[0] = 0.0F;
            arrayOfFloat40[1] = 0.0F;
            arrayOfFloat40[2] = -0.1F;
            arrayOfFloat40[3] = 0.2F;
            arrayOfFloat40[4] = 0.0F;
            arrayOfFloat40[5] = -0.1F;
            arrayOfFloat40[6] = -0.2F;
            arrayOfFloat40[7] = 0.0F;
            arrayOfFloat40[8] = -0.1F;
            arrayOfFloat40[9] = -0.2F;
            arrayOfFloat40[10] = -0.2F;
            arrayOfFloat29[10] = arrayOfFloat40;
            float[] arrayOfFloat41 = new float[12];
            arrayOfFloat41[0] = 0.0F;
            arrayOfFloat41[1] = 0.0F;
            arrayOfFloat41[2] = 0.0F;
            arrayOfFloat41[3] = 0.1F;
            arrayOfFloat41[4] = 0.0F;
            arrayOfFloat41[5] = 0.0F;
            arrayOfFloat41[6] = 0.0F;
            arrayOfFloat41[7] = 0.0F;
            arrayOfFloat41[8] = 0.0F;
            arrayOfFloat41[9] = 0.0F;
            arrayOfFloat41[10] = -0.1F;
            arrayOfFloat41[11] = 0.0F;
            arrayOfFloat29[11] = arrayOfFloat41;
            float[] arrayOfFloat42 = new float[13];
            arrayOfFloat42[0] = 0.0F;
            arrayOfFloat42[1] = 0.0F;
            arrayOfFloat42[2] = 0.1F;
            arrayOfFloat42[3] = 0.1F;
            arrayOfFloat42[4] = -0.1F;
            arrayOfFloat42[5] = 0.0F;
            arrayOfFloat42[6] = 0.0F;
            arrayOfFloat42[7] = 0.0F;
            arrayOfFloat42[8] = 0.0F;
            arrayOfFloat42[9] = 0.0F;
            arrayOfFloat42[10] = 0.0F;
            arrayOfFloat42[11] = -0.1F;
            arrayOfFloat42[12] = 0.1F;
            arrayOfFloat29[12] = arrayOfFloat42;
            DELTA_G = arrayOfFloat29;
            float[][] arrayOfFloat43 = new float[13][];
            float[] arrayOfFloat44 = new float[1];
            arrayOfFloat44[0] = 0.0F;
            arrayOfFloat43[0] = arrayOfFloat44;
            float[] arrayOfFloat45 = new float[2];
            arrayOfFloat45[0] = 0.0F;
            arrayOfFloat45[1] = -25.9F;
            arrayOfFloat43[1] = arrayOfFloat45;
            float[] arrayOfFloat46 = new float[3];
            arrayOfFloat46[0] = 0.0F;
            arrayOfFloat46[1] = -22.5F;
            arrayOfFloat46[2] = -11.8F;
            arrayOfFloat43[2] = arrayOfFloat46;
            float[] arrayOfFloat47 = new float[4];
            arrayOfFloat47[0] = 0.0F;
            arrayOfFloat47[1] = 7.3F;
            arrayOfFloat47[2] = -3.9F;
            arrayOfFloat47[3] = -2.6F;
            arrayOfFloat43[3] = arrayOfFloat47;
            float[] arrayOfFloat48 = new float[5];
            arrayOfFloat48[0] = 0.0F;
            arrayOfFloat48[1] = 1.1F;
            arrayOfFloat48[2] = 2.7F;
            arrayOfFloat48[3] = 3.9F;
            arrayOfFloat48[4] = -0.8F;
            arrayOfFloat43[4] = arrayOfFloat48;
            float[] arrayOfFloat49 = new float[6];
            arrayOfFloat49[0] = 0.0F;
            arrayOfFloat49[1] = 0.4F;
            arrayOfFloat49[2] = 1.8F;
            arrayOfFloat49[3] = 1.2F;
            arrayOfFloat49[4] = 4.0F;
            arrayOfFloat49[5] = -0.6F;
            arrayOfFloat43[5] = arrayOfFloat49;
            float[] arrayOfFloat50 = new float[7];
            arrayOfFloat50[0] = 0.0F;
            arrayOfFloat50[1] = -0.2F;
            arrayOfFloat50[2] = -2.1F;
            arrayOfFloat50[3] = -0.4F;
            arrayOfFloat50[4] = -0.6F;
            arrayOfFloat50[5] = 0.5F;
            arrayOfFloat50[6] = 0.9F;
            arrayOfFloat43[6] = arrayOfFloat50;
            float[] arrayOfFloat51 = new float[8];
            arrayOfFloat51[0] = 0.0F;
            arrayOfFloat51[1] = 0.7F;
            arrayOfFloat51[2] = 0.3F;
            arrayOfFloat51[3] = -0.1F;
            arrayOfFloat51[4] = -0.1F;
            arrayOfFloat51[5] = -0.8F;
            arrayOfFloat51[6] = -0.3F;
            arrayOfFloat51[7] = 0.3F;
            arrayOfFloat43[7] = arrayOfFloat51;
            float[] arrayOfFloat52 = new float[9];
            arrayOfFloat52[0] = 0.0F;
            arrayOfFloat52[1] = -0.1F;
            arrayOfFloat52[2] = 0.2F;
            arrayOfFloat52[3] = 0.4F;
            arrayOfFloat52[4] = 0.4F;
            arrayOfFloat52[5] = 0.1F;
            arrayOfFloat52[6] = -0.1F;
            arrayOfFloat52[7] = 0.4F;
            arrayOfFloat52[8] = 0.3F;
            arrayOfFloat43[8] = arrayOfFloat52;
            float[] arrayOfFloat53 = new float[10];
            arrayOfFloat53[0] = 0.0F;
            arrayOfFloat53[1] = 0.0F;
            arrayOfFloat53[2] = -0.2F;
            arrayOfFloat53[3] = 0.0F;
            arrayOfFloat53[4] = -0.1F;
            arrayOfFloat53[5] = 0.1F;
            arrayOfFloat53[6] = 0.0F;
            arrayOfFloat53[7] = -0.2F;
            arrayOfFloat53[8] = 0.3F;
            arrayOfFloat53[9] = 0.2F;
            arrayOfFloat43[9] = arrayOfFloat53;
            float[] arrayOfFloat54 = new float[11];
            arrayOfFloat54[0] = 0.0F;
            arrayOfFloat54[1] = 0.1F;
            arrayOfFloat54[2] = -0.1F;
            arrayOfFloat54[3] = 0.0F;
            arrayOfFloat54[4] = -0.1F;
            arrayOfFloat54[5] = -0.1F;
            arrayOfFloat54[6] = 0.0F;
            arrayOfFloat54[7] = -0.1F;
            arrayOfFloat54[8] = -0.2F;
            arrayOfFloat54[9] = 0.0F;
            arrayOfFloat54[10] = -0.1F;
            arrayOfFloat43[10] = arrayOfFloat54;
            float[] arrayOfFloat55 = new float[12];
            arrayOfFloat55[0] = 0.0F;
            arrayOfFloat55[1] = 0.0F;
            arrayOfFloat55[2] = 0.1F;
            arrayOfFloat55[3] = 0.0F;
            arrayOfFloat55[4] = 0.1F;
            arrayOfFloat55[5] = 0.0F;
            arrayOfFloat55[6] = 0.1F;
            arrayOfFloat55[7] = 0.0F;
            arrayOfFloat55[8] = -0.1F;
            arrayOfFloat55[9] = -0.1F;
            arrayOfFloat55[10] = 0.0F;
            arrayOfFloat55[11] = -0.1F;
            arrayOfFloat43[11] = arrayOfFloat55;
            float[] arrayOfFloat56 = new float[13];
            arrayOfFloat56[0] = 0.0F;
            arrayOfFloat56[1] = 0.0F;
            arrayOfFloat56[2] = 0.0F;
            arrayOfFloat56[3] = 0.0F;
            arrayOfFloat56[4] = 0.0F;
            arrayOfFloat56[5] = 0.0F;
            arrayOfFloat56[6] = 0.1F;
            arrayOfFloat56[7] = 0.0F;
            arrayOfFloat56[8] = 0.0F;
            arrayOfFloat56[9] = 0.0F;
            arrayOfFloat56[10] = 0.0F;
            arrayOfFloat56[11] = 0.0F;
            arrayOfFloat56[12] = 0.0F;
            arrayOfFloat43[12] = arrayOfFloat56;
            DELTA_H = arrayOfFloat43;
            BASE_TIME = new GregorianCalendar(2010, 1, 1).getTimeInMillis();
            SCHMIDT_QUASI_NORM_FACTORS = computeSchmidtQuasiNormFactors(G_COEFF.length);
            return;
        }
    }

    public GeomagneticField(float paramFloat1, float paramFloat2, float paramFloat3, long paramLong)
    {
        int i = G_COEFF.length;
        float f1 = Math.min(89.999992F, Math.max(-89.999992F, paramFloat1));
        computeGeocentricCoordinates(f1, paramFloat2, paramFloat3);
        assert (G_COEFF.length == H_COEFF.length);
        LegendreTable localLegendreTable = new LegendreTable(i - 1, (float)(1.570796326794897D - this.mGcLatitudeRad));
        float[] arrayOfFloat1 = new float[i + 2];
        arrayOfFloat1[0] = 1.0F;
        arrayOfFloat1[1] = (6371.2002F / this.mGcRadiusKm);
        for (int j = 2; j < arrayOfFloat1.length; j++)
            arrayOfFloat1[j] = (arrayOfFloat1[(j - 1)] * arrayOfFloat1[1]);
        float[] arrayOfFloat2 = new float[i];
        float[] arrayOfFloat3 = new float[i];
        arrayOfFloat2[0] = 0.0F;
        arrayOfFloat3[0] = 1.0F;
        arrayOfFloat2[1] = ((float)Math.sin(this.mGcLongitudeRad));
        arrayOfFloat3[1] = ((float)Math.cos(this.mGcLongitudeRad));
        for (int k = 2; k < i; k++)
        {
            int i1 = k >> 1;
            arrayOfFloat2[k] = (arrayOfFloat2[(k - i1)] * arrayOfFloat3[i1] + arrayOfFloat3[(k - i1)] * arrayOfFloat2[i1]);
            arrayOfFloat3[k] = (arrayOfFloat3[(k - i1)] * arrayOfFloat3[i1] - arrayOfFloat2[(k - i1)] * arrayOfFloat2[i1]);
        }
        float f2 = 1.0F / (float)Math.cos(this.mGcLatitudeRad);
        float f3 = (float)(paramLong - BASE_TIME) / 3.1536E+10F;
        float f4 = 0.0F;
        float f5 = 0.0F;
        float f6 = 0.0F;
        for (int m = 1; m < i; m++)
            for (int n = 0; n <= m; n++)
            {
                float f7 = G_COEFF[m][n] + f3 * DELTA_G[m][n];
                float f8 = H_COEFF[m][n] + f3 * DELTA_H[m][n];
                f4 += arrayOfFloat1[(m + 2)] * (f7 * arrayOfFloat3[n] + f8 * arrayOfFloat2[n]) * localLegendreTable.mPDeriv[m][n] * SCHMIDT_QUASI_NORM_FACTORS[m][n];
                f5 += f2 * (arrayOfFloat1[(m + 2)] * n * (f7 * arrayOfFloat2[n] - f8 * arrayOfFloat3[n]) * localLegendreTable.mP[m][n] * SCHMIDT_QUASI_NORM_FACTORS[m][n]);
                f6 -= (m + 1) * arrayOfFloat1[(m + 2)] * (f7 * arrayOfFloat3[n] + f8 * arrayOfFloat2[n]) * localLegendreTable.mP[m][n] * SCHMIDT_QUASI_NORM_FACTORS[m][n];
            }
        double d = Math.toRadians(f1) - this.mGcLatitudeRad;
        this.mX = ((float)(f4 * Math.cos(d) + f6 * Math.sin(d)));
        this.mY = f5;
        this.mZ = ((float)(-f4 * Math.sin(d) + f6 * Math.cos(d)));
    }

    private void computeGeocentricCoordinates(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        float f1 = paramFloat3 / 1000.0F;
        double d = Math.toRadians(paramFloat1);
        float f2 = (float)Math.cos(d);
        float f3 = (float)Math.sin(d);
        float f4 = f3 / f2;
        float f5 = (float)Math.sqrt(f2 * (40680636.0F * f2) + f3 * (40408300.0F * f3));
        this.mGcLatitudeRad = ((float)Math.atan(f4 * (40408300.0F + f5 * f1) / (40680636.0F + f5 * f1)));
        this.mGcLongitudeRad = ((float)Math.toRadians(paramFloat2));
        this.mGcRadiusKm = ((float)Math.sqrt(f1 * f1 + 2.0F * f1 * (float)Math.sqrt(f2 * (40680636.0F * f2) + f3 * (40408300.0F * f3)) + (f2 * (f2 * (40680636.0F * 40680636.0F)) + f3 * (f3 * (40408300.0F * 40408300.0F))) / (f2 * (40680636.0F * f2) + f3 * (40408300.0F * f3))));
    }

    private static float[][] computeSchmidtQuasiNormFactors(int paramInt)
    {
        float[][] arrayOfFloat = new float[paramInt + 1][];
        float[] arrayOfFloat1 = new float[1];
        arrayOfFloat1[0] = 1.0F;
        arrayOfFloat[0] = arrayOfFloat1;
        for (int i = 1; i <= paramInt; i++)
        {
            arrayOfFloat[i] = new float[i + 1];
            arrayOfFloat[i][0] = (arrayOfFloat[(i - 1)][0] * (-1 + i * 2) / i);
            int j = 1;
            if (j <= i)
            {
                [F local[F = arrayOfFloat[i];
                float f = arrayOfFloat[i][(j - 1)];
                int k = 1 + (i - j);
                if (j == 1);
                for (int m = 2; ; m = 1)
                {
                    local[F[j] = (f * (float)Math.sqrt(m * k / (i + j)));
                    j++;
                    break;
                }
            }
        }
        return arrayOfFloat;
    }

    public float getDeclination()
    {
        return (float)Math.toDegrees(Math.atan2(this.mY, this.mX));
    }

    public float getFieldStrength()
    {
        return (float)Math.sqrt(this.mX * this.mX + this.mY * this.mY + this.mZ * this.mZ);
    }

    public float getHorizontalStrength()
    {
        return (float)Math.sqrt(this.mX * this.mX + this.mY * this.mY);
    }

    public float getInclination()
    {
        return (float)Math.toDegrees(Math.atan2(this.mZ, getHorizontalStrength()));
    }

    public float getX()
    {
        return this.mX;
    }

    public float getY()
    {
        return this.mY;
    }

    public float getZ()
    {
        return this.mZ;
    }

    private static class LegendreTable
    {
        public final float[][] mP;
        public final float[][] mPDeriv;

        static
        {
            if (!GeomagneticField.class.desiredAssertionStatus());
            for (boolean bool = true; ; bool = false)
            {
                $assertionsDisabled = bool;
                return;
            }
        }

        public LegendreTable(int paramInt, float paramFloat)
        {
            float f1 = (float)Math.cos(paramFloat);
            float f2 = (float)Math.sin(paramFloat);
            this.mP = new float[paramInt + 1][];
            this.mPDeriv = new float[paramInt + 1][];
            float[][] arrayOfFloat1 = this.mP;
            float[] arrayOfFloat2 = new float[1];
            arrayOfFloat2[0] = 1.0F;
            arrayOfFloat1[0] = arrayOfFloat2;
            float[][] arrayOfFloat3 = this.mPDeriv;
            float[] arrayOfFloat4 = new float[1];
            arrayOfFloat4[0] = 0.0F;
            arrayOfFloat3[0] = arrayOfFloat4;
            for (int i = 1; i <= paramInt; i++)
            {
                this.mP[i] = new float[i + 1];
                this.mPDeriv[i] = new float[i + 1];
                int j = 0;
                if (j <= i)
                {
                    if (i == j)
                    {
                        this.mP[i][j] = (f2 * this.mP[(i - 1)][(j - 1)]);
                        this.mPDeriv[i][j] = (f1 * this.mP[(i - 1)][(j - 1)] + f2 * this.mPDeriv[(i - 1)][(j - 1)]);
                    }
                    while (true)
                    {
                        j++;
                        break;
                        if ((i == 1) || (j == i - 1))
                        {
                            this.mP[i][j] = (f1 * this.mP[(i - 1)][j]);
                            this.mPDeriv[i][j] = (-f2 * this.mP[(i - 1)][j] + f1 * this.mPDeriv[(i - 1)][j]);
                        }
                        else
                        {
                            assert ((i > 1) && (j < i - 1));
                            float f3 = ((i - 1) * (i - 1) - j * j) / ((-1 + i * 2) * (-3 + i * 2));
                            this.mP[i][j] = (f1 * this.mP[(i - 1)][j] - f3 * this.mP[(i - 2)][j]);
                            this.mPDeriv[i][j] = (-f2 * this.mP[(i - 1)][j] + f1 * this.mPDeriv[(i - 1)][j] - f3 * this.mPDeriv[(i - 2)][j]);
                        }
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.hardware.GeomagneticField
 * JD-Core Version:        0.6.2
 */