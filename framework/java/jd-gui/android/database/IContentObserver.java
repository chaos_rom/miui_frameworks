package android.database;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IContentObserver extends IInterface
{
    public abstract void onChange(boolean paramBoolean, Uri paramUri)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IContentObserver
    {
        private static final String DESCRIPTOR = "android.database.IContentObserver";
        static final int TRANSACTION_onChange = 1;

        public Stub()
        {
            attachInterface(this, "android.database.IContentObserver");
        }

        public static IContentObserver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.database.IContentObserver");
                if ((localIInterface != null) && ((localIInterface instanceof IContentObserver)))
                    localObject = (IContentObserver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.database.IContentObserver");
                }
            case 1:
            }
            paramParcel1.enforceInterface("android.database.IContentObserver");
            boolean bool2;
            if (paramParcel1.readInt() != 0)
            {
                bool2 = bool1;
                label72: if (paramParcel1.readInt() == 0)
                    break label110;
            }
            label110: for (Uri localUri = (Uri)Uri.CREATOR.createFromParcel(paramParcel1); ; localUri = null)
            {
                onChange(bool2, localUri);
                break;
                bool2 = false;
                break label72;
            }
        }

        private static class Proxy
            implements IContentObserver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.database.IContentObserver";
            }

            public void onChange(boolean paramBoolean, Uri paramUri)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.database.IContentObserver");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        if (paramUri == null)
                            break label67;
                        localParcel.writeInt(1);
                        paramUri.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        i = 0;
                        break;
                        label67: localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.IContentObserver
 * JD-Core Version:        0.6.2
 */