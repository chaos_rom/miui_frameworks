package android.database.sqlite;

public class SQLiteDoneException extends SQLiteException
{
    public SQLiteDoneException()
    {
    }

    public SQLiteDoneException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteDoneException
 * JD-Core Version:        0.6.2
 */