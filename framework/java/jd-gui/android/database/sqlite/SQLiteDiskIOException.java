package android.database.sqlite;

public class SQLiteDiskIOException extends SQLiteException
{
    public SQLiteDiskIOException()
    {
    }

    public SQLiteDiskIOException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteDiskIOException
 * JD-Core Version:        0.6.2
 */