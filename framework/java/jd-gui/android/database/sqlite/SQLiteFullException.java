package android.database.sqlite;

public class SQLiteFullException extends SQLiteException
{
    public SQLiteFullException()
    {
    }

    public SQLiteFullException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteFullException
 * JD-Core Version:        0.6.2
 */