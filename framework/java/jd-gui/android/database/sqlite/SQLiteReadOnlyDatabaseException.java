package android.database.sqlite;

public class SQLiteReadOnlyDatabaseException extends SQLiteException
{
    public SQLiteReadOnlyDatabaseException()
    {
    }

    public SQLiteReadOnlyDatabaseException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteReadOnlyDatabaseException
 * JD-Core Version:        0.6.2
 */