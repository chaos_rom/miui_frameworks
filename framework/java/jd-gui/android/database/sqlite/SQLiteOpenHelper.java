package android.database.sqlite;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.util.Log;
import java.io.File;

public abstract class SQLiteOpenHelper
{
    private static final boolean DEBUG_STRICT_READONLY;
    private static final String TAG = SQLiteOpenHelper.class.getSimpleName();
    private final Context mContext;
    private SQLiteDatabase mDatabase;
    private boolean mEnableWriteAheadLogging;
    private final DatabaseErrorHandler mErrorHandler;
    private final SQLiteDatabase.CursorFactory mFactory;
    private boolean mIsInitializing;
    private final String mName;
    private final int mNewVersion;

    public SQLiteOpenHelper(Context paramContext, String paramString, SQLiteDatabase.CursorFactory paramCursorFactory, int paramInt)
    {
        this(paramContext, paramString, paramCursorFactory, paramInt, null);
    }

    public SQLiteOpenHelper(Context paramContext, String paramString, SQLiteDatabase.CursorFactory paramCursorFactory, int paramInt, DatabaseErrorHandler paramDatabaseErrorHandler)
    {
        if (paramInt < 1)
            throw new IllegalArgumentException("Version must be >= 1, was " + paramInt);
        this.mContext = paramContext;
        this.mName = paramString;
        this.mFactory = paramCursorFactory;
        this.mNewVersion = paramInt;
        this.mErrorHandler = paramDatabaseErrorHandler;
    }

    private SQLiteDatabase getDatabaseLocked(boolean paramBoolean)
    {
        if (this.mDatabase != null)
        {
            if (this.mDatabase.isOpen())
                break label39;
            this.mDatabase = null;
        }
        while (this.mIsInitializing)
        {
            throw new IllegalStateException("getDatabase called recursively");
            label39: if ((!paramBoolean) || (!this.mDatabase.isReadOnly()))
            {
                localObject1 = this.mDatabase;
                return localObject1;
            }
        }
        Object localObject1 = this.mDatabase;
        int i;
        while (true)
        {
            try
            {
                this.mIsInitializing = true;
                if (localObject1 != null)
                {
                    if ((paramBoolean) && (((SQLiteDatabase)localObject1).isReadOnly()))
                        ((SQLiteDatabase)localObject1).reopenReadWrite();
                    onConfigure((SQLiteDatabase)localObject1);
                    i = ((SQLiteDatabase)localObject1).getVersion();
                    if (i == this.mNewVersion)
                        break label372;
                    if (!((SQLiteDatabase)localObject1).isReadOnly())
                        break;
                    throw new SQLiteException("Can't upgrade read-only database from version " + ((SQLiteDatabase)localObject1).getVersion() + " to " + this.mNewVersion + ": " + this.mName);
                }
            }
            finally
            {
                this.mIsInitializing = false;
                if ((localObject1 != null) && (localObject1 != this.mDatabase))
                    ((SQLiteDatabase)localObject1).close();
            }
            if (this.mName == null)
            {
                SQLiteDatabase localSQLiteDatabase2 = SQLiteDatabase.create(null);
                localObject1 = localSQLiteDatabase2;
            }
            else
            {
                try
                {
                    Context localContext = this.mContext;
                    String str = this.mName;
                    if (this.mEnableWriteAheadLogging);
                    for (int j = 8; ; j = 0)
                    {
                        SQLiteDatabase localSQLiteDatabase1 = localContext.openOrCreateDatabase(str, j, this.mFactory, this.mErrorHandler);
                        localObject1 = localSQLiteDatabase1;
                        break;
                    }
                }
                catch (SQLiteException localSQLiteException)
                {
                    if (paramBoolean)
                        throw localSQLiteException;
                    Log.e(TAG, "Couldn't open " + this.mName + " for writing (will try read-only):", localSQLiteException);
                    localObject1 = SQLiteDatabase.openDatabase(this.mContext.getDatabasePath(this.mName).getPath(), this.mFactory, 1, this.mErrorHandler);
                }
            }
        }
        ((SQLiteDatabase)localObject1).beginTransaction();
        if (i == 0);
        while (true)
        {
            try
            {
                onCreate((SQLiteDatabase)localObject1);
                ((SQLiteDatabase)localObject1).setVersion(this.mNewVersion);
                ((SQLiteDatabase)localObject1).setTransactionSuccessful();
                ((SQLiteDatabase)localObject1).endTransaction();
                label372: onOpen((SQLiteDatabase)localObject1);
                if (((SQLiteDatabase)localObject1).isReadOnly())
                    Log.w(TAG, "Opened " + this.mName + " in read-only mode");
                this.mDatabase = ((SQLiteDatabase)localObject1);
                this.mIsInitializing = false;
                if ((localObject1 == null) || (localObject1 == this.mDatabase))
                    break;
                ((SQLiteDatabase)localObject1).close();
                break;
                if (i > this.mNewVersion)
                {
                    onDowngrade((SQLiteDatabase)localObject1, i, this.mNewVersion);
                    continue;
                }
            }
            finally
            {
                ((SQLiteDatabase)localObject1).endTransaction();
            }
            onUpgrade((SQLiteDatabase)localObject1, i, this.mNewVersion);
        }
    }

    /** @deprecated */
    public void close()
    {
        try
        {
            if (this.mIsInitializing)
                throw new IllegalStateException("Closed during initialization");
        }
        finally
        {
        }
        if ((this.mDatabase != null) && (this.mDatabase.isOpen()))
        {
            this.mDatabase.close();
            this.mDatabase = null;
        }
    }

    public String getDatabaseName()
    {
        return this.mName;
    }

    public SQLiteDatabase getReadableDatabase()
    {
        try
        {
            SQLiteDatabase localSQLiteDatabase = getDatabaseLocked(false);
            return localSQLiteDatabase;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public SQLiteDatabase getWritableDatabase()
    {
        try
        {
            SQLiteDatabase localSQLiteDatabase = getDatabaseLocked(true);
            return localSQLiteDatabase;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void onConfigure(SQLiteDatabase paramSQLiteDatabase)
    {
    }

    public abstract void onCreate(SQLiteDatabase paramSQLiteDatabase);

    public void onDowngrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
    {
        throw new SQLiteException("Can't downgrade database from version " + paramInt1 + " to " + paramInt2);
    }

    public void onOpen(SQLiteDatabase paramSQLiteDatabase)
    {
    }

    public abstract void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2);

    public void setWriteAheadLoggingEnabled(boolean paramBoolean)
    {
        try
        {
            if (this.mEnableWriteAheadLogging != paramBoolean)
            {
                if ((this.mDatabase != null) && (this.mDatabase.isOpen()) && (!this.mDatabase.isReadOnly()))
                {
                    if (paramBoolean)
                        this.mDatabase.enableWriteAheadLogging();
                }
                else
                    this.mEnableWriteAheadLogging = paramBoolean;
            }
            else
                return;
            this.mDatabase.disableWriteAheadLogging();
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteOpenHelper
 * JD-Core Version:        0.6.2
 */