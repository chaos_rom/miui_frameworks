package android.database.sqlite;

public class SQLiteConstraintException extends SQLiteException
{
    public SQLiteConstraintException()
    {
    }

    public SQLiteConstraintException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteConstraintException
 * JD-Core Version:        0.6.2
 */