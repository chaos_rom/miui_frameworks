package android.database.sqlite;

public class SQLiteDatatypeMismatchException extends SQLiteException
{
    public SQLiteDatatypeMismatchException()
    {
    }

    public SQLiteDatatypeMismatchException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteDatatypeMismatchException
 * JD-Core Version:        0.6.2
 */