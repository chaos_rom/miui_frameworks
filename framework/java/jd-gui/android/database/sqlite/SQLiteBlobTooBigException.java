package android.database.sqlite;

public class SQLiteBlobTooBigException extends SQLiteException
{
    public SQLiteBlobTooBigException()
    {
    }

    public SQLiteBlobTooBigException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteBlobTooBigException
 * JD-Core Version:        0.6.2
 */