package android.database.sqlite;

public class SQLiteTableLockedException extends SQLiteException
{
    public SQLiteTableLockedException()
    {
    }

    public SQLiteTableLockedException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteTableLockedException
 * JD-Core Version:        0.6.2
 */