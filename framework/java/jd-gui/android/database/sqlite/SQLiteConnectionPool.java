package android.database.sqlite;

import android.os.CancellationSignal;
import android.os.CancellationSignal.OnCancelListener;
import android.os.OperationCanceledException;
import android.util.Log;
import dalvik.system.CloseGuard;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;

public final class SQLiteConnectionPool
    implements Closeable
{
    public static final int CONNECTION_FLAG_INTERACTIVE = 4;
    public static final int CONNECTION_FLAG_PRIMARY_CONNECTION_AFFINITY = 2;
    public static final int CONNECTION_FLAG_READ_ONLY = 1;
    private static final long CONNECTION_POOL_BUSY_MILLIS = 30000L;
    private static final String TAG = "SQLiteConnectionPool";
    private final WeakHashMap<SQLiteConnection, AcquiredConnectionStatus> mAcquiredConnections = new WeakHashMap();
    private final ArrayList<SQLiteConnection> mAvailableNonPrimaryConnections = new ArrayList();
    private SQLiteConnection mAvailablePrimaryConnection;
    private final CloseGuard mCloseGuard = CloseGuard.get();
    private final SQLiteDatabaseConfiguration mConfiguration;
    private final AtomicBoolean mConnectionLeaked = new AtomicBoolean();
    private ConnectionWaiter mConnectionWaiterPool;
    private ConnectionWaiter mConnectionWaiterQueue;
    private boolean mIsOpen;
    private final Object mLock = new Object();
    private int mMaxConnectionPoolSize;
    private int mNextConnectionId;

    static
    {
        if (!SQLiteConnectionPool.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    private SQLiteConnectionPool(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration)
    {
        this.mConfiguration = new SQLiteDatabaseConfiguration(paramSQLiteDatabaseConfiguration);
        setMaxConnectionPoolSizeLocked();
    }

    private void cancelConnectionWaiterLocked(ConnectionWaiter paramConnectionWaiter)
    {
        if ((paramConnectionWaiter.mAssignedConnection != null) || (paramConnectionWaiter.mException != null))
            return;
        Object localObject = null;
        for (ConnectionWaiter localConnectionWaiter = this.mConnectionWaiterQueue; localConnectionWaiter != paramConnectionWaiter; localConnectionWaiter = localConnectionWaiter.mNext)
        {
            assert (localConnectionWaiter != null);
            localObject = localConnectionWaiter;
        }
        if (localObject != null)
            localObject.mNext = paramConnectionWaiter.mNext;
        while (true)
        {
            paramConnectionWaiter.mException = new OperationCanceledException();
            LockSupport.unpark(paramConnectionWaiter.mThread);
            wakeConnectionWaitersLocked();
            break;
            this.mConnectionWaiterQueue = paramConnectionWaiter.mNext;
        }
    }

    private void closeAvailableConnectionsAndLogExceptionsLocked()
    {
        closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked();
        if (this.mAvailablePrimaryConnection != null)
        {
            closeConnectionAndLogExceptionsLocked(this.mAvailablePrimaryConnection);
            this.mAvailablePrimaryConnection = null;
        }
    }

    private void closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked()
    {
        int i = this.mAvailableNonPrimaryConnections.size();
        for (int j = 0; j < i; j++)
            closeConnectionAndLogExceptionsLocked((SQLiteConnection)this.mAvailableNonPrimaryConnections.get(j));
        this.mAvailableNonPrimaryConnections.clear();
    }

    private void closeConnectionAndLogExceptionsLocked(SQLiteConnection paramSQLiteConnection)
    {
        try
        {
            paramSQLiteConnection.close();
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Log.e("SQLiteConnectionPool", "Failed to close connection, its fate is now in the hands of the merciful GC: " + paramSQLiteConnection, localRuntimeException);
        }
    }

    private void closeExcessConnectionsAndLogExceptionsLocked()
    {
        int j;
        for (int i = this.mAvailableNonPrimaryConnections.size(); ; i = j)
        {
            j = i - 1;
            if (i <= -1 + this.mMaxConnectionPoolSize)
                break;
            closeConnectionAndLogExceptionsLocked((SQLiteConnection)this.mAvailableNonPrimaryConnections.remove(j));
        }
    }

    private void discardAcquiredConnectionsLocked()
    {
        markAcquiredConnectionsLocked(AcquiredConnectionStatus.DISCARD);
    }

    private void dispose(boolean paramBoolean)
    {
        if (this.mCloseGuard != null)
        {
            if (paramBoolean)
                this.mCloseGuard.warnIfOpen();
            this.mCloseGuard.close();
        }
        if (!paramBoolean)
            synchronized (this.mLock)
            {
                throwIfClosedLocked();
                this.mIsOpen = false;
                closeAvailableConnectionsAndLogExceptionsLocked();
                int i = this.mAcquiredConnections.size();
                if (i != 0)
                    Log.i("SQLiteConnectionPool", "The connection pool for " + this.mConfiguration.label + " has been closed but there are still " + i + " connections in use.    They will be closed " + "as they are released back to the pool.");
                wakeConnectionWaitersLocked();
            }
    }

    private void finishAcquireConnectionLocked(SQLiteConnection paramSQLiteConnection, int paramInt)
    {
        boolean bool;
        if ((paramInt & 0x1) != 0)
            bool = true;
        try
        {
            while (true)
            {
                paramSQLiteConnection.setOnlyAllowReadOnlyOperations(bool);
                this.mAcquiredConnections.put(paramSQLiteConnection, AcquiredConnectionStatus.NORMAL);
                return;
                bool = false;
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            Log.e("SQLiteConnectionPool", "Failed to prepare acquired connection for session, closing it: " + paramSQLiteConnection + ", connectionFlags=" + paramInt);
            closeConnectionAndLogExceptionsLocked(paramSQLiteConnection);
            throw localRuntimeException;
        }
    }

    private static int getPriority(int paramInt)
    {
        if ((paramInt & 0x4) != 0);
        for (int i = 1; ; i = 0)
            return i;
    }

    private boolean isSessionBlockingImportantConnectionWaitersLocked(boolean paramBoolean, int paramInt)
    {
        ConnectionWaiter localConnectionWaiter = this.mConnectionWaiterQueue;
        if (localConnectionWaiter != null)
        {
            int i = getPriority(paramInt);
            if (i <= localConnectionWaiter.mPriority)
                break label30;
        }
        while (true)
        {
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                label30: if ((!paramBoolean) && (localConnectionWaiter.mWantPrimaryConnection))
                    break;
            }
            localConnectionWaiter = localConnectionWaiter.mNext;
            if (localConnectionWaiter != null)
                break;
        }
    }

    private void logConnectionPoolBusyLocked(long paramLong, int paramInt)
    {
        Thread localThread = Thread.currentThread();
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("The connection pool for database '").append(this.mConfiguration.label);
        localStringBuilder.append("' has been unable to grant a connection to thread ");
        localStringBuilder.append(localThread.getId()).append(" (").append(localThread.getName()).append(") ");
        localStringBuilder.append("with flags 0x").append(Integer.toHexString(paramInt));
        localStringBuilder.append(" for ").append(0.001F * (float)paramLong).append(" seconds.\n");
        ArrayList localArrayList = new ArrayList();
        int i = 0;
        int j = 0;
        if (!this.mAcquiredConnections.isEmpty())
        {
            Iterator localIterator2 = this.mAcquiredConnections.keySet().iterator();
            while (localIterator2.hasNext())
            {
                String str2 = ((SQLiteConnection)localIterator2.next()).describeCurrentOperationUnsafe();
                if (str2 != null)
                {
                    localArrayList.add(str2);
                    i++;
                }
                else
                {
                    j++;
                }
            }
        }
        int k = this.mAvailableNonPrimaryConnections.size();
        if (this.mAvailablePrimaryConnection != null)
            k++;
        localStringBuilder.append("Connections: ").append(i).append(" active, ");
        localStringBuilder.append(j).append(" idle, ");
        localStringBuilder.append(k).append(" available.\n");
        if (!localArrayList.isEmpty())
        {
            localStringBuilder.append("\nRequests in progress:\n");
            Iterator localIterator1 = localArrayList.iterator();
            while (localIterator1.hasNext())
            {
                String str1 = (String)localIterator1.next();
                localStringBuilder.append("    ").append(str1).append("\n");
            }
        }
        Log.w("SQLiteConnectionPool", localStringBuilder.toString());
    }

    private void markAcquiredConnectionsLocked(AcquiredConnectionStatus paramAcquiredConnectionStatus)
    {
        if (!this.mAcquiredConnections.isEmpty())
        {
            ArrayList localArrayList = new ArrayList(this.mAcquiredConnections.size());
            Iterator localIterator = this.mAcquiredConnections.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                AcquiredConnectionStatus localAcquiredConnectionStatus = (AcquiredConnectionStatus)localEntry.getValue();
                if ((paramAcquiredConnectionStatus != localAcquiredConnectionStatus) && (localAcquiredConnectionStatus != AcquiredConnectionStatus.DISCARD))
                    localArrayList.add(localEntry.getKey());
            }
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
                this.mAcquiredConnections.put(localArrayList.get(j), paramAcquiredConnectionStatus);
        }
    }

    private ConnectionWaiter obtainConnectionWaiterLocked(Thread paramThread, long paramLong, int paramInt1, boolean paramBoolean, String paramString, int paramInt2)
    {
        ConnectionWaiter localConnectionWaiter = this.mConnectionWaiterPool;
        if (localConnectionWaiter != null)
        {
            this.mConnectionWaiterPool = localConnectionWaiter.mNext;
            localConnectionWaiter.mNext = null;
        }
        while (true)
        {
            localConnectionWaiter.mThread = paramThread;
            localConnectionWaiter.mStartTime = paramLong;
            localConnectionWaiter.mPriority = paramInt1;
            localConnectionWaiter.mWantPrimaryConnection = paramBoolean;
            localConnectionWaiter.mSql = paramString;
            localConnectionWaiter.mConnectionFlags = paramInt2;
            return localConnectionWaiter;
            localConnectionWaiter = new ConnectionWaiter(null);
        }
    }

    public static SQLiteConnectionPool open(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration)
    {
        if (paramSQLiteDatabaseConfiguration == null)
            throw new IllegalArgumentException("configuration must not be null.");
        SQLiteConnectionPool localSQLiteConnectionPool = new SQLiteConnectionPool(paramSQLiteDatabaseConfiguration);
        localSQLiteConnectionPool.open();
        return localSQLiteConnectionPool;
    }

    private void open()
    {
        this.mAvailablePrimaryConnection = openConnectionLocked(this.mConfiguration, true);
        this.mIsOpen = true;
        this.mCloseGuard.open("close");
    }

    private SQLiteConnection openConnectionLocked(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration, boolean paramBoolean)
    {
        int i = this.mNextConnectionId;
        this.mNextConnectionId = (i + 1);
        return SQLiteConnection.open(this, paramSQLiteDatabaseConfiguration, i, paramBoolean);
    }

    private void reconfigureAllConnectionsLocked()
    {
        if (this.mAvailablePrimaryConnection != null);
        try
        {
            this.mAvailablePrimaryConnection.reconfigure(this.mConfiguration);
            i = this.mAvailableNonPrimaryConnections.size();
            j = 0;
            if (j < i)
                localSQLiteConnection = (SQLiteConnection)this.mAvailableNonPrimaryConnections.get(j);
        }
        catch (RuntimeException localRuntimeException2)
        {
            try
            {
                while (true)
                {
                    localSQLiteConnection.reconfigure(this.mConfiguration);
                    k = j;
                    j = k + 1;
                }
                localRuntimeException2 = localRuntimeException2;
                Log.e("SQLiteConnectionPool", "Failed to reconfigure available primary connection, closing it: " + this.mAvailablePrimaryConnection, localRuntimeException2);
                closeConnectionAndLogExceptionsLocked(this.mAvailablePrimaryConnection);
                this.mAvailablePrimaryConnection = null;
            }
            catch (RuntimeException localRuntimeException1)
            {
                while (true)
                {
                    int i;
                    int j;
                    SQLiteConnection localSQLiteConnection;
                    Log.e("SQLiteConnectionPool", "Failed to reconfigure available non-primary connection, closing it: " + localSQLiteConnection, localRuntimeException1);
                    closeConnectionAndLogExceptionsLocked(localSQLiteConnection);
                    ArrayList localArrayList = this.mAvailableNonPrimaryConnections;
                    int k = j - 1;
                    localArrayList.remove(j);
                    i--;
                }
            }
            markAcquiredConnectionsLocked(AcquiredConnectionStatus.RECONFIGURE);
        }
    }

    private boolean recycleConnectionLocked(SQLiteConnection paramSQLiteConnection, AcquiredConnectionStatus paramAcquiredConnectionStatus)
    {
        if (paramAcquiredConnectionStatus == AcquiredConnectionStatus.RECONFIGURE);
        try
        {
            paramSQLiteConnection.reconfigure(this.mConfiguration);
            if (paramAcquiredConnectionStatus == AcquiredConnectionStatus.DISCARD)
            {
                closeConnectionAndLogExceptionsLocked(paramSQLiteConnection);
                bool = false;
                return bool;
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                Log.e("SQLiteConnectionPool", "Failed to reconfigure released connection, closing it: " + paramSQLiteConnection, localRuntimeException);
                paramAcquiredConnectionStatus = AcquiredConnectionStatus.DISCARD;
                continue;
                boolean bool = true;
            }
        }
    }

    private void recycleConnectionWaiterLocked(ConnectionWaiter paramConnectionWaiter)
    {
        paramConnectionWaiter.mNext = this.mConnectionWaiterPool;
        paramConnectionWaiter.mThread = null;
        paramConnectionWaiter.mSql = null;
        paramConnectionWaiter.mAssignedConnection = null;
        paramConnectionWaiter.mException = null;
        paramConnectionWaiter.mNonce = (1 + paramConnectionWaiter.mNonce);
        this.mConnectionWaiterPool = paramConnectionWaiter;
    }

    private void setMaxConnectionPoolSizeLocked()
    {
        if ((0x20000000 & this.mConfiguration.openFlags) != 0);
        for (this.mMaxConnectionPoolSize = SQLiteGlobal.getWALConnectionPoolSize(); ; this.mMaxConnectionPoolSize = 1)
            return;
    }

    private void throwIfClosedLocked()
    {
        if (!this.mIsOpen)
            throw new IllegalStateException("Cannot perform this operation because the connection pool has been closed.");
    }

    private SQLiteConnection tryAcquireNonPrimaryConnectionLocked(String paramString, int paramInt)
    {
        int i = this.mAvailableNonPrimaryConnections.size();
        int k;
        SQLiteConnection localSQLiteConnection;
        if ((i > 1) && (paramString != null))
        {
            k = 0;
            if (k < i)
            {
                localSQLiteConnection = (SQLiteConnection)this.mAvailableNonPrimaryConnections.get(k);
                if (localSQLiteConnection.isPreparedStatementInCache(paramString))
                {
                    this.mAvailableNonPrimaryConnections.remove(k);
                    finishAcquireConnectionLocked(localSQLiteConnection, paramInt);
                }
            }
        }
        while (true)
        {
            return localSQLiteConnection;
            k++;
            break;
            if (i > 0)
            {
                localSQLiteConnection = (SQLiteConnection)this.mAvailableNonPrimaryConnections.remove(i - 1);
                finishAcquireConnectionLocked(localSQLiteConnection, paramInt);
            }
            else
            {
                int j = this.mAcquiredConnections.size();
                if (this.mAvailablePrimaryConnection != null)
                    j++;
                if (j >= this.mMaxConnectionPoolSize)
                {
                    localSQLiteConnection = null;
                }
                else
                {
                    localSQLiteConnection = openConnectionLocked(this.mConfiguration, false);
                    finishAcquireConnectionLocked(localSQLiteConnection, paramInt);
                }
            }
        }
    }

    private SQLiteConnection tryAcquirePrimaryConnectionLocked(int paramInt)
    {
        Object localObject = null;
        SQLiteConnection localSQLiteConnection1 = this.mAvailablePrimaryConnection;
        if (localSQLiteConnection1 != null)
        {
            this.mAvailablePrimaryConnection = null;
            finishAcquireConnectionLocked(localSQLiteConnection1, paramInt);
            localObject = localSQLiteConnection1;
        }
        while (true)
        {
            return localObject;
            Iterator localIterator = this.mAcquiredConnections.keySet().iterator();
            while (true)
                if (localIterator.hasNext())
                    if (((SQLiteConnection)localIterator.next()).isPrimaryConnection())
                        break;
            SQLiteConnection localSQLiteConnection2 = openConnectionLocked(this.mConfiguration, true);
            finishAcquireConnectionLocked(localSQLiteConnection2, paramInt);
            localObject = localSQLiteConnection2;
        }
    }

    // ERROR //
    private SQLiteConnection waitForConnection(String paramString, int paramInt, CancellationSignal paramCancellationSignal)
    {
        // Byte code:
        //     0: iload_2
        //     1: iconst_2
        //     2: iand
        //     3: ifeq +412 -> 415
        //     6: iconst_1
        //     7: istore 4
        //     9: aload_0
        //     10: getfield 78	android/database/sqlite/SQLiteConnectionPool:mLock	Ljava/lang/Object;
        //     13: astore 5
        //     15: aload 5
        //     17: monitorenter
        //     18: aload_0
        //     19: invokespecial 214	android/database/sqlite/SQLiteConnectionPool:throwIfClosedLocked	()V
        //     22: aload_3
        //     23: ifnull +7 -> 30
        //     26: aload_3
        //     27: invokevirtual 466	android/os/CancellationSignal:throwIfCanceled	()V
        //     30: aconst_null
        //     31: astore 7
        //     33: iload 4
        //     35: ifne +11 -> 46
        //     38: aload_0
        //     39: aload_1
        //     40: iload_2
        //     41: invokespecial 468	android/database/sqlite/SQLiteConnectionPool:tryAcquireNonPrimaryConnectionLocked	(Ljava/lang/String;I)Landroid/database/sqlite/SQLiteConnection;
        //     44: astore 7
        //     46: aload 7
        //     48: ifnonnull +10 -> 58
        //     51: aload_0
        //     52: iload_2
        //     53: invokespecial 470	android/database/sqlite/SQLiteConnectionPool:tryAcquirePrimaryConnectionLocked	(I)Landroid/database/sqlite/SQLiteConnection;
        //     56: astore 7
        //     58: aload 7
        //     60: ifnull +9 -> 69
        //     63: aload 5
        //     65: monitorexit
        //     66: goto +346 -> 412
        //     69: iload_2
        //     70: invokestatic 261	android/database/sqlite/SQLiteConnectionPool:getPriority	(I)I
        //     73: istore 8
        //     75: invokestatic 475	android/os/SystemClock:uptimeMillis	()J
        //     78: lstore 9
        //     80: aload_0
        //     81: invokestatic 275	java/lang/Thread:currentThread	()Ljava/lang/Thread;
        //     84: lload 9
        //     86: iload 8
        //     88: iload 4
        //     90: aload_1
        //     91: iload_2
        //     92: invokespecial 477	android/database/sqlite/SQLiteConnectionPool:obtainConnectionWaiterLocked	(Ljava/lang/Thread;JIZLjava/lang/String;I)Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     95: astore 11
        //     97: aconst_null
        //     98: astore 12
        //     100: aload_0
        //     101: getfield 119	android/database/sqlite/SQLiteConnectionPool:mConnectionWaiterQueue	Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     104: astore 13
        //     106: aload 13
        //     108: ifnull +20 -> 128
        //     111: iload 8
        //     113: aload 13
        //     115: getfield 264	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mPriority	I
        //     118: if_icmple +176 -> 294
        //     121: aload 11
        //     123: aload 13
        //     125: putfield 125	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mNext	Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     128: aload 12
        //     130: ifnull +178 -> 308
        //     133: aload 12
        //     135: aload 11
        //     137: putfield 125	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mNext	Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     140: aload 11
        //     142: getfield 432	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mNonce	I
        //     145: istore 14
        //     147: aload 5
        //     149: monitorexit
        //     150: aload_3
        //     151: ifnull +19 -> 170
        //     154: aload_3
        //     155: new 8	android/database/sqlite/SQLiteConnectionPool$1
        //     158: dup
        //     159: aload_0
        //     160: aload 11
        //     162: iload 14
        //     164: invokespecial 480	android/database/sqlite/SQLiteConnectionPool$1:<init>	(Landroid/database/sqlite/SQLiteConnectionPool;Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;I)V
        //     167: invokevirtual 484	android/os/CancellationSignal:setOnCancelListener	(Landroid/os/CancellationSignal$OnCancelListener;)V
        //     170: ldc2_w 27
        //     173: lstore 15
        //     175: lload 15
        //     177: aload 11
        //     179: getfield 380	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mStartTime	J
        //     182: ladd
        //     183: lstore 18
        //     185: aload_0
        //     186: getfield 83	android/database/sqlite/SQLiteConnectionPool:mConnectionLeaked	Ljava/util/concurrent/atomic/AtomicBoolean;
        //     189: iconst_1
        //     190: iconst_0
        //     191: invokevirtual 488	java/util/concurrent/atomic/AtomicBoolean:compareAndSet	(ZZ)Z
        //     194: ifeq +19 -> 213
        //     197: aload_0
        //     198: getfield 78	android/database/sqlite/SQLiteConnectionPool:mLock	Ljava/lang/Object;
        //     201: astore 28
        //     203: aload 28
        //     205: monitorenter
        //     206: aload_0
        //     207: invokespecial 141	android/database/sqlite/SQLiteConnectionPool:wakeConnectionWaitersLocked	()V
        //     210: aload 28
        //     212: monitorexit
        //     213: ldc2_w 489
        //     216: lload 15
        //     218: lmul
        //     219: lstore 20
        //     221: aload_0
        //     222: lload 20
        //     224: invokestatic 494	java/util/concurrent/locks/LockSupport:parkNanos	(Ljava/lang/Object;J)V
        //     227: invokestatic 497	java/lang/Thread:interrupted	()Z
        //     230: pop
        //     231: aload_0
        //     232: getfield 78	android/database/sqlite/SQLiteConnectionPool:mLock	Ljava/lang/Object;
        //     235: astore 23
        //     237: aload 23
        //     239: monitorenter
        //     240: aload_0
        //     241: invokespecial 214	android/database/sqlite/SQLiteConnectionPool:throwIfClosedLocked	()V
        //     244: aload 11
        //     246: getfield 113	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mAssignedConnection	Landroid/database/sqlite/SQLiteConnection;
        //     249: astore 7
        //     251: aload 11
        //     253: getfield 117	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mException	Ljava/lang/RuntimeException;
        //     256: astore 25
        //     258: aload 7
        //     260: ifnonnull +8 -> 268
        //     263: aload 25
        //     265: ifnull +93 -> 358
        //     268: aload_0
        //     269: aload 11
        //     271: invokespecial 499	android/database/sqlite/SQLiteConnectionPool:recycleConnectionWaiterLocked	(Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;)V
        //     274: aload 7
        //     276: ifnull +71 -> 347
        //     279: aload 23
        //     281: monitorexit
        //     282: aload_3
        //     283: ifnull +129 -> 412
        //     286: aload_3
        //     287: aconst_null
        //     288: invokevirtual 484	android/os/CancellationSignal:setOnCancelListener	(Landroid/os/CancellationSignal$OnCancelListener;)V
        //     291: goto +121 -> 412
        //     294: aload 13
        //     296: astore 12
        //     298: aload 13
        //     300: getfield 125	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mNext	Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     303: astore 13
        //     305: goto -199 -> 106
        //     308: aload_0
        //     309: aload 11
        //     311: putfield 119	android/database/sqlite/SQLiteConnectionPool:mConnectionWaiterQueue	Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     314: goto -174 -> 140
        //     317: astore 6
        //     319: aload 5
        //     321: monitorexit
        //     322: aload 6
        //     324: athrow
        //     325: astore 29
        //     327: aload 28
        //     329: monitorexit
        //     330: aload 29
        //     332: athrow
        //     333: astore 17
        //     335: aload_3
        //     336: ifnull +8 -> 344
        //     339: aload_3
        //     340: aconst_null
        //     341: invokevirtual 484	android/os/CancellationSignal:setOnCancelListener	(Landroid/os/CancellationSignal$OnCancelListener;)V
        //     344: aload 17
        //     346: athrow
        //     347: aload 25
        //     349: athrow
        //     350: astore 24
        //     352: aload 23
        //     354: monitorexit
        //     355: aload 24
        //     357: athrow
        //     358: invokestatic 475	android/os/SystemClock:uptimeMillis	()J
        //     361: lstore 26
        //     363: lload 26
        //     365: lload 18
        //     367: lcmp
        //     368: ifge +16 -> 384
        //     371: lload 26
        //     373: lload 18
        //     375: lsub
        //     376: lstore 15
        //     378: aload 23
        //     380: monitorexit
        //     381: goto -196 -> 185
        //     384: aload_0
        //     385: lload 26
        //     387: aload 11
        //     389: getfield 380	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mStartTime	J
        //     392: lsub
        //     393: iload_2
        //     394: invokespecial 501	android/database/sqlite/SQLiteConnectionPool:logConnectionPoolBusyLocked	(JI)V
        //     397: ldc2_w 27
        //     400: lstore 15
        //     402: lload 26
        //     404: lload 15
        //     406: ladd
        //     407: lstore 18
        //     409: goto -31 -> 378
        //     412: aload 7
        //     414: areturn
        //     415: iconst_0
        //     416: istore 4
        //     418: goto -409 -> 9
        //
        // Exception table:
        //     from	to	target	type
        //     18	150	317	finally
        //     298	322	317	finally
        //     206	213	325	finally
        //     327	330	325	finally
        //     175	206	333	finally
        //     221	240	333	finally
        //     330	333	333	finally
        //     355	358	333	finally
        //     240	282	350	finally
        //     347	355	350	finally
        //     358	397	350	finally
    }

    private void wakeConnectionWaitersLocked()
    {
        Object localObject1 = null;
        Object localObject2 = this.mConnectionWaiterQueue;
        int i = 0;
        int j = 0;
        int k;
        label29: ConnectionWaiter localConnectionWaiter;
        if (localObject2 != null)
        {
            k = 0;
            if (!this.mIsOpen)
            {
                k = 1;
                localConnectionWaiter = ((ConnectionWaiter)localObject2).mNext;
                if (k == 0)
                    break label181;
                if (localObject1 == null)
                    break label172;
                localObject1.mNext = localConnectionWaiter;
                label50: ((ConnectionWaiter)localObject2).mNext = null;
                LockSupport.unpark(((ConnectionWaiter)localObject2).mThread);
            }
        }
        while (true)
        {
            while (true)
            {
                localObject2 = localConnectionWaiter;
                break;
                SQLiteConnection localSQLiteConnection = null;
                try
                {
                    if ((!((ConnectionWaiter)localObject2).mWantPrimaryConnection) && (j == 0))
                    {
                        localSQLiteConnection = tryAcquireNonPrimaryConnectionLocked(((ConnectionWaiter)localObject2).mSql, ((ConnectionWaiter)localObject2).mConnectionFlags);
                        if (localSQLiteConnection == null)
                            j = 1;
                    }
                    if ((localSQLiteConnection == null) && (i == 0))
                    {
                        localSQLiteConnection = tryAcquirePrimaryConnectionLocked(((ConnectionWaiter)localObject2).mConnectionFlags);
                        if (localSQLiteConnection == null)
                            i = 1;
                    }
                    if (localSQLiteConnection != null)
                    {
                        ((ConnectionWaiter)localObject2).mAssignedConnection = localSQLiteConnection;
                        k = 1;
                        break label29;
                    }
                    if ((j == 0) || (i == 0))
                        break label29;
                    return;
                }
                catch (RuntimeException localRuntimeException)
                {
                    ((ConnectionWaiter)localObject2).mException = localRuntimeException;
                    k = 1;
                }
            }
            break label29;
            label172: this.mConnectionWaiterQueue = localConnectionWaiter;
            break label50;
            label181: localObject1 = localObject2;
        }
    }

    public SQLiteConnection acquireConnection(String paramString, int paramInt, CancellationSignal paramCancellationSignal)
    {
        return waitForConnection(paramString, paramInt, paramCancellationSignal);
    }

    public void close()
    {
        dispose(false);
    }

    // ERROR //
    public void collectDbStats(ArrayList<SQLiteDebug.DbStats> paramArrayList)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 78	android/database/sqlite/SQLiteConnectionPool:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 147	android/database/sqlite/SQLiteConnectionPool:mAvailablePrimaryConnection	Landroid/database/sqlite/SQLiteConnection;
        //     11: ifnull +11 -> 22
        //     14: aload_0
        //     15: getfield 147	android/database/sqlite/SQLiteConnectionPool:mAvailablePrimaryConnection	Landroid/database/sqlite/SQLiteConnection;
        //     18: aload_1
        //     19: invokevirtual 510	android/database/sqlite/SQLiteConnection:collectDbStats	(Ljava/util/ArrayList;)V
        //     22: aload_0
        //     23: getfield 88	android/database/sqlite/SQLiteConnectionPool:mAvailableNonPrimaryConnections	Ljava/util/ArrayList;
        //     26: invokevirtual 350	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     29: astore 4
        //     31: aload 4
        //     33: invokeinterface 327 1 0
        //     38: ifeq +25 -> 63
        //     41: aload 4
        //     43: invokeinterface 331 1 0
        //     48: checkcast 160	android/database/sqlite/SQLiteConnection
        //     51: aload_1
        //     52: invokevirtual 510	android/database/sqlite/SQLiteConnection:collectDbStats	(Ljava/util/ArrayList;)V
        //     55: goto -24 -> 31
        //     58: astore_3
        //     59: aload_2
        //     60: monitorexit
        //     61: aload_3
        //     62: athrow
        //     63: aload_0
        //     64: getfield 93	android/database/sqlite/SQLiteConnectionPool:mAcquiredConnections	Ljava/util/WeakHashMap;
        //     67: invokevirtual 316	java/util/WeakHashMap:keySet	()Ljava/util/Set;
        //     70: invokeinterface 322 1 0
        //     75: astore 5
        //     77: aload 5
        //     79: invokeinterface 327 1 0
        //     84: ifeq +20 -> 104
        //     87: aload 5
        //     89: invokeinterface 331 1 0
        //     94: checkcast 160	android/database/sqlite/SQLiteConnection
        //     97: aload_1
        //     98: invokevirtual 513	android/database/sqlite/SQLiteConnection:collectDbStatsUnsafe	(Ljava/util/ArrayList;)V
        //     101: goto -24 -> 77
        //     104: aload_2
        //     105: monitorexit
        //     106: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	61	58	finally
        //     63	106	58	finally
    }

    // ERROR //
    public void dump(android.util.Printer paramPrinter, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc_w 517
        //     4: invokestatic 523	android/util/PrefixPrinter:create	(Landroid/util/Printer;Ljava/lang/String;)Landroid/util/Printer;
        //     7: astore_3
        //     8: aload_0
        //     9: getfield 78	android/database/sqlite/SQLiteConnectionPool:mLock	Ljava/lang/Object;
        //     12: astore 4
        //     14: aload 4
        //     16: monitorenter
        //     17: aload_1
        //     18: new 170	java/lang/StringBuilder
        //     21: dup
        //     22: invokespecial 171	java/lang/StringBuilder:<init>	()V
        //     25: ldc_w 525
        //     28: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     31: aload_0
        //     32: getfield 99	android/database/sqlite/SQLiteConnectionPool:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     35: getfield 528	android/database/sqlite/SQLiteDatabaseConfiguration:path	Ljava/lang/String;
        //     38: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     41: ldc_w 530
        //     44: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     47: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     50: invokeinterface 535 2 0
        //     55: aload_1
        //     56: new 170	java/lang/StringBuilder
        //     59: dup
        //     60: invokespecial 171	java/lang/StringBuilder:<init>	()V
        //     63: ldc_w 537
        //     66: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     69: aload_0
        //     70: getfield 216	android/database/sqlite/SQLiteConnectionPool:mIsOpen	Z
        //     73: invokevirtual 540	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     76: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     79: invokeinterface 535 2 0
        //     84: aload_1
        //     85: new 170	java/lang/StringBuilder
        //     88: dup
        //     89: invokespecial 171	java/lang/StringBuilder:<init>	()V
        //     92: ldc_w 542
        //     95: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     98: aload_0
        //     99: getfield 193	android/database/sqlite/SQLiteConnectionPool:mMaxConnectionPoolSize	I
        //     102: invokevirtual 229	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     105: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     108: invokeinterface 535 2 0
        //     113: aload_1
        //     114: ldc_w 544
        //     117: invokeinterface 535 2 0
        //     122: aload_0
        //     123: getfield 147	android/database/sqlite/SQLiteConnectionPool:mAvailablePrimaryConnection	Landroid/database/sqlite/SQLiteConnection;
        //     126: ifnull +73 -> 199
        //     129: aload_0
        //     130: getfield 147	android/database/sqlite/SQLiteConnectionPool:mAvailablePrimaryConnection	Landroid/database/sqlite/SQLiteConnection;
        //     133: aload_3
        //     134: iload_2
        //     135: invokevirtual 546	android/database/sqlite/SQLiteConnection:dump	(Landroid/util/Printer;Z)V
        //     138: aload_1
        //     139: ldc_w 548
        //     142: invokeinterface 535 2 0
        //     147: aload_0
        //     148: getfield 88	android/database/sqlite/SQLiteConnectionPool:mAvailableNonPrimaryConnections	Ljava/util/ArrayList;
        //     151: invokevirtual 347	java/util/ArrayList:isEmpty	()Z
        //     154: ifne +65 -> 219
        //     157: aload_0
        //     158: getfield 88	android/database/sqlite/SQLiteConnectionPool:mAvailableNonPrimaryConnections	Ljava/util/ArrayList;
        //     161: invokevirtual 155	java/util/ArrayList:size	()I
        //     164: istore 12
        //     166: iconst_0
        //     167: istore 13
        //     169: iload 13
        //     171: iload 12
        //     173: if_icmpge +55 -> 228
        //     176: aload_0
        //     177: getfield 88	android/database/sqlite/SQLiteConnectionPool:mAvailableNonPrimaryConnections	Ljava/util/ArrayList;
        //     180: iload 13
        //     182: invokevirtual 158	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     185: checkcast 160	android/database/sqlite/SQLiteConnection
        //     188: aload_3
        //     189: iload_2
        //     190: invokevirtual 546	android/database/sqlite/SQLiteConnection:dump	(Landroid/util/Printer;Z)V
        //     193: iinc 13 1
        //     196: goto -27 -> 169
        //     199: aload_3
        //     200: ldc_w 550
        //     203: invokeinterface 535 2 0
        //     208: goto -70 -> 138
        //     211: astore 5
        //     213: aload 4
        //     215: monitorexit
        //     216: aload 5
        //     218: athrow
        //     219: aload_3
        //     220: ldc_w 550
        //     223: invokeinterface 535 2 0
        //     228: aload_1
        //     229: ldc_w 552
        //     232: invokeinterface 535 2 0
        //     237: aload_0
        //     238: getfield 93	android/database/sqlite/SQLiteConnectionPool:mAcquiredConnections	Ljava/util/WeakHashMap;
        //     241: invokevirtual 312	java/util/WeakHashMap:isEmpty	()Z
        //     244: ifne +89 -> 333
        //     247: aload_0
        //     248: getfield 93	android/database/sqlite/SQLiteConnectionPool:mAcquiredConnections	Ljava/util/WeakHashMap;
        //     251: invokevirtual 365	java/util/WeakHashMap:entrySet	()Ljava/util/Set;
        //     254: invokeinterface 322 1 0
        //     259: astore 10
        //     261: aload 10
        //     263: invokeinterface 327 1 0
        //     268: ifeq +74 -> 342
        //     271: aload 10
        //     273: invokeinterface 331 1 0
        //     278: checkcast 367	java/util/Map$Entry
        //     281: astore 11
        //     283: aload 11
        //     285: invokeinterface 373 1 0
        //     290: checkcast 160	android/database/sqlite/SQLiteConnection
        //     293: aload_3
        //     294: iload_2
        //     295: invokevirtual 555	android/database/sqlite/SQLiteConnection:dumpUnsafe	(Landroid/util/Printer;Z)V
        //     298: aload_3
        //     299: new 170	java/lang/StringBuilder
        //     302: dup
        //     303: invokespecial 171	java/lang/StringBuilder:<init>	()V
        //     306: ldc_w 557
        //     309: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     312: aload 11
        //     314: invokeinterface 370 1 0
        //     319: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     322: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     325: invokeinterface 535 2 0
        //     330: goto -69 -> 261
        //     333: aload_3
        //     334: ldc_w 550
        //     337: invokeinterface 535 2 0
        //     342: aload_1
        //     343: ldc_w 559
        //     346: invokeinterface 535 2 0
        //     351: aload_0
        //     352: getfield 119	android/database/sqlite/SQLiteConnectionPool:mConnectionWaiterQueue	Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     355: ifnull +126 -> 481
        //     358: iconst_0
        //     359: istore 6
        //     361: invokestatic 475	android/os/SystemClock:uptimeMillis	()J
        //     364: lstore 7
        //     366: aload_0
        //     367: getfield 119	android/database/sqlite/SQLiteConnectionPool:mConnectionWaiterQueue	Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     370: astore 9
        //     372: aload 9
        //     374: ifnull +116 -> 490
        //     377: aload_3
        //     378: new 170	java/lang/StringBuilder
        //     381: dup
        //     382: invokespecial 171	java/lang/StringBuilder:<init>	()V
        //     385: iload 6
        //     387: invokevirtual 229	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     390: ldc_w 561
        //     393: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     396: ldc_w 304
        //     399: lload 7
        //     401: aload 9
        //     403: getfield 380	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mStartTime	J
        //     406: lsub
        //     407: l2f
        //     408: fmul
        //     409: invokevirtual 307	java/lang/StringBuilder:append	(F)Ljava/lang/StringBuilder;
        //     412: ldc_w 563
        //     415: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     418: aload 9
        //     420: getfield 132	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mThread	Ljava/lang/Thread;
        //     423: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     426: ldc_w 565
        //     429: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     432: aload 9
        //     434: getfield 264	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mPriority	I
        //     437: invokevirtual 229	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     440: ldc_w 567
        //     443: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     446: aload 9
        //     448: getfield 383	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mSql	Ljava/lang/String;
        //     451: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     454: ldc_w 569
        //     457: invokevirtual 177	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     460: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     463: invokeinterface 535 2 0
        //     468: aload 9
        //     470: getfield 125	android/database/sqlite/SQLiteConnectionPool$ConnectionWaiter:mNext	Landroid/database/sqlite/SQLiteConnectionPool$ConnectionWaiter;
        //     473: astore 9
        //     475: iinc 6 1
        //     478: goto -106 -> 372
        //     481: aload_3
        //     482: ldc_w 550
        //     485: invokeinterface 535 2 0
        //     490: aload 4
        //     492: monitorexit
        //     493: return
        //
        // Exception table:
        //     from	to	target	type
        //     17	216	211	finally
        //     219	493	211	finally
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            dispose(true);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    void onConnectionLeaked()
    {
        Log.w("SQLiteConnectionPool", "A SQLiteConnection object for database '" + this.mConfiguration.label + "' was leaked!    Please fix your application " + "to end transactions in progress properly and to close the database " + "when it is no longer needed.");
        this.mConnectionLeaked.set(true);
    }

    // ERROR //
    public void reconfigure(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration)
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_2
        //     2: aload_1
        //     3: ifnonnull +14 -> 17
        //     6: new 393	java/lang/IllegalArgumentException
        //     9: dup
        //     10: ldc_w 395
        //     13: invokespecial 398	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: aload_0
        //     18: getfield 78	android/database/sqlite/SQLiteConnectionPool:mLock	Ljava/lang/Object;
        //     21: astore_3
        //     22: aload_3
        //     23: monitorenter
        //     24: aload_0
        //     25: invokespecial 214	android/database/sqlite/SQLiteConnectionPool:throwIfClosedLocked	()V
        //     28: ldc_w 433
        //     31: aload_1
        //     32: getfield 436	android/database/sqlite/SQLiteDatabaseConfiguration:openFlags	I
        //     35: aload_0
        //     36: getfield 99	android/database/sqlite/SQLiteConnectionPool:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     39: getfield 436	android/database/sqlite/SQLiteDatabaseConfiguration:openFlags	I
        //     42: ixor
        //     43: iand
        //     44: ifeq +39 -> 83
        //     47: iload_2
        //     48: istore 5
        //     50: iload 5
        //     52: ifeq +65 -> 117
        //     55: aload_0
        //     56: getfield 93	android/database/sqlite/SQLiteConnectionPool:mAcquiredConnections	Ljava/util/WeakHashMap;
        //     59: invokevirtual 312	java/util/WeakHashMap:isEmpty	()Z
        //     62: ifne +27 -> 89
        //     65: new 443	java/lang/IllegalStateException
        //     68: dup
        //     69: ldc_w 588
        //     72: invokespecial 446	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     75: athrow
        //     76: astore 4
        //     78: aload_3
        //     79: monitorexit
        //     80: aload 4
        //     82: athrow
        //     83: iconst_0
        //     84: istore 5
        //     86: goto -36 -> 50
        //     89: aload_0
        //     90: invokespecial 145	android/database/sqlite/SQLiteConnectionPool:closeAvailableNonPrimaryConnectionsAndLogExceptionsLocked	()V
        //     93: getstatic 64	android/database/sqlite/SQLiteConnectionPool:$assertionsDisabled	Z
        //     96: ifne +21 -> 117
        //     99: aload_0
        //     100: getfield 88	android/database/sqlite/SQLiteConnectionPool:mAvailableNonPrimaryConnections	Ljava/util/ArrayList;
        //     103: invokevirtual 347	java/util/ArrayList:isEmpty	()Z
        //     106: ifne +11 -> 117
        //     109: new 121	java/lang/AssertionError
        //     112: dup
        //     113: invokespecial 122	java/lang/AssertionError:<init>	()V
        //     116: athrow
        //     117: aload_1
        //     118: getfield 591	android/database/sqlite/SQLiteDatabaseConfiguration:foreignKeyConstraintsEnabled	Z
        //     121: aload_0
        //     122: getfield 99	android/database/sqlite/SQLiteConnectionPool:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     125: getfield 591	android/database/sqlite/SQLiteDatabaseConfiguration:foreignKeyConstraintsEnabled	Z
        //     128: if_icmpeq +115 -> 243
        //     131: iload_2
        //     132: ifeq +24 -> 156
        //     135: aload_0
        //     136: getfield 93	android/database/sqlite/SQLiteConnectionPool:mAcquiredConnections	Ljava/util/WeakHashMap;
        //     139: invokevirtual 312	java/util/WeakHashMap:isEmpty	()Z
        //     142: ifne +14 -> 156
        //     145: new 443	java/lang/IllegalStateException
        //     148: dup
        //     149: ldc_w 593
        //     152: invokespecial 446	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     155: athrow
        //     156: aload_0
        //     157: getfield 99	android/database/sqlite/SQLiteConnectionPool:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     160: getfield 436	android/database/sqlite/SQLiteDatabaseConfiguration:openFlags	I
        //     163: aload_1
        //     164: getfield 436	android/database/sqlite/SQLiteDatabaseConfiguration:openFlags	I
        //     167: if_icmpeq +53 -> 220
        //     170: iload 5
        //     172: ifeq +7 -> 179
        //     175: aload_0
        //     176: invokespecial 218	android/database/sqlite/SQLiteConnectionPool:closeAvailableConnectionsAndLogExceptionsLocked	()V
        //     179: aload_0
        //     180: aload_1
        //     181: iconst_1
        //     182: invokespecial 405	android/database/sqlite/SQLiteConnectionPool:openConnectionLocked	(Landroid/database/sqlite/SQLiteDatabaseConfiguration;Z)Landroid/database/sqlite/SQLiteConnection;
        //     185: astore 6
        //     187: aload_0
        //     188: invokespecial 218	android/database/sqlite/SQLiteConnectionPool:closeAvailableConnectionsAndLogExceptionsLocked	()V
        //     191: aload_0
        //     192: invokespecial 595	android/database/sqlite/SQLiteConnectionPool:discardAcquiredConnectionsLocked	()V
        //     195: aload_0
        //     196: aload 6
        //     198: putfield 147	android/database/sqlite/SQLiteConnectionPool:mAvailablePrimaryConnection	Landroid/database/sqlite/SQLiteConnection;
        //     201: aload_0
        //     202: getfield 99	android/database/sqlite/SQLiteConnectionPool:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     205: aload_1
        //     206: invokevirtual 598	android/database/sqlite/SQLiteDatabaseConfiguration:updateParametersFrom	(Landroid/database/sqlite/SQLiteDatabaseConfiguration;)V
        //     209: aload_0
        //     210: invokespecial 102	android/database/sqlite/SQLiteConnectionPool:setMaxConnectionPoolSizeLocked	()V
        //     213: aload_0
        //     214: invokespecial 141	android/database/sqlite/SQLiteConnectionPool:wakeConnectionWaitersLocked	()V
        //     217: aload_3
        //     218: monitorexit
        //     219: return
        //     220: aload_0
        //     221: getfield 99	android/database/sqlite/SQLiteConnectionPool:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     224: aload_1
        //     225: invokevirtual 598	android/database/sqlite/SQLiteDatabaseConfiguration:updateParametersFrom	(Landroid/database/sqlite/SQLiteDatabaseConfiguration;)V
        //     228: aload_0
        //     229: invokespecial 102	android/database/sqlite/SQLiteConnectionPool:setMaxConnectionPoolSizeLocked	()V
        //     232: aload_0
        //     233: invokespecial 600	android/database/sqlite/SQLiteConnectionPool:closeExcessConnectionsAndLogExceptionsLocked	()V
        //     236: aload_0
        //     237: invokespecial 602	android/database/sqlite/SQLiteConnectionPool:reconfigureAllConnectionsLocked	()V
        //     240: goto -27 -> 213
        //     243: iconst_0
        //     244: istore_2
        //     245: goto -114 -> 131
        //
        // Exception table:
        //     from	to	target	type
        //     24	80	76	finally
        //     89	240	76	finally
    }

    // ERROR //
    public void releaseConnection(SQLiteConnection paramSQLiteConnection)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 78	android/database/sqlite/SQLiteConnectionPool:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 93	android/database/sqlite/SQLiteConnectionPool:mAcquiredConnections	Ljava/util/WeakHashMap;
        //     11: aload_1
        //     12: invokevirtual 606	java/util/WeakHashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     15: checkcast 13	android/database/sqlite/SQLiteConnectionPool$AcquiredConnectionStatus
        //     18: astore 4
        //     20: aload 4
        //     22: ifnonnull +19 -> 41
        //     25: new 443	java/lang/IllegalStateException
        //     28: dup
        //     29: ldc_w 608
        //     32: invokespecial 446	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     35: athrow
        //     36: astore_3
        //     37: aload_2
        //     38: monitorexit
        //     39: aload_3
        //     40: athrow
        //     41: aload_0
        //     42: getfield 216	android/database/sqlite/SQLiteConnectionPool:mIsOpen	Z
        //     45: ifne +11 -> 56
        //     48: aload_0
        //     49: aload_1
        //     50: invokespecial 151	android/database/sqlite/SQLiteConnectionPool:closeConnectionAndLogExceptionsLocked	(Landroid/database/sqlite/SQLiteConnection;)V
        //     53: aload_2
        //     54: monitorexit
        //     55: return
        //     56: aload_1
        //     57: invokevirtual 459	android/database/sqlite/SQLiteConnection:isPrimaryConnection	()Z
        //     60: ifeq +46 -> 106
        //     63: aload_0
        //     64: aload_1
        //     65: aload 4
        //     67: invokespecial 610	android/database/sqlite/SQLiteConnectionPool:recycleConnectionLocked	(Landroid/database/sqlite/SQLiteConnection;Landroid/database/sqlite/SQLiteConnectionPool$AcquiredConnectionStatus;)Z
        //     70: ifeq +29 -> 99
        //     73: getstatic 64	android/database/sqlite/SQLiteConnectionPool:$assertionsDisabled	Z
        //     76: ifne +18 -> 94
        //     79: aload_0
        //     80: getfield 147	android/database/sqlite/SQLiteConnectionPool:mAvailablePrimaryConnection	Landroid/database/sqlite/SQLiteConnection;
        //     83: ifnull +11 -> 94
        //     86: new 121	java/lang/AssertionError
        //     89: dup
        //     90: invokespecial 122	java/lang/AssertionError:<init>	()V
        //     93: athrow
        //     94: aload_0
        //     95: aload_1
        //     96: putfield 147	android/database/sqlite/SQLiteConnectionPool:mAvailablePrimaryConnection	Landroid/database/sqlite/SQLiteConnection;
        //     99: aload_0
        //     100: invokespecial 141	android/database/sqlite/SQLiteConnectionPool:wakeConnectionWaitersLocked	()V
        //     103: goto -50 -> 53
        //     106: aload_0
        //     107: getfield 88	android/database/sqlite/SQLiteConnectionPool:mAvailableNonPrimaryConnections	Ljava/util/ArrayList;
        //     110: invokevirtual 155	java/util/ArrayList:size	()I
        //     113: bipush 255
        //     115: aload_0
        //     116: getfield 193	android/database/sqlite/SQLiteConnectionPool:mMaxConnectionPoolSize	I
        //     119: iadd
        //     120: if_icmplt +11 -> 131
        //     123: aload_0
        //     124: aload_1
        //     125: invokespecial 151	android/database/sqlite/SQLiteConnectionPool:closeConnectionAndLogExceptionsLocked	(Landroid/database/sqlite/SQLiteConnection;)V
        //     128: goto -75 -> 53
        //     131: aload_0
        //     132: aload_1
        //     133: aload 4
        //     135: invokespecial 610	android/database/sqlite/SQLiteConnectionPool:recycleConnectionLocked	(Landroid/database/sqlite/SQLiteConnection;Landroid/database/sqlite/SQLiteConnectionPool$AcquiredConnectionStatus;)Z
        //     138: ifeq +12 -> 150
        //     141: aload_0
        //     142: getfield 88	android/database/sqlite/SQLiteConnectionPool:mAvailableNonPrimaryConnections	Ljava/util/ArrayList;
        //     145: aload_1
        //     146: invokevirtual 338	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     149: pop
        //     150: aload_0
        //     151: invokespecial 141	android/database/sqlite/SQLiteConnectionPool:wakeConnectionWaitersLocked	()V
        //     154: goto -101 -> 53
        //
        // Exception table:
        //     from	to	target	type
        //     7	39	36	finally
        //     41	154	36	finally
    }

    // ERROR //
    public boolean shouldYieldConnection(SQLiteConnection paramSQLiteConnection, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 78	android/database/sqlite/SQLiteConnectionPool:mLock	Ljava/lang/Object;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 93	android/database/sqlite/SQLiteConnectionPool:mAcquiredConnections	Ljava/util/WeakHashMap;
        //     11: aload_1
        //     12: invokevirtual 615	java/util/WeakHashMap:containsKey	(Ljava/lang/Object;)Z
        //     15: ifne +21 -> 36
        //     18: new 443	java/lang/IllegalStateException
        //     21: dup
        //     22: ldc_w 608
        //     25: invokespecial 446	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     28: athrow
        //     29: astore 4
        //     31: aload_3
        //     32: monitorexit
        //     33: aload 4
        //     35: athrow
        //     36: aload_0
        //     37: getfield 216	android/database/sqlite/SQLiteConnectionPool:mIsOpen	Z
        //     40: ifne +11 -> 51
        //     43: iconst_0
        //     44: istore 5
        //     46: aload_3
        //     47: monitorexit
        //     48: goto +16 -> 64
        //     51: aload_0
        //     52: aload_1
        //     53: invokevirtual 459	android/database/sqlite/SQLiteConnection:isPrimaryConnection	()Z
        //     56: iload_2
        //     57: invokespecial 617	android/database/sqlite/SQLiteConnectionPool:isSessionBlockingImportantConnectionWaitersLocked	(ZI)Z
        //     60: istore 5
        //     62: aload_3
        //     63: monitorexit
        //     64: iload 5
        //     66: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	33	29	finally
        //     36	64	29	finally
    }

    public String toString()
    {
        return "SQLiteConnectionPool: " + this.mConfiguration.path;
    }

    private static final class ConnectionWaiter
    {
        public SQLiteConnection mAssignedConnection;
        public int mConnectionFlags;
        public RuntimeException mException;
        public ConnectionWaiter mNext;
        public int mNonce;
        public int mPriority;
        public String mSql;
        public long mStartTime;
        public Thread mThread;
        public boolean mWantPrimaryConnection;
    }

    static enum AcquiredConnectionStatus
    {
        static
        {
            DISCARD = new AcquiredConnectionStatus("DISCARD", 2);
            AcquiredConnectionStatus[] arrayOfAcquiredConnectionStatus = new AcquiredConnectionStatus[3];
            arrayOfAcquiredConnectionStatus[0] = NORMAL;
            arrayOfAcquiredConnectionStatus[1] = RECONFIGURE;
            arrayOfAcquiredConnectionStatus[2] = DISCARD;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteConnectionPool
 * JD-Core Version:        0.6.2
 */