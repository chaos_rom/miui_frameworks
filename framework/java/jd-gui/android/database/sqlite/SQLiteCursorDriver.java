package android.database.sqlite;

import android.database.Cursor;

public abstract interface SQLiteCursorDriver
{
    public abstract void cursorClosed();

    public abstract void cursorDeactivated();

    public abstract void cursorRequeried(Cursor paramCursor);

    public abstract Cursor query(SQLiteDatabase.CursorFactory paramCursorFactory, String[] paramArrayOfString);

    public abstract void setBindArguments(String[] paramArrayOfString);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteCursorDriver
 * JD-Core Version:        0.6.2
 */