package android.database.sqlite;

public class SQLiteOutOfMemoryException extends SQLiteException
{
    public SQLiteOutOfMemoryException()
    {
    }

    public SQLiteOutOfMemoryException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteOutOfMemoryException
 * JD-Core Version:        0.6.2
 */