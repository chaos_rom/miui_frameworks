package android.database.sqlite;

public abstract interface SQLiteTransactionListener
{
    public abstract void onBegin();

    public abstract void onCommit();

    public abstract void onRollback();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteTransactionListener
 * JD-Core Version:        0.6.2
 */