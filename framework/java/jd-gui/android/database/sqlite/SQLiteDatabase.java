package android.database.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.DefaultDatabaseErrorHandler;
import android.database.SQLException;
import android.os.CancellationSignal;
import android.os.Looper;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Log;
import android.util.Printer;
import dalvik.system.CloseGuard;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public final class SQLiteDatabase extends SQLiteClosable
{
    public static final int CONFLICT_ABORT = 2;
    public static final int CONFLICT_FAIL = 3;
    public static final int CONFLICT_IGNORE = 4;
    public static final int CONFLICT_NONE = 0;
    public static final int CONFLICT_REPLACE = 5;
    public static final int CONFLICT_ROLLBACK = 1;
    private static final String[] CONFLICT_VALUES;
    public static final int CREATE_IF_NECESSARY = 268435456;
    public static final int ENABLE_WRITE_AHEAD_LOGGING = 536870912;
    private static final int EVENT_DB_CORRUPT = 75004;
    public static final int MAX_SQL_CACHE_SIZE = 100;
    public static final int NO_LOCALIZED_COLLATORS = 16;
    public static final int OPEN_READONLY = 1;
    public static final int OPEN_READWRITE = 0;
    private static final int OPEN_READ_MASK = 1;
    public static final int SQLITE_MAX_LIKE_PATTERN_LENGTH = 50000;
    private static final String TAG = "SQLiteDatabase";
    private static WeakHashMap<SQLiteDatabase, Object> sActiveDatabases;
    private final CloseGuard mCloseGuardLocked = CloseGuard.get();
    private final SQLiteDatabaseConfiguration mConfigurationLocked;
    private SQLiteConnectionPool mConnectionPoolLocked;
    private final CursorFactory mCursorFactory;
    private final DatabaseErrorHandler mErrorHandler;
    private boolean mHasAttachedDbsLocked;
    private final Object mLock = new Object();
    private final ThreadLocal<SQLiteSession> mThreadSession = new ThreadLocal()
    {
        protected SQLiteSession initialValue()
        {
            return SQLiteDatabase.this.createSession();
        }
    };

    static
    {
        if (!SQLiteDatabase.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            sActiveDatabases = new WeakHashMap();
            String[] arrayOfString = new String[6];
            arrayOfString[0] = "";
            arrayOfString[1] = " OR ROLLBACK ";
            arrayOfString[2] = " OR ABORT ";
            arrayOfString[3] = " OR FAIL ";
            arrayOfString[4] = " OR IGNORE ";
            arrayOfString[5] = " OR REPLACE ";
            CONFLICT_VALUES = arrayOfString;
            return;
        }
    }

    private SQLiteDatabase(String paramString, int paramInt, CursorFactory paramCursorFactory, DatabaseErrorHandler paramDatabaseErrorHandler)
    {
        this.mCursorFactory = paramCursorFactory;
        if (paramDatabaseErrorHandler != null);
        while (true)
        {
            this.mErrorHandler = paramDatabaseErrorHandler;
            this.mConfigurationLocked = new SQLiteDatabaseConfiguration(paramString, paramInt);
            return;
            paramDatabaseErrorHandler = new DefaultDatabaseErrorHandler();
        }
    }

    private void beginTransaction(SQLiteTransactionListener paramSQLiteTransactionListener, boolean paramBoolean)
    {
        acquireReference();
        try
        {
            SQLiteSession localSQLiteSession = getThreadSession();
            if (paramBoolean)
            {
                i = 2;
                localSQLiteSession.beginTransaction(i, paramSQLiteTransactionListener, getThreadDefaultConnectionFlags(false), null);
                return;
            }
            int i = 1;
        }
        finally
        {
            releaseReference();
        }
    }

    private void collectDbStats(ArrayList<SQLiteDebug.DbStats> paramArrayList)
    {
        synchronized (this.mLock)
        {
            if (this.mConnectionPoolLocked != null)
                this.mConnectionPoolLocked.collectDbStats(paramArrayList);
            return;
        }
    }

    public static SQLiteDatabase create(CursorFactory paramCursorFactory)
    {
        return openDatabase(":memory:", paramCursorFactory, 268435456);
    }

    public static boolean deleteDatabase(File paramFile)
    {
        if (paramFile == null)
            throw new IllegalArgumentException("file must not be null");
        boolean bool = false | paramFile.delete() | new File(paramFile.getPath() + "-journal").delete() | new File(paramFile.getPath() + "-shm").delete() | new File(paramFile.getPath() + "-wal").delete();
        File localFile = paramFile.getParentFile();
        if (localFile != null)
        {
            File[] arrayOfFile = localFile.listFiles(new FileFilter()
            {
                public boolean accept(File paramAnonymousFile)
                {
                    return paramAnonymousFile.getName().startsWith(SQLiteDatabase.this);
                }
            });
            int i = arrayOfFile.length;
            for (int j = 0; j < i; j++)
                bool |= arrayOfFile[j].delete();
        }
        return bool;
    }

    private void dispose(boolean paramBoolean)
    {
        SQLiteConnectionPool localSQLiteConnectionPool;
        synchronized (this.mLock)
        {
            if (this.mCloseGuardLocked != null)
            {
                if (paramBoolean)
                    this.mCloseGuardLocked.warnIfOpen();
                this.mCloseGuardLocked.close();
            }
            localSQLiteConnectionPool = this.mConnectionPoolLocked;
            this.mConnectionPoolLocked = null;
            if (paramBoolean);
        }
        synchronized (sActiveDatabases)
        {
            sActiveDatabases.remove(this);
            if (localSQLiteConnectionPool != null)
                localSQLiteConnectionPool.close();
            return;
            localObject2 = finally;
            throw localObject2;
        }
    }

    private void dump(Printer paramPrinter, boolean paramBoolean)
    {
        synchronized (this.mLock)
        {
            if (this.mConnectionPoolLocked != null)
            {
                paramPrinter.println("");
                this.mConnectionPoolLocked.dump(paramPrinter, paramBoolean);
            }
            return;
        }
    }

    static void dumpAll(Printer paramPrinter, boolean paramBoolean)
    {
        Iterator localIterator = getActiveDatabases().iterator();
        while (localIterator.hasNext())
            ((SQLiteDatabase)localIterator.next()).dump(paramPrinter, paramBoolean);
    }

    // ERROR //
    private int executeSql(String paramString, Object[] paramArrayOfObject)
        throws SQLException
    {
        // Byte code:
        //     0: aload_0
        //     1: invokevirtual 141	android/database/sqlite/SQLiteDatabase:acquireReference	()V
        //     4: aload_1
        //     5: invokestatic 275	android/database/DatabaseUtils:getSqlStatementType	(Ljava/lang/String;)I
        //     8: iconst_3
        //     9: if_icmpne +42 -> 51
        //     12: iconst_0
        //     13: istore 7
        //     15: aload_0
        //     16: getfield 114	android/database/sqlite/SQLiteDatabase:mLock	Ljava/lang/Object;
        //     19: astore 8
        //     21: aload 8
        //     23: monitorenter
        //     24: aload_0
        //     25: getfield 277	android/database/sqlite/SQLiteDatabase:mHasAttachedDbsLocked	Z
        //     28: ifne +11 -> 39
        //     31: aload_0
        //     32: iconst_1
        //     33: putfield 277	android/database/sqlite/SQLiteDatabase:mHasAttachedDbsLocked	Z
        //     36: iconst_1
        //     37: istore 7
        //     39: aload 8
        //     41: monitorexit
        //     42: iload 7
        //     44: ifeq +7 -> 51
        //     47: aload_0
        //     48: invokevirtual 280	android/database/sqlite/SQLiteDatabase:disableWriteAheadLogging	()V
        //     51: new 282	android/database/sqlite/SQLiteStatement
        //     54: dup
        //     55: aload_0
        //     56: aload_1
        //     57: aload_2
        //     58: invokespecial 285	android/database/sqlite/SQLiteStatement:<init>	(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;)V
        //     61: astore 4
        //     63: aload 4
        //     65: invokevirtual 289	android/database/sqlite/SQLiteStatement:executeUpdateDelete	()I
        //     68: istore 6
        //     70: aload 4
        //     72: invokevirtual 290	android/database/sqlite/SQLiteStatement:close	()V
        //     75: aload_0
        //     76: invokevirtual 157	android/database/sqlite/SQLiteDatabase:releaseReference	()V
        //     79: iload 6
        //     81: ireturn
        //     82: astore 9
        //     84: aload 8
        //     86: monitorexit
        //     87: aload 9
        //     89: athrow
        //     90: astore_3
        //     91: aload_0
        //     92: invokevirtual 157	android/database/sqlite/SQLiteDatabase:releaseReference	()V
        //     95: aload_3
        //     96: athrow
        //     97: astore 5
        //     99: aload 4
        //     101: invokevirtual 290	android/database/sqlite/SQLiteStatement:close	()V
        //     104: aload 5
        //     106: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     24	42	82	finally
        //     84	87	82	finally
        //     4	24	90	finally
        //     47	63	90	finally
        //     70	75	90	finally
        //     87	90	90	finally
        //     99	107	90	finally
        //     63	70	97	finally
    }

    public static String findEditTable(String paramString)
    {
        if (!TextUtils.isEmpty(paramString))
        {
            int i = paramString.indexOf(' ');
            int j = paramString.indexOf(',');
            if ((i > 0) && ((i < j) || (j < 0)));
            for (paramString = paramString.substring(0, i); ; paramString = paramString.substring(0, j))
                do
                    return paramString;
                while ((j <= 0) || ((j >= i) && (i >= 0)));
        }
        throw new IllegalStateException("Invalid tables");
    }

    private static ArrayList<SQLiteDatabase> getActiveDatabases()
    {
        ArrayList localArrayList = new ArrayList();
        synchronized (sActiveDatabases)
        {
            localArrayList.addAll(sActiveDatabases.keySet());
            return localArrayList;
        }
    }

    static ArrayList<SQLiteDebug.DbStats> getDbStats()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = getActiveDatabases().iterator();
        while (localIterator.hasNext())
            ((SQLiteDatabase)localIterator.next()).collectDbStats(localArrayList);
        return localArrayList;
    }

    private static boolean isMainThread()
    {
        Looper localLooper = Looper.myLooper();
        if ((localLooper != null) && (localLooper == Looper.getMainLooper()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isReadOnlyLocked()
    {
        int i = 1;
        if ((0x1 & this.mConfigurationLocked.openFlags) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void open()
    {
        try
        {
            openInner();
            return;
        }
        catch (SQLiteDatabaseCorruptException localSQLiteDatabaseCorruptException)
        {
            while (true)
            {
                onCorruption();
                openInner();
            }
        }
        catch (SQLiteException localSQLiteException)
        {
            Log.e("SQLiteDatabase", "Failed to open database '" + getLabel() + "'.", localSQLiteException);
            close();
            throw localSQLiteException;
        }
    }

    public static SQLiteDatabase openDatabase(String paramString, CursorFactory paramCursorFactory, int paramInt)
    {
        return openDatabase(paramString, paramCursorFactory, paramInt, null);
    }

    public static SQLiteDatabase openDatabase(String paramString, CursorFactory paramCursorFactory, int paramInt, DatabaseErrorHandler paramDatabaseErrorHandler)
    {
        SQLiteDatabase localSQLiteDatabase = new SQLiteDatabase(paramString, paramInt, paramCursorFactory, paramDatabaseErrorHandler);
        localSQLiteDatabase.open();
        return localSQLiteDatabase;
    }

    // ERROR //
    private void openInner()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 114	android/database/sqlite/SQLiteDatabase:mLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: getstatic 79	android/database/sqlite/SQLiteDatabase:$assertionsDisabled	Z
        //     10: ifne +23 -> 33
        //     13: aload_0
        //     14: getfield 161	android/database/sqlite/SQLiteDatabase:mConnectionPoolLocked	Landroid/database/sqlite/SQLiteConnectionPool;
        //     17: ifnull +16 -> 33
        //     20: new 370	java/lang/AssertionError
        //     23: dup
        //     24: invokespecial 371	java/lang/AssertionError:<init>	()V
        //     27: athrow
        //     28: astore_2
        //     29: aload_1
        //     30: monitorexit
        //     31: aload_2
        //     32: athrow
        //     33: aload_0
        //     34: aload_0
        //     35: getfield 133	android/database/sqlite/SQLiteDatabase:mConfigurationLocked	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     38: invokestatic 374	android/database/sqlite/SQLiteConnectionPool:open	(Landroid/database/sqlite/SQLiteDatabaseConfiguration;)Landroid/database/sqlite/SQLiteConnectionPool;
        //     41: putfield 161	android/database/sqlite/SQLiteDatabase:mConnectionPoolLocked	Landroid/database/sqlite/SQLiteConnectionPool;
        //     44: aload_0
        //     45: getfield 122	android/database/sqlite/SQLiteDatabase:mCloseGuardLocked	Ldalvik/system/CloseGuard;
        //     48: ldc_w 375
        //     51: invokevirtual 377	dalvik/system/CloseGuard:open	(Ljava/lang/String;)V
        //     54: aload_1
        //     55: monitorexit
        //     56: getstatic 86	android/database/sqlite/SQLiteDatabase:sActiveDatabases	Ljava/util/WeakHashMap;
        //     59: astore_3
        //     60: aload_3
        //     61: monitorenter
        //     62: getstatic 86	android/database/sqlite/SQLiteDatabase:sActiveDatabases	Ljava/util/WeakHashMap;
        //     65: aload_0
        //     66: aconst_null
        //     67: invokevirtual 381	java/util/WeakHashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     70: pop
        //     71: aload_3
        //     72: monitorexit
        //     73: return
        //     74: astore 4
        //     76: aload_3
        //     77: monitorexit
        //     78: aload 4
        //     80: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     7	31	28	finally
        //     33	56	28	finally
        //     62	78	74	finally
    }

    public static SQLiteDatabase openOrCreateDatabase(File paramFile, CursorFactory paramCursorFactory)
    {
        return openOrCreateDatabase(paramFile.getPath(), paramCursorFactory);
    }

    public static SQLiteDatabase openOrCreateDatabase(String paramString, CursorFactory paramCursorFactory)
    {
        return openDatabase(paramString, paramCursorFactory, 268435456, null);
    }

    public static SQLiteDatabase openOrCreateDatabase(String paramString, CursorFactory paramCursorFactory, DatabaseErrorHandler paramDatabaseErrorHandler)
    {
        return openDatabase(paramString, paramCursorFactory, 268435456, paramDatabaseErrorHandler);
    }

    public static int releaseMemory()
    {
        return SQLiteGlobal.releaseMemory();
    }

    private void throwIfNotOpenLocked()
    {
        if (this.mConnectionPoolLocked == null)
            throw new IllegalStateException("The database '" + this.mConfigurationLocked.label + "' is not open.");
    }

    private boolean yieldIfContendedHelper(boolean paramBoolean, long paramLong)
    {
        acquireReference();
        try
        {
            boolean bool = getThreadSession().yieldTransaction(paramLong, paramBoolean, null);
            return bool;
        }
        finally
        {
            releaseReference();
        }
    }

    public void addCustomFunction(String paramString, int paramInt, CustomFunction paramCustomFunction)
    {
        SQLiteCustomFunction localSQLiteCustomFunction = new SQLiteCustomFunction(paramString, paramInt, paramCustomFunction);
        synchronized (this.mLock)
        {
            throwIfNotOpenLocked();
            this.mConfigurationLocked.customFunctions.add(localSQLiteCustomFunction);
            try
            {
                this.mConnectionPoolLocked.reconfigure(this.mConfigurationLocked);
                return;
            }
            catch (RuntimeException localRuntimeException)
            {
                this.mConfigurationLocked.customFunctions.remove(localSQLiteCustomFunction);
                throw localRuntimeException;
            }
        }
    }

    public void beginTransaction()
    {
        beginTransaction(null, true);
    }

    public void beginTransactionNonExclusive()
    {
        beginTransaction(null, false);
    }

    public void beginTransactionWithListener(SQLiteTransactionListener paramSQLiteTransactionListener)
    {
        beginTransaction(paramSQLiteTransactionListener, true);
    }

    public void beginTransactionWithListenerNonExclusive(SQLiteTransactionListener paramSQLiteTransactionListener)
    {
        beginTransaction(paramSQLiteTransactionListener, false);
    }

    public SQLiteStatement compileStatement(String paramString)
        throws SQLException
    {
        acquireReference();
        try
        {
            SQLiteStatement localSQLiteStatement = new SQLiteStatement(this, paramString, null);
            return localSQLiteStatement;
        }
        finally
        {
            releaseReference();
        }
    }

    SQLiteSession createSession()
    {
        synchronized (this.mLock)
        {
            throwIfNotOpenLocked();
            SQLiteConnectionPool localSQLiteConnectionPool = this.mConnectionPoolLocked;
            return new SQLiteSession(localSQLiteConnectionPool);
        }
    }

    public int delete(String paramString1, String paramString2, String[] paramArrayOfString)
    {
        acquireReference();
        try
        {
            StringBuilder localStringBuilder = new StringBuilder().append("DELETE FROM ").append(paramString1);
            if (!TextUtils.isEmpty(paramString2))
            {
                str = " WHERE " + paramString2;
                localSQLiteStatement = new SQLiteStatement(this, str, paramArrayOfString);
            }
        }
        finally
        {
            try
            {
                int i = localSQLiteStatement.executeUpdateDelete();
                localSQLiteStatement.close();
                return i;
                String str = "";
            }
            finally
            {
                SQLiteStatement localSQLiteStatement;
                localSQLiteStatement.close();
            }
            releaseReference();
        }
    }

    public void disableWriteAheadLogging()
    {
        synchronized (this.mLock)
        {
            throwIfNotOpenLocked();
            if ((0x20000000 & this.mConfigurationLocked.openFlags) == 0)
                return;
            SQLiteDatabaseConfiguration localSQLiteDatabaseConfiguration1 = this.mConfigurationLocked;
            localSQLiteDatabaseConfiguration1.openFlags = (0xDFFFFFFF & localSQLiteDatabaseConfiguration1.openFlags);
        }
        try
        {
            this.mConnectionPoolLocked.reconfigure(this.mConfigurationLocked);
            return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RuntimeException localRuntimeException)
        {
            SQLiteDatabaseConfiguration localSQLiteDatabaseConfiguration2 = this.mConfigurationLocked;
            localSQLiteDatabaseConfiguration2.openFlags = (0x20000000 | localSQLiteDatabaseConfiguration2.openFlags);
            throw localRuntimeException;
        }
    }

    public boolean enableWriteAheadLogging()
    {
        boolean bool = true;
        synchronized (this.mLock)
        {
            throwIfNotOpenLocked();
            if ((0x20000000 & this.mConfigurationLocked.openFlags) != 0)
                break label197;
            if (isReadOnlyLocked())
            {
                bool = false;
                break label197;
            }
            if (this.mConfigurationLocked.isInMemoryDb())
            {
                Log.i("SQLiteDatabase", "can't enable WAL for memory databases.");
                bool = false;
                break label197;
            }
            if (this.mHasAttachedDbsLocked)
            {
                if (Log.isLoggable("SQLiteDatabase", 3))
                    Log.d("SQLiteDatabase", "this database: " + this.mConfigurationLocked.label + " has attached databases. can't    enable WAL.");
                bool = false;
                break label197;
            }
            SQLiteDatabaseConfiguration localSQLiteDatabaseConfiguration1 = this.mConfigurationLocked;
            localSQLiteDatabaseConfiguration1.openFlags = (0x20000000 | localSQLiteDatabaseConfiguration1.openFlags);
        }
        try
        {
            this.mConnectionPoolLocked.reconfigure(this.mConfigurationLocked);
            break label197;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RuntimeException localRuntimeException)
        {
            SQLiteDatabaseConfiguration localSQLiteDatabaseConfiguration2 = this.mConfigurationLocked;
            localSQLiteDatabaseConfiguration2.openFlags = (0xDFFFFFFF & localSQLiteDatabaseConfiguration2.openFlags);
            throw localRuntimeException;
        }
        label197: return bool;
    }

    public void endTransaction()
    {
        acquireReference();
        try
        {
            getThreadSession().endTransaction(null);
            return;
        }
        finally
        {
            releaseReference();
        }
    }

    public void execSQL(String paramString)
        throws SQLException
    {
        executeSql(paramString, null);
    }

    public void execSQL(String paramString, Object[] paramArrayOfObject)
        throws SQLException
    {
        if (paramArrayOfObject == null)
            throw new IllegalArgumentException("Empty bindArgs");
        executeSql(paramString, paramArrayOfObject);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            dispose(true);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    // ERROR //
    public java.util.List<android.util.Pair<String, String>> getAttachedDbs()
    {
        // Byte code:
        //     0: new 251	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 312	java/util/ArrayList:<init>	()V
        //     7: astore_1
        //     8: aload_0
        //     9: getfield 114	android/database/sqlite/SQLiteDatabase:mLock	Ljava/lang/Object;
        //     12: astore_2
        //     13: aload_2
        //     14: monitorenter
        //     15: aload_0
        //     16: getfield 161	android/database/sqlite/SQLiteDatabase:mConnectionPoolLocked	Landroid/database/sqlite/SQLiteConnectionPool;
        //     19: ifnonnull +10 -> 29
        //     22: aload_2
        //     23: monitorexit
        //     24: aconst_null
        //     25: astore_1
        //     26: goto +144 -> 170
        //     29: aload_0
        //     30: getfield 277	android/database/sqlite/SQLiteDatabase:mHasAttachedDbsLocked	Z
        //     33: ifne +35 -> 68
        //     36: aload_1
        //     37: new 492	android/util/Pair
        //     40: dup
        //     41: ldc_w 494
        //     44: aload_0
        //     45: getfield 133	android/database/sqlite/SQLiteDatabase:mConfigurationLocked	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     48: getfield 497	android/database/sqlite/SQLiteDatabaseConfiguration:path	Ljava/lang/String;
        //     51: invokespecial 500	android/util/Pair:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
        //     54: invokevirtual 424	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     57: pop
        //     58: aload_2
        //     59: monitorexit
        //     60: goto +110 -> 170
        //     63: astore_3
        //     64: aload_2
        //     65: monitorexit
        //     66: aload_3
        //     67: athrow
        //     68: aload_0
        //     69: invokevirtual 141	android/database/sqlite/SQLiteDatabase:acquireReference	()V
        //     72: aload_2
        //     73: monitorexit
        //     74: aconst_null
        //     75: astore 4
        //     77: aload_0
        //     78: ldc_w 502
        //     81: aconst_null
        //     82: invokevirtual 506	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
        //     85: astore 4
        //     87: aload 4
        //     89: invokeinterface 511 1 0
        //     94: ifeq +60 -> 154
        //     97: aload_1
        //     98: new 492	android/util/Pair
        //     101: dup
        //     102: aload 4
        //     104: iconst_1
        //     105: invokeinterface 515 2 0
        //     110: aload 4
        //     112: iconst_2
        //     113: invokeinterface 515 2 0
        //     118: invokespecial 500	android/util/Pair:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
        //     121: invokevirtual 424	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     124: pop
        //     125: goto -38 -> 87
        //     128: astore 5
        //     130: aload 4
        //     132: ifnull +10 -> 142
        //     135: aload 4
        //     137: invokeinterface 516 1 0
        //     142: aload 5
        //     144: athrow
        //     145: astore 6
        //     147: aload_0
        //     148: invokevirtual 157	android/database/sqlite/SQLiteDatabase:releaseReference	()V
        //     151: aload 6
        //     153: athrow
        //     154: aload 4
        //     156: ifnull +10 -> 166
        //     159: aload 4
        //     161: invokeinterface 516 1 0
        //     166: aload_0
        //     167: invokevirtual 157	android/database/sqlite/SQLiteDatabase:releaseReference	()V
        //     170: aload_1
        //     171: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     15	66	63	finally
        //     68	74	63	finally
        //     77	125	128	finally
        //     135	145	145	finally
        //     159	166	145	finally
    }

    String getLabel()
    {
        synchronized (this.mLock)
        {
            String str = this.mConfigurationLocked.label;
            return str;
        }
    }

    public long getMaximumSize()
    {
        return DatabaseUtils.longForQuery(this, "PRAGMA max_page_count;", null) * getPageSize();
    }

    public long getPageSize()
    {
        return DatabaseUtils.longForQuery(this, "PRAGMA page_size;", null);
    }

    public final String getPath()
    {
        synchronized (this.mLock)
        {
            String str = this.mConfigurationLocked.path;
            return str;
        }
    }

    @Deprecated
    public Map<String, String> getSyncedTables()
    {
        return new HashMap(0);
    }

    int getThreadDefaultConnectionFlags(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 2)
        {
            if (isMainThread())
                i |= 4;
            return i;
        }
    }

    SQLiteSession getThreadSession()
    {
        return (SQLiteSession)this.mThreadSession.get();
    }

    public int getVersion()
    {
        return Long.valueOf(DatabaseUtils.longForQuery(this, "PRAGMA user_version;", null)).intValue();
    }

    public boolean inTransaction()
    {
        acquireReference();
        try
        {
            boolean bool = getThreadSession().hasTransaction();
            return bool;
        }
        finally
        {
            releaseReference();
        }
    }

    public long insert(String paramString1, String paramString2, ContentValues paramContentValues)
    {
        try
        {
            long l2 = insertWithOnConflict(paramString1, paramString2, paramContentValues, 0);
            l1 = l2;
            return l1;
        }
        catch (SQLException localSQLException)
        {
            while (true)
            {
                Log.e("SQLiteDatabase", "Error inserting " + paramContentValues, localSQLException);
                long l1 = -1L;
            }
        }
    }

    public long insertOrThrow(String paramString1, String paramString2, ContentValues paramContentValues)
        throws SQLException
    {
        return insertWithOnConflict(paramString1, paramString2, paramContentValues, 0);
    }

    public long insertWithOnConflict(String paramString1, String paramString2, ContentValues paramContentValues, int paramInt)
    {
        acquireReference();
        while (true)
        {
            int k;
            String str1;
            try
            {
                StringBuilder localStringBuilder = new StringBuilder();
                localStringBuilder.append("INSERT");
                localStringBuilder.append(CONFLICT_VALUES[paramInt]);
                localStringBuilder.append(" INTO ");
                localStringBuilder.append(paramString1);
                localStringBuilder.append('(');
                Object[] arrayOfObject = null;
                if ((paramContentValues != null) && (paramContentValues.size() > 0))
                {
                    i = paramContentValues.size();
                    if (i > 0)
                    {
                        arrayOfObject = new Object[i];
                        Iterator localIterator = paramContentValues.keySet().iterator();
                        int j = 0;
                        if (localIterator.hasNext())
                        {
                            String str2 = (String)localIterator.next();
                            if (j <= 0)
                                break label308;
                            str3 = ",";
                            localStringBuilder.append(str3);
                            localStringBuilder.append(str2);
                            int m = j + 1;
                            arrayOfObject[j] = paramContentValues.get(str2);
                            j = m;
                            continue;
                        }
                        localStringBuilder.append(')');
                        localStringBuilder.append(" VALUES (");
                        k = 0;
                        break label315;
                        localStringBuilder.append(str1);
                        k++;
                        break label315;
                    }
                    localStringBuilder.append(paramString2 + ") VALUES (NULL");
                    localStringBuilder.append(')');
                    SQLiteStatement localSQLiteStatement = new SQLiteStatement(this, localStringBuilder.toString(), arrayOfObject);
                    try
                    {
                        long l = localSQLiteStatement.executeInsert();
                        return l;
                    }
                    finally
                    {
                    }
                }
            }
            finally
            {
                releaseReference();
            }
            int i = 0;
            continue;
            label308: String str3 = "";
            continue;
            label315: if (k < i)
                if (k > 0)
                    str1 = ",?";
                else
                    str1 = "?";
        }
    }

    // ERROR //
    public boolean isDatabaseIntegrityOk()
    {
        // Byte code:
        //     0: aload_0
        //     1: invokevirtual 141	android/database/sqlite/SQLiteDatabase:acquireReference	()V
        //     4: aconst_null
        //     5: astore_1
        //     6: aload_0
        //     7: invokevirtual 608	android/database/sqlite/SQLiteDatabase:getAttachedDbs	()Ljava/util/List;
        //     10: astore_1
        //     11: aload_1
        //     12: ifnonnull +281 -> 293
        //     15: new 308	java/lang/IllegalStateException
        //     18: dup
        //     19: new 189	java/lang/StringBuilder
        //     22: dup
        //     23: invokespecial 190	java/lang/StringBuilder:<init>	()V
        //     26: ldc_w 610
        //     29: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     32: aload_0
        //     33: invokevirtual 611	android/database/sqlite/SQLiteDatabase:getPath	()Ljava/lang/String;
        //     36: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: ldc_w 613
        //     42: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     45: ldc_w 615
        //     48: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     51: invokevirtual 203	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     54: invokespecial 311	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     57: athrow
        //     58: astore_3
        //     59: aload_1
        //     60: pop
        //     61: new 251	java/util/ArrayList
        //     64: dup
        //     65: invokespecial 312	java/util/ArrayList:<init>	()V
        //     68: astore_1
        //     69: aload_1
        //     70: new 492	android/util/Pair
        //     73: dup
        //     74: ldc_w 494
        //     77: aload_0
        //     78: invokevirtual 611	android/database/sqlite/SQLiteDatabase:getPath	()Ljava/lang/String;
        //     81: invokespecial 500	android/util/Pair:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
        //     84: invokeinterface 618 2 0
        //     89: pop
        //     90: goto +203 -> 293
        //     93: iload 6
        //     95: aload_1
        //     96: invokeinterface 619 1 0
        //     101: if_icmpge +178 -> 279
        //     104: aload_1
        //     105: iload 6
        //     107: invokeinterface 622 2 0
        //     112: checkcast 492	android/util/Pair
        //     115: astore 8
        //     117: aconst_null
        //     118: astore 9
        //     120: aload_0
        //     121: new 189	java/lang/StringBuilder
        //     124: dup
        //     125: invokespecial 190	java/lang/StringBuilder:<init>	()V
        //     128: ldc_w 624
        //     131: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     134: aload 8
        //     136: getfield 627	android/util/Pair:first	Ljava/lang/Object;
        //     139: checkcast 88	java/lang/String
        //     142: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     145: ldc_w 629
        //     148: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     151: invokevirtual 203	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     154: invokevirtual 631	android/database/sqlite/SQLiteDatabase:compileStatement	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
        //     157: astore 9
        //     159: aload 9
        //     161: invokevirtual 634	android/database/sqlite/SQLiteStatement:simpleQueryForString	()Ljava/lang/String;
        //     164: astore 11
        //     166: aload 11
        //     168: ldc_w 636
        //     171: invokevirtual 640	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     174: ifne +89 -> 263
        //     177: ldc 50
        //     179: new 189	java/lang/StringBuilder
        //     182: dup
        //     183: invokespecial 190	java/lang/StringBuilder:<init>	()V
        //     186: ldc_w 642
        //     189: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     192: aload 8
        //     194: getfield 645	android/util/Pair:second	Ljava/lang/Object;
        //     197: checkcast 88	java/lang/String
        //     200: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     203: ldc_w 647
        //     206: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     209: aload 11
        //     211: invokevirtual 198	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     214: invokevirtual 203	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     217: invokestatic 649	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     220: pop
        //     221: iconst_0
        //     222: istore 7
        //     224: aload 9
        //     226: ifnull +8 -> 234
        //     229: aload 9
        //     231: invokevirtual 290	android/database/sqlite/SQLiteStatement:close	()V
        //     234: aload_0
        //     235: invokevirtual 157	android/database/sqlite/SQLiteDatabase:releaseReference	()V
        //     238: iload 7
        //     240: ireturn
        //     241: astore 10
        //     243: aload 9
        //     245: ifnull +8 -> 253
        //     248: aload 9
        //     250: invokevirtual 290	android/database/sqlite/SQLiteStatement:close	()V
        //     253: aload 10
        //     255: athrow
        //     256: astore_2
        //     257: aload_0
        //     258: invokevirtual 157	android/database/sqlite/SQLiteDatabase:releaseReference	()V
        //     261: aload_2
        //     262: athrow
        //     263: aload 9
        //     265: ifnull +8 -> 273
        //     268: aload 9
        //     270: invokevirtual 290	android/database/sqlite/SQLiteStatement:close	()V
        //     273: iinc 6 1
        //     276: goto -183 -> 93
        //     279: aload_0
        //     280: invokevirtual 157	android/database/sqlite/SQLiteDatabase:releaseReference	()V
        //     283: iconst_1
        //     284: istore 7
        //     286: goto -48 -> 238
        //     289: astore_2
        //     290: goto -33 -> 257
        //     293: iconst_0
        //     294: istore 6
        //     296: goto -203 -> 93
        //
        // Exception table:
        //     from	to	target	type
        //     6	58	58	android/database/sqlite/SQLiteException
        //     120	221	241	finally
        //     6	58	256	finally
        //     69	117	256	finally
        //     229	234	256	finally
        //     248	256	256	finally
        //     268	273	256	finally
        //     61	69	289	finally
    }

    public boolean isDbLockedByCurrentThread()
    {
        acquireReference();
        try
        {
            boolean bool = getThreadSession().hasConnection();
            return bool;
        }
        finally
        {
            releaseReference();
        }
    }

    @Deprecated
    public boolean isDbLockedByOtherThreads()
    {
        return false;
    }

    public boolean isInMemoryDatabase()
    {
        synchronized (this.mLock)
        {
            boolean bool = this.mConfigurationLocked.isInMemoryDb();
            return bool;
        }
    }

    public boolean isOpen()
    {
        while (true)
        {
            synchronized (this.mLock)
            {
                if (this.mConnectionPoolLocked != null)
                {
                    bool = true;
                    return bool;
                }
            }
            boolean bool = false;
        }
    }

    public boolean isReadOnly()
    {
        synchronized (this.mLock)
        {
            boolean bool = isReadOnlyLocked();
            return bool;
        }
    }

    public boolean isWriteAheadLoggingEnabled()
    {
        while (true)
        {
            synchronized (this.mLock)
            {
                throwIfNotOpenLocked();
                if ((0x20000000 & this.mConfigurationLocked.openFlags) != 0)
                {
                    bool = true;
                    return bool;
                }
            }
            boolean bool = false;
        }
    }

    @Deprecated
    public void markTableSyncable(String paramString1, String paramString2)
    {
    }

    @Deprecated
    public void markTableSyncable(String paramString1, String paramString2, String paramString3)
    {
    }

    public boolean needUpgrade(int paramInt)
    {
        if (paramInt > getVersion());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void onAllReferencesReleased()
    {
        dispose(false);
    }

    void onCorruption()
    {
        EventLog.writeEvent(75004, getLabel());
        this.mErrorHandler.onCorruption(this);
    }

    public Cursor query(String paramString1, String[] paramArrayOfString1, String paramString2, String[] paramArrayOfString2, String paramString3, String paramString4, String paramString5)
    {
        return query(false, paramString1, paramArrayOfString1, paramString2, paramArrayOfString2, paramString3, paramString4, paramString5, null);
    }

    public Cursor query(String paramString1, String[] paramArrayOfString1, String paramString2, String[] paramArrayOfString2, String paramString3, String paramString4, String paramString5, String paramString6)
    {
        return query(false, paramString1, paramArrayOfString1, paramString2, paramArrayOfString2, paramString3, paramString4, paramString5, paramString6);
    }

    public Cursor query(boolean paramBoolean, String paramString1, String[] paramArrayOfString1, String paramString2, String[] paramArrayOfString2, String paramString3, String paramString4, String paramString5, String paramString6)
    {
        return queryWithFactory(null, paramBoolean, paramString1, paramArrayOfString1, paramString2, paramArrayOfString2, paramString3, paramString4, paramString5, paramString6, null);
    }

    public Cursor query(boolean paramBoolean, String paramString1, String[] paramArrayOfString1, String paramString2, String[] paramArrayOfString2, String paramString3, String paramString4, String paramString5, String paramString6, CancellationSignal paramCancellationSignal)
    {
        return queryWithFactory(null, paramBoolean, paramString1, paramArrayOfString1, paramString2, paramArrayOfString2, paramString3, paramString4, paramString5, paramString6, paramCancellationSignal);
    }

    public Cursor queryWithFactory(CursorFactory paramCursorFactory, boolean paramBoolean, String paramString1, String[] paramArrayOfString1, String paramString2, String[] paramArrayOfString2, String paramString3, String paramString4, String paramString5, String paramString6)
    {
        return queryWithFactory(paramCursorFactory, paramBoolean, paramString1, paramArrayOfString1, paramString2, paramArrayOfString2, paramString3, paramString4, paramString5, paramString6, null);
    }

    public Cursor queryWithFactory(CursorFactory paramCursorFactory, boolean paramBoolean, String paramString1, String[] paramArrayOfString1, String paramString2, String[] paramArrayOfString2, String paramString3, String paramString4, String paramString5, String paramString6, CancellationSignal paramCancellationSignal)
    {
        acquireReference();
        try
        {
            Cursor localCursor = rawQueryWithFactory(paramCursorFactory, SQLiteQueryBuilder.buildQueryString(paramBoolean, paramString1, paramArrayOfString1, paramString2, paramString3, paramString4, paramString5, paramString6), paramArrayOfString2, findEditTable(paramString1), paramCancellationSignal);
            return localCursor;
        }
        finally
        {
            releaseReference();
        }
    }

    public Cursor rawQuery(String paramString, String[] paramArrayOfString)
    {
        return rawQueryWithFactory(null, paramString, paramArrayOfString, null, null);
    }

    public Cursor rawQuery(String paramString, String[] paramArrayOfString, CancellationSignal paramCancellationSignal)
    {
        return rawQueryWithFactory(null, paramString, paramArrayOfString, null, paramCancellationSignal);
    }

    public Cursor rawQueryWithFactory(CursorFactory paramCursorFactory, String paramString1, String[] paramArrayOfString, String paramString2)
    {
        return rawQueryWithFactory(paramCursorFactory, paramString1, paramArrayOfString, paramString2, null);
    }

    public Cursor rawQueryWithFactory(CursorFactory paramCursorFactory, String paramString1, String[] paramArrayOfString, String paramString2, CancellationSignal paramCancellationSignal)
    {
        acquireReference();
        try
        {
            SQLiteDirectCursorDriver localSQLiteDirectCursorDriver = new SQLiteDirectCursorDriver(this, paramString1, paramString2, paramCancellationSignal);
            if (paramCursorFactory != null);
            while (true)
            {
                Cursor localCursor = localSQLiteDirectCursorDriver.query(paramCursorFactory, paramArrayOfString);
                return localCursor;
                paramCursorFactory = this.mCursorFactory;
            }
        }
        finally
        {
            releaseReference();
        }
    }

    public void reopenReadWrite()
    {
        int i;
        synchronized (this.mLock)
        {
            throwIfNotOpenLocked();
            if (!isReadOnlyLocked())
                return;
            i = this.mConfigurationLocked.openFlags;
            this.mConfigurationLocked.openFlags = (0x0 | 0xFFFFFFFE & this.mConfigurationLocked.openFlags);
        }
        try
        {
            this.mConnectionPoolLocked.reconfigure(this.mConfigurationLocked);
            return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RuntimeException localRuntimeException)
        {
            this.mConfigurationLocked.openFlags = i;
            throw localRuntimeException;
        }
    }

    public long replace(String paramString1, String paramString2, ContentValues paramContentValues)
    {
        try
        {
            long l2 = insertWithOnConflict(paramString1, paramString2, paramContentValues, 5);
            l1 = l2;
            return l1;
        }
        catch (SQLException localSQLException)
        {
            while (true)
            {
                Log.e("SQLiteDatabase", "Error inserting " + paramContentValues, localSQLException);
                long l1 = -1L;
            }
        }
    }

    public long replaceOrThrow(String paramString1, String paramString2, ContentValues paramContentValues)
        throws SQLException
    {
        return insertWithOnConflict(paramString1, paramString2, paramContentValues, 5);
    }

    public void setForeignKeyConstraintsEnabled(boolean paramBoolean)
    {
        synchronized (this.mLock)
        {
            throwIfNotOpenLocked();
            if (this.mConfigurationLocked.foreignKeyConstraintsEnabled == paramBoolean)
                return;
            this.mConfigurationLocked.foreignKeyConstraintsEnabled = paramBoolean;
        }
        try
        {
            this.mConnectionPoolLocked.reconfigure(this.mConfigurationLocked);
            return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RuntimeException localRuntimeException)
        {
            SQLiteDatabaseConfiguration localSQLiteDatabaseConfiguration = this.mConfigurationLocked;
            if (!paramBoolean);
            for (boolean bool = true; ; bool = false)
            {
                localSQLiteDatabaseConfiguration.foreignKeyConstraintsEnabled = bool;
                throw localRuntimeException;
            }
        }
    }

    public void setLocale(Locale paramLocale)
    {
        if (paramLocale == null)
            throw new IllegalArgumentException("locale must not be null.");
        synchronized (this.mLock)
        {
            throwIfNotOpenLocked();
            Locale localLocale = this.mConfigurationLocked.locale;
            this.mConfigurationLocked.locale = paramLocale;
            try
            {
                this.mConnectionPoolLocked.reconfigure(this.mConfigurationLocked);
                return;
            }
            catch (RuntimeException localRuntimeException)
            {
                this.mConfigurationLocked.locale = localLocale;
                throw localRuntimeException;
            }
        }
    }

    @Deprecated
    public void setLockingEnabled(boolean paramBoolean)
    {
    }

    public void setMaxSqlCacheSize(int paramInt)
    {
        if ((paramInt > 100) || (paramInt < 0))
            throw new IllegalStateException("expected value between 0 and 100");
        synchronized (this.mLock)
        {
            throwIfNotOpenLocked();
            int i = this.mConfigurationLocked.maxSqlCacheSize;
            this.mConfigurationLocked.maxSqlCacheSize = paramInt;
            try
            {
                this.mConnectionPoolLocked.reconfigure(this.mConfigurationLocked);
                return;
            }
            catch (RuntimeException localRuntimeException)
            {
                this.mConfigurationLocked.maxSqlCacheSize = i;
                throw localRuntimeException;
            }
        }
    }

    public long setMaximumSize(long paramLong)
    {
        long l1 = getPageSize();
        long l2 = paramLong / l1;
        if (paramLong % l1 != 0L)
            l2 += 1L;
        return l1 * DatabaseUtils.longForQuery(this, "PRAGMA max_page_count = " + l2, null);
    }

    public void setPageSize(long paramLong)
    {
        execSQL("PRAGMA page_size = " + paramLong);
    }

    public void setTransactionSuccessful()
    {
        acquireReference();
        try
        {
            getThreadSession().setTransactionSuccessful();
            return;
        }
        finally
        {
            releaseReference();
        }
    }

    public void setVersion(int paramInt)
    {
        execSQL("PRAGMA user_version = " + paramInt);
    }

    public String toString()
    {
        return "SQLiteDatabase: " + getPath();
    }

    public int update(String paramString1, ContentValues paramContentValues, String paramString2, String[] paramArrayOfString)
    {
        return updateWithOnConflict(paramString1, paramContentValues, paramString2, paramArrayOfString, 0);
    }

    public int updateWithOnConflict(String paramString1, ContentValues paramContentValues, String paramString2, String[] paramArrayOfString, int paramInt)
    {
        if ((paramContentValues == null) || (paramContentValues.size() == 0))
            throw new IllegalArgumentException("Empty values");
        acquireReference();
        int k;
        while (true)
        {
            int i;
            int n;
            try
            {
                StringBuilder localStringBuilder = new StringBuilder(120);
                localStringBuilder.append("UPDATE ");
                localStringBuilder.append(CONFLICT_VALUES[paramInt]);
                localStringBuilder.append(paramString1);
                localStringBuilder.append(" SET ");
                i = paramContentValues.size();
                int j;
                Object[] arrayOfObject;
                if (paramArrayOfString == null)
                {
                    j = i;
                    arrayOfObject = new Object[j];
                    Iterator localIterator = paramContentValues.keySet().iterator();
                    k = 0;
                    if (!localIterator.hasNext())
                        break label313;
                    String str1 = (String)localIterator.next();
                    if (k > 0)
                    {
                        str2 = ",";
                        localStringBuilder.append(str2);
                        localStringBuilder.append(str1);
                        int i1 = k + 1;
                        arrayOfObject[k] = paramContentValues.get(str1);
                        localStringBuilder.append("=?");
                        k = i1;
                        continue;
                    }
                }
                else
                {
                    j = i + paramArrayOfString.length;
                    continue;
                    if (n < j)
                    {
                        arrayOfObject[n] = paramArrayOfString[(n - i)];
                        n++;
                        continue;
                    }
                    if (!TextUtils.isEmpty(paramString2))
                    {
                        localStringBuilder.append(" WHERE ");
                        localStringBuilder.append(paramString2);
                    }
                    SQLiteStatement localSQLiteStatement = new SQLiteStatement(this, localStringBuilder.toString(), arrayOfObject);
                    try
                    {
                        int m = localSQLiteStatement.executeUpdateDelete();
                        return m;
                    }
                    finally
                    {
                    }
                }
            }
            finally
            {
                releaseReference();
            }
            String str2 = "";
            continue;
            label313: if (paramArrayOfString != null)
                n = i;
        }
    }

    @Deprecated
    public boolean yieldIfContended()
    {
        return yieldIfContendedHelper(false, -1L);
    }

    public boolean yieldIfContendedSafely()
    {
        return yieldIfContendedHelper(true, -1L);
    }

    public boolean yieldIfContendedSafely(long paramLong)
    {
        return yieldIfContendedHelper(true, paramLong);
    }

    public static abstract interface CustomFunction
    {
        public abstract void callback(String[] paramArrayOfString);
    }

    public static abstract interface CursorFactory
    {
        public abstract Cursor newCursor(SQLiteDatabase paramSQLiteDatabase, SQLiteCursorDriver paramSQLiteCursorDriver, String paramString, SQLiteQuery paramSQLiteQuery);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteDatabase
 * JD-Core Version:        0.6.2
 */