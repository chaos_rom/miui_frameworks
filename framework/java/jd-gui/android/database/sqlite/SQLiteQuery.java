package android.database.sqlite;

import android.os.CancellationSignal;

public final class SQLiteQuery extends SQLiteProgram
{
    private static final String TAG = "SQLiteQuery";
    private final CancellationSignal mCancellationSignal;

    SQLiteQuery(SQLiteDatabase paramSQLiteDatabase, String paramString, CancellationSignal paramCancellationSignal)
    {
        super(paramSQLiteDatabase, paramString, null, paramCancellationSignal);
        this.mCancellationSignal = paramCancellationSignal;
    }

    // ERROR //
    int fillWindow(android.database.CursorWindow paramCursorWindow, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokevirtual 27	android/database/sqlite/SQLiteQuery:acquireReference	()V
        //     4: aload_1
        //     5: invokevirtual 30	android/database/CursorWindow:acquireReference	()V
        //     8: aload_0
        //     9: invokevirtual 34	android/database/sqlite/SQLiteQuery:getSession	()Landroid/database/sqlite/SQLiteSession;
        //     12: aload_0
        //     13: invokevirtual 38	android/database/sqlite/SQLiteQuery:getSql	()Ljava/lang/String;
        //     16: aload_0
        //     17: invokevirtual 42	android/database/sqlite/SQLiteQuery:getBindArgs	()[Ljava/lang/Object;
        //     20: aload_1
        //     21: iload_2
        //     22: iload_3
        //     23: iload 4
        //     25: aload_0
        //     26: invokevirtual 46	android/database/sqlite/SQLiteQuery:getConnectionFlags	()I
        //     29: aload_0
        //     30: getfield 17	android/database/sqlite/SQLiteQuery:mCancellationSignal	Landroid/os/CancellationSignal;
        //     33: invokevirtual 52	android/database/sqlite/SQLiteSession:executeForCursorWindow	(Ljava/lang/String;[Ljava/lang/Object;Landroid/database/CursorWindow;IIZILandroid/os/CancellationSignal;)I
        //     36: istore 10
        //     38: aload_1
        //     39: invokevirtual 55	android/database/CursorWindow:releaseReference	()V
        //     42: aload_0
        //     43: invokevirtual 56	android/database/sqlite/SQLiteQuery:releaseReference	()V
        //     46: iload 10
        //     48: ireturn
        //     49: astore 9
        //     51: aload_0
        //     52: invokevirtual 59	android/database/sqlite/SQLiteQuery:onCorruption	()V
        //     55: aload 9
        //     57: athrow
        //     58: astore 8
        //     60: aload_1
        //     61: invokevirtual 55	android/database/CursorWindow:releaseReference	()V
        //     64: aload 8
        //     66: athrow
        //     67: astore 5
        //     69: aload_0
        //     70: invokevirtual 56	android/database/sqlite/SQLiteQuery:releaseReference	()V
        //     73: aload 5
        //     75: athrow
        //     76: astore 6
        //     78: ldc 8
        //     80: new 61	java/lang/StringBuilder
        //     83: dup
        //     84: invokespecial 63	java/lang/StringBuilder:<init>	()V
        //     87: ldc 65
        //     89: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     92: aload 6
        //     94: invokevirtual 72	android/database/sqlite/SQLiteException:getMessage	()Ljava/lang/String;
        //     97: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     100: ldc 74
        //     102: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     105: aload_0
        //     106: invokevirtual 38	android/database/sqlite/SQLiteQuery:getSql	()Ljava/lang/String;
        //     109: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     112: invokevirtual 77	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     115: invokestatic 83	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     118: pop
        //     119: aload 6
        //     121: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     8	38	49	android/database/sqlite/SQLiteDatabaseCorruptException
        //     8	38	58	finally
        //     51	58	58	finally
        //     78	122	58	finally
        //     4	8	67	finally
        //     38	42	67	finally
        //     60	67	67	finally
        //     8	38	76	android/database/sqlite/SQLiteException
    }

    public String toString()
    {
        return "SQLiteQuery: " + getSql();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteQuery
 * JD-Core Version:        0.6.2
 */