package android.database.sqlite;

public class SQLiteAccessPermException extends SQLiteException
{
    public SQLiteAccessPermException()
    {
    }

    public SQLiteAccessPermException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteAccessPermException
 * JD-Core Version:        0.6.2
 */