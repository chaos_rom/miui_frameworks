package android.database.sqlite;

import android.database.AbstractWindowedCursor;
import android.database.CursorWindow;
import android.database.DatabaseUtils;
import android.os.StrictMode;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

public class SQLiteCursor extends AbstractWindowedCursor
{
    static final int NO_COUNT = -1;
    static final String TAG = "SQLiteCursor";
    private Map<String, Integer> mColumnNameMap;
    private final String[] mColumns;
    private int mCount = -1;
    private int mCursorWindowCapacity;
    private final SQLiteCursorDriver mDriver;
    private final String mEditTable;
    private final SQLiteQuery mQuery;
    private final Throwable mStackTrace;

    public SQLiteCursor(SQLiteCursorDriver paramSQLiteCursorDriver, String paramString, SQLiteQuery paramSQLiteQuery)
    {
        if (paramSQLiteQuery == null)
            throw new IllegalArgumentException("query object cannot be null");
        if (StrictMode.vmSqliteObjectLeaksEnabled());
        for (this.mStackTrace = new DatabaseObjectNotClosedException().fillInStackTrace(); ; this.mStackTrace = null)
        {
            this.mDriver = paramSQLiteCursorDriver;
            this.mEditTable = paramString;
            this.mColumnNameMap = null;
            this.mQuery = paramSQLiteQuery;
            this.mColumns = paramSQLiteQuery.getColumnNames();
            this.mRowIdColumnIndex = DatabaseUtils.findRowIdColumnIndex(this.mColumns);
            return;
        }
    }

    @Deprecated
    public SQLiteCursor(SQLiteDatabase paramSQLiteDatabase, SQLiteCursorDriver paramSQLiteCursorDriver, String paramString, SQLiteQuery paramSQLiteQuery)
    {
        this(paramSQLiteCursorDriver, paramString, paramSQLiteQuery);
    }

    private void fillWindow(int paramInt)
    {
        clearOrCreateWindow(getDatabase().getPath());
        if (this.mCount == -1)
        {
            int j = DatabaseUtils.cursorPickFillWindowStartPosition(paramInt, 0);
            this.mCount = this.mQuery.fillWindow(this.mWindow, j, paramInt, true);
            this.mCursorWindowCapacity = this.mWindow.getNumRows();
            if (Log.isLoggable("SQLiteCursor", 3))
                Log.d("SQLiteCursor", "received count(*) from native_fill_window: " + this.mCount);
        }
        while (true)
        {
            return;
            int i = DatabaseUtils.cursorPickFillWindowStartPosition(paramInt, this.mCursorWindowCapacity);
            this.mQuery.fillWindow(this.mWindow, i, paramInt, false);
        }
    }

    public void close()
    {
        super.close();
        try
        {
            this.mQuery.close();
            this.mDriver.cursorClosed();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void deactivate()
    {
        super.deactivate();
        this.mDriver.cursorDeactivated();
    }

    protected void finalize()
    {
        try
        {
            if (this.mWindow != null)
            {
                if (this.mStackTrace != null)
                {
                    String str = this.mQuery.getSql();
                    int i = str.length();
                    StringBuilder localStringBuilder = new StringBuilder().append("Finalizing a Cursor that has not been deactivated or closed. database = ").append(this.mQuery.getDatabase().getLabel()).append(", table = ").append(this.mEditTable).append(", query = ");
                    if (i > 1000)
                        i = 1000;
                    StrictMode.onSqliteObjectLeaked(str.substring(0, i), this.mStackTrace);
                }
                close();
            }
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getColumnIndex(String paramString)
    {
        int i = -1;
        if (this.mColumnNameMap == null)
        {
            String[] arrayOfString = this.mColumns;
            int k = arrayOfString.length;
            HashMap localHashMap = new HashMap(k, 1.0F);
            for (int m = 0; m < k; m++)
                localHashMap.put(arrayOfString[m], Integer.valueOf(m));
            this.mColumnNameMap = localHashMap;
        }
        int j = paramString.lastIndexOf('.');
        if (j != i)
        {
            Exception localException = new Exception();
            Log.e("SQLiteCursor", "requesting column name with table name -- " + paramString, localException);
            paramString = paramString.substring(j + 1);
        }
        Integer localInteger = (Integer)this.mColumnNameMap.get(paramString);
        if (localInteger != null)
            i = localInteger.intValue();
        return i;
    }

    public String[] getColumnNames()
    {
        return this.mColumns;
    }

    public int getCount()
    {
        if (this.mCount == -1)
            fillWindow(0);
        return this.mCount;
    }

    public SQLiteDatabase getDatabase()
    {
        return this.mQuery.getDatabase();
    }

    public boolean onMove(int paramInt1, int paramInt2)
    {
        if ((this.mWindow == null) || (paramInt2 < this.mWindow.getStartPosition()) || (paramInt2 >= this.mWindow.getStartPosition() + this.mWindow.getNumRows()))
            fillWindow(paramInt2);
        return true;
    }

    // ERROR //
    public boolean requery()
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_1
        //     2: aload_0
        //     3: invokevirtual 245	android/database/sqlite/SQLiteCursor:isClosed	()Z
        //     6: ifeq +5 -> 11
        //     9: iload_1
        //     10: ireturn
        //     11: aload_0
        //     12: monitorenter
        //     13: aload_0
        //     14: getfield 62	android/database/sqlite/SQLiteCursor:mQuery	Landroid/database/sqlite/SQLiteQuery;
        //     17: invokevirtual 171	android/database/sqlite/SQLiteQuery:getDatabase	()Landroid/database/sqlite/SQLiteDatabase;
        //     20: invokevirtual 248	android/database/sqlite/SQLiteDatabase:isOpen	()Z
        //     23: ifne +13 -> 36
        //     26: aload_0
        //     27: monitorexit
        //     28: goto -19 -> 9
        //     31: astore_2
        //     32: aload_0
        //     33: monitorexit
        //     34: aload_2
        //     35: athrow
        //     36: aload_0
        //     37: getfield 108	android/database/AbstractWindowedCursor:mWindow	Landroid/database/CursorWindow;
        //     40: ifnull +10 -> 50
        //     43: aload_0
        //     44: getfield 108	android/database/AbstractWindowedCursor:mWindow	Landroid/database/CursorWindow;
        //     47: invokevirtual 251	android/database/CursorWindow:clear	()V
        //     50: aload_0
        //     51: bipush 255
        //     53: putfield 254	android/database/AbstractCursor:mPos	I
        //     56: aload_0
        //     57: bipush 255
        //     59: putfield 32	android/database/sqlite/SQLiteCursor:mCount	I
        //     62: aload_0
        //     63: getfield 56	android/database/sqlite/SQLiteCursor:mDriver	Landroid/database/sqlite/SQLiteCursorDriver;
        //     66: aload_0
        //     67: invokeinterface 258 2 0
        //     72: aload_0
        //     73: monitorexit
        //     74: aload_0
        //     75: invokespecial 260	android/database/AbstractWindowedCursor:requery	()Z
        //     78: istore 5
        //     80: iload 5
        //     82: istore_1
        //     83: goto -74 -> 9
        //     86: astore_3
        //     87: ldc 11
        //     89: new 127	java/lang/StringBuilder
        //     92: dup
        //     93: invokespecial 128	java/lang/StringBuilder:<init>	()V
        //     96: ldc_w 262
        //     99: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     102: aload_3
        //     103: invokevirtual 265	java/lang/IllegalStateException:getMessage	()Ljava/lang/String;
        //     106: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     109: invokevirtual 140	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     112: aload_3
        //     113: invokestatic 268	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     116: pop
        //     117: goto -108 -> 9
        //
        // Exception table:
        //     from	to	target	type
        //     13	34	31	finally
        //     36	74	31	finally
        //     74	80	86	java/lang/IllegalStateException
    }

    public void setSelectionArguments(String[] paramArrayOfString)
    {
        this.mDriver.setBindArguments(paramArrayOfString);
    }

    public void setWindow(CursorWindow paramCursorWindow)
    {
        super.setWindow(paramCursorWindow);
        this.mCount = -1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteCursor
 * JD-Core Version:        0.6.2
 */