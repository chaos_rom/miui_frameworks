package android.database.sqlite;

public class SQLiteAbortException extends SQLiteException
{
    public SQLiteAbortException()
    {
    }

    public SQLiteAbortException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteAbortException
 * JD-Core Version:        0.6.2
 */