package android.database.sqlite;

public class SQLiteBindOrColumnIndexOutOfRangeException extends SQLiteException
{
    public SQLiteBindOrColumnIndexOutOfRangeException()
    {
    }

    public SQLiteBindOrColumnIndexOutOfRangeException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteBindOrColumnIndexOutOfRangeException
 * JD-Core Version:        0.6.2
 */