package android.database.sqlite;

import android.database.CursorWindow;
import android.database.DatabaseUtils;
import android.os.CancellationSignal;
import android.os.CancellationSignal.OnCancelListener;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.util.LruCache;
import android.util.Printer;
import dalvik.system.BlockGuard;
import dalvik.system.BlockGuard.Policy;
import dalvik.system.CloseGuard;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class SQLiteConnection
    implements CancellationSignal.OnCancelListener
{
    private static final boolean DEBUG = false;
    private static final byte[] EMPTY_BYTE_ARRAY;
    private static final String[] EMPTY_STRING_ARRAY;
    private static final String TAG = "SQLiteConnection";
    private static final Pattern TRIM_SQL_PATTERN;
    private int mCancellationSignalAttachCount;
    private final CloseGuard mCloseGuard = CloseGuard.get();
    private final SQLiteDatabaseConfiguration mConfiguration;
    private final int mConnectionId;
    private int mConnectionPtr;
    private final boolean mIsPrimaryConnection;
    private final boolean mIsReadOnlyConnection;
    private boolean mOnlyAllowReadOnlyOperations;
    private final SQLiteConnectionPool mPool;
    private final PreparedStatementCache mPreparedStatementCache;
    private PreparedStatement mPreparedStatementPool;
    private final OperationLog mRecentOperations = new OperationLog(null);

    static
    {
        if (!SQLiteConnection.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            EMPTY_STRING_ARRAY = new String[0];
            EMPTY_BYTE_ARRAY = new byte[0];
            TRIM_SQL_PATTERN = Pattern.compile("[\\s]*\\n+[\\s]*");
            return;
        }
    }

    private SQLiteConnection(SQLiteConnectionPool paramSQLiteConnectionPool, SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration, int paramInt, boolean paramBoolean)
    {
        this.mPool = paramSQLiteConnectionPool;
        this.mConfiguration = new SQLiteDatabaseConfiguration(paramSQLiteDatabaseConfiguration);
        this.mConnectionId = paramInt;
        this.mIsPrimaryConnection = paramBoolean;
        if ((0x1 & paramSQLiteDatabaseConfiguration.openFlags) != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mIsReadOnlyConnection = bool;
            this.mPreparedStatementCache = new PreparedStatementCache(this.mConfiguration.maxSqlCacheSize);
            this.mCloseGuard.open("close");
            return;
        }
    }

    private PreparedStatement acquirePreparedStatement(String paramString)
    {
        PreparedStatement localPreparedStatement1 = (PreparedStatement)this.mPreparedStatementCache.get(paramString);
        int i = 0;
        PreparedStatement localPreparedStatement2;
        if (localPreparedStatement1 != null)
            if (!localPreparedStatement1.mInUse)
                localPreparedStatement2 = localPreparedStatement1;
        while (true)
        {
            return localPreparedStatement2;
            i = 1;
            int j = nativePrepareStatement(this.mConnectionPtr, paramString);
            try
            {
                int k = nativeGetParameterCount(this.mConnectionPtr, j);
                int m = DatabaseUtils.getSqlStatementType(paramString);
                localPreparedStatement1 = obtainPreparedStatement(paramString, j, k, m, nativeIsReadOnly(this.mConnectionPtr, j));
                if ((i == 0) && (isCacheable(m)))
                {
                    this.mPreparedStatementCache.put(paramString, localPreparedStatement1);
                    localPreparedStatement1.mInCache = true;
                }
                localPreparedStatement1.mInUse = true;
                localPreparedStatement2 = localPreparedStatement1;
            }
            catch (RuntimeException localRuntimeException)
            {
                if ((localPreparedStatement1 == null) || (!localPreparedStatement1.mInCache))
                    nativeFinalizeStatement(this.mConnectionPtr, j);
                throw localRuntimeException;
            }
        }
    }

    private void applyBlockGuardPolicy(PreparedStatement paramPreparedStatement)
    {
        if (!this.mConfiguration.isInMemoryDb())
        {
            if (!paramPreparedStatement.mReadOnly)
                break label26;
            BlockGuard.getThreadPolicy().onReadFromDisk();
        }
        while (true)
        {
            return;
            label26: BlockGuard.getThreadPolicy().onWriteToDisk();
        }
    }

    private void attachCancellationSignal(CancellationSignal paramCancellationSignal)
    {
        if (paramCancellationSignal != null)
        {
            paramCancellationSignal.throwIfCanceled();
            this.mCancellationSignalAttachCount = (1 + this.mCancellationSignalAttachCount);
            if (this.mCancellationSignalAttachCount == 1)
            {
                nativeResetCancel(this.mConnectionPtr, true);
                paramCancellationSignal.setOnCancelListener(this);
            }
        }
    }

    private void bindArguments(PreparedStatement paramPreparedStatement, Object[] paramArrayOfObject)
    {
        if (paramArrayOfObject != null);
        for (int i = paramArrayOfObject.length; i != paramPreparedStatement.mNumParameters; i = 0)
            throw new SQLiteBindOrColumnIndexOutOfRangeException("Expected " + paramPreparedStatement.mNumParameters + " bind arguments but " + paramArrayOfObject.length + " were provided.");
        if (i == 0);
        int j;
        int k;
        do
        {
            return;
            j = paramPreparedStatement.mStatementPtr;
            k = 0;
        }
        while (k >= i);
        Object localObject = paramArrayOfObject[k];
        long l;
        switch (DatabaseUtils.getTypeOfObject(localObject))
        {
        case 3:
        default:
            if ((localObject instanceof Boolean))
            {
                int m = this.mConnectionPtr;
                int n = k + 1;
                if (((Boolean)localObject).booleanValue())
                {
                    l = 1L;
                    label166: nativeBindLong(m, j, n, l);
                }
            }
            break;
        case 0:
        case 1:
        case 2:
        case 4:
        }
        while (true)
        {
            k++;
            break;
            nativeBindNull(this.mConnectionPtr, j, k + 1);
            continue;
            nativeBindLong(this.mConnectionPtr, j, k + 1, ((Number)localObject).longValue());
            continue;
            nativeBindDouble(this.mConnectionPtr, j, k + 1, ((Number)localObject).doubleValue());
            continue;
            nativeBindBlob(this.mConnectionPtr, j, k + 1, (byte[])localObject);
            continue;
            l = 0L;
            break label166;
            nativeBindString(this.mConnectionPtr, j, k + 1, localObject.toString());
        }
    }

    private static String canonicalizeSyncMode(String paramString)
    {
        if (paramString.equals("0"))
            paramString = "OFF";
        while (true)
        {
            return paramString;
            if (paramString.equals("1"))
                paramString = "NORMAL";
            else if (paramString.equals("2"))
                paramString = "FULL";
        }
    }

    private void detachCancellationSignal(CancellationSignal paramCancellationSignal)
    {
        if (paramCancellationSignal != null)
        {
            assert (this.mCancellationSignalAttachCount > 0);
            this.mCancellationSignalAttachCount = (-1 + this.mCancellationSignalAttachCount);
            if (this.mCancellationSignalAttachCount == 0)
            {
                paramCancellationSignal.setOnCancelListener(null);
                nativeResetCancel(this.mConnectionPtr, false);
            }
        }
    }

    private void dispose(boolean paramBoolean)
    {
        if (this.mCloseGuard != null)
        {
            if (paramBoolean)
                this.mCloseGuard.warnIfOpen();
            this.mCloseGuard.close();
        }
        int i;
        if (this.mConnectionPtr != 0)
            i = this.mRecentOperations.beginOperation("close", null, null);
        try
        {
            this.mPreparedStatementCache.evictAll();
            nativeClose(this.mConnectionPtr);
            this.mConnectionPtr = 0;
            return;
        }
        finally
        {
            this.mRecentOperations.endOperation(i);
        }
    }

    private void finalizePreparedStatement(PreparedStatement paramPreparedStatement)
    {
        nativeFinalizeStatement(this.mConnectionPtr, paramPreparedStatement.mStatementPtr);
        recyclePreparedStatement(paramPreparedStatement);
    }

    private SQLiteDebug.DbStats getMainDbStatsUnsafe(int paramInt, long paramLong1, long paramLong2)
    {
        String str = this.mConfiguration.path;
        if (!this.mIsPrimaryConnection)
            str = str + " (" + this.mConnectionId + ")";
        return new SQLiteDebug.DbStats(str, paramLong1, paramLong2, paramInt, this.mPreparedStatementCache.hitCount(), this.mPreparedStatementCache.missCount(), this.mPreparedStatementCache.size());
    }

    private static boolean isCacheable(int paramInt)
    {
        int i = 1;
        if ((paramInt == 2) || (paramInt == i));
        while (true)
        {
            return i;
            i = 0;
        }
    }

    private static native void nativeBindBlob(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfByte);

    private static native void nativeBindDouble(int paramInt1, int paramInt2, int paramInt3, double paramDouble);

    private static native void nativeBindLong(int paramInt1, int paramInt2, int paramInt3, long paramLong);

    private static native void nativeBindNull(int paramInt1, int paramInt2, int paramInt3);

    private static native void nativeBindString(int paramInt1, int paramInt2, int paramInt3, String paramString);

    private static native void nativeCancel(int paramInt);

    private static native void nativeClose(int paramInt);

    private static native void nativeExecute(int paramInt1, int paramInt2);

    private static native int nativeExecuteForBlobFileDescriptor(int paramInt1, int paramInt2);

    private static native int nativeExecuteForChangedRowCount(int paramInt1, int paramInt2);

    private static native long nativeExecuteForCursorWindow(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean);

    private static native long nativeExecuteForLastInsertedRowId(int paramInt1, int paramInt2);

    private static native long nativeExecuteForLong(int paramInt1, int paramInt2);

    private static native String nativeExecuteForString(int paramInt1, int paramInt2);

    private static native void nativeFinalizeStatement(int paramInt1, int paramInt2);

    private static native int nativeGetColumnCount(int paramInt1, int paramInt2);

    private static native String nativeGetColumnName(int paramInt1, int paramInt2, int paramInt3);

    private static native int nativeGetDbLookaside(int paramInt);

    private static native int nativeGetParameterCount(int paramInt1, int paramInt2);

    private static native boolean nativeIsReadOnly(int paramInt1, int paramInt2);

    private static native int nativeOpen(String paramString1, int paramInt, String paramString2, boolean paramBoolean1, boolean paramBoolean2);

    private static native int nativePrepareStatement(int paramInt, String paramString);

    private static native void nativeRegisterCustomFunction(int paramInt, SQLiteCustomFunction paramSQLiteCustomFunction);

    private static native void nativeRegisterLocalizedCollators(int paramInt, String paramString);

    private static native void nativeResetCancel(int paramInt, boolean paramBoolean);

    private static native void nativeResetStatementAndClearBindings(int paramInt1, int paramInt2);

    private PreparedStatement obtainPreparedStatement(String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        PreparedStatement localPreparedStatement = this.mPreparedStatementPool;
        if (localPreparedStatement != null)
        {
            this.mPreparedStatementPool = localPreparedStatement.mPoolNext;
            localPreparedStatement.mPoolNext = null;
            localPreparedStatement.mInCache = false;
        }
        while (true)
        {
            localPreparedStatement.mSql = paramString;
            localPreparedStatement.mStatementPtr = paramInt1;
            localPreparedStatement.mNumParameters = paramInt2;
            localPreparedStatement.mType = paramInt3;
            localPreparedStatement.mReadOnly = paramBoolean;
            return localPreparedStatement;
            localPreparedStatement = new PreparedStatement(null);
        }
    }

    static SQLiteConnection open(SQLiteConnectionPool paramSQLiteConnectionPool, SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration, int paramInt, boolean paramBoolean)
    {
        SQLiteConnection localSQLiteConnection = new SQLiteConnection(paramSQLiteConnectionPool, paramSQLiteDatabaseConfiguration, paramInt, paramBoolean);
        try
        {
            localSQLiteConnection.open();
            return localSQLiteConnection;
        }
        catch (SQLiteException localSQLiteException)
        {
            localSQLiteConnection.dispose(false);
            throw localSQLiteException;
        }
    }

    private void open()
    {
        this.mConnectionPtr = nativeOpen(this.mConfiguration.path, this.mConfiguration.openFlags, this.mConfiguration.label, SQLiteDebug.DEBUG_SQL_STATEMENTS, SQLiteDebug.DEBUG_SQL_TIME);
        setPageSize();
        setForeignKeyModeFromConfiguration();
        setWalModeFromConfiguration();
        setJournalSizeLimit();
        setAutoCheckpointInterval();
        setLocaleFromConfiguration();
    }

    private void recyclePreparedStatement(PreparedStatement paramPreparedStatement)
    {
        paramPreparedStatement.mSql = null;
        paramPreparedStatement.mPoolNext = this.mPreparedStatementPool;
        this.mPreparedStatementPool = paramPreparedStatement;
    }

    private void releasePreparedStatement(PreparedStatement paramPreparedStatement)
    {
        paramPreparedStatement.mInUse = false;
        if (paramPreparedStatement.mInCache);
        while (true)
        {
            try
            {
                nativeResetStatementAndClearBindings(this.mConnectionPtr, paramPreparedStatement.mStatementPtr);
                return;
            }
            catch (SQLiteException localSQLiteException)
            {
                this.mPreparedStatementCache.remove(paramPreparedStatement.mSql);
                continue;
            }
            finalizePreparedStatement(paramPreparedStatement);
        }
    }

    private void setAutoCheckpointInterval()
    {
        if ((!this.mConfiguration.isInMemoryDb()) && (!this.mIsReadOnlyConnection))
        {
            long l = SQLiteGlobal.getWALAutoCheckpoint();
            if (executeForLong("PRAGMA wal_autocheckpoint", null, null) != l)
                executeForLong("PRAGMA wal_autocheckpoint=" + l, null, null);
        }
    }

    private void setForeignKeyModeFromConfiguration()
    {
        if (!this.mIsReadOnlyConnection)
            if (!this.mConfiguration.foreignKeyConstraintsEnabled)
                break label60;
        label60: for (long l = 1L; ; l = 0L)
        {
            if (executeForLong("PRAGMA foreign_keys", null, null) != l)
                execute("PRAGMA foreign_keys=" + l, null, null);
            return;
        }
    }

    private void setJournalMode(String paramString)
    {
        String str = executeForString("PRAGMA journal_mode", null, null);
        if (!str.equalsIgnoreCase(paramString));
        try
        {
            boolean bool = executeForString("PRAGMA journal_mode=" + paramString, null, null).equalsIgnoreCase(paramString);
            if (bool)
                return;
        }
        catch (SQLiteDatabaseLockedException localSQLiteDatabaseLockedException)
        {
            while (true)
                Log.w("SQLiteConnection", "Could not change the database journal mode of '" + this.mConfiguration.label + "' from '" + str + "' to '" + paramString + "' because the database is locked.    This usually means that " + "there are other open connections to the database which prevents " + "the database from enabling or disabling write-ahead logging mode.    " + "Proceeding without changing the journal mode.");
        }
    }

    private void setJournalSizeLimit()
    {
        if ((!this.mConfiguration.isInMemoryDb()) && (!this.mIsReadOnlyConnection))
        {
            long l = SQLiteGlobal.getJournalSizeLimit();
            if (executeForLong("PRAGMA journal_size_limit", null, null) != l)
                executeForLong("PRAGMA journal_size_limit=" + l, null, null);
        }
    }

    // ERROR //
    private void setLocaleFromConfiguration()
    {
        // Byte code:
        //     0: bipush 16
        //     2: aload_0
        //     3: getfield 105	android/database/sqlite/SQLiteConnection:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     6: getfield 112	android/database/sqlite/SQLiteDatabaseConfiguration:openFlags	I
        //     9: iand
        //     10: ifeq +4 -> 14
        //     13: return
        //     14: aload_0
        //     15: getfield 105	android/database/sqlite/SQLiteConnection:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     18: getfield 524	android/database/sqlite/SQLiteDatabaseConfiguration:locale	Ljava/util/Locale;
        //     21: invokevirtual 527	java/util/Locale:toString	()Ljava/lang/String;
        //     24: astore_1
        //     25: aload_0
        //     26: getfield 153	android/database/sqlite/SQLiteConnection:mConnectionPtr	I
        //     29: aload_1
        //     30: invokestatic 529	android/database/sqlite/SQLiteConnection:nativeRegisterLocalizedCollators	(ILjava/lang/String;)V
        //     33: aload_0
        //     34: getfield 114	android/database/sqlite/SQLiteConnection:mIsReadOnlyConnection	Z
        //     37: ifne -24 -> 13
        //     40: aload_0
        //     41: ldc_w 531
        //     44: aconst_null
        //     45: aconst_null
        //     46: invokevirtual 478	android/database/sqlite/SQLiteConnection:execute	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
        //     49: aload_0
        //     50: ldc_w 533
        //     53: aconst_null
        //     54: aconst_null
        //     55: invokevirtual 487	android/database/sqlite/SQLiteConnection:executeForString	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)Ljava/lang/String;
        //     58: astore_3
        //     59: aload_3
        //     60: ifnull +11 -> 71
        //     63: aload_3
        //     64: aload_1
        //     65: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     68: ifne -55 -> 13
        //     71: aload_0
        //     72: ldc_w 535
        //     75: aconst_null
        //     76: aconst_null
        //     77: invokevirtual 478	android/database/sqlite/SQLiteConnection:execute	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
        //     80: aload_0
        //     81: ldc_w 537
        //     84: aconst_null
        //     85: aconst_null
        //     86: invokevirtual 478	android/database/sqlite/SQLiteConnection:execute	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
        //     89: iconst_1
        //     90: anewarray 4	java/lang/Object
        //     93: astore 6
        //     95: aload 6
        //     97: iconst_0
        //     98: aload_1
        //     99: aastore
        //     100: aload_0
        //     101: ldc_w 539
        //     104: aload 6
        //     106: aconst_null
        //     107: invokevirtual 478	android/database/sqlite/SQLiteConnection:execute	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
        //     110: aload_0
        //     111: ldc_w 541
        //     114: aconst_null
        //     115: aconst_null
        //     116: invokevirtual 478	android/database/sqlite/SQLiteConnection:execute	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
        //     119: iconst_1
        //     120: ifeq +93 -> 213
        //     123: ldc_w 543
        //     126: astore 7
        //     128: aload_0
        //     129: aload 7
        //     131: aconst_null
        //     132: aconst_null
        //     133: invokevirtual 478	android/database/sqlite/SQLiteConnection:execute	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
        //     136: goto -123 -> 13
        //     139: astore_2
        //     140: new 408	android/database/sqlite/SQLiteException
        //     143: dup
        //     144: new 237	java/lang/StringBuilder
        //     147: dup
        //     148: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     151: ldc_w 545
        //     154: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     157: aload_0
        //     158: getfield 105	android/database/sqlite/SQLiteConnection:mConfiguration	Landroid/database/sqlite/SQLiteDatabaseConfiguration;
        //     161: getfield 417	android/database/sqlite/SQLiteDatabaseConfiguration:label	Ljava/lang/String;
        //     164: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     167: ldc_w 499
        //     170: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     173: aload_1
        //     174: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     177: ldc_w 547
        //     180: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     183: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     186: aload_2
        //     187: invokespecial 550	android/database/sqlite/SQLiteException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     190: athrow
        //     191: astore 4
        //     193: iconst_0
        //     194: ifeq +27 -> 221
        //     197: ldc_w 543
        //     200: astore 5
        //     202: aload_0
        //     203: aload 5
        //     205: aconst_null
        //     206: aconst_null
        //     207: invokevirtual 478	android/database/sqlite/SQLiteConnection:execute	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)V
        //     210: aload 4
        //     212: athrow
        //     213: ldc_w 552
        //     216: astore 7
        //     218: goto -90 -> 128
        //     221: ldc_w 552
        //     224: astore 5
        //     226: goto -24 -> 202
        //
        // Exception table:
        //     from	to	target	type
        //     40	80	139	java/lang/RuntimeException
        //     123	136	139	java/lang/RuntimeException
        //     197	218	139	java/lang/RuntimeException
        //     80	119	191	finally
    }

    private void setPageSize()
    {
        if ((!this.mConfiguration.isInMemoryDb()) && (!this.mIsReadOnlyConnection))
        {
            long l = SQLiteGlobal.getDefaultPageSize();
            if (executeForLong("PRAGMA page_size", null, null) != l)
                execute("PRAGMA page_size=" + l, null, null);
        }
    }

    private void setSyncMode(String paramString)
    {
        if (!canonicalizeSyncMode(executeForString("PRAGMA synchronous", null, null)).equalsIgnoreCase(canonicalizeSyncMode(paramString)))
            execute("PRAGMA synchronous=" + paramString, null, null);
    }

    private void setWalModeFromConfiguration()
    {
        if ((!this.mConfiguration.isInMemoryDb()) && (!this.mIsReadOnlyConnection))
        {
            if ((0x20000000 & this.mConfiguration.openFlags) == 0)
                break label46;
            setJournalMode("WAL");
            setSyncMode(SQLiteGlobal.getWALSyncMode());
        }
        while (true)
        {
            return;
            label46: setJournalMode(SQLiteGlobal.getDefaultJournalMode());
            setSyncMode(SQLiteGlobal.getDefaultSyncMode());
        }
    }

    private void throwIfStatementForbidden(PreparedStatement paramPreparedStatement)
    {
        if ((this.mOnlyAllowReadOnlyOperations) && (!paramPreparedStatement.mReadOnly))
            throw new SQLiteException("Cannot execute this statement because it might modify the database but the connection is read-only.");
    }

    private static String trimSqlForDisplay(String paramString)
    {
        return TRIM_SQL_PATTERN.matcher(paramString).replaceAll(" ");
    }

    void close()
    {
        dispose(false);
    }

    // ERROR //
    void collectDbStats(ArrayList<SQLiteDebug.DbStats> paramArrayList)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 153	android/database/sqlite/SQLiteConnection:mConnectionPtr	I
        //     4: invokestatic 603	android/database/sqlite/SQLiteConnection:nativeGetDbLookaside	(I)I
        //     7: istore_2
        //     8: lconst_0
        //     9: lstore_3
        //     10: lconst_0
        //     11: lstore 5
        //     13: aload_0
        //     14: ldc_w 605
        //     17: aconst_null
        //     18: aconst_null
        //     19: invokevirtual 462	android/database/sqlite/SQLiteConnection:executeForLong	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
        //     22: lstore_3
        //     23: aload_0
        //     24: ldc_w 607
        //     27: aconst_null
        //     28: aconst_null
        //     29: invokevirtual 462	android/database/sqlite/SQLiteConnection:executeForLong	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
        //     32: lstore 26
        //     34: lload 26
        //     36: lstore 5
        //     38: aload_1
        //     39: aload_0
        //     40: iload_2
        //     41: lload_3
        //     42: lload 5
        //     44: invokespecial 609	android/database/sqlite/SQLiteConnection:getMainDbStatsUnsafe	(IJJ)Landroid/database/sqlite/SQLiteDebug$DbStats;
        //     47: invokevirtual 614	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     50: pop
        //     51: new 616	android/database/CursorWindow
        //     54: dup
        //     55: ldc_w 617
        //     58: invokespecial 618	android/database/CursorWindow:<init>	(Ljava/lang/String;)V
        //     61: astore 9
        //     63: aload_0
        //     64: ldc_w 620
        //     67: aconst_null
        //     68: aload 9
        //     70: iconst_0
        //     71: iconst_0
        //     72: iconst_0
        //     73: aconst_null
        //     74: invokevirtual 624	android/database/sqlite/SQLiteConnection:executeForCursorWindow	(Ljava/lang/String;[Ljava/lang/Object;Landroid/database/CursorWindow;IIZLandroid/os/CancellationSignal;)I
        //     77: pop
        //     78: iconst_1
        //     79: istore 13
        //     81: aload 9
        //     83: invokevirtual 627	android/database/CursorWindow:getNumRows	()I
        //     86: istore 14
        //     88: iload 13
        //     90: iload 14
        //     92: if_icmpge +202 -> 294
        //     95: aload 9
        //     97: iload 13
        //     99: iconst_1
        //     100: invokevirtual 630	android/database/CursorWindow:getString	(II)Ljava/lang/String;
        //     103: astore 15
        //     105: aload 9
        //     107: iload 13
        //     109: iconst_2
        //     110: invokevirtual 630	android/database/CursorWindow:getString	(II)Ljava/lang/String;
        //     113: astore 16
        //     115: lconst_0
        //     116: lstore 17
        //     118: lconst_0
        //     119: lstore 19
        //     121: aload_0
        //     122: new 237	java/lang/StringBuilder
        //     125: dup
        //     126: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     129: ldc_w 632
        //     132: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     135: aload 15
        //     137: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     140: ldc_w 634
        //     143: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     146: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     149: aconst_null
        //     150: aconst_null
        //     151: invokevirtual 462	android/database/sqlite/SQLiteConnection:executeForLong	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
        //     154: lstore 17
        //     156: aload_0
        //     157: new 237	java/lang/StringBuilder
        //     160: dup
        //     161: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     164: ldc_w 632
        //     167: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     170: aload 15
        //     172: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     175: ldc_w 636
        //     178: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     181: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     184: aconst_null
        //     185: aconst_null
        //     186: invokevirtual 462	android/database/sqlite/SQLiteConnection:executeForLong	(Ljava/lang/String;[Ljava/lang/Object;Landroid/os/CancellationSignal;)J
        //     189: lstore 24
        //     191: lload 24
        //     193: lstore 19
        //     195: new 237	java/lang/StringBuilder
        //     198: dup
        //     199: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     202: ldc_w 638
        //     205: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     208: aload 15
        //     210: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     213: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     216: astore 22
        //     218: aload 16
        //     220: invokevirtual 641	java/lang/String:isEmpty	()Z
        //     223: ifne +31 -> 254
        //     226: new 237	java/lang/StringBuilder
        //     229: dup
        //     230: invokespecial 238	java/lang/StringBuilder:<init>	()V
        //     233: aload 22
        //     235: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     238: ldc_w 643
        //     241: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     244: aload 16
        //     246: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     249: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     252: astore 22
        //     254: aload_1
        //     255: new 357	android/database/sqlite/SQLiteDebug$DbStats
        //     258: dup
        //     259: aload 22
        //     261: lload 17
        //     263: lload 19
        //     265: iconst_0
        //     266: iconst_0
        //     267: iconst_0
        //     268: iconst_0
        //     269: invokespecial 370	android/database/sqlite/SQLiteDebug$DbStats:<init>	(Ljava/lang/String;JJIIII)V
        //     272: invokevirtual 614	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     275: pop
        //     276: iinc 13 1
        //     279: goto -198 -> 81
        //     282: astore 11
        //     284: aload 9
        //     286: invokevirtual 644	android/database/CursorWindow:close	()V
        //     289: aload 11
        //     291: athrow
        //     292: astore 10
        //     294: aload 9
        //     296: invokevirtual 644	android/database/CursorWindow:close	()V
        //     299: return
        //     300: astore 21
        //     302: goto -107 -> 195
        //     305: astore 7
        //     307: goto -269 -> 38
        //
        // Exception table:
        //     from	to	target	type
        //     63	115	282	finally
        //     121	191	282	finally
        //     195	276	282	finally
        //     63	115	292	android/database/sqlite/SQLiteException
        //     195	276	292	android/database/sqlite/SQLiteException
        //     121	191	300	android/database/sqlite/SQLiteException
        //     13	34	305	android/database/sqlite/SQLiteException
    }

    void collectDbStatsUnsafe(ArrayList<SQLiteDebug.DbStats> paramArrayList)
    {
        paramArrayList.add(getMainDbStatsUnsafe(0, 0L, 0L));
    }

    String describeCurrentOperationUnsafe()
    {
        return this.mRecentOperations.describeCurrentOperation();
    }

    public void dump(Printer paramPrinter, boolean paramBoolean)
    {
        dumpUnsafe(paramPrinter, paramBoolean);
    }

    void dumpUnsafe(Printer paramPrinter, boolean paramBoolean)
    {
        paramPrinter.println("Connection #" + this.mConnectionId + ":");
        if (paramBoolean)
            paramPrinter.println("    connectionPtr: 0x" + Integer.toHexString(this.mConnectionPtr));
        paramPrinter.println("    isPrimaryConnection: " + this.mIsPrimaryConnection);
        paramPrinter.println("    onlyAllowReadOnlyOperations: " + this.mOnlyAllowReadOnlyOperations);
        this.mRecentOperations.dump(paramPrinter);
        if (paramBoolean)
            this.mPreparedStatementCache.dump(paramPrinter);
    }

    public void execute(String paramString, Object[] paramArrayOfObject, CancellationSignal paramCancellationSignal)
    {
        if (paramString == null)
            throw new IllegalArgumentException("sql must not be null.");
        int i = this.mRecentOperations.beginOperation("execute", paramString, paramArrayOfObject);
        try
        {
            PreparedStatement localPreparedStatement = acquirePreparedStatement(paramString);
            try
            {
                throwIfStatementForbidden(localPreparedStatement);
                bindArguments(localPreparedStatement, paramArrayOfObject);
                applyBlockGuardPolicy(localPreparedStatement);
                attachCancellationSignal(paramCancellationSignal);
                try
                {
                    nativeExecute(this.mConnectionPtr, localPreparedStatement.mStatementPtr);
                    detachCancellationSignal(paramCancellationSignal);
                    releasePreparedStatement(localPreparedStatement);
                    return;
                }
                finally
                {
                }
            }
            finally
            {
                releasePreparedStatement(localPreparedStatement);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            this.mRecentOperations.failOperation(i, localRuntimeException);
            throw localRuntimeException;
        }
        finally
        {
            this.mRecentOperations.endOperation(i);
        }
    }

    public ParcelFileDescriptor executeForBlobFileDescriptor(String paramString, Object[] paramArrayOfObject, CancellationSignal paramCancellationSignal)
    {
        if (paramString == null)
            throw new IllegalArgumentException("sql must not be null.");
        int i = this.mRecentOperations.beginOperation("executeForBlobFileDescriptor", paramString, paramArrayOfObject);
        try
        {
            PreparedStatement localPreparedStatement = acquirePreparedStatement(paramString);
            try
            {
                throwIfStatementForbidden(localPreparedStatement);
                bindArguments(localPreparedStatement, paramArrayOfObject);
                applyBlockGuardPolicy(localPreparedStatement);
                attachCancellationSignal(paramCancellationSignal);
            }
            finally
            {
                try
                {
                    int j = nativeExecuteForBlobFileDescriptor(this.mConnectionPtr, localPreparedStatement.mStatementPtr);
                    if (j >= 0)
                    {
                        ParcelFileDescriptor localParcelFileDescriptor2 = ParcelFileDescriptor.adoptFd(j);
                        localParcelFileDescriptor1 = localParcelFileDescriptor2;
                        detachCancellationSignal(paramCancellationSignal);
                        releasePreparedStatement(localPreparedStatement);
                        return localParcelFileDescriptor1;
                    }
                    ParcelFileDescriptor localParcelFileDescriptor1 = null;
                }
                finally
                {
                    detachCancellationSignal(paramCancellationSignal);
                }
                releasePreparedStatement(localPreparedStatement);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            this.mRecentOperations.failOperation(i, localRuntimeException);
            throw localRuntimeException;
        }
        finally
        {
            this.mRecentOperations.endOperation(i);
        }
    }

    public int executeForChangedRowCount(String paramString, Object[] paramArrayOfObject, CancellationSignal paramCancellationSignal)
    {
        if (paramString == null)
            throw new IllegalArgumentException("sql must not be null.");
        int i = 0;
        int j = this.mRecentOperations.beginOperation("executeForChangedRowCount", paramString, paramArrayOfObject);
        try
        {
            PreparedStatement localPreparedStatement = acquirePreparedStatement(paramString);
            try
            {
                throwIfStatementForbidden(localPreparedStatement);
                bindArguments(localPreparedStatement, paramArrayOfObject);
                applyBlockGuardPolicy(localPreparedStatement);
                attachCancellationSignal(paramCancellationSignal);
                try
                {
                    int k = nativeExecuteForChangedRowCount(this.mConnectionPtr, localPreparedStatement.mStatementPtr);
                    i = k;
                    detachCancellationSignal(paramCancellationSignal);
                    releasePreparedStatement(localPreparedStatement);
                    return i;
                }
                finally
                {
                }
            }
            finally
            {
                releasePreparedStatement(localPreparedStatement);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            this.mRecentOperations.failOperation(j, localRuntimeException);
            throw localRuntimeException;
        }
        finally
        {
            if (this.mRecentOperations.endOperationDeferLog(j))
                this.mRecentOperations.logOperation(j, "changedRows=" + i);
        }
    }

    public int executeForCursorWindow(String paramString, Object[] paramArrayOfObject, CursorWindow paramCursorWindow, int paramInt1, int paramInt2, boolean paramBoolean, CancellationSignal paramCancellationSignal)
    {
        if (paramString == null)
            throw new IllegalArgumentException("sql must not be null.");
        if (paramCursorWindow == null)
            throw new IllegalArgumentException("window must not be null.");
        paramCursorWindow.acquireReference();
        int i = -1;
        int j = -1;
        int k = -1;
        try
        {
            int m = this.mRecentOperations.beginOperation("executeForCursorWindow", paramString, paramArrayOfObject);
            try
            {
                PreparedStatement localPreparedStatement = acquirePreparedStatement(paramString);
                try
                {
                    throwIfStatementForbidden(localPreparedStatement);
                    bindArguments(localPreparedStatement, paramArrayOfObject);
                    applyBlockGuardPolicy(localPreparedStatement);
                    attachCancellationSignal(paramCancellationSignal);
                    try
                    {
                        long l = nativeExecuteForCursorWindow(this.mConnectionPtr, localPreparedStatement.mStatementPtr, paramCursorWindow.mWindowPtr, paramInt1, paramInt2, paramBoolean);
                        i = (int)(l >> 32);
                        j = (int)l;
                        k = paramCursorWindow.getNumRows();
                        paramCursorWindow.setStartPosition(i);
                        detachCancellationSignal(paramCancellationSignal);
                        releasePreparedStatement(localPreparedStatement);
                        if (this.mRecentOperations.endOperationDeferLog(m))
                            this.mRecentOperations.logOperation(m, "window='" + paramCursorWindow + "', startPos=" + paramInt1 + ", actualPos=" + i + ", filledRows=" + k + ", countedRows=" + j);
                        return j;
                    }
                    finally
                    {
                    }
                }
                finally
                {
                    releasePreparedStatement(localPreparedStatement);
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                this.mRecentOperations.failOperation(m, localRuntimeException);
                throw localRuntimeException;
            }
            finally
            {
                if (this.mRecentOperations.endOperationDeferLog(m))
                    this.mRecentOperations.logOperation(m, "window='" + paramCursorWindow + "', startPos=" + paramInt1 + ", actualPos=" + i + ", filledRows=" + k + ", countedRows=" + j);
            }
        }
        finally
        {
            paramCursorWindow.releaseReference();
        }
    }

    public long executeForLastInsertedRowId(String paramString, Object[] paramArrayOfObject, CancellationSignal paramCancellationSignal)
    {
        if (paramString == null)
            throw new IllegalArgumentException("sql must not be null.");
        int i = this.mRecentOperations.beginOperation("executeForLastInsertedRowId", paramString, paramArrayOfObject);
        try
        {
            PreparedStatement localPreparedStatement = acquirePreparedStatement(paramString);
            try
            {
                throwIfStatementForbidden(localPreparedStatement);
                bindArguments(localPreparedStatement, paramArrayOfObject);
                applyBlockGuardPolicy(localPreparedStatement);
                attachCancellationSignal(paramCancellationSignal);
                try
                {
                    long l = nativeExecuteForLastInsertedRowId(this.mConnectionPtr, localPreparedStatement.mStatementPtr);
                    detachCancellationSignal(paramCancellationSignal);
                    releasePreparedStatement(localPreparedStatement);
                    return l;
                }
                finally
                {
                }
            }
            finally
            {
                releasePreparedStatement(localPreparedStatement);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            this.mRecentOperations.failOperation(i, localRuntimeException);
            throw localRuntimeException;
        }
        finally
        {
            this.mRecentOperations.endOperation(i);
        }
    }

    public long executeForLong(String paramString, Object[] paramArrayOfObject, CancellationSignal paramCancellationSignal)
    {
        if (paramString == null)
            throw new IllegalArgumentException("sql must not be null.");
        int i = this.mRecentOperations.beginOperation("executeForLong", paramString, paramArrayOfObject);
        try
        {
            PreparedStatement localPreparedStatement = acquirePreparedStatement(paramString);
            try
            {
                throwIfStatementForbidden(localPreparedStatement);
                bindArguments(localPreparedStatement, paramArrayOfObject);
                applyBlockGuardPolicy(localPreparedStatement);
                attachCancellationSignal(paramCancellationSignal);
                try
                {
                    long l = nativeExecuteForLong(this.mConnectionPtr, localPreparedStatement.mStatementPtr);
                    detachCancellationSignal(paramCancellationSignal);
                    releasePreparedStatement(localPreparedStatement);
                    return l;
                }
                finally
                {
                }
            }
            finally
            {
                releasePreparedStatement(localPreparedStatement);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            this.mRecentOperations.failOperation(i, localRuntimeException);
            throw localRuntimeException;
        }
        finally
        {
            this.mRecentOperations.endOperation(i);
        }
    }

    public String executeForString(String paramString, Object[] paramArrayOfObject, CancellationSignal paramCancellationSignal)
    {
        if (paramString == null)
            throw new IllegalArgumentException("sql must not be null.");
        int i = this.mRecentOperations.beginOperation("executeForString", paramString, paramArrayOfObject);
        try
        {
            PreparedStatement localPreparedStatement = acquirePreparedStatement(paramString);
            try
            {
                throwIfStatementForbidden(localPreparedStatement);
                bindArguments(localPreparedStatement, paramArrayOfObject);
                applyBlockGuardPolicy(localPreparedStatement);
                attachCancellationSignal(paramCancellationSignal);
                try
                {
                    String str = nativeExecuteForString(this.mConnectionPtr, localPreparedStatement.mStatementPtr);
                    detachCancellationSignal(paramCancellationSignal);
                    releasePreparedStatement(localPreparedStatement);
                    return str;
                }
                finally
                {
                }
            }
            finally
            {
                releasePreparedStatement(localPreparedStatement);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            this.mRecentOperations.failOperation(i, localRuntimeException);
            throw localRuntimeException;
        }
        finally
        {
            this.mRecentOperations.endOperation(i);
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if ((this.mPool != null) && (this.mConnectionPtr != 0))
                this.mPool.onConnectionLeaked();
            dispose(true);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int getConnectionId()
    {
        return this.mConnectionId;
    }

    boolean isPreparedStatementInCache(String paramString)
    {
        if (this.mPreparedStatementCache.get(paramString) != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPrimaryConnection()
    {
        return this.mIsPrimaryConnection;
    }

    public void onCancel()
    {
        nativeCancel(this.mConnectionPtr);
    }

    public void prepare(String paramString, SQLiteStatementInfo paramSQLiteStatementInfo)
    {
        if (paramString == null)
            throw new IllegalArgumentException("sql must not be null.");
        int i = this.mRecentOperations.beginOperation("prepare", paramString, null);
        try
        {
            PreparedStatement localPreparedStatement = acquirePreparedStatement(paramString);
            if (paramSQLiteStatementInfo != null);
            try
            {
                paramSQLiteStatementInfo.numParameters = localPreparedStatement.mNumParameters;
                paramSQLiteStatementInfo.readOnly = localPreparedStatement.mReadOnly;
                int j = nativeGetColumnCount(this.mConnectionPtr, localPreparedStatement.mStatementPtr);
                if (j == 0)
                    paramSQLiteStatementInfo.columnNames = EMPTY_STRING_ARRAY;
                while (true)
                {
                    releasePreparedStatement(localPreparedStatement);
                    return;
                    paramSQLiteStatementInfo.columnNames = new String[j];
                    for (int k = 0; k < j; k++)
                        paramSQLiteStatementInfo.columnNames[k] = nativeGetColumnName(this.mConnectionPtr, localPreparedStatement.mStatementPtr, k);
                }
            }
            finally
            {
                releasePreparedStatement(localPreparedStatement);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            this.mRecentOperations.failOperation(i, localRuntimeException);
            throw localRuntimeException;
        }
        finally
        {
            this.mRecentOperations.endOperation(i);
        }
    }

    void reconfigure(SQLiteDatabaseConfiguration paramSQLiteDatabaseConfiguration)
    {
        this.mOnlyAllowReadOnlyOperations = false;
        int i = paramSQLiteDatabaseConfiguration.customFunctions.size();
        for (int j = 0; j < i; j++)
        {
            SQLiteCustomFunction localSQLiteCustomFunction = (SQLiteCustomFunction)paramSQLiteDatabaseConfiguration.customFunctions.get(j);
            if (!this.mConfiguration.customFunctions.contains(localSQLiteCustomFunction))
                nativeRegisterCustomFunction(this.mConnectionPtr, localSQLiteCustomFunction);
        }
        int k;
        int m;
        if (paramSQLiteDatabaseConfiguration.foreignKeyConstraintsEnabled != this.mConfiguration.foreignKeyConstraintsEnabled)
        {
            k = 1;
            if ((0x20000000 & (paramSQLiteDatabaseConfiguration.openFlags ^ this.mConfiguration.openFlags)) == 0)
                break label175;
            m = 1;
            label102: if (paramSQLiteDatabaseConfiguration.locale.equals(this.mConfiguration.locale))
                break label181;
        }
        label175: label181: for (int n = 1; ; n = 0)
        {
            this.mConfiguration.updateParametersFrom(paramSQLiteDatabaseConfiguration);
            this.mPreparedStatementCache.resize(paramSQLiteDatabaseConfiguration.maxSqlCacheSize);
            if (k != 0)
                setForeignKeyModeFromConfiguration();
            if (m != 0)
                setWalModeFromConfiguration();
            if (n != 0)
                setLocaleFromConfiguration();
            return;
            k = 0;
            break;
            m = 0;
            break label102;
        }
    }

    void setOnlyAllowReadOnlyOperations(boolean paramBoolean)
    {
        this.mOnlyAllowReadOnlyOperations = paramBoolean;
    }

    public String toString()
    {
        return "SQLiteConnection: " + this.mConfiguration.path + " (" + this.mConnectionId + ")";
    }

    private static final class Operation
    {
        private static final SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        public ArrayList<Object> mBindArgs;
        public int mCookie;
        public long mEndTime;
        public Exception mException;
        public boolean mFinished;
        public String mKind;
        public String mSql;
        public long mStartTime;

        private String getFormattedStartTime()
        {
            return sDateFormat.format(new Date(this.mStartTime));
        }

        private String getStatus()
        {
            String str;
            if (!this.mFinished)
                str = "running";
            while (true)
            {
                return str;
                if (this.mException != null)
                    str = "failed";
                else
                    str = "succeeded";
            }
        }

        public void describe(StringBuilder paramStringBuilder)
        {
            paramStringBuilder.append(this.mKind);
            int j;
            label119: Object localObject;
            if (this.mFinished)
            {
                paramStringBuilder.append(" took ").append(this.mEndTime - this.mStartTime).append("ms");
                paramStringBuilder.append(" - ").append(getStatus());
                if (this.mSql != null)
                    paramStringBuilder.append(", sql=\"").append(SQLiteConnection.trimSqlForDisplay(this.mSql)).append("\"");
                if ((this.mBindArgs == null) || (this.mBindArgs.size() == 0))
                    break label259;
                paramStringBuilder.append(", bindArgs=[");
                int i = this.mBindArgs.size();
                j = 0;
                if (j >= i)
                    break label252;
                localObject = this.mBindArgs.get(j);
                if (j != 0)
                    paramStringBuilder.append(", ");
                if (localObject != null)
                    break label193;
                paramStringBuilder.append("null");
            }
            while (true)
            {
                j++;
                break label119;
                paramStringBuilder.append(" started ").append(System.currentTimeMillis() - this.mStartTime).append("ms ago");
                break;
                label193: if ((localObject instanceof byte[]))
                    paramStringBuilder.append("<byte[]>");
                else if ((localObject instanceof String))
                    paramStringBuilder.append("\"").append((String)localObject).append("\"");
                else
                    paramStringBuilder.append(localObject);
            }
            label252: paramStringBuilder.append("]");
            label259: if (this.mException != null)
                paramStringBuilder.append(", exception=\"").append(this.mException.getMessage()).append("\"");
        }
    }

    private static final class OperationLog
    {
        private static final int COOKIE_GENERATION_SHIFT = 8;
        private static final int COOKIE_INDEX_MASK = 255;
        private static final int MAX_RECENT_OPERATIONS = 20;
        private int mGeneration;
        private int mIndex;
        private final SQLiteConnection.Operation[] mOperations = new SQLiteConnection.Operation[20];

        private boolean endOperationDeferLogLocked(int paramInt)
        {
            boolean bool = true;
            SQLiteConnection.Operation localOperation = getOperationLocked(paramInt);
            if (localOperation != null)
            {
                localOperation.mEndTime = System.currentTimeMillis();
                localOperation.mFinished = bool;
                if ((!SQLiteDebug.DEBUG_LOG_SLOW_QUERIES) || (!SQLiteDebug.shouldLogSlowQuery(localOperation.mEndTime - localOperation.mStartTime)));
            }
            while (true)
            {
                return bool;
                bool = false;
                continue;
                bool = false;
            }
        }

        private SQLiteConnection.Operation getOperationLocked(int paramInt)
        {
            int i = paramInt & 0xFF;
            SQLiteConnection.Operation localOperation = this.mOperations[i];
            if (localOperation.mCookie == paramInt);
            while (true)
            {
                return localOperation;
                localOperation = null;
            }
        }

        private void logOperationLocked(int paramInt, String paramString)
        {
            SQLiteConnection.Operation localOperation = getOperationLocked(paramInt);
            StringBuilder localStringBuilder = new StringBuilder();
            localOperation.describe(localStringBuilder);
            if (paramString != null)
                localStringBuilder.append(", ").append(paramString);
            Log.d("SQLiteConnection", localStringBuilder.toString());
        }

        private int newOperationCookieLocked(int paramInt)
        {
            int i = this.mGeneration;
            this.mGeneration = (i + 1);
            return paramInt | i << 8;
        }

        // ERROR //
        public int beginOperation(String paramString1, String paramString2, Object[] paramArrayOfObject)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 26	android/database/sqlite/SQLiteConnection$OperationLog:mOperations	[Landroid/database/sqlite/SQLiteConnection$Operation;
            //     4: astore 4
            //     6: aload 4
            //     8: monitorenter
            //     9: iconst_1
            //     10: aload_0
            //     11: getfield 98	android/database/sqlite/SQLiteConnection$OperationLog:mIndex	I
            //     14: iadd
            //     15: bipush 20
            //     17: irem
            //     18: istore 6
            //     20: aload_0
            //     21: getfield 26	android/database/sqlite/SQLiteConnection$OperationLog:mOperations	[Landroid/database/sqlite/SQLiteConnection$Operation;
            //     24: iload 6
            //     26: aaload
            //     27: astore 7
            //     29: aload 7
            //     31: ifnonnull +110 -> 141
            //     34: new 24	android/database/sqlite/SQLiteConnection$Operation
            //     37: dup
            //     38: aconst_null
            //     39: invokespecial 100	android/database/sqlite/SQLiteConnection$Operation:<init>	(Landroid/database/sqlite/SQLiteConnection$1;)V
            //     42: astore 7
            //     44: aload_0
            //     45: getfield 26	android/database/sqlite/SQLiteConnection$OperationLog:mOperations	[Landroid/database/sqlite/SQLiteConnection$Operation;
            //     48: iload 6
            //     50: aload 7
            //     52: aastore
            //     53: aload 7
            //     55: invokestatic 40	java/lang/System:currentTimeMillis	()J
            //     58: putfield 56	android/database/sqlite/SQLiteConnection$Operation:mStartTime	J
            //     61: aload 7
            //     63: aload_1
            //     64: putfield 104	android/database/sqlite/SQLiteConnection$Operation:mKind	Ljava/lang/String;
            //     67: aload 7
            //     69: aload_2
            //     70: putfield 107	android/database/sqlite/SQLiteConnection$Operation:mSql	Ljava/lang/String;
            //     73: aload_3
            //     74: ifnull +131 -> 205
            //     77: aload 7
            //     79: getfield 111	android/database/sqlite/SQLiteConnection$Operation:mBindArgs	Ljava/util/ArrayList;
            //     82: ifnonnull +98 -> 180
            //     85: aload 7
            //     87: new 113	java/util/ArrayList
            //     90: dup
            //     91: invokespecial 114	java/util/ArrayList:<init>	()V
            //     94: putfield 111	android/database/sqlite/SQLiteConnection$Operation:mBindArgs	Ljava/util/ArrayList;
            //     97: goto +138 -> 235
            //     100: iload 9
            //     102: aload_3
            //     103: arraylength
            //     104: if_icmpge +101 -> 205
            //     107: aload_3
            //     108: iload 9
            //     110: aaload
            //     111: astore 10
            //     113: aload 10
            //     115: ifnull +76 -> 191
            //     118: aload 10
            //     120: instanceof 116
            //     123: ifeq +68 -> 191
            //     126: aload 7
            //     128: getfield 111	android/database/sqlite/SQLiteConnection$Operation:mBindArgs	Ljava/util/ArrayList;
            //     131: invokestatic 120	android/database/sqlite/SQLiteConnection:access$500	()[B
            //     134: invokevirtual 124	java/util/ArrayList:add	(Ljava/lang/Object;)Z
            //     137: pop
            //     138: goto +103 -> 241
            //     141: aload 7
            //     143: iconst_0
            //     144: putfield 48	android/database/sqlite/SQLiteConnection$Operation:mFinished	Z
            //     147: aload 7
            //     149: aconst_null
            //     150: putfield 128	android/database/sqlite/SQLiteConnection$Operation:mException	Ljava/lang/Exception;
            //     153: aload 7
            //     155: getfield 111	android/database/sqlite/SQLiteConnection$Operation:mBindArgs	Ljava/util/ArrayList;
            //     158: ifnull -105 -> 53
            //     161: aload 7
            //     163: getfield 111	android/database/sqlite/SQLiteConnection$Operation:mBindArgs	Ljava/util/ArrayList;
            //     166: invokevirtual 131	java/util/ArrayList:clear	()V
            //     169: goto -116 -> 53
            //     172: astore 5
            //     174: aload 4
            //     176: monitorexit
            //     177: aload 5
            //     179: athrow
            //     180: aload 7
            //     182: getfield 111	android/database/sqlite/SQLiteConnection$Operation:mBindArgs	Ljava/util/ArrayList;
            //     185: invokevirtual 131	java/util/ArrayList:clear	()V
            //     188: goto +47 -> 235
            //     191: aload 7
            //     193: getfield 111	android/database/sqlite/SQLiteConnection$Operation:mBindArgs	Ljava/util/ArrayList;
            //     196: aload 10
            //     198: invokevirtual 124	java/util/ArrayList:add	(Ljava/lang/Object;)Z
            //     201: pop
            //     202: goto +39 -> 241
            //     205: aload 7
            //     207: aload_0
            //     208: iload 6
            //     210: invokespecial 133	android/database/sqlite/SQLiteConnection$OperationLog:newOperationCookieLocked	(I)I
            //     213: putfield 63	android/database/sqlite/SQLiteConnection$Operation:mCookie	I
            //     216: aload_0
            //     217: iload 6
            //     219: putfield 98	android/database/sqlite/SQLiteConnection$OperationLog:mIndex	I
            //     222: aload 7
            //     224: getfield 63	android/database/sqlite/SQLiteConnection$Operation:mCookie	I
            //     227: istore 8
            //     229: aload 4
            //     231: monitorexit
            //     232: iload 8
            //     234: ireturn
            //     235: iconst_0
            //     236: istore 9
            //     238: goto -138 -> 100
            //     241: iinc 9 1
            //     244: goto -144 -> 100
            //
            // Exception table:
            //     from	to	target	type
            //     9	177	172	finally
            //     180	232	172	finally
        }

        public String describeCurrentOperation()
        {
            String str;
            synchronized (this.mOperations)
            {
                SQLiteConnection.Operation localOperation = this.mOperations[this.mIndex];
                if ((localOperation != null) && (!localOperation.mFinished))
                {
                    StringBuilder localStringBuilder = new StringBuilder();
                    localOperation.describe(localStringBuilder);
                    str = localStringBuilder.toString();
                }
                else
                {
                    str = null;
                }
            }
            return str;
        }

        public void dump(Printer paramPrinter)
        {
            while (true)
            {
                int i;
                synchronized (this.mOperations)
                {
                    paramPrinter.println("    Most recently executed operations:");
                    i = this.mIndex;
                    SQLiteConnection.Operation localOperation = this.mOperations[i];
                    if (localOperation != null)
                    {
                        int j = 0;
                        StringBuilder localStringBuilder = new StringBuilder();
                        localStringBuilder.append("        ").append(j).append(": [");
                        localStringBuilder.append(localOperation.getFormattedStartTime());
                        localStringBuilder.append("] ");
                        localOperation.describe(localStringBuilder);
                        paramPrinter.println(localStringBuilder.toString());
                        if (i > 0)
                        {
                            i--;
                            j++;
                            localOperation = this.mOperations[i];
                            if ((localOperation != null) && (j < 20))
                                continue;
                        }
                    }
                    else
                    {
                        paramPrinter.println("        <none>");
                    }
                }
            }
        }

        public void endOperation(int paramInt)
        {
            synchronized (this.mOperations)
            {
                if (endOperationDeferLogLocked(paramInt))
                    logOperationLocked(paramInt, null);
                return;
            }
        }

        public boolean endOperationDeferLog(int paramInt)
        {
            synchronized (this.mOperations)
            {
                boolean bool = endOperationDeferLogLocked(paramInt);
                return bool;
            }
        }

        public void failOperation(int paramInt, Exception paramException)
        {
            synchronized (this.mOperations)
            {
                SQLiteConnection.Operation localOperation = getOperationLocked(paramInt);
                if (localOperation != null)
                    localOperation.mException = paramException;
                return;
            }
        }

        public void logOperation(int paramInt, String paramString)
        {
            synchronized (this.mOperations)
            {
                logOperationLocked(paramInt, paramString);
                return;
            }
        }
    }

    private final class PreparedStatementCache extends LruCache<String, SQLiteConnection.PreparedStatement>
    {
        public PreparedStatementCache(int arg2)
        {
            super();
        }

        public void dump(Printer paramPrinter)
        {
            paramPrinter.println("    Prepared statement cache:");
            Map localMap = snapshot();
            if (!localMap.isEmpty())
            {
                int i = 0;
                Iterator localIterator = localMap.entrySet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    SQLiteConnection.PreparedStatement localPreparedStatement = (SQLiteConnection.PreparedStatement)localEntry.getValue();
                    if (localPreparedStatement.mInCache)
                    {
                        String str = (String)localEntry.getKey();
                        paramPrinter.println("        " + i + ": statementPtr=0x" + Integer.toHexString(localPreparedStatement.mStatementPtr) + ", numParameters=" + localPreparedStatement.mNumParameters + ", type=" + localPreparedStatement.mType + ", readOnly=" + localPreparedStatement.mReadOnly + ", sql=\"" + SQLiteConnection.trimSqlForDisplay(str) + "\"");
                    }
                    i++;
                }
            }
            paramPrinter.println("        <none>");
        }

        protected void entryRemoved(boolean paramBoolean, String paramString, SQLiteConnection.PreparedStatement paramPreparedStatement1, SQLiteConnection.PreparedStatement paramPreparedStatement2)
        {
            paramPreparedStatement1.mInCache = false;
            if (!paramPreparedStatement1.mInUse)
                SQLiteConnection.this.finalizePreparedStatement(paramPreparedStatement1);
        }
    }

    private static final class PreparedStatement
    {
        public boolean mInCache;
        public boolean mInUse;
        public int mNumParameters;
        public PreparedStatement mPoolNext;
        public boolean mReadOnly;
        public String mSql;
        public int mStatementPtr;
        public int mType;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteConnection
 * JD-Core Version:        0.6.2
 */