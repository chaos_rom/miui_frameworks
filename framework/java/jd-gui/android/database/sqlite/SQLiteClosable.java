package android.database.sqlite;

import java.io.Closeable;

public abstract class SQLiteClosable
    implements Closeable
{
    private int mReferenceCount = 1;

    // ERROR //
    public void acquireReference()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 14	android/database/sqlite/SQLiteClosable:mReferenceCount	I
        //     6: ifgt +35 -> 41
        //     9: new 17	java/lang/IllegalStateException
        //     12: dup
        //     13: new 19	java/lang/StringBuilder
        //     16: dup
        //     17: invokespecial 20	java/lang/StringBuilder:<init>	()V
        //     20: ldc 22
        //     22: invokevirtual 26	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     25: aload_0
        //     26: invokevirtual 29	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     29: invokevirtual 33	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     32: invokespecial 36	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     35: athrow
        //     36: astore_1
        //     37: aload_0
        //     38: monitorexit
        //     39: aload_1
        //     40: athrow
        //     41: aload_0
        //     42: iconst_1
        //     43: aload_0
        //     44: getfield 14	android/database/sqlite/SQLiteClosable:mReferenceCount	I
        //     47: iadd
        //     48: putfield 14	android/database/sqlite/SQLiteClosable:mReferenceCount	I
        //     51: aload_0
        //     52: monitorexit
        //     53: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	39	36	finally
        //     41	53	36	finally
    }

    public void close()
    {
        releaseReference();
    }

    protected abstract void onAllReferencesReleased();

    @Deprecated
    protected void onAllReferencesReleasedFromContainer()
    {
        onAllReferencesReleased();
    }

    public void releaseReference()
    {
        try
        {
            int i = -1 + this.mReferenceCount;
            this.mReferenceCount = i;
            if (i == 0);
            for (int j = 1; ; j = 0)
            {
                if (j != 0)
                    onAllReferencesReleased();
                return;
            }
        }
        finally
        {
        }
    }

    @Deprecated
    public void releaseReferenceFromContainer()
    {
        try
        {
            int i = -1 + this.mReferenceCount;
            this.mReferenceCount = i;
            if (i == 0);
            for (int j = 1; ; j = 0)
            {
                if (j != 0)
                    onAllReferencesReleasedFromContainer();
                return;
            }
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteClosable
 * JD-Core Version:        0.6.2
 */