package android.database.sqlite;

public final class SQLiteStatementInfo
{
    public String[] columnNames;
    public int numParameters;
    public boolean readOnly;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteStatementInfo
 * JD-Core Version:        0.6.2
 */