package android.database.sqlite;

public class DatabaseObjectNotClosedException extends RuntimeException
{
    private static final String s = "Application did not close the cursor or database object that was opened here";

    public DatabaseObjectNotClosedException()
    {
        super("Application did not close the cursor or database object that was opened here");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.DatabaseObjectNotClosedException
 * JD-Core Version:        0.6.2
 */