package android.database.sqlite;

public class SQLiteCantOpenDatabaseException extends SQLiteException
{
    public SQLiteCantOpenDatabaseException()
    {
    }

    public SQLiteCantOpenDatabaseException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteCantOpenDatabaseException
 * JD-Core Version:        0.6.2
 */