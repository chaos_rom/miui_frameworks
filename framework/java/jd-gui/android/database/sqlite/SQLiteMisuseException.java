package android.database.sqlite;

public class SQLiteMisuseException extends SQLiteException
{
    public SQLiteMisuseException()
    {
    }

    public SQLiteMisuseException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteMisuseException
 * JD-Core Version:        0.6.2
 */