package android.database.sqlite;

import android.database.Cursor;
import android.os.CancellationSignal;

public final class SQLiteDirectCursorDriver
    implements SQLiteCursorDriver
{
    private final CancellationSignal mCancellationSignal;
    private final SQLiteDatabase mDatabase;
    private final String mEditTable;
    private SQLiteQuery mQuery;
    private final String mSql;

    public SQLiteDirectCursorDriver(SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2, CancellationSignal paramCancellationSignal)
    {
        this.mDatabase = paramSQLiteDatabase;
        this.mEditTable = paramString2;
        this.mSql = paramString1;
        this.mCancellationSignal = paramCancellationSignal;
    }

    public void cursorClosed()
    {
    }

    public void cursorDeactivated()
    {
    }

    public void cursorRequeried(Cursor paramCursor)
    {
    }

    public Cursor query(SQLiteDatabase.CursorFactory paramCursorFactory, String[] paramArrayOfString)
    {
        SQLiteQuery localSQLiteQuery = new SQLiteQuery(this.mDatabase, this.mSql, this.mCancellationSignal);
        try
        {
            localSQLiteQuery.bindAllArgsAsStrings(paramArrayOfString);
            if (paramCursorFactory == null);
            Cursor localCursor;
            for (Object localObject = new SQLiteCursor(this, this.mEditTable, localSQLiteQuery); ; localObject = localCursor)
            {
                this.mQuery = localSQLiteQuery;
                return localObject;
                localCursor = paramCursorFactory.newCursor(this.mDatabase, this, this.mEditTable, localSQLiteQuery);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            localSQLiteQuery.close();
            throw localRuntimeException;
        }
    }

    public void setBindArguments(String[] paramArrayOfString)
    {
        this.mQuery.bindAllArgsAsStrings(paramArrayOfString);
    }

    public String toString()
    {
        return "SQLiteDirectCursorDriver: " + this.mSql;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteDirectCursorDriver
 * JD-Core Version:        0.6.2
 */