package android.database.sqlite;

import android.database.SQLException;

public class SQLiteException extends SQLException
{
    public SQLiteException()
    {
    }

    public SQLiteException(String paramString)
    {
        super(paramString);
    }

    public SQLiteException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteException
 * JD-Core Version:        0.6.2
 */