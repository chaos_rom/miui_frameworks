package android.database.sqlite;

import android.content.res.Resources;
import android.os.StatFs;
import android.os.SystemProperties;

public final class SQLiteGlobal
{
    private static final String TAG = "SQLiteGlobal";
    private static int sDefaultPageSize;
    private static final Object sLock = new Object();

    public static String getDefaultJournalMode()
    {
        return SystemProperties.get("debug.sqlite.journalmode", Resources.getSystem().getString(17039393));
    }

    public static int getDefaultPageSize()
    {
        synchronized (sLock)
        {
            if (sDefaultPageSize == 0)
                sDefaultPageSize = new StatFs("/data").getBlockSize();
            int i = SystemProperties.getInt("debug.sqlite.pagesize", sDefaultPageSize);
            return i;
        }
    }

    public static String getDefaultSyncMode()
    {
        return SystemProperties.get("debug.sqlite.syncmode", Resources.getSystem().getString(17039394));
    }

    public static int getJournalSizeLimit()
    {
        return SystemProperties.getInt("debug.sqlite.journalsizelimit", Resources.getSystem().getInteger(17694762));
    }

    public static int getWALAutoCheckpoint()
    {
        return Math.max(1, SystemProperties.getInt("debug.sqlite.wal.autocheckpoint", Resources.getSystem().getInteger(17694763)));
    }

    public static int getWALConnectionPoolSize()
    {
        return Math.max(2, SystemProperties.getInt("debug.sqlite.wal.poolsize", Resources.getSystem().getInteger(17694761)));
    }

    public static String getWALSyncMode()
    {
        return SystemProperties.get("debug.sqlite.wal.syncmode", Resources.getSystem().getString(17039395));
    }

    private static native int nativeReleaseMemory();

    public static int releaseMemory()
    {
        return nativeReleaseMemory();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.sqlite.SQLiteGlobal
 * JD-Core Version:        0.6.2
 */