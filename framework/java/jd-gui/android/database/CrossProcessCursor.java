package android.database;

public abstract interface CrossProcessCursor extends Cursor
{
    public abstract void fillWindow(int paramInt, CursorWindow paramCursorWindow);

    public abstract CursorWindow getWindow();

    public abstract boolean onMove(int paramInt1, int paramInt2);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.CrossProcessCursor
 * JD-Core Version:        0.6.2
 */