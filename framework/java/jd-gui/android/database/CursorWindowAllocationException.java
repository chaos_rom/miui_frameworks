package android.database;

public class CursorWindowAllocationException extends RuntimeException
{
    public CursorWindowAllocationException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.CursorWindowAllocationException
 * JD-Core Version:        0.6.2
 */