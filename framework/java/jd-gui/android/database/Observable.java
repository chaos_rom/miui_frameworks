package android.database;

import java.util.ArrayList;

public abstract class Observable<T>
{
    protected final ArrayList<T> mObservers = new ArrayList();

    // ERROR //
    public void registerObserver(T paramT)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +13 -> 14
        //     4: new 21	java/lang/IllegalArgumentException
        //     7: dup
        //     8: ldc 23
        //     10: invokespecial 26	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     13: athrow
        //     14: aload_0
        //     15: getfield 17	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     18: astore_2
        //     19: aload_2
        //     20: monitorenter
        //     21: aload_0
        //     22: getfield 17	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     25: aload_1
        //     26: invokevirtual 30	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
        //     29: ifeq +40 -> 69
        //     32: new 32	java/lang/IllegalStateException
        //     35: dup
        //     36: new 34	java/lang/StringBuilder
        //     39: dup
        //     40: invokespecial 35	java/lang/StringBuilder:<init>	()V
        //     43: ldc 37
        //     45: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     48: aload_1
        //     49: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     52: ldc 46
        //     54: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokespecial 51	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     63: athrow
        //     64: astore_3
        //     65: aload_2
        //     66: monitorexit
        //     67: aload_3
        //     68: athrow
        //     69: aload_0
        //     70: getfield 17	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     73: aload_1
        //     74: invokevirtual 54	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     77: pop
        //     78: aload_2
        //     79: monitorexit
        //     80: return
        //
        // Exception table:
        //     from	to	target	type
        //     21	67	64	finally
        //     69	80	64	finally
    }

    public void unregisterAll()
    {
        synchronized (this.mObservers)
        {
            this.mObservers.clear();
            return;
        }
    }

    // ERROR //
    public void unregisterObserver(T paramT)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +13 -> 14
        //     4: new 21	java/lang/IllegalArgumentException
        //     7: dup
        //     8: ldc 23
        //     10: invokespecial 26	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     13: athrow
        //     14: aload_0
        //     15: getfield 17	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     18: astore_2
        //     19: aload_2
        //     20: monitorenter
        //     21: aload_0
        //     22: getfield 17	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     25: aload_1
        //     26: invokevirtual 63	java/util/ArrayList:indexOf	(Ljava/lang/Object;)I
        //     29: istore 4
        //     31: iload 4
        //     33: bipush 255
        //     35: if_icmpne +40 -> 75
        //     38: new 32	java/lang/IllegalStateException
        //     41: dup
        //     42: new 34	java/lang/StringBuilder
        //     45: dup
        //     46: invokespecial 35	java/lang/StringBuilder:<init>	()V
        //     49: ldc 37
        //     51: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     54: aload_1
        //     55: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     58: ldc 65
        //     60: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     63: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     66: invokespecial 51	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     69: athrow
        //     70: astore_3
        //     71: aload_2
        //     72: monitorexit
        //     73: aload_3
        //     74: athrow
        //     75: aload_0
        //     76: getfield 17	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     79: iload 4
        //     81: invokevirtual 69	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     84: pop
        //     85: aload_2
        //     86: monitorexit
        //     87: return
        //
        // Exception table:
        //     from	to	target	type
        //     21	73	70	finally
        //     75	87	70	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.Observable
 * JD-Core Version:        0.6.2
 */