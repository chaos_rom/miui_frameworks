package android.database;

public class CrossProcessCursorWrapper extends CursorWrapper
    implements CrossProcessCursor
{
    public CrossProcessCursorWrapper(Cursor paramCursor)
    {
        super(paramCursor);
    }

    public void fillWindow(int paramInt, CursorWindow paramCursorWindow)
    {
        if ((this.mCursor instanceof CrossProcessCursor))
            ((CrossProcessCursor)this.mCursor).fillWindow(paramInt, paramCursorWindow);
        while (true)
        {
            return;
            DatabaseUtils.cursorFillWindow(this.mCursor, paramInt, paramCursorWindow);
        }
    }

    public CursorWindow getWindow()
    {
        if ((this.mCursor instanceof CrossProcessCursor));
        for (CursorWindow localCursorWindow = ((CrossProcessCursor)this.mCursor).getWindow(); ; localCursorWindow = null)
            return localCursorWindow;
    }

    public boolean onMove(int paramInt1, int paramInt2)
    {
        if ((this.mCursor instanceof CrossProcessCursor));
        for (boolean bool = ((CrossProcessCursor)this.mCursor).onMove(paramInt1, paramInt2); ; bool = true)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.CrossProcessCursorWrapper
 * JD-Core Version:        0.6.2
 */