package android.database;

import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;

public final class CursorToBulkCursorAdaptor extends BulkCursorNative
    implements IBinder.DeathRecipient
{
    private static final String TAG = "Cursor";
    private CrossProcessCursor mCursor;
    private CursorWindow mFilledWindow;
    private final Object mLock = new Object();
    private ContentObserverProxy mObserver;
    private final String mProviderName;

    public CursorToBulkCursorAdaptor(Cursor paramCursor, IContentObserver paramIContentObserver, String paramString)
    {
        if ((paramCursor instanceof CrossProcessCursor))
            this.mCursor = ((CrossProcessCursor)paramCursor);
        while (true)
        {
            this.mProviderName = paramString;
            synchronized (this.mLock)
            {
                createAndRegisterObserverProxyLocked(paramIContentObserver);
                return;
                this.mCursor = new CrossProcessCursorWrapper(paramCursor);
            }
        }
    }

    private void closeFilledWindowLocked()
    {
        if (this.mFilledWindow != null)
        {
            this.mFilledWindow.close();
            this.mFilledWindow = null;
        }
    }

    private void createAndRegisterObserverProxyLocked(IContentObserver paramIContentObserver)
    {
        if (this.mObserver != null)
            throw new IllegalStateException("an observer is already registered");
        this.mObserver = new ContentObserverProxy(paramIContentObserver, this);
        this.mCursor.registerContentObserver(this.mObserver);
    }

    private void disposeLocked()
    {
        if (this.mCursor != null)
        {
            unregisterObserverProxyLocked();
            this.mCursor.close();
            this.mCursor = null;
        }
        closeFilledWindowLocked();
    }

    private void throwIfCursorIsClosed()
    {
        if (this.mCursor == null)
            throw new StaleDataException("Attempted to access a cursor after it has been closed.");
    }

    private void unregisterObserverProxyLocked()
    {
        if (this.mObserver != null)
        {
            this.mCursor.unregisterContentObserver(this.mObserver);
            this.mObserver.unlinkToDeath(this);
            this.mObserver = null;
        }
    }

    public void binderDied()
    {
        synchronized (this.mLock)
        {
            disposeLocked();
            return;
        }
    }

    public void close()
    {
        synchronized (this.mLock)
        {
            disposeLocked();
            return;
        }
    }

    public void deactivate()
    {
        synchronized (this.mLock)
        {
            if (this.mCursor != null)
            {
                unregisterObserverProxyLocked();
                this.mCursor.deactivate();
            }
            closeFilledWindowLocked();
            return;
        }
    }

    public BulkCursorDescriptor getBulkCursorDescriptor()
    {
        synchronized (this.mLock)
        {
            throwIfCursorIsClosed();
            BulkCursorDescriptor localBulkCursorDescriptor = new BulkCursorDescriptor();
            localBulkCursorDescriptor.cursor = this;
            localBulkCursorDescriptor.columnNames = this.mCursor.getColumnNames();
            localBulkCursorDescriptor.wantsAllOnMoveCalls = this.mCursor.getWantsAllOnMoveCalls();
            localBulkCursorDescriptor.count = this.mCursor.getCount();
            localBulkCursorDescriptor.window = this.mCursor.getWindow();
            if (localBulkCursorDescriptor.window != null)
                localBulkCursorDescriptor.window.acquireReference();
            return localBulkCursorDescriptor;
        }
    }

    public Bundle getExtras()
    {
        synchronized (this.mLock)
        {
            throwIfCursorIsClosed();
            Bundle localBundle = this.mCursor.getExtras();
            return localBundle;
        }
    }

    // ERROR //
    public CursorWindow getWindow(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 32	android/database/CursorToBulkCursorAdaptor:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: invokespecial 101	android/database/CursorToBulkCursorAdaptor:throwIfCursorIsClosed	()V
        //     11: aload_0
        //     12: getfield 36	android/database/CursorToBulkCursorAdaptor:mCursor	Landroid/database/CrossProcessCursor;
        //     15: iload_1
        //     16: invokeinterface 151 2 0
        //     21: ifne +15 -> 36
        //     24: aload_0
        //     25: invokespecial 78	android/database/CursorToBulkCursorAdaptor:closeFilledWindowLocked	()V
        //     28: aconst_null
        //     29: astore 4
        //     31: aload_2
        //     32: monitorexit
        //     33: goto +122 -> 155
        //     36: aload_0
        //     37: getfield 36	android/database/CursorToBulkCursorAdaptor:mCursor	Landroid/database/CrossProcessCursor;
        //     40: invokeinterface 136 1 0
        //     45: astore 4
        //     47: aload 4
        //     49: ifnull +27 -> 76
        //     52: aload_0
        //     53: invokespecial 78	android/database/CursorToBulkCursorAdaptor:closeFilledWindowLocked	()V
        //     56: aload 4
        //     58: ifnull +8 -> 66
        //     61: aload 4
        //     63: invokevirtual 142	android/database/CursorWindow:acquireReference	()V
        //     66: aload_2
        //     67: monitorexit
        //     68: goto +87 -> 155
        //     71: astore_3
        //     72: aload_2
        //     73: monitorexit
        //     74: aload_3
        //     75: athrow
        //     76: aload_0
        //     77: getfield 50	android/database/CursorToBulkCursorAdaptor:mFilledWindow	Landroid/database/CursorWindow;
        //     80: astore 4
        //     82: aload 4
        //     84: ifnonnull +39 -> 123
        //     87: aload_0
        //     88: new 52	android/database/CursorWindow
        //     91: dup
        //     92: aload_0
        //     93: getfield 38	android/database/CursorToBulkCursorAdaptor:mProviderName	Ljava/lang/String;
        //     96: invokespecial 152	android/database/CursorWindow:<init>	(Ljava/lang/String;)V
        //     99: putfield 50	android/database/CursorToBulkCursorAdaptor:mFilledWindow	Landroid/database/CursorWindow;
        //     102: aload_0
        //     103: getfield 50	android/database/CursorToBulkCursorAdaptor:mFilledWindow	Landroid/database/CursorWindow;
        //     106: astore 4
        //     108: aload_0
        //     109: getfield 36	android/database/CursorToBulkCursorAdaptor:mCursor	Landroid/database/CrossProcessCursor;
        //     112: iload_1
        //     113: aload 4
        //     115: invokeinterface 156 3 0
        //     120: goto -64 -> 56
        //     123: iload_1
        //     124: aload 4
        //     126: invokevirtual 159	android/database/CursorWindow:getStartPosition	()I
        //     129: if_icmplt +18 -> 147
        //     132: iload_1
        //     133: aload 4
        //     135: invokevirtual 159	android/database/CursorWindow:getStartPosition	()I
        //     138: aload 4
        //     140: invokevirtual 162	android/database/CursorWindow:getNumRows	()I
        //     143: iadd
        //     144: if_icmplt -36 -> 108
        //     147: aload 4
        //     149: invokevirtual 165	android/database/CursorWindow:clear	()V
        //     152: goto -44 -> 108
        //     155: aload 4
        //     157: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	74	71	finally
        //     76	152	71	finally
    }

    public void onMove(int paramInt)
    {
        synchronized (this.mLock)
        {
            throwIfCursorIsClosed();
            this.mCursor.onMove(this.mCursor.getPosition(), paramInt);
            return;
        }
    }

    public int requery(IContentObserver paramIContentObserver)
    {
        synchronized (this.mLock)
        {
            throwIfCursorIsClosed();
            closeFilledWindowLocked();
            try
            {
                boolean bool = this.mCursor.requery();
                if (!bool)
                    i = -1;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                throw new IllegalStateException(this.mProviderName + " Requery misuse db, mCursor isClosed:" + this.mCursor.isClosed(), localIllegalStateException);
            }
        }
        unregisterObserverProxyLocked();
        createAndRegisterObserverProxyLocked(paramIContentObserver);
        int i = this.mCursor.getCount();
        return i;
    }

    public Bundle respond(Bundle paramBundle)
    {
        synchronized (this.mLock)
        {
            throwIfCursorIsClosed();
            Bundle localBundle = this.mCursor.respond(paramBundle);
            return localBundle;
        }
    }

    private static final class ContentObserverProxy extends ContentObserver
    {
        protected IContentObserver mRemote;

        public ContentObserverProxy(IContentObserver paramIContentObserver, IBinder.DeathRecipient paramDeathRecipient)
        {
            super();
            this.mRemote = paramIContentObserver;
            try
            {
                paramIContentObserver.asBinder().linkToDeath(paramDeathRecipient, 0);
                label23: return;
            }
            catch (RemoteException localRemoteException)
            {
                break label23;
            }
        }

        public boolean deliverSelfNotifications()
        {
            return false;
        }

        public void onChange(boolean paramBoolean, Uri paramUri)
        {
            try
            {
                this.mRemote.onChange(paramBoolean, paramUri);
                label11: return;
            }
            catch (RemoteException localRemoteException)
            {
                break label11;
            }
        }

        public boolean unlinkToDeath(IBinder.DeathRecipient paramDeathRecipient)
        {
            return this.mRemote.asBinder().unlinkToDeath(paramDeathRecipient, 0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.CursorToBulkCursorAdaptor
 * JD-Core Version:        0.6.2
 */