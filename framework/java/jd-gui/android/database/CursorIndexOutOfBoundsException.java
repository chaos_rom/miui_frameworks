package android.database;

public class CursorIndexOutOfBoundsException extends IndexOutOfBoundsException
{
    public CursorIndexOutOfBoundsException(int paramInt1, int paramInt2)
    {
        super("Index " + paramInt1 + " requested, with a size of " + paramInt2);
    }

    public CursorIndexOutOfBoundsException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.CursorIndexOutOfBoundsException
 * JD-Core Version:        0.6.2
 */