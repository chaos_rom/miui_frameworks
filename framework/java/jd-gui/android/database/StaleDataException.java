package android.database;

public class StaleDataException extends RuntimeException
{
    public StaleDataException()
    {
    }

    public StaleDataException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.StaleDataException
 * JD-Core Version:        0.6.2
 */