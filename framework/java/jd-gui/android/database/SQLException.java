package android.database;

public class SQLException extends RuntimeException
{
    public SQLException()
    {
    }

    public SQLException(String paramString)
    {
        super(paramString);
    }

    public SQLException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.SQLException
 * JD-Core Version:        0.6.2
 */