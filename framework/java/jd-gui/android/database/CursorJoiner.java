package android.database;

import java.util.Iterator;

public final class CursorJoiner
    implements Iterator<Result>, Iterable<Result>
{
    private int[] mColumnsLeft;
    private int[] mColumnsRight;
    private Result mCompareResult;
    private boolean mCompareResultIsValid;
    private Cursor mCursorLeft;
    private Cursor mCursorRight;
    private String[] mValues;

    static
    {
        if (!CursorJoiner.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    public CursorJoiner(Cursor paramCursor1, String[] paramArrayOfString1, Cursor paramCursor2, String[] paramArrayOfString2)
    {
        if (paramArrayOfString1.length != paramArrayOfString2.length)
            throw new IllegalArgumentException("you must have the same number of columns on the left and right, " + paramArrayOfString1.length + " != " + paramArrayOfString2.length);
        this.mCursorLeft = paramCursor1;
        this.mCursorRight = paramCursor2;
        this.mCursorLeft.moveToFirst();
        this.mCursorRight.moveToFirst();
        this.mCompareResultIsValid = false;
        this.mColumnsLeft = buildColumnIndiciesArray(paramCursor1, paramArrayOfString1);
        this.mColumnsRight = buildColumnIndiciesArray(paramCursor2, paramArrayOfString2);
        this.mValues = new String[2 * this.mColumnsLeft.length];
    }

    private int[] buildColumnIndiciesArray(Cursor paramCursor, String[] paramArrayOfString)
    {
        int[] arrayOfInt = new int[paramArrayOfString.length];
        for (int i = 0; i < paramArrayOfString.length; i++)
            arrayOfInt[i] = paramCursor.getColumnIndexOrThrow(paramArrayOfString[i]);
        return arrayOfInt;
    }

    private static int compareStrings(String[] paramArrayOfString)
    {
        int i = -1;
        if (paramArrayOfString.length % 2 != 0)
            throw new IllegalArgumentException("you must specify an even number of values");
        int j = 0;
        while (true)
            if (j < paramArrayOfString.length)
                if (paramArrayOfString[j] == null)
                {
                    if (paramArrayOfString[(j + 1)] == null)
                        j += 2;
                }
                else if (paramArrayOfString[(j + 1)] == null)
                    i = 1;
        while (true)
        {
            return i;
            int k = paramArrayOfString[j].compareTo(paramArrayOfString[(j + 1)]);
            if (k == 0)
                break;
            if (k >= 0)
            {
                i = 1;
                continue;
                i = 0;
            }
        }
    }

    private void incrementCursors()
    {
        if (this.mCompareResultIsValid)
            switch (1.$SwitchMap$android$database$CursorJoiner$Result[this.mCompareResult.ordinal()])
            {
            default:
            case 2:
            case 3:
            case 1:
            }
        while (true)
        {
            this.mCompareResultIsValid = false;
            return;
            this.mCursorLeft.moveToNext();
            continue;
            this.mCursorRight.moveToNext();
            continue;
            this.mCursorLeft.moveToNext();
            this.mCursorRight.moveToNext();
        }
    }

    private static void populateValues(String[] paramArrayOfString, Cursor paramCursor, int[] paramArrayOfInt, int paramInt)
    {
        assert ((paramInt == 0) || (paramInt == 1));
        for (int i = 0; i < paramArrayOfInt.length; i++)
            paramArrayOfString[(paramInt + i * 2)] = paramCursor.getString(paramArrayOfInt[i]);
    }

    public boolean hasNext()
    {
        boolean bool = false;
        if (this.mCompareResultIsValid)
            switch (1.$SwitchMap$android$database$CursorJoiner$Result[this.mCompareResult.ordinal()])
            {
            default:
                throw new IllegalStateException("bad value for mCompareResult, " + this.mCompareResult);
            case 1:
                if ((!this.mCursorLeft.isLast()) || (!this.mCursorRight.isLast()))
                    bool = true;
                break;
            case 2:
            case 3:
            }
        while (true)
        {
            return bool;
            if ((!this.mCursorLeft.isLast()) || (!this.mCursorRight.isAfterLast()))
            {
                bool = true;
                continue;
                if ((!this.mCursorLeft.isAfterLast()) || (!this.mCursorRight.isLast()))
                {
                    bool = true;
                    continue;
                    if ((!this.mCursorLeft.isAfterLast()) || (!this.mCursorRight.isAfterLast()))
                        bool = true;
                }
            }
        }
    }

    public Iterator<Result> iterator()
    {
        return this;
    }

    public Result next()
    {
        if (!hasNext())
            throw new IllegalStateException("you must only call next() when hasNext() is true");
        incrementCursors();
        assert (hasNext());
        int i;
        int j;
        if (!this.mCursorLeft.isAfterLast())
        {
            i = 1;
            if (this.mCursorRight.isAfterLast())
                break label159;
            j = 1;
            label70: if ((i == 0) || (j == 0))
                break label194;
            populateValues(this.mValues, this.mCursorLeft, this.mColumnsLeft, 0);
            populateValues(this.mValues, this.mCursorRight, this.mColumnsRight, 1);
            switch (compareStrings(this.mValues))
            {
            default:
            case -1:
            case 0:
            case 1:
            }
        }
        while (true)
        {
            this.mCompareResultIsValid = true;
            return this.mCompareResult;
            i = 0;
            break;
            label159: j = 0;
            break label70;
            this.mCompareResult = Result.LEFT;
            continue;
            this.mCompareResult = Result.BOTH;
            continue;
            this.mCompareResult = Result.RIGHT;
            continue;
            label194: if (i != 0)
            {
                this.mCompareResult = Result.LEFT;
            }
            else
            {
                assert (j != 0);
                this.mCompareResult = Result.RIGHT;
            }
        }
    }

    public void remove()
    {
        throw new UnsupportedOperationException("not implemented");
    }

    public static enum Result
    {
        static
        {
            LEFT = new Result("LEFT", 1);
            BOTH = new Result("BOTH", 2);
            Result[] arrayOfResult = new Result[3];
            arrayOfResult[0] = RIGHT;
            arrayOfResult[1] = LEFT;
            arrayOfResult[2] = BOTH;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.CursorJoiner
 * JD-Core Version:        0.6.2
 */