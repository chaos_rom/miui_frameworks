package android.database;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

public final class BulkCursorToCursorAdaptor extends AbstractWindowedCursor
{
    private static final String TAG = "BulkCursor";
    private IBulkCursor mBulkCursor;
    private String[] mColumns;
    private int mCount;
    private AbstractCursor.SelfContentObserver mObserverBridge = new AbstractCursor.SelfContentObserver(this);
    private boolean mWantsAllOnMoveCalls;

    private void throwIfCursorIsClosed()
    {
        if (this.mBulkCursor == null)
            throw new StaleDataException("Attempted to access a cursor after it has been closed.");
    }

    public void close()
    {
        super.close();
        if (this.mBulkCursor != null);
        try
        {
            this.mBulkCursor.close();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("BulkCursor", "Remote process exception when closing");
        }
        finally
        {
            this.mBulkCursor = null;
        }
    }

    public void deactivate()
    {
        super.deactivate();
        if (this.mBulkCursor != null);
        try
        {
            this.mBulkCursor.deactivate();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("BulkCursor", "Remote process exception when deactivating");
        }
    }

    public String[] getColumnNames()
    {
        throwIfCursorIsClosed();
        return this.mColumns;
    }

    public int getCount()
    {
        throwIfCursorIsClosed();
        return this.mCount;
    }

    public Bundle getExtras()
    {
        throwIfCursorIsClosed();
        try
        {
            Bundle localBundle = this.mBulkCursor.getExtras();
            return localBundle;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public IContentObserver getObserver()
    {
        return this.mObserverBridge.getContentObserver();
    }

    public void initialize(BulkCursorDescriptor paramBulkCursorDescriptor)
    {
        this.mBulkCursor = paramBulkCursorDescriptor.cursor;
        this.mColumns = paramBulkCursorDescriptor.columnNames;
        this.mRowIdColumnIndex = DatabaseUtils.findRowIdColumnIndex(this.mColumns);
        this.mWantsAllOnMoveCalls = paramBulkCursorDescriptor.wantsAllOnMoveCalls;
        this.mCount = paramBulkCursorDescriptor.count;
        if (paramBulkCursorDescriptor.window != null)
            setWindow(paramBulkCursorDescriptor.window);
    }

    public boolean onMove(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        throwIfCursorIsClosed();
        try
        {
            if ((this.mWindow == null) || (paramInt2 < this.mWindow.getStartPosition()) || (paramInt2 >= this.mWindow.getStartPosition() + this.mWindow.getNumRows()))
                setWindow(this.mBulkCursor.getWindow(paramInt2));
            while (this.mWindow == null)
            {
                return bool;
                if (this.mWantsAllOnMoveCalls)
                    this.mBulkCursor.onMove(paramInt2);
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("BulkCursor", "Unable to get window because the remote process is dead");
                continue;
                bool = true;
            }
        }
    }

    public boolean requery()
    {
        boolean bool = false;
        throwIfCursorIsClosed();
        try
        {
            this.mCount = this.mBulkCursor.requery(getObserver());
            if (this.mCount != -1)
            {
                this.mPos = -1;
                closeWindow();
                super.requery();
                bool = true;
            }
            else
            {
                deactivate();
            }
        }
        catch (Exception localException)
        {
            Log.e("BulkCursor", "Unable to requery because the remote process exception " + localException.getMessage());
            deactivate();
        }
        return bool;
    }

    public Bundle respond(Bundle paramBundle)
    {
        throwIfCursorIsClosed();
        try
        {
            Bundle localBundle2 = this.mBulkCursor.respond(paramBundle);
            localBundle1 = localBundle2;
            return localBundle1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.w("BulkCursor", "respond() threw RemoteException, returning an empty bundle.", localRemoteException);
                Bundle localBundle1 = Bundle.EMPTY;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.BulkCursorToCursorAdaptor
 * JD-Core Version:        0.6.2
 */