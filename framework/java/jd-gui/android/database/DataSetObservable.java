package android.database;

import java.util.ArrayList;

public class DataSetObservable extends Observable<DataSetObserver>
{
    public void notifyChanged()
    {
        synchronized (this.mObservers)
        {
            for (int i = -1 + this.mObservers.size(); i >= 0; i--)
                ((DataSetObserver)this.mObservers.get(i)).onChanged();
            return;
        }
    }

    public void notifyInvalidated()
    {
        synchronized (this.mObservers)
        {
            for (int i = -1 + this.mObservers.size(); i >= 0; i--)
                ((DataSetObserver)this.mObservers.get(i)).onInvalidated();
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.DataSetObservable
 * JD-Core Version:        0.6.2
 */