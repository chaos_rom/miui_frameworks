package android.database;

public abstract class DataSetObserver
{
    public void onChanged()
    {
    }

    public void onInvalidated()
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.DataSetObserver
 * JD-Core Version:        0.6.2
 */