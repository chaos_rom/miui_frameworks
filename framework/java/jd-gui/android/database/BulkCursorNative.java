package android.database;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class BulkCursorNative extends Binder
    implements IBulkCursor
{
    public BulkCursorNative()
    {
        attachInterface(this, "android.content.IBulkCursor");
    }

    public static IBulkCursor asInterface(IBinder paramIBinder)
    {
        Object localObject;
        if (paramIBinder == null)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = (IBulkCursor)paramIBinder.queryLocalInterface("android.content.IBulkCursor");
            if (localObject == null)
                localObject = new BulkCursorProxy(paramIBinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        boolean bool = true;
        switch (paramInt1)
        {
        default:
            bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
        case 1:
        case 2:
        case 7:
        case 3:
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return bool;
            CursorWindow localCursorWindow;
            try
            {
                paramParcel1.enforceInterface("android.content.IBulkCursor");
                localCursorWindow = getWindow(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localCursorWindow != null)
                    break label106;
                paramParcel2.writeInt(0);
            }
            catch (Exception localException)
            {
                DatabaseUtils.writeExceptionToParcel(paramParcel2, localException);
            }
            continue;
            label106: paramParcel2.writeInt(1);
            localCursorWindow.writeToParcel(paramParcel2, 1);
            continue;
            paramParcel1.enforceInterface("android.content.IBulkCursor");
            deactivate();
            paramParcel2.writeNoException();
            continue;
            paramParcel1.enforceInterface("android.content.IBulkCursor");
            close();
            paramParcel2.writeNoException();
            continue;
            paramParcel1.enforceInterface("android.content.IBulkCursor");
            int i = requery(IContentObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
            paramParcel2.writeNoException();
            paramParcel2.writeInt(i);
            paramParcel2.writeBundle(getExtras());
            continue;
            paramParcel1.enforceInterface("android.content.IBulkCursor");
            onMove(paramParcel1.readInt());
            paramParcel2.writeNoException();
            continue;
            paramParcel1.enforceInterface("android.content.IBulkCursor");
            Bundle localBundle2 = getExtras();
            paramParcel2.writeNoException();
            paramParcel2.writeBundle(localBundle2);
            continue;
            paramParcel1.enforceInterface("android.content.IBulkCursor");
            Bundle localBundle1 = respond(paramParcel1.readBundle());
            paramParcel2.writeNoException();
            paramParcel2.writeBundle(localBundle1);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.BulkCursorNative
 * JD-Core Version:        0.6.2
 */