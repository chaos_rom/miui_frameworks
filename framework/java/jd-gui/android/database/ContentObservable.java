package android.database;

public class ContentObservable extends Observable<ContentObserver>
{
    @Deprecated
    public void dispatchChange(boolean paramBoolean)
    {
        dispatchChange(paramBoolean, null);
    }

    // ERROR //
    public void dispatchChange(boolean paramBoolean, android.net.Uri paramUri)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 19	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 19	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     11: invokevirtual 25	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     14: astore 5
        //     16: aload 5
        //     18: invokeinterface 31 1 0
        //     23: ifeq +44 -> 67
        //     26: aload 5
        //     28: invokeinterface 35 1 0
        //     33: checkcast 37	android/database/ContentObserver
        //     36: astore 6
        //     38: iload_1
        //     39: ifeq +11 -> 50
        //     42: aload 6
        //     44: invokevirtual 40	android/database/ContentObserver:deliverSelfNotifications	()Z
        //     47: ifeq -31 -> 16
        //     50: aload 6
        //     52: iload_1
        //     53: aload_2
        //     54: invokevirtual 41	android/database/ContentObserver:dispatchChange	(ZLandroid/net/Uri;)V
        //     57: goto -41 -> 16
        //     60: astore 4
        //     62: aload_3
        //     63: monitorexit
        //     64: aload 4
        //     66: athrow
        //     67: aload_3
        //     68: monitorexit
        //     69: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	64	60	finally
        //     67	69	60	finally
    }

    // ERROR //
    @Deprecated
    public void notifyChange(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 19	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 19	android/database/Observable:mObservers	Ljava/util/ArrayList;
        //     11: invokevirtual 25	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     14: astore 4
        //     16: aload 4
        //     18: invokeinterface 31 1 0
        //     23: ifeq +26 -> 49
        //     26: aload 4
        //     28: invokeinterface 35 1 0
        //     33: checkcast 37	android/database/ContentObserver
        //     36: iload_1
        //     37: aconst_null
        //     38: invokevirtual 45	android/database/ContentObserver:onChange	(ZLandroid/net/Uri;)V
        //     41: goto -25 -> 16
        //     44: astore_3
        //     45: aload_2
        //     46: monitorexit
        //     47: aload_3
        //     48: athrow
        //     49: aload_2
        //     50: monitorexit
        //     51: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	47	44	finally
        //     49	51	44	finally
    }

    public void registerObserver(ContentObserver paramContentObserver)
    {
        super.registerObserver(paramContentObserver);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.ContentObservable
 * JD-Core Version:        0.6.2
 */