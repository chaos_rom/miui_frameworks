package android.database;

import android.util.Log;
import java.io.File;

public final class DefaultDatabaseErrorHandler
    implements DatabaseErrorHandler
{
    private static final String TAG = "DefaultDatabaseErrorHandler";

    private void deleteDatabaseFile(String paramString)
    {
        if ((paramString.equalsIgnoreCase(":memory:")) || (paramString.trim().length() == 0));
        while (true)
        {
            return;
            Log.e("DefaultDatabaseErrorHandler", "deleting the database file: " + paramString);
            try
            {
                new File(paramString).delete();
            }
            catch (Exception localException)
            {
                Log.w("DefaultDatabaseErrorHandler", "delete failed: " + localException.getMessage());
            }
        }
    }

    // ERROR //
    public void onCorruption(android.database.sqlite.SQLiteDatabase paramSQLiteDatabase)
    {
        // Byte code:
        //     0: ldc 10
        //     2: new 36	java/lang/StringBuilder
        //     5: dup
        //     6: invokespecial 37	java/lang/StringBuilder:<init>	()V
        //     9: ldc 74
        //     11: invokevirtual 43	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     14: aload_1
        //     15: invokevirtual 79	android/database/sqlite/SQLiteDatabase:getPath	()Ljava/lang/String;
        //     18: invokevirtual 43	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     21: invokevirtual 46	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     24: invokestatic 52	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     27: pop
        //     28: aload_1
        //     29: invokevirtual 82	android/database/sqlite/SQLiteDatabase:isOpen	()Z
        //     32: ifne +12 -> 44
        //     35: aload_0
        //     36: aload_1
        //     37: invokevirtual 79	android/database/sqlite/SQLiteDatabase:getPath	()Ljava/lang/String;
        //     40: invokespecial 84	android/database/DefaultDatabaseErrorHandler:deleteDatabaseFile	(Ljava/lang/String;)V
        //     43: return
        //     44: aconst_null
        //     45: astore_3
        //     46: aload_1
        //     47: invokevirtual 88	android/database/sqlite/SQLiteDatabase:getAttachedDbs	()Ljava/util/List;
        //     50: astore 9
        //     52: aload 9
        //     54: astore_3
        //     55: aload_1
        //     56: invokevirtual 91	android/database/sqlite/SQLiteDatabase:close	()V
        //     59: aload_3
        //     60: ifnull +44 -> 104
        //     63: aload_3
        //     64: invokeinterface 97 1 0
        //     69: astore 6
        //     71: aload 6
        //     73: invokeinterface 102 1 0
        //     78: ifeq -35 -> 43
        //     81: aload_0
        //     82: aload 6
        //     84: invokeinterface 106 1 0
        //     89: checkcast 108	android/util/Pair
        //     92: getfield 112	android/util/Pair:second	Ljava/lang/Object;
        //     95: checkcast 22	java/lang/String
        //     98: invokespecial 84	android/database/DefaultDatabaseErrorHandler:deleteDatabaseFile	(Ljava/lang/String;)V
        //     101: goto -30 -> 71
        //     104: aload_0
        //     105: aload_1
        //     106: invokevirtual 79	android/database/sqlite/SQLiteDatabase:getPath	()Ljava/lang/String;
        //     109: invokespecial 84	android/database/DefaultDatabaseErrorHandler:deleteDatabaseFile	(Ljava/lang/String;)V
        //     112: goto -69 -> 43
        //     115: astore 7
        //     117: aload_3
        //     118: ifnull +44 -> 162
        //     121: aload_3
        //     122: invokeinterface 97 1 0
        //     127: astore 8
        //     129: aload 8
        //     131: invokeinterface 102 1 0
        //     136: ifeq +34 -> 170
        //     139: aload_0
        //     140: aload 8
        //     142: invokeinterface 106 1 0
        //     147: checkcast 108	android/util/Pair
        //     150: getfield 112	android/util/Pair:second	Ljava/lang/Object;
        //     153: checkcast 22	java/lang/String
        //     156: invokespecial 84	android/database/DefaultDatabaseErrorHandler:deleteDatabaseFile	(Ljava/lang/String;)V
        //     159: goto -30 -> 129
        //     162: aload_0
        //     163: aload_1
        //     164: invokevirtual 79	android/database/sqlite/SQLiteDatabase:getPath	()Ljava/lang/String;
        //     167: invokespecial 84	android/database/DefaultDatabaseErrorHandler:deleteDatabaseFile	(Ljava/lang/String;)V
        //     170: aload 7
        //     172: athrow
        //     173: astore 4
        //     175: goto -120 -> 55
        //     178: astore 5
        //     180: goto -121 -> 59
        //
        // Exception table:
        //     from	to	target	type
        //     46	52	115	finally
        //     55	59	115	finally
        //     46	52	173	android/database/sqlite/SQLiteException
        //     55	59	178	android/database/sqlite/SQLiteException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.DefaultDatabaseErrorHandler
 * JD-Core Version:        0.6.2
 */