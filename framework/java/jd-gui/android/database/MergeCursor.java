package android.database;

public class MergeCursor extends AbstractCursor
{
    private Cursor mCursor;
    private Cursor[] mCursors;
    private DataSetObserver mObserver = new DataSetObserver()
    {
        public void onChanged()
        {
            MergeCursor.this.mPos = -1;
        }

        public void onInvalidated()
        {
            MergeCursor.this.mPos = -1;
        }
    };

    public MergeCursor(Cursor[] paramArrayOfCursor)
    {
        this.mCursors = paramArrayOfCursor;
        this.mCursor = paramArrayOfCursor[0];
        int i = 0;
        if (i < this.mCursors.length)
        {
            if (this.mCursors[i] == null);
            while (true)
            {
                i++;
                break;
                this.mCursors[i].registerDataSetObserver(this.mObserver);
            }
        }
    }

    public void close()
    {
        int i = this.mCursors.length;
        int j = 0;
        if (j < i)
        {
            if (this.mCursors[j] == null);
            while (true)
            {
                j++;
                break;
                this.mCursors[j].close();
            }
        }
        super.close();
    }

    public void deactivate()
    {
        int i = this.mCursors.length;
        for (int j = 0; j < i; j++)
            if (this.mCursors[j] != null)
                this.mCursors[j].deactivate();
        super.deactivate();
    }

    public byte[] getBlob(int paramInt)
    {
        return this.mCursor.getBlob(paramInt);
    }

    public String[] getColumnNames()
    {
        if (this.mCursor != null);
        for (String[] arrayOfString = this.mCursor.getColumnNames(); ; arrayOfString = new String[0])
            return arrayOfString;
    }

    public int getCount()
    {
        int i = 0;
        int j = this.mCursors.length;
        for (int k = 0; k < j; k++)
            if (this.mCursors[k] != null)
                i += this.mCursors[k].getCount();
        return i;
    }

    public double getDouble(int paramInt)
    {
        return this.mCursor.getDouble(paramInt);
    }

    public float getFloat(int paramInt)
    {
        return this.mCursor.getFloat(paramInt);
    }

    public int getInt(int paramInt)
    {
        return this.mCursor.getInt(paramInt);
    }

    public long getLong(int paramInt)
    {
        return this.mCursor.getLong(paramInt);
    }

    public short getShort(int paramInt)
    {
        return this.mCursor.getShort(paramInt);
    }

    public String getString(int paramInt)
    {
        return this.mCursor.getString(paramInt);
    }

    public int getType(int paramInt)
    {
        return this.mCursor.getType(paramInt);
    }

    public boolean isNull(int paramInt)
    {
        return this.mCursor.isNull(paramInt);
    }

    public boolean onMove(int paramInt1, int paramInt2)
    {
        this.mCursor = null;
        int i = 0;
        int j = this.mCursors.length;
        int k = 0;
        while (k < j)
            if (this.mCursors[k] == null)
            {
                k++;
            }
            else
            {
                if (paramInt2 >= i + this.mCursors[k].getCount())
                    break label93;
                this.mCursor = this.mCursors[k];
            }
        if (this.mCursor != null);
        for (boolean bool = this.mCursor.moveToPosition(paramInt2 - i); ; bool = false)
        {
            return bool;
            label93: i += this.mCursors[k].getCount();
            break;
        }
    }

    public void registerContentObserver(ContentObserver paramContentObserver)
    {
        int i = this.mCursors.length;
        for (int j = 0; j < i; j++)
            if (this.mCursors[j] != null)
                this.mCursors[j].registerContentObserver(paramContentObserver);
    }

    public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        int i = this.mCursors.length;
        for (int j = 0; j < i; j++)
            if (this.mCursors[j] != null)
                this.mCursors[j].registerDataSetObserver(paramDataSetObserver);
    }

    public boolean requery()
    {
        int i = this.mCursors.length;
        int j = 0;
        if (j < i)
        {
            if (this.mCursors[j] == null);
            while (this.mCursors[j].requery())
            {
                j++;
                break;
            }
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public void unregisterContentObserver(ContentObserver paramContentObserver)
    {
        int i = this.mCursors.length;
        for (int j = 0; j < i; j++)
            if (this.mCursors[j] != null)
                this.mCursors[j].unregisterContentObserver(paramContentObserver);
    }

    public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        int i = this.mCursors.length;
        for (int j = 0; j < i; j++)
            if (this.mCursors[j] != null)
                this.mCursors[j].unregisterDataSetObserver(paramDataSetObserver);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.MergeCursor
 * JD-Core Version:        0.6.2
 */