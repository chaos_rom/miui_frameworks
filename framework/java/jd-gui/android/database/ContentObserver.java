package android.database;

import android.net.Uri;
import android.os.Handler;

public abstract class ContentObserver
{
    Handler mHandler;
    private final Object mLock = new Object();
    private Transport mTransport;

    public ContentObserver(Handler paramHandler)
    {
        this.mHandler = paramHandler;
    }

    public boolean deliverSelfNotifications()
    {
        return false;
    }

    @Deprecated
    public final void dispatchChange(boolean paramBoolean)
    {
        dispatchChange(paramBoolean, null);
    }

    public final void dispatchChange(boolean paramBoolean, Uri paramUri)
    {
        if (this.mHandler == null)
            onChange(paramBoolean, paramUri);
        while (true)
        {
            return;
            this.mHandler.post(new NotificationRunnable(paramBoolean, paramUri));
        }
    }

    public IContentObserver getContentObserver()
    {
        synchronized (this.mLock)
        {
            if (this.mTransport == null)
                this.mTransport = new Transport(this);
            Transport localTransport = this.mTransport;
            return localTransport;
        }
    }

    public void onChange(boolean paramBoolean)
    {
    }

    public void onChange(boolean paramBoolean, Uri paramUri)
    {
        onChange(paramBoolean);
    }

    public IContentObserver releaseContentObserver()
    {
        synchronized (this.mLock)
        {
            Transport localTransport = this.mTransport;
            if (localTransport != null)
            {
                localTransport.releaseContentObserver();
                this.mTransport = null;
            }
            return localTransport;
        }
    }

    private static final class Transport extends IContentObserver.Stub
    {
        private ContentObserver mContentObserver;

        public Transport(ContentObserver paramContentObserver)
        {
            this.mContentObserver = paramContentObserver;
        }

        public void onChange(boolean paramBoolean, Uri paramUri)
        {
            ContentObserver localContentObserver = this.mContentObserver;
            if (localContentObserver != null)
                localContentObserver.dispatchChange(paramBoolean, paramUri);
        }

        public void releaseContentObserver()
        {
            this.mContentObserver = null;
        }
    }

    private final class NotificationRunnable
        implements Runnable
    {
        private final boolean mSelfChange;
        private final Uri mUri;

        public NotificationRunnable(boolean paramUri, Uri arg3)
        {
            this.mSelfChange = paramUri;
            Object localObject;
            this.mUri = localObject;
        }

        public void run()
        {
            ContentObserver.this.onChange(this.mSelfChange, this.mUri);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.ContentObserver
 * JD-Core Version:        0.6.2
 */