package android.database;

public final class CharArrayBuffer
{
    public char[] data;
    public int sizeCopied;

    public CharArrayBuffer(int paramInt)
    {
        this.data = new char[paramInt];
    }

    public CharArrayBuffer(char[] paramArrayOfChar)
    {
        this.data = paramArrayOfChar;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.CharArrayBuffer
 * JD-Core Version:        0.6.2
 */