package android.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteProgram;
import android.database.sqlite.SQLiteStatement;
import android.os.OperationCanceledException;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.Log;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.CollationKey;
import java.text.Collator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.codec.binary.Hex;

public class DatabaseUtils
{
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    public static final int STATEMENT_ABORT = 6;
    public static final int STATEMENT_ATTACH = 3;
    public static final int STATEMENT_BEGIN = 4;
    public static final int STATEMENT_COMMIT = 5;
    public static final int STATEMENT_DDL = 8;
    public static final int STATEMENT_OTHER = 99;
    public static final int STATEMENT_PRAGMA = 7;
    public static final int STATEMENT_SELECT = 1;
    public static final int STATEMENT_UNPREPARED = 9;
    public static final int STATEMENT_UPDATE = 2;
    private static final String TAG = "DatabaseUtils";
    private static final String[] countProjection = arrayOfString;
    private static Collator mColl = null;

    static
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "count(*)";
    }

    public static void appendEscapedSQLString(StringBuilder paramStringBuilder, String paramString)
    {
        paramStringBuilder.append('\'');
        if (paramString.indexOf('\'') != -1)
        {
            int i = paramString.length();
            for (int j = 0; j < i; j++)
            {
                char c = paramString.charAt(j);
                if (c == '\'')
                    paramStringBuilder.append('\'');
                paramStringBuilder.append(c);
            }
        }
        paramStringBuilder.append(paramString);
        paramStringBuilder.append('\'');
    }

    public static String[] appendSelectionArgs(String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        String[] arrayOfString;
        if ((paramArrayOfString1 == null) || (paramArrayOfString1.length == 0))
            arrayOfString = paramArrayOfString2;
        while (true)
        {
            return arrayOfString;
            arrayOfString = new String[paramArrayOfString1.length + paramArrayOfString2.length];
            System.arraycopy(paramArrayOfString1, 0, arrayOfString, 0, paramArrayOfString1.length);
            System.arraycopy(paramArrayOfString2, 0, arrayOfString, paramArrayOfString1.length, paramArrayOfString2.length);
        }
    }

    public static final void appendValueToSql(StringBuilder paramStringBuilder, Object paramObject)
    {
        if (paramObject == null)
            paramStringBuilder.append("NULL");
        while (true)
        {
            return;
            if ((paramObject instanceof Boolean))
            {
                if (((Boolean)paramObject).booleanValue())
                    paramStringBuilder.append('1');
                else
                    paramStringBuilder.append('0');
            }
            else
                appendEscapedSQLString(paramStringBuilder, paramObject.toString());
        }
    }

    public static void bindObjectToProgram(SQLiteProgram paramSQLiteProgram, int paramInt, Object paramObject)
    {
        if (paramObject == null)
            paramSQLiteProgram.bindNull(paramInt);
        while (true)
        {
            return;
            if (((paramObject instanceof Double)) || ((paramObject instanceof Float)))
                paramSQLiteProgram.bindDouble(paramInt, ((Number)paramObject).doubleValue());
            else if ((paramObject instanceof Number))
                paramSQLiteProgram.bindLong(paramInt, ((Number)paramObject).longValue());
            else if ((paramObject instanceof Boolean))
            {
                if (((Boolean)paramObject).booleanValue())
                    paramSQLiteProgram.bindLong(paramInt, 1L);
                else
                    paramSQLiteProgram.bindLong(paramInt, 0L);
            }
            else if ((paramObject instanceof byte[]))
                paramSQLiteProgram.bindBlob(paramInt, (byte[])paramObject);
            else
                paramSQLiteProgram.bindString(paramInt, paramObject.toString());
        }
    }

    public static ParcelFileDescriptor blobFileDescriptorForQuery(SQLiteDatabase paramSQLiteDatabase, String paramString, String[] paramArrayOfString)
    {
        SQLiteStatement localSQLiteStatement = paramSQLiteDatabase.compileStatement(paramString);
        try
        {
            ParcelFileDescriptor localParcelFileDescriptor = blobFileDescriptorForQuery(localSQLiteStatement, paramArrayOfString);
            return localParcelFileDescriptor;
        }
        finally
        {
            localSQLiteStatement.close();
        }
    }

    public static ParcelFileDescriptor blobFileDescriptorForQuery(SQLiteStatement paramSQLiteStatement, String[] paramArrayOfString)
    {
        paramSQLiteStatement.bindAllArgsAsStrings(paramArrayOfString);
        return paramSQLiteStatement.simpleQueryForBlobFileDescriptor();
    }

    public static String concatenateWhere(String paramString1, String paramString2)
    {
        if (TextUtils.isEmpty(paramString1));
        while (true)
        {
            return paramString2;
            if (TextUtils.isEmpty(paramString2))
                paramString2 = paramString1;
            else
                paramString2 = "(" + paramString1 + ") AND (" + paramString2 + ")";
        }
    }

    public static void createDbFromSqlStatements(Context paramContext, String paramString1, int paramInt, String paramString2)
    {
        SQLiteDatabase localSQLiteDatabase = paramContext.openOrCreateDatabase(paramString1, 0, null);
        String[] arrayOfString = TextUtils.split(paramString2, ";\n");
        int i = arrayOfString.length;
        int j = 0;
        if (j < i)
        {
            String str = arrayOfString[j];
            if (TextUtils.isEmpty(str));
            while (true)
            {
                j++;
                break;
                localSQLiteDatabase.execSQL(str);
            }
        }
        localSQLiteDatabase.setVersion(paramInt);
        localSQLiteDatabase.close();
    }

    public static void cursorDoubleToContentValues(Cursor paramCursor, String paramString1, ContentValues paramContentValues, String paramString2)
    {
        int i = paramCursor.getColumnIndex(paramString1);
        if (!paramCursor.isNull(i))
            paramContentValues.put(paramString2, Double.valueOf(paramCursor.getDouble(i)));
        while (true)
        {
            return;
            paramContentValues.put(paramString2, (Double)null);
        }
    }

    public static void cursorDoubleToContentValuesIfPresent(Cursor paramCursor, ContentValues paramContentValues, String paramString)
    {
        int i = paramCursor.getColumnIndex(paramString);
        if ((i != -1) && (!paramCursor.isNull(i)))
            paramContentValues.put(paramString, Double.valueOf(paramCursor.getDouble(i)));
    }

    public static void cursorDoubleToCursorValues(Cursor paramCursor, String paramString, ContentValues paramContentValues)
    {
        cursorDoubleToContentValues(paramCursor, paramString, paramContentValues, paramString);
    }

    public static void cursorFillWindow(Cursor paramCursor, int paramInt, CursorWindow paramCursorWindow)
    {
        if ((paramInt < 0) || (paramInt >= paramCursor.getCount()));
        int j;
        while (true)
        {
            return;
            int i = paramCursor.getPosition();
            j = paramCursor.getColumnCount();
            paramCursorWindow.clear();
            paramCursorWindow.setStartPosition(paramInt);
            paramCursorWindow.setNumColumns(j);
            if (paramCursor.moveToPosition(paramInt))
                if (paramCursorWindow.allocRow())
                    break;
            label63: paramCursor.moveToPosition(i);
        }
        label283: for (int k = 0; ; k++)
        {
            boolean bool;
            if (k < j)
                switch (paramCursor.getType(k))
                {
                case 3:
                default:
                    String str = paramCursor.getString(k);
                    if (str != null)
                        bool = paramCursorWindow.putString(str, paramInt, k);
                    break;
                case 0:
                case 1:
                case 2:
                case 4:
                }
            while (true)
            {
                if (bool)
                    break label283;
                paramCursorWindow.freeLastRow();
                paramInt++;
                if (paramCursor.moveToNext())
                    break;
                break label63;
                bool = paramCursorWindow.putNull(paramInt, k);
                continue;
                bool = paramCursorWindow.putLong(paramCursor.getLong(k), paramInt, k);
                continue;
                bool = paramCursorWindow.putDouble(paramCursor.getDouble(k), paramInt, k);
                continue;
                byte[] arrayOfByte = paramCursor.getBlob(k);
                if (arrayOfByte != null);
                for (bool = paramCursorWindow.putBlob(arrayOfByte, paramInt, k); ; bool = paramCursorWindow.putNull(paramInt, k))
                    break;
                bool = paramCursorWindow.putNull(paramInt, k);
            }
        }
    }

    public static void cursorFloatToContentValuesIfPresent(Cursor paramCursor, ContentValues paramContentValues, String paramString)
    {
        int i = paramCursor.getColumnIndex(paramString);
        if ((i != -1) && (!paramCursor.isNull(i)))
            paramContentValues.put(paramString, Float.valueOf(paramCursor.getFloat(i)));
    }

    public static void cursorIntToContentValues(Cursor paramCursor, String paramString, ContentValues paramContentValues)
    {
        cursorIntToContentValues(paramCursor, paramString, paramContentValues, paramString);
    }

    public static void cursorIntToContentValues(Cursor paramCursor, String paramString1, ContentValues paramContentValues, String paramString2)
    {
        int i = paramCursor.getColumnIndex(paramString1);
        if (!paramCursor.isNull(i))
            paramContentValues.put(paramString2, Integer.valueOf(paramCursor.getInt(i)));
        while (true)
        {
            return;
            paramContentValues.put(paramString2, (Integer)null);
        }
    }

    public static void cursorIntToContentValuesIfPresent(Cursor paramCursor, ContentValues paramContentValues, String paramString)
    {
        int i = paramCursor.getColumnIndex(paramString);
        if ((i != -1) && (!paramCursor.isNull(i)))
            paramContentValues.put(paramString, Integer.valueOf(paramCursor.getInt(i)));
    }

    public static void cursorLongToContentValues(Cursor paramCursor, String paramString, ContentValues paramContentValues)
    {
        cursorLongToContentValues(paramCursor, paramString, paramContentValues, paramString);
    }

    public static void cursorLongToContentValues(Cursor paramCursor, String paramString1, ContentValues paramContentValues, String paramString2)
    {
        int i = paramCursor.getColumnIndex(paramString1);
        if (!paramCursor.isNull(i))
            paramContentValues.put(paramString2, Long.valueOf(paramCursor.getLong(i)));
        while (true)
        {
            return;
            paramContentValues.put(paramString2, (Long)null);
        }
    }

    public static void cursorLongToContentValuesIfPresent(Cursor paramCursor, ContentValues paramContentValues, String paramString)
    {
        int i = paramCursor.getColumnIndex(paramString);
        if ((i != -1) && (!paramCursor.isNull(i)))
            paramContentValues.put(paramString, Long.valueOf(paramCursor.getLong(i)));
    }

    public static int cursorPickFillWindowStartPosition(int paramInt1, int paramInt2)
    {
        return Math.max(paramInt1 - paramInt2 / 3, 0);
    }

    public static void cursorRowToContentValues(Cursor paramCursor, ContentValues paramContentValues)
    {
        AbstractWindowedCursor localAbstractWindowedCursor;
        String[] arrayOfString;
        int j;
        if ((paramCursor instanceof AbstractWindowedCursor))
        {
            localAbstractWindowedCursor = (AbstractWindowedCursor)paramCursor;
            arrayOfString = paramCursor.getColumnNames();
            int i = arrayOfString.length;
            j = 0;
            label26: if (j >= i)
                return;
            if ((localAbstractWindowedCursor == null) || (!localAbstractWindowedCursor.isBlob(j)))
                break label73;
            paramContentValues.put(arrayOfString[j], paramCursor.getBlob(j));
        }
        while (true)
        {
            j++;
            break label26;
            localAbstractWindowedCursor = null;
            break;
            label73: paramContentValues.put(arrayOfString[j], paramCursor.getString(j));
        }
    }

    public static void cursorShortToContentValuesIfPresent(Cursor paramCursor, ContentValues paramContentValues, String paramString)
    {
        int i = paramCursor.getColumnIndex(paramString);
        if ((i != -1) && (!paramCursor.isNull(i)))
            paramContentValues.put(paramString, Short.valueOf(paramCursor.getShort(i)));
    }

    public static void cursorStringToContentValues(Cursor paramCursor, String paramString, ContentValues paramContentValues)
    {
        cursorStringToContentValues(paramCursor, paramString, paramContentValues, paramString);
    }

    public static void cursorStringToContentValues(Cursor paramCursor, String paramString1, ContentValues paramContentValues, String paramString2)
    {
        paramContentValues.put(paramString2, paramCursor.getString(paramCursor.getColumnIndexOrThrow(paramString1)));
    }

    public static void cursorStringToContentValuesIfPresent(Cursor paramCursor, ContentValues paramContentValues, String paramString)
    {
        int i = paramCursor.getColumnIndex(paramString);
        if ((i != -1) && (!paramCursor.isNull(i)))
            paramContentValues.put(paramString, paramCursor.getString(i));
    }

    public static void cursorStringToInsertHelper(Cursor paramCursor, String paramString, InsertHelper paramInsertHelper, int paramInt)
    {
        paramInsertHelper.bind(paramInt, paramCursor.getString(paramCursor.getColumnIndexOrThrow(paramString)));
    }

    public static void dumpCurrentRow(Cursor paramCursor)
    {
        dumpCurrentRow(paramCursor, System.out);
    }

    public static void dumpCurrentRow(Cursor paramCursor, PrintStream paramPrintStream)
    {
        String[] arrayOfString = paramCursor.getColumnNames();
        paramPrintStream.println("" + paramCursor.getPosition() + " {");
        int i = arrayOfString.length;
        int j = 0;
        while (true)
            if (j < i)
                try
                {
                    String str2 = paramCursor.getString(j);
                    str1 = str2;
                    paramPrintStream.println("     " + arrayOfString[j] + '=' + str1);
                    j++;
                }
                catch (SQLiteException localSQLiteException)
                {
                    while (true)
                        String str1 = "<unprintable>";
                }
        paramPrintStream.println("}");
    }

    public static void dumpCurrentRow(Cursor paramCursor, StringBuilder paramStringBuilder)
    {
        String[] arrayOfString = paramCursor.getColumnNames();
        paramStringBuilder.append("" + paramCursor.getPosition() + " {\n");
        int i = arrayOfString.length;
        int j = 0;
        while (true)
            if (j < i)
                try
                {
                    String str2 = paramCursor.getString(j);
                    str1 = str2;
                    paramStringBuilder.append("     " + arrayOfString[j] + '=' + str1 + "\n");
                    j++;
                }
                catch (SQLiteException localSQLiteException)
                {
                    while (true)
                        String str1 = "<unprintable>";
                }
        paramStringBuilder.append("}\n");
    }

    public static String dumpCurrentRowToString(Cursor paramCursor)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        dumpCurrentRow(paramCursor, localStringBuilder);
        return localStringBuilder.toString();
    }

    public static void dumpCursor(Cursor paramCursor)
    {
        dumpCursor(paramCursor, System.out);
    }

    public static void dumpCursor(Cursor paramCursor, PrintStream paramPrintStream)
    {
        paramPrintStream.println(">>>>> Dumping cursor " + paramCursor);
        if (paramCursor != null)
        {
            int i = paramCursor.getPosition();
            paramCursor.moveToPosition(-1);
            while (paramCursor.moveToNext())
                dumpCurrentRow(paramCursor, paramPrintStream);
            paramCursor.moveToPosition(i);
        }
        paramPrintStream.println("<<<<<");
    }

    public static void dumpCursor(Cursor paramCursor, StringBuilder paramStringBuilder)
    {
        paramStringBuilder.append(">>>>> Dumping cursor " + paramCursor + "\n");
        if (paramCursor != null)
        {
            int i = paramCursor.getPosition();
            paramCursor.moveToPosition(-1);
            while (paramCursor.moveToNext())
                dumpCurrentRow(paramCursor, paramStringBuilder);
            paramCursor.moveToPosition(i);
        }
        paramStringBuilder.append("<<<<<\n");
    }

    public static String dumpCursorToString(Cursor paramCursor)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        dumpCursor(paramCursor, localStringBuilder);
        return localStringBuilder.toString();
    }

    public static int findRowIdColumnIndex(String[] paramArrayOfString)
    {
        int i = paramArrayOfString.length;
        int j = 0;
        if (j < i)
            if (!paramArrayOfString[j].equals("_id"));
        while (true)
        {
            return j;
            j++;
            break;
            j = -1;
        }
    }

    public static String getCollationKey(String paramString)
    {
        byte[] arrayOfByte = getCollationKeyInBytes(paramString);
        try
        {
            str = new String(arrayOfByte, 0, getKeyLen(arrayOfByte), "ISO8859_1");
            return str;
        }
        catch (Exception localException)
        {
            while (true)
                String str = "";
        }
    }

    private static byte[] getCollationKeyInBytes(String paramString)
    {
        if (mColl == null)
        {
            mColl = Collator.getInstance();
            mColl.setStrength(0);
        }
        return mColl.getCollationKey(paramString).toByteArray();
    }

    public static String getHexCollationKey(String paramString)
    {
        byte[] arrayOfByte = getCollationKeyInBytes(paramString);
        return new String(Hex.encodeHex(arrayOfByte), 0, 2 * getKeyLen(arrayOfByte));
    }

    private static int getKeyLen(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte[(-1 + paramArrayOfByte.length)] != 0);
        for (int i = paramArrayOfByte.length; ; i = -1 + paramArrayOfByte.length)
            return i;
    }

    public static int getSqlStatementType(String paramString)
    {
        int i = 99;
        String str1 = paramString.trim();
        if (str1.length() < 3);
        while (true)
        {
            return i;
            String str2 = str1.substring(0, 3).toUpperCase(Locale.US);
            if (str2.equals("SEL"))
                i = 1;
            else if ((str2.equals("INS")) || (str2.equals("UPD")) || (str2.equals("REP")) || (str2.equals("DEL")))
                i = 2;
            else if (str2.equals("ATT"))
                i = 3;
            else if (str2.equals("COM"))
                i = 5;
            else if (str2.equals("END"))
                i = 5;
            else if (str2.equals("ROL"))
                i = 6;
            else if (str2.equals("BEG"))
                i = 4;
            else if (str2.equals("PRA"))
                i = 7;
            else if ((str2.equals("CRE")) || (str2.equals("DRO")) || (str2.equals("ALT")))
                i = 8;
            else if ((str2.equals("ANA")) || (str2.equals("DET")))
                i = 9;
        }
    }

    public static int getTypeOfObject(Object paramObject)
    {
        int i;
        if (paramObject == null)
            i = 0;
        while (true)
        {
            return i;
            if ((paramObject instanceof byte[]))
                i = 4;
            else if (((paramObject instanceof Float)) || ((paramObject instanceof Double)))
                i = 2;
            else if (((paramObject instanceof Long)) || ((paramObject instanceof Integer)) || ((paramObject instanceof Short)) || ((paramObject instanceof Byte)))
                i = 1;
            else
                i = 3;
        }
    }

    public static long longForQuery(SQLiteDatabase paramSQLiteDatabase, String paramString, String[] paramArrayOfString)
    {
        SQLiteStatement localSQLiteStatement = paramSQLiteDatabase.compileStatement(paramString);
        try
        {
            long l = longForQuery(localSQLiteStatement, paramArrayOfString);
            return l;
        }
        finally
        {
            localSQLiteStatement.close();
        }
    }

    public static long longForQuery(SQLiteStatement paramSQLiteStatement, String[] paramArrayOfString)
    {
        paramSQLiteStatement.bindAllArgsAsStrings(paramArrayOfString);
        return paramSQLiteStatement.simpleQueryForLong();
    }

    public static long queryNumEntries(SQLiteDatabase paramSQLiteDatabase, String paramString)
    {
        return queryNumEntries(paramSQLiteDatabase, paramString, null, null);
    }

    public static long queryNumEntries(SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2)
    {
        return queryNumEntries(paramSQLiteDatabase, paramString1, paramString2, null);
    }

    public static long queryNumEntries(SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2, String[] paramArrayOfString)
    {
        if (!TextUtils.isEmpty(paramString2));
        for (String str = " where " + paramString2; ; str = "")
            return longForQuery(paramSQLiteDatabase, "select count(*) from " + paramString1 + str, paramArrayOfString);
    }

    public static final void readExceptionFromParcel(Parcel paramParcel)
    {
        int i = paramParcel.readExceptionCode();
        if (i == 0);
        while (true)
        {
            return;
            readExceptionFromParcel(paramParcel, paramParcel.readString(), i);
        }
    }

    private static final void readExceptionFromParcel(Parcel paramParcel, String paramString, int paramInt)
    {
        switch (paramInt)
        {
        case 10:
        default:
            paramParcel.readException(paramInt, paramString);
            return;
        case 2:
            throw new IllegalArgumentException(paramString);
        case 3:
            throw new UnsupportedOperationException(paramString);
        case 4:
            throw new SQLiteAbortException(paramString);
        case 5:
            throw new SQLiteConstraintException(paramString);
        case 6:
            throw new SQLiteDatabaseCorruptException(paramString);
        case 7:
            throw new SQLiteFullException(paramString);
        case 8:
            throw new SQLiteDiskIOException(paramString);
        case 9:
            throw new SQLiteException(paramString);
        case 11:
        }
        throw new OperationCanceledException(paramString);
    }

    public static void readExceptionWithFileNotFoundExceptionFromParcel(Parcel paramParcel)
        throws FileNotFoundException
    {
        int i = paramParcel.readExceptionCode();
        if (i == 0);
        while (true)
        {
            return;
            String str = paramParcel.readString();
            if (i == 1)
                throw new FileNotFoundException(str);
            readExceptionFromParcel(paramParcel, str, i);
        }
    }

    public static void readExceptionWithOperationApplicationExceptionFromParcel(Parcel paramParcel)
        throws OperationApplicationException
    {
        int i = paramParcel.readExceptionCode();
        if (i == 0);
        while (true)
        {
            return;
            String str = paramParcel.readString();
            if (i == 10)
                throw new OperationApplicationException(str);
            readExceptionFromParcel(paramParcel, str, i);
        }
    }

    public static String sqlEscapeString(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        appendEscapedSQLString(localStringBuilder, paramString);
        return localStringBuilder.toString();
    }

    public static String stringForQuery(SQLiteDatabase paramSQLiteDatabase, String paramString, String[] paramArrayOfString)
    {
        SQLiteStatement localSQLiteStatement = paramSQLiteDatabase.compileStatement(paramString);
        try
        {
            String str = stringForQuery(localSQLiteStatement, paramArrayOfString);
            return str;
        }
        finally
        {
            localSQLiteStatement.close();
        }
    }

    public static String stringForQuery(SQLiteStatement paramSQLiteStatement, String[] paramArrayOfString)
    {
        paramSQLiteStatement.bindAllArgsAsStrings(paramArrayOfString);
        return paramSQLiteStatement.simpleQueryForString();
    }

    public static final void writeExceptionToParcel(Parcel paramParcel, Exception paramException)
    {
        int i = 1;
        int j;
        if ((paramException instanceof FileNotFoundException))
        {
            j = 1;
            i = 0;
            paramParcel.writeInt(j);
            paramParcel.writeString(paramException.getMessage());
            if (i != 0)
                Log.e("DatabaseUtils", "Writing exception to parcel", paramException);
        }
        while (true)
        {
            return;
            if ((paramException instanceof IllegalArgumentException))
            {
                j = 2;
                break;
            }
            if ((paramException instanceof UnsupportedOperationException))
            {
                j = 3;
                break;
            }
            if ((paramException instanceof SQLiteAbortException))
            {
                j = 4;
                break;
            }
            if ((paramException instanceof SQLiteConstraintException))
            {
                j = 5;
                break;
            }
            if ((paramException instanceof SQLiteDatabaseCorruptException))
            {
                j = 6;
                break;
            }
            if ((paramException instanceof SQLiteFullException))
            {
                j = 7;
                break;
            }
            if ((paramException instanceof SQLiteDiskIOException))
            {
                j = 8;
                break;
            }
            if ((paramException instanceof SQLiteException))
            {
                j = 9;
                break;
            }
            if ((paramException instanceof OperationApplicationException))
            {
                j = 10;
                break;
            }
            if ((paramException instanceof OperationCanceledException))
            {
                j = 11;
                i = 0;
                break;
            }
            paramParcel.writeException(paramException);
            Log.e("DatabaseUtils", "Writing exception to parcel", paramException);
        }
    }

    public static class InsertHelper
    {
        public static final int TABLE_INFO_PRAGMA_COLUMNNAME_INDEX = 1;
        public static final int TABLE_INFO_PRAGMA_DEFAULT_INDEX = 4;
        private HashMap<String, Integer> mColumns;
        private final SQLiteDatabase mDb;
        private String mInsertSQL = null;
        private SQLiteStatement mInsertStatement = null;
        private SQLiteStatement mPreparedStatement = null;
        private SQLiteStatement mReplaceStatement = null;
        private final String mTableName;

        public InsertHelper(SQLiteDatabase paramSQLiteDatabase, String paramString)
        {
            this.mDb = paramSQLiteDatabase;
            this.mTableName = paramString;
        }

        private void buildSQL()
            throws SQLException
        {
            StringBuilder localStringBuilder1 = new StringBuilder(128);
            localStringBuilder1.append("INSERT INTO ");
            localStringBuilder1.append(this.mTableName);
            localStringBuilder1.append(" (");
            StringBuilder localStringBuilder2 = new StringBuilder(128);
            localStringBuilder2.append("VALUES (");
            int i = 1;
            Cursor localCursor = null;
            while (true)
            {
                try
                {
                    localCursor = this.mDb.rawQuery("PRAGMA table_info(" + this.mTableName + ")", null);
                    this.mColumns = new HashMap(localCursor.getCount());
                    if (!localCursor.moveToNext())
                        break;
                    String str1 = localCursor.getString(1);
                    String str2 = localCursor.getString(4);
                    this.mColumns.put(str1, Integer.valueOf(i));
                    localStringBuilder1.append("'");
                    localStringBuilder1.append(str1);
                    localStringBuilder1.append("'");
                    if (str2 == null)
                    {
                        localStringBuilder2.append("?");
                        if (i != localCursor.getCount())
                            break label291;
                        str3 = ") ";
                        localStringBuilder1.append(str3);
                        if (i != localCursor.getCount())
                            break label298;
                        str4 = ");";
                        localStringBuilder2.append(str4);
                        i++;
                        continue;
                    }
                    localStringBuilder2.append("COALESCE(?, ");
                    localStringBuilder2.append(str2);
                    localStringBuilder2.append(")");
                    continue;
                }
                finally
                {
                    if (localCursor != null)
                        localCursor.close();
                }
                label291: String str3 = ", ";
                continue;
                label298: String str4 = ", ";
            }
            if (localCursor != null)
                localCursor.close();
            localStringBuilder1.append(localStringBuilder2);
            this.mInsertSQL = localStringBuilder1.toString();
        }

        private SQLiteStatement getStatement(boolean paramBoolean)
            throws SQLException
        {
            if (paramBoolean)
                if (this.mReplaceStatement == null)
                {
                    if (this.mInsertSQL == null)
                        buildSQL();
                    String str = "INSERT OR REPLACE" + this.mInsertSQL.substring(6);
                    this.mReplaceStatement = this.mDb.compileStatement(str);
                }
            for (SQLiteStatement localSQLiteStatement = this.mReplaceStatement; ; localSQLiteStatement = this.mInsertStatement)
            {
                return localSQLiteStatement;
                if (this.mInsertStatement == null)
                {
                    if (this.mInsertSQL == null)
                        buildSQL();
                    this.mInsertStatement = this.mDb.compileStatement(this.mInsertSQL);
                }
            }
        }

        /** @deprecated */
        private long insertInternal(ContentValues paramContentValues, boolean paramBoolean)
        {
            try
            {
                localSQLiteStatement = getStatement(paramBoolean);
                localSQLiteStatement.clearBindings();
                Iterator localIterator = paramContentValues.valueSet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    DatabaseUtils.bindObjectToProgram(localSQLiteStatement, getColumnIndex((String)localEntry.getKey()), localEntry.getValue());
                }
            }
            catch (SQLException localSQLException)
            {
                SQLiteStatement localSQLiteStatement;
                Log.e("DatabaseUtils", "Error inserting " + paramContentValues + " into table    " + this.mTableName, localSQLException);
                long l2;
                for (long l1 = -1L; ; l1 = l2)
                {
                    return l1;
                    l2 = localSQLiteStatement.executeInsert();
                }
            }
            finally
            {
            }
        }

        public void bind(int paramInt, double paramDouble)
        {
            this.mPreparedStatement.bindDouble(paramInt, paramDouble);
        }

        public void bind(int paramInt, float paramFloat)
        {
            this.mPreparedStatement.bindDouble(paramInt, paramFloat);
        }

        public void bind(int paramInt1, int paramInt2)
        {
            this.mPreparedStatement.bindLong(paramInt1, paramInt2);
        }

        public void bind(int paramInt, long paramLong)
        {
            this.mPreparedStatement.bindLong(paramInt, paramLong);
        }

        public void bind(int paramInt, String paramString)
        {
            if (paramString == null)
                this.mPreparedStatement.bindNull(paramInt);
            while (true)
            {
                return;
                this.mPreparedStatement.bindString(paramInt, paramString);
            }
        }

        public void bind(int paramInt, boolean paramBoolean)
        {
            SQLiteStatement localSQLiteStatement = this.mPreparedStatement;
            if (paramBoolean);
            for (long l = 1L; ; l = 0L)
            {
                localSQLiteStatement.bindLong(paramInt, l);
                return;
            }
        }

        public void bind(int paramInt, byte[] paramArrayOfByte)
        {
            if (paramArrayOfByte == null)
                this.mPreparedStatement.bindNull(paramInt);
            while (true)
            {
                return;
                this.mPreparedStatement.bindBlob(paramInt, paramArrayOfByte);
            }
        }

        public void bindNull(int paramInt)
        {
            this.mPreparedStatement.bindNull(paramInt);
        }

        public void close()
        {
            if (this.mInsertStatement != null)
            {
                this.mInsertStatement.close();
                this.mInsertStatement = null;
            }
            if (this.mReplaceStatement != null)
            {
                this.mReplaceStatement.close();
                this.mReplaceStatement = null;
            }
            this.mInsertSQL = null;
            this.mColumns = null;
        }

        public long execute()
        {
            if (this.mPreparedStatement == null)
                throw new IllegalStateException("you must prepare this inserter before calling execute");
            try
            {
                long l2 = this.mPreparedStatement.executeInsert();
                l1 = l2;
                return l1;
            }
            catch (SQLException localSQLException)
            {
                while (true)
                {
                    Log.e("DatabaseUtils", "Error executing InsertHelper with table " + this.mTableName, localSQLException);
                    long l1 = -1L;
                    this.mPreparedStatement = null;
                }
            }
            finally
            {
                this.mPreparedStatement = null;
            }
        }

        public int getColumnIndex(String paramString)
        {
            getStatement(false);
            Integer localInteger = (Integer)this.mColumns.get(paramString);
            if (localInteger == null)
                throw new IllegalArgumentException("column '" + paramString + "' is invalid");
            return localInteger.intValue();
        }

        public long insert(ContentValues paramContentValues)
        {
            return insertInternal(paramContentValues, false);
        }

        public void prepareForInsert()
        {
            this.mPreparedStatement = getStatement(false);
            this.mPreparedStatement.clearBindings();
        }

        public void prepareForReplace()
        {
            this.mPreparedStatement = getStatement(true);
            this.mPreparedStatement.clearBindings();
        }

        public long replace(ContentValues paramContentValues)
        {
            return insertInternal(paramContentValues, true);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.DatabaseUtils
 * JD-Core Version:        0.6.2
 */