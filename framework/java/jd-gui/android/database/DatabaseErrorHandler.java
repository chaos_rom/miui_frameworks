package android.database;

import android.database.sqlite.SQLiteDatabase;

public abstract interface DatabaseErrorHandler
{
    public abstract void onCorruption(SQLiteDatabase paramSQLiteDatabase);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.DatabaseErrorHandler
 * JD-Core Version:        0.6.2
 */