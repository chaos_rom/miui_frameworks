package android.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class BulkCursorDescriptor
    implements Parcelable
{
    public static final Parcelable.Creator<BulkCursorDescriptor> CREATOR = new Parcelable.Creator()
    {
        public BulkCursorDescriptor createFromParcel(Parcel paramAnonymousParcel)
        {
            BulkCursorDescriptor localBulkCursorDescriptor = new BulkCursorDescriptor();
            localBulkCursorDescriptor.readFromParcel(paramAnonymousParcel);
            return localBulkCursorDescriptor;
        }

        public BulkCursorDescriptor[] newArray(int paramAnonymousInt)
        {
            return new BulkCursorDescriptor[paramAnonymousInt];
        }
    };
    public String[] columnNames;
    public int count;
    public IBulkCursor cursor;
    public boolean wantsAllOnMoveCalls;
    public CursorWindow window;

    public int describeContents()
    {
        return 0;
    }

    public void readFromParcel(Parcel paramParcel)
    {
        this.cursor = BulkCursorNative.asInterface(paramParcel.readStrongBinder());
        this.columnNames = paramParcel.readStringArray();
        if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.wantsAllOnMoveCalls = bool;
            this.count = paramParcel.readInt();
            if (paramParcel.readInt() != 0)
                this.window = ((CursorWindow)CursorWindow.CREATOR.createFromParcel(paramParcel));
            return;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStrongBinder(this.cursor.asBinder());
        paramParcel.writeStringArray(this.columnNames);
        int i;
        if (this.wantsAllOnMoveCalls)
        {
            i = 1;
            paramParcel.writeInt(i);
            paramParcel.writeInt(this.count);
            if (this.window == null)
                break label70;
            paramParcel.writeInt(1);
            this.window.writeToParcel(paramParcel, paramInt);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label70: paramParcel.writeInt(0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.database.BulkCursorDescriptor
 * JD-Core Version:        0.6.2
 */