package com.google.android.util;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import java.util.ArrayList;

public class SmileyParser extends AbstractMessageParser
{
    private SmileyResources mRes;

    public SmileyParser(String paramString, SmileyResources paramSmileyResources)
    {
        super(paramString, true, false, false, false, false, false);
        this.mRes = paramSmileyResources;
    }

    protected AbstractMessageParser.Resources getResources()
    {
        return this.mRes;
    }

    public CharSequence getSpannableString(Context paramContext)
    {
        Object localObject = new SpannableStringBuilder();
        if (getPartCount() == 0)
            localObject = "";
        while (true)
        {
            return localObject;
            ArrayList localArrayList = getPart(0).getTokens();
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
            {
                AbstractMessageParser.Token localToken = (AbstractMessageParser.Token)localArrayList.get(j);
                int k = ((SpannableStringBuilder)localObject).length();
                ((SpannableStringBuilder)localObject).append(localToken.getRawText());
                if (localToken.getType() == AbstractMessageParser.Token.Type.SMILEY)
                {
                    int m = this.mRes.getSmileyRes(localToken.getRawText());
                    if (m != -1)
                        ((SpannableStringBuilder)localObject).setSpan(new ImageSpan(paramContext, m), k, ((SpannableStringBuilder)localObject).length(), 33);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.util.SmileyParser
 * JD-Core Version:        0.6.2
 */