package com.google.android.util;

import java.util.HashMap;
import java.util.Set;

public class SmileyResources
    implements AbstractMessageParser.Resources
{
    private HashMap<String, Integer> mSmileyToRes = new HashMap();
    private final AbstractMessageParser.TrieNode smileys = new AbstractMessageParser.TrieNode();

    public SmileyResources(String[] paramArrayOfString, int[] paramArrayOfInt)
    {
        for (int i = 0; i < paramArrayOfString.length; i++)
        {
            AbstractMessageParser.TrieNode.addToTrie(this.smileys, paramArrayOfString[i], "");
            this.mSmileyToRes.put(paramArrayOfString[i], Integer.valueOf(paramArrayOfInt[i]));
        }
    }

    public AbstractMessageParser.TrieNode getAcronyms()
    {
        return null;
    }

    public AbstractMessageParser.TrieNode getDomainSuffixes()
    {
        return null;
    }

    public Set<String> getSchemes()
    {
        return null;
    }

    public int getSmileyRes(String paramString)
    {
        Integer localInteger = (Integer)this.mSmileyToRes.get(paramString);
        if (localInteger == null);
        for (int i = -1; ; i = localInteger.intValue())
            return i;
    }

    public AbstractMessageParser.TrieNode getSmileys()
    {
        return this.smileys;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.util.SmileyResources
 * JD-Core Version:        0.6.2
 */