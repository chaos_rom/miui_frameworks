package com.google.android.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractMessageParser
{
    public static final String musicNote = "♫ ";
    private HashMap<Character, Format> formatStart;
    private int nextChar;
    private int nextClass;
    private boolean parseAcronyms;
    private boolean parseFormatting;
    private boolean parseMeText;
    private boolean parseMusic;
    private boolean parseSmilies;
    private boolean parseUrls;
    private ArrayList<Part> parts;
    private String text;
    private ArrayList<Token> tokens;

    public AbstractMessageParser(String paramString)
    {
        this(paramString, true, true, true, true, true, true);
    }

    public AbstractMessageParser(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6)
    {
        this.text = paramString;
        this.nextChar = 0;
        this.nextClass = 10;
        this.parts = new ArrayList();
        this.tokens = new ArrayList();
        this.formatStart = new HashMap();
        this.parseSmilies = paramBoolean1;
        this.parseAcronyms = paramBoolean2;
        this.parseFormatting = paramBoolean3;
        this.parseUrls = paramBoolean4;
        this.parseMusic = paramBoolean5;
        this.parseMeText = paramBoolean6;
    }

    private void addToken(Token paramToken)
    {
        this.tokens.add(paramToken);
    }

    private void addURLToken(String paramString1, String paramString2)
    {
        addToken(tokenForUrl(paramString1, paramString2));
    }

    private void buildParts(String paramString)
    {
        for (int i = 0; i < this.tokens.size(); i++)
        {
            Token localToken = (Token)this.tokens.get(i);
            if ((localToken.isMedia()) || (this.parts.size() == 0) || (lastPart().isMedia()))
                this.parts.add(new Part());
            lastPart().add(localToken);
        }
        if (this.parts.size() > 0)
            ((Part)this.parts.get(0)).setMeText(paramString);
    }

    private int getCharClass(int paramInt)
    {
        int i;
        if ((paramInt < 0) || (this.text.length() <= paramInt))
            i = 0;
        while (true)
        {
            return i;
            char c = this.text.charAt(paramInt);
            if (Character.isWhitespace(c))
            {
                i = 1;
            }
            else if (Character.isLetter(c))
            {
                i = 2;
            }
            else if (Character.isDigit(c))
            {
                i = 3;
            }
            else if (isPunctuation(c))
            {
                i = 1 + this.nextClass;
                this.nextClass = i;
            }
            else
            {
                i = 4;
            }
        }
    }

    private boolean isDomainChar(char paramChar)
    {
        if ((paramChar == '-') || (Character.isLetter(paramChar)) || (Character.isDigit(paramChar)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isFormatChar(char paramChar)
    {
        switch (paramChar)
        {
        default:
        case '*':
        case '^':
        case '_':
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private static boolean isPunctuation(char paramChar)
    {
        switch (paramChar)
        {
        default:
        case '!':
        case '"':
        case '(':
        case ')':
        case ',':
        case '.':
        case ':':
        case ';':
        case '?':
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private static boolean isSmileyBreak(char paramChar1, char paramChar2)
    {
        switch (paramChar1)
        {
        default:
        case '$':
        case '&':
        case '*':
        case '+':
        case '-':
        case '/':
        case '<':
        case '=':
        case '>':
        case '@':
        case '[':
        case '\\':
        case ']':
        case '^':
        case '|':
        case '}':
        case '~':
        }
        label148: for (boolean bool = false; ; bool = true)
        {
            return bool;
            switch (paramChar2)
            {
            default:
                break label148;
            case '#':
            case '$':
            case '%':
            case '*':
            case '/':
            case '<':
            case '=':
            case '>':
            case '@':
            case '[':
            case '\\':
            case '^':
            case '~':
            }
        }
    }

    private boolean isSmileyBreak(int paramInt)
    {
        if ((paramInt > 0) && (paramInt < this.text.length()) && (isSmileyBreak(this.text.charAt(paramInt - 1), this.text.charAt(paramInt))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isURLBreak(int paramInt)
    {
        switch (getCharClass(paramInt - 1))
        {
        default:
        case 2:
        case 3:
        case 4:
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isValidDomain(String paramString)
    {
        if (matches(getResources().getDomainSuffixes(), reverse(paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isWordBreak(int paramInt)
    {
        if (getCharClass(paramInt - 1) != getCharClass(paramInt));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private Part lastPart()
    {
        return (Part)this.parts.get(-1 + this.parts.size());
    }

    private static TrieNode longestMatch(TrieNode paramTrieNode, AbstractMessageParser paramAbstractMessageParser, int paramInt)
    {
        return longestMatch(paramTrieNode, paramAbstractMessageParser, paramInt, false);
    }

    private static TrieNode longestMatch(TrieNode paramTrieNode, AbstractMessageParser paramAbstractMessageParser, int paramInt, boolean paramBoolean)
    {
        int i = paramInt;
        TrieNode localTrieNode = null;
        while (true)
        {
            int j;
            if (i < paramAbstractMessageParser.getRawText().length())
            {
                String str = paramAbstractMessageParser.getRawText();
                j = i + 1;
                paramTrieNode = paramTrieNode.getChild(str.charAt(i));
                if (paramTrieNode != null);
            }
            else
            {
                return localTrieNode;
            }
            if (paramTrieNode.exists())
            {
                if (paramAbstractMessageParser.isWordBreak(j))
                {
                    localTrieNode = paramTrieNode;
                    i = j;
                }
                else if ((paramBoolean) && (paramAbstractMessageParser.isSmileyBreak(j)))
                {
                    localTrieNode = paramTrieNode;
                    i = j;
                }
            }
            else
                i = j;
        }
    }

    private static boolean matches(TrieNode paramTrieNode, String paramString)
    {
        int j;
        for (int i = 0; ; i = j)
        {
            if (i < paramString.length())
            {
                j = i + 1;
                paramTrieNode = paramTrieNode.getChild(paramString.charAt(i));
                if (paramTrieNode != null)
                    break label33;
            }
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                label33: if (!paramTrieNode.exists())
                    break;
            }
        }
    }

    private boolean parseAcronym()
    {
        boolean bool = false;
        if (!this.parseAcronyms);
        while (true)
        {
            return bool;
            TrieNode localTrieNode = longestMatch(getResources().getAcronyms(), this, this.nextChar);
            if (localTrieNode != null)
            {
                addToken(new Acronym(localTrieNode.getText(), localTrieNode.getValue()));
                this.nextChar += localTrieNode.getText().length();
                bool = true;
            }
        }
    }

    private boolean parseFormatting()
    {
        boolean bool = false;
        if (!this.parseFormatting);
        while (true)
        {
            return bool;
            for (int i = this.nextChar; (i < this.text.length()) && (isFormatChar(this.text.charAt(i))); i++);
            if ((i != this.nextChar) && (isWordBreak(i)))
            {
                LinkedHashMap localLinkedHashMap = new LinkedHashMap();
                int j = this.nextChar;
                if (j < i)
                {
                    char c = this.text.charAt(j);
                    Character localCharacter2 = Character.valueOf(c);
                    if (localLinkedHashMap.containsKey(localCharacter2))
                        addToken(new Format(c, false));
                    while (true)
                    {
                        j++;
                        break;
                        Format localFormat2 = (Format)this.formatStart.get(localCharacter2);
                        if (localFormat2 != null)
                        {
                            localFormat2.setMatched(true);
                            this.formatStart.remove(localCharacter2);
                            localLinkedHashMap.put(localCharacter2, Boolean.TRUE);
                        }
                        else
                        {
                            Format localFormat3 = new Format(c, true);
                            this.formatStart.put(localCharacter2, localFormat3);
                            addToken(localFormat3);
                            localLinkedHashMap.put(localCharacter2, Boolean.FALSE);
                        }
                    }
                }
                Iterator localIterator = localLinkedHashMap.keySet().iterator();
                while (localIterator.hasNext())
                {
                    Character localCharacter1 = (Character)localIterator.next();
                    if (localLinkedHashMap.get(localCharacter1) == Boolean.TRUE)
                    {
                        Format localFormat1 = new Format(localCharacter1.charValue(), false);
                        localFormat1.setMatched(true);
                        addToken(localFormat1);
                    }
                }
                this.nextChar = i;
                bool = true;
            }
        }
    }

    private boolean parseMusicTrack()
    {
        if ((this.parseMusic) && (this.text.startsWith("♫ ")))
        {
            addToken(new MusicTrack(this.text.substring("♫ ".length())));
            this.nextChar = this.text.length();
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean parseSmiley()
    {
        boolean bool = false;
        if (!this.parseSmilies);
        while (true)
        {
            return bool;
            TrieNode localTrieNode = longestMatch(getResources().getSmileys(), this, this.nextChar, true);
            if (localTrieNode != null)
            {
                int i = getCharClass(-1 + this.nextChar);
                int j = getCharClass(this.nextChar + localTrieNode.getText().length());
                if (((i != 2) && (i != 3)) || ((j != 2) && (j != 3)))
                {
                    addToken(new Smiley(localTrieNode.getText()));
                    this.nextChar += localTrieNode.getText().length();
                    bool = true;
                }
            }
        }
    }

    private void parseText()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = this.nextChar;
        String str = this.text;
        int j = this.nextChar;
        this.nextChar = (j + 1);
        char c = str.charAt(j);
        switch (c)
        {
        default:
            localStringBuilder.append(c);
        case '<':
        case '>':
        case '&':
        case '"':
        case '\'':
        case '\n':
        }
        while (isWordBreak(this.nextChar))
        {
            addToken(new Html(this.text.substring(i, this.nextChar), localStringBuilder.toString()));
            return;
            localStringBuilder.append("&lt;");
            continue;
            localStringBuilder.append("&gt;");
            continue;
            localStringBuilder.append("&amp;");
            continue;
            localStringBuilder.append("&quot;");
            continue;
            localStringBuilder.append("&apos;");
            continue;
            localStringBuilder.append("<br>");
        }
    }

    private boolean parseURL()
    {
        boolean bool = false;
        if ((!this.parseUrls) || (!isURLBreak(this.nextChar)));
        while (true)
        {
            return bool;
            int i = this.nextChar;
            for (int j = i; (j < this.text.length()) && (isDomainChar(this.text.charAt(j))); j++);
            String str1 = "";
            int k = 0;
            if (j != this.text.length())
                if (this.text.charAt(j) == ':')
                {
                    String str3 = this.text.substring(this.nextChar, j);
                    if (getResources().getSchemes().contains(str3))
                        label126: if (k == 0)
                            while ((j < this.text.length()) && (!Character.isWhitespace(this.text.charAt(j))))
                                j++;
                }
                else if (this.text.charAt(j) == '.')
                {
                    while (true)
                    {
                        if (j < this.text.length())
                        {
                            char c3 = this.text.charAt(j);
                            if ((c3 == '.') || (isDomainChar(c3)));
                        }
                        else
                        {
                            if (!isValidDomain(this.text.substring(this.nextChar, j)))
                                break;
                            if ((j + 1 >= this.text.length()) || (this.text.charAt(j) != ':') || (!Character.isDigit(this.text.charAt(j + 1))))
                                break label313;
                            j++;
                            while ((j < this.text.length()) && (Character.isDigit(this.text.charAt(j))))
                                j++;
                        }
                        j++;
                    }
                    label313: if (j == this.text.length())
                        k = 1;
                    while (true)
                    {
                        str1 = "http://";
                        break label126;
                        char c1 = this.text.charAt(j);
                        if (c1 == '?')
                        {
                            if (j + 1 == this.text.length())
                            {
                                k = 1;
                            }
                            else
                            {
                                char c2 = this.text.charAt(j + 1);
                                if ((Character.isWhitespace(c2)) || (isPunctuation(c2)))
                                    k = 1;
                            }
                        }
                        else if (isPunctuation(c1))
                            k = 1;
                        else if (Character.isWhitespace(c1))
                            k = 1;
                        else if (c1 != '/')
                            if (c1 != '#')
                                break;
                    }
                    String str2 = this.text.substring(i, j);
                    addURLToken(str1 + str2, str2);
                    this.nextChar = j;
                    bool = true;
                }
        }
    }

    protected static String reverse(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        for (int i = -1 + paramString.length(); i >= 0; i--)
            localStringBuilder.append(paramString.charAt(i));
        return localStringBuilder.toString();
    }

    public static Token tokenForUrl(String paramString1, String paramString2)
    {
        Object localObject;
        if (paramString1 == null)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = Video.matchURL(paramString1, paramString2);
            if (localObject == null)
            {
                YouTubeVideo localYouTubeVideo = YouTubeVideo.matchURL(paramString1, paramString2);
                if (localYouTubeVideo != null)
                {
                    localObject = localYouTubeVideo;
                }
                else
                {
                    Photo localPhoto = Photo.matchURL(paramString1, paramString2);
                    if (localPhoto != null)
                    {
                        localObject = localPhoto;
                    }
                    else
                    {
                        FlickrPhoto localFlickrPhoto = FlickrPhoto.matchURL(paramString1, paramString2);
                        if (localFlickrPhoto != null)
                            localObject = localFlickrPhoto;
                        else
                            localObject = new Link(paramString1, paramString2);
                    }
                }
            }
        }
    }

    public final Part getPart(int paramInt)
    {
        return (Part)this.parts.get(paramInt);
    }

    public final int getPartCount()
    {
        return this.parts.size();
    }

    public final List<Part> getParts()
    {
        return this.parts;
    }

    public final String getRawText()
    {
        return this.text;
    }

    protected abstract Resources getResources();

    public void parse()
    {
        if (parseMusicTrack())
            buildParts(null);
        while (true)
        {
            return;
            String str = null;
            if ((this.parseMeText) && (this.text.startsWith("/me")) && (this.text.length() > 3) && (Character.isWhitespace(this.text.charAt(3))))
            {
                str = this.text.substring(0, 4);
                this.text = this.text.substring(4);
            }
            int i = 0;
            while (this.nextChar < this.text.length())
            {
                if ((!isWordBreak(this.nextChar)) && ((i == 0) || (!isSmileyBreak(this.nextChar))))
                    throw new AssertionError("last chunk did not end at word break");
                if (parseSmiley())
                {
                    i = 1;
                }
                else
                {
                    i = 0;
                    if ((!parseAcronym()) && (!parseURL()) && (!parseFormatting()))
                        parseText();
                }
            }
            for (int j = 0; j < this.tokens.size(); j++)
                if (((Token)this.tokens.get(j)).isMedia())
                {
                    if ((j > 0) && ((this.tokens.get(j - 1) instanceof Html)))
                        ((Html)this.tokens.get(j - 1)).trimLeadingWhitespace();
                    if ((j + 1 < this.tokens.size()) && ((this.tokens.get(j + 1) instanceof Html)))
                        ((Html)this.tokens.get(j + 1)).trimTrailingWhitespace();
                }
            for (int k = 0; k < this.tokens.size(); k++)
                if ((((Token)this.tokens.get(k)).isHtml()) && (((Token)this.tokens.get(k)).toHtml(true).length() == 0))
                {
                    this.tokens.remove(k);
                    k--;
                }
            buildParts(str);
        }
    }

    public String toHtml()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        Iterator localIterator1 = this.parts.iterator();
        while (localIterator1.hasNext())
        {
            Part localPart = (Part)localIterator1.next();
            boolean bool = false;
            localStringBuilder.append("<p>");
            Iterator localIterator2 = localPart.getTokens().iterator();
            label529: 
            while (localIterator2.hasNext())
            {
                Token localToken = (Token)localIterator2.next();
                if (localToken.isHtml())
                    localStringBuilder.append(localToken.toHtml(bool));
                while (true)
                {
                    if (!localToken.controlCaps())
                        break label529;
                    bool = localToken.setCaps();
                    break;
                    switch (1.$SwitchMap$com$google$android$util$AbstractMessageParser$Token$Type[localToken.getType().ordinal()])
                    {
                    default:
                        throw new AssertionError("unknown token type: " + localToken.getType());
                    case 1:
                        localStringBuilder.append("<a href=\"");
                        localStringBuilder.append(((Link)localToken).getURL());
                        localStringBuilder.append("\">");
                        localStringBuilder.append(localToken.getRawText());
                        localStringBuilder.append("</a>");
                        break;
                    case 2:
                        localStringBuilder.append(localToken.getRawText());
                        break;
                    case 3:
                        localStringBuilder.append(localToken.getRawText());
                        break;
                    case 4:
                        localStringBuilder.append(((MusicTrack)localToken).getTrack());
                        break;
                    case 5:
                        localStringBuilder.append("<a href=\"");
                        ((Video)localToken);
                        localStringBuilder.append(Video.getURL(((Video)localToken).getDocID()));
                        localStringBuilder.append("\">");
                        localStringBuilder.append(localToken.getRawText());
                        localStringBuilder.append("</a>");
                        break;
                    case 6:
                        localStringBuilder.append("<a href=\"");
                        ((YouTubeVideo)localToken);
                        localStringBuilder.append(YouTubeVideo.getURL(((YouTubeVideo)localToken).getDocID()));
                        localStringBuilder.append("\">");
                        localStringBuilder.append(localToken.getRawText());
                        localStringBuilder.append("</a>");
                        break;
                    case 7:
                        localStringBuilder.append("<a href=\"");
                        localStringBuilder.append(Photo.getAlbumURL(((Photo)localToken).getUser(), ((Photo)localToken).getAlbum()));
                        localStringBuilder.append("\">");
                        localStringBuilder.append(localToken.getRawText());
                        localStringBuilder.append("</a>");
                        break;
                    case 8:
                        ((Photo)localToken);
                        localStringBuilder.append("<a href=\"");
                        localStringBuilder.append(((FlickrPhoto)localToken).getUrl());
                        localStringBuilder.append("\">");
                        localStringBuilder.append(localToken.getRawText());
                        localStringBuilder.append("</a>");
                    }
                }
            }
            localStringBuilder.append("</p>\n");
        }
        return localStringBuilder.toString();
    }

    public static class Part
    {
        private String meText;
        private ArrayList<AbstractMessageParser.Token> tokens = new ArrayList();

        private String getPartType()
        {
            String str;
            if (isMedia())
                str = "d";
            while (true)
            {
                return str;
                if (this.meText != null)
                    str = "m";
                else
                    str = "";
            }
        }

        public void add(AbstractMessageParser.Token paramToken)
        {
            if (isMedia())
                throw new AssertionError("media ");
            this.tokens.add(paramToken);
        }

        public AbstractMessageParser.Token getMediaToken()
        {
            if (isMedia());
            for (AbstractMessageParser.Token localToken = (AbstractMessageParser.Token)this.tokens.get(0); ; localToken = null)
                return localToken;
        }

        public String getRawText()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            if (this.meText != null)
                localStringBuilder.append(this.meText);
            for (int i = 0; i < this.tokens.size(); i++)
                localStringBuilder.append(((AbstractMessageParser.Token)this.tokens.get(i)).getRawText());
            return localStringBuilder.toString();
        }

        public ArrayList<AbstractMessageParser.Token> getTokens()
        {
            return this.tokens;
        }

        public String getType(boolean paramBoolean)
        {
            StringBuilder localStringBuilder = new StringBuilder();
            if (paramBoolean);
            for (String str = "s"; ; str = "r")
                return str + getPartType();
        }

        public boolean isMedia()
        {
            if ((this.tokens.size() == 1) && (((AbstractMessageParser.Token)this.tokens.get(0)).isMedia()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void setMeText(String paramString)
        {
            this.meText = paramString;
        }
    }

    public static class TrieNode
    {
        private final HashMap<Character, TrieNode> children = new HashMap();
        private String text;
        private String value;

        public TrieNode()
        {
            this("");
        }

        public TrieNode(String paramString)
        {
            this.text = paramString;
        }

        public static void addToTrie(TrieNode paramTrieNode, String paramString1, String paramString2)
        {
            int j;
            for (int i = 0; i < paramString1.length(); i = j)
            {
                j = i + 1;
                paramTrieNode = paramTrieNode.getOrCreateChild(paramString1.charAt(i));
            }
            paramTrieNode.setValue(paramString2);
        }

        public final boolean exists()
        {
            if (this.value != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public TrieNode getChild(char paramChar)
        {
            return (TrieNode)this.children.get(Character.valueOf(paramChar));
        }

        public TrieNode getOrCreateChild(char paramChar)
        {
            Character localCharacter = Character.valueOf(paramChar);
            TrieNode localTrieNode = (TrieNode)this.children.get(localCharacter);
            if (localTrieNode == null)
            {
                localTrieNode = new TrieNode(this.text + String.valueOf(paramChar));
                this.children.put(localCharacter, localTrieNode);
            }
            return localTrieNode;
        }

        public final String getText()
        {
            return this.text;
        }

        public final String getValue()
        {
            return this.value;
        }

        public void setValue(String paramString)
        {
            this.value = paramString;
        }
    }

    public static class Format extends AbstractMessageParser.Token
    {
        private char ch;
        private boolean matched;
        private boolean start;

        public Format(char paramChar, boolean paramBoolean)
        {
            super(String.valueOf(paramChar));
            this.ch = paramChar;
            this.start = paramBoolean;
        }

        private String getFormatEnd(char paramChar)
        {
            String str;
            switch (paramChar)
            {
            default:
                throw new AssertionError("unknown format '" + paramChar + "'");
            case '*':
                str = "</b>";
            case '_':
            case '^':
            case '"':
            }
            while (true)
            {
                return str;
                str = "</i>";
                continue;
                str = "</font></b>";
                continue;
                str = "”</font>";
            }
        }

        private String getFormatStart(char paramChar)
        {
            String str;
            switch (paramChar)
            {
            default:
                throw new AssertionError("unknown format '" + paramChar + "'");
            case '*':
                str = "<b>";
            case '_':
            case '^':
            case '"':
            }
            while (true)
            {
                return str;
                str = "<i>";
                continue;
                str = "<b><font color=\"#005FFF\">";
                continue;
                str = "<font color=\"#999999\">“";
            }
        }

        public boolean controlCaps()
        {
            if (this.ch == '^');
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public List<String> getInfo()
        {
            throw new UnsupportedOperationException();
        }

        public boolean isHtml()
        {
            return true;
        }

        public boolean setCaps()
        {
            return this.start;
        }

        public void setMatched(boolean paramBoolean)
        {
            this.matched = paramBoolean;
        }

        public String toHtml(boolean paramBoolean)
        {
            String str;
            if (this.matched)
                if (this.start)
                    str = getFormatStart(this.ch);
            while (true)
            {
                return str;
                str = getFormatEnd(this.ch);
                continue;
                if (this.ch == '"')
                    str = "&quot;";
                else
                    str = String.valueOf(this.ch);
            }
        }
    }

    public static class Acronym extends AbstractMessageParser.Token
    {
        private String value;

        public Acronym(String paramString1, String paramString2)
        {
            super(paramString1);
            this.value = paramString2;
        }

        public List<String> getInfo()
        {
            List localList = super.getInfo();
            localList.add(getRawText());
            localList.add(getValue());
            return localList;
        }

        public String getValue()
        {
            return this.value;
        }

        public boolean isHtml()
        {
            return false;
        }
    }

    public static class Smiley extends AbstractMessageParser.Token
    {
        public Smiley(String paramString)
        {
            super(paramString);
        }

        public List<String> getInfo()
        {
            List localList = super.getInfo();
            localList.add(getRawText());
            return localList;
        }

        public boolean isHtml()
        {
            return false;
        }
    }

    public static class FlickrPhoto extends AbstractMessageParser.Token
    {
        private static final Pattern GROUPING_PATTERN = Pattern.compile("http://(?:www.)?flickr.com/photos/([^/?#&]+)/(tags|sets)/([^/?#&]+)/?");
        private static final String SETS = "sets";
        private static final String TAGS = "tags";
        private static final Pattern URL_PATTERN = Pattern.compile("http://(?:www.)?flickr.com/photos/([^/?#&]+)/?([^/?#&]+)?/?.*");
        private String grouping;
        private String groupingId;
        private String photo;
        private String user;

        public FlickrPhoto(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
        {
            super(paramString5);
            if (!"tags".equals(paramString1))
            {
                this.user = paramString1;
                if (!"show".equals(paramString2))
                {
                    this.photo = paramString2;
                    this.grouping = paramString3;
                }
            }
            for (this.groupingId = paramString4; ; this.groupingId = paramString2)
            {
                return;
                paramString2 = null;
                break;
                this.user = null;
                this.photo = null;
                this.grouping = "tags";
            }
        }

        public static String getPhotoURL(String paramString1, String paramString2)
        {
            return "http://flickr.com/photos/" + paramString1 + "/" + paramString2;
        }

        public static String getRssUrl(String paramString)
        {
            return null;
        }

        public static String getTagsURL(String paramString)
        {
            return "http://flickr.com/photos/tags/" + paramString;
        }

        public static String getUserSetsURL(String paramString1, String paramString2)
        {
            return "http://flickr.com/photos/" + paramString1 + "/sets/" + paramString2;
        }

        public static String getUserTagsURL(String paramString1, String paramString2)
        {
            return "http://flickr.com/photos/" + paramString1 + "/tags/" + paramString2;
        }

        public static String getUserURL(String paramString)
        {
            return "http://flickr.com/photos/" + paramString;
        }

        public static FlickrPhoto matchURL(String paramString1, String paramString2)
        {
            Matcher localMatcher1 = GROUPING_PATTERN.matcher(paramString1);
            FlickrPhoto localFlickrPhoto;
            if (localMatcher1.matches())
                localFlickrPhoto = new FlickrPhoto(localMatcher1.group(1), null, localMatcher1.group(2), localMatcher1.group(3), paramString2);
            while (true)
            {
                return localFlickrPhoto;
                Matcher localMatcher2 = URL_PATTERN.matcher(paramString1);
                if (localMatcher2.matches())
                    localFlickrPhoto = new FlickrPhoto(localMatcher2.group(1), localMatcher2.group(2), null, null, paramString2);
                else
                    localFlickrPhoto = null;
            }
        }

        public String getGrouping()
        {
            return this.grouping;
        }

        public String getGroupingId()
        {
            return this.groupingId;
        }

        public List<String> getInfo()
        {
            List localList = super.getInfo();
            localList.add(getUrl());
            String str1;
            String str2;
            label49: String str3;
            if (getUser() != null)
            {
                str1 = getUser();
                localList.add(str1);
                if (getPhoto() == null)
                    break label110;
                str2 = getPhoto();
                localList.add(str2);
                if (getGrouping() == null)
                    break label117;
                str3 = getGrouping();
                label71: localList.add(str3);
                if (getGroupingId() == null)
                    break label124;
            }
            label110: label117: label124: for (String str4 = getGroupingId(); ; str4 = "")
            {
                localList.add(str4);
                return localList;
                str1 = "";
                break;
                str2 = "";
                break label49;
                str3 = "";
                break label71;
            }
        }

        public String getPhoto()
        {
            return this.photo;
        }

        public String getUrl()
        {
            String str;
            if ("sets".equals(this.grouping))
                str = getUserSetsURL(this.user, this.groupingId);
            while (true)
            {
                return str;
                if ("tags".equals(this.grouping))
                {
                    if (this.user != null)
                        str = getUserTagsURL(this.user, this.groupingId);
                    else
                        str = getTagsURL(this.groupingId);
                }
                else if (this.photo != null)
                    str = getPhotoURL(this.user, this.photo);
                else
                    str = getUserURL(this.user);
            }
        }

        public String getUser()
        {
            return this.user;
        }

        public boolean isHtml()
        {
            return false;
        }

        public boolean isMedia()
        {
            return true;
        }
    }

    public static class Photo extends AbstractMessageParser.Token
    {
        private static final Pattern URL_PATTERN = Pattern.compile("http://picasaweb.google.com/([^/?#&]+)/+((?!searchbrowse)[^/?#&]+)(?:/|/photo)?(?:\\?[^#]*)?(?:#(.*))?");
        private String album;
        private String photo;
        private String user;

        public Photo(String paramString1, String paramString2, String paramString3, String paramString4)
        {
            super(paramString4);
            this.user = paramString1;
            this.album = paramString2;
            this.photo = paramString3;
        }

        public static String getAlbumURL(String paramString1, String paramString2)
        {
            return "http://picasaweb.google.com/" + paramString1 + "/" + paramString2;
        }

        public static String getPhotoURL(String paramString1, String paramString2, String paramString3)
        {
            return "http://picasaweb.google.com/" + paramString1 + "/" + paramString2 + "/photo#" + paramString3;
        }

        public static String getRssUrl(String paramString)
        {
            return "http://picasaweb.google.com/data/feed/api/user/" + paramString + "?category=album&alt=rss";
        }

        public static Photo matchURL(String paramString1, String paramString2)
        {
            Matcher localMatcher = URL_PATTERN.matcher(paramString1);
            if (localMatcher.matches());
            for (Photo localPhoto = new Photo(localMatcher.group(1), localMatcher.group(2), localMatcher.group(3), paramString2); ; localPhoto = null)
                return localPhoto;
        }

        public String getAlbum()
        {
            return this.album;
        }

        public List<String> getInfo()
        {
            List localList = super.getInfo();
            localList.add(getRssUrl(getUser()));
            localList.add(getAlbumURL(getUser(), getAlbum()));
            if (getPhoto() != null)
                localList.add(getPhotoURL(getUser(), getAlbum(), getPhoto()));
            while (true)
            {
                return localList;
                localList.add((String)null);
            }
        }

        public String getPhoto()
        {
            return this.photo;
        }

        public String getUser()
        {
            return this.user;
        }

        public boolean isHtml()
        {
            return false;
        }

        public boolean isMedia()
        {
            return true;
        }
    }

    public static class YouTubeVideo extends AbstractMessageParser.Token
    {
        private static final Pattern URL_PATTERN = Pattern.compile("(?i)http://(?:[a-z0-9]+\\.)?youtube\\.[a-z0-9]+(?:\\.[a-z0-9]+)?/watch\\?.*\\bv=([-_a-zA-Z0-9=]+).*");
        private String docid;

        public YouTubeVideo(String paramString1, String paramString2)
        {
            super(paramString2);
            this.docid = paramString1;
        }

        public static String getPrefixedURL(boolean paramBoolean, String paramString1, String paramString2, String paramString3)
        {
            String str = "";
            if (paramBoolean)
                str = "http://";
            if (paramString1 == null)
                paramString1 = "";
            if (paramString3 == null)
                paramString3 = "";
            while (true)
            {
                return str + paramString1 + "youtube.com/watch?" + paramString3 + "v=" + paramString2;
                if (paramString3.length() > 0)
                    paramString3 = paramString3 + "&";
            }
        }

        public static String getRssUrl(String paramString)
        {
            return "http://youtube.com/watch?v=" + paramString;
        }

        public static String getURL(String paramString)
        {
            return getURL(paramString, null);
        }

        public static String getURL(String paramString1, String paramString2)
        {
            if (paramString2 == null)
                paramString2 = "";
            while (true)
            {
                return "http://youtube.com/watch?" + paramString2 + "v=" + paramString1;
                if (paramString2.length() > 0)
                    paramString2 = paramString2 + "&";
            }
        }

        public static YouTubeVideo matchURL(String paramString1, String paramString2)
        {
            Matcher localMatcher = URL_PATTERN.matcher(paramString1);
            if (localMatcher.matches());
            for (YouTubeVideo localYouTubeVideo = new YouTubeVideo(localMatcher.group(1), paramString2); ; localYouTubeVideo = null)
                return localYouTubeVideo;
        }

        public String getDocID()
        {
            return this.docid;
        }

        public List<String> getInfo()
        {
            List localList = super.getInfo();
            localList.add(getRssUrl(this.docid));
            localList.add(getURL(this.docid));
            return localList;
        }

        public boolean isHtml()
        {
            return false;
        }

        public boolean isMedia()
        {
            return true;
        }
    }

    public static class Video extends AbstractMessageParser.Token
    {
        private static final Pattern URL_PATTERN = Pattern.compile("(?i)http://video\\.google\\.[a-z0-9]+(?:\\.[a-z0-9]+)?/videoplay\\?.*?\\bdocid=(-?\\d+).*");
        private String docid;

        public Video(String paramString1, String paramString2)
        {
            super(paramString2);
            this.docid = paramString1;
        }

        public static String getRssUrl(String paramString)
        {
            return "http://video.google.com/videofeed?type=docid&output=rss&sourceid=gtalk&docid=" + paramString;
        }

        public static String getURL(String paramString)
        {
            return getURL(paramString, null);
        }

        public static String getURL(String paramString1, String paramString2)
        {
            if (paramString2 == null)
                paramString2 = "";
            while (true)
            {
                return "http://video.google.com/videoplay?" + paramString2 + "docid=" + paramString1;
                if (paramString2.length() > 0)
                    paramString2 = paramString2 + "&";
            }
        }

        public static Video matchURL(String paramString1, String paramString2)
        {
            Matcher localMatcher = URL_PATTERN.matcher(paramString1);
            if (localMatcher.matches());
            for (Video localVideo = new Video(localMatcher.group(1), paramString2); ; localVideo = null)
                return localVideo;
        }

        public String getDocID()
        {
            return this.docid;
        }

        public List<String> getInfo()
        {
            List localList = super.getInfo();
            localList.add(getRssUrl(this.docid));
            localList.add(getURL(this.docid));
            return localList;
        }

        public boolean isHtml()
        {
            return false;
        }

        public boolean isMedia()
        {
            return true;
        }
    }

    public static class Link extends AbstractMessageParser.Token
    {
        private String url;

        public Link(String paramString1, String paramString2)
        {
            super(paramString2);
            this.url = paramString1;
        }

        public List<String> getInfo()
        {
            List localList = super.getInfo();
            localList.add(getURL());
            localList.add(getRawText());
            return localList;
        }

        public String getURL()
        {
            return this.url;
        }

        public boolean isHtml()
        {
            return false;
        }
    }

    public static class MusicTrack extends AbstractMessageParser.Token
    {
        private String track;

        public MusicTrack(String paramString)
        {
            super(paramString);
            this.track = paramString;
        }

        public List<String> getInfo()
        {
            List localList = super.getInfo();
            localList.add(getTrack());
            return localList;
        }

        public String getTrack()
        {
            return this.track;
        }

        public boolean isHtml()
        {
            return false;
        }
    }

    public static class Html extends AbstractMessageParser.Token
    {
        private String html;

        public Html(String paramString1, String paramString2)
        {
            super(paramString1);
            this.html = paramString2;
        }

        private static String trimLeadingWhitespace(String paramString)
        {
            for (int i = 0; (i < paramString.length()) && (Character.isWhitespace(paramString.charAt(i))); i++);
            return paramString.substring(i);
        }

        public static String trimTrailingWhitespace(String paramString)
        {
            for (int i = paramString.length(); (i > 0) && (Character.isWhitespace(paramString.charAt(i - 1))); i--);
            return paramString.substring(0, i);
        }

        public List<String> getInfo()
        {
            throw new UnsupportedOperationException();
        }

        public boolean isHtml()
        {
            return true;
        }

        public String toHtml(boolean paramBoolean)
        {
            if (paramBoolean);
            for (String str = this.html.toUpperCase(); ; str = this.html)
                return str;
        }

        public void trimLeadingWhitespace()
        {
            this.text = trimLeadingWhitespace(this.text);
            this.html = trimLeadingWhitespace(this.html);
        }

        public void trimTrailingWhitespace()
        {
            this.text = trimTrailingWhitespace(this.text);
            this.html = trimTrailingWhitespace(this.html);
        }
    }

    public static abstract class Token
    {
        protected String text;
        protected Type type;

        protected Token(Type paramType, String paramString)
        {
            this.type = paramType;
            this.text = paramString;
        }

        public boolean controlCaps()
        {
            return false;
        }

        public List<String> getInfo()
        {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(getType().toString());
            return localArrayList;
        }

        public String getRawText()
        {
            return this.text;
        }

        public Type getType()
        {
            return this.type;
        }

        public boolean isArray()
        {
            if (!isHtml());
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public abstract boolean isHtml();

        public boolean isMedia()
        {
            return false;
        }

        public boolean setCaps()
        {
            return false;
        }

        public String toHtml(boolean paramBoolean)
        {
            throw new AssertionError("not html");
        }

        public static enum Type
        {
            private String stringRep;

            static
            {
                FORMAT = new Type("FORMAT", 1, "format");
                LINK = new Type("LINK", 2, "l");
                SMILEY = new Type("SMILEY", 3, "e");
                ACRONYM = new Type("ACRONYM", 4, "a");
                MUSIC = new Type("MUSIC", 5, "m");
                GOOGLE_VIDEO = new Type("GOOGLE_VIDEO", 6, "v");
                YOUTUBE_VIDEO = new Type("YOUTUBE_VIDEO", 7, "yt");
                PHOTO = new Type("PHOTO", 8, "p");
                FLICKR = new Type("FLICKR", 9, "f");
                Type[] arrayOfType = new Type[10];
                arrayOfType[0] = HTML;
                arrayOfType[1] = FORMAT;
                arrayOfType[2] = LINK;
                arrayOfType[3] = SMILEY;
                arrayOfType[4] = ACRONYM;
                arrayOfType[5] = MUSIC;
                arrayOfType[6] = GOOGLE_VIDEO;
                arrayOfType[7] = YOUTUBE_VIDEO;
                arrayOfType[8] = PHOTO;
                arrayOfType[9] = FLICKR;
            }

            private Type(String paramString)
            {
                this.stringRep = paramString;
            }

            public String toString()
            {
                return this.stringRep;
            }
        }
    }

    public static abstract interface Resources
    {
        public abstract AbstractMessageParser.TrieNode getAcronyms();

        public abstract AbstractMessageParser.TrieNode getDomainSuffixes();

        public abstract Set<String> getSchemes();

        public abstract AbstractMessageParser.TrieNode getSmileys();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.util.AbstractMessageParser
 * JD-Core Version:        0.6.2
 */