package com.google.android.util;

public abstract interface Procedure<T>
{
    public abstract void apply(T paramT);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.util.Procedure
 * JD-Core Version:        0.6.2
 */