package com.google.android.gles_jni;

import javax.microedition.khronos.egl.EGLDisplay;

public class EGLDisplayImpl extends EGLDisplay
{
    int mEGLDisplay;

    public EGLDisplayImpl(int paramInt)
    {
        this.mEGLDisplay = paramInt;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (getClass() != paramObject.getClass()))
            {
                bool = false;
            }
            else
            {
                EGLDisplayImpl localEGLDisplayImpl = (EGLDisplayImpl)paramObject;
                if (this.mEGLDisplay != localEGLDisplayImpl.mEGLDisplay)
                    bool = false;
            }
        }
    }

    public int hashCode()
    {
        return this.mEGLDisplay;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.gles_jni.EGLDisplayImpl
 * JD-Core Version:        0.6.2
 */