package com.google.android.gles_jni;

import javax.microedition.khronos.egl.EGLSurface;

public class EGLSurfaceImpl extends EGLSurface
{
    int mEGLSurface;
    private int mNativePixelRef;

    public EGLSurfaceImpl()
    {
        this.mEGLSurface = 0;
        this.mNativePixelRef = 0;
    }

    public EGLSurfaceImpl(int paramInt)
    {
        this.mEGLSurface = paramInt;
        this.mNativePixelRef = 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (getClass() != paramObject.getClass()))
            {
                bool = false;
            }
            else
            {
                EGLSurfaceImpl localEGLSurfaceImpl = (EGLSurfaceImpl)paramObject;
                if (this.mEGLSurface != localEGLSurfaceImpl.mEGLSurface)
                    bool = false;
            }
        }
    }

    public int hashCode()
    {
        return this.mEGLSurface;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.gles_jni.EGLSurfaceImpl
 * JD-Core Version:        0.6.2
 */