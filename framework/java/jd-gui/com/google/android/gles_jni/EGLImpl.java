package com.google.android.gles_jni;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

public class EGLImpl
    implements EGL10
{
    private EGLContextImpl mContext = new EGLContextImpl(-1);
    private EGLDisplayImpl mDisplay = new EGLDisplayImpl(-1);
    private EGLSurfaceImpl mSurface = new EGLSurfaceImpl(-1);

    static
    {
        _nativeClassInit();
    }

    private native int _eglCreateContext(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, EGLContext paramEGLContext, int[] paramArrayOfInt);

    private native int _eglCreatePbufferSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int[] paramArrayOfInt);

    private native void _eglCreatePixmapSurface(EGLSurface paramEGLSurface, EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfInt);

    private native int _eglCreateWindowSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfInt);

    private native int _eglCreateWindowSurfaceTexture(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfInt);

    private native int _eglGetCurrentContext();

    private native int _eglGetCurrentDisplay();

    private native int _eglGetCurrentSurface(int paramInt);

    private native int _eglGetDisplay(Object paramObject);

    private static native void _nativeClassInit();

    public static native int getInitCount(EGLDisplay paramEGLDisplay);

    public native boolean eglChooseConfig(EGLDisplay paramEGLDisplay, int[] paramArrayOfInt1, EGLConfig[] paramArrayOfEGLConfig, int paramInt, int[] paramArrayOfInt2);

    public native boolean eglCopyBuffers(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface, Object paramObject);

    public EGLContext eglCreateContext(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, EGLContext paramEGLContext, int[] paramArrayOfInt)
    {
        int i = _eglCreateContext(paramEGLDisplay, paramEGLConfig, paramEGLContext, paramArrayOfInt);
        if (i == 0);
        for (Object localObject = EGL10.EGL_NO_CONTEXT; ; localObject = new EGLContextImpl(i))
            return localObject;
    }

    public EGLSurface eglCreatePbufferSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int[] paramArrayOfInt)
    {
        int i = _eglCreatePbufferSurface(paramEGLDisplay, paramEGLConfig, paramArrayOfInt);
        if (i == 0);
        for (Object localObject = EGL10.EGL_NO_SURFACE; ; localObject = new EGLSurfaceImpl(i))
            return localObject;
    }

    public EGLSurface eglCreatePixmapSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfInt)
    {
        Object localObject = new EGLSurfaceImpl();
        _eglCreatePixmapSurface((EGLSurface)localObject, paramEGLDisplay, paramEGLConfig, paramObject, paramArrayOfInt);
        if (((EGLSurfaceImpl)localObject).mEGLSurface == 0)
            localObject = EGL10.EGL_NO_SURFACE;
        return localObject;
    }

    public EGLSurface eglCreateWindowSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfInt)
    {
        Surface localSurface = null;
        int i;
        if ((paramObject instanceof SurfaceView))
        {
            localSurface = ((SurfaceView)paramObject).getHolder().getSurface();
            if (localSurface == null)
                break label75;
            i = _eglCreateWindowSurface(paramEGLDisplay, paramEGLConfig, localSurface, paramArrayOfInt);
            label41: if (i != 0)
                break label106;
        }
        label75: label106: for (Object localObject = EGL10.EGL_NO_SURFACE; ; localObject = new EGLSurfaceImpl(i))
        {
            return localObject;
            if (!(paramObject instanceof SurfaceHolder))
                break;
            localSurface = ((SurfaceHolder)paramObject).getSurface();
            break;
            if ((paramObject instanceof SurfaceTexture))
            {
                i = _eglCreateWindowSurfaceTexture(paramEGLDisplay, paramEGLConfig, paramObject, paramArrayOfInt);
                break label41;
            }
            throw new UnsupportedOperationException("eglCreateWindowSurface() can only be called with an instance of SurfaceView, SurfaceHolder or SurfaceTexture at the moment, this will be fixed later.");
        }
    }

    public native boolean eglDestroyContext(EGLDisplay paramEGLDisplay, EGLContext paramEGLContext);

    public native boolean eglDestroySurface(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface);

    public native boolean eglGetConfigAttrib(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int paramInt, int[] paramArrayOfInt);

    public native boolean eglGetConfigs(EGLDisplay paramEGLDisplay, EGLConfig[] paramArrayOfEGLConfig, int paramInt, int[] paramArrayOfInt);

    /** @deprecated */
    public EGLContext eglGetCurrentContext()
    {
        try
        {
            int i = _eglGetCurrentContext();
            if (i == 0);
            for (Object localObject2 = EGL10.EGL_NO_CONTEXT; ; localObject2 = this.mContext)
            {
                return localObject2;
                if (this.mContext.mEGLContext != i)
                    this.mContext = new EGLContextImpl(i);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public EGLDisplay eglGetCurrentDisplay()
    {
        try
        {
            int i = _eglGetCurrentDisplay();
            if (i == 0);
            for (Object localObject2 = EGL10.EGL_NO_DISPLAY; ; localObject2 = this.mDisplay)
            {
                return localObject2;
                if (this.mDisplay.mEGLDisplay != i)
                    this.mDisplay = new EGLDisplayImpl(i);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public EGLSurface eglGetCurrentSurface(int paramInt)
    {
        try
        {
            int i = _eglGetCurrentSurface(paramInt);
            if (i == 0);
            for (Object localObject2 = EGL10.EGL_NO_SURFACE; ; localObject2 = this.mSurface)
            {
                return localObject2;
                if (this.mSurface.mEGLSurface != i)
                    this.mSurface = new EGLSurfaceImpl(i);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public EGLDisplay eglGetDisplay(Object paramObject)
    {
        try
        {
            int i = _eglGetDisplay(paramObject);
            if (i == 0);
            for (Object localObject2 = EGL10.EGL_NO_DISPLAY; ; localObject2 = this.mDisplay)
            {
                return localObject2;
                if (this.mDisplay.mEGLDisplay != i)
                    this.mDisplay = new EGLDisplayImpl(i);
            }
        }
        finally
        {
        }
    }

    public native int eglGetError();

    public native boolean eglInitialize(EGLDisplay paramEGLDisplay, int[] paramArrayOfInt);

    public native boolean eglMakeCurrent(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface1, EGLSurface paramEGLSurface2, EGLContext paramEGLContext);

    public native boolean eglQueryContext(EGLDisplay paramEGLDisplay, EGLContext paramEGLContext, int paramInt, int[] paramArrayOfInt);

    public native String eglQueryString(EGLDisplay paramEGLDisplay, int paramInt);

    public native boolean eglQuerySurface(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface, int paramInt, int[] paramArrayOfInt);

    public native boolean eglReleaseThread();

    public native boolean eglSwapBuffers(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface);

    public native boolean eglTerminate(EGLDisplay paramEGLDisplay);

    public native boolean eglWaitGL();

    public native boolean eglWaitNative(int paramInt, Object paramObject);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.gles_jni.EGLImpl
 * JD-Core Version:        0.6.2
 */