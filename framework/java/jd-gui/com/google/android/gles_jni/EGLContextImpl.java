package com.google.android.gles_jni;

import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL;

public class EGLContextImpl extends EGLContext
{
    int mEGLContext;
    private GLImpl mGLContext;

    public EGLContextImpl(int paramInt)
    {
        this.mEGLContext = paramInt;
        this.mGLContext = new GLImpl();
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if ((paramObject == null) || (getClass() != paramObject.getClass()))
            {
                bool = false;
            }
            else
            {
                EGLContextImpl localEGLContextImpl = (EGLContextImpl)paramObject;
                if (this.mEGLContext != localEGLContextImpl.mEGLContext)
                    bool = false;
            }
        }
    }

    public GL getGL()
    {
        return this.mGLContext;
    }

    public int hashCode()
    {
        return this.mEGLContext;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.gles_jni.EGLContextImpl
 * JD-Core Version:        0.6.2
 */