package com.google.android.collect;

import java.util.ArrayList;
import java.util.Collections;

public class Lists
{
    public static <E> ArrayList<E> newArrayList()
    {
        return new ArrayList();
    }

    public static <E> ArrayList<E> newArrayList(E[] paramArrayOfE)
    {
        ArrayList localArrayList = new ArrayList(5 + 110 * paramArrayOfE.length / 100);
        Collections.addAll(localArrayList, paramArrayOfE);
        return localArrayList;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.collect.Lists
 * JD-Core Version:        0.6.2
 */