package com.google.android.mms.util;

import java.util.HashMap;

public abstract class AbstractCache<K, V>
{
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final int MAX_CACHED_ITEMS = 500;
    private static final String TAG = "AbstractCache";
    private final HashMap<K, CacheEntry<V>> mCacheMap = new HashMap();

    public V get(K paramK)
    {
        CacheEntry localCacheEntry;
        if (paramK != null)
        {
            localCacheEntry = (CacheEntry)this.mCacheMap.get(paramK);
            if (localCacheEntry != null)
                localCacheEntry.hit = (1 + localCacheEntry.hit);
        }
        for (Object localObject = localCacheEntry.value; ; localObject = null)
            return localObject;
    }

    public V purge(K paramK)
    {
        CacheEntry localCacheEntry = (CacheEntry)this.mCacheMap.remove(paramK);
        if (localCacheEntry != null);
        for (Object localObject = localCacheEntry.value; ; localObject = null)
            return localObject;
    }

    public void purgeAll()
    {
        this.mCacheMap.clear();
    }

    public boolean put(K paramK, V paramV)
    {
        boolean bool = false;
        if (this.mCacheMap.size() >= 500);
        while (true)
        {
            return bool;
            if (paramK != null)
            {
                CacheEntry localCacheEntry = new CacheEntry(null);
                localCacheEntry.value = paramV;
                this.mCacheMap.put(paramK, localCacheEntry);
                bool = true;
            }
        }
    }

    public int size()
    {
        return this.mCacheMap.size();
    }

    private static class CacheEntry<V>
    {
        int hit;
        V value;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.util.AbstractCache
 * JD-Core Version:        0.6.2
 */