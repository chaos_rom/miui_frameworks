package com.google.android.mms.util;

import com.google.android.mms.pdu.GenericPdu;

public final class PduCacheEntry
{
    private final int mMessageBox;
    private final GenericPdu mPdu;
    private final long mThreadId;

    public PduCacheEntry(GenericPdu paramGenericPdu, int paramInt, long paramLong)
    {
        this.mPdu = paramGenericPdu;
        this.mMessageBox = paramInt;
        this.mThreadId = paramLong;
    }

    public int getMessageBox()
    {
        return this.mMessageBox;
    }

    public GenericPdu getPdu()
    {
        return this.mPdu;
    }

    public long getThreadId()
    {
        return this.mThreadId;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.util.PduCacheEntry
 * JD-Core Version:        0.6.2
 */