package com.google.android.mms.util;

import android.content.Context;
import android.drm.DrmConvertedStatus;
import android.drm.DrmManagerClient;
import android.util.Log;

public class DrmConvertSession
{
    private static final String TAG = "DrmConvertSession";
    private int mConvertSessionId;
    private DrmManagerClient mDrmClient;

    private DrmConvertSession(DrmManagerClient paramDrmManagerClient, int paramInt)
    {
        this.mDrmClient = paramDrmManagerClient;
        this.mConvertSessionId = paramInt;
    }

    public static DrmConvertSession open(Context paramContext, String paramString)
    {
        Object localObject = null;
        int i = -1;
        if ((paramContext != null) && (paramString != null) && (!paramString.equals("")));
        try
        {
            DrmManagerClient localDrmManagerClient = new DrmManagerClient(paramContext);
            try
            {
                int j = localDrmManagerClient.openConvertSession(paramString);
                i = j;
                localObject = localDrmManagerClient;
                if ((localObject == null) || (i < 0))
                {
                    localDrmConvertSession = null;
                    return localDrmConvertSession;
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException2)
            {
            }
            catch (IllegalStateException localIllegalStateException1)
            {
                while (true)
                {
                    try
                    {
                        Log.w("DrmConvertSession", "Conversion of Mimetype: " + paramString + " is not supported.", localIllegalArgumentException2);
                    }
                    catch (IllegalArgumentException localIllegalArgumentException1)
                    {
                        localObject = localDrmManagerClient;
                        Log.w("DrmConvertSession", "DrmManagerClient instance could not be created, context is Illegal.");
                        continue;
                        localIllegalStateException1 = localIllegalStateException1;
                        Log.w("DrmConvertSession", "Could not access Open DrmFramework.", localIllegalStateException1);
                    }
                    catch (IllegalStateException localIllegalStateException2)
                    {
                        label102: localObject = localDrmManagerClient;
                    }
                    label133: Log.w("DrmConvertSession", "DrmManagerClient didn't initialize properly.");
                    continue;
                    DrmConvertSession localDrmConvertSession = new DrmConvertSession(localObject, i);
                }
            }
        }
        catch (IllegalStateException localIllegalStateException3)
        {
            break label133;
        }
        catch (IllegalArgumentException localIllegalArgumentException3)
        {
            break label102;
        }
    }

    // ERROR //
    public int close(String paramString)
    {
        // Byte code:
        //     0: sipush 491
        //     3: istore_2
        //     4: aload_0
        //     5: getfield 19	com/google/android/mms/util/DrmConvertSession:mDrmClient	Landroid/drm/DrmManagerClient;
        //     8: ifnull +53 -> 61
        //     11: aload_0
        //     12: getfield 21	com/google/android/mms/util/DrmConvertSession:mConvertSessionId	I
        //     15: iflt +46 -> 61
        //     18: aload_0
        //     19: getfield 19	com/google/android/mms/util/DrmConvertSession:mDrmClient	Landroid/drm/DrmManagerClient;
        //     22: aload_0
        //     23: getfield 21	com/google/android/mms/util/DrmConvertSession:mConvertSessionId	I
        //     26: invokevirtual 87	android/drm/DrmManagerClient:closeConvertSession	(I)Landroid/drm/DrmConvertedStatus;
        //     29: astore 5
        //     31: aload 5
        //     33: ifnull +24 -> 57
        //     36: aload 5
        //     38: getfield 92	android/drm/DrmConvertedStatus:statusCode	I
        //     41: iconst_1
        //     42: if_icmpne +15 -> 57
        //     45: aload 5
        //     47: getfield 96	android/drm/DrmConvertedStatus:convertedData	[B
        //     50: astore 6
        //     52: aload 6
        //     54: ifnonnull +9 -> 63
        //     57: sipush 406
        //     60: istore_2
        //     61: iload_2
        //     62: ireturn
        //     63: aconst_null
        //     64: astore 7
        //     66: new 98	java/io/RandomAccessFile
        //     69: dup
        //     70: aload_1
        //     71: ldc 100
        //     73: invokespecial 103	java/io/RandomAccessFile:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     76: astore 8
        //     78: aload 8
        //     80: aload 5
        //     82: getfield 106	android/drm/DrmConvertedStatus:offset	I
        //     85: i2l
        //     86: invokevirtual 110	java/io/RandomAccessFile:seek	(J)V
        //     89: aload 8
        //     91: aload 5
        //     93: getfield 96	android/drm/DrmConvertedStatus:convertedData	[B
        //     96: invokevirtual 114	java/io/RandomAccessFile:write	([B)V
        //     99: sipush 200
        //     102: istore_2
        //     103: aload 8
        //     105: ifnull -44 -> 61
        //     108: aload 8
        //     110: invokevirtual 116	java/io/RandomAccessFile:close	()V
        //     113: goto -52 -> 61
        //     116: astore 28
        //     118: sipush 492
        //     121: istore_2
        //     122: ldc 8
        //     124: new 46	java/lang/StringBuilder
        //     127: dup
        //     128: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     131: ldc 118
        //     133: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     136: aload_1
        //     137: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     140: ldc 120
        //     142: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     145: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     148: aload 28
        //     150: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     153: pop
        //     154: goto -93 -> 61
        //     157: astore_3
        //     158: ldc 8
        //     160: new 46	java/lang/StringBuilder
        //     163: dup
        //     164: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     167: ldc 122
        //     169: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     172: aload_0
        //     173: getfield 21	com/google/android/mms/util/DrmConvertSession:mConvertSessionId	I
        //     176: invokevirtual 125	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     179: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     182: aload_3
        //     183: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     186: pop
        //     187: goto -126 -> 61
        //     190: astore 9
        //     192: sipush 492
        //     195: istore_2
        //     196: ldc 8
        //     198: new 46	java/lang/StringBuilder
        //     201: dup
        //     202: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     205: ldc 127
        //     207: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     210: aload_1
        //     211: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     214: ldc 129
        //     216: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     219: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     222: aload 9
        //     224: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     227: pop
        //     228: aload 7
        //     230: ifnull -169 -> 61
        //     233: aload 7
        //     235: invokevirtual 116	java/io/RandomAccessFile:close	()V
        //     238: goto -177 -> 61
        //     241: astore 14
        //     243: sipush 492
        //     246: istore_2
        //     247: ldc 8
        //     249: new 46	java/lang/StringBuilder
        //     252: dup
        //     253: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     256: ldc 118
        //     258: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     261: aload_1
        //     262: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     265: ldc 120
        //     267: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     270: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     273: aload 14
        //     275: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     278: pop
        //     279: goto -218 -> 61
        //     282: astore 16
        //     284: sipush 492
        //     287: istore_2
        //     288: ldc 8
        //     290: new 46	java/lang/StringBuilder
        //     293: dup
        //     294: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     297: ldc 131
        //     299: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     302: aload_1
        //     303: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     306: ldc 133
        //     308: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     311: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     314: aload 16
        //     316: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     319: pop
        //     320: aload 7
        //     322: ifnull -261 -> 61
        //     325: aload 7
        //     327: invokevirtual 116	java/io/RandomAccessFile:close	()V
        //     330: goto -269 -> 61
        //     333: astore 18
        //     335: sipush 492
        //     338: istore_2
        //     339: ldc 8
        //     341: new 46	java/lang/StringBuilder
        //     344: dup
        //     345: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     348: ldc 118
        //     350: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     353: aload_1
        //     354: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     357: ldc 120
        //     359: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     362: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     365: aload 18
        //     367: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     370: pop
        //     371: goto -310 -> 61
        //     374: astore 20
        //     376: sipush 492
        //     379: istore_2
        //     380: ldc 8
        //     382: ldc 135
        //     384: aload 20
        //     386: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     389: pop
        //     390: aload 7
        //     392: ifnull -331 -> 61
        //     395: aload 7
        //     397: invokevirtual 116	java/io/RandomAccessFile:close	()V
        //     400: goto -339 -> 61
        //     403: astore 22
        //     405: sipush 492
        //     408: istore_2
        //     409: ldc 8
        //     411: new 46	java/lang/StringBuilder
        //     414: dup
        //     415: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     418: ldc 118
        //     420: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     423: aload_1
        //     424: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     427: ldc 120
        //     429: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     432: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     435: aload 22
        //     437: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     440: pop
        //     441: goto -380 -> 61
        //     444: astore 24
        //     446: ldc 8
        //     448: new 46	java/lang/StringBuilder
        //     451: dup
        //     452: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     455: ldc 137
        //     457: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     460: aload_1
        //     461: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     464: ldc 139
        //     466: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     469: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     472: aload 24
        //     474: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     477: pop
        //     478: aload 7
        //     480: ifnull -419 -> 61
        //     483: aload 7
        //     485: invokevirtual 116	java/io/RandomAccessFile:close	()V
        //     488: goto -427 -> 61
        //     491: astore 26
        //     493: sipush 492
        //     496: istore_2
        //     497: ldc 8
        //     499: new 46	java/lang/StringBuilder
        //     502: dup
        //     503: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     506: ldc 118
        //     508: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     511: aload_1
        //     512: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     515: ldc 120
        //     517: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     520: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     523: aload 26
        //     525: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     528: pop
        //     529: goto -468 -> 61
        //     532: astore 10
        //     534: aload 7
        //     536: ifnull +8 -> 544
        //     539: aload 7
        //     541: invokevirtual 116	java/io/RandomAccessFile:close	()V
        //     544: aload 10
        //     546: athrow
        //     547: astore 11
        //     549: sipush 492
        //     552: istore_2
        //     553: ldc 8
        //     555: new 46	java/lang/StringBuilder
        //     558: dup
        //     559: invokespecial 47	java/lang/StringBuilder:<init>	()V
        //     562: ldc 118
        //     564: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     567: aload_1
        //     568: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     571: ldc 120
        //     573: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     576: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     579: aload 11
        //     581: invokestatic 65	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     584: pop
        //     585: goto -41 -> 544
        //     588: astore 10
        //     590: aload 8
        //     592: astore 7
        //     594: goto -60 -> 534
        //     597: astore 24
        //     599: aload 8
        //     601: astore 7
        //     603: goto -157 -> 446
        //     606: astore 20
        //     608: aload 8
        //     610: astore 7
        //     612: goto -236 -> 376
        //     615: astore 16
        //     617: aload 8
        //     619: astore 7
        //     621: goto -337 -> 284
        //     624: astore 9
        //     626: aload 8
        //     628: astore 7
        //     630: goto -438 -> 192
        //
        // Exception table:
        //     from	to	target	type
        //     108	113	116	java/io/IOException
        //     18	52	157	java/lang/IllegalStateException
        //     108	113	157	java/lang/IllegalStateException
        //     122	154	157	java/lang/IllegalStateException
        //     233	238	157	java/lang/IllegalStateException
        //     247	279	157	java/lang/IllegalStateException
        //     325	330	157	java/lang/IllegalStateException
        //     339	371	157	java/lang/IllegalStateException
        //     395	400	157	java/lang/IllegalStateException
        //     409	441	157	java/lang/IllegalStateException
        //     483	488	157	java/lang/IllegalStateException
        //     497	529	157	java/lang/IllegalStateException
        //     539	544	157	java/lang/IllegalStateException
        //     544	585	157	java/lang/IllegalStateException
        //     66	78	190	java/io/FileNotFoundException
        //     233	238	241	java/io/IOException
        //     66	78	282	java/io/IOException
        //     325	330	333	java/io/IOException
        //     66	78	374	java/lang/IllegalArgumentException
        //     395	400	403	java/io/IOException
        //     66	78	444	java/lang/SecurityException
        //     483	488	491	java/io/IOException
        //     66	78	532	finally
        //     196	228	532	finally
        //     288	320	532	finally
        //     380	390	532	finally
        //     446	478	532	finally
        //     539	544	547	java/io/IOException
        //     78	99	588	finally
        //     78	99	597	java/lang/SecurityException
        //     78	99	606	java/lang/IllegalArgumentException
        //     78	99	615	java/io/IOException
        //     78	99	624	java/io/FileNotFoundException
    }

    public byte[] convert(byte[] paramArrayOfByte, int paramInt)
    {
        byte[] arrayOfByte1 = null;
        if (paramArrayOfByte != null)
            try
            {
                byte[] arrayOfByte2;
                if (paramInt != paramArrayOfByte.length)
                {
                    arrayOfByte2 = new byte[paramInt];
                    System.arraycopy(paramArrayOfByte, 0, arrayOfByte2, 0, paramInt);
                }
                DrmConvertedStatus localDrmConvertedStatus;
                for (Object localObject = this.mDrmClient.convertData(this.mConvertSessionId, arrayOfByte2); (localObject != null) && (((DrmConvertedStatus)localObject).statusCode == 1) && (((DrmConvertedStatus)localObject).convertedData != null); localObject = localDrmConvertedStatus)
                {
                    arrayOfByte1 = ((DrmConvertedStatus)localObject).convertedData;
                    break;
                    localDrmConvertedStatus = this.mDrmClient.convertData(this.mConvertSessionId, paramArrayOfByte);
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                Log.w("DrmConvertSession", "Buffer with data to convert is illegal. Convertsession: " + this.mConvertSessionId, localIllegalArgumentException);
            }
            catch (IllegalStateException localIllegalStateException)
            {
                Log.w("DrmConvertSession", "Could not convert data. Convertsession: " + this.mConvertSessionId, localIllegalStateException);
            }
        else
            throw new IllegalArgumentException("Parameter inBuffer is null");
        return arrayOfByte1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.util.DrmConvertSession
 * JD-Core Version:        0.6.2
 */