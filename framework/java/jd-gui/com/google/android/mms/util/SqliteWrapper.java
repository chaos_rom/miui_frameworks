package com.google.android.mms.util;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

public final class SqliteWrapper
{
    private static final String SQLITE_EXCEPTION_DETAIL_MESSAGE = "unable to open database file";
    private static final String TAG = "SqliteWrapper";

    public static void checkSQLiteException(Context paramContext, SQLiteException paramSQLiteException)
    {
        if (isLowMemory(paramSQLiteException))
        {
            Toast.makeText(paramContext, 17039651, 0).show();
            return;
        }
        throw paramSQLiteException;
    }

    public static int delete(Context paramContext, ContentResolver paramContentResolver, Uri paramUri, String paramString, String[] paramArrayOfString)
    {
        try
        {
            int j = paramContentResolver.delete(paramUri, paramString, paramArrayOfString);
            i = j;
            return i;
        }
        catch (SQLiteException localSQLiteException)
        {
            while (true)
            {
                Log.e("SqliteWrapper", "Catch a SQLiteException when delete: ", localSQLiteException);
                checkSQLiteException(paramContext, localSQLiteException);
                int i = -1;
            }
        }
    }

    public static Uri insert(Context paramContext, ContentResolver paramContentResolver, Uri paramUri, ContentValues paramContentValues)
    {
        try
        {
            Uri localUri2 = paramContentResolver.insert(paramUri, paramContentValues);
            localUri1 = localUri2;
            return localUri1;
        }
        catch (SQLiteException localSQLiteException)
        {
            while (true)
            {
                Log.e("SqliteWrapper", "Catch a SQLiteException when insert: ", localSQLiteException);
                checkSQLiteException(paramContext, localSQLiteException);
                Uri localUri1 = null;
            }
        }
    }

    private static boolean isLowMemory(Context paramContext)
    {
        if (paramContext == null);
        ActivityManager.MemoryInfo localMemoryInfo;
        for (boolean bool = false; ; bool = localMemoryInfo.lowMemory)
        {
            return bool;
            ActivityManager localActivityManager = (ActivityManager)paramContext.getSystemService("activity");
            localMemoryInfo = new ActivityManager.MemoryInfo();
            localActivityManager.getMemoryInfo(localMemoryInfo);
        }
    }

    private static boolean isLowMemory(SQLiteException paramSQLiteException)
    {
        return paramSQLiteException.getMessage().equals("unable to open database file");
    }

    public static Cursor query(Context paramContext, ContentResolver paramContentResolver, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        try
        {
            Cursor localCursor2 = paramContentResolver.query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2);
            localCursor1 = localCursor2;
            return localCursor1;
        }
        catch (SQLiteException localSQLiteException)
        {
            while (true)
            {
                Log.e("SqliteWrapper", "Catch a SQLiteException when query: ", localSQLiteException);
                checkSQLiteException(paramContext, localSQLiteException);
                Cursor localCursor1 = null;
            }
        }
    }

    public static boolean requery(Context paramContext, Cursor paramCursor)
    {
        try
        {
            boolean bool2 = paramCursor.requery();
            bool1 = bool2;
            return bool1;
        }
        catch (SQLiteException localSQLiteException)
        {
            while (true)
            {
                Log.e("SqliteWrapper", "Catch a SQLiteException when requery: ", localSQLiteException);
                checkSQLiteException(paramContext, localSQLiteException);
                boolean bool1 = false;
            }
        }
    }

    public static int update(Context paramContext, ContentResolver paramContentResolver, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
    {
        try
        {
            int j = paramContentResolver.update(paramUri, paramContentValues, paramString, paramArrayOfString);
            i = j;
            return i;
        }
        catch (SQLiteException localSQLiteException)
        {
            while (true)
            {
                Log.e("SqliteWrapper", "Catch a SQLiteException when update: ", localSQLiteException);
                checkSQLiteException(paramContext, localSQLiteException);
                int i = -1;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.util.SqliteWrapper
 * JD-Core Version:        0.6.2
 */