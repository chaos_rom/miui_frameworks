package com.google.android.mms.util;

import android.content.Context;
import android.drm.DrmManagerClient;
import android.util.Log;

public class DownloadDrmHelper
{
    public static final String EXTENSION_DRM_MESSAGE = ".dm";
    public static final String EXTENSION_INTERNAL_FWDL = ".fl";
    public static final String MIMETYPE_DRM_MESSAGE = "application/vnd.oma.drm.message";
    private static final String TAG = "DownloadDrmHelper";

    public static String getOriginalMimeType(Context paramContext, String paramString1, String paramString2)
    {
        Object localObject = paramString2;
        DrmManagerClient localDrmManagerClient = new DrmManagerClient(paramContext);
        try
        {
            if (localDrmManagerClient.canHandle(paramString1, null))
            {
                String str = localDrmManagerClient.getOriginalMimeType(paramString1);
                localObject = str;
            }
            return localObject;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
                Log.w("DownloadDrmHelper", "Can't get original mime type since path is null or empty string.");
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Log.w("DownloadDrmHelper", "DrmManagerClient didn't initialize properly.");
        }
    }

    public static boolean isDrmConvertNeeded(String paramString)
    {
        return "application/vnd.oma.drm.message".equals(paramString);
    }

    public static boolean isDrmMimeType(Context paramContext, String paramString)
    {
        boolean bool1 = false;
        if (paramContext != null);
        try
        {
            DrmManagerClient localDrmManagerClient = new DrmManagerClient(paramContext);
            if ((localDrmManagerClient != null) && (paramString != null) && (paramString.length() > 0))
            {
                boolean bool2 = localDrmManagerClient.canHandle("", paramString);
                bool1 = bool2;
            }
            return bool1;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
                Log.w("DownloadDrmHelper", "DrmManagerClient instance could not be created, context is Illegal.");
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Log.w("DownloadDrmHelper", "DrmManagerClient didn't initialize properly.");
        }
    }

    public static String modifyDrmFwLockFileExtension(String paramString)
    {
        if (paramString != null)
        {
            int i = paramString.lastIndexOf(".");
            if (i != -1)
                paramString = paramString.substring(0, i);
            paramString = paramString.concat(".fl");
        }
        return paramString;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.util.DownloadDrmHelper
 * JD-Core Version:        0.6.2
 */