package com.google.android.mms.util;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.Telephony.Mms;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class PduCache extends AbstractCache<Uri, PduCacheEntry>
{
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final HashMap<Integer, Integer> MATCH_TO_MSGBOX_ID_MAP;
    private static final int MMS_ALL = 0;
    private static final int MMS_ALL_ID = 1;
    private static final int MMS_CONVERSATION = 10;
    private static final int MMS_CONVERSATION_ID = 11;
    private static final int MMS_DRAFTS = 6;
    private static final int MMS_DRAFTS_ID = 7;
    private static final int MMS_INBOX = 2;
    private static final int MMS_INBOX_ID = 3;
    private static final int MMS_OUTBOX = 8;
    private static final int MMS_OUTBOX_ID = 9;
    private static final int MMS_SENT = 4;
    private static final int MMS_SENT_ID = 5;
    private static final String TAG = "PduCache";
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1);
    private static PduCache sInstance;
    private final HashMap<Integer, HashSet<Uri>> mMessageBoxes = new HashMap();
    private final HashMap<Long, HashSet<Uri>> mThreads = new HashMap();
    private final HashSet<Uri> mUpdating = new HashSet();

    static
    {
        URI_MATCHER.addURI("mms", null, 0);
        URI_MATCHER.addURI("mms", "#", 1);
        URI_MATCHER.addURI("mms", "inbox", 2);
        URI_MATCHER.addURI("mms", "inbox/#", 3);
        URI_MATCHER.addURI("mms", "sent", 4);
        URI_MATCHER.addURI("mms", "sent/#", 5);
        URI_MATCHER.addURI("mms", "drafts", 6);
        URI_MATCHER.addURI("mms", "drafts/#", 7);
        URI_MATCHER.addURI("mms", "outbox", 8);
        URI_MATCHER.addURI("mms", "outbox/#", 9);
        URI_MATCHER.addURI("mms-sms", "conversations", 10);
        URI_MATCHER.addURI("mms-sms", "conversations/#", 11);
        MATCH_TO_MSGBOX_ID_MAP = new HashMap();
        MATCH_TO_MSGBOX_ID_MAP.put(Integer.valueOf(2), Integer.valueOf(1));
        MATCH_TO_MSGBOX_ID_MAP.put(Integer.valueOf(4), Integer.valueOf(2));
        MATCH_TO_MSGBOX_ID_MAP.put(Integer.valueOf(6), Integer.valueOf(3));
        MATCH_TO_MSGBOX_ID_MAP.put(Integer.valueOf(8), Integer.valueOf(4));
    }

    /** @deprecated */
    public static final PduCache getInstance()
    {
        try
        {
            if (sInstance == null)
                sInstance = new PduCache();
            PduCache localPduCache = sInstance;
            return localPduCache;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private Uri normalizeKey(Uri paramUri)
    {
        Object localObject;
        switch (URI_MATCHER.match(paramUri))
        {
        case 2:
        case 4:
        case 6:
        case 8:
        default:
            localObject = null;
            return localObject;
        case 1:
        case 3:
        case 5:
        case 7:
        case 9:
        }
        String str;
        for (Uri localUri = paramUri; ; localUri = Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, str))
        {
            localObject = localUri;
            break;
            str = paramUri.getLastPathSegment();
        }
    }

    private void purgeByMessageBox(Integer paramInteger)
    {
        if (paramInteger != null)
        {
            HashSet localHashSet = (HashSet)this.mMessageBoxes.remove(paramInteger);
            if (localHashSet != null)
            {
                Iterator localIterator = localHashSet.iterator();
                while (localIterator.hasNext())
                {
                    Uri localUri = (Uri)localIterator.next();
                    this.mUpdating.remove(localUri);
                    PduCacheEntry localPduCacheEntry = (PduCacheEntry)super.purge(localUri);
                    if (localPduCacheEntry != null)
                        removeFromThreads(localUri, localPduCacheEntry);
                }
            }
        }
    }

    private void purgeByThreadId(long paramLong)
    {
        HashSet localHashSet = (HashSet)this.mThreads.remove(Long.valueOf(paramLong));
        if (localHashSet != null)
        {
            Iterator localIterator = localHashSet.iterator();
            while (localIterator.hasNext())
            {
                Uri localUri = (Uri)localIterator.next();
                this.mUpdating.remove(localUri);
                PduCacheEntry localPduCacheEntry = (PduCacheEntry)super.purge(localUri);
                if (localPduCacheEntry != null)
                    removeFromMessageBoxes(localUri, localPduCacheEntry);
            }
        }
    }

    private PduCacheEntry purgeSingleEntry(Uri paramUri)
    {
        this.mUpdating.remove(paramUri);
        PduCacheEntry localPduCacheEntry = (PduCacheEntry)super.purge(paramUri);
        if (localPduCacheEntry != null)
        {
            removeFromThreads(paramUri, localPduCacheEntry);
            removeFromMessageBoxes(paramUri, localPduCacheEntry);
        }
        while (true)
        {
            return localPduCacheEntry;
            localPduCacheEntry = null;
        }
    }

    private void removeFromMessageBoxes(Uri paramUri, PduCacheEntry paramPduCacheEntry)
    {
        HashSet localHashSet = (HashSet)this.mThreads.get(Long.valueOf(paramPduCacheEntry.getMessageBox()));
        if (localHashSet != null)
            localHashSet.remove(paramUri);
    }

    private void removeFromThreads(Uri paramUri, PduCacheEntry paramPduCacheEntry)
    {
        HashSet localHashSet = (HashSet)this.mThreads.get(Long.valueOf(paramPduCacheEntry.getThreadId()));
        if (localHashSet != null)
            localHashSet.remove(paramUri);
    }

    /** @deprecated */
    public boolean isUpdating(Uri paramUri)
    {
        try
        {
            boolean bool = this.mUpdating.contains(paramUri);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public PduCacheEntry purge(Uri paramUri)
    {
        try
        {
            int i = URI_MATCHER.match(paramUri);
            PduCacheEntry localPduCacheEntry;
            switch (i)
            {
            default:
                localPduCacheEntry = null;
            case 1:
            case 3:
            case 5:
            case 7:
            case 9:
            case 0:
            case 10:
            case 2:
            case 4:
            case 6:
            case 8:
            case 11:
            }
            while (true)
            {
                return localPduCacheEntry;
                localPduCacheEntry = purgeSingleEntry(paramUri);
                continue;
                String str = paramUri.getLastPathSegment();
                localPduCacheEntry = purgeSingleEntry(Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, str));
                continue;
                purgeAll();
                localPduCacheEntry = null;
                continue;
                purgeByMessageBox((Integer)MATCH_TO_MSGBOX_ID_MAP.get(Integer.valueOf(i)));
                localPduCacheEntry = null;
                continue;
                purgeByThreadId(ContentUris.parseId(paramUri));
                localPduCacheEntry = null;
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void purgeAll()
    {
        try
        {
            super.purgeAll();
            this.mMessageBoxes.clear();
            this.mThreads.clear();
            this.mUpdating.clear();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean put(Uri paramUri, PduCacheEntry paramPduCacheEntry)
    {
        try
        {
            int i = paramPduCacheEntry.getMessageBox();
            HashSet localHashSet1 = (HashSet)this.mMessageBoxes.get(Integer.valueOf(i));
            if (localHashSet1 == null)
            {
                localHashSet1 = new HashSet();
                this.mMessageBoxes.put(Integer.valueOf(i), localHashSet1);
            }
            long l = paramPduCacheEntry.getThreadId();
            HashSet localHashSet2 = (HashSet)this.mThreads.get(Long.valueOf(l));
            if (localHashSet2 == null)
            {
                localHashSet2 = new HashSet();
                this.mThreads.put(Long.valueOf(l), localHashSet2);
            }
            Uri localUri = normalizeKey(paramUri);
            boolean bool = super.put(localUri, paramPduCacheEntry);
            if (bool)
            {
                localHashSet1.add(localUri);
                localHashSet2.add(localUri);
            }
            setUpdating(paramUri, false);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setUpdating(Uri paramUri, boolean paramBoolean)
    {
        if (paramBoolean);
        try
        {
            this.mUpdating.add(paramUri);
            while (true)
            {
                return;
                this.mUpdating.remove(paramUri);
            }
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.util.PduCache
 * JD-Core Version:        0.6.2
 */