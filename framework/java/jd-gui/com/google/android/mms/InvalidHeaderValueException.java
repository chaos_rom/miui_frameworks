package com.google.android.mms;

public class InvalidHeaderValueException extends MmsException
{
    private static final long serialVersionUID = -2053384496042052262L;

    public InvalidHeaderValueException()
    {
    }

    public InvalidHeaderValueException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.InvalidHeaderValueException
 * JD-Core Version:        0.6.2
 */