package com.google.android.mms.pdu;

public class Base64
{
    static final int BASELENGTH = 255;
    static final int FOURBYTE = 4;
    static final byte PAD = 61;
    private static byte[] base64Alphabet = new byte['ÿ'];

    static
    {
        for (int i = 0; i < 255; i++)
            base64Alphabet[i] = -1;
        for (int j = 90; j >= 65; j--)
            base64Alphabet[j] = ((byte)(j - 65));
        for (int k = 122; k >= 97; k--)
            base64Alphabet[k] = ((byte)(26 + (k - 97)));
        for (int m = 57; m >= 48; m--)
            base64Alphabet[m] = ((byte)(52 + (m - 48)));
        base64Alphabet[43] = 62;
        base64Alphabet[47] = 63;
    }

    public static byte[] decodeBase64(byte[] paramArrayOfByte)
    {
        byte[] arrayOfByte1 = discardNonBase64(paramArrayOfByte);
        if (arrayOfByte1.length == 0);
        int i;
        int j;
        int k;
        for (byte[] arrayOfByte2 = new byte[0]; ; arrayOfByte2 = new byte[0])
        {
            return arrayOfByte2;
            i = arrayOfByte1.length / 4;
            j = 0;
            k = arrayOfByte1.length;
            do
            {
                if (arrayOfByte1[(k - 1)] != 61)
                    break;
                k--;
            }
            while (k != 0);
        }
        arrayOfByte2 = new byte[k - i];
        int m = 0;
        label67: int i1;
        int i2;
        int i3;
        int i4;
        if (m < i)
        {
            int n = m * 4;
            i1 = arrayOfByte1[(n + 2)];
            i2 = arrayOfByte1[(n + 3)];
            i3 = base64Alphabet[arrayOfByte1[n]];
            i4 = base64Alphabet[arrayOfByte1[(n + 1)]];
            if ((i1 == 61) || (i2 == 61))
                break label207;
            int i6 = base64Alphabet[i1];
            int i7 = base64Alphabet[i2];
            arrayOfByte2[j] = ((byte)(i3 << 2 | i4 >> 4));
            arrayOfByte2[(j + 1)] = ((byte)((i4 & 0xF) << 4 | 0xF & i6 >> 2));
            arrayOfByte2[(j + 2)] = ((byte)(i7 | i6 << 6));
        }
        while (true)
        {
            j += 3;
            m++;
            break label67;
            break;
            label207: if (i1 == 61)
            {
                arrayOfByte2[j] = ((byte)(i3 << 2 | i4 >> 4));
            }
            else if (i2 == 61)
            {
                int i5 = base64Alphabet[i1];
                arrayOfByte2[j] = ((byte)(i3 << 2 | i4 >> 4));
                arrayOfByte2[(j + 1)] = ((byte)((i4 & 0xF) << 4 | 0xF & i5 >> 2));
            }
        }
    }

    static byte[] discardNonBase64(byte[] paramArrayOfByte)
    {
        byte[] arrayOfByte1 = new byte[paramArrayOfByte.length];
        int i = 0;
        for (int j = 0; j < paramArrayOfByte.length; j++)
            if (isBase64(paramArrayOfByte[j]))
            {
                int k = i + 1;
                arrayOfByte1[i] = paramArrayOfByte[j];
                i = k;
            }
        byte[] arrayOfByte2 = new byte[i];
        System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, i);
        return arrayOfByte2;
    }

    private static boolean isBase64(byte paramByte)
    {
        boolean bool = true;
        if (paramByte == 61);
        while (true)
        {
            return bool;
            if (base64Alphabet[paramByte] == -1)
                bool = false;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.Base64
 * JD-Core Version:        0.6.2
 */