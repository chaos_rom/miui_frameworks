package com.google.android.mms.pdu;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class EncodedStringValue
    implements Cloneable
{
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "EncodedStringValue";
    private int mCharacterSet;
    private byte[] mData;

    public EncodedStringValue(int paramInt, byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte == null)
            throw new NullPointerException("EncodedStringValue: Text-string is null.");
        this.mCharacterSet = paramInt;
        this.mData = new byte[paramArrayOfByte.length];
        System.arraycopy(paramArrayOfByte, 0, this.mData, 0, paramArrayOfByte.length);
    }

    public EncodedStringValue(String paramString)
    {
        try
        {
            this.mData = paramString.getBytes("utf-8");
            this.mCharacterSet = 106;
            return;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
            while (true)
                Log.e("EncodedStringValue", "Default encoding must be supported.", localUnsupportedEncodingException);
        }
    }

    public EncodedStringValue(byte[] paramArrayOfByte)
    {
        this(106, paramArrayOfByte);
    }

    public static String concat(EncodedStringValue[] paramArrayOfEncodedStringValue)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = -1 + paramArrayOfEncodedStringValue.length;
        for (int j = 0; j <= i; j++)
        {
            localStringBuilder.append(paramArrayOfEncodedStringValue[j].getString());
            if (j < i)
                localStringBuilder.append(";");
        }
        return localStringBuilder.toString();
    }

    public static EncodedStringValue copy(EncodedStringValue paramEncodedStringValue)
    {
        if (paramEncodedStringValue == null);
        for (EncodedStringValue localEncodedStringValue = null; ; localEncodedStringValue = new EncodedStringValue(paramEncodedStringValue.mCharacterSet, paramEncodedStringValue.mData))
            return localEncodedStringValue;
    }

    public static EncodedStringValue[] encodeStrings(String[] paramArrayOfString)
    {
        int i = paramArrayOfString.length;
        if (i > 0)
        {
            arrayOfEncodedStringValue = new EncodedStringValue[i];
            for (int j = 0; j < i; j++)
                arrayOfEncodedStringValue[j] = new EncodedStringValue(paramArrayOfString[j]);
        }
        EncodedStringValue[] arrayOfEncodedStringValue = null;
        return arrayOfEncodedStringValue;
    }

    public static EncodedStringValue[] extract(String paramString)
    {
        String[] arrayOfString = paramString.split(";");
        ArrayList localArrayList = new ArrayList();
        for (int i = 0; i < arrayOfString.length; i++)
            if (arrayOfString[i].length() > 0)
                localArrayList.add(new EncodedStringValue(arrayOfString[i]));
        int j = localArrayList.size();
        if (j > 0);
        for (EncodedStringValue[] arrayOfEncodedStringValue = (EncodedStringValue[])localArrayList.toArray(new EncodedStringValue[j]); ; arrayOfEncodedStringValue = null)
            return arrayOfEncodedStringValue;
    }

    public void appendTextString(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte == null)
            throw new NullPointerException("Text-string is null.");
        if (this.mData == null)
        {
            this.mData = new byte[paramArrayOfByte.length];
            System.arraycopy(paramArrayOfByte, 0, this.mData, 0, paramArrayOfByte.length);
        }
        while (true)
        {
            return;
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
            try
            {
                localByteArrayOutputStream.write(this.mData);
                localByteArrayOutputStream.write(paramArrayOfByte);
                this.mData = localByteArrayOutputStream.toByteArray();
            }
            catch (IOException localIOException)
            {
                localIOException.printStackTrace();
            }
        }
        throw new NullPointerException("appendTextString: failed when write a new Text-string");
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        super.clone();
        int i = this.mData.length;
        byte[] arrayOfByte = new byte[i];
        System.arraycopy(this.mData, 0, arrayOfByte, 0, i);
        try
        {
            EncodedStringValue localEncodedStringValue = new EncodedStringValue(this.mCharacterSet, arrayOfByte);
            return localEncodedStringValue;
        }
        catch (Exception localException)
        {
            Log.e("EncodedStringValue", "failed to clone an EncodedStringValue: " + this);
            localException.printStackTrace();
            throw new CloneNotSupportedException(localException.getMessage());
        }
    }

    public int getCharacterSet()
    {
        return this.mCharacterSet;
    }

    public String getString()
    {
        String str1;
        if (this.mCharacterSet == 0)
            str1 = new String(this.mData);
        while (true)
        {
            return str1;
            try
            {
                String str2 = CharacterSets.getMimeName(this.mCharacterSet);
                str1 = new String(this.mData, str2);
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException1)
            {
                try
                {
                    str1 = new String(this.mData, "iso-8859-1");
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException2)
                {
                    str1 = new String(this.mData);
                }
            }
        }
    }

    public byte[] getTextString()
    {
        byte[] arrayOfByte = new byte[this.mData.length];
        System.arraycopy(this.mData, 0, arrayOfByte, 0, this.mData.length);
        return arrayOfByte;
    }

    public void setCharacterSet(int paramInt)
    {
        this.mCharacterSet = paramInt;
    }

    public void setTextString(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte == null)
            throw new NullPointerException("EncodedStringValue: Text-string is null.");
        this.mData = new byte[paramArrayOfByte.length];
        System.arraycopy(paramArrayOfByte, 0, this.mData, 0, paramArrayOfByte.length);
    }

    public EncodedStringValue[] split(String paramString)
    {
        String[] arrayOfString = getString().split(paramString);
        EncodedStringValue[] arrayOfEncodedStringValue = new EncodedStringValue[arrayOfString.length];
        int i = 0;
        while (i < arrayOfEncodedStringValue.length)
            try
            {
                arrayOfEncodedStringValue[i] = new EncodedStringValue(this.mCharacterSet, arrayOfString[i].getBytes());
                i++;
            }
            catch (NullPointerException localNullPointerException)
            {
                arrayOfEncodedStringValue = null;
            }
        return arrayOfEncodedStringValue;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.EncodedStringValue
 * JD-Core Version:        0.6.2
 */