package com.google.android.mms.pdu;

public class PduContentTypes
{
    static final String[] contentTypes = arrayOfString;

    static
    {
        String[] arrayOfString = new String[83];
        arrayOfString[0] = "*/*";
        arrayOfString[1] = "text/*";
        arrayOfString[2] = "text/html";
        arrayOfString[3] = "text/plain";
        arrayOfString[4] = "text/x-hdml";
        arrayOfString[5] = "text/x-ttml";
        arrayOfString[6] = "text/x-vCalendar";
        arrayOfString[7] = "text/x-vCard";
        arrayOfString[8] = "text/vnd.wap.wml";
        arrayOfString[9] = "text/vnd.wap.wmlscript";
        arrayOfString[10] = "text/vnd.wap.wta-event";
        arrayOfString[11] = "multipart/*";
        arrayOfString[12] = "multipart/mixed";
        arrayOfString[13] = "multipart/form-data";
        arrayOfString[14] = "multipart/byterantes";
        arrayOfString[15] = "multipart/alternative";
        arrayOfString[16] = "application/*";
        arrayOfString[17] = "application/java-vm";
        arrayOfString[18] = "application/x-www-form-urlencoded";
        arrayOfString[19] = "application/x-hdmlc";
        arrayOfString[20] = "application/vnd.wap.wmlc";
        arrayOfString[21] = "application/vnd.wap.wmlscriptc";
        arrayOfString[22] = "application/vnd.wap.wta-eventc";
        arrayOfString[23] = "application/vnd.wap.uaprof";
        arrayOfString[24] = "application/vnd.wap.wtls-ca-certificate";
        arrayOfString[25] = "application/vnd.wap.wtls-user-certificate";
        arrayOfString[26] = "application/x-x509-ca-cert";
        arrayOfString[27] = "application/x-x509-user-cert";
        arrayOfString[28] = "image/*";
        arrayOfString[29] = "image/gif";
        arrayOfString[30] = "image/jpeg";
        arrayOfString[31] = "image/tiff";
        arrayOfString[32] = "image/png";
        arrayOfString[33] = "image/vnd.wap.wbmp";
        arrayOfString[34] = "application/vnd.wap.multipart.*";
        arrayOfString[35] = "application/vnd.wap.multipart.mixed";
        arrayOfString[36] = "application/vnd.wap.multipart.form-data";
        arrayOfString[37] = "application/vnd.wap.multipart.byteranges";
        arrayOfString[38] = "application/vnd.wap.multipart.alternative";
        arrayOfString[39] = "application/xml";
        arrayOfString[40] = "text/xml";
        arrayOfString[41] = "application/vnd.wap.wbxml";
        arrayOfString[42] = "application/x-x968-cross-cert";
        arrayOfString[43] = "application/x-x968-ca-cert";
        arrayOfString[44] = "application/x-x968-user-cert";
        arrayOfString[45] = "text/vnd.wap.si";
        arrayOfString[46] = "application/vnd.wap.sic";
        arrayOfString[47] = "text/vnd.wap.sl";
        arrayOfString[48] = "application/vnd.wap.slc";
        arrayOfString[49] = "text/vnd.wap.co";
        arrayOfString[50] = "application/vnd.wap.coc";
        arrayOfString[51] = "application/vnd.wap.multipart.related";
        arrayOfString[52] = "application/vnd.wap.sia";
        arrayOfString[53] = "text/vnd.wap.connectivity-xml";
        arrayOfString[54] = "application/vnd.wap.connectivity-wbxml";
        arrayOfString[55] = "application/pkcs7-mime";
        arrayOfString[56] = "application/vnd.wap.hashed-certificate";
        arrayOfString[57] = "application/vnd.wap.signed-certificate";
        arrayOfString[58] = "application/vnd.wap.cert-response";
        arrayOfString[59] = "application/xhtml+xml";
        arrayOfString[60] = "application/wml+xml";
        arrayOfString[61] = "text/css";
        arrayOfString[62] = "application/vnd.wap.mms-message";
        arrayOfString[63] = "application/vnd.wap.rollover-certificate";
        arrayOfString[64] = "application/vnd.wap.locc+wbxml";
        arrayOfString[65] = "application/vnd.wap.loc+xml";
        arrayOfString[66] = "application/vnd.syncml.dm+wbxml";
        arrayOfString[67] = "application/vnd.syncml.dm+xml";
        arrayOfString[68] = "application/vnd.syncml.notification";
        arrayOfString[69] = "application/vnd.wap.xhtml+xml";
        arrayOfString[70] = "application/vnd.wv.csp.cir";
        arrayOfString[71] = "application/vnd.oma.dd+xml";
        arrayOfString[72] = "application/vnd.oma.drm.message";
        arrayOfString[73] = "application/vnd.oma.drm.content";
        arrayOfString[74] = "application/vnd.oma.drm.rights+xml";
        arrayOfString[75] = "application/vnd.oma.drm.rights+wbxml";
        arrayOfString[76] = "application/vnd.wv.csp+xml";
        arrayOfString[77] = "application/vnd.wv.csp+wbxml";
        arrayOfString[78] = "application/vnd.syncml.ds.notification";
        arrayOfString[79] = "audio/*";
        arrayOfString[80] = "video/*";
        arrayOfString[81] = "application/vnd.oma.dd2+xml";
        arrayOfString[82] = "application/mikey";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.PduContentTypes
 * JD-Core Version:        0.6.2
 */