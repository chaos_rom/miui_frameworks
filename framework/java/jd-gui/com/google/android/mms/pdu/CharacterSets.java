package com.google.android.mms.pdu;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class CharacterSets
{
    public static final int ANY_CHARSET = 0;
    public static final int BIG5 = 2026;
    public static final int DEFAULT_CHARSET = 106;
    public static final String DEFAULT_CHARSET_NAME = "utf-8";
    public static final int ISO_8859_1 = 4;
    public static final int ISO_8859_2 = 5;
    public static final int ISO_8859_3 = 6;
    public static final int ISO_8859_4 = 7;
    public static final int ISO_8859_5 = 8;
    public static final int ISO_8859_6 = 9;
    public static final int ISO_8859_7 = 10;
    public static final int ISO_8859_8 = 11;
    public static final int ISO_8859_9 = 12;
    private static final int[] MIBENUM_NUMBERS;
    private static final HashMap<Integer, String> MIBENUM_TO_NAME_MAP;
    public static final String MIMENAME_ANY_CHARSET = "*";
    public static final String MIMENAME_BIG5 = "big5";
    public static final String MIMENAME_ISO_8859_1 = "iso-8859-1";
    public static final String MIMENAME_ISO_8859_2 = "iso-8859-2";
    public static final String MIMENAME_ISO_8859_3 = "iso-8859-3";
    public static final String MIMENAME_ISO_8859_4 = "iso-8859-4";
    public static final String MIMENAME_ISO_8859_5 = "iso-8859-5";
    public static final String MIMENAME_ISO_8859_6 = "iso-8859-6";
    public static final String MIMENAME_ISO_8859_7 = "iso-8859-7";
    public static final String MIMENAME_ISO_8859_8 = "iso-8859-8";
    public static final String MIMENAME_ISO_8859_9 = "iso-8859-9";
    public static final String MIMENAME_SHIFT_JIS = "shift_JIS";
    public static final String MIMENAME_UCS2 = "iso-10646-ucs-2";
    public static final String MIMENAME_US_ASCII = "us-ascii";
    public static final String MIMENAME_UTF_16 = "utf-16";
    public static final String MIMENAME_UTF_8 = "utf-8";
    private static final String[] MIME_NAMES;
    private static final HashMap<String, Integer> NAME_TO_MIBENUM_MAP;
    public static final int SHIFT_JIS = 17;
    public static final int UCS2 = 1000;
    public static final int US_ASCII = 3;
    public static final int UTF_16 = 1015;
    public static final int UTF_8 = 106;

    static
    {
        if (!CharacterSets.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            int[] arrayOfInt = new int[16];
            arrayOfInt[0] = 0;
            arrayOfInt[1] = 3;
            arrayOfInt[2] = 4;
            arrayOfInt[3] = 5;
            arrayOfInt[4] = 6;
            arrayOfInt[5] = 7;
            arrayOfInt[6] = 8;
            arrayOfInt[7] = 9;
            arrayOfInt[8] = 10;
            arrayOfInt[9] = 11;
            arrayOfInt[10] = 12;
            arrayOfInt[11] = 17;
            arrayOfInt[12] = 106;
            arrayOfInt[13] = 2026;
            arrayOfInt[14] = 1000;
            arrayOfInt[15] = 1015;
            MIBENUM_NUMBERS = arrayOfInt;
            String[] arrayOfString = new String[16];
            arrayOfString[0] = "*";
            arrayOfString[1] = "us-ascii";
            arrayOfString[2] = "iso-8859-1";
            arrayOfString[3] = "iso-8859-2";
            arrayOfString[4] = "iso-8859-3";
            arrayOfString[5] = "iso-8859-4";
            arrayOfString[6] = "iso-8859-5";
            arrayOfString[7] = "iso-8859-6";
            arrayOfString[8] = "iso-8859-7";
            arrayOfString[9] = "iso-8859-8";
            arrayOfString[10] = "iso-8859-9";
            arrayOfString[11] = "shift_JIS";
            arrayOfString[12] = "utf-8";
            arrayOfString[13] = "big5";
            arrayOfString[14] = "iso-10646-ucs-2";
            arrayOfString[15] = "utf-16";
            MIME_NAMES = arrayOfString;
            MIBENUM_TO_NAME_MAP = new HashMap();
            NAME_TO_MIBENUM_MAP = new HashMap();
            if (($assertionsDisabled) || (MIBENUM_NUMBERS.length == MIME_NAMES.length))
                break;
            throw new AssertionError();
        }
        int i = -1 + MIBENUM_NUMBERS.length;
        for (int j = 0; j <= i; j++)
        {
            MIBENUM_TO_NAME_MAP.put(Integer.valueOf(MIBENUM_NUMBERS[j]), MIME_NAMES[j]);
            NAME_TO_MIBENUM_MAP.put(MIME_NAMES[j], Integer.valueOf(MIBENUM_NUMBERS[j]));
        }
    }

    public static int getMibEnumValue(String paramString)
        throws UnsupportedEncodingException
    {
        if (paramString == null);
        Integer localInteger;
        for (int i = -1; ; i = localInteger.intValue())
        {
            return i;
            localInteger = (Integer)NAME_TO_MIBENUM_MAP.get(paramString);
            if (localInteger == null)
                throw new UnsupportedEncodingException();
        }
    }

    public static String getMimeName(int paramInt)
        throws UnsupportedEncodingException
    {
        String str = (String)MIBENUM_TO_NAME_MAP.get(Integer.valueOf(paramInt));
        if (str == null)
            throw new UnsupportedEncodingException();
        return str;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.CharacterSets
 * JD-Core Version:        0.6.2
 */