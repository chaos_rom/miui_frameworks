package com.google.android.mms.pdu;

import android.content.ContentResolver;
import android.content.Context;
import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;

public class PduComposer
{
    private static final int END_STRING_FLAG = 0;
    private static final int LENGTH_QUOTE = 31;
    private static final int LONG_INTEGER_LENGTH_MAX = 8;
    private static final int PDU_COMPOSER_BLOCK_SIZE = 1024;
    private static final int PDU_COMPOSE_CONTENT_ERROR = 1;
    private static final int PDU_COMPOSE_FIELD_NOT_SET = 2;
    private static final int PDU_COMPOSE_FIELD_NOT_SUPPORTED = 3;
    private static final int PDU_COMPOSE_SUCCESS = 0;
    private static final int PDU_EMAIL_ADDRESS_TYPE = 2;
    private static final int PDU_IPV4_ADDRESS_TYPE = 3;
    private static final int PDU_IPV6_ADDRESS_TYPE = 4;
    private static final int PDU_PHONE_NUMBER_ADDRESS_TYPE = 1;
    private static final int PDU_UNKNOWN_ADDRESS_TYPE = 5;
    private static final int QUOTED_STRING_FLAG = 34;
    static final String REGEXP_EMAIL_ADDRESS_TYPE = "[a-zA-Z| ]*\\<{0,1}[a-zA-Z| ]+@{1}[a-zA-Z| ]+\\.{1}[a-zA-Z| ]+\\>{0,1}";
    static final String REGEXP_IPV4_ADDRESS_TYPE = "[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}";
    static final String REGEXP_IPV6_ADDRESS_TYPE = "[a-fA-F]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}";
    static final String REGEXP_PHONE_NUMBER_ADDRESS_TYPE = "\\+?[0-9|\\.|\\-]+";
    private static final int SHORT_INTEGER_MAX = 127;
    static final String STRING_IPV4_ADDRESS_TYPE = "/TYPE=IPV4";
    static final String STRING_IPV6_ADDRESS_TYPE = "/TYPE=IPV6";
    static final String STRING_PHONE_NUMBER_ADDRESS_TYPE = "/TYPE=PLMN";
    private static final int TEXT_MAX = 127;
    private static HashMap<String, Integer> mContentTypeMap;
    protected ByteArrayOutputStream mMessage = null;
    private GenericPdu mPdu = null;
    private PduHeaders mPduHeader = null;
    protected int mPosition = 0;
    private final ContentResolver mResolver;
    private BufferStack mStack = null;

    static
    {
        if (!PduComposer.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            mContentTypeMap = null;
            mContentTypeMap = new HashMap();
            for (int i = 0; i < PduContentTypes.contentTypes.length; i++)
                mContentTypeMap.put(PduContentTypes.contentTypes[i], Integer.valueOf(i));
        }
    }

    public PduComposer(Context paramContext, GenericPdu paramGenericPdu)
    {
        this.mPdu = paramGenericPdu;
        this.mResolver = paramContext.getContentResolver();
        this.mPduHeader = paramGenericPdu.getPduHeaders();
        this.mStack = new BufferStack(null);
        this.mMessage = new ByteArrayOutputStream();
        this.mPosition = 0;
    }

    private EncodedStringValue appendAddressType(EncodedStringValue paramEncodedStringValue)
    {
        try
        {
            int i = checkAddressType(paramEncodedStringValue.getString());
            EncodedStringValue localEncodedStringValue2 = EncodedStringValue.copy(paramEncodedStringValue);
            if (1 == i)
                localEncodedStringValue2.appendTextString("/TYPE=PLMN".getBytes());
            else if (3 == i)
                localEncodedStringValue2.appendTextString("/TYPE=IPV4".getBytes());
            else if (4 == i)
                localEncodedStringValue2.appendTextString("/TYPE=IPV6".getBytes());
            localEncodedStringValue1 = localEncodedStringValue2;
            return localEncodedStringValue1;
        }
        catch (NullPointerException localNullPointerException)
        {
            while (true)
                EncodedStringValue localEncodedStringValue1 = null;
        }
    }

    private int appendHeader(int paramInt)
    {
        int j;
        int i1;
        switch (paramInt)
        {
        case 131:
        case 132:
        case 135:
        case 140:
        case 142:
        case 146:
        case 147:
        case 148:
        case 153:
        case 154:
        default:
            j = 3;
            return j;
        case 141:
            appendOctet(paramInt);
            i1 = this.mPduHeader.getOctet(paramInt);
            if (i1 == 0)
                appendShortInteger(18);
            break;
        case 139:
        case 152:
        case 129:
        case 130:
        case 151:
        case 137:
        case 134:
        case 143:
        case 144:
        case 145:
        case 149:
        case 155:
        case 133:
        case 150:
        case 138:
        case 136:
        }
        while (true)
        {
            label156: j = 0;
            break;
            appendShortInteger(i1);
            continue;
            byte[] arrayOfByte2 = this.mPduHeader.getTextString(paramInt);
            if (arrayOfByte2 == null)
            {
                j = 2;
                break;
            }
            appendOctet(paramInt);
            appendTextString(arrayOfByte2);
            continue;
            EncodedStringValue[] arrayOfEncodedStringValue = this.mPduHeader.getEncodedStringValues(paramInt);
            if (arrayOfEncodedStringValue == null)
            {
                j = 2;
                break;
            }
            for (int n = 0; ; n++)
            {
                if (n >= arrayOfEncodedStringValue.length)
                    break label156;
                EncodedStringValue localEncodedStringValue4 = appendAddressType(arrayOfEncodedStringValue[n]);
                if (localEncodedStringValue4 == null)
                {
                    j = 1;
                    break;
                }
                appendOctet(paramInt);
                appendEncodedString(localEncodedStringValue4);
            }
            appendOctet(paramInt);
            EncodedStringValue localEncodedStringValue2 = this.mPduHeader.getEncodedStringValue(paramInt);
            if ((localEncodedStringValue2 == null) || (TextUtils.isEmpty(localEncodedStringValue2.getString())) || (new String(localEncodedStringValue2.getTextString()).equals("insert-address-token")))
            {
                append(1);
                append(129);
            }
            else
            {
                this.mStack.newbuf();
                PositionMarker localPositionMarker2 = this.mStack.mark();
                append(128);
                EncodedStringValue localEncodedStringValue3 = appendAddressType(localEncodedStringValue2);
                if (localEncodedStringValue3 == null)
                {
                    j = 1;
                    break;
                }
                appendEncodedString(localEncodedStringValue3);
                int m = localPositionMarker2.getLength();
                this.mStack.pop();
                appendValueLength(m);
                this.mStack.copy();
                continue;
                int k = this.mPduHeader.getOctet(paramInt);
                if (k == 0)
                {
                    j = 2;
                    break;
                }
                appendOctet(paramInt);
                appendOctet(k);
                continue;
                long l2 = this.mPduHeader.getLongInteger(paramInt);
                if (-1L == l2)
                {
                    j = 2;
                    break;
                }
                appendOctet(paramInt);
                appendDateValue(l2);
                continue;
                EncodedStringValue localEncodedStringValue1 = this.mPduHeader.getEncodedStringValue(paramInt);
                if (localEncodedStringValue1 == null)
                {
                    j = 2;
                    break;
                }
                appendOctet(paramInt);
                appendEncodedString(localEncodedStringValue1);
                continue;
                byte[] arrayOfByte1 = this.mPduHeader.getTextString(paramInt);
                if (arrayOfByte1 == null)
                {
                    j = 2;
                    break;
                }
                appendOctet(paramInt);
                if (Arrays.equals(arrayOfByte1, "advertisement".getBytes()))
                {
                    appendOctet(129);
                }
                else if (Arrays.equals(arrayOfByte1, "auto".getBytes()))
                {
                    appendOctet(131);
                }
                else if (Arrays.equals(arrayOfByte1, "personal".getBytes()))
                {
                    appendOctet(128);
                }
                else if (Arrays.equals(arrayOfByte1, "informational".getBytes()))
                {
                    appendOctet(130);
                }
                else
                {
                    appendTextString(arrayOfByte1);
                    continue;
                    long l1 = this.mPduHeader.getLongInteger(paramInt);
                    if (-1L == l1)
                    {
                        j = 2;
                        break;
                    }
                    appendOctet(paramInt);
                    this.mStack.newbuf();
                    PositionMarker localPositionMarker1 = this.mStack.mark();
                    append(129);
                    appendLongInteger(l1);
                    int i = localPositionMarker1.getLength();
                    this.mStack.pop();
                    appendValueLength(i);
                    this.mStack.copy();
                }
            }
        }
    }

    protected static int checkAddressType(String paramString)
    {
        int i = 5;
        if (paramString == null);
        while (true)
        {
            return i;
            if (paramString.matches("[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}"))
                i = 3;
            else if (paramString.matches("\\+?[0-9|\\.|\\-]+"))
                i = 1;
            else if (paramString.matches("[a-zA-Z| ]*\\<{0,1}[a-zA-Z| ]+@{1}[a-zA-Z| ]+\\.{1}[a-zA-Z| ]+\\>{0,1}"))
                i = 2;
            else if (paramString.matches("[a-fA-F]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}"))
                i = 4;
        }
    }

    private int makeAckInd()
    {
        int i = 1;
        if (this.mMessage == null)
        {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(140);
        appendOctet(133);
        if (appendHeader(152) != 0);
        while (true)
        {
            return i;
            if (appendHeader(141) == 0)
            {
                appendHeader(145);
                i = 0;
            }
        }
    }

    private int makeMessageBody()
    {
        this.mStack.newbuf();
        PositionMarker localPositionMarker1 = this.mStack.mark();
        String str1 = new String(this.mPduHeader.getTextString(132));
        Integer localInteger1 = (Integer)mContentTypeMap.get(str1);
        int i;
        if (localInteger1 == null)
            i = 1;
        while (true)
        {
            return i;
            appendShortInteger(localInteger1.intValue());
            PduBody localPduBody = ((SendReq)this.mPdu).getBody();
            if ((localPduBody == null) || (localPduBody.getPartsNum() == 0))
            {
                appendUintvarInteger(0L);
                this.mStack.pop();
                this.mStack.copy();
                i = 0;
            }
            else
            {
                while (true)
                {
                    int m;
                    label446: int i2;
                    int i3;
                    try
                    {
                        PduPart localPduPart2 = localPduBody.getPart(0);
                        byte[] arrayOfByte7 = localPduPart2.getContentId();
                        if (arrayOfByte7 != null)
                        {
                            appendOctet(138);
                            if ((60 == arrayOfByte7[0]) && (62 == arrayOfByte7[(-1 + arrayOfByte7.length)]))
                                appendTextString(arrayOfByte7);
                        }
                        else
                        {
                            appendOctet(137);
                            appendTextString(localPduPart2.getContentType());
                            int j = localPositionMarker1.getLength();
                            this.mStack.pop();
                            appendValueLength(j);
                            this.mStack.copy();
                            int k = localPduBody.getPartsNum();
                            appendUintvarInteger(k);
                            m = 0;
                            if (m >= k)
                                break label827;
                            localPduPart1 = localPduBody.getPart(m);
                            this.mStack.newbuf();
                            localPositionMarker2 = this.mStack.mark();
                            this.mStack.newbuf();
                            localPositionMarker3 = this.mStack.mark();
                            arrayOfByte1 = localPduPart1.getContentType();
                            if (arrayOfByte1 != null)
                                continue;
                            i = 1;
                            break;
                        }
                        StringBuilder localStringBuilder2 = new StringBuilder().append("<");
                        String str4 = new String(arrayOfByte7);
                        appendTextString(str4 + ">");
                        continue;
                    }
                    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
                    {
                        while (true)
                        {
                            PduPart localPduPart1;
                            PositionMarker localPositionMarker2;
                            PositionMarker localPositionMarker3;
                            byte[] arrayOfByte1;
                            localArrayIndexOutOfBoundsException.printStackTrace();
                            continue;
                            HashMap localHashMap = mContentTypeMap;
                            String str2 = new String(arrayOfByte1);
                            Integer localInteger2 = (Integer)localHashMap.get(str2);
                            if (localInteger2 == null)
                                appendTextString(arrayOfByte1);
                            byte[] arrayOfByte2;
                            while (true)
                            {
                                arrayOfByte2 = localPduPart1.getName();
                                if (arrayOfByte2 != null)
                                    break label446;
                                arrayOfByte2 = localPduPart1.getFilename();
                                if (arrayOfByte2 != null)
                                    break label446;
                                arrayOfByte2 = localPduPart1.getContentLocation();
                                if (arrayOfByte2 != null)
                                    break label446;
                                i = 1;
                                break;
                                appendShortInteger(localInteger2.intValue());
                            }
                            appendOctet(133);
                            appendTextString(arrayOfByte2);
                            int n = localPduPart1.getCharset();
                            if (n != 0)
                            {
                                appendOctet(129);
                                appendShortInteger(n);
                            }
                            int i1 = localPositionMarker3.getLength();
                            this.mStack.pop();
                            appendValueLength(i1);
                            this.mStack.copy();
                            byte[] arrayOfByte3 = localPduPart1.getContentId();
                            if (arrayOfByte3 != null)
                            {
                                appendOctet(192);
                                if ((60 != arrayOfByte3[0]) || (62 != arrayOfByte3[(-1 + arrayOfByte3.length)]))
                                    break label650;
                                appendQuotedString(arrayOfByte3);
                            }
                            while (true)
                            {
                                byte[] arrayOfByte4 = localPduPart1.getContentLocation();
                                if (arrayOfByte4 != null)
                                {
                                    appendOctet(142);
                                    appendTextString(arrayOfByte4);
                                }
                                i2 = localPositionMarker2.getLength();
                                i3 = 0;
                                byte[] arrayOfByte5 = localPduPart1.getData();
                                if (arrayOfByte5 == null)
                                    break;
                                arraycopy(arrayOfByte5, 0, arrayOfByte5.length);
                                i3 = arrayOfByte5.length;
                                int i5 = localPositionMarker2.getLength() - i2;
                                if (i3 == i5)
                                    break label793;
                                throw new RuntimeException("BUG: Length sanity check failed");
                                label650: StringBuilder localStringBuilder1 = new StringBuilder().append("<");
                                String str3 = new String(arrayOfByte3);
                                appendQuotedString(str3 + ">");
                            }
                            try
                            {
                                byte[] arrayOfByte6 = new byte[1024];
                                InputStream localInputStream = this.mResolver.openInputStream(localPduPart1.getDataUri());
                                while (true)
                                {
                                    int i4 = localInputStream.read(arrayOfByte6);
                                    if (i4 == -1)
                                        break;
                                    this.mMessage.write(arrayOfByte6, 0, i4);
                                    this.mPosition = (i4 + this.mPosition);
                                    i3 += i4;
                                }
                            }
                            catch (FileNotFoundException localFileNotFoundException)
                            {
                                i = 1;
                            }
                            catch (IOException localIOException)
                            {
                                i = 1;
                            }
                            catch (RuntimeException localRuntimeException)
                            {
                                i = 1;
                            }
                        }
                    }
                    break;
                    label793: this.mStack.pop();
                    appendUintvarInteger(i2);
                    appendUintvarInteger(i3);
                    this.mStack.copy();
                    m++;
                }
                label827: i = 0;
            }
        }
    }

    private int makeNotifyResp()
    {
        int i = 1;
        if (this.mMessage == null)
        {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(140);
        appendOctet(131);
        if (appendHeader(152) != 0);
        while (true)
        {
            return i;
            if ((appendHeader(141) == 0) && (appendHeader(149) == 0))
                i = 0;
        }
    }

    private int makeReadRecInd()
    {
        int i = 1;
        if (this.mMessage == null)
        {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(140);
        appendOctet(135);
        if (appendHeader(141) != 0);
        while (true)
        {
            return i;
            if ((appendHeader(139) == 0) && (appendHeader(151) == 0) && (appendHeader(137) == 0))
            {
                appendHeader(133);
                if (appendHeader(155) == 0)
                    i = 0;
            }
        }
    }

    private int makeSendReqPdu()
    {
        int i = 1;
        if (this.mMessage == null)
        {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(140);
        appendOctet(128);
        appendOctet(152);
        byte[] arrayOfByte = this.mPduHeader.getTextString(152);
        if (arrayOfByte == null)
            throw new IllegalArgumentException("Transaction-ID is null.");
        appendTextString(arrayOfByte);
        if (appendHeader(141) != 0);
        while (true)
        {
            return i;
            appendHeader(133);
            if (appendHeader(137) == 0)
            {
                int j = 0;
                if (appendHeader(151) != i)
                    j = 1;
                if (appendHeader(130) != i)
                    j = 1;
                if (appendHeader(129) != i)
                    j = 1;
                if (j != 0)
                {
                    appendHeader(150);
                    appendHeader(138);
                    appendHeader(136);
                    appendHeader(143);
                    appendHeader(134);
                    appendHeader(144);
                    appendOctet(132);
                    i = makeMessageBody();
                }
            }
        }
    }

    protected void append(int paramInt)
    {
        this.mMessage.write(paramInt);
        this.mPosition = (1 + this.mPosition);
    }

    protected void appendDateValue(long paramLong)
    {
        appendLongInteger(paramLong);
    }

    protected void appendEncodedString(EncodedStringValue paramEncodedStringValue)
    {
        assert (paramEncodedStringValue != null);
        int i = paramEncodedStringValue.getCharacterSet();
        byte[] arrayOfByte = paramEncodedStringValue.getTextString();
        if (arrayOfByte == null);
        while (true)
        {
            return;
            this.mStack.newbuf();
            PositionMarker localPositionMarker = this.mStack.mark();
            appendShortInteger(i);
            appendTextString(arrayOfByte);
            int j = localPositionMarker.getLength();
            this.mStack.pop();
            appendValueLength(j);
            this.mStack.copy();
        }
    }

    protected void appendLongInteger(long paramLong)
    {
        long l = paramLong;
        for (int i = 0; (l != 0L) && (i < 8); i++)
            l >>>= 8;
        appendShortLength(i);
        int j = 8 * (i - 1);
        for (int k = 0; k < i; k++)
        {
            append((int)(0xFF & paramLong >>> j));
            j -= 8;
        }
    }

    protected void appendOctet(int paramInt)
    {
        append(paramInt);
    }

    protected void appendQuotedString(String paramString)
    {
        appendQuotedString(paramString.getBytes());
    }

    protected void appendQuotedString(byte[] paramArrayOfByte)
    {
        append(34);
        arraycopy(paramArrayOfByte, 0, paramArrayOfByte.length);
        append(0);
    }

    protected void appendShortInteger(int paramInt)
    {
        append(0xFF & (paramInt | 0x80));
    }

    protected void appendShortLength(int paramInt)
    {
        append(paramInt);
    }

    protected void appendTextString(String paramString)
    {
        appendTextString(paramString.getBytes());
    }

    protected void appendTextString(byte[] paramArrayOfByte)
    {
        if ((0xFF & paramArrayOfByte[0]) > 127)
            append(127);
        arraycopy(paramArrayOfByte, 0, paramArrayOfByte.length);
        append(0);
    }

    protected void appendUintvarInteger(long paramLong)
    {
        long l = 127L;
        for (int i = 0; ; i++)
        {
            if ((i >= 5) || (paramLong < l))
                while (i > 0)
                {
                    append((int)(0xFF & (0x80 | 0x7F & paramLong >>> i * 7)));
                    i--;
                }
            l = 0x7F | l << 7;
        }
        append((int)(paramLong & 0x7F));
    }

    protected void appendValueLength(long paramLong)
    {
        if (paramLong < 31L)
            appendShortLength((int)paramLong);
        while (true)
        {
            return;
            append(31);
            appendUintvarInteger(paramLong);
        }
    }

    protected void arraycopy(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        this.mMessage.write(paramArrayOfByte, paramInt1, paramInt2);
        this.mPosition = (paramInt2 + this.mPosition);
    }

    public byte[] make()
    {
        byte[] arrayOfByte = null;
        switch (this.mPdu.getMessageType())
        {
        case 129:
        case 130:
        case 132:
        case 134:
        default:
        case 128:
        case 131:
        case 133:
        case 135:
        }
        while (true)
        {
            return arrayOfByte;
            if (makeSendReqPdu() == 0)
                do
                {
                    do
                    {
                        do
                        {
                            arrayOfByte = this.mMessage.toByteArray();
                            break;
                        }
                        while (makeNotifyResp() == 0);
                        break;
                    }
                    while (makeAckInd() == 0);
                    break;
                }
                while (makeReadRecInd() == 0);
        }
    }

    private class BufferStack
    {
        private PduComposer.LengthRecordNode stack = null;
        int stackSize = 0;
        private PduComposer.LengthRecordNode toCopy = null;

        private BufferStack()
        {
        }

        void copy()
        {
            PduComposer.this.arraycopy(this.toCopy.currentMessage.toByteArray(), 0, this.toCopy.currentPosition);
            this.toCopy = null;
        }

        PduComposer.PositionMarker mark()
        {
            PduComposer.PositionMarker localPositionMarker = new PduComposer.PositionMarker(PduComposer.this, null);
            PduComposer.PositionMarker.access$402(localPositionMarker, PduComposer.this.mPosition);
            PduComposer.PositionMarker.access$502(localPositionMarker, this.stackSize);
            return localPositionMarker;
        }

        void newbuf()
        {
            if (this.toCopy != null)
                throw new RuntimeException("BUG: Invalid newbuf() before copy()");
            PduComposer.LengthRecordNode localLengthRecordNode = new PduComposer.LengthRecordNode(null);
            localLengthRecordNode.currentMessage = PduComposer.this.mMessage;
            localLengthRecordNode.currentPosition = PduComposer.this.mPosition;
            localLengthRecordNode.next = this.stack;
            this.stack = localLengthRecordNode;
            this.stackSize = (1 + this.stackSize);
            PduComposer.this.mMessage = new ByteArrayOutputStream();
            PduComposer.this.mPosition = 0;
        }

        void pop()
        {
            ByteArrayOutputStream localByteArrayOutputStream = PduComposer.this.mMessage;
            int i = PduComposer.this.mPosition;
            PduComposer.this.mMessage = this.stack.currentMessage;
            PduComposer.this.mPosition = this.stack.currentPosition;
            this.toCopy = this.stack;
            this.stack = this.stack.next;
            this.stackSize = (-1 + this.stackSize);
            this.toCopy.currentMessage = localByteArrayOutputStream;
            this.toCopy.currentPosition = i;
        }
    }

    private class PositionMarker
    {
        private int c_pos;
        private int currentStackSize;

        private PositionMarker()
        {
        }

        int getLength()
        {
            if (this.currentStackSize != PduComposer.this.mStack.stackSize)
                throw new RuntimeException("BUG: Invalid call to getLength()");
            return PduComposer.this.mPosition - this.c_pos;
        }
    }

    private static class LengthRecordNode
    {
        ByteArrayOutputStream currentMessage = null;
        public int currentPosition = 0;
        public LengthRecordNode next = null;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.PduComposer
 * JD-Core Version:        0.6.2
 */