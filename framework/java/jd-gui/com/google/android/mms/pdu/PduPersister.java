package com.google.android.mms.pdu;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.drm.DrmManagerClient;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.Telephony.Mms.Draft;
import android.provider.Telephony.Mms.Inbox;
import android.provider.Telephony.Mms.Outbox;
import android.provider.Telephony.Mms.Sent;
import android.provider.Telephony.MmsSms.PendingMessages;
import android.provider.Telephony.Threads;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.mms.ContentType;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.MmsException;
import com.google.android.mms.util.PduCache;
import com.google.android.mms.util.SqliteWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class PduPersister
{
    private static final int[] ADDRESS_FIELDS;
    private static final HashMap<Integer, Integer> CHARSET_COLUMN_INDEX_MAP;
    private static final HashMap<Integer, String> CHARSET_COLUMN_NAME_MAP;
    private static final boolean DEBUG = false;
    private static final long DUMMY_THREAD_ID = 9223372036854775807L;
    private static final HashMap<Integer, Integer> ENCODED_STRING_COLUMN_INDEX_MAP;
    private static final HashMap<Integer, String> ENCODED_STRING_COLUMN_NAME_MAP;
    private static final boolean LOCAL_LOGV = false;
    private static final HashMap<Integer, Integer> LONG_COLUMN_INDEX_MAP;
    private static final HashMap<Integer, String> LONG_COLUMN_NAME_MAP;
    private static final HashMap<Uri, Integer> MESSAGE_BOX_MAP;
    private static final HashMap<Integer, Integer> OCTET_COLUMN_INDEX_MAP;
    private static final HashMap<Integer, String> OCTET_COLUMN_NAME_MAP;
    private static final int PART_COLUMN_CHARSET = 1;
    private static final int PART_COLUMN_CONTENT_DISPOSITION = 2;
    private static final int PART_COLUMN_CONTENT_ID = 3;
    private static final int PART_COLUMN_CONTENT_LOCATION = 4;
    private static final int PART_COLUMN_CONTENT_TYPE = 5;
    private static final int PART_COLUMN_FILENAME = 6;
    private static final int PART_COLUMN_ID = 0;
    private static final int PART_COLUMN_NAME = 7;
    private static final int PART_COLUMN_TEXT = 8;
    private static final String[] PART_PROJECTION;
    private static final PduCache PDU_CACHE_INSTANCE;
    private static final int PDU_COLUMN_CONTENT_CLASS = 11;
    private static final int PDU_COLUMN_CONTENT_LOCATION = 5;
    private static final int PDU_COLUMN_CONTENT_TYPE = 6;
    private static final int PDU_COLUMN_DATE = 21;
    private static final int PDU_COLUMN_DELIVERY_REPORT = 12;
    private static final int PDU_COLUMN_DELIVERY_TIME = 22;
    private static final int PDU_COLUMN_EXPIRY = 23;
    private static final int PDU_COLUMN_ID = 0;
    private static final int PDU_COLUMN_MESSAGE_BOX = 1;
    private static final int PDU_COLUMN_MESSAGE_CLASS = 7;
    private static final int PDU_COLUMN_MESSAGE_ID = 8;
    private static final int PDU_COLUMN_MESSAGE_SIZE = 24;
    private static final int PDU_COLUMN_MESSAGE_TYPE = 13;
    private static final int PDU_COLUMN_MMS_VERSION = 14;
    private static final int PDU_COLUMN_PRIORITY = 15;
    private static final int PDU_COLUMN_READ_REPORT = 16;
    private static final int PDU_COLUMN_READ_STATUS = 17;
    private static final int PDU_COLUMN_REPORT_ALLOWED = 18;
    private static final int PDU_COLUMN_RESPONSE_TEXT = 9;
    private static final int PDU_COLUMN_RETRIEVE_STATUS = 19;
    private static final int PDU_COLUMN_RETRIEVE_TEXT = 3;
    private static final int PDU_COLUMN_RETRIEVE_TEXT_CHARSET = 26;
    private static final int PDU_COLUMN_STATUS = 20;
    private static final int PDU_COLUMN_SUBJECT = 4;
    private static final int PDU_COLUMN_SUBJECT_CHARSET = 25;
    private static final int PDU_COLUMN_THREAD_ID = 2;
    private static final int PDU_COLUMN_TRANSACTION_ID = 10;
    private static final String[] PDU_PROJECTION;
    public static final int PROC_STATUS_COMPLETED = 3;
    public static final int PROC_STATUS_PERMANENTLY_FAILURE = 2;
    public static final int PROC_STATUS_TRANSIENT_FAILURE = 1;
    private static final String TAG = "PduPersister";
    public static final String TEMPORARY_DRM_OBJECT_URI = "content://mms/9223372036854775807/part";
    private static final HashMap<Integer, Integer> TEXT_STRING_COLUMN_INDEX_MAP;
    private static final HashMap<Integer, String> TEXT_STRING_COLUMN_NAME_MAP;
    private static PduPersister sPersister;
    private final ContentResolver mContentResolver;
    private final Context mContext;
    private final DrmManagerClient mDrmManagerClient;

    static
    {
        if (!PduPersister.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            int[] arrayOfInt = new int[4];
            arrayOfInt[0] = 129;
            arrayOfInt[1] = 130;
            arrayOfInt[2] = 137;
            arrayOfInt[3] = 151;
            ADDRESS_FIELDS = arrayOfInt;
            String[] arrayOfString1 = new String[27];
            arrayOfString1[0] = "_id";
            arrayOfString1[1] = "msg_box";
            arrayOfString1[2] = "thread_id";
            arrayOfString1[3] = "retr_txt";
            arrayOfString1[4] = "sub";
            arrayOfString1[5] = "ct_l";
            arrayOfString1[6] = "ct_t";
            arrayOfString1[7] = "m_cls";
            arrayOfString1[8] = "m_id";
            arrayOfString1[9] = "resp_txt";
            arrayOfString1[10] = "tr_id";
            arrayOfString1[11] = "ct_cls";
            arrayOfString1[12] = "d_rpt";
            arrayOfString1[13] = "m_type";
            arrayOfString1[14] = "v";
            arrayOfString1[15] = "pri";
            arrayOfString1[16] = "rr";
            arrayOfString1[17] = "read_status";
            arrayOfString1[18] = "rpt_a";
            arrayOfString1[19] = "retr_st";
            arrayOfString1[20] = "st";
            arrayOfString1[21] = "date";
            arrayOfString1[22] = "d_tm";
            arrayOfString1[23] = "exp";
            arrayOfString1[24] = "m_size";
            arrayOfString1[25] = "sub_cs";
            arrayOfString1[26] = "retr_txt_cs";
            PDU_PROJECTION = arrayOfString1;
            String[] arrayOfString2 = new String[9];
            arrayOfString2[0] = "_id";
            arrayOfString2[1] = "chset";
            arrayOfString2[2] = "cd";
            arrayOfString2[3] = "cid";
            arrayOfString2[4] = "cl";
            arrayOfString2[5] = "ct";
            arrayOfString2[6] = "fn";
            arrayOfString2[7] = "name";
            arrayOfString2[8] = "text";
            PART_PROJECTION = arrayOfString2;
            MESSAGE_BOX_MAP = new HashMap();
            MESSAGE_BOX_MAP.put(Telephony.Mms.Inbox.CONTENT_URI, Integer.valueOf(1));
            MESSAGE_BOX_MAP.put(Telephony.Mms.Sent.CONTENT_URI, Integer.valueOf(2));
            MESSAGE_BOX_MAP.put(Telephony.Mms.Draft.CONTENT_URI, Integer.valueOf(3));
            MESSAGE_BOX_MAP.put(Telephony.Mms.Outbox.CONTENT_URI, Integer.valueOf(4));
            CHARSET_COLUMN_INDEX_MAP = new HashMap();
            CHARSET_COLUMN_INDEX_MAP.put(Integer.valueOf(150), Integer.valueOf(25));
            CHARSET_COLUMN_INDEX_MAP.put(Integer.valueOf(154), Integer.valueOf(26));
            CHARSET_COLUMN_NAME_MAP = new HashMap();
            CHARSET_COLUMN_NAME_MAP.put(Integer.valueOf(150), "sub_cs");
            CHARSET_COLUMN_NAME_MAP.put(Integer.valueOf(154), "retr_txt_cs");
            ENCODED_STRING_COLUMN_INDEX_MAP = new HashMap();
            ENCODED_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(154), Integer.valueOf(3));
            ENCODED_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(150), Integer.valueOf(4));
            ENCODED_STRING_COLUMN_NAME_MAP = new HashMap();
            ENCODED_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(154), "retr_txt");
            ENCODED_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(150), "sub");
            TEXT_STRING_COLUMN_INDEX_MAP = new HashMap();
            TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(131), Integer.valueOf(5));
            TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(132), Integer.valueOf(6));
            TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(138), Integer.valueOf(7));
            TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(139), Integer.valueOf(8));
            TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(147), Integer.valueOf(9));
            TEXT_STRING_COLUMN_INDEX_MAP.put(Integer.valueOf(152), Integer.valueOf(10));
            TEXT_STRING_COLUMN_NAME_MAP = new HashMap();
            TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(131), "ct_l");
            TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(132), "ct_t");
            TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(138), "m_cls");
            TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(139), "m_id");
            TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(147), "resp_txt");
            TEXT_STRING_COLUMN_NAME_MAP.put(Integer.valueOf(152), "tr_id");
            OCTET_COLUMN_INDEX_MAP = new HashMap();
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(186), Integer.valueOf(11));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(134), Integer.valueOf(12));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(140), Integer.valueOf(13));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(141), Integer.valueOf(14));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(143), Integer.valueOf(15));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(144), Integer.valueOf(16));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(155), Integer.valueOf(17));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(145), Integer.valueOf(18));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(153), Integer.valueOf(19));
            OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf(149), Integer.valueOf(20));
            OCTET_COLUMN_NAME_MAP = new HashMap();
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(186), "ct_cls");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(134), "d_rpt");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(140), "m_type");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(141), "v");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(143), "pri");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(144), "rr");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(155), "read_status");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(145), "rpt_a");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(153), "retr_st");
            OCTET_COLUMN_NAME_MAP.put(Integer.valueOf(149), "st");
            LONG_COLUMN_INDEX_MAP = new HashMap();
            LONG_COLUMN_INDEX_MAP.put(Integer.valueOf(133), Integer.valueOf(21));
            LONG_COLUMN_INDEX_MAP.put(Integer.valueOf(135), Integer.valueOf(22));
            LONG_COLUMN_INDEX_MAP.put(Integer.valueOf(136), Integer.valueOf(23));
            LONG_COLUMN_INDEX_MAP.put(Integer.valueOf(142), Integer.valueOf(24));
            LONG_COLUMN_NAME_MAP = new HashMap();
            LONG_COLUMN_NAME_MAP.put(Integer.valueOf(133), "date");
            LONG_COLUMN_NAME_MAP.put(Integer.valueOf(135), "d_tm");
            LONG_COLUMN_NAME_MAP.put(Integer.valueOf(136), "exp");
            LONG_COLUMN_NAME_MAP.put(Integer.valueOf(142), "m_size");
            PDU_CACHE_INSTANCE = PduCache.getInstance();
            return;
        }
    }

    private PduPersister(Context paramContext)
    {
        this.mContext = paramContext;
        this.mContentResolver = paramContext.getContentResolver();
        this.mDrmManagerClient = new DrmManagerClient(paramContext);
    }

    public static String convertUriToPath(Context paramContext, Uri paramUri)
    {
        Object localObject1 = null;
        String str1;
        if (paramUri != null)
        {
            str1 = paramUri.getScheme();
            if ((str1 != null) && (!str1.equals("")) && (!str1.equals("file")))
                break label42;
            localObject1 = paramUri.getPath();
        }
        while (true)
        {
            return localObject1;
            label42: if (str1.equals("http"))
            {
                localObject1 = paramUri.toString();
            }
            else
            {
                if (!str1.equals("content"))
                    break;
                String[] arrayOfString = new String[1];
                arrayOfString[0] = "_data";
                Cursor localCursor = null;
                try
                {
                    localCursor = paramContext.getContentResolver().query(paramUri, arrayOfString, null, null, null);
                    if ((localCursor == null) || (localCursor.getCount() == 0) || (!localCursor.moveToFirst()))
                        throw new IllegalArgumentException("Given Uri could not be found in media store");
                }
                catch (SQLiteException localSQLiteException)
                {
                    throw new IllegalArgumentException("Given Uri is not formatted in a way so that it can be found in media store.");
                }
                finally
                {
                    if (localCursor != null)
                        localCursor.close();
                }
                String str2 = localCursor.getString(localCursor.getColumnIndexOrThrow("_data"));
                localObject1 = str2;
                if (localCursor != null)
                    localCursor.close();
            }
        }
        throw new IllegalArgumentException("Given Uri scheme is not supported");
    }

    private byte[] getByteArrayFromPartColumn(Cursor paramCursor, int paramInt)
    {
        if (!paramCursor.isNull(paramInt));
        for (byte[] arrayOfByte = getBytes(paramCursor.getString(paramInt)); ; arrayOfByte = null)
            return arrayOfByte;
    }

    public static byte[] getBytes(String paramString)
    {
        try
        {
            byte[] arrayOfByte2 = paramString.getBytes("iso-8859-1");
            arrayOfByte1 = arrayOfByte2;
            return arrayOfByte1;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
            while (true)
            {
                Log.e("PduPersister", "ISO_8859_1 must be supported!", localUnsupportedEncodingException);
                byte[] arrayOfByte1 = new byte[0];
            }
        }
    }

    private Integer getIntegerFromPartColumn(Cursor paramCursor, int paramInt)
    {
        if (!paramCursor.isNull(paramInt));
        for (Integer localInteger = Integer.valueOf(paramCursor.getInt(paramInt)); ; localInteger = null)
            return localInteger;
    }

    public static PduPersister getPduPersister(Context paramContext)
    {
        if ((sPersister == null) || (!paramContext.equals(sPersister.mContext)))
            sPersister = new PduPersister(paramContext);
        return sPersister;
    }

    private void loadAddress(long paramLong, PduHeaders paramPduHeaders)
    {
        Context localContext = this.mContext;
        ContentResolver localContentResolver = this.mContentResolver;
        Uri localUri = Uri.parse("content://mms/" + paramLong + "/addr");
        String[] arrayOfString = new String[3];
        arrayOfString[0] = "address";
        arrayOfString[1] = "charset";
        arrayOfString[2] = "type";
        Cursor localCursor = SqliteWrapper.query(localContext, localContentResolver, localUri, arrayOfString, null, null, null);
        if (localCursor != null)
        {
            while (true)
            {
                String str;
                int i;
                try
                {
                    if (!localCursor.moveToNext())
                        break;
                    str = localCursor.getString(0);
                    if (TextUtils.isEmpty(str))
                        continue;
                    i = localCursor.getInt(2);
                    switch (i)
                    {
                    default:
                        Log.e("PduPersister", "Unknown address type: " + i);
                        continue;
                    case 137:
                    case 129:
                    case 130:
                    case 151:
                    }
                }
                finally
                {
                    localCursor.close();
                }
                paramPduHeaders.setEncodedStringValue(new EncodedStringValue(localCursor.getInt(1), getBytes(str)), i);
                continue;
                paramPduHeaders.appendEncodedStringValue(new EncodedStringValue(localCursor.getInt(1), getBytes(str)), i);
            }
            localCursor.close();
        }
    }

    private PduPart[] loadParts(long paramLong)
        throws MmsException
    {
        Cursor localCursor = SqliteWrapper.query(this.mContext, this.mContentResolver, Uri.parse("content://mms/" + paramLong + "/part"), PART_PROJECTION, null, null, null);
        if (localCursor != null);
        while (true)
        {
            PduPart[] arrayOfPduPart;
            Uri localUri;
            ByteArrayOutputStream localByteArrayOutputStream;
            InputStream localInputStream;
            try
            {
                int i = localCursor.getCount();
                if (i == 0)
                {
                    localObject1 = null;
                    return localObject1;
                }
                arrayOfPduPart = new PduPart[localCursor.getCount()];
                int j = 0;
                if (!localCursor.moveToNext())
                    break label626;
                PduPart localPduPart = new PduPart();
                Integer localInteger = getIntegerFromPartColumn(localCursor, 1);
                if (localInteger != null)
                    localPduPart.setCharset(localInteger.intValue());
                byte[] arrayOfByte1 = getByteArrayFromPartColumn(localCursor, 2);
                if (arrayOfByte1 != null)
                    localPduPart.setContentDisposition(arrayOfByte1);
                byte[] arrayOfByte2 = getByteArrayFromPartColumn(localCursor, 3);
                if (arrayOfByte2 != null)
                    localPduPart.setContentId(arrayOfByte2);
                byte[] arrayOfByte3 = getByteArrayFromPartColumn(localCursor, 4);
                if (arrayOfByte3 != null)
                    localPduPart.setContentLocation(arrayOfByte3);
                byte[] arrayOfByte4 = getByteArrayFromPartColumn(localCursor, 5);
                if (arrayOfByte4 != null)
                {
                    localPduPart.setContentType(arrayOfByte4);
                    byte[] arrayOfByte5 = getByteArrayFromPartColumn(localCursor, 6);
                    if (arrayOfByte5 != null)
                        localPduPart.setFilename(arrayOfByte5);
                    byte[] arrayOfByte6 = getByteArrayFromPartColumn(localCursor, 7);
                    if (arrayOfByte6 != null)
                        localPduPart.setName(arrayOfByte6);
                    long l = localCursor.getLong(0);
                    localUri = Uri.parse("content://mms/part/" + l);
                    localPduPart.setDataUri(localUri);
                    String str1 = toIsoString(arrayOfByte4);
                    if ((!ContentType.isImageType(str1)) && (!ContentType.isAudioType(str1)) && (!ContentType.isVideoType(str1)))
                    {
                        localByteArrayOutputStream = new ByteArrayOutputStream();
                        localInputStream = null;
                        if ((!"text/plain".equals(str1)) && (!"application/smil".equals(str1)) && (!"text/html".equals(str1)))
                            break label479;
                        str2 = localCursor.getString(8);
                        if (str2 == null)
                            break label471;
                        byte[] arrayOfByte7 = new EncodedStringValue(str2).getTextString();
                        localByteArrayOutputStream.write(arrayOfByte7, 0, arrayOfByte7.length);
                        localPduPart.setData(localByteArrayOutputStream.toByteArray());
                    }
                    int k = j + 1;
                    arrayOfPduPart[j] = localPduPart;
                    j = k;
                    continue;
                }
                throw new MmsException("Content-Type must be set.");
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            label471: String str2 = "";
            continue;
            try
            {
                label479: localInputStream = this.mContentResolver.openInputStream(localUri);
                byte[] arrayOfByte8 = new byte[256];
                int n;
                for (int m = localInputStream.read(arrayOfByte8); m >= 0; m = n)
                {
                    localByteArrayOutputStream.write(arrayOfByte8, 0, m);
                    n = localInputStream.read(arrayOfByte8);
                }
                if (localInputStream == null)
                    continue;
                try
                {
                    localInputStream.close();
                }
                catch (IOException localIOException3)
                {
                    Log.e("PduPersister", "Failed to close stream", localIOException3);
                }
                continue;
            }
            catch (IOException localIOException2)
            {
                Log.e("PduPersister", "Failed to load part data", localIOException2);
                localCursor.close();
                throw new MmsException(localIOException2);
            }
            finally
            {
                if (localInputStream == null);
            }
            try
            {
                localInputStream.close();
                throw localObject3;
            }
            catch (IOException localIOException1)
            {
                while (true)
                    Log.e("PduPersister", "Failed to close stream", localIOException1);
            }
            label626: if (localCursor != null)
                localCursor.close();
            Object localObject1 = arrayOfPduPart;
        }
    }

    private void persistAddress(long paramLong, int paramInt, EncodedStringValue[] paramArrayOfEncodedStringValue)
    {
        ContentValues localContentValues = new ContentValues(3);
        int i = paramArrayOfEncodedStringValue.length;
        for (int j = 0; j < i; j++)
        {
            EncodedStringValue localEncodedStringValue = paramArrayOfEncodedStringValue[j];
            localContentValues.clear();
            localContentValues.put("address", toIsoString(localEncodedStringValue.getTextString()));
            localContentValues.put("charset", Integer.valueOf(localEncodedStringValue.getCharacterSet()));
            localContentValues.put("type", Integer.valueOf(paramInt));
            Uri localUri = Uri.parse("content://mms/" + paramLong + "/addr");
            SqliteWrapper.insert(this.mContext, this.mContentResolver, localUri, localContentValues);
        }
    }

    // ERROR //
    private void persistData(PduPart paramPduPart, Uri paramUri, String paramString)
        throws MmsException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: aconst_null
        //     4: astore 5
        //     6: aconst_null
        //     7: astore 6
        //     9: aconst_null
        //     10: astore 7
        //     12: aload_1
        //     13: invokevirtual 590	com/google/android/mms/pdu/PduPart:getData	()[B
        //     16: astore 21
        //     18: ldc_w 519
        //     21: aload_3
        //     22: invokevirtual 298	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     25: ifne +23 -> 48
        //     28: ldc_w 521
        //     31: aload_3
        //     32: invokevirtual 298	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     35: ifne +13 -> 48
        //     38: ldc_w 523
        //     41: aload_3
        //     42: invokevirtual 298	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     45: ifeq +203 -> 248
        //     48: new 563	android/content/ContentValues
        //     51: dup
        //     52: invokespecial 591	android/content/ContentValues:<init>	()V
        //     55: astore 22
        //     57: aload 22
        //     59: ldc 202
        //     61: new 435	com/google/android/mms/pdu/EncodedStringValue
        //     64: dup
        //     65: aload 21
        //     67: invokespecial 593	com/google/android/mms/pdu/EncodedStringValue:<init>	([B)V
        //     70: invokevirtual 595	com/google/android/mms/pdu/EncodedStringValue:getString	()Ljava/lang/String;
        //     73: invokevirtual 571	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
        //     76: aload_0
        //     77: getfield 276	com/google/android/mms/pdu/PduPersister:mContentResolver	Landroid/content/ContentResolver;
        //     80: aload_2
        //     81: aload 22
        //     83: aconst_null
        //     84: aconst_null
        //     85: invokevirtual 599	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
        //     88: iconst_1
        //     89: if_icmpeq +590 -> 679
        //     92: new 451	com/google/android/mms/MmsException
        //     95: dup
        //     96: new 388	java/lang/StringBuilder
        //     99: dup
        //     100: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     103: ldc_w 601
        //     106: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     109: aload_2
        //     110: invokevirtual 308	android/net/Uri:toString	()Ljava/lang/String;
        //     113: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     116: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     119: invokespecial 541	com/google/android/mms/MmsException:<init>	(Ljava/lang/String;)V
        //     122: athrow
        //     123: astore 19
        //     125: ldc 103
        //     127: ldc_w 603
        //     130: aload 19
        //     132: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     135: pop
        //     136: new 451	com/google/android/mms/MmsException
        //     139: dup
        //     140: aload 19
        //     142: invokespecial 559	com/google/android/mms/MmsException:<init>	(Ljava/lang/Throwable;)V
        //     145: athrow
        //     146: astore 10
        //     148: aload 4
        //     150: ifnull +8 -> 158
        //     153: aload 4
        //     155: invokevirtual 606	java/io/OutputStream:close	()V
        //     158: aload 5
        //     160: ifnull +8 -> 168
        //     163: aload 5
        //     165: invokevirtual 552	java/io/InputStream:close	()V
        //     168: aload 6
        //     170: ifnull +75 -> 245
        //     173: aload 6
        //     175: aload 7
        //     177: invokevirtual 610	com/google/android/mms/util/DrmConvertSession:close	(Ljava/lang/String;)I
        //     180: pop
        //     181: new 612	java/io/File
        //     184: dup
        //     185: aload 7
        //     187: invokespecial 613	java/io/File:<init>	(Ljava/lang/String;)V
        //     190: astore 12
        //     192: new 563	android/content/ContentValues
        //     195: dup
        //     196: iconst_0
        //     197: invokespecial 565	android/content/ContentValues:<init>	(I)V
        //     200: astore 13
        //     202: aload_0
        //     203: getfield 268	com/google/android/mms/pdu/PduPersister:mContext	Landroid/content/Context;
        //     206: aload_0
        //     207: getfield 276	com/google/android/mms/pdu/PduPersister:mContentResolver	Landroid/content/ContentResolver;
        //     210: new 388	java/lang/StringBuilder
        //     213: dup
        //     214: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     217: ldc_w 615
        //     220: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     223: aload 12
        //     225: invokevirtual 618	java/io/File:getName	()Ljava/lang/String;
        //     228: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     231: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     234: invokestatic 405	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
        //     237: aload 13
        //     239: aconst_null
        //     240: aconst_null
        //     241: invokestatic 621	com/google/android/mms/util/SqliteWrapper:update	(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
        //     244: pop
        //     245: aload 10
        //     247: athrow
        //     248: aload_3
        //     249: invokestatic 626	com/google/android/mms/util/DownloadDrmHelper:isDrmConvertNeeded	(Ljava/lang/String;)Z
        //     252: istore 34
        //     254: iload 34
        //     256: ifeq +179 -> 435
        //     259: aload_2
        //     260: ifnull +103 -> 363
        //     263: aload_0
        //     264: getfield 268	com/google/android/mms/pdu/PduPersister:mContext	Landroid/content/Context;
        //     267: aload_2
        //     268: invokestatic 628	com/google/android/mms/pdu/PduPersister:convertUriToPath	(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
        //     271: astore 7
        //     273: new 612	java/io/File
        //     276: dup
        //     277: aload 7
        //     279: invokespecial 613	java/io/File:<init>	(Ljava/lang/String;)V
        //     282: invokevirtual 632	java/io/File:length	()J
        //     285: lstore 51
        //     287: lload 51
        //     289: lconst_0
        //     290: lcmp
        //     291: ifle +72 -> 363
        //     294: iconst_0
        //     295: ifeq +5 -> 300
        //     298: aconst_null
        //     299: athrow
        //     300: iconst_0
        //     301: ifeq +5 -> 306
        //     304: aconst_null
        //     305: athrow
        //     306: iconst_0
        //     307: ifeq +22 -> 329
        //     310: aload 7
        //     312: pop
        //     313: aconst_null
        //     314: athrow
        //     315: aload 26
        //     317: aload 27
        //     319: aload 28
        //     321: aload 25
        //     323: aconst_null
        //     324: aconst_null
        //     325: invokestatic 621	com/google/android/mms/util/SqliteWrapper:update	(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
        //     328: pop
        //     329: return
        //     330: astore 49
        //     332: ldc 103
        //     334: new 388	java/lang/StringBuilder
        //     337: dup
        //     338: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     341: ldc_w 634
        //     344: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     347: aload_1
        //     348: invokevirtual 638	com/google/android/mms/pdu/PduPart:getDataUri	()Landroid/net/Uri;
        //     351: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     354: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     357: aload 49
        //     359: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     362: pop
        //     363: aload_0
        //     364: getfield 268	com/google/android/mms/pdu/PduPersister:mContext	Landroid/content/Context;
        //     367: aload_3
        //     368: invokestatic 645	com/google/android/mms/util/DrmConvertSession:open	(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/mms/util/DrmConvertSession;
        //     371: astore 6
        //     373: aload 6
        //     375: ifnonnull +60 -> 435
        //     378: new 451	com/google/android/mms/MmsException
        //     381: dup
        //     382: new 388	java/lang/StringBuilder
        //     385: dup
        //     386: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     389: ldc_w 647
        //     392: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     395: aload_3
        //     396: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     399: ldc_w 649
        //     402: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     405: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     408: invokespecial 541	com/google/android/mms/MmsException:<init>	(Ljava/lang/String;)V
        //     411: athrow
        //     412: astore 8
        //     414: ldc 103
        //     416: ldc_w 651
        //     419: aload 8
        //     421: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     424: pop
        //     425: new 451	com/google/android/mms/MmsException
        //     428: dup
        //     429: aload 8
        //     431: invokespecial 559	com/google/android/mms/MmsException:<init>	(Ljava/lang/Throwable;)V
        //     434: athrow
        //     435: aload_0
        //     436: getfield 276	com/google/android/mms/pdu/PduPersister:mContentResolver	Landroid/content/ContentResolver;
        //     439: aload_2
        //     440: invokevirtual 655	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
        //     443: astore 4
        //     445: aload 21
        //     447: ifnonnull +220 -> 667
        //     450: aload_1
        //     451: invokevirtual 638	com/google/android/mms/pdu/PduPart:getDataUri	()Landroid/net/Uri;
        //     454: astore 37
        //     456: aload 37
        //     458: ifnull +9 -> 467
        //     461: aload 37
        //     463: aload_2
        //     464: if_acmpne +106 -> 570
        //     467: ldc 103
        //     469: ldc_w 657
        //     472: invokestatic 660	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     475: pop
        //     476: aload 4
        //     478: ifnull +8 -> 486
        //     481: aload 4
        //     483: invokevirtual 606	java/io/OutputStream:close	()V
        //     486: iconst_0
        //     487: ifeq +5 -> 492
        //     490: aconst_null
        //     491: athrow
        //     492: aload 6
        //     494: ifnull -165 -> 329
        //     497: aload 6
        //     499: aload 7
        //     501: invokevirtual 610	com/google/android/mms/util/DrmConvertSession:close	(Ljava/lang/String;)I
        //     504: pop
        //     505: new 612	java/io/File
        //     508: dup
        //     509: aload 7
        //     511: invokespecial 613	java/io/File:<init>	(Ljava/lang/String;)V
        //     514: astore 40
        //     516: new 563	android/content/ContentValues
        //     519: dup
        //     520: iconst_0
        //     521: invokespecial 565	android/content/ContentValues:<init>	(I)V
        //     524: astore 25
        //     526: aload_0
        //     527: getfield 268	com/google/android/mms/pdu/PduPersister:mContext	Landroid/content/Context;
        //     530: astore 26
        //     532: aload_0
        //     533: getfield 276	com/google/android/mms/pdu/PduPersister:mContentResolver	Landroid/content/ContentResolver;
        //     536: astore 27
        //     538: new 388	java/lang/StringBuilder
        //     541: dup
        //     542: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     545: ldc_w 615
        //     548: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     551: aload 40
        //     553: invokevirtual 618	java/io/File:getName	()Ljava/lang/String;
        //     556: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     559: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     562: invokestatic 405	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
        //     565: astore 28
        //     567: goto -252 -> 315
        //     570: aload_0
        //     571: getfield 276	com/google/android/mms/pdu/PduPersister:mContentResolver	Landroid/content/ContentResolver;
        //     574: aload 37
        //     576: invokevirtual 545	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
        //     579: astore 5
        //     581: sipush 8192
        //     584: newarray byte
        //     586: astore 45
        //     588: aload 5
        //     590: aload 45
        //     592: invokevirtual 551	java/io/InputStream:read	([B)I
        //     595: istore 46
        //     597: iload 46
        //     599: bipush 255
        //     601: if_icmpeq +78 -> 679
        //     604: iload 34
        //     606: ifne +16 -> 622
        //     609: aload 4
        //     611: aload 45
        //     613: iconst_0
        //     614: iload 46
        //     616: invokevirtual 661	java/io/OutputStream:write	([BII)V
        //     619: goto -31 -> 588
        //     622: aload 6
        //     624: aload 45
        //     626: iload 46
        //     628: invokevirtual 665	com/google/android/mms/util/DrmConvertSession:convert	([BI)[B
        //     631: astore 47
        //     633: aload 47
        //     635: ifnull +21 -> 656
        //     638: aload 47
        //     640: arraylength
        //     641: istore 48
        //     643: aload 4
        //     645: aload 47
        //     647: iconst_0
        //     648: iload 48
        //     650: invokevirtual 661	java/io/OutputStream:write	([BII)V
        //     653: goto -65 -> 588
        //     656: new 451	com/google/android/mms/MmsException
        //     659: dup
        //     660: ldc_w 667
        //     663: invokespecial 541	com/google/android/mms/MmsException:<init>	(Ljava/lang/String;)V
        //     666: athrow
        //     667: iload 34
        //     669: ifne +108 -> 777
        //     672: aload 4
        //     674: aload 21
        //     676: invokevirtual 669	java/io/OutputStream:write	([B)V
        //     679: aload 4
        //     681: ifnull +8 -> 689
        //     684: aload 4
        //     686: invokevirtual 606	java/io/OutputStream:close	()V
        //     689: aload 5
        //     691: ifnull +8 -> 699
        //     694: aload 5
        //     696: invokevirtual 552	java/io/InputStream:close	()V
        //     699: aload 6
        //     701: ifnull -372 -> 329
        //     704: aload 6
        //     706: aload 7
        //     708: invokevirtual 610	com/google/android/mms/util/DrmConvertSession:close	(Ljava/lang/String;)I
        //     711: pop
        //     712: new 612	java/io/File
        //     715: dup
        //     716: aload 7
        //     718: invokespecial 613	java/io/File:<init>	(Ljava/lang/String;)V
        //     721: astore 24
        //     723: new 563	android/content/ContentValues
        //     726: dup
        //     727: iconst_0
        //     728: invokespecial 565	android/content/ContentValues:<init>	(I)V
        //     731: astore 25
        //     733: aload_0
        //     734: getfield 268	com/google/android/mms/pdu/PduPersister:mContext	Landroid/content/Context;
        //     737: astore 26
        //     739: aload_0
        //     740: getfield 276	com/google/android/mms/pdu/PduPersister:mContentResolver	Landroid/content/ContentResolver;
        //     743: astore 27
        //     745: new 388	java/lang/StringBuilder
        //     748: dup
        //     749: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     752: ldc_w 615
        //     755: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     758: aload 24
        //     760: invokevirtual 618	java/io/File:getName	()Ljava/lang/String;
        //     763: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     766: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     769: invokestatic 405	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
        //     772: astore 28
        //     774: goto -459 -> 315
        //     777: aload 6
        //     779: aload 21
        //     781: aload 21
        //     783: arraylength
        //     784: invokevirtual 665	com/google/android/mms/util/DrmConvertSession:convert	([BI)[B
        //     787: astore 35
        //     789: aload 35
        //     791: ifnull +21 -> 812
        //     794: aload 35
        //     796: arraylength
        //     797: istore 36
        //     799: aload 4
        //     801: aload 35
        //     803: iconst_0
        //     804: iload 36
        //     806: invokevirtual 661	java/io/OutputStream:write	([BII)V
        //     809: goto -130 -> 679
        //     812: new 451	com/google/android/mms/MmsException
        //     815: dup
        //     816: ldc_w 667
        //     819: invokespecial 541	com/google/android/mms/MmsException:<init>	(Ljava/lang/String;)V
        //     822: athrow
        //     823: astore 17
        //     825: ldc 103
        //     827: new 388	java/lang/StringBuilder
        //     830: dup
        //     831: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     834: ldc_w 671
        //     837: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     840: aload 4
        //     842: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     845: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     848: aload 17
        //     850: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     853: pop
        //     854: goto -696 -> 158
        //     857: astore 15
        //     859: ldc 103
        //     861: new 388	java/lang/StringBuilder
        //     864: dup
        //     865: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     868: ldc_w 671
        //     871: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     874: aload 5
        //     876: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     879: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     882: aload 15
        //     884: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     887: pop
        //     888: goto -720 -> 168
        //     891: astore 32
        //     893: ldc 103
        //     895: new 388	java/lang/StringBuilder
        //     898: dup
        //     899: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     902: ldc_w 671
        //     905: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     908: aload 4
        //     910: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     913: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     916: aload 32
        //     918: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     921: pop
        //     922: goto -233 -> 689
        //     925: astore 30
        //     927: ldc 103
        //     929: new 388	java/lang/StringBuilder
        //     932: dup
        //     933: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     936: ldc_w 671
        //     939: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     942: aload 5
        //     944: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     947: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     950: aload 30
        //     952: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     955: pop
        //     956: goto -257 -> 699
        //     959: astore 43
        //     961: ldc 103
        //     963: new 388	java/lang/StringBuilder
        //     966: dup
        //     967: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     970: ldc_w 671
        //     973: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     976: aload 4
        //     978: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     981: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     984: aload 43
        //     986: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     989: pop
        //     990: goto -504 -> 486
        //     993: astore 41
        //     995: ldc 103
        //     997: new 388	java/lang/StringBuilder
        //     1000: dup
        //     1001: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     1004: ldc_w 671
        //     1007: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1010: aconst_null
        //     1011: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1014: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1017: aload 41
        //     1019: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     1022: pop
        //     1023: goto -531 -> 492
        //     1026: astore 56
        //     1028: ldc 103
        //     1030: new 388	java/lang/StringBuilder
        //     1033: dup
        //     1034: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     1037: ldc_w 671
        //     1040: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1043: aconst_null
        //     1044: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1047: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1050: aload 56
        //     1052: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     1055: pop
        //     1056: goto -756 -> 300
        //     1059: astore 54
        //     1061: ldc 103
        //     1063: new 388	java/lang/StringBuilder
        //     1066: dup
        //     1067: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     1070: ldc_w 671
        //     1073: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1076: aconst_null
        //     1077: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1080: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1083: aload 54
        //     1085: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     1088: pop
        //     1089: goto -783 -> 306
        //
        // Exception table:
        //     from	to	target	type
        //     12	123	123	java/io/FileNotFoundException
        //     248	254	123	java/io/FileNotFoundException
        //     263	287	123	java/io/FileNotFoundException
        //     332	412	123	java/io/FileNotFoundException
        //     435	476	123	java/io/FileNotFoundException
        //     570	679	123	java/io/FileNotFoundException
        //     777	823	123	java/io/FileNotFoundException
        //     12	123	146	finally
        //     125	146	146	finally
        //     248	254	146	finally
        //     263	287	146	finally
        //     332	412	146	finally
        //     414	435	146	finally
        //     435	476	146	finally
        //     570	679	146	finally
        //     777	823	146	finally
        //     263	287	330	java/lang/Exception
        //     12	123	412	java/io/IOException
        //     248	254	412	java/io/IOException
        //     263	287	412	java/io/IOException
        //     332	412	412	java/io/IOException
        //     435	476	412	java/io/IOException
        //     570	679	412	java/io/IOException
        //     777	823	412	java/io/IOException
        //     153	158	823	java/io/IOException
        //     163	168	857	java/io/IOException
        //     684	689	891	java/io/IOException
        //     694	699	925	java/io/IOException
        //     481	486	959	java/io/IOException
        //     490	492	993	java/io/IOException
        //     298	300	1026	java/io/IOException
        //     304	306	1059	java/io/IOException
    }

    private void setEncodedStringValueToHeaders(Cursor paramCursor, int paramInt1, PduHeaders paramPduHeaders, int paramInt2)
    {
        String str = paramCursor.getString(paramInt1);
        if ((str != null) && (str.length() > 0))
            paramPduHeaders.setEncodedStringValue(new EncodedStringValue(paramCursor.getInt(((Integer)CHARSET_COLUMN_INDEX_MAP.get(Integer.valueOf(paramInt2))).intValue()), getBytes(str)), paramInt2);
    }

    private void setLongToHeaders(Cursor paramCursor, int paramInt1, PduHeaders paramPduHeaders, int paramInt2)
    {
        if (!paramCursor.isNull(paramInt1))
            paramPduHeaders.setLongInteger(paramCursor.getLong(paramInt1), paramInt2);
    }

    private void setOctetToHeaders(Cursor paramCursor, int paramInt1, PduHeaders paramPduHeaders, int paramInt2)
        throws InvalidHeaderValueException
    {
        if (!paramCursor.isNull(paramInt1))
            paramPduHeaders.setOctet(paramCursor.getInt(paramInt1), paramInt2);
    }

    private void setTextStringToHeaders(Cursor paramCursor, int paramInt1, PduHeaders paramPduHeaders, int paramInt2)
    {
        String str = paramCursor.getString(paramInt1);
        if (str != null)
            paramPduHeaders.setTextString(getBytes(str), paramInt2);
    }

    public static String toIsoString(byte[] paramArrayOfByte)
    {
        try
        {
            str = new String(paramArrayOfByte, "iso-8859-1");
            return str;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
            while (true)
            {
                Log.e("PduPersister", "ISO_8859_1 must be supported!", localUnsupportedEncodingException);
                String str = "";
            }
        }
    }

    private void updateAddress(long paramLong, int paramInt, EncodedStringValue[] paramArrayOfEncodedStringValue)
    {
        SqliteWrapper.delete(this.mContext, this.mContentResolver, Uri.parse("content://mms/" + paramLong + "/addr"), "type=" + paramInt, null);
        persistAddress(paramLong, paramInt, paramArrayOfEncodedStringValue);
    }

    private void updatePart(Uri paramUri, PduPart paramPduPart)
        throws MmsException
    {
        ContentValues localContentValues = new ContentValues(7);
        int i = paramPduPart.getCharset();
        if (i != 0)
            localContentValues.put("chset", Integer.valueOf(i));
        if (paramPduPart.getContentType() != null)
        {
            String str = toIsoString(paramPduPart.getContentType());
            localContentValues.put("ct", str);
            if (paramPduPart.getFilename() != null)
                localContentValues.put("fn", new String(paramPduPart.getFilename()));
            if (paramPduPart.getName() != null)
                localContentValues.put("name", new String(paramPduPart.getName()));
            if (paramPduPart.getContentDisposition() != null)
                localContentValues.put("cd", (String)toIsoString(paramPduPart.getContentDisposition()));
            if (paramPduPart.getContentId() != null)
                localContentValues.put("cid", (String)toIsoString(paramPduPart.getContentId()));
            if (paramPduPart.getContentLocation() != null)
                localContentValues.put("cl", (String)toIsoString(paramPduPart.getContentLocation()));
            SqliteWrapper.update(this.mContext, this.mContentResolver, paramUri, localContentValues, null, null);
            if ((paramPduPart.getData() != null) || (paramUri != paramPduPart.getDataUri()))
                persistData(paramPduPart, paramUri, str);
            return;
        }
        throw new MmsException("MIME type of the part must be set.");
    }

    public Cursor getPendingMessages(long paramLong)
    {
        Uri.Builder localBuilder = Telephony.MmsSms.PendingMessages.CONTENT_URI.buildUpon();
        localBuilder.appendQueryParameter("protocol", "mms");
        String[] arrayOfString = new String[2];
        arrayOfString[0] = String.valueOf(10);
        arrayOfString[1] = String.valueOf(paramLong);
        return SqliteWrapper.query(this.mContext, this.mContentResolver, localBuilder.build(), null, "err_type < ? AND due_time <= ?", arrayOfString, "due_time");
    }

    // ERROR //
    public GenericPdu load(Uri paramUri)
        throws MmsException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: iconst_0
        //     3: istore_3
        //     4: ldc2_w 771
        //     7: lstore 4
        //     9: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     12: astore 11
        //     14: aload 11
        //     16: monitorenter
        //     17: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     20: aload_1
        //     21: invokevirtual 776	com/google/android/mms/util/PduCache:isUpdating	(Landroid/net/Uri;)Z
        //     24: istore 13
        //     26: iload 13
        //     28: ifeq +141 -> 169
        //     31: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     34: invokevirtual 779	java/lang/Object:wait	()V
        //     37: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     40: aload_1
        //     41: invokevirtual 780	com/google/android/mms/util/PduCache:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     44: checkcast 782	com/google/android/mms/util/PduCacheEntry
        //     47: astore_2
        //     48: aload_2
        //     49: ifnull +120 -> 169
        //     52: aload_2
        //     53: invokevirtual 786	com/google/android/mms/util/PduCacheEntry:getPdu	()Lcom/google/android/mms/pdu/GenericPdu;
        //     56: astore 31
        //     58: aload 11
        //     60: monitorexit
        //     61: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     64: astore 50
        //     66: aload 50
        //     68: monitorenter
        //     69: iconst_0
        //     70: ifeq +1018 -> 1088
        //     73: getstatic 126	com/google/android/mms/pdu/PduPersister:$assertionsDisabled	Z
        //     76: ifne +989 -> 1065
        //     79: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     82: aload_1
        //     83: invokevirtual 780	com/google/android/mms/util/PduCache:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     86: ifnull +979 -> 1065
        //     89: new 788	java/lang/AssertionError
        //     92: dup
        //     93: invokespecial 789	java/lang/AssertionError:<init>	()V
        //     96: athrow
        //     97: aload 50
        //     99: monitorexit
        //     100: aload 51
        //     102: athrow
        //     103: astore 48
        //     105: ldc 103
        //     107: ldc_w 791
        //     110: aload 48
        //     112: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     115: pop
        //     116: goto -79 -> 37
        //     119: aload 11
        //     121: monitorexit
        //     122: aload 12
        //     124: athrow
        //     125: astore 6
        //     127: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     130: astore 7
        //     132: aload 7
        //     134: monitorenter
        //     135: iconst_0
        //     136: ifeq +861 -> 997
        //     139: getstatic 126	com/google/android/mms/pdu/PduPersister:$assertionsDisabled	Z
        //     142: ifne +832 -> 974
        //     145: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     148: aload_1
        //     149: invokevirtual 780	com/google/android/mms/util/PduCache:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     152: ifnull +822 -> 974
        //     155: new 788	java/lang/AssertionError
        //     158: dup
        //     159: invokespecial 789	java/lang/AssertionError:<init>	()V
        //     162: athrow
        //     163: aload 7
        //     165: monitorexit
        //     166: aload 8
        //     168: athrow
        //     169: aload_2
        //     170: pop
        //     171: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     174: aload_1
        //     175: iconst_1
        //     176: invokevirtual 795	com/google/android/mms/util/PduCache:setUpdating	(Landroid/net/Uri;Z)V
        //     179: aload 11
        //     181: monitorexit
        //     182: aload_0
        //     183: getfield 268	com/google/android/mms/pdu/PduPersister:mContext	Landroid/content/Context;
        //     186: aload_0
        //     187: getfield 276	com/google/android/mms/pdu/PduPersister:mContentResolver	Landroid/content/ContentResolver;
        //     190: aload_1
        //     191: getstatic 186	com/google/android/mms/pdu/PduPersister:PDU_PROJECTION	[Ljava/lang/String;
        //     194: aconst_null
        //     195: aconst_null
        //     196: aconst_null
        //     197: invokestatic 416	com/google/android/mms/util/SqliteWrapper:query	(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //     200: astore 15
        //     202: new 440	com/google/android/mms/pdu/PduHeaders
        //     205: dup
        //     206: invokespecial 796	com/google/android/mms/pdu/PduHeaders:<init>	()V
        //     209: astore 16
        //     211: aload_1
        //     212: invokestatic 802	android/content/ContentUris:parseId	(Landroid/net/Uri;)J
        //     215: lstore 17
        //     217: aload 15
        //     219: ifnull +24 -> 243
        //     222: aload 15
        //     224: invokeinterface 324 1 0
        //     229: iconst_1
        //     230: if_icmpne +13 -> 243
        //     233: aload 15
        //     235: invokeinterface 327 1 0
        //     240: ifne +53 -> 293
        //     243: new 451	com/google/android/mms/MmsException
        //     246: dup
        //     247: new 388	java/lang/StringBuilder
        //     250: dup
        //     251: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     254: ldc_w 804
        //     257: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     260: aload_1
        //     261: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     264: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     267: invokespecial 541	com/google/android/mms/MmsException:<init>	(Ljava/lang/String;)V
        //     270: athrow
        //     271: astore 19
        //     273: aload 15
        //     275: ifnull +10 -> 285
        //     278: aload 15
        //     280: invokeinterface 339 1 0
        //     285: aload 19
        //     287: athrow
        //     288: astore 6
        //     290: goto -163 -> 127
        //     293: aload 15
        //     295: iconst_1
        //     296: invokeinterface 378 2 0
        //     301: istore_3
        //     302: aload 15
        //     304: iconst_2
        //     305: invokeinterface 492 2 0
        //     310: lstore 4
        //     312: getstatic 242	com/google/android/mms/pdu/PduPersister:ENCODED_STRING_COLUMN_INDEX_MAP	Ljava/util/HashMap;
        //     315: invokevirtual 808	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     318: invokeinterface 814 1 0
        //     323: astore 20
        //     325: aload 20
        //     327: invokeinterface 819 1 0
        //     332: ifeq +52 -> 384
        //     335: aload 20
        //     337: invokeinterface 823 1 0
        //     342: checkcast 825	java/util/Map$Entry
        //     345: astore 47
        //     347: aload_0
        //     348: aload 15
        //     350: aload 47
        //     352: invokeinterface 828 1 0
        //     357: checkcast 219	java/lang/Integer
        //     360: invokevirtual 463	java/lang/Integer:intValue	()I
        //     363: aload 16
        //     365: aload 47
        //     367: invokeinterface 831 1 0
        //     372: checkcast 219	java/lang/Integer
        //     375: invokevirtual 463	java/lang/Integer:intValue	()I
        //     378: invokespecial 833	com/google/android/mms/pdu/PduPersister:setEncodedStringValueToHeaders	(Landroid/database/Cursor;ILcom/google/android/mms/pdu/PduHeaders;I)V
        //     381: goto -56 -> 325
        //     384: getstatic 246	com/google/android/mms/pdu/PduPersister:TEXT_STRING_COLUMN_INDEX_MAP	Ljava/util/HashMap;
        //     387: invokevirtual 808	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     390: invokeinterface 814 1 0
        //     395: astore 21
        //     397: aload 21
        //     399: invokeinterface 819 1 0
        //     404: ifeq +52 -> 456
        //     407: aload 21
        //     409: invokeinterface 823 1 0
        //     414: checkcast 825	java/util/Map$Entry
        //     417: astore 46
        //     419: aload_0
        //     420: aload 15
        //     422: aload 46
        //     424: invokeinterface 828 1 0
        //     429: checkcast 219	java/lang/Integer
        //     432: invokevirtual 463	java/lang/Integer:intValue	()I
        //     435: aload 16
        //     437: aload 46
        //     439: invokeinterface 831 1 0
        //     444: checkcast 219	java/lang/Integer
        //     447: invokevirtual 463	java/lang/Integer:intValue	()I
        //     450: invokespecial 835	com/google/android/mms/pdu/PduPersister:setTextStringToHeaders	(Landroid/database/Cursor;ILcom/google/android/mms/pdu/PduHeaders;I)V
        //     453: goto -56 -> 397
        //     456: getstatic 250	com/google/android/mms/pdu/PduPersister:OCTET_COLUMN_INDEX_MAP	Ljava/util/HashMap;
        //     459: invokevirtual 808	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     462: invokeinterface 814 1 0
        //     467: astore 22
        //     469: aload 22
        //     471: invokeinterface 819 1 0
        //     476: ifeq +52 -> 528
        //     479: aload 22
        //     481: invokeinterface 823 1 0
        //     486: checkcast 825	java/util/Map$Entry
        //     489: astore 45
        //     491: aload_0
        //     492: aload 15
        //     494: aload 45
        //     496: invokeinterface 828 1 0
        //     501: checkcast 219	java/lang/Integer
        //     504: invokevirtual 463	java/lang/Integer:intValue	()I
        //     507: aload 16
        //     509: aload 45
        //     511: invokeinterface 831 1 0
        //     516: checkcast 219	java/lang/Integer
        //     519: invokevirtual 463	java/lang/Integer:intValue	()I
        //     522: invokespecial 837	com/google/android/mms/pdu/PduPersister:setOctetToHeaders	(Landroid/database/Cursor;ILcom/google/android/mms/pdu/PduHeaders;I)V
        //     525: goto -56 -> 469
        //     528: getstatic 254	com/google/android/mms/pdu/PduPersister:LONG_COLUMN_INDEX_MAP	Ljava/util/HashMap;
        //     531: invokevirtual 808	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     534: invokeinterface 814 1 0
        //     539: astore 23
        //     541: aload 23
        //     543: invokeinterface 819 1 0
        //     548: ifeq +52 -> 600
        //     551: aload 23
        //     553: invokeinterface 823 1 0
        //     558: checkcast 825	java/util/Map$Entry
        //     561: astore 44
        //     563: aload_0
        //     564: aload 15
        //     566: aload 44
        //     568: invokeinterface 828 1 0
        //     573: checkcast 219	java/lang/Integer
        //     576: invokevirtual 463	java/lang/Integer:intValue	()I
        //     579: aload 16
        //     581: aload 44
        //     583: invokeinterface 831 1 0
        //     588: checkcast 219	java/lang/Integer
        //     591: invokevirtual 463	java/lang/Integer:intValue	()I
        //     594: invokespecial 839	com/google/android/mms/pdu/PduPersister:setLongToHeaders	(Landroid/database/Cursor;ILcom/google/android/mms/pdu/PduHeaders;I)V
        //     597: goto -56 -> 541
        //     600: aload 15
        //     602: ifnull +10 -> 612
        //     605: aload 15
        //     607: invokeinterface 339 1 0
        //     612: lload 17
        //     614: ldc2_w 771
        //     617: lcmp
        //     618: ifne +14 -> 632
        //     621: new 451	com/google/android/mms/MmsException
        //     624: dup
        //     625: ldc_w 841
        //     628: invokespecial 541	com/google/android/mms/MmsException:<init>	(Ljava/lang/String;)V
        //     631: athrow
        //     632: aload_0
        //     633: lload 17
        //     635: aload 16
        //     637: invokespecial 843	com/google/android/mms/pdu/PduPersister:loadAddress	(JLcom/google/android/mms/pdu/PduHeaders;)V
        //     640: aload 16
        //     642: sipush 140
        //     645: invokevirtual 846	com/google/android/mms/pdu/PduHeaders:getOctet	(I)I
        //     648: istore 24
        //     650: new 848	com/google/android/mms/pdu/PduBody
        //     653: dup
        //     654: invokespecial 849	com/google/android/mms/pdu/PduBody:<init>	()V
        //     657: astore 25
        //     659: iload 24
        //     661: sipush 132
        //     664: if_icmpeq +11 -> 675
        //     667: iload 24
        //     669: sipush 128
        //     672: if_icmpne +474 -> 1146
        //     675: aload_0
        //     676: lload 17
        //     678: invokespecial 851	com/google/android/mms/pdu/PduPersister:loadParts	(J)[Lcom/google/android/mms/pdu/PduPart;
        //     681: astore 26
        //     683: aload 26
        //     685: ifnull +461 -> 1146
        //     688: aload 26
        //     690: arraylength
        //     691: istore 41
        //     693: iconst_0
        //     694: istore 42
        //     696: iload 42
        //     698: iload 41
        //     700: if_icmpge +446 -> 1146
        //     703: aload 25
        //     705: aload 26
        //     707: iload 42
        //     709: aaload
        //     710: invokevirtual 855	com/google/android/mms/pdu/PduBody:addPart	(Lcom/google/android/mms/pdu/PduPart;)Z
        //     713: pop
        //     714: iinc 42 1
        //     717: goto -21 -> 696
        //     720: new 451	com/google/android/mms/MmsException
        //     723: dup
        //     724: new 388	java/lang/StringBuilder
        //     727: dup
        //     728: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     731: ldc_w 857
        //     734: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     737: iload 24
        //     739: invokestatic 860	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     742: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     745: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     748: invokespecial 541	com/google/android/mms/MmsException:<init>	(Ljava/lang/String;)V
        //     751: athrow
        //     752: new 862	com/google/android/mms/pdu/NotificationInd
        //     755: dup
        //     756: aload 16
        //     758: invokespecial 865	com/google/android/mms/pdu/NotificationInd:<init>	(Lcom/google/android/mms/pdu/PduHeaders;)V
        //     761: astore 40
        //     763: aload 40
        //     765: astore 28
        //     767: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     770: astore 29
        //     772: aload 29
        //     774: monitorenter
        //     775: aload 28
        //     777: ifnull +351 -> 1128
        //     780: getstatic 126	com/google/android/mms/pdu/PduPersister:$assertionsDisabled	Z
        //     783: ifne +234 -> 1017
        //     786: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     789: aload_1
        //     790: invokevirtual 780	com/google/android/mms/util/PduCache:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     793: ifnull +224 -> 1017
        //     796: new 788	java/lang/AssertionError
        //     799: dup
        //     800: invokespecial 789	java/lang/AssertionError:<init>	()V
        //     803: athrow
        //     804: astore 30
        //     806: aload 29
        //     808: monitorexit
        //     809: aload 30
        //     811: athrow
        //     812: new 867	com/google/android/mms/pdu/DeliveryInd
        //     815: dup
        //     816: aload 16
        //     818: invokespecial 868	com/google/android/mms/pdu/DeliveryInd:<init>	(Lcom/google/android/mms/pdu/PduHeaders;)V
        //     821: astore 39
        //     823: aload 39
        //     825: astore 28
        //     827: goto -60 -> 767
        //     830: new 870	com/google/android/mms/pdu/ReadOrigInd
        //     833: dup
        //     834: aload 16
        //     836: invokespecial 871	com/google/android/mms/pdu/ReadOrigInd:<init>	(Lcom/google/android/mms/pdu/PduHeaders;)V
        //     839: astore 38
        //     841: aload 38
        //     843: astore 28
        //     845: goto -78 -> 767
        //     848: new 873	com/google/android/mms/pdu/RetrieveConf
        //     851: dup
        //     852: aload 16
        //     854: aload 25
        //     856: invokespecial 876	com/google/android/mms/pdu/RetrieveConf:<init>	(Lcom/google/android/mms/pdu/PduHeaders;Lcom/google/android/mms/pdu/PduBody;)V
        //     859: astore 37
        //     861: aload 37
        //     863: astore 28
        //     865: goto -98 -> 767
        //     868: new 878	com/google/android/mms/pdu/SendReq
        //     871: dup
        //     872: aload 16
        //     874: aload 25
        //     876: invokespecial 879	com/google/android/mms/pdu/SendReq:<init>	(Lcom/google/android/mms/pdu/PduHeaders;Lcom/google/android/mms/pdu/PduBody;)V
        //     879: astore 36
        //     881: aload 36
        //     883: astore 28
        //     885: goto -118 -> 767
        //     888: new 881	com/google/android/mms/pdu/AcknowledgeInd
        //     891: dup
        //     892: aload 16
        //     894: invokespecial 882	com/google/android/mms/pdu/AcknowledgeInd:<init>	(Lcom/google/android/mms/pdu/PduHeaders;)V
        //     897: astore 35
        //     899: aload 35
        //     901: astore 28
        //     903: goto -136 -> 767
        //     906: new 884	com/google/android/mms/pdu/NotifyRespInd
        //     909: dup
        //     910: aload 16
        //     912: invokespecial 885	com/google/android/mms/pdu/NotifyRespInd:<init>	(Lcom/google/android/mms/pdu/PduHeaders;)V
        //     915: astore 34
        //     917: aload 34
        //     919: astore 28
        //     921: goto -154 -> 767
        //     924: new 887	com/google/android/mms/pdu/ReadRecInd
        //     927: dup
        //     928: aload 16
        //     930: invokespecial 888	com/google/android/mms/pdu/ReadRecInd:<init>	(Lcom/google/android/mms/pdu/PduHeaders;)V
        //     933: astore 27
        //     935: aload 27
        //     937: astore 28
        //     939: goto -172 -> 767
        //     942: new 451	com/google/android/mms/MmsException
        //     945: dup
        //     946: new 388	java/lang/StringBuilder
        //     949: dup
        //     950: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     953: ldc_w 890
        //     956: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     959: iload 24
        //     961: invokestatic 860	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     964: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     967: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     970: invokespecial 541	com/google/android/mms/MmsException:<init>	(Ljava/lang/String;)V
        //     973: athrow
        //     974: new 782	com/google/android/mms/util/PduCacheEntry
        //     977: dup
        //     978: aconst_null
        //     979: iload_3
        //     980: lload 4
        //     982: invokespecial 893	com/google/android/mms/util/PduCacheEntry:<init>	(Lcom/google/android/mms/pdu/GenericPdu;IJ)V
        //     985: astore 9
        //     987: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     990: aload_1
        //     991: aload 9
        //     993: invokevirtual 896	com/google/android/mms/util/PduCache:put	(Landroid/net/Uri;Lcom/google/android/mms/util/PduCacheEntry;)Z
        //     996: pop
        //     997: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     1000: aload_1
        //     1001: iconst_0
        //     1002: invokevirtual 795	com/google/android/mms/util/PduCache:setUpdating	(Landroid/net/Uri;Z)V
        //     1005: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     1008: invokevirtual 899	java/lang/Object:notifyAll	()V
        //     1011: aload 7
        //     1013: monitorexit
        //     1014: aload 6
        //     1016: athrow
        //     1017: new 782	com/google/android/mms/util/PduCacheEntry
        //     1020: dup
        //     1021: aload 28
        //     1023: iload_3
        //     1024: lload 4
        //     1026: invokespecial 893	com/google/android/mms/util/PduCacheEntry:<init>	(Lcom/google/android/mms/pdu/GenericPdu;IJ)V
        //     1029: astore 32
        //     1031: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     1034: aload_1
        //     1035: aload 32
        //     1037: invokevirtual 896	com/google/android/mms/util/PduCache:put	(Landroid/net/Uri;Lcom/google/android/mms/util/PduCacheEntry;)Z
        //     1040: pop
        //     1041: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     1044: aload_1
        //     1045: iconst_0
        //     1046: invokevirtual 795	com/google/android/mms/util/PduCache:setUpdating	(Landroid/net/Uri;Z)V
        //     1049: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     1052: invokevirtual 899	java/lang/Object:notifyAll	()V
        //     1055: aload 29
        //     1057: monitorexit
        //     1058: aload 28
        //     1060: astore 31
        //     1062: aload 31
        //     1064: areturn
        //     1065: new 782	com/google/android/mms/util/PduCacheEntry
        //     1068: dup
        //     1069: aconst_null
        //     1070: iconst_0
        //     1071: lload 4
        //     1073: invokespecial 893	com/google/android/mms/util/PduCacheEntry:<init>	(Lcom/google/android/mms/pdu/GenericPdu;IJ)V
        //     1076: astore 52
        //     1078: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     1081: aload_1
        //     1082: aload 52
        //     1084: invokevirtual 896	com/google/android/mms/util/PduCache:put	(Landroid/net/Uri;Lcom/google/android/mms/util/PduCacheEntry;)Z
        //     1087: pop
        //     1088: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     1091: aload_1
        //     1092: iconst_0
        //     1093: invokevirtual 795	com/google/android/mms/util/PduCache:setUpdating	(Landroid/net/Uri;Z)V
        //     1096: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     1099: invokevirtual 899	java/lang/Object:notifyAll	()V
        //     1102: aload 50
        //     1104: monitorexit
        //     1105: goto -43 -> 1062
        //     1108: astore 30
        //     1110: goto -304 -> 806
        //     1113: astore 51
        //     1115: goto -1018 -> 97
        //     1118: astore 8
        //     1120: goto -957 -> 163
        //     1123: astore 12
        //     1125: goto -1006 -> 119
        //     1128: goto -87 -> 1041
        //     1131: astore 51
        //     1133: goto -1036 -> 97
        //     1136: astore 12
        //     1138: goto -1019 -> 119
        //     1141: astore 8
        //     1143: goto -980 -> 163
        //     1146: iload 24
        //     1148: tableswitch	default:+-428 -> 720, 128:+-280->868, 129:+-206->942, 130:+-396->752, 131:+-242->906, 132:+-300->848, 133:+-260->888, 134:+-336->812, 135:+-224->924, 136:+-318->830, 137:+-206->942, 138:+-206->942, 139:+-206->942, 140:+-206->942, 141:+-206->942, 142:+-206->942, 143:+-206->942, 144:+-206->942, 145:+-206->942, 146:+-206->942, 147:+-206->942, 148:+-206->942, 149:+-206->942, 150:+-206->942, 151:+-206->942
        //
        // Exception table:
        //     from	to	target	type
        //     31	37	103	java/lang/InterruptedException
        //     9	17	125	finally
        //     122	125	125	finally
        //     222	271	271	finally
        //     293	597	271	finally
        //     182	217	288	finally
        //     278	288	288	finally
        //     605	763	288	finally
        //     812	974	288	finally
        //     780	804	804	finally
        //     1017	1031	804	finally
        //     806	809	1108	finally
        //     1031	1058	1108	finally
        //     1078	1088	1113	finally
        //     987	997	1118	finally
        //     171	182	1123	finally
        //     73	100	1131	finally
        //     1065	1078	1131	finally
        //     1088	1105	1131	finally
        //     17	26	1136	finally
        //     31	37	1136	finally
        //     37	61	1136	finally
        //     105	122	1136	finally
        //     139	166	1141	finally
        //     974	987	1141	finally
        //     997	1014	1141	finally
    }

    public Uri move(Uri paramUri1, Uri paramUri2)
        throws MmsException
    {
        long l = ContentUris.parseId(paramUri1);
        if (l == -1L)
            throw new MmsException("Error! ID of the message: -1.");
        Integer localInteger = (Integer)MESSAGE_BOX_MAP.get(paramUri2);
        if (localInteger == null)
            throw new MmsException("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
        ContentValues localContentValues = new ContentValues(1);
        localContentValues.put("msg_box", localInteger);
        SqliteWrapper.update(this.mContext, this.mContentResolver, paramUri1, localContentValues, null, null);
        return ContentUris.withAppendedId(paramUri2, l);
    }

    public Uri persist(GenericPdu paramGenericPdu, Uri paramUri)
        throws MmsException
    {
        if (paramUri == null)
            throw new MmsException("Uri may not be null.");
        long l1 = -1L;
        label28: Uri localUri;
        try
        {
            long l4 = ContentUris.parseId(paramUri);
            l1 = l4;
            if (l1 != -1L);
            for (int i = 1; (i == 0) && (MESSAGE_BOX_MAP.get(paramUri) == null); i = 0)
                throw new MmsException("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
            PduHeaders localPduHeaders;
            ContentValues localContentValues1;
            synchronized (PDU_CACHE_INSTANCE)
            {
                boolean bool = PDU_CACHE_INSTANCE.isUpdating(paramUri);
                if (bool);
                try
                {
                    PDU_CACHE_INSTANCE.wait();
                    PDU_CACHE_INSTANCE.purge(paramUri);
                    localPduHeaders = paramGenericPdu.getPduHeaders();
                    localContentValues1 = new ContentValues();
                    Iterator localIterator1 = ENCODED_STRING_COLUMN_NAME_MAP.entrySet().iterator();
                    while (localIterator1.hasNext())
                    {
                        Map.Entry localEntry4 = (Map.Entry)localIterator1.next();
                        int i9 = ((Integer)localEntry4.getKey()).intValue();
                        EncodedStringValue localEncodedStringValue3 = localPduHeaders.getEncodedStringValue(i9);
                        if (localEncodedStringValue3 != null)
                        {
                            String str = (String)CHARSET_COLUMN_NAME_MAP.get(Integer.valueOf(i9));
                            localContentValues1.put((String)localEntry4.getValue(), toIsoString(localEncodedStringValue3.getTextString()));
                            localContentValues1.put(str, Integer.valueOf(localEncodedStringValue3.getCharacterSet()));
                        }
                    }
                }
                catch (InterruptedException localInterruptedException)
                {
                    while (true)
                        Log.e("PduPersister", "persist1: ", localInterruptedException);
                }
            }
            Iterator localIterator2 = TEXT_STRING_COLUMN_NAME_MAP.entrySet().iterator();
            while (localIterator2.hasNext())
            {
                Map.Entry localEntry3 = (Map.Entry)localIterator2.next();
                byte[] arrayOfByte = localPduHeaders.getTextString(((Integer)localEntry3.getKey()).intValue());
                if (arrayOfByte != null)
                    localContentValues1.put((String)localEntry3.getValue(), toIsoString(arrayOfByte));
            }
            Iterator localIterator3 = OCTET_COLUMN_NAME_MAP.entrySet().iterator();
            while (localIterator3.hasNext())
            {
                Map.Entry localEntry2 = (Map.Entry)localIterator3.next();
                int i8 = localPduHeaders.getOctet(((Integer)localEntry2.getKey()).intValue());
                if (i8 != 0)
                    localContentValues1.put((String)localEntry2.getValue(), Integer.valueOf(i8));
            }
            Iterator localIterator4 = LONG_COLUMN_NAME_MAP.entrySet().iterator();
            while (localIterator4.hasNext())
            {
                Map.Entry localEntry1 = (Map.Entry)localIterator4.next();
                long l3 = localPduHeaders.getLongInteger(((Integer)localEntry1.getKey()).intValue());
                if (l3 != -1L)
                    localContentValues1.put((String)localEntry1.getValue(), Long.valueOf(l3));
            }
            HashMap localHashMap = new HashMap(ADDRESS_FIELDS.length);
            int[] arrayOfInt1 = ADDRESS_FIELDS;
            int j = arrayOfInt1.length;
            int k = 0;
            if (k < j)
            {
                int i7 = arrayOfInt1[k];
                EncodedStringValue[] arrayOfEncodedStringValue4 = null;
                if (i7 == 137)
                {
                    EncodedStringValue localEncodedStringValue2 = localPduHeaders.getEncodedStringValue(i7);
                    if (localEncodedStringValue2 != null)
                    {
                        arrayOfEncodedStringValue4 = new EncodedStringValue[1];
                        arrayOfEncodedStringValue4[0] = localEncodedStringValue2;
                    }
                }
                while (true)
                {
                    localHashMap.put(Integer.valueOf(i7), arrayOfEncodedStringValue4);
                    k++;
                    break;
                    arrayOfEncodedStringValue4 = localPduHeaders.getEncodedStringValues(i7);
                }
            }
            HashSet localHashSet = new HashSet();
            int m = paramGenericPdu.getMessageType();
            if ((m == 130) || (m == 132) || (m == 128))
            {
                EncodedStringValue[] arrayOfEncodedStringValue1 = null;
                switch (m)
                {
                case 129:
                case 131:
                default:
                case 130:
                case 132:
                case 128:
                }
                while (arrayOfEncodedStringValue1 != null)
                {
                    for (EncodedStringValue localEncodedStringValue1 : arrayOfEncodedStringValue1)
                        if (localEncodedStringValue1 != null)
                            localHashSet.add(localEncodedStringValue1.getString());
                    arrayOfEncodedStringValue1 = (EncodedStringValue[])localHashMap.get(Integer.valueOf(137));
                    continue;
                    arrayOfEncodedStringValue1 = (EncodedStringValue[])localHashMap.get(Integer.valueOf(151));
                }
                if (!localHashSet.isEmpty())
                    localContentValues1.put("thread_id", Long.valueOf(Telephony.Threads.getOrCreateThreadId(this.mContext, localHashSet)));
            }
            long l2 = System.currentTimeMillis();
            if ((paramGenericPdu instanceof MultimediaMessagePdu))
            {
                PduBody localPduBody = ((MultimediaMessagePdu)paramGenericPdu).getBody();
                if (localPduBody != null)
                {
                    int i3 = localPduBody.getPartsNum();
                    for (int i4 = 0; i4 < i3; i4++)
                        persistPart(localPduBody.getPart(i4), l2);
                }
            }
            if (i != 0)
            {
                localUri = paramUri;
                SqliteWrapper.update(this.mContext, this.mContentResolver, localUri, localContentValues1, null, null);
            }
            while (true)
            {
                ContentValues localContentValues2 = new ContentValues(1);
                localContentValues2.put("mid", Long.valueOf(l1));
                SqliteWrapper.update(this.mContext, this.mContentResolver, Uri.parse("content://mms/" + l2 + "/part"), localContentValues2, null, null);
                if (i == 0)
                    localUri = Uri.parse(paramUri + "/" + l1);
                for (int i2 : ADDRESS_FIELDS)
                {
                    EncodedStringValue[] arrayOfEncodedStringValue2 = (EncodedStringValue[])localHashMap.get(Integer.valueOf(i2));
                    if (arrayOfEncodedStringValue2 != null)
                        persistAddress(l1, i2, arrayOfEncodedStringValue2);
                }
                localUri = SqliteWrapper.insert(this.mContext, this.mContentResolver, paramUri, localContentValues1);
                if (localUri == null)
                    throw new MmsException("persist() failed: return null.");
                l1 = ContentUris.parseId(localUri);
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label28;
        }
        return localUri;
    }

    public Uri persistPart(PduPart paramPduPart, long paramLong)
        throws MmsException
    {
        Uri localUri1 = Uri.parse("content://mms/" + paramLong + "/part");
        ContentValues localContentValues = new ContentValues(8);
        int i = paramPduPart.getCharset();
        if (i != 0)
            localContentValues.put("chset", Integer.valueOf(i));
        String str;
        Uri localUri2;
        if (paramPduPart.getContentType() != null)
        {
            str = toIsoString(paramPduPart.getContentType());
            if ("image/jpg".equals(str))
                str = "image/jpeg";
            localContentValues.put("ct", str);
            if ("application/smil".equals(str))
                localContentValues.put("seq", Integer.valueOf(-1));
            if (paramPduPart.getFilename() != null)
                localContentValues.put("fn", new String(paramPduPart.getFilename()));
            if (paramPduPart.getName() != null)
                localContentValues.put("name", new String(paramPduPart.getName()));
            if (paramPduPart.getContentDisposition() != null)
                localContentValues.put("cd", (String)toIsoString(paramPduPart.getContentDisposition()));
            if (paramPduPart.getContentId() != null)
                localContentValues.put("cid", (String)toIsoString(paramPduPart.getContentId()));
            if (paramPduPart.getContentLocation() != null)
                localContentValues.put("cl", (String)toIsoString(paramPduPart.getContentLocation()));
            localUri2 = SqliteWrapper.insert(this.mContext, this.mContentResolver, localUri1, localContentValues);
            if (localUri2 == null)
                throw new MmsException("Failed to persist part, return null.");
        }
        else
        {
            throw new MmsException("MIME type of the part must be set.");
        }
        persistData(paramPduPart, localUri2, str);
        paramPduPart.setDataUri(localUri2);
        return localUri2;
    }

    public void release()
    {
        Uri localUri = Uri.parse("content://mms/9223372036854775807/part");
        SqliteWrapper.delete(this.mContext, this.mContentResolver, localUri, null, null);
    }

    public void updateHeaders(Uri paramUri, SendReq paramSendReq)
    {
        ContentValues localContentValues;
        HashSet localHashSet;
        while (true)
        {
            PduHeaders localPduHeaders;
            int n;
            int i1;
            synchronized (PDU_CACHE_INSTANCE)
            {
                boolean bool = PDU_CACHE_INSTANCE.isUpdating(paramUri);
                if (bool);
                try
                {
                    PDU_CACHE_INSTANCE.wait();
                    PDU_CACHE_INSTANCE.purge(paramUri);
                    localContentValues = new ContentValues(10);
                    byte[] arrayOfByte1 = paramSendReq.getContentType();
                    if (arrayOfByte1 != null)
                        localContentValues.put("ct_t", toIsoString(arrayOfByte1));
                    long l1 = paramSendReq.getDate();
                    if (l1 != -1L)
                        localContentValues.put("date", Long.valueOf(l1));
                    int i = paramSendReq.getDeliveryReport();
                    if (i != 0)
                        localContentValues.put("d_rpt", Integer.valueOf(i));
                    long l2 = paramSendReq.getExpiry();
                    if (l2 != -1L)
                        localContentValues.put("exp", Long.valueOf(l2));
                    byte[] arrayOfByte2 = paramSendReq.getMessageClass();
                    if (arrayOfByte2 != null)
                        localContentValues.put("m_cls", toIsoString(arrayOfByte2));
                    int j = paramSendReq.getPriority();
                    if (j != 0)
                        localContentValues.put("pri", Integer.valueOf(j));
                    int k = paramSendReq.getReadReport();
                    if (k != 0)
                        localContentValues.put("rr", Integer.valueOf(k));
                    byte[] arrayOfByte3 = paramSendReq.getTransactionId();
                    if (arrayOfByte3 != null)
                        localContentValues.put("tr_id", toIsoString(arrayOfByte3));
                    EncodedStringValue localEncodedStringValue1 = paramSendReq.getSubject();
                    if (localEncodedStringValue1 != null)
                    {
                        localContentValues.put("sub", toIsoString(localEncodedStringValue1.getTextString()));
                        localContentValues.put("sub_cs", Integer.valueOf(localEncodedStringValue1.getCharacterSet()));
                        long l3 = paramSendReq.getMessageSize();
                        if (l3 > 0L)
                            localContentValues.put("m_size", Long.valueOf(l3));
                        localPduHeaders = paramSendReq.getPduHeaders();
                        localHashSet = new HashSet();
                        int[] arrayOfInt = ADDRESS_FIELDS;
                        int m = arrayOfInt.length;
                        n = 0;
                        if (n >= m)
                            break;
                        i1 = arrayOfInt[n];
                        arrayOfEncodedStringValue1 = null;
                        if (i1 != 137)
                            break label493;
                        EncodedStringValue localEncodedStringValue3 = localPduHeaders.getEncodedStringValue(i1);
                        if (localEncodedStringValue3 != null)
                        {
                            arrayOfEncodedStringValue1 = new EncodedStringValue[1];
                            arrayOfEncodedStringValue1[0] = localEncodedStringValue3;
                        }
                        if (arrayOfEncodedStringValue1 == null)
                            break label505;
                        updateAddress(ContentUris.parseId(paramUri), i1, arrayOfEncodedStringValue1);
                        if (i1 != 151)
                            break label505;
                        EncodedStringValue[] arrayOfEncodedStringValue2 = arrayOfEncodedStringValue1;
                        int i2 = arrayOfEncodedStringValue2.length;
                        int i3 = 0;
                        if (i3 >= i2)
                            break label505;
                        EncodedStringValue localEncodedStringValue2 = arrayOfEncodedStringValue2[i3];
                        if (localEncodedStringValue2 != null)
                            localHashSet.add(localEncodedStringValue2.getString());
                        i3++;
                        continue;
                    }
                }
                catch (InterruptedException localInterruptedException)
                {
                    Log.e("PduPersister", "updateHeaders: ", localInterruptedException);
                    continue;
                }
            }
            localContentValues.put("sub", "");
            continue;
            label493: EncodedStringValue[] arrayOfEncodedStringValue1 = localPduHeaders.getEncodedStringValues(i1);
            continue;
            label505: n++;
        }
        if (!localHashSet.isEmpty())
            localContentValues.put("thread_id", Long.valueOf(Telephony.Threads.getOrCreateThreadId(this.mContext, localHashSet)));
        SqliteWrapper.update(this.mContext, this.mContentResolver, paramUri, localContentValues, null, null);
    }

    // ERROR //
    public void updateParts(Uri paramUri, PduBody paramPduBody)
        throws MmsException
    {
        // Byte code:
        //     0: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     3: astore 6
        //     5: aload 6
        //     7: monitorenter
        //     8: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     11: aload_1
        //     12: invokevirtual 776	com/google/android/mms/util/PduCache:isUpdating	(Landroid/net/Uri;)Z
        //     15: istore 8
        //     17: iload 8
        //     19: ifeq +38 -> 57
        //     22: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     25: invokevirtual 779	java/lang/Object:wait	()V
        //     28: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     31: aload_1
        //     32: invokevirtual 780	com/google/android/mms/util/PduCache:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     35: checkcast 782	com/google/android/mms/util/PduCacheEntry
        //     38: astore 37
        //     40: aload 37
        //     42: ifnull +15 -> 57
        //     45: aload 37
        //     47: invokevirtual 786	com/google/android/mms/util/PduCacheEntry:getPdu	()Lcom/google/android/mms/pdu/GenericPdu;
        //     50: checkcast 974	com/google/android/mms/pdu/MultimediaMessagePdu
        //     53: aload_2
        //     54: invokevirtual 1046	com/google/android/mms/pdu/MultimediaMessagePdu:setBody	(Lcom/google/android/mms/pdu/PduBody;)V
        //     57: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     60: aload_1
        //     61: iconst_1
        //     62: invokevirtual 795	com/google/android/mms/util/PduCache:setUpdating	(Landroid/net/Uri;Z)V
        //     65: aload 6
        //     67: monitorexit
        //     68: new 1048	java/util/ArrayList
        //     71: dup
        //     72: invokespecial 1049	java/util/ArrayList:<init>	()V
        //     75: astore 9
        //     77: new 206	java/util/HashMap
        //     80: dup
        //     81: invokespecial 209	java/util/HashMap:<init>	()V
        //     84: astore 10
        //     86: aload_2
        //     87: invokevirtual 981	com/google/android/mms/pdu/PduBody:getPartsNum	()I
        //     90: istore 11
        //     92: new 388	java/lang/StringBuilder
        //     95: dup
        //     96: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     99: bipush 40
        //     101: invokevirtual 1052	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     104: astore 12
        //     106: iconst_0
        //     107: istore 13
        //     109: iload 13
        //     111: iload 11
        //     113: if_icmpge +161 -> 274
        //     116: aload_2
        //     117: iload 13
        //     119: invokevirtual 985	com/google/android/mms/pdu/PduBody:getPart	(I)Lcom/google/android/mms/pdu/PduPart;
        //     122: astore 28
        //     124: aload 28
        //     126: invokevirtual 638	com/google/android/mms/pdu/PduPart:getDataUri	()Landroid/net/Uri;
        //     129: astore 29
        //     131: aload 29
        //     133: ifnull +17 -> 150
        //     136: aload 29
        //     138: invokevirtual 1055	android/net/Uri:getAuthority	()Ljava/lang/String;
        //     141: ldc_w 748
        //     144: invokevirtual 1058	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     147: ifne +69 -> 216
        //     150: aload 9
        //     152: aload 28
        //     154: invokevirtual 1059	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     157: pop
        //     158: iinc 13 1
        //     161: goto -52 -> 109
        //     164: astore 35
        //     166: ldc 103
        //     168: ldc_w 1061
        //     171: aload 35
        //     173: invokestatic 372	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     176: pop
        //     177: goto -149 -> 28
        //     180: astore 7
        //     182: aload 6
        //     184: monitorexit
        //     185: aload 7
        //     187: athrow
        //     188: astore_3
        //     189: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     192: astore 4
        //     194: aload 4
        //     196: monitorenter
        //     197: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     200: aload_1
        //     201: iconst_0
        //     202: invokevirtual 795	com/google/android/mms/util/PduCache:setUpdating	(Landroid/net/Uri;Z)V
        //     205: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     208: invokevirtual 899	java/lang/Object:notifyAll	()V
        //     211: aload 4
        //     213: monitorexit
        //     214: aload_3
        //     215: athrow
        //     216: aload 10
        //     218: aload 29
        //     220: aload 28
        //     222: invokevirtual 227	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     225: pop
        //     226: aload 12
        //     228: invokevirtual 1062	java/lang/StringBuilder:length	()I
        //     231: iconst_1
        //     232: if_icmple +12 -> 244
        //     235: aload 12
        //     237: ldc_w 1064
        //     240: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     243: pop
        //     244: aload 12
        //     246: ldc 132
        //     248: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     251: pop
        //     252: aload 12
        //     254: ldc_w 1066
        //     257: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     260: pop
        //     261: aload 12
        //     263: aload 29
        //     265: invokevirtual 1069	android/net/Uri:getLastPathSegment	()Ljava/lang/String;
        //     268: invokestatic 1075	android/database/DatabaseUtils:appendEscapedSQLString	(Ljava/lang/StringBuilder;Ljava/lang/String;)V
        //     271: goto -113 -> 158
        //     274: aload 12
        //     276: bipush 41
        //     278: invokevirtual 1052	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     281: pop
        //     282: aload_1
        //     283: invokestatic 802	android/content/ContentUris:parseId	(Landroid/net/Uri;)J
        //     286: lstore 15
        //     288: aload_0
        //     289: getfield 268	com/google/android/mms/pdu/PduPersister:mContext	Landroid/content/Context;
        //     292: astore 17
        //     294: aload_0
        //     295: getfield 276	com/google/android/mms/pdu/PduPersister:mContentResolver	Landroid/content/ContentResolver;
        //     298: astore 18
        //     300: new 388	java/lang/StringBuilder
        //     303: dup
        //     304: invokespecial 389	java/lang/StringBuilder:<init>	()V
        //     307: getstatic 1078	android/provider/Telephony$Mms:CONTENT_URI	Landroid/net/Uri;
        //     310: invokevirtual 641	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     313: ldc_w 993
        //     316: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     319: lload 15
        //     321: invokevirtual 398	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     324: ldc_w 455
        //     327: invokevirtual 395	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     330: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     333: invokestatic 405	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
        //     336: astore 19
        //     338: aload 12
        //     340: invokevirtual 1062	java/lang/StringBuilder:length	()I
        //     343: iconst_2
        //     344: if_icmple +163 -> 507
        //     347: aload 12
        //     349: invokevirtual 401	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     352: astore 20
        //     354: aload 17
        //     356: aload 18
        //     358: aload 19
        //     360: aload 20
        //     362: aconst_null
        //     363: invokestatic 706	com/google/android/mms/util/SqliteWrapper:delete	(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
        //     366: pop
        //     367: aload 9
        //     369: invokevirtual 1079	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     372: astore 22
        //     374: aload 22
        //     376: invokeinterface 819 1 0
        //     381: ifeq +23 -> 404
        //     384: aload_0
        //     385: aload 22
        //     387: invokeinterface 823 1 0
        //     392: checkcast 457	com/google/android/mms/pdu/PduPart
        //     395: lload 15
        //     397: invokevirtual 989	com/google/android/mms/pdu/PduPersister:persistPart	(Lcom/google/android/mms/pdu/PduPart;J)Landroid/net/Uri;
        //     400: pop
        //     401: goto -27 -> 374
        //     404: aload 10
        //     406: invokevirtual 808	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     409: invokeinterface 814 1 0
        //     414: astore 23
        //     416: aload 23
        //     418: invokeinterface 819 1 0
        //     423: ifeq +42 -> 465
        //     426: aload 23
        //     428: invokeinterface 823 1 0
        //     433: checkcast 825	java/util/Map$Entry
        //     436: astore 26
        //     438: aload_0
        //     439: aload 26
        //     441: invokeinterface 831 1 0
        //     446: checkcast 288	android/net/Uri
        //     449: aload 26
        //     451: invokeinterface 828 1 0
        //     456: checkcast 457	com/google/android/mms/pdu/PduPart
        //     459: invokespecial 1081	com/google/android/mms/pdu/PduPersister:updatePart	(Landroid/net/Uri;Lcom/google/android/mms/pdu/PduPart;)V
        //     462: goto -46 -> 416
        //     465: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     468: astore 24
        //     470: aload 24
        //     472: monitorenter
        //     473: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     476: aload_1
        //     477: iconst_0
        //     478: invokevirtual 795	com/google/android/mms/util/PduCache:setUpdating	(Landroid/net/Uri;Z)V
        //     481: getstatic 264	com/google/android/mms/pdu/PduPersister:PDU_CACHE_INSTANCE	Lcom/google/android/mms/util/PduCache;
        //     484: invokevirtual 899	java/lang/Object:notifyAll	()V
        //     487: aload 24
        //     489: monitorexit
        //     490: return
        //     491: astore 5
        //     493: aload 4
        //     495: monitorexit
        //     496: aload 5
        //     498: athrow
        //     499: astore 25
        //     501: aload 24
        //     503: monitorexit
        //     504: aload 25
        //     506: athrow
        //     507: aconst_null
        //     508: astore 20
        //     510: goto -156 -> 354
        //
        // Exception table:
        //     from	to	target	type
        //     22	28	164	java/lang/InterruptedException
        //     8	17	180	finally
        //     22	28	180	finally
        //     28	68	180	finally
        //     166	185	180	finally
        //     0	8	188	finally
        //     68	158	188	finally
        //     185	188	188	finally
        //     216	462	188	finally
        //     197	214	491	finally
        //     493	496	491	finally
        //     473	490	499	finally
        //     501	504	499	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.PduPersister
 * JD-Core Version:        0.6.2
 */