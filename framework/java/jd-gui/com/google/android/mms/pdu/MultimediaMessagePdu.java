package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class MultimediaMessagePdu extends GenericPdu
{
    private PduBody mMessageBody;

    public MultimediaMessagePdu()
    {
    }

    MultimediaMessagePdu(PduHeaders paramPduHeaders)
    {
        super(paramPduHeaders);
    }

    public MultimediaMessagePdu(PduHeaders paramPduHeaders, PduBody paramPduBody)
    {
        super(paramPduHeaders);
        this.mMessageBody = paramPduBody;
    }

    public void addTo(EncodedStringValue paramEncodedStringValue)
    {
        this.mPduHeaders.appendEncodedStringValue(paramEncodedStringValue, 151);
    }

    public PduBody getBody()
    {
        return this.mMessageBody;
    }

    public long getDate()
    {
        return this.mPduHeaders.getLongInteger(133);
    }

    public int getPriority()
    {
        return this.mPduHeaders.getOctet(143);
    }

    public EncodedStringValue getSubject()
    {
        return this.mPduHeaders.getEncodedStringValue(150);
    }

    public EncodedStringValue[] getTo()
    {
        return this.mPduHeaders.getEncodedStringValues(151);
    }

    public void setBody(PduBody paramPduBody)
    {
        this.mMessageBody = paramPduBody;
    }

    public void setDate(long paramLong)
    {
        this.mPduHeaders.setLongInteger(paramLong, 133);
    }

    public void setPriority(int paramInt)
        throws InvalidHeaderValueException
    {
        this.mPduHeaders.setOctet(paramInt, 143);
    }

    public void setSubject(EncodedStringValue paramEncodedStringValue)
    {
        this.mPduHeaders.setEncodedStringValue(paramEncodedStringValue, 150);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.MultimediaMessagePdu
 * JD-Core Version:        0.6.2
 */