package com.google.android.mms.pdu;

import java.util.Map;
import java.util.Vector;

public class PduBody
{
    private Map<String, PduPart> mPartMapByContentId = null;
    private Map<String, PduPart> mPartMapByContentLocation = null;
    private Map<String, PduPart> mPartMapByFileName = null;
    private Map<String, PduPart> mPartMapByName = null;
    private Vector<PduPart> mParts = null;

    private void putPartToMaps(PduPart paramPduPart)
    {
        byte[] arrayOfByte1 = paramPduPart.getContentId();
        if (arrayOfByte1 != null)
            this.mPartMapByContentId.put(new String(arrayOfByte1), paramPduPart);
        byte[] arrayOfByte2 = paramPduPart.getContentLocation();
        if (arrayOfByte2 != null)
        {
            String str1 = new String(arrayOfByte2);
            this.mPartMapByContentLocation.put(str1, paramPduPart);
        }
        byte[] arrayOfByte3 = paramPduPart.getName();
        if (arrayOfByte3 != null)
        {
            String str2 = new String(arrayOfByte3);
            this.mPartMapByName.put(str2, paramPduPart);
        }
        byte[] arrayOfByte4 = paramPduPart.getFilename();
        if (arrayOfByte4 != null)
        {
            String str3 = new String(arrayOfByte4);
            this.mPartMapByFileName.put(str3, paramPduPart);
        }
    }

    public void addPart(int paramInt, PduPart paramPduPart)
    {
        if (paramPduPart == null)
            throw new NullPointerException();
        putPartToMaps(paramPduPart);
        this.mParts.add(paramInt, paramPduPart);
    }

    public boolean addPart(PduPart paramPduPart)
    {
        if (paramPduPart == null)
            throw new NullPointerException();
        putPartToMaps(paramPduPart);
        return this.mParts.add(paramPduPart);
    }

    public PduPart getPart(int paramInt)
    {
        return (PduPart)this.mParts.get(paramInt);
    }

    public PduPart getPartByContentId(String paramString)
    {
        return (PduPart)this.mPartMapByContentId.get(paramString);
    }

    public PduPart getPartByContentLocation(String paramString)
    {
        return (PduPart)this.mPartMapByContentLocation.get(paramString);
    }

    public PduPart getPartByFileName(String paramString)
    {
        return (PduPart)this.mPartMapByFileName.get(paramString);
    }

    public PduPart getPartByName(String paramString)
    {
        return (PduPart)this.mPartMapByName.get(paramString);
    }

    public int getPartIndex(PduPart paramPduPart)
    {
        return this.mParts.indexOf(paramPduPart);
    }

    public int getPartsNum()
    {
        return this.mParts.size();
    }

    public void removeAll()
    {
        this.mParts.clear();
    }

    public PduPart removePart(int paramInt)
    {
        return (PduPart)this.mParts.remove(paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.PduBody
 * JD-Core Version:        0.6.2
 */