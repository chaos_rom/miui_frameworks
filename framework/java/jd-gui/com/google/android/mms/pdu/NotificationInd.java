package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class NotificationInd extends GenericPdu
{
    public NotificationInd()
        throws InvalidHeaderValueException
    {
        setMessageType(130);
    }

    NotificationInd(PduHeaders paramPduHeaders)
    {
        super(paramPduHeaders);
    }

    public int getContentClass()
    {
        return this.mPduHeaders.getOctet(186);
    }

    public byte[] getContentLocation()
    {
        return this.mPduHeaders.getTextString(131);
    }

    public int getDeliveryReport()
    {
        return this.mPduHeaders.getOctet(134);
    }

    public long getExpiry()
    {
        return this.mPduHeaders.getLongInteger(136);
    }

    public EncodedStringValue getFrom()
    {
        return this.mPduHeaders.getEncodedStringValue(137);
    }

    public byte[] getMessageClass()
    {
        return this.mPduHeaders.getTextString(138);
    }

    public long getMessageSize()
    {
        return this.mPduHeaders.getLongInteger(142);
    }

    public EncodedStringValue getSubject()
    {
        return this.mPduHeaders.getEncodedStringValue(150);
    }

    public byte[] getTransactionId()
    {
        return this.mPduHeaders.getTextString(152);
    }

    public void setContentClass(int paramInt)
        throws InvalidHeaderValueException
    {
        this.mPduHeaders.setOctet(paramInt, 186);
    }

    public void setContentLocation(byte[] paramArrayOfByte)
    {
        this.mPduHeaders.setTextString(paramArrayOfByte, 131);
    }

    public void setDeliveryReport(int paramInt)
        throws InvalidHeaderValueException
    {
        this.mPduHeaders.setOctet(paramInt, 134);
    }

    public void setExpiry(long paramLong)
    {
        this.mPduHeaders.setLongInteger(paramLong, 136);
    }

    public void setFrom(EncodedStringValue paramEncodedStringValue)
    {
        this.mPduHeaders.setEncodedStringValue(paramEncodedStringValue, 137);
    }

    public void setMessageClass(byte[] paramArrayOfByte)
    {
        this.mPduHeaders.setTextString(paramArrayOfByte, 138);
    }

    public void setMessageSize(long paramLong)
    {
        this.mPduHeaders.setLongInteger(paramLong, 142);
    }

    public void setSubject(EncodedStringValue paramEncodedStringValue)
    {
        this.mPduHeaders.setEncodedStringValue(paramEncodedStringValue, 150);
    }

    public void setTransactionId(byte[] paramArrayOfByte)
    {
        this.mPduHeaders.setTextString(paramArrayOfByte, 152);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.NotificationInd
 * JD-Core Version:        0.6.2
 */