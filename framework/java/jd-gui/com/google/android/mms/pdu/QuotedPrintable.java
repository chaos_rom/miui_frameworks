package com.google.android.mms.pdu;

import java.io.ByteArrayOutputStream;

public class QuotedPrintable
{
    private static byte ESCAPE_CHAR = 61;

    public static final byte[] decodeQuotedPrintable(byte[] paramArrayOfByte)
    {
        byte[] arrayOfByte = null;
        if (paramArrayOfByte == null)
            return arrayOfByte;
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; ; i++)
        {
            int j;
            while (true)
            {
                if (i >= paramArrayOfByte.length)
                    break label145;
                j = paramArrayOfByte[i];
                if (j != ESCAPE_CHAR)
                    break label136;
                int k = i + 1;
                try
                {
                    if (('\r' == (char)paramArrayOfByte[k]) && ('\n' == (char)paramArrayOfByte[(i + 2)]))
                    {
                        i += 2;
                    }
                    else
                    {
                        int m = i + 1;
                        int n = Character.digit((char)paramArrayOfByte[m], 16);
                        i = m + 1;
                        int i1 = Character.digit((char)paramArrayOfByte[i], 16);
                        if ((n == -1) || (i1 == -1))
                            break;
                        localByteArrayOutputStream.write((char)(i1 + (n << 4)));
                    }
                }
                catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
                {
                }
            }
            break;
            label136: localByteArrayOutputStream.write(j);
            continue;
            label145: arrayOfByte = localByteArrayOutputStream.toByteArray();
            break;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.QuotedPrintable
 * JD-Core Version:        0.6.2
 */