package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class GenericPdu
{
    PduHeaders mPduHeaders = null;

    public GenericPdu()
    {
        this.mPduHeaders = new PduHeaders();
    }

    GenericPdu(PduHeaders paramPduHeaders)
    {
        this.mPduHeaders = paramPduHeaders;
    }

    public EncodedStringValue getFrom()
    {
        return this.mPduHeaders.getEncodedStringValue(137);
    }

    public int getMessageType()
    {
        return this.mPduHeaders.getOctet(140);
    }

    public int getMmsVersion()
    {
        return this.mPduHeaders.getOctet(141);
    }

    PduHeaders getPduHeaders()
    {
        return this.mPduHeaders;
    }

    public void setFrom(EncodedStringValue paramEncodedStringValue)
    {
        this.mPduHeaders.setEncodedStringValue(paramEncodedStringValue, 137);
    }

    public void setMessageType(int paramInt)
        throws InvalidHeaderValueException
    {
        this.mPduHeaders.setOctet(paramInt, 140);
    }

    public void setMmsVersion(int paramInt)
        throws InvalidHeaderValueException
    {
        this.mPduHeaders.setOctet(paramInt, 141);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.GenericPdu
 * JD-Core Version:        0.6.2
 */