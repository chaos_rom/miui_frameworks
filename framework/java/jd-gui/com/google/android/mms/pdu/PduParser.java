package com.google.android.mms.pdu;

import android.content.res.Resources;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;

public class PduParser
{
    private static final boolean DEBUG = false;
    private static final int END_STRING_FLAG = 0;
    private static final int LENGTH_QUOTE = 31;
    private static final boolean LOCAL_LOGV = false;
    private static final String LOG_TAG = "PduParser";
    private static final int LONG_INTEGER_LENGTH_MAX = 8;
    private static final int QUOTE = 127;
    private static final int QUOTED_STRING_FLAG = 34;
    private static final int SHORT_INTEGER_MAX = 127;
    private static final int SHORT_LENGTH_MAX = 30;
    private static final int TEXT_MAX = 127;
    private static final int TEXT_MIN = 32;
    private static final int THE_FIRST_PART = 0;
    private static final int THE_LAST_PART = 1;
    private static final int TYPE_QUOTED_STRING = 1;
    private static final int TYPE_TEXT_STRING = 0;
    private static final int TYPE_TOKEN_STRING = 2;
    private static byte[] mStartParam;
    private static byte[] mTypeParam;
    private PduBody mBody = null;
    private PduHeaders mHeaders = null;
    private ByteArrayInputStream mPduDataStream = null;

    static
    {
        if (!PduParser.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            mTypeParam = null;
            mStartParam = null;
            return;
        }
    }

    public PduParser(byte[] paramArrayOfByte)
    {
        this.mPduDataStream = new ByteArrayInputStream(paramArrayOfByte);
    }

    protected static boolean checkMandatoryHeader(PduHeaders paramPduHeaders)
    {
        boolean bool;
        if (paramPduHeaders == null)
            bool = false;
        while (true)
        {
            return bool;
            int i = paramPduHeaders.getOctet(140);
            if (paramPduHeaders.getOctet(141) == 0)
                bool = false;
            else
                switch (i)
                {
                default:
                    bool = false;
                    break;
                case 128:
                    if (paramPduHeaders.getTextString(132) == null)
                        bool = false;
                    else if (paramPduHeaders.getEncodedStringValue(137) == null)
                        bool = false;
                    else if (paramPduHeaders.getTextString(152) == null)
                        bool = false;
                    break;
                case 129:
                    if (paramPduHeaders.getOctet(146) == 0)
                        bool = false;
                    else if (paramPduHeaders.getTextString(152) == null)
                        bool = false;
                    break;
                case 130:
                    if (paramPduHeaders.getTextString(131) == null)
                        bool = false;
                    else if (-1L == paramPduHeaders.getLongInteger(136))
                        bool = false;
                    else if (paramPduHeaders.getTextString(138) == null)
                        bool = false;
                    else if (-1L == paramPduHeaders.getLongInteger(142))
                        bool = false;
                    else if (paramPduHeaders.getTextString(152) == null)
                        bool = false;
                    break;
                case 131:
                    if (paramPduHeaders.getOctet(149) == 0)
                        bool = false;
                    else if (paramPduHeaders.getTextString(152) == null)
                        bool = false;
                    break;
                case 132:
                    if (paramPduHeaders.getTextString(132) == null)
                        bool = false;
                    else if (-1L == paramPduHeaders.getLongInteger(133))
                        bool = false;
                    break;
                case 134:
                    if (-1L == paramPduHeaders.getLongInteger(133))
                        bool = false;
                    else if (paramPduHeaders.getTextString(139) == null)
                        bool = false;
                    else if (paramPduHeaders.getOctet(149) == 0)
                        bool = false;
                    else if (paramPduHeaders.getEncodedStringValues(151) == null)
                        bool = false;
                    break;
                case 133:
                    if (paramPduHeaders.getTextString(152) == null)
                        bool = false;
                    break;
                case 136:
                    if (-1L == paramPduHeaders.getLongInteger(133))
                        bool = false;
                    else if (paramPduHeaders.getEncodedStringValue(137) == null)
                        bool = false;
                    else if (paramPduHeaders.getTextString(139) == null)
                        bool = false;
                    else if (paramPduHeaders.getOctet(155) == 0)
                        bool = false;
                    else if (paramPduHeaders.getEncodedStringValues(151) == null)
                        bool = false;
                    break;
                case 135:
                    if (paramPduHeaders.getEncodedStringValue(137) == null)
                        bool = false;
                    else if (paramPduHeaders.getTextString(139) == null)
                        bool = false;
                    else if (paramPduHeaders.getOctet(155) == 0)
                        bool = false;
                    else if (paramPduHeaders.getEncodedStringValues(151) == null)
                        bool = false;
                    else
                        bool = true;
                    break;
                }
        }
    }

    private static int checkPartPosition(PduPart paramPduPart)
    {
        int i = 1;
        assert (paramPduPart != null);
        if ((mTypeParam == null) && (mStartParam == null));
        while (true)
        {
            return i;
            if (mStartParam != null)
            {
                byte[] arrayOfByte2 = paramPduPart.getContentId();
                if ((arrayOfByte2 != null) && (i == Arrays.equals(mStartParam, arrayOfByte2)))
                    i = 0;
            }
            else if (mTypeParam != null)
            {
                byte[] arrayOfByte1 = paramPduPart.getContentType();
                if ((arrayOfByte1 != null) && (i == Arrays.equals(mTypeParam, arrayOfByte1)))
                    i = 0;
            }
        }
    }

    protected static int extractByteValue(ByteArrayInputStream paramByteArrayInputStream)
    {
        assert (paramByteArrayInputStream != null);
        int i = paramByteArrayInputStream.read();
        assert (-1 != i);
        return i & 0xFF;
    }

    protected static byte[] getWapString(ByteArrayInputStream paramByteArrayInputStream, int paramInt)
    {
        assert (paramByteArrayInputStream != null);
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        int i = paramByteArrayInputStream.read();
        assert (-1 != i);
        if ((-1 != i) && (i != 0))
        {
            if (paramInt == 2)
                if (isTokenCharacter(i))
                    localByteArrayOutputStream.write(i);
            while (true)
            {
                i = paramByteArrayInputStream.read();
                if (($assertionsDisabled) || (-1 != i))
                    break;
                throw new AssertionError();
                if (isText(i))
                    localByteArrayOutputStream.write(i);
            }
        }
        if (localByteArrayOutputStream.size() > 0);
        for (byte[] arrayOfByte = localByteArrayOutputStream.toByteArray(); ; arrayOfByte = null)
            return arrayOfByte;
    }

    protected static boolean isText(int paramInt)
    {
        boolean bool = true;
        if (((paramInt >= 32) && (paramInt <= 126)) || ((paramInt >= 128) && (paramInt <= 255)));
        while (true)
        {
            return bool;
            switch (paramInt)
            {
            case 9:
            case 10:
            case 13:
            case 11:
            case 12:
            }
            bool = false;
        }
    }

    protected static boolean isTokenCharacter(int paramInt)
    {
        boolean bool = false;
        if ((paramInt < 33) || (paramInt > 126));
        while (true)
        {
            return bool;
            switch (paramInt)
            {
            case 34:
            case 40:
            case 41:
            case 44:
            case 47:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 91:
            case 92:
            case 93:
            case 123:
            case 125:
            }
            bool = true;
        }
    }

    private static void log(String paramString)
    {
    }

    protected static byte[] parseContentType(ByteArrayInputStream paramByteArrayInputStream, HashMap<Integer, Object> paramHashMap)
    {
        assert (paramByteArrayInputStream != null);
        paramByteArrayInputStream.mark(1);
        int i = paramByteArrayInputStream.read();
        assert (-1 != i);
        paramByteArrayInputStream.reset();
        int j = i & 0xFF;
        byte[] arrayOfByte2;
        if (j < 32)
        {
            int k = parseValueLength(paramByteArrayInputStream);
            int m = paramByteArrayInputStream.available();
            paramByteArrayInputStream.mark(1);
            int n = paramByteArrayInputStream.read();
            assert (-1 != n);
            paramByteArrayInputStream.reset();
            int i1 = n & 0xFF;
            if ((i1 >= 32) && (i1 <= 127))
            {
                arrayOfByte1 = parseWapString(paramByteArrayInputStream, 0);
                int i3 = k - (m - paramByteArrayInputStream.available());
                if (i3 > 0)
                    parseContentTypeParams(paramByteArrayInputStream, paramHashMap, Integer.valueOf(i3));
                if (i3 < 0)
                    Log.e("PduParser", "Corrupt MMS message");
            }
            else
            {
                for (arrayOfByte2 = PduContentTypes.contentTypes[0].getBytes(); ; arrayOfByte2 = PduContentTypes.contentTypes[0].getBytes())
                {
                    return arrayOfByte2;
                    if (i1 > 127)
                    {
                        int i2 = parseShortInteger(paramByteArrayInputStream);
                        if (i2 < PduContentTypes.contentTypes.length)
                        {
                            arrayOfByte1 = PduContentTypes.contentTypes[i2].getBytes();
                            break;
                        }
                        paramByteArrayInputStream.reset();
                        arrayOfByte1 = parseWapString(paramByteArrayInputStream, 0);
                        break;
                    }
                    Log.e("PduParser", "Corrupt content-type");
                }
            }
        }
        else
        {
            if (j > 127)
                break label285;
        }
        label285: for (byte[] arrayOfByte1 = parseWapString(paramByteArrayInputStream, 0); ; arrayOfByte1 = PduContentTypes.contentTypes[parseShortInteger(paramByteArrayInputStream)].getBytes())
        {
            arrayOfByte2 = arrayOfByte1;
            break;
        }
    }

    protected static void parseContentTypeParams(ByteArrayInputStream paramByteArrayInputStream, HashMap<Integer, Object> paramHashMap, Integer paramInteger)
    {
        assert (paramByteArrayInputStream != null);
        assert (paramInteger.intValue() > 0);
        int i = paramByteArrayInputStream.available();
        int j = paramInteger.intValue();
        while (j > 0)
        {
            int k = paramByteArrayInputStream.read();
            assert (-1 != k);
            j--;
            switch (k)
            {
            default:
                if (-1 == skipWapValue(paramByteArrayInputStream, j))
                    Log.e("PduParser", "Corrupt Content-Type");
                break;
            case 131:
            case 137:
                paramByteArrayInputStream.mark(1);
                int i5 = extractByteValue(paramByteArrayInputStream);
                paramByteArrayInputStream.reset();
                if (i5 > 127)
                {
                    int i7 = parseShortInteger(paramByteArrayInputStream);
                    if (i7 < PduContentTypes.contentTypes.length)
                    {
                        byte[] arrayOfByte5 = PduContentTypes.contentTypes[i7].getBytes();
                        paramHashMap.put(Integer.valueOf(131), arrayOfByte5);
                    }
                }
                while (true)
                {
                    int i6 = paramByteArrayInputStream.available();
                    j = paramInteger.intValue() - (i - i6);
                    break;
                    byte[] arrayOfByte4 = parseWapString(paramByteArrayInputStream, 0);
                    if ((arrayOfByte4 != null) && (paramHashMap != null))
                        paramHashMap.put(Integer.valueOf(131), arrayOfByte4);
                }
            case 138:
            case 153:
                byte[] arrayOfByte3 = parseWapString(paramByteArrayInputStream, 0);
                if ((arrayOfByte3 != null) && (paramHashMap != null))
                    paramHashMap.put(Integer.valueOf(153), arrayOfByte3);
                int i4 = paramByteArrayInputStream.available();
                j = paramInteger.intValue() - (i - i4);
                break;
            case 129:
                paramByteArrayInputStream.mark(1);
                int n = extractByteValue(paramByteArrayInputStream);
                paramByteArrayInputStream.reset();
                byte[] arrayOfByte2;
                if (((n > 32) && (n < 127)) || (n == 0))
                    arrayOfByte2 = parseWapString(paramByteArrayInputStream, 0);
                while (true)
                {
                    try
                    {
                        String str = new String(arrayOfByte2);
                        int i3 = CharacterSets.getMibEnumValue(str);
                        paramHashMap.put(Integer.valueOf(129), Integer.valueOf(i3));
                        int i2 = paramByteArrayInputStream.available();
                        j = paramInteger.intValue() - (i - i2);
                    }
                    catch (UnsupportedEncodingException localUnsupportedEncodingException)
                    {
                        Log.e("PduParser", Arrays.toString(arrayOfByte2), localUnsupportedEncodingException);
                        paramHashMap.put(Integer.valueOf(129), Integer.valueOf(0));
                        continue;
                    }
                    int i1 = (int)parseIntegerValue(paramByteArrayInputStream);
                    if (paramHashMap != null)
                        paramHashMap.put(Integer.valueOf(129), Integer.valueOf(i1));
                }
            case 133:
            case 151:
                byte[] arrayOfByte1 = parseWapString(paramByteArrayInputStream, 0);
                if ((arrayOfByte1 != null) && (paramHashMap != null))
                    paramHashMap.put(Integer.valueOf(151), arrayOfByte1);
                int m = paramByteArrayInputStream.available();
                j = paramInteger.intValue() - (i - m);
                continue;
                j = 0;
            }
        }
        if (j != 0)
            Log.e("PduParser", "Corrupt Content-Type");
    }

    protected static EncodedStringValue parseEncodedStringValue(ByteArrayInputStream paramByteArrayInputStream)
    {
        Object localObject1 = null;
        assert (paramByteArrayInputStream != null);
        paramByteArrayInputStream.mark(1);
        int i = 0;
        int j = paramByteArrayInputStream.read();
        assert (-1 != j);
        int k = j & 0xFF;
        if (k == 0);
        while (true)
        {
            return localObject1;
            paramByteArrayInputStream.reset();
            if (k < 32)
            {
                parseValueLength(paramByteArrayInputStream);
                i = parseShortInteger(paramByteArrayInputStream);
            }
            byte[] arrayOfByte = parseWapString(paramByteArrayInputStream, 0);
            if (i != 0);
            Object localObject2;
            try
            {
                localObject2 = new EncodedStringValue(i, arrayOfByte);
                break label136;
                EncodedStringValue localEncodedStringValue = new EncodedStringValue(arrayOfByte);
                localObject2 = localEncodedStringValue;
            }
            catch (Exception localException)
            {
            }
            continue;
            label136: localObject1 = localObject2;
        }
    }

    protected static long parseIntegerValue(ByteArrayInputStream paramByteArrayInputStream)
    {
        assert (paramByteArrayInputStream != null);
        paramByteArrayInputStream.mark(1);
        int i = paramByteArrayInputStream.read();
        assert (-1 != i);
        paramByteArrayInputStream.reset();
        if (i > 127);
        for (long l = parseShortInteger(paramByteArrayInputStream); ; l = parseLongInteger(paramByteArrayInputStream))
            return l;
    }

    protected static long parseLongInteger(ByteArrayInputStream paramByteArrayInputStream)
    {
        assert (paramByteArrayInputStream != null);
        int i = paramByteArrayInputStream.read();
        assert (-1 != i);
        int j = i & 0xFF;
        if (j > 8)
            throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
        long l = 0L;
        for (int k = 0; k < j; k++)
        {
            int m = paramByteArrayInputStream.read();
            assert (-1 != m);
            l = (l << 8) + (m & 0xFF);
        }
        return l;
    }

    protected static boolean parsePartHeaders(ByteArrayInputStream paramByteArrayInputStream, PduPart paramPduPart, int paramInt)
    {
        assert (paramByteArrayInputStream != null);
        assert (paramPduPart != null);
        assert (paramInt > 0);
        int i = paramByteArrayInputStream.available();
        int j = paramInt;
        int k;
        boolean bool;
        if (j > 0)
        {
            k = paramByteArrayInputStream.read();
            assert (-1 != k);
            j--;
            if (k > 127)
                switch (k)
                {
                default:
                    if (-1 == skipWapValue(paramByteArrayInputStream, j))
                    {
                        Log.e("PduParser", "Corrupt Part headers");
                        bool = false;
                    }
                    break;
                case 142:
                case 192:
                case 174:
                case 197:
                }
        }
        while (true)
        {
            return bool;
            byte[] arrayOfByte4 = parseWapString(paramByteArrayInputStream, 0);
            if (arrayOfByte4 != null)
                paramPduPart.setContentLocation(arrayOfByte4);
            j = paramInt - (i - paramByteArrayInputStream.available());
            break;
            byte[] arrayOfByte3 = parseWapString(paramByteArrayInputStream, 1);
            if (arrayOfByte3 != null)
                paramPduPart.setContentId(arrayOfByte3);
            j = paramInt - (i - paramByteArrayInputStream.available());
            break;
            if (!Resources.getSystem().getBoolean(17891377))
                break;
            int m = parseValueLength(paramByteArrayInputStream);
            paramByteArrayInputStream.mark(1);
            int n = paramByteArrayInputStream.available();
            int i1 = paramByteArrayInputStream.read();
            if (i1 == 128)
                paramPduPart.setContentDisposition(PduPart.DISPOSITION_FROM_DATA);
            while (true)
            {
                if (n - paramByteArrayInputStream.available() < m)
                {
                    if (paramByteArrayInputStream.read() == 152)
                        paramPduPart.setFilename(parseWapString(paramByteArrayInputStream, 0));
                    int i2 = paramByteArrayInputStream.available();
                    if (n - i2 < m)
                    {
                        int i3 = m - (n - i2);
                        paramByteArrayInputStream.read(new byte[i3], 0, i3);
                    }
                }
                j = paramInt - (i - paramByteArrayInputStream.available());
                break;
                if (i1 == 129)
                {
                    paramPduPart.setContentDisposition(PduPart.DISPOSITION_ATTACHMENT);
                }
                else if (i1 == 130)
                {
                    paramPduPart.setContentDisposition(PduPart.DISPOSITION_INLINE);
                }
                else
                {
                    paramByteArrayInputStream.reset();
                    paramPduPart.setContentDisposition(parseWapString(paramByteArrayInputStream, 0));
                }
            }
            j = 0;
            break;
            if ((k >= 32) && (k <= 127))
            {
                byte[] arrayOfByte1 = parseWapString(paramByteArrayInputStream, 0);
                byte[] arrayOfByte2 = parseWapString(paramByteArrayInputStream, 0);
                String str = new String(arrayOfByte1);
                if (true == "Content-Transfer-Encoding".equalsIgnoreCase(str))
                    paramPduPart.setContentTransferEncoding(arrayOfByte2);
                j = paramInt - (i - paramByteArrayInputStream.available());
                break;
            }
            if (-1 == skipWapValue(paramByteArrayInputStream, j))
            {
                Log.e("PduParser", "Corrupt Part headers");
                bool = false;
            }
            else
            {
                j = 0;
                break;
                if (j != 0)
                {
                    Log.e("PduParser", "Corrupt Part headers");
                    bool = false;
                }
                else
                {
                    bool = true;
                }
            }
        }
    }

    protected static PduBody parseParts(ByteArrayInputStream paramByteArrayInputStream)
    {
        PduBody localPduBody;
        if (paramByteArrayInputStream == null)
            localPduBody = null;
        int j;
        label23: int m;
        PduPart localPduPart;
        while (true)
        {
            return localPduBody;
            int i = parseUnsignedInt(paramByteArrayInputStream);
            localPduBody = new PduBody();
            j = 0;
            if (j >= i)
                break label348;
            int k = parseUnsignedInt(paramByteArrayInputStream);
            m = parseUnsignedInt(paramByteArrayInputStream);
            localPduPart = new PduPart();
            int n = paramByteArrayInputStream.available();
            if (n <= 0)
            {
                localPduBody = null;
            }
            else
            {
                HashMap localHashMap = new HashMap();
                byte[] arrayOfByte1 = parseContentType(paramByteArrayInputStream, localHashMap);
                if (arrayOfByte1 != null)
                    localPduPart.setContentType(arrayOfByte1);
                int i1;
                while (true)
                {
                    byte[] arrayOfByte2 = (byte[])localHashMap.get(Integer.valueOf(151));
                    if (arrayOfByte2 != null)
                        localPduPart.setName(arrayOfByte2);
                    Integer localInteger = (Integer)localHashMap.get(Integer.valueOf(129));
                    if (localInteger != null)
                        localPduPart.setCharset(localInteger.intValue());
                    i1 = k - (n - paramByteArrayInputStream.available());
                    if (i1 <= 0)
                        break label205;
                    if (parsePartHeaders(paramByteArrayInputStream, localPduPart, i1))
                        break label215;
                    localPduBody = null;
                    break;
                    localPduPart.setContentType(PduContentTypes.contentTypes[0].getBytes());
                }
                label205: if (i1 >= 0)
                    break;
                localPduBody = null;
            }
        }
        label215: if ((localPduPart.getContentLocation() == null) && (localPduPart.getName() == null) && (localPduPart.getFilename() == null) && (localPduPart.getContentId() == null))
            localPduPart.setContentLocation(Long.toOctalString(System.currentTimeMillis()).getBytes());
        byte[] arrayOfByte3;
        if (m > 0)
        {
            arrayOfByte3 = new byte[m];
            String str1 = new String(localPduPart.getContentType());
            paramByteArrayInputStream.read(arrayOfByte3, 0, m);
            if (str1.equalsIgnoreCase("application/vnd.wap.multipart.alternative"))
            {
                ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(arrayOfByte3);
                localPduPart = parseParts(localByteArrayInputStream).getPart(0);
            }
        }
        else
        {
            label329: if (checkPartPosition(localPduPart) != 0)
                break label438;
            localPduBody.addPart(0, localPduPart);
        }
        while (true)
        {
            j++;
            break label23;
            label348: break;
            byte[] arrayOfByte4 = localPduPart.getContentTransferEncoding();
            String str2;
            if (arrayOfByte4 != null)
            {
                str2 = new String(arrayOfByte4);
                if (!str2.equalsIgnoreCase("base64"))
                    break label407;
                arrayOfByte3 = Base64.decodeBase64(arrayOfByte3);
            }
            while (true)
            {
                if (arrayOfByte3 != null)
                    break label428;
                log("Decode part data error!");
                localPduBody = null;
                break;
                label407: if (str2.equalsIgnoreCase("quoted-printable"))
                    arrayOfByte3 = QuotedPrintable.decodeQuotedPrintable(arrayOfByte3);
            }
            label428: localPduPart.setData(arrayOfByte3);
            break label329;
            label438: localPduBody.addPart(localPduPart);
        }
    }

    protected static int parseShortInteger(ByteArrayInputStream paramByteArrayInputStream)
    {
        assert (paramByteArrayInputStream != null);
        int i = paramByteArrayInputStream.read();
        assert (-1 != i);
        return i & 0x7F;
    }

    protected static int parseUnsignedInt(ByteArrayInputStream paramByteArrayInputStream)
    {
        assert (paramByteArrayInputStream != null);
        int i = 0;
        int j = paramByteArrayInputStream.read();
        int k;
        if (j == -1)
            k = j;
        while (true)
        {
            return k;
            while (true)
                if ((j & 0x80) != 0)
                {
                    i = i << 7 | j & 0x7F;
                    j = paramByteArrayInputStream.read();
                    if (j == -1)
                    {
                        k = j;
                        break;
                    }
                }
            k = i << 7 | j & 0x7F;
        }
    }

    protected static int parseValueLength(ByteArrayInputStream paramByteArrayInputStream)
    {
        assert (paramByteArrayInputStream != null);
        int i = paramByteArrayInputStream.read();
        assert (-1 != i);
        int j = i & 0xFF;
        if (j <= 30);
        while (true)
        {
            return j;
            if (j != 31)
                break;
            j = parseUnsignedInt(paramByteArrayInputStream);
        }
        throw new RuntimeException("Value length > LENGTH_QUOTE!");
    }

    protected static byte[] parseWapString(ByteArrayInputStream paramByteArrayInputStream, int paramInt)
    {
        assert (paramByteArrayInputStream != null);
        paramByteArrayInputStream.mark(1);
        int i = paramByteArrayInputStream.read();
        assert (-1 != i);
        if ((1 == paramInt) && (34 == i))
            paramByteArrayInputStream.mark(1);
        while (true)
        {
            return getWapString(paramByteArrayInputStream, paramInt);
            if ((paramInt == 0) && (127 == i))
                paramByteArrayInputStream.mark(1);
            else
                paramByteArrayInputStream.reset();
        }
    }

    protected static int skipWapValue(ByteArrayInputStream paramByteArrayInputStream, int paramInt)
    {
        assert (paramByteArrayInputStream != null);
        int i = paramByteArrayInputStream.read(new byte[paramInt], 0, paramInt);
        if (i < paramInt)
            i = -1;
        return i;
    }

    public GenericPdu parse()
    {
        Object localObject;
        if (this.mPduDataStream == null)
            localObject = null;
        while (true)
        {
            return localObject;
            this.mHeaders = parseHeaders(this.mPduDataStream);
            if (this.mHeaders == null)
            {
                localObject = null;
            }
            else
            {
                int i = this.mHeaders.getOctet(140);
                if (!checkMandatoryHeader(this.mHeaders))
                {
                    log("check mandatory headers failed!");
                    localObject = null;
                }
                else if ((128 == i) || (132 == i))
                {
                    this.mBody = parseParts(this.mPduDataStream);
                    if (this.mBody == null)
                        localObject = null;
                }
                else
                {
                    switch (i)
                    {
                    default:
                        log("Parser doesn't support this message type in this version!");
                        localObject = null;
                        break;
                    case 128:
                        localObject = new SendReq(this.mHeaders, this.mBody);
                        break;
                    case 129:
                        localObject = new SendConf(this.mHeaders);
                        break;
                    case 130:
                        localObject = new NotificationInd(this.mHeaders);
                        break;
                    case 131:
                        localObject = new NotifyRespInd(this.mHeaders);
                        break;
                    case 132:
                        localObject = new RetrieveConf(this.mHeaders, this.mBody);
                        byte[] arrayOfByte = ((RetrieveConf)localObject).getContentType();
                        if (arrayOfByte == null)
                        {
                            localObject = null;
                        }
                        else
                        {
                            String str = new String(arrayOfByte);
                            if ((!str.equals("application/vnd.wap.multipart.mixed")) && (!str.equals("application/vnd.wap.multipart.related")) && (!str.equals("application/vnd.wap.multipart.alternative")))
                                if (str.equals("application/vnd.wap.multipart.alternative"))
                                {
                                    PduPart localPduPart = this.mBody.getPart(0);
                                    this.mBody.removeAll();
                                    this.mBody.addPart(0, localPduPart);
                                }
                                else
                                {
                                    localObject = null;
                                }
                        }
                        break;
                    case 134:
                        localObject = new DeliveryInd(this.mHeaders);
                        break;
                    case 133:
                        localObject = new AcknowledgeInd(this.mHeaders);
                        break;
                    case 136:
                        localObject = new ReadOrigInd(this.mHeaders);
                        break;
                    case 135:
                        localObject = new ReadRecInd(this.mHeaders);
                    }
                }
            }
        }
    }

    // ERROR //
    protected PduHeaders parseHeaders(ByteArrayInputStream paramByteArrayInputStream)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +7 -> 8
        //     4: aconst_null
        //     5: astore_3
        //     6: aload_3
        //     7: areturn
        //     8: iconst_1
        //     9: istore_2
        //     10: new 77	com/google/android/mms/pdu/PduHeaders
        //     13: dup
        //     14: invokespecial 457	com/google/android/mms/pdu/PduHeaders:<init>	()V
        //     17: astore_3
        //     18: iload_2
        //     19: ifeq -13 -> 6
        //     22: aload_1
        //     23: invokevirtual 163	java/io/ByteArrayInputStream:available	()I
        //     26: ifle -20 -> 6
        //     29: aload_1
        //     30: iconst_1
        //     31: invokevirtual 154	java/io/ByteArrayInputStream:mark	(I)V
        //     34: aload_1
        //     35: invokestatic 213	com/google/android/mms/pdu/PduParser:extractByteValue	(Ljava/io/ByteArrayInputStream;)I
        //     38: istore 4
        //     40: iload 4
        //     42: bipush 32
        //     44: if_icmplt +23 -> 67
        //     47: iload 4
        //     49: bipush 127
        //     51: if_icmpgt +16 -> 67
        //     54: aload_1
        //     55: invokevirtual 157	java/io/ByteArrayInputStream:reset	()V
        //     58: aload_1
        //     59: iconst_0
        //     60: invokestatic 166	com/google/android/mms/pdu/PduParser:parseWapString	(Ljava/io/ByteArrayInputStream;I)[B
        //     63: pop
        //     64: goto -46 -> 18
        //     67: iload 4
        //     69: tableswitch	default:+267 -> 336, 129:+755->824, 130:+755->824, 131:+626->695, 132:+1790->1859, 133:+538->607, 134:+448->517, 135:+890->959, 136:+890->959, 137:+1004->1073, 138:+1204->1273, 139:+626->695, 140:+276->345, 141:+1431->1500, 142:+538->607, 143:+448->517, 144:+448->517, 145:+448->517, 146:+448->517, 147:+691->760, 148:+448->517, 149:+448->517, 150:+691->760, 151:+755->824, 152:+626->695, 153:+448->517, 154:+691->760, 155:+448->517, 156:+448->517, 157:+890->959, 158:+626->695, 159:+538->607, 160:+1522->1591, 161:+1628->1697, 162:+448->517, 163:+448->517, 164:+1714->1783, 165:+448->517, 166:+691->760, 167:+448->517, 168:+267->336, 169:+448->517, 170:+1732->1801, 171:+448->517, 172:+1732->1801, 173:+582->651, 174:+267->336, 175:+582->651, 176:+267->336, 177:+448->517, 178:+1781->1850, 179:+582->651, 180:+448->517, 181:+691->760, 182:+691->760, 183:+626->695, 184:+626->695, 185:+626->695, 186:+448->517, 187:+448->517, 188:+448->517, 189:+626->695, 190:+626->695, 191:+448->517
        //     337: aconst_null
        //     338: <illegal opcode>
        //     339: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     342: goto -324 -> 18
        //     345: aload_1
        //     346: invokestatic 213	com/google/android/mms/pdu/PduParser:extractByteValue	(Ljava/io/ByteArrayInputStream;)I
        //     349: istore 74
        //     351: iload 74
        //     353: tableswitch	default:+75 -> 428, 137:+128->481, 138:+128->481, 139:+128->481, 140:+128->481, 141:+128->481, 142:+128->481, 143:+128->481, 144:+128->481, 145:+128->481, 146:+128->481, 147:+128->481, 148:+128->481, 149:+128->481, 150:+128->481, 151:+128->481
        //     429: iload 74
        //     431: iload 4
        //     433: invokevirtual 463	com/google/android/mms/pdu/PduHeaders:setOctet	(II)V
        //     436: goto -418 -> 18
        //     439: astore 76
        //     441: new 465	java/lang/StringBuilder
        //     444: dup
        //     445: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     448: ldc_w 468
        //     451: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     454: iload 74
        //     456: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     459: ldc_w 477
        //     462: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     465: iload 4
        //     467: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     470: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     473: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     476: aconst_null
        //     477: astore_3
        //     478: goto -472 -> 6
        //     481: aconst_null
        //     482: astore_3
        //     483: goto -477 -> 6
        //     486: astore 75
        //     488: new 465	java/lang/StringBuilder
        //     491: dup
        //     492: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     495: iload 4
        //     497: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     500: ldc_w 482
        //     503: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     506: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     509: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     512: aconst_null
        //     513: astore_3
        //     514: goto -508 -> 6
        //     517: aload_1
        //     518: invokestatic 213	com/google/android/mms/pdu/PduParser:extractByteValue	(Ljava/io/ByteArrayInputStream;)I
        //     521: istore 71
        //     523: aload_3
        //     524: iload 71
        //     526: iload 4
        //     528: invokevirtual 463	com/google/android/mms/pdu/PduHeaders:setOctet	(II)V
        //     531: goto -513 -> 18
        //     534: astore 73
        //     536: new 465	java/lang/StringBuilder
        //     539: dup
        //     540: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     543: ldc_w 468
        //     546: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     549: iload 71
        //     551: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     554: ldc_w 477
        //     557: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     560: iload 4
        //     562: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     565: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     568: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     571: aconst_null
        //     572: astore_3
        //     573: goto -567 -> 6
        //     576: astore 72
        //     578: new 465	java/lang/StringBuilder
        //     581: dup
        //     582: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     585: iload 4
        //     587: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     590: ldc_w 482
        //     593: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     596: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     599: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     602: aconst_null
        //     603: astore_3
        //     604: goto -598 -> 6
        //     607: aload_3
        //     608: aload_1
        //     609: invokestatic 250	com/google/android/mms/pdu/PduParser:parseLongInteger	(Ljava/io/ByteArrayInputStream;)J
        //     612: iload 4
        //     614: invokevirtual 486	com/google/android/mms/pdu/PduHeaders:setLongInteger	(JI)V
        //     617: goto -599 -> 18
        //     620: astore 70
        //     622: new 465	java/lang/StringBuilder
        //     625: dup
        //     626: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     629: iload 4
        //     631: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     634: ldc_w 488
        //     637: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     640: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     643: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     646: aconst_null
        //     647: astore_3
        //     648: goto -642 -> 6
        //     651: aload_3
        //     652: aload_1
        //     653: invokestatic 237	com/google/android/mms/pdu/PduParser:parseIntegerValue	(Ljava/io/ByteArrayInputStream;)J
        //     656: iload 4
        //     658: invokevirtual 486	com/google/android/mms/pdu/PduHeaders:setLongInteger	(JI)V
        //     661: goto -643 -> 18
        //     664: astore 69
        //     666: new 465	java/lang/StringBuilder
        //     669: dup
        //     670: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     673: iload 4
        //     675: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     678: ldc_w 488
        //     681: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     684: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     687: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     690: aconst_null
        //     691: astore_3
        //     692: goto -686 -> 6
        //     695: aload_1
        //     696: iconst_0
        //     697: invokestatic 166	com/google/android/mms/pdu/PduParser:parseWapString	(Ljava/io/ByteArrayInputStream;I)[B
        //     700: astore 66
        //     702: aload 66
        //     704: ifnull -686 -> 18
        //     707: aload_3
        //     708: aload 66
        //     710: iload 4
        //     712: invokevirtual 492	com/google/android/mms/pdu/PduHeaders:setTextString	([BI)V
        //     715: goto -697 -> 18
        //     718: astore 68
        //     720: ldc_w 494
        //     723: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     726: goto -708 -> 18
        //     729: astore 67
        //     731: new 465	java/lang/StringBuilder
        //     734: dup
        //     735: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     738: iload 4
        //     740: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     743: ldc_w 496
        //     746: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     749: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     752: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     755: aconst_null
        //     756: astore_3
        //     757: goto -751 -> 6
        //     760: aload_1
        //     761: invokestatic 498	com/google/android/mms/pdu/PduParser:parseEncodedStringValue	(Ljava/io/ByteArrayInputStream;)Lcom/google/android/mms/pdu/EncodedStringValue;
        //     764: astore 63
        //     766: aload 63
        //     768: ifnull -750 -> 18
        //     771: aload_3
        //     772: aload 63
        //     774: iload 4
        //     776: invokevirtual 502	com/google/android/mms/pdu/PduHeaders:setEncodedStringValue	(Lcom/google/android/mms/pdu/EncodedStringValue;I)V
        //     779: goto -761 -> 18
        //     782: astore 65
        //     784: ldc_w 494
        //     787: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     790: goto -772 -> 18
        //     793: astore 64
        //     795: new 465	java/lang/StringBuilder
        //     798: dup
        //     799: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     802: iload 4
        //     804: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     807: ldc_w 504
        //     810: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     813: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     816: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     819: aconst_null
        //     820: astore_3
        //     821: goto -815 -> 6
        //     824: aload_1
        //     825: invokestatic 498	com/google/android/mms/pdu/PduParser:parseEncodedStringValue	(Ljava/io/ByteArrayInputStream;)Lcom/google/android/mms/pdu/EncodedStringValue;
        //     828: astore 56
        //     830: aload 56
        //     832: ifnull -814 -> 18
        //     835: aload 56
        //     837: invokevirtual 506	com/google/android/mms/pdu/EncodedStringValue:getTextString	()[B
        //     840: astore 57
        //     842: aload 57
        //     844: ifnull +49 -> 893
        //     847: new 192	java/lang/String
        //     850: dup
        //     851: aload 57
        //     853: invokespecial 220	java/lang/String:<init>	([B)V
        //     856: astore 58
        //     858: aload 58
        //     860: ldc_w 508
        //     863: invokevirtual 511	java/lang/String:indexOf	(Ljava/lang/String;)I
        //     866: istore 59
        //     868: iload 59
        //     870: ifle +13 -> 883
        //     873: aload 58
        //     875: iconst_0
        //     876: iload 59
        //     878: invokevirtual 515	java/lang/String:substring	(II)Ljava/lang/String;
        //     881: astore 58
        //     883: aload 56
        //     885: aload 58
        //     887: invokevirtual 195	java/lang/String:getBytes	()[B
        //     890: invokevirtual 517	com/google/android/mms/pdu/EncodedStringValue:setTextString	([B)V
        //     893: aload_3
        //     894: aload 56
        //     896: iload 4
        //     898: invokevirtual 520	com/google/android/mms/pdu/PduHeaders:appendEncodedStringValue	(Lcom/google/android/mms/pdu/EncodedStringValue;I)V
        //     901: goto -883 -> 18
        //     904: astore 62
        //     906: ldc_w 494
        //     909: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     912: goto -894 -> 18
        //     915: astore 60
        //     917: ldc_w 494
        //     920: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     923: aconst_null
        //     924: astore_3
        //     925: goto -919 -> 6
        //     928: astore 61
        //     930: new 465	java/lang/StringBuilder
        //     933: dup
        //     934: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     937: iload 4
        //     939: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     942: ldc_w 504
        //     945: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     948: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     951: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     954: aconst_null
        //     955: astore_3
        //     956: goto -950 -> 6
        //     959: aload_1
        //     960: invokestatic 160	com/google/android/mms/pdu/PduParser:parseValueLength	(Ljava/io/ByteArrayInputStream;)I
        //     963: pop
        //     964: aload_1
        //     965: invokestatic 213	com/google/android/mms/pdu/PduParser:extractByteValue	(Ljava/io/ByteArrayInputStream;)I
        //     968: istore 49
        //     970: aload_1
        //     971: invokestatic 250	com/google/android/mms/pdu/PduParser:parseLongInteger	(Ljava/io/ByteArrayInputStream;)J
        //     974: lstore 51
        //     976: lload 51
        //     978: lstore 53
        //     980: sipush 129
        //     983: iload 49
        //     985: if_icmpne +15 -> 1000
        //     988: lload 53
        //     990: invokestatic 346	java/lang/System:currentTimeMillis	()J
        //     993: ldc2_w 521
        //     996: ldiv
        //     997: ladd
        //     998: lstore 53
        //     1000: aload_3
        //     1001: lload 53
        //     1003: iload 4
        //     1005: invokevirtual 486	com/google/android/mms/pdu/PduHeaders:setLongInteger	(JI)V
        //     1008: goto -990 -> 18
        //     1011: astore 55
        //     1013: new 465	java/lang/StringBuilder
        //     1016: dup
        //     1017: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1020: iload 4
        //     1022: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1025: ldc_w 488
        //     1028: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1031: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1034: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1037: aconst_null
        //     1038: astore_3
        //     1039: goto -1033 -> 6
        //     1042: astore 50
        //     1044: new 465	java/lang/StringBuilder
        //     1047: dup
        //     1048: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1051: iload 4
        //     1053: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1056: ldc_w 488
        //     1059: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1062: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1065: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1068: aconst_null
        //     1069: astore_3
        //     1070: goto -1064 -> 6
        //     1073: aload_1
        //     1074: invokestatic 160	com/google/android/mms/pdu/PduParser:parseValueLength	(Ljava/io/ByteArrayInputStream;)I
        //     1077: pop
        //     1078: sipush 128
        //     1081: aload_1
        //     1082: invokestatic 213	com/google/android/mms/pdu/PduParser:extractByteValue	(Ljava/io/ByteArrayInputStream;)I
        //     1085: if_icmpne +108 -> 1193
        //     1088: aload_1
        //     1089: invokestatic 498	com/google/android/mms/pdu/PduParser:parseEncodedStringValue	(Ljava/io/ByteArrayInputStream;)Lcom/google/android/mms/pdu/EncodedStringValue;
        //     1092: astore 40
        //     1094: aload 40
        //     1096: ifnull +61 -> 1157
        //     1099: aload 40
        //     1101: invokevirtual 506	com/google/android/mms/pdu/EncodedStringValue:getTextString	()[B
        //     1104: astore 44
        //     1106: aload 44
        //     1108: ifnull +49 -> 1157
        //     1111: new 192	java/lang/String
        //     1114: dup
        //     1115: aload 44
        //     1117: invokespecial 220	java/lang/String:<init>	([B)V
        //     1120: astore 45
        //     1122: aload 45
        //     1124: ldc_w 508
        //     1127: invokevirtual 511	java/lang/String:indexOf	(Ljava/lang/String;)I
        //     1130: istore 46
        //     1132: iload 46
        //     1134: ifle +13 -> 1147
        //     1137: aload 45
        //     1139: iconst_0
        //     1140: iload 46
        //     1142: invokevirtual 515	java/lang/String:substring	(II)Ljava/lang/String;
        //     1145: astore 45
        //     1147: aload 40
        //     1149: aload 45
        //     1151: invokevirtual 195	java/lang/String:getBytes	()[B
        //     1154: invokevirtual 517	com/google/android/mms/pdu/EncodedStringValue:setTextString	([B)V
        //     1157: aload_3
        //     1158: aload 40
        //     1160: sipush 137
        //     1163: invokevirtual 502	com/google/android/mms/pdu/PduHeaders:setEncodedStringValue	(Lcom/google/android/mms/pdu/EncodedStringValue;I)V
        //     1166: goto -1148 -> 18
        //     1169: astore 42
        //     1171: ldc_w 494
        //     1174: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1177: goto -1159 -> 18
        //     1180: astore 47
        //     1182: ldc_w 494
        //     1185: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1188: aconst_null
        //     1189: astore_3
        //     1190: goto -1184 -> 6
        //     1193: new 243	com/google/android/mms/pdu/EncodedStringValue
        //     1196: dup
        //     1197: ldc_w 524
        //     1200: invokevirtual 195	java/lang/String:getBytes	()[B
        //     1203: invokespecial 247	com/google/android/mms/pdu/EncodedStringValue:<init>	([B)V
        //     1206: astore 40
        //     1208: goto -51 -> 1157
        //     1211: astore 43
        //     1213: new 465	java/lang/StringBuilder
        //     1216: dup
        //     1217: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1220: iload 4
        //     1222: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1225: ldc_w 504
        //     1228: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1231: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1234: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1237: aconst_null
        //     1238: astore_3
        //     1239: goto -1233 -> 6
        //     1242: astore 41
        //     1244: new 465	java/lang/StringBuilder
        //     1247: dup
        //     1248: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1251: iload 4
        //     1253: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1256: ldc_w 504
        //     1259: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1262: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1265: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1268: aconst_null
        //     1269: astore_3
        //     1270: goto -1264 -> 6
        //     1273: aload_1
        //     1274: iconst_1
        //     1275: invokevirtual 154	java/io/ByteArrayInputStream:mark	(I)V
        //     1278: aload_1
        //     1279: invokestatic 213	com/google/android/mms/pdu/PduParser:extractByteValue	(Ljava/io/ByteArrayInputStream;)I
        //     1282: istore 33
        //     1284: iload 33
        //     1286: sipush 128
        //     1289: if_icmplt +141 -> 1430
        //     1292: sipush 128
        //     1295: iload 33
        //     1297: if_icmpne +30 -> 1327
        //     1300: aload_3
        //     1301: ldc_w 526
        //     1304: invokevirtual 195	java/lang/String:getBytes	()[B
        //     1307: sipush 138
        //     1310: invokevirtual 492	com/google/android/mms/pdu/PduHeaders:setTextString	([BI)V
        //     1313: goto -1295 -> 18
        //     1316: astore 38
        //     1318: ldc_w 494
        //     1321: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1324: goto -1306 -> 18
        //     1327: sipush 129
        //     1330: iload 33
        //     1332: if_icmpne +50 -> 1382
        //     1335: aload_3
        //     1336: ldc_w 528
        //     1339: invokevirtual 195	java/lang/String:getBytes	()[B
        //     1342: sipush 138
        //     1345: invokevirtual 492	com/google/android/mms/pdu/PduHeaders:setTextString	([BI)V
        //     1348: goto -1330 -> 18
        //     1351: astore 37
        //     1353: new 465	java/lang/StringBuilder
        //     1356: dup
        //     1357: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1360: iload 4
        //     1362: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1365: ldc_w 496
        //     1368: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1371: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1374: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1377: aconst_null
        //     1378: astore_3
        //     1379: goto -1373 -> 6
        //     1382: sipush 130
        //     1385: iload 33
        //     1387: if_icmpne +19 -> 1406
        //     1390: aload_3
        //     1391: ldc_w 530
        //     1394: invokevirtual 195	java/lang/String:getBytes	()[B
        //     1397: sipush 138
        //     1400: invokevirtual 492	com/google/android/mms/pdu/PduHeaders:setTextString	([BI)V
        //     1403: goto -1385 -> 18
        //     1406: sipush 131
        //     1409: iload 33
        //     1411: if_icmpne -1393 -> 18
        //     1414: aload_3
        //     1415: ldc_w 532
        //     1418: invokevirtual 195	java/lang/String:getBytes	()[B
        //     1421: sipush 138
        //     1424: invokevirtual 492	com/google/android/mms/pdu/PduHeaders:setTextString	([BI)V
        //     1427: goto -1409 -> 18
        //     1430: aload_1
        //     1431: invokevirtual 157	java/io/ByteArrayInputStream:reset	()V
        //     1434: aload_1
        //     1435: iconst_0
        //     1436: invokestatic 166	com/google/android/mms/pdu/PduParser:parseWapString	(Ljava/io/ByteArrayInputStream;I)[B
        //     1439: astore 34
        //     1441: aload 34
        //     1443: ifnull -1425 -> 18
        //     1446: aload_3
        //     1447: aload 34
        //     1449: sipush 138
        //     1452: invokevirtual 492	com/google/android/mms/pdu/PduHeaders:setTextString	([BI)V
        //     1455: goto -1437 -> 18
        //     1458: astore 36
        //     1460: ldc_w 494
        //     1463: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1466: goto -1448 -> 18
        //     1469: astore 35
        //     1471: new 465	java/lang/StringBuilder
        //     1474: dup
        //     1475: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1478: iload 4
        //     1480: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1483: ldc_w 496
        //     1486: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1489: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1492: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1495: aconst_null
        //     1496: astore_3
        //     1497: goto -1491 -> 6
        //     1500: aload_1
        //     1501: invokestatic 198	com/google/android/mms/pdu/PduParser:parseShortInteger	(Ljava/io/ByteArrayInputStream;)I
        //     1504: istore 30
        //     1506: aload_3
        //     1507: iload 30
        //     1509: sipush 141
        //     1512: invokevirtual 463	com/google/android/mms/pdu/PduHeaders:setOctet	(II)V
        //     1515: goto -1497 -> 18
        //     1518: astore 32
        //     1520: new 465	java/lang/StringBuilder
        //     1523: dup
        //     1524: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1527: ldc_w 468
        //     1530: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1533: iload 30
        //     1535: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1538: ldc_w 477
        //     1541: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1544: iload 4
        //     1546: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1549: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1552: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1555: aconst_null
        //     1556: astore_3
        //     1557: goto -1551 -> 6
        //     1560: astore 31
        //     1562: new 465	java/lang/StringBuilder
        //     1565: dup
        //     1566: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1569: iload 4
        //     1571: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1574: ldc_w 482
        //     1577: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1580: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1583: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1586: aconst_null
        //     1587: astore_3
        //     1588: goto -1582 -> 6
        //     1591: aload_1
        //     1592: invokestatic 160	com/google/android/mms/pdu/PduParser:parseValueLength	(Ljava/io/ByteArrayInputStream;)I
        //     1595: pop
        //     1596: aload_1
        //     1597: invokestatic 237	com/google/android/mms/pdu/PduParser:parseIntegerValue	(Ljava/io/ByteArrayInputStream;)J
        //     1600: pop2
        //     1601: aload_1
        //     1602: invokestatic 498	com/google/android/mms/pdu/PduParser:parseEncodedStringValue	(Ljava/io/ByteArrayInputStream;)Lcom/google/android/mms/pdu/EncodedStringValue;
        //     1605: astore 27
        //     1607: aload 27
        //     1609: ifnull -1591 -> 18
        //     1612: aload_3
        //     1613: aload 27
        //     1615: sipush 160
        //     1618: invokevirtual 502	com/google/android/mms/pdu/PduHeaders:setEncodedStringValue	(Lcom/google/android/mms/pdu/EncodedStringValue;I)V
        //     1621: goto -1603 -> 18
        //     1624: astore 29
        //     1626: ldc_w 494
        //     1629: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1632: goto -1614 -> 18
        //     1635: astore 24
        //     1637: new 465	java/lang/StringBuilder
        //     1640: dup
        //     1641: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1644: iload 4
        //     1646: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1649: ldc_w 534
        //     1652: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1655: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1658: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1661: aconst_null
        //     1662: astore_3
        //     1663: goto -1657 -> 6
        //     1666: astore 28
        //     1668: new 465	java/lang/StringBuilder
        //     1671: dup
        //     1672: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1675: iload 4
        //     1677: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1680: ldc_w 504
        //     1683: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1686: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1689: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1692: aconst_null
        //     1693: astore_3
        //     1694: goto -1688 -> 6
        //     1697: aload_1
        //     1698: invokestatic 160	com/google/android/mms/pdu/PduParser:parseValueLength	(Ljava/io/ByteArrayInputStream;)I
        //     1701: pop
        //     1702: aload_1
        //     1703: invokestatic 237	com/google/android/mms/pdu/PduParser:parseIntegerValue	(Ljava/io/ByteArrayInputStream;)J
        //     1706: pop2
        //     1707: aload_3
        //     1708: aload_1
        //     1709: invokestatic 250	com/google/android/mms/pdu/PduParser:parseLongInteger	(Ljava/io/ByteArrayInputStream;)J
        //     1712: sipush 161
        //     1715: invokevirtual 486	com/google/android/mms/pdu/PduHeaders:setLongInteger	(JI)V
        //     1718: goto -1700 -> 18
        //     1721: astore 22
        //     1723: new 465	java/lang/StringBuilder
        //     1726: dup
        //     1727: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1730: iload 4
        //     1732: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1735: ldc_w 488
        //     1738: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1741: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1744: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1747: aconst_null
        //     1748: astore_3
        //     1749: goto -1743 -> 6
        //     1752: astore 19
        //     1754: new 465	java/lang/StringBuilder
        //     1757: dup
        //     1758: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1761: iload 4
        //     1763: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1766: ldc_w 534
        //     1769: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1772: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1775: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1778: aconst_null
        //     1779: astore_3
        //     1780: goto -1774 -> 6
        //     1783: aload_1
        //     1784: invokestatic 160	com/google/android/mms/pdu/PduParser:parseValueLength	(Ljava/io/ByteArrayInputStream;)I
        //     1787: pop
        //     1788: aload_1
        //     1789: invokestatic 213	com/google/android/mms/pdu/PduParser:extractByteValue	(Ljava/io/ByteArrayInputStream;)I
        //     1792: pop
        //     1793: aload_1
        //     1794: invokestatic 498	com/google/android/mms/pdu/PduParser:parseEncodedStringValue	(Ljava/io/ByteArrayInputStream;)Lcom/google/android/mms/pdu/EncodedStringValue;
        //     1797: pop
        //     1798: goto -1780 -> 18
        //     1801: aload_1
        //     1802: invokestatic 160	com/google/android/mms/pdu/PduParser:parseValueLength	(Ljava/io/ByteArrayInputStream;)I
        //     1805: pop
        //     1806: aload_1
        //     1807: invokestatic 213	com/google/android/mms/pdu/PduParser:extractByteValue	(Ljava/io/ByteArrayInputStream;)I
        //     1810: pop
        //     1811: aload_1
        //     1812: invokestatic 237	com/google/android/mms/pdu/PduParser:parseIntegerValue	(Ljava/io/ByteArrayInputStream;)J
        //     1815: pop2
        //     1816: goto -1798 -> 18
        //     1819: astore 12
        //     1821: new 465	java/lang/StringBuilder
        //     1824: dup
        //     1825: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1828: iload 4
        //     1830: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1833: ldc_w 534
        //     1836: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1839: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1842: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1845: aconst_null
        //     1846: astore_3
        //     1847: goto -1841 -> 6
        //     1850: aload_1
        //     1851: aconst_null
        //     1852: invokestatic 315	com/google/android/mms/pdu/PduParser:parseContentType	(Ljava/io/ByteArrayInputStream;Ljava/util/HashMap;)[B
        //     1855: pop
        //     1856: goto -1838 -> 18
        //     1859: new 215	java/util/HashMap
        //     1862: dup
        //     1863: invokespecial 313	java/util/HashMap:<init>	()V
        //     1866: astore 5
        //     1868: aload_1
        //     1869: aload 5
        //     1871: invokestatic 315	com/google/android/mms/pdu/PduParser:parseContentType	(Ljava/io/ByteArrayInputStream;Ljava/util/HashMap;)[B
        //     1874: astore 6
        //     1876: aload 6
        //     1878: ifnull +12 -> 1890
        //     1881: aload_3
        //     1882: aload 6
        //     1884: sipush 132
        //     1887: invokevirtual 492	com/google/android/mms/pdu/PduHeaders:setTextString	([BI)V
        //     1890: aload 5
        //     1892: sipush 153
        //     1895: invokestatic 172	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     1898: invokevirtual 322	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     1901: checkcast 323	[B
        //     1904: checkcast 323	[B
        //     1907: putstatic 59	com/google/android/mms/pdu/PduParser:mStartParam	[B
        //     1910: aload 5
        //     1912: sipush 131
        //     1915: invokestatic 172	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     1918: invokevirtual 322	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     1921: checkcast 323	[B
        //     1924: checkcast 323	[B
        //     1927: putstatic 57	com/google/android/mms/pdu/PduParser:mTypeParam	[B
        //     1930: iconst_0
        //     1931: istore_2
        //     1932: goto -1914 -> 18
        //     1935: astore 8
        //     1937: ldc_w 494
        //     1940: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1943: goto -53 -> 1890
        //     1946: astore 7
        //     1948: new 465	java/lang/StringBuilder
        //     1951: dup
        //     1952: invokespecial 466	java/lang/StringBuilder:<init>	()V
        //     1955: iload 4
        //     1957: invokevirtual 475	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1960: ldc_w 496
        //     1963: invokevirtual 472	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1966: invokevirtual 480	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1969: invokestatic 381	com/google/android/mms/pdu/PduParser:log	(Ljava/lang/String;)V
        //     1972: aconst_null
        //     1973: astore_3
        //     1974: goto -1968 -> 6
        //
        // Exception table:
        //     from	to	target	type
        //     428	436	439	com/google/android/mms/InvalidHeaderValueException
        //     428	436	486	java/lang/RuntimeException
        //     523	531	534	com/google/android/mms/InvalidHeaderValueException
        //     523	531	576	java/lang/RuntimeException
        //     607	617	620	java/lang/RuntimeException
        //     651	661	664	java/lang/RuntimeException
        //     707	715	718	java/lang/NullPointerException
        //     707	715	729	java/lang/RuntimeException
        //     771	779	782	java/lang/NullPointerException
        //     771	779	793	java/lang/RuntimeException
        //     893	901	904	java/lang/NullPointerException
        //     883	893	915	java/lang/NullPointerException
        //     893	901	928	java/lang/RuntimeException
        //     1000	1008	1011	java/lang/RuntimeException
        //     970	976	1042	java/lang/RuntimeException
        //     1157	1166	1169	java/lang/NullPointerException
        //     1147	1157	1180	java/lang/NullPointerException
        //     1193	1208	1211	java/lang/NullPointerException
        //     1157	1166	1242	java/lang/RuntimeException
        //     1300	1313	1316	java/lang/NullPointerException
        //     1335	1348	1316	java/lang/NullPointerException
        //     1390	1427	1316	java/lang/NullPointerException
        //     1300	1313	1351	java/lang/RuntimeException
        //     1335	1348	1351	java/lang/RuntimeException
        //     1390	1427	1351	java/lang/RuntimeException
        //     1446	1455	1458	java/lang/NullPointerException
        //     1446	1455	1469	java/lang/RuntimeException
        //     1506	1515	1518	com/google/android/mms/InvalidHeaderValueException
        //     1506	1515	1560	java/lang/RuntimeException
        //     1612	1621	1624	java/lang/NullPointerException
        //     1596	1601	1635	java/lang/RuntimeException
        //     1612	1621	1666	java/lang/RuntimeException
        //     1707	1718	1721	java/lang/RuntimeException
        //     1702	1707	1752	java/lang/RuntimeException
        //     1811	1816	1819	java/lang/RuntimeException
        //     1881	1890	1935	java/lang/NullPointerException
        //     1881	1890	1946	java/lang/RuntimeException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.PduParser
 * JD-Core Version:        0.6.2
 */