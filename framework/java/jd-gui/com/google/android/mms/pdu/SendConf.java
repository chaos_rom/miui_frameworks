package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class SendConf extends GenericPdu
{
    public SendConf()
        throws InvalidHeaderValueException
    {
        setMessageType(129);
    }

    SendConf(PduHeaders paramPduHeaders)
    {
        super(paramPduHeaders);
    }

    public byte[] getMessageId()
    {
        return this.mPduHeaders.getTextString(139);
    }

    public int getResponseStatus()
    {
        return this.mPduHeaders.getOctet(146);
    }

    public byte[] getTransactionId()
    {
        return this.mPduHeaders.getTextString(152);
    }

    public void setMessageId(byte[] paramArrayOfByte)
    {
        this.mPduHeaders.setTextString(paramArrayOfByte, 139);
    }

    public void setResponseStatus(int paramInt)
        throws InvalidHeaderValueException
    {
        this.mPduHeaders.setOctet(paramInt, 146);
    }

    public void setTransactionId(byte[] paramArrayOfByte)
    {
        this.mPduHeaders.setTextString(paramArrayOfByte, 152);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.SendConf
 * JD-Core Version:        0.6.2
 */