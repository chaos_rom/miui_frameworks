package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class DeliveryInd extends GenericPdu
{
    public DeliveryInd()
        throws InvalidHeaderValueException
    {
        setMessageType(134);
    }

    DeliveryInd(PduHeaders paramPduHeaders)
    {
        super(paramPduHeaders);
    }

    public long getDate()
    {
        return this.mPduHeaders.getLongInteger(133);
    }

    public byte[] getMessageId()
    {
        return this.mPduHeaders.getTextString(139);
    }

    public int getStatus()
    {
        return this.mPduHeaders.getOctet(149);
    }

    public EncodedStringValue[] getTo()
    {
        return this.mPduHeaders.getEncodedStringValues(151);
    }

    public void setDate(long paramLong)
    {
        this.mPduHeaders.setLongInteger(paramLong, 133);
    }

    public void setMessageId(byte[] paramArrayOfByte)
    {
        this.mPduHeaders.setTextString(paramArrayOfByte, 139);
    }

    public void setStatus(int paramInt)
        throws InvalidHeaderValueException
    {
        this.mPduHeaders.setOctet(paramInt, 149);
    }

    public void setTo(EncodedStringValue[] paramArrayOfEncodedStringValue)
    {
        this.mPduHeaders.setEncodedStringValues(paramArrayOfEncodedStringValue, 151);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.pdu.DeliveryInd
 * JD-Core Version:        0.6.2
 */