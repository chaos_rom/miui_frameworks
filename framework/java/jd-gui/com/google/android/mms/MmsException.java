package com.google.android.mms;

public class MmsException extends Exception
{
    private static final long serialVersionUID = -7323249827281485390L;

    public MmsException()
    {
    }

    public MmsException(String paramString)
    {
        super(paramString);
    }

    public MmsException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public MmsException(Throwable paramThrowable)
    {
        super(paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.google.android.mms.MmsException
 * JD-Core Version:        0.6.2
 */