package com.android.server;

import android.os.SystemProperties;
import android.util.Log;
import android.util.Slog;
import dalvik.system.SocketTagger;
import java.io.FileDescriptor;
import java.net.SocketException;

public final class NetworkManagementSocketTagger extends SocketTagger
{
    private static final boolean LOGD = false;
    public static final String PROP_QTAGUID_ENABLED = "net.qtaguid_enabled";
    private static final String TAG = "NetworkManagementSocketTagger";
    private static ThreadLocal<SocketTags> threadSocketTags = new ThreadLocal()
    {
        protected NetworkManagementSocketTagger.SocketTags initialValue()
        {
            return new NetworkManagementSocketTagger.SocketTags();
        }
    };

    public static int getThreadSocketStatsTag()
    {
        return ((SocketTags)threadSocketTags.get()).statsTag;
    }

    public static void install()
    {
        SocketTagger.set(new NetworkManagementSocketTagger());
    }

    public static int kernelToTag(String paramString)
    {
        int i = 0;
        int j = paramString.length();
        if (j > 10)
            i = Long.decode(paramString.substring(0, j - 8)).intValue();
        return i;
    }

    private static native int native_deleteTagData(int paramInt1, int paramInt2);

    private static native int native_setCounterSet(int paramInt1, int paramInt2);

    private static native int native_tagSocketFd(FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2);

    private static native int native_untagSocketFd(FileDescriptor paramFileDescriptor);

    public static void resetKernelUidStats(int paramInt)
    {
        if (SystemProperties.getBoolean("net.qtaguid_enabled", false))
        {
            int i = native_deleteTagData(0, paramInt);
            if (i < 0)
                Slog.w("NetworkManagementSocketTagger", "problem clearing counters for uid " + paramInt + " : errno " + i);
        }
    }

    public static void setKernelCounterSet(int paramInt1, int paramInt2)
    {
        if (SystemProperties.getBoolean("net.qtaguid_enabled", false))
        {
            int i = native_setCounterSet(paramInt2, paramInt1);
            if (i < 0)
                Log.w("NetworkManagementSocketTagger", "setKernelCountSet(" + paramInt1 + ", " + paramInt2 + ") failed with errno " + i);
        }
    }

    public static void setThreadSocketStatsTag(int paramInt)
    {
        ((SocketTags)threadSocketTags.get()).statsTag = paramInt;
    }

    public static void setThreadSocketStatsUid(int paramInt)
    {
        ((SocketTags)threadSocketTags.get()).statsUid = paramInt;
    }

    private void tagSocketFd(FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2)
    {
        if ((paramInt1 == -1) && (paramInt2 == -1));
        while (true)
        {
            return;
            if (SystemProperties.getBoolean("net.qtaguid_enabled", false))
            {
                int i = native_tagSocketFd(paramFileDescriptor, paramInt1, paramInt2);
                if (i < 0)
                    Log.i("NetworkManagementSocketTagger", "tagSocketFd(" + paramFileDescriptor.getInt$() + ", " + paramInt1 + ", " + paramInt2 + ") failed with errno" + i);
            }
        }
    }

    private void unTagSocketFd(FileDescriptor paramFileDescriptor)
    {
        SocketTags localSocketTags = (SocketTags)threadSocketTags.get();
        if ((localSocketTags.statsTag == -1) && (localSocketTags.statsUid == -1));
        while (true)
        {
            return;
            if (SystemProperties.getBoolean("net.qtaguid_enabled", false))
            {
                int i = native_untagSocketFd(paramFileDescriptor);
                if (i < 0)
                    Log.w("NetworkManagementSocketTagger", "untagSocket(" + paramFileDescriptor.getInt$() + ") failed with errno " + i);
            }
        }
    }

    public void tag(FileDescriptor paramFileDescriptor)
        throws SocketException
    {
        SocketTags localSocketTags = (SocketTags)threadSocketTags.get();
        tagSocketFd(paramFileDescriptor, localSocketTags.statsTag, localSocketTags.statsUid);
    }

    public void untag(FileDescriptor paramFileDescriptor)
        throws SocketException
    {
        unTagSocketFd(paramFileDescriptor);
    }

    public static class SocketTags
    {
        public int statsTag = -1;
        public int statsUid = -1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.server.NetworkManagementSocketTagger
 * JD-Core Version:        0.6.2
 */