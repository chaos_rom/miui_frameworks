package com.android.server;

import android.os.ConditionVariable;
import android.os.SystemClock;

abstract class ResettableTimeout
{
    private ConditionVariable mLock = new ConditionVariable();
    private volatile long mOffAt;
    private volatile boolean mOffCalled;
    private Thread mThread;

    public void cancel()
    {
        try
        {
            this.mOffAt = 0L;
            if (this.mThread != null)
            {
                this.mThread.interrupt();
                this.mThread = null;
            }
            if (!this.mOffCalled)
            {
                this.mOffCalled = true;
                off();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void go(long paramLong)
    {
        try
        {
            this.mOffAt = (paramLong + SystemClock.uptimeMillis());
            if (this.mThread == null)
            {
                bool = false;
                this.mLock.close();
                this.mThread = new T(null);
                this.mThread.start();
                this.mLock.block();
                this.mOffCalled = false;
                on(bool);
                return;
            }
            boolean bool = true;
            this.mThread.interrupt();
        }
        finally
        {
        }
    }

    public abstract void off();

    public abstract void on(boolean paramBoolean);

    private class T extends Thread
    {
        private T()
        {
        }

        public void run()
        {
            ResettableTimeout.this.mLock.open();
            while (true)
                try
                {
                    long l = ResettableTimeout.this.mOffAt - SystemClock.uptimeMillis();
                    if (l <= 0L)
                    {
                        ResettableTimeout.access$302(ResettableTimeout.this, true);
                        ResettableTimeout.this.off();
                        ResettableTimeout.access$402(ResettableTimeout.this, null);
                        return;
                    }
                    try
                    {
                        sleep(l);
                    }
                    catch (InterruptedException localInterruptedException)
                    {
                    }
                }
                finally
                {
                }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.server.ResettableTimeout
 * JD-Core Version:        0.6.2
 */