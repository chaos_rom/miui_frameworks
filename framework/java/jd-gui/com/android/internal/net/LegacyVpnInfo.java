package com.android.internal.net;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class LegacyVpnInfo
    implements Parcelable
{
    public static final Parcelable.Creator<LegacyVpnInfo> CREATOR = new Parcelable.Creator()
    {
        public LegacyVpnInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            LegacyVpnInfo localLegacyVpnInfo = new LegacyVpnInfo();
            localLegacyVpnInfo.key = paramAnonymousParcel.readString();
            localLegacyVpnInfo.state = paramAnonymousParcel.readInt();
            localLegacyVpnInfo.intent = ((PendingIntent)paramAnonymousParcel.readParcelable(null));
            return localLegacyVpnInfo;
        }

        public LegacyVpnInfo[] newArray(int paramAnonymousInt)
        {
            return new LegacyVpnInfo[paramAnonymousInt];
        }
    };
    public static final int STATE_CONNECTED = 3;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_FAILED = 5;
    public static final int STATE_INITIALIZING = 1;
    public static final int STATE_TIMEOUT = 4;
    public PendingIntent intent;
    public String key;
    public int state = -1;

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.key);
        paramParcel.writeInt(this.state);
        paramParcel.writeParcelable(this.intent, paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.net.LegacyVpnInfo
 * JD-Core Version:        0.6.2
 */