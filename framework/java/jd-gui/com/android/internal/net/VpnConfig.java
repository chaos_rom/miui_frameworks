package com.android.internal.net;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.List;

public class VpnConfig
    implements Parcelable
{
    public static final Parcelable.Creator<VpnConfig> CREATOR = new Parcelable.Creator()
    {
        public VpnConfig createFromParcel(Parcel paramAnonymousParcel)
        {
            VpnConfig localVpnConfig = new VpnConfig();
            localVpnConfig.user = paramAnonymousParcel.readString();
            localVpnConfig.interfaze = paramAnonymousParcel.readString();
            localVpnConfig.session = paramAnonymousParcel.readString();
            localVpnConfig.mtu = paramAnonymousParcel.readInt();
            localVpnConfig.addresses = paramAnonymousParcel.readString();
            localVpnConfig.routes = paramAnonymousParcel.readString();
            localVpnConfig.dnsServers = paramAnonymousParcel.createStringArrayList();
            localVpnConfig.searchDomains = paramAnonymousParcel.createStringArrayList();
            localVpnConfig.configureIntent = ((PendingIntent)paramAnonymousParcel.readParcelable(null));
            localVpnConfig.startTime = paramAnonymousParcel.readLong();
            return localVpnConfig;
        }

        public VpnConfig[] newArray(int paramAnonymousInt)
        {
            return new VpnConfig[paramAnonymousInt];
        }
    };
    public static final String DIALOGS_PACKAGE = "com.android.vpndialogs";
    public static final String LEGACY_VPN = "[Legacy VPN]";
    public static final String SERVICE_INTERFACE = "android.net.VpnService";
    public String addresses;
    public PendingIntent configureIntent;
    public List<String> dnsServers;
    public String interfaze;
    public int mtu = -1;
    public String routes;
    public List<String> searchDomains;
    public String session;
    public long startTime = -1L;
    public String user;

    public static Intent getIntentForConfirmation()
    {
        Intent localIntent = new Intent();
        localIntent.setClassName("com.android.vpndialogs", "com.android.vpndialogs.ConfirmDialog");
        return localIntent;
    }

    public static PendingIntent getIntentForStatusPanel(Context paramContext, VpnConfig paramVpnConfig)
    {
        Intent localIntent = new Intent();
        localIntent.setClassName("com.android.vpndialogs", "com.android.vpndialogs.ManageDialog");
        localIntent.putExtra("config", paramVpnConfig);
        localIntent.addFlags(1350565888);
        if (paramVpnConfig == null);
        for (int i = 536870912; ; i = 268435456)
            return PendingIntent.getActivity(paramContext, 0, localIntent, i);
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.user);
        paramParcel.writeString(this.interfaze);
        paramParcel.writeString(this.session);
        paramParcel.writeInt(this.mtu);
        paramParcel.writeString(this.addresses);
        paramParcel.writeString(this.routes);
        paramParcel.writeStringList(this.dnsServers);
        paramParcel.writeStringList(this.searchDomains);
        paramParcel.writeParcelable(this.configureIntent, paramInt);
        paramParcel.writeLong(this.startTime);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.net.VpnConfig
 * JD-Core Version:        0.6.2
 */