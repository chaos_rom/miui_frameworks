package com.android.internal.net;

import android.net.NetworkStats;
import java.io.File;

public class NetworkStatsFactory
{
    private static final String TAG = "NetworkStatsFactory";
    private final File mStatsXtIfaceAll;
    private final File mStatsXtIfaceFmt;
    private final File mStatsXtUid;

    public NetworkStatsFactory()
    {
        this(new File("/proc/"));
    }

    public NetworkStatsFactory(File paramFile)
    {
        this.mStatsXtIfaceAll = new File(paramFile, "net/xt_qtaguid/iface_stat_all");
        this.mStatsXtIfaceFmt = new File(paramFile, "net/xt_qtaguid/iface_stat_fmt");
        this.mStatsXtUid = new File(paramFile, "net/xt_qtaguid/stats");
    }

    public NetworkStats readNetworkStatsDetail()
    {
        return readNetworkStatsDetail(-1);
    }

    // ERROR //
    public NetworkStats readNetworkStatsDetail(int paramInt)
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: invokestatic 60	android/os/StrictMode:allowThreadDiskReads	()Landroid/os/StrictMode$ThreadPolicy;
        //     3: astore_2
        //     4: new 62	android/net/NetworkStats
        //     7: dup
        //     8: invokestatic 68	android/os/SystemClock:elapsedRealtime	()J
        //     11: bipush 24
        //     13: invokespecial 71	android/net/NetworkStats:<init>	(JI)V
        //     16: astore_3
        //     17: new 73	android/net/NetworkStats$Entry
        //     20: dup
        //     21: invokespecial 74	android/net/NetworkStats$Entry:<init>	()V
        //     24: astore 4
        //     26: iconst_1
        //     27: istore 5
        //     29: iconst_1
        //     30: istore 6
        //     32: aconst_null
        //     33: astore 7
        //     35: new 76	com/android/internal/util/ProcFileReader
        //     38: dup
        //     39: new 78	java/io/FileInputStream
        //     42: dup
        //     43: aload_0
        //     44: getfield 41	com/android/internal/net/NetworkStatsFactory:mStatsXtUid	Ljava/io/File;
        //     47: invokespecial 79	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     50: invokespecial 82	com/android/internal/util/ProcFileReader:<init>	(Ljava/io/InputStream;)V
        //     53: astore 8
        //     55: aload 8
        //     57: invokevirtual 85	com/android/internal/util/ProcFileReader:finishLine	()V
        //     60: aload 8
        //     62: invokevirtual 89	com/android/internal/util/ProcFileReader:hasMoreData	()Z
        //     65: ifeq +260 -> 325
        //     68: aload 8
        //     70: invokevirtual 93	com/android/internal/util/ProcFileReader:nextInt	()I
        //     73: istore 5
        //     75: iload 5
        //     77: iload 6
        //     79: iconst_1
        //     80: iadd
        //     81: if_icmpeq +91 -> 172
        //     84: new 48	java/lang/IllegalStateException
        //     87: dup
        //     88: new 95	java/lang/StringBuilder
        //     91: dup
        //     92: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     95: ldc 98
        //     97: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     100: iload 5
        //     102: invokevirtual 105	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     105: ldc 107
        //     107: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     110: iload 6
        //     112: invokevirtual 105	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     115: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     118: invokespecial 112	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     121: athrow
        //     122: astore 12
        //     124: aload 8
        //     126: astore 7
        //     128: new 48	java/lang/IllegalStateException
        //     131: dup
        //     132: new 95	java/lang/StringBuilder
        //     135: dup
        //     136: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     139: ldc 114
        //     141: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     144: iload 5
        //     146: invokevirtual 105	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     149: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     152: aload 12
        //     154: invokespecial 117	java/lang/IllegalStateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     157: athrow
        //     158: astore 10
        //     160: aload 7
        //     162: invokestatic 123	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     165: aload_2
        //     166: invokestatic 127	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     169: aload 10
        //     171: athrow
        //     172: iload 5
        //     174: istore 6
        //     176: aload 4
        //     178: aload 8
        //     180: invokevirtual 130	com/android/internal/util/ProcFileReader:nextString	()Ljava/lang/String;
        //     183: putfield 133	android/net/NetworkStats$Entry:iface	Ljava/lang/String;
        //     186: aload 4
        //     188: aload 8
        //     190: invokevirtual 130	com/android/internal/util/ProcFileReader:nextString	()Ljava/lang/String;
        //     193: invokestatic 139	com/android/server/NetworkManagementSocketTagger:kernelToTag	(Ljava/lang/String;)I
        //     196: putfield 143	android/net/NetworkStats$Entry:tag	I
        //     199: aload 4
        //     201: aload 8
        //     203: invokevirtual 93	com/android/internal/util/ProcFileReader:nextInt	()I
        //     206: putfield 146	android/net/NetworkStats$Entry:uid	I
        //     209: aload 4
        //     211: aload 8
        //     213: invokevirtual 93	com/android/internal/util/ProcFileReader:nextInt	()I
        //     216: putfield 149	android/net/NetworkStats$Entry:set	I
        //     219: aload 4
        //     221: aload 8
        //     223: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     226: putfield 156	android/net/NetworkStats$Entry:rxBytes	J
        //     229: aload 4
        //     231: aload 8
        //     233: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     236: putfield 159	android/net/NetworkStats$Entry:rxPackets	J
        //     239: aload 4
        //     241: aload 8
        //     243: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     246: putfield 162	android/net/NetworkStats$Entry:txBytes	J
        //     249: aload 4
        //     251: aload 8
        //     253: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     256: putfield 165	android/net/NetworkStats$Entry:txPackets	J
        //     259: iload_1
        //     260: bipush 255
        //     262: if_icmpeq +12 -> 274
        //     265: iload_1
        //     266: aload 4
        //     268: getfield 146	android/net/NetworkStats$Entry:uid	I
        //     271: if_icmpne +10 -> 281
        //     274: aload_3
        //     275: aload 4
        //     277: invokevirtual 169	android/net/NetworkStats:addValues	(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;
        //     280: pop
        //     281: aload 8
        //     283: invokevirtual 85	com/android/internal/util/ProcFileReader:finishLine	()V
        //     286: goto -226 -> 60
        //     289: astore 11
        //     291: aload 8
        //     293: astore 7
        //     295: new 48	java/lang/IllegalStateException
        //     298: dup
        //     299: new 95	java/lang/StringBuilder
        //     302: dup
        //     303: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     306: ldc 114
        //     308: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     311: iload 5
        //     313: invokevirtual 105	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     316: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     319: aload 11
        //     321: invokespecial 117	java/lang/IllegalStateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     324: athrow
        //     325: aload 8
        //     327: invokestatic 123	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     330: aload_2
        //     331: invokestatic 127	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     334: aload_3
        //     335: areturn
        //     336: astore 9
        //     338: new 48	java/lang/IllegalStateException
        //     341: dup
        //     342: new 95	java/lang/StringBuilder
        //     345: dup
        //     346: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     349: ldc 114
        //     351: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     354: iload 5
        //     356: invokevirtual 105	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     359: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     362: aload 9
        //     364: invokespecial 117	java/lang/IllegalStateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     367: athrow
        //     368: astore 10
        //     370: aload 8
        //     372: astore 7
        //     374: goto -214 -> 160
        //     377: astore 9
        //     379: aload 8
        //     381: astore 7
        //     383: goto -45 -> 338
        //     386: astore 11
        //     388: goto -93 -> 295
        //     391: astore 12
        //     393: goto -265 -> 128
        //
        // Exception table:
        //     from	to	target	type
        //     55	122	122	java/lang/NullPointerException
        //     176	286	122	java/lang/NullPointerException
        //     35	55	158	finally
        //     128	158	158	finally
        //     295	325	158	finally
        //     338	368	158	finally
        //     55	122	289	java/lang/NumberFormatException
        //     176	286	289	java/lang/NumberFormatException
        //     35	55	336	java/io/IOException
        //     55	122	368	finally
        //     176	286	368	finally
        //     55	122	377	java/io/IOException
        //     176	286	377	java/io/IOException
        //     35	55	386	java/lang/NumberFormatException
        //     35	55	391	java/lang/NullPointerException
    }

    // ERROR //
    public NetworkStats readNetworkStatsSummaryDev()
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: invokestatic 60	android/os/StrictMode:allowThreadDiskReads	()Landroid/os/StrictMode$ThreadPolicy;
        //     3: astore_1
        //     4: new 62	android/net/NetworkStats
        //     7: dup
        //     8: invokestatic 68	android/os/SystemClock:elapsedRealtime	()J
        //     11: bipush 6
        //     13: invokespecial 71	android/net/NetworkStats:<init>	(JI)V
        //     16: astore_2
        //     17: new 73	android/net/NetworkStats$Entry
        //     20: dup
        //     21: invokespecial 74	android/net/NetworkStats$Entry:<init>	()V
        //     24: astore_3
        //     25: aconst_null
        //     26: astore 4
        //     28: new 76	com/android/internal/util/ProcFileReader
        //     31: dup
        //     32: new 78	java/io/FileInputStream
        //     35: dup
        //     36: aload_0
        //     37: getfield 33	com/android/internal/net/NetworkStatsFactory:mStatsXtIfaceAll	Ljava/io/File;
        //     40: invokespecial 79	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     43: invokespecial 82	com/android/internal/util/ProcFileReader:<init>	(Ljava/io/InputStream;)V
        //     46: astore 5
        //     48: aload 5
        //     50: invokevirtual 89	com/android/internal/util/ProcFileReader:hasMoreData	()Z
        //     53: ifeq +205 -> 258
        //     56: aload_3
        //     57: aload 5
        //     59: invokevirtual 130	com/android/internal/util/ProcFileReader:nextString	()Ljava/lang/String;
        //     62: putfield 133	android/net/NetworkStats$Entry:iface	Ljava/lang/String;
        //     65: aload_3
        //     66: bipush 255
        //     68: putfield 146	android/net/NetworkStats$Entry:uid	I
        //     71: aload_3
        //     72: bipush 255
        //     74: putfield 149	android/net/NetworkStats$Entry:set	I
        //     77: aload_3
        //     78: iconst_0
        //     79: putfield 143	android/net/NetworkStats$Entry:tag	I
        //     82: aload 5
        //     84: invokevirtual 93	com/android/internal/util/ProcFileReader:nextInt	()I
        //     87: ifeq +165 -> 252
        //     90: iconst_1
        //     91: istore 10
        //     93: aload_3
        //     94: aload 5
        //     96: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     99: putfield 156	android/net/NetworkStats$Entry:rxBytes	J
        //     102: aload_3
        //     103: aload 5
        //     105: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     108: putfield 159	android/net/NetworkStats$Entry:rxPackets	J
        //     111: aload_3
        //     112: aload 5
        //     114: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     117: putfield 162	android/net/NetworkStats$Entry:txBytes	J
        //     120: aload_3
        //     121: aload 5
        //     123: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     126: putfield 165	android/net/NetworkStats$Entry:txPackets	J
        //     129: iload 10
        //     131: ifeq +59 -> 190
        //     134: aload_3
        //     135: aload_3
        //     136: getfield 156	android/net/NetworkStats$Entry:rxBytes	J
        //     139: aload 5
        //     141: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     144: ladd
        //     145: putfield 156	android/net/NetworkStats$Entry:rxBytes	J
        //     148: aload_3
        //     149: aload_3
        //     150: getfield 159	android/net/NetworkStats$Entry:rxPackets	J
        //     153: aload 5
        //     155: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     158: ladd
        //     159: putfield 159	android/net/NetworkStats$Entry:rxPackets	J
        //     162: aload_3
        //     163: aload_3
        //     164: getfield 162	android/net/NetworkStats$Entry:txBytes	J
        //     167: aload 5
        //     169: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     172: ladd
        //     173: putfield 162	android/net/NetworkStats$Entry:txBytes	J
        //     176: aload_3
        //     177: aload_3
        //     178: getfield 165	android/net/NetworkStats$Entry:txPackets	J
        //     181: aload 5
        //     183: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     186: ladd
        //     187: putfield 165	android/net/NetworkStats$Entry:txPackets	J
        //     190: aload_2
        //     191: aload_3
        //     192: invokevirtual 169	android/net/NetworkStats:addValues	(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;
        //     195: pop
        //     196: aload 5
        //     198: invokevirtual 85	com/android/internal/util/ProcFileReader:finishLine	()V
        //     201: goto -153 -> 48
        //     204: astore 9
        //     206: aload 5
        //     208: astore 4
        //     210: new 48	java/lang/IllegalStateException
        //     213: dup
        //     214: new 95	java/lang/StringBuilder
        //     217: dup
        //     218: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     221: ldc 172
        //     223: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     226: aload 9
        //     228: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     231: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     234: invokespecial 112	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     237: athrow
        //     238: astore 7
        //     240: aload 4
        //     242: invokestatic 123	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     245: aload_1
        //     246: invokestatic 127	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     249: aload 7
        //     251: athrow
        //     252: iconst_0
        //     253: istore 10
        //     255: goto -162 -> 93
        //     258: aload 5
        //     260: invokestatic 123	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     263: aload_1
        //     264: invokestatic 127	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     267: aload_2
        //     268: areturn
        //     269: astore 6
        //     271: new 48	java/lang/IllegalStateException
        //     274: dup
        //     275: new 95	java/lang/StringBuilder
        //     278: dup
        //     279: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     282: ldc 172
        //     284: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     287: aload 6
        //     289: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     292: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     295: invokespecial 112	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     298: athrow
        //     299: new 48	java/lang/IllegalStateException
        //     302: dup
        //     303: new 95	java/lang/StringBuilder
        //     306: dup
        //     307: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     310: ldc 172
        //     312: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     315: aload 8
        //     317: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     320: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     323: invokespecial 112	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     326: athrow
        //     327: astore 7
        //     329: aload 5
        //     331: astore 4
        //     333: goto -93 -> 240
        //     336: astore 8
        //     338: aload 5
        //     340: astore 4
        //     342: goto -43 -> 299
        //     345: astore 6
        //     347: aload 5
        //     349: astore 4
        //     351: goto -80 -> 271
        //     354: astore 9
        //     356: goto -146 -> 210
        //     359: astore 8
        //     361: goto -62 -> 299
        //
        // Exception table:
        //     from	to	target	type
        //     48	201	204	java/lang/NullPointerException
        //     28	48	238	finally
        //     210	238	238	finally
        //     271	327	238	finally
        //     28	48	269	java/lang/NumberFormatException
        //     48	201	327	finally
        //     48	201	336	java/io/IOException
        //     48	201	345	java/lang/NumberFormatException
        //     28	48	354	java/lang/NullPointerException
        //     28	48	359	java/io/IOException
    }

    // ERROR //
    public NetworkStats readNetworkStatsSummaryXt()
        throws java.lang.IllegalStateException
    {
        // Byte code:
        //     0: invokestatic 60	android/os/StrictMode:allowThreadDiskReads	()Landroid/os/StrictMode$ThreadPolicy;
        //     3: astore_1
        //     4: aload_0
        //     5: getfield 37	com/android/internal/net/NetworkStatsFactory:mStatsXtIfaceFmt	Ljava/io/File;
        //     8: invokevirtual 179	java/io/File:exists	()Z
        //     11: ifne +7 -> 18
        //     14: aconst_null
        //     15: astore_2
        //     16: aload_2
        //     17: areturn
        //     18: new 62	android/net/NetworkStats
        //     21: dup
        //     22: invokestatic 68	android/os/SystemClock:elapsedRealtime	()J
        //     25: bipush 6
        //     27: invokespecial 71	android/net/NetworkStats:<init>	(JI)V
        //     30: astore_2
        //     31: new 73	android/net/NetworkStats$Entry
        //     34: dup
        //     35: invokespecial 74	android/net/NetworkStats$Entry:<init>	()V
        //     38: astore_3
        //     39: aconst_null
        //     40: astore 4
        //     42: new 76	com/android/internal/util/ProcFileReader
        //     45: dup
        //     46: new 78	java/io/FileInputStream
        //     49: dup
        //     50: aload_0
        //     51: getfield 37	com/android/internal/net/NetworkStatsFactory:mStatsXtIfaceFmt	Ljava/io/File;
        //     54: invokespecial 79	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     57: invokespecial 82	com/android/internal/util/ProcFileReader:<init>	(Ljava/io/InputStream;)V
        //     60: astore 5
        //     62: aload 5
        //     64: invokevirtual 85	com/android/internal/util/ProcFileReader:finishLine	()V
        //     67: aload 5
        //     69: invokevirtual 89	com/android/internal/util/ProcFileReader:hasMoreData	()Z
        //     72: ifeq +127 -> 199
        //     75: aload_3
        //     76: aload 5
        //     78: invokevirtual 130	com/android/internal/util/ProcFileReader:nextString	()Ljava/lang/String;
        //     81: putfield 133	android/net/NetworkStats$Entry:iface	Ljava/lang/String;
        //     84: aload_3
        //     85: bipush 255
        //     87: putfield 146	android/net/NetworkStats$Entry:uid	I
        //     90: aload_3
        //     91: bipush 255
        //     93: putfield 149	android/net/NetworkStats$Entry:set	I
        //     96: aload_3
        //     97: iconst_0
        //     98: putfield 143	android/net/NetworkStats$Entry:tag	I
        //     101: aload_3
        //     102: aload 5
        //     104: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     107: putfield 156	android/net/NetworkStats$Entry:rxBytes	J
        //     110: aload_3
        //     111: aload 5
        //     113: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     116: putfield 159	android/net/NetworkStats$Entry:rxPackets	J
        //     119: aload_3
        //     120: aload 5
        //     122: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     125: putfield 162	android/net/NetworkStats$Entry:txBytes	J
        //     128: aload_3
        //     129: aload 5
        //     131: invokevirtual 152	com/android/internal/util/ProcFileReader:nextLong	()J
        //     134: putfield 165	android/net/NetworkStats$Entry:txPackets	J
        //     137: aload_2
        //     138: aload_3
        //     139: invokevirtual 169	android/net/NetworkStats:addValues	(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;
        //     142: pop
        //     143: aload 5
        //     145: invokevirtual 85	com/android/internal/util/ProcFileReader:finishLine	()V
        //     148: goto -81 -> 67
        //     151: astore 9
        //     153: aload 5
        //     155: astore 4
        //     157: new 48	java/lang/IllegalStateException
        //     160: dup
        //     161: new 95	java/lang/StringBuilder
        //     164: dup
        //     165: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     168: ldc 172
        //     170: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     173: aload 9
        //     175: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     178: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     181: invokespecial 112	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     184: athrow
        //     185: astore 7
        //     187: aload 4
        //     189: invokestatic 123	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     192: aload_1
        //     193: invokestatic 127	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     196: aload 7
        //     198: athrow
        //     199: aload 5
        //     201: invokestatic 123	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     204: aload_1
        //     205: invokestatic 127	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     208: goto -192 -> 16
        //     211: astore 6
        //     213: new 48	java/lang/IllegalStateException
        //     216: dup
        //     217: new 95	java/lang/StringBuilder
        //     220: dup
        //     221: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     224: ldc 172
        //     226: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     229: aload 6
        //     231: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     234: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     237: invokespecial 112	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     240: athrow
        //     241: new 48	java/lang/IllegalStateException
        //     244: dup
        //     245: new 95	java/lang/StringBuilder
        //     248: dup
        //     249: invokespecial 96	java/lang/StringBuilder:<init>	()V
        //     252: ldc 172
        //     254: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     257: aload 8
        //     259: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     262: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     265: invokespecial 112	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     268: athrow
        //     269: astore 7
        //     271: aload 5
        //     273: astore 4
        //     275: goto -88 -> 187
        //     278: astore 8
        //     280: aload 5
        //     282: astore 4
        //     284: goto -43 -> 241
        //     287: astore 6
        //     289: aload 5
        //     291: astore 4
        //     293: goto -80 -> 213
        //     296: astore 9
        //     298: goto -141 -> 157
        //     301: astore 8
        //     303: goto -62 -> 241
        //
        // Exception table:
        //     from	to	target	type
        //     62	148	151	java/lang/NullPointerException
        //     42	62	185	finally
        //     157	185	185	finally
        //     213	269	185	finally
        //     42	62	211	java/lang/NumberFormatException
        //     62	148	269	finally
        //     62	148	278	java/io/IOException
        //     62	148	287	java/lang/NumberFormatException
        //     42	62	296	java/lang/NullPointerException
        //     42	62	301	java/io/IOException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.net.NetworkStatsFactory
 * JD-Core Version:        0.6.2
 */