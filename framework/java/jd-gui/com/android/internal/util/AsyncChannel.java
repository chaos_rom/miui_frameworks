package com.android.internal.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Slog;
import java.util.Stack;

public class AsyncChannel
{
    private static final int BASE = 69632;
    public static final int CMD_CHANNEL_DISCONNECT = 69635;
    public static final int CMD_CHANNEL_DISCONNECTED = 69636;
    public static final int CMD_CHANNEL_FULLY_CONNECTED = 69634;
    public static final int CMD_CHANNEL_FULL_CONNECTION = 69633;
    public static final int CMD_CHANNEL_HALF_CONNECTED = 69632;
    private static final int CMD_TO_STRING_COUNT = 5;
    private static final boolean DBG = false;
    public static final int STATUS_BINDING_UNSUCCESSFUL = 1;
    public static final int STATUS_FULL_CONNECTION_REFUSED_ALREADY_CONNECTED = 3;
    public static final int STATUS_SEND_UNSUCCESSFUL = 2;
    public static final int STATUS_SUCCESSFUL = 0;
    private static final String TAG = "AsyncChannel";
    private static String[] sCmdToString = new String[5];
    private AsyncChannelConnection mConnection;
    private Messenger mDstMessenger;
    private Context mSrcContext;
    private Handler mSrcHandler;
    private Messenger mSrcMessenger;

    static
    {
        sCmdToString[0] = "CMD_CHANNEL_HALF_CONNECTED";
        sCmdToString[1] = "CMD_CHANNEL_FULL_CONNECTION";
        sCmdToString[2] = "CMD_CHANNEL_FULLY_CONNECTED";
        sCmdToString[3] = "CMD_CHANNEL_DISCONNECT";
        sCmdToString[4] = "CMD_CHANNEL_DISCONNECTED";
    }

    protected static String cmdToString(int paramInt)
    {
        int i = paramInt - 69632;
        if ((i >= 0) && (i < sCmdToString.length));
        for (String str = sCmdToString[i]; ; str = null)
            return str;
    }

    private static void log(String paramString)
    {
        Slog.d("AsyncChannel", paramString);
    }

    private void replyDisconnected(int paramInt)
    {
        Message localMessage = this.mSrcHandler.obtainMessage(69636);
        localMessage.arg1 = paramInt;
        localMessage.obj = this;
        localMessage.replyTo = this.mDstMessenger;
        this.mSrcHandler.sendMessage(localMessage);
    }

    private void replyHalfConnected(int paramInt)
    {
        Message localMessage = this.mSrcHandler.obtainMessage(69632);
        localMessage.arg1 = paramInt;
        localMessage.obj = this;
        localMessage.replyTo = this.mDstMessenger;
        this.mSrcHandler.sendMessage(localMessage);
    }

    public void connect(Context paramContext, Handler paramHandler1, Handler paramHandler2)
    {
        connect(paramContext, paramHandler1, new Messenger(paramHandler2));
    }

    public void connect(Context paramContext, Handler paramHandler, Messenger paramMessenger)
    {
        connected(paramContext, paramHandler, paramMessenger);
        replyHalfConnected(0);
    }

    public void connect(Context paramContext, Handler paramHandler, Class<?> paramClass)
    {
        connect(paramContext, paramHandler, paramClass.getPackage().getName(), paramClass.getName());
    }

    public void connect(Context paramContext, Handler paramHandler, String paramString1, String paramString2)
    {
        new Thread(new AsyncChannel.1ConnectAsync(this, paramContext, paramHandler, paramString1, paramString2)).start();
    }

    public void connect(AsyncService paramAsyncService, Messenger paramMessenger)
    {
        connect(paramAsyncService, paramAsyncService.getHandler(), paramMessenger);
    }

    public int connectSrcHandlerToPackageSync(Context paramContext, Handler paramHandler, String paramString1, String paramString2)
    {
        int i = 1;
        this.mConnection = new AsyncChannelConnection();
        this.mSrcContext = paramContext;
        this.mSrcHandler = paramHandler;
        this.mSrcMessenger = new Messenger(paramHandler);
        this.mDstMessenger = null;
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.setClassName(paramString1, paramString2);
        if (paramContext.bindService(localIntent, this.mConnection, i))
            i = 0;
        return i;
    }

    public int connectSync(Context paramContext, Handler paramHandler1, Handler paramHandler2)
    {
        return connectSync(paramContext, paramHandler1, new Messenger(paramHandler2));
    }

    public int connectSync(Context paramContext, Handler paramHandler, Messenger paramMessenger)
    {
        connected(paramContext, paramHandler, paramMessenger);
        return 0;
    }

    public void connected(Context paramContext, Handler paramHandler, Messenger paramMessenger)
    {
        this.mSrcContext = paramContext;
        this.mSrcHandler = paramHandler;
        this.mSrcMessenger = new Messenger(this.mSrcHandler);
        this.mDstMessenger = paramMessenger;
    }

    public void disconnect()
    {
        if ((this.mConnection != null) && (this.mSrcContext != null))
            this.mSrcContext.unbindService(this.mConnection);
        if (this.mSrcHandler != null)
            replyDisconnected(0);
    }

    public void disconnected()
    {
        this.mSrcContext = null;
        this.mSrcHandler = null;
        this.mSrcMessenger = null;
        this.mDstMessenger = null;
        this.mConnection = null;
    }

    public int fullyConnectSync(Context paramContext, Handler paramHandler1, Handler paramHandler2)
    {
        int i = connectSync(paramContext, paramHandler1, paramHandler2);
        if (i == 0)
            i = sendMessageSynchronously(69633).arg1;
        return i;
    }

    public void replyToMessage(Message paramMessage, int paramInt)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt;
        replyToMessage(paramMessage, localMessage);
    }

    public void replyToMessage(Message paramMessage, int paramInt1, int paramInt2)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        replyToMessage(paramMessage, localMessage);
    }

    public void replyToMessage(Message paramMessage, int paramInt1, int paramInt2, int paramInt3)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        localMessage.arg2 = paramInt3;
        replyToMessage(paramMessage, localMessage);
    }

    public void replyToMessage(Message paramMessage, int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        localMessage.arg2 = paramInt3;
        localMessage.obj = paramObject;
        replyToMessage(paramMessage, localMessage);
    }

    public void replyToMessage(Message paramMessage, int paramInt, Object paramObject)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt;
        localMessage.obj = paramObject;
        replyToMessage(paramMessage, localMessage);
    }

    public void replyToMessage(Message paramMessage1, Message paramMessage2)
    {
        try
        {
            paramMessage2.replyTo = this.mSrcMessenger;
            paramMessage1.replyTo.send(paramMessage2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                log("TODO: handle replyToMessage RemoteException" + localRemoteException);
                localRemoteException.printStackTrace();
            }
        }
    }

    public void sendMessage(int paramInt)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt;
        sendMessage(localMessage);
    }

    public void sendMessage(int paramInt1, int paramInt2)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        sendMessage(localMessage);
    }

    public void sendMessage(int paramInt1, int paramInt2, int paramInt3)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        localMessage.arg2 = paramInt3;
        sendMessage(localMessage);
    }

    public void sendMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        localMessage.arg2 = paramInt3;
        localMessage.obj = paramObject;
        sendMessage(localMessage);
    }

    public void sendMessage(int paramInt, Object paramObject)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt;
        localMessage.obj = paramObject;
        sendMessage(localMessage);
    }

    public void sendMessage(Message paramMessage)
    {
        paramMessage.replyTo = this.mSrcMessenger;
        try
        {
            this.mDstMessenger.send(paramMessage);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                replyDisconnected(2);
        }
    }

    public Message sendMessageSynchronously(int paramInt)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt;
        return sendMessageSynchronously(localMessage);
    }

    public Message sendMessageSynchronously(int paramInt1, int paramInt2)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        return sendMessageSynchronously(localMessage);
    }

    public Message sendMessageSynchronously(int paramInt1, int paramInt2, int paramInt3)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        localMessage.arg2 = paramInt3;
        return sendMessageSynchronously(localMessage);
    }

    public Message sendMessageSynchronously(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt1;
        localMessage.arg1 = paramInt2;
        localMessage.arg2 = paramInt3;
        localMessage.obj = paramObject;
        return sendMessageSynchronously(localMessage);
    }

    public Message sendMessageSynchronously(int paramInt, Object paramObject)
    {
        Message localMessage = Message.obtain();
        localMessage.what = paramInt;
        localMessage.obj = paramObject;
        return sendMessageSynchronously(localMessage);
    }

    public Message sendMessageSynchronously(Message paramMessage)
    {
        return SyncMessenger.sendMessageSynchronously(this.mDstMessenger, paramMessage);
    }

    class AsyncChannelConnection
        implements ServiceConnection
    {
        AsyncChannelConnection()
        {
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            AsyncChannel.access$502(AsyncChannel.this, new Messenger(paramIBinder));
            AsyncChannel.this.replyHalfConnected(0);
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            AsyncChannel.this.replyDisconnected(0);
        }
    }

    private static class SyncMessenger
    {
        private static int sCount = 0;
        private static Stack<SyncMessenger> sStack = new Stack();
        private SyncHandler mHandler;
        private HandlerThread mHandlerThread;
        private Messenger mMessenger;

        private static SyncMessenger obtain()
        {
            synchronized (sStack)
            {
                if (sStack.isEmpty())
                {
                    localSyncMessenger = new SyncMessenger();
                    StringBuilder localStringBuilder = new StringBuilder().append("SyncHandler-");
                    int i = sCount;
                    sCount = i + 1;
                    localSyncMessenger.mHandlerThread = new HandlerThread(i);
                    localSyncMessenger.mHandlerThread.start();
                    localSyncMessenger.getClass();
                    localSyncMessenger.mHandler = new SyncHandler(localSyncMessenger.mHandlerThread.getLooper(), null);
                    localSyncMessenger.mMessenger = new Messenger(localSyncMessenger.mHandler);
                    return localSyncMessenger;
                }
                SyncMessenger localSyncMessenger = (SyncMessenger)sStack.pop();
            }
        }

        private void recycle()
        {
            synchronized (sStack)
            {
                sStack.push(this);
                return;
            }
        }

        private static Message sendMessageSynchronously(Messenger paramMessenger, Message paramMessage)
        {
            SyncMessenger localSyncMessenger = obtain();
            if ((paramMessenger != null) && (paramMessage != null));
            try
            {
                paramMessage.replyTo = localSyncMessenger.mMessenger;
                synchronized (localSyncMessenger.mHandler.mLockObject)
                {
                    paramMessenger.send(paramMessage);
                    localSyncMessenger.mHandler.mLockObject.wait();
                    Message localMessage = localSyncMessenger.mHandler.mResultMsg;
                    localSyncMessenger.recycle();
                    return localMessage;
                }
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                {
                    SyncHandler.access$402(localSyncMessenger.mHandler, null);
                    continue;
                    SyncHandler.access$402(localSyncMessenger.mHandler, null);
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    SyncHandler.access$402(localSyncMessenger.mHandler, null);
            }
        }

        private class SyncHandler extends Handler
        {
            private Object mLockObject = new Object();
            private Message mResultMsg;

            private SyncHandler(Looper arg2)
            {
                super();
            }

            public void handleMessage(Message paramMessage)
            {
                this.mResultMsg = Message.obtain();
                this.mResultMsg.copyFrom(paramMessage);
                synchronized (this.mLockObject)
                {
                    this.mLockObject.notify();
                    return;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.AsyncChannel
 * JD-Core Version:        0.6.2
 */