package com.android.internal.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TypedProperties extends HashMap<String, Object>
{
    static final String NULL_STRING = new String("<TypedProperties:NULL_STRING>");
    public static final int STRING_NOT_SET = -1;
    public static final int STRING_NULL = 0;
    public static final int STRING_SET = 1;
    public static final int STRING_TYPE_MISMATCH = -2;
    static final int TYPE_BOOLEAN = 90;
    static final int TYPE_BYTE = 329;
    static final int TYPE_DOUBLE = 2118;
    static final int TYPE_ERROR = -1;
    static final int TYPE_FLOAT = 1094;
    static final int TYPE_INT = 1097;
    static final int TYPE_LONG = 2121;
    static final int TYPE_SHORT = 585;
    static final int TYPE_STRING = 29516;
    static final int TYPE_UNSET = 120;

    static StreamTokenizer initTokenizer(Reader paramReader)
    {
        StreamTokenizer localStreamTokenizer = new StreamTokenizer(paramReader);
        localStreamTokenizer.resetSyntax();
        localStreamTokenizer.wordChars(48, 57);
        localStreamTokenizer.wordChars(65, 90);
        localStreamTokenizer.wordChars(97, 122);
        localStreamTokenizer.wordChars(95, 95);
        localStreamTokenizer.wordChars(36, 36);
        localStreamTokenizer.wordChars(46, 46);
        localStreamTokenizer.wordChars(45, 45);
        localStreamTokenizer.wordChars(43, 43);
        localStreamTokenizer.ordinaryChar(61);
        localStreamTokenizer.whitespaceChars(32, 32);
        localStreamTokenizer.whitespaceChars(9, 9);
        localStreamTokenizer.whitespaceChars(10, 10);
        localStreamTokenizer.whitespaceChars(13, 13);
        localStreamTokenizer.quoteChar(34);
        localStreamTokenizer.slashStarComments(true);
        localStreamTokenizer.slashSlashComments(true);
        return localStreamTokenizer;
    }

    static int interpretType(String paramString)
    {
        int i;
        if ("unset".equals(paramString))
            i = 120;
        while (true)
        {
            return i;
            if ("boolean".equals(paramString))
                i = 90;
            else if ("byte".equals(paramString))
                i = 329;
            else if ("short".equals(paramString))
                i = 585;
            else if ("int".equals(paramString))
                i = 1097;
            else if ("long".equals(paramString))
                i = 2121;
            else if ("float".equals(paramString))
                i = 1094;
            else if ("double".equals(paramString))
                i = 2118;
            else if ("String".equals(paramString))
                i = 29516;
            else
                i = -1;
        }
    }

    static void parse(Reader paramReader, Map<String, Object> paramMap)
        throws TypedProperties.ParseException, IOException
    {
        StreamTokenizer localStreamTokenizer = initTokenizer(paramReader);
        Pattern localPattern = Pattern.compile("([a-zA-Z_$][0-9a-zA-Z_$]*\\.)*[a-zA-Z_$][0-9a-zA-Z_$]*");
        while (true)
        {
            int i = localStreamTokenizer.nextToken();
            if (i == -1)
                return;
            if (i != -3)
                throw new ParseException(localStreamTokenizer, "type name");
            int j = interpretType(localStreamTokenizer.sval);
            if (j == -1)
                throw new ParseException(localStreamTokenizer, "valid type name");
            localStreamTokenizer.sval = null;
            if ((j == 120) && (localStreamTokenizer.nextToken() != 40))
                throw new ParseException(localStreamTokenizer, "'('");
            if (localStreamTokenizer.nextToken() != -3)
                throw new ParseException(localStreamTokenizer, "property name");
            String str = localStreamTokenizer.sval;
            if (!localPattern.matcher(str).matches())
                throw new ParseException(localStreamTokenizer, "valid property name");
            localStreamTokenizer.sval = null;
            if (j == 120)
            {
                if (localStreamTokenizer.nextToken() != 41)
                    throw new ParseException(localStreamTokenizer, "')'");
                paramMap.remove(str);
            }
            while (localStreamTokenizer.nextToken() != 59)
            {
                throw new ParseException(localStreamTokenizer, "';'");
                if (localStreamTokenizer.nextToken() != 61)
                    throw new ParseException(localStreamTokenizer, "'='");
                Object localObject1 = parseValue(localStreamTokenizer, j);
                Object localObject2 = paramMap.remove(str);
                if ((localObject2 != null) && (localObject1.getClass() != localObject2.getClass()))
                    throw new ParseException(localStreamTokenizer, "(property previously declared as a different type)");
                paramMap.put(str, localObject1);
            }
        }
    }

    static Object parseValue(StreamTokenizer paramStreamTokenizer, int paramInt)
        throws IOException
    {
        int i = paramStreamTokenizer.nextToken();
        Object localObject;
        if (paramInt == 90)
        {
            if (i != -3)
                throw new ParseException(paramStreamTokenizer, "boolean constant");
            if ("true".equals(paramStreamTokenizer.sval))
                localObject = Boolean.TRUE;
        }
        while (true)
        {
            return localObject;
            if ("false".equals(paramStreamTokenizer.sval))
            {
                localObject = Boolean.FALSE;
            }
            else
            {
                throw new ParseException(paramStreamTokenizer, "boolean constant");
                if ((paramInt & 0xFF) == 73)
                {
                    if (i != -3)
                        throw new ParseException(paramStreamTokenizer, "integer constant");
                    long l;
                    try
                    {
                        l = Long.decode(paramStreamTokenizer.sval).longValue();
                        int j = 0xFF & paramInt >> 8;
                        switch (j)
                        {
                        case 3:
                        case 5:
                        case 6:
                        case 7:
                        default:
                            throw new IllegalStateException("Internal error; unexpected integer type width " + j);
                        case 1:
                        case 2:
                        case 4:
                        case 8:
                        }
                    }
                    catch (NumberFormatException localNumberFormatException2)
                    {
                        throw new ParseException(paramStreamTokenizer, "integer constant");
                    }
                    if ((l < -128L) || (l > 127L))
                        throw new ParseException(paramStreamTokenizer, "8-bit integer constant");
                    localObject = new Byte((byte)(int)l);
                    continue;
                    if ((l < -32768L) || (l > 32767L))
                        throw new ParseException(paramStreamTokenizer, "16-bit integer constant");
                    localObject = new Short((short)(int)l);
                    continue;
                    if ((l < -2147483648L) || (l > 2147483647L))
                        throw new ParseException(paramStreamTokenizer, "32-bit integer constant");
                    localObject = new Integer((int)l);
                    continue;
                    if ((l < -9223372036854775808L) || (l > 9223372036854775807L))
                        throw new ParseException(paramStreamTokenizer, "64-bit integer constant");
                    localObject = new Long(l);
                }
                else if ((paramInt & 0xFF) == 70)
                {
                    if (i != -3)
                        throw new ParseException(paramStreamTokenizer, "float constant");
                    double d1;
                    try
                    {
                        d1 = Double.parseDouble(paramStreamTokenizer.sval);
                        if ((0xFF & paramInt >> 8) != 4)
                            break label525;
                        double d2 = Math.abs(d1);
                        if ((d2 != 0.0D) && (!Double.isInfinite(d1)) && (!Double.isNaN(d1)) && ((d2 < 1.401298464324817E-45D) || (d2 > 3.402823466385289E+38D)))
                            throw new ParseException(paramStreamTokenizer, "32-bit float constant");
                    }
                    catch (NumberFormatException localNumberFormatException1)
                    {
                        throw new ParseException(paramStreamTokenizer, "float constant");
                    }
                    localObject = new Float((float)d1);
                    continue;
                    label525: localObject = new Double(d1);
                }
                else
                {
                    if (paramInt != 29516)
                        break label597;
                    if (i == 34)
                    {
                        localObject = paramStreamTokenizer.sval;
                    }
                    else
                    {
                        if ((i != -3) || (!"null".equals(paramStreamTokenizer.sval)))
                            break;
                        localObject = NULL_STRING;
                    }
                }
            }
        }
        throw new ParseException(paramStreamTokenizer, "double-quoted string or 'null'");
        label597: throw new IllegalStateException("Internal error; unknown type " + paramInt);
    }

    public Object get(Object paramObject)
    {
        Object localObject = super.get(paramObject);
        if (localObject == NULL_STRING)
            localObject = null;
        return localObject;
    }

    public boolean getBoolean(String paramString)
    {
        return getBoolean(paramString, false);
    }

    public boolean getBoolean(String paramString, boolean paramBoolean)
    {
        Object localObject = super.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramBoolean;
            if (!(localObject instanceof Boolean))
                break;
            paramBoolean = ((Boolean)localObject).booleanValue();
        }
        throw new TypeException(paramString, localObject, "boolean");
    }

    public byte getByte(String paramString)
    {
        return getByte(paramString, (byte)0);
    }

    public byte getByte(String paramString, byte paramByte)
    {
        Object localObject = super.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramByte;
            if (!(localObject instanceof Byte))
                break;
            paramByte = ((Byte)localObject).byteValue();
        }
        throw new TypeException(paramString, localObject, "byte");
    }

    public double getDouble(String paramString)
    {
        return getDouble(paramString, 0.0D);
    }

    public double getDouble(String paramString, double paramDouble)
    {
        Object localObject = super.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramDouble;
            if (!(localObject instanceof Double))
                break;
            paramDouble = ((Double)localObject).doubleValue();
        }
        throw new TypeException(paramString, localObject, "double");
    }

    public float getFloat(String paramString)
    {
        return getFloat(paramString, 0.0F);
    }

    public float getFloat(String paramString, float paramFloat)
    {
        Object localObject = super.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramFloat;
            if (!(localObject instanceof Float))
                break;
            paramFloat = ((Float)localObject).floatValue();
        }
        throw new TypeException(paramString, localObject, "float");
    }

    public int getInt(String paramString)
    {
        return getInt(paramString, 0);
    }

    public int getInt(String paramString, int paramInt)
    {
        Object localObject = super.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramInt;
            if (!(localObject instanceof Integer))
                break;
            paramInt = ((Integer)localObject).intValue();
        }
        throw new TypeException(paramString, localObject, "int");
    }

    public long getLong(String paramString)
    {
        return getLong(paramString, 0L);
    }

    public long getLong(String paramString, long paramLong)
    {
        Object localObject = super.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramLong;
            if (!(localObject instanceof Long))
                break;
            paramLong = ((Long)localObject).longValue();
        }
        throw new TypeException(paramString, localObject, "long");
    }

    public short getShort(String paramString)
    {
        return getShort(paramString, (short)0);
    }

    public short getShort(String paramString, short paramShort)
    {
        Object localObject = super.get(paramString);
        if (localObject == null);
        while (true)
        {
            return paramShort;
            if (!(localObject instanceof Short))
                break;
            paramShort = ((Short)localObject).shortValue();
        }
        throw new TypeException(paramString, localObject, "short");
    }

    public String getString(String paramString)
    {
        return getString(paramString, "");
    }

    public String getString(String paramString1, String paramString2)
    {
        Object localObject = super.get(paramString1);
        if (localObject == null);
        while (true)
        {
            return paramString2;
            if (localObject == NULL_STRING)
            {
                paramString2 = null;
            }
            else
            {
                if (!(localObject instanceof String))
                    break;
                paramString2 = (String)localObject;
            }
        }
        throw new TypeException(paramString1, localObject, "string");
    }

    public int getStringInfo(String paramString)
    {
        Object localObject = super.get(paramString);
        int i;
        if (localObject == null)
            i = -1;
        while (true)
        {
            return i;
            if (localObject == NULL_STRING)
                i = 0;
            else if ((localObject instanceof String))
                i = 1;
            else
                i = -2;
        }
    }

    public void load(Reader paramReader)
        throws IOException
    {
        parse(paramReader, this);
    }

    public static class TypeException extends IllegalArgumentException
    {
        TypeException(String paramString1, Object paramObject, String paramString2)
        {
            super();
        }
    }

    public static class ParseException extends IllegalArgumentException
    {
        ParseException(StreamTokenizer paramStreamTokenizer, String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.TypedProperties
 * JD-Core Version:        0.6.2
 */