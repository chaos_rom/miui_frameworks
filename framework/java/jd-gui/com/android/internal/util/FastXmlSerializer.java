package com.android.internal.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import org.xmlpull.v1.XmlSerializer;

public class FastXmlSerializer
    implements XmlSerializer
{
    private static final int BUFFER_LEN = 8192;
    private static final String[] ESCAPE_TABLE = arrayOfString;
    private ByteBuffer mBytes = ByteBuffer.allocate(8192);
    private CharsetEncoder mCharset;
    private boolean mInTag;
    private OutputStream mOutputStream;
    private int mPos;
    private final char[] mText = new char[8192];
    private Writer mWriter;

    static
    {
        String[] arrayOfString = new String[64];
        arrayOfString[0] = null;
        arrayOfString[1] = null;
        arrayOfString[2] = null;
        arrayOfString[3] = null;
        arrayOfString[4] = null;
        arrayOfString[5] = null;
        arrayOfString[6] = null;
        arrayOfString[7] = null;
        arrayOfString[8] = null;
        arrayOfString[9] = null;
        arrayOfString[10] = null;
        arrayOfString[11] = null;
        arrayOfString[12] = null;
        arrayOfString[13] = null;
        arrayOfString[14] = null;
        arrayOfString[15] = null;
        arrayOfString[16] = null;
        arrayOfString[17] = null;
        arrayOfString[18] = null;
        arrayOfString[19] = null;
        arrayOfString[20] = null;
        arrayOfString[21] = null;
        arrayOfString[22] = null;
        arrayOfString[23] = null;
        arrayOfString[24] = null;
        arrayOfString[25] = null;
        arrayOfString[26] = null;
        arrayOfString[27] = null;
        arrayOfString[28] = null;
        arrayOfString[29] = null;
        arrayOfString[30] = null;
        arrayOfString[31] = null;
        arrayOfString[32] = null;
        arrayOfString[33] = null;
        arrayOfString[34] = "&quot;";
        arrayOfString[35] = null;
        arrayOfString[36] = null;
        arrayOfString[37] = null;
        arrayOfString[38] = "&amp;";
        arrayOfString[39] = null;
        arrayOfString[40] = null;
        arrayOfString[41] = null;
        arrayOfString[42] = null;
        arrayOfString[43] = null;
        arrayOfString[44] = null;
        arrayOfString[45] = null;
        arrayOfString[46] = null;
        arrayOfString[47] = null;
        arrayOfString[48] = null;
        arrayOfString[49] = null;
        arrayOfString[50] = null;
        arrayOfString[51] = null;
        arrayOfString[52] = null;
        arrayOfString[53] = null;
        arrayOfString[54] = null;
        arrayOfString[55] = null;
        arrayOfString[56] = null;
        arrayOfString[57] = null;
        arrayOfString[58] = null;
        arrayOfString[59] = null;
        arrayOfString[60] = "&lt;";
        arrayOfString[61] = null;
        arrayOfString[62] = "&gt;";
        arrayOfString[63] = null;
    }

    private void append(char paramChar)
        throws IOException
    {
        int i = this.mPos;
        if (i >= 8191)
        {
            flush();
            i = this.mPos;
        }
        this.mText[i] = paramChar;
        this.mPos = (i + 1);
    }

    private void append(String paramString)
        throws IOException
    {
        append(paramString, 0, paramString.length());
    }

    private void append(String paramString, int paramInt1, int paramInt2)
        throws IOException
    {
        if (paramInt2 > 8192)
        {
            int j = paramInt1 + paramInt2;
            if (paramInt1 < j)
            {
                int k = paramInt1 + 8192;
                if (k < j);
                for (int m = 8192; ; m = j - paramInt1)
                {
                    append(paramString, paramInt1, m);
                    paramInt1 = k;
                    break;
                }
            }
        }
        else
        {
            int i = this.mPos;
            if (i + paramInt2 > 8192)
            {
                flush();
                i = this.mPos;
            }
            paramString.getChars(paramInt1, paramInt1 + paramInt2, this.mText, i);
            this.mPos = (i + paramInt2);
        }
    }

    private void append(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        throws IOException
    {
        if (paramInt2 > 8192)
        {
            int j = paramInt1 + paramInt2;
            if (paramInt1 < j)
            {
                int k = paramInt1 + 8192;
                if (k < j);
                for (int m = 8192; ; m = j - paramInt1)
                {
                    append(paramArrayOfChar, paramInt1, m);
                    paramInt1 = k;
                    break;
                }
            }
        }
        else
        {
            int i = this.mPos;
            if (i + paramInt2 > 8192)
            {
                flush();
                i = this.mPos;
            }
            System.arraycopy(paramArrayOfChar, paramInt1, this.mText, i, paramInt2);
            this.mPos = (i + paramInt2);
        }
    }

    private void escapeAndAppendString(String paramString)
        throws IOException
    {
        int i = paramString.length();
        int j = (char)ESCAPE_TABLE.length;
        String[] arrayOfString = ESCAPE_TABLE;
        int k = 0;
        int m = 0;
        if (m < i)
        {
            int n = paramString.charAt(m);
            if (n >= j);
            while (true)
            {
                m++;
                break;
                String str = arrayOfString[n];
                if (str != null)
                {
                    if (k < m)
                        append(paramString, k, m - k);
                    k = m + 1;
                    append(str);
                }
            }
        }
        if (k < m)
            append(paramString, k, m - k);
    }

    private void escapeAndAppendString(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        throws IOException
    {
        int i = (char)ESCAPE_TABLE.length;
        String[] arrayOfString = ESCAPE_TABLE;
        int j = paramInt1 + paramInt2;
        int k = paramInt1;
        int m = paramInt1;
        if (m < j)
        {
            int n = paramArrayOfChar[m];
            if (n >= i);
            while (true)
            {
                m++;
                break;
                String str = arrayOfString[n];
                if (str != null)
                {
                    if (k < m)
                        append(paramArrayOfChar, k, m - k);
                    k = m + 1;
                    append(str);
                }
            }
        }
        if (k < m)
            append(paramArrayOfChar, k, m - k);
    }

    private void flushBytes()
        throws IOException
    {
        int i = this.mBytes.position();
        if (i > 0)
        {
            this.mBytes.flip();
            this.mOutputStream.write(this.mBytes.array(), 0, i);
            this.mBytes.clear();
        }
    }

    public XmlSerializer attribute(String paramString1, String paramString2, String paramString3)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        append(' ');
        if (paramString1 != null)
        {
            append(paramString1);
            append(':');
        }
        append(paramString2);
        append("=\"");
        escapeAndAppendString(paramString3);
        append('"');
        return this;
    }

    public void cdsect(String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }

    public void comment(String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }

    public void docdecl(String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }

    public void endDocument()
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        flush();
    }

    public XmlSerializer endTag(String paramString1, String paramString2)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        if (this.mInTag)
            append(" />\n");
        while (true)
        {
            this.mInTag = false;
            return this;
            append("</");
            if (paramString1 != null)
            {
                append(paramString1);
                append(':');
            }
            append(paramString2);
            append(">\n");
        }
    }

    public void entityRef(String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }

    public void flush()
        throws IOException
    {
        if (this.mPos > 0)
        {
            if (this.mOutputStream == null)
                break label105;
            CharBuffer localCharBuffer = CharBuffer.wrap(this.mText, 0, this.mPos);
            for (CoderResult localCoderResult = this.mCharset.encode(localCharBuffer, this.mBytes, true); ; localCoderResult = this.mCharset.encode(localCharBuffer, this.mBytes, true))
            {
                if (localCoderResult.isError())
                    throw new IOException(localCoderResult.toString());
                if (!localCoderResult.isOverflow())
                    break;
                flushBytes();
            }
            flushBytes();
            this.mOutputStream.flush();
        }
        while (true)
        {
            this.mPos = 0;
            return;
            label105: this.mWriter.write(this.mText, 0, this.mPos);
            this.mWriter.flush();
        }
    }

    public int getDepth()
    {
        throw new UnsupportedOperationException();
    }

    public boolean getFeature(String paramString)
    {
        throw new UnsupportedOperationException();
    }

    public String getName()
    {
        throw new UnsupportedOperationException();
    }

    public String getNamespace()
    {
        throw new UnsupportedOperationException();
    }

    public String getPrefix(String paramString, boolean paramBoolean)
        throws IllegalArgumentException
    {
        throw new UnsupportedOperationException();
    }

    public Object getProperty(String paramString)
    {
        throw new UnsupportedOperationException();
    }

    public void ignorableWhitespace(String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }

    public void processingInstruction(String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }

    public void setFeature(String paramString, boolean paramBoolean)
        throws IllegalArgumentException, IllegalStateException
    {
        if (paramString.equals("http://xmlpull.org/v1/doc/features.html#indent-output"))
            return;
        throw new UnsupportedOperationException();
    }

    public void setOutput(OutputStream paramOutputStream, String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        if (paramOutputStream == null)
            throw new IllegalArgumentException();
        try
        {
            this.mCharset = Charset.forName(paramString).newEncoder();
            this.mOutputStream = paramOutputStream;
            return;
        }
        catch (IllegalCharsetNameException localIllegalCharsetNameException)
        {
            throw ((UnsupportedEncodingException)new UnsupportedEncodingException(paramString).initCause(localIllegalCharsetNameException));
        }
        catch (UnsupportedCharsetException localUnsupportedCharsetException)
        {
            throw ((UnsupportedEncodingException)new UnsupportedEncodingException(paramString).initCause(localUnsupportedCharsetException));
        }
    }

    public void setOutput(Writer paramWriter)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        this.mWriter = paramWriter;
    }

    public void setPrefix(String paramString1, String paramString2)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }

    public void setProperty(String paramString, Object paramObject)
        throws IllegalArgumentException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }

    public void startDocument(String paramString, Boolean paramBoolean)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        StringBuilder localStringBuilder = new StringBuilder().append("<?xml version='1.0' encoding='utf-8' standalone='");
        if (paramBoolean.booleanValue());
        for (String str = "yes"; ; str = "no")
        {
            append(str + "' ?>\n");
            return;
        }
    }

    public XmlSerializer startTag(String paramString1, String paramString2)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        if (this.mInTag)
            append(">\n");
        append('<');
        if (paramString1 != null)
        {
            append(paramString1);
            append(':');
        }
        append(paramString2);
        this.mInTag = true;
        return this;
    }

    public XmlSerializer text(String paramString)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        if (this.mInTag)
        {
            append(">");
            this.mInTag = false;
        }
        escapeAndAppendString(paramString);
        return this;
    }

    public XmlSerializer text(char[] paramArrayOfChar, int paramInt1, int paramInt2)
        throws IOException, IllegalArgumentException, IllegalStateException
    {
        if (this.mInTag)
        {
            append(">");
            this.mInTag = false;
        }
        escapeAndAppendString(paramArrayOfChar, paramInt1, paramInt2);
        return this;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.FastXmlSerializer
 * JD-Core Version:        0.6.2
 */