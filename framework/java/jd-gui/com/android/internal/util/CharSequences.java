package com.android.internal.util;

public class CharSequences
{
    public static int compareToIgnoreCase(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        int i = paramCharSequence1.length();
        int j = paramCharSequence2.length();
        int k;
        int m;
        if (i < j)
        {
            k = i;
            m = 0;
        }
        int i2;
        for (int n = 0; ; n = i2)
        {
            int i4;
            int i1;
            if (n < k)
            {
                i2 = n + 1;
                int i3 = Character.toLowerCase(paramCharSequence1.charAt(n));
                i4 = m + 1;
                i1 = i3 - Character.toLowerCase(paramCharSequence2.charAt(m));
                if (i1 == 0);
            }
            else
            {
                while (true)
                {
                    return i1;
                    k = j;
                    break;
                    i1 = i - j;
                }
            }
            m = i4;
        }
    }

    public static boolean equals(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        boolean bool = false;
        if (paramCharSequence1.length() != paramCharSequence2.length());
        while (true)
        {
            return bool;
            int i = paramCharSequence1.length();
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label60;
                if (paramCharSequence1.charAt(j) != paramCharSequence2.charAt(j))
                    break;
            }
            label60: bool = true;
        }
    }

    public static CharSequence forAsciiBytes(byte[] paramArrayOfByte)
    {
        return new CharSequence()
        {
            public char charAt(int paramAnonymousInt)
            {
                return (char)CharSequences.this[paramAnonymousInt];
            }

            public int length()
            {
                return CharSequences.this.length;
            }

            public CharSequence subSequence(int paramAnonymousInt1, int paramAnonymousInt2)
            {
                return CharSequences.forAsciiBytes(CharSequences.this, paramAnonymousInt1, paramAnonymousInt2);
            }

            public String toString()
            {
                return new String(CharSequences.this);
            }
        };
    }

    public static CharSequence forAsciiBytes(byte[] paramArrayOfByte, final int paramInt1, final int paramInt2)
    {
        validate(paramInt1, paramInt2, paramArrayOfByte.length);
        return new CharSequence()
        {
            public char charAt(int paramAnonymousInt)
            {
                return (char)CharSequences.this[(paramAnonymousInt + paramInt1)];
            }

            public int length()
            {
                return paramInt2 - paramInt1;
            }

            public CharSequence subSequence(int paramAnonymousInt1, int paramAnonymousInt2)
            {
                int i = paramAnonymousInt1 - paramInt1;
                int j = paramAnonymousInt2 - paramInt1;
                CharSequences.validate(i, j, length());
                return CharSequences.forAsciiBytes(CharSequences.this, i, j);
            }

            public String toString()
            {
                return new String(CharSequences.this, paramInt1, length());
            }
        };
    }

    static void validate(int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt1 < 0)
            throw new IndexOutOfBoundsException();
        if (paramInt2 < 0)
            throw new IndexOutOfBoundsException();
        if (paramInt2 > paramInt3)
            throw new IndexOutOfBoundsException();
        if (paramInt1 > paramInt2)
            throw new IndexOutOfBoundsException();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.CharSequences
 * JD-Core Version:        0.6.2
 */