package com.android.internal.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charsets;

public class ProcFileReader
    implements Closeable
{
    private final byte[] mBuffer;
    private boolean mLineFinished;
    private final InputStream mStream;
    private int mTail;

    public ProcFileReader(InputStream paramInputStream)
        throws IOException
    {
        this(paramInputStream, 4096);
    }

    public ProcFileReader(InputStream paramInputStream, int paramInt)
        throws IOException
    {
        this.mStream = paramInputStream;
        this.mBuffer = new byte[paramInt];
        fillBuf();
    }

    private void consumeBuf(int paramInt)
        throws IOException
    {
        System.arraycopy(this.mBuffer, paramInt, this.mBuffer, 0, this.mTail - paramInt);
        this.mTail -= paramInt;
        if (this.mTail == 0)
            fillBuf();
    }

    private int fillBuf()
        throws IOException
    {
        int i = this.mBuffer.length - this.mTail;
        if (i == 0)
            throw new IOException("attempting to fill already-full buffer");
        int j = this.mStream.read(this.mBuffer, this.mTail, i);
        if (j != -1)
            this.mTail = (j + this.mTail);
        return j;
    }

    private NumberFormatException invalidLong(int paramInt)
    {
        return new NumberFormatException("invalid long: " + new String(this.mBuffer, 0, paramInt, Charsets.US_ASCII));
    }

    private int nextTokenIndex()
        throws IOException
    {
        if (this.mLineFinished)
            throw new IOException("no tokens remaining on current line");
        int i = 0;
        do
            while (i < this.mTail)
            {
                int j = this.mBuffer[i];
                if (j == 10)
                    this.mLineFinished = true;
                while (j == 32)
                    return i;
                i++;
            }
        while (fillBuf() > 0);
        throw new IOException("end of stream while looking for token boundary");
    }

    public void close()
        throws IOException
    {
        this.mStream.close();
    }

    public void finishLine()
        throws IOException
    {
        if (this.mLineFinished)
        {
            this.mLineFinished = false;
            return;
        }
        int i = 0;
        label50: 
        do
            while (true)
            {
                if (i >= this.mTail)
                    break label50;
                if (this.mBuffer[i] == 10)
                {
                    consumeBuf(i + 1);
                    break;
                }
                i++;
            }
        while (fillBuf() > 0);
        throw new IOException("end of stream while looking for line boundary");
    }

    public boolean hasMoreData()
    {
        if (this.mTail > 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int nextInt()
        throws IOException
    {
        long l = nextLong();
        if ((l > 2147483647L) || (l < -2147483648L))
            throw new NumberFormatException("parsed value larger than integer");
        return (int)l;
    }

    public long nextLong()
        throws IOException
    {
        int i = 1;
        int j = nextTokenIndex();
        int k;
        long l1;
        if (this.mBuffer[0] == 45)
        {
            k = i;
            l1 = 0L;
            if (k == 0)
                break label66;
        }
        while (true)
        {
            if (i >= j)
                break label107;
            int m = -48 + this.mBuffer[i];
            if ((m < 0) || (m > 9))
            {
                throw invalidLong(j);
                k = 0;
                break;
                label66: i = 0;
                continue;
            }
            long l2 = 10L * l1 - m;
            if (l2 > l1)
                throw invalidLong(j);
            l1 = l2;
            i++;
        }
        label107: consumeBuf(j + 1);
        if (k != 0);
        while (true)
        {
            return l1;
            l1 = -l1;
        }
    }

    public String nextString()
        throws IOException
    {
        int i = nextTokenIndex();
        String str = new String(this.mBuffer, 0, i, Charsets.US_ASCII);
        consumeBuf(i + 1);
        return str;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.ProcFileReader
 * JD-Core Version:        0.6.2
 */