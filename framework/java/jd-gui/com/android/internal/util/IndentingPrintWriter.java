package com.android.internal.util;

import java.io.PrintWriter;
import java.io.Writer;

public class IndentingPrintWriter extends PrintWriter
{
    private StringBuilder mBuilder = new StringBuilder();
    private String mCurrent = new String();
    private boolean mEmptyLine = true;
    private final String mIndent;

    public IndentingPrintWriter(Writer paramWriter, String paramString)
    {
        super(paramWriter);
        this.mIndent = paramString;
    }

    public void decreaseIndent()
    {
        this.mBuilder.delete(0, this.mIndent.length());
        this.mCurrent = this.mBuilder.toString();
    }

    public void increaseIndent()
    {
        this.mBuilder.append(this.mIndent);
        this.mCurrent = this.mBuilder.toString();
    }

    public void printPair(String paramString, Object paramObject)
    {
        print(paramString + "=" + String.valueOf(paramObject) + " ");
    }

    public void println()
    {
        super.println();
        this.mEmptyLine = true;
    }

    public void write(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
        if (this.mEmptyLine)
        {
            this.mEmptyLine = false;
            super.print(this.mCurrent);
        }
        super.write(paramArrayOfChar, paramInt1, paramInt2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.IndentingPrintWriter
 * JD-Core Version:        0.6.2
 */