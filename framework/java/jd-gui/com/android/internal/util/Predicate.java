package com.android.internal.util;

public abstract interface Predicate<T>
{
    public abstract boolean apply(T paramT);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.Predicate
 * JD-Core Version:        0.6.2
 */