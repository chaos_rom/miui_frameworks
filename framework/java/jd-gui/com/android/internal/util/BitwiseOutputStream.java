package com.android.internal.util;

public class BitwiseOutputStream
{
    private byte[] mBuf;
    private int mEnd;
    private int mPos;

    public BitwiseOutputStream(int paramInt)
    {
        this.mBuf = new byte[paramInt];
        this.mEnd = (paramInt << 3);
        this.mPos = 0;
    }

    private void possExpand(int paramInt)
    {
        if (paramInt + this.mPos < this.mEnd);
        while (true)
        {
            return;
            byte[] arrayOfByte = new byte[paramInt + this.mPos >>> 2];
            System.arraycopy(this.mBuf, 0, arrayOfByte, 0, this.mEnd >>> 3);
            this.mBuf = arrayOfByte;
            this.mEnd = (arrayOfByte.length << 3);
        }
    }

    public void skip(int paramInt)
    {
        possExpand(paramInt);
        this.mPos = (paramInt + this.mPos);
    }

    public byte[] toByteArray()
    {
        int i = this.mPos >>> 3;
        if ((0x7 & this.mPos) > 0);
        for (int j = 1; ; j = 0)
        {
            int k = i + j;
            byte[] arrayOfByte = new byte[k];
            System.arraycopy(this.mBuf, 0, arrayOfByte, 0, k);
            return arrayOfByte;
        }
    }

    public void write(int paramInt1, int paramInt2)
        throws BitwiseOutputStream.AccessException
    {
        if ((paramInt1 < 0) || (paramInt1 > 8))
            throw new AccessException("illegal write (" + paramInt1 + " bits)");
        possExpand(paramInt1);
        int i = paramInt2 & -1 >>> 32 - paramInt1;
        int j = this.mPos >>> 3;
        int k = 16 - (0x7 & this.mPos) - paramInt1;
        int m = i << k;
        this.mPos = (paramInt1 + this.mPos);
        byte[] arrayOfByte1 = this.mBuf;
        arrayOfByte1[j] = ((byte)(arrayOfByte1[j] | m >>> 8));
        if (k < 8)
        {
            byte[] arrayOfByte2 = this.mBuf;
            int n = j + 1;
            arrayOfByte2[n] = ((byte)(arrayOfByte2[n] | m & 0xFF));
        }
    }

    public void writeByteArray(int paramInt, byte[] paramArrayOfByte)
        throws BitwiseOutputStream.AccessException
    {
        for (int i = 0; i < paramArrayOfByte.length; i++)
        {
            int j = Math.min(8, paramInt - (i << 3));
            if (j > 0)
                write(j, (byte)(paramArrayOfByte[i] >>> 8 - j));
        }
    }

    public static class AccessException extends Exception
    {
        public AccessException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.BitwiseOutputStream
 * JD-Core Version:        0.6.2
 */