package com.android.internal.util;

import android.content.Context;
import android.os.Handler;

final class AsyncChannel$1ConnectAsync
    implements Runnable
{
    String mDstClassName;
    String mDstPackageName;
    Context mSrcCtx;
    Handler mSrcHdlr;

    AsyncChannel$1ConnectAsync(AsyncChannel paramAsyncChannel, Context paramContext, Handler paramHandler, String paramString1, String paramString2)
    {
        this.mSrcCtx = paramContext;
        this.mSrcHdlr = paramHandler;
        this.mDstPackageName = paramString1;
        this.mDstClassName = paramString2;
    }

    public void run()
    {
        int i = this.this$0.connectSrcHandlerToPackageSync(this.mSrcCtx, this.mSrcHdlr, this.mDstPackageName, this.mDstClassName);
        AsyncChannel.access$000(this.this$0, i);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.AsyncChannel.1ConnectAsync
 * JD-Core Version:        0.6.2
 */