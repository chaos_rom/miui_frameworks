package com.android.internal.util;

public class FastMath
{
    public static int round(float paramFloat)
    {
        return (int)(8388608L + ()(16777216.0F * paramFloat) >> 24);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.FastMath
 * JD-Core Version:        0.6.2
 */