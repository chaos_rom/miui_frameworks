package com.android.internal.util;

import java.io.File;
import java.io.IOException;

public class JournaledFile
{
    File mReal;
    File mTemp;
    boolean mWriting;

    public JournaledFile(File paramFile1, File paramFile2)
    {
        this.mReal = paramFile1;
        this.mTemp = paramFile2;
    }

    public File chooseForRead()
    {
        File localFile;
        if (this.mReal.exists())
        {
            localFile = this.mReal;
            if (this.mTemp.exists())
                this.mTemp.delete();
        }
        while (true)
        {
            return localFile;
            if (this.mTemp.exists())
            {
                localFile = this.mTemp;
                this.mTemp.renameTo(this.mReal);
            }
            else
            {
                localFile = this.mReal;
            }
        }
    }

    public File chooseForWrite()
    {
        if (this.mWriting)
            throw new IllegalStateException("uncommitted write already in progress");
        if (!this.mReal.exists());
        try
        {
            this.mReal.createNewFile();
            label35: if (this.mTemp.exists())
                this.mTemp.delete();
            this.mWriting = true;
            return this.mTemp;
        }
        catch (IOException localIOException)
        {
            break label35;
        }
    }

    public void commit()
    {
        if (!this.mWriting)
            throw new IllegalStateException("no file to commit");
        this.mWriting = false;
        this.mTemp.renameTo(this.mReal);
    }

    public void rollback()
    {
        if (!this.mWriting)
            throw new IllegalStateException("no file to roll back");
        this.mWriting = false;
        this.mTemp.delete();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.JournaledFile
 * JD-Core Version:        0.6.2
 */