package com.android.internal.util;

import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class XmlUtils
{
    public static final void beginDocument(XmlPullParser paramXmlPullParser, String paramString)
        throws XmlPullParserException, IOException
    {
        int i;
        do
            i = paramXmlPullParser.next();
        while ((i != 2) && (i != 1));
        if (i != 2)
            throw new XmlPullParserException("No start tag found");
        if (!paramXmlPullParser.getName().equals(paramString))
            throw new XmlPullParserException("Unexpected start tag: found " + paramXmlPullParser.getName() + ", expected " + paramString);
    }

    public static final boolean convertValueToBoolean(CharSequence paramCharSequence, boolean paramBoolean)
    {
        boolean bool = false;
        if (paramCharSequence == null);
        while (true)
        {
            return paramBoolean;
            if ((paramCharSequence.equals("1")) || (paramCharSequence.equals("true")) || (paramCharSequence.equals("TRUE")))
                bool = true;
            paramBoolean = bool;
        }
    }

    public static final int convertValueToInt(CharSequence paramCharSequence, int paramInt)
    {
        if (paramCharSequence == null);
        String str;
        int i;
        int j;
        int m;
        while (true)
        {
            return paramInt;
            str = paramCharSequence.toString();
            i = 1;
            j = 0;
            int k = str.length();
            m = 10;
            if ('-' == str.charAt(0))
            {
                i = -1;
                j = 0 + 1;
            }
            if ('0' != str.charAt(j))
                break label127;
            if (j != k - 1)
                break;
            paramInt = 0;
        }
        int n = str.charAt(j + 1);
        if ((120 == n) || (88 == n))
        {
            j += 2;
            m = 16;
        }
        while (true)
        {
            paramInt = i * Integer.parseInt(str.substring(j), m);
            break;
            j++;
            m = 8;
            continue;
            label127: if ('#' == str.charAt(j))
            {
                j++;
                m = 16;
            }
        }
    }

    public static final int convertValueToList(CharSequence paramCharSequence, String[] paramArrayOfString, int paramInt)
    {
        int i;
        if (paramCharSequence != null)
        {
            i = 0;
            if (i < paramArrayOfString.length)
                if (!paramCharSequence.equals(paramArrayOfString[i]));
        }
        while (true)
        {
            return i;
            i++;
            break;
            i = paramInt;
        }
    }

    public static final int convertValueToUnsignedInt(String paramString, int paramInt)
    {
        if (paramString == null);
        while (true)
        {
            return paramInt;
            paramInt = parseUnsignedIntAttribute(paramString);
        }
    }

    public static final void nextElement(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        int i;
        do
            i = paramXmlPullParser.next();
        while ((i != 2) && (i != 1));
    }

    public static boolean nextElementWithin(XmlPullParser paramXmlPullParser, int paramInt)
        throws IOException, XmlPullParserException
    {
        int i = 1;
        int j = paramXmlPullParser.next();
        if ((j == i) || ((j == 3) && (paramXmlPullParser.getDepth() == paramInt)))
            i = 0;
        while (true)
        {
            return i;
            if ((j != 2) || (paramXmlPullParser.getDepth() != paramInt + 1))
                break;
        }
    }

    public static final int parseUnsignedIntAttribute(CharSequence paramCharSequence)
    {
        String str = paramCharSequence.toString();
        int i = 0;
        int j = str.length();
        int k = 10;
        int m;
        if ('0' == str.charAt(0))
        {
            if (j - 1 == 0)
            {
                m = 0;
                return m;
            }
            int n = str.charAt(1);
            if ((120 == n) || (88 == n))
            {
                i = 0 + 2;
                k = 16;
            }
        }
        while (true)
        {
            m = (int)Long.parseLong(str.substring(i), k);
            break;
            i = 0 + 1;
            k = 8;
            continue;
            if ('#' == str.charAt(0))
            {
                i = 0 + 1;
                k = 16;
            }
        }
    }

    public static final ArrayList readListXml(InputStream paramInputStream)
        throws XmlPullParserException, IOException
    {
        XmlPullParser localXmlPullParser = Xml.newPullParser();
        localXmlPullParser.setInput(paramInputStream, null);
        return (ArrayList)readValueXml(localXmlPullParser, new String[1]);
    }

    public static final HashMap readMapXml(InputStream paramInputStream)
        throws XmlPullParserException, IOException
    {
        XmlPullParser localXmlPullParser = Xml.newPullParser();
        localXmlPullParser.setInput(paramInputStream, null);
        return (HashMap)readValueXml(localXmlPullParser, new String[1]);
    }

    public static final HashSet readSetXml(InputStream paramInputStream)
        throws XmlPullParserException, IOException
    {
        XmlPullParser localXmlPullParser = Xml.newPullParser();
        localXmlPullParser.setInput(paramInputStream, null);
        return (HashSet)readValueXml(localXmlPullParser, new String[1]);
    }

    // ERROR //
    public static final int[] readThisIntArrayXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: aconst_null
        //     2: ldc 132
        //     4: invokeinterface 136 3 0
        //     9: invokestatic 139	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     12: istore 5
        //     14: iload 5
        //     16: newarray int
        //     18: astore 6
        //     20: iconst_0
        //     21: istore 7
        //     23: aload_0
        //     24: invokeinterface 142 1 0
        //     29: istore 8
        //     31: iload 8
        //     33: iconst_2
        //     34: if_icmpne +159 -> 193
        //     37: aload_0
        //     38: invokeinterface 29 1 0
        //     43: ldc 144
        //     45: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     48: ifeq +113 -> 161
        //     51: aload 6
        //     53: iload 7
        //     55: aload_0
        //     56: aconst_null
        //     57: ldc 146
        //     59: invokeinterface 136 3 0
        //     64: invokestatic 139	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     67: iastore
        //     68: aload_0
        //     69: invokeinterface 20 1 0
        //     74: istore 8
        //     76: iload 8
        //     78: iconst_1
        //     79: if_icmpne -48 -> 31
        //     82: new 12	org/xmlpull/v1/XmlPullParserException
        //     85: dup
        //     86: new 37	java/lang/StringBuilder
        //     89: dup
        //     90: invokespecial 38	java/lang/StringBuilder:<init>	()V
        //     93: ldc 148
        //     95: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     98: aload_1
        //     99: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     102: ldc 150
        //     104: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     110: invokespecial 25	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
        //     113: athrow
        //     114: astore 4
        //     116: new 12	org/xmlpull/v1/XmlPullParserException
        //     119: dup
        //     120: ldc 152
        //     122: invokespecial 25	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
        //     125: athrow
        //     126: astore_3
        //     127: new 12	org/xmlpull/v1/XmlPullParserException
        //     130: dup
        //     131: ldc 154
        //     133: invokespecial 25	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
        //     136: athrow
        //     137: astore 10
        //     139: new 12	org/xmlpull/v1/XmlPullParserException
        //     142: dup
        //     143: ldc 156
        //     145: invokespecial 25	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
        //     148: athrow
        //     149: astore 9
        //     151: new 12	org/xmlpull/v1/XmlPullParserException
        //     154: dup
        //     155: ldc 158
        //     157: invokespecial 25	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
        //     160: athrow
        //     161: new 12	org/xmlpull/v1/XmlPullParserException
        //     164: dup
        //     165: new 37	java/lang/StringBuilder
        //     168: dup
        //     169: invokespecial 38	java/lang/StringBuilder:<init>	()V
        //     172: ldc 160
        //     174: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     177: aload_0
        //     178: invokeinterface 29 1 0
        //     183: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     186: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     189: invokespecial 25	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
        //     192: athrow
        //     193: iload 8
        //     195: iconst_3
        //     196: if_icmpne -128 -> 68
        //     199: aload_0
        //     200: invokeinterface 29 1 0
        //     205: aload_1
        //     206: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     209: ifeq +6 -> 215
        //     212: aload 6
        //     214: areturn
        //     215: aload_0
        //     216: invokeinterface 29 1 0
        //     221: ldc 144
        //     223: invokevirtual 35	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     226: ifeq +9 -> 235
        //     229: iinc 7 1
        //     232: goto -164 -> 68
        //     235: new 12	org/xmlpull/v1/XmlPullParserException
        //     238: dup
        //     239: new 37	java/lang/StringBuilder
        //     242: dup
        //     243: invokespecial 38	java/lang/StringBuilder:<init>	()V
        //     246: ldc 162
        //     248: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     251: aload_1
        //     252: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     255: ldc 164
        //     257: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     260: aload_0
        //     261: invokeinterface 29 1 0
        //     266: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     269: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     272: invokespecial 25	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
        //     275: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     0	14	114	java/lang/NullPointerException
        //     0	14	126	java/lang/NumberFormatException
        //     51	68	137	java/lang/NullPointerException
        //     51	68	149	java/lang/NumberFormatException
    }

    public static final ArrayList readThisListXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        ArrayList localArrayList = new ArrayList();
        int i = paramXmlPullParser.getEventType();
        if (i == 2)
            localArrayList.add(readThisValueXml(paramXmlPullParser, paramArrayOfString));
        while (i != 3)
        {
            i = paramXmlPullParser.next();
            if (i != 1)
                break;
            throw new XmlPullParserException("Document ended before " + paramString + " end tag");
        }
        if (paramXmlPullParser.getName().equals(paramString))
            return localArrayList;
        throw new XmlPullParserException("Expected " + paramString + " end tag at: " + paramXmlPullParser.getName());
    }

    public static final HashMap readThisMapXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        HashMap localHashMap = new HashMap();
        int i = paramXmlPullParser.getEventType();
        if (i == 2)
        {
            localObject = readThisValueXml(paramXmlPullParser, paramArrayOfString);
            if (paramArrayOfString[0] != null)
                localHashMap.put(paramArrayOfString[0], localObject);
        }
        while (i != 3)
        {
            Object localObject;
            i = paramXmlPullParser.next();
            if (i != 1)
                break;
            throw new XmlPullParserException("Document ended before " + paramString + " end tag");
            throw new XmlPullParserException("Map value without name attribute: " + paramXmlPullParser.getName());
        }
        if (paramXmlPullParser.getName().equals(paramString))
            return localHashMap;
        throw new XmlPullParserException("Expected " + paramString + " end tag at: " + paramXmlPullParser.getName());
    }

    public static final HashSet readThisSetXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        HashSet localHashSet = new HashSet();
        int i = paramXmlPullParser.getEventType();
        if (i == 2)
            localHashSet.add(readThisValueXml(paramXmlPullParser, paramArrayOfString));
        while (i != 3)
        {
            i = paramXmlPullParser.next();
            if (i != 1)
                break;
            throw new XmlPullParserException("Document ended before " + paramString + " end tag");
        }
        if (paramXmlPullParser.getName().equals(paramString))
            return localHashSet;
        throw new XmlPullParserException("Expected " + paramString + " end tag at: " + paramXmlPullParser.getName());
    }

    private static final Object readThisValueXml(XmlPullParser paramXmlPullParser, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        String str1 = paramXmlPullParser.getAttributeValue(null, "name");
        String str2 = paramXmlPullParser.getName();
        Object localObject;
        if (str2.equals("null"))
            localObject = null;
        int i;
        label194: label232: label511: 
        do
        {
            i = paramXmlPullParser.next();
            if (i == 1)
                break;
            if (i == 3)
            {
                if (paramXmlPullParser.getName().equals(str2))
                {
                    paramArrayOfString[0] = str1;
                    while (true)
                    {
                        return localObject;
                        if (str2.equals("string"))
                        {
                            String str3 = "";
                            int j;
                            do
                                while (true)
                                {
                                    j = paramXmlPullParser.next();
                                    if (j == 1)
                                        break label232;
                                    if (j == 3)
                                    {
                                        if (paramXmlPullParser.getName().equals("string"))
                                        {
                                            paramArrayOfString[0] = str1;
                                            localObject = str3;
                                            break;
                                        }
                                        throw new XmlPullParserException("Unexpected end tag in <string>: " + paramXmlPullParser.getName());
                                    }
                                    if (j != 4)
                                        break label194;
                                    str3 = str3 + paramXmlPullParser.getText();
                                }
                            while (j != 2);
                            throw new XmlPullParserException("Unexpected start tag in <string>: " + paramXmlPullParser.getName());
                            throw new XmlPullParserException("Unexpected end of document in <string>");
                        }
                        if (str2.equals("int"))
                        {
                            localObject = Integer.valueOf(Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value")));
                            break;
                        }
                        if (str2.equals("long"))
                        {
                            localObject = Long.valueOf(paramXmlPullParser.getAttributeValue(null, "value"));
                            break;
                        }
                        if (str2.equals("float"))
                        {
                            localObject = new Float(paramXmlPullParser.getAttributeValue(null, "value"));
                            break;
                        }
                        if (str2.equals("double"))
                        {
                            localObject = new Double(paramXmlPullParser.getAttributeValue(null, "value"));
                            break;
                        }
                        if (str2.equals("boolean"))
                        {
                            localObject = Boolean.valueOf(paramXmlPullParser.getAttributeValue(null, "value"));
                            break;
                        }
                        if (str2.equals("int-array"))
                        {
                            paramXmlPullParser.next();
                            localObject = readThisIntArrayXml(paramXmlPullParser, "int-array", paramArrayOfString);
                            paramArrayOfString[0] = str1;
                        }
                        else if (str2.equals("map"))
                        {
                            paramXmlPullParser.next();
                            localObject = readThisMapXml(paramXmlPullParser, "map", paramArrayOfString);
                            paramArrayOfString[0] = str1;
                        }
                        else if (str2.equals("list"))
                        {
                            paramXmlPullParser.next();
                            localObject = readThisListXml(paramXmlPullParser, "list", paramArrayOfString);
                            paramArrayOfString[0] = str1;
                        }
                        else
                        {
                            if (!str2.equals("set"))
                                break label511;
                            paramXmlPullParser.next();
                            localObject = readThisSetXml(paramXmlPullParser, "set", paramArrayOfString);
                            paramArrayOfString[0] = str1;
                        }
                    }
                    throw new XmlPullParserException("Unknown tag: " + str2);
                }
                throw new XmlPullParserException("Unexpected end tag in <" + str2 + ">: " + paramXmlPullParser.getName());
            }
            if (i == 4)
                throw new XmlPullParserException("Unexpected text in <" + str2 + ">: " + paramXmlPullParser.getName());
        }
        while (i != 2);
        throw new XmlPullParserException("Unexpected start tag in <" + str2 + ">: " + paramXmlPullParser.getName());
        throw new XmlPullParserException("Unexpected end of document in <" + str2 + ">");
    }

    public static final Object readValueXml(XmlPullParser paramXmlPullParser, String[] paramArrayOfString)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlPullParser.getEventType();
        do
        {
            if (i == 2)
                return readThisValueXml(paramXmlPullParser, paramArrayOfString);
            if (i == 3)
                throw new XmlPullParserException("Unexpected end tag at: " + paramXmlPullParser.getName());
            if (i == 4)
                throw new XmlPullParserException("Unexpected text: " + paramXmlPullParser.getText());
            i = paramXmlPullParser.next();
        }
        while (i != 1);
        throw new XmlPullParserException("Unexpected end of document");
    }

    public static void skipCurrentTag(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlPullParser.getDepth();
        int j;
        do
            j = paramXmlPullParser.next();
        while ((j != 1) && ((j != 3) || (paramXmlPullParser.getDepth() > i)));
    }

    public static final void writeByteArrayXml(byte[] paramArrayOfByte, String paramString, XmlSerializer paramXmlSerializer)
        throws XmlPullParserException, IOException
    {
        if (paramArrayOfByte == null)
        {
            paramXmlSerializer.startTag(null, "null");
            paramXmlSerializer.endTag(null, "null");
        }
        while (true)
        {
            return;
            paramXmlSerializer.startTag(null, "byte-array");
            if (paramString != null)
                paramXmlSerializer.attribute(null, "name", paramString);
            int i = paramArrayOfByte.length;
            paramXmlSerializer.attribute(null, "num", Integer.toString(i));
            StringBuilder localStringBuilder = new StringBuilder(2 * paramArrayOfByte.length);
            int j = 0;
            if (j < i)
            {
                int k = paramArrayOfByte[j];
                int m = k >> 4;
                int n;
                label122: int i1;
                if (m >= 10)
                {
                    n = -10 + (m + 97);
                    localStringBuilder.append(n);
                    i1 = k & 0xFF;
                    if (i1 < 10)
                        break label179;
                }
                label179: for (int i2 = -10 + (i1 + 97); ; i2 = i1 + 48)
                {
                    localStringBuilder.append(i2);
                    j++;
                    break;
                    n = m + 48;
                    break label122;
                }
            }
            paramXmlSerializer.text(localStringBuilder.toString());
            paramXmlSerializer.endTag(null, "byte-array");
        }
    }

    public static final void writeIntArrayXml(int[] paramArrayOfInt, String paramString, XmlSerializer paramXmlSerializer)
        throws XmlPullParserException, IOException
    {
        if (paramArrayOfInt == null)
        {
            paramXmlSerializer.startTag(null, "null");
            paramXmlSerializer.endTag(null, "null");
        }
        while (true)
        {
            return;
            paramXmlSerializer.startTag(null, "int-array");
            if (paramString != null)
                paramXmlSerializer.attribute(null, "name", paramString);
            int i = paramArrayOfInt.length;
            paramXmlSerializer.attribute(null, "num", Integer.toString(i));
            for (int j = 0; j < i; j++)
            {
                paramXmlSerializer.startTag(null, "item");
                paramXmlSerializer.attribute(null, "value", Integer.toString(paramArrayOfInt[j]));
                paramXmlSerializer.endTag(null, "item");
            }
            paramXmlSerializer.endTag(null, "int-array");
        }
    }

    public static final void writeListXml(List paramList, OutputStream paramOutputStream)
        throws XmlPullParserException, IOException
    {
        XmlSerializer localXmlSerializer = Xml.newSerializer();
        localXmlSerializer.setOutput(paramOutputStream, "utf-8");
        localXmlSerializer.startDocument(null, Boolean.valueOf(true));
        localXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        writeListXml(paramList, null, localXmlSerializer);
        localXmlSerializer.endDocument();
    }

    public static final void writeListXml(List paramList, String paramString, XmlSerializer paramXmlSerializer)
        throws XmlPullParserException, IOException
    {
        if (paramList == null)
        {
            paramXmlSerializer.startTag(null, "null");
            paramXmlSerializer.endTag(null, "null");
        }
        while (true)
        {
            return;
            paramXmlSerializer.startTag(null, "list");
            if (paramString != null)
                paramXmlSerializer.attribute(null, "name", paramString);
            int i = paramList.size();
            for (int j = 0; j < i; j++)
                writeValueXml(paramList.get(j), null, paramXmlSerializer);
            paramXmlSerializer.endTag(null, "list");
        }
    }

    public static final void writeMapXml(Map paramMap, OutputStream paramOutputStream)
        throws XmlPullParserException, IOException
    {
        FastXmlSerializer localFastXmlSerializer = new FastXmlSerializer();
        localFastXmlSerializer.setOutput(paramOutputStream, "utf-8");
        localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
        localFastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        writeMapXml(paramMap, null, localFastXmlSerializer);
        localFastXmlSerializer.endDocument();
    }

    public static final void writeMapXml(Map paramMap, String paramString, XmlSerializer paramXmlSerializer)
        throws XmlPullParserException, IOException
    {
        if (paramMap == null)
        {
            paramXmlSerializer.startTag(null, "null");
            paramXmlSerializer.endTag(null, "null");
        }
        while (true)
        {
            return;
            Iterator localIterator = paramMap.entrySet().iterator();
            paramXmlSerializer.startTag(null, "map");
            if (paramString != null)
                paramXmlSerializer.attribute(null, "name", paramString);
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                writeValueXml(localEntry.getValue(), (String)localEntry.getKey(), paramXmlSerializer);
            }
            paramXmlSerializer.endTag(null, "map");
        }
    }

    public static final void writeSetXml(Set paramSet, String paramString, XmlSerializer paramXmlSerializer)
        throws XmlPullParserException, IOException
    {
        if (paramSet == null)
        {
            paramXmlSerializer.startTag(null, "null");
            paramXmlSerializer.endTag(null, "null");
        }
        while (true)
        {
            return;
            paramXmlSerializer.startTag(null, "set");
            if (paramString != null)
                paramXmlSerializer.attribute(null, "name", paramString);
            Iterator localIterator = paramSet.iterator();
            while (localIterator.hasNext())
                writeValueXml(localIterator.next(), null, paramXmlSerializer);
            paramXmlSerializer.endTag(null, "set");
        }
    }

    public static final void writeValueXml(Object paramObject, String paramString, XmlSerializer paramXmlSerializer)
        throws XmlPullParserException, IOException
    {
        if (paramObject == null)
        {
            paramXmlSerializer.startTag(null, "null");
            if (paramString != null)
                paramXmlSerializer.attribute(null, "name", paramString);
            paramXmlSerializer.endTag(null, "null");
        }
        while (true)
        {
            return;
            if ((paramObject instanceof String))
            {
                paramXmlSerializer.startTag(null, "string");
                if (paramString != null)
                    paramXmlSerializer.attribute(null, "name", paramString);
                paramXmlSerializer.text(paramObject.toString());
                paramXmlSerializer.endTag(null, "string");
            }
            else
            {
                String str;
                if ((paramObject instanceof Integer))
                    str = "int";
                while (true)
                {
                    paramXmlSerializer.startTag(null, str);
                    if (paramString != null)
                        paramXmlSerializer.attribute(null, "name", paramString);
                    paramXmlSerializer.attribute(null, "value", paramObject.toString());
                    paramXmlSerializer.endTag(null, str);
                    break;
                    if ((paramObject instanceof Long))
                    {
                        str = "long";
                    }
                    else if ((paramObject instanceof Float))
                    {
                        str = "float";
                    }
                    else if ((paramObject instanceof Double))
                    {
                        str = "double";
                    }
                    else
                    {
                        if (!(paramObject instanceof Boolean))
                            break label215;
                        str = "boolean";
                    }
                }
                label215: if ((paramObject instanceof byte[]))
                {
                    writeByteArrayXml((byte[])paramObject, paramString, paramXmlSerializer);
                }
                else if ((paramObject instanceof int[]))
                {
                    writeIntArrayXml((int[])paramObject, paramString, paramXmlSerializer);
                }
                else if ((paramObject instanceof Map))
                {
                    writeMapXml((Map)paramObject, paramString, paramXmlSerializer);
                }
                else if ((paramObject instanceof List))
                {
                    writeListXml((List)paramObject, paramString, paramXmlSerializer);
                }
                else if ((paramObject instanceof Set))
                {
                    writeSetXml((Set)paramObject, paramString, paramXmlSerializer);
                }
                else
                {
                    if (!(paramObject instanceof CharSequence))
                        break;
                    paramXmlSerializer.startTag(null, "string");
                    if (paramString != null)
                        paramXmlSerializer.attribute(null, "name", paramString);
                    paramXmlSerializer.text(paramObject.toString());
                    paramXmlSerializer.endTag(null, "string");
                }
            }
        }
        throw new RuntimeException("writeValueXml: unable to write value " + paramObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.XmlUtils
 * JD-Core Version:        0.6.2
 */