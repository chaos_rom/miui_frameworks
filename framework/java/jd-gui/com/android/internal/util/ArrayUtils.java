package com.android.internal.util;

import java.lang.reflect.Array;

public class ArrayUtils
{
    private static final int CACHE_SIZE = 73;
    private static Object[] EMPTY = new Object[0];
    private static Object[] sCache = new Object[73];

    public static <T> T[] appendElement(Class<T> paramClass, T[] paramArrayOfT, T paramT)
    {
        int i;
        Object[] arrayOfObject;
        if (paramArrayOfT != null)
        {
            i = paramArrayOfT.length;
            arrayOfObject = (Object[])Array.newInstance(paramClass, i + 1);
            System.arraycopy(paramArrayOfT, 0, arrayOfObject, 0, i);
        }
        while (true)
        {
            arrayOfObject[i] = paramT;
            return arrayOfObject;
            i = 0;
            arrayOfObject = (Object[])Array.newInstance(paramClass, 1);
        }
    }

    public static int[] appendInt(int[] paramArrayOfInt, int paramInt)
    {
        if (paramArrayOfInt == null)
        {
            paramArrayOfInt = new int[1];
            paramArrayOfInt[0] = paramInt;
        }
        while (true)
        {
            return paramArrayOfInt;
            int i = paramArrayOfInt.length;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label37;
                if (paramArrayOfInt[j] == paramInt)
                    break;
            }
            label37: int[] arrayOfInt = new int[i + 1];
            System.arraycopy(paramArrayOfInt, 0, arrayOfInt, 0, i);
            arrayOfInt[i] = paramInt;
            paramArrayOfInt = arrayOfInt;
        }
    }

    public static boolean contains(int[] paramArrayOfInt, int paramInt)
    {
        int i = paramArrayOfInt.length;
        int j = 0;
        if (j < i)
            if (paramArrayOfInt[j] != paramInt);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    public static <T> boolean contains(T[] paramArrayOfT, T paramT)
    {
        boolean bool = true;
        int i = paramArrayOfT.length;
        int j = 0;
        T ?;
        if (j < i)
        {
            ? = paramArrayOfT[j];
            if (? == null)
                if (paramT != null)
                    break label44;
        }
        while (true)
        {
            return bool;
            if ((paramT == null) || (!?.equals(paramT)))
            {
                label44: j++;
                break;
                bool = false;
            }
        }
    }

    public static <T> T[] emptyArray(Class<T> paramClass)
    {
        if (paramClass == Object.class);
        Object localObject;
        for (Object[] arrayOfObject = (Object[])EMPTY; ; arrayOfObject = (Object[])localObject)
        {
            return arrayOfObject;
            int i = (0x7FFFFFFF & System.identityHashCode(paramClass) / 8) % 73;
            localObject = sCache[i];
            if ((localObject == null) || (localObject.getClass().getComponentType() != paramClass))
            {
                localObject = Array.newInstance(paramClass, 0);
                sCache[i] = localObject;
            }
        }
    }

    public static boolean equals(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt)
    {
        boolean bool = true;
        if (paramArrayOfByte1 == paramArrayOfByte2);
        label63: 
        while (true)
        {
            return bool;
            if ((paramArrayOfByte1 == null) || (paramArrayOfByte2 == null) || (paramArrayOfByte1.length < paramInt) || (paramArrayOfByte2.length < paramInt))
                bool = false;
            else
                for (int i = 0; ; i++)
                {
                    if (i >= paramInt)
                        break label63;
                    if (paramArrayOfByte1[i] != paramArrayOfByte2[i])
                    {
                        bool = false;
                        break;
                    }
                }
        }
    }

    public static int idealBooleanArraySize(int paramInt)
    {
        return idealByteArraySize(paramInt);
    }

    public static int idealByteArraySize(int paramInt)
    {
        for (int i = 4; ; i++)
            if (i < 32)
            {
                if (paramInt <= -12 + (1 << i))
                    paramInt = -12 + (1 << i);
            }
            else
                return paramInt;
    }

    public static int idealCharArraySize(int paramInt)
    {
        return idealByteArraySize(paramInt * 2) / 2;
    }

    public static int idealFloatArraySize(int paramInt)
    {
        return idealByteArraySize(paramInt * 4) / 4;
    }

    public static int idealIntArraySize(int paramInt)
    {
        return idealByteArraySize(paramInt * 4) / 4;
    }

    public static int idealLongArraySize(int paramInt)
    {
        return idealByteArraySize(paramInt * 8) / 8;
    }

    public static int idealObjectArraySize(int paramInt)
    {
        return idealByteArraySize(paramInt * 4) / 4;
    }

    public static int idealShortArraySize(int paramInt)
    {
        return idealByteArraySize(paramInt * 2) / 2;
    }

    public static <T> T[] removeElement(Class<T> paramClass, T[] paramArrayOfT, T paramT)
    {
        int i;
        int j;
        Object localObject;
        if (paramArrayOfT != null)
        {
            i = paramArrayOfT.length;
            j = 0;
            if (j < i)
                if (paramArrayOfT[j] == paramT)
                    if (i == 1)
                        localObject = null;
        }
        while (true)
        {
            return localObject;
            localObject = (Object[])Array.newInstance(paramClass, i - 1);
            System.arraycopy(paramArrayOfT, 0, localObject, 0, j);
            System.arraycopy(paramArrayOfT, j + 1, localObject, j, -1 + (i - j));
            continue;
            j++;
            break;
            localObject = paramArrayOfT;
        }
    }

    public static int[] removeInt(int[] paramArrayOfInt, int paramInt)
    {
        int[] arrayOfInt;
        if (paramArrayOfInt == null)
            arrayOfInt = null;
        while (true)
        {
            return arrayOfInt;
            int i = paramArrayOfInt.length;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label79;
                if (paramArrayOfInt[j] == paramInt)
                {
                    arrayOfInt = new int[i - 1];
                    if (j > 0)
                        System.arraycopy(paramArrayOfInt, 0, arrayOfInt, 0, j);
                    if (j >= i - 1)
                        break;
                    System.arraycopy(paramArrayOfInt, j + 1, arrayOfInt, j, -1 + (i - j));
                    break;
                }
            }
            label79: arrayOfInt = paramArrayOfInt;
        }
    }

    public static long total(long[] paramArrayOfLong)
    {
        long l = 0L;
        int i = paramArrayOfLong.length;
        for (int j = 0; j < i; j++)
            l += paramArrayOfLong[j];
        return l;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.ArrayUtils
 * JD-Core Version:        0.6.2
 */