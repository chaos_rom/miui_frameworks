package com.android.internal.util;

public class MemInfoReader
{
    byte[] mBuffer = new byte[1024];
    private long mCachedSize;
    private long mFreeSize;
    private long mTotalSize;

    private long extractMemValue(byte[] paramArrayOfByte, int paramInt)
    {
        int i;
        int j;
        if ((paramInt < paramArrayOfByte.length) && (paramArrayOfByte[paramInt] != 10))
            if ((paramArrayOfByte[paramInt] >= 48) && (paramArrayOfByte[paramInt] <= 57))
            {
                i = paramInt;
                for (j = paramInt + 1; (j < paramArrayOfByte.length) && (paramArrayOfByte[j] >= 48) && (paramArrayOfByte[j] <= 57); j++);
            }
        for (long l = 1024L * Integer.parseInt(new String(paramArrayOfByte, 0, i, j - i)); ; l = 0L)
        {
            return l;
            paramInt++;
            break;
        }
    }

    private boolean matchText(byte[] paramArrayOfByte, int paramInt, String paramString)
    {
        boolean bool = false;
        int i = paramString.length();
        if (paramInt + i >= paramArrayOfByte.length);
        while (true)
        {
            return bool;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label52;
                if (paramArrayOfByte[(paramInt + j)] != paramString.charAt(j))
                    break;
            }
            label52: bool = true;
        }
    }

    public long getCachedSize()
    {
        return this.mCachedSize;
    }

    public long getFreeSize()
    {
        return this.mFreeSize;
    }

    public long getTotalSize()
    {
        return this.mTotalSize;
    }

    // ERROR //
    public void readMemInfo()
    {
        // Byte code:
        //     0: invokestatic 62	android/os/StrictMode:allowThreadDiskReads	()Landroid/os/StrictMode$ThreadPolicy;
        //     3: astore_1
        //     4: aload_0
        //     5: lconst_0
        //     6: putfield 51	com/android/internal/util/MemInfoReader:mTotalSize	J
        //     9: aload_0
        //     10: lconst_0
        //     11: putfield 48	com/android/internal/util/MemInfoReader:mFreeSize	J
        //     14: aload_0
        //     15: lconst_0
        //     16: putfield 45	com/android/internal/util/MemInfoReader:mCachedSize	J
        //     19: new 64	java/io/FileInputStream
        //     22: dup
        //     23: ldc 66
        //     25: invokespecial 69	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     28: astore 5
        //     30: aload 5
        //     32: aload_0
        //     33: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     36: invokevirtual 73	java/io/FileInputStream:read	([B)I
        //     39: istore 6
        //     41: aload 5
        //     43: invokevirtual 76	java/io/FileInputStream:close	()V
        //     46: aload_0
        //     47: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     50: arraylength
        //     51: istore 7
        //     53: iconst_0
        //     54: istore 8
        //     56: iconst_0
        //     57: istore 9
        //     59: iload 9
        //     61: iload 6
        //     63: if_icmpge +151 -> 214
        //     66: iload 8
        //     68: iconst_3
        //     69: if_icmpge +145 -> 214
        //     72: aload_0
        //     73: aload_0
        //     74: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     77: iload 9
        //     79: ldc 78
        //     81: invokespecial 80	com/android/internal/util/MemInfoReader:matchText	([BILjava/lang/String;)Z
        //     84: ifeq +48 -> 132
        //     87: iinc 9 8
        //     90: aload_0
        //     91: aload_0
        //     92: aload_0
        //     93: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     96: iload 9
        //     98: invokespecial 82	com/android/internal/util/MemInfoReader:extractMemValue	([BI)J
        //     101: putfield 51	com/android/internal/util/MemInfoReader:mTotalSize	J
        //     104: iinc 8 1
        //     107: iload 9
        //     109: iload 7
        //     111: if_icmpge +97 -> 208
        //     114: aload_0
        //     115: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     118: iload 9
        //     120: baload
        //     121: bipush 10
        //     123: if_icmpeq +85 -> 208
        //     126: iinc 9 1
        //     129: goto -22 -> 107
        //     132: aload_0
        //     133: aload_0
        //     134: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     137: iload 9
        //     139: ldc 84
        //     141: invokespecial 80	com/android/internal/util/MemInfoReader:matchText	([BILjava/lang/String;)Z
        //     144: ifeq +26 -> 170
        //     147: iinc 9 7
        //     150: aload_0
        //     151: aload_0
        //     152: aload_0
        //     153: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     156: iload 9
        //     158: invokespecial 82	com/android/internal/util/MemInfoReader:extractMemValue	([BI)J
        //     161: putfield 48	com/android/internal/util/MemInfoReader:mFreeSize	J
        //     164: iinc 8 1
        //     167: goto -60 -> 107
        //     170: aload_0
        //     171: aload_0
        //     172: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     175: iload 9
        //     177: ldc 86
        //     179: invokespecial 80	com/android/internal/util/MemInfoReader:matchText	([BILjava/lang/String;)Z
        //     182: ifeq -75 -> 107
        //     185: iinc 9 6
        //     188: aload_0
        //     189: aload_0
        //     190: aload_0
        //     191: getfield 16	com/android/internal/util/MemInfoReader:mBuffer	[B
        //     194: iload 9
        //     196: invokespecial 82	com/android/internal/util/MemInfoReader:extractMemValue	([BI)J
        //     199: putfield 45	com/android/internal/util/MemInfoReader:mCachedSize	J
        //     202: iinc 8 1
        //     205: goto -98 -> 107
        //     208: iinc 9 1
        //     211: goto -152 -> 59
        //     214: aload_1
        //     215: invokestatic 90	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     218: return
        //     219: astore 4
        //     221: aload_1
        //     222: invokestatic 90	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     225: goto -7 -> 218
        //     228: astore_3
        //     229: aload_1
        //     230: invokestatic 90	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     233: goto -15 -> 218
        //     236: astore_2
        //     237: aload_1
        //     238: invokestatic 90	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     241: aload_2
        //     242: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     4	202	219	java/io/FileNotFoundException
        //     4	202	228	java/io/IOException
        //     4	202	236	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.MemInfoReader
 * JD-Core Version:        0.6.2
 */