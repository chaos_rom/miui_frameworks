package com.android.internal.util;

import android.os.FileUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import libcore.io.IoUtils;
import libcore.io.Streams;

public class FileRotator
{
    private static final boolean LOGD = false;
    private static final String SUFFIX_BACKUP = ".backup";
    private static final String SUFFIX_NO_BACKUP = ".no_backup";
    private static final String TAG = "FileRotator";
    private final File mBasePath;
    private final long mDeleteAgeMillis;
    private final String mPrefix;
    private final long mRotateAgeMillis;

    public FileRotator(File paramFile, String paramString, long paramLong1, long paramLong2)
    {
        this.mBasePath = ((File)Preconditions.checkNotNull(paramFile));
        this.mPrefix = ((String)Preconditions.checkNotNull(paramString));
        this.mRotateAgeMillis = paramLong1;
        this.mDeleteAgeMillis = paramLong2;
        this.mBasePath.mkdirs();
        String[] arrayOfString = this.mBasePath.list();
        int i = arrayOfString.length;
        int j = 0;
        if (j < i)
        {
            String str = arrayOfString[j];
            if (!str.startsWith(this.mPrefix));
            while (true)
            {
                j++;
                break;
                if (str.endsWith(".backup"))
                {
                    new File(this.mBasePath, str).renameTo(new File(this.mBasePath, str.substring(0, str.length() - ".backup".length())));
                }
                else if (str.endsWith(".no_backup"))
                {
                    File localFile1 = new File(this.mBasePath, str);
                    File localFile2 = new File(this.mBasePath, str.substring(0, str.length() - ".no_backup".length()));
                    localFile1.delete();
                    localFile2.delete();
                }
            }
        }
    }

    private String getActiveName(long paramLong)
    {
        Object localObject = null;
        long l = 9223372036854775807L;
        FileInfo localFileInfo = new FileInfo(this.mPrefix);
        String[] arrayOfString = this.mBasePath.list();
        int i = arrayOfString.length;
        int j = 0;
        if (j < i)
        {
            String str = arrayOfString[j];
            if (!localFileInfo.parse(str));
            while (true)
            {
                j++;
                break;
                if ((localFileInfo.isActive()) && (localFileInfo.startMillis < paramLong) && (localFileInfo.startMillis < l))
                {
                    localObject = str;
                    l = localFileInfo.startMillis;
                }
            }
        }
        if (localObject != null);
        while (true)
        {
            return localObject;
            localFileInfo.startMillis = paramLong;
            localFileInfo.endMillis = 9223372036854775807L;
            localObject = localFileInfo.build();
        }
    }

    private static void readFile(File paramFile, Reader paramReader)
        throws IOException
    {
        BufferedInputStream localBufferedInputStream = new BufferedInputStream(new FileInputStream(paramFile));
        try
        {
            paramReader.read(localBufferedInputStream);
            return;
        }
        finally
        {
            IoUtils.closeQuietly(localBufferedInputStream);
        }
    }

    private static IOException rethrowAsIoException(Throwable paramThrowable)
        throws IOException
    {
        if ((paramThrowable instanceof IOException))
            throw ((IOException)paramThrowable);
        throw new IOException(paramThrowable.getMessage(), paramThrowable);
    }

    private void rewriteSingle(Rewriter paramRewriter, String paramString)
        throws IOException
    {
        File localFile1 = new File(this.mBasePath, paramString);
        paramRewriter.reset();
        if (localFile1.exists())
        {
            readFile(localFile1, paramRewriter);
            if (paramRewriter.shouldWrite());
        }
        while (true)
        {
            return;
            File localFile3 = new File(this.mBasePath, paramString + ".backup");
            localFile1.renameTo(localFile3);
            try
            {
                writeFile(localFile1, paramRewriter);
                localFile3.delete();
            }
            catch (Throwable localThrowable2)
            {
                localFile1.delete();
                localFile3.renameTo(localFile1);
                throw rethrowAsIoException(localThrowable2);
            }
            File localFile2 = new File(this.mBasePath, paramString + ".no_backup");
            localFile2.createNewFile();
            try
            {
                writeFile(localFile1, paramRewriter);
                localFile2.delete();
            }
            catch (Throwable localThrowable1)
            {
                localFile1.delete();
                localFile2.delete();
                throw rethrowAsIoException(localThrowable1);
            }
        }
    }

    private static void writeFile(File paramFile, Writer paramWriter)
        throws IOException
    {
        FileOutputStream localFileOutputStream = new FileOutputStream(paramFile);
        BufferedOutputStream localBufferedOutputStream = new BufferedOutputStream(localFileOutputStream);
        try
        {
            paramWriter.write(localBufferedOutputStream);
            localBufferedOutputStream.flush();
            return;
        }
        finally
        {
            FileUtils.sync(localFileOutputStream);
            IoUtils.closeQuietly(localBufferedOutputStream);
        }
    }

    @Deprecated
    public void combineActive(final Reader paramReader, final Writer paramWriter, long paramLong)
        throws IOException
    {
        rewriteActive(new Rewriter()
        {
            public void read(InputStream paramAnonymousInputStream)
                throws IOException
            {
                paramReader.read(paramAnonymousInputStream);
            }

            public void reset()
            {
            }

            public boolean shouldWrite()
            {
                return true;
            }

            public void write(OutputStream paramAnonymousOutputStream)
                throws IOException
            {
                paramWriter.write(paramAnonymousOutputStream);
            }
        }
        , paramLong);
    }

    public void deleteAll()
    {
        FileInfo localFileInfo = new FileInfo(this.mPrefix);
        for (String str : this.mBasePath.list())
            if (localFileInfo.parse(str))
                new File(this.mBasePath, str).delete();
    }

    public void dumpAll(OutputStream paramOutputStream)
        throws IOException
    {
        ZipOutputStream localZipOutputStream = new ZipOutputStream(paramOutputStream);
        while (true)
        {
            int j;
            try
            {
                FileInfo localFileInfo = new FileInfo(this.mPrefix);
                String[] arrayOfString = this.mBasePath.list();
                int i = arrayOfString.length;
                j = 0;
                if (j < i)
                {
                    String str = arrayOfString[j];
                    if (!localFileInfo.parse(str))
                        break label139;
                    localZipOutputStream.putNextEntry(new ZipEntry(str));
                    localFileInputStream = new FileInputStream(new File(this.mBasePath, str));
                }
            }
            finally
            {
                try
                {
                    Streams.copy(localFileInputStream, localZipOutputStream);
                    IoUtils.closeQuietly(localFileInputStream);
                    localZipOutputStream.closeEntry();
                }
                finally
                {
                    FileInputStream localFileInputStream;
                    IoUtils.closeQuietly(localFileInputStream);
                }
                IoUtils.closeQuietly(localZipOutputStream);
            }
            IoUtils.closeQuietly(localZipOutputStream);
            return;
            label139: j++;
        }
    }

    public void maybeRotate(long paramLong)
    {
        long l1 = paramLong - this.mRotateAgeMillis;
        long l2 = paramLong - this.mDeleteAgeMillis;
        FileInfo localFileInfo = new FileInfo(this.mPrefix);
        String[] arrayOfString = this.mBasePath.list();
        int i = arrayOfString.length;
        int j = 0;
        if (j < i)
        {
            String str = arrayOfString[j];
            if (!localFileInfo.parse(str));
            while (true)
            {
                j++;
                break;
                if (localFileInfo.isActive())
                {
                    if (localFileInfo.startMillis <= l1)
                    {
                        localFileInfo.endMillis = paramLong;
                        new File(this.mBasePath, str).renameTo(new File(this.mBasePath, localFileInfo.build()));
                    }
                }
                else if (localFileInfo.endMillis <= l2)
                    new File(this.mBasePath, str).delete();
            }
        }
    }

    public void readMatching(Reader paramReader, long paramLong1, long paramLong2)
        throws IOException
    {
        FileInfo localFileInfo = new FileInfo(this.mPrefix);
        String[] arrayOfString = this.mBasePath.list();
        int i = arrayOfString.length;
        int j = 0;
        if (j < i)
        {
            String str = arrayOfString[j];
            if (!localFileInfo.parse(str));
            while (true)
            {
                j++;
                break;
                if ((localFileInfo.startMillis <= paramLong2) && (paramLong1 <= localFileInfo.endMillis))
                    readFile(new File(this.mBasePath, str), paramReader);
            }
        }
    }

    public void rewriteActive(Rewriter paramRewriter, long paramLong)
        throws IOException
    {
        rewriteSingle(paramRewriter, getActiveName(paramLong));
    }

    public void rewriteAll(Rewriter paramRewriter)
        throws IOException
    {
        FileInfo localFileInfo = new FileInfo(this.mPrefix);
        String[] arrayOfString = this.mBasePath.list();
        int i = arrayOfString.length;
        int j = 0;
        if (j < i)
        {
            String str = arrayOfString[j];
            if (!localFileInfo.parse(str));
            while (true)
            {
                j++;
                break;
                rewriteSingle(paramRewriter, str);
            }
        }
    }

    private static class FileInfo
    {
        public long endMillis;
        public final String prefix;
        public long startMillis;

        public FileInfo(String paramString)
        {
            this.prefix = ((String)Preconditions.checkNotNull(paramString));
        }

        public String build()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append(this.prefix).append('.').append(this.startMillis).append('-');
            if (this.endMillis != 9223372036854775807L)
                localStringBuilder.append(this.endMillis);
            return localStringBuilder.toString();
        }

        public boolean isActive()
        {
            if (this.endMillis == 9223372036854775807L);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean parse(String paramString)
        {
            boolean bool = false;
            this.endMillis = -1L;
            this.startMillis = -1L;
            int i = paramString.lastIndexOf('.');
            int j = paramString.lastIndexOf('-');
            if ((i == -1) || (j == -1));
            while (true)
            {
                return bool;
                if (this.prefix.equals(paramString.substring(0, i)))
                {
                    int k = i + 1;
                    try
                    {
                        this.startMillis = Long.parseLong(paramString.substring(k, j));
                        if (paramString.length() - j == 1)
                            this.endMillis = 9223372036854775807L;
                        else
                            this.endMillis = Long.parseLong(paramString.substring(j + 1));
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                    }
                    continue;
                    bool = true;
                }
            }
        }
    }

    public static abstract interface Rewriter extends FileRotator.Reader, FileRotator.Writer
    {
        public abstract void reset();

        public abstract boolean shouldWrite();
    }

    public static abstract interface Writer
    {
        public abstract void write(OutputStream paramOutputStream)
            throws IOException;
    }

    public static abstract interface Reader
    {
        public abstract void read(InputStream paramInputStream)
            throws IOException;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.FileRotator
 * JD-Core Version:        0.6.2
 */