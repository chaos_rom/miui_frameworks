package com.android.internal.util;

import android.os.Message;

public class State
    implements IState
{
    public void enter()
    {
    }

    public void exit()
    {
    }

    public String getName()
    {
        String str = getClass().getName();
        return str.substring(1 + str.lastIndexOf('$'));
    }

    public boolean processMessage(Message paramMessage)
    {
        return false;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.State
 * JD-Core Version:        0.6.2
 */