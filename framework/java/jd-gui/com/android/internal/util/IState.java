package com.android.internal.util;

import android.os.Message;

public abstract interface IState
{
    public static final boolean HANDLED = true;
    public static final boolean NOT_HANDLED;

    public abstract void enter();

    public abstract void exit();

    public abstract String getName();

    public abstract boolean processMessage(Message paramMessage);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.IState
 * JD-Core Version:        0.6.2
 */