package com.android.internal.util;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class StateMachine
{
    public static final boolean HANDLED = true;
    public static final boolean NOT_HANDLED = false;
    public static final int SM_INIT_CMD = -2;
    public static final int SM_QUIT_CMD = -1;
    private static final String TAG = "StateMachine";
    private String mName;
    private SmHandler mSmHandler;
    private HandlerThread mSmThread;

    protected StateMachine(String paramString)
    {
        this.mSmThread = new HandlerThread(paramString);
        this.mSmThread.start();
        initStateMachine(paramString, this.mSmThread.getLooper());
    }

    protected StateMachine(String paramString, Looper paramLooper)
    {
        initStateMachine(paramString, paramLooper);
    }

    private void initStateMachine(String paramString, Looper paramLooper)
    {
        this.mName = paramString;
        this.mSmHandler = new SmHandler(paramLooper, this, null);
    }

    protected final void addState(State paramState)
    {
        this.mSmHandler.addState(paramState, null);
    }

    protected final void addState(State paramState1, State paramState2)
    {
        this.mSmHandler.addState(paramState1, paramState2);
    }

    protected final void deferMessage(Message paramMessage)
    {
        this.mSmHandler.deferMessage(paramMessage);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println(getName() + ":");
        paramPrintWriter.println(" total messages=" + getProcessedMessagesCount());
        for (int i = 0; i < getProcessedMessagesSize(); i++)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = Integer.valueOf(i);
            arrayOfObject[1] = getProcessedMessageInfo(i);
            paramPrintWriter.printf(" msg[%d]: %s\n", arrayOfObject);
            paramPrintWriter.flush();
        }
        paramPrintWriter.println("curState=" + getCurrentState().getName());
    }

    protected final Message getCurrentMessage()
    {
        return this.mSmHandler.getCurrentMessage();
    }

    protected final IState getCurrentState()
    {
        return this.mSmHandler.getCurrentState();
    }

    public final Handler getHandler()
    {
        return this.mSmHandler;
    }

    protected String getMessageInfo(Message paramMessage)
    {
        return "";
    }

    public final String getName()
    {
        return this.mName;
    }

    public final ProcessedMessageInfo getProcessedMessageInfo(int paramInt)
    {
        return this.mSmHandler.getProcessedMessageInfo(paramInt);
    }

    public final int getProcessedMessagesCount()
    {
        return this.mSmHandler.getProcessedMessagesCount();
    }

    public final int getProcessedMessagesSize()
    {
        return this.mSmHandler.getProcessedMessagesSize();
    }

    protected void haltedProcessMessage(Message paramMessage)
    {
    }

    protected void halting()
    {
    }

    public boolean isDbg()
    {
        if (this.mSmHandler == null);
        for (boolean bool = false; ; bool = this.mSmHandler.isDbg())
            return bool;
    }

    protected final boolean isQuit(Message paramMessage)
    {
        return this.mSmHandler.isQuit(paramMessage);
    }

    public final Message obtainMessage()
    {
        if (this.mSmHandler == null);
        for (Message localMessage = null; ; localMessage = Message.obtain(this.mSmHandler))
            return localMessage;
    }

    public final Message obtainMessage(int paramInt)
    {
        if (this.mSmHandler == null);
        for (Message localMessage = null; ; localMessage = Message.obtain(this.mSmHandler, paramInt))
            return localMessage;
    }

    public final Message obtainMessage(int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mSmHandler == null);
        for (Message localMessage = null; ; localMessage = Message.obtain(this.mSmHandler, paramInt1, paramInt2, paramInt3))
            return localMessage;
    }

    public final Message obtainMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        if (this.mSmHandler == null);
        for (Message localMessage = null; ; localMessage = Message.obtain(this.mSmHandler, paramInt1, paramInt2, paramInt3, paramObject))
            return localMessage;
    }

    public final Message obtainMessage(int paramInt, Object paramObject)
    {
        if (this.mSmHandler == null);
        for (Message localMessage = null; ; localMessage = Message.obtain(this.mSmHandler, paramInt, paramObject))
            return localMessage;
    }

    public final void quit()
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.quit();
        }
    }

    protected void quitting()
    {
    }

    protected boolean recordProcessedMessage(Message paramMessage)
    {
        return true;
    }

    protected final void removeMessages(int paramInt)
    {
        this.mSmHandler.removeMessages(paramInt);
    }

    public final void sendMessage(int paramInt)
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.sendMessage(obtainMessage(paramInt));
        }
    }

    public final void sendMessage(int paramInt, Object paramObject)
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.sendMessage(obtainMessage(paramInt, paramObject));
        }
    }

    public final void sendMessage(Message paramMessage)
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.sendMessage(paramMessage);
        }
    }

    protected final void sendMessageAtFrontOfQueue(int paramInt)
    {
        this.mSmHandler.sendMessageAtFrontOfQueue(obtainMessage(paramInt));
    }

    protected final void sendMessageAtFrontOfQueue(int paramInt, Object paramObject)
    {
        this.mSmHandler.sendMessageAtFrontOfQueue(obtainMessage(paramInt, paramObject));
    }

    protected final void sendMessageAtFrontOfQueue(Message paramMessage)
    {
        this.mSmHandler.sendMessageAtFrontOfQueue(paramMessage);
    }

    public final void sendMessageDelayed(int paramInt, long paramLong)
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.sendMessageDelayed(obtainMessage(paramInt), paramLong);
        }
    }

    public final void sendMessageDelayed(int paramInt, Object paramObject, long paramLong)
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.sendMessageDelayed(obtainMessage(paramInt, paramObject), paramLong);
        }
    }

    public final void sendMessageDelayed(Message paramMessage, long paramLong)
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.sendMessageDelayed(paramMessage, paramLong);
        }
    }

    public void setDbg(boolean paramBoolean)
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.setDbg(paramBoolean);
        }
    }

    protected final void setInitialState(State paramState)
    {
        this.mSmHandler.setInitialState(paramState);
    }

    public final void setProcessedMessagesSize(int paramInt)
    {
        this.mSmHandler.setProcessedMessagesSize(paramInt);
    }

    public void start()
    {
        if (this.mSmHandler == null);
        while (true)
        {
            return;
            this.mSmHandler.completeConstruction();
        }
    }

    protected final void transitionTo(IState paramIState)
    {
        this.mSmHandler.transitionTo(paramIState);
    }

    protected final void transitionToHaltingState()
    {
        this.mSmHandler.transitionTo(this.mSmHandler.mHaltingState);
    }

    protected void unhandledMessage(Message paramMessage)
    {
        if (this.mSmHandler.mDbg)
            Log.e("StateMachine", this.mName + " - unhandledMessage: msg.what=" + paramMessage.what);
    }

    private static class SmHandler extends Handler
    {
        private static final Object mSmHandlerObj = new Object();
        private boolean mDbg = false;
        private ArrayList<Message> mDeferredMessages = new ArrayList();
        private State mDestState;
        private HaltingState mHaltingState = new HaltingState(null);
        private State mInitialState;
        private boolean mIsConstructionCompleted;
        private Message mMsg;
        private StateMachine.ProcessedMessages mProcessedMessages = new StateMachine.ProcessedMessages(null);
        private QuittingState mQuittingState = new QuittingState(null);
        private StateMachine mSm;
        private HashMap<State, StateInfo> mStateInfo = new HashMap();
        private StateInfo[] mStateStack;
        private int mStateStackTopIndex = -1;
        private StateInfo[] mTempStateStack;
        private int mTempStateStackCount;

        private SmHandler(Looper paramLooper, StateMachine paramStateMachine)
        {
            super();
            this.mSm = paramStateMachine;
            addState(this.mHaltingState, null);
            addState(this.mQuittingState, null);
        }

        private final StateInfo addState(State paramState1, State paramState2)
        {
            StringBuilder localStringBuilder;
            if (this.mDbg)
            {
                localStringBuilder = new StringBuilder().append("addStateInternal: E state=").append(paramState1.getName()).append(",parent=");
                if (paramState2 != null)
                    break label153;
            }
            StateInfo localStateInfo1;
            StateInfo localStateInfo2;
            label153: for (String str = ""; ; str = paramState2.getName())
            {
                Log.d("StateMachine", str);
                localStateInfo1 = null;
                if (paramState2 != null)
                {
                    localStateInfo1 = (StateInfo)this.mStateInfo.get(paramState2);
                    if (localStateInfo1 == null)
                        localStateInfo1 = addState(paramState2, null);
                }
                localStateInfo2 = (StateInfo)this.mStateInfo.get(paramState1);
                if (localStateInfo2 == null)
                {
                    localStateInfo2 = new StateInfo(null);
                    this.mStateInfo.put(paramState1, localStateInfo2);
                }
                if ((localStateInfo2.parentStateInfo == null) || (localStateInfo2.parentStateInfo == localStateInfo1))
                    break;
                throw new RuntimeException("state already added");
            }
            localStateInfo2.state = paramState1;
            localStateInfo2.parentStateInfo = localStateInfo1;
            localStateInfo2.active = false;
            if (this.mDbg)
                Log.d("StateMachine", "addStateInternal: X stateInfo: " + localStateInfo2);
            return localStateInfo2;
        }

        private final void cleanupAfterQuitting()
        {
            this.mSm.quitting();
            if (this.mSm.mSmThread != null)
            {
                getLooper().quit();
                StateMachine.access$402(this.mSm, null);
            }
            StateMachine.access$502(this.mSm, null);
            this.mSm = null;
            this.mMsg = null;
            this.mProcessedMessages.cleanup();
            this.mStateStack = null;
            this.mTempStateStack = null;
            this.mStateInfo.clear();
            this.mInitialState = null;
            this.mDestState = null;
            this.mDeferredMessages.clear();
        }

        private final void completeConstruction()
        {
            if (this.mDbg)
                Log.d("StateMachine", "completeConstruction: E");
            int i = 0;
            Iterator localIterator = this.mStateInfo.values().iterator();
            while (localIterator.hasNext())
            {
                StateInfo localStateInfo1 = (StateInfo)localIterator.next();
                int j = 0;
                StateInfo localStateInfo2 = localStateInfo1;
                while (localStateInfo2 != null)
                {
                    localStateInfo2 = localStateInfo2.parentStateInfo;
                    j++;
                }
                if (i < j)
                    i = j;
            }
            if (this.mDbg)
                Log.d("StateMachine", "completeConstruction: maxDepth=" + i);
            this.mStateStack = new StateInfo[i];
            this.mTempStateStack = new StateInfo[i];
            setupInitialStateStack();
            sendMessageAtFrontOfQueue(obtainMessage(-2, mSmHandlerObj));
            if (this.mDbg)
                Log.d("StateMachine", "completeConstruction: X");
        }

        private final void deferMessage(Message paramMessage)
        {
            if (this.mDbg)
                Log.d("StateMachine", "deferMessage: msg=" + paramMessage.what);
            Message localMessage = obtainMessage();
            localMessage.copyFrom(paramMessage);
            this.mDeferredMessages.add(localMessage);
        }

        private final Message getCurrentMessage()
        {
            return this.mMsg;
        }

        private final IState getCurrentState()
        {
            return this.mStateStack[this.mStateStackTopIndex].state;
        }

        private final StateMachine.ProcessedMessageInfo getProcessedMessageInfo(int paramInt)
        {
            return this.mProcessedMessages.get(paramInt);
        }

        private final int getProcessedMessagesCount()
        {
            return this.mProcessedMessages.count();
        }

        private final int getProcessedMessagesSize()
        {
            return this.mProcessedMessages.size();
        }

        private final void invokeEnterMethods(int paramInt)
        {
            for (int i = paramInt; i <= this.mStateStackTopIndex; i++)
            {
                if (this.mDbg)
                    Log.d("StateMachine", "invokeEnterMethods: " + this.mStateStack[i].state.getName());
                this.mStateStack[i].state.enter();
                this.mStateStack[i].active = true;
            }
        }

        private final void invokeExitMethods(StateInfo paramStateInfo)
        {
            while ((this.mStateStackTopIndex >= 0) && (this.mStateStack[this.mStateStackTopIndex] != paramStateInfo))
            {
                State localState = this.mStateStack[this.mStateStackTopIndex].state;
                if (this.mDbg)
                    Log.d("StateMachine", "invokeExitMethods: " + localState.getName());
                localState.exit();
                this.mStateStack[this.mStateStackTopIndex].active = false;
                this.mStateStackTopIndex = (-1 + this.mStateStackTopIndex);
            }
        }

        private final boolean isDbg()
        {
            return this.mDbg;
        }

        private final boolean isQuit(Message paramMessage)
        {
            if ((paramMessage.what == -1) && (paramMessage.obj == mSmHandlerObj));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private final void moveDeferredMessageAtFrontOfQueue()
        {
            for (int i = -1 + this.mDeferredMessages.size(); i >= 0; i--)
            {
                Message localMessage = (Message)this.mDeferredMessages.get(i);
                if (this.mDbg)
                    Log.d("StateMachine", "moveDeferredMessageAtFrontOfQueue; what=" + localMessage.what);
                sendMessageAtFrontOfQueue(localMessage);
            }
            this.mDeferredMessages.clear();
        }

        private final int moveTempStateStackToStateStack()
        {
            int i = 1 + this.mStateStackTopIndex;
            int j = -1 + this.mTempStateStackCount;
            int k = i;
            while (j >= 0)
            {
                if (this.mDbg)
                    Log.d("StateMachine", "moveTempStackToStateStack: i=" + j + ",j=" + k);
                this.mStateStack[k] = this.mTempStateStack[j];
                k++;
                j--;
            }
            this.mStateStackTopIndex = (k - 1);
            if (this.mDbg)
                Log.d("StateMachine", "moveTempStackToStateStack: X mStateStackTop=" + this.mStateStackTopIndex + ",startingIndex=" + i + ",Top=" + this.mStateStack[this.mStateStackTopIndex].state.getName());
            return i;
        }

        private void performTransitions()
        {
            State localState = null;
            while (this.mDestState != null)
            {
                if (this.mDbg)
                    Log.d("StateMachine", "handleMessage: new destination call exit");
                localState = this.mDestState;
                this.mDestState = null;
                invokeExitMethods(setupTempStateStackWithStatesToEnter(localState));
                invokeEnterMethods(moveTempStateStackToStateStack());
                moveDeferredMessageAtFrontOfQueue();
            }
            if (localState != null)
            {
                if (localState != this.mQuittingState)
                    break label76;
                cleanupAfterQuitting();
            }
            while (true)
            {
                return;
                label76: if (localState == this.mHaltingState)
                    this.mSm.halting();
            }
        }

        private final void processMsg(Message paramMessage)
        {
            StateInfo localStateInfo = this.mStateStack[this.mStateStackTopIndex];
            if (this.mDbg)
                Log.d("StateMachine", "processMsg: " + localStateInfo.state.getName());
            label143: label185: label204: 
            while (true)
            {
                if (!localStateInfo.state.processMessage(paramMessage))
                {
                    localStateInfo = localStateInfo.parentStateInfo;
                    if (localStateInfo != null)
                        break label143;
                    this.mSm.unhandledMessage(paramMessage);
                    if (isQuit(paramMessage))
                        transitionTo(this.mQuittingState);
                }
                if (this.mSm.recordProcessedMessage(paramMessage))
                {
                    if (localStateInfo == null)
                        break label185;
                    State localState = this.mStateStack[this.mStateStackTopIndex].state;
                    this.mProcessedMessages.add(paramMessage, this.mSm.getMessageInfo(paramMessage), localStateInfo.state, localState);
                }
                while (true)
                {
                    return;
                    if (!this.mDbg)
                        break label204;
                    Log.d("StateMachine", "processMsg: " + localStateInfo.state.getName());
                    break;
                    this.mProcessedMessages.add(paramMessage, this.mSm.getMessageInfo(paramMessage), null, null);
                }
            }
        }

        private final void quit()
        {
            if (this.mDbg)
                Log.d("StateMachine", "quit:");
            sendMessage(obtainMessage(-1, mSmHandlerObj));
        }

        private final void setDbg(boolean paramBoolean)
        {
            this.mDbg = paramBoolean;
        }

        private final void setInitialState(State paramState)
        {
            if (this.mDbg)
                Log.d("StateMachine", "setInitialState: initialState=" + paramState.getName());
            this.mInitialState = paramState;
        }

        private final void setProcessedMessagesSize(int paramInt)
        {
            this.mProcessedMessages.setSize(paramInt);
        }

        private final void setupInitialStateStack()
        {
            if (this.mDbg)
                Log.d("StateMachine", "setupInitialStateStack: E mInitialState=" + this.mInitialState.getName());
            StateInfo localStateInfo = (StateInfo)this.mStateInfo.get(this.mInitialState);
            for (this.mTempStateStackCount = 0; localStateInfo != null; this.mTempStateStackCount = (1 + this.mTempStateStackCount))
            {
                this.mTempStateStack[this.mTempStateStackCount] = localStateInfo;
                localStateInfo = localStateInfo.parentStateInfo;
            }
            this.mStateStackTopIndex = -1;
            moveTempStateStackToStateStack();
        }

        private final StateInfo setupTempStateStackWithStatesToEnter(State paramState)
        {
            this.mTempStateStackCount = 0;
            StateInfo localStateInfo = (StateInfo)this.mStateInfo.get(paramState);
            do
            {
                StateInfo[] arrayOfStateInfo = this.mTempStateStack;
                int i = this.mTempStateStackCount;
                this.mTempStateStackCount = (i + 1);
                arrayOfStateInfo[i] = localStateInfo;
                localStateInfo = localStateInfo.parentStateInfo;
            }
            while ((localStateInfo != null) && (!localStateInfo.active));
            if (this.mDbg)
                Log.d("StateMachine", "setupTempStateStackWithStatesToEnter: X mTempStateStackCount=" + this.mTempStateStackCount + ",curStateInfo: " + localStateInfo);
            return localStateInfo;
        }

        private final void transitionTo(IState paramIState)
        {
            this.mDestState = ((State)paramIState);
            if (this.mDbg)
                Log.d("StateMachine", "transitionTo: destState=" + this.mDestState.getName());
        }

        public final void handleMessage(Message paramMessage)
        {
            if (this.mDbg)
                Log.d("StateMachine", "handleMessage: E msg.what=" + paramMessage.what);
            this.mMsg = paramMessage;
            if (this.mIsConstructionCompleted)
                processMsg(paramMessage);
            while (true)
            {
                performTransitions();
                if (this.mDbg)
                    Log.d("StateMachine", "handleMessage: X");
                return;
                if ((this.mIsConstructionCompleted) || (this.mMsg.what != -2) || (this.mMsg.obj != mSmHandlerObj))
                    break;
                this.mIsConstructionCompleted = true;
                invokeEnterMethods(0);
            }
            throw new RuntimeException("StateMachine.handleMessage: The start method not called, received msg: " + paramMessage);
        }

        private class QuittingState extends State
        {
            private QuittingState()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                return false;
            }
        }

        private class HaltingState extends State
        {
            private HaltingState()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                StateMachine.this.haltedProcessMessage(paramMessage);
                return true;
            }
        }

        private class StateInfo
        {
            boolean active;
            StateInfo parentStateInfo;
            State state;

            private StateInfo()
            {
            }

            public String toString()
            {
                StringBuilder localStringBuilder = new StringBuilder().append("state=").append(this.state.getName()).append(",active=").append(this.active).append(",parent=");
                if (this.parentStateInfo == null);
                for (String str = "null"; ; str = this.parentStateInfo.state.getName())
                    return str;
            }
        }
    }

    private static class ProcessedMessages
    {
        private static final int DEFAULT_SIZE = 20;
        private int mCount = 0;
        private int mMaxSize = 20;
        private Vector<StateMachine.ProcessedMessageInfo> mMessages = new Vector();
        private int mOldestIndex = 0;

        void add(Message paramMessage, String paramString, State paramState1, State paramState2)
        {
            this.mCount = (1 + this.mCount);
            if (this.mMessages.size() < this.mMaxSize)
                this.mMessages.add(new StateMachine.ProcessedMessageInfo(paramMessage, paramString, paramState1, paramState2));
            while (true)
            {
                return;
                StateMachine.ProcessedMessageInfo localProcessedMessageInfo = (StateMachine.ProcessedMessageInfo)this.mMessages.get(this.mOldestIndex);
                this.mOldestIndex = (1 + this.mOldestIndex);
                if (this.mOldestIndex >= this.mMaxSize)
                    this.mOldestIndex = 0;
                localProcessedMessageInfo.update(paramMessage, paramString, paramState1, paramState2);
            }
        }

        void cleanup()
        {
            this.mMessages.clear();
        }

        int count()
        {
            return this.mCount;
        }

        StateMachine.ProcessedMessageInfo get(int paramInt)
        {
            int i = paramInt + this.mOldestIndex;
            if (i >= this.mMaxSize)
                i -= this.mMaxSize;
            if (i >= size());
            for (StateMachine.ProcessedMessageInfo localProcessedMessageInfo = null; ; localProcessedMessageInfo = (StateMachine.ProcessedMessageInfo)this.mMessages.get(i))
                return localProcessedMessageInfo;
        }

        void setSize(int paramInt)
        {
            this.mMaxSize = paramInt;
            this.mCount = 0;
            this.mMessages.clear();
        }

        int size()
        {
            return this.mMessages.size();
        }
    }

    public static class ProcessedMessageInfo
    {
        private String mInfo;
        private State mOrgState;
        private State mState;
        private long mTime;
        private int mWhat;

        ProcessedMessageInfo(Message paramMessage, String paramString, State paramState1, State paramState2)
        {
            update(paramMessage, paramString, paramState1, paramState2);
        }

        public String getInfo()
        {
            return this.mInfo;
        }

        public State getOriginalState()
        {
            return this.mOrgState;
        }

        public State getState()
        {
            return this.mState;
        }

        public long getTime()
        {
            return this.mTime;
        }

        public long getWhat()
        {
            return this.mWhat;
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("time=");
            Calendar localCalendar = Calendar.getInstance();
            localCalendar.setTimeInMillis(this.mTime);
            Object[] arrayOfObject = new Object[6];
            arrayOfObject[0] = localCalendar;
            arrayOfObject[1] = localCalendar;
            arrayOfObject[2] = localCalendar;
            arrayOfObject[3] = localCalendar;
            arrayOfObject[4] = localCalendar;
            arrayOfObject[5] = localCalendar;
            localStringBuilder.append(String.format("%tm-%td %tH:%tM:%tS.%tL", arrayOfObject));
            localStringBuilder.append(" state=");
            String str1;
            if (this.mState == null)
            {
                str1 = "<null>";
                localStringBuilder.append(str1);
                localStringBuilder.append(" orgState=");
                if (this.mOrgState != null)
                    break label211;
            }
            label211: for (String str2 = "<null>"; ; str2 = this.mOrgState.getName())
            {
                localStringBuilder.append(str2);
                localStringBuilder.append(" what=");
                localStringBuilder.append(this.mWhat);
                localStringBuilder.append("(0x");
                localStringBuilder.append(Integer.toHexString(this.mWhat));
                localStringBuilder.append(")");
                if (!TextUtils.isEmpty(this.mInfo))
                {
                    localStringBuilder.append(" ");
                    localStringBuilder.append(this.mInfo);
                }
                return localStringBuilder.toString();
                str1 = this.mState.getName();
                break;
            }
        }

        public void update(Message paramMessage, String paramString, State paramState1, State paramState2)
        {
            this.mTime = System.currentTimeMillis();
            this.mWhat = paramMessage.what;
            this.mInfo = paramString;
            this.mState = paramState1;
            this.mOrgState = paramState2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.StateMachine
 * JD-Core Version:        0.6.2
 */