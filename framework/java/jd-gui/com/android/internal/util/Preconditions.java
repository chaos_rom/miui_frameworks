package com.android.internal.util;

public class Preconditions
{
    public static <T> T checkNotNull(T paramT)
    {
        if (paramT == null)
            throw new NullPointerException();
        return paramT;
    }

    public static <T> T checkNotNull(T paramT, Object paramObject)
    {
        if (paramT == null)
            throw new NullPointerException(String.valueOf(paramObject));
        return paramT;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.Preconditions
 * JD-Core Version:        0.6.2
 */