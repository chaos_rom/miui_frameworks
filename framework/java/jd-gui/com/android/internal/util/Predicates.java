package com.android.internal.util;

import java.util.Arrays;
import java.util.Iterator;

public class Predicates
{
    public static <T> Predicate<T> and(Iterable<? extends Predicate<? super T>> paramIterable)
    {
        return new AndPredicate(paramIterable, null);
    }

    public static <T> Predicate<T> and(Predicate<? super T>[] paramArrayOfPredicate)
    {
        return and(Arrays.asList(paramArrayOfPredicate));
    }

    public static <T> Predicate<T> not(Predicate<? super T> paramPredicate)
    {
        return new NotPredicate(paramPredicate, null);
    }

    public static <T> Predicate<T> or(Iterable<? extends Predicate<? super T>> paramIterable)
    {
        return new OrPredicate(paramIterable, null);
    }

    public static <T> Predicate<T> or(Predicate<? super T>[] paramArrayOfPredicate)
    {
        return or(Arrays.asList(paramArrayOfPredicate));
    }

    private static class NotPredicate<T>
        implements Predicate<T>
    {
        private final Predicate<? super T> predicate;

        private NotPredicate(Predicate<? super T> paramPredicate)
        {
            this.predicate = paramPredicate;
        }

        public boolean apply(T paramT)
        {
            if (!this.predicate.apply(paramT));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private static class OrPredicate<T>
        implements Predicate<T>
    {
        private final Iterable<? extends Predicate<? super T>> components;

        private OrPredicate(Iterable<? extends Predicate<? super T>> paramIterable)
        {
            this.components = paramIterable;
        }

        public boolean apply(T paramT)
        {
            Iterator localIterator = this.components.iterator();
            do
                if (!localIterator.hasNext())
                    break;
            while (!((Predicate)localIterator.next()).apply(paramT));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private static class AndPredicate<T>
        implements Predicate<T>
    {
        private final Iterable<? extends Predicate<? super T>> components;

        private AndPredicate(Iterable<? extends Predicate<? super T>> paramIterable)
        {
            this.components = paramIterable;
        }

        public boolean apply(T paramT)
        {
            Iterator localIterator = this.components.iterator();
            do
                if (!localIterator.hasNext())
                    break;
            while (((Predicate)localIterator.next()).apply(paramT));
            for (boolean bool = false; ; bool = true)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.Predicates
 * JD-Core Version:        0.6.2
 */