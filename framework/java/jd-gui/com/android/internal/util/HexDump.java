package com.android.internal.util;

public class HexDump
{
    private static final char[] HEX_DIGITS = arrayOfChar;

    static
    {
        char[] arrayOfChar = new char[16];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
        arrayOfChar[10] = 65;
        arrayOfChar[11] = 66;
        arrayOfChar[12] = 67;
        arrayOfChar[13] = 68;
        arrayOfChar[14] = 69;
        arrayOfChar[15] = 70;
    }

    public static String dumpHexString(byte[] paramArrayOfByte)
    {
        return dumpHexString(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public static String dumpHexString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        byte[] arrayOfByte = new byte[16];
        int i = 0;
        localStringBuilder.append("\n0x");
        localStringBuilder.append(toHexString(paramInt1));
        int j = paramInt1;
        while (j < paramInt1 + paramInt2)
        {
            if (i == 16)
            {
                localStringBuilder.append(" ");
                int i3 = 0;
                if (i3 < 16)
                {
                    if ((arrayOfByte[i3] > 32) && (arrayOfByte[i3] < 126))
                        localStringBuilder.append(new String(arrayOfByte, i3, 1));
                    while (true)
                    {
                        i3++;
                        break;
                        localStringBuilder.append(".");
                    }
                }
                localStringBuilder.append("\n0x");
                localStringBuilder.append(toHexString(j));
                i = 0;
            }
            int i1 = paramArrayOfByte[j];
            localStringBuilder.append(" ");
            localStringBuilder.append(HEX_DIGITS[(0xF & i1 >>> 4)]);
            localStringBuilder.append(HEX_DIGITS[(i1 & 0xF)]);
            int i2 = i + 1;
            arrayOfByte[i] = i1;
            j++;
            i = i2;
        }
        if (i != 16)
        {
            int k = 1 + 3 * (16 - i);
            for (int m = 0; m < k; m++)
                localStringBuilder.append(" ");
            int n = 0;
            if (n < i)
            {
                if ((arrayOfByte[n] > 32) && (arrayOfByte[n] < 126))
                    localStringBuilder.append(new String(arrayOfByte, n, 1));
                while (true)
                {
                    n++;
                    break;
                    localStringBuilder.append(".");
                }
            }
        }
        return localStringBuilder.toString();
    }

    public static byte[] hexStringToByteArray(String paramString)
    {
        int i = paramString.length();
        byte[] arrayOfByte = new byte[i / 2];
        for (int j = 0; j < i; j += 2)
            arrayOfByte[(j / 2)] = ((byte)(toByte(paramString.charAt(j)) << 4 | toByte(paramString.charAt(j + 1))));
        return arrayOfByte;
    }

    private static int toByte(char paramChar)
    {
        int i;
        if ((paramChar >= '0') && (paramChar <= '9'))
            i = paramChar + '\0*0';
        while (true)
        {
            return i;
            if ((paramChar >= 'A') && (paramChar <= 'F'))
            {
                i = 10 + (paramChar + '\0'7');
            }
            else
            {
                if ((paramChar < 'a') || (paramChar > 'f'))
                    break;
                i = 10 + (paramChar + '\0#7');
            }
        }
        throw new RuntimeException("Invalid hex char '" + paramChar + "'");
    }

    public static byte[] toByteArray(byte paramByte)
    {
        byte[] arrayOfByte = new byte[1];
        arrayOfByte[0] = paramByte;
        return arrayOfByte;
    }

    public static byte[] toByteArray(int paramInt)
    {
        byte[] arrayOfByte = new byte[4];
        arrayOfByte[3] = ((byte)(paramInt & 0xFF));
        arrayOfByte[2] = ((byte)(0xFF & paramInt >> 8));
        arrayOfByte[1] = ((byte)(0xFF & paramInt >> 16));
        arrayOfByte[0] = ((byte)(0xFF & paramInt >> 24));
        return arrayOfByte;
    }

    public static String toHexString(byte paramByte)
    {
        return toHexString(toByteArray(paramByte));
    }

    public static String toHexString(int paramInt)
    {
        return toHexString(toByteArray(paramInt));
    }

    public static String toHexString(byte[] paramArrayOfByte)
    {
        return toHexString(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public static String toHexString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        char[] arrayOfChar = new char[paramInt2 * 2];
        int i = paramInt1;
        int j = 0;
        while (i < paramInt1 + paramInt2)
        {
            int k = paramArrayOfByte[i];
            int m = j + 1;
            arrayOfChar[j] = HEX_DIGITS[(0xF & k >>> 4)];
            j = m + 1;
            arrayOfChar[m] = HEX_DIGITS[(k & 0xF)];
            i++;
        }
        return new String(arrayOfChar);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.HexDump
 * JD-Core Version:        0.6.2
 */