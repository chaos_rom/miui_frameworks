package com.android.internal.util;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Slog;

public abstract class AsyncService extends Service
{
    public static final int CMD_ASYNC_SERVICE_DESTROY = 16777216;
    public static final int CMD_ASYNC_SERVICE_ON_START_INTENT = 16777215;
    protected static final boolean DBG = true;
    private static final String TAG = "AsyncService";
    AsyncServiceInfo mAsyncServiceInfo;
    Handler mHandler;
    protected Messenger mMessenger;

    public abstract AsyncServiceInfo createHandler();

    public Handler getHandler()
    {
        return this.mHandler;
    }

    public IBinder onBind(Intent paramIntent)
    {
        return this.mMessenger.getBinder();
    }

    public void onCreate()
    {
        super.onCreate();
        this.mAsyncServiceInfo = createHandler();
        this.mHandler = this.mAsyncServiceInfo.mHandler;
        this.mMessenger = new Messenger(this.mHandler);
    }

    public void onDestroy()
    {
        Slog.d("AsyncService", "onDestroy");
        Message localMessage = this.mHandler.obtainMessage();
        localMessage.what = 16777216;
        this.mHandler.sendMessage(localMessage);
    }

    public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
    {
        Slog.d("AsyncService", "onStartCommand");
        Message localMessage = this.mHandler.obtainMessage();
        localMessage.what = 16777215;
        localMessage.arg1 = paramInt1;
        localMessage.arg2 = paramInt2;
        localMessage.obj = paramIntent;
        this.mHandler.sendMessage(localMessage);
        return this.mAsyncServiceInfo.mRestartFlags;
    }

    public static final class AsyncServiceInfo
    {
        public Handler mHandler;
        public int mRestartFlags;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.AsyncService
 * JD-Core Version:        0.6.2
 */