package com.android.internal.util;

import java.io.PrintStream;
import java.lang.reflect.Method;

class WithFramework
{
    public static void main(String[] paramArrayOfString)
        throws Exception
    {
        if (paramArrayOfString.length == 0)
            printUsage();
        while (true)
        {
            return;
            Class localClass = Class.forName(paramArrayOfString[0]);
            System.loadLibrary("android_runtime");
            if (registerNatives() < 0)
                throw new RuntimeException("Error registering natives.");
            String[] arrayOfString = new String[-1 + paramArrayOfString.length];
            System.arraycopy(paramArrayOfString, 1, arrayOfString, 0, arrayOfString.length);
            Class[] arrayOfClass = new Class[1];
            arrayOfClass[0] = [Ljava.lang.String.class;
            Method localMethod = localClass.getMethod("main", arrayOfClass);
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = arrayOfString;
            localMethod.invoke(null, arrayOfObject);
        }
    }

    private static void printUsage()
    {
        System.err.println("Usage: dalvikvm " + WithFramework.class.getName() + " [main class] [args]");
    }

    static native int registerNatives();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.WithFramework
 * JD-Core Version:        0.6.2
 */