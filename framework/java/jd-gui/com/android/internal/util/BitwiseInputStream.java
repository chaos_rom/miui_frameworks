package com.android.internal.util;

public class BitwiseInputStream
{
    private byte[] mBuf;
    private int mEnd;
    private int mPos;

    public BitwiseInputStream(byte[] paramArrayOfByte)
    {
        this.mBuf = paramArrayOfByte;
        this.mEnd = (paramArrayOfByte.length << 3);
        this.mPos = 0;
    }

    public int available()
    {
        return this.mEnd - this.mPos;
    }

    public int read(int paramInt)
        throws BitwiseInputStream.AccessException
    {
        int i = this.mPos >>> 3;
        int j = 16 - (0x7 & this.mPos) - paramInt;
        if ((paramInt < 0) || (paramInt > 8) || (paramInt + this.mPos > this.mEnd))
            throw new AccessException("illegal read (pos " + this.mPos + ", end " + this.mEnd + ", bits " + paramInt + ")");
        int k = (0xFF & this.mBuf[i]) << 8;
        if (j < 8)
            k |= 0xFF & this.mBuf[(i + 1)];
        int m = k >>> j & -1 >>> 32 - paramInt;
        this.mPos = (paramInt + this.mPos);
        return m;
    }

    public byte[] readByteArray(int paramInt)
        throws BitwiseInputStream.AccessException
    {
        int i = paramInt >>> 3;
        if ((paramInt & 0x7) > 0);
        byte[] arrayOfByte;
        for (int j = 1; ; j = 0)
        {
            int k = i + j;
            arrayOfByte = new byte[k];
            for (int m = 0; m < k; m++)
            {
                int n = Math.min(8, paramInt - (m << 3));
                arrayOfByte[m] = ((byte)(read(n) << 8 - n));
            }
        }
        return arrayOfByte;
    }

    public void skip(int paramInt)
        throws BitwiseInputStream.AccessException
    {
        if (paramInt + this.mPos > this.mEnd)
            throw new AccessException("illegal skip (pos " + this.mPos + ", end " + this.mEnd + ", bits " + paramInt + ")");
        this.mPos = (paramInt + this.mPos);
    }

    public static class AccessException extends Exception
    {
        public AccessException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.util.BitwiseInputStream
 * JD-Core Version:        0.6.2
 */