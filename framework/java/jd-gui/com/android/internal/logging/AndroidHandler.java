package com.android.internal.logging;

import android.util.Log;
import dalvik.system.DalvikLogHandler;
import dalvik.system.DalvikLogging;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class AndroidHandler extends Handler
    implements DalvikLogHandler
{
    private static final Formatter THE_FORMATTER = new Formatter()
    {
        public String format(LogRecord paramAnonymousLogRecord)
        {
            Throwable localThrowable = paramAnonymousLogRecord.getThrown();
            StringWriter localStringWriter;
            if (localThrowable != null)
            {
                localStringWriter = new StringWriter();
                PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
                localStringWriter.write(paramAnonymousLogRecord.getMessage());
                localStringWriter.write("\n");
                localThrowable.printStackTrace(localPrintWriter);
                localPrintWriter.flush();
            }
            for (String str = localStringWriter.toString(); ; str = paramAnonymousLogRecord.getMessage())
                return str;
        }
    };

    public AndroidHandler()
    {
        setFormatter(THE_FORMATTER);
    }

    static int getAndroidLevel(Level paramLevel)
    {
        int i = paramLevel.intValue();
        int j;
        if (i >= 1000)
            j = 6;
        while (true)
        {
            return j;
            if (i >= 900)
                j = 5;
            else if (i >= 800)
                j = 4;
            else
                j = 3;
        }
    }

    public void close()
    {
    }

    public void flush()
    {
    }

    public void publish(LogRecord paramLogRecord)
    {
        int i = getAndroidLevel(paramLogRecord.getLevel());
        String str = DalvikLogging.loggerNameToTag(paramLogRecord.getLoggerName());
        if (!Log.isLoggable(str, i));
        while (true)
        {
            return;
            try
            {
                Log.println(i, str, getFormatter().format(paramLogRecord));
            }
            catch (RuntimeException localRuntimeException)
            {
                Log.e("AndroidHandler", "Error logging message.", localRuntimeException);
            }
        }
    }

    public void publish(Logger paramLogger, String paramString1, Level paramLevel, String paramString2)
    {
        int i = getAndroidLevel(paramLevel);
        if (!Log.isLoggable(paramString1, i));
        while (true)
        {
            return;
            try
            {
                Log.println(i, paramString1, paramString2);
            }
            catch (RuntimeException localRuntimeException)
            {
                Log.e("AndroidHandler", "Error logging message.", localRuntimeException);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.logging.AndroidHandler
 * JD-Core Version:        0.6.2
 */