package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import com.android.internal.R.styleable;

public class SlidingTab extends ViewGroup
{
    private static final int ANIM_DURATION = 250;
    private static final int ANIM_TARGET_TIME = 500;
    private static final boolean DBG = false;
    private static final int HORIZONTAL = 0;
    private static final String LOG_TAG = "SlidingTab";
    private static final float THRESHOLD = 0.6666667F;
    private static final int TRACKING_MARGIN = 50;
    private static final int VERTICAL = 1;
    private static final long VIBRATE_LONG = 40L;
    private static final long VIBRATE_SHORT = 30L;
    private boolean mAnimating;
    private final Animation.AnimationListener mAnimationDoneListener = new Animation.AnimationListener()
    {
        public void onAnimationEnd(Animation paramAnonymousAnimation)
        {
            SlidingTab.this.onAnimationDone();
        }

        public void onAnimationRepeat(Animation paramAnonymousAnimation)
        {
        }

        public void onAnimationStart(Animation paramAnonymousAnimation)
        {
        }
    };
    private Slider mCurrentSlider;
    private final float mDensity;
    private int mGrabbedState = 0;
    private boolean mHoldLeftOnTransition = true;
    private boolean mHoldRightOnTransition = true;
    private final Slider mLeftSlider;
    private OnTriggerListener mOnTriggerListener;
    private final int mOrientation;
    private Slider mOtherSlider;
    private final Slider mRightSlider;
    private float mThreshold;
    private final Rect mTmpRect = new Rect();
    private boolean mTracking;
    private boolean mTriggered = false;
    private Vibrator mVibrator;

    public SlidingTab(Context paramContext)
    {
        this(paramContext, null);
    }

    public SlidingTab(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SlidingTab);
        this.mOrientation = localTypedArray.getInt(0, 0);
        localTypedArray.recycle();
        this.mDensity = getResources().getDisplayMetrics().density;
        this.mLeftSlider = new Slider(this, 17302429, 17302412, 17302443);
        this.mRightSlider = new Slider(this, 17302438, 17302421, 17302443);
    }

    private void cancelGrab()
    {
        this.mTracking = false;
        this.mTriggered = false;
        this.mOtherSlider.show(true);
        this.mCurrentSlider.reset(false);
        this.mCurrentSlider.hideTarget();
        this.mCurrentSlider = null;
        this.mOtherSlider = null;
        setGrabbedState(0);
    }

    private void dispatchTriggerEvent(int paramInt)
    {
        vibrate(40L);
        if (this.mOnTriggerListener != null)
            this.mOnTriggerListener.onTrigger(this, paramInt);
    }

    private boolean isHorizontal()
    {
        if (this.mOrientation == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void log(String paramString)
    {
        Log.d("SlidingTab", paramString);
    }

    private void moveHandle(float paramFloat1, float paramFloat2)
    {
        ImageView localImageView = this.mCurrentSlider.tab;
        TextView localTextView = this.mCurrentSlider.text;
        if (isHorizontal())
        {
            int j = (int)paramFloat1 - localImageView.getLeft() - localImageView.getWidth() / 2;
            localImageView.offsetLeftAndRight(j);
            localTextView.offsetLeftAndRight(j);
        }
        while (true)
        {
            invalidate();
            return;
            int i = (int)paramFloat2 - localImageView.getTop() - localImageView.getHeight() / 2;
            localImageView.offsetTopAndBottom(i);
            localTextView.offsetTopAndBottom(i);
        }
    }

    private void onAnimationDone()
    {
        resetView();
        this.mAnimating = false;
    }

    private void resetView()
    {
        this.mLeftSlider.reset(false);
        this.mRightSlider.reset(false);
    }

    private void setGrabbedState(int paramInt)
    {
        if (paramInt != this.mGrabbedState)
        {
            this.mGrabbedState = paramInt;
            if (this.mOnTriggerListener != null)
                this.mOnTriggerListener.onGrabbedStateChange(this, this.mGrabbedState);
        }
    }

    /** @deprecated */
    private void vibrate(long paramLong)
    {
        try
        {
            if (this.mVibrator == null)
                this.mVibrator = ((Vibrator)getContext().getSystemService("vibrator"));
            this.mVibrator.vibrate(paramLong);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private boolean withinView(float paramFloat1, float paramFloat2, View paramView)
    {
        if (((isHorizontal()) && (paramFloat2 > -50.0F) && (paramFloat2 < 50 + paramView.getHeight())) || ((!isHorizontal()) && (paramFloat1 > -50.0F) && (paramFloat1 < 50 + paramView.getWidth())));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        float f1 = 0.6666667F;
        float f2 = 0.3333333F;
        int i = paramMotionEvent.getAction();
        float f3 = paramMotionEvent.getX();
        float f4 = paramMotionEvent.getY();
        boolean bool3;
        if (this.mAnimating)
            bool3 = false;
        boolean bool1;
        while (true)
        {
            return bool3;
            this.mLeftSlider.tab.getHitRect(this.mTmpRect);
            bool1 = this.mTmpRect.contains((int)f3, (int)f4);
            this.mRightSlider.tab.getHitRect(this.mTmpRect);
            boolean bool2 = this.mTmpRect.contains((int)f3, (int)f4);
            if ((!this.mTracking) && (!bool1) && (!bool2))
                bool3 = false;
            else
                switch (i)
                {
                default:
                    bool3 = true;
                case 0:
                }
        }
        this.mTracking = true;
        this.mTriggered = false;
        vibrate(30L);
        if (bool1)
        {
            this.mCurrentSlider = this.mLeftSlider;
            this.mOtherSlider = this.mRightSlider;
            if (isHorizontal());
            while (true)
            {
                this.mThreshold = f1;
                setGrabbedState(1);
                this.mCurrentSlider.setState(1);
                this.mCurrentSlider.showTarget();
                this.mOtherSlider.hide();
                break;
                f1 = f2;
            }
        }
        this.mCurrentSlider = this.mRightSlider;
        this.mOtherSlider = this.mLeftSlider;
        if (isHorizontal());
        while (true)
        {
            this.mThreshold = f2;
            setGrabbedState(2);
            break;
            f2 = f1;
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (!paramBoolean)
            return;
        Slider localSlider1 = this.mLeftSlider;
        int i;
        label21: Slider localSlider2;
        if (isHorizontal())
        {
            i = 0;
            localSlider1.layout(paramInt1, paramInt2, paramInt3, paramInt4, i);
            localSlider2 = this.mRightSlider;
            if (!isHorizontal())
                break label72;
        }
        label72: for (int j = 1; ; j = 2)
        {
            localSlider2.layout(paramInt1, paramInt2, paramInt3, paramInt4, j);
            break;
            i = 3;
            break label21;
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        View.MeasureSpec.getMode(paramInt1);
        int i = View.MeasureSpec.getSize(paramInt1);
        View.MeasureSpec.getMode(paramInt2);
        int j = View.MeasureSpec.getSize(paramInt2);
        this.mLeftSlider.measure();
        this.mRightSlider.measure();
        int k = this.mLeftSlider.getTabWidth();
        int m = this.mRightSlider.getTabWidth();
        int n = this.mLeftSlider.getTabHeight();
        int i1 = this.mRightSlider.getTabHeight();
        int i2;
        if (isHorizontal())
            i2 = Math.max(i, k + m);
        for (int i3 = Math.max(n, i1); ; i3 = Math.max(j, n + i1))
        {
            setMeasuredDimension(i2, i3);
            return;
            i2 = Math.max(k, i1);
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool1 = false;
        float f1;
        float f2;
        if (this.mTracking)
        {
            int i = paramMotionEvent.getAction();
            f1 = paramMotionEvent.getX();
            f2 = paramMotionEvent.getY();
            switch (i)
            {
            default:
            case 2:
            case 1:
            case 3:
            }
        }
        while (true)
        {
            if ((this.mTracking) || (super.onTouchEvent(paramMotionEvent)))
                bool1 = true;
            return bool1;
            if (withinView(f1, f2, this))
            {
                moveHandle(f1, f2);
                float f3;
                label102: int j;
                label121: float f5;
                int k;
                label158: int m;
                label202: int n;
                if (isHorizontal())
                {
                    f3 = f1;
                    float f4 = this.mThreshold;
                    if (isHorizontal())
                    {
                        j = getWidth();
                        f5 = f4 * j;
                        if (!isHorizontal())
                            break label283;
                        if (this.mCurrentSlider != this.mLeftSlider)
                            break label263;
                        if (f3 <= f5)
                            break label257;
                        k = 1;
                        if ((this.mTriggered) || (k == 0))
                            continue;
                        this.mTriggered = true;
                        this.mTracking = false;
                        this.mCurrentSlider.setState(2);
                        if (this.mCurrentSlider != this.mLeftSlider)
                            break label334;
                        m = 1;
                        if (m == 0)
                            break label340;
                        n = 1;
                        label210: dispatchTriggerEvent(n);
                        if (m == 0)
                            break label346;
                    }
                }
                else
                {
                    label257: label263: label283: label334: label340: label346: for (boolean bool2 = this.mHoldLeftOnTransition; ; bool2 = this.mHoldRightOnTransition)
                    {
                        startAnimating(bool2);
                        setGrabbedState(0);
                        break;
                        f3 = f2;
                        break label102;
                        j = getHeight();
                        break label121;
                        k = 0;
                        break label158;
                        if (f3 < f5)
                        {
                            k = 1;
                            break label158;
                        }
                        k = 0;
                        break label158;
                        if (this.mCurrentSlider == this.mLeftSlider)
                            if (f3 < f5)
                                k = 1;
                        while (true)
                        {
                            break;
                            k = 0;
                            continue;
                            if (f3 > f5)
                                k = 1;
                            else
                                k = 0;
                        }
                        m = 0;
                        break label202;
                        n = 2;
                        break label210;
                    }
                }
            }
            else
            {
                cancelGrab();
            }
        }
    }

    protected void onVisibilityChanged(View paramView, int paramInt)
    {
        super.onVisibilityChanged(paramView, paramInt);
        if ((paramView == this) && (paramInt != 0) && (this.mGrabbedState != 0))
            cancelGrab();
    }

    public void reset(boolean paramBoolean)
    {
        this.mLeftSlider.reset(paramBoolean);
        this.mRightSlider.reset(paramBoolean);
        if (!paramBoolean)
            this.mAnimating = false;
    }

    public void setHoldAfterTrigger(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mHoldLeftOnTransition = paramBoolean1;
        this.mHoldRightOnTransition = paramBoolean2;
    }

    public void setLeftHintText(int paramInt)
    {
        if (isHorizontal())
            this.mLeftSlider.setHintText(paramInt);
    }

    public void setLeftTabResources(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mLeftSlider.setIcon(paramInt1);
        this.mLeftSlider.setTarget(paramInt2);
        this.mLeftSlider.setBarBackgroundResource(paramInt3);
        this.mLeftSlider.setTabBackgroundResource(paramInt4);
        this.mLeftSlider.updateDrawableStates();
    }

    public void setOnTriggerListener(OnTriggerListener paramOnTriggerListener)
    {
        this.mOnTriggerListener = paramOnTriggerListener;
    }

    public void setRightHintText(int paramInt)
    {
        if (isHorizontal())
            this.mRightSlider.setHintText(paramInt);
    }

    public void setRightTabResources(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mRightSlider.setIcon(paramInt1);
        this.mRightSlider.setTarget(paramInt2);
        this.mRightSlider.setBarBackgroundResource(paramInt3);
        this.mRightSlider.setTabBackgroundResource(paramInt4);
        this.mRightSlider.updateDrawableStates();
    }

    public void setVisibility(int paramInt)
    {
        if ((paramInt != getVisibility()) && (paramInt == 4))
            reset(false);
        super.setVisibility(paramInt);
    }

    void startAnimating(final boolean paramBoolean)
    {
        this.mAnimating = true;
        Slider localSlider = this.mCurrentSlider;
        final int i1;
        if (isHorizontal())
        {
            int i3 = localSlider.tab.getRight();
            int i4 = localSlider.tab.getWidth();
            int i5 = localSlider.tab.getLeft();
            int i6 = getWidth();
            int i7;
            if (paramBoolean)
            {
                i7 = 0;
                if (localSlider != this.mRightSlider)
                    break label212;
            }
            label212: for (i1 = -(i3 + i6 - i7); ; i1 = i6 + (i6 - i5) - i7)
            {
                i2 = 0;
                TranslateAnimation localTranslateAnimation1 = new TranslateAnimation(0.0F, i1, 0.0F, i2);
                localTranslateAnimation1.setDuration(250L);
                localTranslateAnimation1.setInterpolator(new LinearInterpolator());
                localTranslateAnimation1.setFillAfter(true);
                TranslateAnimation localTranslateAnimation2 = new TranslateAnimation(0.0F, i1, 0.0F, i2);
                localTranslateAnimation2.setDuration(250L);
                localTranslateAnimation2.setInterpolator(new LinearInterpolator());
                localTranslateAnimation2.setFillAfter(true);
                Animation.AnimationListener local2 = new Animation.AnimationListener()
                {
                    public void onAnimationEnd(Animation paramAnonymousAnimation)
                    {
                        Object localObject;
                        if (paramBoolean)
                        {
                            localObject = new TranslateAnimation(i1, i1, i2, i2);
                            ((TranslateAnimation)localObject).setDuration(1000L);
                            SlidingTab.access$202(SlidingTab.this, false);
                        }
                        while (true)
                        {
                            ((Animation)localObject).setAnimationListener(SlidingTab.this.mAnimationDoneListener);
                            SlidingTab.this.mLeftSlider.startAnimation((Animation)localObject, (Animation)localObject);
                            SlidingTab.this.mRightSlider.startAnimation((Animation)localObject, (Animation)localObject);
                            return;
                            localObject = new AlphaAnimation(0.5F, 1.0F);
                            ((AlphaAnimation)localObject).setDuration(250L);
                            SlidingTab.this.resetView();
                        }
                    }

                    public void onAnimationRepeat(Animation paramAnonymousAnimation)
                    {
                    }

                    public void onAnimationStart(Animation paramAnonymousAnimation)
                    {
                    }
                };
                localTranslateAnimation1.setAnimationListener(local2);
                localSlider.hideTarget();
                localSlider.startAnimation(localTranslateAnimation1, localTranslateAnimation2);
                return;
                i7 = i4;
                break;
            }
        }
        int i = localSlider.tab.getTop();
        int j = localSlider.tab.getBottom();
        int k = localSlider.tab.getHeight();
        int m = getHeight();
        int n;
        if (paramBoolean)
        {
            n = 0;
            label268: i1 = 0;
            if (localSlider != this.mRightSlider)
                break label299;
        }
        label299: for (final int i2 = i + m - n; ; i2 = -(m + (m - j) - n))
        {
            break;
            n = k;
            break label268;
        }
    }

    private static class Slider
    {
        public static final int ALIGN_BOTTOM = 3;
        public static final int ALIGN_LEFT = 0;
        public static final int ALIGN_RIGHT = 1;
        public static final int ALIGN_TOP = 2;
        public static final int ALIGN_UNKNOWN = 4;
        private static final int STATE_ACTIVE = 2;
        private static final int STATE_NORMAL = 0;
        private static final int STATE_PRESSED = 1;
        private int alignment = 4;
        private int alignment_value;
        private int currentState = 0;
        private final ImageView tab;
        private final ImageView target;
        private final TextView text;

        Slider(ViewGroup paramViewGroup, int paramInt1, int paramInt2, int paramInt3)
        {
            this.tab = new ImageView(paramViewGroup.getContext());
            this.tab.setBackgroundResource(paramInt1);
            this.tab.setScaleType(ImageView.ScaleType.CENTER);
            this.tab.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            this.text = new TextView(paramViewGroup.getContext());
            this.text.setLayoutParams(new ViewGroup.LayoutParams(-2, -1));
            this.text.setBackgroundResource(paramInt2);
            this.text.setTextAppearance(paramViewGroup.getContext(), 16974353);
            this.target = new ImageView(paramViewGroup.getContext());
            this.target.setImageResource(paramInt3);
            this.target.setScaleType(ImageView.ScaleType.CENTER);
            this.target.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            this.target.setVisibility(4);
            paramViewGroup.addView(this.target);
            paramViewGroup.addView(this.tab);
            paramViewGroup.addView(this.text);
        }

        public int getTabHeight()
        {
            return this.tab.getMeasuredHeight();
        }

        public int getTabWidth()
        {
            return this.tab.getMeasuredWidth();
        }

        void hide()
        {
            int i = 0;
            int j;
            int k;
            if ((this.alignment == 0) || (this.alignment == 1))
            {
                j = 1;
                if (j == 0)
                    break label124;
                if (this.alignment != 0)
                    break label108;
                k = this.alignment_value - this.tab.getRight();
                label43: if (j == 0)
                    break label129;
            }
            while (true)
            {
                TranslateAnimation localTranslateAnimation = new TranslateAnimation(0.0F, k, 0.0F, i);
                localTranslateAnimation.setDuration(250L);
                localTranslateAnimation.setFillAfter(true);
                this.tab.startAnimation(localTranslateAnimation);
                this.text.startAnimation(localTranslateAnimation);
                this.target.setVisibility(4);
                return;
                j = 0;
                break;
                label108: k = this.alignment_value - this.tab.getLeft();
                break label43;
                label124: k = 0;
                break label43;
                label129: if (this.alignment == 2)
                    i = this.alignment_value - this.tab.getBottom();
                else
                    i = this.alignment_value - this.tab.getTop();
            }
        }

        public void hideTarget()
        {
            this.target.clearAnimation();
            this.target.setVisibility(4);
        }

        void layout(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            this.alignment = paramInt5;
            Drawable localDrawable1 = this.tab.getBackground();
            int i = localDrawable1.getIntrinsicWidth();
            int j = localDrawable1.getIntrinsicHeight();
            Drawable localDrawable2 = this.target.getDrawable();
            int k = localDrawable2.getIntrinsicWidth();
            int m = localDrawable2.getIntrinsicHeight();
            int n = paramInt3 - paramInt1;
            int i1 = paramInt4 - paramInt2;
            int i2 = (int)(0.6666667F * n) - k + i / 2;
            int i3 = (int)(0.3333333F * n) - i / 2;
            int i4 = (n - i) / 2;
            int i5 = i4 + i;
            int i6;
            int i7;
            int i8;
            int i9;
            if ((paramInt5 == 0) || (paramInt5 == 1))
            {
                i6 = (i1 - m) / 2;
                i7 = i6 + m;
                i8 = (i1 - j) / 2;
                i9 = (i1 + j) / 2;
                if (paramInt5 == 0)
                {
                    this.tab.layout(0, i8, i, i9);
                    this.text.layout(0 - n, i8, 0, i9);
                    this.text.setGravity(5);
                    this.target.layout(i2, i6, i2 + k, i7);
                    this.alignment_value = paramInt1;
                }
            }
            while (true)
            {
                return;
                this.tab.layout(n - i, i8, n, i9);
                this.text.layout(n, i8, n + n, i9);
                this.target.layout(i3, i6, i3 + k, i7);
                this.text.setGravity(48);
                this.alignment_value = paramInt3;
                continue;
                int i10 = (n - k) / 2;
                int i11 = (n + k) / 2;
                int i12 = (int)(0.6666667F * i1) + j / 2 - m;
                int i13 = (int)(0.3333333F * i1) - j / 2;
                if (paramInt5 == 2)
                {
                    this.tab.layout(i4, 0, i5, j);
                    this.text.layout(i4, 0 - i1, i5, 0);
                    this.target.layout(i10, i12, i11, i12 + m);
                    this.alignment_value = paramInt2;
                }
                else
                {
                    this.tab.layout(i4, i1 - j, i5, i1);
                    this.text.layout(i4, i1, i5, i1 + i1);
                    this.target.layout(i10, i13, i11, i13 + m);
                    this.alignment_value = paramInt4;
                }
            }
        }

        public void measure()
        {
            this.tab.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            this.text.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        }

        void reset(boolean paramBoolean)
        {
            int i = 1;
            setState(0);
            this.text.setVisibility(0);
            this.text.setTextAppearance(this.text.getContext(), 16974353);
            this.tab.setVisibility(0);
            this.target.setVisibility(4);
            int j;
            label86: int k;
            if ((this.alignment == 0) || (this.alignment == i))
            {
                if (i == 0)
                    break label167;
                if (this.alignment != 0)
                    break label151;
                j = this.alignment_value - this.tab.getLeft();
                if (i == 0)
                    break label172;
                k = 0;
            }
            while (true)
            {
                if (!paramBoolean)
                    break label214;
                TranslateAnimation localTranslateAnimation = new TranslateAnimation(0.0F, j, 0.0F, k);
                localTranslateAnimation.setDuration(250L);
                localTranslateAnimation.setFillAfter(false);
                this.text.startAnimation(localTranslateAnimation);
                this.tab.startAnimation(localTranslateAnimation);
                return;
                i = 0;
                break;
                label151: j = this.alignment_value - this.tab.getRight();
                break label86;
                label167: j = 0;
                break label86;
                label172: if (this.alignment == 2)
                    k = this.alignment_value - this.tab.getTop();
                else
                    k = this.alignment_value - this.tab.getBottom();
            }
            label214: if (i != 0)
            {
                this.text.offsetLeftAndRight(j);
                this.tab.offsetLeftAndRight(j);
            }
            while (true)
            {
                this.text.clearAnimation();
                this.tab.clearAnimation();
                this.target.clearAnimation();
                break;
                this.text.offsetTopAndBottom(k);
                this.tab.offsetTopAndBottom(k);
            }
        }

        void setBarBackgroundResource(int paramInt)
        {
            this.text.setBackgroundResource(paramInt);
        }

        void setHintText(int paramInt)
        {
            this.text.setText(paramInt);
        }

        void setIcon(int paramInt)
        {
            this.tab.setImageResource(paramInt);
        }

        void setState(int paramInt)
        {
            TextView localTextView = this.text;
            boolean bool1;
            boolean bool2;
            if (paramInt == 1)
            {
                bool1 = true;
                localTextView.setPressed(bool1);
                ImageView localImageView = this.tab;
                if (paramInt != 1)
                    break label133;
                bool2 = true;
                label31: localImageView.setPressed(bool2);
                if (paramInt != 2)
                    break label139;
                int[] arrayOfInt = new int[1];
                arrayOfInt[0] = 16842914;
                if (this.text.getBackground().isStateful())
                    this.text.getBackground().setState(arrayOfInt);
                if (this.tab.getBackground().isStateful())
                    this.tab.getBackground().setState(arrayOfInt);
                this.text.setTextAppearance(this.text.getContext(), 16974354);
            }
            while (true)
            {
                this.currentState = paramInt;
                return;
                bool1 = false;
                break;
                label133: bool2 = false;
                break label31;
                label139: this.text.setTextAppearance(this.text.getContext(), 16974353);
            }
        }

        void setTabBackgroundResource(int paramInt)
        {
            this.tab.setBackgroundResource(paramInt);
        }

        void setTarget(int paramInt)
        {
            this.target.setImageResource(paramInt);
        }

        void show(boolean paramBoolean)
        {
            int i = 1;
            int j = 0;
            this.text.setVisibility(0);
            this.tab.setVisibility(0);
            int k;
            if (paramBoolean)
            {
                if ((this.alignment != 0) && (this.alignment != i))
                    break label108;
                if (i == 0)
                    break label126;
                if (this.alignment != 0)
                    break label113;
                k = this.tab.getWidth();
                label59: if (i == 0)
                    break label132;
            }
            while (true)
            {
                TranslateAnimation localTranslateAnimation = new TranslateAnimation(-k, 0.0F, -j, 0.0F);
                localTranslateAnimation.setDuration(250L);
                this.tab.startAnimation(localTranslateAnimation);
                this.text.startAnimation(localTranslateAnimation);
                return;
                label108: i = 0;
                break;
                label113: k = -this.tab.getWidth();
                break label59;
                label126: k = 0;
                break label59;
                label132: if (this.alignment == 2)
                    j = this.tab.getHeight();
                else
                    j = -this.tab.getHeight();
            }
        }

        void showTarget()
        {
            AlphaAnimation localAlphaAnimation = new AlphaAnimation(0.0F, 1.0F);
            localAlphaAnimation.setDuration(500L);
            this.target.startAnimation(localAlphaAnimation);
            this.target.setVisibility(0);
        }

        public void startAnimation(Animation paramAnimation1, Animation paramAnimation2)
        {
            this.tab.startAnimation(paramAnimation1);
            this.text.startAnimation(paramAnimation2);
        }

        public void updateDrawableStates()
        {
            setState(this.currentState);
        }
    }

    public static abstract interface OnTriggerListener
    {
        public static final int LEFT_HANDLE = 1;
        public static final int NO_HANDLE = 0;
        public static final int RIGHT_HANDLE = 2;

        public abstract void onGrabbedStateChange(View paramView, int paramInt);

        public abstract void onTrigger(View paramView, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.SlidingTab
 * JD-Core Version:        0.6.2
 */