package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.android.internal.app.ActionBarImpl;

public class ActionBarOverlayLayout extends FrameLayout
{
    static final int[] mActionBarSizeAttr = arrayOfInt;
    private ActionBarImpl mActionBar;
    private View mActionBarBottom;
    private int mActionBarHeight;
    private View mActionBarTop;
    private ActionBarView mActionView;
    private ActionBarContainer mContainerView;
    private View mContent;
    private int mLastSystemUiVisibility;
    private int mWindowVisibility = 0;
    private final Rect mZeroRect = new Rect(0, 0, 0, 0);

    static
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16843499;
    }

    public ActionBarOverlayLayout(Context paramContext)
    {
        super(paramContext);
        init(paramContext);
    }

    public ActionBarOverlayLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        init(paramContext);
    }

    private boolean applyInsets(View paramView, Rect paramRect, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
    {
        boolean bool = false;
        FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams)paramView.getLayoutParams();
        if ((paramBoolean1) && (localLayoutParams.leftMargin != paramRect.left))
        {
            bool = true;
            localLayoutParams.leftMargin = paramRect.left;
        }
        if ((paramBoolean2) && (localLayoutParams.topMargin != paramRect.top))
        {
            bool = true;
            localLayoutParams.topMargin = paramRect.top;
        }
        if ((paramBoolean4) && (localLayoutParams.rightMargin != paramRect.right))
        {
            bool = true;
            localLayoutParams.rightMargin = paramRect.right;
        }
        if ((paramBoolean3) && (localLayoutParams.bottomMargin != paramRect.bottom))
        {
            bool = true;
            localLayoutParams.bottomMargin = paramRect.bottom;
        }
        return bool;
    }

    private void init(Context paramContext)
    {
        TypedArray localTypedArray = getContext().getTheme().obtainStyledAttributes(mActionBarSizeAttr);
        this.mActionBarHeight = localTypedArray.getDimensionPixelSize(0, 0);
        localTypedArray.recycle();
    }

    protected boolean fitSystemWindows(Rect paramRect)
    {
        pullChildren();
        int i = getWindowSystemUiVisibility();
        int j;
        boolean bool1;
        boolean bool2;
        if ((i & 0x100) != 0)
        {
            j = 1;
            bool1 = applyInsets(this.mActionBarTop, paramRect, true, true, false, true);
            if (this.mActionBarBottom != null)
                bool1 |= applyInsets(this.mActionBarBottom, paramRect, true, false, true, true);
            if ((i & 0x600) != 0)
                break label240;
            bool2 = bool1 | applyInsets(this.mContent, paramRect, true, true, true, true);
            paramRect.set(0, 0, 0, 0);
        }
        while (true)
        {
            if ((j != 0) || (this.mActionBarTop.getVisibility() == 0))
                paramRect.top += this.mActionBarHeight;
            if ((this.mActionBar != null) && (this.mActionBar.hasNonEmbeddedTabs()))
            {
                View localView = this.mContainerView.getTabContainer();
                if ((j != 0) || ((localView != null) && (localView.getVisibility() == 0)))
                    paramRect.top += this.mActionBarHeight;
            }
            if ((this.mActionView.isSplitActionBar()) && ((j != 0) || ((this.mActionBarBottom != null) && (this.mActionBarBottom.getVisibility() == 0))))
                paramRect.bottom += this.mActionBarHeight;
            if (bool2)
                requestLayout();
            return super.fitSystemWindows(paramRect);
            j = 0;
            break;
            label240: bool2 = bool1 | applyInsets(this.mContent, this.mZeroRect, true, true, true, true);
        }
    }

    public void onWindowSystemUiVisibilityChanged(int paramInt)
    {
        super.onWindowSystemUiVisibilityChanged(paramInt);
        pullChildren();
        int i = paramInt ^ this.mLastSystemUiVisibility;
        this.mLastSystemUiVisibility = paramInt;
        int j;
        if ((paramInt & 0x4) == 0)
        {
            j = 1;
            if (this.mActionBar == null)
                break label87;
            this.mActionBar.isSystemShowing();
            label44: if (this.mActionBar != null)
            {
                if (j == 0)
                    break label90;
                this.mActionBar.showForSystem();
            }
        }
        while (true)
        {
            if (((i & 0x100) != 0) && (this.mActionBar != null))
                requestFitSystemWindows();
            return;
            j = 0;
            break;
            label87: break label44;
            label90: this.mActionBar.hideForSystem();
        }
    }

    protected void onWindowVisibilityChanged(int paramInt)
    {
        super.onWindowVisibilityChanged(paramInt);
        this.mWindowVisibility = paramInt;
        if (this.mActionBar != null)
            this.mActionBar.setWindowVisibility(paramInt);
    }

    void pullChildren()
    {
        if (this.mContent == null)
        {
            this.mContent = findViewById(16908290);
            this.mActionBarTop = findViewById(16909091);
            this.mContainerView = ((ActionBarContainer)findViewById(16909086));
            this.mActionView = ((ActionBarView)findViewById(16909087));
            this.mActionBarBottom = findViewById(16909089);
        }
    }

    public void setActionBar(ActionBarImpl paramActionBarImpl)
    {
        this.mActionBar = paramActionBarImpl;
        if (getWindowToken() != null)
        {
            this.mActionBar.setWindowVisibility(this.mWindowVisibility);
            if (this.mLastSystemUiVisibility != 0)
            {
                onWindowSystemUiVisibilityChanged(this.mLastSystemUiVisibility);
                requestFitSystemWindows();
            }
        }
    }

    public void setShowingForActionMode(boolean paramBoolean)
    {
        if (paramBoolean)
            if ((0x500 & getWindowSystemUiVisibility()) == 1280)
                setDisabledSystemUiVisibility(4);
        while (true)
        {
            return;
            setDisabledSystemUiVisibility(0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.ActionBarOverlayLayout
 * JD-Core Version:        0.6.2
 */