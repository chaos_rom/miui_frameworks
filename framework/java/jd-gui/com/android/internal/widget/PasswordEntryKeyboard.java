package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.Keyboard.Row;
import java.util.List;

public class PasswordEntryKeyboard extends Keyboard
{
    public static final int KEYCODE_SPACE = 32;
    private static final int SHIFT_LOCKED = 2;
    private static final int SHIFT_OFF = 0;
    private static final int SHIFT_ON = 1;
    static int sSpacebarVerticalCorrection;
    private Keyboard.Key mEnterKey;
    private Keyboard.Key mF1Key;
    private Drawable[] mOldShiftIcons;
    private Drawable mShiftIcon;
    private Keyboard.Key[] mShiftKeys;
    private Drawable mShiftLockIcon;
    private int mShiftState;
    private Keyboard.Key mSpaceKey;

    public PasswordEntryKeyboard(Context paramContext, int paramInt)
    {
        this(paramContext, paramInt, 0);
    }

    public PasswordEntryKeyboard(Context paramContext, int paramInt1, int paramInt2)
    {
        super(paramContext, paramInt1, paramInt2);
        Drawable[] arrayOfDrawable = new Drawable[2];
        arrayOfDrawable[0] = null;
        arrayOfDrawable[1] = null;
        this.mOldShiftIcons = arrayOfDrawable;
        Keyboard.Key[] arrayOfKey = new Keyboard.Key[2];
        arrayOfKey[0] = null;
        arrayOfKey[1] = null;
        this.mShiftKeys = arrayOfKey;
        this.mShiftState = 0;
        init(paramContext);
    }

    public PasswordEntryKeyboard(Context paramContext, int paramInt1, int paramInt2, int paramInt3)
    {
        this(paramContext, paramInt1, 0, paramInt2, paramInt3);
    }

    public PasswordEntryKeyboard(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super(paramContext, paramInt1, paramInt2, paramInt3, paramInt4);
        Drawable[] arrayOfDrawable = new Drawable[2];
        arrayOfDrawable[0] = null;
        arrayOfDrawable[1] = null;
        this.mOldShiftIcons = arrayOfDrawable;
        Keyboard.Key[] arrayOfKey = new Keyboard.Key[2];
        arrayOfKey[0] = null;
        arrayOfKey[1] = null;
        this.mShiftKeys = arrayOfKey;
        this.mShiftState = 0;
        init(paramContext);
    }

    public PasswordEntryKeyboard(Context paramContext, int paramInt1, CharSequence paramCharSequence, int paramInt2, int paramInt3)
    {
        super(paramContext, paramInt1, paramCharSequence, paramInt2, paramInt3);
        Drawable[] arrayOfDrawable = new Drawable[2];
        arrayOfDrawable[0] = null;
        arrayOfDrawable[1] = null;
        this.mOldShiftIcons = arrayOfDrawable;
        Keyboard.Key[] arrayOfKey = new Keyboard.Key[2];
        arrayOfKey[0] = null;
        arrayOfKey[1] = null;
        this.mShiftKeys = arrayOfKey;
        this.mShiftState = 0;
    }

    private void init(Context paramContext)
    {
        Resources localResources = paramContext.getResources();
        this.mShiftIcon = localResources.getDrawable(17302932);
        this.mShiftLockIcon = localResources.getDrawable(17302933);
        sSpacebarVerticalCorrection = localResources.getDimensionPixelOffset(17104921);
    }

    protected Keyboard.Key createKeyFromXml(Resources paramResources, Keyboard.Row paramRow, int paramInt1, int paramInt2, XmlResourceParser paramXmlResourceParser)
    {
        LatinKey localLatinKey = new LatinKey(paramResources, paramRow, paramInt1, paramInt2, paramXmlResourceParser);
        int i = localLatinKey.codes[0];
        if ((i >= 0) && (i != 10) && ((i < 32) || (i > 127)))
        {
            localLatinKey.label = " ";
            localLatinKey.setEnabled(false);
        }
        switch (localLatinKey.codes[0])
        {
        default:
        case 10:
        case -103:
        case 32:
        }
        while (true)
        {
            return localLatinKey;
            this.mEnterKey = localLatinKey;
            continue;
            this.mF1Key = localLatinKey;
            continue;
            this.mSpaceKey = localLatinKey;
        }
    }

    void enableShiftLock()
    {
        int i = 0;
        for (int m : getShiftKeyIndices())
            if ((m >= 0) && (i < this.mShiftKeys.length))
            {
                this.mShiftKeys[i] = ((Keyboard.Key)getKeys().get(m));
                if ((this.mShiftKeys[i] instanceof LatinKey))
                    ((LatinKey)this.mShiftKeys[i]).enableShiftLock();
                this.mOldShiftIcons[i] = this.mShiftKeys[i].icon;
                i++;
            }
    }

    public boolean isShifted()
    {
        boolean bool = false;
        if (this.mShiftKeys[0] != null)
            if (this.mShiftState == 0);
        for (bool = true; ; bool = super.isShifted())
            return bool;
    }

    void setEnterKeyResources(Resources paramResources, int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mEnterKey != null)
        {
            this.mEnterKey.popupCharacters = null;
            this.mEnterKey.popupResId = 0;
            this.mEnterKey.text = null;
            this.mEnterKey.iconPreview = paramResources.getDrawable(paramInt1);
            this.mEnterKey.icon = paramResources.getDrawable(paramInt2);
            this.mEnterKey.label = paramResources.getText(paramInt3);
            if (this.mEnterKey.iconPreview != null)
                this.mEnterKey.iconPreview.setBounds(0, 0, this.mEnterKey.iconPreview.getIntrinsicWidth(), this.mEnterKey.iconPreview.getIntrinsicHeight());
        }
    }

    void setShiftLocked(boolean paramBoolean)
    {
        for (Keyboard.Key localKey : this.mShiftKeys)
            if (localKey != null)
            {
                localKey.on = paramBoolean;
                localKey.icon = this.mShiftLockIcon;
            }
        if (paramBoolean);
        for (int k = 2; ; k = 1)
        {
            this.mShiftState = k;
            return;
        }
    }

    public boolean setShifted(boolean paramBoolean)
    {
        boolean bool = false;
        label20: int i;
        if (!paramBoolean)
            if (this.mShiftState != 0)
            {
                bool = true;
                this.mShiftState = 0;
                i = 0;
                label22: if (i >= this.mShiftKeys.length)
                    break label142;
                if (this.mShiftKeys[i] != null)
                {
                    if (paramBoolean)
                        break label109;
                    this.mShiftKeys[i].on = false;
                    this.mShiftKeys[i].icon = this.mOldShiftIcons[i];
                }
            }
        while (true)
        {
            i++;
            break label22;
            bool = false;
            break;
            if (this.mShiftState != 0)
                break label20;
            if (this.mShiftState == 0);
            for (bool = true; ; bool = false)
            {
                this.mShiftState = 1;
                break;
            }
            label109: if (this.mShiftState == 0)
            {
                this.mShiftKeys[i].on = false;
                this.mShiftKeys[i].icon = this.mShiftIcon;
            }
        }
        label142: return bool;
    }

    static class LatinKey extends Keyboard.Key
    {
        private boolean mEnabled = true;
        private boolean mShiftLockEnabled;

        public LatinKey(Resources paramResources, Keyboard.Row paramRow, int paramInt1, int paramInt2, XmlResourceParser paramXmlResourceParser)
        {
            super(paramRow, paramInt1, paramInt2, paramXmlResourceParser);
            if ((this.popupCharacters != null) && (this.popupCharacters.length() == 0))
                this.popupResId = 0;
        }

        void enableShiftLock()
        {
            this.mShiftLockEnabled = true;
        }

        public boolean isInside(int paramInt1, int paramInt2)
        {
            boolean bool = false;
            if (!this.mEnabled)
                return bool;
            int i = this.codes[0];
            if ((i == -1) || (i == -5))
            {
                paramInt2 -= this.height / 10;
                if (i == -1)
                    paramInt1 += this.width / 6;
                if (i == -5)
                    paramInt1 -= this.width / 6;
            }
            while (true)
            {
                bool = super.isInside(paramInt1, paramInt2);
                break;
                if (i == 32)
                    paramInt2 += PasswordEntryKeyboard.sSpacebarVerticalCorrection;
            }
        }

        public void onReleased(boolean paramBoolean)
        {
            if (!this.mShiftLockEnabled)
            {
                super.onReleased(paramBoolean);
                return;
            }
            if (!this.pressed);
            for (boolean bool = true; ; bool = false)
            {
                this.pressed = bool;
                break;
            }
        }

        void setEnabled(boolean paramBoolean)
        {
            this.mEnabled = paramBoolean;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.PasswordEntryKeyboard
 * JD-Core Version:        0.6.2
 */