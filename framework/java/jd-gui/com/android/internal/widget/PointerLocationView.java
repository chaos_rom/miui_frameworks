package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.hardware.input.InputManager;
import android.hardware.input.InputManager.InputDeviceListener;
import android.os.SystemProperties;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.MotionEvent.PointerCoords;
import android.view.VelocityTracker;
import android.view.VelocityTracker.Estimator;
import android.view.View;
import android.view.ViewConfiguration;
import java.util.ArrayList;

public class PointerLocationView extends View
    implements InputManager.InputDeviceListener
{
    private static final String ALT_STRATEGY_PROPERY_KEY = "debug.velocitytracker.alt";
    private static final String TAG = "Pointer";
    private final int ESTIMATE_FUTURE_POINTS = 2;
    private final float ESTIMATE_INTERVAL = 0.02F;
    private final int ESTIMATE_PAST_POINTS = 4;
    private int mActivePointerId;
    private final VelocityTracker mAltVelocity;
    private boolean mCurDown;
    private int mCurNumPointers;
    private int mHeaderBottom;
    private final InputManager mIm;
    private int mMaxNumPointers;
    private final Paint mPaint;
    private final Paint mPathPaint;
    private final ArrayList<PointerState> mPointers = new ArrayList();
    private boolean mPrintCoords = true;
    private RectF mReusableOvalRect = new RectF();
    private final Paint mTargetPaint;
    private final MotionEvent.PointerCoords mTempCoords = new MotionEvent.PointerCoords();
    private final FasterStringBuilder mText = new FasterStringBuilder();
    private final Paint mTextBackgroundPaint;
    private final Paint mTextLevelPaint;
    private final Paint.FontMetricsInt mTextMetrics = new Paint.FontMetricsInt();
    private final Paint mTextPaint;
    private final ViewConfiguration mVC;
    private final VelocityTracker mVelocity;

    public PointerLocationView(Context paramContext)
    {
        super(paramContext);
        setFocusableInTouchMode(true);
        this.mIm = ((InputManager)paramContext.getSystemService("input"));
        this.mVC = ViewConfiguration.get(paramContext);
        this.mTextPaint = new Paint();
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextSize(10.0F * getResources().getDisplayMetrics().density);
        this.mTextPaint.setARGB(255, 0, 0, 0);
        this.mTextBackgroundPaint = new Paint();
        this.mTextBackgroundPaint.setAntiAlias(false);
        this.mTextBackgroundPaint.setARGB(128, 255, 255, 255);
        this.mTextLevelPaint = new Paint();
        this.mTextLevelPaint.setAntiAlias(false);
        this.mTextLevelPaint.setARGB(192, 255, 0, 0);
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setARGB(255, 255, 255, 255);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeWidth(2.0F);
        this.mTargetPaint = new Paint();
        this.mTargetPaint.setAntiAlias(false);
        this.mTargetPaint.setARGB(255, 0, 0, 192);
        this.mPathPaint = new Paint();
        this.mPathPaint.setAntiAlias(false);
        this.mPathPaint.setARGB(255, 0, 96, 255);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeWidth(1.0F);
        PointerState localPointerState = new PointerState();
        this.mPointers.add(localPointerState);
        this.mActivePointerId = 0;
        this.mVelocity = VelocityTracker.obtain();
        String str = SystemProperties.get("debug.velocitytracker.alt");
        if (str.length() != 0)
            Log.d("Pointer", "Comparing default velocity tracker strategy with " + str);
        for (this.mAltVelocity = VelocityTracker.obtain(str); ; this.mAltVelocity = null)
            return;
    }

    private void drawOval(Canvas paramCanvas, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, Paint paramPaint)
    {
        paramCanvas.save(1);
        paramCanvas.rotate((float)(180.0F * paramFloat5 / 3.141592653589793D), paramFloat1, paramFloat2);
        this.mReusableOvalRect.left = (paramFloat1 - paramFloat4 / 2.0F);
        this.mReusableOvalRect.right = (paramFloat1 + paramFloat4 / 2.0F);
        this.mReusableOvalRect.top = (paramFloat2 - paramFloat3 / 2.0F);
        this.mReusableOvalRect.bottom = (paramFloat2 + paramFloat3 / 2.0F);
        paramCanvas.drawOval(this.mReusableOvalRect, paramPaint);
        paramCanvas.restore();
    }

    private void logCoords(String paramString, int paramInt1, int paramInt2, MotionEvent.PointerCoords paramPointerCoords, int paramInt3, int paramInt4, int paramInt5)
    {
        String str;
        switch (paramInt1 & 0xFF)
        {
        default:
            str = Integer.toString(paramInt1);
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 9:
        case 10:
        case 8:
        }
        while (true)
        {
            Log.i("Pointer", this.mText.clear().append(paramString).append(" id ").append(paramInt3 + 1).append(": ").append(str).append(" (").append(paramPointerCoords.x, 3).append(", ").append(paramPointerCoords.y, 3).append(") Pressure=").append(paramPointerCoords.pressure, 3).append(" Size=").append(paramPointerCoords.size, 3).append(" TouchMajor=").append(paramPointerCoords.touchMajor, 3).append(" TouchMinor=").append(paramPointerCoords.touchMinor, 3).append(" ToolMajor=").append(paramPointerCoords.toolMajor, 3).append(" ToolMinor=").append(paramPointerCoords.toolMinor, 3).append(" Orientation=").append((float)(180.0F * paramPointerCoords.orientation / 3.141592653589793D), 1).append("deg").append(" Tilt=").append((float)(180.0F * paramPointerCoords.getAxisValue(25) / 3.141592653589793D), 1).append("deg").append(" Distance=").append(paramPointerCoords.getAxisValue(24), 1).append(" VScroll=").append(paramPointerCoords.getAxisValue(9), 1).append(" HScroll=").append(paramPointerCoords.getAxisValue(10), 1).append(" ToolType=").append(MotionEvent.toolTypeToString(paramInt4)).append(" ButtonState=").append(MotionEvent.buttonStateToString(paramInt5)).toString());
            return;
            str = "DOWN";
            continue;
            str = "UP";
            continue;
            str = "MOVE";
            continue;
            str = "CANCEL";
            continue;
            str = "OUTSIDE";
            continue;
            if (paramInt2 == (0xFF00 & paramInt1) >> 8)
            {
                str = "DOWN";
            }
            else
            {
                str = "MOVE";
                continue;
                if (paramInt2 == (0xFF00 & paramInt1) >> 8)
                {
                    str = "UP";
                }
                else
                {
                    str = "MOVE";
                    continue;
                    str = "HOVER MOVE";
                    continue;
                    str = "HOVER ENTER";
                    continue;
                    str = "HOVER EXIT";
                    continue;
                    str = "SCROLL";
                }
            }
        }
    }

    private void logInputDeviceState(int paramInt, String paramString)
    {
        InputDevice localInputDevice = this.mIm.getInputDevice(paramInt);
        if (localInputDevice != null)
            Log.i("Pointer", paramString + ": " + localInputDevice);
        while (true)
        {
            return;
            Log.i("Pointer", paramString + ": " + paramInt);
        }
    }

    private void logInputDevices()
    {
        int[] arrayOfInt = InputDevice.getDeviceIds();
        for (int i = 0; i < arrayOfInt.length; i++)
            logInputDeviceState(arrayOfInt[i], "Device Enumerated");
    }

    private void logMotionEvent(String paramString, MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getAction();
        int j = paramMotionEvent.getHistorySize();
        int k = paramMotionEvent.getPointerCount();
        for (int m = 0; m < j; m++)
            for (int i2 = 0; i2 < k; i2++)
            {
                int i3 = paramMotionEvent.getPointerId(i2);
                paramMotionEvent.getHistoricalPointerCoords(i2, m, this.mTempCoords);
                logCoords(paramString, i, i2, this.mTempCoords, i3, paramMotionEvent.getToolType(i2), paramMotionEvent.getButtonState());
            }
        for (int n = 0; n < k; n++)
        {
            int i1 = paramMotionEvent.getPointerId(n);
            paramMotionEvent.getPointerCoords(n, this.mTempCoords);
            logCoords(paramString, i, n, this.mTempCoords, i1, paramMotionEvent.getToolType(n), paramMotionEvent.getButtonState());
        }
    }

    private static boolean shouldLogKey(int paramInt)
    {
        boolean bool = true;
        switch (paramInt)
        {
        default:
            if ((!KeyEvent.isGamepadButton(paramInt)) && (!KeyEvent.isModifierKey(paramInt)))
                break;
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
        }
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public void addPointerEvent(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getAction();
        int j = this.mPointers.size();
        if ((i == 0) || ((i & 0xFF) == 5))
        {
            int k = (0xFF00 & i) >> 8;
            if (i == 0)
            {
                for (int i10 = 0; i10 < j; i10++)
                {
                    PointerState localPointerState5 = (PointerState)this.mPointers.get(i10);
                    localPointerState5.clearTrace();
                    PointerState.access$102(localPointerState5, false);
                }
                this.mCurDown = true;
                this.mCurNumPointers = 0;
                this.mMaxNumPointers = 0;
                this.mVelocity.clear();
                if (this.mAltVelocity != null)
                    this.mAltVelocity.clear();
            }
            this.mCurNumPointers = (1 + this.mCurNumPointers);
            if (this.mMaxNumPointers < this.mCurNumPointers)
                this.mMaxNumPointers = this.mCurNumPointers;
            int m = paramMotionEvent.getPointerId(k);
            while (j <= m)
            {
                PointerState localPointerState1 = new PointerState();
                this.mPointers.add(localPointerState1);
                j++;
            }
            if ((this.mActivePointerId < 0) || (!((PointerState)this.mPointers.get(this.mActivePointerId)).mCurDown))
                this.mActivePointerId = m;
            PointerState.access$102((PointerState)this.mPointers.get(m), true);
        }
        int n = paramMotionEvent.getPointerCount();
        this.mVelocity.addMovement(paramMotionEvent);
        this.mVelocity.computeCurrentVelocity(1);
        if (this.mAltVelocity != null)
        {
            this.mAltVelocity.addMovement(paramMotionEvent);
            this.mAltVelocity.computeCurrentVelocity(1);
        }
        int i1 = paramMotionEvent.getHistorySize();
        for (int i2 = 0; i2 < i1; i2++)
        {
            int i8 = 0;
            if (i8 < n)
            {
                int i9 = paramMotionEvent.getPointerId(i8);
                PointerState localPointerState4;
                if (this.mCurDown)
                {
                    localPointerState4 = (PointerState)this.mPointers.get(i9);
                    label335: if (localPointerState4 == null)
                        break label419;
                }
                label419: for (MotionEvent.PointerCoords localPointerCoords2 = localPointerState4.mCoords; ; localPointerCoords2 = this.mTempCoords)
                {
                    paramMotionEvent.getHistoricalPointerCoords(i8, i2, localPointerCoords2);
                    if (this.mPrintCoords)
                        logCoords("Pointer", i, i8, localPointerCoords2, i9, paramMotionEvent.getToolType(i8), paramMotionEvent.getButtonState());
                    if (localPointerState4 != null)
                        localPointerState4.addTrace(localPointerCoords2.x, localPointerCoords2.y);
                    i8++;
                    break;
                    localPointerState4 = null;
                    break label335;
                }
            }
        }
        int i3 = 0;
        if (i3 < n)
        {
            int i7 = paramMotionEvent.getPointerId(i3);
            PointerState localPointerState3;
            if (this.mCurDown)
            {
                localPointerState3 = (PointerState)this.mPointers.get(i7);
                label473: if (localPointerState3 == null)
                    break label664;
            }
            label664: for (MotionEvent.PointerCoords localPointerCoords1 = localPointerState3.mCoords; ; localPointerCoords1 = this.mTempCoords)
            {
                paramMotionEvent.getPointerCoords(i3, localPointerCoords1);
                if (this.mPrintCoords)
                    logCoords("Pointer", i, i3, localPointerCoords1, i7, paramMotionEvent.getToolType(i3), paramMotionEvent.getButtonState());
                if (localPointerState3 != null)
                {
                    localPointerState3.addTrace(localPointerCoords1.x, localPointerCoords1.y);
                    PointerState.access$502(localPointerState3, this.mVelocity.getXVelocity(i7));
                    PointerState.access$602(localPointerState3, this.mVelocity.getYVelocity(i7));
                    this.mVelocity.getEstimator(i7, localPointerState3.mEstimator);
                    if (this.mAltVelocity != null)
                    {
                        PointerState.access$902(localPointerState3, this.mAltVelocity.getXVelocity(i7));
                        PointerState.access$1002(localPointerState3, this.mAltVelocity.getYVelocity(i7));
                        this.mAltVelocity.getEstimator(i7, localPointerState3.mAltEstimator);
                    }
                    PointerState.access$1102(localPointerState3, paramMotionEvent.getToolType(i3));
                }
                i3++;
                break;
                localPointerState3 = null;
                break label473;
            }
        }
        int i4;
        int i5;
        PointerState localPointerState2;
        if ((i == 1) || (i == 3) || ((i & 0xFF) == 6))
        {
            i4 = (0xFF00 & i) >> 8;
            i5 = paramMotionEvent.getPointerId(i4);
            localPointerState2 = (PointerState)this.mPointers.get(i5);
            PointerState.access$102(localPointerState2, false);
            if ((i == 1) || (i == 3))
            {
                this.mCurDown = false;
                this.mCurNumPointers = 0;
            }
        }
        else
        {
            invalidate();
            return;
        }
        this.mCurNumPointers = (-1 + this.mCurNumPointers);
        if (this.mActivePointerId == i5)
            if (i4 != 0)
                break label809;
        label809: for (int i6 = 1; ; i6 = 0)
        {
            this.mActivePointerId = paramMotionEvent.getPointerId(i6);
            localPointerState2.addTrace((0.0F / 0.0F), (0.0F / 0.0F));
            break;
        }
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.mIm.registerInputDeviceListener(this, getHandler());
        logInputDevices();
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.mIm.unregisterInputDeviceListener(this);
    }

    protected void onDraw(Canvas paramCanvas)
    {
        int i = getWidth();
        int j = i / 7;
        int k = 1 + -this.mTextMetrics.ascent;
        int m = this.mHeaderBottom;
        int n = this.mPointers.size();
        PointerState localPointerState2;
        int i9;
        int i1;
        PointerState localPointerState1;
        float f1;
        float f2;
        int i4;
        int i5;
        label683: float f23;
        float f24;
        if (this.mActivePointerId >= 0)
        {
            localPointerState2 = (PointerState)this.mPointers.get(this.mActivePointerId);
            paramCanvas.drawRect(0.0F, 0.0F, j - 1, m, this.mTextBackgroundPaint);
            paramCanvas.drawText(this.mText.clear().append("P: ").append(this.mCurNumPointers).append(" / ").append(this.mMaxNumPointers).toString(), 1.0F, k, this.mTextPaint);
            i9 = localPointerState2.mTraceCount;
            if (((this.mCurDown) && (localPointerState2.mCurDown)) || (i9 == 0))
            {
                paramCanvas.drawRect(j, 0.0F, -1 + j * 2, m, this.mTextBackgroundPaint);
                paramCanvas.drawText(this.mText.clear().append("X: ").append(localPointerState2.mCoords.x, 1).toString(), j + 1, k, this.mTextPaint);
                paramCanvas.drawRect(j * 2, 0.0F, -1 + j * 3, m, this.mTextBackgroundPaint);
                paramCanvas.drawText(this.mText.clear().append("Y: ").append(localPointerState2.mCoords.y, 1).toString(), 1 + j * 2, k, this.mTextPaint);
                paramCanvas.drawRect(j * 3, 0.0F, -1 + j * 4, m, this.mTextBackgroundPaint);
                paramCanvas.drawText(this.mText.clear().append("Xv: ").append(localPointerState2.mXVelocity, 3).toString(), 1 + j * 3, k, this.mTextPaint);
                paramCanvas.drawRect(j * 4, 0.0F, -1 + j * 5, m, this.mTextBackgroundPaint);
                paramCanvas.drawText(this.mText.clear().append("Yv: ").append(localPointerState2.mYVelocity, 3).toString(), 1 + j * 4, k, this.mTextPaint);
                paramCanvas.drawRect(j * 5, 0.0F, -1 + j * 6, m, this.mTextBackgroundPaint);
                paramCanvas.drawRect(j * 5, 0.0F, j * 5 + localPointerState2.mCoords.pressure * j - 1.0F, m, this.mTextLevelPaint);
                paramCanvas.drawText(this.mText.clear().append("Prs: ").append(localPointerState2.mCoords.pressure, 2).toString(), 1 + j * 5, k, this.mTextPaint);
                paramCanvas.drawRect(j * 6, 0.0F, i, m, this.mTextBackgroundPaint);
                paramCanvas.drawRect(j * 6, 0.0F, j * 6 + localPointerState2.mCoords.size * j - 1.0F, m, this.mTextLevelPaint);
                paramCanvas.drawText(this.mText.clear().append("Size: ").append(localPointerState2.mCoords.size, 2).toString(), 1 + j * 6, k, this.mTextPaint);
            }
        }
        else
        {
            i1 = 0;
            if (i1 >= n)
                return;
            localPointerState1 = (PointerState)this.mPointers.get(i1);
            int i2 = localPointerState1.mTraceCount;
            f1 = 0.0F;
            f2 = 0.0F;
            i3 = 0;
            i4 = 0;
            this.mPaint.setARGB(255, 128, 255, 255);
            i5 = 0;
            if (i5 >= i2)
                break label1024;
            f23 = localPointerState1.mTraceX[i5];
            f24 = localPointerState1.mTraceY[i5];
            if (!Float.isNaN(f23))
                break label974;
        }
        for (int i3 = 0; ; i3 = 1)
        {
            i5++;
            break label683;
            float f25 = localPointerState2.mTraceX[(i9 - 1)] - localPointerState2.mTraceX[0];
            float f26 = localPointerState2.mTraceY[(i9 - 1)] - localPointerState2.mTraceY[0];
            float f27 = j;
            float f28 = -1 + j * 2;
            float f29 = m;
            Paint localPaint3;
            label808: float f30;
            float f31;
            float f32;
            if (Math.abs(f25) < this.mVC.getScaledTouchSlop())
            {
                localPaint3 = this.mTextBackgroundPaint;
                paramCanvas.drawRect(f27, 0.0F, f28, f29, localPaint3);
                paramCanvas.drawText(this.mText.clear().append("dX: ").append(f25, 1).toString(), j + 1, k, this.mTextPaint);
                f30 = j * 2;
                f31 = -1 + j * 3;
                f32 = m;
                if (Math.abs(f26) >= this.mVC.getScaledTouchSlop())
                    break label965;
            }
            label965: for (Paint localPaint4 = this.mTextBackgroundPaint; ; localPaint4 = this.mTextLevelPaint)
            {
                paramCanvas.drawRect(f30, 0.0F, f31, f32, localPaint4);
                paramCanvas.drawText(this.mText.clear().append("dY: ").append(f26, 1).toString(), 1 + j * 2, k, this.mTextPaint);
                break;
                localPaint3 = this.mTextLevelPaint;
                break label808;
            }
            label974: if (i3 != 0)
            {
                paramCanvas.drawLine(f1, f2, f23, f24, this.mPathPaint);
                paramCanvas.drawPoint(f1, f2, this.mPaint);
                i4 = 1;
            }
            f1 = f23;
            f2 = f24;
        }
        label1024: if (i4 != 0)
        {
            this.mPaint.setARGB(128, 128, 0, 128);
            float f7 = localPointerState1.mEstimator.estimateX(-0.08F);
            float f8 = localPointerState1.mEstimator.estimateY(-0.08F);
            for (int i7 = -3; i7 <= 2; i7++)
            {
                float f21 = localPointerState1.mEstimator.estimateX(0.02F * i7);
                float f22 = localPointerState1.mEstimator.estimateY(0.02F * i7);
                paramCanvas.drawLine(f7, f8, f21, f22, this.mPaint);
                f7 = f21;
                f8 = f22;
            }
            this.mPaint.setARGB(255, 255, 64, 128);
            float f9 = 16.0F * localPointerState1.mXVelocity;
            float f10 = 16.0F * localPointerState1.mYVelocity;
            float f11 = f1 + f9;
            float f12 = f2 + f10;
            Paint localPaint1 = this.mPaint;
            paramCanvas.drawLine(f1, f2, f11, f12, localPaint1);
            if (this.mAltVelocity != null)
            {
                this.mPaint.setARGB(128, 0, 128, 128);
                float f13 = localPointerState1.mAltEstimator.estimateX(-0.08F);
                float f14 = localPointerState1.mAltEstimator.estimateY(-0.08F);
                for (int i8 = -3; i8 <= 2; i8++)
                {
                    float f19 = localPointerState1.mAltEstimator.estimateX(0.02F * i8);
                    float f20 = localPointerState1.mAltEstimator.estimateY(0.02F * i8);
                    paramCanvas.drawLine(f13, f14, f19, f20, this.mPaint);
                    f13 = f19;
                    f14 = f20;
                }
                this.mPaint.setARGB(255, 64, 255, 128);
                float f15 = 16.0F * localPointerState1.mAltXVelocity;
                float f16 = 16.0F * localPointerState1.mAltYVelocity;
                float f17 = f1 + f15;
                float f18 = f2 + f16;
                Paint localPaint2 = this.mPaint;
                paramCanvas.drawLine(f1, f2, f17, f18, localPaint2);
            }
        }
        float f4;
        float f5;
        if ((this.mCurDown) && (localPointerState1.mCurDown))
        {
            paramCanvas.drawLine(0.0F, localPointerState1.mCoords.y, getWidth(), localPointerState1.mCoords.y, this.mTargetPaint);
            paramCanvas.drawLine(localPointerState1.mCoords.x, 0.0F, localPointerState1.mCoords.x, getHeight(), this.mTargetPaint);
            int i6 = (int)(255.0F * localPointerState1.mCoords.pressure);
            this.mPaint.setARGB(255, i6, 255, 255 - i6);
            paramCanvas.drawPoint(localPointerState1.mCoords.x, localPointerState1.mCoords.y, this.mPaint);
            this.mPaint.setARGB(255, i6, 255 - i6, 128);
            drawOval(paramCanvas, localPointerState1.mCoords.x, localPointerState1.mCoords.y, localPointerState1.mCoords.touchMajor, localPointerState1.mCoords.touchMinor, localPointerState1.mCoords.orientation, this.mPaint);
            this.mPaint.setARGB(255, i6, 128, 255 - i6);
            drawOval(paramCanvas, localPointerState1.mCoords.x, localPointerState1.mCoords.y, localPointerState1.mCoords.toolMajor, localPointerState1.mCoords.toolMinor, localPointerState1.mCoords.orientation, this.mPaint);
            float f3 = 0.7F * localPointerState1.mCoords.toolMajor;
            if (f3 < 20.0F)
                f3 = 20.0F;
            this.mPaint.setARGB(255, i6, 255, 0);
            f4 = (float)(Math.sin(localPointerState1.mCoords.orientation) * f3);
            f5 = (float)(-Math.cos(localPointerState1.mCoords.orientation) * f3);
            if ((localPointerState1.mToolType != 2) && (localPointerState1.mToolType != 4))
                break label1898;
            paramCanvas.drawLine(localPointerState1.mCoords.x, localPointerState1.mCoords.y, f4 + localPointerState1.mCoords.x, f5 + localPointerState1.mCoords.y, this.mPaint);
        }
        while (true)
        {
            float f6 = (float)Math.sin(localPointerState1.mCoords.getAxisValue(25));
            paramCanvas.drawCircle(localPointerState1.mCoords.x + f4 * f6, localPointerState1.mCoords.y + f5 * f6, 3.0F, this.mPaint);
            i1++;
            break;
            label1898: paramCanvas.drawLine(localPointerState1.mCoords.x - f4, localPointerState1.mCoords.y - f5, f4 + localPointerState1.mCoords.x, f5 + localPointerState1.mCoords.y, this.mPaint);
        }
    }

    public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getSource();
        if ((i & 0x2) != 0)
            addPointerEvent(paramMotionEvent);
        while (true)
        {
            return true;
            if ((i & 0x10) != 0)
                logMotionEvent("Joystick", paramMotionEvent);
            else if ((i & 0x8) != 0)
                logMotionEvent("Position", paramMotionEvent);
            else
                logMotionEvent("Generic", paramMotionEvent);
        }
    }

    public void onInputDeviceAdded(int paramInt)
    {
        logInputDeviceState(paramInt, "Device Added");
    }

    public void onInputDeviceChanged(int paramInt)
    {
        logInputDeviceState(paramInt, "Device Changed");
    }

    public void onInputDeviceRemoved(int paramInt)
    {
        logInputDeviceState(paramInt, "Device Removed");
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        int i;
        if (shouldLogKey(paramInt))
        {
            i = paramKeyEvent.getRepeatCount();
            if (i == 0)
                Log.i("Pointer", "Key Down: " + paramKeyEvent);
        }
        for (boolean bool = true; ; bool = super.onKeyDown(paramInt, paramKeyEvent))
        {
            return bool;
            Log.i("Pointer", "Key Repeat #" + i + ": " + paramKeyEvent);
            break;
        }
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        if (shouldLogKey(paramInt))
            Log.i("Pointer", "Key Up: " + paramKeyEvent);
        for (boolean bool = true; ; bool = super.onKeyUp(paramInt, paramKeyEvent))
            return bool;
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        this.mTextPaint.getFontMetricsInt(this.mTextMetrics);
        this.mHeaderBottom = (2 + (-this.mTextMetrics.ascent + this.mTextMetrics.descent));
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        addPointerEvent(paramMotionEvent);
        if ((paramMotionEvent.getAction() == 0) && (!isFocused()))
            requestFocus();
        return true;
    }

    public boolean onTrackballEvent(MotionEvent paramMotionEvent)
    {
        logMotionEvent("Trackball", paramMotionEvent);
        return true;
    }

    public void setPrintCoords(boolean paramBoolean)
    {
        this.mPrintCoords = paramBoolean;
    }

    private static final class FasterStringBuilder
    {
        private char[] mChars = new char[64];
        private int mLength;

        private int reserve(int paramInt)
        {
            int i = this.mLength;
            int j = paramInt + this.mLength;
            char[] arrayOfChar1 = this.mChars;
            int k = arrayOfChar1.length;
            if (j > k)
            {
                char[] arrayOfChar2 = new char[k * 2];
                System.arraycopy(arrayOfChar1, 0, arrayOfChar2, 0, i);
                this.mChars = arrayOfChar2;
            }
            return i;
        }

        public FasterStringBuilder append(float paramFloat, int paramInt)
        {
            int i = 1;
            for (int j = 0; j < paramInt; j++)
                i *= 10;
            float f1 = (float)(Math.rint(paramFloat * i) / i);
            append((int)f1);
            if (paramInt != 0)
            {
                append(".");
                float f2 = Math.abs(f1);
                append((int)((float)(f2 - Math.floor(f2)) * i), paramInt);
            }
            return this;
        }

        public FasterStringBuilder append(int paramInt)
        {
            return append(paramInt, 0);
        }

        public FasterStringBuilder append(int paramInt1, int paramInt2)
        {
            int i;
            if (paramInt1 < 0)
            {
                i = 1;
                if (i == 0)
                    break label31;
                paramInt1 = -paramInt1;
                if (paramInt1 >= 0)
                    break label31;
                append("-2147483648");
            }
            while (true)
            {
                return this;
                i = 0;
                break;
                label31: int j = reserve(11);
                char[] arrayOfChar = this.mChars;
                if (paramInt1 == 0)
                {
                    (j + 1);
                    arrayOfChar[j] = '0';
                    this.mLength = (1 + this.mLength);
                }
                else
                {
                    if (i != 0)
                    {
                        int i4 = j + 1;
                        arrayOfChar[j] = '-';
                        j = i4;
                    }
                    int k = 1000000000;
                    int m = 10;
                    int n = j;
                    while (paramInt1 < k)
                    {
                        k /= 10;
                        m--;
                        if (m < paramInt2)
                        {
                            int i3 = n + 1;
                            arrayOfChar[n] = '0';
                            n = i3;
                        }
                    }
                    do
                    {
                        int i1 = n;
                        int i2 = paramInt1 / k;
                        paramInt1 -= i2 * k;
                        k /= 10;
                        n = i1 + 1;
                        arrayOfChar[i1] = ((char)(i2 + 48));
                    }
                    while (k != 0);
                    this.mLength = n;
                }
            }
        }

        public FasterStringBuilder append(String paramString)
        {
            int i = paramString.length();
            int j = reserve(i);
            paramString.getChars(0, i, this.mChars, j);
            this.mLength = (i + this.mLength);
            return this;
        }

        public FasterStringBuilder clear()
        {
            this.mLength = 0;
            return this;
        }

        public String toString()
        {
            return new String(this.mChars, 0, this.mLength);
        }
    }

    public static class PointerState
    {
        private VelocityTracker.Estimator mAltEstimator = new VelocityTracker.Estimator();
        private float mAltXVelocity;
        private float mAltYVelocity;
        private MotionEvent.PointerCoords mCoords = new MotionEvent.PointerCoords();
        private boolean mCurDown;
        private VelocityTracker.Estimator mEstimator = new VelocityTracker.Estimator();
        private int mToolType;
        private int mTraceCount;
        private float[] mTraceX = new float[32];
        private float[] mTraceY = new float[32];
        private float mXVelocity;
        private float mYVelocity;

        public void addTrace(float paramFloat1, float paramFloat2)
        {
            int i = this.mTraceX.length;
            if (this.mTraceCount == i)
            {
                int j = i * 2;
                float[] arrayOfFloat1 = new float[j];
                System.arraycopy(this.mTraceX, 0, arrayOfFloat1, 0, this.mTraceCount);
                this.mTraceX = arrayOfFloat1;
                float[] arrayOfFloat2 = new float[j];
                System.arraycopy(this.mTraceY, 0, arrayOfFloat2, 0, this.mTraceCount);
                this.mTraceY = arrayOfFloat2;
            }
            this.mTraceX[this.mTraceCount] = paramFloat1;
            this.mTraceY[this.mTraceCount] = paramFloat2;
            this.mTraceCount = (1 + this.mTraceCount);
        }

        public void clearTrace()
        {
            this.mTraceCount = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.PointerLocationView
 * JD-Core Version:        0.6.2
 */