package com.android.internal.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActionBar.Tab;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import com.android.internal.view.ActionBarPolicy;

public class ScrollingTabContainerView extends HorizontalScrollView
    implements AdapterView.OnItemClickListener
{
    private static final int FADE_DURATION = 200;
    private static final String TAG = "ScrollingTabContainerView";
    private static final TimeInterpolator sAlphaInterpolator = new DecelerateInterpolator();
    private boolean mAllowCollapse;
    private int mContentHeight;
    int mMaxTabWidth;
    private int mSelectedTabIndex;
    int mStackedTabMaxWidth;
    private TabClickListener mTabClickListener;
    private LinearLayout mTabLayout;
    Runnable mTabSelector;
    private Spinner mTabSpinner;
    protected final VisibilityAnimListener mVisAnimListener = new VisibilityAnimListener();
    protected Animator mVisibilityAnim;

    public ScrollingTabContainerView(Context paramContext)
    {
        super(paramContext);
        setHorizontalScrollBarEnabled(false);
        ActionBarPolicy localActionBarPolicy = ActionBarPolicy.get(paramContext);
        setContentHeight(localActionBarPolicy.getTabContainerHeight());
        this.mStackedTabMaxWidth = localActionBarPolicy.getStackedTabMaxWidth();
        this.mTabLayout = createTabLayout();
        addView(this.mTabLayout, new ViewGroup.LayoutParams(-2, -1));
    }

    private Spinner createSpinner()
    {
        Spinner localSpinner = new Spinner(getContext(), null, 16843479);
        localSpinner.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        localSpinner.setOnItemClickListenerInt(this);
        return localSpinner;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private LinearLayout createTabLayout()
    {
        MiuiTabLayout localMiuiTabLayout = new MiuiTabLayout(getContext(), null, 16843508);
        localMiuiTabLayout.setMeasureWithLargestChildEnabled(true);
        localMiuiTabLayout.setGravity(17);
        localMiuiTabLayout.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        return localMiuiTabLayout;
    }

    private TabView createTabView(ActionBar.Tab paramTab, boolean paramBoolean)
    {
        TabView localTabView = new TabView(getContext(), paramTab, paramBoolean);
        if (paramBoolean)
        {
            localTabView.setBackgroundDrawable(null);
            localTabView.setLayoutParams(new AbsListView.LayoutParams(-1, this.mContentHeight));
        }
        while (true)
        {
            return localTabView;
            localTabView.setFocusable(true);
            if (this.mTabClickListener == null)
                this.mTabClickListener = new TabClickListener(null);
            localTabView.setOnClickListener(this.mTabClickListener);
        }
    }

    private boolean isCollapsed()
    {
        if ((this.mTabSpinner != null) && (this.mTabSpinner.getParent() == this));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void performCollapse()
    {
        if (isCollapsed());
        while (true)
        {
            return;
            if (this.mTabSpinner == null)
                this.mTabSpinner = createSpinner();
            removeView(this.mTabLayout);
            addView(this.mTabSpinner, new ViewGroup.LayoutParams(-2, -1));
            if (this.mTabSpinner.getAdapter() == null)
                this.mTabSpinner.setAdapter(new TabAdapter(null));
            if (this.mTabSelector != null)
            {
                removeCallbacks(this.mTabSelector);
                this.mTabSelector = null;
            }
            this.mTabSpinner.setSelection(this.mSelectedTabIndex);
        }
    }

    private boolean performExpand()
    {
        if (!isCollapsed());
        while (true)
        {
            return false;
            removeView(this.mTabSpinner);
            addView(this.mTabLayout, new ViewGroup.LayoutParams(-2, -1));
            setTabSelected(this.mTabSpinner.getSelectedItemPosition());
        }
    }

    public void addTab(ActionBar.Tab paramTab, int paramInt, boolean paramBoolean)
    {
        TabView localTabView = createTabView(paramTab, false);
        this.mTabLayout.addView(localTabView, paramInt, new LinearLayout.LayoutParams(0, -1, 1.0F));
        if (this.mTabSpinner != null)
            ((TabAdapter)this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        if (paramBoolean)
            localTabView.setSelected(true);
        if (this.mAllowCollapse)
            requestLayout();
    }

    public void addTab(ActionBar.Tab paramTab, boolean paramBoolean)
    {
        TabView localTabView = createTabView(paramTab, false);
        this.mTabLayout.addView(localTabView, new LinearLayout.LayoutParams(0, -1, 1.0F));
        if (this.mTabSpinner != null)
            ((TabAdapter)this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        if (paramBoolean)
            localTabView.setSelected(true);
        if (this.mAllowCollapse)
            requestLayout();
    }

    public void animateToTab(int paramInt)
    {
        final View localView = this.mTabLayout.getChildAt(paramInt);
        if (this.mTabSelector != null)
            removeCallbacks(this.mTabSelector);
        this.mTabSelector = new Runnable()
        {
            public void run()
            {
                int i = localView.getLeft() - (ScrollingTabContainerView.this.getWidth() - localView.getWidth()) / 2;
                ScrollingTabContainerView.this.smoothScrollTo(i, 0);
                ScrollingTabContainerView.this.mTabSelector = null;
            }
        };
        post(this.mTabSelector);
    }

    public void animateToVisibility(int paramInt)
    {
        if (this.mVisibilityAnim != null)
            this.mVisibilityAnim.cancel();
        if (paramInt == 0)
        {
            if (getVisibility() != 0)
                setAlpha(0.0F);
            float[] arrayOfFloat2 = new float[1];
            arrayOfFloat2[0] = 1.0F;
            ObjectAnimator localObjectAnimator2 = ObjectAnimator.ofFloat(this, "alpha", arrayOfFloat2);
            localObjectAnimator2.setDuration(200L);
            localObjectAnimator2.setInterpolator(sAlphaInterpolator);
            localObjectAnimator2.addListener(this.mVisAnimListener.withFinalVisibility(paramInt));
            localObjectAnimator2.start();
        }
        while (true)
        {
            return;
            float[] arrayOfFloat1 = new float[1];
            arrayOfFloat1[0] = 0.0F;
            ObjectAnimator localObjectAnimator1 = ObjectAnimator.ofFloat(this, "alpha", arrayOfFloat1);
            localObjectAnimator1.setDuration(200L);
            localObjectAnimator1.setInterpolator(sAlphaInterpolator);
            localObjectAnimator1.addListener(this.mVisAnimListener.withFinalVisibility(paramInt));
            localObjectAnimator1.start();
        }
    }

    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (this.mTabSelector != null)
            post(this.mTabSelector);
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        ActionBarPolicy localActionBarPolicy = ActionBarPolicy.get(getContext());
        setContentHeight(localActionBarPolicy.getTabContainerHeight());
        this.mStackedTabMaxWidth = localActionBarPolicy.getStackedTabMaxWidth();
    }

    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.mTabSelector != null)
            removeCallbacks(this.mTabSelector);
    }

    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
        ((TabView)paramView).getTab().select();
    }

    public void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getMode(paramInt1);
        boolean bool;
        label70: label85: int k;
        int m;
        if (i == 1073741824)
        {
            bool = true;
            setFillViewport(bool);
            int j = this.mTabLayout.getChildCount();
            if ((j <= 1) || ((i != 1073741824) && (i != -2147483648)))
                break label204;
            if (j <= 2)
                break label191;
            this.mMaxTabWidth = ((int)(0.4F * View.MeasureSpec.getSize(paramInt1)));
            this.mMaxTabWidth = Math.min(this.mMaxTabWidth, this.mStackedTabMaxWidth);
            k = View.MeasureSpec.makeMeasureSpec(this.mContentHeight, 1073741824);
            if ((bool) || (!this.mAllowCollapse))
                break label213;
            m = 1;
            label112: if (m == 0)
                break label227;
            this.mTabLayout.measure(0, k);
            if (this.mTabLayout.getMeasuredWidth() <= View.MeasureSpec.getSize(paramInt1))
                break label219;
            performCollapse();
        }
        while (true)
        {
            int n = getMeasuredWidth();
            super.onMeasure(paramInt1, k);
            int i1 = getMeasuredWidth();
            if ((bool) && (n != i1))
                setTabSelected(this.mSelectedTabIndex);
            return;
            bool = false;
            break;
            label191: this.mMaxTabWidth = (View.MeasureSpec.getSize(paramInt1) / 2);
            break label70;
            label204: this.mMaxTabWidth = -1;
            break label85;
            label213: m = 0;
            break label112;
            label219: performExpand();
            continue;
            label227: performExpand();
        }
    }

    public void removeAllTabs()
    {
        this.mTabLayout.removeAllViews();
        if (this.mTabSpinner != null)
            ((TabAdapter)this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        if (this.mAllowCollapse)
            requestLayout();
    }

    public void removeTabAt(int paramInt)
    {
        this.mTabLayout.removeViewAt(paramInt);
        if (this.mTabSpinner != null)
            ((TabAdapter)this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        if (this.mAllowCollapse)
            requestLayout();
    }

    public void setAllowCollapse(boolean paramBoolean)
    {
        this.mAllowCollapse = paramBoolean;
    }

    public void setContentHeight(int paramInt)
    {
        this.mContentHeight = paramInt;
        requestLayout();
    }

    public void setTabSelected(int paramInt)
    {
        this.mSelectedTabIndex = paramInt;
        int i = this.mTabLayout.getChildCount();
        int j = 0;
        if (j < i)
        {
            View localView = this.mTabLayout.getChildAt(j);
            if (j == paramInt);
            for (boolean bool = true; ; bool = false)
            {
                localView.setSelected(bool);
                if (bool)
                    animateToTab(paramInt);
                j++;
                break;
            }
        }
    }

    public void updateTab(int paramInt)
    {
        ((TabView)this.mTabLayout.getChildAt(paramInt)).update();
        if (this.mTabSpinner != null)
            ((TabAdapter)this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        if (this.mAllowCollapse)
            requestLayout();
    }

    protected class VisibilityAnimListener
        implements Animator.AnimatorListener
    {
        private boolean mCanceled = false;
        private int mFinalVisibility;

        protected VisibilityAnimListener()
        {
        }

        public void onAnimationCancel(Animator paramAnimator)
        {
            this.mCanceled = true;
        }

        public void onAnimationEnd(Animator paramAnimator)
        {
            if (this.mCanceled);
            while (true)
            {
                return;
                ScrollingTabContainerView.this.mVisibilityAnim = null;
                ScrollingTabContainerView.this.setVisibility(this.mFinalVisibility);
            }
        }

        public void onAnimationRepeat(Animator paramAnimator)
        {
        }

        public void onAnimationStart(Animator paramAnimator)
        {
            ScrollingTabContainerView.this.setVisibility(0);
            ScrollingTabContainerView.this.mVisibilityAnim = paramAnimator;
            this.mCanceled = false;
        }

        public VisibilityAnimListener withFinalVisibility(int paramInt)
        {
            this.mFinalVisibility = paramInt;
            return this;
        }
    }

    private class TabClickListener
        implements View.OnClickListener
    {
        private TabClickListener()
        {
        }

        public void onClick(View paramView)
        {
            ((ScrollingTabContainerView.TabView)paramView).getTab().select();
            int i = ScrollingTabContainerView.this.mTabLayout.getChildCount();
            int j = 0;
            if (j < i)
            {
                View localView = ScrollingTabContainerView.this.mTabLayout.getChildAt(j);
                if (localView == paramView);
                for (boolean bool = true; ; bool = false)
                {
                    localView.setSelected(bool);
                    j++;
                    break;
                }
            }
        }
    }

    private class TabAdapter extends BaseAdapter
    {
        private TabAdapter()
        {
        }

        public int getCount()
        {
            return ScrollingTabContainerView.this.mTabLayout.getChildCount();
        }

        public Object getItem(int paramInt)
        {
            return ((ScrollingTabContainerView.TabView)ScrollingTabContainerView.this.mTabLayout.getChildAt(paramInt)).getTab();
        }

        public long getItemId(int paramInt)
        {
            return paramInt;
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            if (paramView == null)
                paramView = ScrollingTabContainerView.this.createTabView((ActionBar.Tab)getItem(paramInt), true);
            while (true)
            {
                return paramView;
                ((ScrollingTabContainerView.TabView)paramView).bindTab((ActionBar.Tab)getItem(paramInt));
            }
        }
    }

    private class TabView extends LinearLayout
    {
        private View mCustomView;
        private ImageView mIconView;
        private ActionBar.Tab mTab;
        private TextView mTextView;

        public TabView(Context paramTab, ActionBar.Tab paramBoolean, boolean arg4)
        {
            super(null, 16843507);
            this.mTab = paramBoolean;
            int i;
            if (i != 0)
                setGravity(19);
            update();
        }

        public void bindTab(ActionBar.Tab paramTab)
        {
            this.mTab = paramTab;
            update();
        }

        public ActionBar.Tab getTab()
        {
            return this.mTab;
        }

        public void onMeasure(int paramInt1, int paramInt2)
        {
            super.onMeasure(paramInt1, paramInt2);
            if ((ScrollingTabContainerView.this.mMaxTabWidth > 0) && (getMeasuredWidth() > ScrollingTabContainerView.this.mMaxTabWidth))
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(ScrollingTabContainerView.this.mMaxTabWidth, 1073741824), paramInt2);
        }

        public void update()
        {
            ActionBar.Tab localTab = this.mTab;
            View localView = localTab.getCustomView();
            if (localView != null)
            {
                ViewParent localViewParent = localView.getParent();
                if (localViewParent != this)
                {
                    if (localViewParent != null)
                        ((ViewGroup)localViewParent).removeView(localView);
                    addView(localView);
                }
                this.mCustomView = localView;
                if (this.mTextView != null)
                    this.mTextView.setVisibility(8);
                if (this.mIconView != null)
                {
                    this.mIconView.setVisibility(8);
                    this.mIconView.setImageDrawable(null);
                }
            }
            label202: label342: label367: 
            while (true)
            {
                return;
                if (this.mCustomView != null)
                {
                    removeView(this.mCustomView);
                    this.mCustomView = null;
                }
                Drawable localDrawable = localTab.getIcon();
                CharSequence localCharSequence = localTab.getText();
                if (localDrawable != null)
                {
                    if (this.mIconView == null)
                    {
                        ImageView localImageView = new ImageView(getContext());
                        LinearLayout.LayoutParams localLayoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                        localLayoutParams2.gravity = 16;
                        localImageView.setLayoutParams(localLayoutParams2);
                        addView(localImageView, 0);
                        this.mIconView = localImageView;
                    }
                    this.mIconView.setImageDrawable(localDrawable);
                    this.mIconView.setVisibility(0);
                    if (localCharSequence == null)
                        break label342;
                    if (this.mTextView == null)
                    {
                        TextView localTextView = new TextView(getContext(), null, 16843509);
                        localTextView.setEllipsize(TextUtils.TruncateAt.END);
                        LinearLayout.LayoutParams localLayoutParams1 = new LinearLayout.LayoutParams(-2, -2);
                        localLayoutParams1.gravity = 16;
                        localTextView.setLayoutParams(localLayoutParams1);
                        addView(localTextView);
                        this.mTextView = localTextView;
                    }
                    this.mTextView.setText(localCharSequence);
                    this.mTextView.setVisibility(0);
                }
                while (true)
                {
                    if (this.mIconView == null)
                        break label367;
                    this.mIconView.setContentDescription(localTab.getContentDescription());
                    break;
                    if (this.mIconView == null)
                        break label202;
                    this.mIconView.setVisibility(8);
                    this.mIconView.setImageDrawable(null);
                    break label202;
                    if (this.mTextView != null)
                    {
                        this.mTextView.setVisibility(8);
                        this.mTextView.setText(null);
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.ScrollingTabContainerView
 * JD-Core Version:        0.6.2
 */