package com.android.internal.widget;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface ILockSettings extends IInterface
{
    public abstract boolean checkPassword(byte[] paramArrayOfByte, int paramInt)
        throws RemoteException;

    public abstract boolean checkPattern(byte[] paramArrayOfByte, int paramInt)
        throws RemoteException;

    public abstract boolean getBoolean(String paramString, boolean paramBoolean, int paramInt)
        throws RemoteException;

    public abstract long getLong(String paramString, long paramLong, int paramInt)
        throws RemoteException;

    public abstract String getString(String paramString1, String paramString2, int paramInt)
        throws RemoteException;

    public abstract boolean havePassword(int paramInt)
        throws RemoteException;

    public abstract boolean havePattern(int paramInt)
        throws RemoteException;

    public abstract void removeUser(int paramInt)
        throws RemoteException;

    public abstract void setBoolean(String paramString, boolean paramBoolean, int paramInt)
        throws RemoteException;

    public abstract void setLockPassword(byte[] paramArrayOfByte, int paramInt)
        throws RemoteException;

    public abstract void setLockPattern(byte[] paramArrayOfByte, int paramInt)
        throws RemoteException;

    public abstract void setLong(String paramString, long paramLong, int paramInt)
        throws RemoteException;

    public abstract void setString(String paramString1, String paramString2, int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ILockSettings
    {
        private static final String DESCRIPTOR = "com.android.internal.widget.ILockSettings";
        static final int TRANSACTION_checkPassword = 10;
        static final int TRANSACTION_checkPattern = 8;
        static final int TRANSACTION_getBoolean = 4;
        static final int TRANSACTION_getLong = 5;
        static final int TRANSACTION_getString = 6;
        static final int TRANSACTION_havePassword = 12;
        static final int TRANSACTION_havePattern = 11;
        static final int TRANSACTION_removeUser = 13;
        static final int TRANSACTION_setBoolean = 1;
        static final int TRANSACTION_setLockPassword = 9;
        static final int TRANSACTION_setLockPattern = 7;
        static final int TRANSACTION_setLong = 2;
        static final int TRANSACTION_setString = 3;

        public Stub()
        {
            attachInterface(this, "com.android.internal.widget.ILockSettings");
        }

        public static ILockSettings asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.widget.ILockSettings");
                if ((localIInterface != null) && ((localIInterface instanceof ILockSettings)))
                    localObject = (ILockSettings)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("com.android.internal.widget.ILockSettings");
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                String str3 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (boolean bool7 = j; ; bool7 = false)
                {
                    setBoolean(str3, bool7, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                setLong(paramParcel1.readString(), paramParcel1.readLong(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                setString(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                String str2 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (boolean bool5 = j; ; bool5 = false)
                {
                    boolean bool6 = getBoolean(str2, bool5, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool6)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                long l = getLong(paramParcel1.readString(), paramParcel1.readLong(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l);
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                String str1 = getString(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeString(str1);
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                setLockPattern(paramParcel1.createByteArray(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                boolean bool4 = checkPattern(paramParcel1.createByteArray(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool4)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                setLockPassword(paramParcel1.createByteArray(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                boolean bool3 = checkPassword(paramParcel1.createByteArray(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool3)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                boolean bool2 = havePattern(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                boolean bool1 = havePassword(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.ILockSettings");
                removeUser(paramParcel1.readInt());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements ILockSettings
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public boolean checkPassword(byte[] paramArrayOfByte, int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeByteArray(paramArrayOfByte);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean checkPattern(byte[] paramArrayOfByte, int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeByteArray(paramArrayOfByte);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getBoolean(String paramString, boolean paramBoolean, int paramInt)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeString(paramString);
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.widget.ILockSettings";
            }

            public long getLong(String paramString, long paramLong, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeString(paramString);
                    localParcel1.writeLong(paramLong);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getString(String paramString1, String paramString2, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean havePassword(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean havePattern(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeUser(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBoolean(String paramString, boolean paramBoolean, int paramInt)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                    {
                        localParcel1.writeInt(i);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setLockPassword(byte[] paramArrayOfByte, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeByteArray(paramArrayOfByte);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setLockPattern(byte[] paramArrayOfByte, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeByteArray(paramArrayOfByte);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setLong(String paramString, long paramLong, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeString(paramString);
                    localParcel1.writeLong(paramLong);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setString(String paramString1, String paramString2, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.ILockSettings
 * JD-Core Version:        0.6.2
 */