package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.SystemClock;
import android.provider.Settings.System;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewRootImpl;

public class PasswordEntryKeyboardHelper
    implements KeyboardView.OnKeyboardActionListener
{
    public static final int KEYBOARD_MODE_ALPHA = 0;
    public static final int KEYBOARD_MODE_NUMERIC = 1;
    private static final int KEYBOARD_STATE_CAPSLOCK = 2;
    private static final int KEYBOARD_STATE_NORMAL = 0;
    private static final int KEYBOARD_STATE_SHIFTED = 1;
    private static final String TAG = "PasswordEntryKeyboardHelper";
    private final Context mContext;
    private boolean mEnableHaptics = false;
    private int mKeyboardMode = 0;
    private int mKeyboardState = 0;
    private final KeyboardView mKeyboardView;
    private PasswordEntryKeyboard mNumericKeyboard;
    private PasswordEntryKeyboard mQwertyKeyboard;
    private PasswordEntryKeyboard mQwertyKeyboardShifted;
    private PasswordEntryKeyboard mSymbolsKeyboard;
    private PasswordEntryKeyboard mSymbolsKeyboardShifted;
    private final View mTargetView;
    private long[] mVibratePattern;

    public PasswordEntryKeyboardHelper(Context paramContext, KeyboardView paramKeyboardView, View paramView)
    {
        this(paramContext, paramKeyboardView, paramView, true);
    }

    public PasswordEntryKeyboardHelper(Context paramContext, KeyboardView paramKeyboardView, View paramView, boolean paramBoolean)
    {
        this.mContext = paramContext;
        this.mTargetView = paramView;
        this.mKeyboardView = paramKeyboardView;
        if ((paramBoolean) || (this.mKeyboardView.getLayoutParams().width == -1))
            createKeyboards();
        while (true)
        {
            this.mKeyboardView.setOnKeyboardActionListener(this);
            return;
            createKeyboardsWithSpecificSize(this.mKeyboardView.getLayoutParams().width, this.mKeyboardView.getLayoutParams().height);
        }
    }

    private void createKeyboards()
    {
        this.mNumericKeyboard = new PasswordEntryKeyboard(this.mContext, 17760260);
        this.mQwertyKeyboard = new PasswordEntryKeyboard(this.mContext, 17760262, 16909169);
        this.mQwertyKeyboard.enableShiftLock();
        this.mQwertyKeyboardShifted = new PasswordEntryKeyboard(this.mContext, 17760263, 16909169);
        this.mQwertyKeyboardShifted.enableShiftLock();
        this.mQwertyKeyboardShifted.setShifted(true);
        this.mSymbolsKeyboard = new PasswordEntryKeyboard(this.mContext, 17760264);
        this.mSymbolsKeyboard.enableShiftLock();
        this.mSymbolsKeyboardShifted = new PasswordEntryKeyboard(this.mContext, 17760265);
        this.mSymbolsKeyboardShifted.enableShiftLock();
        this.mSymbolsKeyboardShifted.setShifted(true);
    }

    private void createKeyboardsWithSpecificSize(int paramInt1, int paramInt2)
    {
        this.mNumericKeyboard = new PasswordEntryKeyboard(this.mContext, 17760260, paramInt1, paramInt2);
        this.mQwertyKeyboard = new PasswordEntryKeyboard(this.mContext, 17760262, 16909169, paramInt1, paramInt2);
        this.mQwertyKeyboard.enableShiftLock();
        this.mQwertyKeyboardShifted = new PasswordEntryKeyboard(this.mContext, 17760263, 16909169, paramInt1, paramInt2);
        this.mQwertyKeyboardShifted.enableShiftLock();
        this.mQwertyKeyboardShifted.setShifted(true);
        this.mSymbolsKeyboard = new PasswordEntryKeyboard(this.mContext, 17760264, paramInt1, paramInt2);
        this.mSymbolsKeyboard.enableShiftLock();
        this.mSymbolsKeyboardShifted = new PasswordEntryKeyboard(this.mContext, 17760265, paramInt1, paramInt2);
        this.mSymbolsKeyboardShifted.enableShiftLock();
        this.mSymbolsKeyboardShifted.setShifted(true);
    }

    private void handleCharacter(int paramInt, int[] paramArrayOfInt)
    {
        if ((this.mKeyboardView.isShifted()) && (paramInt != 32) && (paramInt != 10))
            paramInt = Character.toUpperCase(paramInt);
        sendKeyEventsToTarget(paramInt);
    }

    private void handleClose()
    {
    }

    private void handleModeChange()
    {
        Keyboard localKeyboard = this.mKeyboardView.getKeyboard();
        PasswordEntryKeyboard localPasswordEntryKeyboard = null;
        if ((localKeyboard == this.mQwertyKeyboard) || (localKeyboard == this.mQwertyKeyboardShifted));
        for (localPasswordEntryKeyboard = this.mSymbolsKeyboard; ; localPasswordEntryKeyboard = this.mQwertyKeyboard)
            do
            {
                if (localPasswordEntryKeyboard != null)
                {
                    this.mKeyboardView.setKeyboard(localPasswordEntryKeyboard);
                    this.mKeyboardState = 0;
                }
                return;
            }
            while ((localKeyboard != this.mSymbolsKeyboard) && (localKeyboard != this.mSymbolsKeyboardShifted));
    }

    private void handleShift()
    {
        int i = 1;
        if (this.mKeyboardView == null)
            return;
        Keyboard localKeyboard = this.mKeyboardView.getKeyboard();
        PasswordEntryKeyboard localPasswordEntryKeyboard = null;
        label39: label54: label70: label98: KeyboardView localKeyboardView;
        if ((localKeyboard == this.mQwertyKeyboard) || (localKeyboard == this.mQwertyKeyboardShifted))
        {
            int k = i;
            if (this.mKeyboardState != 0)
                break label147;
            if (k == 0)
                break label133;
            int i2 = i;
            this.mKeyboardState = i2;
            if (k == 0)
                break label139;
            localPasswordEntryKeyboard = this.mQwertyKeyboardShifted;
            if (localPasswordEntryKeyboard == null)
                break label205;
            if (localPasswordEntryKeyboard != localKeyboard)
                this.mKeyboardView.setKeyboard(localPasswordEntryKeyboard);
            if (this.mKeyboardState != 2)
                break label215;
            int n = i;
            localPasswordEntryKeyboard.setShiftLocked(n);
            localKeyboardView = this.mKeyboardView;
            if (this.mKeyboardState == 0)
                break label221;
        }
        while (true)
        {
            localKeyboardView.setShifted(i);
            break;
            int m = 0;
            break label39;
            label133: int i3 = 2;
            break label54;
            label139: localPasswordEntryKeyboard = this.mSymbolsKeyboardShifted;
            break label70;
            label147: if (this.mKeyboardState == i)
            {
                this.mKeyboardState = 2;
                if (m != 0);
                for (localPasswordEntryKeyboard = this.mQwertyKeyboardShifted; ; localPasswordEntryKeyboard = this.mSymbolsKeyboardShifted)
                    break;
            }
            if (this.mKeyboardState != 2)
                break label70;
            this.mKeyboardState = 0;
            if (m != 0);
            for (localPasswordEntryKeyboard = this.mQwertyKeyboard; ; localPasswordEntryKeyboard = this.mSymbolsKeyboard)
            {
                break label70;
                label205: break;
            }
            label215: int i1 = 0;
            break label98;
            label221: int j = 0;
        }
    }

    private void performHapticFeedback()
    {
        if (this.mEnableHaptics)
            this.mKeyboardView.performHapticFeedback(1, 3);
    }

    private void sendKeyEventsToTarget(int paramInt)
    {
        ViewRootImpl localViewRootImpl = this.mTargetView.getViewRootImpl();
        KeyCharacterMap localKeyCharacterMap = KeyCharacterMap.load(-1);
        char[] arrayOfChar = new char[1];
        arrayOfChar[0] = ((char)paramInt);
        KeyEvent[] arrayOfKeyEvent = localKeyCharacterMap.getEvents(arrayOfChar);
        if (arrayOfKeyEvent != null)
        {
            int i = arrayOfKeyEvent.length;
            for (int j = 0; j < i; j++)
            {
                KeyEvent localKeyEvent = arrayOfKeyEvent[j];
                localViewRootImpl.dispatchKey(KeyEvent.changeFlags(localKeyEvent, 0x4 | (0x2 | localKeyEvent.getFlags())));
            }
        }
    }

    public void handleBackspace()
    {
        sendDownUpKeyEvents(67);
        performHapticFeedback();
    }

    public boolean isAlpha()
    {
        if (this.mKeyboardMode == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onKey(int paramInt, int[] paramArrayOfInt)
    {
        if (paramInt == -5)
            handleBackspace();
        while (true)
        {
            return;
            if (paramInt == -1)
            {
                handleShift();
            }
            else if (paramInt == -3)
            {
                handleClose();
            }
            else if ((paramInt == -2) && (this.mKeyboardView != null))
            {
                handleModeChange();
            }
            else
            {
                handleCharacter(paramInt, paramArrayOfInt);
                if (this.mKeyboardState == 1)
                {
                    this.mKeyboardState = 2;
                    handleShift();
                }
            }
        }
    }

    public void onPress(int paramInt)
    {
        performHapticFeedback();
    }

    public void onRelease(int paramInt)
    {
    }

    public void onText(CharSequence paramCharSequence)
    {
    }

    public void sendDownUpKeyEvents(int paramInt)
    {
        long l = SystemClock.uptimeMillis();
        ViewRootImpl localViewRootImpl = this.mTargetView.getViewRootImpl();
        localViewRootImpl.dispatchKeyFromIme(new KeyEvent(l, l, 0, paramInt, 0, 0, -1, 0, 6));
        localViewRootImpl.dispatchKeyFromIme(new KeyEvent(l, l, 1, paramInt, 0, 0, -1, 0, 6));
    }

    public void setEnableHaptics(boolean paramBoolean)
    {
        this.mEnableHaptics = paramBoolean;
    }

    public void setKeyboardMode(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        }
        while (true)
        {
            this.mKeyboardMode = paramInt;
            return;
            this.mKeyboardView.setKeyboard(this.mQwertyKeyboard);
            this.mKeyboardState = 0;
            if (Settings.System.getInt(this.mContext.getContentResolver(), "show_password", i) != 0);
            while (true)
            {
                KeyboardView localKeyboardView = this.mKeyboardView;
                if (i != 0);
                localKeyboardView.setPreviewEnabled(false);
                break;
                i = 0;
            }
            this.mKeyboardView.setKeyboard(this.mNumericKeyboard);
            this.mKeyboardState = 0;
            this.mKeyboardView.setPreviewEnabled(false);
        }
    }

    public void setVibratePattern(int paramInt)
    {
        Object localObject = null;
        while (true)
        {
            int i;
            try
            {
                int[] arrayOfInt = this.mContext.getResources().getIntArray(paramInt);
                localObject = arrayOfInt;
                if (localObject == null)
                {
                    this.mVibratePattern = null;
                    return;
                }
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                if (paramInt == 0)
                    continue;
                Log.e("PasswordEntryKeyboardHelper", "Vibrate pattern missing", localNotFoundException);
                continue;
                this.mVibratePattern = new long[localObject.length];
                i = 0;
            }
            while (i < localObject.length)
            {
                this.mVibratePattern[i] = localObject[i];
                i++;
            }
        }
    }

    public void swipeDown()
    {
    }

    public void swipeLeft()
    {
    }

    public void swipeRight()
    {
    }

    public void swipeUp()
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.PasswordEntryKeyboardHelper
 * JD-Core Version:        0.6.2
 */