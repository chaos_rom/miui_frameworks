package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

public class LinearLayoutWithDefaultTouchRecepient extends LinearLayout
{
    private View mDefaultTouchRecepient;
    private final Rect mTempRect = new Rect();

    public LinearLayoutWithDefaultTouchRecepient(Context paramContext)
    {
        super(paramContext);
    }

    public LinearLayoutWithDefaultTouchRecepient(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool;
        if (this.mDefaultTouchRecepient == null)
            bool = super.dispatchTouchEvent(paramMotionEvent);
        while (true)
        {
            return bool;
            if (super.dispatchTouchEvent(paramMotionEvent))
            {
                bool = true;
            }
            else
            {
                this.mTempRect.set(0, 0, 0, 0);
                offsetRectIntoDescendantCoords(this.mDefaultTouchRecepient, this.mTempRect);
                paramMotionEvent.setLocation(paramMotionEvent.getX() + this.mTempRect.left, paramMotionEvent.getY() + this.mTempRect.top);
                bool = this.mDefaultTouchRecepient.dispatchTouchEvent(paramMotionEvent);
            }
        }
    }

    public void setDefaultTouchRecepient(View paramView)
    {
        this.mDefaultTouchRecepient = paramView;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.LinearLayoutWithDefaultTouchRecepient
 * JD-Core Version:        0.6.2
 */