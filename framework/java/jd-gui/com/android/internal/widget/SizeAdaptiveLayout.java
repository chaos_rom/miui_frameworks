package com.android.internal.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.StateSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RemoteViews.RemoteView;
import com.android.internal.R.styleable;

@RemoteViews.RemoteView
public class SizeAdaptiveLayout extends ViewGroup
{
    private static final long CROSSFADE_TIME = 250L;
    private static final boolean DEBUG = false;
    private static final int MAX_VALID_HEIGHT = 0;
    private static final int MIN_VALID_HEIGHT = 1;
    private static final boolean REPORT_BAD_BOUNDS = true;
    private static final String TAG = "SizeAdaptiveLayout";
    private View mActiveChild;
    private Animator.AnimatorListener mAnimatorListener;
    private int mCanceledAnimationCount;
    private View mEnteringView;
    private ObjectAnimator mFadePanel;
    private ObjectAnimator mFadeView;
    private View mLastActive;
    private View mLeavingView;
    private View mModestyPanel;
    private int mModestyPanelTop;
    private AnimatorSet mTransitionAnimation;

    public SizeAdaptiveLayout(Context paramContext)
    {
        super(paramContext);
        initialize();
    }

    public SizeAdaptiveLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        initialize();
    }

    public SizeAdaptiveLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        initialize();
    }

    private int clampSizeToBounds(int paramInt, View paramView)
    {
        LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
        int i = 0xFFFFFF & paramInt;
        int j = Math.max(i, localLayoutParams.minHeight);
        if (localLayoutParams.maxHeight != -1)
            j = Math.min(j, localLayoutParams.maxHeight);
        if (i != j)
            Log.d("SizeAdaptiveLayout", this + "child view " + paramView + " " + "measured out of bounds at " + i + "px " + "clamped to " + j + "px");
        return j;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void initialize()
    {
        this.mModestyPanel = new View(getContext());
        Drawable localDrawable = getBackground();
        if ((localDrawable instanceof StateListDrawable))
        {
            StateListDrawable localStateListDrawable = (StateListDrawable)localDrawable;
            localStateListDrawable.setState(StateSet.WILD_CARD);
            localDrawable = localStateListDrawable.getCurrent();
        }
        if ((localDrawable instanceof ColorDrawable))
            this.mModestyPanel.setBackgroundDrawable(localDrawable);
        while (true)
        {
            LayoutParams localLayoutParams = new LayoutParams(-1, -1);
            this.mModestyPanel.setLayoutParams(localLayoutParams);
            addView(this.mModestyPanel);
            View localView = this.mModestyPanel;
            float[] arrayOfFloat1 = new float[1];
            arrayOfFloat1[0] = 0.0F;
            this.mFadePanel = ObjectAnimator.ofFloat(localView, "alpha", arrayOfFloat1);
            float[] arrayOfFloat2 = new float[1];
            arrayOfFloat2[0] = 0.0F;
            this.mFadeView = ObjectAnimator.ofFloat(null, "alpha", arrayOfFloat2);
            this.mAnimatorListener = new BringToFrontOnEnd();
            this.mTransitionAnimation = new AnimatorSet();
            this.mTransitionAnimation.play(this.mFadeView).with(this.mFadePanel);
            this.mTransitionAnimation.setDuration(250L);
            this.mTransitionAnimation.addListener(this.mAnimatorListener);
            return;
            this.mModestyPanel.setBackgroundColor(0);
        }
    }

    private View selectActiveChild(int paramInt)
    {
        int i = View.MeasureSpec.getMode(paramInt);
        int j = View.MeasureSpec.getSize(paramInt);
        Object localObject1 = null;
        Object localObject2 = null;
        int k = 0;
        Object localObject3 = null;
        int m = 2147483647;
        int n = 0;
        Object localObject4;
        if (n < getChildCount())
        {
            localObject4 = getChildAt(n);
            if (localObject4 != this.mModestyPanel)
            {
                LayoutParams localLayoutParams = (LayoutParams)((View)localObject4).getLayoutParams();
                if ((localLayoutParams.maxHeight == -1) && (localObject1 == null))
                    localObject1 = localObject4;
                if (localLayoutParams.maxHeight > k)
                {
                    k = localLayoutParams.maxHeight;
                    localObject2 = localObject4;
                }
                if (localLayoutParams.minHeight < m)
                {
                    m = localLayoutParams.minHeight;
                    localObject3 = localObject4;
                }
                if ((i == 0) || (j < localLayoutParams.minHeight) || (j > localLayoutParams.maxHeight));
            }
        }
        while (true)
        {
            return localObject4;
            n++;
            break;
            if (localObject1 != null)
                localObject2 = localObject1;
            if (i == 0)
                localObject4 = localObject2;
            else if (j > k)
                localObject4 = localObject2;
            else
                localObject4 = localObject3;
        }
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    protected LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams();
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return new LayoutParams(paramLayoutParams);
    }

    public View getModestyPanel()
    {
        return this.mModestyPanel;
    }

    public Animator getTransitionAnimation()
    {
        return this.mTransitionAnimation;
    }

    public void onAttachedToWindow()
    {
        this.mLastActive = null;
        for (int i = 0; i < getChildCount(); i++)
            getChildAt(i).setVisibility(8);
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mLastActive = this.mActiveChild;
        this.mActiveChild = selectActiveChild(View.MeasureSpec.makeMeasureSpec(paramInt4 - paramInt2, 1073741824));
        this.mActiveChild.setVisibility(0);
        if ((this.mLastActive != this.mActiveChild) && (this.mLastActive != null))
        {
            this.mEnteringView = this.mActiveChild;
            this.mLeavingView = this.mLastActive;
            this.mEnteringView.setAlpha(1.0F);
            this.mModestyPanel.setAlpha(1.0F);
            this.mModestyPanel.bringToFront();
            this.mModestyPanelTop = this.mLeavingView.getHeight();
            this.mModestyPanel.setVisibility(0);
            this.mLeavingView.bringToFront();
            if (this.mTransitionAnimation.isRunning())
                this.mTransitionAnimation.cancel();
            this.mFadeView.setTarget(this.mLeavingView);
            ObjectAnimator localObjectAnimator1 = this.mFadeView;
            float[] arrayOfFloat1 = new float[1];
            arrayOfFloat1[0] = 0.0F;
            localObjectAnimator1.setFloatValues(arrayOfFloat1);
            ObjectAnimator localObjectAnimator2 = this.mFadePanel;
            float[] arrayOfFloat2 = new float[1];
            arrayOfFloat2[0] = 0.0F;
            localObjectAnimator2.setFloatValues(arrayOfFloat2);
            this.mTransitionAnimation.setupStartValues();
            this.mTransitionAnimation.start();
        }
        int i = this.mActiveChild.getMeasuredWidth();
        int j = this.mActiveChild.getMeasuredHeight();
        this.mActiveChild.layout(0, 0, i + 0, j + 0);
        this.mModestyPanel.layout(0, this.mModestyPanelTop, i + 0, j + this.mModestyPanelTop);
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        View localView = selectActiveChild(paramInt2);
        ((LayoutParams)localView.getLayoutParams());
        measureChild(localView, paramInt1, paramInt2);
        int i = localView.getMeasuredHeight();
        int j = localView.getMeasuredHeight();
        int k = combineMeasuredStates(0, localView.getMeasuredState());
        setMeasuredDimension(resolveSizeAndState(j, paramInt1, k), clampSizeToBounds(resolveSizeAndState(i, paramInt2, k), localView));
    }

    class BringToFrontOnEnd
        implements Animator.AnimatorListener
    {
        static
        {
            if (!SizeAdaptiveLayout.class.desiredAssertionStatus());
            for (boolean bool = true; ; bool = false)
            {
                $assertionsDisabled = bool;
                return;
            }
        }

        BringToFrontOnEnd()
        {
        }

        public void onAnimationCancel(Animator paramAnimator)
        {
            SizeAdaptiveLayout.access$008(SizeAdaptiveLayout.this);
        }

        public void onAnimationEnd(Animator paramAnimator)
        {
            if (SizeAdaptiveLayout.this.mCanceledAnimationCount == 0)
            {
                SizeAdaptiveLayout.this.mLeavingView.setVisibility(8);
                SizeAdaptiveLayout.this.mModestyPanel.setVisibility(8);
                SizeAdaptiveLayout.this.mEnteringView.bringToFront();
                SizeAdaptiveLayout.access$302(SizeAdaptiveLayout.this, null);
                SizeAdaptiveLayout.access$102(SizeAdaptiveLayout.this, null);
            }
            while (true)
            {
                return;
                SizeAdaptiveLayout.access$010(SizeAdaptiveLayout.this);
            }
        }

        public void onAnimationRepeat(Animator paramAnimator)
        {
            if (!$assertionsDisabled)
                throw new AssertionError();
        }

        public void onAnimationStart(Animator paramAnimator)
        {
        }
    }

    public static class LayoutParams extends ViewGroup.LayoutParams
    {
        public static final int UNBOUNDED = -1;

        @ViewDebug.ExportedProperty(category="layout")
        public int maxHeight;

        @ViewDebug.ExportedProperty(category="layout")
        public int minHeight;

        public LayoutParams()
        {
            this(0, 0);
        }

        public LayoutParams(int paramInt1, int paramInt2)
        {
            this(paramInt1, paramInt2, -1, -1);
        }

        public LayoutParams(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            super(paramInt2);
            this.minHeight = paramInt3;
            this.maxHeight = paramInt4;
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SizeAdaptiveLayout_Layout);
            this.minHeight = localTypedArray.getDimensionPixelSize(1, 0);
            try
            {
                this.maxHeight = localTypedArray.getLayoutDimension(0, -1);
                label36: localTypedArray.recycle();
                return;
            }
            catch (Exception localException)
            {
                break label36;
            }
        }

        public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
        {
            super();
            this.minHeight = -1;
            this.maxHeight = -1;
        }

        public String debug(String paramString)
        {
            return paramString + "SizeAdaptiveLayout.LayoutParams={" + ", max=" + this.maxHeight + ", max=" + this.minHeight + "}";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.SizeAdaptiveLayout
 * JD-Core Version:        0.6.2
 */