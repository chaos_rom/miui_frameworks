package com.android.internal.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.android.internal.R.styleable;

public class ActionBarContainer extends FrameLayout
{
    private ActionBarView mActionBarView;
    private Drawable mBackground;
    private boolean mIsSplit;
    private boolean mIsStacked;
    private boolean mIsTransitioning;
    private Drawable mSplitBackground;
    private Drawable mStackedBackground;
    private View mTabContainer;

    public ActionBarContainer(Context paramContext)
    {
        this(paramContext, null);
    }

    public ActionBarContainer(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        setBackgroundDrawable(null);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActionBar);
        this.mBackground = localTypedArray.getDrawable(2);
        this.mStackedBackground = localTypedArray.getDrawable(17);
        if (getId() == 16909089)
        {
            this.mIsSplit = bool;
            this.mSplitBackground = localTypedArray.getDrawable(18);
        }
        localTypedArray.recycle();
        if (this.mIsSplit)
            if (this.mSplitBackground != null);
        while (true)
        {
            setWillNotDraw(bool);
            return;
            bool = false;
            continue;
            if ((this.mBackground != null) || (this.mStackedBackground != null))
                bool = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Drawable getActionBarBackground()
    {
        return this.mBackground;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    ActionBarView getActionBarView()
    {
        return this.mActionBarView;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Drawable getStackedBackground()
    {
        return this.mStackedBackground;
    }

    public View getTabContainer()
    {
        return this.mTabContainer;
    }

    public void onDraw(Canvas paramCanvas)
    {
        if ((getWidth() == 0) || (getHeight() == 0));
        while (true)
        {
            return;
            if (this.mIsSplit)
            {
                if (this.mSplitBackground != null)
                    this.mSplitBackground.draw(paramCanvas);
            }
            else
            {
                if (this.mBackground != null)
                    this.mBackground.draw(paramCanvas);
                if ((this.mStackedBackground != null) && (this.mIsStacked))
                    this.mStackedBackground.draw(paramCanvas);
            }
        }
    }

    public void onFinishInflate()
    {
        super.onFinishInflate();
        this.mActionBarView = ((ActionBarView)findViewById(16909087));
    }

    public boolean onHoverEvent(MotionEvent paramMotionEvent)
    {
        super.onHoverEvent(paramMotionEvent);
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
        if ((this.mIsTransitioning) || (super.onInterceptTouchEvent(paramMotionEvent)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        int i;
        int k;
        int m;
        int i1;
        label88: View localView;
        if ((this.mTabContainer != null) && (this.mTabContainer.getVisibility() != 8))
        {
            i = 1;
            if ((this.mTabContainer == null) || (this.mTabContainer.getVisibility() == 8))
                break label157;
            k = getMeasuredHeight();
            m = this.mTabContainer.getMeasuredHeight();
            if ((0x2 & this.mActionBarView.getDisplayOptions()) != 0)
                break label204;
            int n = getChildCount();
            i1 = 0;
            if (i1 >= n)
                break label144;
            localView = getChildAt(i1);
            if (localView != this.mTabContainer)
                break label124;
        }
        while (true)
        {
            i1++;
            break label88;
            i = 0;
            break;
            label124: if (!this.mActionBarView.isCollapsed())
                localView.offsetTopAndBottom(m);
        }
        label144: this.mTabContainer.layout(paramInt1, 0, paramInt3, m);
        label157: int j;
        while (true)
        {
            j = 0;
            if (!this.mIsSplit)
                break;
            if (this.mSplitBackground != null)
            {
                this.mSplitBackground.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
                j = 1;
            }
            if (j != 0)
                invalidate();
            return;
            label204: this.mTabContainer.layout(paramInt1, k - m, paramInt3, k);
        }
        if (this.mBackground != null)
        {
            this.mBackground.setBounds(this.mActionBarView.getLeft(), this.mActionBarView.getTop(), this.mActionBarView.getRight(), this.mActionBarView.getBottom());
            j = 1;
        }
        if ((i != 0) && (this.mStackedBackground != null));
        for (boolean bool = true; ; bool = false)
        {
            this.mIsStacked = bool;
            if (!bool)
                break;
            Injector.setBounds(this, this.mTabContainer.getLeft(), this.mTabContainer.getTop(), this.mTabContainer.getRight(), this.mTabContainer.getBottom());
            j = 1;
            break;
        }
    }

    public void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        if (this.mActionBarView == null);
        label118: 
        while (true)
        {
            return;
            FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams)this.mActionBarView.getLayoutParams();
            if (this.mActionBarView.isCollapsed());
            for (int i = 0; ; i = this.mActionBarView.getMeasuredHeight() + localLayoutParams.topMargin + localLayoutParams.bottomMargin)
            {
                if ((this.mTabContainer == null) || (this.mTabContainer.getVisibility() == 8) || (View.MeasureSpec.getMode(paramInt2) != -2147483648))
                    break label118;
                int j = View.MeasureSpec.getSize(paramInt2);
                setMeasuredDimension(getMeasuredWidth(), Math.min(i + this.mTabContainer.getMeasuredHeight(), j));
                break;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        super.onTouchEvent(paramMotionEvent);
        return true;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setIsStacked(boolean paramBoolean)
    {
        this.mIsStacked = paramBoolean;
    }

    public void setPrimaryBackground(Drawable paramDrawable)
    {
        this.mBackground = paramDrawable;
        invalidate();
    }

    public void setSplitBackground(Drawable paramDrawable)
    {
        this.mSplitBackground = paramDrawable;
        invalidate();
    }

    public void setStackedBackground(Drawable paramDrawable)
    {
        this.mStackedBackground = paramDrawable;
        invalidate();
    }

    public void setTabContainer(ScrollingTabContainerView paramScrollingTabContainerView)
    {
        if (this.mTabContainer != null)
            removeView(this.mTabContainer);
        this.mTabContainer = paramScrollingTabContainerView;
        if (paramScrollingTabContainerView != null)
        {
            addView(paramScrollingTabContainerView);
            ViewGroup.LayoutParams localLayoutParams = paramScrollingTabContainerView.getLayoutParams();
            localLayoutParams.width = -1;
            localLayoutParams.height = -2;
            paramScrollingTabContainerView.setAllowCollapse(false);
        }
    }

    public void setTransitioning(boolean paramBoolean)
    {
        this.mIsTransitioning = paramBoolean;
        if (paramBoolean);
        for (int i = 393216; ; i = 262144)
        {
            setDescendantFocusability(i);
            return;
        }
    }

    public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback)
    {
        return null;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void setBounds(ActionBarContainer paramActionBarContainer, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            Drawable localDrawable = paramActionBarContainer.getActionBarBackground();
            if ((localDrawable != null) && (paramActionBarContainer.getActionBarView().getHeight() == 0))
            {
                localDrawable.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
                paramActionBarContainer.setIsStacked(false);
            }
            while (true)
            {
                return;
                paramActionBarContainer.getStackedBackground().setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.ActionBarContainer
 * JD-Core Version:        0.6.2
 */