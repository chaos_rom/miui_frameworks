package com.android.internal.widget;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IRemoteViewsAdapterConnection extends IInterface
{
    public abstract void onServiceConnected(IBinder paramIBinder)
        throws RemoteException;

    public abstract void onServiceDisconnected()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRemoteViewsAdapterConnection
    {
        private static final String DESCRIPTOR = "com.android.internal.widget.IRemoteViewsAdapterConnection";
        static final int TRANSACTION_onServiceConnected = 1;
        static final int TRANSACTION_onServiceDisconnected = 2;

        public Stub()
        {
            attachInterface(this, "com.android.internal.widget.IRemoteViewsAdapterConnection");
        }

        public static IRemoteViewsAdapterConnection asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.widget.IRemoteViewsAdapterConnection");
                if ((localIInterface != null) && ((localIInterface instanceof IRemoteViewsAdapterConnection)))
                    localObject = (IRemoteViewsAdapterConnection)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("com.android.internal.widget.IRemoteViewsAdapterConnection");
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsAdapterConnection");
                onServiceConnected(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsAdapterConnection");
                onServiceDisconnected();
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IRemoteViewsAdapterConnection
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.widget.IRemoteViewsAdapterConnection";
            }

            public void onServiceConnected(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsAdapterConnection");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onServiceDisconnected()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsAdapterConnection");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.IRemoteViewsAdapterConnection
 * JD-Core Version:        0.6.2
 */