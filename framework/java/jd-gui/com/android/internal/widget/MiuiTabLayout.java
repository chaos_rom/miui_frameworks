package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import miui.util.ResourceMapper;

public class MiuiTabLayout extends LinearLayout
{
    private final boolean mMIUITheme;
    private int mTabBackgroundId;
    private int mTabBackgroundIdLeft;
    private int mTabBackgroundIdRight;

    public MiuiTabLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        int[] arrayOfInt = new int[bool];
        arrayOfInt[0] = 16842964;
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(null, arrayOfInt, 16843507, 0);
        this.mTabBackgroundId = localTypedArray.getResourceId(0, 0);
        localTypedArray.recycle();
        if (this.mTabBackgroundId == ResourceMapper.resolveReference(paramContext, 100794821));
        while (true)
        {
            this.mMIUITheme = bool;
            changeBackgroundRes(100794609, 100794610, 100794611);
            return;
            bool = false;
        }
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        super.addView(paramView, paramInt, paramLayoutParams);
        if (this.mTabBackgroundId == 0);
        int i;
        do
        {
            do
            {
                return;
                paramView.setBackgroundResource(this.mTabBackgroundId);
                i = getChildCount();
            }
            while (i <= 1);
            if (paramInt == 0)
            {
                getChildAt(0).setBackgroundResource(this.mTabBackgroundIdLeft);
                View localView2 = getChildAt(1);
                if (i > 2);
                for (int k = this.mTabBackgroundId; ; k = this.mTabBackgroundIdRight)
                {
                    localView2.setBackgroundResource(k);
                    break;
                }
            }
        }
        while ((paramInt != -1) && (paramInt != i - 1));
        getChildAt(i - 1).setBackgroundResource(this.mTabBackgroundIdRight);
        View localView1 = getChildAt(i - 2);
        if (i > 2);
        for (int j = this.mTabBackgroundId; ; j = this.mTabBackgroundIdLeft)
        {
            localView1.setBackgroundResource(j);
            break;
        }
    }

    public boolean changeBackgroundRes(int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mMIUITheme)
        {
            this.mTabBackgroundId = paramInt1;
            this.mTabBackgroundIdLeft = paramInt2;
            this.mTabBackgroundIdRight = paramInt3;
            setDividerDrawable(null);
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void removeViewAt(int paramInt)
    {
        super.removeViewAt(paramInt);
        int i = getChildCount();
        if ((this.mTabBackgroundId == 0) || (i == 0));
        while (true)
        {
            return;
            if (i == 1)
                getChildAt(0).setBackgroundResource(this.mTabBackgroundId);
            else if (paramInt == 0)
                getChildAt(0).setBackgroundResource(this.mTabBackgroundIdLeft);
            else if (paramInt == i)
                getChildAt(i - 1).setBackgroundResource(this.mTabBackgroundIdRight);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.MiuiTabLayout
 * JD-Core Version:        0.6.2
 */