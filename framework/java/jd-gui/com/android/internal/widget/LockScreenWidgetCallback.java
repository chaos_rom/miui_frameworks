package com.android.internal.widget;

import android.view.View;

public abstract interface LockScreenWidgetCallback
{
    public abstract boolean isVisible(View paramView);

    public abstract void requestHide(View paramView);

    public abstract void requestShow(View paramView);

    public abstract void userActivity(View paramView);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.LockScreenWidgetCallback
 * JD-Core Version:        0.6.2
 */