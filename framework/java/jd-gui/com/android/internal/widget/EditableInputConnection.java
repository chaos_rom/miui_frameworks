package com.android.internal.widget;

import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.method.KeyListener;
import android.text.style.SuggestionSpan;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public class EditableInputConnection extends BaseInputConnection
{
    private static final boolean DEBUG = false;
    private static final String TAG = "EditableInputConnection";
    private int mBatchEditNesting;
    private final TextView mTextView;

    public EditableInputConnection(TextView paramTextView)
    {
        super(paramTextView, true);
        this.mTextView = paramTextView;
    }

    public boolean beginBatchEdit()
    {
        boolean bool;
        try
        {
            if (this.mBatchEditNesting >= 0)
            {
                this.mTextView.beginBatchEdit();
                this.mBatchEditNesting = (1 + this.mBatchEditNesting);
                bool = true;
            }
            else
            {
                bool = false;
            }
        }
        finally
        {
        }
        return bool;
    }

    public boolean clearMetaKeyStates(int paramInt)
    {
        Editable localEditable = getEditable();
        boolean bool;
        if (localEditable == null)
            bool = false;
        while (true)
        {
            return bool;
            KeyListener localKeyListener = this.mTextView.getKeyListener();
            if (localKeyListener != null);
            try
            {
                localKeyListener.clearMetaKeyState(this.mTextView, localEditable, paramInt);
                label39: bool = true;
            }
            catch (AbstractMethodError localAbstractMethodError)
            {
                break label39;
            }
        }
    }

    public boolean commitCompletion(CompletionInfo paramCompletionInfo)
    {
        this.mTextView.beginBatchEdit();
        this.mTextView.onCommitCompletion(paramCompletionInfo);
        this.mTextView.endBatchEdit();
        return true;
    }

    public boolean commitCorrection(CorrectionInfo paramCorrectionInfo)
    {
        this.mTextView.beginBatchEdit();
        this.mTextView.onCommitCorrection(paramCorrectionInfo);
        this.mTextView.endBatchEdit();
        return true;
    }

    public boolean commitText(CharSequence paramCharSequence, int paramInt)
    {
        boolean bool;
        if (this.mTextView == null)
            bool = super.commitText(paramCharSequence, paramInt);
        while (true)
        {
            return bool;
            if ((paramCharSequence instanceof Spanned))
            {
                SuggestionSpan[] arrayOfSuggestionSpan = (SuggestionSpan[])((Spanned)paramCharSequence).getSpans(0, paramCharSequence.length(), SuggestionSpan.class);
                this.mIMM.registerSuggestionSpansForNotification(arrayOfSuggestionSpan);
            }
            this.mTextView.resetErrorChangedFlag();
            bool = super.commitText(paramCharSequence, paramInt);
            this.mTextView.hideErrorIfUnchanged();
        }
    }

    public boolean endBatchEdit()
    {
        boolean bool;
        try
        {
            if (this.mBatchEditNesting > 0)
            {
                this.mTextView.endBatchEdit();
                this.mBatchEditNesting = (-1 + this.mBatchEditNesting);
                bool = true;
            }
            else
            {
                bool = false;
            }
        }
        finally
        {
        }
        return bool;
    }

    public Editable getEditable()
    {
        TextView localTextView = this.mTextView;
        if (localTextView != null);
        for (Editable localEditable = localTextView.getEditableText(); ; localEditable = null)
            return localEditable;
    }

    public ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt)
    {
        ExtractedText localExtractedText;
        if (this.mTextView != null)
        {
            localExtractedText = new ExtractedText();
            if (this.mTextView.extractText(paramExtractedTextRequest, localExtractedText))
                if ((paramInt & 0x1) != 0)
                    this.mTextView.setExtracting(paramExtractedTextRequest);
        }
        while (true)
        {
            return localExtractedText;
            localExtractedText = null;
        }
    }

    public boolean performContextMenuAction(int paramInt)
    {
        this.mTextView.beginBatchEdit();
        this.mTextView.onTextContextMenuItem(paramInt);
        this.mTextView.endBatchEdit();
        return true;
    }

    public boolean performEditorAction(int paramInt)
    {
        this.mTextView.onEditorAction(paramInt);
        return true;
    }

    public boolean performPrivateCommand(String paramString, Bundle paramBundle)
    {
        this.mTextView.onPrivateIMECommand(paramString, paramBundle);
        return true;
    }

    // ERROR //
    protected void reportFinish()
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 134	android/view/inputmethod/BaseInputConnection:reportFinish	()V
        //     4: aload_0
        //     5: monitorenter
        //     6: aload_0
        //     7: getfield 26	com/android/internal/widget/EditableInputConnection:mBatchEditNesting	I
        //     10: ifle +16 -> 26
        //     13: aload_0
        //     14: invokevirtual 136	com/android/internal/widget/EditableInputConnection:endBatchEdit	()Z
        //     17: pop
        //     18: goto -12 -> 6
        //     21: astore_1
        //     22: aload_0
        //     23: monitorexit
        //     24: aload_1
        //     25: athrow
        //     26: aload_0
        //     27: bipush 255
        //     29: putfield 26	com/android/internal/widget/EditableInputConnection:mBatchEditNesting	I
        //     32: aload_0
        //     33: monitorexit
        //     34: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	24	21	finally
        //     26	34	21	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.EditableInputConnection
 * JD-Core Version:        0.6.2
 */