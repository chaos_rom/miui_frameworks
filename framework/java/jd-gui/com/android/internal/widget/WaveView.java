package com.android.internal.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.accessibility.AccessibilityManager;
import java.util.ArrayList;

public class WaveView extends View
    implements ValueAnimator.AnimatorUpdateListener
{
    private static final boolean DBG = false;
    private static final long DELAY_INCREMENT = 15L;
    private static final long DELAY_INCREMENT2 = 12L;
    private static final long DURATION = 300L;
    private static final long FINAL_DELAY = 200L;
    private static final long FINAL_DURATION = 200L;
    private static final float GRAB_HANDLE_RADIUS_SCALE_ACCESSIBILITY_DISABLED = 0.5F;
    private static final float GRAB_HANDLE_RADIUS_SCALE_ACCESSIBILITY_ENABLED = 1.0F;
    private static final long RESET_TIMEOUT = 3000L;
    private static final long RING_DELAY = 1300L;
    private static final long SHORT_DELAY = 100L;
    private static final int STATE_ATTEMPTING = 3;
    private static final int STATE_READY = 1;
    private static final int STATE_RESET_LOCK = 0;
    private static final int STATE_START_ATTEMPT = 2;
    private static final int STATE_UNLOCK_ATTEMPT = 4;
    private static final int STATE_UNLOCK_SUCCESS = 5;
    private static final String TAG = "WaveView";
    private static final long VIBRATE_LONG = 20L;
    private static final long VIBRATE_SHORT = 20L;
    private static final int WAVE_COUNT = 20;
    private static final long WAVE_DELAY = 100L;
    private static final long WAVE_DURATION = 2000L;
    private final Runnable mAddWaveAction = new Runnable()
    {
        public void run()
        {
            int i = (int)Math.ceil(Math.hypot(WaveView.this.mMouseX - WaveView.this.mLockCenterX, WaveView.this.mMouseY - WaveView.this.mLockCenterY));
            if ((WaveView.this.mLockState == 3) && (i < WaveView.this.mSnapRadius) && (WaveView.this.mWaveTimerDelay >= 100L))
            {
                WaveView.access$602(WaveView.this, Math.min(2000L, 15L + WaveView.this.mWaveTimerDelay));
                DrawableHolder localDrawableHolder = (DrawableHolder)WaveView.this.mLightWaves.get(WaveView.this.mCurrentWave);
                localDrawableHolder.setAlpha(0.0F);
                localDrawableHolder.setScaleX(0.2F);
                localDrawableHolder.setScaleY(0.2F);
                localDrawableHolder.setX(WaveView.this.mMouseX);
                localDrawableHolder.setY(WaveView.this.mMouseY);
                localDrawableHolder.addAnimTo(2000L, 0L, "x", WaveView.this.mLockCenterX, true);
                localDrawableHolder.addAnimTo(2000L, 0L, "y", WaveView.this.mLockCenterY, true);
                localDrawableHolder.addAnimTo(1333L, 0L, "alpha", 1.0F, true);
                localDrawableHolder.addAnimTo(2000L, 0L, "scaleX", 1.0F, true);
                localDrawableHolder.addAnimTo(2000L, 0L, "scaleY", 1.0F, true);
                localDrawableHolder.addAnimTo(1000L, 1300L, "alpha", 0.0F, false);
                localDrawableHolder.startAnimations(WaveView.this);
                WaveView.access$702(WaveView.this, (1 + WaveView.this.mCurrentWave) % WaveView.this.mWaveCount);
                if (!WaveView.this.mFinishWaves)
                    break label333;
                WaveView.access$1102(WaveView.this, false);
            }
            while (true)
            {
                return;
                WaveView.access$614(WaveView.this, 12L);
                break;
                label333: WaveView.this.postDelayed(WaveView.this.mAddWaveAction, WaveView.this.mWaveTimerDelay);
            }
        }
    };
    private int mCurrentWave = 0;
    private ArrayList<DrawableHolder> mDrawables = new ArrayList(3);
    private boolean mFingerDown = false;
    private boolean mFinishWaves;
    private int mGrabbedState = 0;
    private ArrayList<DrawableHolder> mLightWaves = new ArrayList(20);
    private float mLockCenterX;
    private float mLockCenterY;
    private int mLockState = 0;
    private final Runnable mLockTimerActions = new Runnable()
    {
        public void run()
        {
            if (WaveView.this.mLockState == 3)
                WaveView.access$002(WaveView.this, 0);
            if (WaveView.this.mLockState == 5)
                WaveView.access$002(WaveView.this, 0);
            WaveView.this.invalidate();
        }
    };
    private float mMouseX;
    private float mMouseY;
    private OnTriggerListener mOnTriggerListener;
    private float mRingRadius = 182.0F;
    private int mSnapRadius = 136;
    private DrawableHolder mUnlockDefault;
    private DrawableHolder mUnlockHalo;
    private DrawableHolder mUnlockRing;
    private Vibrator mVibrator;
    private int mWaveCount = 20;
    private long mWaveTimerDelay = 100L;
    private boolean mWavesRunning;

    public WaveView(Context paramContext)
    {
        this(paramContext, null);
    }

    public WaveView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        initDrawables();
    }

    private void announceUnlockHandle()
    {
        setContentDescription(this.mContext.getString(17040589));
        sendAccessibilityEvent(8);
        setContentDescription(null);
    }

    private void dispatchTriggerEvent(int paramInt)
    {
        vibrate(20L);
        if (this.mOnTriggerListener != null)
            this.mOnTriggerListener.onTrigger(this, paramInt);
    }

    private float getScaledGrabHandleRadius()
    {
        if (AccessibilityManager.getInstance(this.mContext).isEnabled());
        for (float f = 1.0F * this.mUnlockHalo.getWidth(); ; f = 0.5F * this.mUnlockHalo.getWidth())
            return f;
    }

    private void initDrawables()
    {
        this.mUnlockRing = new DrawableHolder(createDrawable(17303032));
        this.mUnlockRing.setX(this.mLockCenterX);
        this.mUnlockRing.setY(this.mLockCenterY);
        this.mUnlockRing.setScaleX(0.1F);
        this.mUnlockRing.setScaleY(0.1F);
        this.mUnlockRing.setAlpha(0.0F);
        this.mDrawables.add(this.mUnlockRing);
        this.mUnlockDefault = new DrawableHolder(createDrawable(17303030));
        this.mUnlockDefault.setX(this.mLockCenterX);
        this.mUnlockDefault.setY(this.mLockCenterY);
        this.mUnlockDefault.setScaleX(0.1F);
        this.mUnlockDefault.setScaleY(0.1F);
        this.mUnlockDefault.setAlpha(0.0F);
        this.mDrawables.add(this.mUnlockDefault);
        this.mUnlockHalo = new DrawableHolder(createDrawable(17303031));
        this.mUnlockHalo.setX(this.mLockCenterX);
        this.mUnlockHalo.setY(this.mLockCenterY);
        this.mUnlockHalo.setScaleX(0.1F);
        this.mUnlockHalo.setScaleY(0.1F);
        this.mUnlockHalo.setAlpha(0.0F);
        this.mDrawables.add(this.mUnlockHalo);
        BitmapDrawable localBitmapDrawable = createDrawable(17303033);
        for (int i = 0; i < this.mWaveCount; i++)
        {
            DrawableHolder localDrawableHolder = new DrawableHolder(localBitmapDrawable);
            this.mLightWaves.add(localDrawableHolder);
            localDrawableHolder.setAlpha(0.0F);
        }
    }

    private void setGrabbedState(int paramInt)
    {
        if (paramInt != this.mGrabbedState)
        {
            this.mGrabbedState = paramInt;
            if (this.mOnTriggerListener != null)
                this.mOnTriggerListener.onGrabbedStateChange(this, this.mGrabbedState);
        }
    }

    private void tryTransitionToStartAttemptState(MotionEvent paramMotionEvent)
    {
        float f1 = paramMotionEvent.getX() - this.mUnlockHalo.getX();
        float f2 = paramMotionEvent.getY() - this.mUnlockHalo.getY();
        if ((float)Math.hypot(f1, f2) <= getScaledGrabHandleRadius())
        {
            setGrabbedState(10);
            if (this.mLockState == 1)
            {
                this.mLockState = 2;
                if (AccessibilityManager.getInstance(this.mContext).isEnabled())
                    announceUnlockHandle();
            }
        }
    }

    /** @deprecated */
    private void vibrate(long paramLong)
    {
        try
        {
            if (this.mVibrator == null)
                this.mVibrator = ((Vibrator)getContext().getSystemService("vibrator"));
            this.mVibrator.vibrate(paramLong);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void waveUpdateFrame(float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        double d1 = paramFloat1 - this.mLockCenterX;
        double d2 = paramFloat2 - this.mLockCenterY;
        int i = (int)Math.ceil(Math.hypot(d1, d2));
        double d3 = Math.atan2(d1, d2);
        float f1 = (float)(this.mLockCenterX + this.mRingRadius * Math.sin(d3));
        float f2 = (float)(this.mLockCenterY + this.mRingRadius * Math.cos(d3));
        switch (this.mLockState)
        {
        default:
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            this.mUnlockDefault.startAnimations(this);
            this.mUnlockHalo.startAnimations(this);
            this.mUnlockRing.startAnimations(this);
            return;
            this.mWaveTimerDelay = 100L;
            for (int i1 = 0; ; i1++)
            {
                int i2 = this.mLightWaves.size();
                if (i1 >= i2)
                    break;
                ((DrawableHolder)this.mLightWaves.get(i1)).addAnimTo(300L, 0L, "alpha", 0.0F, false);
            }
            for (int i3 = 0; ; i3++)
            {
                int i4 = this.mLightWaves.size();
                if (i3 >= i4)
                    break;
                ((DrawableHolder)this.mLightWaves.get(i3)).startAnimations(this);
            }
            this.mUnlockRing.addAnimTo(300L, 0L, "x", this.mLockCenterX, true);
            this.mUnlockRing.addAnimTo(300L, 0L, "y", this.mLockCenterY, true);
            this.mUnlockRing.addAnimTo(300L, 0L, "scaleX", 0.1F, true);
            this.mUnlockRing.addAnimTo(300L, 0L, "scaleY", 0.1F, true);
            this.mUnlockRing.addAnimTo(300L, 0L, "alpha", 0.0F, true);
            this.mUnlockDefault.removeAnimationFor("x");
            this.mUnlockDefault.removeAnimationFor("y");
            this.mUnlockDefault.removeAnimationFor("scaleX");
            this.mUnlockDefault.removeAnimationFor("scaleY");
            this.mUnlockDefault.removeAnimationFor("alpha");
            this.mUnlockDefault.setX(this.mLockCenterX);
            this.mUnlockDefault.setY(this.mLockCenterY);
            this.mUnlockDefault.setScaleX(0.1F);
            this.mUnlockDefault.setScaleY(0.1F);
            this.mUnlockDefault.setAlpha(0.0F);
            this.mUnlockDefault.addAnimTo(300L, 100L, "scaleX", 1.0F, true);
            this.mUnlockDefault.addAnimTo(300L, 100L, "scaleY", 1.0F, true);
            this.mUnlockDefault.addAnimTo(300L, 100L, "alpha", 1.0F, true);
            this.mUnlockHalo.removeAnimationFor("x");
            this.mUnlockHalo.removeAnimationFor("y");
            this.mUnlockHalo.removeAnimationFor("scaleX");
            this.mUnlockHalo.removeAnimationFor("scaleY");
            this.mUnlockHalo.removeAnimationFor("alpha");
            this.mUnlockHalo.setX(this.mLockCenterX);
            this.mUnlockHalo.setY(this.mLockCenterY);
            this.mUnlockHalo.setScaleX(0.1F);
            this.mUnlockHalo.setScaleY(0.1F);
            this.mUnlockHalo.setAlpha(0.0F);
            this.mUnlockHalo.addAnimTo(300L, 100L, "x", this.mLockCenterX, true);
            this.mUnlockHalo.addAnimTo(300L, 100L, "y", this.mLockCenterY, true);
            this.mUnlockHalo.addAnimTo(300L, 100L, "scaleX", 1.0F, true);
            this.mUnlockHalo.addAnimTo(300L, 100L, "scaleY", 1.0F, true);
            this.mUnlockHalo.addAnimTo(300L, 100L, "alpha", 1.0F, true);
            removeCallbacks(this.mLockTimerActions);
            this.mLockState = 1;
            continue;
            this.mWaveTimerDelay = 100L;
            continue;
            this.mUnlockDefault.removeAnimationFor("x");
            this.mUnlockDefault.removeAnimationFor("y");
            this.mUnlockDefault.removeAnimationFor("scaleX");
            this.mUnlockDefault.removeAnimationFor("scaleY");
            this.mUnlockDefault.removeAnimationFor("alpha");
            this.mUnlockDefault.setX(182.0F + this.mLockCenterX);
            this.mUnlockDefault.setY(this.mLockCenterY);
            this.mUnlockDefault.setScaleX(0.1F);
            this.mUnlockDefault.setScaleY(0.1F);
            this.mUnlockDefault.setAlpha(0.0F);
            this.mUnlockDefault.addAnimTo(300L, 100L, "scaleX", 1.0F, false);
            this.mUnlockDefault.addAnimTo(300L, 100L, "scaleY", 1.0F, false);
            this.mUnlockDefault.addAnimTo(300L, 100L, "alpha", 1.0F, false);
            this.mUnlockRing.addAnimTo(300L, 0L, "scaleX", 1.0F, true);
            this.mUnlockRing.addAnimTo(300L, 0L, "scaleY", 1.0F, true);
            this.mUnlockRing.addAnimTo(300L, 0L, "alpha", 1.0F, true);
            this.mLockState = 3;
            continue;
            if (i > this.mSnapRadius)
            {
                this.mFinishWaves = true;
                if (paramBoolean)
                {
                    this.mUnlockHalo.addAnimTo(0L, 0L, "x", f1, true);
                    this.mUnlockHalo.addAnimTo(0L, 0L, "y", f2, true);
                    this.mUnlockHalo.addAnimTo(0L, 0L, "scaleX", 1.0F, true);
                    this.mUnlockHalo.addAnimTo(0L, 0L, "scaleY", 1.0F, true);
                    this.mUnlockHalo.addAnimTo(0L, 0L, "alpha", 1.0F, true);
                }
                else
                {
                    this.mLockState = 4;
                }
            }
            else
            {
                if (!this.mWavesRunning)
                {
                    this.mWavesRunning = true;
                    this.mFinishWaves = false;
                    postDelayed(this.mAddWaveAction, this.mWaveTimerDelay);
                }
                this.mUnlockHalo.addAnimTo(0L, 0L, "x", paramFloat1, true);
                this.mUnlockHalo.addAnimTo(0L, 0L, "y", paramFloat2, true);
                this.mUnlockHalo.addAnimTo(0L, 0L, "scaleX", 1.0F, true);
                this.mUnlockHalo.addAnimTo(0L, 0L, "scaleY", 1.0F, true);
                this.mUnlockHalo.addAnimTo(0L, 0L, "alpha", 1.0F, true);
                continue;
                if (i > this.mSnapRadius)
                {
                    for (int j = 0; ; j++)
                    {
                        int k = this.mLightWaves.size();
                        if (j >= k)
                            break;
                        DrawableHolder localDrawableHolder = (DrawableHolder)this.mLightWaves.get(j);
                        long l = 1000L * (j + 6 - this.mCurrentWave) / 10L;
                        localDrawableHolder.addAnimTo(200L, l, "x", f1, true);
                        localDrawableHolder.addAnimTo(200L, l, "y", f2, true);
                        localDrawableHolder.addAnimTo(200L, l, "scaleX", 0.1F, true);
                        localDrawableHolder.addAnimTo(200L, l, "scaleY", 0.1F, true);
                        localDrawableHolder.addAnimTo(200L, l, "alpha", 0.0F, true);
                    }
                    for (int m = 0; ; m++)
                    {
                        int n = this.mLightWaves.size();
                        if (m >= n)
                            break;
                        ((DrawableHolder)this.mLightWaves.get(m)).startAnimations(this);
                    }
                    this.mUnlockRing.addAnimTo(200L, 0L, "x", f1, false);
                    this.mUnlockRing.addAnimTo(200L, 0L, "y", f2, false);
                    this.mUnlockRing.addAnimTo(200L, 0L, "scaleX", 0.1F, false);
                    this.mUnlockRing.addAnimTo(200L, 0L, "scaleY", 0.1F, false);
                    this.mUnlockRing.addAnimTo(200L, 0L, "alpha", 0.0F, false);
                    this.mUnlockRing.addAnimTo(200L, 200L, "alpha", 0.0F, false);
                    this.mUnlockDefault.removeAnimationFor("x");
                    this.mUnlockDefault.removeAnimationFor("y");
                    this.mUnlockDefault.removeAnimationFor("scaleX");
                    this.mUnlockDefault.removeAnimationFor("scaleY");
                    this.mUnlockDefault.removeAnimationFor("alpha");
                    this.mUnlockDefault.setX(f1);
                    this.mUnlockDefault.setY(f2);
                    this.mUnlockDefault.setScaleX(0.1F);
                    this.mUnlockDefault.setScaleY(0.1F);
                    this.mUnlockDefault.setAlpha(0.0F);
                    this.mUnlockDefault.addAnimTo(200L, 0L, "x", f1, true);
                    this.mUnlockDefault.addAnimTo(200L, 0L, "y", f2, true);
                    this.mUnlockDefault.addAnimTo(200L, 0L, "scaleX", 1.0F, true);
                    this.mUnlockDefault.addAnimTo(200L, 0L, "scaleY", 1.0F, true);
                    this.mUnlockDefault.addAnimTo(200L, 0L, "alpha", 1.0F, true);
                    this.mUnlockDefault.addAnimTo(200L, 200L, "scaleX", 3.0F, false);
                    this.mUnlockDefault.addAnimTo(200L, 200L, "scaleY", 3.0F, false);
                    this.mUnlockDefault.addAnimTo(200L, 200L, "alpha", 0.0F, false);
                    this.mUnlockHalo.addAnimTo(200L, 0L, "x", f1, false);
                    this.mUnlockHalo.addAnimTo(200L, 0L, "y", f2, false);
                    this.mUnlockHalo.addAnimTo(200L, 200L, "scaleX", 3.0F, false);
                    this.mUnlockHalo.addAnimTo(200L, 200L, "scaleY", 3.0F, false);
                    this.mUnlockHalo.addAnimTo(200L, 200L, "alpha", 0.0F, false);
                    removeCallbacks(this.mLockTimerActions);
                    postDelayed(this.mLockTimerActions, 3000L);
                    dispatchTriggerEvent(10);
                    this.mLockState = 5;
                }
                else
                {
                    this.mLockState = 0;
                    continue;
                    removeCallbacks(this.mAddWaveAction);
                }
            }
        }
    }

    BitmapDrawable createDrawable(int paramInt)
    {
        Resources localResources = getResources();
        return new BitmapDrawable(localResources, BitmapFactory.decodeResource(localResources, paramInt));
    }

    protected int getSuggestedMinimumHeight()
    {
        return this.mUnlockRing.getHeight() + this.mUnlockHalo.getHeight();
    }

    protected int getSuggestedMinimumWidth()
    {
        return this.mUnlockRing.getWidth() + this.mUnlockHalo.getWidth();
    }

    public void onAnimationUpdate(ValueAnimator paramValueAnimator)
    {
        invalidate();
    }

    protected void onDraw(Canvas paramCanvas)
    {
        waveUpdateFrame(this.mMouseX, this.mMouseY, this.mFingerDown);
        for (int i = 0; i < this.mDrawables.size(); i++)
            ((DrawableHolder)this.mDrawables.get(i)).draw(paramCanvas);
        for (int j = 0; j < this.mLightWaves.size(); j++)
            ((DrawableHolder)this.mLightWaves.get(j)).draw(paramCanvas);
    }

    public boolean onHoverEvent(MotionEvent paramMotionEvent)
    {
        int i;
        if (AccessibilityManager.getInstance(this.mContext).isTouchExplorationEnabled())
        {
            i = paramMotionEvent.getAction();
            switch (i)
            {
            case 8:
            default:
            case 9:
            case 7:
            case 10:
            }
        }
        while (true)
        {
            onTouchEvent(paramMotionEvent);
            paramMotionEvent.setAction(i);
            return super.onHoverEvent(paramMotionEvent);
            paramMotionEvent.setAction(0);
            continue;
            paramMotionEvent.setAction(2);
            continue;
            paramMotionEvent.setAction(1);
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getMode(paramInt1);
        int j = View.MeasureSpec.getMode(paramInt2);
        int k = View.MeasureSpec.getSize(paramInt1);
        int m = View.MeasureSpec.getSize(paramInt2);
        int n;
        int i1;
        if (i == -2147483648)
        {
            n = Math.min(k, getSuggestedMinimumWidth());
            if (j != -2147483648)
                break label92;
            i1 = Math.min(m, getSuggestedMinimumWidth());
        }
        while (true)
        {
            setMeasuredDimension(n, i1);
            return;
            if (i == 1073741824)
            {
                n = k;
                break;
            }
            n = getSuggestedMinimumWidth();
            break;
            label92: if (j == 1073741824)
                i1 = m;
            else
                i1 = getSuggestedMinimumHeight();
        }
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mLockCenterX = (0.5F * paramInt1);
        this.mLockCenterY = (0.5F * paramInt2);
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = true;
        int i = paramMotionEvent.getAction();
        this.mMouseX = paramMotionEvent.getX();
        this.mMouseY = paramMotionEvent.getY();
        int j = 0;
        switch (i)
        {
        default:
            invalidate();
            if (j == 0)
                break;
        case 0:
        case 2:
        case 1:
        case 3:
        }
        while (true)
        {
            return bool;
            removeCallbacks(this.mLockTimerActions);
            this.mFingerDown = bool;
            tryTransitionToStartAttemptState(paramMotionEvent);
            j = 1;
            break;
            tryTransitionToStartAttemptState(paramMotionEvent);
            j = 1;
            break;
            this.mFingerDown = false;
            postDelayed(this.mLockTimerActions, 3000L);
            setGrabbedState(0);
            waveUpdateFrame(this.mMouseX, this.mMouseY, this.mFingerDown);
            j = 1;
            break;
            this.mFingerDown = false;
            j = 1;
            break;
            bool = super.onTouchEvent(paramMotionEvent);
        }
    }

    public void reset()
    {
        this.mLockState = 0;
        invalidate();
    }

    public void setOnTriggerListener(OnTriggerListener paramOnTriggerListener)
    {
        this.mOnTriggerListener = paramOnTriggerListener;
    }

    public static abstract interface OnTriggerListener
    {
        public static final int CENTER_HANDLE = 10;
        public static final int NO_HANDLE;

        public abstract void onGrabbedStateChange(View paramView, int paramInt);

        public abstract void onTrigger(View paramView, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.WaveView
 * JD-Core Version:        0.6.2
 */