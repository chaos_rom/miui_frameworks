package com.android.internal.widget;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Chronometer;
import android.widget.Chronometer.OnChronometerTickListener;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.RemoteViews.RemoteView;

@RemoteViews.RemoteView
public class TextProgressBar extends RelativeLayout
    implements Chronometer.OnChronometerTickListener
{
    static final int CHRONOMETER_ID = 16908308;
    static final int PROGRESSBAR_ID = 16908301;
    public static final String TAG = "TextProgressBar";
    Chronometer mChronometer = null;
    boolean mChronometerFollow = false;
    int mChronometerGravity = 0;
    int mDuration = -1;
    long mDurationBase = -1L;
    ProgressBar mProgressBar = null;

    public TextProgressBar(Context paramContext)
    {
        super(paramContext);
    }

    public TextProgressBar(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public TextProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
    {
        super.addView(paramView, paramInt, paramLayoutParams);
        int i = paramView.getId();
        boolean bool;
        if ((i == 16908308) && ((paramView instanceof Chronometer)))
        {
            this.mChronometer = ((Chronometer)paramView);
            this.mChronometer.setOnChronometerTickListener(this);
            if (paramLayoutParams.width == -2)
            {
                bool = true;
                this.mChronometerFollow = bool;
                this.mChronometerGravity = (0x800007 & this.mChronometer.getGravity());
            }
        }
        while (true)
        {
            return;
            bool = false;
            break;
            if ((i == 16908301) && ((paramView instanceof ProgressBar)))
                this.mProgressBar = ((ProgressBar)paramView);
        }
    }

    public void onChronometerTick(Chronometer paramChronometer)
    {
        if (this.mProgressBar == null)
            throw new RuntimeException("Expecting child ProgressBar with id 'android.R.id.progress'");
        long l = SystemClock.elapsedRealtime();
        if (l >= this.mDurationBase)
            this.mChronometer.stop();
        int i = (int)(this.mDurationBase - l);
        this.mProgressBar.setProgress(this.mDuration - i);
        int m;
        int n;
        int i1;
        int i2;
        if (this.mChronometerFollow)
        {
            RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)this.mProgressBar.getLayoutParams();
            int j = this.mProgressBar.getWidth() - (localLayoutParams.leftMargin + localLayoutParams.rightMargin);
            int k = j * this.mProgressBar.getProgress() / this.mProgressBar.getMax() + localLayoutParams.leftMargin;
            m = 0;
            n = this.mChronometer.getWidth();
            if (this.mChronometerGravity != 5)
                break label211;
            m = -n;
            i1 = k + m;
            i2 = j - localLayoutParams.rightMargin - n;
            if (i1 >= localLayoutParams.leftMargin)
                break label229;
            i1 = localLayoutParams.leftMargin;
        }
        while (true)
        {
            ((RelativeLayout.LayoutParams)this.mChronometer.getLayoutParams()).leftMargin = i1;
            this.mChronometer.requestLayout();
            return;
            label211: if (this.mChronometerGravity != 1)
                break;
            m = -(n / 2);
            break;
            label229: if (i1 > i2)
                i1 = i2;
        }
    }

    @RemotableViewMethod
    public void setDurationBase(long paramLong)
    {
        this.mDurationBase = paramLong;
        if ((this.mProgressBar == null) || (this.mChronometer == null))
            throw new RuntimeException("Expecting child ProgressBar with id 'android.R.id.progress' and Chronometer id 'android.R.id.text1'");
        this.mDuration = ((int)(paramLong - this.mChronometer.getBase()));
        if (this.mDuration <= 0)
            this.mDuration = 1;
        this.mProgressBar.setMax(this.mDuration);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.TextProgressBar
 * JD-Core Version:        0.6.2
 */