package com.android.internal.widget.multiwaveview;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;

public class TargetDrawable
{
    private static final boolean DEBUG = false;
    public static final int[] STATE_ACTIVE;
    public static final int[] STATE_FOCUSED = arrayOfInt3;
    public static final int[] STATE_INACTIVE;
    private static final String TAG = "TargetDrawable";
    private float mAlpha = 1.0F;
    private Drawable mDrawable;
    private boolean mEnabled = true;
    private float mPositionX = 0.0F;
    private float mPositionY = 0.0F;
    private final int mResourceId;
    private float mScaleX = 1.0F;
    private float mScaleY = 1.0F;
    private float mTranslationX = 0.0F;
    private float mTranslationY = 0.0F;

    static
    {
        int[] arrayOfInt1 = new int[2];
        arrayOfInt1[0] = 16842910;
        arrayOfInt1[1] = 16842914;
        STATE_ACTIVE = arrayOfInt1;
        int[] arrayOfInt2 = new int[2];
        arrayOfInt2[0] = 16842910;
        arrayOfInt2[1] = -16842914;
        STATE_INACTIVE = arrayOfInt2;
        int[] arrayOfInt3 = new int[3];
        arrayOfInt3[0] = 16842910;
        arrayOfInt3[1] = -16842914;
        arrayOfInt3[2] = 16842908;
    }

    public TargetDrawable(Resources paramResources, int paramInt)
    {
        this.mResourceId = paramInt;
        setDrawable(paramResources, paramInt);
    }

    public TargetDrawable(TargetDrawable paramTargetDrawable)
    {
        this.mResourceId = paramTargetDrawable.mResourceId;
        if (paramTargetDrawable.mDrawable != null);
        for (Drawable localDrawable = paramTargetDrawable.mDrawable.mutate(); ; localDrawable = null)
        {
            this.mDrawable = localDrawable;
            resizeDrawables();
            setState(STATE_INACTIVE);
            return;
        }
    }

    private void resizeDrawables()
    {
        if ((this.mDrawable instanceof StateListDrawable))
        {
            StateListDrawable localStateListDrawable = (StateListDrawable)this.mDrawable;
            int i = 0;
            int j = 0;
            for (int k = 0; k < localStateListDrawable.getStateCount(); k++)
            {
                Drawable localDrawable = localStateListDrawable.getStateDrawable(k);
                i = Math.max(i, localDrawable.getIntrinsicWidth());
                j = Math.max(j, localDrawable.getIntrinsicHeight());
            }
            localStateListDrawable.setBounds(0, 0, i, j);
            for (int m = 0; m < localStateListDrawable.getStateCount(); m++)
                localStateListDrawable.getStateDrawable(m).setBounds(0, 0, i, j);
        }
        if (this.mDrawable != null)
            this.mDrawable.setBounds(0, 0, this.mDrawable.getIntrinsicWidth(), this.mDrawable.getIntrinsicHeight());
    }

    public void draw(Canvas paramCanvas)
    {
        if ((this.mDrawable == null) || (!this.mEnabled));
        while (true)
        {
            return;
            paramCanvas.save(1);
            paramCanvas.scale(this.mScaleX, this.mScaleY, this.mPositionX, this.mPositionY);
            paramCanvas.translate(this.mTranslationX + this.mPositionX, this.mTranslationY + this.mPositionY);
            paramCanvas.translate(-0.5F * getWidth(), -0.5F * getHeight());
            this.mDrawable.setAlpha(Math.round(255.0F * this.mAlpha));
            this.mDrawable.draw(paramCanvas);
            paramCanvas.restore();
        }
    }

    public float getAlpha()
    {
        return this.mAlpha;
    }

    public int getHeight()
    {
        if (this.mDrawable != null);
        for (int i = this.mDrawable.getIntrinsicHeight(); ; i = 0)
            return i;
    }

    public float getPositionX()
    {
        return this.mPositionX;
    }

    public float getPositionY()
    {
        return this.mPositionY;
    }

    public int getResourceId()
    {
        return this.mResourceId;
    }

    public float getScaleX()
    {
        return this.mScaleX;
    }

    public float getScaleY()
    {
        return this.mScaleY;
    }

    public int getWidth()
    {
        if (this.mDrawable != null);
        for (int i = this.mDrawable.getIntrinsicWidth(); ; i = 0)
            return i;
    }

    public float getX()
    {
        return this.mTranslationX;
    }

    public float getY()
    {
        return this.mTranslationY;
    }

    public boolean hasState(int[] paramArrayOfInt)
    {
        boolean bool = false;
        if (((this.mDrawable instanceof StateListDrawable)) && (((StateListDrawable)this.mDrawable).getStateDrawableIndex(paramArrayOfInt) != -1))
            bool = true;
        return bool;
    }

    public boolean isActive()
    {
        int i;
        if ((this.mDrawable instanceof StateListDrawable))
        {
            int[] arrayOfInt = ((StateListDrawable)this.mDrawable).getState();
            i = 0;
            if (i < arrayOfInt.length)
                if (arrayOfInt[i] != 16842908);
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    public boolean isEnabled()
    {
        if ((this.mDrawable != null) && (this.mEnabled));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setAlpha(float paramFloat)
    {
        this.mAlpha = paramFloat;
    }

    public void setDrawable(Resources paramResources, int paramInt)
    {
        Drawable localDrawable1 = null;
        if (paramInt == 0);
        for (Drawable localDrawable2 = null; ; localDrawable2 = paramResources.getDrawable(paramInt))
        {
            if (localDrawable2 != null)
                localDrawable1 = localDrawable2.mutate();
            this.mDrawable = localDrawable1;
            resizeDrawables();
            setState(STATE_INACTIVE);
            return;
        }
    }

    public void setEnabled(boolean paramBoolean)
    {
        this.mEnabled = paramBoolean;
    }

    public void setPositionX(float paramFloat)
    {
        this.mPositionX = paramFloat;
    }

    public void setPositionY(float paramFloat)
    {
        this.mPositionY = paramFloat;
    }

    public void setScaleX(float paramFloat)
    {
        this.mScaleX = paramFloat;
    }

    public void setScaleY(float paramFloat)
    {
        this.mScaleY = paramFloat;
    }

    public void setState(int[] paramArrayOfInt)
    {
        if ((this.mDrawable instanceof StateListDrawable))
            ((StateListDrawable)this.mDrawable).setState(paramArrayOfInt);
    }

    public void setX(float paramFloat)
    {
        this.mTranslationX = paramFloat;
    }

    public void setY(float paramFloat)
    {
        this.mTranslationY = paramFloat;
    }

    static class DrawableWithAlpha extends Drawable
    {
        private float mAlpha = 1.0F;
        private Drawable mRealDrawable;

        public DrawableWithAlpha(Drawable paramDrawable)
        {
            this.mRealDrawable = paramDrawable;
        }

        public void draw(Canvas paramCanvas)
        {
            this.mRealDrawable.setAlpha(Math.round(255.0F * this.mAlpha));
            this.mRealDrawable.draw(paramCanvas);
        }

        public float getAlpha()
        {
            return this.mAlpha;
        }

        public int getOpacity()
        {
            return this.mRealDrawable.getOpacity();
        }

        public void setAlpha(float paramFloat)
        {
            this.mAlpha = paramFloat;
        }

        public void setAlpha(int paramInt)
        {
            this.mRealDrawable.setAlpha(paramInt);
        }

        public void setColorFilter(ColorFilter paramColorFilter)
        {
            this.mRealDrawable.setColorFilter(paramColorFilter);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.multiwaveview.TargetDrawable
 * JD-Core Version:        0.6.2
 */