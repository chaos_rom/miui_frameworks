package com.android.internal.widget.multiwaveview;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.accessibility.AccessibilityManager;
import java.util.ArrayList;

public class MultiWaveView extends View
{
    private static final int CHEVRON_ANIMATION_DURATION = 850;
    private static final int CHEVRON_INCREMENTAL_DELAY = 160;
    private static final boolean DEBUG = false;
    private static final int HIDE_ANIMATION_DELAY = 200;
    private static final int HIDE_ANIMATION_DURATION = 200;
    private static final int INITIAL_SHOW_HANDLE_DURATION = 200;
    private static final int RETURN_TO_HOME_DELAY = 1200;
    private static final int RETURN_TO_HOME_DURATION = 200;
    private static final float RING_SCALE_COLLAPSED = 0.5F;
    private static final float RING_SCALE_EXPANDED = 1.0F;
    private static final int SHOW_ANIMATION_DELAY = 50;
    private static final int SHOW_ANIMATION_DURATION = 200;
    private static final float SNAP_MARGIN_DEFAULT = 20.0F;
    private static final int STATE_FINISH = 5;
    private static final int STATE_FIRST_TOUCH = 2;
    private static final int STATE_IDLE = 0;
    private static final int STATE_SNAP = 4;
    private static final int STATE_START = 1;
    private static final int STATE_TRACKING = 3;
    private static final String TAG = "MultiWaveView";
    private static final float TAP_RADIUS_SCALE_ACCESSIBILITY_ENABLED = 1.3F;
    private static final float TARGET_SCALE_COLLAPSED = 0.8F;
    private static final float TARGET_SCALE_EXPANDED = 1.0F;
    private int mActiveTarget = -1;
    private boolean mAlwaysTrackFinger;
    private boolean mAnimatingTargets;
    private Tweener mBackgroundAnimator;
    private TimeInterpolator mChevronAnimationInterpolator = Ease.Quad.easeOut;
    private AnimationBundle mChevronAnimations = new AnimationBundle(null);
    private ArrayList<TargetDrawable> mChevronDrawables = new ArrayList();
    private ArrayList<String> mDirectionDescriptions;
    private int mDirectionDescriptionsResourceId;
    private boolean mDragging;
    private int mFeedbackCount = 3;
    private int mGrabbedState;
    private int mGravity = 48;
    private AnimationBundle mHandleAnimations = new AnimationBundle(null);
    private TargetDrawable mHandleDrawable;
    private int mHorizontalInset;
    private boolean mInitialLayout = true;
    private int mMaxTargetHeight;
    private int mMaxTargetWidth;
    private int mNewTargetResources;
    private OnTriggerListener mOnTriggerListener;
    private float mOuterRadius = 0.0F;
    private TargetDrawable mOuterRing;
    private Animator.AnimatorListener mResetListener = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            MultiWaveView.this.switchToState(0, MultiWaveView.this.mWaveCenterX, MultiWaveView.this.mWaveCenterY);
            MultiWaveView.this.dispatchOnFinishFinalAnimation();
        }
    };
    private Animator.AnimatorListener mResetListenerWithPing = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            MultiWaveView.this.ping();
            MultiWaveView.this.switchToState(0, MultiWaveView.this.mWaveCenterX, MultiWaveView.this.mWaveCenterY);
            MultiWaveView.this.dispatchOnFinishFinalAnimation();
        }
    };
    private float mSnapMargin = 0.0F;
    private float mTapRadius;
    private AnimationBundle mTargetAnimations = new AnimationBundle(null);
    private ArrayList<String> mTargetDescriptions;
    private int mTargetDescriptionsResourceId;
    private ArrayList<TargetDrawable> mTargetDrawables = new ArrayList();
    private int mTargetResourceId;
    private Animator.AnimatorListener mTargetUpdateListener = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            if (MultiWaveView.this.mNewTargetResources != 0)
            {
                MultiWaveView.this.internalSetTargetResources(MultiWaveView.this.mNewTargetResources);
                MultiWaveView.access$602(MultiWaveView.this, 0);
                MultiWaveView.this.hideTargets(false, false);
            }
            MultiWaveView.access$902(MultiWaveView.this, false);
        }
    };
    private ValueAnimator.AnimatorUpdateListener mUpdateListener = new ValueAnimator.AnimatorUpdateListener()
    {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
            MultiWaveView.this.invalidateGlobalRegion(MultiWaveView.this.mHandleDrawable);
            MultiWaveView.this.invalidate();
        }
    };
    private int mVerticalInset;
    private int mVibrationDuration = 0;
    private Vibrator mVibrator;
    private float mWaveCenterX;
    private float mWaveCenterY;

    public MultiWaveView(Context paramContext)
    {
        this(paramContext, null);
    }

    public MultiWaveView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        Resources localResources = paramContext.getResources();
        TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, com.android.internal.R.styleable.MultiWaveView);
        this.mOuterRadius = localTypedArray1.getDimension(6, this.mOuterRadius);
        this.mSnapMargin = localTypedArray1.getDimension(8, this.mSnapMargin);
        this.mVibrationDuration = localTypedArray1.getInt(7, this.mVibrationDuration);
        this.mFeedbackCount = localTypedArray1.getInt(9, this.mFeedbackCount);
        this.mHandleDrawable = new TargetDrawable(localResources, localTypedArray1.peekValue(3).resourceId);
        this.mTapRadius = (this.mHandleDrawable.getWidth() / 2);
        this.mOuterRing = new TargetDrawable(localResources, localTypedArray1.peekValue(5).resourceId);
        this.mAlwaysTrackFinger = localTypedArray1.getBoolean(10, false);
        TypedValue localTypedValue = new TypedValue();
        if (localTypedArray1.getValue(4, localTypedValue))
        {
            ArrayList localArrayList1 = loadDrawableArray(localTypedValue.resourceId);
            for (int k = 0; k < localArrayList1.size(); k++)
            {
                TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList1.get(k);
                int m = 0;
                if (m < this.mFeedbackCount)
                {
                    ArrayList localArrayList2 = this.mChevronDrawables;
                    if (localTargetDrawable == null);
                    for (Object localObject = null; ; localObject = new TargetDrawable(localTargetDrawable))
                    {
                        localArrayList2.add(localObject);
                        m++;
                        break;
                    }
                }
            }
        }
        if (localTypedArray1.getValue(2, localTypedValue))
            internalSetTargetResources(localTypedValue.resourceId);
        if ((this.mTargetDrawables == null) || (this.mTargetDrawables.size() == 0))
            throw new IllegalStateException("Must specify at least one target drawable");
        if (localTypedArray1.getValue(0, localTypedValue))
        {
            int j = localTypedValue.resourceId;
            if (j == 0)
                throw new IllegalStateException("Must specify target descriptions");
            setTargetDescriptionsResourceId(j);
        }
        if (localTypedArray1.getValue(1, localTypedValue))
        {
            int i = localTypedValue.resourceId;
            if (i == 0)
                throw new IllegalStateException("Must specify direction descriptions");
            setDirectionDescriptionsResourceId(i);
        }
        localTypedArray1.recycle();
        TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, android.R.styleable.LinearLayout);
        this.mGravity = localTypedArray2.getInt(0, 48);
        localTypedArray2.recycle();
        if (this.mVibrationDuration > 0);
        for (boolean bool = true; ; bool = false)
        {
            setVibrateEnabled(bool);
            assignDefaultsIfNeeded();
            return;
        }
    }

    private void activateHandle(int paramInt1, int paramInt2, float paramFloat, Animator.AnimatorListener paramAnimatorListener)
    {
        this.mHandleAnimations.cancel();
        AnimationBundle localAnimationBundle = this.mHandleAnimations;
        TargetDrawable localTargetDrawable = this.mHandleDrawable;
        long l = paramInt1;
        Object[] arrayOfObject = new Object[10];
        arrayOfObject[0] = "ease";
        arrayOfObject[1] = Ease.Cubic.easeIn;
        arrayOfObject[2] = "delay";
        arrayOfObject[3] = Integer.valueOf(paramInt2);
        arrayOfObject[4] = "alpha";
        arrayOfObject[5] = Float.valueOf(paramFloat);
        arrayOfObject[6] = "onUpdate";
        arrayOfObject[7] = this.mUpdateListener;
        arrayOfObject[8] = "onComplete";
        arrayOfObject[9] = paramAnimatorListener;
        localAnimationBundle.add(Tweener.to(localTargetDrawable, l, arrayOfObject));
        this.mHandleAnimations.start();
    }

    private void announceTargets()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = this.mTargetDrawables.size();
        for (int j = 0; j < i; j++)
        {
            String str1 = getTargetDescription(j);
            String str2 = getDirectionDescription(j);
            if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2)))
            {
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = str1;
                localStringBuilder.append(String.format(str2, arrayOfObject));
            }
            if (localStringBuilder.length() > 0)
                announceText(localStringBuilder.toString());
        }
    }

    private void announceText(String paramString)
    {
        setContentDescription(paramString);
        sendAccessibilityEvent(8);
        setContentDescription(null);
    }

    private void assignDefaultsIfNeeded()
    {
        if (this.mOuterRadius == 0.0F)
            this.mOuterRadius = (Math.max(this.mOuterRing.getWidth(), this.mOuterRing.getHeight()) / 2.0F);
        if (this.mSnapMargin == 0.0F)
            this.mSnapMargin = TypedValue.applyDimension(1, 20.0F, getContext().getResources().getDisplayMetrics());
    }

    private void computeInsets(int paramInt1, int paramInt2)
    {
        int i = getResolvedLayoutDirection();
        int j = Gravity.getAbsoluteGravity(this.mGravity, i);
        switch (j & 0x7)
        {
        case 4:
        default:
            this.mHorizontalInset = (paramInt1 / 2);
            switch (j & 0x70)
            {
            default:
                this.mVerticalInset = (paramInt2 / 2);
            case 48:
            case 80:
            }
            break;
        case 3:
        case 5:
        }
        while (true)
        {
            return;
            this.mHorizontalInset = 0;
            break;
            this.mHorizontalInset = paramInt1;
            break;
            this.mVerticalInset = 0;
            continue;
            this.mVerticalInset = paramInt2;
        }
    }

    private void deactivateHandle(int paramInt1, int paramInt2, float paramFloat, Animator.AnimatorListener paramAnimatorListener)
    {
        this.mHandleAnimations.cancel();
        AnimationBundle localAnimationBundle = this.mHandleAnimations;
        TargetDrawable localTargetDrawable = this.mHandleDrawable;
        long l = paramInt1;
        Object[] arrayOfObject = new Object[14];
        arrayOfObject[0] = "ease";
        arrayOfObject[1] = Ease.Quart.easeOut;
        arrayOfObject[2] = "delay";
        arrayOfObject[3] = Integer.valueOf(paramInt2);
        arrayOfObject[4] = "alpha";
        arrayOfObject[5] = Float.valueOf(paramFloat);
        arrayOfObject[6] = "x";
        arrayOfObject[7] = Integer.valueOf(0);
        arrayOfObject[8] = "y";
        arrayOfObject[9] = Integer.valueOf(0);
        arrayOfObject[10] = "onUpdate";
        arrayOfObject[11] = this.mUpdateListener;
        arrayOfObject[12] = "onComplete";
        arrayOfObject[13] = paramAnimatorListener;
        localAnimationBundle.add(Tweener.to(localTargetDrawable, l, arrayOfObject));
        this.mHandleAnimations.start();
    }

    private void deactivateTargets()
    {
        int i = this.mTargetDrawables.size();
        for (int j = 0; j < i; j++)
            ((TargetDrawable)this.mTargetDrawables.get(j)).setState(TargetDrawable.STATE_INACTIVE);
        this.mActiveTarget = -1;
    }

    private void dispatchOnFinishFinalAnimation()
    {
        if (this.mOnTriggerListener != null)
            this.mOnTriggerListener.onFinishFinalAnimation();
    }

    private void dispatchTriggerEvent(int paramInt)
    {
        vibrate();
        if (this.mOnTriggerListener != null)
            this.mOnTriggerListener.onTrigger(this, paramInt);
    }

    private float dist2(float paramFloat1, float paramFloat2)
    {
        return paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2;
    }

    private void doFinish()
    {
        int i = this.mActiveTarget;
        int j;
        if (i != -1)
        {
            j = 1;
            if (j == 0)
                break label67;
            highlightSelected(i);
            deactivateHandle(200, 1200, 0.0F, this.mResetListener);
            dispatchTriggerEvent(i);
            if (!this.mAlwaysTrackFinger)
                this.mTargetAnimations.stop();
        }
        while (true)
        {
            setGrabbedState(0);
            return;
            j = 0;
            break;
            label67: deactivateHandle(200, 200, 1.0F, this.mResetListenerWithPing);
            hideTargets(true, false);
        }
    }

    private void dump()
    {
        Log.v("MultiWaveView", "Outer Radius = " + this.mOuterRadius);
        Log.v("MultiWaveView", "SnapMargin = " + this.mSnapMargin);
        Log.v("MultiWaveView", "FeedbackCount = " + this.mFeedbackCount);
        Log.v("MultiWaveView", "VibrationDuration = " + this.mVibrationDuration);
        Log.v("MultiWaveView", "TapRadius = " + this.mTapRadius);
        Log.v("MultiWaveView", "WaveCenterX = " + this.mWaveCenterX);
        Log.v("MultiWaveView", "WaveCenterY = " + this.mWaveCenterY);
    }

    private String getDirectionDescription(int paramInt)
    {
        if ((this.mDirectionDescriptions == null) || (this.mDirectionDescriptions.isEmpty()))
        {
            this.mDirectionDescriptions = loadDescriptions(this.mDirectionDescriptionsResourceId);
            if (this.mTargetDrawables.size() != this.mDirectionDescriptions.size())
                Log.w("MultiWaveView", "The number of target drawables must be euqal to the number of direction descriptions.");
        }
        for (String str = null; ; str = (String)this.mDirectionDescriptions.get(paramInt))
            return str;
    }

    private float getScaledTapRadiusSquared()
    {
        if (AccessibilityManager.getInstance(this.mContext).isEnabled());
        for (float f = 1.3F * this.mTapRadius; ; f = this.mTapRadius)
            return square(f);
    }

    private String getTargetDescription(int paramInt)
    {
        if ((this.mTargetDescriptions == null) || (this.mTargetDescriptions.isEmpty()))
        {
            this.mTargetDescriptions = loadDescriptions(this.mTargetDescriptionsResourceId);
            if (this.mTargetDrawables.size() != this.mTargetDescriptions.size())
                Log.w("MultiWaveView", "The number of target drawables must be euqal to the number of target descriptions.");
        }
        for (String str = null; ; str = (String)this.mTargetDescriptions.get(paramInt))
            return str;
    }

    private void handleCancel(MotionEvent paramMotionEvent)
    {
        switchToState(5, paramMotionEvent.getX(), paramMotionEvent.getY());
    }

    private void handleDown(MotionEvent paramMotionEvent)
    {
        float f1 = paramMotionEvent.getX();
        float f2 = paramMotionEvent.getY();
        switchToState(1, f1, f2);
        if (!trySwitchToFirstTouchState(f1, f2))
        {
            this.mDragging = false;
            ping();
        }
    }

    private void handleMove(MotionEvent paramMotionEvent)
    {
        int i = -1;
        int j = paramMotionEvent.getHistorySize();
        ArrayList localArrayList = this.mTargetDrawables;
        int k = localArrayList.size();
        float f1 = 0.0F;
        float f2 = 0.0F;
        for (int m = 0; m < j + 1; m++)
        {
            float f3;
            float f4;
            label66: float f5;
            float f6;
            float f8;
            label118: float f9;
            float f10;
            float f12;
            int n;
            if (m < j)
            {
                f3 = paramMotionEvent.getHistoricalX(m);
                if (m >= j)
                    break label337;
                f4 = paramMotionEvent.getHistoricalY(m);
                f5 = f3 - this.mWaveCenterX;
                f6 = f4 - this.mWaveCenterY;
                float f7 = (float)Math.sqrt(dist2(f5, f6));
                if (f7 <= this.mOuterRadius)
                    break label346;
                f8 = this.mOuterRadius / f7;
                f9 = f5 * f8;
                f10 = f6 * f8;
                double d1 = Math.atan2(-f6, f5);
                if (!this.mDragging)
                    trySwitchToFirstTouchState(f3, f4);
                if (!this.mDragging)
                    break label358;
                float f11 = this.mOuterRadius - this.mSnapMargin;
                f12 = f11 * f11;
                n = 0;
                label188: if (n >= k)
                    break label358;
                TargetDrawable localTargetDrawable3 = (TargetDrawable)localArrayList.get(n);
                double d2 = 3.141592653589793D * (2.0D * (n - 0.5D)) / k;
                double d3 = 3.141592653589793D * (2.0D * (0.5D + n)) / k;
                if (localTargetDrawable3.isEnabled())
                    if (((d1 <= d2) || (d1 > d3)) && ((6.283185307179586D + d1 <= d2) || (6.283185307179586D + d1 > d3)))
                        break label352;
            }
            label337: label346: label352: for (int i1 = 1; ; i1 = 0)
            {
                if ((i1 != 0) && (dist2(f5, f6) > f12))
                    i = n;
                n++;
                break label188;
                f3 = paramMotionEvent.getX();
                break;
                f4 = paramMotionEvent.getY();
                break label66;
                f8 = 1.0F;
                break label118;
            }
            label358: f1 = f9;
            f2 = f10;
        }
        if (!this.mDragging)
            return;
        if (i != -1)
        {
            switchToState(4, f1, f2);
            moveHandleTo(f1, f2, false);
            label404: invalidateGlobalRegion(this.mHandleDrawable);
            if (this.mActiveTarget != i)
            {
                if (this.mActiveTarget != -1)
                {
                    TargetDrawable localTargetDrawable2 = (TargetDrawable)localArrayList.get(this.mActiveTarget);
                    if (localTargetDrawable2.hasState(TargetDrawable.STATE_FOCUSED))
                        localTargetDrawable2.setState(TargetDrawable.STATE_INACTIVE);
                }
                if (i == -1)
                    break label557;
                TargetDrawable localTargetDrawable1 = (TargetDrawable)localArrayList.get(i);
                if (localTargetDrawable1.hasState(TargetDrawable.STATE_FOCUSED))
                    localTargetDrawable1.setState(TargetDrawable.STATE_FOCUSED);
                if (AccessibilityManager.getInstance(this.mContext).isEnabled())
                    announceText(getTargetDescription(i));
                activateHandle(0, 0, 0.0F, null);
            }
        }
        while (true)
        {
            this.mActiveTarget = i;
            break;
            switchToState(3, f1, f2);
            moveHandleTo(f1, f2, false);
            break label404;
            label557: activateHandle(0, 0, 1.0F, null);
        }
    }

    private void handleUp(MotionEvent paramMotionEvent)
    {
        switchToState(5, paramMotionEvent.getX(), paramMotionEvent.getY());
    }

    private void hideChevrons()
    {
        ArrayList localArrayList = this.mChevronDrawables;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
        {
            TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(j);
            if (localTargetDrawable != null)
                localTargetDrawable.setAlpha(0.0F);
        }
    }

    private void hideTargets(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mTargetAnimations.cancel();
        this.mAnimatingTargets = paramBoolean1;
        int i;
        int j;
        if (paramBoolean1)
        {
            i = 200;
            if (!paramBoolean1)
                break label218;
            j = 200;
            label29: if (!paramBoolean2)
                break label224;
        }
        label218: label224: for (float f1 = 1.0F; ; f1 = 0.8F)
        {
            int k = this.mTargetDrawables.size();
            for (int m = 0; m < k; m++)
            {
                TargetDrawable localTargetDrawable2 = (TargetDrawable)this.mTargetDrawables.get(m);
                localTargetDrawable2.setState(TargetDrawable.STATE_INACTIVE);
                AnimationBundle localAnimationBundle2 = this.mTargetAnimations;
                long l2 = i;
                Object[] arrayOfObject2 = new Object[12];
                arrayOfObject2[0] = "ease";
                arrayOfObject2[1] = Ease.Cubic.easeOut;
                arrayOfObject2[2] = "alpha";
                arrayOfObject2[3] = Float.valueOf(0.0F);
                arrayOfObject2[4] = "scaleX";
                arrayOfObject2[5] = Float.valueOf(f1);
                arrayOfObject2[6] = "scaleY";
                arrayOfObject2[7] = Float.valueOf(f1);
                arrayOfObject2[8] = "delay";
                arrayOfObject2[9] = Integer.valueOf(j);
                arrayOfObject2[10] = "onUpdate";
                arrayOfObject2[11] = this.mUpdateListener;
                localAnimationBundle2.add(Tweener.to(localTargetDrawable2, l2, arrayOfObject2));
            }
            i = 0;
            break;
            j = 0;
            break label29;
        }
        if (paramBoolean2);
        for (float f2 = 1.0F; ; f2 = 0.5F)
        {
            AnimationBundle localAnimationBundle1 = this.mTargetAnimations;
            TargetDrawable localTargetDrawable1 = this.mOuterRing;
            long l1 = i;
            Object[] arrayOfObject1 = new Object[14];
            arrayOfObject1[0] = "ease";
            arrayOfObject1[1] = Ease.Cubic.easeOut;
            arrayOfObject1[2] = "alpha";
            arrayOfObject1[3] = Float.valueOf(0.0F);
            arrayOfObject1[4] = "scaleX";
            arrayOfObject1[5] = Float.valueOf(f2);
            arrayOfObject1[6] = "scaleY";
            arrayOfObject1[7] = Float.valueOf(f2);
            arrayOfObject1[8] = "delay";
            arrayOfObject1[9] = Integer.valueOf(j);
            arrayOfObject1[10] = "onUpdate";
            arrayOfObject1[11] = this.mUpdateListener;
            arrayOfObject1[12] = "onComplete";
            arrayOfObject1[13] = this.mTargetUpdateListener;
            localAnimationBundle1.add(Tweener.to(localTargetDrawable1, l1, arrayOfObject1));
            this.mTargetAnimations.start();
            return;
        }
    }

    private void hideUnselected(int paramInt)
    {
        for (int i = 0; i < this.mTargetDrawables.size(); i++)
            if (i != paramInt)
                ((TargetDrawable)this.mTargetDrawables.get(i)).setAlpha(0.0F);
    }

    private void highlightSelected(int paramInt)
    {
        ((TargetDrawable)this.mTargetDrawables.get(paramInt)).setState(TargetDrawable.STATE_ACTIVE);
        hideUnselected(paramInt);
    }

    private void internalSetTargetResources(int paramInt)
    {
        this.mTargetDrawables = loadDrawableArray(paramInt);
        this.mTargetResourceId = paramInt;
        int i = this.mTargetDrawables.size();
        int j = this.mHandleDrawable.getWidth();
        int k = this.mHandleDrawable.getHeight();
        for (int m = 0; m < i; m++)
        {
            TargetDrawable localTargetDrawable = (TargetDrawable)this.mTargetDrawables.get(m);
            j = Math.max(j, localTargetDrawable.getWidth());
            k = Math.max(k, localTargetDrawable.getHeight());
        }
        if ((this.mMaxTargetWidth != j) || (this.mMaxTargetHeight != k))
        {
            this.mMaxTargetWidth = j;
            this.mMaxTargetHeight = k;
            requestLayout();
        }
        while (true)
        {
            return;
            updateTargetPositions(this.mWaveCenterX, this.mWaveCenterY);
            updateChevronPositions(this.mWaveCenterX, this.mWaveCenterY);
        }
    }

    private ArrayList<String> loadDescriptions(int paramInt)
    {
        TypedArray localTypedArray = getContext().getResources().obtainTypedArray(paramInt);
        int i = localTypedArray.length();
        ArrayList localArrayList = new ArrayList(i);
        for (int j = 0; j < i; j++)
            localArrayList.add(localTypedArray.getString(j));
        localTypedArray.recycle();
        return localArrayList;
    }

    private ArrayList<TargetDrawable> loadDrawableArray(int paramInt)
    {
        Resources localResources = getContext().getResources();
        TypedArray localTypedArray = localResources.obtainTypedArray(paramInt);
        int i = localTypedArray.length();
        ArrayList localArrayList = new ArrayList(i);
        int j = 0;
        if (j < i)
        {
            TypedValue localTypedValue = localTypedArray.peekValue(j);
            if (localTypedValue != null);
            for (int k = localTypedValue.resourceId; ; k = 0)
            {
                localArrayList.add(new TargetDrawable(localResources, k));
                j++;
                break;
            }
        }
        localTypedArray.recycle();
        return localArrayList;
    }

    private void moveHandleTo(float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        this.mHandleDrawable.setX(paramFloat1);
        this.mHandleDrawable.setY(paramFloat2);
    }

    private boolean replaceTargetDrawables(Resources paramResources, int paramInt1, int paramInt2)
    {
        boolean bool;
        if ((paramInt1 == 0) || (paramInt2 == 0))
            bool = false;
        while (true)
        {
            return bool;
            bool = false;
            ArrayList localArrayList = this.mTargetDrawables;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
            {
                TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(j);
                if ((localTargetDrawable != null) && (localTargetDrawable.getResourceId() == paramInt1))
                {
                    localTargetDrawable.setDrawable(paramResources, paramInt2);
                    bool = true;
                }
            }
            if (bool)
                requestLayout();
        }
    }

    private int resolveMeasured(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getSize(paramInt1);
        int j;
        switch (View.MeasureSpec.getMode(paramInt1))
        {
        default:
            j = i;
        case 0:
        case -2147483648:
        }
        while (true)
        {
            return j;
            j = paramInt2;
            continue;
            j = Math.min(i, paramInt2);
        }
    }

    private void setGrabbedState(int paramInt)
    {
        if (paramInt != this.mGrabbedState)
        {
            if (paramInt != 0)
                vibrate();
            this.mGrabbedState = paramInt;
            if (this.mOnTriggerListener != null)
            {
                if (paramInt != 0)
                    break label55;
                this.mOnTriggerListener.onReleased(this, 1);
            }
        }
        while (true)
        {
            this.mOnTriggerListener.onGrabbedStateChange(this, paramInt);
            return;
            label55: this.mOnTriggerListener.onGrabbed(this, 1);
        }
    }

    private void showTargets(boolean paramBoolean)
    {
        this.mTargetAnimations.stop();
        this.mAnimatingTargets = paramBoolean;
        int i;
        if (paramBoolean)
        {
            i = 50;
            if (!paramBoolean)
                break label206;
        }
        label206: for (int j = 200; ; j = 0)
        {
            int k = this.mTargetDrawables.size();
            for (int m = 0; m < k; m++)
            {
                TargetDrawable localTargetDrawable2 = (TargetDrawable)this.mTargetDrawables.get(m);
                localTargetDrawable2.setState(TargetDrawable.STATE_INACTIVE);
                AnimationBundle localAnimationBundle2 = this.mTargetAnimations;
                long l2 = j;
                Object[] arrayOfObject2 = new Object[12];
                arrayOfObject2[0] = "ease";
                arrayOfObject2[1] = Ease.Cubic.easeOut;
                arrayOfObject2[2] = "alpha";
                arrayOfObject2[3] = Float.valueOf(1.0F);
                arrayOfObject2[4] = "scaleX";
                arrayOfObject2[5] = Float.valueOf(1.0F);
                arrayOfObject2[6] = "scaleY";
                arrayOfObject2[7] = Float.valueOf(1.0F);
                arrayOfObject2[8] = "delay";
                arrayOfObject2[9] = Integer.valueOf(i);
                arrayOfObject2[10] = "onUpdate";
                arrayOfObject2[11] = this.mUpdateListener;
                localAnimationBundle2.add(Tweener.to(localTargetDrawable2, l2, arrayOfObject2));
            }
            i = 0;
            break;
        }
        AnimationBundle localAnimationBundle1 = this.mTargetAnimations;
        TargetDrawable localTargetDrawable1 = this.mOuterRing;
        long l1 = j;
        Object[] arrayOfObject1 = new Object[14];
        arrayOfObject1[0] = "ease";
        arrayOfObject1[1] = Ease.Cubic.easeOut;
        arrayOfObject1[2] = "alpha";
        arrayOfObject1[3] = Float.valueOf(1.0F);
        arrayOfObject1[4] = "scaleX";
        arrayOfObject1[5] = Float.valueOf(1.0F);
        arrayOfObject1[6] = "scaleY";
        arrayOfObject1[7] = Float.valueOf(1.0F);
        arrayOfObject1[8] = "delay";
        arrayOfObject1[9] = Integer.valueOf(i);
        arrayOfObject1[10] = "onUpdate";
        arrayOfObject1[11] = this.mUpdateListener;
        arrayOfObject1[12] = "onComplete";
        arrayOfObject1[13] = this.mTargetUpdateListener;
        localAnimationBundle1.add(Tweener.to(localTargetDrawable1, l1, arrayOfObject1));
        this.mTargetAnimations.start();
    }

    private float square(float paramFloat)
    {
        return paramFloat * paramFloat;
    }

    private void startBackgroundAnimation(int paramInt, float paramFloat)
    {
        Drawable localDrawable = getBackground();
        if ((this.mAlwaysTrackFinger) && (localDrawable != null))
        {
            if (this.mBackgroundAnimator != null)
                this.mBackgroundAnimator.animator.end();
            long l = paramInt;
            Object[] arrayOfObject = new Object[6];
            arrayOfObject[0] = "ease";
            arrayOfObject[1] = Ease.Cubic.easeIn;
            arrayOfObject[2] = "alpha";
            int[] arrayOfInt = new int[2];
            arrayOfInt[0] = 0;
            arrayOfInt[1] = ((int)(255.0F * paramFloat));
            arrayOfObject[3] = arrayOfInt;
            arrayOfObject[4] = "delay";
            arrayOfObject[5] = Integer.valueOf(50);
            this.mBackgroundAnimator = Tweener.to(localDrawable, l, arrayOfObject);
            this.mBackgroundAnimator.animator.start();
        }
    }

    private void startChevronAnimation()
    {
        float f1 = 0.8F * this.mHandleDrawable.getWidth();
        float f2 = 0.9F * this.mOuterRadius / 2.0F;
        int i;
        if (this.mFeedbackCount > 0)
        {
            i = this.mChevronDrawables.size() / this.mFeedbackCount;
            this.mChevronAnimations.stop();
        }
        for (int j = 0; ; j++)
        {
            if (j >= i)
                break label395;
            double d = 6.283185307179586D * j / i;
            float f3 = (float)Math.cos(d);
            float f4 = 0.0F - (float)Math.sin(d);
            float[] arrayOfFloat1 = new float[2];
            arrayOfFloat1[0] = (f3 * f1);
            arrayOfFloat1[1] = (f3 * f2);
            float[] arrayOfFloat2 = new float[2];
            arrayOfFloat2[0] = (f4 * f1);
            arrayOfFloat2[1] = (f4 * f2);
            int k = 0;
            label134: if (k < this.mFeedbackCount)
            {
                int m = k * 160;
                TargetDrawable localTargetDrawable = (TargetDrawable)this.mChevronDrawables.get(k + j * this.mFeedbackCount);
                if (localTargetDrawable == null);
                while (true)
                {
                    k++;
                    break label134;
                    i = 0;
                    break;
                    AnimationBundle localAnimationBundle = this.mChevronAnimations;
                    Object[] arrayOfObject = new Object[16];
                    arrayOfObject[0] = "ease";
                    arrayOfObject[1] = this.mChevronAnimationInterpolator;
                    arrayOfObject[2] = "delay";
                    arrayOfObject[3] = Integer.valueOf(m);
                    arrayOfObject[4] = "x";
                    arrayOfObject[5] = arrayOfFloat1;
                    arrayOfObject[6] = "y";
                    arrayOfObject[7] = arrayOfFloat2;
                    arrayOfObject[8] = "alpha";
                    float[] arrayOfFloat3 = new float[2];
                    arrayOfFloat3[0] = 1.0F;
                    arrayOfFloat3[1] = 0.0F;
                    arrayOfObject[9] = arrayOfFloat3;
                    arrayOfObject[10] = "scaleX";
                    float[] arrayOfFloat4 = new float[2];
                    arrayOfFloat4[0] = 0.5F;
                    arrayOfFloat4[1] = 2.0F;
                    arrayOfObject[11] = arrayOfFloat4;
                    arrayOfObject[12] = "scaleY";
                    float[] arrayOfFloat5 = new float[2];
                    arrayOfFloat5[0] = 0.5F;
                    arrayOfFloat5[1] = 2.0F;
                    arrayOfObject[13] = arrayOfFloat5;
                    arrayOfObject[14] = "onUpdate";
                    arrayOfObject[15] = this.mUpdateListener;
                    localAnimationBundle.add(Tweener.to(localTargetDrawable, 850L, arrayOfObject));
                }
            }
        }
        label395: this.mChevronAnimations.start();
    }

    private void switchToState(int paramInt, float paramFloat1, float paramFloat2)
    {
        switch (paramInt)
        {
        case 3:
        case 4:
        default:
        case 0:
        case 1:
        case 2:
        case 5:
        }
        while (true)
        {
            return;
            deactivateTargets();
            hideTargets(true, false);
            startBackgroundAnimation(0, 0.0F);
            this.mHandleDrawable.setState(TargetDrawable.STATE_INACTIVE);
            continue;
            deactivateHandle(0, 0, 1.0F, null);
            startBackgroundAnimation(0, 0.0F);
            continue;
            deactivateTargets();
            showTargets(true);
            this.mHandleDrawable.setState(TargetDrawable.STATE_ACTIVE);
            startBackgroundAnimation(200, 1.0F);
            setGrabbedState(1);
            if (AccessibilityManager.getInstance(this.mContext).isEnabled())
            {
                announceTargets();
                continue;
                doFinish();
            }
        }
    }

    private boolean trySwitchToFirstTouchState(float paramFloat1, float paramFloat2)
    {
        boolean bool = false;
        float f1 = paramFloat1 - this.mWaveCenterX;
        float f2 = paramFloat2 - this.mWaveCenterY;
        if ((this.mAlwaysTrackFinger) || (dist2(f1, f2) <= getScaledTapRadiusSquared()))
        {
            switchToState(2, paramFloat1, paramFloat2);
            moveHandleTo(f1, f2, false);
            this.mDragging = true;
            bool = true;
        }
        return bool;
    }

    private void updateChevronPositions(float paramFloat1, float paramFloat2)
    {
        ArrayList localArrayList = this.mChevronDrawables;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
        {
            TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(j);
            if (localTargetDrawable != null)
            {
                localTargetDrawable.setPositionX(paramFloat1);
                localTargetDrawable.setPositionY(paramFloat2);
            }
        }
    }

    private void updateTargetPositions(float paramFloat1, float paramFloat2)
    {
        ArrayList localArrayList = this.mTargetDrawables;
        int i = localArrayList.size();
        float f1 = (float)(-6.283185307179586D / i);
        for (int j = 0; j < i; j++)
        {
            TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(j);
            float f2 = f1 * j;
            localTargetDrawable.setPositionX(paramFloat1);
            localTargetDrawable.setPositionY(paramFloat2);
            localTargetDrawable.setX(this.mOuterRadius * (float)Math.cos(f2));
            localTargetDrawable.setY(this.mOuterRadius * (float)Math.sin(f2));
        }
    }

    private void vibrate()
    {
        if (this.mVibrator != null)
            this.mVibrator.vibrate(this.mVibrationDuration);
    }

    public int getDirectionDescriptionsResourceId()
    {
        return this.mDirectionDescriptionsResourceId;
    }

    public int getResourceIdForTarget(int paramInt)
    {
        TargetDrawable localTargetDrawable = (TargetDrawable)this.mTargetDrawables.get(paramInt);
        if (localTargetDrawable == null);
        for (int i = 0; ; i = localTargetDrawable.getResourceId())
            return i;
    }

    protected int getSuggestedMinimumHeight()
    {
        return (int)(Math.max(this.mOuterRing.getHeight(), 2.0F * this.mOuterRadius) + this.mMaxTargetHeight);
    }

    protected int getSuggestedMinimumWidth()
    {
        return (int)(Math.max(this.mOuterRing.getWidth(), 2.0F * this.mOuterRadius) + this.mMaxTargetWidth);
    }

    public int getTargetDescriptionsResourceId()
    {
        return this.mTargetDescriptionsResourceId;
    }

    public int getTargetPosition(int paramInt)
    {
        int i = 0;
        if (i < this.mTargetDrawables.size())
            if (((TargetDrawable)this.mTargetDrawables.get(i)).getResourceId() != paramInt);
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    public int getTargetResourceId()
    {
        return this.mTargetResourceId;
    }

    void invalidateGlobalRegion(TargetDrawable paramTargetDrawable)
    {
        int i = paramTargetDrawable.getWidth();
        int j = paramTargetDrawable.getHeight();
        RectF localRectF = new RectF(0.0F, 0.0F, i, j);
        localRectF.offset(paramTargetDrawable.getX() - i / 2, paramTargetDrawable.getY() - j / 2);
        Object localObject = this;
        while ((((View)localObject).getParent() != null) && ((((View)localObject).getParent() instanceof View)))
        {
            localObject = (View)((View)localObject).getParent();
            ((View)localObject).getMatrix().mapRect(localRectF);
            ((View)localObject).invalidate((int)Math.floor(localRectF.left), (int)Math.floor(localRectF.top), (int)Math.ceil(localRectF.right), (int)Math.ceil(localRectF.bottom));
        }
    }

    protected void onDraw(Canvas paramCanvas)
    {
        this.mOuterRing.draw(paramCanvas);
        int i = this.mTargetDrawables.size();
        for (int j = 0; j < i; j++)
        {
            TargetDrawable localTargetDrawable2 = (TargetDrawable)this.mTargetDrawables.get(j);
            if (localTargetDrawable2 != null)
                localTargetDrawable2.draw(paramCanvas);
        }
        int k = this.mChevronDrawables.size();
        for (int m = 0; m < k; m++)
        {
            TargetDrawable localTargetDrawable1 = (TargetDrawable)this.mChevronDrawables.get(m);
            if (localTargetDrawable1 != null)
                localTargetDrawable1.draw(paramCanvas);
        }
        this.mHandleDrawable.draw(paramCanvas);
    }

    public boolean onHoverEvent(MotionEvent paramMotionEvent)
    {
        int i;
        if (AccessibilityManager.getInstance(this.mContext).isTouchExplorationEnabled())
        {
            i = paramMotionEvent.getAction();
            switch (i)
            {
            case 8:
            default:
            case 9:
            case 7:
            case 10:
            }
        }
        while (true)
        {
            onTouchEvent(paramMotionEvent);
            paramMotionEvent.setAction(i);
            return super.onHoverEvent(paramMotionEvent);
            paramMotionEvent.setAction(0);
            continue;
            paramMotionEvent.setAction(2);
            continue;
            paramMotionEvent.setAction(1);
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        int i = paramInt3 - paramInt1;
        int j = paramInt4 - paramInt2;
        float f1 = Math.max(this.mOuterRing.getWidth(), 2.0F * this.mOuterRadius);
        float f2 = Math.max(this.mOuterRing.getHeight(), 2.0F * this.mOuterRadius);
        float f3 = this.mHorizontalInset + Math.max(i, f1 + this.mMaxTargetWidth) / 2.0F;
        float f4 = this.mVerticalInset + Math.max(j, f2 + this.mMaxTargetHeight) / 2.0F;
        if (this.mInitialLayout)
        {
            hideChevrons();
            hideTargets(false, false);
            moveHandleTo(0.0F, 0.0F, false);
            this.mInitialLayout = false;
        }
        this.mOuterRing.setPositionX(f3);
        this.mOuterRing.setPositionY(f4);
        this.mHandleDrawable.setPositionX(f3);
        this.mHandleDrawable.setPositionY(f4);
        updateTargetPositions(f3, f4);
        updateChevronPositions(f3, f4);
        this.mWaveCenterX = f3;
        this.mWaveCenterY = f4;
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = getSuggestedMinimumWidth();
        int j = getSuggestedMinimumHeight();
        int k = resolveMeasured(paramInt1, i);
        int m = resolveMeasured(paramInt2, j);
        computeInsets(k - i, m - j);
        setMeasuredDimension(k, m);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getAction();
        int j = 0;
        switch (i)
        {
        default:
            invalidate();
            if (j == 0)
                break;
        case 0:
        case 2:
        case 1:
        case 3:
        }
        for (boolean bool = true; ; bool = super.onTouchEvent(paramMotionEvent))
        {
            return bool;
            handleDown(paramMotionEvent);
            j = 1;
            break;
            handleMove(paramMotionEvent);
            j = 1;
            break;
            handleMove(paramMotionEvent);
            handleUp(paramMotionEvent);
            j = 1;
            break;
            handleMove(paramMotionEvent);
            handleCancel(paramMotionEvent);
            j = 1;
            break;
        }
    }

    public void ping()
    {
        startChevronAnimation();
    }

    public boolean replaceTargetDrawablesIfPresent(ComponentName paramComponentName, String paramString, int paramInt)
    {
        boolean bool1 = false;
        if (paramInt == 0);
        while (true)
        {
            return bool1;
            try
            {
                PackageManager localPackageManager = this.mContext.getPackageManager();
                Bundle localBundle = localPackageManager.getActivityInfo(paramComponentName, 128).metaData;
                if (localBundle != null)
                {
                    int i = localBundle.getInt(paramString);
                    if (i != 0)
                    {
                        boolean bool2 = replaceTargetDrawables(localPackageManager.getResourcesForActivity(paramComponentName), paramInt, i);
                        bool1 = bool2;
                    }
                }
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                Log.w("MultiWaveView", "Failed to swap drawable; " + paramComponentName.flattenToShortString() + " not found", localNameNotFoundException);
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                Log.w("MultiWaveView", "Failed to swap drawable from " + paramComponentName.flattenToShortString(), localNotFoundException);
            }
        }
    }

    public void reset(boolean paramBoolean)
    {
        this.mChevronAnimations.stop();
        this.mHandleAnimations.stop();
        this.mTargetAnimations.stop();
        startBackgroundAnimation(0, 0.0F);
        hideChevrons();
        hideTargets(paramBoolean, false);
        deactivateHandle(0, 0, 1.0F, null);
        Tweener.reset();
    }

    public void resumeAnimations()
    {
        this.mChevronAnimations.setSuspended(false);
        this.mTargetAnimations.setSuspended(false);
        this.mHandleAnimations.setSuspended(false);
        this.mChevronAnimations.start();
        this.mTargetAnimations.start();
        this.mHandleAnimations.start();
    }

    public void setDirectionDescriptionsResourceId(int paramInt)
    {
        this.mDirectionDescriptionsResourceId = paramInt;
        if (this.mDirectionDescriptions != null)
            this.mDirectionDescriptions.clear();
    }

    public void setEnableTarget(int paramInt, boolean paramBoolean)
    {
        for (int i = 0; ; i++)
            if (i < this.mTargetDrawables.size())
            {
                TargetDrawable localTargetDrawable = (TargetDrawable)this.mTargetDrawables.get(i);
                if (localTargetDrawable.getResourceId() == paramInt)
                    localTargetDrawable.setEnabled(paramBoolean);
            }
            else
            {
                return;
            }
    }

    public void setOnTriggerListener(OnTriggerListener paramOnTriggerListener)
    {
        this.mOnTriggerListener = paramOnTriggerListener;
    }

    public void setTargetDescriptionsResourceId(int paramInt)
    {
        this.mTargetDescriptionsResourceId = paramInt;
        if (this.mTargetDescriptions != null)
            this.mTargetDescriptions.clear();
    }

    public void setTargetResources(int paramInt)
    {
        if (this.mAnimatingTargets)
            this.mNewTargetResources = paramInt;
        while (true)
        {
            return;
            internalSetTargetResources(paramInt);
        }
    }

    public void setVibrateEnabled(boolean paramBoolean)
    {
        if ((paramBoolean) && (this.mVibrator == null));
        for (this.mVibrator = ((Vibrator)getContext().getSystemService("vibrator")); ; this.mVibrator = null)
            return;
    }

    public void suspendAnimations()
    {
        this.mChevronAnimations.setSuspended(true);
        this.mTargetAnimations.setSuspended(true);
        this.mHandleAnimations.setSuspended(true);
    }

    private class AnimationBundle extends ArrayList<Tweener>
    {
        private static final long serialVersionUID = -6319262269245852568L;
        private boolean mSuspended;

        private AnimationBundle()
        {
        }

        public void cancel()
        {
            int i = size();
            for (int j = 0; j < i; j++)
                ((Tweener)get(j)).animator.cancel();
            clear();
        }

        public void setSuspended(boolean paramBoolean)
        {
            this.mSuspended = paramBoolean;
        }

        public void start()
        {
            if (this.mSuspended);
            while (true)
            {
                return;
                int i = size();
                for (int j = 0; j < i; j++)
                    ((Tweener)get(j)).animator.start();
            }
        }

        public void stop()
        {
            int i = size();
            for (int j = 0; j < i; j++)
                ((Tweener)get(j)).animator.end();
            clear();
        }
    }

    public static abstract interface OnTriggerListener
    {
        public static final int CENTER_HANDLE = 1;
        public static final int NO_HANDLE;

        public abstract void onFinishFinalAnimation();

        public abstract void onGrabbed(View paramView, int paramInt);

        public abstract void onGrabbedStateChange(View paramView, int paramInt);

        public abstract void onReleased(View paramView, int paramInt);

        public abstract void onTrigger(View paramView, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.multiwaveview.MultiWaveView
 * JD-Core Version:        0.6.2
 */