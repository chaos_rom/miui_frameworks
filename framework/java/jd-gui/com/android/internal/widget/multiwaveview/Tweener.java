package com.android.internal.widget.multiwaveview;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

class Tweener
{
    private static final boolean DEBUG = false;
    private static final String TAG = "Tweener";
    private static Animator.AnimatorListener mCleanupListener = new AnimatorListenerAdapter()
    {
        public void onAnimationCancel(Animator paramAnonymousAnimator)
        {
            Tweener.remove(paramAnonymousAnimator);
        }

        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            Tweener.remove(paramAnonymousAnimator);
        }
    };
    private static HashMap<Object, Tweener> sTweens = new HashMap();
    ObjectAnimator animator;

    public Tweener(ObjectAnimator paramObjectAnimator)
    {
        this.animator = paramObjectAnimator;
    }

    private static void remove(Animator paramAnimator)
    {
        Iterator localIterator = sTweens.entrySet().iterator();
        while (localIterator.hasNext())
            if (((Tweener)((Map.Entry)localIterator.next()).getValue()).animator == paramAnimator)
                localIterator.remove();
    }

    private static void replace(ArrayList<PropertyValuesHolder> paramArrayList, Object[] paramArrayOfObject)
    {
        int i = paramArrayOfObject.length;
        int j = 0;
        if (j < i)
        {
            Object localObject = paramArrayOfObject[j];
            Tweener localTweener = (Tweener)sTweens.get(localObject);
            if (localTweener != null)
            {
                localTweener.animator.cancel();
                if (paramArrayList == null)
                    break label73;
                localTweener.animator.setValues((PropertyValuesHolder[])paramArrayList.toArray(new PropertyValuesHolder[paramArrayList.size()]));
            }
            while (true)
            {
                j++;
                break;
                label73: sTweens.remove(localTweener);
            }
        }
    }

    public static void reset()
    {
        sTweens.clear();
    }

    public static Tweener to(Object paramObject, long paramLong, Object[] paramArrayOfObject)
    {
        long l = 0L;
        ValueAnimator.AnimatorUpdateListener localAnimatorUpdateListener = null;
        Animator.AnimatorListener localAnimatorListener = null;
        TimeInterpolator localTimeInterpolator = null;
        ArrayList localArrayList = new ArrayList(paramArrayOfObject.length / 2);
        int i = 0;
        if (i < paramArrayOfObject.length)
        {
            if (!(paramArrayOfObject[i] instanceof String))
                throw new IllegalArgumentException("Key must be a string: " + paramArrayOfObject[i]);
            String str = (String)paramArrayOfObject[i];
            Object localObject = paramArrayOfObject[(i + 1)];
            if ("simultaneousTween".equals(str));
            while (true)
            {
                i += 2;
                break;
                if ("ease".equals(str))
                    localTimeInterpolator = (TimeInterpolator)localObject;
                else if (("onUpdate".equals(str)) || ("onUpdateListener".equals(str)))
                    localAnimatorUpdateListener = (ValueAnimator.AnimatorUpdateListener)localObject;
                else if (("onComplete".equals(str)) || ("onCompleteListener".equals(str)))
                    localAnimatorListener = (Animator.AnimatorListener)localObject;
                else if ("delay".equals(str))
                    l = ((Number)localObject).longValue();
                else if (!"syncWith".equals(str))
                    if ((localObject instanceof float[]))
                    {
                        float[] arrayOfFloat2 = new float[2];
                        arrayOfFloat2[0] = ((float[])(float[])localObject)[0];
                        arrayOfFloat2[1] = ((float[])(float[])localObject)[1];
                        localArrayList.add(PropertyValuesHolder.ofFloat(str, arrayOfFloat2));
                    }
                    else if ((localObject instanceof int[]))
                    {
                        int[] arrayOfInt = new int[2];
                        arrayOfInt[0] = ((int[])(int[])localObject)[0];
                        arrayOfInt[1] = ((int[])(int[])localObject)[1];
                        localArrayList.add(PropertyValuesHolder.ofInt(str, arrayOfInt));
                    }
                    else
                    {
                        if (!(localObject instanceof Number))
                            break label380;
                        float f = ((Number)localObject).floatValue();
                        float[] arrayOfFloat1 = new float[1];
                        arrayOfFloat1[0] = f;
                        localArrayList.add(PropertyValuesHolder.ofFloat(str, arrayOfFloat1));
                    }
            }
            label380: throw new IllegalArgumentException("Bad argument for key \"" + str + "\" with value " + localObject.getClass());
        }
        Tweener localTweener = (Tweener)sTweens.get(paramObject);
        ObjectAnimator localObjectAnimator;
        if (localTweener == null)
        {
            localObjectAnimator = ObjectAnimator.ofPropertyValuesHolder(paramObject, (PropertyValuesHolder[])localArrayList.toArray(new PropertyValuesHolder[localArrayList.size()]));
            localTweener = new Tweener(localObjectAnimator);
            sTweens.put(paramObject, localTweener);
        }
        while (true)
        {
            if (localTimeInterpolator != null)
                localObjectAnimator.setInterpolator(localTimeInterpolator);
            localObjectAnimator.setStartDelay(l);
            localObjectAnimator.setDuration(paramLong);
            if (localAnimatorUpdateListener != null)
            {
                localObjectAnimator.removeAllUpdateListeners();
                localObjectAnimator.addUpdateListener(localAnimatorUpdateListener);
            }
            if (localAnimatorListener != null)
            {
                localObjectAnimator.removeAllListeners();
                localObjectAnimator.addListener(localAnimatorListener);
            }
            localObjectAnimator.addListener(mCleanupListener);
            return localTweener;
            localObjectAnimator = ((Tweener)sTweens.get(paramObject)).animator;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = paramObject;
            replace(localArrayList, arrayOfObject);
        }
    }

    Tweener from(Object paramObject, long paramLong, Object[] paramArrayOfObject)
    {
        return to(paramObject, paramLong, paramArrayOfObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.multiwaveview.Tweener
 * JD-Core Version:        0.6.2
 */