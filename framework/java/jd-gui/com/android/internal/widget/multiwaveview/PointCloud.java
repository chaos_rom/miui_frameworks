package com.android.internal.widget.multiwaveview;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.FloatMath;
import android.util.Log;
import java.util.ArrayList;

public class PointCloud
{
    private static final int INNER_POINTS = 8;
    private static final float MAX_POINT_SIZE = 4.0F;
    private static final float MIN_POINT_SIZE = 2.0F;
    private static final float PI = 3.141593F;
    private static final String TAG = "PointCloud";
    GlowManager glowManager = new GlowManager();
    private float mCenterX;
    private float mCenterY;
    private Drawable mDrawable;
    private float mOuterRadius;
    private Paint mPaint = new Paint();
    private ArrayList<Point> mPointCloud = new ArrayList();
    private float mScale = 1.0F;
    WaveManager waveManager = new WaveManager();

    public PointCloud(Drawable paramDrawable)
    {
        this.mPaint.setFilterBitmap(true);
        this.mPaint.setColor(Color.rgb(255, 255, 255));
        this.mPaint.setAntiAlias(true);
        this.mPaint.setDither(true);
        this.mDrawable = paramDrawable;
        if (this.mDrawable != null)
            paramDrawable.setBounds(0, 0, paramDrawable.getIntrinsicWidth(), paramDrawable.getIntrinsicHeight());
    }

    private static float hypot(float paramFloat1, float paramFloat2)
    {
        return FloatMath.sqrt(paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2);
    }

    private float interp(float paramFloat1, float paramFloat2, float paramFloat3)
    {
        return paramFloat1 + paramFloat3 * (paramFloat2 - paramFloat1);
    }

    private static float max(float paramFloat1, float paramFloat2)
    {
        if (paramFloat1 > paramFloat2);
        while (true)
        {
            return paramFloat1;
            paramFloat1 = paramFloat2;
        }
    }

    public void draw(Canvas paramCanvas)
    {
        ArrayList localArrayList = this.mPointCloud;
        paramCanvas.save(1);
        paramCanvas.scale(this.mScale, this.mScale, this.mCenterX, this.mCenterY);
        int i = 0;
        if (i < localArrayList.size())
        {
            Point localPoint = (Point)localArrayList.get(i);
            float f1 = interp(4.0F, 2.0F, localPoint.radius / this.mOuterRadius);
            float f2 = localPoint.x + this.mCenterX;
            float f3 = localPoint.y + this.mCenterY;
            int j = getAlphaForPoint(localPoint);
            if (j == 0);
            while (true)
            {
                i++;
                break;
                if (this.mDrawable != null)
                {
                    paramCanvas.save(1);
                    float f4 = 0.5F * this.mDrawable.getIntrinsicWidth();
                    float f5 = 0.5F * this.mDrawable.getIntrinsicHeight();
                    float f6 = f1 / 4.0F;
                    paramCanvas.scale(f6, f6, f2, f3);
                    paramCanvas.translate(f2 - f4, f3 - f5);
                    this.mDrawable.setAlpha(j);
                    this.mDrawable.draw(paramCanvas);
                    paramCanvas.restore();
                }
                else
                {
                    this.mPaint.setAlpha(j);
                    paramCanvas.drawCircle(f2, f3, f1, this.mPaint);
                }
            }
        }
        paramCanvas.restore();
    }

    public int getAlphaForPoint(Point paramPoint)
    {
        float f1 = hypot(this.glowManager.x - paramPoint.x, this.glowManager.y - paramPoint.y);
        float f2 = 0.0F;
        if (f1 < this.glowManager.radius)
        {
            float f6 = FloatMath.cos(0.7853982F * f1 / this.glowManager.radius);
            f2 = this.glowManager.alpha * max(0.0F, (float)Math.pow(f6, 10.0D));
        }
        float f3 = hypot(paramPoint.x, paramPoint.y) - this.waveManager.radius;
        float f4 = 0.0F;
        if ((f3 < 0.5F * this.waveManager.width) && (f3 < 0.0F))
        {
            float f5 = FloatMath.cos(0.7853982F * f3 / this.waveManager.width);
            f4 = this.waveManager.alpha * max(0.0F, (float)Math.pow(f5, 20.0D));
        }
        return (int)(255.0F * max(f2, f4));
    }

    public float getScale()
    {
        return this.mScale;
    }

    public void makePointCloud(float paramFloat1, float paramFloat2)
    {
        if (paramFloat1 == 0.0F)
            Log.w("PointCloud", "Must specify an inner radius");
        while (true)
        {
            return;
            this.mOuterRadius = paramFloat2;
            this.mPointCloud.clear();
            float f1 = paramFloat2 - paramFloat1;
            float f2 = 6.283186F * paramFloat1 / 8.0F;
            int i = Math.round(f1 / f2);
            float f3 = f1 / i;
            float f4 = paramFloat1;
            int j = 0;
            while (j <= i)
            {
                int k = (int)(6.283186F * f4 / f2);
                float f5 = 1.570796F;
                float f6 = 6.283186F / k;
                for (int m = 0; m < k; m++)
                {
                    float f7 = f4 * FloatMath.cos(f5);
                    float f8 = f4 * FloatMath.sin(f5);
                    f5 += f6;
                    ArrayList localArrayList = this.mPointCloud;
                    Point localPoint = new Point(f7, f8, f4);
                    localArrayList.add(localPoint);
                }
                j++;
                f4 += f3;
            }
        }
    }

    public void setCenter(float paramFloat1, float paramFloat2)
    {
        this.mCenterX = paramFloat1;
        this.mCenterY = paramFloat2;
    }

    public void setScale(float paramFloat)
    {
        this.mScale = paramFloat;
    }

    class Point
    {
        float radius;
        float x;
        float y;

        public Point(float paramFloat1, float paramFloat2, float arg4)
        {
            this.x = paramFloat1;
            this.y = paramFloat2;
            Object localObject;
            this.radius = localObject;
        }
    }

    public class GlowManager
    {
        private float alpha = 0.0F;
        private float radius = 0.0F;
        private float x;
        private float y;

        public GlowManager()
        {
        }

        public float getAlpha()
        {
            return this.alpha;
        }

        public float getRadius()
        {
            return this.radius;
        }

        public float getX()
        {
            return this.x;
        }

        public float getY()
        {
            return this.y;
        }

        public void setAlpha(float paramFloat)
        {
            this.alpha = paramFloat;
        }

        public void setRadius(float paramFloat)
        {
            this.radius = paramFloat;
        }

        public void setX(float paramFloat)
        {
            this.x = paramFloat;
        }

        public void setY(float paramFloat)
        {
            this.y = paramFloat;
        }
    }

    public class WaveManager
    {
        private float alpha = 0.0F;
        private float radius = 50.0F;
        private float width = 200.0F;

        public WaveManager()
        {
        }

        public float getAlpha()
        {
            return this.alpha;
        }

        public float getRadius()
        {
            return this.radius;
        }

        public void setAlpha(float paramFloat)
        {
            this.alpha = paramFloat;
        }

        public void setRadius(float paramFloat)
        {
            this.radius = paramFloat;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.multiwaveview.PointCloud
 * JD-Core Version:        0.6.2
 */