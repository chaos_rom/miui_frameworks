package com.android.internal.widget.multiwaveview;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import java.util.ArrayList;
import java.util.Iterator;
import miui.R.styleable;

public class MiuiInCallAnswerWidgetHorizontal extends GlowPadView
{
    private static final int HIDE_ANIMATION_DELAY = 200;
    private static final int HIDE_ANIMATION_DURATION = 200;
    private static final int HIDE_TARGET_LIGHT_DELAY = 20;
    private static final int RETURN_TO_HOME_DELAY = 1200;
    private static final int RETURN_TO_HOME_DURATION = 300;
    private static final int SHOW_ANIMATION_DELAY = 200;
    private static final int SHOW_ANIMATION_DURATION = 200;
    private static final int SHOW_HINT_ANIMATION_DELAY = 200;
    private static final int SHOW_HINT_ANIMATION_DURATION = 1500;
    private static final int SHOW_HINT_DELAY = 400;
    private static final int SHOW_HINT_DURATION = 300;
    private static final int SHOW_OUTERRING_DELAY = 200;
    private static final int SHOW_OUTERRING_DURATION = 300;
    private static final int SHOW_TARGET_LIGHT_DELAY = 20;
    private static final int STATE_ANSWER = 7;
    public static final int[] STATE_ANSWER_HANDLE;
    private static final int STATE_DECLINE = 6;
    public static final int[] STATE_DECLINE_HANDLE;
    private static final int STATE_FINISH = 5;
    private static final int STATE_FIRST_TOUCH = 2;
    public static final int[][] STATE_HINT_ANIM = arrayOfInt;
    private static final int STATE_IDLE = 0;
    private static final int STATE_SNAP = 4;
    private static final int STATE_TRACKING = 3;
    private TargetDrawable mCurrentTargetLight;
    private GlowPadView.AnimationBundle mHandleAnimation = new GlowPadView.AnimationBundle(this);
    private ValueAnimator mHintAnimation;
    private GlowPadView.AnimationBundle mOuterRingAnimation = new GlowPadView.AnimationBundle(this);
    private Animator.AnimatorListener mOuterRingListener = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            MiuiInCallAnswerWidgetHorizontal.access$002(MiuiInCallAnswerWidgetHorizontal.this, false);
            MiuiInCallAnswerWidgetHorizontal.this.showTargets(true);
        }
    };
    private TargetDrawable mRightHintAnimTargetDrawable;
    private boolean mShowingOuterRing = false;
    private ArrayList<Tweener> mTargetLightAnimations = new ArrayList();
    private ArrayList<TargetDrawable> mTargetLightDrawables = new ArrayList();

    static
    {
        int[] arrayOfInt1 = new int[3];
        arrayOfInt1[0] = 16842910;
        arrayOfInt1[1] = -16842914;
        arrayOfInt1[2] = 16842908;
        STATE_DECLINE_HANDLE = arrayOfInt1;
        int[] arrayOfInt2 = new int[3];
        arrayOfInt2[0] = 16842910;
        arrayOfInt2[1] = 16842914;
        arrayOfInt2[2] = 16842908;
        STATE_ANSWER_HANDLE = arrayOfInt2;
        int[][] arrayOfInt = new int[7][];
        int[] arrayOfInt3 = new int[3];
        arrayOfInt3[0] = 16842910;
        arrayOfInt3[1] = -16842914;
        arrayOfInt3[2] = -16842908;
        arrayOfInt[0] = arrayOfInt3;
        int[] arrayOfInt4 = new int[3];
        arrayOfInt4[0] = -16842910;
        arrayOfInt4[1] = 16842914;
        arrayOfInt4[2] = -16842908;
        arrayOfInt[1] = arrayOfInt4;
        int[] arrayOfInt5 = new int[3];
        arrayOfInt5[0] = -16842910;
        arrayOfInt5[1] = -16842914;
        arrayOfInt5[2] = 16842908;
        arrayOfInt[2] = arrayOfInt5;
        int[] arrayOfInt6 = new int[3];
        arrayOfInt6[0] = 16842910;
        arrayOfInt6[1] = 16842914;
        arrayOfInt6[2] = -16842908;
        arrayOfInt[3] = arrayOfInt6;
        int[] arrayOfInt7 = new int[3];
        arrayOfInt7[0] = -16842910;
        arrayOfInt7[1] = 16842914;
        arrayOfInt7[2] = 16842908;
        arrayOfInt[4] = arrayOfInt7;
        int[] arrayOfInt8 = new int[3];
        arrayOfInt8[0] = 16842910;
        arrayOfInt8[1] = 16842914;
        arrayOfInt8[2] = 16842908;
        arrayOfInt[5] = arrayOfInt8;
        int[] arrayOfInt9 = new int[3];
        arrayOfInt9[0] = -16842910;
        arrayOfInt9[1] = -16842914;
        arrayOfInt9[2] = -16842908;
        arrayOfInt[6] = arrayOfInt9;
    }

    public MiuiInCallAnswerWidgetHorizontal(Context paramContext)
    {
        this(paramContext, null);
    }

    public MiuiInCallAnswerWidgetHorizontal(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MiuiInCallAnswerWidgetHorizontal);
        TypedValue localTypedValue = new TypedValue();
        if (localTypedArray.getValue(0, localTypedValue))
            internalSetTargetLightResources(localTypedValue.resourceId);
        this.mRightHintAnimTargetDrawable = new TargetDrawable(paramContext.getResources(), localTypedArray.peekValue(1).resourceId);
        setGlowRadius(getHandleDrawable().getWidth() / 2);
    }

    private float dist2(float paramFloat1, float paramFloat2)
    {
        return paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2;
    }

    private void hideRightHintAnim()
    {
        this.mRightHintAnimTargetDrawable.setAlpha(0.0F);
        if (this.mHintAnimation != null)
            this.mHintAnimation.end();
    }

    private void hideTargetLight()
    {
        Iterator localIterator = this.mTargetLightDrawables.iterator();
        while (localIterator.hasNext())
            ((TargetDrawable)localIterator.next()).setAlpha(0.0F);
    }

    private void internalSetTargetLightResources(int paramInt)
    {
        Resources localResources = getContext().getResources();
        TypedArray localTypedArray = localResources.obtainTypedArray(paramInt);
        int i = localTypedArray.length();
        ArrayList localArrayList = new ArrayList(i);
        int j = 0;
        if (j < i)
        {
            TypedValue localTypedValue = localTypedArray.peekValue(j);
            if (localTypedValue != null);
            for (int k = localTypedValue.resourceId; ; k = 0)
            {
                localArrayList.add(new TargetDrawable(localResources, k));
                j++;
                break;
            }
        }
        localTypedArray.recycle();
        this.mTargetLightDrawables = localArrayList;
        updateTargetLightPositions();
    }

    private void moveHandleTo(float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
        getHandleDrawable().setX(paramFloat1);
        getHandleDrawable().setY(paramFloat2);
    }

    private void showOuterRing()
    {
        final TargetDrawable localTargetDrawable = getOuterRing();
        this.mShowingOuterRing = true;
        this.mOuterRingAnimation.cancel();
        localTargetDrawable.setScaleX(0.0F);
        localTargetDrawable.setScaleY(0.0F);
        GlowPadView.AnimationBundle localAnimationBundle = this.mOuterRingAnimation;
        Object[] arrayOfObject = new Object[12];
        arrayOfObject[0] = "alpha";
        arrayOfObject[1] = Float.valueOf(1.0F);
        arrayOfObject[2] = "scaleX";
        arrayOfObject[3] = Float.valueOf(1.0F);
        arrayOfObject[4] = "scaleY";
        arrayOfObject[5] = Float.valueOf(1.0F);
        arrayOfObject[6] = "delay";
        arrayOfObject[7] = Integer.valueOf(200);
        arrayOfObject[8] = "onUpdate";
        arrayOfObject[9] = new ValueAnimator.AnimatorUpdateListener()
        {
            public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
            {
                if (localTargetDrawable.getAlpha() < 0.1F)
                    localTargetDrawable.setAlpha(1.0F);
                MiuiInCallAnswerWidgetHorizontal.this.invalidate();
            }
        };
        arrayOfObject[10] = "onComplete";
        arrayOfObject[11] = this.mOuterRingListener;
        localAnimationBundle.add(Tweener.to(localTargetDrawable, 300L, arrayOfObject));
        this.mOuterRingAnimation.start();
    }

    private void showRightHintAnim(int paramInt)
    {
        if (paramInt < STATE_HINT_ANIM.length)
        {
            this.mRightHintAnimTargetDrawable.setState(STATE_HINT_ANIM[paramInt]);
            invalidate();
        }
    }

    private void startRightHintAnim()
    {
        TargetDrawable localTargetDrawable = this.mRightHintAnimTargetDrawable;
        float[] arrayOfFloat1 = new float[2];
        arrayOfFloat1[0] = 0.0F;
        arrayOfFloat1[1] = 1.0F;
        ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(localTargetDrawable, "alpha", arrayOfFloat1);
        localObjectAnimator.setDuration(300L);
        localObjectAnimator.setStartDelay(400L);
        localObjectAnimator.setInterpolator(null);
        localObjectAnimator.start();
        float[] arrayOfFloat2 = new float[2];
        arrayOfFloat2[0] = 0.0F;
        arrayOfFloat2[1] = 1.0F;
        this.mHintAnimation = ValueAnimator.ofFloat(arrayOfFloat2);
        this.mHintAnimation.setDuration(1500L);
        this.mHintAnimation.setInterpolator(null);
        this.mHintAnimation.setStartDelay(200L);
        this.mHintAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
            {
                if (MiuiInCallAnswerWidgetHorizontal.this.getVisibility() != 0)
                    if (MiuiInCallAnswerWidgetHorizontal.this.mHintAnimation != null)
                    {
                        ValueAnimator localValueAnimator = MiuiInCallAnswerWidgetHorizontal.this.mHintAnimation;
                        MiuiInCallAnswerWidgetHorizontal.access$102(MiuiInCallAnswerWidgetHorizontal.this, null);
                        localValueAnimator.end();
                    }
                while (true)
                {
                    return;
                    int i = (int)(100.0F * MiuiInCallAnswerWidgetHorizontal.this.mHintAnimation.getAnimatedFraction() / 14.0F);
                    MiuiInCallAnswerWidgetHorizontal.this.showRightHintAnim(i);
                }
            }
        });
        this.mHintAnimation.setRepeatCount(-1);
        this.mHintAnimation.setRepeatMode(1);
        this.mHintAnimation.start();
    }

    private void stopTargetLightAnimation()
    {
        Iterator localIterator = this.mTargetLightAnimations.iterator();
        while (localIterator.hasNext())
            ((Tweener)localIterator.next()).animator.end();
        this.mTargetLightAnimations.clear();
    }

    private void updateRightHintAnimPositions()
    {
        this.mRightHintAnimTargetDrawable.setX(getWaveCenterX() + 0.65F * getOuterRadius());
        this.mRightHintAnimTargetDrawable.setY(getWaveCenterY());
    }

    private void updateTargetLightPositions()
    {
        Iterator localIterator = this.mTargetLightDrawables.iterator();
        while (localIterator.hasNext())
        {
            TargetDrawable localTargetDrawable = (TargetDrawable)localIterator.next();
            localTargetDrawable.setX(getWaveCenterX());
            localTargetDrawable.setY(Math.max(getWaveCenterX(), getWaveCenterY()));
        }
    }

    void doFinish()
    {
        int i = getActiveTarget();
        if (i != -1);
        for (int j = 1; ; j = 0)
        {
            this.mHandleAnimation.cancel();
            if (j == 0)
                break;
            ((TargetDrawable)getTargetDrawables().get(i)).setState(TargetDrawable.STATE_ACTIVE);
            hideTargets(false, false);
            hideRightHintAnim();
            callDispatchTriggerEvent(i);
            GlowPadView.AnimationBundle localAnimationBundle2 = this.mHandleAnimation;
            TargetDrawable localTargetDrawable2 = getHandleDrawable();
            Object[] arrayOfObject2 = new Object[14];
            arrayOfObject2[0] = "ease";
            arrayOfObject2[1] = Ease.Quart.easeOut;
            arrayOfObject2[2] = "delay";
            arrayOfObject2[3] = Integer.valueOf(1200);
            arrayOfObject2[4] = "alpha";
            arrayOfObject2[5] = Float.valueOf(1.0F);
            arrayOfObject2[6] = "x";
            arrayOfObject2[7] = Integer.valueOf(0);
            arrayOfObject2[8] = "y";
            arrayOfObject2[9] = Integer.valueOf(0);
            arrayOfObject2[10] = "onUpdate";
            arrayOfObject2[11] = getUpdateListener();
            arrayOfObject2[12] = "onComplete";
            arrayOfObject2[13] = getResetListener();
            localAnimationBundle2.add(Tweener.to(localTargetDrawable2, 0L, arrayOfObject2));
            this.mHandleAnimation.start();
            setDragging(false);
            callSetGrabbedState(0);
            return;
        }
        GlowPadView.AnimationBundle localAnimationBundle1 = this.mHandleAnimation;
        TargetDrawable localTargetDrawable1 = getHandleDrawable();
        Object[] arrayOfObject1 = new Object[14];
        arrayOfObject1[0] = "ease";
        arrayOfObject1[1] = Ease.Quart.easeOut;
        arrayOfObject1[2] = "delay";
        arrayOfObject1[3] = Integer.valueOf(0);
        arrayOfObject1[4] = "alpha";
        arrayOfObject1[5] = Float.valueOf(1.0F);
        arrayOfObject1[6] = "x";
        arrayOfObject1[7] = Integer.valueOf(0);
        arrayOfObject1[8] = "y";
        arrayOfObject1[9] = Integer.valueOf(0);
        arrayOfObject1[10] = "onUpdate";
        arrayOfObject1[11] = getUpdateListener();
        arrayOfObject1[12] = "onComplete";
        if (getDragging());
        for (Animator.AnimatorListener localAnimatorListener = getResetListenerWithPing(); ; localAnimatorListener = getResetListener())
        {
            arrayOfObject1[13] = localAnimatorListener;
            localAnimationBundle1.add(Tweener.to(localTargetDrawable1, 300L, arrayOfObject1));
            break;
        }
    }

    void handleMove(MotionEvent paramMotionEvent)
    {
        if (!getDragging());
        while (true)
        {
            return;
            int i = -1;
            int j = paramMotionEvent.getHistorySize();
            ArrayList localArrayList1 = getTargetDrawables();
            int k = localArrayList1.size();
            float f1 = 0.0F;
            float f2 = 0.0F;
            int m = 0;
            if (m < j + 1)
            {
                float f3;
                label60: float f4;
                label74: float f5;
                float f6;
                float f8;
                label126: float f9;
                float f10;
                double d1;
                int n;
                label166: label196: label226: TargetDrawable localTargetDrawable2;
                if (m < j)
                {
                    f3 = paramMotionEvent.getHistoricalX(m);
                    if (m >= j)
                        break label344;
                    f4 = paramMotionEvent.getHistoricalY(m);
                    f5 = f3 - getWaveCenterX();
                    f6 = f4 - getWaveCenterY();
                    float f7 = (float)Math.sqrt(dist2(f5, f6));
                    if (f7 <= getOuterRadius())
                        break label353;
                    f8 = getOuterRadius() / f7;
                    f9 = f5 * f8;
                    f10 = f6 * f8;
                    d1 = Math.atan2(-f6, f5);
                    if (getTargetDrawables().size() != 1)
                        break label359;
                    n = 1;
                    if (n == 0)
                        break label365;
                    if (f7 > getOuterRadius() - getSnapMargin())
                    {
                        i = 0;
                        f1 = f9;
                        f2 = f10;
                    }
                    moveHandleTo(f1, f2, false);
                    if (i == -1)
                        break label653;
                    if (i != 2)
                        break label543;
                    switchToState(6, f1, f2);
                    localTargetDrawable2 = (TargetDrawable)this.mTargetLightDrawables.get(i);
                    if (this.mCurrentTargetLight != null)
                        break label572;
                    this.mCurrentTargetLight = localTargetDrawable2;
                    label252: if (this.mTargetLightAnimations.size() > 0)
                        stopTargetLightAnimation();
                    ArrayList localArrayList4 = this.mTargetLightAnimations;
                    TargetDrawable localTargetDrawable4 = this.mCurrentTargetLight;
                    Object[] arrayOfObject3 = new Object[4];
                    arrayOfObject3[0] = "delay";
                    arrayOfObject3[1] = Integer.valueOf(0);
                    arrayOfObject3[2] = "alpha";
                    arrayOfObject3[3] = Float.valueOf(1.0F);
                    localArrayList4.add(Tweener.to(localTargetDrawable4, 20L, arrayOfObject3));
                }
                while (true)
                {
                    m++;
                    break;
                    f3 = paramMotionEvent.getX();
                    break label60;
                    label344: f4 = paramMotionEvent.getY();
                    break label74;
                    label353: f8 = 1.0F;
                    break label126;
                    label359: n = 0;
                    break label166;
                    label365: float f11 = getOuterRadius() - getSnapMargin();
                    float f12 = f11 * f11;
                    int i1 = 0;
                    if (i1 < k)
                    {
                        TargetDrawable localTargetDrawable5 = (TargetDrawable)localArrayList1.get(i1);
                        double d2 = 3.141592653589793D * (2.0D * (i1 - 0.5D)) / k;
                        double d3 = 3.141592653589793D * (2.0D * (0.5D + i1)) / k;
                        if (localTargetDrawable5.isEnabled())
                            if (((d1 <= d2) || (d1 > d3)) && ((6.283185307179586D + d1 <= d2) || (6.283185307179586D + d1 > d3)))
                                break label526;
                        label526: for (int i2 = 1; ; i2 = 0)
                        {
                            if ((i2 != 0) && (dist2(f5, f6) > f12))
                                i = i1;
                            i1++;
                            break;
                        }
                    }
                    f1 = f9;
                    f2 = f10;
                    break label196;
                    label543: if (i == 0)
                    {
                        switchToState(7, f1, f2);
                        break label226;
                    }
                    switchToState(4, f1, f2);
                    break label226;
                    label572: if (this.mCurrentTargetLight == localTargetDrawable2)
                        break label252;
                    ArrayList localArrayList3 = this.mTargetLightAnimations;
                    TargetDrawable localTargetDrawable3 = this.mCurrentTargetLight;
                    Object[] arrayOfObject2 = new Object[4];
                    arrayOfObject2[0] = "delay";
                    arrayOfObject2[1] = Integer.valueOf(0);
                    arrayOfObject2[2] = "alpha";
                    arrayOfObject2[3] = Float.valueOf(0.0F);
                    localArrayList3.add(Tweener.to(localTargetDrawable3, 20L, arrayOfObject2));
                    this.mCurrentTargetLight = localTargetDrawable2;
                    break label252;
                    label653: if (this.mTargetLightAnimations.size() > 0)
                        stopTargetLightAnimation();
                    if (this.mCurrentTargetLight != null)
                    {
                        ArrayList localArrayList2 = this.mTargetLightAnimations;
                        TargetDrawable localTargetDrawable1 = this.mCurrentTargetLight;
                        Object[] arrayOfObject1 = new Object[4];
                        arrayOfObject1[0] = "delay";
                        arrayOfObject1[1] = Integer.valueOf(0);
                        arrayOfObject1[2] = "alpha";
                        arrayOfObject1[3] = Float.valueOf(0.0F);
                        localArrayList2.add(Tweener.to(localTargetDrawable1, 20L, arrayOfObject1));
                    }
                    switchToState(3, f1, f2);
                    getHandleDrawable().setAlpha(1.0F);
                }
            }
            if ((getActiveTarget() != i) && (i != -1))
            {
                callVibrate();
                if (AccessibilityManager.getInstance(this.mContext).isEnabled())
                    announceForAccessibility(callGetTargetDescription(i));
            }
            setActiveTarget(i);
        }
    }

    void hideTargets(boolean paramBoolean1, boolean paramBoolean2)
    {
        GlowPadView.AnimationBundle localAnimationBundle = getTargetAnimations();
        ArrayList localArrayList = getTargetDrawables();
        localAnimationBundle.cancel();
        setAnimatingTargets(paramBoolean1);
        int i;
        if (paramBoolean1)
        {
            i = 200;
            if (!paramBoolean1)
                break label178;
        }
        label178: for (int j = 200; ; j = 0)
        {
            int k = localArrayList.size();
            TimeInterpolator localTimeInterpolator = Ease.Cubic.easeOut;
            for (int m = 0; m < k; m++)
            {
                TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(m);
                localTargetDrawable.setState(TargetDrawable.STATE_INACTIVE);
                long l = i;
                Object[] arrayOfObject = new Object[8];
                arrayOfObject[0] = "ease";
                arrayOfObject[1] = localTimeInterpolator;
                arrayOfObject[2] = "alpha";
                arrayOfObject[3] = Float.valueOf(0.0F);
                arrayOfObject[4] = "delay";
                arrayOfObject[5] = Integer.valueOf(j);
                arrayOfObject[6] = "onUpdate";
                arrayOfObject[7] = getUpdateListener();
                localAnimationBundle.add(Tweener.to(localTargetDrawable, l, arrayOfObject));
            }
            i = 0;
            break;
        }
        localAnimationBundle.start();
        getOuterRing().setAlpha(0.0F);
        if (this.mTargetLightAnimations.size() > 0)
            stopTargetLightAnimation();
        hideTargetLight();
    }

    protected void onDraw(Canvas paramCanvas)
    {
        getOuterRing().draw(paramCanvas);
        Iterator localIterator1 = this.mTargetLightDrawables.iterator();
        while (localIterator1.hasNext())
        {
            TargetDrawable localTargetDrawable2 = (TargetDrawable)localIterator1.next();
            if (localTargetDrawable2 != null)
                localTargetDrawable2.draw(paramCanvas);
        }
        Iterator localIterator2 = getTargetDrawables().iterator();
        while (localIterator2.hasNext())
        {
            TargetDrawable localTargetDrawable1 = (TargetDrawable)localIterator2.next();
            if (localTargetDrawable1 != null)
                localTargetDrawable1.draw(paramCanvas);
        }
        this.mRightHintAnimTargetDrawable.draw(paramCanvas);
        getHandleDrawable().draw(paramCanvas);
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        updateTargetLightPositions();
        updateRightHintAnimPositions();
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = 1;
        if (this.mShowingOuterRing);
        while (true)
        {
            return i;
            if (paramMotionEvent.getPointerCount() > i)
            {
                if (getDragging())
                {
                    setActiveTarget(-1);
                    switchToState(5, paramMotionEvent.getX(), paramMotionEvent.getY());
                }
            }
            else
                boolean bool = super.onTouchEvent(paramMotionEvent);
        }
    }

    public void reset(boolean paramBoolean)
    {
        super.reset(paramBoolean);
        hideRightHintAnim();
        this.mHandleAnimation.stop();
        getTargetAnimations().stop();
        showOuterRing();
        startRightHintAnim();
    }

    void showTargets(boolean paramBoolean)
    {
        int i = 200;
        GlowPadView.AnimationBundle localAnimationBundle = getTargetAnimations();
        ArrayList localArrayList = getTargetDrawables();
        localAnimationBundle.stop();
        if (this.mTargetLightAnimations.size() > 0)
            stopTargetLightAnimation();
        setAnimatingTargets(paramBoolean);
        int j;
        if (paramBoolean)
        {
            j = i;
            if (!paramBoolean)
                break label218;
        }
        while (true)
        {
            int k = localArrayList.size();
            for (int m = 0; m < k; m++)
            {
                TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(m);
                localTargetDrawable.setState(TargetDrawable.STATE_INACTIVE);
                long l = i;
                Object[] arrayOfObject = new Object[12];
                arrayOfObject[0] = "ease";
                arrayOfObject[1] = Ease.Cubic.easeOut;
                arrayOfObject[2] = "alpha";
                arrayOfObject[3] = Float.valueOf(1.0F);
                arrayOfObject[4] = "scaleX";
                arrayOfObject[5] = Float.valueOf(1.0F);
                arrayOfObject[6] = "scaleY";
                arrayOfObject[7] = Float.valueOf(1.0F);
                arrayOfObject[8] = "delay";
                arrayOfObject[9] = Integer.valueOf(j);
                arrayOfObject[10] = "onUpdate";
                arrayOfObject[11] = getUpdateListener();
                localAnimationBundle.add(Tweener.to(localTargetDrawable, l, arrayOfObject));
            }
            j = 0;
            break;
            label218: i = 0;
        }
        localAnimationBundle.start();
        hideTargetLight();
    }

    void switchToState(int paramInt, float paramFloat1, float paramFloat2)
    {
        TargetDrawable localTargetDrawable = getHandleDrawable();
        switch (paramInt)
        {
        case 1:
        default:
        case 0:
        case 2:
        case 3:
        case 6:
        case 7:
        case 4:
        case 5:
        }
        while (true)
        {
            return;
            callDeactivateTargets();
            localTargetDrawable.setState(TargetDrawable.STATE_INACTIVE);
            localTargetDrawable.setAlpha(1.0F);
            continue;
            this.mHandleAnimation.stop();
            callDeactivateTargets();
            localTargetDrawable.setState(TargetDrawable.STATE_ACTIVE);
            callSetGrabbedState(1);
            if (AccessibilityManager.getInstance(this.mContext).isEnabled())
            {
                callAnnounceTargets();
                continue;
                localTargetDrawable.setState(TargetDrawable.STATE_ACTIVE);
                continue;
                localTargetDrawable.setState(STATE_DECLINE_HANDLE);
                continue;
                localTargetDrawable.setState(STATE_ANSWER_HANDLE);
                continue;
                localTargetDrawable.setState(TargetDrawable.STATE_ACTIVE);
                continue;
                doFinish();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.multiwaveview.MiuiInCallAnswerWidgetHorizontal
 * JD-Core Version:        0.6.2
 */