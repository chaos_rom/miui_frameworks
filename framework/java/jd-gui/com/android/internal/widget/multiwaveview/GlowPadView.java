package com.android.internal.widget.multiwaveview;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.accessibility.AccessibilityManager;
import java.util.ArrayList;

public class GlowPadView extends View
{
    private static final boolean DEBUG = false;
    private static final int HIDE_ANIMATION_DELAY = 200;
    private static final int HIDE_ANIMATION_DURATION = 200;
    private static final int INITIAL_SHOW_HANDLE_DURATION = 200;
    private static final int RETURN_TO_HOME_DELAY = 1200;
    private static final int RETURN_TO_HOME_DURATION = 200;
    private static final int REVEAL_GLOW_DELAY = 0;
    private static final int REVEAL_GLOW_DURATION = 0;
    private static final float RING_SCALE_COLLAPSED = 0.5F;
    private static final float RING_SCALE_EXPANDED = 1.0F;
    private static final int SHOW_ANIMATION_DELAY = 50;
    private static final int SHOW_ANIMATION_DURATION = 200;
    private static final float SNAP_MARGIN_DEFAULT = 20.0F;
    private static final int STATE_FINISH = 5;
    private static final int STATE_FIRST_TOUCH = 2;
    private static final int STATE_IDLE = 0;
    private static final int STATE_SNAP = 4;
    private static final int STATE_START = 1;
    private static final int STATE_TRACKING = 3;
    private static final String TAG = "GlowPadView";
    private static final float TAP_RADIUS_SCALE_ACCESSIBILITY_ENABLED = 1.3F;
    private static final float TARGET_SCALE_COLLAPSED = 0.8F;
    private static final float TARGET_SCALE_EXPANDED = 1.0F;
    private static final int WAVE_ANIMATION_DURATION = 1350;
    private int mActiveTarget = -1;
    private boolean mAlwaysTrackFinger;
    private boolean mAnimatingTargets;
    private Tweener mBackgroundAnimator;
    private ArrayList<String> mDirectionDescriptions;
    private int mDirectionDescriptionsResourceId;
    private boolean mDragging;
    private int mFeedbackCount = 3;
    private AnimationBundle mGlowAnimations = new AnimationBundle();
    private float mGlowRadius;
    private int mGrabbedState;
    private int mGravity = 48;
    private TargetDrawable mHandleDrawable;
    private int mHorizontalInset;
    private boolean mInitialLayout;
    private float mInnerRadius;
    private int mMaxTargetHeight;
    private int mMaxTargetWidth;
    private int mNewTargetResources;
    private OnTriggerListener mOnTriggerListener;
    private float mOuterRadius = 0.0F;
    private TargetDrawable mOuterRing;
    private PointCloud mPointCloud;
    private Animator.AnimatorListener mResetListener = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            GlowPadView.this.switchToState(0, GlowPadView.this.mWaveCenterX, GlowPadView.this.mWaveCenterY);
            GlowPadView.this.dispatchOnFinishFinalAnimation();
        }
    };
    private Animator.AnimatorListener mResetListenerWithPing = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            GlowPadView.this.ping();
            GlowPadView.this.switchToState(0, GlowPadView.this.mWaveCenterX, GlowPadView.this.mWaveCenterY);
            GlowPadView.this.dispatchOnFinishFinalAnimation();
        }
    };
    private float mSnapMargin = 0.0F;
    private AnimationBundle mTargetAnimations = new AnimationBundle();
    private ArrayList<String> mTargetDescriptions;
    private int mTargetDescriptionsResourceId;
    private ArrayList<TargetDrawable> mTargetDrawables = new ArrayList();
    private int mTargetResourceId;
    private Animator.AnimatorListener mTargetUpdateListener = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            if (GlowPadView.this.mNewTargetResources != 0)
            {
                GlowPadView.this.internalSetTargetResources(GlowPadView.this.mNewTargetResources);
                GlowPadView.access$302(GlowPadView.this, 0);
                GlowPadView.this.hideTargets(false, false);
            }
            GlowPadView.access$502(GlowPadView.this, false);
        }
    };
    private ValueAnimator.AnimatorUpdateListener mUpdateListener = new ValueAnimator.AnimatorUpdateListener()
    {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
            GlowPadView.this.invalidate();
        }
    };
    private int mVerticalInset;
    private int mVibrationDuration = 0;
    private Vibrator mVibrator;
    private AnimationBundle mWaveAnimations = new AnimationBundle();
    private float mWaveCenterX;
    private float mWaveCenterY;

    public GlowPadView(Context paramContext)
    {
        this(paramContext, null);
    }

    public GlowPadView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        this.mInitialLayout = i;
        Resources localResources = paramContext.getResources();
        TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, com.android.internal.R.styleable.GlowPadView);
        this.mInnerRadius = localTypedArray1.getDimension(0, this.mInnerRadius);
        this.mOuterRadius = localTypedArray1.getDimension(8, this.mOuterRadius);
        this.mSnapMargin = localTypedArray1.getDimension(10, this.mSnapMargin);
        this.mVibrationDuration = localTypedArray1.getInt(9, this.mVibrationDuration);
        this.mFeedbackCount = localTypedArray1.getInt(11, this.mFeedbackCount);
        this.mHandleDrawable = new TargetDrawable(localResources, localTypedArray1.peekValue(7).resourceId);
        this.mHandleDrawable.setState(TargetDrawable.STATE_INACTIVE);
        this.mOuterRing = new TargetDrawable(localResources, getResourceId(localTypedArray1, 3));
        this.mAlwaysTrackFinger = localTypedArray1.getBoolean(12, false);
        int k = getResourceId(localTypedArray1, 4);
        if (k != 0);
        TypedValue localTypedValue;
        for (Drawable localDrawable = localResources.getDrawable(k); ; localDrawable = null)
        {
            this.mGlowRadius = localTypedArray1.getDimension(5, 0.0F);
            localTypedValue = new TypedValue();
            if (localTypedArray1.getValue(6, localTypedValue))
                internalSetTargetResources(localTypedValue.resourceId);
            if ((this.mTargetDrawables != null) && (this.mTargetDrawables.size() != 0))
                break;
            throw new IllegalStateException("Must specify at least one target drawable");
        }
        if (localTypedArray1.getValue(i, localTypedValue))
        {
            int n = localTypedValue.resourceId;
            if (n == 0)
                throw new IllegalStateException("Must specify target descriptions");
            setTargetDescriptionsResourceId(n);
        }
        if (localTypedArray1.getValue(2, localTypedValue))
        {
            int m = localTypedValue.resourceId;
            if (m == 0)
                throw new IllegalStateException("Must specify direction descriptions");
            setDirectionDescriptionsResourceId(m);
        }
        localTypedArray1.recycle();
        TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, android.R.styleable.LinearLayout);
        this.mGravity = localTypedArray2.getInt(0, 48);
        localTypedArray2.recycle();
        if (this.mVibrationDuration > 0);
        while (true)
        {
            setVibrateEnabled(i);
            assignDefaultsIfNeeded();
            this.mPointCloud = new PointCloud(localDrawable);
            this.mPointCloud.makePointCloud(this.mInnerRadius, this.mOuterRadius);
            this.mPointCloud.glowManager.setRadius(this.mGlowRadius);
            return;
            int j = 0;
        }
    }

    private void announceTargets()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = this.mTargetDrawables.size();
        for (int j = 0; j < i; j++)
        {
            String str1 = getTargetDescription(j);
            String str2 = getDirectionDescription(j);
            if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2)))
            {
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = str1;
                localStringBuilder.append(String.format(str2, arrayOfObject));
            }
        }
        if (localStringBuilder.length() > 0)
            announceForAccessibility(localStringBuilder.toString());
    }

    private void assignDefaultsIfNeeded()
    {
        if (this.mOuterRadius == 0.0F)
            this.mOuterRadius = (Math.max(this.mOuterRing.getWidth(), this.mOuterRing.getHeight()) / 2.0F);
        if (this.mSnapMargin == 0.0F)
            this.mSnapMargin = TypedValue.applyDimension(1, 20.0F, getContext().getResources().getDisplayMetrics());
        if (this.mInnerRadius == 0.0F)
            this.mInnerRadius = (this.mHandleDrawable.getWidth() / 10.0F);
    }

    private void computeInsets(int paramInt1, int paramInt2)
    {
        int i = getResolvedLayoutDirection();
        int j = Gravity.getAbsoluteGravity(this.mGravity, i);
        switch (j & 0x7)
        {
        case 4:
        default:
            this.mHorizontalInset = (paramInt1 / 2);
            switch (j & 0x70)
            {
            default:
                this.mVerticalInset = (paramInt2 / 2);
            case 48:
            case 80:
            }
            break;
        case 3:
        case 5:
        }
        while (true)
        {
            return;
            this.mHorizontalInset = 0;
            break;
            this.mHorizontalInset = paramInt1;
            break;
            this.mVerticalInset = 0;
            continue;
            this.mVerticalInset = paramInt2;
        }
    }

    private void deactivateTargets()
    {
        int i = this.mTargetDrawables.size();
        for (int j = 0; j < i; j++)
            ((TargetDrawable)this.mTargetDrawables.get(j)).setState(TargetDrawable.STATE_INACTIVE);
        this.mActiveTarget = -1;
    }

    private void dispatchOnFinishFinalAnimation()
    {
        if (this.mOnTriggerListener != null)
            this.mOnTriggerListener.onFinishFinalAnimation();
    }

    private void dispatchTriggerEvent(int paramInt)
    {
        vibrate();
        if (this.mOnTriggerListener != null)
            this.mOnTriggerListener.onTrigger(this, paramInt);
    }

    private float dist2(float paramFloat1, float paramFloat2)
    {
        return paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2;
    }

    private void dump()
    {
        Log.v("GlowPadView", "Outer Radius = " + this.mOuterRadius);
        Log.v("GlowPadView", "SnapMargin = " + this.mSnapMargin);
        Log.v("GlowPadView", "FeedbackCount = " + this.mFeedbackCount);
        Log.v("GlowPadView", "VibrationDuration = " + this.mVibrationDuration);
        Log.v("GlowPadView", "GlowRadius = " + this.mGlowRadius);
        Log.v("GlowPadView", "WaveCenterX = " + this.mWaveCenterX);
        Log.v("GlowPadView", "WaveCenterY = " + this.mWaveCenterY);
    }

    private String getDirectionDescription(int paramInt)
    {
        if ((this.mDirectionDescriptions == null) || (this.mDirectionDescriptions.isEmpty()))
        {
            this.mDirectionDescriptions = loadDescriptions(this.mDirectionDescriptionsResourceId);
            if (this.mTargetDrawables.size() != this.mDirectionDescriptions.size())
                Log.w("GlowPadView", "The number of target drawables must be equal to the number of direction descriptions.");
        }
        for (String str = null; ; str = (String)this.mDirectionDescriptions.get(paramInt))
            return str;
    }

    private int getResourceId(TypedArray paramTypedArray, int paramInt)
    {
        TypedValue localTypedValue = paramTypedArray.peekValue(paramInt);
        if (localTypedValue == null);
        for (int i = 0; ; i = localTypedValue.resourceId)
            return i;
    }

    private float getScaledGlowRadiusSquared()
    {
        if (AccessibilityManager.getInstance(this.mContext).isEnabled());
        for (float f = 1.3F * this.mGlowRadius; ; f = this.mGlowRadius)
            return square(f);
    }

    private String getTargetDescription(int paramInt)
    {
        if ((this.mTargetDescriptions == null) || (this.mTargetDescriptions.isEmpty()))
        {
            this.mTargetDescriptions = loadDescriptions(this.mTargetDescriptionsResourceId);
            if (this.mTargetDrawables.size() != this.mTargetDescriptions.size())
                Log.w("GlowPadView", "The number of target drawables must be equal to the number of target descriptions.");
        }
        for (String str = null; ; str = (String)this.mTargetDescriptions.get(paramInt))
            return str;
    }

    private void handleCancel(MotionEvent paramMotionEvent)
    {
        switchToState(5, paramMotionEvent.getX(), paramMotionEvent.getY());
    }

    private void handleDown(MotionEvent paramMotionEvent)
    {
        float f1 = paramMotionEvent.getX();
        float f2 = paramMotionEvent.getY();
        switchToState(1, f1, f2);
        if (!trySwitchToFirstTouchState(f1, f2))
            this.mDragging = false;
        while (true)
        {
            return;
            updateGlowPosition(f1, f2);
        }
    }

    private void handleUp(MotionEvent paramMotionEvent)
    {
        switchToState(5, paramMotionEvent.getX(), paramMotionEvent.getY());
    }

    private void hideGlow(int paramInt1, int paramInt2, float paramFloat, Animator.AnimatorListener paramAnimatorListener)
    {
        this.mGlowAnimations.cancel();
        AnimationBundle localAnimationBundle = this.mGlowAnimations;
        PointCloud.GlowManager localGlowManager = this.mPointCloud.glowManager;
        long l = paramInt1;
        Object[] arrayOfObject = new Object[14];
        arrayOfObject[0] = "ease";
        arrayOfObject[1] = Ease.Quart.easeOut;
        arrayOfObject[2] = "delay";
        arrayOfObject[3] = Integer.valueOf(paramInt2);
        arrayOfObject[4] = "alpha";
        arrayOfObject[5] = Float.valueOf(paramFloat);
        arrayOfObject[6] = "x";
        arrayOfObject[7] = Float.valueOf(0.0F);
        arrayOfObject[8] = "y";
        arrayOfObject[9] = Float.valueOf(0.0F);
        arrayOfObject[10] = "onUpdate";
        arrayOfObject[11] = this.mUpdateListener;
        arrayOfObject[12] = "onComplete";
        arrayOfObject[13] = paramAnimatorListener;
        localAnimationBundle.add(Tweener.to(localGlowManager, l, arrayOfObject));
        this.mGlowAnimations.start();
    }

    private void hideUnselected(int paramInt)
    {
        for (int i = 0; i < this.mTargetDrawables.size(); i++)
            if (i != paramInt)
                ((TargetDrawable)this.mTargetDrawables.get(i)).setAlpha(0.0F);
    }

    private void highlightSelected(int paramInt)
    {
        ((TargetDrawable)this.mTargetDrawables.get(paramInt)).setState(TargetDrawable.STATE_ACTIVE);
        hideUnselected(paramInt);
    }

    private void internalSetTargetResources(int paramInt)
    {
        ArrayList localArrayList = loadDrawableArray(paramInt);
        this.mTargetDrawables = localArrayList;
        this.mTargetResourceId = paramInt;
        int i = this.mHandleDrawable.getWidth();
        int j = this.mHandleDrawable.getHeight();
        int k = localArrayList.size();
        for (int m = 0; m < k; m++)
        {
            TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(m);
            i = Math.max(i, localTargetDrawable.getWidth());
            j = Math.max(j, localTargetDrawable.getHeight());
        }
        if ((this.mMaxTargetWidth != i) || (this.mMaxTargetHeight != j))
        {
            this.mMaxTargetWidth = i;
            this.mMaxTargetHeight = j;
            requestLayout();
        }
        while (true)
        {
            return;
            updateTargetPositions(this.mWaveCenterX, this.mWaveCenterY);
            updatePointCloudPosition(this.mWaveCenterX, this.mWaveCenterY);
        }
    }

    private ArrayList<String> loadDescriptions(int paramInt)
    {
        TypedArray localTypedArray = getContext().getResources().obtainTypedArray(paramInt);
        int i = localTypedArray.length();
        ArrayList localArrayList = new ArrayList(i);
        for (int j = 0; j < i; j++)
            localArrayList.add(localTypedArray.getString(j));
        localTypedArray.recycle();
        return localArrayList;
    }

    private ArrayList<TargetDrawable> loadDrawableArray(int paramInt)
    {
        Resources localResources = getContext().getResources();
        TypedArray localTypedArray = localResources.obtainTypedArray(paramInt);
        int i = localTypedArray.length();
        ArrayList localArrayList = new ArrayList(i);
        int j = 0;
        if (j < i)
        {
            TypedValue localTypedValue = localTypedArray.peekValue(j);
            if (localTypedValue != null);
            for (int k = localTypedValue.resourceId; ; k = 0)
            {
                localArrayList.add(new TargetDrawable(localResources, k));
                j++;
                break;
            }
        }
        localTypedArray.recycle();
        return localArrayList;
    }

    private boolean replaceTargetDrawables(Resources paramResources, int paramInt1, int paramInt2)
    {
        boolean bool;
        if ((paramInt1 == 0) || (paramInt2 == 0))
            bool = false;
        while (true)
        {
            return bool;
            bool = false;
            ArrayList localArrayList = this.mTargetDrawables;
            int i = localArrayList.size();
            for (int j = 0; j < i; j++)
            {
                TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(j);
                if ((localTargetDrawable != null) && (localTargetDrawable.getResourceId() == paramInt1))
                {
                    localTargetDrawable.setDrawable(paramResources, paramInt2);
                    bool = true;
                }
            }
            if (bool)
                requestLayout();
        }
    }

    private int resolveMeasured(int paramInt1, int paramInt2)
    {
        int i = View.MeasureSpec.getSize(paramInt1);
        int j;
        switch (View.MeasureSpec.getMode(paramInt1))
        {
        default:
            j = i;
        case 0:
        case -2147483648:
        }
        while (true)
        {
            return j;
            j = paramInt2;
            continue;
            j = Math.min(i, paramInt2);
        }
    }

    private void setGrabbedState(int paramInt)
    {
        if (paramInt != this.mGrabbedState)
        {
            if (paramInt != 0)
                vibrate();
            this.mGrabbedState = paramInt;
            if (this.mOnTriggerListener != null)
            {
                if (paramInt != 0)
                    break label55;
                this.mOnTriggerListener.onReleased(this, 1);
            }
        }
        while (true)
        {
            this.mOnTriggerListener.onGrabbedStateChange(this, paramInt);
            return;
            label55: this.mOnTriggerListener.onGrabbed(this, 1);
        }
    }

    private void showGlow(int paramInt1, int paramInt2, float paramFloat, Animator.AnimatorListener paramAnimatorListener)
    {
        this.mGlowAnimations.cancel();
        AnimationBundle localAnimationBundle = this.mGlowAnimations;
        PointCloud.GlowManager localGlowManager = this.mPointCloud.glowManager;
        long l = paramInt1;
        Object[] arrayOfObject = new Object[10];
        arrayOfObject[0] = "ease";
        arrayOfObject[1] = Ease.Cubic.easeIn;
        arrayOfObject[2] = "delay";
        arrayOfObject[3] = Integer.valueOf(paramInt2);
        arrayOfObject[4] = "alpha";
        arrayOfObject[5] = Float.valueOf(paramFloat);
        arrayOfObject[6] = "onUpdate";
        arrayOfObject[7] = this.mUpdateListener;
        arrayOfObject[8] = "onComplete";
        arrayOfObject[9] = paramAnimatorListener;
        localAnimationBundle.add(Tweener.to(localGlowManager, l, arrayOfObject));
        this.mGlowAnimations.start();
    }

    private float square(float paramFloat)
    {
        return paramFloat * paramFloat;
    }

    private void startBackgroundAnimation(int paramInt, float paramFloat)
    {
        Drawable localDrawable = getBackground();
        if ((this.mAlwaysTrackFinger) && (localDrawable != null))
        {
            if (this.mBackgroundAnimator != null)
                this.mBackgroundAnimator.animator.cancel();
            long l = paramInt;
            Object[] arrayOfObject = new Object[6];
            arrayOfObject[0] = "ease";
            arrayOfObject[1] = Ease.Cubic.easeIn;
            arrayOfObject[2] = "alpha";
            arrayOfObject[3] = Integer.valueOf((int)(255.0F * paramFloat));
            arrayOfObject[4] = "delay";
            arrayOfObject[5] = Integer.valueOf(50);
            this.mBackgroundAnimator = Tweener.to(localDrawable, l, arrayOfObject);
            this.mBackgroundAnimator.animator.start();
        }
    }

    private void startWaveAnimation()
    {
        this.mWaveAnimations.cancel();
        this.mPointCloud.waveManager.setAlpha(1.0F);
        this.mPointCloud.waveManager.setRadius(this.mHandleDrawable.getWidth() / 2.0F);
        AnimationBundle localAnimationBundle = this.mWaveAnimations;
        PointCloud.WaveManager localWaveManager = this.mPointCloud.waveManager;
        Object[] arrayOfObject = new Object[10];
        arrayOfObject[0] = "ease";
        arrayOfObject[1] = Ease.Quad.easeOut;
        arrayOfObject[2] = "delay";
        arrayOfObject[3] = Integer.valueOf(0);
        arrayOfObject[4] = "radius";
        arrayOfObject[5] = Float.valueOf(2.0F * this.mOuterRadius);
        arrayOfObject[6] = "onUpdate";
        arrayOfObject[7] = this.mUpdateListener;
        arrayOfObject[8] = "onComplete";
        arrayOfObject[9] = new AnimatorListenerAdapter()
        {
            public void onAnimationEnd(Animator paramAnonymousAnimator)
            {
                GlowPadView.this.mPointCloud.waveManager.setRadius(0.0F);
                GlowPadView.this.mPointCloud.waveManager.setAlpha(0.0F);
            }
        };
        localAnimationBundle.add(Tweener.to(localWaveManager, 1350L, arrayOfObject));
        this.mWaveAnimations.start();
    }

    private void stopAndHideWaveAnimation()
    {
        this.mWaveAnimations.cancel();
        this.mPointCloud.waveManager.setAlpha(0.0F);
    }

    private boolean trySwitchToFirstTouchState(float paramFloat1, float paramFloat2)
    {
        boolean bool = true;
        float f1 = paramFloat1 - this.mWaveCenterX;
        float f2 = paramFloat2 - this.mWaveCenterY;
        if ((this.mAlwaysTrackFinger) || (dist2(f1, f2) <= getScaledGlowRadiusSquared()))
        {
            switchToState(2, paramFloat1, paramFloat2);
            updateGlowPosition(f1, f2);
            this.mDragging = bool;
        }
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    private void updateGlowPosition(float paramFloat1, float paramFloat2)
    {
        this.mPointCloud.glowManager.setX(paramFloat1);
        this.mPointCloud.glowManager.setY(paramFloat2);
    }

    private void updatePointCloudPosition(float paramFloat1, float paramFloat2)
    {
        this.mPointCloud.setCenter(paramFloat1, paramFloat2);
    }

    private void updateTargetPositions(float paramFloat1, float paramFloat2)
    {
        ArrayList localArrayList = this.mTargetDrawables;
        int i = localArrayList.size();
        float f1 = (float)(-6.283185307179586D / i);
        for (int j = 0; j < i; j++)
        {
            TargetDrawable localTargetDrawable = (TargetDrawable)localArrayList.get(j);
            float f2 = f1 * j;
            localTargetDrawable.setPositionX(paramFloat1);
            localTargetDrawable.setPositionY(paramFloat2);
            localTargetDrawable.setX(this.mOuterRadius * (float)Math.cos(f2));
            localTargetDrawable.setY(this.mOuterRadius * (float)Math.sin(f2));
        }
    }

    private void vibrate()
    {
        if (this.mVibrator != null)
            this.mVibrator.vibrate(this.mVibrationDuration);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callAnnounceTargets()
    {
        announceTargets();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callDeactivateTargets()
    {
        deactivateTargets();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callDispatchTriggerEvent(int paramInt)
    {
        dispatchTriggerEvent(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    String callGetTargetDescription(int paramInt)
    {
        return getTargetDescription(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callSetGrabbedState(int paramInt)
    {
        setGrabbedState(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callVibrate()
    {
        vibrate();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    void doFinish()
    {
        int i = this.mActiveTarget;
        int j;
        if (i != -1)
        {
            j = 1;
            if (j == 0)
                break label67;
            highlightSelected(i);
            hideGlow(200, 1200, 0.0F, this.mResetListener);
            dispatchTriggerEvent(i);
            if (!this.mAlwaysTrackFinger)
                this.mTargetAnimations.stop();
        }
        while (true)
        {
            setGrabbedState(0);
            return;
            j = 0;
            break;
            label67: hideGlow(200, 0, 0.0F, this.mResetListenerWithPing);
            hideTargets(true, false);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    int getActiveTarget()
    {
        return this.mActiveTarget;
    }

    public int getDirectionDescriptionsResourceId()
    {
        return this.mDirectionDescriptionsResourceId;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean getDragging()
    {
        return this.mDragging;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    TargetDrawable getHandleDrawable()
    {
        return this.mHandleDrawable;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    float getOuterRadius()
    {
        return this.mOuterRadius;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    TargetDrawable getOuterRing()
    {
        return this.mOuterRing;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Animator.AnimatorListener getResetListener()
    {
        return this.mResetListener;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Animator.AnimatorListener getResetListenerWithPing()
    {
        return this.mResetListenerWithPing;
    }

    public int getResourceIdForTarget(int paramInt)
    {
        TargetDrawable localTargetDrawable = (TargetDrawable)this.mTargetDrawables.get(paramInt);
        if (localTargetDrawable == null);
        for (int i = 0; ; i = localTargetDrawable.getResourceId())
            return i;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    float getSnapMargin()
    {
        return this.mSnapMargin;
    }

    protected int getSuggestedMinimumHeight()
    {
        return (int)(Math.max(this.mOuterRing.getHeight(), 2.0F * this.mOuterRadius) + this.mMaxTargetHeight);
    }

    protected int getSuggestedMinimumWidth()
    {
        return (int)(Math.max(this.mOuterRing.getWidth(), 2.0F * this.mOuterRadius) + this.mMaxTargetWidth);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    AnimationBundle getTargetAnimations()
    {
        return this.mTargetAnimations;
    }

    public int getTargetDescriptionsResourceId()
    {
        return this.mTargetDescriptionsResourceId;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    ArrayList<TargetDrawable> getTargetDrawables()
    {
        return this.mTargetDrawables;
    }

    public int getTargetPosition(int paramInt)
    {
        int i = 0;
        if (i < this.mTargetDrawables.size())
            if (((TargetDrawable)this.mTargetDrawables.get(i)).getResourceId() != paramInt);
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    public int getTargetResourceId()
    {
        return this.mTargetResourceId;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    ValueAnimator.AnimatorUpdateListener getUpdateListener()
    {
        return this.mUpdateListener;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    float getWaveCenterX()
    {
        return this.mWaveCenterX;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    float getWaveCenterY()
    {
        return this.mWaveCenterY;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    void handleMove(MotionEvent paramMotionEvent)
    {
        int i = -1;
        int j = paramMotionEvent.getHistorySize();
        ArrayList localArrayList = this.mTargetDrawables;
        int k = localArrayList.size();
        float f1 = 0.0F;
        float f2 = 0.0F;
        for (int m = 0; m < j + 1; m++)
        {
            float f3;
            float f4;
            label66: float f5;
            float f6;
            float f8;
            label118: float f9;
            float f10;
            float f12;
            int n;
            if (m < j)
            {
                f3 = paramMotionEvent.getHistoricalX(m);
                if (m >= j)
                    break label337;
                f4 = paramMotionEvent.getHistoricalY(m);
                f5 = f3 - this.mWaveCenterX;
                f6 = f4 - this.mWaveCenterY;
                float f7 = (float)Math.sqrt(dist2(f5, f6));
                if (f7 <= this.mOuterRadius)
                    break label346;
                f8 = this.mOuterRadius / f7;
                f9 = f5 * f8;
                f10 = f6 * f8;
                double d1 = Math.atan2(-f6, f5);
                if (!this.mDragging)
                    trySwitchToFirstTouchState(f3, f4);
                if (!this.mDragging)
                    break label358;
                float f11 = this.mOuterRadius - this.mSnapMargin;
                f12 = f11 * f11;
                n = 0;
                label188: if (n >= k)
                    break label358;
                TargetDrawable localTargetDrawable3 = (TargetDrawable)localArrayList.get(n);
                double d2 = 3.141592653589793D * (2.0D * (n - 0.5D)) / k;
                double d3 = 3.141592653589793D * (2.0D * (0.5D + n)) / k;
                if (localTargetDrawable3.isEnabled())
                    if (((d1 <= d2) || (d1 > d3)) && ((6.283185307179586D + d1 <= d2) || (6.283185307179586D + d1 > d3)))
                        break label352;
            }
            label337: label346: label352: for (int i1 = 1; ; i1 = 0)
            {
                if ((i1 != 0) && (dist2(f5, f6) > f12))
                    i = n;
                n++;
                break label188;
                f3 = paramMotionEvent.getX();
                break;
                f4 = paramMotionEvent.getY();
                break label66;
                f8 = 1.0F;
                break label118;
            }
            label358: f1 = f9;
            f2 = f10;
        }
        if (!this.mDragging)
            return;
        if (i != -1)
        {
            switchToState(4, f1, f2);
            updateGlowPosition(f1, f2);
        }
        while (true)
        {
            if (this.mActiveTarget != i)
            {
                if (this.mActiveTarget != -1)
                {
                    TargetDrawable localTargetDrawable2 = (TargetDrawable)localArrayList.get(this.mActiveTarget);
                    if (localTargetDrawable2.hasState(TargetDrawable.STATE_FOCUSED))
                        localTargetDrawable2.setState(TargetDrawable.STATE_INACTIVE);
                }
                if (i != -1)
                {
                    TargetDrawable localTargetDrawable1 = (TargetDrawable)localArrayList.get(i);
                    if (localTargetDrawable1.hasState(TargetDrawable.STATE_FOCUSED))
                        localTargetDrawable1.setState(TargetDrawable.STATE_FOCUSED);
                    if (AccessibilityManager.getInstance(this.mContext).isEnabled())
                        announceForAccessibility(getTargetDescription(i));
                }
            }
            this.mActiveTarget = i;
            break;
            switchToState(3, f1, f2);
            updateGlowPosition(f1, f2);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    void hideTargets(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mTargetAnimations.cancel();
        this.mAnimatingTargets = paramBoolean1;
        int i;
        int j;
        if (paramBoolean1)
        {
            i = 200;
            if (!paramBoolean1)
                break label222;
            j = 200;
            label29: if (!paramBoolean2)
                break label228;
        }
        TimeInterpolator localTimeInterpolator;
        label222: label228: for (float f1 = 1.0F; ; f1 = 0.8F)
        {
            int k = this.mTargetDrawables.size();
            localTimeInterpolator = Ease.Cubic.easeOut;
            for (int m = 0; m < k; m++)
            {
                TargetDrawable localTargetDrawable2 = (TargetDrawable)this.mTargetDrawables.get(m);
                localTargetDrawable2.setState(TargetDrawable.STATE_INACTIVE);
                AnimationBundle localAnimationBundle2 = this.mTargetAnimations;
                long l2 = i;
                Object[] arrayOfObject2 = new Object[12];
                arrayOfObject2[0] = "ease";
                arrayOfObject2[1] = localTimeInterpolator;
                arrayOfObject2[2] = "alpha";
                arrayOfObject2[3] = Float.valueOf(0.0F);
                arrayOfObject2[4] = "scaleX";
                arrayOfObject2[5] = Float.valueOf(f1);
                arrayOfObject2[6] = "scaleY";
                arrayOfObject2[7] = Float.valueOf(f1);
                arrayOfObject2[8] = "delay";
                arrayOfObject2[9] = Integer.valueOf(j);
                arrayOfObject2[10] = "onUpdate";
                arrayOfObject2[11] = this.mUpdateListener;
                localAnimationBundle2.add(Tweener.to(localTargetDrawable2, l2, arrayOfObject2));
            }
            i = 0;
            break;
            j = 0;
            break label29;
        }
        if (paramBoolean2);
        for (float f2 = 1.0F; ; f2 = 0.5F)
        {
            AnimationBundle localAnimationBundle1 = this.mTargetAnimations;
            TargetDrawable localTargetDrawable1 = this.mOuterRing;
            long l1 = i;
            Object[] arrayOfObject1 = new Object[14];
            arrayOfObject1[0] = "ease";
            arrayOfObject1[1] = localTimeInterpolator;
            arrayOfObject1[2] = "alpha";
            arrayOfObject1[3] = Float.valueOf(0.0F);
            arrayOfObject1[4] = "scaleX";
            arrayOfObject1[5] = Float.valueOf(f2);
            arrayOfObject1[6] = "scaleY";
            arrayOfObject1[7] = Float.valueOf(f2);
            arrayOfObject1[8] = "delay";
            arrayOfObject1[9] = Integer.valueOf(j);
            arrayOfObject1[10] = "onUpdate";
            arrayOfObject1[11] = this.mUpdateListener;
            arrayOfObject1[12] = "onComplete";
            arrayOfObject1[13] = this.mTargetUpdateListener;
            localAnimationBundle1.add(Tweener.to(localTargetDrawable1, l1, arrayOfObject1));
            this.mTargetAnimations.start();
            return;
        }
    }

    protected void onDraw(Canvas paramCanvas)
    {
        this.mPointCloud.draw(paramCanvas);
        this.mOuterRing.draw(paramCanvas);
        int i = this.mTargetDrawables.size();
        for (int j = 0; j < i; j++)
        {
            TargetDrawable localTargetDrawable = (TargetDrawable)this.mTargetDrawables.get(j);
            if (localTargetDrawable != null)
                localTargetDrawable.draw(paramCanvas);
        }
        this.mHandleDrawable.draw(paramCanvas);
    }

    public boolean onHoverEvent(MotionEvent paramMotionEvent)
    {
        int i;
        if (AccessibilityManager.getInstance(this.mContext).isTouchExplorationEnabled())
        {
            i = paramMotionEvent.getAction();
            switch (i)
            {
            case 8:
            default:
            case 9:
            case 7:
            case 10:
            }
        }
        while (true)
        {
            onTouchEvent(paramMotionEvent);
            paramMotionEvent.setAction(i);
            return super.onHoverEvent(paramMotionEvent);
            paramMotionEvent.setAction(0);
            continue;
            paramMotionEvent.setAction(2);
            continue;
            paramMotionEvent.setAction(1);
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        int i = paramInt3 - paramInt1;
        int j = paramInt4 - paramInt2;
        float f1 = Math.max(this.mOuterRing.getWidth(), 2.0F * this.mOuterRadius);
        float f2 = Math.max(this.mOuterRing.getHeight(), 2.0F * this.mOuterRadius);
        float f3 = this.mHorizontalInset + Math.max(i, f1 + this.mMaxTargetWidth) / 2.0F;
        float f4 = this.mVerticalInset + Math.max(j, f2 + this.mMaxTargetHeight) / 2.0F;
        if (this.mInitialLayout)
        {
            stopAndHideWaveAnimation();
            hideTargets(false, false);
            this.mInitialLayout = false;
        }
        this.mOuterRing.setPositionX(f3);
        this.mOuterRing.setPositionY(f4);
        this.mHandleDrawable.setPositionX(f3);
        this.mHandleDrawable.setPositionY(f4);
        updateTargetPositions(f3, f4);
        updatePointCloudPosition(f3, f4);
        updateGlowPosition(f3, f4);
        this.mWaveCenterX = f3;
        this.mWaveCenterY = f4;
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = getSuggestedMinimumWidth();
        int j = getSuggestedMinimumHeight();
        int k = resolveMeasured(paramInt1, i);
        int m = resolveMeasured(paramInt2, j);
        computeInsets(k - i, m - j);
        setMeasuredDimension(k, m);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getAction();
        int j = 0;
        switch (i)
        {
        default:
            invalidate();
            if (j == 0)
                break;
        case 0:
        case 2:
        case 1:
        case 3:
        }
        for (boolean bool = true; ; bool = super.onTouchEvent(paramMotionEvent))
        {
            return bool;
            handleDown(paramMotionEvent);
            handleMove(paramMotionEvent);
            j = 1;
            break;
            handleMove(paramMotionEvent);
            j = 1;
            break;
            handleMove(paramMotionEvent);
            handleUp(paramMotionEvent);
            j = 1;
            break;
            handleMove(paramMotionEvent);
            handleCancel(paramMotionEvent);
            j = 1;
            break;
        }
    }

    public void ping()
    {
        if (this.mFeedbackCount > 0)
        {
            int i = 1;
            AnimationBundle localAnimationBundle = this.mWaveAnimations;
            if ((localAnimationBundle.size() > 0) && (((Tweener)localAnimationBundle.get(0)).animator.isRunning()) && (((Tweener)localAnimationBundle.get(0)).animator.getCurrentPlayTime() < 675L))
                i = 0;
            if (i != 0)
                startWaveAnimation();
        }
    }

    public boolean replaceTargetDrawablesIfPresent(ComponentName paramComponentName, String paramString, int paramInt)
    {
        boolean bool1;
        if (paramInt == 0)
            bool1 = false;
        while (true)
        {
            return bool1;
            bool1 = false;
            if (paramComponentName != null);
            try
            {
                PackageManager localPackageManager = this.mContext.getPackageManager();
                Bundle localBundle = localPackageManager.getActivityInfo(paramComponentName, 128).metaData;
                if (localBundle != null)
                {
                    int i = localBundle.getInt(paramString);
                    if (i != 0)
                    {
                        boolean bool2 = replaceTargetDrawables(localPackageManager.getResourcesForActivity(paramComponentName), paramInt, i);
                        bool1 = bool2;
                    }
                }
                if (bool1)
                    continue;
                replaceTargetDrawables(this.mContext.getResources(), paramInt, paramInt);
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                while (true)
                    Log.w("GlowPadView", "Failed to swap drawable; " + paramComponentName.flattenToShortString() + " not found", localNameNotFoundException);
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                while (true)
                    Log.w("GlowPadView", "Failed to swap drawable from " + paramComponentName.flattenToShortString(), localNotFoundException);
            }
        }
    }

    public void reset(boolean paramBoolean)
    {
        this.mGlowAnimations.stop();
        this.mTargetAnimations.stop();
        startBackgroundAnimation(0, 0.0F);
        stopAndHideWaveAnimation();
        hideTargets(paramBoolean, false);
        hideGlow(0, 0, 1.0F, null);
        Tweener.reset();
    }

    public void resumeAnimations()
    {
        this.mWaveAnimations.setSuspended(false);
        this.mTargetAnimations.setSuspended(false);
        this.mGlowAnimations.setSuspended(false);
        this.mWaveAnimations.start();
        this.mTargetAnimations.start();
        this.mGlowAnimations.start();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setActiveTarget(int paramInt)
    {
        this.mActiveTarget = paramInt;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setAnimatingTargets(boolean paramBoolean)
    {
        this.mAnimatingTargets = paramBoolean;
    }

    public void setDirectionDescriptionsResourceId(int paramInt)
    {
        this.mDirectionDescriptionsResourceId = paramInt;
        if (this.mDirectionDescriptions != null)
            this.mDirectionDescriptions.clear();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setDragging(boolean paramBoolean)
    {
        this.mDragging = paramBoolean;
    }

    public void setEnableTarget(int paramInt, boolean paramBoolean)
    {
        for (int i = 0; ; i++)
            if (i < this.mTargetDrawables.size())
            {
                TargetDrawable localTargetDrawable = (TargetDrawable)this.mTargetDrawables.get(i);
                if (localTargetDrawable.getResourceId() == paramInt)
                    localTargetDrawable.setEnabled(paramBoolean);
            }
            else
            {
                return;
            }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setGlowRadius(float paramFloat)
    {
        this.mGlowRadius = paramFloat;
    }

    public void setOnTriggerListener(OnTriggerListener paramOnTriggerListener)
    {
        this.mOnTriggerListener = paramOnTriggerListener;
    }

    public void setTargetDescriptionsResourceId(int paramInt)
    {
        this.mTargetDescriptionsResourceId = paramInt;
        if (this.mTargetDescriptions != null)
            this.mTargetDescriptions.clear();
    }

    public void setTargetResources(int paramInt)
    {
        if (this.mAnimatingTargets)
            this.mNewTargetResources = paramInt;
        while (true)
        {
            return;
            internalSetTargetResources(paramInt);
        }
    }

    public void setVibrateEnabled(boolean paramBoolean)
    {
        if ((paramBoolean) && (this.mVibrator == null));
        for (this.mVibrator = ((Vibrator)getContext().getSystemService("vibrator")); ; this.mVibrator = null)
            return;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    void showTargets(boolean paramBoolean)
    {
        this.mTargetAnimations.stop();
        this.mAnimatingTargets = paramBoolean;
        int i;
        if (paramBoolean)
        {
            i = 50;
            if (!paramBoolean)
                break label206;
        }
        label206: for (int j = 200; ; j = 0)
        {
            int k = this.mTargetDrawables.size();
            for (int m = 0; m < k; m++)
            {
                TargetDrawable localTargetDrawable2 = (TargetDrawable)this.mTargetDrawables.get(m);
                localTargetDrawable2.setState(TargetDrawable.STATE_INACTIVE);
                AnimationBundle localAnimationBundle2 = this.mTargetAnimations;
                long l2 = j;
                Object[] arrayOfObject2 = new Object[12];
                arrayOfObject2[0] = "ease";
                arrayOfObject2[1] = Ease.Cubic.easeOut;
                arrayOfObject2[2] = "alpha";
                arrayOfObject2[3] = Float.valueOf(1.0F);
                arrayOfObject2[4] = "scaleX";
                arrayOfObject2[5] = Float.valueOf(1.0F);
                arrayOfObject2[6] = "scaleY";
                arrayOfObject2[7] = Float.valueOf(1.0F);
                arrayOfObject2[8] = "delay";
                arrayOfObject2[9] = Integer.valueOf(i);
                arrayOfObject2[10] = "onUpdate";
                arrayOfObject2[11] = this.mUpdateListener;
                localAnimationBundle2.add(Tweener.to(localTargetDrawable2, l2, arrayOfObject2));
            }
            i = 0;
            break;
        }
        AnimationBundle localAnimationBundle1 = this.mTargetAnimations;
        TargetDrawable localTargetDrawable1 = this.mOuterRing;
        long l1 = j;
        Object[] arrayOfObject1 = new Object[14];
        arrayOfObject1[0] = "ease";
        arrayOfObject1[1] = Ease.Cubic.easeOut;
        arrayOfObject1[2] = "alpha";
        arrayOfObject1[3] = Float.valueOf(1.0F);
        arrayOfObject1[4] = "scaleX";
        arrayOfObject1[5] = Float.valueOf(1.0F);
        arrayOfObject1[6] = "scaleY";
        arrayOfObject1[7] = Float.valueOf(1.0F);
        arrayOfObject1[8] = "delay";
        arrayOfObject1[9] = Integer.valueOf(i);
        arrayOfObject1[10] = "onUpdate";
        arrayOfObject1[11] = this.mUpdateListener;
        arrayOfObject1[12] = "onComplete";
        arrayOfObject1[13] = this.mTargetUpdateListener;
        localAnimationBundle1.add(Tweener.to(localTargetDrawable1, l1, arrayOfObject1));
        this.mTargetAnimations.start();
    }

    public void suspendAnimations()
    {
        this.mWaveAnimations.setSuspended(true);
        this.mTargetAnimations.setSuspended(true);
        this.mGlowAnimations.setSuspended(true);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    void switchToState(int paramInt, float paramFloat1, float paramFloat2)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return;
            deactivateTargets();
            hideGlow(0, 0, 0.0F, null);
            startBackgroundAnimation(0, 0.0F);
            this.mHandleDrawable.setState(TargetDrawable.STATE_INACTIVE);
            this.mHandleDrawable.setAlpha(1.0F);
            continue;
            startBackgroundAnimation(0, 0.0F);
            continue;
            this.mHandleDrawable.setAlpha(0.0F);
            deactivateTargets();
            showTargets(true);
            startBackgroundAnimation(200, 1.0F);
            setGrabbedState(1);
            if (AccessibilityManager.getInstance(this.mContext).isEnabled())
            {
                announceTargets();
                continue;
                this.mHandleDrawable.setAlpha(0.0F);
                showGlow(0, 0, 1.0F, null);
                continue;
                this.mHandleDrawable.setAlpha(0.0F);
                showGlow(0, 0, 0.0F, null);
                continue;
                doFinish();
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    class AnimationBundle extends ArrayList<Tweener>
    {
        private static final long serialVersionUID = -6319262269245852568L;
        private boolean mSuspended;

        AnimationBundle()
        {
        }

        public void cancel()
        {
            int i = size();
            for (int j = 0; j < i; j++)
                ((Tweener)get(j)).animator.cancel();
            clear();
        }

        public void setSuspended(boolean paramBoolean)
        {
            this.mSuspended = paramBoolean;
        }

        public void start()
        {
            if (this.mSuspended);
            while (true)
            {
                return;
                int i = size();
                for (int j = 0; j < i; j++)
                    ((Tweener)get(j)).animator.start();
            }
        }

        public void stop()
        {
            int i = size();
            for (int j = 0; j < i; j++)
                ((Tweener)get(j)).animator.end();
            clear();
        }
    }

    public static abstract interface OnTriggerListener
    {
        public static final int CENTER_HANDLE = 1;
        public static final int NO_HANDLE;

        public abstract void onFinishFinalAnimation();

        public abstract void onGrabbed(View paramView, int paramInt);

        public abstract void onGrabbedStateChange(View paramView, int paramInt);

        public abstract void onReleased(View paramView, int paramInt);

        public abstract void onTrigger(View paramView, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.multiwaveview.GlowPadView
 * JD-Core Version:        0.6.2
 */