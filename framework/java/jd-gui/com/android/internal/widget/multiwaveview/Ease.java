package com.android.internal.widget.multiwaveview;

import android.animation.TimeInterpolator;

class Ease
{
    private static final float DOMAIN = 1.0F;
    private static final float DURATION = 1.0F;
    private static final float START;

    static class Sine
    {
        public static final TimeInterpolator easeIn = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                return 0.0F + (1.0F + -1.0F * (float)Math.cos(1.570796326794897D * (paramAnonymousFloat / 1.0F)));
            }
        };
        public static final TimeInterpolator easeInOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                return 0.0F + -0.5F * ((float)Math.cos(3.141592653589793D * paramAnonymousFloat / 1.0D) - 1.0F);
            }
        };
        public static final TimeInterpolator easeOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                return 0.0F + 1.0F * (float)Math.sin(1.570796326794897D * (paramAnonymousFloat / 1.0F));
            }
        };
    }

    static class Quint
    {
        public static final TimeInterpolator easeIn = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f = paramAnonymousFloat / 1.0F;
                return 0.0F + f * (f * (f * (f * (1.0F * f))));
            }
        };
        public static final TimeInterpolator easeInOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f1 = paramAnonymousFloat / 0.5F;
                if (f1 < 1.0F);
                float f2;
                for (float f3 = 0.0F + f1 * (f1 * (f1 * (f1 * (0.5F * f1)))); ; f3 = 0.0F + 0.5F * (2.0F + f2 * (f2 * (f2 * (f2 * f2)))))
                {
                    return f3;
                    f2 = f1 - 2.0F;
                }
            }
        };
        public static final TimeInterpolator easeOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f = paramAnonymousFloat / 1.0F - 1.0F;
                return 0.0F + 1.0F * (1.0F + f * (f * (f * (f * f))));
            }
        };
    }

    static class Quart
    {
        public static final TimeInterpolator easeIn = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f = paramAnonymousFloat / 1.0F;
                return 0.0F + f * (f * (f * (1.0F * f)));
            }
        };
        public static final TimeInterpolator easeInOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f1 = paramAnonymousFloat / 0.5F;
                if (f1 < 1.0F);
                float f2;
                for (float f3 = 0.0F + f1 * (f1 * (f1 * (0.5F * f1))); ; f3 = 0.0F + -0.5F * (f2 * (f2 * (f2 * f2)) - 2.0F))
                {
                    return f3;
                    f2 = f1 - 2.0F;
                }
            }
        };
        public static final TimeInterpolator easeOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f = paramAnonymousFloat / 1.0F - 1.0F;
                return 0.0F + -1.0F * (f * (f * (f * f)) - 1.0F);
            }
        };
    }

    static class Quad
    {
        public static final TimeInterpolator easeIn = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f = paramAnonymousFloat / 1.0F;
                return 0.0F + f * (1.0F * f);
            }
        };
        public static final TimeInterpolator easeInOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f1 = paramAnonymousFloat / 0.5F;
                if (f1 < 1.0F);
                float f2;
                for (float f3 = 0.0F + f1 * (0.5F * f1); ; f3 = 0.0F + -0.5F * (f2 * (f2 - 2.0F) - 1.0F))
                {
                    return f3;
                    f2 = f1 - 1.0F;
                }
            }
        };
        public static final TimeInterpolator easeOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f = paramAnonymousFloat / 1.0F;
                return 0.0F + -1.0F * f * (f - 2.0F);
            }
        };
    }

    static class Cubic
    {
        public static final TimeInterpolator easeIn = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f = paramAnonymousFloat / 1.0F;
                return 0.0F + f * (f * (1.0F * f));
            }
        };
        public static final TimeInterpolator easeInOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f1 = paramAnonymousFloat / 0.5F;
                if (f1 < 1.0F);
                float f2;
                for (float f3 = 0.0F + f1 * (f1 * (0.5F * f1)); ; f3 = 0.0F + 0.5F * (2.0F + f2 * (f2 * f2)))
                {
                    return f3;
                    f2 = f1 - 2.0F;
                }
            }
        };
        public static final TimeInterpolator easeOut = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                float f = paramAnonymousFloat / 1.0F - 1.0F;
                return 0.0F + 1.0F * (1.0F + f * (f * f));
            }
        };
    }

    static class Linear
    {
        public static final TimeInterpolator easeNone = new TimeInterpolator()
        {
            public float getInterpolation(float paramAnonymousFloat)
            {
                return paramAnonymousFloat;
            }
        };
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.multiwaveview.Ease
 * JD-Core Version:        0.6.2
 */