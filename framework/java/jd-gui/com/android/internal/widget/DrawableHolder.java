package com.android.internal.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.view.animation.DecelerateInterpolator;
import java.util.ArrayList;
import java.util.Iterator;

public class DrawableHolder
    implements Animator.AnimatorListener
{
    private static final boolean DBG = false;
    public static final DecelerateInterpolator EASE_OUT_INTERPOLATOR = new DecelerateInterpolator();
    private static final String TAG = "DrawableHolder";
    private float mAlpha = 1.0F;
    private ArrayList<ObjectAnimator> mAnimators = new ArrayList();
    private BitmapDrawable mDrawable;
    private ArrayList<ObjectAnimator> mNeedToStart = new ArrayList();
    private float mScaleX = 1.0F;
    private float mScaleY = 1.0F;
    private float mX = 0.0F;
    private float mY = 0.0F;

    public DrawableHolder(BitmapDrawable paramBitmapDrawable)
    {
        this(paramBitmapDrawable, 0.0F, 0.0F);
    }

    public DrawableHolder(BitmapDrawable paramBitmapDrawable, float paramFloat1, float paramFloat2)
    {
        this.mDrawable = paramBitmapDrawable;
        this.mX = paramFloat1;
        this.mY = paramFloat2;
        this.mDrawable.getPaint().setAntiAlias(true);
        this.mDrawable.setBounds(0, 0, this.mDrawable.getIntrinsicWidth(), this.mDrawable.getIntrinsicHeight());
    }

    private DrawableHolder addAnimation(ObjectAnimator paramObjectAnimator, boolean paramBoolean)
    {
        if (paramObjectAnimator != null)
            this.mAnimators.add(paramObjectAnimator);
        this.mNeedToStart.add(paramObjectAnimator);
        return this;
    }

    public ObjectAnimator addAnimTo(long paramLong1, long paramLong2, String paramString, float paramFloat, boolean paramBoolean)
    {
        if (paramBoolean)
            removeAnimationFor(paramString);
        float[] arrayOfFloat = new float[1];
        arrayOfFloat[0] = paramFloat;
        ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this, paramString, arrayOfFloat);
        localObjectAnimator.setDuration(paramLong1);
        localObjectAnimator.setStartDelay(paramLong2);
        localObjectAnimator.setInterpolator(EASE_OUT_INTERPOLATOR);
        addAnimation(localObjectAnimator, paramBoolean);
        return localObjectAnimator;
    }

    public void clearAnimations()
    {
        Iterator localIterator = this.mAnimators.iterator();
        while (localIterator.hasNext())
            ((ObjectAnimator)localIterator.next()).cancel();
        this.mAnimators.clear();
    }

    public void draw(Canvas paramCanvas)
    {
        if (this.mAlpha <= 0.0039063F);
        while (true)
        {
            return;
            paramCanvas.save(1);
            paramCanvas.translate(this.mX, this.mY);
            paramCanvas.scale(this.mScaleX, this.mScaleY);
            paramCanvas.translate(-0.5F * getWidth(), -0.5F * getHeight());
            this.mDrawable.setAlpha(Math.round(255.0F * this.mAlpha));
            this.mDrawable.draw(paramCanvas);
            paramCanvas.restore();
        }
    }

    public float getAlpha()
    {
        return this.mAlpha;
    }

    public BitmapDrawable getDrawable()
    {
        return this.mDrawable;
    }

    public int getHeight()
    {
        return this.mDrawable.getIntrinsicHeight();
    }

    public float getScaleX()
    {
        return this.mScaleX;
    }

    public float getScaleY()
    {
        return this.mScaleY;
    }

    public int getWidth()
    {
        return this.mDrawable.getIntrinsicWidth();
    }

    public float getX()
    {
        return this.mX;
    }

    public float getY()
    {
        return this.mY;
    }

    public void onAnimationCancel(Animator paramAnimator)
    {
    }

    public void onAnimationEnd(Animator paramAnimator)
    {
        this.mAnimators.remove(paramAnimator);
    }

    public void onAnimationRepeat(Animator paramAnimator)
    {
    }

    public void onAnimationStart(Animator paramAnimator)
    {
    }

    public void removeAnimationFor(String paramString)
    {
        Iterator localIterator = ((ArrayList)this.mAnimators.clone()).iterator();
        while (localIterator.hasNext())
        {
            ObjectAnimator localObjectAnimator = (ObjectAnimator)localIterator.next();
            if (paramString.equals(localObjectAnimator.getPropertyName()))
                localObjectAnimator.cancel();
        }
    }

    public void setAlpha(float paramFloat)
    {
        this.mAlpha = paramFloat;
    }

    public void setScaleX(float paramFloat)
    {
        this.mScaleX = paramFloat;
    }

    public void setScaleY(float paramFloat)
    {
        this.mScaleY = paramFloat;
    }

    public void setX(float paramFloat)
    {
        this.mX = paramFloat;
    }

    public void setY(float paramFloat)
    {
        this.mY = paramFloat;
    }

    public void startAnimations(ValueAnimator.AnimatorUpdateListener paramAnimatorUpdateListener)
    {
        for (int i = 0; i < this.mNeedToStart.size(); i++)
        {
            ObjectAnimator localObjectAnimator = (ObjectAnimator)this.mNeedToStart.get(i);
            localObjectAnimator.addUpdateListener(paramAnimatorUpdateListener);
            localObjectAnimator.addListener(this);
            localObjectAnimator.start();
        }
        this.mNeedToStart.clear();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.DrawableHolder
 * JD-Core Version:        0.6.2
 */