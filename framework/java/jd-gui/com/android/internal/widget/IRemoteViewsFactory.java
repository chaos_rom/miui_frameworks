package com.android.internal.widget;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.widget.RemoteViews;

public abstract interface IRemoteViewsFactory extends IInterface
{
    public abstract int getCount()
        throws RemoteException;

    public abstract long getItemId(int paramInt)
        throws RemoteException;

    public abstract RemoteViews getLoadingView()
        throws RemoteException;

    public abstract RemoteViews getViewAt(int paramInt)
        throws RemoteException;

    public abstract int getViewTypeCount()
        throws RemoteException;

    public abstract boolean hasStableIds()
        throws RemoteException;

    public abstract boolean isCreated()
        throws RemoteException;

    public abstract void onDataSetChanged()
        throws RemoteException;

    public abstract void onDataSetChangedAsync()
        throws RemoteException;

    public abstract void onDestroy(Intent paramIntent)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IRemoteViewsFactory
    {
        private static final String DESCRIPTOR = "com.android.internal.widget.IRemoteViewsFactory";
        static final int TRANSACTION_getCount = 4;
        static final int TRANSACTION_getItemId = 8;
        static final int TRANSACTION_getLoadingView = 6;
        static final int TRANSACTION_getViewAt = 5;
        static final int TRANSACTION_getViewTypeCount = 7;
        static final int TRANSACTION_hasStableIds = 9;
        static final int TRANSACTION_isCreated = 10;
        static final int TRANSACTION_onDataSetChanged = 1;
        static final int TRANSACTION_onDataSetChangedAsync = 2;
        static final int TRANSACTION_onDestroy = 3;

        public Stub()
        {
            attachInterface(this, "com.android.internal.widget.IRemoteViewsFactory");
        }

        public static IRemoteViewsFactory asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.widget.IRemoteViewsFactory");
                if ((localIInterface != null) && ((localIInterface instanceof IRemoteViewsFactory)))
                    localObject = (IRemoteViewsFactory)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("com.android.internal.widget.IRemoteViewsFactory");
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                onDataSetChanged();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                onDataSetChangedAsync();
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                if (paramParcel1.readInt() != 0);
                for (Intent localIntent = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent = null)
                {
                    onDestroy(localIntent);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                int m = getCount();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(m);
                continue;
                paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                RemoteViews localRemoteViews2 = getViewAt(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localRemoteViews2 != null)
                {
                    paramParcel2.writeInt(j);
                    localRemoteViews2.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                    RemoteViews localRemoteViews1 = getLoadingView();
                    paramParcel2.writeNoException();
                    if (localRemoteViews1 != null)
                    {
                        paramParcel2.writeInt(j);
                        localRemoteViews1.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                        int k = getViewTypeCount();
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(k);
                        continue;
                        paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                        long l = getItemId(paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeLong(l);
                        continue;
                        paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                        boolean bool2 = hasStableIds();
                        paramParcel2.writeNoException();
                        if (bool2)
                            i = j;
                        paramParcel2.writeInt(i);
                        continue;
                        paramParcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
                        boolean bool1 = isCreated();
                        paramParcel2.writeNoException();
                        if (bool1)
                            i = j;
                        paramParcel2.writeInt(i);
                    }
                }
            }
        }

        private static class Proxy
            implements IRemoteViewsFactory
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public int getCount()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.widget.IRemoteViewsFactory";
            }

            public long getItemId(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public RemoteViews getLoadingView()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localRemoteViews = (RemoteViews)RemoteViews.CREATOR.createFromParcel(localParcel2);
                        return localRemoteViews;
                    }
                    RemoteViews localRemoteViews = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public RemoteViews getViewAt(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localRemoteViews = (RemoteViews)RemoteViews.CREATOR.createFromParcel(localParcel2);
                        return localRemoteViews;
                    }
                    RemoteViews localRemoteViews = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getViewTypeCount()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasStableIds()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isCreated()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onDataSetChanged()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onDataSetChangedAsync()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onDestroy(Intent paramIntent)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
                    if (paramIntent != null)
                    {
                        localParcel.writeInt(1);
                        paramIntent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.IRemoteViewsFactory
 * JD-Core Version:        0.6.2
 */