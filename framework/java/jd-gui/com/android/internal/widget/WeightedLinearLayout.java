package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View.MeasureSpec;
import android.widget.LinearLayout;
import com.android.internal.R.styleable;

public class WeightedLinearLayout extends LinearLayout
{
    private float mMajorWeightMax;
    private float mMajorWeightMin;
    private float mMinorWeightMax;
    private float mMinorWeightMin;

    public WeightedLinearLayout(Context paramContext)
    {
        super(paramContext);
    }

    public WeightedLinearLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.WeightedLinearLayout);
        this.mMajorWeightMin = localTypedArray.getFloat(0, 0.0F);
        this.mMinorWeightMin = localTypedArray.getFloat(1, 0.0F);
        this.mMajorWeightMax = localTypedArray.getFloat(2, 0.0F);
        this.mMinorWeightMax = localTypedArray.getFloat(3, 0.0F);
        localTypedArray.recycle();
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        DisplayMetrics localDisplayMetrics = getContext().getResources().getDisplayMetrics();
        int i = localDisplayMetrics.widthPixels;
        int j;
        int m;
        int i1;
        float f1;
        label70: float f2;
        label81: int i3;
        if (i < localDisplayMetrics.heightPixels)
        {
            j = 1;
            int k = View.MeasureSpec.getMode(paramInt1);
            super.onMeasure(paramInt1, paramInt2);
            m = getMeasuredWidth();
            n = 0;
            i1 = View.MeasureSpec.makeMeasureSpec(m, 1073741824);
            if (j == 0)
                break label151;
            f1 = this.mMinorWeightMin;
            if (j == 0)
                break label160;
            f2 = this.mMinorWeightMax;
            if (k == -2147483648)
            {
                int i2 = (int)(f1 * i);
                i3 = (int)(f1 * i);
                if ((f1 <= 0.0F) || (m >= i2))
                    break label169;
                i1 = View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
            }
        }
        for (int n = 1; ; n = 1)
        {
            label151: label160: label169: 
            do
            {
                if (n != 0)
                    super.onMeasure(i1, paramInt2);
                return;
                j = 0;
                break;
                f1 = this.mMajorWeightMin;
                break label70;
                f2 = this.mMajorWeightMax;
                break label81;
            }
            while ((f2 <= 0.0F) || (m <= i3));
            i1 = View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.WeightedLinearLayout
 * JD-Core Version:        0.6.2
 */