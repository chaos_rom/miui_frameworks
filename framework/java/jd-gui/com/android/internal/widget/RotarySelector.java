package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import com.android.internal.R.styleable;

public class RotarySelector extends View
{
    private static final int ARROW_SCRUNCH_DIP = 6;
    private static final boolean DBG = false;
    private static final int EDGE_PADDING_DIP = 9;
    private static final int EDGE_TRIGGER_DIP = 100;
    public static final int HORIZONTAL = 0;
    public static final int LEFT_HANDLE_GRABBED = 1;
    private static final String LOG_TAG = "RotarySelector";
    public static final int NOTHING_GRABBED = 0;
    static final int OUTER_ROTARY_RADIUS_DIP = 390;
    public static final int RIGHT_HANDLE_GRABBED = 2;
    static final int ROTARY_STROKE_WIDTH_DIP = 83;
    static final int SNAP_BACK_ANIMATION_DURATION_MILLIS = 300;
    static final int SPIN_ANIMATION_DURATION_MILLIS = 800;
    public static final int VERTICAL = 1;
    private static final long VIBRATE_LONG = 20L;
    private static final long VIBRATE_SHORT = 20L;
    private static final boolean VISUAL_DEBUG;
    private boolean mAnimating = false;
    private int mAnimatingDeltaXEnd;
    private int mAnimatingDeltaXStart;
    private long mAnimationDuration;
    private long mAnimationStartTime;
    private Bitmap mArrowLongLeft;
    private Bitmap mArrowLongRight;
    final Matrix mArrowMatrix = new Matrix();
    private Bitmap mArrowShortLeftAndRight;
    private Bitmap mBackground;
    private int mBackgroundHeight;
    private int mBackgroundWidth;
    final Matrix mBgMatrix = new Matrix();
    private float mDensity;
    private Bitmap mDimple;
    private Bitmap mDimpleDim;
    private int mDimpleSpacing;
    private int mDimpleWidth;
    private int mDimplesOfFling = 0;
    private int mEdgeTriggerThresh;
    private int mGrabbedState = 0;
    private final int mInnerRadius;
    private DecelerateInterpolator mInterpolator;
    private Bitmap mLeftHandleIcon;
    private int mLeftHandleX;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private OnDialTriggerListener mOnDialTriggerListener;
    private int mOrientation;
    private final int mOuterRadius;
    private Paint mPaint = new Paint();
    private Bitmap mRightHandleIcon;
    private int mRightHandleX;
    private int mRotaryOffsetX = 0;
    private boolean mTriggered = false;
    private VelocityTracker mVelocityTracker;
    private Vibrator mVibrator;

    public RotarySelector(Context paramContext)
    {
        this(paramContext, null);
    }

    public RotarySelector(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RotarySelector);
        this.mOrientation = localTypedArray.getInt(0, 0);
        localTypedArray.recycle();
        this.mDensity = getResources().getDisplayMetrics().density;
        this.mBackground = getBitmapFor(17302402);
        this.mDimple = getBitmapFor(17302403);
        this.mDimpleDim = getBitmapFor(17302404);
        this.mArrowLongLeft = getBitmapFor(17302394);
        this.mArrowLongRight = getBitmapFor(17302397);
        this.mArrowShortLeftAndRight = getBitmapFor(17302400);
        this.mInterpolator = new DecelerateInterpolator(1.0F);
        this.mEdgeTriggerThresh = ((int)(100.0F * this.mDensity));
        this.mDimpleWidth = this.mDimple.getWidth();
        this.mBackgroundWidth = this.mBackground.getWidth();
        this.mBackgroundHeight = this.mBackground.getHeight();
        this.mOuterRadius = ((int)(390.0F * this.mDensity));
        this.mInnerRadius = ((int)(307.0F * this.mDensity));
        ViewConfiguration localViewConfiguration = ViewConfiguration.get(this.mContext);
        this.mMinimumVelocity = (2 * localViewConfiguration.getScaledMinimumFlingVelocity());
        this.mMaximumVelocity = localViewConfiguration.getScaledMaximumFlingVelocity();
    }

    private void dispatchTriggerEvent(int paramInt)
    {
        vibrate(20L);
        if (this.mOnDialTriggerListener != null)
            this.mOnDialTriggerListener.onDialTrigger(this, paramInt);
    }

    private void drawCentered(Bitmap paramBitmap, Canvas paramCanvas, int paramInt1, int paramInt2)
    {
        int i = paramBitmap.getWidth();
        int j = paramBitmap.getHeight();
        paramCanvas.drawBitmap(paramBitmap, paramInt1 - i / 2, paramInt2 - j / 2, this.mPaint);
    }

    private Bitmap getBitmapFor(int paramInt)
    {
        return BitmapFactory.decodeResource(getContext().getResources(), paramInt);
    }

    private int getYOnArc(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = (paramInt3 - paramInt2) / 2;
        int j = paramInt2 + i;
        int k = paramInt1 / 2 - paramInt4;
        return i + (j - (int)Math.sqrt(j * j - k * k));
    }

    private boolean isHoriz()
    {
        if (this.mOrientation == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void log(String paramString)
    {
        Log.d("RotarySelector", paramString);
    }

    private void reset()
    {
        this.mAnimating = false;
        this.mRotaryOffsetX = 0;
        this.mDimplesOfFling = 0;
        setGrabbedState(0);
        this.mTriggered = false;
    }

    private void setGrabbedState(int paramInt)
    {
        if (paramInt != this.mGrabbedState)
        {
            this.mGrabbedState = paramInt;
            if (this.mOnDialTriggerListener != null)
                this.mOnDialTriggerListener.onGrabbedStateChange(this, this.mGrabbedState);
        }
    }

    private void startAnimation(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mAnimating = true;
        this.mAnimationStartTime = AnimationUtils.currentAnimationTimeMillis();
        this.mAnimationDuration = paramInt3;
        this.mAnimatingDeltaXStart = paramInt1;
        this.mAnimatingDeltaXEnd = paramInt2;
        setGrabbedState(0);
        this.mDimplesOfFling = 0;
        invalidate();
    }

    private void startAnimationWithVelocity(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mAnimating = true;
        this.mAnimationStartTime = AnimationUtils.currentAnimationTimeMillis();
        this.mAnimationDuration = (1000 * (paramInt2 - paramInt1) / paramInt3);
        this.mAnimatingDeltaXStart = paramInt1;
        this.mAnimatingDeltaXEnd = paramInt2;
        setGrabbedState(0);
        invalidate();
    }

    private void updateAnimation()
    {
        long l1 = AnimationUtils.currentAnimationTimeMillis() - this.mAnimationStartTime;
        long l2 = this.mAnimationDuration - l1;
        int i = this.mAnimatingDeltaXStart - this.mAnimatingDeltaXEnd;
        if (i < 0);
        for (int j = 1; l2 <= 0L; j = 0)
        {
            reset();
            return;
        }
        float f = this.mInterpolator.getInterpolation((float)l1 / (float)this.mAnimationDuration);
        this.mRotaryOffsetX = ((int)(i * (1.0F - f)) + this.mAnimatingDeltaXEnd);
        if (this.mDimplesOfFling > 0)
            if ((j != 0) || (this.mRotaryOffsetX >= -3 * this.mDimpleSpacing))
                break label138;
        for (this.mRotaryOffsetX += this.mDimplesOfFling * this.mDimpleSpacing; ; this.mRotaryOffsetX -= this.mDimplesOfFling * this.mDimpleSpacing)
            label138: 
            do
            {
                invalidate();
                break;
            }
            while ((j == 0) || (this.mRotaryOffsetX <= 3 * this.mDimpleSpacing));
    }

    /** @deprecated */
    private void vibrate(long paramLong)
    {
        try
        {
            if (this.mVibrator == null)
                this.mVibrator = ((Vibrator)getContext().getSystemService("vibrator"));
            this.mVibrator.vibrate(paramLong);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void onDraw(Canvas paramCanvas)
    {
        super.onDraw(paramCanvas);
        int i = getWidth();
        int j = getHeight();
        if (this.mAnimating)
            updateAnimation();
        paramCanvas.drawBitmap(this.mBackground, this.mBgMatrix, this.mPaint);
        this.mArrowMatrix.reset();
        int k;
        int m;
        label186: int n;
        int i1;
        int i2;
        label228: int i3;
        label242: int i4;
        label276: label293: int i5;
        label336: int i6;
        int i7;
        int i8;
        label378: int i9;
        label392: label426: int i10;
        int i11;
        label450: int i15;
        switch (this.mGrabbedState)
        {
        default:
            throw new IllegalStateException("invalid mGrabbedState: " + this.mGrabbedState);
        case 1:
            this.mArrowMatrix.setTranslate(0.0F, 0.0F);
            if (!isHoriz())
            {
                this.mArrowMatrix.preRotate(-90.0F, 0.0F, 0.0F);
                this.mArrowMatrix.postTranslate(0.0F, j);
            }
            paramCanvas.drawBitmap(this.mArrowLongLeft, this.mArrowMatrix, this.mPaint);
        case 0:
            k = this.mBackgroundHeight;
            if (isHoriz())
            {
                m = j - k;
                n = this.mLeftHandleX + this.mRotaryOffsetX;
                i1 = getYOnArc(this.mBackgroundWidth, this.mInnerRadius, this.mOuterRadius, n);
                if (!isHoriz())
                    break label588;
                i2 = n;
                if (!isHoriz())
                    break label598;
                i3 = i1 + m;
                if (this.mGrabbedState == 2)
                    break label607;
                drawCentered(this.mDimple, paramCanvas, i2, i3);
                drawCentered(this.mLeftHandleIcon, paramCanvas, i2, i3);
                if (!isHoriz())
                    break label623;
                i4 = i / 2 + this.mRotaryOffsetX;
                i5 = getYOnArc(this.mBackgroundWidth, this.mInnerRadius, this.mOuterRadius, i4);
                if (!isHoriz())
                    break label636;
                drawCentered(this.mDimpleDim, paramCanvas, i4, i5 + m);
                i6 = this.mRightHandleX + this.mRotaryOffsetX;
                i7 = getYOnArc(this.mBackgroundWidth, this.mInnerRadius, this.mOuterRadius, i6);
                if (!isHoriz())
                    break label657;
                i8 = i6;
                if (!isHoriz())
                    break label667;
                i9 = i7 + m;
                if (this.mGrabbedState == 1)
                    break label676;
                drawCentered(this.mDimple, paramCanvas, i8, i9);
                drawCentered(this.mRightHandleIcon, paramCanvas, i8, i9);
                i10 = this.mRotaryOffsetX + this.mLeftHandleX - this.mDimpleSpacing;
                i11 = this.mDimpleWidth / 2;
                if (i10 <= -i11)
                    break label713;
                i15 = getYOnArc(this.mBackgroundWidth, this.mInnerRadius, this.mOuterRadius, i10);
                if (!isHoriz())
                    break label692;
                drawCentered(this.mDimpleDim, paramCanvas, i10, i15 + m);
            }
            break;
        case 2:
        }
        while (true)
        {
            i10 -= this.mDimpleSpacing;
            break label450;
            this.mArrowMatrix.setTranslate(0.0F, 0.0F);
            if (!isHoriz())
            {
                this.mArrowMatrix.preRotate(-90.0F, 0.0F, 0.0F);
                this.mArrowMatrix.postTranslate(0.0F, j + (this.mBackgroundWidth - j));
            }
            paramCanvas.drawBitmap(this.mArrowLongRight, this.mArrowMatrix, this.mPaint);
            break;
            m = i - k;
            break label186;
            label588: i2 = i1 + m;
            break label228;
            label598: i3 = j - n;
            break label242;
            label607: drawCentered(this.mDimpleDim, paramCanvas, i2, i3);
            break label276;
            label623: i4 = j / 2 + this.mRotaryOffsetX;
            break label293;
            label636: drawCentered(this.mDimpleDim, paramCanvas, i5 + m, j - i4);
            break label336;
            label657: i8 = i7 + m;
            break label378;
            label667: i9 = j - i6;
            break label392;
            label676: drawCentered(this.mDimpleDim, paramCanvas, i8, i9);
            break label426;
            label692: drawCentered(this.mDimpleDim, paramCanvas, i15 + m, j - i10);
        }
        label713: int i12 = this.mRotaryOffsetX + this.mRightHandleX + this.mDimpleSpacing;
        int i13 = i11 + this.mRight;
        if (i12 < i13)
        {
            int i14 = getYOnArc(this.mBackgroundWidth, this.mInnerRadius, this.mOuterRadius, i12);
            if (isHoriz())
                drawCentered(this.mDimpleDim, paramCanvas, i12, i14 + m);
            while (true)
            {
                i12 += this.mDimpleSpacing;
                break;
                drawCentered(this.mDimpleDim, paramCanvas, i14 + m, j - i12);
            }
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i;
        int k;
        if (isHoriz())
        {
            i = View.MeasureSpec.getSize(paramInt1);
            int j = (int)(6.0F * this.mDensity);
            k = this.mArrowShortLeftAndRight.getHeight() + this.mBackgroundHeight - j;
            if (!isHoriz())
                break label63;
            setMeasuredDimension(i, k);
        }
        while (true)
        {
            return;
            i = View.MeasureSpec.getSize(paramInt2);
            break;
            label63: setMeasuredDimension(k, i);
        }
    }

    protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
        int i = (int)(9.0F * this.mDensity);
        this.mLeftHandleX = (i + this.mDimpleWidth / 2);
        int j;
        if (isHoriz())
        {
            j = paramInt1;
            this.mRightHandleX = (j - i - this.mDimpleWidth / 2);
            this.mDimpleSpacing = (j / 2 - this.mLeftHandleX);
            this.mBgMatrix.setTranslate(0.0F, 0.0F);
            if (isHoriz())
                break label129;
            int k = paramInt1 - this.mBackgroundHeight;
            this.mBgMatrix.preRotate(-90.0F, 0.0F, 0.0F);
            this.mBgMatrix.postTranslate(k, paramInt2);
        }
        while (true)
        {
            return;
            j = paramInt2;
            break;
            label129: this.mBgMatrix.postTranslate(0.0F, paramInt2 - this.mBackgroundHeight);
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if (this.mAnimating);
        while (true)
        {
            return true;
            if (this.mVelocityTracker == null)
                this.mVelocityTracker = VelocityTracker.obtain();
            this.mVelocityTracker.addMovement(paramMotionEvent);
            int i = getHeight();
            if (isHoriz());
            int k;
            for (int j = (int)paramMotionEvent.getX(); ; j = i - (int)paramMotionEvent.getY())
            {
                k = this.mDimpleWidth;
                switch (paramMotionEvent.getAction())
                {
                default:
                    break;
                case 0:
                    this.mTriggered = false;
                    if (this.mGrabbedState != 0)
                    {
                        reset();
                        invalidate();
                    }
                    if (j >= k + this.mLeftHandleX)
                        break label162;
                    this.mRotaryOffsetX = (j - this.mLeftHandleX);
                    setGrabbedState(1);
                    invalidate();
                    vibrate(20L);
                    break;
                case 2:
                case 1:
                case 3:
                }
            }
            label162: if (j > this.mRightHandleX - k)
            {
                this.mRotaryOffsetX = (j - this.mRightHandleX);
                setGrabbedState(2);
                invalidate();
                vibrate(20L);
                continue;
                if (this.mGrabbedState == 1)
                {
                    this.mRotaryOffsetX = (j - this.mLeftHandleX);
                    invalidate();
                    int i1;
                    label237: VelocityTracker localVelocityTracker2;
                    if (isHoriz())
                    {
                        i1 = getRight();
                        if ((j >= i1 - this.mEdgeTriggerThresh) && (!this.mTriggered))
                        {
                            this.mTriggered = true;
                            dispatchTriggerEvent(1);
                            localVelocityTracker2 = this.mVelocityTracker;
                            localVelocityTracker2.computeCurrentVelocity(1000, this.mMaximumVelocity);
                            if (!isHoriz());
                        }
                    }
                    else
                    {
                        for (int i2 = (int)localVelocityTracker2.getXVelocity(); ; i2 = -(int)localVelocityTracker2.getYVelocity())
                        {
                            int i3 = Math.max(this.mMinimumVelocity, i2);
                            this.mDimplesOfFling = Math.max(8, Math.abs(i3 / this.mDimpleSpacing));
                            startAnimationWithVelocity(j - this.mLeftHandleX, this.mDimplesOfFling * this.mDimpleSpacing, i3);
                            break;
                            i1 = i;
                            break label237;
                        }
                    }
                }
                else if (this.mGrabbedState == 2)
                {
                    this.mRotaryOffsetX = (j - this.mRightHandleX);
                    invalidate();
                    if ((j <= this.mEdgeTriggerThresh) && (!this.mTriggered))
                    {
                        this.mTriggered = true;
                        dispatchTriggerEvent(2);
                        VelocityTracker localVelocityTracker1 = this.mVelocityTracker;
                        localVelocityTracker1.computeCurrentVelocity(1000, this.mMaximumVelocity);
                        if (isHoriz());
                        for (int m = (int)localVelocityTracker1.getXVelocity(); ; m = -(int)localVelocityTracker1.getYVelocity())
                        {
                            int n = Math.min(-this.mMinimumVelocity, m);
                            this.mDimplesOfFling = Math.max(8, Math.abs(n / this.mDimpleSpacing));
                            startAnimationWithVelocity(j - this.mRightHandleX, -(this.mDimplesOfFling * this.mDimpleSpacing), n);
                            break;
                        }
                        if ((this.mGrabbedState == 1) && (Math.abs(j - this.mLeftHandleX) > 5))
                            startAnimation(j - this.mLeftHandleX, 0, 300);
                        while (true)
                        {
                            this.mRotaryOffsetX = 0;
                            setGrabbedState(0);
                            invalidate();
                            if (this.mVelocityTracker == null)
                                break;
                            this.mVelocityTracker.recycle();
                            this.mVelocityTracker = null;
                            break;
                            if ((this.mGrabbedState == 2) && (Math.abs(j - this.mRightHandleX) > 5))
                                startAnimation(j - this.mRightHandleX, 0, 300);
                        }
                        reset();
                        invalidate();
                        if (this.mVelocityTracker != null)
                        {
                            this.mVelocityTracker.recycle();
                            this.mVelocityTracker = null;
                        }
                    }
                }
            }
        }
    }

    public void setLeftHandleResource(int paramInt)
    {
        if (paramInt != 0)
            this.mLeftHandleIcon = getBitmapFor(paramInt);
        invalidate();
    }

    public void setOnDialTriggerListener(OnDialTriggerListener paramOnDialTriggerListener)
    {
        this.mOnDialTriggerListener = paramOnDialTriggerListener;
    }

    public void setRightHandleResource(int paramInt)
    {
        if (paramInt != 0)
            this.mRightHandleIcon = getBitmapFor(paramInt);
        invalidate();
    }

    public static abstract interface OnDialTriggerListener
    {
        public static final int LEFT_HANDLE = 1;
        public static final int RIGHT_HANDLE = 2;

        public abstract void onDialTrigger(View paramView, int paramInt);

        public abstract void onGrabbedStateChange(View paramView, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.RotarySelector
 * JD-Core Version:        0.6.2
 */