package com.android.internal.widget;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.IRemoteControlDisplay.Stub;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import java.lang.ref.WeakReference;

public class TransportControlView extends FrameLayout
    implements View.OnClickListener, LockScreenWidgetInterface
{
    protected static final boolean DEBUG = false;
    private static final int DISPLAY_TIMEOUT_MS = 5000;
    private static final int MAXDIM = 512;
    private static final int MSG_SET_ARTWORK = 103;
    private static final int MSG_SET_GENERATION_ID = 104;
    private static final int MSG_SET_METADATA = 101;
    private static final int MSG_SET_TRANSPORT_CONTROLS = 102;
    private static final int MSG_UPDATE_STATE = 100;
    protected static final String TAG = "TransportControlView";
    private ImageView mAlbumArt;
    private boolean mAttached;
    private AudioManager mAudioManager;
    private ImageView mBtnNext;
    private ImageView mBtnPlay;
    private ImageView mBtnPrev;
    private int mClientGeneration;
    private PendingIntent mClientIntent;
    private int mCurrentPlayState;
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            }
            while (true)
            {
                return;
                if (TransportControlView.this.mClientGeneration == paramAnonymousMessage.arg1)
                {
                    TransportControlView.this.updatePlayPauseState(paramAnonymousMessage.arg2);
                    continue;
                    if (TransportControlView.this.mClientGeneration == paramAnonymousMessage.arg1)
                    {
                        TransportControlView.this.updateMetadata((Bundle)paramAnonymousMessage.obj);
                        continue;
                        if (TransportControlView.this.mClientGeneration == paramAnonymousMessage.arg1)
                        {
                            TransportControlView.this.updateTransportControls(paramAnonymousMessage.arg2);
                            continue;
                            if (TransportControlView.this.mClientGeneration == paramAnonymousMessage.arg1)
                            {
                                if (TransportControlView.Metadata.access$500(TransportControlView.this.mMetadata) != null)
                                    TransportControlView.Metadata.access$500(TransportControlView.this.mMetadata).recycle();
                                TransportControlView.Metadata.access$502(TransportControlView.this.mMetadata, (Bitmap)paramAnonymousMessage.obj);
                                TransportControlView.this.mAlbumArt.setImageBitmap(TransportControlView.Metadata.access$500(TransportControlView.this.mMetadata));
                                continue;
                                if ((paramAnonymousMessage.arg2 != 0) && (TransportControlView.this.mWidgetCallbacks != null))
                                    TransportControlView.this.mWidgetCallbacks.requestHide(TransportControlView.this);
                                TransportControlView.access$002(TransportControlView.this, paramAnonymousMessage.arg1);
                                TransportControlView.access$802(TransportControlView.this, (PendingIntent)paramAnonymousMessage.obj);
                            }
                        }
                    }
                }
            }
        }
    };
    private IRemoteControlDisplayWeak mIRCD;
    private Metadata mMetadata = new Metadata();
    private Bundle mPopulateMetadataWhenAttached = null;
    private TextView mTrackTitle;
    private int mTransportControlFlags;
    private LockScreenWidgetCallback mWidgetCallbacks;

    public TransportControlView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        Log.v("TransportControlView", "Create TCV " + this);
        this.mAudioManager = new AudioManager(this.mContext);
        this.mCurrentPlayState = 0;
        this.mIRCD = new IRemoteControlDisplayWeak(this.mHandler);
    }

    private String getMdString(Bundle paramBundle, int paramInt)
    {
        return paramBundle.getString(Integer.toString(paramInt));
    }

    private void populateMetadata()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = 0;
        if (!TextUtils.isEmpty(this.mMetadata.trackTitle))
        {
            localStringBuilder.append(this.mMetadata.trackTitle);
            i = this.mMetadata.trackTitle.length();
        }
        if (!TextUtils.isEmpty(this.mMetadata.artist))
        {
            if (localStringBuilder.length() != 0)
                localStringBuilder.append(" - ");
            localStringBuilder.append(this.mMetadata.artist);
        }
        if (!TextUtils.isEmpty(this.mMetadata.albumTitle))
        {
            if (localStringBuilder.length() != 0)
                localStringBuilder.append(" - ");
            localStringBuilder.append(this.mMetadata.albumTitle);
        }
        this.mTrackTitle.setText(localStringBuilder.toString(), TextView.BufferType.SPANNABLE);
        Spannable localSpannable = (Spannable)this.mTrackTitle.getText();
        if (i != 0)
        {
            localSpannable.setSpan(new ForegroundColorSpan(-1), 0, i, 33);
            i++;
        }
        if (localStringBuilder.length() > i)
            localSpannable.setSpan(new ForegroundColorSpan(2147483647), i, localStringBuilder.length(), 33);
        this.mAlbumArt.setImageBitmap(this.mMetadata.bitmap);
        int j = this.mTransportControlFlags;
        setVisibilityBasedOnFlag(this.mBtnPrev, j, 1);
        setVisibilityBasedOnFlag(this.mBtnNext, j, 128);
        setVisibilityBasedOnFlag(this.mBtnPlay, j, 60);
        updatePlayPauseState(this.mCurrentPlayState);
    }

    private void sendMediaButtonClick(int paramInt)
    {
        if (this.mClientIntent == null)
            Log.e("TransportControlView", "sendMediaButtonClick(): No client is currently registered");
        while (true)
        {
            return;
            KeyEvent localKeyEvent1 = new KeyEvent(0, paramInt);
            Intent localIntent1 = new Intent("android.intent.action.MEDIA_BUTTON");
            localIntent1.putExtra("android.intent.extra.KEY_EVENT", localKeyEvent1);
            try
            {
                this.mClientIntent.send(getContext(), 0, localIntent1);
                KeyEvent localKeyEvent2 = new KeyEvent(1, paramInt);
                Intent localIntent2 = new Intent("android.intent.action.MEDIA_BUTTON");
                localIntent2.putExtra("android.intent.extra.KEY_EVENT", localKeyEvent2);
                try
                {
                    this.mClientIntent.send(getContext(), 0, localIntent2);
                }
                catch (PendingIntent.CanceledException localCanceledException2)
                {
                    Log.e("TransportControlView", "Error sending intent for media button up: " + localCanceledException2);
                    localCanceledException2.printStackTrace();
                }
            }
            catch (PendingIntent.CanceledException localCanceledException1)
            {
                while (true)
                {
                    Log.e("TransportControlView", "Error sending intent for media button down: " + localCanceledException1);
                    localCanceledException1.printStackTrace();
                }
            }
        }
    }

    private static void setVisibilityBasedOnFlag(View paramView, int paramInt1, int paramInt2)
    {
        if ((paramInt1 & paramInt2) != 0)
            paramView.setVisibility(0);
        while (true)
        {
            return;
            paramView.setVisibility(8);
        }
    }

    private void updateMetadata(Bundle paramBundle)
    {
        if (this.mAttached)
        {
            Metadata.access$902(this.mMetadata, getMdString(paramBundle, 13));
            Metadata.access$1002(this.mMetadata, getMdString(paramBundle, 7));
            Metadata.access$1102(this.mMetadata, getMdString(paramBundle, 1));
            populateMetadata();
        }
        while (true)
        {
            return;
            this.mPopulateMetadataWhenAttached = paramBundle;
        }
    }

    private void updatePlayPauseState(int paramInt)
    {
        if (paramInt == this.mCurrentPlayState)
            return;
        int i = 0;
        int j;
        int k;
        switch (paramInt)
        {
        default:
            j = 17301540;
            k = 17040142;
            i = 0;
        case 9:
        case 3:
        case 8:
        }
        while (true)
        {
            this.mBtnPlay.setImageResource(j);
            this.mBtnPlay.setContentDescription(getResources().getString(k));
            if ((i != 0) && (this.mWidgetCallbacks != null) && (!this.mWidgetCallbacks.isVisible(this)))
                this.mWidgetCallbacks.requestShow(this);
            this.mCurrentPlayState = paramInt;
            break;
            j = 17301642;
            k = 17040142;
            continue;
            j = 17301539;
            k = 17040141;
            i = 1;
            continue;
            j = 17302295;
            k = 17040143;
            i = 1;
        }
    }

    private void updateTransportControls(int paramInt)
    {
        this.mTransportControlFlags = paramInt;
    }

    private boolean wasPlayingRecently(int paramInt, long paramLong)
    {
        boolean bool = true;
        switch (paramInt)
        {
        default:
            Log.e("TransportControlView", "Unknown playback state " + paramInt + " in wasPlayingRecently()");
            bool = false;
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 0:
        case 1:
        case 2:
        case 9:
        }
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if (SystemClock.elapsedRealtime() - paramLong >= 5000L)
                bool = false;
        }
    }

    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (this.mPopulateMetadataWhenAttached != null)
        {
            updateMetadata(this.mPopulateMetadataWhenAttached);
            this.mPopulateMetadataWhenAttached = null;
        }
        if (!this.mAttached)
            this.mAudioManager.registerRemoteControlDisplay(this.mIRCD);
        this.mAttached = true;
    }

    public void onClick(View paramView)
    {
        int i = -1;
        if (paramView == this.mBtnPrev)
            i = 88;
        while (true)
        {
            if (i != -1)
            {
                sendMediaButtonClick(i);
                if (this.mWidgetCallbacks != null)
                    this.mWidgetCallbacks.userActivity(this);
            }
            return;
            if (paramView == this.mBtnNext)
                i = 87;
            else if (paramView == this.mBtnPlay)
                i = 85;
        }
    }

    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.mAttached)
            this.mAudioManager.unregisterRemoteControlDisplay(this.mIRCD);
        this.mAttached = false;
    }

    public void onFinishInflate()
    {
        super.onFinishInflate();
        this.mTrackTitle = ((TextView)findViewById(16908310));
        this.mTrackTitle.setSelected(true);
        this.mAlbumArt = ((ImageView)findViewById(16908999));
        this.mBtnPrev = ((ImageView)findViewById(16909000));
        this.mBtnPlay = ((ImageView)findViewById(16909001));
        this.mBtnNext = ((ImageView)findViewById(16909002));
        View[] arrayOfView = new View[3];
        arrayOfView[0] = this.mBtnPrev;
        arrayOfView[1] = this.mBtnPlay;
        arrayOfView[2] = this.mBtnNext;
        int i = arrayOfView.length;
        for (int j = 0; j < i; j++)
            arrayOfView[j].setOnClickListener(this);
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        super.onMeasure(paramInt1, paramInt2);
        Math.min(512, Math.max(getWidth(), getHeight()));
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if (!(paramParcelable instanceof SavedState))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            if ((localSavedState.wasShowing) && (this.mWidgetCallbacks != null))
                this.mWidgetCallbacks.requestShow(this);
        }
    }

    public Parcelable onSaveInstanceState()
    {
        SavedState localSavedState = new SavedState(super.onSaveInstanceState());
        if ((this.mWidgetCallbacks != null) && (this.mWidgetCallbacks.isVisible(this)));
        for (boolean bool = true; ; bool = false)
        {
            localSavedState.wasShowing = bool;
            return localSavedState;
        }
    }

    public boolean providesClock()
    {
        return false;
    }

    public void setCallback(LockScreenWidgetCallback paramLockScreenWidgetCallback)
    {
        this.mWidgetCallbacks = paramLockScreenWidgetCallback;
    }

    static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public TransportControlView.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new TransportControlView.SavedState(paramAnonymousParcel, null);
            }

            public TransportControlView.SavedState[] newArray(int paramAnonymousInt)
            {
                return new TransportControlView.SavedState[paramAnonymousInt];
            }
        };
        boolean wasShowing;

        private SavedState(Parcel paramParcel)
        {
            super();
            if (paramParcel.readInt() != 0);
            for (boolean bool = true; ; bool = false)
            {
                this.wasShowing = bool;
                return;
            }
        }

        SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            if (this.wasShowing);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                return;
            }
        }
    }

    class Metadata
    {
        private String albumTitle;
        private String artist;
        private Bitmap bitmap;
        private String trackTitle;

        Metadata()
        {
        }

        public String toString()
        {
            return "Metadata[artist=" + this.artist + " trackTitle=" + this.trackTitle + " albumTitle=" + this.albumTitle + "]";
        }
    }

    private static class IRemoteControlDisplayWeak extends IRemoteControlDisplay.Stub
    {
        private WeakReference<Handler> mLocalHandler;

        IRemoteControlDisplayWeak(Handler paramHandler)
        {
            this.mLocalHandler = new WeakReference(paramHandler);
        }

        public void setAllMetadata(int paramInt, Bundle paramBundle, Bitmap paramBitmap)
        {
            Handler localHandler = (Handler)this.mLocalHandler.get();
            if (localHandler != null)
            {
                localHandler.obtainMessage(101, paramInt, 0, paramBundle).sendToTarget();
                localHandler.obtainMessage(103, paramInt, 0, paramBitmap).sendToTarget();
            }
        }

        public void setArtwork(int paramInt, Bitmap paramBitmap)
        {
            Handler localHandler = (Handler)this.mLocalHandler.get();
            if (localHandler != null)
                localHandler.obtainMessage(103, paramInt, 0, paramBitmap).sendToTarget();
        }

        public void setCurrentClientId(int paramInt, PendingIntent paramPendingIntent, boolean paramBoolean)
            throws RemoteException
        {
            Handler localHandler = (Handler)this.mLocalHandler.get();
            if (localHandler != null)
                if (!paramBoolean)
                    break label39;
            label39: for (int i = 1; ; i = 0)
            {
                localHandler.obtainMessage(104, paramInt, i, paramPendingIntent).sendToTarget();
                return;
            }
        }

        public void setMetadata(int paramInt, Bundle paramBundle)
        {
            Handler localHandler = (Handler)this.mLocalHandler.get();
            if (localHandler != null)
                localHandler.obtainMessage(101, paramInt, 0, paramBundle).sendToTarget();
        }

        public void setPlaybackState(int paramInt1, int paramInt2, long paramLong)
        {
            Handler localHandler = (Handler)this.mLocalHandler.get();
            if (localHandler != null)
                localHandler.obtainMessage(100, paramInt1, paramInt2).sendToTarget();
        }

        public void setTransportControlFlags(int paramInt1, int paramInt2)
        {
            Handler localHandler = (Handler)this.mLocalHandler.get();
            if (localHandler != null)
                localHandler.obtainMessage(102, paramInt1, paramInt2).sendToTarget();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.TransportControlView
 * JD-Core Version:        0.6.2
 */