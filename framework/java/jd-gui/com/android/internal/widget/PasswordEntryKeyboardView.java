package com.android.internal.widget;

import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;

public class PasswordEntryKeyboardView extends KeyboardView
{
    static final int KEYCODE_F1 = -103;
    static final int KEYCODE_NEXT_LANGUAGE = -104;
    static final int KEYCODE_OPTIONS = -100;
    static final int KEYCODE_SHIFT_LONGPRESS = -101;
    static final int KEYCODE_VOICE = -102;

    public PasswordEntryKeyboardView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public PasswordEntryKeyboardView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public boolean setShifted(boolean paramBoolean)
    {
        boolean bool = super.setShifted(paramBoolean);
        int[] arrayOfInt = getKeyboard().getShiftKeyIndices();
        int i = arrayOfInt.length;
        for (int j = 0; j < i; j++)
            invalidateKey(arrayOfInt[j]);
        return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.PasswordEntryKeyboardView
 * JD-Core Version:        0.6.2
 */