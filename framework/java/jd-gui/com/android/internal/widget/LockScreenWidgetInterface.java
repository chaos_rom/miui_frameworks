package com.android.internal.widget;

public abstract interface LockScreenWidgetInterface
{
    public abstract boolean providesClock();

    public abstract void setCallback(LockScreenWidgetCallback paramLockScreenWidgetCallback);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.LockScreenWidgetInterface
 * JD-Core Version:        0.6.2
 */