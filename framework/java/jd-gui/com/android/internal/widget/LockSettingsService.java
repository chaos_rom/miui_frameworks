package com.android.internal.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Binder;
import android.os.Environment;
import android.os.RemoteException;
import android.os.UserId;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Slog;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class LockSettingsService extends ILockSettings.Stub
{
    private static final String[] COLUMNS_FOR_QUERY;
    private static final String COLUMN_KEY = "name";
    private static final String COLUMN_USERID = "user";
    private static final String COLUMN_VALUE = "value";
    private static final String LOCK_PASSWORD_FILE = "password.key";
    private static final String LOCK_PATTERN_FILE = "gesture.key";
    private static final String SYSTEM_DIRECTORY = "/system/";
    private static final String TABLE = "locksettings";
    private static final String TAG = "LockSettingsService";
    private static final String[] VALID_SETTINGS = arrayOfString2;
    private final Context mContext;
    private final DatabaseHelper mOpenHelper;

    static
    {
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "value";
        COLUMNS_FOR_QUERY = arrayOfString1;
        String[] arrayOfString2 = new String[16];
        arrayOfString2[0] = "lockscreen.lockedoutpermanently";
        arrayOfString2[1] = "lockscreen.lockoutattemptdeadline";
        arrayOfString2[2] = "lockscreen.patterneverchosen";
        arrayOfString2[3] = "lockscreen.password_type";
        arrayOfString2[4] = "lockscreen.password_type_alternate";
        arrayOfString2[5] = "lockscreen.password_salt";
        arrayOfString2[6] = "lockscreen.disabled";
        arrayOfString2[7] = "lockscreen.options";
        arrayOfString2[8] = "lockscreen.biometric_weak_fallback";
        arrayOfString2[9] = "lockscreen.biometricweakeverchosen";
        arrayOfString2[10] = "lockscreen.power_button_instantly_locks";
        arrayOfString2[11] = "lockscreen.passwordhistory";
        arrayOfString2[12] = "lock_pattern_autolock";
        arrayOfString2[13] = "lock_biometric_weak_flags";
        arrayOfString2[14] = "lock_pattern_visible_pattern";
        arrayOfString2[15] = "lock_pattern_tactile_feedback_enabled";
    }

    public LockSettingsService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mOpenHelper = new DatabaseHelper(this.mContext);
    }

    private static final void checkPasswordReadPermission(int paramInt)
    {
        int i = Binder.getCallingUid();
        if (UserId.getAppId(i) != 1000)
            throw new SecurityException("uid=" + i + " not authorized to read lock password");
    }

    private static final void checkReadPermission(int paramInt)
    {
        int i = Binder.getCallingUid();
        if ((UserId.getAppId(i) != 1000) && (UserId.getUserId(i) != paramInt))
            throw new SecurityException("uid=" + i + " not authorized to read settings of user " + paramInt);
    }

    private static final void checkWritePermission(int paramInt)
    {
        int i = Binder.getCallingUid();
        if (UserId.getAppId(i) != 1000)
            throw new SecurityException("uid=" + i + " not authorized to write lock settings");
    }

    private String getLockPasswordFilename(int paramInt)
    {
        String str1 = Environment.getDataDirectory().getAbsolutePath() + "/system/";
        if (paramInt == 0);
        for (String str2 = str1 + "password.key"; ; str2 = str1 + "users/" + paramInt + "/" + "password.key")
            return str2;
    }

    private String getLockPatternFilename(int paramInt)
    {
        String str1 = Environment.getDataDirectory().getAbsolutePath() + "/system/";
        if (paramInt == 0);
        for (String str2 = str1 + "gesture.key"; ; str2 = str1 + "users/" + paramInt + "/" + "gesture.key")
            return str2;
    }

    private void migrateOldData()
    {
        while (true)
        {
            int j;
            try
            {
                if (getString("migrated", null, 0) == null)
                {
                    ContentResolver localContentResolver = this.mContext.getContentResolver();
                    String[] arrayOfString = VALID_SETTINGS;
                    int i = arrayOfString.length;
                    j = 0;
                    if (j < i)
                    {
                        String str1 = arrayOfString[j];
                        String str2 = Settings.Secure.getString(localContentResolver, str1);
                        if (str2 == null)
                            break label104;
                        setString(str1, str2, 0);
                        break label104;
                    }
                    setString("migrated", "true", 0);
                    Slog.i("LockSettingsService", "Migrated lock settings to new location");
                }
            }
            catch (RemoteException localRemoteException)
            {
                Slog.e("LockSettingsService", "Unable to migrate old data");
            }
            return;
            label104: j++;
        }
    }

    private String readFromDb(String paramString1, String paramString2, int paramInt)
    {
        String str = paramString2;
        SQLiteDatabase localSQLiteDatabase = this.mOpenHelper.getReadableDatabase();
        String[] arrayOfString1 = COLUMNS_FOR_QUERY;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = Integer.toString(paramInt);
        arrayOfString2[1] = paramString1;
        Cursor localCursor = localSQLiteDatabase.query("locksettings", arrayOfString1, "user=? AND name=?", arrayOfString2, null, null, null);
        if (localCursor != null)
        {
            if (localCursor.moveToFirst())
                str = localCursor.getString(0);
            localCursor.close();
        }
        return str;
    }

    private void writeFile(String paramString, byte[] paramArrayOfByte)
    {
        try
        {
            RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramString, "rw");
            if ((paramArrayOfByte == null) || (paramArrayOfByte.length == 0))
                localRandomAccessFile.setLength(0L);
            while (true)
            {
                localRandomAccessFile.close();
                break;
                localRandomAccessFile.write(paramArrayOfByte, 0, paramArrayOfByte.length);
            }
        }
        catch (IOException localIOException)
        {
            Slog.e("LockSettingsService", "Error writing to file " + localIOException);
        }
    }

    private void writeToDb(String paramString1, String paramString2, int paramInt)
    {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("name", paramString1);
        localContentValues.put("user", Integer.valueOf(paramInt));
        localContentValues.put("value", paramString2);
        SQLiteDatabase localSQLiteDatabase = this.mOpenHelper.getWritableDatabase();
        localSQLiteDatabase.beginTransaction();
        try
        {
            String[] arrayOfString = new String[2];
            arrayOfString[0] = paramString1;
            arrayOfString[1] = Integer.toString(paramInt);
            localSQLiteDatabase.delete("locksettings", "name=? AND user=?", arrayOfString);
            localSQLiteDatabase.insert("locksettings", null, localContentValues);
            localSQLiteDatabase.setTransactionSuccessful();
            return;
        }
        finally
        {
            localSQLiteDatabase.endTransaction();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean checkPassword(byte[] paramArrayOfByte, int paramInt)
        throws RemoteException
    {
        boolean bool1 = true;
        checkPasswordReadPermission(paramInt);
        try
        {
            RandomAccessFile localRandomAccessFile = new RandomAccessFile(getLockPasswordFilename(paramInt), "r");
            byte[] arrayOfByte = new byte[(int)localRandomAccessFile.length()];
            int i = localRandomAccessFile.read(arrayOfByte, 0, arrayOfByte.length);
            localRandomAccessFile.close();
            if (i > 0)
            {
                boolean bool2 = Arrays.equals(arrayOfByte, Injector.passwordToHash(arrayOfByte, paramArrayOfByte));
                bool1 = bool2;
            }
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            Slog.e("LockSettingsService", "Cannot read file " + localFileNotFoundException);
        }
        catch (IOException localIOException)
        {
            Slog.e("LockSettingsService", "Cannot read file " + localIOException);
        }
        return bool1;
    }

    public boolean checkPattern(byte[] paramArrayOfByte, int paramInt)
        throws RemoteException
    {
        boolean bool1 = true;
        checkPasswordReadPermission(paramInt);
        try
        {
            RandomAccessFile localRandomAccessFile = new RandomAccessFile(getLockPatternFilename(paramInt), "r");
            byte[] arrayOfByte = new byte[(int)localRandomAccessFile.length()];
            int i = localRandomAccessFile.read(arrayOfByte, 0, arrayOfByte.length);
            localRandomAccessFile.close();
            if (i > 0)
            {
                boolean bool2 = Arrays.equals(arrayOfByte, paramArrayOfByte);
                bool1 = bool2;
            }
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            Slog.e("LockSettingsService", "Cannot read file " + localFileNotFoundException);
        }
        catch (IOException localIOException)
        {
            Slog.e("LockSettingsService", "Cannot read file " + localIOException);
        }
        return bool1;
    }

    public boolean getBoolean(String paramString, boolean paramBoolean, int paramInt)
        throws RemoteException
    {
        String str = readFromDb(paramString, null, paramInt);
        if (TextUtils.isEmpty(str));
        while (true)
        {
            return paramBoolean;
            if ((str.equals("1")) || (str.equals("true")))
                paramBoolean = true;
            else
                paramBoolean = false;
        }
    }

    public long getLong(String paramString, long paramLong, int paramInt)
        throws RemoteException
    {
        String str = readFromDb(paramString, null, paramInt);
        if (TextUtils.isEmpty(str));
        while (true)
        {
            return paramLong;
            paramLong = Long.parseLong(str);
        }
    }

    public String getString(String paramString1, String paramString2, int paramInt)
        throws RemoteException
    {
        return readFromDb(paramString1, paramString2, paramInt);
    }

    public boolean havePassword(int paramInt)
        throws RemoteException
    {
        if (new File(getLockPasswordFilename(paramInt)).length() > 0L);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean havePattern(int paramInt)
        throws RemoteException
    {
        if (new File(getLockPatternFilename(paramInt)).length() > 0L);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void removeUser(int paramInt)
    {
        checkWritePermission(paramInt);
        SQLiteDatabase localSQLiteDatabase = this.mOpenHelper.getWritableDatabase();
        try
        {
            File localFile1 = new File(getLockPasswordFilename(paramInt));
            if (localFile1.exists())
                localFile1.delete();
            File localFile2 = new File(getLockPatternFilename(paramInt));
            if (localFile2.exists())
                localFile2.delete();
            localSQLiteDatabase.beginTransaction();
            localSQLiteDatabase.delete("locksettings", "user='" + paramInt + "'", null);
            localSQLiteDatabase.setTransactionSuccessful();
            return;
        }
        finally
        {
            localSQLiteDatabase.endTransaction();
        }
    }

    public void setBoolean(String paramString, boolean paramBoolean, int paramInt)
        throws RemoteException
    {
        checkWritePermission(paramInt);
        if (paramBoolean);
        for (String str = "1"; ; str = "0")
        {
            writeToDb(paramString, str, paramInt);
            return;
        }
    }

    public void setLockPassword(byte[] paramArrayOfByte, int paramInt)
        throws RemoteException
    {
        checkWritePermission(paramInt);
        writeFile(getLockPasswordFilename(paramInt), paramArrayOfByte);
    }

    public void setLockPattern(byte[] paramArrayOfByte, int paramInt)
        throws RemoteException
    {
        checkWritePermission(paramInt);
        writeFile(getLockPatternFilename(paramInt), paramArrayOfByte);
    }

    public void setLong(String paramString, long paramLong, int paramInt)
        throws RemoteException
    {
        checkWritePermission(paramInt);
        writeToDb(paramString, Long.toString(paramLong), paramInt);
    }

    public void setString(String paramString1, String paramString2, int paramInt)
        throws RemoteException
    {
        checkWritePermission(paramInt);
        writeToDb(paramString1, paramString2, paramInt);
    }

    public void systemReady()
    {
        migrateOldData();
    }

    class DatabaseHelper extends SQLiteOpenHelper
    {
        private static final String DATABASE_NAME = "locksettings.db";
        private static final int DATABASE_VERSION = 1;
        private static final String TAG = "LockSettingsDB";

        public DatabaseHelper(Context arg2)
        {
            super("locksettings.db", null, 1);
            setWriteAheadLoggingEnabled(true);
        }

        private void createTable(SQLiteDatabase paramSQLiteDatabase)
        {
            paramSQLiteDatabase.execSQL("CREATE TABLE locksettings (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,user INTEGER,value TEXT);");
        }

        public void onCreate(SQLiteDatabase paramSQLiteDatabase)
        {
            createTable(paramSQLiteDatabase);
        }

        public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
        {
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static byte[] passwordToHash(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
        {
            Object localObject;
            if ((paramArrayOfByte2 == null) || (paramArrayOfByte1.length == 72))
                localObject = paramArrayOfByte2;
            while (true)
            {
                return localObject;
                localObject = null;
                try
                {
                    localObject = MessageDigest.getInstance("MD5").digest(paramArrayOfByte2);
                    byte[] arrayOfByte = toHex((byte[])localObject).getBytes();
                    localObject = arrayOfByte;
                }
                catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
                {
                }
            }
        }

        static String toHex(byte[] paramArrayOfByte)
        {
            String str1 = "";
            for (int i = 0; i < paramArrayOfByte.length; i++)
            {
                String str2 = str1 + "0123456789ABCDEF".charAt(0xF & paramArrayOfByte[i] >> 4);
                str1 = str2 + "0123456789ABCDEF".charAt(0xF & paramArrayOfByte[i]);
            }
            return str1;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.LockSettingsService
 * JD-Core Version:        0.6.2
 */