package com.android.internal.widget;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.admin.DevicePolicyManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.UserId;
import android.os.storage.IMountService;
import android.os.storage.IMountService.Stub;
import android.provider.Settings.System;
import android.security.KeyStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephony.Stub;
import com.google.android.collect.Lists;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class LockPatternUtils
{
    public static final String BIOMETRIC_WEAK_EVER_CHOSEN_KEY = "lockscreen.biometricweakeverchosen";
    protected static final String DISABLE_LOCKSCREEN_KEY = "lockscreen.disabled";
    public static final int FAILED_ATTEMPTS_BEFORE_RESET = 20;
    public static final int FAILED_ATTEMPTS_BEFORE_TIMEOUT = 5;
    public static final int FAILED_ATTEMPTS_BEFORE_WIPE_GRACE = 5;
    public static final long FAILED_ATTEMPT_COUNTDOWN_INTERVAL_MS = 1000L;
    public static final long FAILED_ATTEMPT_TIMEOUT_MS = 30000L;
    public static final int FLAG_BIOMETRIC_WEAK_LIVELINESS = 1;
    protected static final String LOCKOUT_ATTEMPT_DEADLINE = "lockscreen.lockoutattemptdeadline";
    protected static final String LOCKOUT_PERMANENT_KEY = "lockscreen.lockedoutpermanently";
    public static final String LOCKSCREEN_BIOMETRIC_WEAK_FALLBACK = "lockscreen.biometric_weak_fallback";
    protected static final String LOCKSCREEN_OPTIONS = "lockscreen.options";
    public static final String LOCKSCREEN_POWER_BUTTON_INSTANTLY_LOCKS = "lockscreen.power_button_instantly_locks";
    protected static final String LOCK_PASSWORD_SALT_KEY = "lockscreen.password_salt";
    public static final int MIN_LOCK_PATTERN_SIZE = 4;
    public static final int MIN_PATTERN_REGISTER_FAIL = 4;
    private static final String OPTION_ENABLE_FACELOCK = "enable_facelock";
    protected static final String PASSWORD_HISTORY_KEY = "lockscreen.passwordhistory";
    public static final String PASSWORD_TYPE_ALTERNATE_KEY = "lockscreen.password_type_alternate";
    public static final String PASSWORD_TYPE_KEY = "lockscreen.password_type";
    protected static final String PATTERN_EVER_CHOSEN_KEY = "lockscreen.patterneverchosen";
    private static final String TAG = "LockPatternUtils";
    private final ContentResolver mContentResolver;
    private final Context mContext;
    private int mCurrentUserId = 0;
    private DevicePolicyManager mDevicePolicyManager;
    private ILockSettings mLockSettingsService;

    public LockPatternUtils(Context paramContext)
    {
        this.mContext = paramContext;
        this.mContentResolver = paramContext.getContentResolver();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public static byte[] callPatternToHash(List<LockPatternView.Cell> paramList)
    {
        return patternToHash(paramList);
    }

    public static int computePasswordQuality(String paramString)
    {
        int i = 0;
        int j = 0;
        int k = paramString.length();
        int m = 0;
        if (m < k)
        {
            if (Character.isDigit(paramString.charAt(m)))
                i = 1;
            while (true)
            {
                m++;
                break;
                j = 1;
            }
        }
        int n;
        if ((j != 0) && (i != 0))
            n = 327680;
        while (true)
        {
            return n;
            if (j != 0)
                n = 262144;
            else if (i != 0)
                n = 131072;
            else
                n = 0;
        }
    }

    private void finishBiometricWeak()
    {
        setBoolean("lockscreen.biometricweakeverchosen", true);
        Intent localIntent = new Intent();
        localIntent.setClassName("com.android.facelock", "com.android.facelock.SetupEndScreen");
        this.mContext.startActivity(localIntent);
    }

    private boolean getBoolean(String paramString, boolean paramBoolean)
    {
        try
        {
            boolean bool = getLockSettings().getBoolean(paramString, paramBoolean, getCurrentOrCallingUserId());
            paramBoolean = bool;
            label20: return paramBoolean;
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    private int getCurrentOrCallingUserId()
    {
        int i = Binder.getCallingUid();
        if (i == 1000);
        for (int j = this.mCurrentUserId; ; j = UserId.getUserId(i))
            return j;
    }

    private ILockSettings getLockSettings()
    {
        if (this.mLockSettingsService == null)
            this.mLockSettingsService = ILockSettings.Stub.asInterface(ServiceManager.getService("lock_settings"));
        return this.mLockSettingsService;
    }

    private long getLong(String paramString, long paramLong)
    {
        try
        {
            long l = getLockSettings().getLong(paramString, paramLong, getCurrentOrCallingUserId());
            paramLong = l;
            label20: return paramLong;
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    private String getSalt()
    {
        long l = getLong("lockscreen.password_salt", 0L);
        if (l == 0L);
        try
        {
            l = SecureRandom.getInstance("SHA1PRNG").nextLong();
            setLong("lockscreen.password_salt", l);
            Log.v("LockPatternUtils", "Initialized lock password salt");
            return Long.toHexString(l);
        }
        catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
        {
            throw new IllegalStateException("Couldn't get SecureRandom number", localNoSuchAlgorithmException);
        }
    }

    private String getString(String paramString)
    {
        Object localObject = null;
        try
        {
            String str = getLockSettings().getString(paramString, null, getCurrentOrCallingUserId());
            localObject = str;
            label22: return localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label22;
        }
    }

    private static byte[] patternToHash(List<LockPatternView.Cell> paramList)
    {
        Object localObject;
        if (paramList == null)
            localObject = null;
        while (true)
        {
            return localObject;
            int i = paramList.size();
            byte[] arrayOfByte1 = new byte[i];
            for (int j = 0; j < i; j++)
            {
                LockPatternView.Cell localCell = (LockPatternView.Cell)paramList.get(j);
                arrayOfByte1[j] = ((byte)(3 * localCell.getRow() + localCell.getColumn()));
            }
            try
            {
                byte[] arrayOfByte2 = MessageDigest.getInstance("SHA-1").digest(arrayOfByte1);
                localObject = arrayOfByte2;
            }
            catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
            {
                localObject = arrayOfByte1;
            }
        }
    }

    public static String patternToString(List<LockPatternView.Cell> paramList)
    {
        if (paramList == null);
        byte[] arrayOfByte;
        for (String str = ""; ; str = new String(arrayOfByte))
        {
            return str;
            int i = paramList.size();
            arrayOfByte = new byte[i];
            for (int j = 0; j < i; j++)
            {
                LockPatternView.Cell localCell = (LockPatternView.Cell)paramList.get(j);
                arrayOfByte[j] = ((byte)(3 * localCell.getRow() + localCell.getColumn()));
            }
        }
    }

    private void setBoolean(String paramString, boolean paramBoolean)
    {
        try
        {
            getLockSettings().setBoolean(paramString, paramBoolean, getCurrentOrCallingUserId());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LockPatternUtils", "Couldn't write boolean " + paramString + localRemoteException);
        }
    }

    private void setLong(String paramString, long paramLong)
    {
        try
        {
            getLockSettings().setLong(paramString, paramLong, getCurrentOrCallingUserId());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LockPatternUtils", "Couldn't write long " + paramString + localRemoteException);
        }
    }

    private void setString(String paramString1, String paramString2)
    {
        try
        {
            getLockSettings().setString(paramString1, paramString2, getCurrentOrCallingUserId());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LockPatternUtils", "Couldn't write string " + paramString1 + localRemoteException);
        }
    }

    public static List<LockPatternView.Cell> stringToPattern(String paramString)
    {
        ArrayList localArrayList = Lists.newArrayList();
        byte[] arrayOfByte = paramString.getBytes();
        for (int i = 0; i < arrayOfByte.length; i++)
        {
            int j = arrayOfByte[i];
            localArrayList.add(LockPatternView.Cell.of(j / 3, j % 3));
        }
        return localArrayList;
    }

    private static String toHex(byte[] paramArrayOfByte)
    {
        String str1 = "";
        for (int i = 0; i < paramArrayOfByte.length; i++)
        {
            String str2 = str1 + "0123456789ABCDEF".charAt(0xF & paramArrayOfByte[i] >> 4);
            str1 = str2 + "0123456789ABCDEF".charAt(0xF & paramArrayOfByte[i]);
        }
        return str1;
    }

    private void updateEncryptionPassword(String paramString)
    {
        if (getDevicePolicyManager().getStorageEncryptionStatus() != 3);
        while (true)
        {
            return;
            IBinder localIBinder = ServiceManager.getService("mount");
            if (localIBinder == null)
            {
                Log.e("LockPatternUtils", "Could not find the mount service to update the encryption password");
            }
            else
            {
                IMountService localIMountService = IMountService.Stub.asInterface(localIBinder);
                try
                {
                    localIMountService.changeEncryptionPassword(paramString);
                }
                catch (RemoteException localRemoteException)
                {
                    Log.e("LockPatternUtils", "Error changing encryption password", localRemoteException);
                }
            }
        }
    }

    public boolean checkPassword(String paramString)
    {
        int i = getCurrentOrCallingUserId();
        try
        {
            boolean bool2 = getLockSettings().checkPassword(passwordToHash(paramString), i);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = true;
        }
    }

    public boolean checkPasswordHistory(String paramString)
    {
        boolean bool = false;
        String str1 = new String(passwordToHash(paramString));
        String str2 = getString("lockscreen.passwordhistory");
        if (str2 == null);
        while (true)
        {
            return bool;
            int i = str1.length();
            int j = getRequestedPasswordHistoryLength();
            if (j != 0)
            {
                int k = -1 + (j + i * j);
                if (str2.length() > k)
                    str2 = str2.substring(0, k);
                bool = str2.contains(str1);
            }
        }
    }

    public boolean checkPattern(List<LockPatternView.Cell> paramList)
    {
        int i = getCurrentOrCallingUserId();
        try
        {
            boolean bool2 = getLockSettings().checkPattern(patternToHash(paramList), i);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = true;
        }
    }

    public void clearLock(boolean paramBoolean)
    {
        if (!paramBoolean)
            deleteGallery();
        saveLockPassword(null, 65536);
        setLockPatternEnabled(false);
        saveLockPattern(null);
        setLong("lockscreen.password_type", 0L);
        setLong("lockscreen.password_type_alternate", 0L);
    }

    void deleteGallery()
    {
        if (usingBiometricWeak())
        {
            Intent localIntent = new Intent().setAction("com.android.facelock.DELETE_GALLERY");
            localIntent.putExtra("deleteGallery", true);
            this.mContext.sendBroadcast(localIntent);
        }
    }

    public void deleteTempGallery()
    {
        Intent localIntent = new Intent().setAction("com.android.facelock.DELETE_GALLERY");
        localIntent.putExtra("deleteTempGallery", true);
        this.mContext.sendBroadcast(localIntent);
    }

    public int getActivePasswordQuality()
    {
        int i = 0;
        switch ((int)getLong("lockscreen.password_type", 65536L))
        {
        default:
        case 65536:
        case 32768:
        case 131072:
        case 262144:
        case 327680:
        case 393216:
        }
        while (true)
        {
            return i;
            if (isLockPatternEnabled())
            {
                i = 65536;
                continue;
                if (isBiometricWeakInstalled())
                {
                    i = 32768;
                    continue;
                    if (isLockPasswordEnabled())
                    {
                        i = 131072;
                        continue;
                        if (isLockPasswordEnabled())
                        {
                            i = 262144;
                            continue;
                            if (isLockPasswordEnabled())
                            {
                                i = 327680;
                                continue;
                                if (isLockPasswordEnabled())
                                    i = 393216;
                            }
                        }
                    }
                }
            }
        }
    }

    public int getCurrentUser()
    {
        if (Process.myUid() == 1000)
            return this.mCurrentUserId;
        throw new SecurityException("Only the system process can get the current user");
    }

    public DevicePolicyManager getDevicePolicyManager()
    {
        if (this.mDevicePolicyManager == null)
        {
            this.mDevicePolicyManager = ((DevicePolicyManager)this.mContext.getSystemService("device_policy"));
            if (this.mDevicePolicyManager == null)
                Log.e("LockPatternUtils", "Can't get DevicePolicyManagerService: is it running?", new IllegalStateException("Stack trace:"));
        }
        return this.mDevicePolicyManager;
    }

    public int getKeyguardStoredPasswordQuality()
    {
        int i = (int)getLong("lockscreen.password_type", 65536L);
        if (i == 32768)
            i = (int)getLong("lockscreen.password_type_alternate", 65536L);
        return i;
    }

    public long getLockoutAttemptDeadline()
    {
        long l1 = getLong("lockscreen.lockoutattemptdeadline", 0L);
        long l2 = SystemClock.elapsedRealtime();
        if ((l1 < l2) || (l1 > 30000L + l2))
            l1 = 0L;
        return l1;
    }

    public String getNextAlarm()
    {
        String str = Settings.System.getString(this.mContentResolver, "next_alarm_formatted");
        if ((str == null) || (TextUtils.isEmpty(str)))
            str = null;
        return str;
    }

    public boolean getPowerButtonInstantlyLocks()
    {
        return getBoolean("lockscreen.power_button_instantly_locks", true);
    }

    public int getRequestedMinimumPasswordLength()
    {
        return getDevicePolicyManager().getPasswordMinimumLength(null);
    }

    public int getRequestedPasswordHistoryLength()
    {
        return getDevicePolicyManager().getPasswordHistoryLength(null);
    }

    public int getRequestedPasswordMinimumLetters()
    {
        return getDevicePolicyManager().getPasswordMinimumLetters(null);
    }

    public int getRequestedPasswordMinimumLowerCase()
    {
        return getDevicePolicyManager().getPasswordMinimumLowerCase(null);
    }

    public int getRequestedPasswordMinimumNonLetter()
    {
        return getDevicePolicyManager().getPasswordMinimumNonLetter(null);
    }

    public int getRequestedPasswordMinimumNumeric()
    {
        return getDevicePolicyManager().getPasswordMinimumNumeric(null);
    }

    public int getRequestedPasswordMinimumSymbols()
    {
        return getDevicePolicyManager().getPasswordMinimumSymbols(null);
    }

    public int getRequestedPasswordMinimumUpperCase()
    {
        return getDevicePolicyManager().getPasswordMinimumUpperCase(null);
    }

    public int getRequestedPasswordQuality()
    {
        return getDevicePolicyManager().getPasswordQuality(null);
    }

    public boolean isBiometricWeakEverChosen()
    {
        return getBoolean("lockscreen.biometricweakeverchosen", false);
    }

    public boolean isBiometricWeakInstalled()
    {
        boolean bool = false;
        PackageManager localPackageManager = this.mContext.getPackageManager();
        try
        {
            localPackageManager.getPackageInfo("com.android.facelock", 1);
            if (!localPackageManager.hasSystemFeature("android.hardware.camera.front"))
                return bool;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
            {
                continue;
                if (!getDevicePolicyManager().getCameraDisabled(null))
                    bool = true;
            }
        }
    }

    public boolean isBiometricWeakLivelinessEnabled()
    {
        if ((1L & getLong("lock_biometric_weak_flags", 0L)) != 0L);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEmergencyCallCapable()
    {
        return this.mContext.getResources().getBoolean(17891368);
    }

    public boolean isEmergencyCallEnabledWhileSimLocked()
    {
        return this.mContext.getResources().getBoolean(17891362);
    }

    public boolean isLockPasswordEnabled()
    {
        boolean bool1 = true;
        long l1 = getLong("lockscreen.password_type", 0L);
        long l2 = getLong("lockscreen.password_type_alternate", 0L);
        boolean bool2;
        boolean bool3;
        if ((l1 == 262144L) || (l1 == 131072L) || (l1 == 327680L) || (l1 == 393216L))
        {
            bool2 = bool1;
            if ((l2 != 262144L) && (l2 != 131072L) && (l2 != 327680L) && (l2 != 393216L))
                break label125;
            bool3 = bool1;
            label93: if ((!savedPasswordExists()) || ((!bool2) && ((!usingBiometricWeak()) || (!bool3))))
                break label131;
        }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            label125: bool3 = false;
            break label93;
            label131: bool1 = false;
        }
    }

    public boolean isLockPatternEnabled()
    {
        boolean bool1 = true;
        boolean bool2;
        if (getLong("lockscreen.password_type_alternate", 65536L) == 65536L)
        {
            bool2 = bool1;
            if ((!getBoolean("lock_pattern_autolock", false)) || ((getLong("lockscreen.password_type", 65536L) != 65536L) && ((!usingBiometricWeak()) || (!bool2))))
                break label65;
        }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            label65: bool1 = false;
        }
    }

    public boolean isLockScreenDisabled()
    {
        if ((!isSecure()) && (getLong("lockscreen.disabled", 0L) != 0L));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPatternEverChosen()
    {
        return getBoolean("lockscreen.patterneverchosen", false);
    }

    public boolean isPermanentlyLocked()
    {
        return getBoolean("lockscreen.lockedoutpermanently", false);
    }

    public boolean isPukUnlockScreenEnable()
    {
        return this.mContext.getResources().getBoolean(17891361);
    }

    public boolean isSecure()
    {
        boolean bool1 = true;
        long l = getKeyguardStoredPasswordQuality();
        boolean bool2;
        boolean bool3;
        if (l == 65536L)
        {
            bool2 = bool1;
            if ((l != 131072L) && (l != 262144L) && (l != 327680L) && (l != 393216L))
                break label93;
            bool3 = bool1;
            label54: if (((!bool2) || (!isLockPatternEnabled()) || (!savedPatternExists())) && ((!bool3) || (!savedPasswordExists())))
                break label99;
        }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            label93: bool3 = false;
            break label54;
            label99: bool1 = false;
        }
    }

    public boolean isTactileFeedbackEnabled()
    {
        return getBoolean("lock_pattern_tactile_feedback_enabled", false);
    }

    public boolean isVisiblePatternEnabled()
    {
        return getBoolean("lock_pattern_visible_pattern", false);
    }

    public byte[] passwordToHash(String paramString)
    {
        Object localObject;
        if (paramString == null)
            localObject = null;
        while (true)
        {
            return localObject;
            String str = null;
            localObject = null;
            try
            {
                byte[] arrayOfByte1 = (paramString + getSalt()).getBytes();
                str = "SHA-1";
                byte[] arrayOfByte2 = MessageDigest.getInstance(str).digest(arrayOfByte1);
                str = "MD5";
                byte[] arrayOfByte3 = MessageDigest.getInstance(str).digest(arrayOfByte1);
                byte[] arrayOfByte4 = (toHex(arrayOfByte2) + toHex(arrayOfByte3)).getBytes();
                localObject = arrayOfByte4;
            }
            catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
            {
                Log.w("LockPatternUtils", "Failed to encode string because of missing algorithm: " + str);
            }
        }
    }

    public void removeUser(int paramInt)
    {
        if (Process.myUid() == 1000);
        try
        {
            getLockSettings().removeUser(paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("LockPatternUtils", "Couldn't remove lock settings for user " + paramInt);
        }
    }

    public void reportFailedPasswordAttempt()
    {
        getDevicePolicyManager().reportFailedPasswordAttempt();
    }

    public void reportSuccessfulPasswordAttempt()
    {
        getDevicePolicyManager().reportSuccessfulPasswordAttempt();
    }

    public boolean resumeCall()
    {
        ITelephony localITelephony = ITelephony.Stub.asInterface(ServiceManager.checkService("phone"));
        if (localITelephony != null);
        while (true)
        {
            try
            {
                boolean bool2 = localITelephony.showCallScreen();
                if (bool2)
                {
                    bool1 = true;
                    return bool1;
                }
            }
            catch (RemoteException localRemoteException)
            {
            }
            boolean bool1 = false;
        }
    }

    public void saveLockPassword(String paramString, int paramInt)
    {
        saveLockPassword(paramString, paramInt, false);
    }

    public void saveLockPassword(String paramString, int paramInt, boolean paramBoolean)
    {
        byte[] arrayOfByte = passwordToHash(paramString);
        try
        {
            getLockSettings().setLockPassword(arrayOfByte, getCurrentOrCallingUserId());
            localDevicePolicyManager = getDevicePolicyManager();
            localKeyStore = KeyStore.getInstance();
            if (paramString != null)
            {
                updateEncryptionPassword(paramString);
                localKeyStore.password(paramString);
                i = computePasswordQuality(paramString);
                if (!paramBoolean)
                {
                    deleteGallery();
                    setLong("lockscreen.password_type", Math.max(paramInt, i));
                    if (i != 0)
                    {
                        k = 0;
                        int m = 0;
                        n = 0;
                        i1 = 0;
                        i2 = 0;
                        i3 = 0;
                        i4 = 0;
                        int i5 = paramString.length();
                        if (i4 < i5)
                        {
                            i6 = paramString.charAt(i4);
                            if ((i6 < 65) || (i6 > 90))
                                break label416;
                            k++;
                            m++;
                            break label410;
                        }
                        localDevicePolicyManager.setActivePasswordState(Math.max(paramInt, i), paramString.length(), k, m, n, i1, i2, i3);
                    }
                    while (true)
                    {
                        str1 = getString("lockscreen.passwordhistory");
                        if (str1 == null)
                            str1 = new String();
                        j = getRequestedPasswordHistoryLength();
                        if (j != 0)
                            break;
                        str3 = "";
                        setString("lockscreen.passwordhistory", str3);
                        return;
                        localDevicePolicyManager.setActivePasswordState(0, 0, 0, 0, 0, 0, 0, 0);
                    }
                }
            }
        }
        catch (RemoteException localRemoteException)
        {
            DevicePolicyManager localDevicePolicyManager;
            KeyStore localKeyStore;
            int k;
            int n;
            int i1;
            int i2;
            int i3;
            int i4;
            int i6;
            while (true)
            {
                int i;
                String str1;
                int j;
                Log.e("LockPatternUtils", "Unable to save lock password " + localRemoteException);
                break;
                setLong("lockscreen.password_type", 32768L);
                setLong("lockscreen.password_type_alternate", Math.max(paramInt, i));
                finishBiometricWeak();
                localDevicePolicyManager.setActivePasswordState(32768, 0, 0, 0, 0, 0, 0, 0);
                continue;
                String str2 = new String(arrayOfByte) + "," + str1;
                String str3 = str2.substring(0, Math.min(-1 + (j + j * arrayOfByte.length), str2.length()));
            }
            if (localKeyStore.isEmpty())
                localKeyStore.reset();
            localDevicePolicyManager.setActivePasswordState(0, 0, 0, 0, 0, 0, 0, 0);
            return;
            while (true)
            {
                label410: i4++;
                break;
                label416: if ((i6 >= 97) && (i6 <= 122))
                {
                    k++;
                    n++;
                }
                else if ((i6 >= 48) && (i6 <= 57))
                {
                    i1++;
                    i3++;
                }
                else
                {
                    i2++;
                    i3++;
                }
            }
        }
    }

    public void saveLockPattern(List<LockPatternView.Cell> paramList)
    {
        saveLockPattern(paramList, false);
    }

    public void saveLockPattern(List<LockPatternView.Cell> paramList, boolean paramBoolean)
    {
        byte[] arrayOfByte = patternToHash(paramList);
        DevicePolicyManager localDevicePolicyManager;
        KeyStore localKeyStore;
        try
        {
            getLockSettings().setLockPattern(arrayOfByte, getCurrentOrCallingUserId());
            localDevicePolicyManager = getDevicePolicyManager();
            localKeyStore = KeyStore.getInstance();
            if (paramList != null)
            {
                localKeyStore.password(patternToString(paramList));
                setBoolean("lockscreen.patterneverchosen", true);
                if (!paramBoolean)
                {
                    deleteGallery();
                    setLong("lockscreen.password_type", 65536L);
                    localDevicePolicyManager.setActivePasswordState(65536, paramList.size(), 0, 0, 0, 0, 0, 0);
                }
                else
                {
                    setLong("lockscreen.password_type", 32768L);
                    setLong("lockscreen.password_type_alternate", 65536L);
                    finishBiometricWeak();
                    localDevicePolicyManager.setActivePasswordState(32768, 0, 0, 0, 0, 0, 0, 0);
                }
            }
        }
        catch (RemoteException localRemoteException)
        {
            Log.e("LockPatternUtils", "Couldn't save lock pattern " + localRemoteException);
        }
        if (localKeyStore.isEmpty())
            localKeyStore.reset();
        localDevicePolicyManager.setActivePasswordState(0, 0, 0, 0, 0, 0, 0, 0);
    }

    public boolean savedPasswordExists()
    {
        try
        {
            boolean bool2 = getLockSettings().havePassword(getCurrentOrCallingUserId());
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean savedPatternExists()
    {
        try
        {
            boolean bool2 = getLockSettings().havePattern(getCurrentOrCallingUserId());
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void setBiometricWeakLivelinessEnabled(boolean paramBoolean)
    {
        long l1 = getLong("lock_biometric_weak_flags", 0L);
        if (paramBoolean);
        for (long l2 = l1 | 1L; ; l2 = l1 & 0xFFFFFFFE)
        {
            setLong("lock_biometric_weak_flags", l2);
            return;
        }
    }

    public void setCurrentUser(int paramInt)
    {
        if (Process.myUid() == 1000)
        {
            this.mCurrentUserId = paramInt;
            return;
        }
        throw new SecurityException("Only the system process can set the current user");
    }

    public void setLockPatternEnabled(boolean paramBoolean)
    {
        setBoolean("lock_pattern_autolock", paramBoolean);
    }

    public void setLockScreenDisabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (long l = 1L; ; l = 0L)
        {
            setLong("lockscreen.disabled", l);
            return;
        }
    }

    public long setLockoutAttemptDeadline()
    {
        long l = 30000L + SystemClock.elapsedRealtime();
        setLong("lockscreen.lockoutattemptdeadline", l);
        return l;
    }

    public void setPermanentlyLocked(boolean paramBoolean)
    {
        setBoolean("lockscreen.lockedoutpermanently", paramBoolean);
    }

    public void setPowerButtonInstantlyLocks(boolean paramBoolean)
    {
        setBoolean("lockscreen.power_button_instantly_locks", paramBoolean);
    }

    public void setTactileFeedbackEnabled(boolean paramBoolean)
    {
        setBoolean("lock_pattern_tactile_feedback_enabled", paramBoolean);
    }

    public void setVisiblePatternEnabled(boolean paramBoolean)
    {
        setBoolean("lock_pattern_visible_pattern", paramBoolean);
    }

    public void updateEmergencyCallButtonState(Button paramButton, int paramInt, boolean paramBoolean)
    {
        int i;
        if ((isEmergencyCallCapable()) && (paramBoolean))
        {
            paramButton.setVisibility(0);
            if (paramInt != 2)
                break label52;
            i = 17040124;
            paramButton.setCompoundDrawablesWithIntrinsicBounds(17301636, 0, 0, 0);
        }
        while (true)
        {
            paramButton.setText(i);
            while (true)
            {
                return;
                paramButton.setVisibility(8);
            }
            label52: i = 17040123;
            paramButton.setCompoundDrawablesWithIntrinsicBounds(17302219, 0, 0, 0);
        }
    }

    public boolean usingBiometricWeak()
    {
        if ((int)getLong("lockscreen.password_type", 65536L) == 32768);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.widget.LockPatternUtils
 * JD-Core Version:        0.6.2
 */