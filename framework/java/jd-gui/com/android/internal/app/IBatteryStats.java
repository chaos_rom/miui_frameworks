package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.WorkSource;
import android.telephony.SignalStrength;

public abstract interface IBatteryStats extends IInterface
{
    public abstract long getAwakeTimeBattery()
        throws RemoteException;

    public abstract long getAwakeTimePlugged()
        throws RemoteException;

    public abstract byte[] getStatistics()
        throws RemoteException;

    public abstract void noteBluetoothOff()
        throws RemoteException;

    public abstract void noteBluetoothOn()
        throws RemoteException;

    public abstract void noteFullWifiLockAcquired(int paramInt)
        throws RemoteException;

    public abstract void noteFullWifiLockAcquiredFromSource(WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void noteFullWifiLockReleased(int paramInt)
        throws RemoteException;

    public abstract void noteFullWifiLockReleasedFromSource(WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void noteInputEvent()
        throws RemoteException;

    public abstract void noteNetworkInterfaceType(String paramString, int paramInt)
        throws RemoteException;

    public abstract void notePhoneDataConnectionState(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract void notePhoneOff()
        throws RemoteException;

    public abstract void notePhoneOn()
        throws RemoteException;

    public abstract void notePhoneSignalStrength(SignalStrength paramSignalStrength)
        throws RemoteException;

    public abstract void notePhoneState(int paramInt)
        throws RemoteException;

    public abstract void noteScanWifiLockAcquired(int paramInt)
        throws RemoteException;

    public abstract void noteScanWifiLockAcquiredFromSource(WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void noteScanWifiLockReleased(int paramInt)
        throws RemoteException;

    public abstract void noteScanWifiLockReleasedFromSource(WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void noteScreenBrightness(int paramInt)
        throws RemoteException;

    public abstract void noteScreenOff()
        throws RemoteException;

    public abstract void noteScreenOn()
        throws RemoteException;

    public abstract void noteStartGps(int paramInt)
        throws RemoteException;

    public abstract void noteStartSensor(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void noteStartWakelock(int paramInt1, int paramInt2, String paramString, int paramInt3)
        throws RemoteException;

    public abstract void noteStartWakelockFromSource(WorkSource paramWorkSource, int paramInt1, String paramString, int paramInt2)
        throws RemoteException;

    public abstract void noteStopGps(int paramInt)
        throws RemoteException;

    public abstract void noteStopSensor(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void noteStopWakelock(int paramInt1, int paramInt2, String paramString, int paramInt3)
        throws RemoteException;

    public abstract void noteStopWakelockFromSource(WorkSource paramWorkSource, int paramInt1, String paramString, int paramInt2)
        throws RemoteException;

    public abstract void noteUserActivity(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void noteWifiMulticastDisabled(int paramInt)
        throws RemoteException;

    public abstract void noteWifiMulticastDisabledFromSource(WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void noteWifiMulticastEnabled(int paramInt)
        throws RemoteException;

    public abstract void noteWifiMulticastEnabledFromSource(WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void noteWifiOff()
        throws RemoteException;

    public abstract void noteWifiOn()
        throws RemoteException;

    public abstract void noteWifiRunning(WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void noteWifiRunningChanged(WorkSource paramWorkSource1, WorkSource paramWorkSource2)
        throws RemoteException;

    public abstract void noteWifiStopped(WorkSource paramWorkSource)
        throws RemoteException;

    public abstract void setBatteryState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBatteryStats
    {
        private static final String DESCRIPTOR = "com.android.internal.app.IBatteryStats";
        static final int TRANSACTION_getAwakeTimeBattery = 41;
        static final int TRANSACTION_getAwakeTimePlugged = 42;
        static final int TRANSACTION_getStatistics = 1;
        static final int TRANSACTION_noteBluetoothOff = 26;
        static final int TRANSACTION_noteBluetoothOn = 25;
        static final int TRANSACTION_noteFullWifiLockAcquired = 27;
        static final int TRANSACTION_noteFullWifiLockAcquiredFromSource = 33;
        static final int TRANSACTION_noteFullWifiLockReleased = 28;
        static final int TRANSACTION_noteFullWifiLockReleasedFromSource = 34;
        static final int TRANSACTION_noteInputEvent = 13;
        static final int TRANSACTION_noteNetworkInterfaceType = 39;
        static final int TRANSACTION_notePhoneDataConnectionState = 18;
        static final int TRANSACTION_notePhoneOff = 16;
        static final int TRANSACTION_notePhoneOn = 15;
        static final int TRANSACTION_notePhoneSignalStrength = 17;
        static final int TRANSACTION_notePhoneState = 19;
        static final int TRANSACTION_noteScanWifiLockAcquired = 29;
        static final int TRANSACTION_noteScanWifiLockAcquiredFromSource = 35;
        static final int TRANSACTION_noteScanWifiLockReleased = 30;
        static final int TRANSACTION_noteScanWifiLockReleasedFromSource = 36;
        static final int TRANSACTION_noteScreenBrightness = 11;
        static final int TRANSACTION_noteScreenOff = 12;
        static final int TRANSACTION_noteScreenOn = 10;
        static final int TRANSACTION_noteStartGps = 8;
        static final int TRANSACTION_noteStartSensor = 4;
        static final int TRANSACTION_noteStartWakelock = 2;
        static final int TRANSACTION_noteStartWakelockFromSource = 6;
        static final int TRANSACTION_noteStopGps = 9;
        static final int TRANSACTION_noteStopSensor = 5;
        static final int TRANSACTION_noteStopWakelock = 3;
        static final int TRANSACTION_noteStopWakelockFromSource = 7;
        static final int TRANSACTION_noteUserActivity = 14;
        static final int TRANSACTION_noteWifiMulticastDisabled = 32;
        static final int TRANSACTION_noteWifiMulticastDisabledFromSource = 38;
        static final int TRANSACTION_noteWifiMulticastEnabled = 31;
        static final int TRANSACTION_noteWifiMulticastEnabledFromSource = 37;
        static final int TRANSACTION_noteWifiOff = 21;
        static final int TRANSACTION_noteWifiOn = 20;
        static final int TRANSACTION_noteWifiRunning = 22;
        static final int TRANSACTION_noteWifiRunningChanged = 23;
        static final int TRANSACTION_noteWifiStopped = 24;
        static final int TRANSACTION_setBatteryState = 40;

        public Stub()
        {
            attachInterface(this, "com.android.internal.app.IBatteryStats");
        }

        public static IBatteryStats asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.app.IBatteryStats");
                if ((localIInterface != null) && ((localIInterface instanceof IBatteryStats)))
                    localObject = (IBatteryStats)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.app.IBatteryStats");
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                byte[] arrayOfByte = getStatistics();
                paramParcel2.writeNoException();
                paramParcel2.writeByteArray(arrayOfByte);
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteStartWakelock(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteStopWakelock(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteStartSensor(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteStopSensor(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource12 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource12 = null)
                {
                    noteStartWakelockFromSource(localWorkSource12, paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource11 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource11 = null)
                {
                    noteStopWakelockFromSource(localWorkSource11, paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteStartGps(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteStopGps(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteScreenOn();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteScreenBrightness(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteScreenOff();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteInputEvent();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteUserActivity(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                notePhoneOn();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                notePhoneOff();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (SignalStrength localSignalStrength = (SignalStrength)SignalStrength.CREATOR.createFromParcel(paramParcel1); ; localSignalStrength = null)
                {
                    notePhoneSignalStrength(localSignalStrength);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                int i = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    notePhoneDataConnectionState(i, bool2);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                notePhoneState(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteWifiOn();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteWifiOff();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource10 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource10 = null)
                {
                    noteWifiRunning(localWorkSource10);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                WorkSource localWorkSource8;
                if (paramParcel1.readInt() != 0)
                {
                    localWorkSource8 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1);
                    label1031: if (paramParcel1.readInt() == 0)
                        break label1073;
                }
                label1073: for (WorkSource localWorkSource9 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource9 = null)
                {
                    noteWifiRunningChanged(localWorkSource8, localWorkSource9);
                    paramParcel2.writeNoException();
                    break;
                    localWorkSource8 = null;
                    break label1031;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource7 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource7 = null)
                {
                    noteWifiStopped(localWorkSource7);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteBluetoothOn();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteBluetoothOff();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteFullWifiLockAcquired(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteFullWifiLockReleased(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteScanWifiLockAcquired(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteScanWifiLockReleased(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteWifiMulticastEnabled(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteWifiMulticastDisabled(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource6 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource6 = null)
                {
                    noteFullWifiLockAcquiredFromSource(localWorkSource6);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource5 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource5 = null)
                {
                    noteFullWifiLockReleasedFromSource(localWorkSource5);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource4 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource4 = null)
                {
                    noteScanWifiLockAcquiredFromSource(localWorkSource4);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource3 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource3 = null)
                {
                    noteScanWifiLockReleasedFromSource(localWorkSource3);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource2 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource2 = null)
                {
                    noteWifiMulticastEnabledFromSource(localWorkSource2);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                if (paramParcel1.readInt() != 0);
                for (WorkSource localWorkSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource1 = null)
                {
                    noteWifiMulticastDisabledFromSource(localWorkSource1);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                noteNetworkInterfaceType(paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                setBatteryState(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                long l2 = getAwakeTimeBattery();
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l2);
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IBatteryStats");
                long l1 = getAwakeTimePlugged();
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l1);
            }
        }

        private static class Proxy
            implements IBatteryStats
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public long getAwakeTimeBattery()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(41, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getAwakeTimePlugged()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(42, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.app.IBatteryStats";
            }

            public byte[] getStatistics()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    byte[] arrayOfByte = localParcel2.createByteArray();
                    return arrayOfByte;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteBluetoothOff()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteBluetoothOn()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteFullWifiLockAcquired(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(27, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteFullWifiLockAcquiredFromSource(WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(33, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteFullWifiLockReleased(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(28, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteFullWifiLockReleasedFromSource(WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(34, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteInputEvent()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteNetworkInterfaceType(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(39, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notePhoneDataConnectionState(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notePhoneOff()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notePhoneOn()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notePhoneSignalStrength(SignalStrength paramSignalStrength)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramSignalStrength != null)
                    {
                        localParcel1.writeInt(1);
                        paramSignalStrength.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(17, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notePhoneState(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteScanWifiLockAcquired(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteScanWifiLockAcquiredFromSource(WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(35, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteScanWifiLockReleased(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteScanWifiLockReleasedFromSource(WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(36, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteScreenBrightness(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteScreenOff()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteScreenOn()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteStartGps(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteStartSensor(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteStartWakelock(int paramInt1, int paramInt2, String paramString, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteStartWakelockFromSource(WorkSource paramWorkSource, int paramInt1, String paramString, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteStopGps(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteStopSensor(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteStopWakelock(int paramInt1, int paramInt2, String paramString, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteStopWakelockFromSource(WorkSource paramWorkSource, int paramInt1, String paramString, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteUserActivity(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteWifiMulticastDisabled(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteWifiMulticastDisabledFromSource(WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(38, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteWifiMulticastEnabled(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(31, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteWifiMulticastEnabledFromSource(WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(37, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteWifiOff()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteWifiOn()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteWifiRunning(WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(22, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteWifiRunningChanged(WorkSource paramWorkSource1, WorkSource paramWorkSource2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                        if (paramWorkSource1 != null)
                        {
                            localParcel1.writeInt(1);
                            paramWorkSource1.writeToParcel(localParcel1, 0);
                            if (paramWorkSource2 != null)
                            {
                                localParcel1.writeInt(1);
                                paramWorkSource2.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(23, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public void noteWifiStopped(WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(24, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBatteryState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    localParcel1.writeInt(paramInt4);
                    localParcel1.writeInt(paramInt5);
                    localParcel1.writeInt(paramInt6);
                    this.mRemote.transact(40, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.IBatteryStats
 * JD-Core Version:        0.6.2
 */