package com.android.internal.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class NetInitiatedActivity extends AlertActivity
    implements DialogInterface.OnClickListener
{
    private static final boolean DEBUG = true;
    private static final int GPS_NO_RESPONSE_TIME_OUT = 1;
    private static final int NEGATIVE_BUTTON = -2;
    private static final int POSITIVE_BUTTON = -1;
    private static final String TAG = "NetInitiatedActivity";
    private static final boolean VERBOSE;
    private int default_response = -1;
    private int default_response_timeout = 6;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            }
            while (true)
            {
                return;
                if (NetInitiatedActivity.this.notificationId != -1)
                    NetInitiatedActivity.this.sendUserResponse(NetInitiatedActivity.this.default_response);
                NetInitiatedActivity.this.finish();
            }
        }
    };
    private BroadcastReceiver mNetInitiatedReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            Log.d("NetInitiatedActivity", "NetInitiatedReceiver onReceive: " + paramAnonymousIntent.getAction());
            if (paramAnonymousIntent.getAction() == "android.intent.action.NETWORK_INITIATED_VERIFY")
                NetInitiatedActivity.this.handleNIVerify(paramAnonymousIntent);
        }
    };
    private int notificationId = -1;
    private int timeout = -1;

    private void handleNIVerify(Intent paramIntent)
    {
        this.notificationId = paramIntent.getIntExtra("notif_id", -1);
        Log.d("NetInitiatedActivity", "handleNIVerify action: " + paramIntent.getAction());
    }

    private void sendUserResponse(int paramInt)
    {
        Log.d("NetInitiatedActivity", "sendUserResponse, response: " + paramInt);
        ((LocationManager)getSystemService("location")).sendNiResponse(this.notificationId, paramInt);
    }

    private void showNIError()
    {
        Toast.makeText(this, "NI error", 1).show();
    }

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        if (paramInt == -1)
            sendUserResponse(1);
        if (paramInt == -2)
            sendUserResponse(2);
        finish();
        this.notificationId = -1;
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        Intent localIntent = getIntent();
        AlertController.AlertParams localAlertParams = this.mAlertParams;
        Context localContext = getApplicationContext();
        localAlertParams.mIconId = 17302218;
        localAlertParams.mTitle = localIntent.getStringExtra("title");
        localAlertParams.mMessage = localIntent.getStringExtra("message");
        localAlertParams.mPositiveButtonText = String.format(localContext.getString(17040543), new Object[0]);
        localAlertParams.mPositiveButtonListener = this;
        localAlertParams.mNegativeButtonText = String.format(localContext.getString(17040544), new Object[0]);
        localAlertParams.mNegativeButtonListener = this;
        this.notificationId = localIntent.getIntExtra("notif_id", -1);
        this.timeout = localIntent.getIntExtra("timeout", this.default_response_timeout);
        this.default_response = localIntent.getIntExtra("default_resp", 1);
        Log.d("NetInitiatedActivity", "onCreate() : notificationId: " + this.notificationId + " timeout: " + this.timeout + " default_response:" + this.default_response);
        this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1), 1000 * this.timeout);
        setupAlert();
    }

    protected void onPause()
    {
        super.onPause();
        Log.d("NetInitiatedActivity", "onPause");
        unregisterReceiver(this.mNetInitiatedReceiver);
    }

    protected void onResume()
    {
        super.onResume();
        Log.d("NetInitiatedActivity", "onResume");
        registerReceiver(this.mNetInitiatedReceiver, new IntentFilter("android.intent.action.NETWORK_INITIATED_VERIFY"));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.NetInitiatedActivity
 * JD-Core Version:        0.6.2
 */