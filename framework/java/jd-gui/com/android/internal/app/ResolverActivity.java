package com.android.internal.app;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManager;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.AuthorityEntry;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ResolveInfo.DisplayNameComparator;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.PatternMatcher;
import android.os.RemoteException;
import android.os.UserId;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.content.PackageMonitor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ResolverActivity extends AlertActivity
    implements AdapterView.OnItemClickListener
{
    private static final String TAG = "ResolverActivity";
    private ResolveListAdapter mAdapter;
    private Button mAlwaysButton;
    private boolean mAlwaysUseOption;
    private GridView mGrid;
    private int mIconDpi;
    private int mIconSize;
    private int mLaunchedFromUid;
    private int mMaxColumns;
    private Button mOnceButton;
    private final PackageMonitor mPackageMonitor = new PackageMonitor()
    {
        public void onSomePackagesChanged()
        {
            ResolverActivity.this.mAdapter.handlePackagesChanged();
        }
    };
    private PackageManager mPm;
    private boolean mRegistered;
    private boolean mShowExtended;

    private Intent makeMyIntent()
    {
        Intent localIntent = new Intent(getIntent());
        localIntent.setFlags(0xFF7FFFFF & localIntent.getFlags());
        return localIntent;
    }

    Drawable getIcon(Resources paramResources, int paramInt)
    {
        try
        {
            Drawable localDrawable2 = paramResources.getDrawableForDensity(paramInt, this.mIconDpi);
            localDrawable1 = localDrawable2;
            return localDrawable1;
        }
        catch (Resources.NotFoundException localNotFoundException)
        {
            while (true)
                Drawable localDrawable1 = null;
        }
    }

    Drawable loadIconForResolveInfo(ResolveInfo paramResolveInfo)
    {
        Object localObject;
        try
        {
            if ((paramResolveInfo.resolvePackageName != null) && (paramResolveInfo.icon != 0))
            {
                localObject = getIcon(this.mPm.getResourcesForApplication(paramResolveInfo.resolvePackageName), paramResolveInfo.icon);
                if (localObject != null);
            }
            else
            {
                int i = paramResolveInfo.getIconResource();
                if (i != 0)
                {
                    Drawable localDrawable = getIcon(this.mPm.getResourcesForApplication(paramResolveInfo.activityInfo.packageName), i);
                    localObject = localDrawable;
                    if (localObject != null);
                }
                else
                {
                    localObject = paramResolveInfo.loadIcon(this.mPm);
                }
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
                Log.e("ResolverActivity", "Couldn't find resources for package", localNameNotFoundException);
        }
        return localObject;
    }

    public void onButtonClick(View paramView)
    {
        int i = paramView.getId();
        int j = this.mGrid.getCheckedItemPosition();
        if (i == 16909079);
        for (boolean bool = true; ; bool = false)
        {
            startSelected(j, bool);
            dismiss();
            return;
        }
    }

    protected void onCreate(Bundle paramBundle)
    {
        onCreate(paramBundle, makeMyIntent(), getResources().getText(17040329), null, null, true);
    }

    protected void onCreate(Bundle paramBundle, Intent paramIntent, CharSequence paramCharSequence, Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean)
    {
        setTheme(16974594);
        super.onCreate(paramBundle);
        try
        {
            this.mLaunchedFromUid = ActivityManagerNative.getDefault().getLaunchedFromUid(getActivityToken());
            this.mPm = getPackageManager();
            this.mAlwaysUseOption = paramBoolean;
            this.mMaxColumns = getResources().getInteger(17694770);
            paramIntent.setComponent(null);
            localAlertParams = this.mAlertParams;
            localAlertParams.mTitle = paramCharSequence;
            this.mPackageMonitor.register(this, getMainLooper(), false);
            this.mRegistered = true;
            ActivityManager localActivityManager = (ActivityManager)getSystemService("activity");
            this.mIconDpi = localActivityManager.getLauncherLargeIconDensity();
            this.mIconSize = localActivityManager.getLauncherLargeIconSize();
            this.mAdapter = new ResolveListAdapter(this, paramIntent, paramArrayOfIntent, paramList, this.mLaunchedFromUid);
            i = this.mAdapter.getCount();
            if ((this.mLaunchedFromUid < 0) || (UserId.isIsolated(this.mLaunchedFromUid)))
            {
                finish();
                return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                AlertController.AlertParams localAlertParams;
                int i;
                this.mLaunchedFromUid = -1;
                continue;
                if (i > 1)
                {
                    localAlertParams.mView = getLayoutInflater().inflate(17367188, null);
                    this.mGrid = ((GridView)localAlertParams.mView.findViewById(16909078));
                    this.mGrid.setAdapter(this.mAdapter);
                    this.mGrid.setOnItemClickListener(this);
                    this.mGrid.setOnItemLongClickListener(new ItemLongClickListener());
                    if (paramBoolean)
                        this.mGrid.setChoiceMode(1);
                    resizeGrid();
                }
                while (true)
                {
                    setupAlert();
                    if (!paramBoolean)
                        break;
                    ViewGroup localViewGroup = (ViewGroup)findViewById(16908909);
                    if (localViewGroup == null)
                        break label393;
                    localViewGroup.setVisibility(0);
                    this.mAlwaysButton = ((Button)localViewGroup.findViewById(16909079));
                    this.mOnceButton = ((Button)localViewGroup.findViewById(16909080));
                    break;
                    if (i == 1)
                    {
                        startActivity(this.mAdapter.intentForPosition(0));
                        this.mPackageMonitor.unregister();
                        this.mRegistered = false;
                        finish();
                        break;
                    }
                    localAlertParams.mMessage = getResources().getText(17040334);
                }
                label393: this.mAlwaysUseOption = false;
            }
        }
    }

    protected void onIntentSelected(ResolveInfo paramResolveInfo, Intent paramIntent, boolean paramBoolean)
    {
        if (paramBoolean)
        {
            IntentFilter localIntentFilter = new IntentFilter();
            if (paramIntent.getAction() != null)
                localIntentFilter.addAction(paramIntent.getAction());
            Set localSet = paramIntent.getCategories();
            if (localSet != null)
            {
                Iterator localIterator3 = localSet.iterator();
                while (localIterator3.hasNext())
                    localIntentFilter.addCategory((String)localIterator3.next());
            }
            localIntentFilter.addCategory("android.intent.category.DEFAULT");
            int i = 0xFFF0000 & paramResolveInfo.match;
            Uri localUri = paramIntent.getData();
            String str4;
            if (i == 6291456)
            {
                str4 = paramIntent.resolveType(this);
                if (str4 == null);
            }
            try
            {
                localIntentFilter.addDataType(str4);
                if ((localUri != null) && (localUri.getScheme() != null) && ((i != 6291456) || ((!"file".equals(localUri.getScheme())) && (!"content".equals(localUri.getScheme())))))
                {
                    localIntentFilter.addDataScheme(localUri.getScheme());
                    Iterator localIterator1 = paramResolveInfo.filter.authoritiesIterator();
                    if (localIterator1 != null)
                        while (localIterator1.hasNext())
                        {
                            IntentFilter.AuthorityEntry localAuthorityEntry = (IntentFilter.AuthorityEntry)localIterator1.next();
                            if (localAuthorityEntry.match(localUri) >= 0)
                            {
                                int n = localAuthorityEntry.getPort();
                                String str2 = localAuthorityEntry.getHost();
                                if (n < 0)
                                    break label469;
                                str3 = Integer.toString(n);
                                localIntentFilter.addDataAuthority(str2, str3);
                            }
                        }
                    Iterator localIterator2 = paramResolveInfo.filter.pathsIterator();
                    if (localIterator2 != null)
                    {
                        String str1 = localUri.getPath();
                        while ((str1 != null) && (localIterator2.hasNext()))
                        {
                            PatternMatcher localPatternMatcher = (PatternMatcher)localIterator2.next();
                            if (localPatternMatcher.match(str1))
                                localIntentFilter.addDataPath(localPatternMatcher.getPath(), localPatternMatcher.getType());
                        }
                    }
                }
                if (localIntentFilter != null)
                {
                    int j = this.mAdapter.mList.size();
                    arrayOfComponentName = new ComponentName[j];
                    k = 0;
                    for (int m = 0; m < j; m++)
                    {
                        ResolveInfo localResolveInfo = ((DisplayResolveInfo)this.mAdapter.mList.get(m)).ri;
                        arrayOfComponentName[m] = new ComponentName(localResolveInfo.activityInfo.packageName, localResolveInfo.activityInfo.name);
                        if (localResolveInfo.match > k)
                            k = localResolveInfo.match;
                    }
                }
            }
            catch (IntentFilter.MalformedMimeTypeException localMalformedMimeTypeException)
            {
                ComponentName[] arrayOfComponentName;
                int k;
                while (true)
                {
                    Log.w("ResolverActivity", localMalformedMimeTypeException);
                    localIntentFilter = null;
                    continue;
                    label469: String str3 = null;
                }
                getPackageManager().addPreferredActivity(localIntentFilter, k, arrayOfComponentName, paramIntent.getComponent());
            }
        }
        if (paramIntent != null)
            startActivity(paramIntent);
    }

    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
        boolean bool = false;
        if (this.mAlwaysUseOption)
        {
            int i = this.mGrid.getCheckedItemPosition();
            if (i != -1)
                bool = true;
            this.mAlwaysButton.setEnabled(bool);
            this.mOnceButton.setEnabled(bool);
            if (bool)
                this.mGrid.smoothScrollToPosition(i);
        }
        while (true)
        {
            return;
            startSelected(paramInt, false);
        }
    }

    protected void onRestart()
    {
        super.onRestart();
        if (!this.mRegistered)
        {
            this.mPackageMonitor.register(this, getMainLooper(), false);
            this.mRegistered = true;
        }
        this.mAdapter.handlePackagesChanged();
    }

    protected void onRestoreInstanceState(Bundle paramBundle)
    {
        super.onRestoreInstanceState(paramBundle);
        int i;
        if (this.mAlwaysUseOption)
        {
            i = this.mGrid.getCheckedItemPosition();
            if (i == -1)
                break label57;
        }
        label57: for (boolean bool = true; ; bool = false)
        {
            this.mAlwaysButton.setEnabled(bool);
            this.mOnceButton.setEnabled(bool);
            if (bool)
                this.mGrid.setSelection(i);
            return;
        }
    }

    protected void onStop()
    {
        super.onStop();
        if (this.mRegistered)
        {
            this.mPackageMonitor.unregister();
            this.mRegistered = false;
        }
        if (((0x10000000 & getIntent().getFlags()) != 0) && (!isChangingConfigurations()))
            finish();
    }

    void resizeGrid()
    {
        int i = this.mAdapter.getCount();
        this.mGrid.setNumColumns(Math.min(i, this.mMaxColumns));
    }

    void showAppDetails(ResolveInfo paramResolveInfo)
    {
        startActivity(new Intent().setAction("android.settings.APPLICATION_DETAILS_SETTINGS").setData(Uri.fromParts("package", paramResolveInfo.activityInfo.packageName, null)));
    }

    void startSelected(int paramInt, boolean paramBoolean)
    {
        onIntentSelected(this.mAdapter.resolveInfoForPosition(paramInt), this.mAdapter.intentForPosition(paramInt), paramBoolean);
        finish();
    }

    class ItemLongClickListener
        implements AdapterView.OnItemLongClickListener
    {
        ItemLongClickListener()
        {
        }

        public boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
        {
            ResolveInfo localResolveInfo = ResolverActivity.this.mAdapter.resolveInfoForPosition(paramInt);
            ResolverActivity.this.showAppDetails(localResolveInfo);
            return true;
        }
    }

    private final class ResolveListAdapter extends BaseAdapter
    {
        private final List<ResolveInfo> mBaseResolveList;
        private List<ResolveInfo> mCurrentResolveList;
        private final LayoutInflater mInflater;
        private final Intent[] mInitialIntents;
        private final Intent mIntent;
        private final int mLaunchedFromUid;
        private List<ResolverActivity.DisplayResolveInfo> mList;

        public ResolveListAdapter(Intent paramArrayOfIntent, Intent[] paramList, List<ResolveInfo> paramInt, int arg5)
        {
            this.mIntent = new Intent(paramList);
            this.mIntent.setComponent(null);
            this.mInitialIntents = paramInt;
            Object localObject;
            this.mBaseResolveList = localObject;
            int i;
            this.mLaunchedFromUid = i;
            this.mInflater = ((LayoutInflater)paramArrayOfIntent.getSystemService("layout_inflater"));
            rebuildList();
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        private final void bindView(View paramView, ResolverActivity.DisplayResolveInfo paramDisplayResolveInfo)
        {
            TextView localTextView1 = (TextView)paramView.findViewById(16908308);
            TextView localTextView2 = (TextView)paramView.findViewById(16908309);
            ImageView localImageView = (ImageView)paramView.findViewById(16908294);
            localTextView1.setText(paramDisplayResolveInfo.displayLabel);
            if (ResolverActivity.this.mShowExtended)
            {
                localTextView2.setVisibility(0);
                localTextView2.setText(paramDisplayResolveInfo.extendedInfo);
            }
            while (true)
            {
                if (paramDisplayResolveInfo.displayIcon == null)
                    paramDisplayResolveInfo.displayIcon = ResolverActivity.Injector.loadIconForResolveInfo(ResolverActivity.this, paramDisplayResolveInfo.ri);
                localImageView.setImageDrawable(paramDisplayResolveInfo.displayIcon);
                return;
                localTextView2.setVisibility(8);
            }
        }

        private void processGroup(List<ResolveInfo> paramList, int paramInt1, int paramInt2, ResolveInfo paramResolveInfo, CharSequence paramCharSequence)
        {
            if (1 + (paramInt2 - paramInt1) == 1)
            {
                this.mList.add(new ResolverActivity.DisplayResolveInfo(ResolverActivity.this, paramResolveInfo, paramCharSequence, null, null));
                return;
            }
            ResolverActivity.access$402(ResolverActivity.this, true);
            int i = 0;
            CharSequence localCharSequence1 = paramResolveInfo.activityInfo.applicationInfo.loadLabel(ResolverActivity.this.mPm);
            if (localCharSequence1 == null)
                i = 1;
            HashSet localHashSet;
            int j;
            label104: CharSequence localCharSequence2;
            int k;
            label165: ResolveInfo localResolveInfo;
            if (i == 0)
            {
                localHashSet = new HashSet();
                localHashSet.add(localCharSequence1);
                j = paramInt1 + 1;
                if (j <= paramInt2)
                {
                    localCharSequence2 = ((ResolveInfo)paramList.get(j)).activityInfo.applicationInfo.loadLabel(ResolverActivity.this.mPm);
                    if ((localCharSequence2 == null) || (localHashSet.contains(localCharSequence2)))
                        i = 1;
                }
                else
                {
                    localHashSet.clear();
                }
            }
            else
            {
                k = paramInt1;
                if (k <= paramInt2)
                {
                    localResolveInfo = (ResolveInfo)paramList.get(k);
                    if (i == 0)
                        break label243;
                    this.mList.add(new ResolverActivity.DisplayResolveInfo(ResolverActivity.this, localResolveInfo, paramCharSequence, localResolveInfo.activityInfo.packageName, null));
                }
            }
            while (true)
            {
                k++;
                break label165;
                break;
                localHashSet.add(localCharSequence2);
                j++;
                break label104;
                label243: this.mList.add(new ResolverActivity.DisplayResolveInfo(ResolverActivity.this, localResolveInfo, paramCharSequence, localResolveInfo.activityInfo.applicationInfo.loadLabel(ResolverActivity.this.mPm), null));
            }
        }

        private void rebuildList()
        {
            int k;
            ResolveInfo localResolveInfo1;
            if (this.mBaseResolveList != null)
            {
                this.mCurrentResolveList = this.mBaseResolveList;
                if (this.mCurrentResolveList != null)
                {
                    k = this.mCurrentResolveList.size();
                    if (k > 0)
                        localResolveInfo1 = (ResolveInfo)this.mCurrentResolveList.get(0);
                }
            }
            else
            {
                for (int m = 1; ; m++)
                {
                    if (m >= k)
                        break label271;
                    ResolveInfo localResolveInfo4 = (ResolveInfo)this.mCurrentResolveList.get(m);
                    if ((localResolveInfo1.priority != localResolveInfo4.priority) || (localResolveInfo1.isDefault != localResolveInfo4.isDefault))
                        while (m < k)
                        {
                            this.mCurrentResolveList.remove(m);
                            k--;
                            continue;
                            PackageManager localPackageManager = ResolverActivity.this.mPm;
                            Intent localIntent1 = this.mIntent;
                            if (ResolverActivity.this.mAlwaysUseOption);
                            for (int i = 64; ; i = 0)
                            {
                                this.mCurrentResolveList = localPackageManager.queryIntentActivities(localIntent1, i | 0x10000);
                                if (this.mCurrentResolveList == null)
                                    break;
                                for (int j = -1 + this.mCurrentResolveList.size(); j >= 0; j--)
                                {
                                    ActivityInfo localActivityInfo1 = ((ResolveInfo)this.mCurrentResolveList.get(j)).activityInfo;
                                    if (ActivityManager.checkComponentPermission(localActivityInfo1.permission, this.mLaunchedFromUid, localActivityInfo1.applicationInfo.uid, localActivityInfo1.exported) != 0)
                                        this.mCurrentResolveList.remove(j);
                                }
                                break;
                            }
                        }
                }
                label271: if (k > 1)
                {
                    ResolveInfo.DisplayNameComparator localDisplayNameComparator = new ResolveInfo.DisplayNameComparator(ResolverActivity.this.mPm);
                    Collections.sort(this.mCurrentResolveList, localDisplayNameComparator);
                }
                this.mList = new ArrayList();
                if (this.mInitialIntents != null)
                {
                    int i2 = 0;
                    int i3 = this.mInitialIntents.length;
                    if (i2 < i3)
                    {
                        Intent localIntent2 = this.mInitialIntents[i2];
                        if (localIntent2 == null);
                        while (true)
                        {
                            i2++;
                            break;
                            ActivityInfo localActivityInfo2 = localIntent2.resolveActivityInfo(ResolverActivity.this.getPackageManager(), 0);
                            if (localActivityInfo2 == null)
                            {
                                Log.w("ResolverActivity", "No activity found for " + localIntent2);
                            }
                            else
                            {
                                ResolveInfo localResolveInfo3 = new ResolveInfo();
                                localResolveInfo3.activityInfo = localActivityInfo2;
                                if ((localIntent2 instanceof LabeledIntent))
                                {
                                    LabeledIntent localLabeledIntent = (LabeledIntent)localIntent2;
                                    localResolveInfo3.resolvePackageName = localLabeledIntent.getSourcePackage();
                                    localResolveInfo3.labelRes = localLabeledIntent.getLabelResource();
                                    localResolveInfo3.nonLocalizedLabel = localLabeledIntent.getNonLocalizedLabel();
                                    localResolveInfo3.icon = localLabeledIntent.getIconResource();
                                }
                                this.mList.add(new ResolverActivity.DisplayResolveInfo(ResolverActivity.this, localResolveInfo3, localResolveInfo3.loadLabel(ResolverActivity.this.getPackageManager()), null, localIntent2));
                            }
                        }
                    }
                }
                Object localObject1 = (ResolveInfo)this.mCurrentResolveList.get(0);
                int n = 0;
                Object localObject2 = ((ResolveInfo)localObject1).loadLabel(ResolverActivity.this.mPm);
                ResolverActivity.access$402(ResolverActivity.this, false);
                int i1 = 1;
                if (i1 < k)
                {
                    if (localObject2 == null)
                        localObject2 = ((ResolveInfo)localObject1).activityInfo.packageName;
                    ResolveInfo localResolveInfo2 = (ResolveInfo)this.mCurrentResolveList.get(i1);
                    Object localObject3 = localResolveInfo2.loadLabel(ResolverActivity.this.mPm);
                    if (localObject3 == null)
                        localObject3 = localResolveInfo2.activityInfo.packageName;
                    if (localObject3.equals(localObject2));
                    while (true)
                    {
                        i1++;
                        break;
                        processGroup(this.mCurrentResolveList, n, i1 - 1, (ResolveInfo)localObject1, (CharSequence)localObject2);
                        localObject1 = localResolveInfo2;
                        localObject2 = localObject3;
                        n = i1;
                    }
                }
                processGroup(this.mCurrentResolveList, n, k - 1, (ResolveInfo)localObject1, (CharSequence)localObject2);
            }
        }

        public int getCount()
        {
            if (this.mList != null);
            for (int i = this.mList.size(); ; i = 0)
                return i;
        }

        public Object getItem(int paramInt)
        {
            return Integer.valueOf(paramInt);
        }

        public long getItemId(int paramInt)
        {
            return paramInt;
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            View localView;
            if (paramView == null)
            {
                localView = this.mInflater.inflate(17367187, paramViewGroup, false);
                ViewGroup.LayoutParams localLayoutParams = ((ImageView)localView.findViewById(16908294)).getLayoutParams();
                int i = ResolverActivity.this.mIconSize;
                localLayoutParams.height = i;
                localLayoutParams.width = i;
            }
            while (true)
            {
                bindView(localView, (ResolverActivity.DisplayResolveInfo)this.mList.get(paramInt));
                return localView;
                localView = paramView;
            }
        }

        public void handlePackagesChanged()
        {
            int i = getCount();
            rebuildList();
            notifyDataSetChanged();
            if (this.mList.size() <= 0)
                ResolverActivity.this.finish();
            if (getCount() != i)
                ResolverActivity.this.resizeGrid();
        }

        public Intent intentForPosition(int paramInt)
        {
            Intent localIntent2;
            if (this.mList == null)
            {
                localIntent2 = null;
                return localIntent2;
            }
            ResolverActivity.DisplayResolveInfo localDisplayResolveInfo = (ResolverActivity.DisplayResolveInfo)this.mList.get(paramInt);
            if (localDisplayResolveInfo.origIntent != null);
            for (Intent localIntent1 = localDisplayResolveInfo.origIntent; ; localIntent1 = this.mIntent)
            {
                localIntent2 = new Intent(localIntent1);
                localIntent2.addFlags(50331648);
                ActivityInfo localActivityInfo = localDisplayResolveInfo.ri.activityInfo;
                localIntent2.setComponent(new ComponentName(localActivityInfo.applicationInfo.packageName, localActivityInfo.name));
                break;
            }
        }

        public ResolveInfo resolveInfoForPosition(int paramInt)
        {
            if (this.mList == null);
            for (ResolveInfo localResolveInfo = null; ; localResolveInfo = ((ResolverActivity.DisplayResolveInfo)this.mList.get(paramInt)).ri)
                return localResolveInfo;
        }
    }

    private final class DisplayResolveInfo
    {
        Drawable displayIcon;
        CharSequence displayLabel;
        CharSequence extendedInfo;
        Intent origIntent;
        ResolveInfo ri;

        DisplayResolveInfo(ResolveInfo paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramIntent, Intent arg5)
        {
            this.ri = paramCharSequence1;
            this.displayLabel = paramCharSequence2;
            this.extendedInfo = paramIntent;
            Object localObject;
            this.origIntent = localObject;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static Drawable loadIconForResolveInfo(ResolverActivity paramResolverActivity, ResolveInfo paramResolveInfo)
        {
            return paramResolveInfo.loadIcon(paramResolverActivity.getPackageManager());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.ResolverActivity
 * JD-Core Version:        0.6.2
 */