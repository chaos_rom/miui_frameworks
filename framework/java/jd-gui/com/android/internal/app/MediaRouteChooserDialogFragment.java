package com.android.internal.app;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.media.MediaRouter;
import android.media.MediaRouter.Callback;
import android.media.MediaRouter.RouteCategory;
import android.media.MediaRouter.RouteGroup;
import android.media.MediaRouter.RouteInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MediaRouteChooserDialogFragment extends DialogFragment
{
    public static final String FRAGMENT_TAG = "android:MediaRouteChooserDialogFragment";
    private static final int[] ITEM_LAYOUTS = arrayOfInt;
    private static final String TAG = "MediaRouteChooserDialogFragment";
    private RouteAdapter mAdapter;
    final MediaRouterCallback mCallback = new MediaRouterCallback();
    final RouteComparator mComparator = new RouteComparator();
    private View.OnClickListener mExtendedSettingsListener;
    private boolean mIgnoreCallbackVolumeChanges;
    private boolean mIgnoreSliderVolumeChanges;
    private LayoutInflater mInflater;
    private LauncherListener mLauncherListener;
    private ListView mListView;
    private int mRouteTypes;
    MediaRouter mRouter;
    private ImageView mVolumeIcon;
    private SeekBar mVolumeSlider;

    static
    {
        int[] arrayOfInt = new int[5];
        arrayOfInt[0] = 17367147;
        arrayOfInt[1] = 17367146;
        arrayOfInt[2] = 17367143;
        arrayOfInt[3] = 17367144;
        arrayOfInt[4] = 17367145;
    }

    public MediaRouteChooserDialogFragment()
    {
        setStyle(1, 16974126);
    }

    void changeVolume(int paramInt)
    {
        if (this.mIgnoreSliderVolumeChanges);
        while (true)
        {
            return;
            MediaRouter.RouteInfo localRouteInfo = this.mRouter.getSelectedRoute(this.mRouteTypes);
            if (localRouteInfo.getVolumeHandling() == 1)
                localRouteInfo.requestSetVolume(Math.max(0, Math.min(paramInt, localRouteInfo.getVolumeMax())));
        }
    }

    public void onAttach(Activity paramActivity)
    {
        super.onAttach(paramActivity);
        this.mRouter = ((MediaRouter)paramActivity.getSystemService("media_router"));
    }

    public Dialog onCreateDialog(Bundle paramBundle)
    {
        return new RouteChooserDialog(getActivity(), getTheme());
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
    {
        this.mInflater = paramLayoutInflater;
        View localView1 = paramLayoutInflater.inflate(17367142, paramViewGroup, false);
        this.mVolumeIcon = ((ImageView)localView1.findViewById(16909019));
        this.mVolumeSlider = ((SeekBar)localView1.findViewById(16909020));
        updateVolume();
        this.mVolumeSlider.setOnSeekBarChangeListener(new VolumeSliderChangeListener());
        if (this.mExtendedSettingsListener != null)
        {
            View localView2 = localView1.findViewById(16909021);
            localView2.setVisibility(0);
            localView2.setOnClickListener(this.mExtendedSettingsListener);
        }
        ListView localListView = (ListView)localView1.findViewById(16908298);
        localListView.setItemsCanFocus(true);
        RouteAdapter localRouteAdapter = new RouteAdapter();
        this.mAdapter = localRouteAdapter;
        localListView.setAdapter(localRouteAdapter);
        localListView.setOnItemClickListener(this.mAdapter);
        this.mListView = localListView;
        this.mRouter.addCallback(this.mRouteTypes, this.mCallback);
        this.mAdapter.scrollToSelectedItem();
        return localView1;
    }

    public void onDetach()
    {
        super.onDetach();
        if (this.mLauncherListener != null)
            this.mLauncherListener.onDetached(this);
        if (this.mAdapter != null)
            this.mAdapter = null;
        this.mInflater = null;
        this.mRouter.removeCallback(this.mCallback);
        this.mRouter = null;
    }

    public void onResume()
    {
        super.onResume();
    }

    public void setExtendedSettingsClickListener(View.OnClickListener paramOnClickListener)
    {
        this.mExtendedSettingsListener = paramOnClickListener;
    }

    public void setLauncherListener(LauncherListener paramLauncherListener)
    {
        this.mLauncherListener = paramLauncherListener;
    }

    public void setRouteTypes(int paramInt)
    {
        this.mRouteTypes = paramInt;
    }

    void updateVolume()
    {
        if (this.mRouter == null)
            return;
        MediaRouter.RouteInfo localRouteInfo = this.mRouter.getSelectedRoute(this.mRouteTypes);
        ImageView localImageView = this.mVolumeIcon;
        int i;
        if (localRouteInfo.getPlaybackType() == 0)
        {
            i = 17302182;
            label36: localImageView.setImageResource(i);
            this.mIgnoreSliderVolumeChanges = true;
            if (localRouteInfo.getVolumeHandling() != 0)
                break label92;
            this.mVolumeSlider.setMax(1);
            this.mVolumeSlider.setProgress(1);
            this.mVolumeSlider.setEnabled(false);
        }
        while (true)
        {
            this.mIgnoreSliderVolumeChanges = false;
            break;
            i = 17302293;
            break label36;
            label92: this.mVolumeSlider.setEnabled(true);
            this.mVolumeSlider.setMax(localRouteInfo.getVolumeMax());
            this.mVolumeSlider.setProgress(localRouteInfo.getVolume());
        }
    }

    class VolumeSliderChangeListener
        implements SeekBar.OnSeekBarChangeListener
    {
        VolumeSliderChangeListener()
        {
        }

        public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
        {
            MediaRouteChooserDialogFragment.this.changeVolume(paramInt);
        }

        public void onStartTrackingTouch(SeekBar paramSeekBar)
        {
            MediaRouteChooserDialogFragment.access$802(MediaRouteChooserDialogFragment.this, true);
        }

        public void onStopTrackingTouch(SeekBar paramSeekBar)
        {
            MediaRouteChooserDialogFragment.access$802(MediaRouteChooserDialogFragment.this, false);
            MediaRouteChooserDialogFragment.this.updateVolume();
        }
    }

    public static abstract interface LauncherListener
    {
        public abstract void onDetached(MediaRouteChooserDialogFragment paramMediaRouteChooserDialogFragment);
    }

    class RouteChooserDialog extends Dialog
    {
        public RouteChooserDialog(Context paramInt, int arg3)
        {
            super(i);
        }

        public void onBackPressed()
        {
            if ((MediaRouteChooserDialogFragment.this.mAdapter != null) && (MediaRouteChooserDialogFragment.this.mAdapter.isGrouping()))
                MediaRouteChooserDialogFragment.this.mAdapter.finishGrouping();
            while (true)
            {
                return;
                super.onBackPressed();
            }
        }

        public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
        {
            int i = 1;
            if ((paramInt == 25) && (MediaRouteChooserDialogFragment.this.mVolumeSlider.isEnabled()))
                MediaRouteChooserDialogFragment.this.mRouter.getSelectedRoute(MediaRouteChooserDialogFragment.this.mRouteTypes).requestUpdateVolume(-1);
            while (true)
            {
                return i;
                if ((paramInt == 24) && (MediaRouteChooserDialogFragment.this.mVolumeSlider.isEnabled()))
                    MediaRouteChooserDialogFragment.this.mRouter.getSelectedRoute(MediaRouteChooserDialogFragment.this.mRouteTypes).requestUpdateVolume(i);
                else
                    boolean bool = super.onKeyDown(paramInt, paramKeyEvent);
            }
        }

        public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
        {
            boolean bool = true;
            if ((paramInt == 25) && (MediaRouteChooserDialogFragment.this.mVolumeSlider.isEnabled()));
            while (true)
            {
                return bool;
                if ((paramInt != 24) || (!MediaRouteChooserDialogFragment.this.mVolumeSlider.isEnabled()))
                    bool = super.onKeyUp(paramInt, paramKeyEvent);
            }
        }
    }

    class RouteComparator
        implements Comparator<MediaRouter.RouteInfo>
    {
        RouteComparator()
        {
        }

        public int compare(MediaRouter.RouteInfo paramRouteInfo1, MediaRouter.RouteInfo paramRouteInfo2)
        {
            return paramRouteInfo1.getName(MediaRouteChooserDialogFragment.this.getActivity()).toString().compareTo(paramRouteInfo2.getName(MediaRouteChooserDialogFragment.this.getActivity()).toString());
        }
    }

    class MediaRouterCallback extends MediaRouter.Callback
    {
        MediaRouterCallback()
        {
        }

        public void onRouteAdded(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteChooserDialogFragment.this.mAdapter.update();
        }

        public void onRouteChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteChooserDialogFragment.this.mAdapter.notifyDataSetChanged();
        }

        public void onRouteGrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup, int paramInt)
        {
            MediaRouteChooserDialogFragment.this.mAdapter.update();
        }

        public void onRouteRemoved(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
            if (paramRouteInfo == MediaRouteChooserDialogFragment.RouteAdapter.access$500(MediaRouteChooserDialogFragment.this.mAdapter))
                MediaRouteChooserDialogFragment.this.mAdapter.finishGrouping();
            MediaRouteChooserDialogFragment.this.mAdapter.update();
        }

        public void onRouteSelected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteChooserDialogFragment.this.mAdapter.update();
            MediaRouteChooserDialogFragment.this.updateVolume();
        }

        public void onRouteUngrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup)
        {
            MediaRouteChooserDialogFragment.this.mAdapter.update();
        }

        public void onRouteUnselected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo)
        {
            MediaRouteChooserDialogFragment.this.mAdapter.update();
        }

        public void onRouteVolumeChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
        {
            if (!MediaRouteChooserDialogFragment.this.mIgnoreCallbackVolumeChanges)
                MediaRouteChooserDialogFragment.this.updateVolume();
        }
    }

    private class RouteAdapter extends BaseAdapter
        implements AdapterView.OnItemClickListener
    {
        private static final int VIEW_GROUPING_DONE = 4;
        private static final int VIEW_GROUPING_ROUTE = 3;
        private static final int VIEW_ROUTE = 2;
        private static final int VIEW_SECTION_HEADER = 1;
        private static final int VIEW_TOP_HEADER;
        private final ArrayList<MediaRouter.RouteInfo> mCatRouteList = new ArrayList();
        private MediaRouter.RouteCategory mCategoryEditingGroups;
        private MediaRouter.RouteGroup mEditingGroup;
        private boolean mIgnoreUpdates;
        private final ArrayList<Object> mItems = new ArrayList();
        private int mSelectedItemPosition = -1;
        private final ArrayList<MediaRouter.RouteInfo> mSortRouteList = new ArrayList();

        RouteAdapter()
        {
            update();
        }

        void addGroupEditingCategoryRoutes(List<MediaRouter.RouteInfo> paramList)
        {
            int i = paramList.size();
            for (int j = 0; j < i; j++)
            {
                MediaRouter.RouteInfo localRouteInfo1 = (MediaRouter.RouteInfo)paramList.get(j);
                MediaRouter.RouteGroup localRouteGroup = localRouteInfo1.getGroup();
                if (localRouteGroup == localRouteInfo1)
                {
                    int k = localRouteGroup.getRouteCount();
                    for (int m = 0; m < k; m++)
                    {
                        MediaRouter.RouteInfo localRouteInfo2 = localRouteGroup.getRouteAt(m);
                        this.mSortRouteList.add(localRouteInfo2);
                    }
                }
                this.mSortRouteList.add(localRouteInfo1);
            }
            Collections.sort(this.mSortRouteList, MediaRouteChooserDialogFragment.this.mComparator);
            this.mItems.addAll(this.mSortRouteList);
            this.mSortRouteList.clear();
            this.mItems.add(null);
        }

        void addSelectableRoutes(MediaRouter.RouteInfo paramRouteInfo, List<MediaRouter.RouteInfo> paramList)
        {
            int i = paramList.size();
            for (int j = 0; j < i; j++)
            {
                MediaRouter.RouteInfo localRouteInfo = (MediaRouter.RouteInfo)paramList.get(j);
                if (localRouteInfo == paramRouteInfo)
                    this.mSelectedItemPosition = this.mItems.size();
                this.mItems.add(localRouteInfo);
            }
        }

        public boolean areAllItemsEnabled()
        {
            return false;
        }

        void bindHeaderView(int paramInt, MediaRouteChooserDialogFragment.ViewHolder paramViewHolder)
        {
            MediaRouter.RouteCategory localRouteCategory = (MediaRouter.RouteCategory)this.mItems.get(paramInt);
            paramViewHolder.text1.setText(localRouteCategory.getName(MediaRouteChooserDialogFragment.this.getActivity()));
        }

        void bindItemView(int paramInt, MediaRouteChooserDialogFragment.ViewHolder paramViewHolder)
        {
            int i = 1;
            int k = 0;
            MediaRouter.RouteInfo localRouteInfo = (MediaRouter.RouteInfo)this.mItems.get(paramInt);
            paramViewHolder.text1.setText(localRouteInfo.getName(MediaRouteChooserDialogFragment.this.getActivity()));
            CharSequence localCharSequence = localRouteInfo.getStatus();
            int m;
            label113: MediaRouter.RouteCategory localRouteCategory;
            int n;
            label164: label186: label192: ImageButton localImageButton;
            if (TextUtils.isEmpty(localCharSequence))
            {
                paramViewHolder.text2.setVisibility(8);
                Drawable localDrawable = localRouteInfo.getIconDrawable();
                if (localDrawable != null)
                    localDrawable = localDrawable.getConstantState().newDrawable(MediaRouteChooserDialogFragment.this.getResources());
                paramViewHolder.icon.setImageDrawable(localDrawable);
                ImageView localImageView = paramViewHolder.icon;
                if (localDrawable == null)
                    break label246;
                m = 0;
                localImageView.setVisibility(m);
                localRouteCategory = localRouteInfo.getCategory();
                n = 0;
                if (localRouteCategory != this.mCategoryEditingGroups)
                    break label264;
                MediaRouter.RouteGroup localRouteGroup = localRouteInfo.getGroup();
                CheckBox localCheckBox1 = paramViewHolder.check;
                if (localRouteGroup.getRouteCount() <= i)
                    break label253;
                int i1 = i;
                localCheckBox1.setEnabled(i1);
                CheckBox localCheckBox2 = paramViewHolder.check;
                if (localRouteGroup != this.mEditingGroup)
                    break label259;
                localCheckBox2.setChecked(i);
                if (paramViewHolder.expandGroupButton != null)
                {
                    localImageButton = paramViewHolder.expandGroupButton;
                    if (n == 0)
                        break label329;
                }
            }
            while (true)
            {
                localImageButton.setVisibility(k);
                paramViewHolder.expandGroupListener.position = paramInt;
                return;
                paramViewHolder.text2.setVisibility(0);
                paramViewHolder.text2.setText(localCharSequence);
                break;
                label246: m = 8;
                break label113;
                label253: int i2 = 0;
                break label164;
                label259: int j = 0;
                break label186;
                label264: if (!localRouteCategory.isGroupable())
                    break label192;
                if ((((MediaRouter.RouteGroup)localRouteInfo).getRouteCount() > j) || (getItemViewType(paramInt - 1) == 2) || ((paramInt < -1 + getCount()) && (getItemViewType(paramInt + 1) == 2)));
                for (n = j; ; n = 0)
                    break;
                label329: k = 8;
            }
        }

        void finishGrouping()
        {
            this.mCategoryEditingGroups = null;
            this.mEditingGroup = null;
            MediaRouteChooserDialogFragment.this.getDialog().setCanceledOnTouchOutside(true);
            update();
            scrollToSelectedItem();
        }

        public int getCount()
        {
            return this.mItems.size();
        }

        public Object getItem(int paramInt)
        {
            return this.mItems.get(paramInt);
        }

        public long getItemId(int paramInt)
        {
            return paramInt;
        }

        public int getItemViewType(int paramInt)
        {
            Object localObject = getItem(paramInt);
            int i;
            if ((localObject instanceof MediaRouter.RouteCategory))
                if (paramInt == 0)
                    i = 0;
            while (true)
            {
                return i;
                i = 1;
                continue;
                if (localObject == null)
                    i = 4;
                else if (((MediaRouter.RouteInfo)localObject).getCategory() == this.mCategoryEditingGroups)
                    i = 3;
                else
                    i = 2;
            }
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            int i = getItemViewType(paramInt);
            final MediaRouteChooserDialogFragment.ViewHolder localViewHolder;
            if (paramView == null)
            {
                paramView = MediaRouteChooserDialogFragment.this.mInflater.inflate(MediaRouteChooserDialogFragment.ITEM_LAYOUTS[i], paramViewGroup, false);
                localViewHolder = new MediaRouteChooserDialogFragment.ViewHolder(null);
                localViewHolder.position = paramInt;
                localViewHolder.text1 = ((TextView)paramView.findViewById(16908308));
                localViewHolder.text2 = ((TextView)paramView.findViewById(16908309));
                localViewHolder.icon = ((ImageView)paramView.findViewById(16908294));
                localViewHolder.check = ((CheckBox)paramView.findViewById(16909023));
                localViewHolder.expandGroupButton = ((ImageButton)paramView.findViewById(16909022));
                if (localViewHolder.expandGroupButton != null)
                {
                    localViewHolder.expandGroupListener = new ExpandGroupListener();
                    localViewHolder.expandGroupButton.setOnClickListener(localViewHolder.expandGroupListener);
                }
                final View localView = paramView;
                paramView.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View paramAnonymousView)
                    {
                        this.val$list.performItemClick(localView, localViewHolder.position, 0L);
                    }
                });
                paramView.setTag(localViewHolder);
                switch (i)
                {
                default:
                    label216: if (paramInt != this.mSelectedItemPosition)
                        break;
                case 2:
                case 3:
                case 0:
                case 1:
                }
            }
            for (boolean bool = true; ; bool = false)
            {
                paramView.setActivated(bool);
                return paramView;
                localViewHolder = (MediaRouteChooserDialogFragment.ViewHolder)paramView.getTag();
                localViewHolder.position = paramInt;
                break;
                bindItemView(paramInt, localViewHolder);
                break label216;
                bindHeaderView(paramInt, localViewHolder);
                break label216;
            }
        }

        public int getViewTypeCount()
        {
            return 5;
        }

        public boolean isEnabled(int paramInt)
        {
            switch (getItemViewType(paramInt))
            {
            default:
            case 2:
            case 3:
            case 4:
            }
            for (boolean bool = false; ; bool = true)
                return bool;
        }

        boolean isGrouping()
        {
            if (this.mCategoryEditingGroups != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
        {
            int i = getItemViewType(paramInt);
            if ((i == 1) || (i == 0));
            MediaRouter.RouteInfo localRouteInfo;
            do
                while (true)
                {
                    return;
                    if (i == 4)
                    {
                        finishGrouping();
                    }
                    else
                    {
                        Object localObject = getItem(paramInt);
                        if ((localObject instanceof MediaRouter.RouteInfo))
                        {
                            localRouteInfo = (MediaRouter.RouteInfo)localObject;
                            if (i != 2)
                                break;
                            MediaRouteChooserDialogFragment.this.mRouter.selectRouteInt(MediaRouteChooserDialogFragment.this.mRouteTypes, localRouteInfo);
                            MediaRouteChooserDialogFragment.this.dismiss();
                        }
                    }
                }
            while (i != 3);
            Checkable localCheckable = (Checkable)paramView;
            boolean bool = localCheckable.isChecked();
            this.mIgnoreUpdates = true;
            MediaRouter.RouteGroup localRouteGroup = localRouteInfo.getGroup();
            if ((!bool) && (localRouteGroup != this.mEditingGroup))
            {
                if (MediaRouteChooserDialogFragment.this.mRouter.getSelectedRoute(MediaRouteChooserDialogFragment.this.mRouteTypes) == localRouteGroup)
                    MediaRouteChooserDialogFragment.this.mRouter.selectRouteInt(MediaRouteChooserDialogFragment.this.mRouteTypes, this.mEditingGroup);
                localRouteGroup.removeRoute(localRouteInfo);
                this.mEditingGroup.addRoute(localRouteInfo);
                localCheckable.setChecked(true);
            }
            while (true)
            {
                this.mIgnoreUpdates = false;
                update();
                break;
                if ((bool) && (this.mEditingGroup.getRouteCount() > 1))
                {
                    this.mEditingGroup.removeRoute(localRouteInfo);
                    MediaRouteChooserDialogFragment.this.mRouter.addRouteInt(localRouteInfo);
                }
            }
        }

        void scrollToEditingGroup()
        {
            if ((this.mCategoryEditingGroups == null) || (MediaRouteChooserDialogFragment.this.mListView == null))
                return;
            int i = 0;
            int j = 0;
            int k = this.mItems.size();
            for (int m = 0; ; m++)
                if (m < k)
                {
                    Object localObject = this.mItems.get(m);
                    if ((localObject != null) && (localObject == this.mCategoryEditingGroups))
                        j = m;
                    if (localObject == null)
                        i = m;
                }
                else
                {
                    MediaRouteChooserDialogFragment.this.mListView.smoothScrollToPosition(i, j);
                    break;
                }
        }

        void scrollToSelectedItem()
        {
            if ((MediaRouteChooserDialogFragment.this.mListView == null) || (this.mSelectedItemPosition < 0));
            while (true)
            {
                return;
                MediaRouteChooserDialogFragment.this.mListView.smoothScrollToPosition(this.mSelectedItemPosition);
            }
        }

        void update()
        {
            if (this.mIgnoreUpdates);
            while (true)
            {
                return;
                this.mItems.clear();
                MediaRouter.RouteInfo localRouteInfo = MediaRouteChooserDialogFragment.this.mRouter.getSelectedRoute(MediaRouteChooserDialogFragment.this.mRouteTypes);
                this.mSelectedItemPosition = -1;
                int i = MediaRouteChooserDialogFragment.this.mRouter.getCategoryCount();
                int j = 0;
                if (j < i)
                {
                    MediaRouter.RouteCategory localRouteCategory = MediaRouteChooserDialogFragment.this.mRouter.getCategoryAt(j);
                    List localList = localRouteCategory.getRoutes(this.mCatRouteList);
                    this.mItems.add(localRouteCategory);
                    if (localRouteCategory == this.mCategoryEditingGroups)
                        addGroupEditingCategoryRoutes(localList);
                    while (true)
                    {
                        localList.clear();
                        j++;
                        break;
                        addSelectableRoutes(localRouteInfo, localList);
                    }
                }
                notifyDataSetChanged();
                if ((MediaRouteChooserDialogFragment.this.mListView != null) && (this.mSelectedItemPosition >= 0))
                    MediaRouteChooserDialogFragment.this.mListView.setItemChecked(this.mSelectedItemPosition, true);
            }
        }

        class ExpandGroupListener
            implements View.OnClickListener
        {
            int position;

            ExpandGroupListener()
            {
            }

            public void onClick(View paramView)
            {
                MediaRouter.RouteGroup localRouteGroup = (MediaRouter.RouteGroup)MediaRouteChooserDialogFragment.RouteAdapter.this.getItem(this.position);
                MediaRouteChooserDialogFragment.RouteAdapter.access$502(MediaRouteChooserDialogFragment.RouteAdapter.this, localRouteGroup);
                MediaRouteChooserDialogFragment.RouteAdapter.access$602(MediaRouteChooserDialogFragment.RouteAdapter.this, localRouteGroup.getCategory());
                MediaRouteChooserDialogFragment.this.getDialog().setCanceledOnTouchOutside(false);
                MediaRouteChooserDialogFragment.this.mRouter.selectRouteInt(MediaRouteChooserDialogFragment.this.mRouteTypes, MediaRouteChooserDialogFragment.RouteAdapter.this.mEditingGroup);
                MediaRouteChooserDialogFragment.RouteAdapter.this.update();
                MediaRouteChooserDialogFragment.RouteAdapter.this.scrollToEditingGroup();
            }
        }
    }

    private static class ViewHolder
    {
        public CheckBox check;
        public ImageButton expandGroupButton;
        public MediaRouteChooserDialogFragment.RouteAdapter.ExpandGroupListener expandGroupListener;
        public ImageView icon;
        public int position;
        public TextView text1;
        public TextView text2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.MediaRouteChooserDialogFragment
 * JD-Core Version:        0.6.2
 */