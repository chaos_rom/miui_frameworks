package com.android.internal.app;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.ActionBar.OnMenuVisibilityListener;
import android.app.ActionBar.OnNavigationListener;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.SpinnerAdapter;
import com.android.internal.view.ActionBarPolicy;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuBuilder.Callback;
import com.android.internal.view.menu.MenuPopupHelper;
import com.android.internal.view.menu.SubMenuBuilder;
import com.android.internal.widget.ActionBarContainer;
import com.android.internal.widget.ActionBarContextView;
import com.android.internal.widget.ActionBarOverlayLayout;
import com.android.internal.widget.ActionBarView;
import com.android.internal.widget.ScrollingTabContainerView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ActionBarImpl extends ActionBar
{
    private static final int CONTEXT_DISPLAY_NORMAL = 0;
    private static final int CONTEXT_DISPLAY_SPLIT = 1;
    private static final int INVALID_POSITION = -1;
    private static final String TAG = "ActionBarImpl";
    ActionModeImpl mActionMode;
    private ActionBarView mActionView;
    private Activity mActivity;
    private ActionBarContainer mContainerView;
    private View mContentView;
    private Context mContext;
    private int mContextDisplayMode;
    private ActionBarContextView mContextView;
    private int mCurWindowVisibility = 0;
    private Animator mCurrentShowAnim;
    ActionMode mDeferredDestroyActionMode;
    ActionMode.Callback mDeferredModeDestroyCallback;
    private Dialog mDialog;
    private boolean mDisplayHomeAsUpSet;
    final Handler mHandler = new Handler();
    private boolean mHasEmbeddedTabs;
    private boolean mHiddenByApp;
    private boolean mHiddenBySystem;
    final Animator.AnimatorListener mHideListener = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            if (ActionBarImpl.this.mContentView != null)
            {
                ActionBarImpl.this.mContentView.setTranslationY(0.0F);
                ActionBarImpl.this.mTopVisibilityView.setTranslationY(0.0F);
            }
            if ((ActionBarImpl.this.mSplitView != null) && (ActionBarImpl.this.mContextDisplayMode == 1))
                ActionBarImpl.this.mSplitView.setVisibility(8);
            ActionBarImpl.this.mTopVisibilityView.setVisibility(8);
            ActionBarImpl.this.mContainerView.setTransitioning(false);
            ActionBarImpl.access$502(ActionBarImpl.this, null);
            ActionBarImpl.this.completeDeferredDestroyActionMode();
            if (ActionBarImpl.this.mOverlayLayout != null)
                ActionBarImpl.this.mOverlayLayout.requestFitSystemWindows();
        }
    };
    private boolean mLastMenuVisibility;
    private ArrayList<ActionBar.OnMenuVisibilityListener> mMenuVisibilityListeners = new ArrayList();
    private boolean mNowShowing = true;
    private ActionBarOverlayLayout mOverlayLayout;
    private int mSavedTabPosition = -1;
    private TabImpl mSelectedTab;
    private boolean mShowHideAnimationEnabled;
    final Animator.AnimatorListener mShowListener = new AnimatorListenerAdapter()
    {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
            ActionBarImpl.access$502(ActionBarImpl.this, null);
            ActionBarImpl.this.mTopVisibilityView.requestLayout();
        }
    };
    private boolean mShowingForMode;
    private ActionBarContainer mSplitView;
    private ScrollingTabContainerView mTabScrollView;
    Runnable mTabSelector;
    private ArrayList<TabImpl> mTabs = new ArrayList();
    private Context mThemedContext;
    private ViewGroup mTopVisibilityView;

    public ActionBarImpl(Activity paramActivity)
    {
        this.mActivity = paramActivity;
        View localView = paramActivity.getWindow().getDecorView();
        init(localView);
        if (!this.mActivity.getWindow().hasFeature(9))
            this.mContentView = localView.findViewById(16908290);
    }

    public ActionBarImpl(Dialog paramDialog)
    {
        this.mDialog = paramDialog;
        init(paramDialog.getWindow().getDecorView());
    }

    private static boolean checkShowingFlags(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        boolean bool = true;
        if (paramBoolean3);
        while (true)
        {
            return bool;
            if ((paramBoolean1) || (paramBoolean2))
                bool = false;
        }
    }

    private void cleanupTabs()
    {
        if (this.mSelectedTab != null)
            selectTab(null);
        this.mTabs.clear();
        if (this.mTabScrollView != null)
            this.mTabScrollView.removeAllTabs();
        this.mSavedTabPosition = -1;
    }

    private void configureTab(ActionBar.Tab paramTab, int paramInt)
    {
        TabImpl localTabImpl = (TabImpl)paramTab;
        if (localTabImpl.getCallback() == null)
            throw new IllegalStateException("Action Bar Tab must have a Callback");
        localTabImpl.setPosition(paramInt);
        this.mTabs.add(paramInt, localTabImpl);
        int i = this.mTabs.size();
        for (int j = paramInt + 1; j < i; j++)
            ((TabImpl)this.mTabs.get(j)).setPosition(j);
    }

    private void ensureTabsExist()
    {
        if (this.mTabScrollView != null);
        ScrollingTabContainerView localScrollingTabContainerView;
        while (true)
        {
            return;
            localScrollingTabContainerView = new ScrollingTabContainerView(this.mContext);
            if (!this.mHasEmbeddedTabs)
                break;
            localScrollingTabContainerView.setVisibility(0);
            this.mActionView.setEmbeddedTabView(localScrollingTabContainerView);
            this.mTabScrollView = localScrollingTabContainerView;
        }
        if (getNavigationMode() == 2)
        {
            localScrollingTabContainerView.setVisibility(0);
            if (this.mOverlayLayout != null)
                this.mOverlayLayout.requestFitSystemWindows();
        }
        while (true)
        {
            this.mContainerView.setTabContainer(localScrollingTabContainerView);
            break;
            localScrollingTabContainerView.setVisibility(8);
        }
    }

    private void hideForActionMode()
    {
        if (this.mShowingForMode)
        {
            this.mShowingForMode = false;
            if (this.mOverlayLayout != null)
                this.mOverlayLayout.setShowingForActionMode(false);
            updateVisibility(false);
        }
    }

    private void init(View paramView)
    {
        boolean bool = false;
        this.mContext = paramView.getContext();
        this.mOverlayLayout = ((ActionBarOverlayLayout)paramView.findViewById(16909090));
        if (this.mOverlayLayout != null)
            this.mOverlayLayout.setActionBar(this);
        this.mActionView = ((ActionBarView)paramView.findViewById(16909087));
        this.mContextView = ((ActionBarContextView)paramView.findViewById(16909088));
        this.mContainerView = ((ActionBarContainer)paramView.findViewById(16909086));
        this.mTopVisibilityView = ((ViewGroup)paramView.findViewById(16909091));
        if (this.mTopVisibilityView == null)
            this.mTopVisibilityView = this.mContainerView;
        this.mSplitView = ((ActionBarContainer)paramView.findViewById(16909089));
        if ((this.mActionView == null) || (this.mContextView == null) || (this.mContainerView == null))
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        this.mActionView.setContextView(this.mContextView);
        int i;
        if (this.mActionView.isSplitActionBar())
        {
            i = 1;
            this.mContextDisplayMode = i;
            if ((0x4 & this.mActionView.getDisplayOptions()) == 0)
                break label282;
        }
        label282: for (int j = 1; ; j = 0)
        {
            if (j != 0)
                this.mDisplayHomeAsUpSet = true;
            ActionBarPolicy localActionBarPolicy = ActionBarPolicy.get(this.mContext);
            if ((localActionBarPolicy.enableHomeButtonByDefault()) || (j != 0))
                bool = true;
            setHomeButtonEnabled(bool);
            setHasEmbeddedTabs(localActionBarPolicy.hasEmbeddedTabs());
            return;
            i = 0;
            break;
        }
    }

    private void setHasEmbeddedTabs(boolean paramBoolean)
    {
        boolean bool1 = true;
        this.mHasEmbeddedTabs = paramBoolean;
        boolean bool2;
        label43: label76: ActionBarView localActionBarView;
        if (!this.mHasEmbeddedTabs)
        {
            this.mActionView.setEmbeddedTabView(null);
            this.mContainerView.setTabContainer(this.mTabScrollView);
            if (getNavigationMode() != 2)
                break label122;
            bool2 = bool1;
            if (this.mTabScrollView != null)
            {
                if (!bool2)
                    break label127;
                this.mTabScrollView.setVisibility(0);
                if (this.mOverlayLayout != null)
                    this.mOverlayLayout.requestFitSystemWindows();
            }
            localActionBarView = this.mActionView;
            if ((this.mHasEmbeddedTabs) || (!bool2))
                break label139;
        }
        while (true)
        {
            localActionBarView.setCollapsable(bool1);
            return;
            this.mContainerView.setTabContainer(null);
            this.mActionView.setEmbeddedTabView(this.mTabScrollView);
            break;
            label122: bool2 = false;
            break label43;
            label127: this.mTabScrollView.setVisibility(8);
            break label76;
            label139: bool1 = false;
        }
    }

    private void showForActionMode()
    {
        if (!this.mShowingForMode)
        {
            this.mShowingForMode = true;
            if (this.mOverlayLayout != null)
                this.mOverlayLayout.setShowingForActionMode(true);
            updateVisibility(false);
        }
    }

    private void updateVisibility(boolean paramBoolean)
    {
        if (checkShowingFlags(this.mHiddenByApp, this.mHiddenBySystem, this.mShowingForMode))
            if (!this.mNowShowing)
            {
                this.mNowShowing = true;
                doShow(paramBoolean);
            }
        while (true)
        {
            return;
            if (this.mNowShowing)
            {
                this.mNowShowing = false;
                doHide(paramBoolean);
            }
        }
    }

    public void addOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener paramOnMenuVisibilityListener)
    {
        this.mMenuVisibilityListeners.add(paramOnMenuVisibilityListener);
    }

    public void addTab(ActionBar.Tab paramTab)
    {
        addTab(paramTab, this.mTabs.isEmpty());
    }

    public void addTab(ActionBar.Tab paramTab, int paramInt)
    {
        addTab(paramTab, paramInt, this.mTabs.isEmpty());
    }

    public void addTab(ActionBar.Tab paramTab, int paramInt, boolean paramBoolean)
    {
        ensureTabsExist();
        this.mTabScrollView.addTab(paramTab, paramInt, paramBoolean);
        configureTab(paramTab, paramInt);
        if (paramBoolean)
            selectTab(paramTab);
    }

    public void addTab(ActionBar.Tab paramTab, boolean paramBoolean)
    {
        ensureTabsExist();
        this.mTabScrollView.addTab(paramTab, paramBoolean);
        configureTab(paramTab, this.mTabs.size());
        if (paramBoolean)
            selectTab(paramTab);
    }

    void animateToMode(boolean paramBoolean)
    {
        int i = 8;
        int j;
        label23: int k;
        label42: ScrollingTabContainerView localScrollingTabContainerView;
        if (paramBoolean)
        {
            showForActionMode();
            ActionBarView localActionBarView = this.mActionView;
            if (!paramBoolean)
                break label100;
            j = i;
            localActionBarView.animateToVisibility(j);
            ActionBarContextView localActionBarContextView = this.mContextView;
            if (!paramBoolean)
                break label106;
            k = 0;
            localActionBarContextView.animateToVisibility(k);
            if ((this.mTabScrollView != null) && (!this.mActionView.hasEmbeddedTabs()) && (this.mActionView.isCollapsed()))
            {
                localScrollingTabContainerView = this.mTabScrollView;
                if (!paramBoolean)
                    break label112;
            }
        }
        while (true)
        {
            localScrollingTabContainerView.animateToVisibility(i);
            return;
            hideForActionMode();
            break;
            label100: j = 0;
            break label23;
            label106: k = i;
            break label42;
            label112: i = 0;
        }
    }

    void completeDeferredDestroyActionMode()
    {
        if (this.mDeferredModeDestroyCallback != null)
        {
            this.mDeferredModeDestroyCallback.onDestroyActionMode(this.mDeferredDestroyActionMode);
            this.mDeferredDestroyActionMode = null;
            this.mDeferredModeDestroyCallback = null;
        }
    }

    public void dispatchMenuVisibilityChanged(boolean paramBoolean)
    {
        if (paramBoolean == this.mLastMenuVisibility);
        while (true)
        {
            return;
            this.mLastMenuVisibility = paramBoolean;
            int i = this.mMenuVisibilityListeners.size();
            for (int j = 0; j < i; j++)
                ((ActionBar.OnMenuVisibilityListener)this.mMenuVisibilityListeners.get(j)).onMenuVisibilityChanged(paramBoolean);
        }
    }

    public void doHide(boolean paramBoolean)
    {
        if (this.mCurrentShowAnim != null)
            this.mCurrentShowAnim.end();
        if ((this.mCurWindowVisibility == 0) && ((this.mShowHideAnimationEnabled) || (paramBoolean)))
        {
            this.mTopVisibilityView.setAlpha(1.0F);
            this.mContainerView.setTransitioning(true);
            AnimatorSet localAnimatorSet = new AnimatorSet();
            float f = -this.mTopVisibilityView.getHeight();
            if (paramBoolean)
            {
                int[] arrayOfInt = new int[2];
                arrayOfInt[0] = 0;
                arrayOfInt[1] = 0;
                this.mTopVisibilityView.getLocationInWindow(arrayOfInt);
                f -= arrayOfInt[1];
            }
            ViewGroup localViewGroup = this.mTopVisibilityView;
            float[] arrayOfFloat1 = new float[1];
            arrayOfFloat1[0] = f;
            AnimatorSet.Builder localBuilder = localAnimatorSet.play(ObjectAnimator.ofFloat(localViewGroup, "translationY", arrayOfFloat1));
            if (this.mContentView != null)
            {
                View localView = this.mContentView;
                float[] arrayOfFloat3 = new float[2];
                arrayOfFloat3[0] = 0.0F;
                arrayOfFloat3[1] = f;
                localBuilder.with(ObjectAnimator.ofFloat(localView, "translationY", arrayOfFloat3));
            }
            if ((this.mSplitView != null) && (this.mSplitView.getVisibility() == 0))
            {
                this.mSplitView.setAlpha(1.0F);
                ActionBarContainer localActionBarContainer = this.mSplitView;
                float[] arrayOfFloat2 = new float[1];
                arrayOfFloat2[0] = this.mSplitView.getHeight();
                localBuilder.with(ObjectAnimator.ofFloat(localActionBarContainer, "translationY", arrayOfFloat2));
            }
            localAnimatorSet.setInterpolator(AnimationUtils.loadInterpolator(this.mContext, 17563650));
            localAnimatorSet.setDuration(250L);
            localAnimatorSet.addListener(this.mHideListener);
            this.mCurrentShowAnim = localAnimatorSet;
            localAnimatorSet.start();
        }
        while (true)
        {
            return;
            this.mHideListener.onAnimationEnd(null);
        }
    }

    public void doShow(boolean paramBoolean)
    {
        if (this.mCurrentShowAnim != null)
            this.mCurrentShowAnim.end();
        this.mTopVisibilityView.setVisibility(0);
        if ((this.mCurWindowVisibility == 0) && ((this.mShowHideAnimationEnabled) || (paramBoolean)))
        {
            this.mTopVisibilityView.setTranslationY(0.0F);
            float f = -this.mTopVisibilityView.getHeight();
            if (paramBoolean)
            {
                int[] arrayOfInt = new int[2];
                arrayOfInt[0] = 0;
                arrayOfInt[1] = 0;
                this.mTopVisibilityView.getLocationInWindow(arrayOfInt);
                f -= arrayOfInt[1];
            }
            this.mTopVisibilityView.setTranslationY(f);
            AnimatorSet localAnimatorSet = new AnimatorSet();
            ViewGroup localViewGroup = this.mTopVisibilityView;
            float[] arrayOfFloat1 = new float[1];
            arrayOfFloat1[0] = 0.0F;
            AnimatorSet.Builder localBuilder = localAnimatorSet.play(ObjectAnimator.ofFloat(localViewGroup, "translationY", arrayOfFloat1));
            if (this.mContentView != null)
            {
                View localView = this.mContentView;
                float[] arrayOfFloat3 = new float[2];
                arrayOfFloat3[0] = f;
                arrayOfFloat3[1] = 0.0F;
                localBuilder.with(ObjectAnimator.ofFloat(localView, "translationY", arrayOfFloat3));
            }
            if ((this.mSplitView != null) && (this.mContextDisplayMode == 1))
            {
                this.mSplitView.setTranslationY(this.mSplitView.getHeight());
                this.mSplitView.setVisibility(0);
                ActionBarContainer localActionBarContainer = this.mSplitView;
                float[] arrayOfFloat2 = new float[1];
                arrayOfFloat2[0] = 0.0F;
                localBuilder.with(ObjectAnimator.ofFloat(localActionBarContainer, "translationY", arrayOfFloat2));
            }
            localAnimatorSet.setInterpolator(AnimationUtils.loadInterpolator(this.mContext, 17563651));
            localAnimatorSet.setDuration(250L);
            localAnimatorSet.addListener(this.mShowListener);
            this.mCurrentShowAnim = localAnimatorSet;
            localAnimatorSet.start();
        }
        while (true)
        {
            if (this.mOverlayLayout != null)
                this.mOverlayLayout.requestFitSystemWindows();
            return;
            this.mTopVisibilityView.setAlpha(1.0F);
            this.mTopVisibilityView.setTranslationY(0.0F);
            if (this.mContentView != null)
                this.mContentView.setTranslationY(0.0F);
            if ((this.mSplitView != null) && (this.mContextDisplayMode == 1))
            {
                this.mSplitView.setAlpha(1.0F);
                this.mSplitView.setTranslationY(0.0F);
                this.mSplitView.setVisibility(0);
            }
            this.mShowListener.onAnimationEnd(null);
        }
    }

    public View getCustomView()
    {
        return this.mActionView.getCustomNavigationView();
    }

    public int getDisplayOptions()
    {
        return this.mActionView.getDisplayOptions();
    }

    public int getHeight()
    {
        return this.mContainerView.getHeight();
    }

    public int getNavigationItemCount()
    {
        int i = 0;
        switch (this.mActionView.getNavigationMode())
        {
        default:
        case 2:
        case 1:
        }
        while (true)
        {
            return i;
            i = this.mTabs.size();
            continue;
            SpinnerAdapter localSpinnerAdapter = this.mActionView.getDropdownAdapter();
            if (localSpinnerAdapter != null)
                i = localSpinnerAdapter.getCount();
        }
    }

    public int getNavigationMode()
    {
        return this.mActionView.getNavigationMode();
    }

    public int getSelectedNavigationIndex()
    {
        int i = -1;
        switch (this.mActionView.getNavigationMode())
        {
        default:
        case 2:
        case 1:
        }
        while (true)
        {
            return i;
            if (this.mSelectedTab != null)
            {
                i = this.mSelectedTab.getPosition();
                continue;
                i = this.mActionView.getDropdownSelectedPosition();
            }
        }
    }

    public ActionBar.Tab getSelectedTab()
    {
        return this.mSelectedTab;
    }

    public CharSequence getSubtitle()
    {
        return this.mActionView.getSubtitle();
    }

    public ActionBar.Tab getTabAt(int paramInt)
    {
        return (ActionBar.Tab)this.mTabs.get(paramInt);
    }

    public int getTabCount()
    {
        return this.mTabs.size();
    }

    public Context getThemedContext()
    {
        int i;
        if (this.mThemedContext == null)
        {
            TypedValue localTypedValue = new TypedValue();
            this.mContext.getTheme().resolveAttribute(16843671, localTypedValue, true);
            i = localTypedValue.resourceId;
            if ((i == 0) || (this.mContext.getThemeResId() == i))
                break label72;
        }
        label72: for (this.mThemedContext = new ContextThemeWrapper(this.mContext, i); ; this.mThemedContext = this.mContext)
            return this.mThemedContext;
    }

    public CharSequence getTitle()
    {
        return this.mActionView.getTitle();
    }

    public boolean hasNonEmbeddedTabs()
    {
        if ((!this.mHasEmbeddedTabs) && (getNavigationMode() == 2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void hide()
    {
        if (!this.mHiddenByApp)
        {
            this.mHiddenByApp = true;
            updateVisibility(false);
        }
    }

    public void hideForSystem()
    {
        if (!this.mHiddenBySystem)
        {
            this.mHiddenBySystem = true;
            updateVisibility(true);
        }
    }

    public boolean isShowing()
    {
        return this.mNowShowing;
    }

    public boolean isSystemShowing()
    {
        if (!this.mHiddenBySystem);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public ActionBar.Tab newTab()
    {
        return new TabImpl();
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
        setHasEmbeddedTabs(ActionBarPolicy.get(this.mContext).hasEmbeddedTabs());
    }

    public void removeAllTabs()
    {
        cleanupTabs();
    }

    public void removeOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener paramOnMenuVisibilityListener)
    {
        this.mMenuVisibilityListeners.remove(paramOnMenuVisibilityListener);
    }

    public void removeTab(ActionBar.Tab paramTab)
    {
        removeTabAt(paramTab.getPosition());
    }

    public void removeTabAt(int paramInt)
    {
        if (this.mTabScrollView == null);
        int i;
        do
        {
            return;
            if (this.mSelectedTab != null);
            for (i = this.mSelectedTab.getPosition(); ; i = this.mSavedTabPosition)
            {
                this.mTabScrollView.removeTabAt(paramInt);
                TabImpl localTabImpl = (TabImpl)this.mTabs.remove(paramInt);
                if (localTabImpl != null)
                    localTabImpl.setPosition(-1);
                int j = this.mTabs.size();
                for (int k = paramInt; k < j; k++)
                    ((TabImpl)this.mTabs.get(k)).setPosition(k);
            }
        }
        while (i != paramInt);
        if (this.mTabs.isEmpty());
        for (Object localObject = null; ; localObject = (TabImpl)this.mTabs.get(Math.max(0, paramInt - 1)))
        {
            selectTab((ActionBar.Tab)localObject);
            break;
        }
    }

    public void selectTab(ActionBar.Tab paramTab)
    {
        int i = -1;
        if (getNavigationMode() != 2)
        {
            if (paramTab != null)
                i = paramTab.getPosition();
            this.mSavedTabPosition = i;
        }
        label176: 
        while (true)
        {
            return;
            FragmentTransaction localFragmentTransaction = this.mActivity.getFragmentManager().beginTransaction().disallowAddToBackStack();
            if (this.mSelectedTab == paramTab)
                if (this.mSelectedTab != null)
                {
                    this.mSelectedTab.getCallback().onTabReselected(this.mSelectedTab, localFragmentTransaction);
                    this.mTabScrollView.animateToTab(paramTab.getPosition());
                }
            while (true)
            {
                if (localFragmentTransaction.isEmpty())
                    break label176;
                localFragmentTransaction.commit();
                break;
                ScrollingTabContainerView localScrollingTabContainerView = this.mTabScrollView;
                if (paramTab != null)
                    i = paramTab.getPosition();
                localScrollingTabContainerView.setTabSelected(i);
                if (this.mSelectedTab != null)
                    this.mSelectedTab.getCallback().onTabUnselected(this.mSelectedTab, localFragmentTransaction);
                this.mSelectedTab = ((TabImpl)paramTab);
                if (this.mSelectedTab != null)
                    this.mSelectedTab.getCallback().onTabSelected(this.mSelectedTab, localFragmentTransaction);
            }
        }
    }

    public void setBackgroundDrawable(Drawable paramDrawable)
    {
        this.mContainerView.setPrimaryBackground(paramDrawable);
    }

    public void setCustomView(int paramInt)
    {
        setCustomView(LayoutInflater.from(getThemedContext()).inflate(paramInt, this.mActionView, false));
    }

    public void setCustomView(View paramView)
    {
        this.mActionView.setCustomNavigationView(paramView);
    }

    public void setCustomView(View paramView, ActionBar.LayoutParams paramLayoutParams)
    {
        paramView.setLayoutParams(paramLayoutParams);
        this.mActionView.setCustomNavigationView(paramView);
    }

    public void setDefaultDisplayHomeAsUpEnabled(boolean paramBoolean)
    {
        if (!this.mDisplayHomeAsUpSet)
            setDisplayHomeAsUpEnabled(paramBoolean);
    }

    public void setDisplayHomeAsUpEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 4; ; i = 0)
        {
            setDisplayOptions(i, 4);
            return;
        }
    }

    public void setDisplayOptions(int paramInt)
    {
        if ((paramInt & 0x4) != 0)
            this.mDisplayHomeAsUpSet = true;
        this.mActionView.setDisplayOptions(paramInt);
    }

    public void setDisplayOptions(int paramInt1, int paramInt2)
    {
        int i = this.mActionView.getDisplayOptions();
        if ((paramInt2 & 0x4) != 0)
            this.mDisplayHomeAsUpSet = true;
        this.mActionView.setDisplayOptions(paramInt1 & paramInt2 | i & (paramInt2 ^ 0xFFFFFFFF));
    }

    public void setDisplayShowCustomEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 16; ; i = 0)
        {
            setDisplayOptions(i, 16);
            return;
        }
    }

    public void setDisplayShowHomeEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 2; ; i = 0)
        {
            setDisplayOptions(i, 2);
            return;
        }
    }

    public void setDisplayShowTitleEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 8; ; i = 0)
        {
            setDisplayOptions(i, 8);
            return;
        }
    }

    public void setDisplayUseLogoEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            setDisplayOptions(i, 1);
            return;
        }
    }

    public void setHomeButtonEnabled(boolean paramBoolean)
    {
        this.mActionView.setHomeButtonEnabled(paramBoolean);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void setHomeViewBackground(int paramInt)
    {
        this.mActionView.setHomeViewBackground(paramInt);
    }

    public void setIcon(int paramInt)
    {
        this.mActionView.setIcon(paramInt);
    }

    public void setIcon(Drawable paramDrawable)
    {
        this.mActionView.setIcon(paramDrawable);
    }

    public void setListNavigationCallbacks(SpinnerAdapter paramSpinnerAdapter, ActionBar.OnNavigationListener paramOnNavigationListener)
    {
        this.mActionView.setDropdownAdapter(paramSpinnerAdapter);
        this.mActionView.setCallback(paramOnNavigationListener);
    }

    public void setLogo(int paramInt)
    {
        this.mActionView.setLogo(paramInt);
    }

    public void setLogo(Drawable paramDrawable)
    {
        this.mActionView.setLogo(paramDrawable);
    }

    public void setNavigationMode(int paramInt)
    {
        boolean bool = false;
        int i = this.mActionView.getNavigationMode();
        switch (i)
        {
        default:
            if ((i != paramInt) && (!this.mHasEmbeddedTabs) && (this.mOverlayLayout != null))
                this.mOverlayLayout.requestFitSystemWindows();
            this.mActionView.setNavigationMode(paramInt);
            switch (paramInt)
            {
            default:
            case 2:
            }
            break;
        case 2:
        }
        while (true)
        {
            ActionBarView localActionBarView = this.mActionView;
            if ((paramInt == 2) && (!this.mHasEmbeddedTabs))
                bool = true;
            localActionBarView.setCollapsable(bool);
            return;
            this.mSavedTabPosition = getSelectedNavigationIndex();
            selectTab(null);
            this.mTabScrollView.setVisibility(8);
            break;
            ensureTabsExist();
            this.mTabScrollView.setVisibility(0);
            if (this.mSavedTabPosition != -1)
            {
                setSelectedNavigationItem(this.mSavedTabPosition);
                this.mSavedTabPosition = -1;
            }
        }
    }

    public void setSelectedNavigationItem(int paramInt)
    {
        switch (this.mActionView.getNavigationMode())
        {
        default:
            throw new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
        case 2:
            selectTab((ActionBar.Tab)this.mTabs.get(paramInt));
        case 1:
        }
        while (true)
        {
            return;
            this.mActionView.setDropdownSelectedPosition(paramInt);
        }
    }

    public void setShowHideAnimationEnabled(boolean paramBoolean)
    {
        this.mShowHideAnimationEnabled = paramBoolean;
        if ((!paramBoolean) && (this.mCurrentShowAnim != null))
            this.mCurrentShowAnim.end();
    }

    public void setSplitBackgroundDrawable(Drawable paramDrawable)
    {
        if (this.mSplitView != null)
            this.mSplitView.setSplitBackground(paramDrawable);
    }

    public void setStackedBackgroundDrawable(Drawable paramDrawable)
    {
        this.mContainerView.setStackedBackground(paramDrawable);
    }

    public void setSubtitle(int paramInt)
    {
        setSubtitle(this.mContext.getString(paramInt));
    }

    public void setSubtitle(CharSequence paramCharSequence)
    {
        this.mActionView.setSubtitle(paramCharSequence);
    }

    public void setTitle(int paramInt)
    {
        setTitle(this.mContext.getString(paramInt));
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        this.mActionView.setTitle(paramCharSequence);
    }

    public void setWindowVisibility(int paramInt)
    {
        this.mCurWindowVisibility = paramInt;
    }

    public void show()
    {
        if (this.mHiddenByApp)
        {
            this.mHiddenByApp = false;
            updateVisibility(false);
        }
    }

    public void showForSystem()
    {
        if (this.mHiddenBySystem)
        {
            this.mHiddenBySystem = false;
            updateVisibility(true);
        }
    }

    public ActionMode startActionMode(ActionMode.Callback paramCallback)
    {
        if (this.mActionMode != null)
            this.mActionMode.finish();
        this.mContextView.killMode();
        ActionModeImpl localActionModeImpl = new ActionModeImpl(paramCallback);
        if (localActionModeImpl.dispatchOnCreate())
        {
            localActionModeImpl.invalidate();
            this.mContextView.initForMode(localActionModeImpl);
            animateToMode(true);
            if ((this.mSplitView != null) && (this.mContextDisplayMode == 1) && (this.mSplitView.getVisibility() != 0))
            {
                this.mSplitView.setVisibility(0);
                if (this.mOverlayLayout != null)
                    this.mOverlayLayout.requestFitSystemWindows();
            }
            this.mContextView.sendAccessibilityEvent(32);
            this.mActionMode = localActionModeImpl;
        }
        while (true)
        {
            return localActionModeImpl;
            localActionModeImpl = null;
        }
    }

    public class TabImpl extends ActionBar.Tab
    {
        private ActionBar.TabListener mCallback;
        private CharSequence mContentDesc;
        private View mCustomView;
        private Drawable mIcon;
        private int mPosition = -1;
        private Object mTag;
        private CharSequence mText;

        public TabImpl()
        {
        }

        public ActionBar.TabListener getCallback()
        {
            return this.mCallback;
        }

        public CharSequence getContentDescription()
        {
            return this.mContentDesc;
        }

        public View getCustomView()
        {
            return this.mCustomView;
        }

        public Drawable getIcon()
        {
            return this.mIcon;
        }

        public int getPosition()
        {
            return this.mPosition;
        }

        public Object getTag()
        {
            return this.mTag;
        }

        public CharSequence getText()
        {
            return this.mText;
        }

        public void select()
        {
            ActionBarImpl.this.selectTab(this);
        }

        public ActionBar.Tab setContentDescription(int paramInt)
        {
            return setContentDescription(ActionBarImpl.this.mContext.getResources().getText(paramInt));
        }

        public ActionBar.Tab setContentDescription(CharSequence paramCharSequence)
        {
            this.mContentDesc = paramCharSequence;
            if (this.mPosition >= 0)
                ActionBarImpl.this.mTabScrollView.updateTab(this.mPosition);
            return this;
        }

        public ActionBar.Tab setCustomView(int paramInt)
        {
            return setCustomView(LayoutInflater.from(ActionBarImpl.this.getThemedContext()).inflate(paramInt, null));
        }

        public ActionBar.Tab setCustomView(View paramView)
        {
            this.mCustomView = paramView;
            if (this.mPosition >= 0)
                ActionBarImpl.this.mTabScrollView.updateTab(this.mPosition);
            return this;
        }

        public ActionBar.Tab setIcon(int paramInt)
        {
            return setIcon(ActionBarImpl.this.mContext.getResources().getDrawable(paramInt));
        }

        public ActionBar.Tab setIcon(Drawable paramDrawable)
        {
            this.mIcon = paramDrawable;
            if (this.mPosition >= 0)
                ActionBarImpl.this.mTabScrollView.updateTab(this.mPosition);
            return this;
        }

        public void setPosition(int paramInt)
        {
            this.mPosition = paramInt;
        }

        public ActionBar.Tab setTabListener(ActionBar.TabListener paramTabListener)
        {
            this.mCallback = paramTabListener;
            return this;
        }

        public ActionBar.Tab setTag(Object paramObject)
        {
            this.mTag = paramObject;
            return this;
        }

        public ActionBar.Tab setText(int paramInt)
        {
            return setText(ActionBarImpl.this.mContext.getResources().getText(paramInt));
        }

        public ActionBar.Tab setText(CharSequence paramCharSequence)
        {
            this.mText = paramCharSequence;
            if (this.mPosition >= 0)
                ActionBarImpl.this.mTabScrollView.updateTab(this.mPosition);
            return this;
        }
    }

    public class ActionModeImpl extends ActionMode
        implements MenuBuilder.Callback
    {
        private ActionMode.Callback mCallback;
        private WeakReference<View> mCustomView;
        private MenuBuilder mMenu;

        public ActionModeImpl(ActionMode.Callback arg2)
        {
            Object localObject;
            this.mCallback = localObject;
            this.mMenu = new MenuBuilder(ActionBarImpl.this.getThemedContext()).setDefaultShowAsAction(1);
            this.mMenu.setCallback(this);
        }

        public boolean dispatchOnCreate()
        {
            this.mMenu.stopDispatchingItemsChanged();
            try
            {
                boolean bool = this.mCallback.onCreateActionMode(this, this.mMenu);
                return bool;
            }
            finally
            {
                this.mMenu.startDispatchingItemsChanged();
            }
        }

        public void finish()
        {
            if (ActionBarImpl.this.mActionMode != this)
                return;
            if (!ActionBarImpl.checkShowingFlags(ActionBarImpl.this.mHiddenByApp, ActionBarImpl.this.mHiddenBySystem, false))
            {
                ActionBarImpl.this.mDeferredDestroyActionMode = this;
                ActionBarImpl.this.mDeferredModeDestroyCallback = this.mCallback;
            }
            while (true)
            {
                this.mCallback = null;
                ActionBarImpl.this.animateToMode(false);
                ActionBarImpl.this.mContextView.closeMode();
                ActionBarImpl.this.mActionView.sendAccessibilityEvent(32);
                ActionBarImpl.this.mActionMode = null;
                break;
                this.mCallback.onDestroyActionMode(this);
            }
        }

        public View getCustomView()
        {
            if (this.mCustomView != null);
            for (View localView = (View)this.mCustomView.get(); ; localView = null)
                return localView;
        }

        public Menu getMenu()
        {
            return this.mMenu;
        }

        public MenuInflater getMenuInflater()
        {
            return new MenuInflater(ActionBarImpl.this.getThemedContext());
        }

        public CharSequence getSubtitle()
        {
            return ActionBarImpl.this.mContextView.getSubtitle();
        }

        public CharSequence getTitle()
        {
            return ActionBarImpl.this.mContextView.getTitle();
        }

        public void invalidate()
        {
            this.mMenu.stopDispatchingItemsChanged();
            try
            {
                this.mCallback.onPrepareActionMode(this, this.mMenu);
                return;
            }
            finally
            {
                this.mMenu.startDispatchingItemsChanged();
            }
        }

        public boolean isTitleOptional()
        {
            return ActionBarImpl.this.mContextView.isTitleOptional();
        }

        public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean)
        {
        }

        public void onCloseSubMenu(SubMenuBuilder paramSubMenuBuilder)
        {
        }

        public boolean onMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
        {
            if (this.mCallback != null);
            for (boolean bool = this.mCallback.onActionItemClicked(this, paramMenuItem); ; bool = false)
                return bool;
        }

        public void onMenuModeChange(MenuBuilder paramMenuBuilder)
        {
            if (this.mCallback == null);
            while (true)
            {
                return;
                invalidate();
                ActionBarImpl.this.mContextView.showOverflowMenu();
            }
        }

        public boolean onSubMenuSelected(SubMenuBuilder paramSubMenuBuilder)
        {
            boolean bool = true;
            if (this.mCallback == null)
                bool = false;
            while (true)
            {
                return bool;
                if (paramSubMenuBuilder.hasVisibleItems())
                    new MenuPopupHelper(ActionBarImpl.this.getThemedContext(), paramSubMenuBuilder).show();
            }
        }

        public void setCustomView(View paramView)
        {
            ActionBarImpl.this.mContextView.setCustomView(paramView);
            this.mCustomView = new WeakReference(paramView);
        }

        public void setSubtitle(int paramInt)
        {
            setSubtitle(ActionBarImpl.this.mContext.getResources().getString(paramInt));
        }

        public void setSubtitle(CharSequence paramCharSequence)
        {
            ActionBarImpl.this.mContextView.setSubtitle(paramCharSequence);
        }

        public void setTitle(int paramInt)
        {
            setTitle(ActionBarImpl.this.mContext.getResources().getString(paramInt));
        }

        public void setTitle(CharSequence paramCharSequence)
        {
            ActionBarImpl.this.mContextView.setTitle(paramCharSequence);
        }

        public void setTitleOptionalHint(boolean paramBoolean)
        {
            super.setTitleOptionalHint(paramBoolean);
            ActionBarImpl.this.mContextView.setTitleOptional(paramBoolean);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.ActionBarImpl
 * JD-Core Version:        0.6.2
 */