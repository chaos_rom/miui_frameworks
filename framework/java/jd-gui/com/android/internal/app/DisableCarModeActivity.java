package com.android.internal.app;

import android.app.Activity;
import android.app.IUiModeManager;
import android.app.IUiModeManager.Stub;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class DisableCarModeActivity extends Activity
{
    private static final String TAG = "DisableCarModeActivity";

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        try
        {
            IUiModeManager.Stub.asInterface(ServiceManager.getService("uimode")).disableCarMode(1);
            finish();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("DisableCarModeActivity", "Failed to disable car mode", localRemoteException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.DisableCarModeActivity
 * JD-Core Version:        0.6.2
 */