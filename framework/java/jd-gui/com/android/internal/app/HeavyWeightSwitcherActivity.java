package com.android.internal.app;

import android.app.Activity;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class HeavyWeightSwitcherActivity extends Activity
{
    public static final String KEY_CUR_APP = "cur_app";
    public static final String KEY_CUR_TASK = "cur_task";
    public static final String KEY_HAS_RESULT = "has_result";
    public static final String KEY_INTENT = "intent";
    public static final String KEY_NEW_APP = "new_app";
    private View.OnClickListener mCancelListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            HeavyWeightSwitcherActivity.this.finish();
        }
    };
    String mCurApp;
    int mCurTask;
    boolean mHasResult;
    String mNewApp;
    IntentSender mStartIntent;
    private View.OnClickListener mSwitchNewListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            try
            {
                ActivityManagerNative.getDefault().finishHeavyWeightApp();
                try
                {
                    label8: if (HeavyWeightSwitcherActivity.this.mHasResult)
                        HeavyWeightSwitcherActivity.this.startIntentSenderForResult(HeavyWeightSwitcherActivity.this.mStartIntent, -1, null, 33554432, 33554432, 0);
                    while (true)
                    {
                        HeavyWeightSwitcherActivity.this.finish();
                        return;
                        HeavyWeightSwitcherActivity.this.startIntentSenderForResult(HeavyWeightSwitcherActivity.this.mStartIntent, -1, null, 0, 0, 0);
                    }
                }
                catch (IntentSender.SendIntentException localSendIntentException)
                {
                    while (true)
                        Log.w("HeavyWeightSwitcherActivity", "Failure starting", localSendIntentException);
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label8;
            }
        }
    };
    private View.OnClickListener mSwitchOldListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            try
            {
                ActivityManagerNative.getDefault().moveTaskToFront(HeavyWeightSwitcherActivity.this.mCurTask, 0, null);
                label17: HeavyWeightSwitcherActivity.this.finish();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label17;
            }
        }
    };

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        requestWindowFeature(3);
        this.mStartIntent = ((IntentSender)getIntent().getParcelableExtra("intent"));
        this.mHasResult = getIntent().getBooleanExtra("has_result", false);
        this.mCurApp = getIntent().getStringExtra("cur_app");
        this.mCurTask = getIntent().getIntExtra("cur_task", 0);
        this.mNewApp = getIntent().getStringExtra("new_app");
        setContentView(17367108);
        setIconAndText(16908932, 16908933, 16908934, this.mCurApp, 17040363, 17040364);
        setIconAndText(16908936, 16908937, 16908938, this.mNewApp, 17040365, 17040366);
        findViewById(16908931).setOnClickListener(this.mSwitchOldListener);
        findViewById(16908935).setOnClickListener(this.mSwitchNewListener);
        findViewById(16908905).setOnClickListener(this.mCancelListener);
        TypedValue localTypedValue = new TypedValue();
        getTheme().resolveAttribute(16843605, localTypedValue, true);
        getWindow().setFeatureDrawableResource(3, localTypedValue.resourceId);
    }

    void setDrawable(int paramInt, Drawable paramDrawable)
    {
        if (paramDrawable != null)
            ((ImageView)findViewById(paramInt)).setImageDrawable(paramDrawable);
    }

    void setIconAndText(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5)
    {
        Object localObject1 = "";
        Object localObject2 = null;
        if (this.mCurApp != null);
        try
        {
            ApplicationInfo localApplicationInfo = getPackageManager().getApplicationInfo(paramString, 0);
            localObject1 = localApplicationInfo.loadLabel(getPackageManager());
            Drawable localDrawable = localApplicationInfo.loadIcon(getPackageManager());
            localObject2 = localDrawable;
            label52: setDrawable(paramInt1, localObject2);
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = localObject1;
            setText(paramInt2, getString(paramInt4, arrayOfObject));
            setText(paramInt3, getText(paramInt5));
            return;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            break label52;
        }
    }

    void setText(int paramInt, CharSequence paramCharSequence)
    {
        ((TextView)findViewById(paramInt)).setText(paramCharSequence);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.HeavyWeightSwitcherActivity
 * JD-Core Version:        0.6.2
 */