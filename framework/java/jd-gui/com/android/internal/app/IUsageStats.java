package com.android.internal.app;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.android.internal.os.PkgUsageStats;

public abstract interface IUsageStats extends IInterface
{
    public abstract PkgUsageStats[] getAllPkgUsageStats()
        throws RemoteException;

    public abstract PkgUsageStats getPkgUsageStats(ComponentName paramComponentName)
        throws RemoteException;

    public abstract void noteLaunchTime(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract void notePauseComponent(ComponentName paramComponentName)
        throws RemoteException;

    public abstract void noteResumeComponent(ComponentName paramComponentName)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IUsageStats
    {
        private static final String DESCRIPTOR = "com.android.internal.app.IUsageStats";
        static final int TRANSACTION_getAllPkgUsageStats = 5;
        static final int TRANSACTION_getPkgUsageStats = 4;
        static final int TRANSACTION_noteLaunchTime = 3;
        static final int TRANSACTION_notePauseComponent = 2;
        static final int TRANSACTION_noteResumeComponent = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.app.IUsageStats");
        }

        public static IUsageStats asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.app.IUsageStats");
                if ((localIInterface != null) && ((localIInterface instanceof IUsageStats)))
                    localObject = (IUsageStats)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("com.android.internal.app.IUsageStats");
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IUsageStats");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName4 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName4 = null)
                {
                    noteResumeComponent(localComponentName4);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IUsageStats");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName3 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName3 = null)
                {
                    notePauseComponent(localComponentName3);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IUsageStats");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName2 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName2 = null)
                {
                    noteLaunchTime(localComponentName2, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IUsageStats");
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName1 = null)
                {
                    PkgUsageStats localPkgUsageStats = getPkgUsageStats(localComponentName1);
                    paramParcel2.writeNoException();
                    if (localPkgUsageStats == null)
                        break label296;
                    paramParcel2.writeInt(i);
                    localPkgUsageStats.writeToParcel(paramParcel2, i);
                    break;
                }
                label296: paramParcel2.writeInt(0);
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IUsageStats");
                PkgUsageStats[] arrayOfPkgUsageStats = getAllPkgUsageStats();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedArray(arrayOfPkgUsageStats, i);
            }
        }

        private static class Proxy
            implements IUsageStats
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public PkgUsageStats[] getAllPkgUsageStats()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IUsageStats");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    PkgUsageStats[] arrayOfPkgUsageStats = (PkgUsageStats[])localParcel2.createTypedArray(PkgUsageStats.CREATOR);
                    return arrayOfPkgUsageStats;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.app.IUsageStats";
            }

            public PkgUsageStats getPkgUsageStats(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.app.IUsageStats");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(4, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localPkgUsageStats = (PkgUsageStats)PkgUsageStats.CREATOR.createFromParcel(localParcel2);
                                return localPkgUsageStats;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    PkgUsageStats localPkgUsageStats = null;
                }
            }

            public void noteLaunchTime(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IUsageStats");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notePauseComponent(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IUsageStats");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void noteResumeComponent(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IUsageStats");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.IUsageStats
 * JD-Core Version:        0.6.2
 */