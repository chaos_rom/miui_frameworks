package com.android.internal.app;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

public abstract class AlertActivity extends Activity
    implements DialogInterface
{
    protected AlertController mAlert;
    protected AlertController.AlertParams mAlertParams;

    public void cancel()
    {
        finish();
    }

    public void dismiss()
    {
        if (!isFinishing())
            finish();
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        this.mAlert = new AlertController(this, this, getWindow());
        this.mAlertParams = new AlertController.AlertParams(this);
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        if (this.mAlert.onKeyDown(paramInt, paramKeyEvent));
        for (boolean bool = true; ; bool = super.onKeyDown(paramInt, paramKeyEvent))
            return bool;
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        if (this.mAlert.onKeyUp(paramInt, paramKeyEvent));
        for (boolean bool = true; ; bool = super.onKeyUp(paramInt, paramKeyEvent))
            return bool;
    }

    protected void setupAlert()
    {
        this.mAlertParams.apply(this.mAlert);
        this.mAlert.installContent();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.AlertActivity
 * JD-Core Version:        0.6.2
 */