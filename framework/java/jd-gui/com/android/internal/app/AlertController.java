package com.android.internal.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.android.internal.R.styleable;
import java.lang.ref.WeakReference;

public class AlertController
{
    private ListAdapter mAdapter;
    private int mAlertDialogLayout;
    View.OnClickListener mButtonHandler = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            Message localMessage = null;
            if ((paramAnonymousView == AlertController.this.mButtonPositive) && (AlertController.this.mButtonPositiveMessage != null))
                localMessage = Message.obtain(AlertController.this.mButtonPositiveMessage);
            while (true)
            {
                if (localMessage != null)
                    localMessage.sendToTarget();
                AlertController.this.mHandler.obtainMessage(1, AlertController.this.mDialogInterface).sendToTarget();
                return;
                if ((paramAnonymousView == AlertController.this.mButtonNegative) && (AlertController.this.mButtonNegativeMessage != null))
                    localMessage = Message.obtain(AlertController.this.mButtonNegativeMessage);
                else if ((paramAnonymousView == AlertController.this.mButtonNeutral) && (AlertController.this.mButtonNeutralMessage != null))
                    localMessage = Message.obtain(AlertController.this.mButtonNeutralMessage);
            }
        }
    };
    private Button mButtonNegative;
    private Message mButtonNegativeMessage;
    private CharSequence mButtonNegativeText;
    private Button mButtonNeutral;
    private Message mButtonNeutralMessage;
    private CharSequence mButtonNeutralText;
    private Button mButtonPositive;
    private Message mButtonPositiveMessage;
    private CharSequence mButtonPositiveText;
    private int mCheckedItem = -1;
    private final Context mContext;
    private View mCustomTitleView;
    private final DialogInterface mDialogInterface;
    private boolean mForceInverseBackground;
    private Handler mHandler;
    private Drawable mIcon;
    private int mIconId = -1;
    private ImageView mIconView;
    private int mListItemLayout;
    private int mListLayout;
    private ListView mListView;
    private CharSequence mMessage;
    private TextView mMessageView;
    private int mMultiChoiceItemLayout;
    private ScrollView mScrollView;
    private int mSingleChoiceItemLayout;
    private CharSequence mTitle;
    private TextView mTitleView;
    private View mView;
    private int mViewSpacingBottom;
    private int mViewSpacingLeft;
    private int mViewSpacingRight;
    private boolean mViewSpacingSpecified = false;
    private int mViewSpacingTop;
    private final Window mWindow;

    public AlertController(Context paramContext, DialogInterface paramDialogInterface, Window paramWindow)
    {
        this.mContext = paramContext;
        this.mDialogInterface = paramDialogInterface;
        this.mWindow = paramWindow;
        this.mHandler = new ButtonHandler(paramDialogInterface);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(null, R.styleable.AlertDialog, 16842845, 0);
        this.mAlertDialogLayout = localTypedArray.getResourceId(10, 17367075);
        this.mListLayout = localTypedArray.getResourceId(11, 17367204);
        this.mMultiChoiceItemLayout = localTypedArray.getResourceId(12, 17367059);
        this.mSingleChoiceItemLayout = localTypedArray.getResourceId(13, 17367058);
        this.mListItemLayout = localTypedArray.getResourceId(14, 17367057);
        localTypedArray.recycle();
    }

    static boolean canTextInput(View paramView)
    {
        boolean bool = true;
        if (paramView.onCheckIsTextEditor());
        while (true)
        {
            return bool;
            if (!(paramView instanceof ViewGroup))
            {
                bool = false;
            }
            else
            {
                ViewGroup localViewGroup = (ViewGroup)paramView;
                int i = localViewGroup.getChildCount();
                while (true)
                    if (i > 0)
                    {
                        i--;
                        if (canTextInput(localViewGroup.getChildAt(i)))
                            break;
                    }
                bool = false;
            }
        }
    }

    private void centerButton(Button paramButton)
    {
        LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)paramButton.getLayoutParams();
        localLayoutParams.gravity = 1;
        localLayoutParams.weight = 0.5F;
        paramButton.setLayoutParams(localLayoutParams);
        View localView1 = this.mWindow.findViewById(16908880);
        if (localView1 != null)
            localView1.setVisibility(0);
        View localView2 = this.mWindow.findViewById(16908881);
        if (localView2 != null)
            localView2.setVisibility(0);
    }

    private void setBackground(LinearLayout paramLinearLayout1, LinearLayout paramLinearLayout2, View paramView1, boolean paramBoolean1, TypedArray paramTypedArray, boolean paramBoolean2, View paramView2)
    {
        int i = paramTypedArray.getResourceId(0, 17302627);
        int j = paramTypedArray.getResourceId(1, 17302635);
        int k = paramTypedArray.getResourceId(2, 17302624);
        int m = paramTypedArray.getResourceId(3, 17302621);
        int n = paramTypedArray.getResourceId(4, 17302626);
        int i1 = paramTypedArray.getResourceId(5, 17302634);
        int i2 = paramTypedArray.getResourceId(6, 17302623);
        int i3 = paramTypedArray.getResourceId(7, 17302620);
        int i4 = paramTypedArray.getResourceId(8, 17302622);
        View[] arrayOfView = new View[4];
        boolean[] arrayOfBoolean = new boolean[4];
        Object localObject = null;
        int i5 = 0;
        int i6 = 0;
        if (paramBoolean2)
        {
            arrayOfView[i6] = paramLinearLayout1;
            arrayOfBoolean[i6] = false;
            i6 = 0 + 1;
        }
        if (paramLinearLayout2.getVisibility() == 8)
            paramLinearLayout2 = null;
        arrayOfView[i6] = paramLinearLayout2;
        if (this.mListView != null);
        int i9;
        int i10;
        View localView;
        for (int i7 = 1; ; i7 = 0)
        {
            arrayOfBoolean[i6] = i7;
            int i8 = i6 + 1;
            if (paramView1 != null)
            {
                arrayOfView[i8] = paramView1;
                arrayOfBoolean[i8] = this.mForceInverseBackground;
                i8++;
            }
            if (paramBoolean1)
            {
                arrayOfView[i8] = paramView2;
                arrayOfBoolean[i8] = true;
            }
            i9 = 0;
            for (i10 = 0; ; i10++)
            {
                if (i10 >= arrayOfView.length)
                    break label329;
                localView = arrayOfView[i10];
                if (localView != null)
                    break;
            }
        }
        if (localObject != null)
        {
            if (i9 != 0)
                break label303;
            if (i5 == 0)
                break label296;
        }
        label296: for (int i12 = i1; ; i12 = j)
        {
            localObject.setBackgroundResource(i12);
            i9 = 1;
            localObject = localView;
            i5 = arrayOfBoolean[i10];
            break;
        }
        label303: if (i5 != 0);
        for (int i11 = i2; ; i11 = k)
        {
            localObject.setBackgroundResource(i11);
            break;
        }
        label329: if (localObject != null)
        {
            if (i9 == 0)
                break label428;
            if (i5 == 0)
                break label421;
            if (!paramBoolean1)
                break label414;
        }
        while (true)
        {
            localObject.setBackgroundResource(i4);
            if ((this.mListView != null) && (this.mAdapter != null))
            {
                this.mListView.setAdapter(this.mAdapter);
                if (this.mCheckedItem > -1)
                {
                    this.mListView.setItemChecked(this.mCheckedItem, true);
                    this.mListView.setSelection(this.mCheckedItem);
                }
            }
            return;
            label414: i4 = i3;
            continue;
            label421: i4 = m;
        }
        label428: if (i5 != 0);
        while (true)
        {
            localObject.setBackgroundResource(n);
            break;
            n = i;
        }
    }

    private boolean setupButtons()
    {
        int i = 0;
        this.mButtonPositive = ((Button)this.mWindow.findViewById(16908313));
        this.mButtonPositive.setOnClickListener(this.mButtonHandler);
        if (TextUtils.isEmpty(this.mButtonPositiveText))
        {
            this.mButtonPositive.setVisibility(8);
            this.mButtonNegative = ((Button)this.mWindow.findViewById(16908314));
            this.mButtonNegative.setOnClickListener(this.mButtonHandler);
            if (!TextUtils.isEmpty(this.mButtonNegativeText))
                break label200;
            this.mButtonNegative.setVisibility(8);
            label96: this.mButtonNeutral = ((Button)this.mWindow.findViewById(16908315));
            this.mButtonNeutral.setOnClickListener(this.mButtonHandler);
            if (!TextUtils.isEmpty(this.mButtonNeutralText))
                break label226;
            this.mButtonNeutral.setVisibility(8);
            label143: if (shouldCenterSingleButton(this.mContext))
            {
                if (i != 1)
                    break label252;
                centerButton(this.mButtonPositive);
            }
            label166: if (i == 0)
                break label284;
        }
        label284: for (boolean bool = true; ; bool = false)
        {
            return bool;
            this.mButtonPositive.setText(this.mButtonPositiveText);
            this.mButtonPositive.setVisibility(0);
            i = 0x0 | 0x1;
            break;
            label200: this.mButtonNegative.setText(this.mButtonNegativeText);
            this.mButtonNegative.setVisibility(0);
            i |= 2;
            break label96;
            label226: this.mButtonNeutral.setText(this.mButtonNeutralText);
            this.mButtonNeutral.setVisibility(0);
            i |= 4;
            break label143;
            label252: if (i == 2)
            {
                centerButton(this.mButtonNeutral);
                break label166;
            }
            if (i != 4)
                break label166;
            centerButton(this.mButtonNeutral);
            break label166;
        }
    }

    private void setupContent(LinearLayout paramLinearLayout)
    {
        this.mScrollView = ((ScrollView)this.mWindow.findViewById(16908883));
        this.mScrollView.setFocusable(false);
        this.mMessageView = ((TextView)this.mWindow.findViewById(16908299));
        if (this.mMessageView == null);
        while (true)
        {
            return;
            if (this.mMessage != null)
            {
                this.mMessageView.setText(this.mMessage);
            }
            else
            {
                this.mMessageView.setVisibility(8);
                this.mScrollView.removeView(this.mMessageView);
                if (this.mListView != null)
                {
                    paramLinearLayout.removeView(this.mWindow.findViewById(16908883));
                    paramLinearLayout.addView(this.mListView, new LinearLayout.LayoutParams(-1, -1));
                    paramLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0F));
                }
                else
                {
                    paramLinearLayout.setVisibility(8);
                }
            }
        }
    }

    private boolean setupTitle(LinearLayout paramLinearLayout)
    {
        int i = 0;
        boolean bool = true;
        if (this.mCustomTitleView != null)
        {
            LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
            paramLinearLayout.addView(this.mCustomTitleView, 0, localLayoutParams);
            this.mWindow.findViewById(16908876).setVisibility(8);
        }
        while (true)
        {
            return bool;
            if (!TextUtils.isEmpty(this.mTitle))
                i = 1;
            this.mIconView = ((ImageView)this.mWindow.findViewById(16908294));
            if (i != 0)
            {
                this.mTitleView = ((TextView)this.mWindow.findViewById(16908877));
                this.mTitleView.setText(this.mTitle);
                if (this.mIconId > 0)
                {
                    this.mIconView.setImageResource(this.mIconId);
                }
                else if (this.mIcon != null)
                {
                    this.mIconView.setImageDrawable(this.mIcon);
                }
                else if (this.mIconId == 0)
                {
                    this.mTitleView.setPadding(this.mIconView.getPaddingLeft(), this.mIconView.getPaddingTop(), this.mIconView.getPaddingRight(), this.mIconView.getPaddingBottom());
                    this.mIconView.setVisibility(8);
                }
            }
            else
            {
                this.mWindow.findViewById(16908876).setVisibility(8);
                this.mIconView.setVisibility(8);
                paramLinearLayout.setVisibility(8);
                bool = false;
            }
        }
    }

    private void setupView()
    {
        LinearLayout localLinearLayout1 = (LinearLayout)this.mWindow.findViewById(16908882);
        setupContent(localLinearLayout1);
        boolean bool1 = setupButtons();
        LinearLayout localLinearLayout2 = (LinearLayout)this.mWindow.findViewById(16908875);
        TypedArray localTypedArray = this.mContext.obtainStyledAttributes(null, R.styleable.AlertDialog, 16842845, 0);
        boolean bool2 = setupTitle(localLinearLayout2);
        View localView1 = this.mWindow.findViewById(16908879);
        if (!bool1)
        {
            localView1.setVisibility(8);
            this.mWindow.setCloseOnTouchOutsideIfNotSet(true);
        }
        FrameLayout localFrameLayout1 = null;
        if (this.mView != null)
        {
            localFrameLayout1 = (FrameLayout)this.mWindow.findViewById(16908884);
            FrameLayout localFrameLayout2 = (FrameLayout)this.mWindow.findViewById(16908331);
            localFrameLayout2.addView(this.mView, new ViewGroup.LayoutParams(-1, -1));
            if (this.mViewSpacingSpecified)
                localFrameLayout2.setPadding(this.mViewSpacingLeft, this.mViewSpacingTop, this.mViewSpacingRight, this.mViewSpacingBottom);
            if (this.mListView != null)
                ((LinearLayout.LayoutParams)localFrameLayout1.getLayoutParams()).weight = 0.0F;
            if (bool2)
                if ((this.mMessage == null) && (this.mView == null) && (this.mListView == null))
                    break label287;
        }
        label287: for (View localView2 = this.mWindow.findViewById(16908878); ; localView2 = this.mWindow.findViewById(16908885))
        {
            if (localView2 != null)
                localView2.setVisibility(0);
            setBackground(localLinearLayout2, localLinearLayout1, localFrameLayout1, bool1, localTypedArray, bool2, localView1);
            localTypedArray.recycle();
            return;
            this.mWindow.findViewById(16908884).setVisibility(8);
            break;
        }
    }

    private static boolean shouldCenterSingleButton(Context paramContext)
    {
        boolean bool = true;
        TypedValue localTypedValue = new TypedValue();
        paramContext.getTheme().resolveAttribute(16843707, localTypedValue, bool);
        if (localTypedValue.data != 0);
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public Button getButton(int paramInt)
    {
        Button localButton;
        switch (paramInt)
        {
        default:
            localButton = null;
        case -1:
        case -2:
        case -3:
        }
        while (true)
        {
            return localButton;
            localButton = this.mButtonPositive;
            continue;
            localButton = this.mButtonNegative;
            continue;
            localButton = this.mButtonNeutral;
        }
    }

    public ListView getListView()
    {
        return this.mListView;
    }

    public void installContent()
    {
        this.mWindow.requestFeature(1);
        if ((this.mView == null) || (!canTextInput(this.mView)))
            this.mWindow.setFlags(131072, 131072);
        this.mWindow.setContentView(this.mAlertDialogLayout);
        setupView();
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        if ((this.mScrollView != null) && (this.mScrollView.executeKeyEvent(paramKeyEvent)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        if ((this.mScrollView != null) && (this.mScrollView.executeKeyEvent(paramKeyEvent)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setButton(int paramInt, CharSequence paramCharSequence, DialogInterface.OnClickListener paramOnClickListener, Message paramMessage)
    {
        if ((paramMessage == null) && (paramOnClickListener != null))
            paramMessage = this.mHandler.obtainMessage(paramInt, paramOnClickListener);
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Button does not exist");
        case -1:
            this.mButtonPositiveText = paramCharSequence;
            this.mButtonPositiveMessage = paramMessage;
        case -2:
        case -3:
        }
        while (true)
        {
            return;
            this.mButtonNegativeText = paramCharSequence;
            this.mButtonNegativeMessage = paramMessage;
            continue;
            this.mButtonNeutralText = paramCharSequence;
            this.mButtonNeutralMessage = paramMessage;
        }
    }

    public void setCustomTitle(View paramView)
    {
        this.mCustomTitleView = paramView;
    }

    public void setIcon(int paramInt)
    {
        this.mIconId = paramInt;
        if (this.mIconView != null)
        {
            if (paramInt <= 0)
                break label28;
            this.mIconView.setImageResource(this.mIconId);
        }
        while (true)
        {
            return;
            label28: if (paramInt == 0)
                this.mIconView.setVisibility(8);
        }
    }

    public void setIcon(Drawable paramDrawable)
    {
        this.mIcon = paramDrawable;
        if ((this.mIconView != null) && (this.mIcon != null))
            this.mIconView.setImageDrawable(paramDrawable);
    }

    public void setInverseBackgroundForced(boolean paramBoolean)
    {
        this.mForceInverseBackground = paramBoolean;
    }

    public void setMessage(CharSequence paramCharSequence)
    {
        this.mMessage = paramCharSequence;
        if (this.mMessageView != null)
            this.mMessageView.setText(paramCharSequence);
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        this.mTitle = paramCharSequence;
        if (this.mTitleView != null)
            this.mTitleView.setText(paramCharSequence);
    }

    public void setView(View paramView)
    {
        this.mView = paramView;
        this.mViewSpacingSpecified = false;
    }

    public void setView(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mView = paramView;
        this.mViewSpacingSpecified = true;
        this.mViewSpacingLeft = paramInt1;
        this.mViewSpacingTop = paramInt2;
        this.mViewSpacingRight = paramInt3;
        this.mViewSpacingBottom = paramInt4;
    }

    public static class AlertParams
    {
        public ListAdapter mAdapter;
        public boolean mCancelable;
        public int mCheckedItem = -1;
        public boolean[] mCheckedItems;
        public final Context mContext;
        public Cursor mCursor;
        public View mCustomTitleView;
        public boolean mForceInverseBackground;
        public Drawable mIcon;
        public int mIconId = 0;
        public final LayoutInflater mInflater;
        public String mIsCheckedColumn;
        public boolean mIsMultiChoice;
        public boolean mIsSingleChoice;
        public CharSequence[] mItems;
        public String mLabelColumn;
        public CharSequence mMessage;
        public DialogInterface.OnClickListener mNegativeButtonListener;
        public CharSequence mNegativeButtonText;
        public DialogInterface.OnClickListener mNeutralButtonListener;
        public CharSequence mNeutralButtonText;
        public DialogInterface.OnCancelListener mOnCancelListener;
        public DialogInterface.OnMultiChoiceClickListener mOnCheckboxClickListener;
        public DialogInterface.OnClickListener mOnClickListener;
        public AdapterView.OnItemSelectedListener mOnItemSelectedListener;
        public DialogInterface.OnKeyListener mOnKeyListener;
        public OnPrepareListViewListener mOnPrepareListViewListener;
        public DialogInterface.OnClickListener mPositiveButtonListener;
        public CharSequence mPositiveButtonText;
        public boolean mRecycleOnMeasure = true;
        public CharSequence mTitle;
        public View mView;
        public int mViewSpacingBottom;
        public int mViewSpacingLeft;
        public int mViewSpacingRight;
        public boolean mViewSpacingSpecified = false;
        public int mViewSpacingTop;

        public AlertParams(Context paramContext)
        {
            this.mContext = paramContext;
            this.mCancelable = true;
            this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
        }

        private void createListView(final AlertController paramAlertController)
        {
            final AlertController.RecycleListView localRecycleListView = (AlertController.RecycleListView)this.mInflater.inflate(paramAlertController.mListLayout, null);
            Object localObject;
            if (this.mIsMultiChoice)
                if (this.mCursor == null)
                {
                    localObject = new ArrayAdapter(this.mContext, paramAlertController.mMultiChoiceItemLayout, 16908308, this.mItems)
                    {
                        public View getView(int paramAnonymousInt, View paramAnonymousView, ViewGroup paramAnonymousViewGroup)
                        {
                            View localView = super.getView(paramAnonymousInt, paramAnonymousView, paramAnonymousViewGroup);
                            if ((AlertController.AlertParams.this.mCheckedItems != null) && (AlertController.AlertParams.this.mCheckedItems[paramAnonymousInt] != 0))
                                localRecycleListView.setItemChecked(paramAnonymousInt, true);
                            return localView;
                        }
                    };
                    if (this.mOnPrepareListViewListener != null)
                        this.mOnPrepareListViewListener.onPrepareListView(localRecycleListView);
                    AlertController.access$1202(paramAlertController, (ListAdapter)localObject);
                    AlertController.access$1302(paramAlertController, this.mCheckedItem);
                    if (this.mOnClickListener == null)
                        break label306;
                    localRecycleListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                        public void onItemClick(AdapterView paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
                        {
                            AlertController.AlertParams.this.mOnClickListener.onClick(paramAlertController.mDialogInterface, paramAnonymousInt);
                            if (!AlertController.AlertParams.this.mIsSingleChoice)
                                paramAlertController.mDialogInterface.dismiss();
                        }
                    });
                    label108: if (this.mOnItemSelectedListener != null)
                        localRecycleListView.setOnItemSelectedListener(this.mOnItemSelectedListener);
                    if (!this.mIsSingleChoice)
                        break label330;
                    localRecycleListView.setChoiceMode(1);
                }
            while (true)
            {
                localRecycleListView.mRecycleOnMeasure = this.mRecycleOnMeasure;
                AlertController.access$1402(paramAlertController, localRecycleListView);
                return;
                Context localContext2 = this.mContext;
                Cursor localCursor2 = this.mCursor;
                localObject = new CursorAdapter(localContext2, localCursor2, false)
                {
                    private final int mIsCheckedIndex;
                    private final int mLabelIndex;

                    public void bindView(View paramAnonymousView, Context paramAnonymousContext, Cursor paramAnonymousCursor)
                    {
                        int i = 1;
                        ((CheckedTextView)paramAnonymousView.findViewById(16908308)).setText(paramAnonymousCursor.getString(this.mLabelIndex));
                        AlertController.RecycleListView localRecycleListView = localRecycleListView;
                        int k = paramAnonymousCursor.getPosition();
                        if (paramAnonymousCursor.getInt(this.mIsCheckedIndex) == i);
                        while (true)
                        {
                            localRecycleListView.setItemChecked(k, i);
                            return;
                            int j = 0;
                        }
                    }

                    public View newView(Context paramAnonymousContext, Cursor paramAnonymousCursor, ViewGroup paramAnonymousViewGroup)
                    {
                        return AlertController.AlertParams.this.mInflater.inflate(paramAlertController.mMultiChoiceItemLayout, paramAnonymousViewGroup, false);
                    }
                };
                break;
                int i;
                if (this.mIsSingleChoice)
                {
                    i = paramAlertController.mSingleChoiceItemLayout;
                    label194: if (this.mCursor != null)
                        break label248;
                    if (this.mAdapter == null)
                        break label225;
                }
                label225: for (localObject = this.mAdapter; ; localObject = new ArrayAdapter(this.mContext, i, 16908308, this.mItems))
                {
                    break;
                    i = paramAlertController.mListItemLayout;
                    break label194;
                }
                label248: Context localContext1 = this.mContext;
                Cursor localCursor1 = this.mCursor;
                String[] arrayOfString = new String[1];
                arrayOfString[0] = this.mLabelColumn;
                int[] arrayOfInt = new int[1];
                arrayOfInt[0] = 16908308;
                localObject = new SimpleCursorAdapter(localContext1, i, localCursor1, arrayOfString, arrayOfInt);
                break;
                label306: if (this.mOnCheckboxClickListener == null)
                    break label108;
                localRecycleListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    public void onItemClick(AdapterView paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
                    {
                        if (AlertController.AlertParams.this.mCheckedItems != null)
                            AlertController.AlertParams.this.mCheckedItems[paramAnonymousInt] = localRecycleListView.isItemChecked(paramAnonymousInt);
                        AlertController.AlertParams.this.mOnCheckboxClickListener.onClick(paramAlertController.mDialogInterface, paramAnonymousInt, localRecycleListView.isItemChecked(paramAnonymousInt));
                    }
                });
                break label108;
                label330: if (this.mIsMultiChoice)
                    localRecycleListView.setChoiceMode(2);
            }
        }

        public void apply(AlertController paramAlertController)
        {
            if (this.mCustomTitleView != null)
            {
                paramAlertController.setCustomTitle(this.mCustomTitleView);
                if (this.mMessage != null)
                    paramAlertController.setMessage(this.mMessage);
                if (this.mPositiveButtonText != null)
                    paramAlertController.setButton(-1, this.mPositiveButtonText, this.mPositiveButtonListener, null);
                if (this.mNegativeButtonText != null)
                    paramAlertController.setButton(-2, this.mNegativeButtonText, this.mNegativeButtonListener, null);
                if (this.mNeutralButtonText != null)
                    paramAlertController.setButton(-3, this.mNeutralButtonText, this.mNeutralButtonListener, null);
                if (this.mForceInverseBackground)
                    paramAlertController.setInverseBackgroundForced(true);
                if ((this.mItems != null) || (this.mCursor != null) || (this.mAdapter != null))
                    createListView(paramAlertController);
                if (this.mView != null)
                {
                    if (!this.mViewSpacingSpecified)
                        break label221;
                    paramAlertController.setView(this.mView, this.mViewSpacingLeft, this.mViewSpacingTop, this.mViewSpacingRight, this.mViewSpacingBottom);
                }
            }
            while (true)
            {
                return;
                if (this.mTitle != null)
                    paramAlertController.setTitle(this.mTitle);
                if (this.mIcon != null)
                    paramAlertController.setIcon(this.mIcon);
                if (this.mIconId < 0)
                    break;
                paramAlertController.setIcon(this.mIconId);
                break;
                label221: paramAlertController.setView(this.mView);
            }
        }

        public static abstract interface OnPrepareListViewListener
        {
            public abstract void onPrepareListView(ListView paramListView);
        }
    }

    public static class RecycleListView extends ListView
    {
        boolean mRecycleOnMeasure = true;

        public RecycleListView(Context paramContext)
        {
            super();
        }

        public RecycleListView(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }

        public RecycleListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
        {
            super(paramAttributeSet, paramInt);
        }

        protected boolean recycleOnMeasure()
        {
            return this.mRecycleOnMeasure;
        }
    }

    private static final class ButtonHandler extends Handler
    {
        private static final int MSG_DISMISS_DIALOG = 1;
        private WeakReference<DialogInterface> mDialog;

        public ButtonHandler(DialogInterface paramDialogInterface)
        {
            this.mDialog = new WeakReference(paramDialogInterface);
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            case 0:
            default:
            case -3:
            case -2:
            case -1:
            case 1:
            }
            while (true)
            {
                return;
                ((DialogInterface.OnClickListener)paramMessage.obj).onClick((DialogInterface)this.mDialog.get(), paramMessage.what);
                continue;
                ((DialogInterface)paramMessage.obj).dismiss();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.AlertController
 * JD-Core Version:        0.6.2
 */