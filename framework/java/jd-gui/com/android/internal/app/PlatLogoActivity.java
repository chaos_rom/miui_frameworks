package com.android.internal.app;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class PlatLogoActivity extends Activity
{
    ImageView mContent;
    int mCount;
    final Handler mHandler = new Handler();
    Toast mToast;

    private View makeView()
    {
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        LinearLayout localLinearLayout = new LinearLayout(this);
        localLinearLayout.setOrientation(1);
        localLinearLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        int i = (int)(8.0F * localDisplayMetrics.density);
        localLinearLayout.setPadding(i, i, i, i);
        Typeface localTypeface1 = Typeface.create("sans-serif-light", 0);
        Typeface localTypeface2 = Typeface.create("sans-serif", 1);
        float f = 14.0F * localDisplayMetrics.density;
        LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-2, -2);
        localLayoutParams.gravity = 1;
        localLayoutParams.bottomMargin = ((int)(-4.0F * localDisplayMetrics.density));
        TextView localTextView1 = new TextView(this);
        if (localTypeface1 != null)
            localTextView1.setTypeface(localTypeface1);
        localTextView1.setTextSize(1.25F * f);
        localTextView1.setTextColor(-1);
        localTextView1.setShadowLayer(4.0F * localDisplayMetrics.density, 0.0F, 2.0F * localDisplayMetrics.density, 1711276032);
        localTextView1.setText("Android " + Build.VERSION.RELEASE);
        localLinearLayout.addView(localTextView1, localLayoutParams);
        TextView localTextView2 = new TextView(this);
        if (localTypeface2 != null)
            localTextView2.setTypeface(localTypeface2);
        localTextView2.setTextSize(f);
        localTextView2.setTextColor(-1);
        localTextView2.setShadowLayer(4.0F * localDisplayMetrics.density, 0.0F, 2.0F * localDisplayMetrics.density, 1711276032);
        localTextView2.setText("JELLY BEAN");
        localLinearLayout.addView(localTextView2, localLayoutParams);
        return localLinearLayout;
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        this.mToast = Toast.makeText(this, "", 1);
        this.mToast.setView(makeView());
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        this.mContent = new ImageView(this);
        this.mContent.setImageResource(17302611);
        this.mContent.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        int i = (int)(32.0F * localDisplayMetrics.density);
        this.mContent.setPadding(i, i, i, i);
        this.mContent.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                PlatLogoActivity.this.mToast.show();
                PlatLogoActivity.this.mContent.setImageResource(17302610);
            }
        });
        this.mContent.setOnLongClickListener(new View.OnLongClickListener()
        {
            public boolean onLongClick(View paramAnonymousView)
            {
                try
                {
                    PlatLogoActivity.this.startActivity(new Intent("android.intent.action.MAIN").setFlags(276856832).addCategory("com.android.internal.category.PLATLOGO"));
                    PlatLogoActivity.this.finish();
                    return true;
                }
                catch (ActivityNotFoundException localActivityNotFoundException)
                {
                    while (true)
                        Log.e("PlatLogoActivity", "Couldn't find a bag of beans.");
                }
            }
        });
        setContentView(this.mContent);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.PlatLogoActivity
 * JD-Core Version:        0.6.2
 */