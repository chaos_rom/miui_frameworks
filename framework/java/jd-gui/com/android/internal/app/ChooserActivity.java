package com.android.internal.app;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

public class ChooserActivity extends ResolverActivity
{
    protected void onCreate(Bundle paramBundle)
    {
        Intent localIntent1 = getIntent();
        Parcelable localParcelable = localIntent1.getParcelableExtra("android.intent.extra.INTENT");
        if (!(localParcelable instanceof Intent))
        {
            Log.w("ChooseActivity", "Target is not an intent: " + localParcelable);
            finish();
        }
        while (true)
        {
            return;
            Intent localIntent2 = (Intent)localParcelable;
            CharSequence localCharSequence = localIntent1.getCharSequenceExtra("android.intent.extra.TITLE");
            if (localCharSequence == null)
                localCharSequence = getResources().getText(17040332);
            Parcelable[] arrayOfParcelable = localIntent1.getParcelableArrayExtra("android.intent.extra.INITIAL_INTENTS");
            Intent[] arrayOfIntent = null;
            if (arrayOfParcelable != null)
            {
                arrayOfIntent = new Intent[arrayOfParcelable.length];
                for (int i = 0; ; i++)
                {
                    if (i >= arrayOfParcelable.length)
                        break label190;
                    if (!(arrayOfParcelable[i] instanceof Intent))
                    {
                        Log.w("ChooseActivity", "Initial intent #" + i + " not an Intent: " + arrayOfParcelable[i]);
                        finish();
                        break;
                    }
                    arrayOfIntent[i] = ((Intent)arrayOfParcelable[i]);
                }
            }
            label190: super.onCreate(paramBundle, localIntent2, localCharSequence, arrayOfIntent, null, false);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.ChooserActivity
 * JD-Core Version:        0.6.2
 */