package com.android.internal.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import com.android.internal.os.storage.ExternalStorageFormatter;

public class ExternalMediaFormatActivity extends AlertActivity
    implements DialogInterface.OnClickListener
{
    private static final int POSITIVE_BUTTON = -1;
    private BroadcastReceiver mStorageReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            Log.d("ExternalMediaFormatActivity", "got action " + str);
            if ((str == "android.intent.action.MEDIA_REMOVED") || (str == "android.intent.action.MEDIA_CHECKING") || (str == "android.intent.action.MEDIA_MOUNTED") || (str == "android.intent.action.MEDIA_SHARED"))
                ExternalMediaFormatActivity.this.finish();
        }
    };

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        if (paramInt == -1)
        {
            Intent localIntent = new Intent("com.android.internal.os.storage.FORMAT_ONLY");
            localIntent.setComponent(ExternalStorageFormatter.COMPONENT_NAME);
            startService(localIntent);
        }
        finish();
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        Log.d("ExternalMediaFormatActivity", "onCreate!");
        AlertController.AlertParams localAlertParams = this.mAlertParams;
        localAlertParams.mIconId = 17301642;
        localAlertParams.mTitle = getString(17040449);
        localAlertParams.mMessage = getString(17040450);
        localAlertParams.mPositiveButtonText = getString(17040451);
        localAlertParams.mPositiveButtonListener = this;
        localAlertParams.mNegativeButtonText = getString(17039360);
        localAlertParams.mNegativeButtonListener = this;
        setupAlert();
    }

    protected void onPause()
    {
        super.onPause();
        unregisterReceiver(this.mStorageReceiver);
    }

    protected void onResume()
    {
        super.onResume();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.MEDIA_REMOVED");
        localIntentFilter.addAction("android.intent.action.MEDIA_CHECKING");
        localIntentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        localIntentFilter.addAction("android.intent.action.MEDIA_SHARED");
        registerReceiver(this.mStorageReceiver, localIntentFilter);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.ExternalMediaFormatActivity
 * JD-Core Version:        0.6.2
 */