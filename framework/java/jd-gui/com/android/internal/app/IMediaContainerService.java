package com.android.internal.app;

import android.content.pm.ContainerEncryptionParams;
import android.content.pm.PackageInfoLite;
import android.content.res.ObbInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IMediaContainerService extends IInterface
{
    public abstract long calculateDirectorySize(String paramString)
        throws RemoteException;

    public abstract boolean checkExternalFreeStorage(Uri paramUri, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean checkInternalFreeStorage(Uri paramUri, boolean paramBoolean, long paramLong)
        throws RemoteException;

    public abstract void clearDirectory(String paramString)
        throws RemoteException;

    public abstract int copyResource(Uri paramUri, ContainerEncryptionParams paramContainerEncryptionParams, ParcelFileDescriptor paramParcelFileDescriptor)
        throws RemoteException;

    public abstract String copyResourceToContainer(Uri paramUri, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public abstract long[] getFileSystemStats(String paramString)
        throws RemoteException;

    public abstract PackageInfoLite getMinimalPackageInfo(String paramString, int paramInt, long paramLong)
        throws RemoteException;

    public abstract ObbInfo getObbInfo(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IMediaContainerService
    {
        private static final String DESCRIPTOR = "com.android.internal.app.IMediaContainerService";
        static final int TRANSACTION_calculateDirectorySize = 7;
        static final int TRANSACTION_checkExternalFreeStorage = 5;
        static final int TRANSACTION_checkInternalFreeStorage = 4;
        static final int TRANSACTION_clearDirectory = 9;
        static final int TRANSACTION_copyResource = 2;
        static final int TRANSACTION_copyResourceToContainer = 1;
        static final int TRANSACTION_getFileSystemStats = 8;
        static final int TRANSACTION_getMinimalPackageInfo = 3;
        static final int TRANSACTION_getObbInfo = 6;

        public Stub()
        {
            attachInterface(this, "com.android.internal.app.IMediaContainerService");
        }

        public static IMediaContainerService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.app.IMediaContainerService");
                if ((localIInterface != null) && ((localIInterface instanceof IMediaContainerService)))
                    localObject = (IMediaContainerService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.app.IMediaContainerService");
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                Uri localUri4;
                label145: String str1;
                String str2;
                String str3;
                String str4;
                boolean bool6;
                if (paramParcel1.readInt() != 0)
                {
                    localUri4 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                    str1 = paramParcel1.readString();
                    str2 = paramParcel1.readString();
                    str3 = paramParcel1.readString();
                    str4 = paramParcel1.readString();
                    if (paramParcel1.readInt() == 0)
                        break label231;
                    bool6 = true;
                    label179: if (paramParcel1.readInt() == 0)
                        break label237;
                }
                label231: label237: for (boolean bool7 = true; ; bool7 = false)
                {
                    String str5 = copyResourceToContainer(localUri4, str1, str2, str3, str4, bool6, bool7);
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str5);
                    bool1 = true;
                    break;
                    localUri4 = null;
                    break label145;
                    bool6 = false;
                    break label179;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                Uri localUri3;
                label270: ContainerEncryptionParams localContainerEncryptionParams;
                if (paramParcel1.readInt() != 0)
                {
                    localUri3 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label346;
                    localContainerEncryptionParams = (ContainerEncryptionParams)ContainerEncryptionParams.CREATOR.createFromParcel(paramParcel1);
                    label291: if (paramParcel1.readInt() == 0)
                        break label352;
                }
                label346: label352: for (ParcelFileDescriptor localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor = null)
                {
                    int k = copyResource(localUri3, localContainerEncryptionParams, localParcelFileDescriptor);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(k);
                    bool1 = true;
                    break;
                    localUri3 = null;
                    break label270;
                    localContainerEncryptionParams = null;
                    break label291;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                PackageInfoLite localPackageInfoLite = getMinimalPackageInfo(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readLong());
                paramParcel2.writeNoException();
                if (localPackageInfoLite != null)
                {
                    paramParcel2.writeInt(1);
                    localPackageInfoLite.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                Uri localUri2;
                label444: boolean bool4;
                if (paramParcel1.readInt() != 0)
                {
                    localUri2 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label498;
                    bool4 = true;
                    label454: boolean bool5 = checkInternalFreeStorage(localUri2, bool4, paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    if (!bool5)
                        break label504;
                }
                label498: label504: for (int j = 1; ; j = 0)
                {
                    paramParcel2.writeInt(j);
                    bool1 = true;
                    break;
                    localUri2 = null;
                    break label444;
                    bool4 = false;
                    break label454;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                Uri localUri1;
                label537: boolean bool2;
                if (paramParcel1.readInt() != 0)
                {
                    localUri1 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                    if (paramParcel1.readInt() == 0)
                        break label587;
                    bool2 = true;
                    label547: boolean bool3 = checkExternalFreeStorage(localUri1, bool2);
                    paramParcel2.writeNoException();
                    if (!bool3)
                        break label593;
                }
                label587: label593: for (int i = 1; ; i = 0)
                {
                    paramParcel2.writeInt(i);
                    bool1 = true;
                    break;
                    localUri1 = null;
                    break label537;
                    bool2 = false;
                    break label547;
                }
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                ObbInfo localObbInfo = getObbInfo(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localObbInfo != null)
                {
                    paramParcel2.writeInt(1);
                    localObbInfo.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                long l = calculateDirectorySize(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                long[] arrayOfLong = getFileSystemStats(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeLongArray(arrayOfLong);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
                clearDirectory(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
            }
        }

        private static class Proxy
            implements IMediaContainerService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public long calculateDirectorySize(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean checkExternalFreeStorage(Uri paramUri, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            break label126;
                            localParcel1.writeInt(j);
                            this.mRemote.transact(5, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int k = localParcel2.readInt();
                            if (k != 0)
                                label79: return i;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label126: 
                    do
                    {
                        j = 0;
                        break;
                        i = 0;
                        break label79;
                    }
                    while (!paramBoolean);
                    int j = i;
                }
            }

            public boolean checkInternalFreeStorage(Uri paramUri, boolean paramBoolean, long paramLong)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            break label135;
                            localParcel1.writeInt(j);
                            localParcel1.writeLong(paramLong);
                            this.mRemote.transact(4, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int k = localParcel2.readInt();
                            if (k != 0)
                                label86: return i;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label135: 
                    do
                    {
                        j = 0;
                        break;
                        i = 0;
                        break label86;
                    }
                    while (!paramBoolean);
                    int j = i;
                }
            }

            public void clearDirectory(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int copyResource(Uri paramUri, ContainerEncryptionParams paramContainerEncryptionParams, ParcelFileDescriptor paramParcelFileDescriptor)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            if (paramContainerEncryptionParams != null)
                            {
                                localParcel1.writeInt(1);
                                paramContainerEncryptionParams.writeToParcel(localParcel1, 0);
                                if (paramParcelFileDescriptor == null)
                                    break label142;
                                localParcel1.writeInt(1);
                                paramParcelFileDescriptor.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(2, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                return i;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label142: localParcel1.writeInt(0);
                }
            }

            public String copyResourceToContainer(Uri paramUri, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString1);
                            localParcel1.writeString(paramString2);
                            localParcel1.writeString(paramString3);
                            localParcel1.writeString(paramString4);
                            if (paramBoolean1)
                            {
                                j = i;
                                localParcel1.writeInt(j);
                                if (!paramBoolean2)
                                    break label162;
                                localParcel1.writeInt(i);
                                this.mRemote.transact(1, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                String str = localParcel2.readString();
                                return str;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    int j = 0;
                    continue;
                    label162: i = 0;
                }
            }

            public long[] getFileSystemStats(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long[] arrayOfLong = localParcel2.createLongArray();
                    return arrayOfLong;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.app.IMediaContainerService";
            }

            public PackageInfoLite getMinimalPackageInfo(String paramString, int paramInt, long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeLong(paramLong);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localPackageInfoLite = (PackageInfoLite)PackageInfoLite.CREATOR.createFromParcel(localParcel2);
                        return localPackageInfoLite;
                    }
                    PackageInfoLite localPackageInfoLite = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ObbInfo getObbInfo(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localObbInfo = (ObbInfo)ObbInfo.CREATOR.createFromParcel(localParcel2);
                        return localObbInfo;
                    }
                    ObbInfo localObbInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.IMediaContainerService
 * JD-Core Version:        0.6.2
 */