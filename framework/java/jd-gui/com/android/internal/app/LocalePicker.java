package com.android.internal.app;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.ListFragment;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.text.Collator;
import java.util.Arrays;
import java.util.Locale;
import miui.util.ExtraLocalePicker;

public class LocalePicker extends ListFragment
{
    private static final boolean DEBUG = false;
    private static final String TAG = "LocalePicker";
    LocaleSelectionListener mListener;

    public static ArrayAdapter<LocaleInfo> constructAdapter(Context paramContext)
    {
        return constructAdapter(paramContext, 17367140, 16908662);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static ArrayAdapter<LocaleInfo> constructAdapter(Context paramContext, int paramInt1, int paramInt2)
    {
        Resources localResources = paramContext.getResources();
        String[] arrayOfString1 = Resources.getSystem().getAssets().getLocales();
        String[] arrayOfString2 = localResources.getStringArray(17235975);
        String[] arrayOfString3 = localResources.getStringArray(17235976);
        Arrays.sort(arrayOfString1);
        int i = arrayOfString1.length;
        LocaleInfo[] arrayOfLocaleInfo1 = new LocaleInfo[i];
        int j = 0;
        int k = 0;
        String str1;
        String str2;
        Locale localLocale;
        int n;
        if (j < i)
        {
            str1 = arrayOfString1[j];
            if (str1.length() != 5)
                break label362;
            str2 = str1.substring(0, 2);
            localLocale = new Locale(str2, str1.substring(3, 5));
            if (k == 0)
            {
                n = k + 1;
                LocaleInfo localLocaleInfo3 = new LocaleInfo(toTitleCase(localLocale.getDisplayLanguage(localLocale)), localLocale);
                arrayOfLocaleInfo1[k] = localLocaleInfo3;
            }
        }
        while (true)
        {
            j++;
            k = n;
            break;
            if (arrayOfLocaleInfo1[(k - 1)].locale.getLanguage().equals(str2))
            {
                arrayOfLocaleInfo1[(k - 1)].label = toTitleCase(getDisplayName(arrayOfLocaleInfo1[(k - 1)].locale, arrayOfString2, arrayOfString3));
                n = k + 1;
                LocaleInfo localLocaleInfo2 = new LocaleInfo(toTitleCase(getDisplayName(localLocale, arrayOfString2, arrayOfString3)), localLocale);
                arrayOfLocaleInfo1[k] = localLocaleInfo2;
            }
            else
            {
                if (str1.equals("zz_ZZ"));
                for (String str3 = "Pseudo..."; ; str3 = toTitleCase(localLocale.getDisplayLanguage(localLocale)))
                {
                    n = k + 1;
                    LocaleInfo localLocaleInfo1 = new LocaleInfo(str3, localLocale);
                    arrayOfLocaleInfo1[k] = localLocaleInfo1;
                    break;
                }
                LocaleInfo[] arrayOfLocaleInfo2 = new LocaleInfo[k];
                for (int m = 0; m < k; m++)
                    arrayOfLocaleInfo2[m] = arrayOfLocaleInfo1[m];
                Arrays.sort(arrayOfLocaleInfo2);
                ExtraLocalePicker.adjustLocaleOrder(arrayOfLocaleInfo2);
                ArrayAdapter localArrayAdapter = new ArrayAdapter(paramContext, paramInt1, paramInt2, arrayOfLocaleInfo2);
                return localArrayAdapter;
                label362: n = k;
            }
        }
    }

    private static String getDisplayName(Locale paramLocale, String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        String str1 = paramLocale.toString();
        int i = 0;
        if (i < paramArrayOfString1.length)
            if (!paramArrayOfString1[i].equals(str1));
        for (String str2 = paramArrayOfString2[i]; ; str2 = paramLocale.getDisplayName(paramLocale))
        {
            return str2;
            i++;
            break;
        }
    }

    private static String toTitleCase(String paramString)
    {
        if (paramString.length() == 0);
        while (true)
        {
            return paramString;
            paramString = Character.toUpperCase(paramString.charAt(0)) + paramString.substring(1);
        }
    }

    public static void updateLocale(Locale paramLocale)
    {
        try
        {
            IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
            Configuration localConfiguration = localIActivityManager.getConfiguration();
            localConfiguration.locale = paramLocale;
            localConfiguration.userSetLocale = true;
            localIActivityManager.updateConfiguration(localConfiguration);
            BackupManager.dataChanged("com.android.providers.settings");
            label33: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label33;
        }
    }

    public void onActivityCreated(Bundle paramBundle)
    {
        super.onActivityCreated(paramBundle);
        setListAdapter(constructAdapter(getActivity()));
    }

    public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
    {
        if (this.mListener != null)
        {
            Locale localLocale = ((LocaleInfo)getListAdapter().getItem(paramInt)).locale;
            this.mListener.onLocaleSelected(localLocale);
        }
    }

    public void onResume()
    {
        super.onResume();
        getListView().requestFocus();
    }

    public void setLocaleSelectionListener(LocaleSelectionListener paramLocaleSelectionListener)
    {
        this.mListener = paramLocaleSelectionListener;
    }

    public static class LocaleInfo
        implements Comparable<LocaleInfo>
    {
        static final Collator sCollator = Collator.getInstance();
        String label;
        Locale locale;

        public LocaleInfo(String paramString, Locale paramLocale)
        {
            this.label = paramString;
            this.locale = paramLocale;
        }

        public int compareTo(LocaleInfo paramLocaleInfo)
        {
            return sCollator.compare(this.label, paramLocaleInfo.label);
        }

        public String getLabel()
        {
            return this.label;
        }

        public Locale getLocale()
        {
            return this.locale;
        }

        public String toString()
        {
            return this.label;
        }
    }

    public static abstract interface LocaleSelectionListener
    {
        public abstract void onLocaleSelected(Locale paramLocale);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.app.LocalePicker
 * JD-Core Version:        0.6.2
 */