package com.android.internal.textservice;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ISpellCheckerService extends IInterface
{
    public abstract ISpellCheckerSession getISpellCheckerSession(String paramString, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISpellCheckerService
    {
        private static final String DESCRIPTOR = "com.android.internal.textservice.ISpellCheckerService";
        static final int TRANSACTION_getISpellCheckerSession = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.textservice.ISpellCheckerService");
        }

        public static ISpellCheckerService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.textservice.ISpellCheckerService");
                if ((localIInterface != null) && ((localIInterface instanceof ISpellCheckerService)))
                    localObject = (ISpellCheckerService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool;
            switch (paramInt1)
            {
            default:
            case 1598968902:
                for (bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2); ; bool = true)
                {
                    return bool;
                    paramParcel2.writeString("com.android.internal.textservice.ISpellCheckerService");
                }
            case 1:
            }
            paramParcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerService");
            String str = paramParcel1.readString();
            ISpellCheckerSessionListener localISpellCheckerSessionListener = ISpellCheckerSessionListener.Stub.asInterface(paramParcel1.readStrongBinder());
            Bundle localBundle;
            label96: ISpellCheckerSession localISpellCheckerSession;
            if (paramParcel1.readInt() != 0)
            {
                localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
                localISpellCheckerSession = getISpellCheckerSession(str, localISpellCheckerSessionListener, localBundle);
                paramParcel2.writeNoException();
                if (localISpellCheckerSession == null)
                    break label144;
            }
            label144: for (IBinder localIBinder = localISpellCheckerSession.asBinder(); ; localIBinder = null)
            {
                paramParcel2.writeStrongBinder(localIBinder);
                bool = true;
                break;
                localBundle = null;
                break label96;
            }
        }

        private static class Proxy
            implements ISpellCheckerService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public ISpellCheckerSession getISpellCheckerSession(String paramString, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerService");
                    localParcel1.writeString(paramString);
                    IBinder localIBinder;
                    if (paramISpellCheckerSessionListener != null)
                    {
                        localIBinder = paramISpellCheckerSessionListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        if (paramBundle == null)
                            break label109;
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        ISpellCheckerSession localISpellCheckerSession = ISpellCheckerSession.Stub.asInterface(localParcel2.readStrongBinder());
                        return localISpellCheckerSession;
                        localIBinder = null;
                        break;
                        label109: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.textservice.ISpellCheckerService";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.textservice.ISpellCheckerService
 * JD-Core Version:        0.6.2
 */