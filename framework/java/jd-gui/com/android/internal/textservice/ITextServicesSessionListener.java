package com.android.internal.textservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface ITextServicesSessionListener extends IInterface
{
    public abstract void onServiceConnected(ISpellCheckerSession paramISpellCheckerSession)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ITextServicesSessionListener
    {
        private static final String DESCRIPTOR = "com.android.internal.textservice.ITextServicesSessionListener";
        static final int TRANSACTION_onServiceConnected = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.textservice.ITextServicesSessionListener");
        }

        public static ITextServicesSessionListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.textservice.ITextServicesSessionListener");
                if ((localIInterface != null) && ((localIInterface instanceof ITextServicesSessionListener)))
                    localObject = (ITextServicesSessionListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("com.android.internal.textservice.ITextServicesSessionListener");
                continue;
                paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesSessionListener");
                onServiceConnected(ISpellCheckerSession.Stub.asInterface(paramParcel1.readStrongBinder()));
            }
        }

        private static class Proxy
            implements ITextServicesSessionListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.textservice.ITextServicesSessionListener";
            }

            public void onServiceConnected(ISpellCheckerSession paramISpellCheckerSession)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ITextServicesSessionListener");
                    if (paramISpellCheckerSession != null)
                        localIBinder = paramISpellCheckerSession.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.textservice.ITextServicesSessionListener
 * JD-Core Version:        0.6.2
 */