package com.android.internal.textservice;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.view.textservice.SpellCheckerInfo;
import android.view.textservice.SpellCheckerSubtype;

public abstract interface ITextServicesManager extends IInterface
{
    public abstract void finishSpellCheckerService(ISpellCheckerSessionListener paramISpellCheckerSessionListener)
        throws RemoteException;

    public abstract SpellCheckerInfo getCurrentSpellChecker(String paramString)
        throws RemoteException;

    public abstract SpellCheckerSubtype getCurrentSpellCheckerSubtype(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract SpellCheckerInfo[] getEnabledSpellCheckers()
        throws RemoteException;

    public abstract void getSpellCheckerService(String paramString1, String paramString2, ITextServicesSessionListener paramITextServicesSessionListener, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle)
        throws RemoteException;

    public abstract boolean isSpellCheckerEnabled()
        throws RemoteException;

    public abstract void setCurrentSpellChecker(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setCurrentSpellCheckerSubtype(String paramString, int paramInt)
        throws RemoteException;

    public abstract void setSpellCheckerEnabled(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ITextServicesManager
    {
        private static final String DESCRIPTOR = "com.android.internal.textservice.ITextServicesManager";
        static final int TRANSACTION_finishSpellCheckerService = 4;
        static final int TRANSACTION_getCurrentSpellChecker = 1;
        static final int TRANSACTION_getCurrentSpellCheckerSubtype = 2;
        static final int TRANSACTION_getEnabledSpellCheckers = 9;
        static final int TRANSACTION_getSpellCheckerService = 3;
        static final int TRANSACTION_isSpellCheckerEnabled = 8;
        static final int TRANSACTION_setCurrentSpellChecker = 5;
        static final int TRANSACTION_setCurrentSpellCheckerSubtype = 6;
        static final int TRANSACTION_setSpellCheckerEnabled = 7;

        public Stub()
        {
            attachInterface(this, "com.android.internal.textservice.ITextServicesManager");
        }

        public static ITextServicesManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.textservice.ITextServicesManager");
                if ((localIInterface != null) && ((localIInterface instanceof ITextServicesManager)))
                    localObject = (ITextServicesManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("com.android.internal.textservice.ITextServicesManager");
                continue;
                paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                SpellCheckerInfo localSpellCheckerInfo = getCurrentSpellChecker(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localSpellCheckerInfo != null)
                {
                    paramParcel2.writeInt(j);
                    localSpellCheckerInfo.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                    String str3 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    int i1;
                    for (int n = j; ; i1 = 0)
                    {
                        SpellCheckerSubtype localSpellCheckerSubtype = getCurrentSpellCheckerSubtype(str3, n);
                        paramParcel2.writeNoException();
                        if (localSpellCheckerSubtype == null)
                            break label234;
                        paramParcel2.writeInt(j);
                        localSpellCheckerSubtype.writeToParcel(paramParcel2, j);
                        break;
                    }
                    label234: paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                    String str1 = paramParcel1.readString();
                    String str2 = paramParcel1.readString();
                    ITextServicesSessionListener localITextServicesSessionListener = ITextServicesSessionListener.Stub.asInterface(paramParcel1.readStrongBinder());
                    ISpellCheckerSessionListener localISpellCheckerSessionListener = ISpellCheckerSessionListener.Stub.asInterface(paramParcel1.readStrongBinder());
                    if (paramParcel1.readInt() != 0);
                    for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                    {
                        getSpellCheckerService(str1, str2, localITextServicesSessionListener, localISpellCheckerSessionListener, localBundle);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                    finishSpellCheckerService(ISpellCheckerSessionListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                    setCurrentSpellChecker(paramParcel1.readString(), paramParcel1.readString());
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                    setCurrentSpellCheckerSubtype(paramParcel1.readString(), paramParcel1.readInt());
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                    if (paramParcel1.readInt() != 0);
                    int m;
                    for (int k = j; ; m = 0)
                    {
                        setSpellCheckerEnabled(k);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                    boolean bool = isSpellCheckerEnabled();
                    paramParcel2.writeNoException();
                    if (bool)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
                    SpellCheckerInfo[] arrayOfSpellCheckerInfo = getEnabledSpellCheckers();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedArray(arrayOfSpellCheckerInfo, j);
                }
            }
        }

        private static class Proxy
            implements ITextServicesManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void finishSpellCheckerService(ISpellCheckerSessionListener paramISpellCheckerSessionListener)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    if (paramISpellCheckerSessionListener != null)
                        localIBinder = paramISpellCheckerSessionListener.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public SpellCheckerInfo getCurrentSpellChecker(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localSpellCheckerInfo = (SpellCheckerInfo)SpellCheckerInfo.CREATOR.createFromParcel(localParcel2);
                        return localSpellCheckerInfo;
                    }
                    SpellCheckerInfo localSpellCheckerInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public SpellCheckerSubtype getCurrentSpellCheckerSubtype(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localSpellCheckerSubtype = (SpellCheckerSubtype)SpellCheckerSubtype.CREATOR.createFromParcel(localParcel2);
                        return localSpellCheckerSubtype;
                    }
                    SpellCheckerSubtype localSpellCheckerSubtype = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public SpellCheckerInfo[] getEnabledSpellCheckers()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    SpellCheckerInfo[] arrayOfSpellCheckerInfo = (SpellCheckerInfo[])localParcel2.createTypedArray(SpellCheckerInfo.CREATOR);
                    return arrayOfSpellCheckerInfo;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.textservice.ITextServicesManager";
            }

            public void getSpellCheckerService(String paramString1, String paramString2, ITextServicesSessionListener paramITextServicesSessionListener, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle)
                throws RemoteException
            {
                IBinder localIBinder1 = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    localParcel.writeString(paramString1);
                    localParcel.writeString(paramString2);
                    IBinder localIBinder2;
                    if (paramITextServicesSessionListener != null)
                    {
                        localIBinder2 = paramITextServicesSessionListener.asBinder();
                        localParcel.writeStrongBinder(localIBinder2);
                        if (paramISpellCheckerSessionListener != null)
                            localIBinder1 = paramISpellCheckerSessionListener.asBinder();
                        localParcel.writeStrongBinder(localIBinder1);
                        if (paramBundle == null)
                            break label113;
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                        localIBinder2 = null;
                        break;
                        label113: localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public boolean isSpellCheckerEnabled()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setCurrentSpellChecker(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    localParcel.writeString(paramString1);
                    localParcel.writeString(paramString2);
                    this.mRemote.transact(5, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setCurrentSpellCheckerSubtype(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    localParcel.writeString(paramString);
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(6, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setSpellCheckerEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(7, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.textservice.ITextServicesManager
 * JD-Core Version:        0.6.2
 */