package com.android.internal.textservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.textservice.TextInfo;

public abstract interface ISpellCheckerSession extends IInterface
{
    public abstract void onCancel()
        throws RemoteException;

    public abstract void onClose()
        throws RemoteException;

    public abstract void onGetSentenceSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt)
        throws RemoteException;

    public abstract void onGetSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt, boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISpellCheckerSession
    {
        private static final String DESCRIPTOR = "com.android.internal.textservice.ISpellCheckerSession";
        static final int TRANSACTION_onCancel = 3;
        static final int TRANSACTION_onClose = 4;
        static final int TRANSACTION_onGetSentenceSuggestionsMultiple = 2;
        static final int TRANSACTION_onGetSuggestionsMultiple = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.textservice.ISpellCheckerSession");
        }

        public static ISpellCheckerSession asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.textservice.ISpellCheckerSession");
                if ((localIInterface != null) && ((localIInterface instanceof ISpellCheckerSession)))
                    localObject = (ISpellCheckerSession)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.textservice.ISpellCheckerSession");
                continue;
                paramParcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSession");
                TextInfo[] arrayOfTextInfo = (TextInfo[])paramParcel1.createTypedArray(TextInfo.CREATOR);
                int i = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    onGetSuggestionsMultiple(arrayOfTextInfo, i, bool2);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSession");
                onGetSentenceSuggestionsMultiple((TextInfo[])paramParcel1.createTypedArray(TextInfo.CREATOR), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSession");
                onCancel();
                continue;
                paramParcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSession");
                onClose();
            }
        }

        private static class Proxy
            implements ISpellCheckerSession
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.textservice.ISpellCheckerSession";
            }

            public void onCancel()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSession");
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onClose()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSession");
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onGetSentenceSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSession");
                    localParcel.writeTypedArray(paramArrayOfTextInfo, 0);
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onGetSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSession");
                    localParcel.writeTypedArray(paramArrayOfTextInfo, 0);
                    localParcel.writeInt(paramInt);
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.textservice.ISpellCheckerSession
 * JD-Core Version:        0.6.2
 */