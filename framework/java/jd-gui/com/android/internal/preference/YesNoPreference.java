package com.android.internal.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.preference.DialogPreference;
import android.preference.Preference.BaseSavedState;
import android.util.AttributeSet;

public class YesNoPreference extends DialogPreference
{
    private boolean mWasPositiveResult;

    public YesNoPreference(Context paramContext)
    {
        this(paramContext, null);
    }

    public YesNoPreference(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 16842896);
    }

    public YesNoPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public boolean getValue()
    {
        return this.mWasPositiveResult;
    }

    protected void onDialogClosed(boolean paramBoolean)
    {
        super.onDialogClosed(paramBoolean);
        if (callChangeListener(Boolean.valueOf(paramBoolean)))
            setValue(paramBoolean);
    }

    protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
    {
        return Boolean.valueOf(paramTypedArray.getBoolean(paramInt, false));
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        if (!paramParcelable.getClass().equals(SavedState.class))
            super.onRestoreInstanceState(paramParcelable);
        while (true)
        {
            return;
            SavedState localSavedState = (SavedState)paramParcelable;
            super.onRestoreInstanceState(localSavedState.getSuperState());
            setValue(localSavedState.wasPositiveResult);
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Object localObject = super.onSaveInstanceState();
        if (isPersistent());
        while (true)
        {
            return localObject;
            SavedState localSavedState = new SavedState((Parcelable)localObject);
            localSavedState.wasPositiveResult = getValue();
            localObject = localSavedState;
        }
    }

    protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
    {
        if (paramBoolean);
        for (boolean bool = getPersistedBoolean(this.mWasPositiveResult); ; bool = ((Boolean)paramObject).booleanValue())
        {
            setValue(bool);
            return;
        }
    }

    public void setValue(boolean paramBoolean)
    {
        this.mWasPositiveResult = paramBoolean;
        persistBoolean(paramBoolean);
        if (!paramBoolean);
        for (boolean bool = true; ; bool = false)
        {
            notifyDependencyChange(bool);
            return;
        }
    }

    public boolean shouldDisableDependents()
    {
        if ((!this.mWasPositiveResult) || (super.shouldDisableDependents()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static class SavedState extends Preference.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public YesNoPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new YesNoPreference.SavedState(paramAnonymousParcel);
            }

            public YesNoPreference.SavedState[] newArray(int paramAnonymousInt)
            {
                return new YesNoPreference.SavedState[paramAnonymousInt];
            }
        };
        boolean wasPositiveResult;

        public SavedState(Parcel paramParcel)
        {
            super();
            if (paramParcel.readInt() == i);
            while (true)
            {
                this.wasPositiveResult = i;
                return;
                i = 0;
            }
        }

        public SavedState(Parcelable paramParcelable)
        {
            super();
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            if (this.wasPositiveResult);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.preference.YesNoPreference
 * JD-Core Version:        0.6.2
 */