package com.android.internal.backup;

import android.app.backup.RestoreSet;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IBackupTransport extends IInterface
{
    public abstract int clearBackupData(PackageInfo paramPackageInfo)
        throws RemoteException;

    public abstract Intent configurationIntent()
        throws RemoteException;

    public abstract String currentDestinationString()
        throws RemoteException;

    public abstract int finishBackup()
        throws RemoteException;

    public abstract void finishRestore()
        throws RemoteException;

    public abstract RestoreSet[] getAvailableRestoreSets()
        throws RemoteException;

    public abstract long getCurrentRestoreSet()
        throws RemoteException;

    public abstract int getRestoreData(ParcelFileDescriptor paramParcelFileDescriptor)
        throws RemoteException;

    public abstract int initializeDevice()
        throws RemoteException;

    public abstract String nextRestorePackage()
        throws RemoteException;

    public abstract int performBackup(PackageInfo paramPackageInfo, ParcelFileDescriptor paramParcelFileDescriptor)
        throws RemoteException;

    public abstract long requestBackupTime()
        throws RemoteException;

    public abstract int startRestore(long paramLong, PackageInfo[] paramArrayOfPackageInfo)
        throws RemoteException;

    public abstract String transportDirName()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IBackupTransport
    {
        private static final String DESCRIPTOR = "com.android.internal.backup.IBackupTransport";
        static final int TRANSACTION_clearBackupData = 7;
        static final int TRANSACTION_configurationIntent = 1;
        static final int TRANSACTION_currentDestinationString = 2;
        static final int TRANSACTION_finishBackup = 8;
        static final int TRANSACTION_finishRestore = 14;
        static final int TRANSACTION_getAvailableRestoreSets = 9;
        static final int TRANSACTION_getCurrentRestoreSet = 10;
        static final int TRANSACTION_getRestoreData = 13;
        static final int TRANSACTION_initializeDevice = 5;
        static final int TRANSACTION_nextRestorePackage = 12;
        static final int TRANSACTION_performBackup = 6;
        static final int TRANSACTION_requestBackupTime = 4;
        static final int TRANSACTION_startRestore = 11;
        static final int TRANSACTION_transportDirName = 3;

        public Stub()
        {
            attachInterface(this, "com.android.internal.backup.IBackupTransport");
        }

        public static IBackupTransport asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.backup.IBackupTransport");
                if ((localIInterface != null) && ((localIInterface instanceof IBackupTransport)))
                    localObject = (IBackupTransport)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("com.android.internal.backup.IBackupTransport");
                continue;
                paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                Intent localIntent = configurationIntent();
                paramParcel2.writeNoException();
                if (localIntent != null)
                {
                    paramParcel2.writeInt(i);
                    localIntent.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    String str3 = currentDestinationString();
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str3);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    String str2 = transportDirName();
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str2);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    long l2 = requestBackupTime();
                    paramParcel2.writeNoException();
                    paramParcel2.writeLong(l2);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    int i2 = initializeDevice();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i2);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    PackageInfo localPackageInfo2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localPackageInfo2 = (PackageInfo)PackageInfo.CREATOR.createFromParcel(paramParcel1);
                        label332: if (paramParcel1.readInt() == 0)
                            break label382;
                    }
                    label382: for (ParcelFileDescriptor localParcelFileDescriptor2 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor2 = null)
                    {
                        int i1 = performBackup(localPackageInfo2, localParcelFileDescriptor2);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i1);
                        break;
                        localPackageInfo2 = null;
                        break label332;
                    }
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    if (paramParcel1.readInt() != 0);
                    for (PackageInfo localPackageInfo1 = (PackageInfo)PackageInfo.CREATOR.createFromParcel(paramParcel1); ; localPackageInfo1 = null)
                    {
                        int n = clearBackupData(localPackageInfo1);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(n);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    int m = finishBackup();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(m);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    RestoreSet[] arrayOfRestoreSet = getAvailableRestoreSets();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedArray(arrayOfRestoreSet, i);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    long l1 = getCurrentRestoreSet();
                    paramParcel2.writeNoException();
                    paramParcel2.writeLong(l1);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    int k = startRestore(paramParcel1.readLong(), (PackageInfo[])paramParcel1.createTypedArray(PackageInfo.CREATOR));
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(k);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    String str1 = nextRestorePackage();
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str1);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    if (paramParcel1.readInt() != 0);
                    for (ParcelFileDescriptor localParcelFileDescriptor1 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor1 = null)
                    {
                        int j = getRestoreData(localParcelFileDescriptor1);
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(j);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
                    finishRestore();
                    paramParcel2.writeNoException();
                }
            }
        }

        private static class Proxy
            implements IBackupTransport
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public int clearBackupData(PackageInfo paramPackageInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    if (paramPackageInfo != null)
                    {
                        localParcel1.writeInt(1);
                        paramPackageInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Intent configurationIntent()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localIntent = (Intent)Intent.CREATOR.createFromParcel(localParcel2);
                        return localIntent;
                    }
                    Intent localIntent = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String currentDestinationString()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int finishBackup()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void finishRestore()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public RestoreSet[] getAvailableRestoreSets()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    RestoreSet[] arrayOfRestoreSet = (RestoreSet[])localParcel2.createTypedArray(RestoreSet.CREATOR);
                    return arrayOfRestoreSet;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getCurrentRestoreSet()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.backup.IBackupTransport";
            }

            public int getRestoreData(ParcelFileDescriptor paramParcelFileDescriptor)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    if (paramParcelFileDescriptor != null)
                    {
                        localParcel1.writeInt(1);
                        paramParcelFileDescriptor.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int initializeDevice()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String nextRestorePackage()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int performBackup(PackageInfo paramPackageInfo, ParcelFileDescriptor paramParcelFileDescriptor)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                        if (paramPackageInfo != null)
                        {
                            localParcel1.writeInt(1);
                            paramPackageInfo.writeToParcel(localParcel1, 0);
                            if (paramParcelFileDescriptor != null)
                            {
                                localParcel1.writeInt(1);
                                paramParcelFileDescriptor.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(6, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                return i;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public long requestBackupTime()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int startRestore(long paramLong, PackageInfo[] paramArrayOfPackageInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    localParcel1.writeLong(paramLong);
                    localParcel1.writeTypedArray(paramArrayOfPackageInfo, 0);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String transportDirName()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.backup.IBackupTransport
 * JD-Core Version:        0.6.2
 */