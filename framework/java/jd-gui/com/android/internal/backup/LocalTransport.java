package com.android.internal.backup;

import android.app.backup.RestoreSet;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Environment;
import android.os.RemoteException;
import android.util.Log;
import java.io.File;

public class LocalTransport extends IBackupTransport.Stub
{
    private static final boolean DEBUG = true;
    private static final long RESTORE_TOKEN = 1L;
    private static final String TAG = "LocalTransport";
    private static final String TRANSPORT_DESTINATION_STRING = "Backing up to debug-only private cache";
    private static final String TRANSPORT_DIR_NAME = "com.android.internal.backup.LocalTransport";
    private Context mContext;
    private File mDataDir = new File(Environment.getDownloadCacheDirectory(), "backup");
    private int mRestorePackage = -1;
    private PackageInfo[] mRestorePackages = null;

    public LocalTransport(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private void deleteContents(File paramFile)
    {
        File[] arrayOfFile = paramFile.listFiles();
        if (arrayOfFile != null)
        {
            int i = arrayOfFile.length;
            for (int j = 0; j < i; j++)
            {
                File localFile = arrayOfFile[j];
                if (localFile.isDirectory())
                    deleteContents(localFile);
                localFile.delete();
            }
        }
    }

    public int clearBackupData(PackageInfo paramPackageInfo)
    {
        Log.v("LocalTransport", "clearBackupData() pkg=" + paramPackageInfo.packageName);
        File localFile = new File(this.mDataDir, paramPackageInfo.packageName);
        File[] arrayOfFile = localFile.listFiles();
        if (arrayOfFile != null)
        {
            int i = arrayOfFile.length;
            for (int j = 0; j < i; j++)
                arrayOfFile[j].delete();
            localFile.delete();
        }
        return 0;
    }

    public Intent configurationIntent()
    {
        return null;
    }

    public String currentDestinationString()
    {
        return "Backing up to debug-only private cache";
    }

    public int finishBackup()
    {
        Log.v("LocalTransport", "finishBackup()");
        return 0;
    }

    public void finishRestore()
    {
        Log.v("LocalTransport", "finishRestore()");
    }

    public RestoreSet[] getAvailableRestoreSets()
        throws RemoteException
    {
        RestoreSet localRestoreSet = new RestoreSet("Local disk image", "flash", 1L);
        RestoreSet[] arrayOfRestoreSet = new RestoreSet[1];
        arrayOfRestoreSet[0] = localRestoreSet;
        return arrayOfRestoreSet;
    }

    public long getCurrentRestoreSet()
    {
        return 1L;
    }

    // ERROR //
    public int getRestoreData(android.os.ParcelFileDescriptor paramParcelFileDescriptor)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 51	com/android/internal/backup/LocalTransport:mRestorePackages	[Landroid/content/pm/PackageInfo;
        //     4: ifnonnull +13 -> 17
        //     7: new 127	java/lang/IllegalStateException
        //     10: dup
        //     11: ldc 129
        //     13: invokespecial 132	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: aload_0
        //     18: getfield 53	com/android/internal/backup/LocalTransport:mRestorePackage	I
        //     21: ifge +13 -> 34
        //     24: new 127	java/lang/IllegalStateException
        //     27: dup
        //     28: ldc 134
        //     30: invokespecial 132	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     33: athrow
        //     34: new 36	java/io/File
        //     37: dup
        //     38: aload_0
        //     39: getfield 49	com/android/internal/backup/LocalTransport:mDataDir	Ljava/io/File;
        //     42: aload_0
        //     43: getfield 51	com/android/internal/backup/LocalTransport:mRestorePackages	[Landroid/content/pm/PackageInfo;
        //     46: aload_0
        //     47: getfield 53	com/android/internal/backup/LocalTransport:mRestorePackage	I
        //     50: aaload
        //     51: getfield 86	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     54: invokespecial 47	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     57: astore_2
        //     58: aload_2
        //     59: invokevirtual 61	java/io/File:listFiles	()[Ljava/io/File;
        //     62: astore_3
        //     63: aload_3
        //     64: ifnonnull +34 -> 98
        //     67: ldc 15
        //     69: new 74	java/lang/StringBuilder
        //     72: dup
        //     73: invokespecial 75	java/lang/StringBuilder:<init>	()V
        //     76: ldc 136
        //     78: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     81: aload_2
        //     82: invokevirtual 139	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     85: invokevirtual 90	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     88: invokestatic 142	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     91: pop
        //     92: iconst_1
        //     93: istore 8
        //     95: iload 8
        //     97: ireturn
        //     98: ldc 15
        //     100: new 74	java/lang/StringBuilder
        //     103: dup
        //     104: invokespecial 75	java/lang/StringBuilder:<init>	()V
        //     107: ldc 144
        //     109: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     112: aload_3
        //     113: arraylength
        //     114: invokevirtual 147	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     117: ldc 149
        //     119: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     122: invokevirtual 90	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     125: invokestatic 96	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     128: pop
        //     129: new 151	android/app/backup/BackupDataOutput
        //     132: dup
        //     133: aload_1
        //     134: invokevirtual 157	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     137: invokespecial 160	android/app/backup/BackupDataOutput:<init>	(Ljava/io/FileDescriptor;)V
        //     140: astore 5
        //     142: aload_3
        //     143: arraylength
        //     144: istore 9
        //     146: iconst_0
        //     147: istore 10
        //     149: iload 10
        //     151: iload 9
        //     153: if_icmpge +154 -> 307
        //     156: aload_3
        //     157: iload 10
        //     159: aaload
        //     160: astore 11
        //     162: new 162	java/io/FileInputStream
        //     165: dup
        //     166: aload 11
        //     168: invokespecial 164	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     171: astore 12
        //     173: aload 11
        //     175: invokevirtual 167	java/io/File:length	()J
        //     178: l2i
        //     179: istore 14
        //     181: iload 14
        //     183: newarray byte
        //     185: astore 15
        //     187: aload 12
        //     189: aload 15
        //     191: invokevirtual 171	java/io/FileInputStream:read	([B)I
        //     194: pop
        //     195: new 173	java/lang/String
        //     198: dup
        //     199: aload 11
        //     201: invokevirtual 176	java/io/File:getName	()Ljava/lang/String;
        //     204: invokestatic 182	com/android/org/bouncycastle/util/encoders/Base64:decode	(Ljava/lang/String;)[B
        //     207: invokespecial 185	java/lang/String:<init>	([B)V
        //     210: astore 17
        //     212: ldc 15
        //     214: new 74	java/lang/StringBuilder
        //     217: dup
        //     218: invokespecial 75	java/lang/StringBuilder:<init>	()V
        //     221: ldc 187
        //     223: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     226: aload 17
        //     228: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     231: ldc 189
        //     233: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     236: iload 14
        //     238: invokevirtual 147	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     241: invokevirtual 90	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     244: invokestatic 96	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     247: pop
        //     248: aload 5
        //     250: aload 17
        //     252: iload 14
        //     254: invokevirtual 193	android/app/backup/BackupDataOutput:writeEntityHeader	(Ljava/lang/String;I)I
        //     257: pop
        //     258: aload 5
        //     260: aload 15
        //     262: iload 14
        //     264: invokevirtual 197	android/app/backup/BackupDataOutput:writeEntityData	([BI)I
        //     267: pop
        //     268: aload 12
        //     270: invokevirtual 200	java/io/FileInputStream:close	()V
        //     273: iinc 10 1
        //     276: goto -127 -> 149
        //     279: astore 13
        //     281: aload 12
        //     283: invokevirtual 200	java/io/FileInputStream:close	()V
        //     286: aload 13
        //     288: athrow
        //     289: astore 6
        //     291: ldc 15
        //     293: ldc 202
        //     295: aload 6
        //     297: invokestatic 205	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     300: pop
        //     301: iconst_1
        //     302: istore 8
        //     304: goto -209 -> 95
        //     307: iconst_0
        //     308: istore 8
        //     310: goto -215 -> 95
        //
        // Exception table:
        //     from	to	target	type
        //     173	268	279	finally
        //     142	173	289	java/io/IOException
        //     268	289	289	java/io/IOException
    }

    public int initializeDevice()
    {
        Log.v("LocalTransport", "wiping all data");
        deleteContents(this.mDataDir);
        return 0;
    }

    public String nextRestorePackage()
    {
        if (this.mRestorePackages == null)
            throw new IllegalStateException("startRestore not called");
        String str;
        do
        {
            int i = 1 + this.mRestorePackage;
            this.mRestorePackage = i;
            if (i >= this.mRestorePackages.length)
                break;
            str = this.mRestorePackages[this.mRestorePackage].packageName;
        }
        while (!new File(this.mDataDir, str).isDirectory());
        Log.v("LocalTransport", "    nextRestorePackage() = " + str);
        while (true)
        {
            return str;
            Log.v("LocalTransport", "    no more packages to restore");
            str = "";
        }
    }

    // ERROR //
    public int performBackup(PackageInfo paramPackageInfo, android.os.ParcelFileDescriptor paramParcelFileDescriptor)
    {
        // Byte code:
        //     0: ldc 15
        //     2: new 74	java/lang/StringBuilder
        //     5: dup
        //     6: invokespecial 75	java/lang/StringBuilder:<init>	()V
        //     9: ldc 219
        //     11: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     14: aload_1
        //     15: getfield 86	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     18: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     21: invokevirtual 90	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     24: invokestatic 96	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     27: pop
        //     28: new 36	java/io/File
        //     31: dup
        //     32: aload_0
        //     33: getfield 49	com/android/internal/backup/LocalTransport:mDataDir	Ljava/io/File;
        //     36: aload_1
        //     37: getfield 86	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     40: invokespecial 47	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     43: astore 4
        //     45: aload 4
        //     47: invokevirtual 222	java/io/File:mkdirs	()Z
        //     50: pop
        //     51: new 224	android/app/backup/BackupDataInput
        //     54: dup
        //     55: aload_2
        //     56: invokevirtual 157	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     59: invokespecial 225	android/app/backup/BackupDataInput:<init>	(Ljava/io/FileDescriptor;)V
        //     62: astore 6
        //     64: sipush 512
        //     67: istore 7
        //     69: iload 7
        //     71: newarray byte
        //     73: astore 11
        //     75: aload 6
        //     77: invokevirtual 228	android/app/backup/BackupDataInput:readNextHeader	()Z
        //     80: ifeq +277 -> 357
        //     83: aload 6
        //     85: invokevirtual 231	android/app/backup/BackupDataInput:getKey	()Ljava/lang/String;
        //     88: astore 12
        //     90: new 173	java/lang/String
        //     93: dup
        //     94: aload 12
        //     96: invokevirtual 235	java/lang/String:getBytes	()[B
        //     99: invokestatic 239	com/android/org/bouncycastle/util/encoders/Base64:encode	([B)[B
        //     102: invokespecial 185	java/lang/String:<init>	([B)V
        //     105: astore 13
        //     107: new 36	java/io/File
        //     110: dup
        //     111: aload 4
        //     113: aload 13
        //     115: invokespecial 47	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     118: astore 14
        //     120: aload 6
        //     122: invokevirtual 242	android/app/backup/BackupDataInput:getDataSize	()I
        //     125: istore 15
        //     127: ldc 15
        //     129: new 74	java/lang/StringBuilder
        //     132: dup
        //     133: invokespecial 75	java/lang/StringBuilder:<init>	()V
        //     136: ldc 244
        //     138: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     141: aload 12
        //     143: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     146: ldc 189
        //     148: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     151: iload 15
        //     153: invokevirtual 147	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     156: ldc 246
        //     158: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     161: aload 13
        //     163: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     166: invokevirtual 90	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     169: invokestatic 96	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     172: pop
        //     173: iload 15
        //     175: iflt +173 -> 348
        //     178: aload 14
        //     180: invokevirtual 249	java/io/File:exists	()Z
        //     183: ifeq +9 -> 192
        //     186: aload 14
        //     188: invokevirtual 70	java/io/File:delete	()Z
        //     191: pop
        //     192: new 251	java/io/FileOutputStream
        //     195: dup
        //     196: aload 14
        //     198: invokespecial 252	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     201: astore 18
        //     203: iload 15
        //     205: iload 7
        //     207: if_icmple +13 -> 220
        //     210: iload 15
        //     212: istore 7
        //     214: iload 7
        //     216: newarray byte
        //     218: astore 11
        //     220: aload 6
        //     222: aload 11
        //     224: iconst_0
        //     225: iload 15
        //     227: invokevirtual 256	android/app/backup/BackupDataInput:readEntityData	([BII)I
        //     230: pop
        //     231: ldc 15
        //     233: new 74	java/lang/StringBuilder
        //     236: dup
        //     237: invokespecial 75	java/lang/StringBuilder:<init>	()V
        //     240: ldc_w 258
        //     243: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     246: iload 15
        //     248: invokevirtual 147	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     251: invokevirtual 90	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     254: invokestatic 96	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     257: pop
        //     258: aload 18
        //     260: aload 11
        //     262: iconst_0
        //     263: iload 15
        //     265: invokevirtual 262	java/io/FileOutputStream:write	([BII)V
        //     268: aload 18
        //     270: invokevirtual 263	java/io/FileOutputStream:close	()V
        //     273: goto -198 -> 75
        //     276: astore 8
        //     278: ldc 15
        //     280: ldc_w 265
        //     283: aload 8
        //     285: invokestatic 267	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     288: pop
        //     289: iconst_1
        //     290: istore 10
        //     292: iload 10
        //     294: ireturn
        //     295: astore 22
        //     297: ldc 15
        //     299: new 74	java/lang/StringBuilder
        //     302: dup
        //     303: invokespecial 75	java/lang/StringBuilder:<init>	()V
        //     306: ldc_w 269
        //     309: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     312: aload 14
        //     314: invokevirtual 272	java/io/File:getAbsolutePath	()Ljava/lang/String;
        //     317: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     320: invokevirtual 90	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     323: invokestatic 142	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     326: pop
        //     327: iconst_1
        //     328: istore 10
        //     330: aload 18
        //     332: invokevirtual 263	java/io/FileOutputStream:close	()V
        //     335: goto -43 -> 292
        //     338: astore 21
        //     340: aload 18
        //     342: invokevirtual 263	java/io/FileOutputStream:close	()V
        //     345: aload 21
        //     347: athrow
        //     348: aload 14
        //     350: invokevirtual 70	java/io/File:delete	()Z
        //     353: pop
        //     354: goto -279 -> 75
        //     357: iconst_0
        //     358: istore 10
        //     360: goto -68 -> 292
        //
        // Exception table:
        //     from	to	target	type
        //     69	258	276	java/io/IOException
        //     268	273	276	java/io/IOException
        //     330	354	276	java/io/IOException
        //     258	268	295	java/io/IOException
        //     258	268	338	finally
        //     297	327	338	finally
    }

    public long requestBackupTime()
    {
        return 0L;
    }

    public int startRestore(long paramLong, PackageInfo[] paramArrayOfPackageInfo)
    {
        Log.v("LocalTransport", "start restore " + paramLong);
        this.mRestorePackages = paramArrayOfPackageInfo;
        this.mRestorePackage = -1;
        return 0;
    }

    public String transportDirName()
    {
        return "com.android.internal.backup.LocalTransport";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.backup.LocalTransport
 * JD-Core Version:        0.6.2
 */