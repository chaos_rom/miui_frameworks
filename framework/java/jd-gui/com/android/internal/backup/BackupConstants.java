package com.android.internal.backup;

public class BackupConstants
{
    public static final int AGENT_ERROR = 3;
    public static final int AGENT_UNKNOWN = 4;
    public static final int TRANSPORT_ERROR = 1;
    public static final int TRANSPORT_NOT_INITIALIZED = 2;
    public static final int TRANSPORT_OK;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.backup.BackupConstants
 * JD-Core Version:        0.6.2
 */