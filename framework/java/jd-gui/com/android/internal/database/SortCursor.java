package com.android.internal.database;

import android.database.AbstractCursor;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.util.Log;
import java.lang.reflect.Array;

public class SortCursor extends AbstractCursor
{
    private static final String TAG = "SortCursor";
    private final int ROWCACHESIZE = 64;
    private int[][] mCurRowNumCache;
    private Cursor mCursor;
    private int[] mCursorCache = new int[64];
    private Cursor[] mCursors;
    private int mLastCacheHit = -1;
    private DataSetObserver mObserver = new DataSetObserver()
    {
        public void onChanged()
        {
            SortCursor.access$002(SortCursor.this, -1);
        }

        public void onInvalidated()
        {
            SortCursor.access$102(SortCursor.this, -1);
        }
    };
    private int[] mRowNumCache = new int[64];
    private int[] mSortColumns;

    public SortCursor(Cursor[] paramArrayOfCursor, String paramString)
    {
        this.mCursors = paramArrayOfCursor;
        int i = this.mCursors.length;
        this.mSortColumns = new int[i];
        int j = 0;
        if (j < i)
        {
            if (this.mCursors[j] == null);
            while (true)
            {
                j++;
                break;
                this.mCursors[j].registerDataSetObserver(this.mObserver);
                this.mCursors[j].moveToFirst();
                this.mSortColumns[j] = this.mCursors[j].getColumnIndexOrThrow(paramString);
            }
        }
        this.mCursor = null;
        Object localObject = "";
        int k = 0;
        if (k < i)
        {
            if ((this.mCursors[k] == null) || (this.mCursors[k].isAfterLast()));
            while (true)
            {
                k++;
                break;
                String str = this.mCursors[k].getString(this.mSortColumns[k]);
                if ((this.mCursor == null) || (str.compareToIgnoreCase((String)localObject) < 0))
                {
                    localObject = str;
                    this.mCursor = this.mCursors[k];
                }
            }
        }
        for (int m = -1 + this.mRowNumCache.length; m >= 0; m--)
            this.mRowNumCache[m] = -2;
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 64;
        arrayOfInt[1] = i;
        this.mCurRowNumCache = ((int[][])Array.newInstance(Integer.TYPE, arrayOfInt));
    }

    public void close()
    {
        int i = this.mCursors.length;
        int j = 0;
        if (j < i)
        {
            if (this.mCursors[j] == null);
            while (true)
            {
                j++;
                break;
                this.mCursors[j].close();
            }
        }
    }

    public void deactivate()
    {
        int i = this.mCursors.length;
        int j = 0;
        if (j < i)
        {
            if (this.mCursors[j] == null);
            while (true)
            {
                j++;
                break;
                this.mCursors[j].deactivate();
            }
        }
    }

    public byte[] getBlob(int paramInt)
    {
        return this.mCursor.getBlob(paramInt);
    }

    public String[] getColumnNames()
    {
        String[] arrayOfString;
        if (this.mCursor != null)
        {
            arrayOfString = this.mCursor.getColumnNames();
            return arrayOfString;
        }
        int i = this.mCursors.length;
        for (int j = 0; ; j++)
        {
            if (j >= i)
                break label62;
            if (this.mCursors[j] != null)
            {
                arrayOfString = this.mCursors[j].getColumnNames();
                break;
            }
        }
        label62: throw new IllegalStateException("No cursor that can return names");
    }

    public int getCount()
    {
        int i = 0;
        int j = this.mCursors.length;
        for (int k = 0; k < j; k++)
            if (this.mCursors[k] != null)
                i += this.mCursors[k].getCount();
        return i;
    }

    public double getDouble(int paramInt)
    {
        return this.mCursor.getDouble(paramInt);
    }

    public float getFloat(int paramInt)
    {
        return this.mCursor.getFloat(paramInt);
    }

    public int getInt(int paramInt)
    {
        return this.mCursor.getInt(paramInt);
    }

    public long getLong(int paramInt)
    {
        return this.mCursor.getLong(paramInt);
    }

    public short getShort(int paramInt)
    {
        return this.mCursor.getShort(paramInt);
    }

    public String getString(int paramInt)
    {
        return this.mCursor.getString(paramInt);
    }

    public int getType(int paramInt)
    {
        return this.mCursor.getType(paramInt);
    }

    public boolean isNull(int paramInt)
    {
        return this.mCursor.isNull(paramInt);
    }

    public boolean onMove(int paramInt1, int paramInt2)
    {
        boolean bool = true;
        if (paramInt1 == paramInt2);
        while (true)
        {
            return bool;
            int i = paramInt2 % 64;
            if (this.mRowNumCache[i] == paramInt2)
            {
                int i4 = this.mCursorCache[i];
                this.mCursor = this.mCursors[i4];
                if (this.mCursor == null)
                {
                    Log.w("SortCursor", "onMove: cache results in a null cursor.");
                    bool = false;
                }
                else
                {
                    this.mCursor.moveToPosition(this.mCurRowNumCache[i][i4]);
                    this.mLastCacheHit = i;
                }
            }
            else
            {
                this.mCursor = null;
                int j = this.mCursors.length;
                if (this.mLastCacheHit >= 0)
                {
                    int i3 = 0;
                    if (i3 < j)
                    {
                        if (this.mCursors[i3] == null);
                        while (true)
                        {
                            i3++;
                            break;
                            this.mCursors[i3].moveToPosition(this.mCurRowNumCache[this.mLastCacheHit][i3]);
                        }
                    }
                }
                if ((paramInt2 < paramInt1) || (paramInt1 == -1))
                {
                    int k = 0;
                    if (k < j)
                    {
                        if (this.mCursors[k] == null);
                        while (true)
                        {
                            k++;
                            break;
                            this.mCursors[k].moveToFirst();
                        }
                    }
                    paramInt1 = 0;
                }
                if (paramInt1 < 0)
                    paramInt1 = 0;
                int m = -1;
                for (int n = paramInt1; ; n++)
                {
                    if (n <= paramInt2)
                    {
                        Object localObject = "";
                        m = -1;
                        int i2 = 0;
                        if (i2 < j)
                        {
                            if ((this.mCursors[i2] == null) || (this.mCursors[i2].isAfterLast()));
                            while (true)
                            {
                                i2++;
                                break;
                                String str = this.mCursors[i2].getString(this.mSortColumns[i2]);
                                if ((m < 0) || (str.compareToIgnoreCase((String)localObject) < 0))
                                {
                                    localObject = str;
                                    m = i2;
                                }
                            }
                        }
                        if (n != paramInt2);
                    }
                    else
                    {
                        this.mCursor = this.mCursors[m];
                        this.mRowNumCache[i] = paramInt2;
                        this.mCursorCache[i] = m;
                        for (int i1 = 0; i1 < j; i1++)
                            if (this.mCursors[i1] != null)
                                this.mCurRowNumCache[i][i1] = this.mCursors[i1].getPosition();
                    }
                    if (this.mCursors[m] != null)
                        this.mCursors[m].moveToNext();
                }
                this.mLastCacheHit = -1;
            }
        }
    }

    public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        int i = this.mCursors.length;
        for (int j = 0; j < i; j++)
            if (this.mCursors[j] != null)
                this.mCursors[j].registerDataSetObserver(paramDataSetObserver);
    }

    public boolean requery()
    {
        int i = this.mCursors.length;
        int j = 0;
        if (j < i)
        {
            if (this.mCursors[j] == null);
            while (this.mCursors[j].requery())
            {
                j++;
                break;
            }
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
        int i = this.mCursors.length;
        for (int j = 0; j < i; j++)
            if (this.mCursors[j] != null)
                this.mCursors[j].unregisterDataSetObserver(paramDataSetObserver);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.database.SortCursor
 * JD-Core Version:        0.6.2
 */