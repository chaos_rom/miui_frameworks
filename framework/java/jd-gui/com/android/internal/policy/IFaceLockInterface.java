package com.android.internal.policy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IFaceLockInterface extends IInterface
{
    public abstract void registerCallback(IFaceLockCallback paramIFaceLockCallback)
        throws RemoteException;

    public abstract void startUi(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
        throws RemoteException;

    public abstract void stopUi()
        throws RemoteException;

    public abstract void unregisterCallback(IFaceLockCallback paramIFaceLockCallback)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IFaceLockInterface
    {
        private static final String DESCRIPTOR = "com.android.internal.policy.IFaceLockInterface";
        static final int TRANSACTION_registerCallback = 3;
        static final int TRANSACTION_startUi = 1;
        static final int TRANSACTION_stopUi = 2;
        static final int TRANSACTION_unregisterCallback = 4;

        public Stub()
        {
            attachInterface(this, "com.android.internal.policy.IFaceLockInterface");
        }

        public static IFaceLockInterface asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.policy.IFaceLockInterface");
                if ((localIInterface != null) && ((localIInterface instanceof IFaceLockInterface)))
                    localObject = (IFaceLockInterface)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.policy.IFaceLockInterface");
                continue;
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockInterface");
                IBinder localIBinder = paramParcel1.readStrongBinder();
                int i = paramParcel1.readInt();
                int j = paramParcel1.readInt();
                int k = paramParcel1.readInt();
                int m = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    startUi(localIBinder, i, j, k, m, bool2);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockInterface");
                stopUi();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockInterface");
                registerCallback(IFaceLockCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockInterface");
                unregisterCallback(IFaceLockCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements IFaceLockInterface
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.policy.IFaceLockInterface";
            }

            public void registerCallback(IFaceLockCallback paramIFaceLockCallback)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.policy.IFaceLockInterface");
                    if (paramIFaceLockCallback != null)
                    {
                        localIBinder = paramIFaceLockCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void startUi(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.policy.IFaceLockInterface");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    localParcel1.writeInt(paramInt4);
                    if (paramBoolean)
                    {
                        localParcel1.writeInt(i);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void stopUi()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.policy.IFaceLockInterface");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unregisterCallback(IFaceLockCallback paramIFaceLockCallback)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.policy.IFaceLockInterface");
                    if (paramIFaceLockCallback != null)
                    {
                        localIBinder = paramIFaceLockCallback.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.IFaceLockInterface
 * JD-Core Version:        0.6.2
 */