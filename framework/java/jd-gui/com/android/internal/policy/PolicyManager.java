package com.android.internal.policy;

import android.content.Context;
import android.view.FallbackEventHandler;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManagerPolicy;

public final class PolicyManager
{
    private static final String POLICY_IMPL_CLASS_NAME = "com.android.internal.policy.impl.Policy";
    private static final IPolicy sPolicy;

    static
    {
        try
        {
            sPolicy = (IPolicy)Class.forName("com.android.internal.policy.impl.Policy").newInstance();
            return;
        }
        catch (ClassNotFoundException localClassNotFoundException)
        {
            throw new RuntimeException("com.android.internal.policy.impl.Policy could not be loaded", localClassNotFoundException);
        }
        catch (InstantiationException localInstantiationException)
        {
            throw new RuntimeException("com.android.internal.policy.impl.Policy could not be instantiated", localInstantiationException);
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
            throw new RuntimeException("com.android.internal.policy.impl.Policy could not be instantiated", localIllegalAccessException);
        }
    }

    public static FallbackEventHandler makeNewFallbackEventHandler(Context paramContext)
    {
        return sPolicy.makeNewFallbackEventHandler(paramContext);
    }

    public static LayoutInflater makeNewLayoutInflater(Context paramContext)
    {
        return sPolicy.makeNewLayoutInflater(paramContext);
    }

    public static Window makeNewWindow(Context paramContext)
    {
        return sPolicy.makeNewWindow(paramContext);
    }

    public static WindowManagerPolicy makeNewWindowManager()
    {
        return sPolicy.makeNewWindowManager();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.PolicyManager
 * JD-Core Version:        0.6.2
 */