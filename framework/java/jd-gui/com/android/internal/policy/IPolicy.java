package com.android.internal.policy;

import android.content.Context;
import android.view.FallbackEventHandler;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManagerPolicy;

public abstract interface IPolicy
{
    public abstract FallbackEventHandler makeNewFallbackEventHandler(Context paramContext);

    public abstract LayoutInflater makeNewLayoutInflater(Context paramContext);

    public abstract Window makeNewWindow(Context paramContext);

    public abstract WindowManagerPolicy makeNewWindowManager();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.IPolicy
 * JD-Core Version:        0.6.2
 */