package com.android.internal.policy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IFaceLockCallback extends IInterface
{
    public abstract void cancel()
        throws RemoteException;

    public abstract void exposeFallback()
        throws RemoteException;

    public abstract void pokeWakelock(int paramInt)
        throws RemoteException;

    public abstract void reportFailedAttempt()
        throws RemoteException;

    public abstract void unlock()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IFaceLockCallback
    {
        private static final String DESCRIPTOR = "com.android.internal.policy.IFaceLockCallback";
        static final int TRANSACTION_cancel = 2;
        static final int TRANSACTION_exposeFallback = 4;
        static final int TRANSACTION_pokeWakelock = 5;
        static final int TRANSACTION_reportFailedAttempt = 3;
        static final int TRANSACTION_unlock = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.policy.IFaceLockCallback");
        }

        public static IFaceLockCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.policy.IFaceLockCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IFaceLockCallback)))
                    localObject = (IFaceLockCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("com.android.internal.policy.IFaceLockCallback");
                continue;
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockCallback");
                unlock();
                continue;
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockCallback");
                cancel();
                continue;
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockCallback");
                reportFailedAttempt();
                continue;
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockCallback");
                exposeFallback();
                continue;
                paramParcel1.enforceInterface("com.android.internal.policy.IFaceLockCallback");
                pokeWakelock(paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements IFaceLockCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancel()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.policy.IFaceLockCallback");
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void exposeFallback()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.policy.IFaceLockCallback");
                    this.mRemote.transact(4, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.policy.IFaceLockCallback";
            }

            public void pokeWakelock(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.policy.IFaceLockCallback");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void reportFailedAttempt()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.policy.IFaceLockCallback");
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void unlock()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.policy.IFaceLockCallback");
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.IFaceLockCallback
 * JD-Core Version:        0.6.2
 */