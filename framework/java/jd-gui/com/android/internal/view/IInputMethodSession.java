package com.android.internal.view;

import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.ExtractedText;

public abstract interface IInputMethodSession extends IInterface
{
    public abstract void appPrivateCommand(String paramString, Bundle paramBundle)
        throws RemoteException;

    public abstract void dispatchKeyEvent(int paramInt, KeyEvent paramKeyEvent, IInputMethodCallback paramIInputMethodCallback)
        throws RemoteException;

    public abstract void dispatchTrackballEvent(int paramInt, MotionEvent paramMotionEvent, IInputMethodCallback paramIInputMethodCallback)
        throws RemoteException;

    public abstract void displayCompletions(CompletionInfo[] paramArrayOfCompletionInfo)
        throws RemoteException;

    public abstract void finishInput()
        throws RemoteException;

    public abstract void finishSession()
        throws RemoteException;

    public abstract void toggleSoftInput(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void updateCursor(Rect paramRect)
        throws RemoteException;

    public abstract void updateExtractedText(int paramInt, ExtractedText paramExtractedText)
        throws RemoteException;

    public abstract void updateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
        throws RemoteException;

    public abstract void viewClicked(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputMethodSession
    {
        private static final String DESCRIPTOR = "com.android.internal.view.IInputMethodSession";
        static final int TRANSACTION_appPrivateCommand = 9;
        static final int TRANSACTION_dispatchKeyEvent = 7;
        static final int TRANSACTION_dispatchTrackballEvent = 8;
        static final int TRANSACTION_displayCompletions = 6;
        static final int TRANSACTION_finishInput = 1;
        static final int TRANSACTION_finishSession = 11;
        static final int TRANSACTION_toggleSoftInput = 10;
        static final int TRANSACTION_updateCursor = 5;
        static final int TRANSACTION_updateExtractedText = 2;
        static final int TRANSACTION_updateSelection = 3;
        static final int TRANSACTION_viewClicked = 4;

        public Stub()
        {
            attachInterface(this, "com.android.internal.view.IInputMethodSession");
        }

        public static IInputMethodSession asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.view.IInputMethodSession");
                if ((localIInterface != null) && ((localIInterface instanceof IInputMethodSession)))
                    localObject = (IInputMethodSession)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.view.IInputMethodSession");
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                finishInput();
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                int k = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (ExtractedText localExtractedText = (ExtractedText)ExtractedText.CREATOR.createFromParcel(paramParcel1); ; localExtractedText = null)
                {
                    updateExtractedText(k, localExtractedText);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                updateSelection(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    viewClicked(bool2);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                if (paramParcel1.readInt() != 0);
                for (Rect localRect = (Rect)Rect.CREATOR.createFromParcel(paramParcel1); ; localRect = null)
                {
                    updateCursor(localRect);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                displayCompletions((CompletionInfo[])paramParcel1.createTypedArray(CompletionInfo.CREATOR));
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                int j = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (KeyEvent localKeyEvent = (KeyEvent)KeyEvent.CREATOR.createFromParcel(paramParcel1); ; localKeyEvent = null)
                {
                    dispatchKeyEvent(j, localKeyEvent, IInputMethodCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                int i = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (MotionEvent localMotionEvent = (MotionEvent)MotionEvent.CREATOR.createFromParcel(paramParcel1); ; localMotionEvent = null)
                {
                    dispatchTrackballEvent(i, localMotionEvent, IInputMethodCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                String str = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                {
                    appPrivateCommand(str, localBundle);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                toggleSoftInput(paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodSession");
                finishSession();
            }
        }

        private static class Proxy
            implements IInputMethodSession
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void appPrivateCommand(String paramString, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    localParcel.writeString(paramString);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void dispatchKeyEvent(int paramInt, KeyEvent paramKeyEvent, IInputMethodCallback paramIInputMethodCallback)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    localParcel.writeInt(paramInt);
                    if (paramKeyEvent != null)
                    {
                        localParcel.writeInt(1);
                        paramKeyEvent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        if (paramIInputMethodCallback != null)
                            localIBinder = paramIInputMethodCallback.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        this.mRemote.transact(7, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void dispatchTrackballEvent(int paramInt, MotionEvent paramMotionEvent, IInputMethodCallback paramIInputMethodCallback)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    localParcel.writeInt(paramInt);
                    if (paramMotionEvent != null)
                    {
                        localParcel.writeInt(1);
                        paramMotionEvent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        if (paramIInputMethodCallback != null)
                            localIBinder = paramIInputMethodCallback.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        this.mRemote.transact(8, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void displayCompletions(CompletionInfo[] paramArrayOfCompletionInfo)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    localParcel.writeTypedArray(paramArrayOfCompletionInfo, 0);
                    this.mRemote.transact(6, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void finishInput()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void finishSession()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    this.mRemote.transact(11, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.view.IInputMethodSession";
            }

            public void toggleSoftInput(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(10, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void updateCursor(Rect paramRect)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    if (paramRect != null)
                    {
                        localParcel.writeInt(1);
                        paramRect.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(5, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void updateExtractedText(int paramInt, ExtractedText paramExtractedText)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    localParcel.writeInt(paramInt);
                    if (paramExtractedText != null)
                    {
                        localParcel.writeInt(1);
                        paramExtractedText.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void updateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    localParcel.writeInt(paramInt4);
                    localParcel.writeInt(paramInt5);
                    localParcel.writeInt(paramInt6);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void viewClicked(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodSession");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.IInputMethodSession
 * JD-Core Version:        0.6.2
 */