package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.text.style.SuggestionSpan;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodSubtype;
import java.util.ArrayList;
import java.util.List;

public abstract interface IInputMethodManager extends IInterface
{
    public abstract void addClient(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void finishInput(IInputMethodClient paramIInputMethodClient)
        throws RemoteException;

    public abstract InputMethodSubtype getCurrentInputMethodSubtype()
        throws RemoteException;

    public abstract List<InputMethodInfo> getEnabledInputMethodList()
        throws RemoteException;

    public abstract List<InputMethodSubtype> getEnabledInputMethodSubtypeList(InputMethodInfo paramInputMethodInfo, boolean paramBoolean)
        throws RemoteException;

    public abstract List<InputMethodInfo> getInputMethodList()
        throws RemoteException;

    public abstract InputMethodSubtype getLastInputMethodSubtype()
        throws RemoteException;

    public abstract List getShortcutInputMethodsAndSubtypes()
        throws RemoteException;

    public abstract void hideMySoftInput(IBinder paramIBinder, int paramInt)
        throws RemoteException;

    public abstract boolean hideSoftInput(IInputMethodClient paramIInputMethodClient, int paramInt, ResultReceiver paramResultReceiver)
        throws RemoteException;

    public abstract boolean notifySuggestionPicked(SuggestionSpan paramSuggestionSpan, String paramString, int paramInt)
        throws RemoteException;

    public abstract void registerSuggestionSpansForNotification(SuggestionSpan[] paramArrayOfSuggestionSpan)
        throws RemoteException;

    public abstract void removeClient(IInputMethodClient paramIInputMethodClient)
        throws RemoteException;

    public abstract void setAdditionalInputMethodSubtypes(String paramString, InputMethodSubtype[] paramArrayOfInputMethodSubtype)
        throws RemoteException;

    public abstract boolean setCurrentInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype)
        throws RemoteException;

    public abstract void setImeWindowStatus(IBinder paramIBinder, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setInputMethod(IBinder paramIBinder, String paramString)
        throws RemoteException;

    public abstract void setInputMethodAndSubtype(IBinder paramIBinder, String paramString, InputMethodSubtype paramInputMethodSubtype)
        throws RemoteException;

    public abstract boolean setInputMethodEnabled(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient paramIInputMethodClient, String paramString)
        throws RemoteException;

    public abstract void showInputMethodPickerFromClient(IInputMethodClient paramIInputMethodClient)
        throws RemoteException;

    public abstract void showMySoftInput(IBinder paramIBinder, int paramInt)
        throws RemoteException;

    public abstract boolean showSoftInput(IInputMethodClient paramIInputMethodClient, int paramInt, ResultReceiver paramResultReceiver)
        throws RemoteException;

    public abstract InputBindResult startInput(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext, EditorInfo paramEditorInfo, int paramInt)
        throws RemoteException;

    public abstract boolean switchToLastInputMethod(IBinder paramIBinder)
        throws RemoteException;

    public abstract boolean switchToNextInputMethod(IBinder paramIBinder, boolean paramBoolean)
        throws RemoteException;

    public abstract void updateStatusIcon(IBinder paramIBinder, String paramString, int paramInt)
        throws RemoteException;

    public abstract InputBindResult windowGainedFocus(IInputMethodClient paramIInputMethodClient, IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, EditorInfo paramEditorInfo, IInputContext paramIInputContext)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputMethodManager
    {
        private static final String DESCRIPTOR = "com.android.internal.view.IInputMethodManager";
        static final int TRANSACTION_addClient = 6;
        static final int TRANSACTION_finishInput = 9;
        static final int TRANSACTION_getCurrentInputMethodSubtype = 23;
        static final int TRANSACTION_getEnabledInputMethodList = 2;
        static final int TRANSACTION_getEnabledInputMethodSubtypeList = 3;
        static final int TRANSACTION_getInputMethodList = 1;
        static final int TRANSACTION_getLastInputMethodSubtype = 4;
        static final int TRANSACTION_getShortcutInputMethodsAndSubtypes = 5;
        static final int TRANSACTION_hideMySoftInput = 17;
        static final int TRANSACTION_hideSoftInput = 11;
        static final int TRANSACTION_notifySuggestionPicked = 22;
        static final int TRANSACTION_registerSuggestionSpansForNotification = 21;
        static final int TRANSACTION_removeClient = 7;
        static final int TRANSACTION_setAdditionalInputMethodSubtypes = 28;
        static final int TRANSACTION_setCurrentInputMethodSubtype = 24;
        static final int TRANSACTION_setImeWindowStatus = 20;
        static final int TRANSACTION_setInputMethod = 15;
        static final int TRANSACTION_setInputMethodAndSubtype = 16;
        static final int TRANSACTION_setInputMethodEnabled = 27;
        static final int TRANSACTION_showInputMethodAndSubtypeEnablerFromClient = 14;
        static final int TRANSACTION_showInputMethodPickerFromClient = 13;
        static final int TRANSACTION_showMySoftInput = 18;
        static final int TRANSACTION_showSoftInput = 10;
        static final int TRANSACTION_startInput = 8;
        static final int TRANSACTION_switchToLastInputMethod = 25;
        static final int TRANSACTION_switchToNextInputMethod = 26;
        static final int TRANSACTION_updateStatusIcon = 19;
        static final int TRANSACTION_windowGainedFocus = 12;

        public Stub()
        {
            attachInterface(this, "com.android.internal.view.IInputMethodManager");
        }

        public static IInputMethodManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.view.IInputMethodManager");
                if ((localIInterface != null) && ((localIInterface instanceof IInputMethodManager)))
                    localObject = (IInputMethodManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.view.IInputMethodManager");
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                List localList4 = getInputMethodList();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList4);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                List localList3 = getEnabledInputMethodList();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList3);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                InputMethodInfo localInputMethodInfo;
                if (paramParcel1.readInt() != 0)
                {
                    localInputMethodInfo = (InputMethodInfo)InputMethodInfo.CREATOR.createFromParcel(paramParcel1);
                    label353: if (paramParcel1.readInt() == 0)
                        break label395;
                }
                label395: for (boolean bool11 = true; ; bool11 = false)
                {
                    List localList2 = getEnabledInputMethodSubtypeList(localInputMethodInfo, bool11);
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList2);
                    bool1 = true;
                    break;
                    localInputMethodInfo = null;
                    break label353;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                InputMethodSubtype localInputMethodSubtype4 = getLastInputMethodSubtype();
                paramParcel2.writeNoException();
                if (localInputMethodSubtype4 != null)
                {
                    paramParcel2.writeInt(1);
                    localInputMethodSubtype4.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                List localList1 = getShortcutInputMethodsAndSubtypes();
                paramParcel2.writeNoException();
                paramParcel2.writeList(localList1);
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                addClient(IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder()), IInputContext.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                removeClient(IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                IInputMethodClient localIInputMethodClient4 = IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder());
                IInputContext localIInputContext = IInputContext.Stub.asInterface(paramParcel1.readStrongBinder());
                EditorInfo localEditorInfo2;
                if (paramParcel1.readInt() != 0)
                {
                    localEditorInfo2 = (EditorInfo)EditorInfo.CREATOR.createFromParcel(paramParcel1);
                    label590: InputBindResult localInputBindResult2 = startInput(localIInputMethodClient4, localIInputContext, localEditorInfo2, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localInputBindResult2 == null)
                        break label639;
                    paramParcel2.writeInt(1);
                    localInputBindResult2.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localEditorInfo2 = null;
                    break label590;
                    label639: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                finishInput(IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                IInputMethodClient localIInputMethodClient3 = IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder());
                int i6 = paramParcel1.readInt();
                ResultReceiver localResultReceiver2;
                if (paramParcel1.readInt() != 0)
                {
                    localResultReceiver2 = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(paramParcel1);
                    label716: boolean bool10 = showSoftInput(localIInputMethodClient3, i6, localResultReceiver2);
                    paramParcel2.writeNoException();
                    if (!bool10)
                        break label758;
                }
                label758: for (int i7 = 1; ; i7 = 0)
                {
                    paramParcel2.writeInt(i7);
                    bool1 = true;
                    break;
                    localResultReceiver2 = null;
                    break label716;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                IInputMethodClient localIInputMethodClient2 = IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder());
                int i4 = paramParcel1.readInt();
                ResultReceiver localResultReceiver1;
                if (paramParcel1.readInt() != 0)
                {
                    localResultReceiver1 = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(paramParcel1);
                    label806: boolean bool9 = hideSoftInput(localIInputMethodClient2, i4, localResultReceiver1);
                    paramParcel2.writeNoException();
                    if (!bool9)
                        break label848;
                }
                label848: for (int i5 = 1; ; i5 = 0)
                {
                    paramParcel2.writeInt(i5);
                    bool1 = true;
                    break;
                    localResultReceiver1 = null;
                    break label806;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                IInputMethodClient localIInputMethodClient1 = IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder());
                IBinder localIBinder3 = paramParcel1.readStrongBinder();
                int i1 = paramParcel1.readInt();
                int i2 = paramParcel1.readInt();
                int i3 = paramParcel1.readInt();
                EditorInfo localEditorInfo1;
                if (paramParcel1.readInt() != 0)
                {
                    localEditorInfo1 = (EditorInfo)EditorInfo.CREATOR.createFromParcel(paramParcel1);
                    label914: InputBindResult localInputBindResult1 = windowGainedFocus(localIInputMethodClient1, localIBinder3, i1, i2, i3, localEditorInfo1, IInputContext.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    if (localInputBindResult1 == null)
                        break label972;
                    paramParcel2.writeInt(1);
                    localInputBindResult1.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localEditorInfo1 = null;
                    break label914;
                    label972: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                showInputMethodPickerFromClient(IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                setInputMethod(paramParcel1.readStrongBinder(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                IBinder localIBinder2 = paramParcel1.readStrongBinder();
                String str2 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (InputMethodSubtype localInputMethodSubtype3 = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(paramParcel1); ; localInputMethodSubtype3 = null)
                {
                    setInputMethodAndSubtype(localIBinder2, str2, localInputMethodSubtype3);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                hideMySoftInput(paramParcel1.readStrongBinder(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                showMySoftInput(paramParcel1.readStrongBinder(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                updateStatusIcon(paramParcel1.readStrongBinder(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                setImeWindowStatus(paramParcel1.readStrongBinder(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                registerSuggestionSpansForNotification((SuggestionSpan[])paramParcel1.createTypedArray(SuggestionSpan.CREATOR));
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                SuggestionSpan localSuggestionSpan;
                if (paramParcel1.readInt() != 0)
                {
                    localSuggestionSpan = (SuggestionSpan)SuggestionSpan.CREATOR.createFromParcel(paramParcel1);
                    label1308: boolean bool8 = notifySuggestionPicked(localSuggestionSpan, paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (!bool8)
                        break label1354;
                }
                label1354: for (int n = 1; ; n = 0)
                {
                    paramParcel2.writeInt(n);
                    bool1 = true;
                    break;
                    localSuggestionSpan = null;
                    break label1308;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                InputMethodSubtype localInputMethodSubtype2 = getCurrentInputMethodSubtype();
                paramParcel2.writeNoException();
                if (localInputMethodSubtype2 != null)
                {
                    paramParcel2.writeInt(1);
                    localInputMethodSubtype2.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                InputMethodSubtype localInputMethodSubtype1;
                if (paramParcel1.readInt() != 0)
                {
                    localInputMethodSubtype1 = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(paramParcel1);
                    label1434: boolean bool7 = setCurrentInputMethodSubtype(localInputMethodSubtype1);
                    paramParcel2.writeNoException();
                    if (!bool7)
                        break label1472;
                }
                label1472: for (int m = 1; ; m = 0)
                {
                    paramParcel2.writeInt(m);
                    bool1 = true;
                    break;
                    localInputMethodSubtype1 = null;
                    break label1434;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                boolean bool6 = switchToLastInputMethod(paramParcel1.readStrongBinder());
                paramParcel2.writeNoException();
                if (bool6);
                for (int k = 1; ; k = 0)
                {
                    paramParcel2.writeInt(k);
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                IBinder localIBinder1 = paramParcel1.readStrongBinder();
                boolean bool4;
                if (paramParcel1.readInt() != 0)
                {
                    bool4 = true;
                    label1546: boolean bool5 = switchToNextInputMethod(localIBinder1, bool4);
                    paramParcel2.writeNoException();
                    if (!bool5)
                        break label1586;
                }
                label1586: for (int j = 1; ; j = 0)
                {
                    paramParcel2.writeInt(j);
                    bool1 = true;
                    break;
                    bool4 = false;
                    break label1546;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                String str1 = paramParcel1.readString();
                boolean bool2;
                if (paramParcel1.readInt() != 0)
                {
                    bool2 = true;
                    label1614: boolean bool3 = setInputMethodEnabled(str1, bool2);
                    paramParcel2.writeNoException();
                    if (!bool3)
                        break label1654;
                }
                label1654: for (int i = 1; ; i = 0)
                {
                    paramParcel2.writeInt(i);
                    bool1 = true;
                    break;
                    bool2 = false;
                    break label1614;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodManager");
                setAdditionalInputMethodSubtypes(paramParcel1.readString(), (InputMethodSubtype[])paramParcel1.createTypedArray(InputMethodSubtype.CREATOR));
                bool1 = true;
            }
        }

        private static class Proxy
            implements IInputMethodManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addClient(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext, int paramInt1, int paramInt2)
                throws RemoteException
            {
                IBinder localIBinder1 = null;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    if (paramIInputMethodClient != null)
                    {
                        localIBinder2 = paramIInputMethodClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder2);
                        if (paramIInputContext != null)
                            localIBinder1 = paramIInputContext.asBinder();
                        localParcel1.writeStrongBinder(localIBinder1);
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder2 = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void finishInput(IInputMethodClient paramIInputMethodClient)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    if (paramIInputMethodClient != null)
                    {
                        localIBinder = paramIInputMethodClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public InputMethodSubtype getCurrentInputMethodSubtype()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localInputMethodSubtype = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(localParcel2);
                        return localInputMethodSubtype;
                    }
                    InputMethodSubtype localInputMethodSubtype = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<InputMethodInfo> getEnabledInputMethodList()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(InputMethodInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<InputMethodSubtype> getEnabledInputMethodSubtypeList(InputMethodInfo paramInputMethodInfo, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                        if (paramInputMethodInfo != null)
                        {
                            localParcel1.writeInt(1);
                            paramInputMethodInfo.writeToParcel(localParcel1, 0);
                            break label118;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(3, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            ArrayList localArrayList = localParcel2.createTypedArrayList(InputMethodSubtype.CREATOR);
                            return localArrayList;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label118: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public List<InputMethodInfo> getInputMethodList()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(InputMethodInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.view.IInputMethodManager";
            }

            public InputMethodSubtype getLastInputMethodSubtype()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localInputMethodSubtype = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(localParcel2);
                        return localInputMethodSubtype;
                    }
                    InputMethodSubtype localInputMethodSubtype = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List getShortcutInputMethodsAndSubtypes()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.readArrayList(getClass().getClassLoader());
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void hideMySoftInput(IBinder paramIBinder, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hideSoftInput(IInputMethodClient paramIInputMethodClient, int paramInt, ResultReceiver paramResultReceiver)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                        IBinder localIBinder;
                        if (paramIInputMethodClient != null)
                        {
                            localIBinder = paramIInputMethodClient.asBinder();
                            localParcel1.writeStrongBinder(localIBinder);
                            localParcel1.writeInt(paramInt);
                            if (paramResultReceiver != null)
                            {
                                localParcel1.writeInt(1);
                                paramResultReceiver.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(11, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label139;
                                return bool;
                            }
                        }
                        else
                        {
                            localIBinder = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label139: bool = false;
                }
            }

            public boolean notifySuggestionPicked(SuggestionSpan paramSuggestionSpan, String paramString, int paramInt)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                        if (paramSuggestionSpan != null)
                        {
                            localParcel1.writeInt(1);
                            paramSuggestionSpan.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(22, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void registerSuggestionSpansForNotification(SuggestionSpan[] paramArrayOfSuggestionSpan)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeTypedArray(paramArrayOfSuggestionSpan, 0);
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeClient(IInputMethodClient paramIInputMethodClient)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    if (paramIInputMethodClient != null)
                    {
                        localIBinder = paramIInputMethodClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAdditionalInputMethodSubtypes(String paramString, InputMethodSubtype[] paramArrayOfInputMethodSubtype)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel.writeString(paramString);
                    localParcel.writeTypedArray(paramArrayOfInputMethodSubtype, 0);
                    this.mRemote.transact(28, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public boolean setCurrentInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                        if (paramInputMethodSubtype != null)
                        {
                            localParcel1.writeInt(1);
                            paramInputMethodSubtype.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(24, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void setImeWindowStatus(IBinder paramIBinder, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInputMethod(IBinder paramIBinder, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInputMethodAndSubtype(IBinder paramIBinder, String paramString, InputMethodSubtype paramInputMethodSubtype)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    if (paramInputMethodSubtype != null)
                    {
                        localParcel1.writeInt(1);
                        paramInputMethodSubtype.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(16, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setInputMethodEnabled(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeString(paramString);
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(27, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient paramIInputMethodClient, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    if (paramIInputMethodClient != null)
                    {
                        localIBinder = paramIInputMethodClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(14, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void showInputMethodPickerFromClient(IInputMethodClient paramIInputMethodClient)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    if (paramIInputMethodClient != null)
                    {
                        localIBinder = paramIInputMethodClient.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void showMySoftInput(IBinder paramIBinder, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean showSoftInput(IInputMethodClient paramIInputMethodClient, int paramInt, ResultReceiver paramResultReceiver)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                        IBinder localIBinder;
                        if (paramIInputMethodClient != null)
                        {
                            localIBinder = paramIInputMethodClient.asBinder();
                            localParcel1.writeStrongBinder(localIBinder);
                            localParcel1.writeInt(paramInt);
                            if (paramResultReceiver != null)
                            {
                                localParcel1.writeInt(1);
                                paramResultReceiver.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(10, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                int i = localParcel2.readInt();
                                if (i == 0)
                                    break label139;
                                return bool;
                            }
                        }
                        else
                        {
                            localIBinder = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label139: bool = false;
                }
            }

            public InputBindResult startInput(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext, EditorInfo paramEditorInfo, int paramInt)
                throws RemoteException
            {
                IBinder localIBinder1 = null;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                        IBinder localIBinder2;
                        if (paramIInputMethodClient != null)
                        {
                            localIBinder2 = paramIInputMethodClient.asBinder();
                            localParcel1.writeStrongBinder(localIBinder2);
                            if (paramIInputContext != null)
                                localIBinder1 = paramIInputContext.asBinder();
                            localParcel1.writeStrongBinder(localIBinder1);
                            if (paramEditorInfo != null)
                            {
                                localParcel1.writeInt(1);
                                paramEditorInfo.writeToParcel(localParcel1, 0);
                                localParcel1.writeInt(paramInt);
                                this.mRemote.transact(8, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                if (localParcel2.readInt() == 0)
                                    break label170;
                                localInputBindResult = (InputBindResult)InputBindResult.CREATOR.createFromParcel(localParcel2);
                                return localInputBindResult;
                            }
                        }
                        else
                        {
                            localIBinder2 = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label170: InputBindResult localInputBindResult = null;
                }
            }

            public boolean switchToLastInputMethod(IBinder paramIBinder)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean switchToNextInputMethod(IBinder paramIBinder, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(26, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateStatusIcon(IBinder paramIBinder, String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public InputBindResult windowGainedFocus(IInputMethodClient paramIInputMethodClient, IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, EditorInfo paramEditorInfo, IInputContext paramIInputContext)
                throws RemoteException
            {
                IBinder localIBinder1 = null;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.view.IInputMethodManager");
                        IBinder localIBinder2;
                        if (paramIInputMethodClient != null)
                        {
                            localIBinder2 = paramIInputMethodClient.asBinder();
                            localParcel1.writeStrongBinder(localIBinder2);
                            localParcel1.writeStrongBinder(paramIBinder);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            localParcel1.writeInt(paramInt3);
                            if (paramEditorInfo != null)
                            {
                                localParcel1.writeInt(1);
                                paramEditorInfo.writeToParcel(localParcel1, 0);
                                if (paramIInputContext != null)
                                    localIBinder1 = paramIInputContext.asBinder();
                                localParcel1.writeStrongBinder(localIBinder1);
                                this.mRemote.transact(12, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                if (localParcel2.readInt() == 0)
                                    break label193;
                                localInputBindResult = (InputBindResult)InputBindResult.CREATOR.createFromParcel(localParcel2);
                                return localInputBindResult;
                            }
                        }
                        else
                        {
                            localIBinder2 = null;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label193: InputBindResult localInputBindResult = null;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.IInputMethodManager
 * JD-Core Version:        0.6.2
 */