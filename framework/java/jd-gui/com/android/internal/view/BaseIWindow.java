package com.android.internal.view;

import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.view.DragEvent;
import android.view.IWindow.Stub;
import android.view.IWindowSession;

public class BaseIWindow extends IWindow.Stub
{
    public int mSeq;
    private IWindowSession mSession;

    public void closeSystemDialogs(String paramString)
    {
    }

    public void dispatchAppVisibility(boolean paramBoolean)
    {
    }

    public void dispatchDragEvent(DragEvent paramDragEvent)
    {
    }

    public void dispatchGetNewSurface()
    {
    }

    public void dispatchScreenState(boolean paramBoolean)
    {
    }

    public void dispatchSystemUiVisibilityChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mSeq = paramInt1;
    }

    public void dispatchWallpaperCommand(String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean)
    {
        if (paramBoolean);
        try
        {
            this.mSession.wallpaperCommandComplete(asBinder(), null);
            label19: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label19;
        }
    }

    public void dispatchWallpaperOffsets(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, boolean paramBoolean)
    {
        if (paramBoolean);
        try
        {
            this.mSession.wallpaperOffsetsComplete(asBinder());
            label18: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }

    public void doneAnimating()
    {
    }

    public void executeCommand(String paramString1, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor)
    {
    }

    public void resized(int paramInt1, int paramInt2, Rect paramRect1, Rect paramRect2, boolean paramBoolean, Configuration paramConfiguration)
    {
        if (paramBoolean);
        try
        {
            this.mSession.finishDrawing(this);
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    public void setSession(IWindowSession paramIWindowSession)
    {
        this.mSession = paramIWindowSession;
    }

    public void windowFocusChanged(boolean paramBoolean1, boolean paramBoolean2)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.BaseIWindow
 * JD-Core Version:        0.6.2
 */