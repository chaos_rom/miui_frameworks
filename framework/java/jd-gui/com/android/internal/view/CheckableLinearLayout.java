package com.android.internal.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class CheckableLinearLayout extends LinearLayout
    implements Checkable
{
    private CheckBox mCheckBox;

    public CheckableLinearLayout(Context paramContext)
    {
        super(paramContext);
    }

    public CheckableLinearLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
    }

    public CheckableLinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
    }

    public boolean isChecked()
    {
        return this.mCheckBox.isChecked();
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        this.mCheckBox = ((CheckBox)findViewById(16909023));
    }

    public void setChecked(boolean paramBoolean)
    {
        this.mCheckBox.setChecked(paramBoolean);
    }

    public void toggle()
    {
        this.mCheckBox.toggle();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.CheckableLinearLayout
 * JD-Core Version:        0.6.2
 */