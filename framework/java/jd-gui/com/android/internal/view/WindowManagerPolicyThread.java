package com.android.internal.view;

import android.os.Looper;

public class WindowManagerPolicyThread
{
    static Looper mLooper;
    static Thread mThread;

    public static Looper getLooper()
    {
        return mLooper;
    }

    public static Thread getThread()
    {
        return mThread;
    }

    public static void set(Thread paramThread, Looper paramLooper)
    {
        mThread = paramThread;
        mLooper = paramLooper;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.WindowManagerPolicyThread
 * JD-Core Version:        0.6.2
 */