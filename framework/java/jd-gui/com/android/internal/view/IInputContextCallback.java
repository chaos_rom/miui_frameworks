package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.inputmethod.ExtractedText;

public abstract interface IInputContextCallback extends IInterface
{
    public abstract void setCursorCapsMode(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setExtractedText(ExtractedText paramExtractedText, int paramInt)
        throws RemoteException;

    public abstract void setSelectedText(CharSequence paramCharSequence, int paramInt)
        throws RemoteException;

    public abstract void setTextAfterCursor(CharSequence paramCharSequence, int paramInt)
        throws RemoteException;

    public abstract void setTextBeforeCursor(CharSequence paramCharSequence, int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputContextCallback
    {
        private static final String DESCRIPTOR = "com.android.internal.view.IInputContextCallback";
        static final int TRANSACTION_setCursorCapsMode = 3;
        static final int TRANSACTION_setExtractedText = 4;
        static final int TRANSACTION_setSelectedText = 5;
        static final int TRANSACTION_setTextAfterCursor = 2;
        static final int TRANSACTION_setTextBeforeCursor = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.view.IInputContextCallback");
        }

        public static IInputContextCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.view.IInputContextCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IInputContextCallback)))
                    localObject = (IInputContextCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("com.android.internal.view.IInputContextCallback");
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.view.IInputContextCallback");
                    if (paramParcel1.readInt() != 0);
                    for (CharSequence localCharSequence3 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel1); ; localCharSequence3 = null)
                    {
                        setTextBeforeCursor(localCharSequence3, paramParcel1.readInt());
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.view.IInputContextCallback");
                    if (paramParcel1.readInt() != 0);
                    for (CharSequence localCharSequence2 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel1); ; localCharSequence2 = null)
                    {
                        setTextAfterCursor(localCharSequence2, paramParcel1.readInt());
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.view.IInputContextCallback");
                    setCursorCapsMode(paramParcel1.readInt(), paramParcel1.readInt());
                }
            case 4:
                paramParcel1.enforceInterface("com.android.internal.view.IInputContextCallback");
                if (paramParcel1.readInt() != 0);
                for (ExtractedText localExtractedText = (ExtractedText)ExtractedText.CREATOR.createFromParcel(paramParcel1); ; localExtractedText = null)
                {
                    setExtractedText(localExtractedText, paramParcel1.readInt());
                    break;
                }
            case 5:
            }
            paramParcel1.enforceInterface("com.android.internal.view.IInputContextCallback");
            if (paramParcel1.readInt() != 0);
            for (CharSequence localCharSequence1 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel1); ; localCharSequence1 = null)
            {
                setSelectedText(localCharSequence1, paramParcel1.readInt());
                break;
            }
        }

        private static class Proxy
            implements IInputContextCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.view.IInputContextCallback";
            }

            public void setCursorCapsMode(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContextCallback");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setExtractedText(ExtractedText paramExtractedText, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContextCallback");
                    if (paramExtractedText != null)
                    {
                        localParcel.writeInt(1);
                        paramExtractedText.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt);
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setSelectedText(CharSequence paramCharSequence, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContextCallback");
                    if (paramCharSequence != null)
                    {
                        localParcel.writeInt(1);
                        TextUtils.writeToParcel(paramCharSequence, localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt);
                        this.mRemote.transact(5, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setTextAfterCursor(CharSequence paramCharSequence, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContextCallback");
                    if (paramCharSequence != null)
                    {
                        localParcel.writeInt(1);
                        TextUtils.writeToParcel(paramCharSequence, localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt);
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setTextBeforeCursor(CharSequence paramCharSequence, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContextCallback");
                    if (paramCharSequence != null)
                    {
                        localParcel.writeInt(1);
                        TextUtils.writeToParcel(paramCharSequence, localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.IInputContextCallback
 * JD-Core Version:        0.6.2
 */