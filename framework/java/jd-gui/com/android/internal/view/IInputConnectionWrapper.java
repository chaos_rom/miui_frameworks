package com.android.internal.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import java.lang.ref.WeakReference;

public class IInputConnectionWrapper extends IInputContext.Stub
{
    private static final int DO_BEGIN_BATCH_EDIT = 90;
    private static final int DO_CLEAR_META_KEY_STATES = 130;
    private static final int DO_COMMIT_COMPLETION = 55;
    private static final int DO_COMMIT_CORRECTION = 56;
    private static final int DO_COMMIT_TEXT = 50;
    private static final int DO_DELETE_SURROUNDING_TEXT = 80;
    private static final int DO_END_BATCH_EDIT = 95;
    private static final int DO_FINISH_COMPOSING_TEXT = 65;
    private static final int DO_GET_CURSOR_CAPS_MODE = 30;
    private static final int DO_GET_EXTRACTED_TEXT = 40;
    private static final int DO_GET_SELECTED_TEXT = 25;
    private static final int DO_GET_TEXT_AFTER_CURSOR = 10;
    private static final int DO_GET_TEXT_BEFORE_CURSOR = 20;
    private static final int DO_PERFORM_CONTEXT_MENU_ACTION = 59;
    private static final int DO_PERFORM_EDITOR_ACTION = 58;
    private static final int DO_PERFORM_PRIVATE_COMMAND = 120;
    private static final int DO_REPORT_FULLSCREEN_MODE = 100;
    private static final int DO_SEND_KEY_EVENT = 70;
    private static final int DO_SET_COMPOSING_REGION = 63;
    private static final int DO_SET_COMPOSING_TEXT = 60;
    private static final int DO_SET_SELECTION = 57;
    static final String TAG = "IInputConnectionWrapper";
    private Handler mH;
    private WeakReference<InputConnection> mInputConnection;
    private Looper mMainLooper;

    public IInputConnectionWrapper(Looper paramLooper, InputConnection paramInputConnection)
    {
        this.mInputConnection = new WeakReference(paramInputConnection);
        this.mMainLooper = paramLooper;
        this.mH = new MyHandler(this.mMainLooper);
    }

    public void beginBatchEdit()
    {
        dispatchMessage(obtainMessage(90));
    }

    public void clearMetaKeyStates(int paramInt)
    {
        dispatchMessage(obtainMessageII(130, paramInt, 0));
    }

    public void commitCompletion(CompletionInfo paramCompletionInfo)
    {
        dispatchMessage(obtainMessageO(55, paramCompletionInfo));
    }

    public void commitCorrection(CorrectionInfo paramCorrectionInfo)
    {
        dispatchMessage(obtainMessageO(56, paramCorrectionInfo));
    }

    public void commitText(CharSequence paramCharSequence, int paramInt)
    {
        dispatchMessage(obtainMessageIO(50, paramInt, paramCharSequence));
    }

    public void deleteSurroundingText(int paramInt1, int paramInt2)
    {
        dispatchMessage(obtainMessageII(80, paramInt1, paramInt2));
    }

    void dispatchMessage(Message paramMessage)
    {
        if (Looper.myLooper() == this.mMainLooper)
        {
            executeMessage(paramMessage);
            paramMessage.recycle();
        }
        while (true)
        {
            return;
            this.mH.sendMessage(paramMessage);
        }
    }

    public void endBatchEdit()
    {
        dispatchMessage(obtainMessage(95));
    }

    void executeMessage(Message paramMessage)
    {
        int i = 1;
        switch (paramMessage.what)
        {
        default:
            Log.w("IInputConnectionWrapper", "Unhandled message code: " + paramMessage.what);
        case 10:
        case 20:
        case 25:
        case 30:
        case 40:
        case 50:
        case 57:
        case 58:
        case 59:
        case 55:
        case 56:
        case 60:
        case 63:
        case 65:
        case 70:
        case 130:
        case 80:
        case 90:
        case 95:
        case 100:
        case 120:
        }
        while (true)
        {
            return;
            SomeArgs localSomeArgs6 = (SomeArgs)paramMessage.obj;
            InputConnection localInputConnection21;
            try
            {
                localInputConnection21 = (InputConnection)this.mInputConnection.get();
                if ((localInputConnection21 != null) && (isActive()))
                    break label288;
                Log.w("IInputConnectionWrapper", "getTextAfterCursor on inactive InputConnection");
                localSomeArgs6.callback.setTextAfterCursor(null, localSomeArgs6.seq);
            }
            catch (RemoteException localRemoteException5)
            {
                Log.w("IInputConnectionWrapper", "Got RemoteException calling setTextAfterCursor", localRemoteException5);
            }
            continue;
            label288: localSomeArgs6.callback.setTextAfterCursor(localInputConnection21.getTextAfterCursor(paramMessage.arg1, paramMessage.arg2), localSomeArgs6.seq);
            continue;
            SomeArgs localSomeArgs5 = (SomeArgs)paramMessage.obj;
            InputConnection localInputConnection20;
            try
            {
                localInputConnection20 = (InputConnection)this.mInputConnection.get();
                if ((localInputConnection20 != null) && (isActive()))
                    break label396;
                Log.w("IInputConnectionWrapper", "getTextBeforeCursor on inactive InputConnection");
                localSomeArgs5.callback.setTextBeforeCursor(null, localSomeArgs5.seq);
            }
            catch (RemoteException localRemoteException4)
            {
                Log.w("IInputConnectionWrapper", "Got RemoteException calling setTextBeforeCursor", localRemoteException4);
            }
            continue;
            label396: localSomeArgs5.callback.setTextBeforeCursor(localInputConnection20.getTextBeforeCursor(paramMessage.arg1, paramMessage.arg2), localSomeArgs5.seq);
            continue;
            SomeArgs localSomeArgs4 = (SomeArgs)paramMessage.obj;
            InputConnection localInputConnection19;
            try
            {
                localInputConnection19 = (InputConnection)this.mInputConnection.get();
                if ((localInputConnection19 != null) && (isActive()))
                    break label504;
                Log.w("IInputConnectionWrapper", "getSelectedText on inactive InputConnection");
                localSomeArgs4.callback.setSelectedText(null, localSomeArgs4.seq);
            }
            catch (RemoteException localRemoteException3)
            {
                Log.w("IInputConnectionWrapper", "Got RemoteException calling setSelectedText", localRemoteException3);
            }
            continue;
            label504: localSomeArgs4.callback.setSelectedText(localInputConnection19.getSelectedText(paramMessage.arg1), localSomeArgs4.seq);
            continue;
            SomeArgs localSomeArgs3 = (SomeArgs)paramMessage.obj;
            InputConnection localInputConnection18;
            try
            {
                localInputConnection18 = (InputConnection)this.mInputConnection.get();
                if ((localInputConnection18 != null) && (isActive()))
                    break label608;
                Log.w("IInputConnectionWrapper", "getCursorCapsMode on inactive InputConnection");
                localSomeArgs3.callback.setCursorCapsMode(0, localSomeArgs3.seq);
            }
            catch (RemoteException localRemoteException2)
            {
                Log.w("IInputConnectionWrapper", "Got RemoteException calling setCursorCapsMode", localRemoteException2);
            }
            continue;
            label608: localSomeArgs3.callback.setCursorCapsMode(localInputConnection18.getCursorCapsMode(paramMessage.arg1), localSomeArgs3.seq);
            continue;
            SomeArgs localSomeArgs2 = (SomeArgs)paramMessage.obj;
            InputConnection localInputConnection17;
            try
            {
                localInputConnection17 = (InputConnection)this.mInputConnection.get();
                if ((localInputConnection17 != null) && (isActive()))
                    break label712;
                Log.w("IInputConnectionWrapper", "getExtractedText on inactive InputConnection");
                localSomeArgs2.callback.setExtractedText(null, localSomeArgs2.seq);
            }
            catch (RemoteException localRemoteException1)
            {
                Log.w("IInputConnectionWrapper", "Got RemoteException calling setExtractedText", localRemoteException1);
            }
            continue;
            label712: localSomeArgs2.callback.setExtractedText(localInputConnection17.getExtractedText((ExtractedTextRequest)localSomeArgs2.arg1, paramMessage.arg1), localSomeArgs2.seq);
            continue;
            InputConnection localInputConnection16 = (InputConnection)this.mInputConnection.get();
            if ((localInputConnection16 == null) || (!isActive()))
            {
                Log.w("IInputConnectionWrapper", "commitText on inactive InputConnection");
            }
            else
            {
                localInputConnection16.commitText((CharSequence)paramMessage.obj, paramMessage.arg1);
                continue;
                InputConnection localInputConnection15 = (InputConnection)this.mInputConnection.get();
                if ((localInputConnection15 == null) || (!isActive()))
                {
                    Log.w("IInputConnectionWrapper", "setSelection on inactive InputConnection");
                }
                else
                {
                    localInputConnection15.setSelection(paramMessage.arg1, paramMessage.arg2);
                    continue;
                    InputConnection localInputConnection14 = (InputConnection)this.mInputConnection.get();
                    if ((localInputConnection14 == null) || (!isActive()))
                    {
                        Log.w("IInputConnectionWrapper", "performEditorAction on inactive InputConnection");
                    }
                    else
                    {
                        localInputConnection14.performEditorAction(paramMessage.arg1);
                        continue;
                        InputConnection localInputConnection13 = (InputConnection)this.mInputConnection.get();
                        if ((localInputConnection13 == null) || (!isActive()))
                        {
                            Log.w("IInputConnectionWrapper", "performContextMenuAction on inactive InputConnection");
                        }
                        else
                        {
                            localInputConnection13.performContextMenuAction(paramMessage.arg1);
                            continue;
                            InputConnection localInputConnection12 = (InputConnection)this.mInputConnection.get();
                            if ((localInputConnection12 == null) || (!isActive()))
                            {
                                Log.w("IInputConnectionWrapper", "commitCompletion on inactive InputConnection");
                            }
                            else
                            {
                                localInputConnection12.commitCompletion((CompletionInfo)paramMessage.obj);
                                continue;
                                InputConnection localInputConnection11 = (InputConnection)this.mInputConnection.get();
                                if ((localInputConnection11 == null) || (!isActive()))
                                {
                                    Log.w("IInputConnectionWrapper", "commitCorrection on inactive InputConnection");
                                }
                                else
                                {
                                    localInputConnection11.commitCorrection((CorrectionInfo)paramMessage.obj);
                                    continue;
                                    InputConnection localInputConnection10 = (InputConnection)this.mInputConnection.get();
                                    if ((localInputConnection10 == null) || (!isActive()))
                                    {
                                        Log.w("IInputConnectionWrapper", "setComposingText on inactive InputConnection");
                                    }
                                    else
                                    {
                                        localInputConnection10.setComposingText((CharSequence)paramMessage.obj, paramMessage.arg1);
                                        continue;
                                        InputConnection localInputConnection9 = (InputConnection)this.mInputConnection.get();
                                        if ((localInputConnection9 == null) || (!isActive()))
                                        {
                                            Log.w("IInputConnectionWrapper", "setComposingRegion on inactive InputConnection");
                                        }
                                        else
                                        {
                                            localInputConnection9.setComposingRegion(paramMessage.arg1, paramMessage.arg2);
                                            continue;
                                            InputConnection localInputConnection8 = (InputConnection)this.mInputConnection.get();
                                            if (localInputConnection8 == null)
                                            {
                                                Log.w("IInputConnectionWrapper", "finishComposingText on inactive InputConnection");
                                            }
                                            else
                                            {
                                                localInputConnection8.finishComposingText();
                                                continue;
                                                InputConnection localInputConnection7 = (InputConnection)this.mInputConnection.get();
                                                if ((localInputConnection7 == null) || (!isActive()))
                                                {
                                                    Log.w("IInputConnectionWrapper", "sendKeyEvent on inactive InputConnection");
                                                }
                                                else
                                                {
                                                    localInputConnection7.sendKeyEvent((KeyEvent)paramMessage.obj);
                                                    continue;
                                                    InputConnection localInputConnection6 = (InputConnection)this.mInputConnection.get();
                                                    if ((localInputConnection6 == null) || (!isActive()))
                                                    {
                                                        Log.w("IInputConnectionWrapper", "clearMetaKeyStates on inactive InputConnection");
                                                    }
                                                    else
                                                    {
                                                        localInputConnection6.clearMetaKeyStates(paramMessage.arg1);
                                                        continue;
                                                        InputConnection localInputConnection5 = (InputConnection)this.mInputConnection.get();
                                                        if ((localInputConnection5 == null) || (!isActive()))
                                                        {
                                                            Log.w("IInputConnectionWrapper", "deleteSurroundingText on inactive InputConnection");
                                                        }
                                                        else
                                                        {
                                                            localInputConnection5.deleteSurroundingText(paramMessage.arg1, paramMessage.arg2);
                                                            continue;
                                                            InputConnection localInputConnection4 = (InputConnection)this.mInputConnection.get();
                                                            if ((localInputConnection4 == null) || (!isActive()))
                                                            {
                                                                Log.w("IInputConnectionWrapper", "beginBatchEdit on inactive InputConnection");
                                                            }
                                                            else
                                                            {
                                                                localInputConnection4.beginBatchEdit();
                                                                continue;
                                                                InputConnection localInputConnection3 = (InputConnection)this.mInputConnection.get();
                                                                if ((localInputConnection3 == null) || (!isActive()))
                                                                {
                                                                    Log.w("IInputConnectionWrapper", "endBatchEdit on inactive InputConnection");
                                                                }
                                                                else
                                                                {
                                                                    localInputConnection3.endBatchEdit();
                                                                    continue;
                                                                    InputConnection localInputConnection2 = (InputConnection)this.mInputConnection.get();
                                                                    if ((localInputConnection2 == null) || (!isActive()))
                                                                    {
                                                                        Log.w("IInputConnectionWrapper", "showStatusIcon on inactive InputConnection");
                                                                    }
                                                                    else
                                                                    {
                                                                        if (paramMessage.arg1 == i);
                                                                        while (true)
                                                                        {
                                                                            localInputConnection2.reportFullscreenMode(i);
                                                                            break;
                                                                            int j = 0;
                                                                        }
                                                                        InputConnection localInputConnection1 = (InputConnection)this.mInputConnection.get();
                                                                        if ((localInputConnection1 == null) || (!isActive()))
                                                                        {
                                                                            Log.w("IInputConnectionWrapper", "performPrivateCommand on inactive InputConnection");
                                                                        }
                                                                        else
                                                                        {
                                                                            SomeArgs localSomeArgs1 = (SomeArgs)paramMessage.obj;
                                                                            localInputConnection1.performPrivateCommand((String)localSomeArgs1.arg1, (Bundle)localSomeArgs1.arg2);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void finishComposingText()
    {
        dispatchMessage(obtainMessage(65));
    }

    public void getCursorCapsMode(int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
    {
        dispatchMessage(obtainMessageISC(30, paramInt1, paramInt2, paramIInputContextCallback));
    }

    public void getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
    {
        dispatchMessage(obtainMessageIOSC(40, paramInt1, paramExtractedTextRequest, paramInt2, paramIInputContextCallback));
    }

    public void getSelectedText(int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
    {
        dispatchMessage(obtainMessageISC(25, paramInt1, paramInt2, paramIInputContextCallback));
    }

    public void getTextAfterCursor(int paramInt1, int paramInt2, int paramInt3, IInputContextCallback paramIInputContextCallback)
    {
        dispatchMessage(obtainMessageIISC(10, paramInt1, paramInt2, paramInt3, paramIInputContextCallback));
    }

    public void getTextBeforeCursor(int paramInt1, int paramInt2, int paramInt3, IInputContextCallback paramIInputContextCallback)
    {
        dispatchMessage(obtainMessageIISC(20, paramInt1, paramInt2, paramInt3, paramIInputContextCallback));
    }

    public boolean isActive()
    {
        return true;
    }

    Message obtainMessage(int paramInt)
    {
        return this.mH.obtainMessage(paramInt);
    }

    Message obtainMessageII(int paramInt1, int paramInt2, int paramInt3)
    {
        return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3);
    }

    Message obtainMessageIISC(int paramInt1, int paramInt2, int paramInt3, int paramInt4, IInputContextCallback paramIInputContextCallback)
    {
        SomeArgs localSomeArgs = new SomeArgs();
        localSomeArgs.callback = paramIInputContextCallback;
        localSomeArgs.seq = paramInt4;
        return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3, localSomeArgs);
    }

    Message obtainMessageIO(int paramInt1, int paramInt2, Object paramObject)
    {
        return this.mH.obtainMessage(paramInt1, paramInt2, 0, paramObject);
    }

    Message obtainMessageIOSC(int paramInt1, int paramInt2, Object paramObject, int paramInt3, IInputContextCallback paramIInputContextCallback)
    {
        SomeArgs localSomeArgs = new SomeArgs();
        localSomeArgs.arg1 = paramObject;
        localSomeArgs.callback = paramIInputContextCallback;
        localSomeArgs.seq = paramInt3;
        return this.mH.obtainMessage(paramInt1, paramInt2, 0, localSomeArgs);
    }

    Message obtainMessageISC(int paramInt1, int paramInt2, int paramInt3, IInputContextCallback paramIInputContextCallback)
    {
        SomeArgs localSomeArgs = new SomeArgs();
        localSomeArgs.callback = paramIInputContextCallback;
        localSomeArgs.seq = paramInt3;
        return this.mH.obtainMessage(paramInt1, paramInt2, 0, localSomeArgs);
    }

    Message obtainMessageO(int paramInt, Object paramObject)
    {
        return this.mH.obtainMessage(paramInt, 0, 0, paramObject);
    }

    Message obtainMessageOO(int paramInt, Object paramObject1, Object paramObject2)
    {
        SomeArgs localSomeArgs = new SomeArgs();
        localSomeArgs.arg1 = paramObject1;
        localSomeArgs.arg2 = paramObject2;
        return this.mH.obtainMessage(paramInt, 0, 0, localSomeArgs);
    }

    public void performContextMenuAction(int paramInt)
    {
        dispatchMessage(obtainMessageII(59, paramInt, 0));
    }

    public void performEditorAction(int paramInt)
    {
        dispatchMessage(obtainMessageII(58, paramInt, 0));
    }

    public void performPrivateCommand(String paramString, Bundle paramBundle)
    {
        dispatchMessage(obtainMessageOO(120, paramString, paramBundle));
    }

    public void reportFullscreenMode(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            dispatchMessage(obtainMessageII(100, i, 0));
            return;
        }
    }

    public void sendKeyEvent(KeyEvent paramKeyEvent)
    {
        dispatchMessage(obtainMessageO(70, paramKeyEvent));
    }

    public void setComposingRegion(int paramInt1, int paramInt2)
    {
        dispatchMessage(obtainMessageII(63, paramInt1, paramInt2));
    }

    public void setComposingText(CharSequence paramCharSequence, int paramInt)
    {
        dispatchMessage(obtainMessageIO(60, paramInt, paramCharSequence));
    }

    public void setSelection(int paramInt1, int paramInt2)
    {
        dispatchMessage(obtainMessageII(57, paramInt1, paramInt2));
    }

    class MyHandler extends Handler
    {
        MyHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            IInputConnectionWrapper.this.executeMessage(paramMessage);
        }
    }

    static class SomeArgs
    {
        Object arg1;
        Object arg2;
        IInputContextCallback callback;
        int seq;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.IInputConnectionWrapper
 * JD-Core Version:        0.6.2
 */