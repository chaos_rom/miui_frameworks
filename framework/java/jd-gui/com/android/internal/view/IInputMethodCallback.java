package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IInputMethodCallback extends IInterface
{
    public abstract void finishedEvent(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract void sessionCreated(IInputMethodSession paramIInputMethodSession)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputMethodCallback
    {
        private static final String DESCRIPTOR = "com.android.internal.view.IInputMethodCallback";
        static final int TRANSACTION_finishedEvent = 1;
        static final int TRANSACTION_sessionCreated = 2;

        public Stub()
        {
            attachInterface(this, "com.android.internal.view.IInputMethodCallback");
        }

        public static IInputMethodCallback asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.view.IInputMethodCallback");
                if ((localIInterface != null) && ((localIInterface instanceof IInputMethodCallback)))
                    localObject = (IInputMethodCallback)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.view.IInputMethodCallback");
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodCallback");
                int i = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    finishedEvent(i, bool2);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodCallback");
                sessionCreated(IInputMethodSession.Stub.asInterface(paramParcel1.readStrongBinder()));
            }
        }

        private static class Proxy
            implements IInputMethodCallback
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void finishedEvent(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodCallback");
                    localParcel.writeInt(paramInt);
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.view.IInputMethodCallback";
            }

            public void sessionCreated(IInputMethodSession paramIInputMethodSession)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodCallback");
                    if (paramIInputMethodSession != null)
                        localIBinder = paramIInputMethodSession.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.IInputMethodCallback
 * JD-Core Version:        0.6.2
 */