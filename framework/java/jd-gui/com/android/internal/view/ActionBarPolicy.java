package com.android.internal.view;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.DisplayMetrics;
import android.view.ViewConfiguration;
import com.android.internal.R.styleable;

public class ActionBarPolicy
{
    private Context mContext;

    private ActionBarPolicy(Context paramContext)
    {
        this.mContext = paramContext;
    }

    public static ActionBarPolicy get(Context paramContext)
    {
        return new ActionBarPolicy(paramContext);
    }

    public boolean enableHomeButtonByDefault()
    {
        if (this.mContext.getApplicationInfo().targetSdkVersion < 14);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int getEmbeddedMenuWidthLimit()
    {
        return this.mContext.getResources().getDisplayMetrics().widthPixels / 2;
    }

    public int getMaxActionButtons()
    {
        return this.mContext.getResources().getInteger(17694771);
    }

    public int getStackedTabMaxWidth()
    {
        return this.mContext.getResources().getDimensionPixelSize(17104977);
    }

    public int getTabContainerHeight()
    {
        TypedArray localTypedArray = this.mContext.obtainStyledAttributes(null, R.styleable.ActionBar, 16843470, 0);
        int i = localTypedArray.getLayoutDimension(4, 0);
        Resources localResources = this.mContext.getResources();
        if (!hasEmbeddedTabs())
            i = Math.min(i, localResources.getDimensionPixelSize(17104976));
        localTypedArray.recycle();
        return i;
    }

    public boolean hasEmbeddedTabs()
    {
        if (this.mContext.getApplicationInfo().targetSdkVersion >= 16);
        for (boolean bool = this.mContext.getResources().getBoolean(17891328); ; bool = this.mContext.getResources().getBoolean(17891329))
            return bool;
    }

    public boolean showsOverflowMenuButton()
    {
        if (!ViewConfiguration.get(this.mContext).hasPermanentMenuKey());
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.ActionBarPolicy
 * JD-Core Version:        0.6.2
 */