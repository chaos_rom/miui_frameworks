package com.android.internal.view;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedTextRequest;

public abstract interface IInputContext extends IInterface
{
    public abstract void beginBatchEdit()
        throws RemoteException;

    public abstract void clearMetaKeyStates(int paramInt)
        throws RemoteException;

    public abstract void commitCompletion(CompletionInfo paramCompletionInfo)
        throws RemoteException;

    public abstract void commitCorrection(CorrectionInfo paramCorrectionInfo)
        throws RemoteException;

    public abstract void commitText(CharSequence paramCharSequence, int paramInt)
        throws RemoteException;

    public abstract void deleteSurroundingText(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void endBatchEdit()
        throws RemoteException;

    public abstract void finishComposingText()
        throws RemoteException;

    public abstract void getCursorCapsMode(int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
        throws RemoteException;

    public abstract void getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
        throws RemoteException;

    public abstract void getSelectedText(int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
        throws RemoteException;

    public abstract void getTextAfterCursor(int paramInt1, int paramInt2, int paramInt3, IInputContextCallback paramIInputContextCallback)
        throws RemoteException;

    public abstract void getTextBeforeCursor(int paramInt1, int paramInt2, int paramInt3, IInputContextCallback paramIInputContextCallback)
        throws RemoteException;

    public abstract void performContextMenuAction(int paramInt)
        throws RemoteException;

    public abstract void performEditorAction(int paramInt)
        throws RemoteException;

    public abstract void performPrivateCommand(String paramString, Bundle paramBundle)
        throws RemoteException;

    public abstract void reportFullscreenMode(boolean paramBoolean)
        throws RemoteException;

    public abstract void sendKeyEvent(KeyEvent paramKeyEvent)
        throws RemoteException;

    public abstract void setComposingRegion(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setComposingText(CharSequence paramCharSequence, int paramInt)
        throws RemoteException;

    public abstract void setSelection(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputContext
    {
        private static final String DESCRIPTOR = "com.android.internal.view.IInputContext";
        static final int TRANSACTION_beginBatchEdit = 14;
        static final int TRANSACTION_clearMetaKeyStates = 18;
        static final int TRANSACTION_commitCompletion = 9;
        static final int TRANSACTION_commitCorrection = 10;
        static final int TRANSACTION_commitText = 8;
        static final int TRANSACTION_deleteSurroundingText = 5;
        static final int TRANSACTION_endBatchEdit = 15;
        static final int TRANSACTION_finishComposingText = 7;
        static final int TRANSACTION_getCursorCapsMode = 3;
        static final int TRANSACTION_getExtractedText = 4;
        static final int TRANSACTION_getSelectedText = 21;
        static final int TRANSACTION_getTextAfterCursor = 2;
        static final int TRANSACTION_getTextBeforeCursor = 1;
        static final int TRANSACTION_performContextMenuAction = 13;
        static final int TRANSACTION_performEditorAction = 12;
        static final int TRANSACTION_performPrivateCommand = 19;
        static final int TRANSACTION_reportFullscreenMode = 16;
        static final int TRANSACTION_sendKeyEvent = 17;
        static final int TRANSACTION_setComposingRegion = 20;
        static final int TRANSACTION_setComposingText = 6;
        static final int TRANSACTION_setSelection = 11;

        public Stub()
        {
            attachInterface(this, "com.android.internal.view.IInputContext");
        }

        public static IInputContext asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.view.IInputContext");
                if ((localIInterface != null) && ((localIInterface instanceof IInputContext)))
                    localObject = (IInputContext)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.view.IInputContext");
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                getTextBeforeCursor(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), IInputContextCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                getTextAfterCursor(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), IInputContextCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                getCursorCapsMode(paramParcel1.readInt(), paramParcel1.readInt(), IInputContextCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                if (paramParcel1.readInt() != 0);
                for (ExtractedTextRequest localExtractedTextRequest = (ExtractedTextRequest)ExtractedTextRequest.CREATOR.createFromParcel(paramParcel1); ; localExtractedTextRequest = null)
                {
                    getExtractedText(localExtractedTextRequest, paramParcel1.readInt(), paramParcel1.readInt(), IInputContextCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                deleteSurroundingText(paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                if (paramParcel1.readInt() != 0);
                for (CharSequence localCharSequence2 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel1); ; localCharSequence2 = null)
                {
                    setComposingText(localCharSequence2, paramParcel1.readInt());
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                finishComposingText();
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                if (paramParcel1.readInt() != 0);
                for (CharSequence localCharSequence1 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel1); ; localCharSequence1 = null)
                {
                    commitText(localCharSequence1, paramParcel1.readInt());
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                if (paramParcel1.readInt() != 0);
                for (CompletionInfo localCompletionInfo = (CompletionInfo)CompletionInfo.CREATOR.createFromParcel(paramParcel1); ; localCompletionInfo = null)
                {
                    commitCompletion(localCompletionInfo);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                if (paramParcel1.readInt() != 0);
                for (CorrectionInfo localCorrectionInfo = (CorrectionInfo)CorrectionInfo.CREATOR.createFromParcel(paramParcel1); ; localCorrectionInfo = null)
                {
                    commitCorrection(localCorrectionInfo);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                setSelection(paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                performEditorAction(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                performContextMenuAction(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                beginBatchEdit();
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                endBatchEdit();
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    reportFullscreenMode(bool2);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                if (paramParcel1.readInt() != 0);
                for (KeyEvent localKeyEvent = (KeyEvent)KeyEvent.CREATOR.createFromParcel(paramParcel1); ; localKeyEvent = null)
                {
                    sendKeyEvent(localKeyEvent);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                clearMetaKeyStates(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                String str = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                {
                    performPrivateCommand(str, localBundle);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                setComposingRegion(paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputContext");
                getSelectedText(paramParcel1.readInt(), paramParcel1.readInt(), IInputContextCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
            }
        }

        private static class Proxy
            implements IInputContext
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void beginBatchEdit()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    this.mRemote.transact(14, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void clearMetaKeyStates(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(18, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void commitCompletion(CompletionInfo paramCompletionInfo)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    if (paramCompletionInfo != null)
                    {
                        localParcel.writeInt(1);
                        paramCompletionInfo.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void commitCorrection(CorrectionInfo paramCorrectionInfo)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    if (paramCorrectionInfo != null)
                    {
                        localParcel.writeInt(1);
                        paramCorrectionInfo.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(10, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void commitText(CharSequence paramCharSequence, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    if (paramCharSequence != null)
                    {
                        localParcel.writeInt(1);
                        TextUtils.writeToParcel(paramCharSequence, localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt);
                        this.mRemote.transact(8, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void deleteSurroundingText(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(5, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void endBatchEdit()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    this.mRemote.transact(15, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void finishComposingText()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    this.mRemote.transact(7, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void getCursorCapsMode(int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    if (paramIInputContextCallback != null)
                        localIBinder = paramIInputContextCallback.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    if (paramExtractedTextRequest != null)
                    {
                        localParcel.writeInt(1);
                        paramExtractedTextRequest.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt1);
                        localParcel.writeInt(paramInt2);
                        if (paramIInputContextCallback != null)
                            localIBinder = paramIInputContextCallback.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.view.IInputContext";
            }

            public void getSelectedText(int paramInt1, int paramInt2, IInputContextCallback paramIInputContextCallback)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    if (paramIInputContextCallback != null)
                        localIBinder = paramIInputContextCallback.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(21, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void getTextAfterCursor(int paramInt1, int paramInt2, int paramInt3, IInputContextCallback paramIInputContextCallback)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    if (paramIInputContextCallback != null)
                        localIBinder = paramIInputContextCallback.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void getTextBeforeCursor(int paramInt1, int paramInt2, int paramInt3, IInputContextCallback paramIInputContextCallback)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    localParcel.writeInt(paramInt3);
                    if (paramIInputContextCallback != null)
                        localIBinder = paramIInputContextCallback.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void performContextMenuAction(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(13, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void performEditorAction(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(12, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void performPrivateCommand(String paramString, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeString(paramString);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(19, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void reportFullscreenMode(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(16, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void sendKeyEvent(KeyEvent paramKeyEvent)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    if (paramKeyEvent != null)
                    {
                        localParcel.writeInt(1);
                        paramKeyEvent.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(17, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setComposingRegion(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(20, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setComposingText(CharSequence paramCharSequence, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    if (paramCharSequence != null)
                    {
                        localParcel.writeInt(1);
                        TextUtils.writeToParcel(paramCharSequence, localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeInt(paramInt);
                        this.mRemote.transact(6, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setSelection(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputContext");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(11, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.IInputContext
 * JD-Core Version:        0.6.2
 */