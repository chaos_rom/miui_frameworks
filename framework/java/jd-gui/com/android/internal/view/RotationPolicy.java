package com.android.internal.view;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings.System;
import android.util.Log;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;

public final class RotationPolicy
{
    private static final String TAG = "RotationPolicy";

    public static boolean isRotationLockToggleSupported(Context paramContext)
    {
        if (paramContext.getResources().getConfiguration().smallestScreenWidthDp >= 600);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isRotationLockToggleVisible(Context paramContext)
    {
        boolean bool = false;
        if ((isRotationLockToggleSupported(paramContext)) && (Settings.System.getInt(paramContext.getContentResolver(), "hide_rotation_lock_toggle_for_accessibility", 0) == 0))
            bool = true;
        return bool;
    }

    public static boolean isRotationLocked(Context paramContext)
    {
        boolean bool = false;
        if (Settings.System.getInt(paramContext.getContentResolver(), "accelerometer_rotation", 0) == 0)
            bool = true;
        return bool;
    }

    public static void registerRotationPolicyListener(Context paramContext, RotationPolicyListener paramRotationPolicyListener)
    {
        paramContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("accelerometer_rotation"), false, paramRotationPolicyListener.mObserver);
        paramContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("hide_rotation_lock_toggle_for_accessibility"), false, paramRotationPolicyListener.mObserver);
    }

    public static void setRotationLock(Context paramContext, boolean paramBoolean)
    {
        Settings.System.putInt(paramContext.getContentResolver(), "hide_rotation_lock_toggle_for_accessibility", 0);
        AsyncTask.execute(new Runnable()
        {
            public void run()
            {
                try
                {
                    IWindowManager localIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
                    if (this.val$enabled)
                        localIWindowManager.freezeRotation(-1);
                    else
                        localIWindowManager.thawRotation();
                }
                catch (RemoteException localRemoteException)
                {
                    Log.w("RotationPolicy", "Unable to save auto-rotate setting");
                }
            }
        });
    }

    public static void setRotationLockForAccessibility(Context paramContext, boolean paramBoolean)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Settings.System.putInt(localContentResolver, "hide_rotation_lock_toggle_for_accessibility", i);
            AsyncTask.execute(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        IWindowManager localIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
                        if (this.val$enabled)
                            localIWindowManager.freezeRotation(0);
                        else
                            localIWindowManager.thawRotation();
                    }
                    catch (RemoteException localRemoteException)
                    {
                        Log.w("RotationPolicy", "Unable to save auto-rotate setting");
                    }
                }
            });
            return;
        }
    }

    public static void unregisterRotationPolicyListener(Context paramContext, RotationPolicyListener paramRotationPolicyListener)
    {
        paramContext.getContentResolver().unregisterContentObserver(paramRotationPolicyListener.mObserver);
    }

    public static abstract class RotationPolicyListener
    {
        final ContentObserver mObserver = new ContentObserver(new Handler())
        {
            public void onChange(boolean paramAnonymousBoolean, Uri paramAnonymousUri)
            {
                RotationPolicy.RotationPolicyListener.this.onChange();
            }
        };

        public abstract void onChange();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.RotationPolicy
 * JD-Core Version:        0.6.2
 */