package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputMethodSubtype;

public abstract interface IInputMethod extends IInterface
{
    public abstract void attachToken(IBinder paramIBinder)
        throws RemoteException;

    public abstract void bindInput(InputBinding paramInputBinding)
        throws RemoteException;

    public abstract void changeInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype)
        throws RemoteException;

    public abstract void createSession(IInputMethodCallback paramIInputMethodCallback)
        throws RemoteException;

    public abstract void hideSoftInput(int paramInt, ResultReceiver paramResultReceiver)
        throws RemoteException;

    public abstract void restartInput(IInputContext paramIInputContext, EditorInfo paramEditorInfo)
        throws RemoteException;

    public abstract void revokeSession(IInputMethodSession paramIInputMethodSession)
        throws RemoteException;

    public abstract void setSessionEnabled(IInputMethodSession paramIInputMethodSession, boolean paramBoolean)
        throws RemoteException;

    public abstract void showSoftInput(int paramInt, ResultReceiver paramResultReceiver)
        throws RemoteException;

    public abstract void startInput(IInputContext paramIInputContext, EditorInfo paramEditorInfo)
        throws RemoteException;

    public abstract void unbindInput()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputMethod
    {
        private static final String DESCRIPTOR = "com.android.internal.view.IInputMethod";
        static final int TRANSACTION_attachToken = 1;
        static final int TRANSACTION_bindInput = 2;
        static final int TRANSACTION_changeInputMethodSubtype = 11;
        static final int TRANSACTION_createSession = 6;
        static final int TRANSACTION_hideSoftInput = 10;
        static final int TRANSACTION_restartInput = 5;
        static final int TRANSACTION_revokeSession = 8;
        static final int TRANSACTION_setSessionEnabled = 7;
        static final int TRANSACTION_showSoftInput = 9;
        static final int TRANSACTION_startInput = 4;
        static final int TRANSACTION_unbindInput = 3;

        public Stub()
        {
            attachInterface(this, "com.android.internal.view.IInputMethod");
        }

        public static IInputMethod asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.view.IInputMethod");
                if ((localIInterface != null) && ((localIInterface instanceof IInputMethod)))
                    localObject = (IInputMethod)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("com.android.internal.view.IInputMethod");
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                    attachToken(paramParcel1.readStrongBinder());
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                    if (paramParcel1.readInt() != 0);
                    for (InputBinding localInputBinding = (InputBinding)InputBinding.CREATOR.createFromParcel(paramParcel1); ; localInputBinding = null)
                    {
                        bindInput(localInputBinding);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                    unbindInput();
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                    IInputContext localIInputContext2 = IInputContext.Stub.asInterface(paramParcel1.readStrongBinder());
                    if (paramParcel1.readInt() != 0);
                    for (EditorInfo localEditorInfo2 = (EditorInfo)EditorInfo.CREATOR.createFromParcel(paramParcel1); ; localEditorInfo2 = null)
                    {
                        startInput(localIInputContext2, localEditorInfo2);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                    IInputContext localIInputContext1 = IInputContext.Stub.asInterface(paramParcel1.readStrongBinder());
                    if (paramParcel1.readInt() != 0);
                    for (EditorInfo localEditorInfo1 = (EditorInfo)EditorInfo.CREATOR.createFromParcel(paramParcel1); ; localEditorInfo1 = null)
                    {
                        restartInput(localIInputContext1, localEditorInfo1);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                    createSession(IInputMethodCallback.Stub.asInterface(paramParcel1.readStrongBinder()));
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                    IInputMethodSession localIInputMethodSession = IInputMethodSession.Stub.asInterface(paramParcel1.readStrongBinder());
                    if (paramParcel1.readInt() != 0);
                    for (boolean bool2 = bool1; ; bool2 = false)
                    {
                        setSessionEnabled(localIInputMethodSession, bool2);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                    revokeSession(IInputMethodSession.Stub.asInterface(paramParcel1.readStrongBinder()));
                }
            case 9:
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                int j = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (ResultReceiver localResultReceiver2 = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(paramParcel1); ; localResultReceiver2 = null)
                {
                    showSoftInput(j, localResultReceiver2);
                    break;
                }
            case 10:
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
                int i = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (ResultReceiver localResultReceiver1 = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(paramParcel1); ; localResultReceiver1 = null)
                {
                    hideSoftInput(i, localResultReceiver1);
                    break;
                }
            case 11:
            }
            paramParcel1.enforceInterface("com.android.internal.view.IInputMethod");
            if (paramParcel1.readInt() != 0);
            for (InputMethodSubtype localInputMethodSubtype = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(paramParcel1); ; localInputMethodSubtype = null)
            {
                changeInputMethodSubtype(localInputMethodSubtype);
                break;
            }
        }

        private static class Proxy
            implements IInputMethod
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void attachToken(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    localParcel.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void bindInput(InputBinding paramInputBinding)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    if (paramInputBinding != null)
                    {
                        localParcel.writeInt(1);
                        paramInputBinding.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void changeInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    if (paramInputMethodSubtype != null)
                    {
                        localParcel.writeInt(1);
                        paramInputMethodSubtype.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(11, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void createSession(IInputMethodCallback paramIInputMethodCallback)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    if (paramIInputMethodCallback != null)
                        localIBinder = paramIInputMethodCallback.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(6, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.view.IInputMethod";
            }

            public void hideSoftInput(int paramInt, ResultReceiver paramResultReceiver)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    localParcel.writeInt(paramInt);
                    if (paramResultReceiver != null)
                    {
                        localParcel.writeInt(1);
                        paramResultReceiver.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(10, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void restartInput(IInputContext paramIInputContext, EditorInfo paramEditorInfo)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    if (paramIInputContext != null)
                        localIBinder = paramIInputContext.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    if (paramEditorInfo != null)
                    {
                        localParcel.writeInt(1);
                        paramEditorInfo.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(5, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void revokeSession(IInputMethodSession paramIInputMethodSession)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    if (paramIInputMethodSession != null)
                        localIBinder = paramIInputMethodSession.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(8, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setSessionEnabled(IInputMethodSession paramIInputMethodSession, boolean paramBoolean)
                throws RemoteException
            {
                IBinder localIBinder = null;
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    if (paramIInputMethodSession != null)
                        localIBinder = paramIInputMethodSession.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(7, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void showSoftInput(int paramInt, ResultReceiver paramResultReceiver)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    localParcel.writeInt(paramInt);
                    if (paramResultReceiver != null)
                    {
                        localParcel.writeInt(1);
                        paramResultReceiver.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void startInput(IInputContext paramIInputContext, EditorInfo paramEditorInfo)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    if (paramIInputContext != null)
                        localIBinder = paramIInputContext.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    if (paramEditorInfo != null)
                    {
                        localParcel.writeInt(1);
                        paramEditorInfo.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void unbindInput()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethod");
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.IInputMethod
 * JD-Core Version:        0.6.2
 */