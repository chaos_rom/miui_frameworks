package com.android.internal.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IInputMethodClient extends IInterface
{
    public abstract void onBindMethod(InputBindResult paramInputBindResult)
        throws RemoteException;

    public abstract void onUnbindMethod(int paramInt)
        throws RemoteException;

    public abstract void setActive(boolean paramBoolean)
        throws RemoteException;

    public abstract void setUsingInputMethod(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IInputMethodClient
    {
        private static final String DESCRIPTOR = "com.android.internal.view.IInputMethodClient";
        static final int TRANSACTION_onBindMethod = 2;
        static final int TRANSACTION_onUnbindMethod = 3;
        static final int TRANSACTION_setActive = 4;
        static final int TRANSACTION_setUsingInputMethod = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.view.IInputMethodClient");
        }

        public static IInputMethodClient asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.view.IInputMethodClient");
                if ((localIInterface != null) && ((localIInterface instanceof IInputMethodClient)))
                    localObject = (IInputMethodClient)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = false;
            boolean bool2 = true;
            switch (paramInt1)
            {
            default:
                bool2 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return bool2;
                paramParcel2.writeString("com.android.internal.view.IInputMethodClient");
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodClient");
                if (paramParcel1.readInt() != 0)
                    bool1 = bool2;
                setUsingInputMethod(bool1);
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodClient");
                if (paramParcel1.readInt() != 0);
                for (InputBindResult localInputBindResult = (InputBindResult)InputBindResult.CREATOR.createFromParcel(paramParcel1); ; localInputBindResult = null)
                {
                    onBindMethod(localInputBindResult);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodClient");
                onUnbindMethod(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.view.IInputMethodClient");
                if (paramParcel1.readInt() != 0)
                    bool1 = bool2;
                setActive(bool1);
            }
        }

        private static class Proxy
            implements IInputMethodClient
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.view.IInputMethodClient";
            }

            public void onBindMethod(InputBindResult paramInputBindResult)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
                    if (paramInputBindResult != null)
                    {
                        localParcel.writeInt(1);
                        paramInputBindResult.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onUnbindMethod(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setActive(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setUsingInputMethod(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.view.IInputMethodClient");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.IInputMethodClient
 * JD-Core Version:        0.6.2
 */