package com.android.internal.view;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.BadSurfaceTypeException;
import android.view.SurfaceHolder.Callback;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public abstract class BaseSurfaceHolder
    implements SurfaceHolder
{
    static final boolean DEBUG = false;
    private static final String TAG = "BaseSurfaceHolder";
    public final ArrayList<SurfaceHolder.Callback> mCallbacks = new ArrayList();
    SurfaceHolder.Callback[] mGottenCallbacks;
    boolean mHaveGottenCallbacks;
    long mLastLockTime = 0L;
    protected int mRequestedFormat = -1;
    int mRequestedHeight = -1;
    int mRequestedType = -1;
    int mRequestedWidth = -1;
    public Surface mSurface = new Surface();
    final Rect mSurfaceFrame = new Rect();
    public final ReentrantLock mSurfaceLock = new ReentrantLock();
    Rect mTmpDirty;
    int mType = -1;

    private final Canvas internalLockCanvas(Rect paramRect)
    {
        if (this.mType == 3)
            throw new SurfaceHolder.BadSurfaceTypeException("Surface type is SURFACE_TYPE_PUSH_BUFFERS");
        this.mSurfaceLock.lock();
        Object localObject = null;
        if (onAllowLockCanvas())
            if (paramRect == null)
            {
                if (this.mTmpDirty == null)
                    this.mTmpDirty = new Rect();
                this.mTmpDirty.set(this.mSurfaceFrame);
                paramRect = this.mTmpDirty;
            }
        while (true)
        {
            long l1;
            long l2;
            try
            {
                Canvas localCanvas = this.mSurface.lockCanvas(paramRect);
                localObject = localCanvas;
                if (localObject != null)
                {
                    this.mLastLockTime = SystemClock.uptimeMillis();
                    return localObject;
                }
            }
            catch (Exception localException)
            {
                Log.e("BaseSurfaceHolder", "Exception locking surface", localException);
                continue;
                l1 = SystemClock.uptimeMillis();
                l2 = 100L + this.mLastLockTime;
                if (l2 <= l1)
                    break label149;
            }
            long l3 = l2 - l1;
            try
            {
                Thread.sleep(l3);
                label145: l1 = SystemClock.uptimeMillis();
                label149: this.mLastLockTime = l1;
                this.mSurfaceLock.unlock();
                localObject = null;
            }
            catch (InterruptedException localInterruptedException)
            {
                break label145;
            }
        }
    }

    public void addCallback(SurfaceHolder.Callback paramCallback)
    {
        synchronized (this.mCallbacks)
        {
            if (!this.mCallbacks.contains(paramCallback))
                this.mCallbacks.add(paramCallback);
            return;
        }
    }

    public SurfaceHolder.Callback[] getCallbacks()
    {
        SurfaceHolder.Callback[] arrayOfCallback;
        if (this.mHaveGottenCallbacks)
            arrayOfCallback = this.mGottenCallbacks;
        while (true)
        {
            return arrayOfCallback;
            synchronized (this.mCallbacks)
            {
                int i = this.mCallbacks.size();
                if (i > 0)
                {
                    if ((this.mGottenCallbacks == null) || (this.mGottenCallbacks.length != i))
                        this.mGottenCallbacks = new SurfaceHolder.Callback[i];
                    this.mCallbacks.toArray(this.mGottenCallbacks);
                    this.mHaveGottenCallbacks = true;
                    arrayOfCallback = this.mGottenCallbacks;
                    continue;
                }
                this.mGottenCallbacks = null;
            }
        }
    }

    public int getRequestedFormat()
    {
        return this.mRequestedFormat;
    }

    public int getRequestedHeight()
    {
        return this.mRequestedHeight;
    }

    public int getRequestedType()
    {
        return this.mRequestedType;
    }

    public int getRequestedWidth()
    {
        return this.mRequestedWidth;
    }

    public Surface getSurface()
    {
        return this.mSurface;
    }

    public Rect getSurfaceFrame()
    {
        return this.mSurfaceFrame;
    }

    public Canvas lockCanvas()
    {
        return internalLockCanvas(null);
    }

    public Canvas lockCanvas(Rect paramRect)
    {
        return internalLockCanvas(paramRect);
    }

    public abstract boolean onAllowLockCanvas();

    public abstract void onRelayoutContainer();

    public abstract void onUpdateSurface();

    public void removeCallback(SurfaceHolder.Callback paramCallback)
    {
        synchronized (this.mCallbacks)
        {
            this.mCallbacks.remove(paramCallback);
            return;
        }
    }

    public void setFixedSize(int paramInt1, int paramInt2)
    {
        if ((this.mRequestedWidth != paramInt1) || (this.mRequestedHeight != paramInt2))
        {
            this.mRequestedWidth = paramInt1;
            this.mRequestedHeight = paramInt2;
            onRelayoutContainer();
        }
    }

    public void setFormat(int paramInt)
    {
        if (this.mRequestedFormat != paramInt)
        {
            this.mRequestedFormat = paramInt;
            onUpdateSurface();
        }
    }

    public void setSizeFromLayout()
    {
        if ((this.mRequestedWidth != -1) || (this.mRequestedHeight != -1))
        {
            this.mRequestedHeight = -1;
            this.mRequestedWidth = -1;
            onRelayoutContainer();
        }
    }

    public void setSurfaceFrameSize(int paramInt1, int paramInt2)
    {
        this.mSurfaceFrame.top = 0;
        this.mSurfaceFrame.left = 0;
        this.mSurfaceFrame.right = paramInt1;
        this.mSurfaceFrame.bottom = paramInt2;
    }

    public void setType(int paramInt)
    {
        switch (paramInt)
        {
        default:
            switch (paramInt)
            {
            case 1:
            case 2:
            default:
            case 0:
            case 3:
            }
            break;
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            paramInt = 0;
            break;
            if (this.mRequestedType != paramInt)
            {
                this.mRequestedType = paramInt;
                onUpdateSurface();
            }
        }
    }

    public void ungetCallbacks()
    {
        this.mHaveGottenCallbacks = false;
    }

    public void unlockCanvasAndPost(Canvas paramCanvas)
    {
        this.mSurface.unlockCanvasAndPost(paramCanvas);
        this.mSurfaceLock.unlock();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.BaseSurfaceHolder
 * JD-Core Version:        0.6.2
 */