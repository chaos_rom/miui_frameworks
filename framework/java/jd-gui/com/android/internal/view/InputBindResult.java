package com.android.internal.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class InputBindResult
    implements Parcelable
{
    public static final Parcelable.Creator<InputBindResult> CREATOR = new Parcelable.Creator()
    {
        public InputBindResult createFromParcel(Parcel paramAnonymousParcel)
        {
            return new InputBindResult(paramAnonymousParcel);
        }

        public InputBindResult[] newArray(int paramAnonymousInt)
        {
            return new InputBindResult[paramAnonymousInt];
        }
    };
    static final String TAG = "InputBindResult";
    public final String id;
    public final IInputMethodSession method;
    public final int sequence;

    InputBindResult(Parcel paramParcel)
    {
        this.method = IInputMethodSession.Stub.asInterface(paramParcel.readStrongBinder());
        this.id = paramParcel.readString();
        this.sequence = paramParcel.readInt();
    }

    public InputBindResult(IInputMethodSession paramIInputMethodSession, String paramString, int paramInt)
    {
        this.method = paramIInputMethodSession;
        this.id = paramString;
        this.sequence = paramInt;
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        return "InputBindResult{" + this.method + " " + this.id + " #" + this.sequence + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStrongInterface(this.method);
        paramParcel.writeString(this.id);
        paramParcel.writeInt(this.sequence);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.InputBindResult
 * JD-Core Version:        0.6.2
 */