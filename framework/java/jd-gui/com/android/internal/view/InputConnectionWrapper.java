package com.android.internal.view;

import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;

public class InputConnectionWrapper
    implements InputConnection
{
    private static final int MAX_WAIT_TIME_MILLIS = 2000;
    private final IInputContext mIInputContext;

    public InputConnectionWrapper(IInputContext paramIInputContext)
    {
        this.mIInputContext = paramIInputContext;
    }

    public boolean beginBatchEdit()
    {
        try
        {
            this.mIInputContext.beginBatchEdit();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean clearMetaKeyStates(int paramInt)
    {
        try
        {
            this.mIInputContext.clearMetaKeyStates(paramInt);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean commitCompletion(CompletionInfo paramCompletionInfo)
    {
        try
        {
            this.mIInputContext.commitCompletion(paramCompletionInfo);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean commitCorrection(CorrectionInfo paramCorrectionInfo)
    {
        try
        {
            this.mIInputContext.commitCorrection(paramCorrectionInfo);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean commitText(CharSequence paramCharSequence, int paramInt)
    {
        try
        {
            this.mIInputContext.commitText(paramCharSequence, paramInt);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean deleteSurroundingText(int paramInt1, int paramInt2)
    {
        try
        {
            this.mIInputContext.deleteSurroundingText(paramInt1, paramInt2);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean endBatchEdit()
    {
        try
        {
            this.mIInputContext.endBatchEdit();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean finishComposingText()
    {
        try
        {
            this.mIInputContext.finishComposingText();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public int getCursorCapsMode(int paramInt)
    {
        int i = 0;
        try
        {
            InputContextCallback localInputContextCallback = InputContextCallback.access$000();
            this.mIInputContext.getCursorCapsMode(paramInt, localInputContextCallback.mSeq, localInputContextCallback);
            try
            {
                localInputContextCallback.waitForResultLocked();
                if (localInputContextCallback.mHaveValue)
                    i = localInputContextCallback.mCursorCapsMode;
                localInputContextCallback.dispose();
                j = i;
                return j;
            }
            finally
            {
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int j = 0;
        }
    }

    public ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt)
    {
        ExtractedText localExtractedText1 = null;
        try
        {
            InputContextCallback localInputContextCallback = InputContextCallback.access$000();
            this.mIInputContext.getExtractedText(paramExtractedTextRequest, paramInt, localInputContextCallback.mSeq, localInputContextCallback);
            try
            {
                localInputContextCallback.waitForResultLocked();
                if (localInputContextCallback.mHaveValue)
                    localExtractedText1 = localInputContextCallback.mExtractedText;
                localInputContextCallback.dispose();
                localExtractedText2 = localExtractedText1;
                return localExtractedText2;
            }
            finally
            {
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                ExtractedText localExtractedText2 = null;
        }
    }

    public CharSequence getSelectedText(int paramInt)
    {
        CharSequence localCharSequence1 = null;
        try
        {
            InputContextCallback localInputContextCallback = InputContextCallback.access$000();
            this.mIInputContext.getSelectedText(paramInt, localInputContextCallback.mSeq, localInputContextCallback);
            try
            {
                localInputContextCallback.waitForResultLocked();
                if (localInputContextCallback.mHaveValue)
                    localCharSequence1 = localInputContextCallback.mSelectedText;
                localInputContextCallback.dispose();
                localCharSequence2 = localCharSequence1;
                return localCharSequence2;
            }
            finally
            {
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                CharSequence localCharSequence2 = null;
        }
    }

    public CharSequence getTextAfterCursor(int paramInt1, int paramInt2)
    {
        CharSequence localCharSequence1 = null;
        try
        {
            InputContextCallback localInputContextCallback = InputContextCallback.access$000();
            this.mIInputContext.getTextAfterCursor(paramInt1, paramInt2, localInputContextCallback.mSeq, localInputContextCallback);
            try
            {
                localInputContextCallback.waitForResultLocked();
                if (localInputContextCallback.mHaveValue)
                    localCharSequence1 = localInputContextCallback.mTextAfterCursor;
                localInputContextCallback.dispose();
                localCharSequence2 = localCharSequence1;
                return localCharSequence2;
            }
            finally
            {
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                CharSequence localCharSequence2 = null;
        }
    }

    public CharSequence getTextBeforeCursor(int paramInt1, int paramInt2)
    {
        CharSequence localCharSequence1 = null;
        try
        {
            InputContextCallback localInputContextCallback = InputContextCallback.access$000();
            this.mIInputContext.getTextBeforeCursor(paramInt1, paramInt2, localInputContextCallback.mSeq, localInputContextCallback);
            try
            {
                localInputContextCallback.waitForResultLocked();
                if (localInputContextCallback.mHaveValue)
                    localCharSequence1 = localInputContextCallback.mTextBeforeCursor;
                localInputContextCallback.dispose();
                localCharSequence2 = localCharSequence1;
                return localCharSequence2;
            }
            finally
            {
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                CharSequence localCharSequence2 = null;
        }
    }

    public boolean performContextMenuAction(int paramInt)
    {
        try
        {
            this.mIInputContext.performContextMenuAction(paramInt);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean performEditorAction(int paramInt)
    {
        try
        {
            this.mIInputContext.performEditorAction(paramInt);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean performPrivateCommand(String paramString, Bundle paramBundle)
    {
        try
        {
            this.mIInputContext.performPrivateCommand(paramString, paramBundle);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean reportFullscreenMode(boolean paramBoolean)
    {
        try
        {
            this.mIInputContext.reportFullscreenMode(paramBoolean);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean sendKeyEvent(KeyEvent paramKeyEvent)
    {
        try
        {
            this.mIInputContext.sendKeyEvent(paramKeyEvent);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean setComposingRegion(int paramInt1, int paramInt2)
    {
        try
        {
            this.mIInputContext.setComposingRegion(paramInt1, paramInt2);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean setComposingText(CharSequence paramCharSequence, int paramInt)
    {
        try
        {
            this.mIInputContext.setComposingText(paramCharSequence, paramInt);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean setSelection(int paramInt1, int paramInt2)
    {
        try
        {
            this.mIInputContext.setSelection(paramInt1, paramInt2);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    static class InputContextCallback extends IInputContextCallback.Stub
    {
        private static final String TAG = "InputConnectionWrapper.ICC";
        private static InputContextCallback sInstance = new InputContextCallback();
        private static int sSequenceNumber = 1;
        public int mCursorCapsMode;
        public ExtractedText mExtractedText;
        public boolean mHaveValue;
        public CharSequence mSelectedText;
        public int mSeq;
        public CharSequence mTextAfterCursor;
        public CharSequence mTextBeforeCursor;

        private void dispose()
        {
            try
            {
                if (sInstance == null)
                {
                    this.mTextAfterCursor = null;
                    this.mTextBeforeCursor = null;
                    this.mExtractedText = null;
                    sInstance = this;
                }
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        private static InputContextCallback getInstance()
        {
            try
            {
                if (sInstance != null)
                {
                    localInputContextCallback = sInstance;
                    sInstance = null;
                    localInputContextCallback.mHaveValue = false;
                    int i = sSequenceNumber;
                    sSequenceNumber = i + 1;
                    localInputContextCallback.mSeq = i;
                    return localInputContextCallback;
                }
                InputContextCallback localInputContextCallback = new InputContextCallback();
            }
            finally
            {
            }
        }

        public void setCursorCapsMode(int paramInt1, int paramInt2)
        {
            try
            {
                if (paramInt2 == this.mSeq)
                {
                    this.mCursorCapsMode = paramInt1;
                    this.mHaveValue = true;
                    notifyAll();
                    return;
                }
                Log.i("InputConnectionWrapper.ICC", "Got out-of-sequence callback " + paramInt2 + " (expected " + this.mSeq + ") in setCursorCapsMode, ignoring.");
            }
            finally
            {
            }
        }

        public void setExtractedText(ExtractedText paramExtractedText, int paramInt)
        {
            try
            {
                if (paramInt == this.mSeq)
                {
                    this.mExtractedText = paramExtractedText;
                    this.mHaveValue = true;
                    notifyAll();
                    return;
                }
                Log.i("InputConnectionWrapper.ICC", "Got out-of-sequence callback " + paramInt + " (expected " + this.mSeq + ") in setExtractedText, ignoring.");
            }
            finally
            {
            }
        }

        public void setSelectedText(CharSequence paramCharSequence, int paramInt)
        {
            try
            {
                if (paramInt == this.mSeq)
                {
                    this.mSelectedText = paramCharSequence;
                    this.mHaveValue = true;
                    notifyAll();
                    return;
                }
                Log.i("InputConnectionWrapper.ICC", "Got out-of-sequence callback " + paramInt + " (expected " + this.mSeq + ") in setSelectedText, ignoring.");
            }
            finally
            {
            }
        }

        public void setTextAfterCursor(CharSequence paramCharSequence, int paramInt)
        {
            try
            {
                if (paramInt == this.mSeq)
                {
                    this.mTextAfterCursor = paramCharSequence;
                    this.mHaveValue = true;
                    notifyAll();
                    return;
                }
                Log.i("InputConnectionWrapper.ICC", "Got out-of-sequence callback " + paramInt + " (expected " + this.mSeq + ") in setTextAfterCursor, ignoring.");
            }
            finally
            {
            }
        }

        public void setTextBeforeCursor(CharSequence paramCharSequence, int paramInt)
        {
            try
            {
                if (paramInt == this.mSeq)
                {
                    this.mTextBeforeCursor = paramCharSequence;
                    this.mHaveValue = true;
                    notifyAll();
                    return;
                }
                Log.i("InputConnectionWrapper.ICC", "Got out-of-sequence callback " + paramInt + " (expected " + this.mSeq + ") in setTextBeforeCursor, ignoring.");
            }
            finally
            {
            }
        }

        void waitForResultLocked()
        {
            long l1 = 2000L + SystemClock.uptimeMillis();
            while (true)
            {
                long l2;
                if (!this.mHaveValue)
                {
                    l2 = l1 - SystemClock.uptimeMillis();
                    if (l2 <= 0L)
                        Log.w("InputConnectionWrapper.ICC", "Timed out waiting on IInputContextCallback");
                }
                else
                {
                    return;
                }
                try
                {
                    wait(l2);
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.InputConnectionWrapper
 * JD-Core Version:        0.6.2
 */