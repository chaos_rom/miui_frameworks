package com.android.internal.view.menu;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.accessibility.AccessibilityEvent;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.R.styleable;
import java.util.List;

public class ActionMenuItemView extends TextView
    implements MenuView.ItemView, View.OnClickListener, View.OnLongClickListener, ActionMenuView.ActionMenuChildView
{
    private static final String TAG = "ActionMenuItemView";
    private boolean mAllowTextWithIcon;
    private boolean mExpandedFormat;
    private Drawable mIcon;
    private MenuItemImpl mItemData;
    private MenuBuilder.ItemInvoker mItemInvoker;
    private int mMinWidth;
    private int mSavedPaddingLeft;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    int mSavedPaddingRight;
    private CharSequence mTitle;

    public ActionMenuItemView(Context paramContext)
    {
        this(paramContext, null);
    }

    public ActionMenuItemView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public ActionMenuItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet, paramInt);
        this.mAllowTextWithIcon = paramContext.getResources().getBoolean(17891382);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActionMenuItemView, 0, 0);
        this.mMinWidth = localTypedArray.getDimensionPixelSize(0, 0);
        localTypedArray.recycle();
        setOnClickListener(this);
        setOnLongClickListener(this);
        this.mSavedPaddingLeft = -1;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void updateTextButtonVisibility()
    {
        int i = 0;
        int j;
        if (!TextUtils.isEmpty(this.mTitle))
        {
            j = 1;
            if ((this.mIcon == null) || (this.mItemData.isForceShowText()) || ((this.mItemData.showsTextAsAction()) && ((this.mAllowTextWithIcon) || (this.mExpandedFormat))))
                i = 1;
            if ((j & i) == 0)
                break label79;
        }
        label79: for (CharSequence localCharSequence = this.mTitle; ; localCharSequence = null)
        {
            setText(localCharSequence);
            return;
            j = 0;
            break;
        }
    }

    public boolean dispatchHoverEvent(MotionEvent paramMotionEvent)
    {
        return onHoverEvent(paramMotionEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        onPopulateAccessibilityEvent(paramAccessibilityEvent);
        return true;
    }

    public MenuItemImpl getItemData()
    {
        return this.mItemData;
    }

    public boolean hasText()
    {
        if (!TextUtils.isEmpty(getText()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void initialize(MenuItemImpl paramMenuItemImpl, int paramInt)
    {
        this.mItemData = paramMenuItemImpl;
        setIcon(paramMenuItemImpl.getIcon());
        setTitle(paramMenuItemImpl.getTitleForItemView(this));
        setId(paramMenuItemImpl.getItemId());
        if (paramMenuItemImpl.isVisible());
        for (int i = 0; ; i = 8)
        {
            setVisibility(i);
            setEnabled(paramMenuItemImpl.isEnabled());
            return;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean miuiOnMeasure(int paramInt1, int paramInt2)
    {
        if (this.mSavedPaddingLeft >= 0)
            super.setPadding(this.mSavedPaddingLeft, getPaddingTop(), this.mSavedPaddingRight, getPaddingBottom());
        super.onMeasure(0, paramInt2);
        int i;
        if (View.MeasureSpec.getMode(paramInt1) != 0)
        {
            i = getMeasuredWidth();
            super.onMeasure(paramInt1, paramInt2);
            if ((hasText()) || (this.mIcon == null))
                break label97;
            super.setPadding((getMeasuredWidth() - this.mIcon.getIntrinsicWidth()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        while (true)
        {
            return true;
            label97: int j = (getMeasuredWidth() - i) / 2;
            super.setPadding(j + this.mPaddingLeft, this.mPaddingTop, j + this.mPaddingRight, this.mPaddingBottom);
            super.onMeasure(paramInt1, paramInt2);
        }
    }

    public boolean needsDividerAfter()
    {
        return hasText();
    }

    public boolean needsDividerBefore()
    {
        if ((hasText()) && (this.mItemData.getIcon() == null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void onClick(View paramView)
    {
        if (this.mItemInvoker != null)
            this.mItemInvoker.invokeItem(this.mItemData);
    }

    public boolean onLongClick(View paramView)
    {
        boolean bool = false;
        if (hasText())
            return bool;
        int[] arrayOfInt = new int[2];
        Rect localRect = new Rect();
        getLocationOnScreen(arrayOfInt);
        getWindowVisibleDisplayFrame(localRect);
        Context localContext = getContext();
        int i = getWidth();
        int j = getHeight();
        int k = arrayOfInt[1] + j / 2;
        int m = localContext.getResources().getDisplayMetrics().widthPixels;
        Toast localToast = Toast.makeText(localContext, this.mItemData.getTitle(), 0);
        if (k < localRect.height())
            localToast.setGravity(53, m - arrayOfInt[0] - i / 2, j);
        while (true)
        {
            localToast.show();
            bool = true;
            break;
            localToast.setGravity(81, 0, j);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onMeasure(int paramInt1, int paramInt2)
    {
        if (miuiOnMeasure(paramInt1, paramInt2))
            return;
        boolean bool = hasText();
        if ((bool) && (this.mSavedPaddingLeft >= 0))
            super.setPadding(this.mSavedPaddingLeft, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        super.onMeasure(paramInt1, paramInt2);
        int i = View.MeasureSpec.getMode(paramInt1);
        int j = View.MeasureSpec.getSize(paramInt1);
        int k = getMeasuredWidth();
        if (i == -2147483648);
        for (int m = Math.min(j, this.mMinWidth); ; m = this.mMinWidth)
        {
            if ((i != 1073741824) && (this.mMinWidth > 0) && (k < m))
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(m, 1073741824), paramInt2);
            if ((bool) || (this.mIcon == null))
                break;
            super.setPadding((getMeasuredWidth() - this.mIcon.getIntrinsicWidth()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
            break;
        }
    }

    public void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        super.onPopulateAccessibilityEvent(paramAccessibilityEvent);
        CharSequence localCharSequence = getContentDescription();
        if (!TextUtils.isEmpty(localCharSequence))
            paramAccessibilityEvent.getText().add(localCharSequence);
    }

    public boolean prefersCondensedTitle()
    {
        return true;
    }

    public void setCheckable(boolean paramBoolean)
    {
    }

    public void setChecked(boolean paramBoolean)
    {
    }

    public void setExpandedFormat(boolean paramBoolean)
    {
        if (this.mExpandedFormat != paramBoolean)
        {
            this.mExpandedFormat = paramBoolean;
            if (this.mItemData != null)
                this.mItemData.actionFormatChanged();
        }
    }

    public void setIcon(Drawable paramDrawable)
    {
        this.mIcon = paramDrawable;
        setCompoundDrawablesWithIntrinsicBounds(paramDrawable, null, null, null);
        updateTextButtonVisibility();
    }

    public void setItemInvoker(MenuBuilder.ItemInvoker paramItemInvoker)
    {
        this.mItemInvoker = paramItemInvoker;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mSavedPaddingLeft = paramInt1;
        this.mSavedPaddingRight = paramInt3;
        super.setPadding(paramInt1, paramInt2, paramInt3, paramInt4);
    }

    public void setShortcut(boolean paramBoolean, char paramChar)
    {
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        this.mTitle = paramCharSequence;
        setContentDescription(this.mTitle);
        updateTextButtonVisibility();
    }

    public boolean showsIcon()
    {
        return true;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.menu.ActionMenuItemView
 * JD-Core Version:        0.6.2
 */