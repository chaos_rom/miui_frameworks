package com.android.internal.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.util.EventLog;
import android.view.ContextMenu;
import android.view.View;
import java.util.ArrayList;

public class ContextMenuBuilder extends MenuBuilder
    implements ContextMenu
{
    public ContextMenuBuilder(Context paramContext)
    {
        super(paramContext);
    }

    public ContextMenu setHeaderIcon(int paramInt)
    {
        return (ContextMenu)super.setHeaderIconInt(paramInt);
    }

    public ContextMenu setHeaderIcon(Drawable paramDrawable)
    {
        return (ContextMenu)super.setHeaderIconInt(paramDrawable);
    }

    public ContextMenu setHeaderTitle(int paramInt)
    {
        return (ContextMenu)super.setHeaderTitleInt(paramInt);
    }

    public ContextMenu setHeaderTitle(CharSequence paramCharSequence)
    {
        return (ContextMenu)super.setHeaderTitleInt(paramCharSequence);
    }

    public ContextMenu setHeaderView(View paramView)
    {
        return (ContextMenu)super.setHeaderViewInt(paramView);
    }

    public MenuDialogHelper show(View paramView, IBinder paramIBinder)
    {
        if (paramView != null)
            paramView.createContextMenu(this);
        MenuDialogHelper localMenuDialogHelper;
        if (getVisibleItems().size() > 0)
        {
            EventLog.writeEvent(50001, 1);
            localMenuDialogHelper = new MenuDialogHelper(this);
            localMenuDialogHelper.show(paramIBinder);
        }
        while (true)
        {
            return localMenuDialogHelper;
            localMenuDialogHelper = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.menu.ContextMenuBuilder
 * JD-Core Version:        0.6.2
 */