package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.android.internal.R.styleable;

public final class ExpandedMenuView extends ListView
    implements MenuBuilder.ItemInvoker, MenuView, AdapterView.OnItemClickListener
{
    private int mAnimations;
    private MenuBuilder mMenu;

    public ExpandedMenuView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MenuView, 0, 0);
        this.mAnimations = localTypedArray.getResourceId(0, 0);
        localTypedArray.recycle();
        setOnItemClickListener(this);
    }

    public int getWindowAnimations()
    {
        return this.mAnimations;
    }

    public void initialize(MenuBuilder paramMenuBuilder)
    {
        this.mMenu = paramMenuBuilder;
    }

    public boolean invokeItem(MenuItemImpl paramMenuItemImpl)
    {
        return this.mMenu.performItemAction(paramMenuItemImpl, 0);
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
    {
        invokeItem((MenuItemImpl)getAdapter().getItem(paramInt));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.menu.ExpandedMenuView
 * JD-Core Version:        0.6.2
 */