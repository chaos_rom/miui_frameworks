package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import com.android.internal.R.styleable;
import java.util.ArrayList;

public final class IconMenuView extends ViewGroup
    implements MenuBuilder.ItemInvoker, MenuView, Runnable
{
    private static final int ITEM_CAPTION_CYCLE_DELAY = 1000;
    private int mAnimations;
    private boolean mHasStaleChildren;
    private Drawable mHorizontalDivider;
    private int mHorizontalDividerHeight;
    private ArrayList<Rect> mHorizontalDividerRects;
    private Drawable mItemBackground;
    private boolean mLastChildrenCaptionMode;
    private int[] mLayout;
    private int mLayoutNumRows;
    private int mMaxItems;
    private int mMaxItemsPerRow;
    private int mMaxRows;
    private MenuBuilder mMenu;
    private boolean mMenuBeingLongpressed = false;
    private Drawable mMoreIcon;
    private int mNumActualItemsShown;
    private int mRowHeight;
    private Drawable mVerticalDivider;
    private ArrayList<Rect> mVerticalDividerRects;
    private int mVerticalDividerWidth;

    public IconMenuView(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.IconMenuView, 0, 0);
        this.mRowHeight = localTypedArray1.getDimensionPixelSize(0, 64);
        this.mMaxRows = localTypedArray1.getInt(1, 2);
        this.mMaxItems = localTypedArray1.getInt(4, 6);
        this.mMaxItemsPerRow = localTypedArray1.getInt(2, 3);
        this.mMoreIcon = localTypedArray1.getDrawable(3);
        localTypedArray1.recycle();
        TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MenuView, 0, 0);
        this.mItemBackground = localTypedArray2.getDrawable(5);
        this.mHorizontalDivider = localTypedArray2.getDrawable(2);
        this.mHorizontalDividerRects = new ArrayList();
        this.mVerticalDivider = localTypedArray2.getDrawable(3);
        this.mVerticalDividerRects = new ArrayList();
        this.mAnimations = localTypedArray2.getResourceId(0, 0);
        localTypedArray2.recycle();
        if (this.mHorizontalDivider != null)
        {
            this.mHorizontalDividerHeight = this.mHorizontalDivider.getIntrinsicHeight();
            if (this.mHorizontalDividerHeight == -1)
                this.mHorizontalDividerHeight = 1;
        }
        if (this.mVerticalDivider != null)
        {
            this.mVerticalDividerWidth = this.mVerticalDivider.getIntrinsicWidth();
            if (this.mVerticalDividerWidth == -1)
                this.mVerticalDividerWidth = 1;
        }
        this.mLayout = new int[this.mMaxRows];
        setWillNotDraw(false);
        setFocusableInTouchMode(true);
        setDescendantFocusability(262144);
    }

    private void calculateItemFittingMetadata(int paramInt)
    {
        int i = this.mMaxItemsPerRow;
        int j = getChildCount();
        int k = 0;
        if (k < j)
        {
            LayoutParams localLayoutParams = (LayoutParams)getChildAt(k).getLayoutParams();
            localLayoutParams.maxNumItemsOnRow = 1;
            for (int m = i; ; m--)
                if (m > 0)
                {
                    if (localLayoutParams.desiredWidth < paramInt / m)
                        localLayoutParams.maxNumItemsOnRow = m;
                }
                else
                {
                    k++;
                    break;
                }
        }
    }

    private boolean doItemsFit()
    {
        int i = 1;
        int j = 0;
        int[] arrayOfInt = this.mLayout;
        int k = this.mLayoutNumRows;
        int m = 0;
        int n;
        if (m < k)
        {
            n = arrayOfInt[m];
            if (n == i)
                j++;
        }
        while (true)
        {
            m++;
            break;
            int i1 = n;
            int i3;
            for (int i2 = j; i1 > 0; i2 = i3)
            {
                i3 = i2 + 1;
                if (((LayoutParams)getChildAt(i2).getLayoutParams()).maxNumItemsOnRow < n)
                {
                    i = 0;
                    return i;
                }
                i1--;
            }
            j = i2;
        }
    }

    private void layoutItems(int paramInt)
    {
        int i = getChildCount();
        if (i == 0)
            this.mLayoutNumRows = 0;
        label66: 
        while (true)
        {
            return;
            for (int j = Math.min((int)Math.ceil(i / this.mMaxItemsPerRow), this.mMaxRows); ; j++)
            {
                if (j > this.mMaxRows)
                    break label66;
                layoutItemsUsingGravity(j, i);
                if ((j >= i) || (doItemsFit()))
                    break;
            }
        }
    }

    private void layoutItemsUsingGravity(int paramInt1, int paramInt2)
    {
        int i = paramInt2 / paramInt1;
        int j = paramInt1 - paramInt2 % paramInt1;
        int[] arrayOfInt = this.mLayout;
        for (int k = 0; k < paramInt1; k++)
        {
            arrayOfInt[k] = i;
            if (k >= j)
                arrayOfInt[k] = (1 + arrayOfInt[k]);
        }
        this.mLayoutNumRows = paramInt1;
    }

    private void positionChildren(int paramInt1, int paramInt2)
    {
        if (this.mHorizontalDivider != null)
            this.mHorizontalDividerRects.clear();
        if (this.mVerticalDivider != null)
            this.mVerticalDividerRects.clear();
        int i = this.mLayoutNumRows;
        int j = i - 1;
        int[] arrayOfInt = this.mLayout;
        int k = 0;
        LayoutParams localLayoutParams = null;
        float f1 = 0.0F;
        float f2 = (paramInt2 - this.mHorizontalDividerHeight * (i - 1)) / i;
        for (int m = 0; m < i; m++)
        {
            float f3 = 0.0F;
            float f4 = (paramInt1 - this.mVerticalDividerWidth * (-1 + arrayOfInt[m])) / arrayOfInt[m];
            for (int n = 0; n < arrayOfInt[m]; n++)
            {
                View localView = getChildAt(k);
                localView.measure(View.MeasureSpec.makeMeasureSpec((int)f4, 1073741824), View.MeasureSpec.makeMeasureSpec((int)f2, 1073741824));
                localLayoutParams = (LayoutParams)localView.getLayoutParams();
                localLayoutParams.left = ((int)f3);
                localLayoutParams.right = ((int)(f3 + f4));
                localLayoutParams.top = ((int)f1);
                localLayoutParams.bottom = ((int)(f1 + f2));
                float f5 = f3 + f4;
                k++;
                if (this.mVerticalDivider != null)
                    this.mVerticalDividerRects.add(new Rect((int)f5, (int)f1, (int)(f5 + this.mVerticalDividerWidth), (int)(f1 + f2)));
                f3 = f5 + this.mVerticalDividerWidth;
            }
            if (localLayoutParams != null)
                localLayoutParams.right = paramInt1;
            f1 += f2;
            if ((this.mHorizontalDivider != null) && (m < j))
            {
                ArrayList localArrayList = this.mHorizontalDividerRects;
                Rect localRect = new Rect(0, (int)f1, paramInt1, (int)(f1 + this.mHorizontalDividerHeight));
                localArrayList.add(localRect);
                f1 += this.mHorizontalDividerHeight;
            }
        }
    }

    private void setChildrenCaptionMode(boolean paramBoolean)
    {
        this.mLastChildrenCaptionMode = paramBoolean;
        for (int i = -1 + getChildCount(); i >= 0; i--)
            ((IconMenuItemView)getChildAt(i)).setCaptionMode(paramBoolean);
    }

    private void setCycleShortcutCaptionMode(boolean paramBoolean)
    {
        if (!paramBoolean)
        {
            removeCallbacks(this);
            setChildrenCaptionMode(false);
            this.mMenuBeingLongpressed = false;
        }
        while (true)
        {
            return;
            setChildrenCaptionMode(true);
        }
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
        return paramLayoutParams instanceof LayoutParams;
    }

    IconMenuItemView createMoreItemView()
    {
        Context localContext = getContext();
        IconMenuItemView localIconMenuItemView = (IconMenuItemView)LayoutInflater.from(localContext).inflate(17367109, null);
        localIconMenuItemView.initialize(localContext.getResources().getText(17040279), this.mMoreIcon);
        localIconMenuItemView.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                IconMenuView.this.mMenu.changeMenuMode();
            }
        });
        return localIconMenuItemView;
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        int i = 1;
        if (paramKeyEvent.getKeyCode() == 82)
        {
            if ((paramKeyEvent.getAction() != 0) || (paramKeyEvent.getRepeatCount() != 0))
                break label49;
            removeCallbacks(this);
            postDelayed(this, ViewConfiguration.getLongPressTimeout());
        }
        while (true)
        {
            i = super.dispatchKeyEvent(paramKeyEvent);
            while (true)
            {
                return i;
                label49: if (paramKeyEvent.getAction() != i)
                    break;
                if (!this.mMenuBeingLongpressed)
                    break label72;
                setCycleShortcutCaptionMode(false);
            }
            label72: removeCallbacks(this);
        }
    }

    public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
    {
        return new LayoutParams(getContext(), paramAttributeSet);
    }

    Drawable getItemBackgroundDrawable()
    {
        return this.mItemBackground.getConstantState().newDrawable(getContext().getResources());
    }

    public int[] getLayout()
    {
        return this.mLayout;
    }

    public int getLayoutNumRows()
    {
        return this.mLayoutNumRows;
    }

    int getMaxItems()
    {
        return this.mMaxItems;
    }

    int getNumActualItemsShown()
    {
        return this.mNumActualItemsShown;
    }

    public int getWindowAnimations()
    {
        return this.mAnimations;
    }

    public void initialize(MenuBuilder paramMenuBuilder)
    {
        this.mMenu = paramMenuBuilder;
    }

    public boolean invokeItem(MenuItemImpl paramMenuItemImpl)
    {
        return this.mMenu.performItemAction(paramMenuItemImpl, 0);
    }

    void markStaleChildren()
    {
        if (!this.mHasStaleChildren)
        {
            this.mHasStaleChildren = true;
            requestLayout();
        }
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        requestFocus();
    }

    protected void onDetachedFromWindow()
    {
        setCycleShortcutCaptionMode(false);
        super.onDetachedFromWindow();
    }

    protected void onDraw(Canvas paramCanvas)
    {
        Drawable localDrawable1 = this.mHorizontalDivider;
        if (localDrawable1 != null)
        {
            ArrayList localArrayList2 = this.mHorizontalDividerRects;
            for (int j = -1 + localArrayList2.size(); j >= 0; j--)
            {
                localDrawable1.setBounds((Rect)localArrayList2.get(j));
                localDrawable1.draw(paramCanvas);
            }
        }
        Drawable localDrawable2 = this.mVerticalDivider;
        if (localDrawable2 != null)
        {
            ArrayList localArrayList1 = this.mVerticalDividerRects;
            for (int i = -1 + localArrayList1.size(); i >= 0; i--)
            {
                localDrawable2.setBounds((Rect)localArrayList1.get(i));
                localDrawable2.draw(paramCanvas);
            }
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        for (int i = -1 + getChildCount(); i >= 0; i--)
        {
            View localView = getChildAt(i);
            LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
            localView.layout(localLayoutParams.left, localLayoutParams.top, localLayoutParams.right, localLayoutParams.bottom);
        }
    }

    protected void onMeasure(int paramInt1, int paramInt2)
    {
        int i = resolveSize(2147483647, paramInt1);
        calculateItemFittingMetadata(i);
        layoutItems(i);
        int j = this.mLayoutNumRows;
        setMeasuredDimension(i, resolveSize(j * (this.mRowHeight + this.mHorizontalDividerHeight) - this.mHorizontalDividerHeight, paramInt2));
        if (j > 0)
            positionChildren(getMeasuredWidth(), getMeasuredHeight());
    }

    protected void onRestoreInstanceState(Parcelable paramParcelable)
    {
        SavedState localSavedState = (SavedState)paramParcelable;
        super.onRestoreInstanceState(localSavedState.getSuperState());
        if (localSavedState.focusedPosition >= getChildCount());
        while (true)
        {
            return;
            View localView = getChildAt(localSavedState.focusedPosition);
            if (localView != null)
                localView.requestFocus();
        }
    }

    protected Parcelable onSaveInstanceState()
    {
        Parcelable localParcelable = super.onSaveInstanceState();
        View localView = getFocusedChild();
        int i = -1 + getChildCount();
        if (i >= 0)
            if (getChildAt(i) != localView);
        for (SavedState localSavedState = new SavedState(localParcelable, i); ; localSavedState = new SavedState(localParcelable, -1))
        {
            return localSavedState;
            i--;
            break;
        }
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        if (!paramBoolean)
            setCycleShortcutCaptionMode(false);
        super.onWindowFocusChanged(paramBoolean);
    }

    public void run()
    {
        boolean bool = true;
        if (this.mMenuBeingLongpressed)
            if (!this.mLastChildrenCaptionMode)
                setChildrenCaptionMode(bool);
        while (true)
        {
            postDelayed(this, 1000L);
            return;
            bool = false;
            break;
            this.mMenuBeingLongpressed = bool;
            setCycleShortcutCaptionMode(bool);
        }
    }

    void setNumActualItemsShown(int paramInt)
    {
        this.mNumActualItemsShown = paramInt;
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams
    {
        int bottom;
        int desiredWidth;
        int left;
        int maxNumItemsOnRow;
        int right;
        int top;

        public LayoutParams(int paramInt1, int paramInt2)
        {
            super(paramInt2);
        }

        public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
        {
            super(paramAttributeSet);
        }
    }

    private static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
        {
            public IconMenuView.SavedState createFromParcel(Parcel paramAnonymousParcel)
            {
                return new IconMenuView.SavedState(paramAnonymousParcel, null);
            }

            public IconMenuView.SavedState[] newArray(int paramAnonymousInt)
            {
                return new IconMenuView.SavedState[paramAnonymousInt];
            }
        };
        int focusedPosition;

        private SavedState(Parcel paramParcel)
        {
            super();
            this.focusedPosition = paramParcel.readInt();
        }

        public SavedState(Parcelable paramParcelable, int paramInt)
        {
            super();
            this.focusedPosition = paramInt;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            super.writeToParcel(paramParcel, paramInt);
            paramParcel.writeInt(this.focusedPosition);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.menu.IconMenuView
 * JD-Core Version:        0.6.2
 */