package com.android.internal.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class IconMenuPresenter extends BaseMenuPresenter
{
    private static final String OPEN_SUBMENU_KEY = "android:menu:icon:submenu";
    private static final String VIEWS_TAG = "android:menu:icon";
    private int mMaxItems = -1;
    private IconMenuItemView mMoreView;
    MenuDialogHelper mOpenSubMenu;
    int mOpenSubMenuId;
    SubMenuPresenterCallback mSubMenuPresenterCallback = new SubMenuPresenterCallback();

    public IconMenuPresenter(Context paramContext)
    {
        super(new ContextThemeWrapper(paramContext, 16974574), 17367110, 17367109);
    }

    protected void addItemView(View paramView, int paramInt)
    {
        IconMenuItemView localIconMenuItemView = (IconMenuItemView)paramView;
        IconMenuView localIconMenuView = (IconMenuView)this.mMenuView;
        localIconMenuItemView.setIconMenuView(localIconMenuView);
        localIconMenuItemView.setItemInvoker(localIconMenuView);
        localIconMenuItemView.setBackgroundDrawable(localIconMenuView.getItemBackgroundDrawable());
        super.addItemView(paramView, paramInt);
    }

    public void bindItemView(MenuItemImpl paramMenuItemImpl, MenuView.ItemView paramItemView)
    {
        IconMenuItemView localIconMenuItemView = (IconMenuItemView)paramItemView;
        localIconMenuItemView.setItemData(paramMenuItemImpl);
        localIconMenuItemView.initialize(paramMenuItemImpl.getTitleForItemView(localIconMenuItemView), paramMenuItemImpl.getIcon());
        if (paramMenuItemImpl.isVisible());
        for (int i = 0; ; i = 8)
        {
            localIconMenuItemView.setVisibility(i);
            localIconMenuItemView.setEnabled(localIconMenuItemView.isEnabled());
            localIconMenuItemView.setLayoutParams(localIconMenuItemView.getTextAppropriateLayoutParams());
            return;
        }
    }

    protected boolean filterLeftoverView(ViewGroup paramViewGroup, int paramInt)
    {
        if (paramViewGroup.getChildAt(paramInt) != this.mMoreView);
        for (boolean bool = super.filterLeftoverView(paramViewGroup, paramInt); ; bool = false)
            return bool;
    }

    public int getNumActualItemsShown()
    {
        return ((IconMenuView)this.mMenuView).getNumActualItemsShown();
    }

    public void initForMenu(Context paramContext, MenuBuilder paramMenuBuilder)
    {
        super.initForMenu(paramContext, paramMenuBuilder);
        this.mMaxItems = -1;
    }

    public void onRestoreInstanceState(Parcelable paramParcelable)
    {
        restoreHierarchyState((Bundle)paramParcelable);
    }

    public Parcelable onSaveInstanceState()
    {
        Object localObject;
        if (this.mMenuView == null)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = new Bundle();
            saveHierarchyState((Bundle)localObject);
            if (this.mOpenSubMenuId > 0)
                ((Bundle)localObject).putInt("android:menu:icon:submenu", this.mOpenSubMenuId);
        }
    }

    public boolean onSubMenuSelected(SubMenuBuilder paramSubMenuBuilder)
    {
        if (!paramSubMenuBuilder.hasVisibleItems());
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            MenuDialogHelper localMenuDialogHelper = new MenuDialogHelper(paramSubMenuBuilder);
            localMenuDialogHelper.setPresenterCallback(this.mSubMenuPresenterCallback);
            localMenuDialogHelper.show(null);
            this.mOpenSubMenu = localMenuDialogHelper;
            this.mOpenSubMenuId = paramSubMenuBuilder.getItem().getItemId();
            super.onSubMenuSelected(paramSubMenuBuilder);
        }
    }

    public void restoreHierarchyState(Bundle paramBundle)
    {
        SparseArray localSparseArray = paramBundle.getSparseParcelableArray("android:menu:icon");
        if (localSparseArray != null)
            ((View)this.mMenuView).restoreHierarchyState(localSparseArray);
        int i = paramBundle.getInt("android:menu:icon:submenu", 0);
        if ((i > 0) && (this.mMenu != null))
        {
            MenuItem localMenuItem = this.mMenu.findItem(i);
            if (localMenuItem != null)
                onSubMenuSelected((SubMenuBuilder)localMenuItem.getSubMenu());
        }
    }

    public void saveHierarchyState(Bundle paramBundle)
    {
        SparseArray localSparseArray = new SparseArray();
        if (this.mMenuView != null)
            ((View)this.mMenuView).saveHierarchyState(localSparseArray);
        paramBundle.putSparseParcelableArray("android:menu:icon", localSparseArray);
    }

    public boolean shouldIncludeItem(int paramInt, MenuItemImpl paramMenuItemImpl)
    {
        boolean bool1 = true;
        boolean bool2;
        if (((this.mMenu.getNonActionItems().size() == this.mMaxItems) && (paramInt < this.mMaxItems)) || (paramInt < -1 + this.mMaxItems))
        {
            bool2 = bool1;
            if ((!bool2) || (paramMenuItemImpl.isActionButton()))
                break label61;
        }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            label61: bool1 = false;
        }
    }

    public void updateMenuView(boolean paramBoolean)
    {
        IconMenuView localIconMenuView = (IconMenuView)this.mMenuView;
        if (this.mMaxItems < 0)
            this.mMaxItems = localIconMenuView.getMaxItems();
        ArrayList localArrayList = this.mMenu.getNonActionItems();
        int i;
        if (localArrayList.size() > this.mMaxItems)
        {
            i = 1;
            super.updateMenuView(paramBoolean);
            if ((i == 0) || ((this.mMoreView != null) && (this.mMoreView.getParent() == localIconMenuView)))
                break label134;
            if (this.mMoreView == null)
            {
                this.mMoreView = localIconMenuView.createMoreItemView();
                this.mMoreView.setBackgroundDrawable(localIconMenuView.getItemBackgroundDrawable());
            }
            localIconMenuView.addView(this.mMoreView);
            label107: if (i == 0)
                break label157;
        }
        label134: label157: for (int j = -1 + this.mMaxItems; ; j = localArrayList.size())
        {
            localIconMenuView.setNumActualItemsShown(j);
            return;
            i = 0;
            break;
            if ((i != 0) || (this.mMoreView == null))
                break label107;
            localIconMenuView.removeView(this.mMoreView);
            break label107;
        }
    }

    class SubMenuPresenterCallback
        implements MenuPresenter.Callback
    {
        SubMenuPresenterCallback()
        {
        }

        public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean)
        {
            IconMenuPresenter.this.mOpenSubMenuId = 0;
            if (IconMenuPresenter.this.mOpenSubMenu != null)
            {
                IconMenuPresenter.this.mOpenSubMenu.dismiss();
                IconMenuPresenter.this.mOpenSubMenu = null;
            }
        }

        public boolean onOpenSubMenu(MenuBuilder paramMenuBuilder)
        {
            if (paramMenuBuilder != null)
                IconMenuPresenter.this.mOpenSubMenuId = ((SubMenuBuilder)paramMenuBuilder).getItem().getItemId();
            return false;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.menu.IconMenuPresenter
 * JD-Core Version:        0.6.2
 */