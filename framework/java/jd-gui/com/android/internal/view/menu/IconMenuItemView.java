package com.android.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewDebug.CapturedViewProperty;
import android.widget.TextView;
import com.android.internal.R.styleable;

public final class IconMenuItemView extends TextView
    implements MenuView.ItemView
{
    private static final int NO_ALPHA = 255;
    private static String sPrependShortcutLabel;
    private float mDisabledAlpha;
    private Drawable mIcon;
    private IconMenuView mIconMenuView;
    private MenuItemImpl mItemData;
    private MenuBuilder.ItemInvoker mItemInvoker;
    private Rect mPositionIconAvailable = new Rect();
    private Rect mPositionIconOutput = new Rect();
    private String mShortcutCaption;
    private boolean mShortcutCaptionMode;
    private int mTextAppearance;
    private Context mTextAppearanceContext;

    public IconMenuItemView(Context paramContext, AttributeSet paramAttributeSet)
    {
        this(paramContext, paramAttributeSet, 0);
    }

    public IconMenuItemView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
        super(paramContext, paramAttributeSet);
        if (sPrependShortcutLabel == null)
            sPrependShortcutLabel = getResources().getString(17040280);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MenuView, paramInt, 0);
        this.mDisabledAlpha = localTypedArray.getFloat(6, 0.8F);
        this.mTextAppearance = localTypedArray.getResourceId(1, -1);
        this.mTextAppearanceContext = paramContext;
        localTypedArray.recycle();
    }

    private void positionIcon()
    {
        if (this.mIcon == null);
        while (true)
        {
            return;
            Rect localRect = this.mPositionIconOutput;
            getLineBounds(0, localRect);
            this.mPositionIconAvailable.set(0, 0, getWidth(), localRect.top);
            int i = getResolvedLayoutDirection();
            Gravity.apply(19, this.mIcon.getIntrinsicWidth(), this.mIcon.getIntrinsicHeight(), this.mPositionIconAvailable, this.mPositionIconOutput, i);
            this.mIcon.setBounds(this.mPositionIconOutput);
        }
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        int i;
        Drawable localDrawable;
        if ((this.mItemData != null) && (this.mIcon != null))
        {
            if ((this.mItemData.isEnabled()) || ((!isPressed()) && (isFocused())))
                break label68;
            i = 1;
            localDrawable = this.mIcon;
            if (i == 0)
                break label73;
        }
        label68: label73: for (int j = (int)(255.0F * this.mDisabledAlpha); ; j = 255)
        {
            localDrawable.setAlpha(j);
            return;
            i = 0;
            break;
        }
    }

    @ViewDebug.CapturedViewProperty(retrieveReturn=true)
    public MenuItemImpl getItemData()
    {
        return this.mItemData;
    }

    IconMenuView.LayoutParams getTextAppropriateLayoutParams()
    {
        IconMenuView.LayoutParams localLayoutParams = (IconMenuView.LayoutParams)getLayoutParams();
        if (localLayoutParams == null)
            localLayoutParams = new IconMenuView.LayoutParams(-1, -1);
        localLayoutParams.desiredWidth = ((int)Layout.getDesiredWidth(getText(), getPaint()));
        return localLayoutParams;
    }

    public void initialize(MenuItemImpl paramMenuItemImpl, int paramInt)
    {
        this.mItemData = paramMenuItemImpl;
        initialize(paramMenuItemImpl.getTitleForItemView(this), paramMenuItemImpl.getIcon());
        if (paramMenuItemImpl.isVisible());
        for (int i = 0; ; i = 8)
        {
            setVisibility(i);
            setEnabled(paramMenuItemImpl.isEnabled());
            return;
        }
    }

    void initialize(CharSequence paramCharSequence, Drawable paramDrawable)
    {
        setClickable(true);
        setFocusable(true);
        if (this.mTextAppearance != -1)
            setTextAppearance(this.mTextAppearanceContext, this.mTextAppearance);
        setTitle(paramCharSequence);
        setIcon(paramDrawable);
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
        positionIcon();
    }

    protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        super.onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
        setLayoutParams(getTextAppropriateLayoutParams());
    }

    public boolean performClick()
    {
        boolean bool = true;
        if (super.performClick());
        while (true)
        {
            return bool;
            if ((this.mItemInvoker != null) && (this.mItemInvoker.invokeItem(this.mItemData)))
                playSoundEffect(0);
            else
                bool = false;
        }
    }

    public boolean prefersCondensedTitle()
    {
        return true;
    }

    void setCaptionMode(boolean paramBoolean)
    {
        if (this.mItemData == null)
            return;
        if ((paramBoolean) && (this.mItemData.shouldShowShortcut()));
        for (boolean bool = true; ; bool = false)
        {
            this.mShortcutCaptionMode = bool;
            Object localObject = this.mItemData.getTitleForItemView(this);
            if (this.mShortcutCaptionMode)
            {
                if (this.mShortcutCaption == null)
                    this.mShortcutCaption = this.mItemData.getShortcutLabel();
                localObject = this.mShortcutCaption;
            }
            setText((CharSequence)localObject);
            break;
        }
    }

    public void setCheckable(boolean paramBoolean)
    {
    }

    public void setChecked(boolean paramBoolean)
    {
    }

    public void setIcon(Drawable paramDrawable)
    {
        this.mIcon = paramDrawable;
        if (paramDrawable != null)
        {
            paramDrawable.setBounds(0, 0, paramDrawable.getIntrinsicWidth(), paramDrawable.getIntrinsicHeight());
            setCompoundDrawables(null, paramDrawable, null, null);
            setGravity(81);
            requestLayout();
        }
        while (true)
        {
            return;
            setCompoundDrawables(null, null, null, null);
            setGravity(17);
        }
    }

    void setIconMenuView(IconMenuView paramIconMenuView)
    {
        this.mIconMenuView = paramIconMenuView;
    }

    public void setItemData(MenuItemImpl paramMenuItemImpl)
    {
        this.mItemData = paramMenuItemImpl;
    }

    public void setItemInvoker(MenuBuilder.ItemInvoker paramItemInvoker)
    {
        this.mItemInvoker = paramItemInvoker;
    }

    public void setShortcut(boolean paramBoolean, char paramChar)
    {
        if (this.mShortcutCaptionMode)
        {
            this.mShortcutCaption = null;
            setCaptionMode(true);
        }
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        if (this.mShortcutCaptionMode)
            setCaptionMode(true);
        while (true)
        {
            return;
            if (paramCharSequence != null)
                setText(paramCharSequence);
        }
    }

    public void setVisibility(int paramInt)
    {
        super.setVisibility(paramInt);
        if (this.mIconMenuView != null)
            this.mIconMenuView.markStaleChildren();
    }

    public boolean showsIcon()
    {
        return true;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.view.menu.IconMenuItemView
 * JD-Core Version:        0.6.2
 */