package com.android.internal.http;

import android.text.format.Time;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class HttpDateTime
{
    private static final Pattern HTTP_DATE_ANSIC_PATTERN = Pattern.compile("[ ]([A-Za-z]{3,9})[ ]+([0-9]{1,2})[ ]([0-9]{1,2}:[0-9][0-9]:[0-9][0-9])[ ]([0-9]{2,4})");
    private static final String HTTP_DATE_ANSIC_REGEXP = "[ ]([A-Za-z]{3,9})[ ]+([0-9]{1,2})[ ]([0-9]{1,2}:[0-9][0-9]:[0-9][0-9])[ ]([0-9]{2,4})";
    private static final Pattern HTTP_DATE_RFC_PATTERN = Pattern.compile("([0-9]{1,2})[- ]([A-Za-z]{3,9})[- ]([0-9]{2,4})[ ]([0-9]{1,2}:[0-9][0-9]:[0-9][0-9])");
    private static final String HTTP_DATE_RFC_REGEXP = "([0-9]{1,2})[- ]([A-Za-z]{3,9})[- ]([0-9]{2,4})[ ]([0-9]{1,2}:[0-9][0-9]:[0-9][0-9])";

    private static int getDate(String paramString)
    {
        if (paramString.length() == 2);
        for (int i = 10 * ('\0*0' + paramString.charAt(0)) + ('\0*0' + paramString.charAt(1)); ; i = '\0*0' + paramString.charAt(0))
            return i;
    }

    private static int getMonth(String paramString)
    {
        int i = 0;
        switch (-291 + (Character.toLowerCase(paramString.charAt(0)) + Character.toLowerCase(paramString.charAt(1)) + Character.toLowerCase(paramString.charAt(2))))
        {
        default:
            throw new IllegalArgumentException();
        case 10:
            i = 1;
        case 22:
        case 29:
        case 32:
        case 36:
        case 42:
        case 40:
        case 26:
        case 37:
        case 35:
        case 48:
        case 9:
        }
        while (true)
        {
            return i;
            i = 2;
            continue;
            i = 3;
            continue;
            i = 4;
            continue;
            i = 5;
            continue;
            i = 6;
            continue;
            i = 7;
            continue;
            i = 8;
            continue;
            i = 9;
            continue;
            i = 10;
            continue;
            i = 11;
        }
    }

    private static TimeOfDay getTime(String paramString)
    {
        int i = 0 + 1;
        int j = '\0*0' + paramString.charAt(0);
        int k;
        if (paramString.charAt(i) != ':')
        {
            int i7 = j * 10;
            k = i + 1;
            j = i7 + ('\0*0' + paramString.charAt(i));
        }
        while (true)
        {
            int m = k + 1;
            int n = m + 1;
            int i1 = 10 * ('\0*0' + paramString.charAt(m));
            int i2 = n + 1;
            int i3 = i1 + ('\0*0' + paramString.charAt(n));
            int i4 = i2 + 1;
            int i5 = i4 + 1;
            int i6 = 10 * ('\0*0' + paramString.charAt(i4));
            (i5 + 1);
            return new TimeOfDay(j, i3, i6 + ('\0*0' + paramString.charAt(i5)));
            k = i;
        }
    }

    private static int getYear(String paramString)
    {
        int j;
        int i;
        if (paramString.length() == 2)
        {
            j = 10 * ('\0*0' + paramString.charAt(0)) + ('\0*0' + paramString.charAt(1));
            if (j >= 70)
                i = j + 1900;
        }
        while (true)
        {
            return i;
            i = j + 2000;
            continue;
            if (paramString.length() == 3)
                i = 1900 + (100 * ('\0*0' + paramString.charAt(0)) + 10 * ('\0*0' + paramString.charAt(1)) + ('\0*0' + paramString.charAt(2)));
            else if (paramString.length() == 4)
                i = 1000 * ('\0*0' + paramString.charAt(0)) + 100 * ('\0*0' + paramString.charAt(1)) + 10 * ('\0*0' + paramString.charAt(2)) + ('\0*0' + paramString.charAt(3));
            else
                i = 1970;
        }
    }

    public static long parse(String paramString)
        throws IllegalArgumentException
    {
        Matcher localMatcher1 = HTTP_DATE_RFC_PATTERN.matcher(paramString);
        int j;
        int i;
        int k;
        TimeOfDay localTimeOfDay;
        if (localMatcher1.find())
        {
            j = getDate(localMatcher1.group(1));
            i = getMonth(localMatcher1.group(2));
            k = getYear(localMatcher1.group(3));
            localTimeOfDay = getTime(localMatcher1.group(4));
        }
        while (true)
        {
            if (k >= 2038)
            {
                k = 2038;
                i = 0;
                j = 1;
            }
            Time localTime = new Time("UTC");
            localTime.set(localTimeOfDay.second, localTimeOfDay.minute, localTimeOfDay.hour, j, i, k);
            return localTime.toMillis(false);
            Matcher localMatcher2 = HTTP_DATE_ANSIC_PATTERN.matcher(paramString);
            if (!localMatcher2.find())
                break;
            i = getMonth(localMatcher2.group(1));
            j = getDate(localMatcher2.group(2));
            localTimeOfDay = getTime(localMatcher2.group(3));
            k = getYear(localMatcher2.group(4));
        }
        throw new IllegalArgumentException();
    }

    private static class TimeOfDay
    {
        int hour;
        int minute;
        int second;

        TimeOfDay(int paramInt1, int paramInt2, int paramInt3)
        {
            this.hour = paramInt1;
            this.minute = paramInt2;
            this.second = paramInt3;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.http.HttpDateTime
 * JD-Core Version:        0.6.2
 */