package com.android.internal.http.multipart;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EncodingUtils;

public class MultipartEntity extends AbstractHttpEntity
{
    public static final String MULTIPART_BOUNDARY = "http.method.multipart.boundary";
    private static byte[] MULTIPART_CHARS = EncodingUtils.getAsciiBytes("-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
    private static final String MULTIPART_FORM_CONTENT_TYPE = "multipart/form-data";
    private static final Log log = LogFactory.getLog(MultipartEntity.class);
    private boolean contentConsumed = false;
    private byte[] multipartBoundary;
    private HttpParams params;
    protected Part[] parts;

    public MultipartEntity(Part[] paramArrayOfPart)
    {
        setContentType("multipart/form-data");
        if (paramArrayOfPart == null)
            throw new IllegalArgumentException("parts cannot be null");
        this.parts = paramArrayOfPart;
        this.params = null;
    }

    public MultipartEntity(Part[] paramArrayOfPart, HttpParams paramHttpParams)
    {
        if (paramArrayOfPart == null)
            throw new IllegalArgumentException("parts cannot be null");
        if (paramHttpParams == null)
            throw new IllegalArgumentException("params cannot be null");
        this.parts = paramArrayOfPart;
        this.params = paramHttpParams;
    }

    private static byte[] generateMultipartBoundary()
    {
        Random localRandom = new Random();
        byte[] arrayOfByte = new byte[30 + localRandom.nextInt(11)];
        for (int i = 0; i < arrayOfByte.length; i++)
            arrayOfByte[i] = MULTIPART_CHARS[localRandom.nextInt(MULTIPART_CHARS.length)];
        return arrayOfByte;
    }

    public InputStream getContent()
        throws IOException, IllegalStateException
    {
        if ((!isRepeatable()) && (this.contentConsumed))
            throw new IllegalStateException("Content has been consumed");
        this.contentConsumed = true;
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        Part.sendParts(localByteArrayOutputStream, this.parts, this.multipartBoundary);
        return new ByteArrayInputStream(localByteArrayOutputStream.toByteArray());
    }

    public long getContentLength()
    {
        try
        {
            long l2 = Part.getLengthOfParts(this.parts, getMultipartBoundary());
            l1 = l2;
            return l1;
        }
        catch (Exception localException)
        {
            while (true)
            {
                log.error("An exception occurred while getting the length of the parts", localException);
                long l1 = 0L;
            }
        }
    }

    public Header getContentType()
    {
        StringBuffer localStringBuffer = new StringBuffer("multipart/form-data");
        localStringBuffer.append("; boundary=");
        localStringBuffer.append(EncodingUtils.getAsciiString(getMultipartBoundary()));
        return new BasicHeader("Content-Type", localStringBuffer.toString());
    }

    protected byte[] getMultipartBoundary()
    {
        String str;
        if (this.multipartBoundary == null)
        {
            str = null;
            if (this.params != null)
                str = (String)this.params.getParameter("http.method.multipart.boundary");
            if (str == null)
                break label48;
        }
        label48: for (this.multipartBoundary = EncodingUtils.getAsciiBytes(str); ; this.multipartBoundary = generateMultipartBoundary())
            return this.multipartBoundary;
    }

    public boolean isRepeatable()
    {
        int i = 0;
        if (i < this.parts.length)
            if (this.parts[i].isRepeatable());
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            i++;
            break;
        }
    }

    public boolean isStreaming()
    {
        return false;
    }

    public void writeTo(OutputStream paramOutputStream)
        throws IOException
    {
        Part.sendParts(paramOutputStream, this.parts, getMultipartBoundary());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.http.multipart.MultipartEntity
 * JD-Core Version:        0.6.2
 */