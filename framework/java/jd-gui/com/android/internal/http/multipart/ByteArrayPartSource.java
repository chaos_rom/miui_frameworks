package com.android.internal.http.multipart;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class ByteArrayPartSource
    implements PartSource
{
    private byte[] bytes;
    private String fileName;

    public ByteArrayPartSource(String paramString, byte[] paramArrayOfByte)
    {
        this.fileName = paramString;
        this.bytes = paramArrayOfByte;
    }

    public InputStream createInputStream()
    {
        return new ByteArrayInputStream(this.bytes);
    }

    public String getFileName()
    {
        return this.fileName;
    }

    public long getLength()
    {
        return this.bytes.length;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.http.multipart.ByteArrayPartSource
 * JD-Core Version:        0.6.2
 */