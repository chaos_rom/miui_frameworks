package com.android.internal.http.multipart;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FilePartSource
    implements PartSource
{
    private File file = null;
    private String fileName = null;

    public FilePartSource(File paramFile)
        throws FileNotFoundException
    {
        this.file = paramFile;
        if (paramFile != null)
        {
            if (!paramFile.isFile())
                throw new FileNotFoundException("File is not a normal file.");
            if (!paramFile.canRead())
                throw new FileNotFoundException("File is not readable.");
            this.fileName = paramFile.getName();
        }
    }

    public FilePartSource(String paramString, File paramFile)
        throws FileNotFoundException
    {
        this(paramFile);
        if (paramString != null)
            this.fileName = paramString;
    }

    public InputStream createInputStream()
        throws IOException
    {
        if (this.file != null);
        for (Object localObject = new FileInputStream(this.file); ; localObject = new ByteArrayInputStream(new byte[0]))
            return localObject;
    }

    public String getFileName()
    {
        if (this.fileName == null);
        for (String str = "noname"; ; str = this.fileName)
            return str;
    }

    public long getLength()
    {
        if (this.file != null);
        for (long l = this.file.length(); ; l = 0L)
            return l;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.http.multipart.FilePartSource
 * JD-Core Version:        0.6.2
 */