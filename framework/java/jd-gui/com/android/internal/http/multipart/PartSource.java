package com.android.internal.http.multipart;

import java.io.IOException;
import java.io.InputStream;

public abstract interface PartSource
{
    public abstract InputStream createInputStream()
        throws IOException;

    public abstract String getFileName();

    public abstract long getLength();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.http.multipart.PartSource
 * JD-Core Version:        0.6.2
 */