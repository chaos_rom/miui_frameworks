package com.android.internal.http.multipart;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.util.EncodingUtils;

public class StringPart extends PartBase
{
    public static final String DEFAULT_CHARSET = "US-ASCII";
    public static final String DEFAULT_CONTENT_TYPE = "text/plain";
    public static final String DEFAULT_TRANSFER_ENCODING = "8bit";
    private static final Log LOG = LogFactory.getLog(StringPart.class);
    private byte[] content;
    private String value;

    public StringPart(String paramString1, String paramString2)
    {
        this(paramString1, paramString2, null);
    }

    public StringPart(String paramString1, String paramString2, String paramString3)
    {
        super(paramString1, "text/plain", paramString3, "8bit");
        if (paramString2 == null)
            throw new IllegalArgumentException("Value may not be null");
        if (paramString2.indexOf(0) != -1)
            throw new IllegalArgumentException("NULs may not be present in string parts");
        this.value = paramString2;
    }

    private byte[] getContent()
    {
        if (this.content == null)
            this.content = EncodingUtils.getBytes(this.value, getCharSet());
        return this.content;
    }

    protected long lengthOfData()
    {
        LOG.trace("enter lengthOfData()");
        return getContent().length;
    }

    protected void sendData(OutputStream paramOutputStream)
        throws IOException
    {
        LOG.trace("enter sendData(OutputStream)");
        paramOutputStream.write(getContent());
    }

    public void setCharSet(String paramString)
    {
        super.setCharSet(paramString);
        this.content = null;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.http.multipart.StringPart
 * JD-Core Version:        0.6.2
 */