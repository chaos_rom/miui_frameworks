package com.android.internal.statusbar;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IStatusBar extends IInterface
{
    public abstract void addNotification(IBinder paramIBinder, StatusBarNotification paramStatusBarNotification)
        throws RemoteException;

    public abstract void animateCollapse()
        throws RemoteException;

    public abstract void animateExpand()
        throws RemoteException;

    public abstract void cancelPreloadRecentApps()
        throws RemoteException;

    public abstract void disable(int paramInt)
        throws RemoteException;

    public abstract void preloadRecentApps()
        throws RemoteException;

    public abstract void removeIcon(int paramInt)
        throws RemoteException;

    public abstract void removeNotification(IBinder paramIBinder)
        throws RemoteException;

    public abstract void setHardKeyboardStatus(boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public abstract void setIcon(int paramInt, StatusBarIcon paramStatusBarIcon)
        throws RemoteException;

    public abstract void setImeWindowStatus(IBinder paramIBinder, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setSystemUiVisibility(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void toggleRecentApps()
        throws RemoteException;

    public abstract void topAppWindowChanged(boolean paramBoolean)
        throws RemoteException;

    public abstract void updateNotification(IBinder paramIBinder, StatusBarNotification paramStatusBarNotification)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IStatusBar
    {
        private static final String DESCRIPTOR = "com.android.internal.statusbar.IStatusBar";
        static final int TRANSACTION_addNotification = 3;
        static final int TRANSACTION_animateCollapse = 8;
        static final int TRANSACTION_animateExpand = 7;
        static final int TRANSACTION_cancelPreloadRecentApps = 15;
        static final int TRANSACTION_disable = 6;
        static final int TRANSACTION_preloadRecentApps = 14;
        static final int TRANSACTION_removeIcon = 2;
        static final int TRANSACTION_removeNotification = 5;
        static final int TRANSACTION_setHardKeyboardStatus = 12;
        static final int TRANSACTION_setIcon = 1;
        static final int TRANSACTION_setImeWindowStatus = 11;
        static final int TRANSACTION_setSystemUiVisibility = 9;
        static final int TRANSACTION_toggleRecentApps = 13;
        static final int TRANSACTION_topAppWindowChanged = 10;
        static final int TRANSACTION_updateNotification = 4;

        public Stub()
        {
            attachInterface(this, "com.android.internal.statusbar.IStatusBar");
        }

        public static IStatusBar asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.statusbar.IStatusBar");
                if ((localIInterface != null) && ((localIInterface instanceof IStatusBar)))
                    localObject = (IStatusBar)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.statusbar.IStatusBar");
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                int i = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (StatusBarIcon localStatusBarIcon = (StatusBarIcon)StatusBarIcon.CREATOR.createFromParcel(paramParcel1); ; localStatusBarIcon = null)
                {
                    setIcon(i, localStatusBarIcon);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                removeIcon(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                IBinder localIBinder2 = paramParcel1.readStrongBinder();
                if (paramParcel1.readInt() != 0);
                for (StatusBarNotification localStatusBarNotification2 = (StatusBarNotification)StatusBarNotification.CREATOR.createFromParcel(paramParcel1); ; localStatusBarNotification2 = null)
                {
                    addNotification(localIBinder2, localStatusBarNotification2);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                IBinder localIBinder1 = paramParcel1.readStrongBinder();
                if (paramParcel1.readInt() != 0);
                for (StatusBarNotification localStatusBarNotification1 = (StatusBarNotification)StatusBarNotification.CREATOR.createFromParcel(paramParcel1); ; localStatusBarNotification1 = null)
                {
                    updateNotification(localIBinder1, localStatusBarNotification1);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                removeNotification(paramParcel1.readStrongBinder());
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                disable(paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                animateExpand();
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                animateCollapse();
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                setSystemUiVisibility(paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                if (paramParcel1.readInt() != 0);
                for (boolean bool4 = bool1; ; bool4 = false)
                {
                    topAppWindowChanged(bool4);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                setImeWindowStatus(paramParcel1.readStrongBinder(), paramParcel1.readInt(), paramParcel1.readInt());
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                boolean bool2;
                if (paramParcel1.readInt() != 0)
                {
                    bool2 = bool1;
                    label489: if (paramParcel1.readInt() == 0)
                        break label517;
                }
                label517: for (boolean bool3 = bool1; ; bool3 = false)
                {
                    setHardKeyboardStatus(bool2, bool3);
                    break;
                    bool2 = false;
                    break label489;
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                toggleRecentApps();
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                preloadRecentApps();
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
                cancelPreloadRecentApps();
            }
        }

        private static class Proxy
            implements IStatusBar
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addNotification(IBinder paramIBinder, StatusBarNotification paramStatusBarNotification)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    localParcel.writeStrongBinder(paramIBinder);
                    if (paramStatusBarNotification != null)
                    {
                        localParcel.writeInt(1);
                        paramStatusBarNotification.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void animateCollapse()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    this.mRemote.transact(8, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void animateExpand()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    this.mRemote.transact(7, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancelPreloadRecentApps()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    this.mRemote.transact(15, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void disable(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(6, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.statusbar.IStatusBar";
            }

            public void preloadRecentApps()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    this.mRemote.transact(14, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void removeIcon(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void removeNotification(IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    localParcel.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(5, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setHardKeyboardStatus(boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    if (paramBoolean1);
                    for (int j = i; ; j = 0)
                    {
                        localParcel.writeInt(j);
                        if (!paramBoolean2)
                            break;
                        localParcel.writeInt(i);
                        this.mRemote.transact(12, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setIcon(int paramInt, StatusBarIcon paramStatusBarIcon)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    localParcel.writeInt(paramInt);
                    if (paramStatusBarIcon != null)
                    {
                        localParcel.writeInt(1);
                        paramStatusBarIcon.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setImeWindowStatus(IBinder paramIBinder, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    localParcel.writeStrongBinder(paramIBinder);
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(11, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void setSystemUiVisibility(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(9, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void toggleRecentApps()
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    this.mRemote.transact(13, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void topAppWindowChanged(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(10, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void updateNotification(IBinder paramIBinder, StatusBarNotification paramStatusBarNotification)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
                    localParcel.writeStrongBinder(paramIBinder);
                    if (paramStatusBarNotification != null)
                    {
                        localParcel.writeInt(1);
                        paramStatusBarNotification.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.statusbar.IStatusBar
 * JD-Core Version:        0.6.2
 */