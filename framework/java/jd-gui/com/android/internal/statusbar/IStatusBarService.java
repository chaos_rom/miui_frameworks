package com.android.internal.statusbar;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IStatusBarService extends IInterface
{
    public abstract void cancelPreloadRecentApps()
        throws RemoteException;

    public abstract void collapse()
        throws RemoteException;

    public abstract void disable(int paramInt, IBinder paramIBinder, String paramString)
        throws RemoteException;

    public abstract void expand()
        throws RemoteException;

    public abstract void onClearAllNotifications()
        throws RemoteException;

    public abstract void onNotificationClear(String paramString1, String paramString2, int paramInt)
        throws RemoteException;

    public abstract void onNotificationClick(String paramString1, String paramString2, int paramInt)
        throws RemoteException;

    public abstract void onNotificationError(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3)
        throws RemoteException;

    public abstract void onPanelRevealed()
        throws RemoteException;

    public abstract void preloadRecentApps()
        throws RemoteException;

    public abstract void registerStatusBar(IStatusBar paramIStatusBar, StatusBarIconList paramStatusBarIconList, List<IBinder> paramList1, List<StatusBarNotification> paramList, int[] paramArrayOfInt, List<IBinder> paramList2)
        throws RemoteException;

    public abstract void removeIcon(String paramString)
        throws RemoteException;

    public abstract void setHardKeyboardEnabled(boolean paramBoolean)
        throws RemoteException;

    public abstract void setIcon(String paramString1, String paramString2, int paramInt1, int paramInt2, String paramString3)
        throws RemoteException;

    public abstract void setIconVisibility(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void setImeWindowStatus(IBinder paramIBinder, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setSystemUiVisibility(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void toggleRecentApps()
        throws RemoteException;

    public abstract void topAppWindowChanged(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IStatusBarService
    {
        private static final String DESCRIPTOR = "com.android.internal.statusbar.IStatusBarService";
        static final int TRANSACTION_cancelPreloadRecentApps = 19;
        static final int TRANSACTION_collapse = 2;
        static final int TRANSACTION_disable = 3;
        static final int TRANSACTION_expand = 1;
        static final int TRANSACTION_onClearAllNotifications = 13;
        static final int TRANSACTION_onNotificationClear = 14;
        static final int TRANSACTION_onNotificationClick = 11;
        static final int TRANSACTION_onNotificationError = 12;
        static final int TRANSACTION_onPanelRevealed = 10;
        static final int TRANSACTION_preloadRecentApps = 18;
        static final int TRANSACTION_registerStatusBar = 9;
        static final int TRANSACTION_removeIcon = 6;
        static final int TRANSACTION_setHardKeyboardEnabled = 16;
        static final int TRANSACTION_setIcon = 4;
        static final int TRANSACTION_setIconVisibility = 5;
        static final int TRANSACTION_setImeWindowStatus = 8;
        static final int TRANSACTION_setSystemUiVisibility = 15;
        static final int TRANSACTION_toggleRecentApps = 17;
        static final int TRANSACTION_topAppWindowChanged = 7;

        public Stub()
        {
            attachInterface(this, "com.android.internal.statusbar.IStatusBarService");
        }

        public static IStatusBarService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.statusbar.IStatusBarService");
                if ((localIInterface != null) && ((localIInterface instanceof IStatusBarService)))
                    localObject = (IStatusBarService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("com.android.internal.statusbar.IStatusBarService");
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                expand();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                collapse();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                disable(paramParcel1.readInt(), paramParcel1.readStrongBinder(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                setIcon(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                String str = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                for (boolean bool4 = true; ; bool4 = false)
                {
                    setIconVisibility(str, bool4);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                removeIcon(paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                if (paramParcel1.readInt() != 0);
                for (boolean bool3 = true; ; bool3 = false)
                {
                    topAppWindowChanged(bool3);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                setImeWindowStatus(paramParcel1.readStrongBinder(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                IStatusBar localIStatusBar = IStatusBar.Stub.asInterface(paramParcel1.readStrongBinder());
                StatusBarIconList localStatusBarIconList = new StatusBarIconList();
                ArrayList localArrayList1 = new ArrayList();
                ArrayList localArrayList2 = new ArrayList();
                int i = paramParcel1.readInt();
                int[] arrayOfInt;
                label506: ArrayList localArrayList3;
                if (i < 0)
                {
                    arrayOfInt = null;
                    localArrayList3 = new ArrayList();
                    registerStatusBar(localIStatusBar, localStatusBarIconList, localArrayList1, localArrayList2, arrayOfInt, localArrayList3);
                    paramParcel2.writeNoException();
                    if (localStatusBarIconList == null)
                        break label591;
                    paramParcel2.writeInt(1);
                    localStatusBarIconList.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    paramParcel2.writeBinderList(localArrayList1);
                    paramParcel2.writeTypedList(localArrayList2);
                    paramParcel2.writeIntArray(arrayOfInt);
                    paramParcel2.writeBinderList(localArrayList3);
                    bool1 = true;
                    break;
                    arrayOfInt = new int[i];
                    break label506;
                    label591: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                onPanelRevealed();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                onNotificationClick(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                onNotificationError(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                onClearAllNotifications();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                onNotificationClear(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                setSystemUiVisibility(paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                if (paramParcel1.readInt() != 0);
                for (boolean bool2 = true; ; bool2 = false)
                {
                    setHardKeyboardEnabled(bool2);
                    paramParcel2.writeNoException();
                    bool1 = true;
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                toggleRecentApps();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                preloadRecentApps();
                paramParcel2.writeNoException();
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
                cancelPreloadRecentApps();
                paramParcel2.writeNoException();
                bool1 = true;
            }
        }

        private static class Proxy
            implements IStatusBarService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancelPreloadRecentApps()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void collapse()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void disable(int paramInt, IBinder paramIBinder, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void expand()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.statusbar.IStatusBarService";
            }

            public void onClearAllNotifications()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onNotificationClear(String paramString1, String paramString2, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onNotificationClick(String paramString1, String paramString2, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onNotificationError(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    localParcel1.writeString(paramString3);
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onPanelRevealed()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void preloadRecentApps()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void registerStatusBar(IStatusBar paramIStatusBar, StatusBarIconList paramStatusBarIconList, List<IBinder> paramList1, List<StatusBarNotification> paramList, int[] paramArrayOfInt, List<IBinder> paramList2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    IBinder localIBinder;
                    if (paramIStatusBar != null)
                    {
                        localIBinder = paramIStatusBar.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        if (paramArrayOfInt != null)
                            break label131;
                        localParcel1.writeInt(-1);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        if (localParcel2.readInt() != 0)
                            paramStatusBarIconList.readFromParcel(localParcel2);
                        localParcel2.readBinderList(paramList1);
                        localParcel2.readTypedList(paramList, StatusBarNotification.CREATOR);
                        localParcel2.readIntArray(paramArrayOfInt);
                        localParcel2.readBinderList(paramList2);
                        return;
                        localIBinder = null;
                        break;
                        label131: localParcel1.writeInt(paramArrayOfInt.length);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removeIcon(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setHardKeyboardEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setIcon(String paramString1, String paramString2, int paramInt1, int paramInt2, String paramString3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeString(paramString3);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setIconVisibility(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setImeWindowStatus(IBinder paramIBinder, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setSystemUiVisibility(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void toggleRecentApps()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void topAppWindowChanged(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.statusbar.IStatusBarService
 * JD-Core Version:        0.6.2
 */