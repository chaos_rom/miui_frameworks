package com.android.internal.statusbar;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class StatusBarIcon
    implements Parcelable
{
    public static final Parcelable.Creator<StatusBarIcon> CREATOR = new Parcelable.Creator()
    {
        public StatusBarIcon createFromParcel(Parcel paramAnonymousParcel)
        {
            return new StatusBarIcon(paramAnonymousParcel);
        }

        public StatusBarIcon[] newArray(int paramAnonymousInt)
        {
            return new StatusBarIcon[paramAnonymousInt];
        }
    };
    public CharSequence contentDescription;
    public int iconId;
    public int iconLevel;
    public String iconPackage;
    public int number;
    public boolean visible = true;

    public StatusBarIcon(Parcel paramParcel)
    {
        readFromParcel(paramParcel);
    }

    public StatusBarIcon(String paramString, int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence)
    {
        this.iconPackage = paramString;
        this.iconId = paramInt1;
        this.iconLevel = paramInt2;
        this.number = paramInt3;
        this.contentDescription = paramCharSequence;
    }

    public StatusBarIcon clone()
    {
        StatusBarIcon localStatusBarIcon = new StatusBarIcon(this.iconPackage, this.iconId, this.iconLevel, this.number, this.contentDescription);
        localStatusBarIcon.visible = this.visible;
        return localStatusBarIcon;
    }

    public int describeContents()
    {
        return 0;
    }

    public void readFromParcel(Parcel paramParcel)
    {
        this.iconPackage = paramParcel.readString();
        this.iconId = paramParcel.readInt();
        this.iconLevel = paramParcel.readInt();
        if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.visible = bool;
            this.number = paramParcel.readInt();
            this.contentDescription = paramParcel.readCharSequence();
            return;
        }
    }

    public String toString()
    {
        return "StatusBarIcon(pkg=" + this.iconPackage + " id=0x" + Integer.toHexString(this.iconId) + " level=" + this.iconLevel + " visible=" + this.visible + " num=" + this.number + " )";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.iconPackage);
        paramParcel.writeInt(this.iconId);
        paramParcel.writeInt(this.iconLevel);
        if (this.visible);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            paramParcel.writeInt(this.number);
            paramParcel.writeCharSequence(this.contentDescription);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.statusbar.StatusBarIcon
 * JD-Core Version:        0.6.2
 */