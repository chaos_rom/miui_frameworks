package com.android.internal.statusbar;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.PrintWriter;

public class StatusBarIconList
    implements Parcelable
{
    public static final Parcelable.Creator<StatusBarIconList> CREATOR = new Parcelable.Creator()
    {
        public StatusBarIconList createFromParcel(Parcel paramAnonymousParcel)
        {
            return new StatusBarIconList(paramAnonymousParcel);
        }

        public StatusBarIconList[] newArray(int paramAnonymousInt)
        {
            return new StatusBarIconList[paramAnonymousInt];
        }
    };
    private StatusBarIcon[] mIcons;
    private String[] mSlots;

    public StatusBarIconList()
    {
    }

    public StatusBarIconList(Parcel paramParcel)
    {
        readFromParcel(paramParcel);
    }

    public void copyFrom(StatusBarIconList paramStatusBarIconList)
    {
        if (paramStatusBarIconList.mSlots == null)
        {
            this.mSlots = null;
            this.mIcons = null;
            return;
        }
        int i = paramStatusBarIconList.mSlots.length;
        this.mSlots = new String[i];
        this.mIcons = new StatusBarIcon[i];
        int j = 0;
        label42: StatusBarIcon[] arrayOfStatusBarIcon;
        if (j < i)
        {
            this.mSlots[j] = paramStatusBarIconList.mSlots[j];
            arrayOfStatusBarIcon = this.mIcons;
            if (paramStatusBarIconList.mIcons[j] == null)
                break label97;
        }
        label97: for (StatusBarIcon localStatusBarIcon = paramStatusBarIconList.mIcons[j].clone(); ; localStatusBarIcon = null)
        {
            arrayOfStatusBarIcon[j] = localStatusBarIcon;
            j++;
            break label42;
            break;
        }
    }

    public void defineSlots(String[] paramArrayOfString)
    {
        int i = paramArrayOfString.length;
        String[] arrayOfString = new String[i];
        this.mSlots = arrayOfString;
        for (int j = 0; j < i; j++)
            arrayOfString[j] = paramArrayOfString[j];
        this.mIcons = new StatusBarIcon[i];
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(PrintWriter paramPrintWriter)
    {
        int i = this.mSlots.length;
        paramPrintWriter.println("Icon list:");
        for (int j = 0; j < i; j++)
        {
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = Integer.valueOf(j);
            arrayOfObject[1] = this.mSlots[j];
            arrayOfObject[2] = this.mIcons[j];
            paramPrintWriter.printf("    %2d: (%s) %s\n", arrayOfObject);
        }
    }

    public StatusBarIcon getIcon(int paramInt)
    {
        return this.mIcons[paramInt];
    }

    public String getSlot(int paramInt)
    {
        return this.mSlots[paramInt];
    }

    public int getSlotIndex(String paramString)
    {
        int i = this.mSlots.length;
        int j = 0;
        if (j < i)
            if (!paramString.equals(this.mSlots[j]));
        while (true)
        {
            return j;
            j++;
            break;
            j = -1;
        }
    }

    public int getViewIndex(int paramInt)
    {
        int i = 0;
        for (int j = 0; j < paramInt; j++)
            if (this.mIcons[j] != null)
                i++;
        return i;
    }

    public void readFromParcel(Parcel paramParcel)
    {
        this.mSlots = paramParcel.readStringArray();
        int i = paramParcel.readInt();
        if (i < 0)
            this.mIcons = null;
        while (true)
        {
            return;
            this.mIcons = new StatusBarIcon[i];
            for (int j = 0; j < i; j++)
                if (paramParcel.readInt() != 0)
                    this.mIcons[j] = new StatusBarIcon(paramParcel);
        }
    }

    public void removeIcon(int paramInt)
    {
        this.mIcons[paramInt] = null;
    }

    public void setIcon(int paramInt, StatusBarIcon paramStatusBarIcon)
    {
        this.mIcons[paramInt] = paramStatusBarIcon.clone();
    }

    public int size()
    {
        return this.mSlots.length;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStringArray(this.mSlots);
        if (this.mIcons == null)
        {
            paramParcel.writeInt(-1);
            return;
        }
        int i = this.mIcons.length;
        paramParcel.writeInt(i);
        int j = 0;
        label36: StatusBarIcon localStatusBarIcon;
        if (j < i)
        {
            localStatusBarIcon = this.mIcons[j];
            if (localStatusBarIcon != null)
                break label67;
            paramParcel.writeInt(0);
        }
        while (true)
        {
            j++;
            break label36;
            break;
            label67: paramParcel.writeInt(1);
            localStatusBarIcon.writeToParcel(paramParcel, paramInt);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.statusbar.StatusBarIconList
 * JD-Core Version:        0.6.2
 */