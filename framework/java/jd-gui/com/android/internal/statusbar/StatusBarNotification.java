package com.android.internal.statusbar;

import android.app.Notification;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class StatusBarNotification
    implements Parcelable
{
    public static final Parcelable.Creator<StatusBarNotification> CREATOR = new Parcelable.Creator()
    {
        public StatusBarNotification createFromParcel(Parcel paramAnonymousParcel)
        {
            return new StatusBarNotification(paramAnonymousParcel);
        }

        public StatusBarNotification[] newArray(int paramAnonymousInt)
        {
            return new StatusBarNotification[paramAnonymousInt];
        }
    };
    public int id;
    public int initialPid;
    public Notification notification;
    public String pkg;
    public int score;
    public String tag;
    public int uid;

    public StatusBarNotification()
    {
    }

    public StatusBarNotification(Parcel paramParcel)
    {
        readFromParcel(paramParcel);
    }

    public StatusBarNotification(String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3, int paramInt4, Notification paramNotification)
    {
        if (paramString1 == null)
            throw new NullPointerException();
        if (paramNotification == null)
            throw new NullPointerException();
        this.pkg = paramString1;
        this.id = paramInt1;
        this.tag = paramString2;
        this.uid = paramInt2;
        this.initialPid = paramInt3;
        this.score = paramInt4;
        this.notification = paramNotification;
    }

    public StatusBarNotification clone()
    {
        return new StatusBarNotification(this.pkg, this.id, this.tag, this.uid, this.initialPid, this.score, this.notification.clone());
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean isClearable()
    {
        if (((0x2 & this.notification.flags) == 0) && ((0x20 & this.notification.flags) == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isOngoing()
    {
        if ((0x2 & this.notification.flags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void readFromParcel(Parcel paramParcel)
    {
        this.pkg = paramParcel.readString();
        this.id = paramParcel.readInt();
        if (paramParcel.readInt() != 0);
        for (this.tag = paramParcel.readString(); ; this.tag = null)
        {
            this.uid = paramParcel.readInt();
            this.initialPid = paramParcel.readInt();
            this.score = paramParcel.readInt();
            this.notification = new Notification(paramParcel);
            return;
        }
    }

    public String toString()
    {
        return "StatusBarNotification(pkg=" + this.pkg + " id=" + this.id + " tag=" + this.tag + " score=" + this.score + " notn=" + this.notification + ")";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.pkg);
        paramParcel.writeInt(this.id);
        if (this.tag != null)
        {
            paramParcel.writeInt(1);
            paramParcel.writeString(this.tag);
        }
        while (true)
        {
            paramParcel.writeInt(this.uid);
            paramParcel.writeInt(this.initialPid);
            paramParcel.writeInt(this.score);
            this.notification.writeToParcel(paramParcel, paramInt);
            return;
            paramParcel.writeInt(0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.statusbar.StatusBarNotification
 * JD-Core Version:        0.6.2
 */