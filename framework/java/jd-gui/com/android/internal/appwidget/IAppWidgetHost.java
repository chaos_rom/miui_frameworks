package com.android.internal.appwidget;

import android.appwidget.AppWidgetProviderInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.widget.RemoteViews;

public abstract interface IAppWidgetHost extends IInterface
{
    public abstract void providerChanged(int paramInt, AppWidgetProviderInfo paramAppWidgetProviderInfo)
        throws RemoteException;

    public abstract void updateAppWidget(int paramInt, RemoteViews paramRemoteViews)
        throws RemoteException;

    public abstract void viewDataChanged(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAppWidgetHost
    {
        private static final String DESCRIPTOR = "com.android.internal.appwidget.IAppWidgetHost";
        static final int TRANSACTION_providerChanged = 2;
        static final int TRANSACTION_updateAppWidget = 1;
        static final int TRANSACTION_viewDataChanged = 3;

        public Stub()
        {
            attachInterface(this, "com.android.internal.appwidget.IAppWidgetHost");
        }

        public static IAppWidgetHost asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.appwidget.IAppWidgetHost");
                if ((localIInterface != null) && ((localIInterface instanceof IAppWidgetHost)))
                    localObject = (IAppWidgetHost)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("com.android.internal.appwidget.IAppWidgetHost");
                continue;
                paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetHost");
                int j = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (RemoteViews localRemoteViews = (RemoteViews)RemoteViews.CREATOR.createFromParcel(paramParcel1); ; localRemoteViews = null)
                {
                    updateAppWidget(j, localRemoteViews);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetHost");
                int i = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (AppWidgetProviderInfo localAppWidgetProviderInfo = (AppWidgetProviderInfo)AppWidgetProviderInfo.CREATOR.createFromParcel(paramParcel1); ; localAppWidgetProviderInfo = null)
                {
                    providerChanged(i, localAppWidgetProviderInfo);
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetHost");
                viewDataChanged(paramParcel1.readInt(), paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements IAppWidgetHost
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.appwidget.IAppWidgetHost";
            }

            public void providerChanged(int paramInt, AppWidgetProviderInfo paramAppWidgetProviderInfo)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetHost");
                    localParcel.writeInt(paramInt);
                    if (paramAppWidgetProviderInfo != null)
                    {
                        localParcel.writeInt(1);
                        paramAppWidgetProviderInfo.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void updateAppWidget(int paramInt, RemoteViews paramRemoteViews)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetHost");
                    localParcel.writeInt(paramInt);
                    if (paramRemoteViews != null)
                    {
                        localParcel.writeInt(1);
                        paramRemoteViews.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void viewDataChanged(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetHost");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(3, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.appwidget.IAppWidgetHost
 * JD-Core Version:        0.6.2
 */