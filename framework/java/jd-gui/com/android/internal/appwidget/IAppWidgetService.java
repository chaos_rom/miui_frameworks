package com.android.internal.appwidget;

import android.appwidget.AppWidgetProviderInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.List;

public abstract interface IAppWidgetService extends IInterface
{
    public abstract int allocateAppWidgetId(String paramString, int paramInt)
        throws RemoteException;

    public abstract void bindAppWidgetId(int paramInt, ComponentName paramComponentName)
        throws RemoteException;

    public abstract boolean bindAppWidgetIdIfAllowed(String paramString, int paramInt, ComponentName paramComponentName)
        throws RemoteException;

    public abstract void bindRemoteViewsService(int paramInt, Intent paramIntent, IBinder paramIBinder)
        throws RemoteException;

    public abstract void deleteAllHosts()
        throws RemoteException;

    public abstract void deleteAppWidgetId(int paramInt)
        throws RemoteException;

    public abstract void deleteHost(int paramInt)
        throws RemoteException;

    public abstract int[] getAppWidgetIds(ComponentName paramComponentName)
        throws RemoteException;

    public abstract AppWidgetProviderInfo getAppWidgetInfo(int paramInt)
        throws RemoteException;

    public abstract Bundle getAppWidgetOptions(int paramInt)
        throws RemoteException;

    public abstract RemoteViews getAppWidgetViews(int paramInt)
        throws RemoteException;

    public abstract List<AppWidgetProviderInfo> getInstalledProviders()
        throws RemoteException;

    public abstract boolean hasBindAppWidgetPermission(String paramString)
        throws RemoteException;

    public abstract void notifyAppWidgetViewDataChanged(int[] paramArrayOfInt, int paramInt)
        throws RemoteException;

    public abstract void partiallyUpdateAppWidgetIds(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
        throws RemoteException;

    public abstract void setBindAppWidgetPermission(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract int[] startListening(IAppWidgetHost paramIAppWidgetHost, String paramString, int paramInt, List<RemoteViews> paramList)
        throws RemoteException;

    public abstract void stopListening(int paramInt)
        throws RemoteException;

    public abstract void unbindRemoteViewsService(int paramInt, Intent paramIntent)
        throws RemoteException;

    public abstract void updateAppWidgetIds(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
        throws RemoteException;

    public abstract void updateAppWidgetOptions(int paramInt, Bundle paramBundle)
        throws RemoteException;

    public abstract void updateAppWidgetProvider(ComponentName paramComponentName, RemoteViews paramRemoteViews)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IAppWidgetService
    {
        private static final String DESCRIPTOR = "com.android.internal.appwidget.IAppWidgetService";
        static final int TRANSACTION_allocateAppWidgetId = 3;
        static final int TRANSACTION_bindAppWidgetId = 18;
        static final int TRANSACTION_bindAppWidgetIdIfAllowed = 19;
        static final int TRANSACTION_bindRemoteViewsService = 20;
        static final int TRANSACTION_deleteAllHosts = 6;
        static final int TRANSACTION_deleteAppWidgetId = 4;
        static final int TRANSACTION_deleteHost = 5;
        static final int TRANSACTION_getAppWidgetIds = 22;
        static final int TRANSACTION_getAppWidgetInfo = 15;
        static final int TRANSACTION_getAppWidgetOptions = 10;
        static final int TRANSACTION_getAppWidgetViews = 7;
        static final int TRANSACTION_getInstalledProviders = 14;
        static final int TRANSACTION_hasBindAppWidgetPermission = 16;
        static final int TRANSACTION_notifyAppWidgetViewDataChanged = 13;
        static final int TRANSACTION_partiallyUpdateAppWidgetIds = 11;
        static final int TRANSACTION_setBindAppWidgetPermission = 17;
        static final int TRANSACTION_startListening = 1;
        static final int TRANSACTION_stopListening = 2;
        static final int TRANSACTION_unbindRemoteViewsService = 21;
        static final int TRANSACTION_updateAppWidgetIds = 8;
        static final int TRANSACTION_updateAppWidgetOptions = 9;
        static final int TRANSACTION_updateAppWidgetProvider = 12;

        public Stub()
        {
            attachInterface(this, "com.android.internal.appwidget.IAppWidgetService");
        }

        public static IAppWidgetService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.appwidget.IAppWidgetService");
                if ((localIInterface != null) && ((localIInterface instanceof IAppWidgetService)))
                    localObject = (IAppWidgetService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                while (true)
                {
                    return j;
                    paramParcel2.writeString("com.android.internal.appwidget.IAppWidgetService");
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                    IAppWidgetHost localIAppWidgetHost = IAppWidgetHost.Stub.asInterface(paramParcel1.readStrongBinder());
                    String str3 = paramParcel1.readString();
                    int i6 = paramParcel1.readInt();
                    ArrayList localArrayList = new ArrayList();
                    int[] arrayOfInt4 = startListening(localIAppWidgetHost, str3, i6, localArrayList);
                    paramParcel2.writeNoException();
                    paramParcel2.writeIntArray(arrayOfInt4);
                    paramParcel2.writeTypedList(localArrayList);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                    stopListening(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                    int i5 = allocateAppWidgetId(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i5);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                    deleteAppWidgetId(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                    deleteHost(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                    deleteAllHosts();
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                    RemoteViews localRemoteViews4 = getAppWidgetViews(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localRemoteViews4 != null)
                    {
                        paramParcel2.writeInt(j);
                        localRemoteViews4.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                        int[] arrayOfInt3 = paramParcel1.createIntArray();
                        if (paramParcel1.readInt() != 0);
                        for (RemoteViews localRemoteViews3 = (RemoteViews)RemoteViews.CREATOR.createFromParcel(paramParcel1); ; localRemoteViews3 = null)
                        {
                            updateAppWidgetIds(arrayOfInt3, localRemoteViews3);
                            paramParcel2.writeNoException();
                            break;
                        }
                        paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                        int i4 = paramParcel1.readInt();
                        if (paramParcel1.readInt() != 0);
                        for (Bundle localBundle2 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle2 = null)
                        {
                            updateAppWidgetOptions(i4, localBundle2);
                            paramParcel2.writeNoException();
                            break;
                        }
                        paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                        Bundle localBundle1 = getAppWidgetOptions(paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localBundle1 != null)
                        {
                            paramParcel2.writeInt(j);
                            localBundle1.writeToParcel(paramParcel2, j);
                        }
                        else
                        {
                            paramParcel2.writeInt(0);
                            continue;
                            paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                            int[] arrayOfInt2 = paramParcel1.createIntArray();
                            if (paramParcel1.readInt() != 0);
                            for (RemoteViews localRemoteViews2 = (RemoteViews)RemoteViews.CREATOR.createFromParcel(paramParcel1); ; localRemoteViews2 = null)
                            {
                                partiallyUpdateAppWidgetIds(arrayOfInt2, localRemoteViews2);
                                paramParcel2.writeNoException();
                                break;
                            }
                            paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                            ComponentName localComponentName4;
                            if (paramParcel1.readInt() != 0)
                            {
                                localComponentName4 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                                if (paramParcel1.readInt() == 0)
                                    break label736;
                            }
                            for (RemoteViews localRemoteViews1 = (RemoteViews)RemoteViews.CREATOR.createFromParcel(paramParcel1); ; localRemoteViews1 = null)
                            {
                                updateAppWidgetProvider(localComponentName4, localRemoteViews1);
                                paramParcel2.writeNoException();
                                break;
                                localComponentName4 = null;
                                break label694;
                            }
                            paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                            notifyAppWidgetViewDataChanged(paramParcel1.createIntArray(), paramParcel1.readInt());
                            paramParcel2.writeNoException();
                            continue;
                            paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                            List localList = getInstalledProviders();
                            paramParcel2.writeNoException();
                            paramParcel2.writeTypedList(localList);
                            continue;
                            paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                            AppWidgetProviderInfo localAppWidgetProviderInfo = getAppWidgetInfo(paramParcel1.readInt());
                            paramParcel2.writeNoException();
                            if (localAppWidgetProviderInfo != null)
                            {
                                paramParcel2.writeInt(j);
                                localAppWidgetProviderInfo.writeToParcel(paramParcel2, j);
                            }
                            else
                            {
                                paramParcel2.writeInt(0);
                                continue;
                                paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                                boolean bool2 = hasBindAppWidgetPermission(paramParcel1.readString());
                                paramParcel2.writeNoException();
                                if (bool2)
                                    i = j;
                                paramParcel2.writeInt(i);
                            }
                        }
                    }
                }
            case 17:
                paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                String str2 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0);
                int i3;
                for (int i2 = j; ; i3 = 0)
                {
                    setBindAppWidgetPermission(str2, i2);
                    paramParcel2.writeNoException();
                    break;
                }
            case 18:
                paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                int i1 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName3 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName3 = null)
                {
                    bindAppWidgetId(i1, localComponentName3);
                    paramParcel2.writeNoException();
                    break;
                }
            case 19:
                paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                String str1 = paramParcel1.readString();
                int n = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (ComponentName localComponentName2 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName2 = null)
                {
                    boolean bool1 = bindAppWidgetIdIfAllowed(str1, n, localComponentName2);
                    paramParcel2.writeNoException();
                    if (bool1)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
            case 20:
                paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                int m = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Intent localIntent2 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent2 = null)
                {
                    bindRemoteViewsService(m, localIntent2, paramParcel1.readStrongBinder());
                    paramParcel2.writeNoException();
                    break;
                }
            case 21:
                label694: label736: paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
                int k = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                for (Intent localIntent1 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent1 = null)
                {
                    unbindRemoteViewsService(k, localIntent1);
                    paramParcel2.writeNoException();
                    break;
                }
            case 22:
            }
            paramParcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            if (paramParcel1.readInt() != 0);
            for (ComponentName localComponentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName1 = null)
            {
                int[] arrayOfInt1 = getAppWidgetIds(localComponentName1);
                paramParcel2.writeNoException();
                paramParcel2.writeIntArray(arrayOfInt1);
                break;
            }
        }

        private static class Proxy
            implements IAppWidgetService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public int allocateAppWidgetId(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void bindAppWidgetId(int paramInt, ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(18, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean bindAppWidgetIdIfAllowed(String paramString, int paramInt, ComponentName paramComponentName)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt);
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(19, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void bindRemoteViewsService(int paramInt, Intent paramIntent, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    if (paramIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeStrongBinder(paramIBinder);
                        this.mRemote.transact(20, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void deleteAllHosts()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void deleteAppWidgetId(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void deleteHost(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int[] getAppWidgetIds(ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(22, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int[] arrayOfInt = localParcel2.createIntArray();
                        return arrayOfInt;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public AppWidgetProviderInfo getAppWidgetInfo(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localAppWidgetProviderInfo = (AppWidgetProviderInfo)AppWidgetProviderInfo.CREATOR.createFromParcel(localParcel2);
                        return localAppWidgetProviderInfo;
                    }
                    AppWidgetProviderInfo localAppWidgetProviderInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Bundle getAppWidgetOptions(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localBundle = (Bundle)Bundle.CREATOR.createFromParcel(localParcel2);
                        return localBundle;
                    }
                    Bundle localBundle = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public RemoteViews getAppWidgetViews(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localRemoteViews = (RemoteViews)RemoteViews.CREATOR.createFromParcel(localParcel2);
                        return localRemoteViews;
                    }
                    RemoteViews localRemoteViews = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<AppWidgetProviderInfo> getInstalledProviders()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(AppWidgetProviderInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.appwidget.IAppWidgetService";
            }

            public boolean hasBindAppWidgetPermission(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyAppWidgetViewDataChanged(int[] paramArrayOfInt, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeIntArray(paramArrayOfInt);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void partiallyUpdateAppWidgetIds(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeIntArray(paramArrayOfInt);
                    if (paramRemoteViews != null)
                    {
                        localParcel1.writeInt(1);
                        paramRemoteViews.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setBindAppWidgetPermission(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int[] startListening(IAppWidgetHost paramIAppWidgetHost, String paramString, int paramInt, List<RemoteViews> paramList)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    if (paramIAppWidgetHost != null)
                    {
                        localIBinder = paramIAppWidgetHost.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int[] arrayOfInt = localParcel2.createIntArray();
                        localParcel2.readTypedList(paramList, RemoteViews.CREATOR);
                        return arrayOfInt;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void stopListening(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unbindRemoteViewsService(int paramInt, Intent paramIntent)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    if (paramIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(21, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateAppWidgetIds(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeIntArray(paramArrayOfInt);
                    if (paramRemoteViews != null)
                    {
                        localParcel1.writeInt(1);
                        paramRemoteViews.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(8, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateAppWidgetOptions(int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                    localParcel1.writeInt(paramInt);
                    if (paramBundle != null)
                    {
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateAppWidgetProvider(ComponentName paramComponentName, RemoteViews paramRemoteViews)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            if (paramRemoteViews != null)
                            {
                                localParcel1.writeInt(1);
                                paramRemoteViews.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(12, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.appwidget.IAppWidgetService
 * JD-Core Version:        0.6.2
 */