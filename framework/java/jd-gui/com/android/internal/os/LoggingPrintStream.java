package com.android.internal.os;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.util.Formatter;
import java.util.Locale;

abstract class LoggingPrintStream extends PrintStream
{
    private final StringBuilder builder = new StringBuilder();
    private CharBuffer decodedChars;
    private CharsetDecoder decoder;
    private ByteBuffer encodedBytes;
    private final Formatter formatter = new Formatter(this.builder, null);

    protected LoggingPrintStream()
    {
        super(new OutputStream()
        {
            public void write(int paramAnonymousInt)
                throws IOException
            {
                throw new AssertionError();
            }
        });
    }

    private void flush(boolean paramBoolean)
    {
        int i = this.builder.length();
        int k;
        for (int j = 0; j < i; j = k + 1)
        {
            k = this.builder.indexOf("\n", j);
            if (k == -1)
                break;
            log(this.builder.substring(j, k));
        }
        if (paramBoolean)
        {
            if (j < i)
                log(this.builder.substring(j));
            this.builder.setLength(0);
        }
        while (true)
        {
            return;
            this.builder.delete(0, j);
        }
    }

    /** @deprecated */
    public PrintStream append(char paramChar)
    {
        try
        {
            print(paramChar);
            return this;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public PrintStream append(CharSequence paramCharSequence)
    {
        try
        {
            this.builder.append(paramCharSequence);
            flush(false);
            return this;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public PrintStream append(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
        try
        {
            this.builder.append(paramCharSequence, paramInt1, paramInt2);
            flush(false);
            return this;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean checkError()
    {
        return false;
    }

    public void close()
    {
    }

    /** @deprecated */
    public void flush()
    {
        try
        {
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public PrintStream format(String paramString, Object[] paramArrayOfObject)
    {
        return format(Locale.getDefault(), paramString, paramArrayOfObject);
    }

    /** @deprecated */
    public PrintStream format(Locale paramLocale, String paramString, Object[] paramArrayOfObject)
    {
        if (paramString == null)
            try
            {
                throw new NullPointerException("format");
            }
            finally
            {
            }
        this.formatter.format(paramLocale, paramString, paramArrayOfObject);
        flush(false);
        return this;
    }

    protected abstract void log(String paramString);

    /** @deprecated */
    public void print(char paramChar)
    {
        try
        {
            this.builder.append(paramChar);
            if (paramChar == '\n')
                flush(false);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void print(double paramDouble)
    {
        try
        {
            this.builder.append(paramDouble);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void print(float paramFloat)
    {
        try
        {
            this.builder.append(paramFloat);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void print(int paramInt)
    {
        try
        {
            this.builder.append(paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void print(long paramLong)
    {
        try
        {
            this.builder.append(paramLong);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void print(Object paramObject)
    {
        try
        {
            this.builder.append(paramObject);
            flush(false);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void print(String paramString)
    {
        try
        {
            this.builder.append(paramString);
            flush(false);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void print(boolean paramBoolean)
    {
        try
        {
            this.builder.append(paramBoolean);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void print(char[] paramArrayOfChar)
    {
        try
        {
            this.builder.append(paramArrayOfChar);
            flush(false);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public PrintStream printf(String paramString, Object[] paramArrayOfObject)
    {
        return format(paramString, paramArrayOfObject);
    }

    public PrintStream printf(Locale paramLocale, String paramString, Object[] paramArrayOfObject)
    {
        return format(paramLocale, paramString, paramArrayOfObject);
    }

    /** @deprecated */
    public void println()
    {
        try
        {
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void println(char paramChar)
    {
        try
        {
            this.builder.append(paramChar);
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void println(double paramDouble)
    {
        try
        {
            this.builder.append(paramDouble);
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void println(float paramFloat)
    {
        try
        {
            this.builder.append(paramFloat);
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void println(int paramInt)
    {
        try
        {
            this.builder.append(paramInt);
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void println(long paramLong)
    {
        try
        {
            this.builder.append(paramLong);
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void println(Object paramObject)
    {
        try
        {
            this.builder.append(paramObject);
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void println(String paramString)
    {
        try
        {
            if (this.builder.length() == 0)
            {
                int i = paramString.length();
                int k;
                for (int j = 0; j < i; j = k + 1)
                {
                    k = paramString.indexOf('\n', j);
                    if (k == -1)
                        break;
                    log(paramString.substring(j, k));
                }
                if (j < i)
                    log(paramString.substring(j));
            }
            while (true)
            {
                return;
                this.builder.append(paramString);
                flush(true);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void println(boolean paramBoolean)
    {
        try
        {
            this.builder.append(paramBoolean);
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void println(char[] paramArrayOfChar)
    {
        try
        {
            this.builder.append(paramArrayOfChar);
            flush(true);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void setError()
    {
    }

    public void write(int paramInt)
    {
        byte[] arrayOfByte = new byte[1];
        arrayOfByte[0] = ((byte)paramInt);
        write(arrayOfByte, 0, 1);
    }

    public void write(byte[] paramArrayOfByte)
    {
        write(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    /** @deprecated */
    public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        while (true)
        {
            try
            {
                if (this.decoder != null)
                    break label179;
                this.encodedBytes = ByteBuffer.allocate(80);
                this.decodedChars = CharBuffer.allocate(80);
                this.decoder = Charset.defaultCharset().newDecoder().onMalformedInput(CodingErrorAction.REPLACE).onUnmappableCharacter(CodingErrorAction.REPLACE);
                break label179;
                if (paramInt1 < i)
                {
                    int j = Math.min(this.encodedBytes.remaining(), i - paramInt1);
                    this.encodedBytes.put(paramArrayOfByte, paramInt1, j);
                    paramInt1 += j;
                    this.encodedBytes.flip();
                    CoderResult localCoderResult = this.decoder.decode(this.encodedBytes, this.decodedChars, false);
                    this.decodedChars.flip();
                    this.builder.append(this.decodedChars);
                    this.decodedChars.clear();
                    if (localCoderResult.isOverflow())
                        continue;
                    this.encodedBytes.compact();
                    continue;
                }
            }
            finally
            {
            }
            flush(false);
            return;
            label179: int i = paramInt1 + paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.LoggingPrintStream
 * JD-Core Version:        0.6.2
 */