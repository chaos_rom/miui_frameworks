package com.android.internal.os;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IResultReceiver extends IInterface
{
    public abstract void send(int paramInt, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IResultReceiver
    {
        private static final String DESCRIPTOR = "com.android.internal.os.IResultReceiver";
        static final int TRANSACTION_send = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.os.IResultReceiver");
        }

        public static IResultReceiver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.os.IResultReceiver");
                if ((localIInterface != null) && ((localIInterface instanceof IResultReceiver)))
                    localObject = (IResultReceiver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("com.android.internal.os.IResultReceiver");
                }
            case 1:
            }
            paramParcel1.enforceInterface("com.android.internal.os.IResultReceiver");
            int i = paramParcel1.readInt();
            if (paramParcel1.readInt() != 0);
            for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
            {
                send(i, localBundle);
                break;
            }
        }

        private static class Proxy
            implements IResultReceiver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.os.IResultReceiver";
            }

            public void send(int paramInt, Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.os.IResultReceiver");
                    localParcel.writeInt(paramInt);
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.IResultReceiver
 * JD-Core Version:        0.6.2
 */