package com.android.internal.os;

import android.content.Context;
import java.util.HashMap;

public class PowerProfile
{
    private static final String ATTR_NAME = "name";
    public static final String POWER_AUDIO = "dsp.audio";
    public static final String POWER_BATTERY_CAPACITY = "battery.capacity";
    public static final String POWER_BLUETOOTH_ACTIVE = "bluetooth.active";
    public static final String POWER_BLUETOOTH_AT_CMD = "bluetooth.at";
    public static final String POWER_BLUETOOTH_ON = "bluetooth.on";
    public static final String POWER_CPU_ACTIVE = "cpu.active";
    public static final String POWER_CPU_AWAKE = "cpu.awake";
    public static final String POWER_CPU_IDLE = "cpu.idle";
    public static final String POWER_CPU_SPEEDS = "cpu.speeds";
    public static final String POWER_GPS_ON = "gps.on";
    public static final String POWER_NONE = "none";
    public static final String POWER_RADIO_ACTIVE = "radio.active";
    public static final String POWER_RADIO_ON = "radio.on";
    public static final String POWER_RADIO_SCANNING = "radio.scanning";
    public static final String POWER_SCREEN_FULL = "screen.full";
    public static final String POWER_SCREEN_ON = "screen.on";
    public static final String POWER_VIDEO = "dsp.video";
    public static final String POWER_WIFI_ACTIVE = "wifi.active";
    public static final String POWER_WIFI_ON = "wifi.on";
    public static final String POWER_WIFI_SCAN = "wifi.scan";
    private static final String TAG_ARRAY = "array";
    private static final String TAG_ARRAYITEM = "value";
    private static final String TAG_DEVICE = "device";
    private static final String TAG_ITEM = "item";
    static final HashMap<String, Object> sPowerMap = new HashMap();

    public PowerProfile(Context paramContext)
    {
        if (sPowerMap.size() == 0)
            readPowerValuesFromXml(paramContext);
    }

    // ERROR //
    private void readPowerValuesFromXml(Context paramContext)
    {
        // Byte code:
        //     0: aload_1
        //     1: invokevirtual 113	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     4: ldc 114
        //     6: invokevirtual 120	android/content/res/Resources:getXml	(I)Landroid/content/res/XmlResourceParser;
        //     9: astore_2
        //     10: iconst_0
        //     11: istore_3
        //     12: new 122	java/util/ArrayList
        //     15: dup
        //     16: invokespecial 123	java/util/ArrayList:<init>	()V
        //     19: astore 4
        //     21: aconst_null
        //     22: astore 5
        //     24: aload_2
        //     25: ldc 77
        //     27: invokestatic 129	com/android/internal/util/XmlUtils:beginDocument	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
        //     30: aload_2
        //     31: invokestatic 133	com/android/internal/util/XmlUtils:nextElement	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     34: aload_2
        //     35: invokeinterface 139 1 0
        //     40: astore 9
        //     42: aload 9
        //     44: ifnonnull +36 -> 80
        //     47: iload_3
        //     48: ifeq +25 -> 73
        //     51: getstatic 92	com/android/internal/os/PowerProfile:sPowerMap	Ljava/util/HashMap;
        //     54: aload 5
        //     56: aload 4
        //     58: aload 4
        //     60: invokevirtual 140	java/util/ArrayList:size	()I
        //     63: anewarray 142	java/lang/Double
        //     66: invokevirtual 146	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     69: invokevirtual 150	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     72: pop
        //     73: aload_2
        //     74: invokeinterface 153 1 0
        //     79: return
        //     80: iload_3
        //     81: ifeq +37 -> 118
        //     84: aload 9
        //     86: ldc 74
        //     88: invokevirtual 159	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     91: ifne +27 -> 118
        //     94: getstatic 92	com/android/internal/os/PowerProfile:sPowerMap	Ljava/util/HashMap;
        //     97: aload 5
        //     99: aload 4
        //     101: aload 4
        //     103: invokevirtual 140	java/util/ArrayList:size	()I
        //     106: anewarray 142	java/lang/Double
        //     109: invokevirtual 146	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     112: invokevirtual 150	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     115: pop
        //     116: iconst_0
        //     117: istore_3
        //     118: aload 9
        //     120: ldc 71
        //     122: invokevirtual 159	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     125: ifeq +24 -> 149
        //     128: iconst_1
        //     129: istore_3
        //     130: aload 4
        //     132: invokevirtual 162	java/util/ArrayList:clear	()V
        //     135: aload_2
        //     136: aconst_null
        //     137: ldc 8
        //     139: invokeinterface 166 3 0
        //     144: astore 5
        //     146: goto -116 -> 30
        //     149: aload 9
        //     151: ldc 80
        //     153: invokevirtual 159	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     156: ifne +13 -> 169
        //     159: aload 9
        //     161: ldc 74
        //     163: invokevirtual 159	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     166: ifeq -136 -> 30
        //     169: aconst_null
        //     170: astore 10
        //     172: iload_3
        //     173: ifne +14 -> 187
        //     176: aload_2
        //     177: aconst_null
        //     178: ldc 8
        //     180: invokeinterface 166 3 0
        //     185: astore 10
        //     187: aload_2
        //     188: invokeinterface 169 1 0
        //     193: iconst_4
        //     194: if_icmpne -164 -> 30
        //     197: aload_2
        //     198: invokeinterface 172 1 0
        //     203: astore 11
        //     205: dconst_0
        //     206: dstore 12
        //     208: aload 11
        //     210: invokestatic 176	java/lang/Double:valueOf	(Ljava/lang/String;)Ljava/lang/Double;
        //     213: invokevirtual 180	java/lang/Double:doubleValue	()D
        //     216: dstore 17
        //     218: dload 17
        //     220: dstore 12
        //     222: aload 9
        //     224: ldc 80
        //     226: invokevirtual 159	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     229: ifeq +43 -> 272
        //     232: getstatic 92	com/android/internal/os/PowerProfile:sPowerMap	Ljava/util/HashMap;
        //     235: aload 10
        //     237: dload 12
        //     239: invokestatic 183	java/lang/Double:valueOf	(D)Ljava/lang/Double;
        //     242: invokevirtual 150	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     245: pop
        //     246: goto -216 -> 30
        //     249: astore 8
        //     251: new 185	java/lang/RuntimeException
        //     254: dup
        //     255: aload 8
        //     257: invokespecial 188	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     260: athrow
        //     261: astore 7
        //     263: aload_2
        //     264: invokeinterface 153 1 0
        //     269: aload 7
        //     271: athrow
        //     272: iload_3
        //     273: ifeq -243 -> 30
        //     276: aload 4
        //     278: dload 12
        //     280: invokestatic 183	java/lang/Double:valueOf	(D)Ljava/lang/Double;
        //     283: invokevirtual 191	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     286: pop
        //     287: goto -257 -> 30
        //     290: astore 6
        //     292: new 185	java/lang/RuntimeException
        //     295: dup
        //     296: aload 6
        //     298: invokespecial 188	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     301: athrow
        //     302: astore 14
        //     304: goto -82 -> 222
        //
        // Exception table:
        //     from	to	target	type
        //     24	73	249	org/xmlpull/v1/XmlPullParserException
        //     84	205	249	org/xmlpull/v1/XmlPullParserException
        //     208	218	249	org/xmlpull/v1/XmlPullParserException
        //     222	246	249	org/xmlpull/v1/XmlPullParserException
        //     276	287	249	org/xmlpull/v1/XmlPullParserException
        //     24	73	261	finally
        //     84	205	261	finally
        //     208	218	261	finally
        //     222	246	261	finally
        //     251	261	261	finally
        //     276	287	261	finally
        //     292	302	261	finally
        //     24	73	290	java/io/IOException
        //     84	205	290	java/io/IOException
        //     208	218	290	java/io/IOException
        //     222	246	290	java/io/IOException
        //     276	287	290	java/io/IOException
        //     208	218	302	java/lang/NumberFormatException
    }

    public double getAveragePower(String paramString)
    {
        double d;
        if (sPowerMap.containsKey(paramString))
        {
            Object localObject = sPowerMap.get(paramString);
            if ((localObject instanceof Double[]))
                d = ((Double[])(Double[])localObject)[0].doubleValue();
        }
        while (true)
        {
            return d;
            d = ((Double)sPowerMap.get(paramString)).doubleValue();
            continue;
            d = 0.0D;
        }
    }

    public double getAveragePower(String paramString, int paramInt)
    {
        double d = 0.0D;
        Object localObject;
        Double[] arrayOfDouble;
        if (sPowerMap.containsKey(paramString))
        {
            localObject = sPowerMap.get(paramString);
            if (!(localObject instanceof Double[]))
                break label80;
            arrayOfDouble = (Double[])localObject;
            if ((arrayOfDouble.length <= paramInt) || (paramInt < 0))
                break label60;
            d = arrayOfDouble[paramInt].doubleValue();
        }
        while (true)
        {
            return d;
            label60: if (paramInt >= 0)
            {
                d = arrayOfDouble[(-1 + arrayOfDouble.length)].doubleValue();
                continue;
                label80: d = ((Double)localObject).doubleValue();
            }
        }
    }

    public double getBatteryCapacity()
    {
        return getAveragePower("battery.capacity");
    }

    public int getNumSpeedSteps()
    {
        Object localObject = sPowerMap.get("cpu.speeds");
        if ((localObject != null) && ((localObject instanceof Double[])));
        for (int i = ((Double[])localObject).length; ; i = 1)
            return i;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.PowerProfile
 * JD-Core Version:        0.6.2
 */