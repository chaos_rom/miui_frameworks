package com.android.internal.os;

import android.util.Log;

class AndroidPrintStream extends LoggingPrintStream
{
    private final int priority;
    private final String tag;

    public AndroidPrintStream(int paramInt, String paramString)
    {
        if (paramString == null)
            throw new NullPointerException("tag");
        this.priority = paramInt;
        this.tag = paramString;
    }

    protected void log(String paramString)
    {
        Log.println(this.priority, this.tag, paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.AndroidPrintStream
 * JD-Core Version:        0.6.2
 */