package com.android.internal.os;

import android.os.Binder;
import android.os.DropBoxManager.Entry;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IDropBoxManagerService extends IInterface
{
    public abstract void add(DropBoxManager.Entry paramEntry)
        throws RemoteException;

    public abstract DropBoxManager.Entry getNextEntry(String paramString, long paramLong)
        throws RemoteException;

    public abstract boolean isTagEnabled(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IDropBoxManagerService
    {
        private static final String DESCRIPTOR = "com.android.internal.os.IDropBoxManagerService";
        static final int TRANSACTION_add = 1;
        static final int TRANSACTION_getNextEntry = 3;
        static final int TRANSACTION_isTagEnabled = 2;

        public Stub()
        {
            attachInterface(this, "com.android.internal.os.IDropBoxManagerService");
        }

        public static IDropBoxManagerService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.os.IDropBoxManagerService");
                if ((localIInterface != null) && ((localIInterface instanceof IDropBoxManagerService)))
                    localObject = (IDropBoxManagerService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("com.android.internal.os.IDropBoxManagerService");
                continue;
                paramParcel1.enforceInterface("com.android.internal.os.IDropBoxManagerService");
                if (paramParcel1.readInt() != 0);
                for (DropBoxManager.Entry localEntry2 = (DropBoxManager.Entry)DropBoxManager.Entry.CREATOR.createFromParcel(paramParcel1); ; localEntry2 = null)
                {
                    add(localEntry2);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("com.android.internal.os.IDropBoxManagerService");
                boolean bool = isTagEnabled(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("com.android.internal.os.IDropBoxManagerService");
                DropBoxManager.Entry localEntry1 = getNextEntry(paramParcel1.readString(), paramParcel1.readLong());
                paramParcel2.writeNoException();
                if (localEntry1 != null)
                {
                    paramParcel2.writeInt(j);
                    localEntry1.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                }
            }
        }

        private static class Proxy
            implements IDropBoxManagerService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void add(DropBoxManager.Entry paramEntry)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.os.IDropBoxManagerService");
                    if (paramEntry != null)
                    {
                        localParcel1.writeInt(1);
                        paramEntry.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.os.IDropBoxManagerService";
            }

            public DropBoxManager.Entry getNextEntry(String paramString, long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.os.IDropBoxManagerService");
                    localParcel1.writeString(paramString);
                    localParcel1.writeLong(paramLong);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localEntry = (DropBoxManager.Entry)DropBoxManager.Entry.CREATOR.createFromParcel(localParcel2);
                        return localEntry;
                    }
                    DropBoxManager.Entry localEntry = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isTagEnabled(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.os.IDropBoxManagerService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.IDropBoxManagerService
 * JD-Core Version:        0.6.2
 */