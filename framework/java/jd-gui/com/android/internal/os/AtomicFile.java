package com.android.internal.os;

import android.os.FileUtils;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AtomicFile
{
    private final File mBackupName;
    private final File mBaseName;

    public AtomicFile(File paramFile)
    {
        this.mBaseName = paramFile;
        this.mBackupName = new File(paramFile.getPath() + ".bak");
    }

    public void failWrite(FileOutputStream paramFileOutputStream)
    {
        if (paramFileOutputStream != null)
            FileUtils.sync(paramFileOutputStream);
        try
        {
            paramFileOutputStream.close();
            this.mBaseName.delete();
            this.mBackupName.renameTo(this.mBaseName);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.w("AtomicFile", "failWrite: Got exception:", localIOException);
        }
    }

    public void finishWrite(FileOutputStream paramFileOutputStream)
    {
        if (paramFileOutputStream != null)
            FileUtils.sync(paramFileOutputStream);
        try
        {
            paramFileOutputStream.close();
            this.mBackupName.delete();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.w("AtomicFile", "finishWrite: Got exception:", localIOException);
        }
    }

    public File getBaseFile()
    {
        return this.mBaseName;
    }

    public FileOutputStream openAppend()
        throws IOException
    {
        try
        {
            FileOutputStream localFileOutputStream = new FileOutputStream(this.mBaseName, true);
            return localFileOutputStream;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
        }
        throw new IOException("Couldn't append " + this.mBaseName);
    }

    public FileInputStream openRead()
        throws FileNotFoundException
    {
        if (this.mBackupName.exists())
        {
            this.mBaseName.delete();
            this.mBackupName.renameTo(this.mBaseName);
        }
        return new FileInputStream(this.mBaseName);
    }

    public byte[] readFully()
        throws IOException
    {
        FileInputStream localFileInputStream = openRead();
        int i = 0;
        try
        {
            Object localObject2 = new byte[localFileInputStream.available()];
            while (true)
            {
                int j = localFileInputStream.read((byte[])localObject2, i, localObject2.length - i);
                if (j <= 0)
                    return localObject2;
                i += j;
                int k = localFileInputStream.available();
                if (k > localObject2.length - i)
                {
                    byte[] arrayOfByte = new byte[i + k];
                    System.arraycopy(localObject2, 0, arrayOfByte, 0, i);
                    localObject2 = arrayOfByte;
                }
            }
        }
        finally
        {
            localFileInputStream.close();
        }
    }

    public FileOutputStream startWrite()
        throws IOException
    {
        if (this.mBaseName.exists())
        {
            if (this.mBackupName.exists())
                break label88;
            if (!this.mBaseName.renameTo(this.mBackupName))
                Log.w("AtomicFile", "Couldn't rename file " + this.mBaseName + " to backup file " + this.mBackupName);
        }
        try
        {
            while (true)
            {
                localFileOutputStream = new FileOutputStream(this.mBaseName);
                return localFileOutputStream;
                label88: this.mBaseName.delete();
            }
        }
        catch (FileNotFoundException localFileNotFoundException1)
        {
            while (true)
            {
                FileOutputStream localFileOutputStream;
                File localFile = this.mBaseName.getParentFile();
                if (!localFile.mkdir())
                    throw new IOException("Couldn't create directory " + this.mBaseName);
                FileUtils.setPermissions(localFile.getPath(), 505, -1, -1);
                try
                {
                    localFileOutputStream = new FileOutputStream(this.mBaseName);
                }
                catch (FileNotFoundException localFileNotFoundException2)
                {
                }
            }
        }
        throw new IOException("Couldn't create " + this.mBaseName);
    }

    public void truncate()
        throws IOException
    {
        try
        {
            FileOutputStream localFileOutputStream = new FileOutputStream(this.mBaseName);
            FileUtils.sync(localFileOutputStream);
            localFileOutputStream.close();
            label21: return;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            throw new IOException("Couldn't append " + this.mBaseName);
        }
        catch (IOException localIOException)
        {
            break label21;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.AtomicFile
 * JD-Core Version:        0.6.2
 */