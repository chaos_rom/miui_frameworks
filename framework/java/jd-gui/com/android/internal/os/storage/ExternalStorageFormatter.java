package com.android.internal.os.storage;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.storage.IMountService;
import android.os.storage.IMountService.Stub;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;
import java.io.File;

public class ExternalStorageFormatter extends Service
    implements DialogInterface.OnCancelListener
{
    public static final ComponentName COMPONENT_NAME = new ComponentName("android", ExternalStorageFormatter.class.getName());
    public static final String EXTRA_ALWAYS_RESET = "always_reset";
    public static final String FORMAT_AND_FACTORY_RESET = "com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET";
    public static final String FORMAT_ONLY = "com.android.internal.os.storage.FORMAT_ONLY";
    static final String TAG = "ExternalStorageFormatter";
    private boolean mAlwaysReset = false;
    private boolean mFactoryReset = false;
    private IMountService mMountService = null;
    private ProgressDialog mProgressDialog = null;
    StorageEventListener mStorageListener = new StorageEventListener()
    {
        public void onStorageStateChanged(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3)
        {
            Log.i("ExternalStorageFormatter", "Received storage state changed notification that " + paramAnonymousString1 + " changed state from " + paramAnonymousString2 + " to " + paramAnonymousString3);
            ExternalStorageFormatter.this.updateProgressState();
        }
    };
    private StorageManager mStorageManager = null;
    private StorageVolume mStorageVolume;
    private PowerManager.WakeLock mWakeLock;

    void fail(int paramInt)
    {
        Toast.makeText(this, paramInt, 1).show();
        if (this.mAlwaysReset)
            sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
        stopSelf();
    }

    IMountService getMountService()
    {
        if (this.mMountService == null)
        {
            IBinder localIBinder = ServiceManager.getService("mount");
            if (localIBinder == null)
                break label30;
            this.mMountService = IMountService.Stub.asInterface(localIBinder);
        }
        while (true)
        {
            return this.mMountService;
            label30: Log.e("ExternalStorageFormatter", "Can't get mount service");
        }
    }

    public IBinder onBind(Intent paramIntent)
    {
        return null;
    }

    public void onCancel(DialogInterface paramDialogInterface)
    {
        IMountService localIMountService = getMountService();
        String str;
        if (this.mStorageVolume == null)
            str = Environment.getExternalStorageDirectory().toString();
        try
        {
            while (true)
            {
                localIMountService.mountVolume(str);
                stopSelf();
                return;
                str = this.mStorageVolume.getPath();
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("ExternalStorageFormatter", "Failed talking with mount service", localRemoteException);
        }
    }

    public void onCreate()
    {
        super.onCreate();
        if (this.mStorageManager == null)
        {
            this.mStorageManager = ((StorageManager)getSystemService("storage"));
            this.mStorageManager.registerListener(this.mStorageListener);
        }
        this.mWakeLock = ((PowerManager)getSystemService("power")).newWakeLock(1, "ExternalStorageFormatter");
        this.mWakeLock.acquire();
    }

    public void onDestroy()
    {
        if (this.mStorageManager != null)
            this.mStorageManager.unregisterListener(this.mStorageListener);
        if (this.mProgressDialog != null)
            this.mProgressDialog.dismiss();
        this.mWakeLock.release();
        super.onDestroy();
    }

    public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
    {
        if ("com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET".equals(paramIntent.getAction()))
            this.mFactoryReset = true;
        if (paramIntent.getBooleanExtra("always_reset", false))
            this.mAlwaysReset = true;
        this.mStorageVolume = ((StorageVolume)paramIntent.getParcelableExtra("storage_volume"));
        if (this.mProgressDialog == null)
        {
            this.mProgressDialog = new ProgressDialog(this);
            this.mProgressDialog.setIndeterminate(true);
            this.mProgressDialog.setCancelable(true);
            this.mProgressDialog.getWindow().setType(2003);
            if (!this.mAlwaysReset)
                this.mProgressDialog.setOnCancelListener(this);
            updateProgressState();
            this.mProgressDialog.show();
        }
        return 3;
    }

    public void updateProgressDialog(int paramInt)
    {
        if (this.mProgressDialog == null)
        {
            this.mProgressDialog = new ProgressDialog(this);
            this.mProgressDialog.setIndeterminate(true);
            this.mProgressDialog.setCancelable(false);
            this.mProgressDialog.getWindow().setType(2003);
            this.mProgressDialog.show();
        }
        this.mProgressDialog.setMessage(getText(paramInt));
    }

    void updateProgressState()
    {
        String str1;
        IMountService localIMountService1;
        String str2;
        if (this.mStorageVolume == null)
        {
            str1 = Environment.getExternalStorageState();
            if ((!"mounted".equals(str1)) && (!"mounted_ro".equals(str1)))
                break label114;
            updateProgressDialog(17040529);
            localIMountService1 = getMountService();
            if (this.mStorageVolume != null)
                break label88;
            str2 = Environment.getExternalStorageDirectory().toString();
        }
        label207: 
        while (true)
        {
            try
            {
                localIMountService1.unmountVolume(str2, true, this.mFactoryReset);
                return;
                str1 = this.mStorageManager.getVolumeState(this.mStorageVolume.getPath());
                break;
                label88: str2 = this.mStorageVolume.getPath();
                continue;
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("ExternalStorageFormatter", "Failed talking with mount service", localRemoteException);
                continue;
            }
            label114: if (("nofs".equals(str1)) || ("unmounted".equals(str1)) || ("unmountable".equals(str1)))
            {
                updateProgressDialog(17040530);
                final IMountService localIMountService2 = getMountService();
                if (this.mStorageVolume == null);
                for (final String str3 = Environment.getExternalStorageDirectory().toString(); ; str3 = this.mStorageVolume.getPath())
                {
                    if (localIMountService2 == null)
                        break label207;
                    new Thread()
                    {
                        public void run()
                        {
                            int i = 0;
                            try
                            {
                                localIMountService2.formatVolume(str3);
                                i = 1;
                                if ((i != 0) && (ExternalStorageFormatter.this.mFactoryReset))
                                {
                                    ExternalStorageFormatter.this.sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
                                    ExternalStorageFormatter.this.stopSelf();
                                    return;
                                }
                            }
                            catch (Exception localException)
                            {
                                while (true)
                                    Toast.makeText(ExternalStorageFormatter.this, 17040531, 1).show();
                                if (i != 0)
                                    break label113;
                            }
                            if (ExternalStorageFormatter.this.mAlwaysReset)
                                ExternalStorageFormatter.this.sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
                            while (true)
                            {
                                ExternalStorageFormatter.this.stopSelf();
                                break;
                                try
                                {
                                    label113: localIMountService2.mountVolume(str3);
                                }
                                catch (RemoteException localRemoteException)
                                {
                                    Log.w("ExternalStorageFormatter", "Failed talking with mount service", localRemoteException);
                                }
                            }
                        }
                    }
                    .start();
                    break;
                }
                Log.w("ExternalStorageFormatter", "Unable to locate IMountService");
            }
            else if ("bad_removal".equals(str1))
            {
                fail(17040532);
            }
            else if ("checking".equals(str1))
            {
                fail(17040533);
            }
            else if ("removed".equals(str1))
            {
                fail(17040534);
            }
            else if ("shared".equals(str1))
            {
                fail(17040535);
            }
            else
            {
                fail(17040536);
                Log.w("ExternalStorageFormatter", "Unknown storage state: " + str1);
                stopSelf();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.storage.ExternalStorageFormatter
 * JD-Core Version:        0.6.2
 */