package com.android.internal.os;

import android.net.Credentials;
import android.net.LocalSocket;
import android.os.SystemProperties;
import android.util.Log;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

class ZygoteConnection
{
    private static final int CONNECTION_TIMEOUT_MILLIS = 1000;
    private static final int MAX_ZYGOTE_ARGC = 1024;
    private static final String TAG = "Zygote";
    private static final int[][] intArray2d = (int[][])Array.newInstance(Integer.TYPE, arrayOfInt);
    private static LocalSocket sPeerWaitSocket = null;
    private final LocalSocket mSocket;
    private final DataOutputStream mSocketOutStream;
    private final BufferedReader mSocketReader;
    private final Credentials peer;

    static
    {
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
    }

    ZygoteConnection(LocalSocket paramLocalSocket)
        throws IOException
    {
        this.mSocket = paramLocalSocket;
        this.mSocketOutStream = new DataOutputStream(paramLocalSocket.getOutputStream());
        this.mSocketReader = new BufferedReader(new InputStreamReader(paramLocalSocket.getInputStream()), 256);
        this.mSocket.setSoTimeout(1000);
        try
        {
            this.peer = this.mSocket.getPeerCredentials();
            return;
        }
        catch (IOException localIOException)
        {
            Log.e("Zygote", "Cannot read peer credentials", localIOException);
            throw localIOException;
        }
    }

    private static void applyCapabilitiesSecurityPolicy(Arguments paramArguments, Credentials paramCredentials)
        throws ZygoteSecurityException
    {
        if ((paramArguments.permittedCapabilities == 0L) && (paramArguments.effectiveCapabilities == 0L));
        long l;
        do
        {
            do
                return;
            while (paramCredentials.getUid() == 0);
            try
            {
                l = ZygoteInit.capgetPermitted(paramCredentials.getPid());
                if (((0xFFFFFFFF ^ paramArguments.permittedCapabilities) & paramArguments.effectiveCapabilities) != 0L)
                    throw new ZygoteSecurityException("Effective capabilities cannot be superset of    permitted capabilities");
            }
            catch (IOException localIOException)
            {
                throw new ZygoteSecurityException("Error retrieving peer's capabilities.");
            }
        }
        while (((l ^ 0xFFFFFFFF) & paramArguments.permittedCapabilities) == 0L);
        throw new ZygoteSecurityException("Peer specified unpermitted capabilities");
    }

    public static void applyDebuggerSystemProperty(Arguments paramArguments)
    {
        if ("1".equals(SystemProperties.get("ro.debuggable")))
            paramArguments.debugFlags = (0x1 | paramArguments.debugFlags);
    }

    private static void applyInvokeWithSecurityPolicy(Arguments paramArguments, Credentials paramCredentials)
        throws ZygoteSecurityException
    {
        int i = paramCredentials.getUid();
        if ((paramArguments.invokeWith != null) && (i != 0))
            throw new ZygoteSecurityException("Peer is not permitted to specify an explicit invoke-with wrapper command");
    }

    public static void applyInvokeWithSystemProperty(Arguments paramArguments)
    {
        if ((paramArguments.invokeWith == null) && (paramArguments.niceName != null) && (paramArguments.niceName != null))
        {
            String str = "wrap." + paramArguments.niceName;
            if (str.length() > 31)
                str = str.substring(0, 31);
            paramArguments.invokeWith = SystemProperties.get(str);
            if ((paramArguments.invokeWith != null) && (paramArguments.invokeWith.length() == 0))
                paramArguments.invokeWith = null;
        }
    }

    private static void applyRlimitSecurityPolicy(Arguments paramArguments, Credentials paramCredentials)
        throws ZygoteSecurityException
    {
        int i = paramCredentials.getUid();
        if ((i != 0) && (i != 1000) && (paramArguments.rlimits != null))
            throw new ZygoteSecurityException("This UID may not specify rlimits.");
    }

    private static void applyUidSecurityPolicy(Arguments paramArguments, Credentials paramCredentials)
        throws ZygoteSecurityException
    {
        int i = paramCredentials.getUid();
        if (i == 0);
        do
            while (true)
            {
                if (!paramArguments.uidSpecified)
                {
                    paramArguments.uid = paramCredentials.getUid();
                    paramArguments.uidSpecified = true;
                }
                if (!paramArguments.gidSpecified)
                {
                    paramArguments.gid = paramCredentials.getGid();
                    paramArguments.gidSpecified = true;
                }
                return;
                if (i != 1000)
                    break;
                String str = SystemProperties.get("ro.factorytest");
                if ((!str.equals("1")) && (!str.equals("2")));
                for (int j = 1; (j != 0) && (paramArguments.uidSpecified) && (paramArguments.uid < 1000); j = 0)
                    throw new ZygoteSecurityException("System UID may not launch process with UID < 1000");
            }
        while ((!paramArguments.uidSpecified) && (!paramArguments.gidSpecified) && (paramArguments.gids == null));
        throw new ZygoteSecurityException("App UIDs may not specify uid's or gid's");
    }

    // ERROR //
    private void handleChildProc(Arguments paramArguments, FileDescriptor[] paramArrayOfFileDescriptor, FileDescriptor paramFileDescriptor, PrintStream paramPrintStream)
        throws ZygoteInit.MethodAndArgsCaller
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 236	com/android/internal/os/ZygoteConnection$Arguments:peerWait	Z
        //     4: ifeq +80 -> 84
        //     7: aload_0
        //     8: getfield 54	com/android/internal/os/ZygoteConnection:mSocket	Landroid/net/LocalSocket;
        //     11: invokevirtual 240	android/net/LocalSocket:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     14: iconst_1
        //     15: invokestatic 244	com/android/internal/os/ZygoteInit:setCloseOnExec	(Ljava/io/FileDescriptor;Z)V
        //     18: aload_0
        //     19: getfield 54	com/android/internal/os/ZygoteConnection:mSocket	Landroid/net/LocalSocket;
        //     22: putstatic 46	com/android/internal/os/ZygoteConnection:sPeerWaitSocket	Landroid/net/LocalSocket;
        //     25: aload_2
        //     26: ifnull +73 -> 99
        //     29: aload_2
        //     30: iconst_0
        //     31: aaload
        //     32: aload_2
        //     33: iconst_1
        //     34: aaload
        //     35: aload_2
        //     36: iconst_2
        //     37: aaload
        //     38: invokestatic 248	com/android/internal/os/ZygoteInit:reopenStdio	(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;)V
        //     41: aload_2
        //     42: arraylength
        //     43: istore 12
        //     45: iconst_0
        //     46: istore 13
        //     48: iload 13
        //     50: iload 12
        //     52: if_icmpge +42 -> 94
        //     55: aload_2
        //     56: iload 13
        //     58: aaload
        //     59: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     62: iinc 13 1
        //     65: goto -17 -> 48
        //     68: astore 14
        //     70: ldc 16
        //     72: ldc_w 256
        //     75: aload 14
        //     77: invokestatic 101	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     80: pop
        //     81: goto -56 -> 25
        //     84: aload_0
        //     85: invokevirtual 259	com/android/internal/os/ZygoteConnection:closeSocket	()V
        //     88: invokestatic 262	com/android/internal/os/ZygoteInit:closeServerSocket	()V
        //     91: goto -66 -> 25
        //     94: getstatic 268	java/lang/System:err	Ljava/io/PrintStream;
        //     97: astore 4
        //     99: aload_1
        //     100: getfield 169	com/android/internal/os/ZygoteConnection$Arguments:niceName	Ljava/lang/String;
        //     103: ifnull +10 -> 113
        //     106: aload_1
        //     107: getfield 169	com/android/internal/os/ZygoteConnection$Arguments:niceName	Ljava/lang/String;
        //     110: invokestatic 273	android/os/Process:setArgV0	(Ljava/lang/String;)V
        //     113: aload_1
        //     114: getfield 276	com/android/internal/os/ZygoteConnection$Arguments:runtimeInit	Z
        //     117: ifeq +61 -> 178
        //     120: aload_1
        //     121: getfield 163	com/android/internal/os/ZygoteConnection$Arguments:invokeWith	Ljava/lang/String;
        //     124: ifnull +40 -> 164
        //     127: aload_1
        //     128: getfield 163	com/android/internal/os/ZygoteConnection$Arguments:invokeWith	Ljava/lang/String;
        //     131: aload_1
        //     132: getfield 169	com/android/internal/os/ZygoteConnection$Arguments:niceName	Ljava/lang/String;
        //     135: aload_1
        //     136: getfield 279	com/android/internal/os/ZygoteConnection$Arguments:targetSdkVersion	I
        //     139: aload_3
        //     140: aload_1
        //     141: getfield 283	com/android/internal/os/ZygoteConnection$Arguments:remainingArgs	[Ljava/lang/String;
        //     144: invokestatic 289	com/android/internal/os/WrapperInit:execApplication	(Ljava/lang/String;Ljava/lang/String;ILjava/io/FileDescriptor;[Ljava/lang/String;)V
        //     147: return
        //     148: astore 10
        //     150: ldc 16
        //     152: ldc_w 291
        //     155: aload 10
        //     157: invokestatic 101	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     160: pop
        //     161: goto -62 -> 99
        //     164: aload_1
        //     165: getfield 279	com/android/internal/os/ZygoteConnection$Arguments:targetSdkVersion	I
        //     168: aload_1
        //     169: getfield 283	com/android/internal/os/ZygoteConnection$Arguments:remainingArgs	[Ljava/lang/String;
        //     172: invokestatic 297	com/android/internal/os/RuntimeInit:zygoteInit	(I[Ljava/lang/String;)V
        //     175: goto -28 -> 147
        //     178: aload_1
        //     179: getfield 283	com/android/internal/os/ZygoteConnection$Arguments:remainingArgs	[Ljava/lang/String;
        //     182: iconst_0
        //     183: aaload
        //     184: astore 6
        //     186: bipush 255
        //     188: aload_1
        //     189: getfield 283	com/android/internal/os/ZygoteConnection$Arguments:remainingArgs	[Ljava/lang/String;
        //     192: arraylength
        //     193: iadd
        //     194: anewarray 152	java/lang/String
        //     197: astore 7
        //     199: aload_1
        //     200: getfield 283	com/android/internal/os/ZygoteConnection$Arguments:remainingArgs	[Ljava/lang/String;
        //     203: iconst_1
        //     204: aload 7
        //     206: iconst_0
        //     207: aload 7
        //     209: arraylength
        //     210: invokestatic 301	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     213: aload_1
        //     214: getfield 163	com/android/internal/os/ZygoteConnection$Arguments:invokeWith	Ljava/lang/String;
        //     217: ifnull +35 -> 252
        //     220: aload_1
        //     221: getfield 163	com/android/internal/os/ZygoteConnection$Arguments:invokeWith	Ljava/lang/String;
        //     224: aload_1
        //     225: getfield 304	com/android/internal/os/ZygoteConnection$Arguments:classpath	Ljava/lang/String;
        //     228: aload 6
        //     230: aload 7
        //     232: invokestatic 308	com/android/internal/os/WrapperInit:execStandalone	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
        //     235: goto -88 -> 147
        //     238: astore 5
        //     240: aload 4
        //     242: ldc_w 310
        //     245: aconst_null
        //     246: invokestatic 314	com/android/internal/os/ZygoteConnection:logAndPrintError	(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V
        //     249: goto -102 -> 147
        //     252: aload_1
        //     253: getfield 304	com/android/internal/os/ZygoteConnection$Arguments:classpath	Ljava/lang/String;
        //     256: ifnull +46 -> 302
        //     259: new 316	dalvik/system/PathClassLoader
        //     262: dup
        //     263: aload_1
        //     264: getfield 304	com/android/internal/os/ZygoteConnection$Arguments:classpath	Ljava/lang/String;
        //     267: invokestatic 322	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
        //     270: invokespecial 325	dalvik/system/PathClassLoader:<init>	(Ljava/lang/String;Ljava/lang/ClassLoader;)V
        //     273: astore 8
        //     275: aload 8
        //     277: aload 6
        //     279: aload 7
        //     281: invokestatic 329	com/android/internal/os/ZygoteInit:invokeStaticMain	(Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/String;)V
        //     284: goto -137 -> 147
        //     287: astore 9
        //     289: aload 4
        //     291: ldc_w 331
        //     294: aload 9
        //     296: invokestatic 314	com/android/internal/os/ZygoteConnection:logAndPrintError	(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V
        //     299: goto -152 -> 147
        //     302: invokestatic 322	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
        //     305: astore 8
        //     307: goto -32 -> 275
        //
        // Exception table:
        //     from	to	target	type
        //     7	25	68	java/io/IOException
        //     29	62	148	java/io/IOException
        //     94	99	148	java/io/IOException
        //     178	186	238	java/lang/ArrayIndexOutOfBoundsException
        //     275	284	287	java/lang/RuntimeException
    }

    // ERROR //
    private boolean handleParentProc(int paramInt, FileDescriptor[] paramArrayOfFileDescriptor, FileDescriptor paramFileDescriptor, Arguments paramArguments)
    {
        // Byte code:
        //     0: iload_1
        //     1: ifle +8 -> 9
        //     4: aload_0
        //     5: iload_1
        //     6: invokespecial 336	com/android/internal/os/ZygoteConnection:setChildPgid	(I)V
        //     9: aload_2
        //     10: ifnull +30 -> 40
        //     13: aload_2
        //     14: arraylength
        //     15: istore 23
        //     17: iconst_0
        //     18: istore 24
        //     20: iload 24
        //     22: iload 23
        //     24: if_icmpge +16 -> 40
        //     27: aload_2
        //     28: iload 24
        //     30: aaload
        //     31: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     34: iinc 24 1
        //     37: goto -17 -> 20
        //     40: iconst_0
        //     41: istore 5
        //     43: aload_3
        //     44: ifnull +148 -> 192
        //     47: iload_1
        //     48: ifle +144 -> 192
        //     51: new 338	java/io/DataInputStream
        //     54: dup
        //     55: new 340	java/io/FileInputStream
        //     58: dup
        //     59: aload_3
        //     60: invokespecial 342	java/io/FileInputStream:<init>	(Ljava/io/FileDescriptor;)V
        //     63: invokespecial 343	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     66: astore 11
        //     68: bipush 255
        //     70: istore 12
        //     72: aload 11
        //     74: invokevirtual 346	java/io/DataInputStream:readInt	()I
        //     77: istore 21
        //     79: iload 21
        //     81: istore 12
        //     83: aload 11
        //     85: invokevirtual 349	java/io/DataInputStream:close	()V
        //     88: iload 12
        //     90: ifle +102 -> 192
        //     93: iload 12
        //     95: istore 18
        //     97: iload 18
        //     99: ifle +55 -> 154
        //     102: iload 18
        //     104: iload_1
        //     105: if_icmpeq +49 -> 154
        //     108: iload 18
        //     110: invokestatic 353	android/os/Process:getParentPid	(I)I
        //     113: istore 18
        //     115: goto -18 -> 97
        //     118: astore 15
        //     120: ldc 16
        //     122: ldc_w 355
        //     125: aload 15
        //     127: invokestatic 358	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     130: pop
        //     131: aload 11
        //     133: invokevirtual 349	java/io/DataInputStream:close	()V
        //     136: goto -48 -> 88
        //     139: astore 17
        //     141: goto -53 -> 88
        //     144: astore 13
        //     146: aload 11
        //     148: invokevirtual 349	java/io/DataInputStream:close	()V
        //     151: aload 13
        //     153: athrow
        //     154: iload 18
        //     156: ifle +74 -> 230
        //     159: ldc 16
        //     161: new 171	java/lang/StringBuilder
        //     164: dup
        //     165: invokespecial 172	java/lang/StringBuilder:<init>	()V
        //     168: ldc_w 360
        //     171: invokevirtual 178	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     174: iload 12
        //     176: invokevirtual 363	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     179: invokevirtual 182	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     182: invokestatic 367	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     185: pop
        //     186: iload 12
        //     188: istore_1
        //     189: iconst_1
        //     190: istore 5
        //     192: aload_0
        //     193: getfield 67	com/android/internal/os/ZygoteConnection:mSocketOutStream	Ljava/io/DataOutputStream;
        //     196: iload_1
        //     197: invokevirtual 370	java/io/DataOutputStream:writeInt	(I)V
        //     200: aload_0
        //     201: getfield 67	com/android/internal/os/ZygoteConnection:mSocketOutStream	Ljava/io/DataOutputStream;
        //     204: iload 5
        //     206: invokevirtual 374	java/io/DataOutputStream:writeBoolean	(Z)V
        //     209: aload 4
        //     211: getfield 236	com/android/internal/os/ZygoteConnection$Arguments:peerWait	Z
        //     214: ifeq +91 -> 305
        //     217: aload_0
        //     218: getfield 54	com/android/internal/os/ZygoteConnection:mSocket	Landroid/net/LocalSocket;
        //     221: invokevirtual 375	android/net/LocalSocket:close	()V
        //     224: iconst_1
        //     225: istore 8
        //     227: iload 8
        //     229: ireturn
        //     230: ldc 16
        //     232: new 171	java/lang/StringBuilder
        //     235: dup
        //     236: invokespecial 172	java/lang/StringBuilder:<init>	()V
        //     239: ldc_w 377
        //     242: invokevirtual 178	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     245: iload_1
        //     246: invokevirtual 363	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     249: ldc_w 379
        //     252: invokevirtual 178	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     255: iload 12
        //     257: invokevirtual 363	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     260: invokevirtual 182	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     263: invokestatic 381	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     266: pop
        //     267: goto -75 -> 192
        //     270: astore 6
        //     272: ldc 16
        //     274: ldc_w 383
        //     277: aload 6
        //     279: invokestatic 101	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     282: pop
        //     283: iconst_1
        //     284: istore 8
        //     286: goto -59 -> 227
        //     289: astore 9
        //     291: ldc 16
        //     293: ldc_w 385
        //     296: aload 9
        //     298: invokestatic 101	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     301: pop
        //     302: goto -78 -> 224
        //     305: iconst_0
        //     306: istore 8
        //     308: goto -81 -> 227
        //     311: astore 22
        //     313: goto -225 -> 88
        //     316: astore 14
        //     318: goto -167 -> 151
        //
        // Exception table:
        //     from	to	target	type
        //     72	79	118	java/io/IOException
        //     131	136	139	java/io/IOException
        //     72	79	144	finally
        //     120	131	144	finally
        //     192	209	270	java/io/IOException
        //     217	224	289	java/io/IOException
        //     83	88	311	java/io/IOException
        //     146	151	316	java/io/IOException
    }

    private static void logAndPrintError(PrintStream paramPrintStream, String paramString, Throwable paramThrowable)
    {
        Log.e("Zygote", paramString, paramThrowable);
        if (paramPrintStream != null)
        {
            StringBuilder localStringBuilder = new StringBuilder().append(paramString);
            if (paramThrowable == null)
                paramThrowable = "";
            paramPrintStream.println(paramThrowable);
        }
    }

    private String[] readArgumentList()
        throws IOException
    {
        int i;
        try
        {
            String str = this.mSocketReader.readLine();
            if (str == null)
            {
                arrayOfString = null;
                break label118;
            }
            i = Integer.parseInt(str);
            if (i > 1024)
                throw new IOException("max arg count exceeded");
        }
        catch (NumberFormatException localNumberFormatException)
        {
            Log.e("Zygote", "invalid Zygote wire format: non-int at argc");
            throw new IOException("invalid wire format");
        }
        String[] arrayOfString = new String[i];
        for (int j = 0; j < i; j++)
        {
            arrayOfString[j] = this.mSocketReader.readLine();
            if (arrayOfString[j] == null)
                throw new IOException("truncated request");
        }
        label118: return arrayOfString;
    }

    private void setChildPgid(int paramInt)
    {
        try
        {
            ZygoteInit.setpgid(paramInt, ZygoteInit.getpgid(this.peer.getPid()));
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.i("Zygote", "Zygote: setpgid failed. This is normal if peer is not in our session");
        }
    }

    void closeSocket()
    {
        try
        {
            this.mSocket.close();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.e("Zygote", "Exception while closing command socket in parent", localIOException);
        }
    }

    FileDescriptor getFileDesciptor()
    {
        return this.mSocket.getFileDescriptor();
    }

    void run()
        throws ZygoteInit.MethodAndArgsCaller
    {
        int i = 10;
        while (true)
        {
            if (i <= 0)
                ZygoteInit.gc();
            for (i = 10; runOnce(); i--)
                return;
        }
    }

    // ERROR //
    boolean runOnce()
        throws ZygoteInit.MethodAndArgsCaller
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: aload_0
        //     3: invokespecial 443	com/android/internal/os/ZygoteConnection:readArgumentList	()[Ljava/lang/String;
        //     6: astore 5
        //     8: aload_0
        //     9: getfield 54	com/android/internal/os/ZygoteConnection:mSocket	Landroid/net/LocalSocket;
        //     12: invokevirtual 447	android/net/LocalSocket:getAncillaryFileDescriptors	()[Ljava/io/FileDescriptor;
        //     15: astore 6
        //     17: aload 5
        //     19: ifnonnull +53 -> 72
        //     22: aload_0
        //     23: invokevirtual 259	com/android/internal/os/ZygoteConnection:closeSocket	()V
        //     26: iconst_1
        //     27: istore 4
        //     29: iload 4
        //     31: ireturn
        //     32: astore_2
        //     33: ldc 16
        //     35: new 171	java/lang/StringBuilder
        //     38: dup
        //     39: invokespecial 172	java/lang/StringBuilder:<init>	()V
        //     42: ldc_w 449
        //     45: invokevirtual 178	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     48: aload_2
        //     49: invokevirtual 452	java/io/IOException:getMessage	()Ljava/lang/String;
        //     52: invokevirtual 178	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     55: invokevirtual 182	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     58: invokestatic 381	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     61: pop
        //     62: aload_0
        //     63: invokevirtual 259	com/android/internal/os/ZygoteConnection:closeSocket	()V
        //     66: iconst_1
        //     67: istore 4
        //     69: goto -40 -> 29
        //     72: aconst_null
        //     73: astore 7
        //     75: aload 6
        //     77: ifnull +30 -> 107
        //     80: aload 6
        //     82: arraylength
        //     83: iconst_3
        //     84: if_icmplt +23 -> 107
        //     87: new 392	java/io/PrintStream
        //     90: dup
        //     91: new 454	java/io/FileOutputStream
        //     94: dup
        //     95: aload 6
        //     97: iconst_2
        //     98: aaload
        //     99: invokespecial 455	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
        //     102: invokespecial 456	java/io/PrintStream:<init>	(Ljava/io/OutputStream;)V
        //     105: astore 7
        //     107: bipush 255
        //     109: istore 8
        //     111: aconst_null
        //     112: astore 9
        //     114: aconst_null
        //     115: astore 10
        //     117: new 6	com/android/internal/os/ZygoteConnection$Arguments
        //     120: dup
        //     121: aload 5
        //     123: invokespecial 459	com/android/internal/os/ZygoteConnection$Arguments:<init>	([Ljava/lang/String;)V
        //     126: astore 11
        //     128: aload 11
        //     130: aload_0
        //     131: getfield 93	com/android/internal/os/ZygoteConnection:peer	Landroid/net/Credentials;
        //     134: invokestatic 461	com/android/internal/os/ZygoteConnection:applyUidSecurityPolicy	(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;)V
        //     137: aload 11
        //     139: aload_0
        //     140: getfield 93	com/android/internal/os/ZygoteConnection:peer	Landroid/net/Credentials;
        //     143: invokestatic 463	com/android/internal/os/ZygoteConnection:applyRlimitSecurityPolicy	(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;)V
        //     146: aload 11
        //     148: aload_0
        //     149: getfield 93	com/android/internal/os/ZygoteConnection:peer	Landroid/net/Credentials;
        //     152: invokestatic 465	com/android/internal/os/ZygoteConnection:applyCapabilitiesSecurityPolicy	(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;)V
        //     155: aload 11
        //     157: aload_0
        //     158: getfield 93	com/android/internal/os/ZygoteConnection:peer	Landroid/net/Credentials;
        //     161: invokestatic 467	com/android/internal/os/ZygoteConnection:applyInvokeWithSecurityPolicy	(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;)V
        //     164: aload 11
        //     166: invokestatic 469	com/android/internal/os/ZygoteConnection:applyDebuggerSystemProperty	(Lcom/android/internal/os/ZygoteConnection$Arguments;)V
        //     169: aload 11
        //     171: invokestatic 471	com/android/internal/os/ZygoteConnection:applyInvokeWithSystemProperty	(Lcom/android/internal/os/ZygoteConnection$Arguments;)V
        //     174: aconst_null
        //     175: checkcast 42	[[I
        //     178: astore 18
        //     180: aload 11
        //     182: getfield 194	com/android/internal/os/ZygoteConnection$Arguments:rlimits	Ljava/util/ArrayList;
        //     185: ifnull +19 -> 204
        //     188: aload 11
        //     190: getfield 194	com/android/internal/os/ZygoteConnection$Arguments:rlimits	Ljava/util/ArrayList;
        //     193: getstatic 44	com/android/internal/os/ZygoteConnection:intArray2d	[[I
        //     196: invokevirtual 477	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     199: checkcast 42	[[I
        //     202: astore 18
        //     204: aload 11
        //     206: getfield 276	com/android/internal/os/ZygoteConnection$Arguments:runtimeInit	Z
        //     209: ifeq +39 -> 248
        //     212: aload 11
        //     214: getfield 163	com/android/internal/os/ZygoteConnection$Arguments:invokeWith	Ljava/lang/String;
        //     217: ifnull +31 -> 248
        //     220: getstatic 483	libcore/io/Libcore:os	Llibcore/io/Os;
        //     223: invokeinterface 488 1 0
        //     228: astore 20
        //     230: aload 20
        //     232: iconst_1
        //     233: aaload
        //     234: astore 9
        //     236: aload 20
        //     238: iconst_0
        //     239: aaload
        //     240: astore 10
        //     242: aload 10
        //     244: iconst_1
        //     245: invokestatic 244	com/android/internal/os/ZygoteInit:setCloseOnExec	(Ljava/io/FileDescriptor;Z)V
        //     248: aload 11
        //     250: getfield 204	com/android/internal/os/ZygoteConnection$Arguments:uid	I
        //     253: aload 11
        //     255: getfield 213	com/android/internal/os/ZygoteConnection$Arguments:gid	I
        //     258: aload 11
        //     260: getfield 223	com/android/internal/os/ZygoteConnection$Arguments:gids	[I
        //     263: aload 11
        //     265: getfield 159	com/android/internal/os/ZygoteConnection$Arguments:debugFlags	I
        //     268: aload 18
        //     270: invokestatic 494	dalvik/system/Zygote:forkAndSpecialize	(II[II[[I)I
        //     273: istore 19
        //     275: iload 19
        //     277: istore 8
        //     279: aload 11
        //     281: astore_1
        //     282: iload 8
        //     284: ifne +97 -> 381
        //     287: aload 10
        //     289: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     292: aconst_null
        //     293: astore 10
        //     295: aload_0
        //     296: aload_1
        //     297: aload 6
        //     299: aload 9
        //     301: aload 7
        //     303: invokespecial 496	com/android/internal/os/ZygoteConnection:handleChildProc	(Lcom/android/internal/os/ZygoteConnection$Arguments;[Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/PrintStream;)V
        //     306: iconst_1
        //     307: istore 4
        //     309: aload 9
        //     311: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     314: aconst_null
        //     315: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     318: goto -289 -> 29
        //     321: astore 12
        //     323: aload 7
        //     325: ldc_w 498
        //     328: aload 12
        //     330: invokestatic 314	com/android/internal/os/ZygoteConnection:logAndPrintError	(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V
        //     333: goto -51 -> 282
        //     336: astore 15
        //     338: aload 7
        //     340: ldc_w 498
        //     343: aload 15
        //     345: invokestatic 314	com/android/internal/os/ZygoteConnection:logAndPrintError	(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V
        //     348: goto -66 -> 282
        //     351: astore 16
        //     353: aload 7
        //     355: ldc_w 500
        //     358: aload 16
        //     360: invokestatic 314	com/android/internal/os/ZygoteConnection:logAndPrintError	(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V
        //     363: goto -81 -> 282
        //     366: astore 17
        //     368: aload 7
        //     370: ldc_w 502
        //     373: aload 17
        //     375: invokestatic 314	com/android/internal/os/ZygoteConnection:logAndPrintError	(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V
        //     378: goto -96 -> 282
        //     381: aload 9
        //     383: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     386: aconst_null
        //     387: astore 9
        //     389: aload_0
        //     390: iload 8
        //     392: aload 6
        //     394: aload 10
        //     396: aload_1
        //     397: invokespecial 504	com/android/internal/os/ZygoteConnection:handleParentProc	(I[Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Lcom/android/internal/os/ZygoteConnection$Arguments;)Z
        //     400: istore 14
        //     402: iload 14
        //     404: istore 4
        //     406: aconst_null
        //     407: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     410: aload 10
        //     412: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     415: goto -386 -> 29
        //     418: astore 13
        //     420: aload 9
        //     422: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     425: aload 10
        //     427: invokestatic 254	libcore/io/IoUtils:closeQuietly	(Ljava/io/FileDescriptor;)V
        //     430: aload 13
        //     432: athrow
        //     433: astore 17
        //     435: aload 11
        //     437: astore_1
        //     438: goto -70 -> 368
        //     441: astore 16
        //     443: aload 11
        //     445: astore_1
        //     446: goto -93 -> 353
        //     449: astore 15
        //     451: aload 11
        //     453: astore_1
        //     454: goto -116 -> 338
        //     457: astore 12
        //     459: aload 11
        //     461: astore_1
        //     462: goto -139 -> 323
        //
        // Exception table:
        //     from	to	target	type
        //     2	17	32	java/io/IOException
        //     117	128	321	java/io/IOException
        //     117	128	336	libcore/io/ErrnoException
        //     117	128	351	java/lang/IllegalArgumentException
        //     117	128	366	com/android/internal/os/ZygoteSecurityException
        //     287	306	418	finally
        //     381	402	418	finally
        //     128	275	433	com/android/internal/os/ZygoteSecurityException
        //     128	275	441	java/lang/IllegalArgumentException
        //     128	275	449	libcore/io/ErrnoException
        //     128	275	457	java/io/IOException
    }

    static class Arguments
    {
        boolean capabilitiesSpecified;
        String classpath;
        int debugFlags;
        long effectiveCapabilities;
        int gid = 0;
        boolean gidSpecified;
        int[] gids;
        String invokeWith;
        String niceName;
        boolean peerWait;
        long permittedCapabilities;
        String[] remainingArgs;
        ArrayList<int[]> rlimits;
        boolean runtimeInit;
        int targetSdkVersion;
        boolean targetSdkVersionSpecified;
        int uid = 0;
        boolean uidSpecified;

        Arguments(String[] paramArrayOfString)
            throws IllegalArgumentException
        {
            parseArgs(paramArrayOfString);
        }

        private void parseArgs(String[] paramArrayOfString)
            throws IllegalArgumentException
        {
            int i = 0;
            String str;
            if (i < paramArrayOfString.length)
            {
                str = paramArrayOfString[i];
                if (str.equals("--"))
                    i++;
            }
            else
            {
                label24: if ((!this.runtimeInit) || (this.classpath == null))
                    break label818;
                throw new IllegalArgumentException("--runtime-init and -classpath are incompatible");
            }
            if (str.startsWith("--setuid="))
            {
                if (this.uidSpecified)
                    throw new IllegalArgumentException("Duplicate arg specified");
                this.uidSpecified = true;
                this.uid = Integer.parseInt(str.substring(1 + str.indexOf('=')));
            }
            while (true)
            {
                i++;
                break;
                if (str.startsWith("--setgid="))
                {
                    if (this.gidSpecified)
                        throw new IllegalArgumentException("Duplicate arg specified");
                    this.gidSpecified = true;
                    this.gid = Integer.parseInt(str.substring(1 + str.indexOf('=')));
                }
                else if (str.startsWith("--target-sdk-version="))
                {
                    if (this.targetSdkVersionSpecified)
                        throw new IllegalArgumentException("Duplicate target-sdk-version specified");
                    this.targetSdkVersionSpecified = true;
                    this.targetSdkVersion = Integer.parseInt(str.substring(1 + str.indexOf('=')));
                }
                else if (str.equals("--enable-debugger"))
                {
                    this.debugFlags = (0x1 | this.debugFlags);
                }
                else if (str.equals("--enable-safemode"))
                {
                    this.debugFlags = (0x8 | this.debugFlags);
                }
                else if (str.equals("--enable-checkjni"))
                {
                    this.debugFlags = (0x2 | this.debugFlags);
                }
                else if (str.equals("--enable-jni-logging"))
                {
                    this.debugFlags = (0x10 | this.debugFlags);
                }
                else if (str.equals("--enable-assert"))
                {
                    this.debugFlags = (0x4 | this.debugFlags);
                }
                else if (str.equals("--peer-wait"))
                {
                    this.peerWait = true;
                }
                else if (str.equals("--runtime-init"))
                {
                    this.runtimeInit = true;
                }
                else if (str.startsWith("--capabilities="))
                {
                    if (this.capabilitiesSpecified)
                        throw new IllegalArgumentException("Duplicate arg specified");
                    this.capabilitiesSpecified = true;
                    String[] arrayOfString3 = str.substring(1 + str.indexOf('=')).split(",", 2);
                    if (arrayOfString3.length == 1)
                    {
                        this.effectiveCapabilities = Long.decode(arrayOfString3[0]).longValue();
                        this.permittedCapabilities = this.effectiveCapabilities;
                    }
                    else
                    {
                        this.permittedCapabilities = Long.decode(arrayOfString3[0]).longValue();
                        this.effectiveCapabilities = Long.decode(arrayOfString3[1]).longValue();
                    }
                }
                else if (str.startsWith("--rlimit="))
                {
                    String[] arrayOfString2 = str.substring(1 + str.indexOf('=')).split(",");
                    if (arrayOfString2.length != 3)
                        throw new IllegalArgumentException("--rlimit= should have 3 comma-delimited ints");
                    int[] arrayOfInt = new int[arrayOfString2.length];
                    for (int k = 0; k < arrayOfString2.length; k++)
                        arrayOfInt[k] = Integer.parseInt(arrayOfString2[k]);
                    if (this.rlimits == null)
                        this.rlimits = new ArrayList();
                    this.rlimits.add(arrayOfInt);
                }
                else if (str.equals("-classpath"))
                {
                    if (this.classpath != null)
                        throw new IllegalArgumentException("Duplicate arg specified");
                    i++;
                    try
                    {
                        this.classpath = paramArrayOfString[i];
                    }
                    catch (IndexOutOfBoundsException localIndexOutOfBoundsException2)
                    {
                        throw new IllegalArgumentException("-classpath requires argument");
                    }
                }
                else if (str.startsWith("--setgroups="))
                {
                    if (this.gids != null)
                        throw new IllegalArgumentException("Duplicate arg specified");
                    String[] arrayOfString1 = str.substring(1 + str.indexOf('=')).split(",");
                    this.gids = new int[arrayOfString1.length];
                    for (int j = -1 + arrayOfString1.length; j >= 0; j--)
                        this.gids[j] = Integer.parseInt(arrayOfString1[j]);
                }
                else if (str.equals("--invoke-with"))
                {
                    if (this.invokeWith != null)
                        throw new IllegalArgumentException("Duplicate arg specified");
                    i++;
                    try
                    {
                        this.invokeWith = paramArrayOfString[i];
                    }
                    catch (IndexOutOfBoundsException localIndexOutOfBoundsException1)
                    {
                        throw new IllegalArgumentException("--invoke-with requires argument");
                    }
                }
                else
                {
                    if (!str.startsWith("--nice-name="))
                        break label24;
                    if (this.niceName != null)
                        throw new IllegalArgumentException("Duplicate arg specified");
                    this.niceName = str.substring(1 + str.indexOf('='));
                }
            }
            label818: this.remainingArgs = new String[paramArrayOfString.length - i];
            System.arraycopy(paramArrayOfString, i, this.remainingArgs, 0, this.remainingArgs.length);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.ZygoteConnection
 * JD-Core Version:        0.6.2
 */