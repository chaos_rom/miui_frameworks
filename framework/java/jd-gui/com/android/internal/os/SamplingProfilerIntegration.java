package com.android.internal.os;

import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.SystemProperties;
import android.util.Log;
import dalvik.system.profiler.SamplingProfiler;
import dalvik.system.profiler.SamplingProfiler.ThreadSet;
import java.io.File;
import java.io.PrintStream;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicBoolean;

public class SamplingProfilerIntegration
{
    public static final String SNAPSHOT_DIR = "/data/snapshots";
    private static final String TAG = "SamplingProfilerIntegration";
    private static final boolean enabled;
    private static final AtomicBoolean pending = new AtomicBoolean(false);
    private static SamplingProfiler samplingProfiler;
    private static final int samplingProfilerDepth;
    private static final int samplingProfilerMilliseconds = SystemProperties.getInt("persist.sys.profiler_ms", 0);
    private static final Executor snapshotWriter;
    private static long startMillis;

    static
    {
        samplingProfilerDepth = SystemProperties.getInt("persist.sys.profiler_depth", 4);
        if (samplingProfilerMilliseconds > 0)
        {
            File localFile = new File("/data/snapshots");
            localFile.mkdirs();
            localFile.setWritable(true, false);
            localFile.setExecutable(true, false);
            if (localFile.isDirectory())
            {
                snapshotWriter = Executors.newSingleThreadExecutor(new ThreadFactory()
                {
                    public Thread newThread(Runnable paramAnonymousRunnable)
                    {
                        return new Thread(paramAnonymousRunnable, "SamplingProfilerIntegration");
                    }
                });
                enabled = true;
                Log.i("SamplingProfilerIntegration", "Profiling enabled. Sampling interval ms: " + samplingProfilerMilliseconds);
            }
        }
        while (true)
        {
            return;
            snapshotWriter = null;
            enabled = true;
            Log.w("SamplingProfilerIntegration", "Profiling setup failed. Could not create /data/snapshots");
            continue;
            snapshotWriter = null;
            enabled = false;
            Log.i("SamplingProfilerIntegration", "Profiling disabled.");
        }
    }

    private static void generateSnapshotHeader(String paramString, PackageInfo paramPackageInfo, PrintStream paramPrintStream)
    {
        paramPrintStream.println("Version: 3");
        paramPrintStream.println("Process: " + paramString);
        if (paramPackageInfo != null)
        {
            paramPrintStream.println("Package: " + paramPackageInfo.packageName);
            paramPrintStream.println("Package-Version: " + paramPackageInfo.versionCode);
        }
        paramPrintStream.println("Build: " + Build.FINGERPRINT);
        paramPrintStream.println();
    }

    public static boolean isEnabled()
    {
        return enabled;
    }

    public static void start()
    {
        if (!enabled);
        while (true)
        {
            return;
            if (samplingProfiler != null)
            {
                Log.e("SamplingProfilerIntegration", "SamplingProfilerIntegration already started at " + new Date(startMillis));
            }
            else
            {
                SamplingProfiler.ThreadSet localThreadSet = SamplingProfiler.newThreadGroupTheadSet(Thread.currentThread().getThreadGroup());
                samplingProfiler = new SamplingProfiler(samplingProfilerDepth, localThreadSet);
                samplingProfiler.start(samplingProfilerMilliseconds);
                startMillis = System.currentTimeMillis();
            }
        }
    }

    public static void writeSnapshot(String paramString, final PackageInfo paramPackageInfo)
    {
        if (!enabled);
        while (true)
        {
            return;
            if (samplingProfiler == null)
                Log.e("SamplingProfilerIntegration", "SamplingProfilerIntegration is not started");
            else if (pending.compareAndSet(false, true))
                snapshotWriter.execute(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            SamplingProfilerIntegration.writeSnapshotFile(SamplingProfilerIntegration.this, paramPackageInfo);
                            return;
                        }
                        finally
                        {
                            SamplingProfilerIntegration.pending.set(false);
                        }
                    }
                });
        }
    }

    // ERROR //
    private static void writeSnapshotFile(String paramString, PackageInfo paramPackageInfo)
    {
        // Byte code:
        //     0: getstatic 83	com/android/internal/os/SamplingProfilerIntegration:enabled	Z
        //     3: ifne +4 -> 7
        //     6: return
        //     7: getstatic 156	com/android/internal/os/SamplingProfilerIntegration:samplingProfiler	Ldalvik/system/profiler/SamplingProfiler;
        //     10: invokevirtual 218	dalvik/system/profiler/SamplingProfiler:stop	()V
        //     13: aload_0
        //     14: ldc 220
        //     16: ldc 222
        //     18: invokevirtual 228	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //     21: astore_2
        //     22: new 85	java/lang/StringBuilder
        //     25: dup
        //     26: invokespecial 86	java/lang/StringBuilder:<init>	()V
        //     29: ldc 230
        //     31: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     34: aload_2
        //     35: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     38: ldc 232
        //     40: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     43: getstatic 162	com/android/internal/os/SamplingProfilerIntegration:startMillis	J
        //     46: invokevirtual 235	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     49: ldc 237
        //     51: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     54: invokevirtual 99	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     57: astore_3
        //     58: invokestatic 199	java/lang/System:currentTimeMillis	()J
        //     61: lstore 4
        //     63: aconst_null
        //     64: astore 6
        //     66: new 239	java/io/BufferedOutputStream
        //     69: dup
        //     70: new 241	java/io/FileOutputStream
        //     73: dup
        //     74: aload_3
        //     75: invokespecial 242	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
        //     78: invokespecial 245	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     81: astore 7
        //     83: new 126	java/io/PrintStream
        //     86: dup
        //     87: aload 7
        //     89: invokespecial 246	java/io/PrintStream:<init>	(Ljava/io/OutputStream;)V
        //     92: astore 8
        //     94: aload_2
        //     95: aload_1
        //     96: aload 8
        //     98: invokestatic 248	com/android/internal/os/SamplingProfilerIntegration:generateSnapshotHeader	(Ljava/lang/String;Landroid/content/pm/PackageInfo;Ljava/io/PrintStream;)V
        //     101: aload 8
        //     103: invokevirtual 251	java/io/PrintStream:checkError	()Z
        //     106: ifeq +52 -> 158
        //     109: new 215	java/io/IOException
        //     112: dup
        //     113: invokespecial 252	java/io/IOException:<init>	()V
        //     116: athrow
        //     117: astore 10
        //     119: aload 7
        //     121: astore 6
        //     123: ldc 15
        //     125: new 85	java/lang/StringBuilder
        //     128: dup
        //     129: invokespecial 86	java/lang/StringBuilder:<init>	()V
        //     132: ldc 254
        //     134: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     137: aload_3
        //     138: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     141: invokevirtual 99	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     144: aload 10
        //     146: invokestatic 257	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     149: pop
        //     150: aload 6
        //     152: invokestatic 263	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     155: goto -149 -> 6
        //     158: getstatic 156	com/android/internal/os/SamplingProfilerIntegration:samplingProfiler	Ldalvik/system/profiler/SamplingProfiler;
        //     161: invokevirtual 267	dalvik/system/profiler/SamplingProfiler:getHprofData	()Ldalvik/system/profiler/HprofData;
        //     164: aload 7
        //     166: invokestatic 273	dalvik/system/profiler/BinaryHprofWriter:write	(Ldalvik/system/profiler/HprofData;Ljava/io/OutputStream;)V
        //     169: aload 7
        //     171: invokestatic 263	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     174: new 54	java/io/File
        //     177: dup
        //     178: aload_3
        //     179: invokespecial 57	java/io/File:<init>	(Ljava/lang/String;)V
        //     182: iconst_1
        //     183: iconst_0
        //     184: invokevirtual 276	java/io/File:setReadable	(ZZ)Z
        //     187: pop
        //     188: invokestatic 199	java/lang/System:currentTimeMillis	()J
        //     191: lload 4
        //     193: lsub
        //     194: lstore 13
        //     196: ldc 15
        //     198: new 85	java/lang/StringBuilder
        //     201: dup
        //     202: invokespecial 86	java/lang/StringBuilder:<init>	()V
        //     205: ldc_w 278
        //     208: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     211: aload_3
        //     212: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     215: ldc_w 280
        //     218: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     221: lload 13
        //     223: invokevirtual 235	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     226: ldc_w 282
        //     229: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     232: invokevirtual 99	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     235: invokestatic 105	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     238: pop
        //     239: getstatic 156	com/android/internal/os/SamplingProfilerIntegration:samplingProfiler	Ldalvik/system/profiler/SamplingProfiler;
        //     242: getstatic 48	com/android/internal/os/SamplingProfilerIntegration:samplingProfilerMilliseconds	I
        //     245: invokevirtual 193	dalvik/system/profiler/SamplingProfiler:start	(I)V
        //     248: goto -242 -> 6
        //     251: astore 9
        //     253: aload 6
        //     255: invokestatic 263	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     258: aload 9
        //     260: athrow
        //     261: astore 9
        //     263: aload 7
        //     265: astore 6
        //     267: goto -14 -> 253
        //     270: astore 10
        //     272: goto -149 -> 123
        //
        // Exception table:
        //     from	to	target	type
        //     83	117	117	java/io/IOException
        //     158	169	117	java/io/IOException
        //     66	83	251	finally
        //     123	150	251	finally
        //     83	117	261	finally
        //     158	169	261	finally
        //     66	83	270	java/io/IOException
    }

    public static void writeZygoteSnapshot()
    {
        if (!enabled);
        while (true)
        {
            return;
            writeSnapshotFile("zygote", null);
            samplingProfiler.shutdown();
            samplingProfiler = null;
            startMillis = 0L;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.SamplingProfilerIntegration
 * JD-Core Version:        0.6.2
 */