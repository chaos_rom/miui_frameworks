package com.android.internal.os;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.net.ConnectivityManager;
import android.net.NetworkStats;
import android.net.NetworkStats.Entry;
import android.os.BatteryStats;
import android.os.BatteryStats.Counter;
import android.os.BatteryStats.HistoryItem;
import android.os.BatteryStats.HistoryPrinter;
import android.os.BatteryStats.Timer;
import android.os.BatteryStats.Uid;
import android.os.BatteryStats.Uid.Pid;
import android.os.BatteryStats.Uid.Pkg;
import android.os.BatteryStats.Uid.Pkg.Serv;
import android.os.BatteryStats.Uid.Proc;
import android.os.BatteryStats.Uid.Proc.ExcessivePower;
import android.os.BatteryStats.Uid.Sensor;
import android.os.BatteryStats.Uid.Wakelock;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.ParcelFormatException;
import android.os.Parcelable.Creator;
import android.os.Process;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.WorkSource;
import android.telephony.SignalStrength;
import android.util.Log;
import android.util.LogWriter;
import android.util.Printer;
import android.util.Slog;
import android.util.SparseArray;
import com.android.internal.net.NetworkStatsFactory;
import com.android.internal.util.JournaledFile;
import com.google.android.collect.Sets;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public final class BatteryStatsImpl extends BatteryStats
{
    private static final String BATCHED_WAKELOCK_NAME = "*overflow*";
    private static final int BATTERY_PLUGGED_NONE = 0;
    public static final Parcelable.Creator<BatteryStatsImpl> CREATOR = new Parcelable.Creator()
    {
        public BatteryStatsImpl createFromParcel(Parcel paramAnonymousParcel)
        {
            return new BatteryStatsImpl(paramAnonymousParcel);
        }

        public BatteryStatsImpl[] newArray(int paramAnonymousInt)
        {
            return new BatteryStatsImpl[paramAnonymousInt];
        }
    };
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_HISTORY = false;
    static final long DELAY_UPDATE_WAKELOCKS = 5000L;
    private static final int MAGIC = -1166707595;
    static final int MAX_HISTORY_BUFFER = 131072;
    private static final int MAX_HISTORY_ITEMS = 2000;
    static final int MAX_MAX_HISTORY_BUFFER = 147456;
    private static final int MAX_MAX_HISTORY_ITEMS = 3000;
    private static final int MAX_WAKELOCKS_PER_UID = 30;
    private static final int MAX_WAKELOCKS_PER_UID_IN_SYSTEM = 50;
    static final int MSG_REPORT_POWER_CHANGE = 2;
    static final int MSG_UPDATE_WAKELOCKS = 1;
    private static final int[] PROC_WAKELOCKS_FORMAT;
    private static final String TAG = "BatteryStatsImpl";
    private static final boolean USE_OLD_HISTORY = false;
    private static final int VERSION = 61;
    private static int sKernelWakelockUpdateVersion = 0;
    private static int sNumSpeedSteps;
    boolean mAudioOn;
    StopwatchTimer mAudioOnTimer;
    long mBatteryLastRealtime;
    long mBatteryLastUptime;
    long mBatteryRealtime;
    long mBatteryUptime;
    boolean mBluetoothOn;
    StopwatchTimer mBluetoothOnTimer;
    private int mBluetoothPingCount;
    private int mBluetoothPingStart = -1;
    BluetoothHeadset mBtHeadset;
    private BatteryCallback mCallback;
    int mChangedBufferStates = 0;
    int mChangedStates = 0;
    int mDischargeAmountScreenOff;
    int mDischargeAmountScreenOffSinceCharge;
    int mDischargeAmountScreenOn;
    int mDischargeAmountScreenOnSinceCharge;
    int mDischargeCurrentLevel;
    int mDischargeScreenOffUnplugLevel;
    int mDischargeScreenOnUnplugLevel;
    int mDischargeStartLevel;
    int mDischargeUnplugLevel;
    private final JournaledFile mFile;
    final ArrayList<StopwatchTimer> mFullTimers = new ArrayList();
    final ArrayList<StopwatchTimer> mFullWifiLockTimers = new ArrayList();
    boolean mGlobalWifiRunning;
    StopwatchTimer mGlobalWifiRunningTimer;
    int mGpsNesting;
    private final MyHandler mHandler;
    boolean mHaveBatteryLevel = false;
    int mHighDischargeAmountSinceCharge;
    BatteryStats.HistoryItem mHistory;
    long mHistoryBaseTime;
    final Parcel mHistoryBuffer = Parcel.obtain();
    int mHistoryBufferLastPos = -1;
    BatteryStats.HistoryItem mHistoryCache;
    final BatteryStats.HistoryItem mHistoryCur = new BatteryStats.HistoryItem();
    BatteryStats.HistoryItem mHistoryEnd;
    private BatteryStats.HistoryItem mHistoryIterator;
    BatteryStats.HistoryItem mHistoryLastEnd;
    final BatteryStats.HistoryItem mHistoryLastLastWritten = new BatteryStats.HistoryItem();
    final BatteryStats.HistoryItem mHistoryLastWritten = new BatteryStats.HistoryItem();
    boolean mHistoryOverflow = false;
    final BatteryStats.HistoryItem mHistoryReadTmp = new BatteryStats.HistoryItem();
    Counter mInputEventCounter;
    private boolean mIteratingHistory;
    private final HashMap<String, SamplingTimer> mKernelWakelockStats = new HashMap();
    long mLastHistoryTime = 0L;
    final ArrayList<StopwatchTimer> mLastPartialTimers = new ArrayList();
    long mLastRealtime;
    long mLastUptime;
    long mLastWriteTime = 0L;
    int mLowDischargeAmountSinceCharge;
    private long[] mMobileDataRx = new long[4];
    private long[] mMobileDataTx = new long[4];
    private HashSet<String> mMobileIfaces = Sets.newHashSet();
    private NetworkStats mNetworkDetailCache;
    private final NetworkStatsFactory mNetworkStatsFactory = new NetworkStatsFactory();
    private NetworkStats mNetworkSummaryCache;
    int mNumHistoryItems;
    boolean mOnBattery;
    boolean mOnBatteryInternal;
    final ArrayList<StopwatchTimer> mPartialTimers = new ArrayList();
    Parcel mPendingWrite = null;
    int mPhoneDataConnectionType = -1;
    final StopwatchTimer[] mPhoneDataConnectionsTimer = new StopwatchTimer[16];
    boolean mPhoneOn;
    StopwatchTimer mPhoneOnTimer;
    private int mPhoneServiceState = -1;
    private int mPhoneServiceStateRaw = -1;
    StopwatchTimer mPhoneSignalScanningTimer;
    int mPhoneSignalStrengthBin = -1;
    int mPhoneSignalStrengthBinRaw = -1;
    final StopwatchTimer[] mPhoneSignalStrengthsTimer = new StopwatchTimer[5];
    private int mPhoneSimStateRaw = -1;
    private final Map<String, KernelWakelockStats> mProcWakelockFileStats = new HashMap();
    private final long[] mProcWakelocksData = new long[3];
    private final String[] mProcWakelocksName = new String[3];
    private long mRadioDataStart;
    private long mRadioDataUptime;
    private boolean mReadOverflow;
    long mRealtime;
    long mRealtimeStart;
    boolean mRecordingHistory = true;
    final ArrayList<StopwatchTimer> mScanWifiLockTimers = new ArrayList();
    int mScreenBrightnessBin = -1;
    final StopwatchTimer[] mScreenBrightnessTimer = new StopwatchTimer[5];
    boolean mScreenOn;
    StopwatchTimer mScreenOnTimer;
    int mSensorNesting;
    final SparseArray<ArrayList<StopwatchTimer>> mSensorTimers = new SparseArray();
    boolean mShuttingDown;
    int mStartCount;
    private long[] mTotalDataRx = new long[4];
    private long[] mTotalDataTx = new long[4];
    long mTrackBatteryPastRealtime;
    long mTrackBatteryPastUptime;
    long mTrackBatteryRealtimeStart;
    long mTrackBatteryUptimeStart;
    private HashMap<String, Integer> mUidCache = new HashMap();
    final SparseArray<Uid> mUidStats = new SparseArray();
    final ArrayList<Unpluggable> mUnpluggables = new ArrayList();
    long mUnpluggedBatteryRealtime;
    long mUnpluggedBatteryUptime;
    long mUptime;
    long mUptimeStart;
    boolean mVideoOn;
    StopwatchTimer mVideoOnTimer;
    int mWakeLockNesting;
    int mWifiFullLockNesting = 0;
    int mWifiMulticastNesting = 0;
    final ArrayList<StopwatchTimer> mWifiMulticastTimers = new ArrayList();
    boolean mWifiOn;
    StopwatchTimer mWifiOnTimer;
    int mWifiOnUid = -1;
    final ArrayList<StopwatchTimer> mWifiRunningTimers = new ArrayList();
    int mWifiScanLockNesting = 0;
    final ArrayList<StopwatchTimer> mWindowTimers = new ArrayList();
    final ReentrantLock mWriteLock = new ReentrantLock();

    static
    {
        int[] arrayOfInt = new int[6];
        arrayOfInt[0] = 4105;
        arrayOfInt[1] = 8201;
        arrayOfInt[2] = 9;
        arrayOfInt[3] = 9;
        arrayOfInt[4] = 9;
        arrayOfInt[5] = 8201;
        PROC_WAKELOCKS_FORMAT = arrayOfInt;
    }

    public BatteryStatsImpl()
    {
        this.mFile = null;
        this.mHandler = null;
    }

    public BatteryStatsImpl(Parcel paramParcel)
    {
        this.mFile = null;
        this.mHandler = null;
        clearHistoryLocked();
        readFromParcel(paramParcel);
    }

    public BatteryStatsImpl(String paramString)
    {
        this.mFile = new JournaledFile(new File(paramString), new File(paramString + ".tmp"));
        this.mHandler = new MyHandler();
        this.mStartCount = (1 + this.mStartCount);
        this.mScreenOnTimer = new StopwatchTimer(null, -1, null, this.mUnpluggables);
        for (int i = 0; i < 5; i++)
            this.mScreenBrightnessTimer[i] = new StopwatchTimer(null, -100 - i, null, this.mUnpluggables);
        this.mInputEventCounter = new Counter(this.mUnpluggables);
        this.mPhoneOnTimer = new StopwatchTimer(null, -2, null, this.mUnpluggables);
        for (int j = 0; j < 5; j++)
            this.mPhoneSignalStrengthsTimer[j] = new StopwatchTimer(null, -200 - j, null, this.mUnpluggables);
        this.mPhoneSignalScanningTimer = new StopwatchTimer(null, -199, null, this.mUnpluggables);
        for (int k = 0; k < 16; k++)
            this.mPhoneDataConnectionsTimer[k] = new StopwatchTimer(null, -300 - k, null, this.mUnpluggables);
        this.mWifiOnTimer = new StopwatchTimer(null, -3, null, this.mUnpluggables);
        this.mGlobalWifiRunningTimer = new StopwatchTimer(null, -4, null, this.mUnpluggables);
        this.mBluetoothOnTimer = new StopwatchTimer(null, -5, null, this.mUnpluggables);
        this.mAudioOnTimer = new StopwatchTimer(null, -6, null, this.mUnpluggables);
        this.mVideoOnTimer = new StopwatchTimer(null, -7, null, this.mUnpluggables);
        this.mOnBatteryInternal = false;
        this.mOnBattery = false;
        initTimes();
        this.mTrackBatteryPastUptime = 0L;
        this.mTrackBatteryPastRealtime = 0L;
        long l1 = 1000L * SystemClock.uptimeMillis();
        this.mTrackBatteryUptimeStart = l1;
        this.mUptimeStart = l1;
        long l2 = 1000L * SystemClock.elapsedRealtime();
        this.mTrackBatteryRealtimeStart = l2;
        this.mRealtimeStart = l2;
        this.mUnpluggedBatteryUptime = getBatteryUptimeLocked(this.mUptimeStart);
        this.mUnpluggedBatteryRealtime = getBatteryRealtimeLocked(this.mRealtimeStart);
        this.mDischargeStartLevel = 0;
        this.mDischargeUnplugLevel = 0;
        this.mDischargeCurrentLevel = 0;
        initDischarge();
        clearHistoryLocked();
    }

    private void doDataPlug(long[] paramArrayOfLong, long paramLong)
    {
        paramArrayOfLong[1] = paramArrayOfLong[3];
        paramArrayOfLong[3] = -1L;
    }

    private void doDataUnplug(long[] paramArrayOfLong, long paramLong)
    {
        paramArrayOfLong[3] = paramLong;
    }

    private int fixPhoneServiceState(int paramInt1, int paramInt2)
    {
        if ((this.mPhoneSimStateRaw == 1) && (paramInt1 == 1) && (paramInt2 > 0))
            paramInt1 = 0;
        return paramInt1;
    }

    private int getCurrentBluetoothPingCount()
    {
        List localList;
        if (this.mBtHeadset != null)
        {
            localList = this.mBtHeadset.getConnectedDevices();
            if (localList.size() <= 0);
        }
        for (int i = this.mBtHeadset.getBatteryUsageHint((BluetoothDevice)localList.get(0)); ; i = -1)
            return i;
    }

    private long getCurrentRadioDataUptime()
    {
        long l1 = 0L;
        try
        {
            File localFile = new File("/sys/devices/virtual/net/rmnet0/awake_time_ms");
            if (localFile.exists())
            {
                BufferedReader localBufferedReader = new BufferedReader(new FileReader(localFile));
                String str = localBufferedReader.readLine();
                localBufferedReader.close();
                long l2 = Long.parseLong(str);
                l1 = l2 * 1000L;
            }
        }
        catch (IOException localIOException)
        {
        }
        catch (NumberFormatException localNumberFormatException)
        {
        }
        return l1;
    }

    private NetworkStats getNetworkStatsDetailGroupedByUid()
    {
        try
        {
            if ((this.mNetworkDetailCache == null) || (this.mNetworkDetailCache.getElapsedRealtimeAge() > 1000L))
            {
                this.mNetworkDetailCache = null;
                boolean bool = SystemProperties.getBoolean("net.qtaguid_enabled", false);
                if (!bool);
            }
            try
            {
                this.mNetworkDetailCache = this.mNetworkStatsFactory.readNetworkStatsDetail().groupedByUid();
                if (this.mNetworkDetailCache == null)
                    this.mNetworkDetailCache = new NetworkStats(SystemClock.elapsedRealtime(), 0);
                NetworkStats localNetworkStats = this.mNetworkDetailCache;
                return localNetworkStats;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                while (true)
                    Log.wtf("BatteryStatsImpl", "problem reading network stats", localIllegalStateException);
            }
        }
        finally
        {
        }
    }

    private NetworkStats getNetworkStatsSummary()
    {
        try
        {
            if ((this.mNetworkSummaryCache == null) || (this.mNetworkSummaryCache.getElapsedRealtimeAge() > 1000L))
            {
                this.mNetworkSummaryCache = null;
                boolean bool = SystemProperties.getBoolean("net.qtaguid_enabled", false);
                if (!bool);
            }
            try
            {
                this.mNetworkSummaryCache = this.mNetworkStatsFactory.readNetworkStatsSummaryDev();
                if (this.mNetworkSummaryCache == null)
                    this.mNetworkSummaryCache = new NetworkStats(SystemClock.elapsedRealtime(), 0);
                NetworkStats localNetworkStats = this.mNetworkSummaryCache;
                return localNetworkStats;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                while (true)
                    Log.wtf("BatteryStatsImpl", "problem reading network stats", localIllegalStateException);
            }
        }
        finally
        {
        }
    }

    private long getTcpBytes(long paramLong, long[] paramArrayOfLong, int paramInt)
    {
        long l;
        if (paramInt == 1)
            l = paramArrayOfLong[1];
        while (true)
        {
            return l;
            if (paramInt == 3)
            {
                if (paramArrayOfLong[3] < 0L)
                    l = paramArrayOfLong[1];
                else
                    l = paramLong - paramArrayOfLong[3];
            }
            else if (paramInt == 0)
                l = paramLong - paramArrayOfLong[2] + paramArrayOfLong[0];
            else
                l = paramLong - paramArrayOfLong[2];
        }
    }

    private final Map<String, KernelWakelockStats> parseProcWakelocks(byte[] paramArrayOfByte, int paramInt)
    {
        int i = 0;
        for (int j = 0; (j < paramInt) && (paramArrayOfByte[j] != 10) && (paramArrayOfByte[j] != 0); j++);
        int k = j + 1;
        int m = k;
        while (true)
            try
            {
                localMap = this.mProcWakelockFileStats;
                sKernelWakelockUpdateVersion = 1 + sKernelWakelockUpdateVersion;
                continue;
                if ((n < paramInt) && (paramArrayOfByte[n] != 10) && (paramArrayOfByte[n] != 0))
                {
                    n++;
                }
                else
                {
                    k = n + 1;
                    if (k >= paramInt - 1);
                    String[] arrayOfString = this.mProcWakelocksName;
                    long[] arrayOfLong = this.mProcWakelocksData;
                    i1 = m;
                    if (i1 < k)
                    {
                        if ((0x80 & paramArrayOfByte[i1]) != 0)
                            paramArrayOfByte[i1] = 63;
                    }
                    else
                    {
                        boolean bool = Process.parseProcLine(paramArrayOfByte, m, k, PROC_WAKELOCKS_FORMAT, arrayOfString, arrayOfLong, null);
                        String str = arrayOfString[0];
                        i2 = (int)arrayOfLong[1];
                        l = (500L + arrayOfLong[2]) / 1000L;
                        if ((!bool) || (str.length() <= 0))
                            continue;
                        if (!localMap.containsKey(str))
                        {
                            localMap.put(str, new KernelWakelockStats(i2, l, sKernelWakelockUpdateVersion));
                            i++;
                        }
                        else
                        {
                            localKernelWakelockStats = (KernelWakelockStats)localMap.get(str);
                            if (localKernelWakelockStats.mVersion == sKernelWakelockUpdateVersion)
                            {
                                localKernelWakelockStats.mCount = (i2 + localKernelWakelockStats.mCount);
                                localKernelWakelockStats.mTotalTime = (l + localKernelWakelockStats.mTotalTime);
                            }
                        }
                    }
                }
            }
            finally
            {
                Map localMap;
                int n;
                int i1;
                int i2;
                long l;
                KernelWakelockStats localKernelWakelockStats;
                throw localObject;
                localKernelWakelockStats.mCount = i2;
                localKernelWakelockStats.mTotalTime = l;
                localKernelWakelockStats.mVersion = sKernelWakelockUpdateVersion;
                i++;
                continue;
                if (localMap.size() != i)
                {
                    Iterator localIterator = localMap.values().iterator();
                    if (localIterator.hasNext())
                    {
                        if (((KernelWakelockStats)localIterator.next()).mVersion == sKernelWakelockUpdateVersion)
                            continue;
                        localIterator.remove();
                        continue;
                    }
                }
                if (k < paramInt)
                {
                    n = m;
                    continue;
                    return localMap;
                    i1++;
                }
            }
    }

    static byte[] readFully(FileInputStream paramFileInputStream)
        throws IOException
    {
        int i = 0;
        Object localObject = new byte[paramFileInputStream.available()];
        while (true)
        {
            int j = paramFileInputStream.read((byte[])localObject, i, localObject.length - i);
            if (j <= 0)
                return localObject;
            i += j;
            int k = paramFileInputStream.available();
            if (k > localObject.length - i)
            {
                byte[] arrayOfByte = new byte[i + k];
                System.arraycopy(localObject, 0, arrayOfByte, 0, i);
                localObject = arrayOfByte;
            }
        }
    }

    private final Map<String, KernelWakelockStats> readKernelWakelockStats()
    {
        Map localMap = null;
        byte[] arrayOfByte = new byte[8192];
        try
        {
            FileInputStream localFileInputStream = new FileInputStream("/proc/wakelocks");
            int i = localFileInputStream.read(arrayOfByte);
            localFileInputStream.close();
            if (i > 0);
            for (int j = 0; ; j++)
                if (j < i)
                {
                    int k = arrayOfByte[j];
                    if (k == 0)
                        i = j;
                }
                else
                {
                    localMap = parseProcWakelocks(arrayOfByte, i);
                    label68: return localMap;
                }
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            break label68;
        }
        catch (IOException localIOException)
        {
            break label68;
        }
    }

    private void readSummaryFromParcel(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        if (i != 61)
            Slog.w("BatteryStats", "readFromParcel: version got " + i + ", expected " + 61 + "; erasing old stats");
        label1348: label1370: 
        while (true)
        {
            return;
            readHistory(paramParcel, true);
            this.mStartCount = paramParcel.readInt();
            this.mBatteryUptime = paramParcel.readLong();
            this.mBatteryRealtime = paramParcel.readLong();
            this.mUptime = paramParcel.readLong();
            this.mRealtime = paramParcel.readLong();
            this.mDischargeUnplugLevel = paramParcel.readInt();
            this.mDischargeCurrentLevel = paramParcel.readInt();
            this.mLowDischargeAmountSinceCharge = paramParcel.readInt();
            this.mHighDischargeAmountSinceCharge = paramParcel.readInt();
            this.mDischargeAmountScreenOnSinceCharge = paramParcel.readInt();
            this.mDischargeAmountScreenOffSinceCharge = paramParcel.readInt();
            this.mStartCount = (1 + this.mStartCount);
            this.mScreenOn = false;
            this.mScreenOnTimer.readSummaryFromParcelLocked(paramParcel);
            for (int j = 0; j < 5; j++)
                this.mScreenBrightnessTimer[j].readSummaryFromParcelLocked(paramParcel);
            this.mInputEventCounter.readSummaryFromParcelLocked(paramParcel);
            this.mPhoneOn = false;
            this.mPhoneOnTimer.readSummaryFromParcelLocked(paramParcel);
            for (int k = 0; k < 5; k++)
                this.mPhoneSignalStrengthsTimer[k].readSummaryFromParcelLocked(paramParcel);
            this.mPhoneSignalScanningTimer.readSummaryFromParcelLocked(paramParcel);
            for (int m = 0; m < 16; m++)
                this.mPhoneDataConnectionsTimer[m].readSummaryFromParcelLocked(paramParcel);
            this.mWifiOn = false;
            this.mWifiOnTimer.readSummaryFromParcelLocked(paramParcel);
            this.mGlobalWifiRunning = false;
            this.mGlobalWifiRunningTimer.readSummaryFromParcelLocked(paramParcel);
            this.mBluetoothOn = false;
            this.mBluetoothOnTimer.readSummaryFromParcelLocked(paramParcel);
            int n = paramParcel.readInt();
            if (n > 10000)
            {
                Slog.w("BatteryStatsImpl", "File corrupt: too many kernel wake locks " + n);
            }
            else
            {
                for (int i1 = 0; i1 < n; i1++)
                    if (paramParcel.readInt() != 0)
                        getKernelWakelockTimerLocked(paramParcel.readString()).readSummaryFromParcelLocked(paramParcel);
                sNumSpeedSteps = paramParcel.readInt();
                int i2 = paramParcel.readInt();
                if (i2 > 10000)
                    Slog.w("BatteryStatsImpl", "File corrupt: too many uids " + i2);
                else
                    for (int i3 = 0; ; i3++)
                    {
                        if (i3 >= i2)
                            break label1370;
                        int i4 = paramParcel.readInt();
                        Uid localUid = new Uid(i4);
                        this.mUidStats.put(i4, localUid);
                        localUid.mWifiRunning = false;
                        if (paramParcel.readInt() != 0)
                            localUid.mWifiRunningTimer.readSummaryFromParcelLocked(paramParcel);
                        localUid.mFullWifiLockOut = false;
                        if (paramParcel.readInt() != 0)
                            localUid.mFullWifiLockTimer.readSummaryFromParcelLocked(paramParcel);
                        localUid.mScanWifiLockOut = false;
                        if (paramParcel.readInt() != 0)
                            localUid.mScanWifiLockTimer.readSummaryFromParcelLocked(paramParcel);
                        localUid.mWifiMulticastEnabled = false;
                        if (paramParcel.readInt() != 0)
                            localUid.mWifiMulticastTimer.readSummaryFromParcelLocked(paramParcel);
                        localUid.mAudioTurnedOn = false;
                        if (paramParcel.readInt() != 0)
                            localUid.mAudioTurnedOnTimer.readSummaryFromParcelLocked(paramParcel);
                        localUid.mVideoTurnedOn = false;
                        if (paramParcel.readInt() != 0)
                            localUid.mVideoTurnedOnTimer.readSummaryFromParcelLocked(paramParcel);
                        if (paramParcel.readInt() != 0)
                        {
                            if (localUid.mUserActivityCounters == null)
                                localUid.initUserActivityLocked();
                            for (int i22 = 0; i22 < 7; i22++)
                                localUid.mUserActivityCounters[i22].readSummaryFromParcelLocked(paramParcel);
                        }
                        int i5 = paramParcel.readInt();
                        if (i5 > 100)
                        {
                            Slog.w("BatteryStatsImpl", "File corrupt: too many wake locks " + i5);
                            break;
                        }
                        for (int i6 = 0; i6 < i5; i6++)
                        {
                            String str2 = paramParcel.readString();
                            if (paramParcel.readInt() != 0)
                                localUid.getWakeTimerLocked(str2, 1).readSummaryFromParcelLocked(paramParcel);
                            if (paramParcel.readInt() != 0)
                                localUid.getWakeTimerLocked(str2, 0).readSummaryFromParcelLocked(paramParcel);
                            if (paramParcel.readInt() != 0)
                                localUid.getWakeTimerLocked(str2, 2).readSummaryFromParcelLocked(paramParcel);
                        }
                        int i7 = paramParcel.readInt();
                        if (i7 > 1000)
                        {
                            Slog.w("BatteryStatsImpl", "File corrupt: too many sensors " + i7);
                            break;
                        }
                        for (int i8 = 0; i8 < i7; i8++)
                        {
                            int i21 = paramParcel.readInt();
                            if (paramParcel.readInt() != 0)
                                localUid.getSensorTimerLocked(i21, true).readSummaryFromParcelLocked(paramParcel);
                        }
                        int i9 = paramParcel.readInt();
                        if (i9 > 1000)
                        {
                            Slog.w("BatteryStatsImpl", "File corrupt: too many processes " + i9);
                            break;
                        }
                        for (int i10 = 0; ; i10++)
                        {
                            if (i10 >= i9)
                                break label1120;
                            BatteryStatsImpl.Uid.Proc localProc = localUid.getProcessStatsLocked(paramParcel.readString());
                            long l2 = paramParcel.readLong();
                            localProc.mLoadedUserTime = l2;
                            localProc.mUserTime = l2;
                            long l3 = paramParcel.readLong();
                            localProc.mLoadedSystemTime = l3;
                            localProc.mSystemTime = l3;
                            int i18 = paramParcel.readInt();
                            localProc.mLoadedStarts = i18;
                            localProc.mStarts = i18;
                            int i19 = paramParcel.readInt();
                            if (i19 > 100)
                            {
                                Slog.w("BatteryStatsImpl", "File corrupt: too many speed bins " + i19);
                                break;
                            }
                            localProc.mSpeedBins = new SamplingCounter[i19];
                            for (int i20 = 0; i20 < i19; i20++)
                                if (paramParcel.readInt() != 0)
                                {
                                    localProc.mSpeedBins[i20] = new SamplingCounter(this.mUnpluggables);
                                    localProc.mSpeedBins[i20].readSummaryFromParcelLocked(paramParcel);
                                }
                            if (!localProc.readExcessivePowerFromParcelLocked(paramParcel))
                                break;
                        }
                        label1120: int i11 = paramParcel.readInt();
                        if (i11 > 10000)
                        {
                            Slog.w("BatteryStatsImpl", "File corrupt: too many packages " + i11);
                            break;
                        }
                        for (int i12 = 0; ; i12++)
                        {
                            if (i12 >= i11)
                                break label1348;
                            String str1 = paramParcel.readString();
                            BatteryStatsImpl.Uid.Pkg localPkg = localUid.getPackageStatsLocked(str1);
                            int i13 = paramParcel.readInt();
                            localPkg.mLoadedWakeups = i13;
                            localPkg.mWakeups = i13;
                            int i14 = paramParcel.readInt();
                            if (i14 > 1000)
                            {
                                Slog.w("BatteryStatsImpl", "File corrupt: too many services " + i14);
                                break;
                            }
                            for (int i15 = 0; i15 < i14; i15++)
                            {
                                BatteryStatsImpl.Uid.Pkg.Serv localServ = localUid.getServiceStatsLocked(str1, paramParcel.readString());
                                long l1 = paramParcel.readLong();
                                localServ.mLoadedStartTime = l1;
                                localServ.mStartTime = l1;
                                int i16 = paramParcel.readInt();
                                localServ.mLoadedStarts = i16;
                                localServ.mStarts = i16;
                                int i17 = paramParcel.readInt();
                                localServ.mLoadedLaunches = i17;
                                localServ.mLaunches = i17;
                            }
                        }
                        localUid.mLoadedTcpBytesReceived = paramParcel.readLong();
                        localUid.mLoadedTcpBytesSent = paramParcel.readLong();
                    }
            }
        }
    }

    private void updateAllPhoneStateLocked(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 0;
        int j = 0;
        this.mPhoneServiceStateRaw = paramInt1;
        this.mPhoneSimStateRaw = paramInt2;
        this.mPhoneSignalStrengthBinRaw = paramInt3;
        if ((paramInt2 == 1) && (paramInt1 == 1) && (paramInt3 > 0))
            paramInt1 = 0;
        if (paramInt1 == 3)
        {
            paramInt3 = -1;
            if ((i == 0) && (this.mPhoneSignalScanningTimer.isRunningLocked()))
            {
                BatteryStats.HistoryItem localHistoryItem2 = this.mHistoryCur;
                localHistoryItem2.states = (0xF7FFFFFF & localHistoryItem2.states);
                j = 1;
                this.mPhoneSignalScanningTimer.stopRunningLocked(this);
            }
            if (this.mPhoneServiceState != paramInt1)
            {
                this.mHistoryCur.states = (0xFFFFF0FF & this.mHistoryCur.states | paramInt1 << 8);
                j = 1;
                this.mPhoneServiceState = paramInt1;
            }
            if (this.mPhoneSignalStrengthBin != paramInt3)
            {
                if (this.mPhoneSignalStrengthBin >= 0)
                    this.mPhoneSignalStrengthsTimer[this.mPhoneSignalStrengthBin].stopRunningLocked(this);
                if (paramInt3 < 0)
                    break label285;
                if (!this.mPhoneSignalStrengthsTimer[paramInt3].isRunningLocked())
                    this.mPhoneSignalStrengthsTimer[paramInt3].startRunningLocked(this);
                this.mHistoryCur.states = (0xFFFFFF0F & this.mHistoryCur.states | paramInt3 << 4);
                j = 1;
            }
        }
        while (true)
        {
            this.mPhoneSignalStrengthBin = paramInt3;
            if (j != 0)
                addHistoryRecordLocked(SystemClock.elapsedRealtime());
            return;
            if ((paramInt1 == 0) || (paramInt1 != 1))
                break;
            i = 1;
            paramInt3 = 0;
            if (this.mPhoneSignalScanningTimer.isRunningLocked())
                break;
            BatteryStats.HistoryItem localHistoryItem1 = this.mHistoryCur;
            localHistoryItem1.states = (0x8000000 | localHistoryItem1.states);
            j = 1;
            this.mPhoneSignalScanningTimer.startRunningLocked(this);
            break;
            label285: stopAllSignalStrengthTimersLocked(-1);
        }
    }

    void addHistoryBufferLocked(long paramLong)
    {
        if ((!this.mHaveBatteryLevel) || (!this.mRecordingHistory));
        while (true)
        {
            return;
            long l = paramLong + this.mHistoryBaseTime - this.mHistoryLastWritten.time;
            if ((this.mHistoryBufferLastPos < 0) || (this.mHistoryLastWritten.cmd != 1) || (l >= 2000L) || (((this.mHistoryLastWritten.states ^ this.mHistoryCur.states) & this.mChangedBufferStates) != 0))
                break label299;
            this.mHistoryBuffer.setDataSize(this.mHistoryBufferLastPos);
            this.mHistoryBuffer.setDataPosition(this.mHistoryBufferLastPos);
            this.mHistoryBufferLastPos = -1;
            if ((this.mHistoryLastLastWritten.cmd != 1) || (l >= 500L) || (!this.mHistoryLastLastWritten.same(this.mHistoryCur)))
                break;
            this.mHistoryLastWritten.setTo(this.mHistoryLastLastWritten);
            this.mHistoryLastLastWritten.cmd = 0;
        }
        this.mChangedBufferStates |= this.mHistoryLastWritten.states ^ this.mHistoryCur.states;
        paramLong = this.mHistoryLastWritten.time - this.mHistoryBaseTime;
        this.mHistoryLastWritten.setTo(this.mHistoryLastLastWritten);
        while (true)
        {
            int i = this.mHistoryBuffer.dataSize();
            if (i >= 131072)
            {
                if (!this.mHistoryOverflow)
                {
                    this.mHistoryOverflow = true;
                    addHistoryBufferLocked(paramLong, (byte)3);
                }
                if ((this.mHistoryLastWritten.batteryLevel == this.mHistoryCur.batteryLevel) && ((i >= 147456) || ((0x101C0000 & (this.mHistoryLastWritten.states ^ this.mHistoryCur.states)) == 0)))
                    break;
            }
            addHistoryBufferLocked(paramLong, (byte)1);
            break;
            label299: this.mChangedBufferStates = 0;
        }
    }

    void addHistoryBufferLocked(long paramLong, byte paramByte)
    {
        int i = 0;
        if (this.mIteratingHistory)
        {
            i = this.mHistoryBuffer.dataPosition();
            this.mHistoryBuffer.setDataPosition(this.mHistoryBuffer.dataSize());
        }
        this.mHistoryBufferLastPos = this.mHistoryBuffer.dataPosition();
        this.mHistoryLastLastWritten.setTo(this.mHistoryLastWritten);
        this.mHistoryLastWritten.setTo(paramLong + this.mHistoryBaseTime, paramByte, this.mHistoryCur);
        this.mHistoryLastWritten.writeDelta(this.mHistoryBuffer, this.mHistoryLastLastWritten);
        this.mLastHistoryTime = paramLong;
        if (this.mIteratingHistory)
            this.mHistoryBuffer.setDataPosition(i);
    }

    void addHistoryRecordLocked(long paramLong)
    {
        addHistoryBufferLocked(paramLong);
    }

    void addHistoryRecordLocked(long paramLong, byte paramByte)
    {
        BatteryStats.HistoryItem localHistoryItem = this.mHistoryCache;
        if (localHistoryItem != null)
            this.mHistoryCache = localHistoryItem.next;
        while (true)
        {
            localHistoryItem.setTo(paramLong + this.mHistoryBaseTime, paramByte, this.mHistoryCur);
            addHistoryRecordLocked(localHistoryItem);
            return;
            localHistoryItem = new BatteryStats.HistoryItem();
        }
    }

    void addHistoryRecordLocked(BatteryStats.HistoryItem paramHistoryItem)
    {
        this.mNumHistoryItems = (1 + this.mNumHistoryItems);
        paramHistoryItem.next = null;
        this.mHistoryLastEnd = this.mHistoryEnd;
        if (this.mHistoryEnd != null)
        {
            this.mHistoryEnd.next = paramHistoryItem;
            this.mHistoryEnd = paramHistoryItem;
        }
        while (true)
        {
            return;
            this.mHistoryEnd = paramHistoryItem;
            this.mHistory = paramHistoryItem;
        }
    }

    void clearHistoryLocked()
    {
        this.mHistoryBaseTime = 0L;
        this.mLastHistoryTime = 0L;
        this.mHistoryBuffer.setDataSize(0);
        this.mHistoryBuffer.setDataPosition(0);
        this.mHistoryBuffer.setDataCapacity(65536);
        this.mHistoryLastLastWritten.cmd = 0;
        this.mHistoryLastWritten.cmd = 0;
        this.mHistoryBufferLastPos = -1;
        this.mHistoryOverflow = false;
    }

    // ERROR //
    public void commitPendingDataToDisk()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 373	com/android/internal/os/BatteryStatsImpl:mPendingWrite	Landroid/os/Parcel;
        //     6: astore_2
        //     7: aload_0
        //     8: aconst_null
        //     9: putfield 373	com/android/internal/os/BatteryStatsImpl:mPendingWrite	Landroid/os/Parcel;
        //     12: aload_2
        //     13: ifnonnull +8 -> 21
        //     16: aload_0
        //     17: monitorexit
        //     18: goto +128 -> 146
        //     21: aload_0
        //     22: getfield 378	com/android/internal/os/BatteryStatsImpl:mWriteLock	Ljava/util/concurrent/locks/ReentrantLock;
        //     25: invokevirtual 1008	java/util/concurrent/locks/ReentrantLock:lock	()V
        //     28: aload_0
        //     29: monitorexit
        //     30: new 1010	java/io/FileOutputStream
        //     33: dup
        //     34: aload_0
        //     35: getfield 380	com/android/internal/os/BatteryStatsImpl:mFile	Lcom/android/internal/util/JournaledFile;
        //     38: invokevirtual 1014	com/android/internal/util/JournaledFile:chooseForWrite	()Ljava/io/File;
        //     41: invokespecial 1015	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     44: astore_3
        //     45: aload_3
        //     46: aload_2
        //     47: invokevirtual 1019	android/os/Parcel:marshall	()[B
        //     50: invokevirtual 1023	java/io/FileOutputStream:write	([B)V
        //     53: aload_3
        //     54: invokevirtual 1026	java/io/FileOutputStream:flush	()V
        //     57: aload_3
        //     58: invokestatic 1032	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     61: pop
        //     62: aload_3
        //     63: invokevirtual 1033	java/io/FileOutputStream:close	()V
        //     66: aload_0
        //     67: getfield 380	com/android/internal/os/BatteryStatsImpl:mFile	Lcom/android/internal/util/JournaledFile;
        //     70: invokevirtual 1036	com/android/internal/util/JournaledFile:commit	()V
        //     73: aload_2
        //     74: invokevirtual 1039	android/os/Parcel:recycle	()V
        //     77: aload_0
        //     78: getfield 378	com/android/internal/os/BatteryStatsImpl:mWriteLock	Ljava/util/concurrent/locks/ReentrantLock;
        //     81: astore 7
        //     83: aload 7
        //     85: invokevirtual 1042	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //     88: goto +58 -> 146
        //     91: astore_1
        //     92: aload_0
        //     93: monitorexit
        //     94: aload_1
        //     95: athrow
        //     96: astore 5
        //     98: ldc_w 705
        //     101: ldc_w 1044
        //     104: aload 5
        //     106: invokestatic 1046	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     109: pop
        //     110: aload_0
        //     111: getfield 380	com/android/internal/os/BatteryStatsImpl:mFile	Lcom/android/internal/util/JournaledFile;
        //     114: invokevirtual 1049	com/android/internal/util/JournaledFile:rollback	()V
        //     117: aload_2
        //     118: invokevirtual 1039	android/os/Parcel:recycle	()V
        //     121: aload_0
        //     122: getfield 378	com/android/internal/os/BatteryStatsImpl:mWriteLock	Ljava/util/concurrent/locks/ReentrantLock;
        //     125: astore 7
        //     127: goto -44 -> 83
        //     130: astore 4
        //     132: aload_2
        //     133: invokevirtual 1039	android/os/Parcel:recycle	()V
        //     136: aload_0
        //     137: getfield 378	com/android/internal/os/BatteryStatsImpl:mWriteLock	Ljava/util/concurrent/locks/ReentrantLock;
        //     140: invokevirtual 1042	java/util/concurrent/locks/ReentrantLock:unlock	()V
        //     143: aload 4
        //     145: athrow
        //     146: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	30	91	finally
        //     92	94	91	finally
        //     30	73	96	java/io/IOException
        //     30	73	130	finally
        //     98	117	130	finally
    }

    public long computeBatteryRealtime(long paramLong, int paramInt)
    {
        long l;
        switch (paramInt)
        {
        default:
            l = 0L;
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return l;
            l = this.mBatteryRealtime + getBatteryRealtimeLocked(paramLong);
            continue;
            l = this.mBatteryLastRealtime;
            continue;
            l = getBatteryRealtimeLocked(paramLong);
            continue;
            l = getBatteryRealtimeLocked(paramLong) - this.mUnpluggedBatteryRealtime;
        }
    }

    public long computeBatteryUptime(long paramLong, int paramInt)
    {
        long l;
        switch (paramInt)
        {
        default:
            l = 0L;
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return l;
            l = this.mBatteryUptime + getBatteryUptime(paramLong);
            continue;
            l = this.mBatteryLastUptime;
            continue;
            l = getBatteryUptime(paramLong);
            continue;
            l = getBatteryUptimeLocked(paramLong) - this.mUnpluggedBatteryUptime;
        }
    }

    public long computeRealtime(long paramLong, int paramInt)
    {
        long l;
        switch (paramInt)
        {
        default:
            l = 0L;
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return l;
            l = this.mRealtime + (paramLong - this.mRealtimeStart);
            continue;
            l = this.mLastRealtime;
            continue;
            l = paramLong - this.mRealtimeStart;
            continue;
            l = paramLong - this.mTrackBatteryRealtimeStart;
        }
    }

    public long computeUptime(long paramLong, int paramInt)
    {
        long l;
        switch (paramInt)
        {
        default:
            l = 0L;
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return l;
            l = this.mUptime + (paramLong - this.mUptimeStart);
            continue;
            l = this.mLastUptime;
            continue;
            l = paramLong - this.mUptimeStart;
            continue;
            l = paramLong - this.mTrackBatteryUptimeStart;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void distributeWorkLocked(int paramInt)
    {
        Uid localUid1 = (Uid)this.mUidStats.get(1010);
        if (localUid1 != null)
        {
            long l1 = computeBatteryRealtime(1000L * SystemClock.elapsedRealtime(), paramInt);
            Iterator localIterator = localUid1.mProcessStats.values().iterator();
            while (localIterator.hasNext())
            {
                BatteryStatsImpl.Uid.Proc localProc1 = (BatteryStatsImpl.Uid.Proc)localIterator.next();
                long l2 = getGlobalWifiRunningTime(l1, paramInt);
                for (int i = 0; i < this.mUidStats.size(); i++)
                {
                    Uid localUid2 = (Uid)this.mUidStats.valueAt(i);
                    if (localUid2.mUid != 1010)
                    {
                        long l3 = localUid2.getWifiRunningTime(l1, paramInt);
                        if (l3 > 0L)
                        {
                            BatteryStatsImpl.Uid.Proc localProc2 = localUid2.getProcessStatsLocked("*wifi*");
                            long l4 = l3 * localProc1.getUserTime(paramInt) / l2;
                            localProc2.mUserTime = (l4 + localProc2.mUserTime);
                            localProc1.mUserTime -= l4;
                            long l5 = l3 * localProc1.getSystemTime(paramInt) / l2;
                            localProc2.mSystemTime = (l5 + localProc2.mSystemTime);
                            localProc1.mSystemTime -= l5;
                            long l6 = l3 * localProc1.getForegroundTime(paramInt) / l2;
                            localProc2.mForegroundTime = (l6 + localProc2.mForegroundTime);
                            localProc1.mForegroundTime -= l6;
                            for (int j = 0; j < localProc1.mSpeedBins.length; j++)
                            {
                                SamplingCounter localSamplingCounter1 = localProc1.mSpeedBins[j];
                                if (localSamplingCounter1 != null)
                                {
                                    long l7 = l3 * localSamplingCounter1.getCountLocked(paramInt) / l2;
                                    SamplingCounter localSamplingCounter2 = localProc2.mSpeedBins[j];
                                    if (localSamplingCounter2 == null)
                                    {
                                        ArrayList localArrayList = this.mUnpluggables;
                                        localSamplingCounter2 = new SamplingCounter(localArrayList);
                                        localProc2.mSpeedBins[j] = localSamplingCounter2;
                                    }
                                    localSamplingCounter2.mCount.addAndGet((int)l7);
                                    localSamplingCounter1.mCount.addAndGet((int)-l7);
                                }
                            }
                            l2 -= l3;
                        }
                    }
                }
            }
        }
    }

    public void doPlugLocked(long paramLong1, long paramLong2)
    {
        for (int i = -1 + this.mUidStats.size(); i >= 0; i--)
        {
            Uid localUid = (Uid)this.mUidStats.valueAt(i);
            if (localUid.mStartedTcpBytesReceived >= 0L)
            {
                localUid.mCurrentTcpBytesReceived = localUid.computeCurrentTcpBytesReceived();
                localUid.mStartedTcpBytesReceived = -1L;
            }
            if (localUid.mStartedTcpBytesSent >= 0L)
            {
                localUid.mCurrentTcpBytesSent = localUid.computeCurrentTcpBytesSent();
                localUid.mStartedTcpBytesSent = -1L;
            }
        }
        for (int j = -1 + this.mUnpluggables.size(); j >= 0; j--)
            ((Unpluggable)this.mUnpluggables.get(j)).plug(paramLong1, paramLong2);
        NetworkStats localNetworkStats = getNetworkStatsSummary();
        NetworkStats.Entry localEntry1 = localNetworkStats.getTotal(null, this.mMobileIfaces);
        doDataPlug(this.mMobileDataRx, localEntry1.rxBytes);
        doDataPlug(this.mMobileDataTx, localEntry1.txBytes);
        NetworkStats.Entry localEntry2 = localNetworkStats.getTotal(localEntry1);
        doDataPlug(this.mTotalDataRx, localEntry2.rxBytes);
        doDataPlug(this.mTotalDataTx, localEntry2.txBytes);
        this.mRadioDataUptime = getRadioDataUptime();
        this.mRadioDataStart = -1L;
        this.mBluetoothPingCount = getBluetoothPingCount();
        this.mBluetoothPingStart = -1;
    }

    public void doUnplugLocked(long paramLong1, long paramLong2)
    {
        NetworkStats.Entry localEntry1 = null;
        NetworkStats localNetworkStats1 = getNetworkStatsDetailGroupedByUid();
        int i = localNetworkStats1.size();
        int j = 0;
        if (j < i)
        {
            localEntry1 = localNetworkStats1.getValues(j, localEntry1);
            Uid localUid = (Uid)this.mUidStats.get(localEntry1.uid);
            if (localUid == null);
            while (true)
            {
                j++;
                break;
                localUid.mStartedTcpBytesReceived = localEntry1.rxBytes;
                localUid.mStartedTcpBytesSent = localEntry1.txBytes;
                localUid.mTcpBytesReceivedAtLastUnplug = localUid.mCurrentTcpBytesReceived;
                localUid.mTcpBytesSentAtLastUnplug = localUid.mCurrentTcpBytesSent;
            }
        }
        for (int k = -1 + this.mUnpluggables.size(); k >= 0; k--)
            ((Unpluggable)this.mUnpluggables.get(k)).unplug(paramLong1, paramLong2);
        NetworkStats localNetworkStats2 = getNetworkStatsSummary();
        NetworkStats.Entry localEntry2 = localNetworkStats2.getTotal(localEntry1, this.mMobileIfaces);
        doDataUnplug(this.mMobileDataRx, localEntry2.rxBytes);
        doDataUnplug(this.mMobileDataTx, localEntry2.txBytes);
        NetworkStats.Entry localEntry3 = localNetworkStats2.getTotal(localEntry2);
        doDataUnplug(this.mTotalDataRx, localEntry3.rxBytes);
        doDataUnplug(this.mTotalDataTx, localEntry3.txBytes);
        this.mRadioDataStart = getCurrentRadioDataUptime();
        this.mRadioDataUptime = 0L;
        this.mBluetoothPingStart = getCurrentBluetoothPingCount();
        this.mBluetoothPingCount = 0;
    }

    public void dumpLocked(PrintWriter paramPrintWriter)
    {
        super.dumpLocked(paramPrintWriter);
    }

    public void finishAddingCpuLocked(int paramInt1, int paramInt2, int paramInt3, long[] paramArrayOfLong)
    {
        int i = this.mPartialTimers.size();
        if (paramInt1 != 0)
        {
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++)
            {
                StopwatchTimer localStopwatchTimer3 = (StopwatchTimer)this.mPartialTimers.get(i5);
                if (localStopwatchTimer3.mInList)
                {
                    Uid localUid3 = localStopwatchTimer3.mUid;
                    if ((localUid3 != null) && (localUid3.mUid != 1000))
                        i4++;
                }
            }
            if (i4 != 0)
                for (int i6 = 0; i6 < i; i6++)
                {
                    StopwatchTimer localStopwatchTimer2 = (StopwatchTimer)this.mPartialTimers.get(i6);
                    if (localStopwatchTimer2.mInList)
                    {
                        Uid localUid2 = localStopwatchTimer2.mUid;
                        if ((localUid2 != null) && (localUid2.mUid != 1000))
                        {
                            int i7 = paramInt2 / i4;
                            int i8 = paramInt3 / i4;
                            paramInt2 -= i7;
                            paramInt3 -= i8;
                            i4--;
                            BatteryStatsImpl.Uid.Proc localProc2 = localUid2.getProcessStatsLocked("*wakelock*");
                            localProc2.addCpuTimeLocked(i7, i8);
                            localProc2.addSpeedStepTimes(paramArrayOfLong);
                        }
                    }
                }
            if ((paramInt2 != 0) || (paramInt3 != 0))
            {
                Uid localUid1 = getUidStatsLocked(1000);
                if (localUid1 != null)
                {
                    BatteryStatsImpl.Uid.Proc localProc1 = localUid1.getProcessStatsLocked("*lost*");
                    localProc1.addCpuTimeLocked(paramInt2, paramInt3);
                    localProc1.addSpeedStepTimes(paramArrayOfLong);
                }
            }
        }
        int j = this.mLastPartialTimers.size();
        int k;
        int m;
        if (i != j)
        {
            k = 1;
            m = 0;
            label265: if ((m >= j) || (k != 0))
                break label326;
            if (this.mPartialTimers.get(m) == this.mLastPartialTimers.get(m))
                break label320;
        }
        label320: for (int i3 = 1; ; i3 = 0)
        {
            k |= i3;
            m++;
            break label265;
            k = 0;
            break;
        }
        label326: if (k == 0)
            for (int i2 = 0; i2 < j; i2++)
                ((StopwatchTimer)this.mPartialTimers.get(i2)).mInList = true;
        for (int n = 0; n < j; n++)
            ((StopwatchTimer)this.mLastPartialTimers.get(n)).mInList = false;
        this.mLastPartialTimers.clear();
        for (int i1 = 0; i1 < i; i1++)
        {
            StopwatchTimer localStopwatchTimer1 = (StopwatchTimer)this.mPartialTimers.get(i1);
            localStopwatchTimer1.mInList = true;
            this.mLastPartialTimers.add(localStopwatchTimer1);
        }
    }

    public void finishIteratingHistoryLocked()
    {
        this.mIteratingHistory = false;
        this.mHistoryBuffer.setDataPosition(this.mHistoryBuffer.dataSize());
    }

    public void finishIteratingOldHistoryLocked()
    {
        this.mIteratingHistory = false;
        this.mHistoryBuffer.setDataPosition(this.mHistoryBuffer.dataSize());
    }

    public long getAwakeTimeBattery()
    {
        return computeBatteryUptime(getBatteryUptimeLocked(), 2);
    }

    public long getAwakeTimePlugged()
    {
        return 1000L * SystemClock.uptimeMillis() - getAwakeTimeBattery();
    }

    public long getBatteryRealtime(long paramLong)
    {
        return getBatteryRealtimeLocked(paramLong);
    }

    long getBatteryRealtimeLocked(long paramLong)
    {
        long l = this.mTrackBatteryPastRealtime;
        if (this.mOnBatteryInternal)
            l += paramLong - this.mTrackBatteryRealtimeStart;
        return l;
    }

    public long getBatteryUptime(long paramLong)
    {
        return getBatteryUptimeLocked(paramLong);
    }

    long getBatteryUptimeLocked()
    {
        return getBatteryUptime(1000L * SystemClock.uptimeMillis());
    }

    long getBatteryUptimeLocked(long paramLong)
    {
        long l = this.mTrackBatteryPastUptime;
        if (this.mOnBatteryInternal)
            l += paramLong - this.mTrackBatteryUptimeStart;
        return l;
    }

    public long getBluetoothOnTime(long paramLong, int paramInt)
    {
        return this.mBluetoothOnTimer.getTotalTimeLocked(paramLong, paramInt);
    }

    public int getBluetoothPingCount()
    {
        int i;
        if (this.mBluetoothPingStart == -1)
            i = this.mBluetoothPingCount;
        while (true)
        {
            return i;
            if (this.mBtHeadset != null)
                i = getCurrentBluetoothPingCount() - this.mBluetoothPingStart;
            else
                i = 0;
        }
    }

    public int getCpuSpeedSteps()
    {
        return sNumSpeedSteps;
    }

    public int getDischargeAmountScreenOff()
    {
        try
        {
            int i = this.mDischargeAmountScreenOff;
            if ((this.mOnBattery) && (!this.mScreenOn) && (this.mDischargeCurrentLevel < this.mDischargeScreenOffUnplugLevel))
                i += this.mDischargeScreenOffUnplugLevel - this.mDischargeCurrentLevel;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getDischargeAmountScreenOffSinceCharge()
    {
        try
        {
            int i = this.mDischargeAmountScreenOffSinceCharge;
            if ((this.mOnBattery) && (!this.mScreenOn) && (this.mDischargeCurrentLevel < this.mDischargeScreenOffUnplugLevel))
                i += this.mDischargeScreenOffUnplugLevel - this.mDischargeCurrentLevel;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getDischargeAmountScreenOn()
    {
        try
        {
            int i = this.mDischargeAmountScreenOn;
            if ((this.mOnBattery) && (this.mScreenOn) && (this.mDischargeCurrentLevel < this.mDischargeScreenOnUnplugLevel))
                i += this.mDischargeScreenOnUnplugLevel - this.mDischargeCurrentLevel;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getDischargeAmountScreenOnSinceCharge()
    {
        try
        {
            int i = this.mDischargeAmountScreenOnSinceCharge;
            if ((this.mOnBattery) && (this.mScreenOn) && (this.mDischargeCurrentLevel < this.mDischargeScreenOnUnplugLevel))
                i += this.mDischargeScreenOnUnplugLevel - this.mDischargeCurrentLevel;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getDischargeCurrentLevel()
    {
        try
        {
            int i = getDischargeCurrentLevelLocked();
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getDischargeCurrentLevelLocked()
    {
        return this.mDischargeCurrentLevel;
    }

    public int getDischargeStartLevel()
    {
        try
        {
            int i = getDischargeStartLevelLocked();
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getDischargeStartLevelLocked()
    {
        return this.mDischargeUnplugLevel;
    }

    public long getGlobalWifiRunningTime(long paramLong, int paramInt)
    {
        return this.mGlobalWifiRunningTimer.getTotalTimeLocked(paramLong, paramInt);
    }

    public int getHighDischargeAmountSinceCharge()
    {
        try
        {
            int i = this.mHighDischargeAmountSinceCharge;
            if ((this.mOnBattery) && (this.mDischargeCurrentLevel < this.mDischargeUnplugLevel))
                i += this.mDischargeUnplugLevel - this.mDischargeCurrentLevel;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public long getHistoryBaseTime()
    {
        return this.mHistoryBaseTime;
    }

    public int getInputEventCount(int paramInt)
    {
        return this.mInputEventCounter.getCountLocked(paramInt);
    }

    public boolean getIsOnBattery()
    {
        return this.mOnBattery;
    }

    public Map<String, ? extends SamplingTimer> getKernelWakelockStats()
    {
        return this.mKernelWakelockStats;
    }

    public SamplingTimer getKernelWakelockTimerLocked(String paramString)
    {
        SamplingTimer localSamplingTimer = (SamplingTimer)this.mKernelWakelockStats.get(paramString);
        if (localSamplingTimer == null)
        {
            localSamplingTimer = new SamplingTimer(this.mUnpluggables, this.mOnBatteryInternal, true);
            this.mKernelWakelockStats.put(paramString, localSamplingTimer);
        }
        return localSamplingTimer;
    }

    public int getLowDischargeAmountSinceCharge()
    {
        try
        {
            int i = this.mLowDischargeAmountSinceCharge;
            if ((this.mOnBattery) && (this.mDischargeCurrentLevel < this.mDischargeUnplugLevel))
                i += -1 + (this.mDischargeUnplugLevel - this.mDischargeCurrentLevel);
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public long getMobileTcpBytesReceived(int paramInt)
    {
        return getTcpBytes(getNetworkStatsSummary().getTotal(null, this.mMobileIfaces).rxBytes, this.mMobileDataRx, paramInt);
    }

    public long getMobileTcpBytesSent(int paramInt)
    {
        return getTcpBytes(getNetworkStatsSummary().getTotal(null, this.mMobileIfaces).txBytes, this.mMobileDataTx, paramInt);
    }

    public boolean getNextHistoryLocked(BatteryStats.HistoryItem paramHistoryItem)
    {
        boolean bool = false;
        int i = this.mHistoryBuffer.dataPosition();
        if (i == 0)
            paramHistoryItem.clear();
        int j;
        if (i >= this.mHistoryBuffer.dataSize())
        {
            j = 1;
            if (j == 0)
                break label45;
        }
        while (true)
        {
            return bool;
            j = 0;
            break;
            label45: paramHistoryItem.readDelta(this.mHistoryBuffer);
            bool = true;
        }
    }

    public boolean getNextOldHistoryLocked(BatteryStats.HistoryItem paramHistoryItem)
    {
        boolean bool1 = false;
        int i;
        boolean bool2;
        if (this.mHistoryBuffer.dataPosition() >= this.mHistoryBuffer.dataSize())
        {
            i = 1;
            if (i == 0)
            {
                this.mHistoryReadTmp.readDelta(this.mHistoryBuffer);
                bool2 = this.mReadOverflow;
                if (this.mHistoryReadTmp.cmd != 3)
                    break label103;
            }
        }
        BatteryStats.HistoryItem localHistoryItem;
        label103: for (boolean bool3 = true; ; bool3 = false)
        {
            this.mReadOverflow = (bool3 | bool2);
            localHistoryItem = this.mHistoryIterator;
            if (localHistoryItem != null)
                break label109;
            if ((!this.mReadOverflow) && (i == 0))
                Slog.w("BatteryStatsImpl", "Old history ends before new history!");
            return bool1;
            i = 0;
            break;
        }
        label109: paramHistoryItem.setTo(localHistoryItem);
        this.mHistoryIterator = localHistoryItem.next;
        if (!this.mReadOverflow)
        {
            if (i == 0)
                break label149;
            Slog.w("BatteryStatsImpl", "New history ends before old history!");
        }
        while (true)
        {
            bool1 = true;
            break;
            label149: if (!paramHistoryItem.same(this.mHistoryReadTmp))
            {
                long l = getHistoryBaseTime() + SystemClock.elapsedRealtime();
                PrintWriter localPrintWriter = new PrintWriter(new LogWriter(5, "BatteryStatsImpl"));
                localPrintWriter.println("Histories differ!");
                localPrintWriter.println("Old history:");
                new BatteryStats.HistoryPrinter().printNextItem(localPrintWriter, paramHistoryItem, l);
                localPrintWriter.println("New history:");
                new BatteryStats.HistoryPrinter().printNextItem(localPrintWriter, this.mHistoryReadTmp, l);
            }
        }
    }

    public BatteryStatsImpl.Uid.Pkg getPackageStatsLocked(int paramInt, String paramString)
    {
        return getUidStatsLocked(paramInt).getPackageStatsLocked(paramString);
    }

    public int getPhoneDataConnectionCount(int paramInt1, int paramInt2)
    {
        return this.mPhoneDataConnectionsTimer[paramInt1].getCountLocked(paramInt2);
    }

    public long getPhoneDataConnectionTime(int paramInt1, long paramLong, int paramInt2)
    {
        return this.mPhoneDataConnectionsTimer[paramInt1].getTotalTimeLocked(paramLong, paramInt2);
    }

    public long getPhoneOnTime(long paramLong, int paramInt)
    {
        return this.mPhoneOnTimer.getTotalTimeLocked(paramLong, paramInt);
    }

    public long getPhoneSignalScanningTime(long paramLong, int paramInt)
    {
        return this.mPhoneSignalScanningTimer.getTotalTimeLocked(paramLong, paramInt);
    }

    public int getPhoneSignalStrengthCount(int paramInt1, int paramInt2)
    {
        return this.mPhoneDataConnectionsTimer[paramInt1].getCountLocked(paramInt2);
    }

    public long getPhoneSignalStrengthTime(int paramInt1, long paramLong, int paramInt2)
    {
        return this.mPhoneSignalStrengthsTimer[paramInt1].getTotalTimeLocked(paramLong, paramInt2);
    }

    public BatteryStatsImpl.Uid.Proc getProcessStatsLocked(int paramInt, String paramString)
    {
        return getUidStatsLocked(paramInt).getProcessStatsLocked(paramString);
    }

    public BatteryStatsImpl.Uid.Proc getProcessStatsLocked(String paramString, int paramInt)
    {
        int i;
        if (this.mUidCache.containsKey(paramString))
            i = ((Integer)this.mUidCache.get(paramString)).intValue();
        while (true)
        {
            return getUidStatsLocked(i).getProcessStatsLocked(paramString);
            i = Process.getUidForPid(paramInt);
            this.mUidCache.put(paramString, Integer.valueOf(i));
        }
    }

    public long getProcessWakeTime(int paramInt1, int paramInt2, long paramLong)
    {
        long l1 = 0L;
        Uid localUid = (Uid)this.mUidStats.get(paramInt1);
        if (localUid != null)
        {
            BatteryStats.Uid.Pid localPid = (BatteryStats.Uid.Pid)localUid.mPids.get(paramInt2);
            if (localPid != null)
            {
                long l2 = localPid.mWakeSum;
                if (localPid.mWakeStart != l1)
                    l1 = paramLong - localPid.mWakeStart;
                l1 += l2;
            }
        }
        return l1;
    }

    public long getRadioDataUptime()
    {
        if (this.mRadioDataStart == -1L);
        for (long l = this.mRadioDataUptime; ; l = getCurrentRadioDataUptime() - this.mRadioDataStart)
            return l;
    }

    public long getRadioDataUptimeMs()
    {
        return getRadioDataUptime() / 1000L;
    }

    public long getScreenBrightnessTime(int paramInt1, long paramLong, int paramInt2)
    {
        return this.mScreenBrightnessTimer[paramInt1].getTotalTimeLocked(paramLong, paramInt2);
    }

    public long getScreenOnTime(long paramLong, int paramInt)
    {
        return this.mScreenOnTimer.getTotalTimeLocked(paramLong, paramInt);
    }

    public BatteryStatsImpl.Uid.Pkg.Serv getServiceStatsLocked(int paramInt, String paramString1, String paramString2)
    {
        return getUidStatsLocked(paramInt).getServiceStatsLocked(paramString1, paramString2);
    }

    public int getStartCount()
    {
        return this.mStartCount;
    }

    public long getTotalTcpBytesReceived(int paramInt)
    {
        return getTcpBytes(getNetworkStatsSummary().getTotal(null).rxBytes, this.mTotalDataRx, paramInt);
    }

    public long getTotalTcpBytesSent(int paramInt)
    {
        return getTcpBytes(getNetworkStatsSummary().getTotal(null).txBytes, this.mTotalDataTx, paramInt);
    }

    public SparseArray<? extends BatteryStats.Uid> getUidStats()
    {
        return this.mUidStats;
    }

    public Uid getUidStatsLocked(int paramInt)
    {
        Uid localUid = (Uid)this.mUidStats.get(paramInt);
        if (localUid == null)
        {
            localUid = new Uid(paramInt);
            this.mUidStats.put(paramInt, localUid);
        }
        return localUid;
    }

    public long getWifiOnTime(long paramLong, int paramInt)
    {
        return this.mWifiOnTimer.getTotalTimeLocked(paramLong, paramInt);
    }

    void initDischarge()
    {
        this.mLowDischargeAmountSinceCharge = 0;
        this.mHighDischargeAmountSinceCharge = 0;
        this.mDischargeAmountScreenOn = 0;
        this.mDischargeAmountScreenOnSinceCharge = 0;
        this.mDischargeAmountScreenOff = 0;
        this.mDischargeAmountScreenOffSinceCharge = 0;
    }

    void initTimes()
    {
        this.mTrackBatteryPastUptime = 0L;
        this.mBatteryRealtime = 0L;
        this.mTrackBatteryPastRealtime = 0L;
        this.mBatteryUptime = 0L;
        long l1 = 1000L * SystemClock.uptimeMillis();
        this.mTrackBatteryUptimeStart = l1;
        this.mUptimeStart = l1;
        long l2 = 1000L * SystemClock.elapsedRealtime();
        this.mTrackBatteryRealtimeStart = l2;
        this.mRealtimeStart = l2;
        this.mUnpluggedBatteryUptime = getBatteryUptimeLocked(this.mUptimeStart);
        this.mUnpluggedBatteryRealtime = getBatteryRealtimeLocked(this.mRealtimeStart);
    }

    public boolean isOnBattery()
    {
        return this.mOnBattery;
    }

    public boolean isScreenOn()
    {
        return this.mScreenOn;
    }

    public void noteAudioOffLocked(int paramInt)
    {
        if (this.mAudioOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFFBFFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mAudioOn = false;
            this.mAudioOnTimer.stopRunningLocked(this);
        }
        getUidStatsLocked(paramInt).noteAudioTurnedOffLocked();
    }

    public void noteAudioOnLocked(int paramInt)
    {
        if (!this.mAudioOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x400000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mAudioOn = true;
            this.mAudioOnTimer.startRunningLocked(this);
        }
        getUidStatsLocked(paramInt).noteAudioTurnedOnLocked();
    }

    public void noteBluetoothOffLocked()
    {
        if (this.mBluetoothOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFFFEFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mBluetoothOn = false;
            this.mBluetoothOnTimer.stopRunningLocked(this);
        }
    }

    public void noteBluetoothOnLocked()
    {
        if (!this.mBluetoothOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x10000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mBluetoothOn = true;
            this.mBluetoothOnTimer.startRunningLocked(this);
        }
    }

    public void noteFullWifiLockAcquiredFromSourceLocked(WorkSource paramWorkSource)
    {
        int i = paramWorkSource.size();
        for (int j = 0; j < i; j++)
            noteFullWifiLockAcquiredLocked(paramWorkSource.get(j));
    }

    public void noteFullWifiLockAcquiredLocked(int paramInt)
    {
        if (this.mWifiFullLockNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x2000000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        this.mWifiFullLockNesting = (1 + this.mWifiFullLockNesting);
        getUidStatsLocked(paramInt).noteFullWifiLockAcquiredLocked();
    }

    public void noteFullWifiLockReleasedFromSourceLocked(WorkSource paramWorkSource)
    {
        int i = paramWorkSource.size();
        for (int j = 0; j < i; j++)
            noteFullWifiLockReleasedLocked(paramWorkSource.get(j));
    }

    public void noteFullWifiLockReleasedLocked(int paramInt)
    {
        this.mWifiFullLockNesting = (-1 + this.mWifiFullLockNesting);
        if (this.mWifiFullLockNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFDFFFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        getUidStatsLocked(paramInt).noteFullWifiLockReleasedLocked();
    }

    public void noteInputEventAtomic()
    {
        this.mInputEventCounter.stepAtomic();
    }

    public void noteNetworkInterfaceTypeLocked(String paramString, int paramInt)
    {
        if (ConnectivityManager.isNetworkTypeMobile(paramInt))
            this.mMobileIfaces.add(paramString);
        while (true)
        {
            return;
            this.mMobileIfaces.remove(paramString);
        }
    }

    public void notePhoneDataConnectionStateLocked(int paramInt, boolean paramBoolean)
    {
        int i = 0;
        if (paramBoolean)
            switch (paramInt)
            {
            default:
                i = 15;
            case 2:
            case 1:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            }
        while (true)
        {
            if (this.mPhoneDataConnectionType != i)
            {
                this.mHistoryCur.states = (0xFFFF0FFF & this.mHistoryCur.states | i << 12);
                addHistoryRecordLocked(SystemClock.elapsedRealtime());
                if (this.mPhoneDataConnectionType >= 0)
                    this.mPhoneDataConnectionsTimer[this.mPhoneDataConnectionType].stopRunningLocked(this);
                this.mPhoneDataConnectionType = i;
                this.mPhoneDataConnectionsTimer[i].startRunningLocked(this);
            }
            return;
            i = 2;
            continue;
            i = 1;
            continue;
            i = 3;
            continue;
            i = 4;
            continue;
            i = 5;
            continue;
            i = 6;
            continue;
            i = 7;
            continue;
            i = 8;
            continue;
            i = 9;
            continue;
            i = 10;
            continue;
            i = 11;
            continue;
            i = 12;
            continue;
            i = 13;
            continue;
            i = 14;
        }
    }

    public void notePhoneOffLocked()
    {
        if (this.mPhoneOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFFFBFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mPhoneOn = false;
            this.mPhoneOnTimer.stopRunningLocked(this);
        }
    }

    public void notePhoneOnLocked()
    {
        if (!this.mPhoneOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x40000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mPhoneOn = true;
            this.mPhoneOnTimer.startRunningLocked(this);
        }
    }

    public void notePhoneSignalStrengthLocked(SignalStrength paramSignalStrength)
    {
        int i = paramSignalStrength.getLevel();
        updateAllPhoneStateLocked(this.mPhoneServiceStateRaw, this.mPhoneSimStateRaw, i);
    }

    public void notePhoneStateLocked(int paramInt1, int paramInt2)
    {
        updateAllPhoneStateLocked(paramInt1, paramInt2, this.mPhoneSignalStrengthBinRaw);
    }

    public void noteProcessDiedLocked(int paramInt1, int paramInt2)
    {
        Uid localUid = (Uid)this.mUidStats.get(paramInt1);
        if (localUid != null)
            localUid.mPids.remove(paramInt2);
    }

    public void noteScanWifiLockAcquiredFromSourceLocked(WorkSource paramWorkSource)
    {
        int i = paramWorkSource.size();
        for (int j = 0; j < i; j++)
            noteScanWifiLockAcquiredLocked(paramWorkSource.get(j));
    }

    public void noteScanWifiLockAcquiredLocked(int paramInt)
    {
        if (this.mWifiScanLockNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x1000000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        this.mWifiScanLockNesting = (1 + this.mWifiScanLockNesting);
        getUidStatsLocked(paramInt).noteScanWifiLockAcquiredLocked();
    }

    public void noteScanWifiLockReleasedFromSourceLocked(WorkSource paramWorkSource)
    {
        int i = paramWorkSource.size();
        for (int j = 0; j < i; j++)
            noteScanWifiLockReleasedLocked(paramWorkSource.get(j));
    }

    public void noteScanWifiLockReleasedLocked(int paramInt)
    {
        this.mWifiScanLockNesting = (-1 + this.mWifiScanLockNesting);
        if (this.mWifiScanLockNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFEFFFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        getUidStatsLocked(paramInt).noteScanWifiLockReleasedLocked();
    }

    public void noteScreenBrightnessLocked(int paramInt)
    {
        int i = paramInt / 51;
        if (i < 0)
            i = 0;
        while (true)
        {
            if (this.mScreenBrightnessBin != i)
            {
                this.mHistoryCur.states = (0xFFFFFFF0 & this.mHistoryCur.states | i << 0);
                addHistoryRecordLocked(SystemClock.elapsedRealtime());
                if (this.mScreenOn)
                {
                    if (this.mScreenBrightnessBin >= 0)
                        this.mScreenBrightnessTimer[this.mScreenBrightnessBin].stopRunningLocked(this);
                    this.mScreenBrightnessTimer[i].startRunningLocked(this);
                }
                this.mScreenBrightnessBin = i;
            }
            return;
            if (i >= 5)
                i = 4;
        }
    }

    public void noteScreenOffLocked()
    {
        if (this.mScreenOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFFEFFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mScreenOn = false;
            this.mScreenOnTimer.stopRunningLocked(this);
            if (this.mScreenBrightnessBin >= 0)
                this.mScreenBrightnessTimer[this.mScreenBrightnessBin].stopRunningLocked(this);
            noteStopWakeLocked(-1, -1, "dummy", 0);
            if (this.mOnBatteryInternal)
                updateDischargeScreenLevelsLocked(true, false);
        }
    }

    public void noteScreenOnLocked()
    {
        if (!this.mScreenOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x100000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mScreenOn = true;
            this.mScreenOnTimer.startRunningLocked(this);
            if (this.mScreenBrightnessBin >= 0)
                this.mScreenBrightnessTimer[this.mScreenBrightnessBin].startRunningLocked(this);
            noteStartWakeLocked(-1, -1, "dummy", 0);
            if (this.mOnBatteryInternal)
                updateDischargeScreenLevelsLocked(false, true);
        }
    }

    public void noteStartGpsLocked(int paramInt)
    {
        if (this.mGpsNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x10000000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        this.mGpsNesting = (1 + this.mGpsNesting);
        getUidStatsLocked(paramInt).noteStartGps();
    }

    public void noteStartSensorLocked(int paramInt1, int paramInt2)
    {
        if (this.mSensorNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x20000000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        this.mSensorNesting = (1 + this.mSensorNesting);
        getUidStatsLocked(paramInt1).noteStartSensor(paramInt2);
    }

    public void noteStartWakeFromSourceLocked(WorkSource paramWorkSource, int paramInt1, String paramString, int paramInt2)
    {
        int i = paramWorkSource.size();
        for (int j = 0; j < i; j++)
            noteStartWakeLocked(paramWorkSource.get(j), paramInt1, paramString, paramInt2);
    }

    public void noteStartWakeLocked(int paramInt1, int paramInt2, String paramString, int paramInt3)
    {
        if (paramInt3 == 0)
        {
            if (this.mWakeLockNesting == 0)
            {
                BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
                localHistoryItem.states = (0x40000000 | localHistoryItem.states);
                addHistoryRecordLocked(SystemClock.elapsedRealtime());
            }
            this.mWakeLockNesting = (1 + this.mWakeLockNesting);
        }
        if (paramInt1 >= 0)
        {
            if (!this.mHandler.hasMessages(1))
            {
                Message localMessage = this.mHandler.obtainMessage(1);
                this.mHandler.sendMessageDelayed(localMessage, 5000L);
            }
            getUidStatsLocked(paramInt1).noteStartWakeLocked(paramInt2, paramString, paramInt3);
        }
    }

    public void noteStopGpsLocked(int paramInt)
    {
        this.mGpsNesting = (-1 + this.mGpsNesting);
        if (this.mGpsNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xEFFFFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        getUidStatsLocked(paramInt).noteStopGps();
    }

    public void noteStopSensorLocked(int paramInt1, int paramInt2)
    {
        this.mSensorNesting = (-1 + this.mSensorNesting);
        if (this.mSensorNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xDFFFFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        getUidStatsLocked(paramInt1).noteStopSensor(paramInt2);
    }

    public void noteStopWakeFromSourceLocked(WorkSource paramWorkSource, int paramInt1, String paramString, int paramInt2)
    {
        int i = paramWorkSource.size();
        for (int j = 0; j < i; j++)
            noteStopWakeLocked(paramWorkSource.get(j), paramInt1, paramString, paramInt2);
    }

    public void noteStopWakeLocked(int paramInt1, int paramInt2, String paramString, int paramInt3)
    {
        if (paramInt3 == 0)
        {
            this.mWakeLockNesting = (-1 + this.mWakeLockNesting);
            if (this.mWakeLockNesting == 0)
            {
                BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
                localHistoryItem.states = (0xBFFFFFFF & localHistoryItem.states);
                addHistoryRecordLocked(SystemClock.elapsedRealtime());
            }
        }
        if (paramInt1 >= 0)
        {
            if (!this.mHandler.hasMessages(1))
            {
                Message localMessage = this.mHandler.obtainMessage(1);
                this.mHandler.sendMessageDelayed(localMessage, 5000L);
            }
            getUidStatsLocked(paramInt1).noteStopWakeLocked(paramInt2, paramString, paramInt3);
        }
    }

    public void noteUserActivityLocked(int paramInt1, int paramInt2)
    {
        getUidStatsLocked(paramInt1).noteUserActivityLocked(paramInt2);
    }

    public void noteVideoOffLocked(int paramInt)
    {
        if (this.mVideoOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFFDFFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mVideoOn = false;
            this.mVideoOnTimer.stopRunningLocked(this);
        }
        getUidStatsLocked(paramInt).noteVideoTurnedOffLocked();
    }

    public void noteVideoOnLocked(int paramInt)
    {
        if (!this.mVideoOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x200000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mVideoOn = true;
            this.mVideoOnTimer.startRunningLocked(this);
        }
        getUidStatsLocked(paramInt).noteVideoTurnedOnLocked();
    }

    public void noteWifiMulticastDisabledFromSourceLocked(WorkSource paramWorkSource)
    {
        int i = paramWorkSource.size();
        for (int j = 0; j < i; j++)
            noteWifiMulticastDisabledLocked(paramWorkSource.get(j));
    }

    public void noteWifiMulticastDisabledLocked(int paramInt)
    {
        this.mWifiMulticastNesting = (-1 + this.mWifiMulticastNesting);
        if (this.mWifiMulticastNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFF7FFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        getUidStatsLocked(paramInt).noteWifiMulticastDisabledLocked();
    }

    public void noteWifiMulticastEnabledFromSourceLocked(WorkSource paramWorkSource)
    {
        int i = paramWorkSource.size();
        for (int j = 0; j < i; j++)
            noteWifiMulticastEnabledLocked(paramWorkSource.get(j));
    }

    public void noteWifiMulticastEnabledLocked(int paramInt)
    {
        if (this.mWifiMulticastNesting == 0)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x800000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
        }
        this.mWifiMulticastNesting = (1 + this.mWifiMulticastNesting);
        getUidStatsLocked(paramInt).noteWifiMulticastEnabledLocked();
    }

    public void noteWifiOffLocked()
    {
        if (this.mWifiOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFFFDFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mWifiOn = false;
            this.mWifiOnTimer.stopRunningLocked(this);
        }
        if (this.mWifiOnUid >= 0)
        {
            getUidStatsLocked(this.mWifiOnUid).noteWifiStoppedLocked();
            this.mWifiOnUid = -1;
        }
    }

    public void noteWifiOnLocked()
    {
        if (!this.mWifiOn)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x20000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mWifiOn = true;
            this.mWifiOnTimer.startRunningLocked(this);
        }
    }

    public void noteWifiRunningChangedLocked(WorkSource paramWorkSource1, WorkSource paramWorkSource2)
    {
        if (this.mGlobalWifiRunning)
        {
            int i = paramWorkSource1.size();
            for (int j = 0; j < i; j++)
                getUidStatsLocked(paramWorkSource1.get(j)).noteWifiStoppedLocked();
            int k = paramWorkSource2.size();
            for (int m = 0; m < k; m++)
                getUidStatsLocked(paramWorkSource2.get(m)).noteWifiRunningLocked();
        }
        Log.w("BatteryStatsImpl", "noteWifiRunningChangedLocked -- called while WIFI not running");
    }

    public void noteWifiRunningLocked(WorkSource paramWorkSource)
    {
        if (!this.mGlobalWifiRunning)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0x4000000 | localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mGlobalWifiRunning = true;
            this.mGlobalWifiRunningTimer.startRunningLocked(this);
            int i = paramWorkSource.size();
            for (int j = 0; j < i; j++)
                getUidStatsLocked(paramWorkSource.get(j)).noteWifiRunningLocked();
        }
        Log.w("BatteryStatsImpl", "noteWifiRunningLocked -- called while WIFI running");
    }

    public void noteWifiStoppedLocked(WorkSource paramWorkSource)
    {
        if (this.mGlobalWifiRunning)
        {
            BatteryStats.HistoryItem localHistoryItem = this.mHistoryCur;
            localHistoryItem.states = (0xFBFFFFFF & localHistoryItem.states);
            addHistoryRecordLocked(SystemClock.elapsedRealtime());
            this.mGlobalWifiRunning = false;
            this.mGlobalWifiRunningTimer.stopRunningLocked(this);
            int i = paramWorkSource.size();
            for (int j = 0; j < i; j++)
                getUidStatsLocked(paramWorkSource.get(j)).noteWifiStoppedLocked();
        }
        Log.w("BatteryStatsImpl", "noteWifiStoppedLocked -- called while WIFI not running");
    }

    public void prepareForDumpLocked()
    {
        updateKernelWakelocksLocked();
    }

    public void readFromParcel(Parcel paramParcel)
    {
        readFromParcelLocked(paramParcel);
    }

    void readFromParcelLocked(Parcel paramParcel)
    {
        if (paramParcel.readInt() != -1166707595)
            throw new ParcelFormatException("Bad magic number");
        readHistory(paramParcel, false);
        this.mStartCount = paramParcel.readInt();
        this.mBatteryUptime = paramParcel.readLong();
        this.mBatteryLastUptime = 0L;
        this.mBatteryRealtime = paramParcel.readLong();
        this.mBatteryLastRealtime = 0L;
        this.mScreenOn = false;
        this.mScreenOnTimer = new StopwatchTimer(null, -1, null, this.mUnpluggables, paramParcel);
        for (int i = 0; i < 5; i++)
            this.mScreenBrightnessTimer[i] = new StopwatchTimer(null, -100 - i, null, this.mUnpluggables, paramParcel);
        this.mInputEventCounter = new Counter(this.mUnpluggables, paramParcel);
        this.mPhoneOn = false;
        this.mPhoneOnTimer = new StopwatchTimer(null, -2, null, this.mUnpluggables, paramParcel);
        for (int j = 0; j < 5; j++)
            this.mPhoneSignalStrengthsTimer[j] = new StopwatchTimer(null, -200 - j, null, this.mUnpluggables, paramParcel);
        this.mPhoneSignalScanningTimer = new StopwatchTimer(null, -199, null, this.mUnpluggables, paramParcel);
        for (int k = 0; k < 16; k++)
            this.mPhoneDataConnectionsTimer[k] = new StopwatchTimer(null, -300 - k, null, this.mUnpluggables, paramParcel);
        this.mWifiOn = false;
        this.mWifiOnTimer = new StopwatchTimer(null, -2, null, this.mUnpluggables, paramParcel);
        this.mGlobalWifiRunning = false;
        this.mGlobalWifiRunningTimer = new StopwatchTimer(null, -2, null, this.mUnpluggables, paramParcel);
        this.mBluetoothOn = false;
        this.mBluetoothOnTimer = new StopwatchTimer(null, -2, null, this.mUnpluggables, paramParcel);
        this.mUptime = paramParcel.readLong();
        this.mUptimeStart = paramParcel.readLong();
        this.mLastUptime = 0L;
        this.mRealtime = paramParcel.readLong();
        this.mRealtimeStart = paramParcel.readLong();
        this.mLastRealtime = 0L;
        if (paramParcel.readInt() != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mOnBattery = bool;
            this.mOnBatteryInternal = false;
            this.mTrackBatteryPastUptime = paramParcel.readLong();
            this.mTrackBatteryUptimeStart = paramParcel.readLong();
            this.mTrackBatteryPastRealtime = paramParcel.readLong();
            this.mTrackBatteryRealtimeStart = paramParcel.readLong();
            this.mUnpluggedBatteryUptime = paramParcel.readLong();
            this.mUnpluggedBatteryRealtime = paramParcel.readLong();
            this.mDischargeUnplugLevel = paramParcel.readInt();
            this.mDischargeCurrentLevel = paramParcel.readInt();
            this.mLowDischargeAmountSinceCharge = paramParcel.readInt();
            this.mHighDischargeAmountSinceCharge = paramParcel.readInt();
            this.mDischargeAmountScreenOn = paramParcel.readInt();
            this.mDischargeAmountScreenOnSinceCharge = paramParcel.readInt();
            this.mDischargeAmountScreenOff = paramParcel.readInt();
            this.mDischargeAmountScreenOffSinceCharge = paramParcel.readInt();
            this.mLastWriteTime = paramParcel.readLong();
            this.mMobileDataRx[1] = paramParcel.readLong();
            this.mMobileDataRx[3] = -1L;
            this.mMobileDataTx[1] = paramParcel.readLong();
            this.mMobileDataTx[3] = -1L;
            this.mTotalDataRx[1] = paramParcel.readLong();
            this.mTotalDataRx[3] = -1L;
            this.mTotalDataTx[1] = paramParcel.readLong();
            this.mTotalDataTx[3] = -1L;
            this.mRadioDataUptime = paramParcel.readLong();
            this.mRadioDataStart = -1L;
            this.mBluetoothPingCount = paramParcel.readInt();
            this.mBluetoothPingStart = -1;
            this.mKernelWakelockStats.clear();
            int m = paramParcel.readInt();
            for (int n = 0; n < m; n++)
                if (paramParcel.readInt() != 0)
                {
                    String str = paramParcel.readString();
                    paramParcel.readInt();
                    SamplingTimer localSamplingTimer = new SamplingTimer(this.mUnpluggables, this.mOnBattery, paramParcel);
                    this.mKernelWakelockStats.put(str, localSamplingTimer);
                }
        }
        this.mPartialTimers.clear();
        this.mFullTimers.clear();
        this.mWindowTimers.clear();
        this.mWifiRunningTimers.clear();
        this.mFullWifiLockTimers.clear();
        this.mScanWifiLockTimers.clear();
        this.mWifiMulticastTimers.clear();
        sNumSpeedSteps = paramParcel.readInt();
        int i1 = paramParcel.readInt();
        this.mUidStats.clear();
        for (int i2 = 0; i2 < i1; i2++)
        {
            int i3 = paramParcel.readInt();
            Uid localUid = new Uid(i3);
            localUid.readFromParcelLocked(this.mUnpluggables, paramParcel);
            this.mUidStats.append(i3, localUid);
        }
    }

    void readHistory(Parcel paramParcel, boolean paramBoolean)
    {
        long l1 = paramParcel.readLong();
        this.mHistoryBuffer.setDataSize(0);
        this.mHistoryBuffer.setDataPosition(0);
        int i = paramParcel.readInt();
        int j = paramParcel.dataPosition();
        if (i >= 442368)
            Slog.w("BatteryStatsImpl", "File corrupt: history data buffer too large " + i);
        while (true)
        {
            if (paramBoolean)
                readOldHistory(paramParcel);
            this.mHistoryBaseTime = l1;
            if (this.mHistoryBaseTime > 0L)
            {
                long l2 = SystemClock.elapsedRealtime();
                this.mHistoryBaseTime = (60000L + (this.mHistoryBaseTime - l2));
            }
            return;
            if ((i & 0xFFFFFFFC) != i)
            {
                Slog.w("BatteryStatsImpl", "File corrupt: history data buffer not aligned " + i);
            }
            else
            {
                this.mHistoryBuffer.appendFrom(paramParcel, j, i);
                paramParcel.setDataPosition(j + i);
            }
        }
    }

    public void readLocked()
    {
        if (this.mFile == null)
            Slog.w("BatteryStats", "readLocked: no file associated with this instance");
        while (true)
        {
            return;
            this.mUidStats.clear();
            try
            {
                File localFile = this.mFile.chooseForRead();
                if (!localFile.exists())
                    continue;
                FileInputStream localFileInputStream = new FileInputStream(localFile);
                byte[] arrayOfByte = readFully(localFileInputStream);
                Parcel localParcel = Parcel.obtain();
                localParcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
                localParcel.setDataPosition(0);
                localFileInputStream.close();
                readSummaryFromParcel(localParcel);
                addHistoryBufferLocked(SystemClock.elapsedRealtime(), (byte)2);
            }
            catch (IOException localIOException)
            {
                while (true)
                    Slog.e("BatteryStats", "Error reading battery statistics", localIOException);
            }
        }
    }

    void readOldHistory(Parcel paramParcel)
    {
    }

    public void removeUidStatsLocked(int paramInt)
    {
        this.mUidStats.remove(paramInt);
    }

    public void reportExcessiveCpuLocked(int paramInt, String paramString, long paramLong1, long paramLong2)
    {
        Uid localUid = (Uid)this.mUidStats.get(paramInt);
        if (localUid != null)
            localUid.reportExcessiveCpuLocked(paramString, paramLong1, paramLong2);
    }

    public void reportExcessiveWakeLocked(int paramInt, String paramString, long paramLong1, long paramLong2)
    {
        Uid localUid = (Uid)this.mUidStats.get(paramInt);
        if (localUid != null)
            localUid.reportExcessiveWakeLocked(paramString, paramLong1, paramLong2);
    }

    public void resetAllStatsLocked()
    {
        this.mStartCount = 0;
        initTimes();
        this.mScreenOnTimer.reset(this, false);
        for (int i = 0; i < 5; i++)
            this.mScreenBrightnessTimer[i].reset(this, false);
        this.mInputEventCounter.reset(false);
        this.mPhoneOnTimer.reset(this, false);
        this.mAudioOnTimer.reset(this, false);
        this.mVideoOnTimer.reset(this, false);
        for (int j = 0; j < 5; j++)
            this.mPhoneSignalStrengthsTimer[j].reset(this, false);
        this.mPhoneSignalScanningTimer.reset(this, false);
        for (int k = 0; k < 16; k++)
            this.mPhoneDataConnectionsTimer[k].reset(this, false);
        this.mWifiOnTimer.reset(this, false);
        this.mGlobalWifiRunningTimer.reset(this, false);
        this.mBluetoothOnTimer.reset(this, false);
        for (int m = 0; m < this.mUidStats.size(); m++)
            if (((Uid)this.mUidStats.valueAt(m)).reset())
            {
                this.mUidStats.remove(this.mUidStats.keyAt(m));
                m--;
            }
        if (this.mKernelWakelockStats.size() > 0)
        {
            Iterator localIterator = this.mKernelWakelockStats.values().iterator();
            while (localIterator.hasNext())
            {
                SamplingTimer localSamplingTimer = (SamplingTimer)localIterator.next();
                this.mUnpluggables.remove(localSamplingTimer);
            }
            this.mKernelWakelockStats.clear();
        }
        initDischarge();
        clearHistoryLocked();
    }

    public void setBatteryState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        boolean bool = true;
        if (paramInt3 == 0);
        while (true)
        {
            int i;
            try
            {
                i = this.mHistoryCur.batteryStatus;
                if (!this.mHaveBatteryLevel)
                {
                    this.mHaveBatteryLevel = true;
                    if (bool != this.mOnBattery)
                        continue;
                    if (bool)
                    {
                        BatteryStats.HistoryItem localHistoryItem2 = this.mHistoryCur;
                        localHistoryItem2.states = (0xFFF7FFFF & localHistoryItem2.states);
                        continue;
                    }
                }
                else
                {
                    if (bool)
                    {
                        this.mDischargeCurrentLevel = paramInt4;
                        this.mRecordingHistory = true;
                    }
                    if (bool == this.mOnBattery)
                        continue;
                    this.mHistoryCur.batteryLevel = paramInt4;
                    this.mHistoryCur.batteryStatus = paramInt1;
                    this.mHistoryCur.batteryHealth = paramInt2;
                    this.mHistoryCur.batteryPlugType = paramInt3;
                    this.mHistoryCur.batteryTemperature = ((char)paramInt5);
                    this.mHistoryCur.batteryVoltage = ((char)paramInt6);
                    setOnBatteryLocked(bool, i, paramInt4);
                    if ((!bool) && (paramInt1 == 5))
                        this.mRecordingHistory = false;
                    return;
                }
                BatteryStats.HistoryItem localHistoryItem1 = this.mHistoryCur;
                localHistoryItem1.states = (0x80000 | localHistoryItem1.states);
            }
            finally
            {
                throw localObject;
                int j = 0;
                if (this.mHistoryCur.batteryLevel != paramInt4)
                {
                    this.mHistoryCur.batteryLevel = paramInt4;
                    j = 1;
                }
                if (this.mHistoryCur.batteryStatus != paramInt1)
                {
                    this.mHistoryCur.batteryStatus = paramInt1;
                    j = 1;
                }
                if (this.mHistoryCur.batteryHealth != paramInt2)
                {
                    this.mHistoryCur.batteryHealth = paramInt2;
                    j = 1;
                }
                if (this.mHistoryCur.batteryPlugType != paramInt3)
                {
                    this.mHistoryCur.batteryPlugType = paramInt3;
                    j = 1;
                }
                if ((paramInt5 >= '\n' + this.mHistoryCur.batteryTemperature) || (paramInt5 <= '\0.6' + this.mHistoryCur.batteryTemperature))
                {
                    this.mHistoryCur.batteryTemperature = ((char)paramInt5);
                    j = 1;
                }
                if ((paramInt6 > '\024' + this.mHistoryCur.batteryVoltage) || (paramInt6 < '\0-4' + this.mHistoryCur.batteryVoltage))
                {
                    this.mHistoryCur.batteryVoltage = ((char)paramInt6);
                    j = 1;
                }
                if (j == 0)
                    continue;
                addHistoryRecordLocked(SystemClock.elapsedRealtime());
            }
            continue;
            bool = false;
        }
    }

    public void setBtHeadset(BluetoothHeadset paramBluetoothHeadset)
    {
        if ((paramBluetoothHeadset != null) && (this.mBtHeadset == null) && (isOnBattery()) && (this.mBluetoothPingStart == -1))
            this.mBluetoothPingStart = getCurrentBluetoothPingCount();
        this.mBtHeadset = paramBluetoothHeadset;
    }

    public void setCallback(BatteryCallback paramBatteryCallback)
    {
        this.mCallback = paramBatteryCallback;
    }

    public void setNumSpeedSteps(int paramInt)
    {
        if (sNumSpeedSteps == 0)
            sNumSpeedSteps = paramInt;
    }

    void setOnBattery(boolean paramBoolean, int paramInt1, int paramInt2)
    {
        try
        {
            setOnBatteryLocked(paramBoolean, paramInt1, paramInt2);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void setOnBatteryLocked(boolean paramBoolean, int paramInt1, int paramInt2)
    {
        int i = 0;
        Message localMessage = this.mHandler.obtainMessage(2);
        int j;
        long l1;
        long l2;
        long l3;
        if (paramBoolean)
        {
            j = 1;
            localMessage.arg1 = j;
            this.mHandler.sendMessage(localMessage);
            this.mOnBatteryInternal = paramBoolean;
            this.mOnBattery = paramBoolean;
            l1 = 1000L * SystemClock.uptimeMillis();
            l2 = SystemClock.elapsedRealtime();
            l3 = l2 * 1000L;
            if (!paramBoolean)
                break label281;
            if ((paramInt1 == 5) || (paramInt2 >= 90) || ((this.mDischargeCurrentLevel < 20) && (paramInt2 >= 80)))
            {
                i = 1;
                resetAllStatsLocked();
                this.mDischargeStartLevel = paramInt2;
            }
            updateKernelWakelocksLocked();
            this.mHistoryCur.batteryLevel = ((byte)paramInt2);
            BatteryStats.HistoryItem localHistoryItem2 = this.mHistoryCur;
            localHistoryItem2.states = (0xFFF7FFFF & localHistoryItem2.states);
            addHistoryRecordLocked(l2);
            this.mTrackBatteryUptimeStart = l1;
            this.mTrackBatteryRealtimeStart = l3;
            this.mUnpluggedBatteryUptime = getBatteryUptimeLocked(l1);
            this.mUnpluggedBatteryRealtime = getBatteryRealtimeLocked(l3);
            this.mDischargeUnplugLevel = paramInt2;
            this.mDischargeCurrentLevel = paramInt2;
            if (!this.mScreenOn)
                break label268;
            this.mDischargeScreenOnUnplugLevel = paramInt2;
            this.mDischargeScreenOffUnplugLevel = 0;
            label209: this.mDischargeAmountScreenOn = 0;
            this.mDischargeAmountScreenOff = 0;
            doUnplugLocked(this.mUnpluggedBatteryUptime, this.mUnpluggedBatteryRealtime);
        }
        while (true)
        {
            if (((i != 0) || (60000L + this.mLastWriteTime < l2)) && (this.mFile != null))
                writeAsyncLocked();
            return;
            j = 0;
            break;
            label268: this.mDischargeScreenOnUnplugLevel = 0;
            this.mDischargeScreenOffUnplugLevel = paramInt2;
            break label209;
            label281: updateKernelWakelocksLocked();
            this.mHistoryCur.batteryLevel = ((byte)paramInt2);
            BatteryStats.HistoryItem localHistoryItem1 = this.mHistoryCur;
            localHistoryItem1.states = (0x80000 | localHistoryItem1.states);
            addHistoryRecordLocked(l2);
            this.mTrackBatteryPastUptime += l1 - this.mTrackBatteryUptimeStart;
            this.mTrackBatteryPastRealtime += l3 - this.mTrackBatteryRealtimeStart;
            this.mDischargeCurrentLevel = paramInt2;
            if (paramInt2 < this.mDischargeUnplugLevel)
            {
                this.mLowDischargeAmountSinceCharge += -1 + (this.mDischargeUnplugLevel - paramInt2);
                this.mHighDischargeAmountSinceCharge += this.mDischargeUnplugLevel - paramInt2;
            }
            updateDischargeScreenLevelsLocked(this.mScreenOn, this.mScreenOn);
            doPlugLocked(getBatteryUptimeLocked(l1), getBatteryRealtimeLocked(l3));
        }
    }

    public void setRadioScanningTimeout(long paramLong)
    {
        if (this.mPhoneSignalScanningTimer != null)
            this.mPhoneSignalScanningTimer.setTimeout(paramLong);
    }

    public void shutdownLocked()
    {
        writeSyncLocked();
        this.mShuttingDown = true;
    }

    public int startAddingCpuLocked()
    {
        int i = 0;
        this.mHandler.removeMessages(1);
        if (this.mScreenOn);
        label102: 
        while (true)
        {
            return i;
            int j = this.mPartialTimers.size();
            if (j == 0)
                this.mLastPartialTimers.clear();
            else
                for (int k = 0; ; k++)
                {
                    if (k >= j)
                        break label102;
                    StopwatchTimer localStopwatchTimer = (StopwatchTimer)this.mPartialTimers.get(k);
                    if (localStopwatchTimer.mInList)
                    {
                        Uid localUid = localStopwatchTimer.mUid;
                        if ((localUid != null) && (localUid.mUid != 1000))
                        {
                            i = 50;
                            break;
                        }
                    }
                }
        }
    }

    public boolean startIteratingHistoryLocked()
    {
        boolean bool = true;
        this.mHistoryBuffer.setDataPosition(0);
        this.mReadOverflow = false;
        this.mIteratingHistory = bool;
        if (this.mHistoryBuffer.dataSize() > 0);
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public boolean startIteratingOldHistoryLocked()
    {
        boolean bool = true;
        this.mHistoryBuffer.setDataPosition(0);
        this.mHistoryReadTmp.clear();
        this.mReadOverflow = false;
        this.mIteratingHistory = bool;
        BatteryStats.HistoryItem localHistoryItem = this.mHistory;
        this.mHistoryIterator = localHistoryItem;
        if (localHistoryItem != null);
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    void stopAllSignalStrengthTimersLocked(int paramInt)
    {
        int i = 0;
        if (i < 5)
        {
            if (i == paramInt);
            while (true)
            {
                i++;
                break;
                while (this.mPhoneSignalStrengthsTimer[i].isRunningLocked())
                    this.mPhoneSignalStrengthsTimer[i].stopRunningLocked(this);
            }
        }
    }

    void updateDischargeScreenLevelsLocked(boolean paramBoolean1, boolean paramBoolean2)
    {
        if (paramBoolean1)
        {
            int j = this.mDischargeScreenOnUnplugLevel - this.mDischargeCurrentLevel;
            if (j > 0)
            {
                this.mDischargeAmountScreenOn = (j + this.mDischargeAmountScreenOn);
                this.mDischargeAmountScreenOnSinceCharge = (j + this.mDischargeAmountScreenOnSinceCharge);
            }
            if (!paramBoolean2)
                break label97;
            this.mDischargeScreenOnUnplugLevel = this.mDischargeCurrentLevel;
        }
        for (this.mDischargeScreenOffUnplugLevel = 0; ; this.mDischargeScreenOffUnplugLevel = this.mDischargeCurrentLevel)
        {
            return;
            int i = this.mDischargeScreenOffUnplugLevel - this.mDischargeCurrentLevel;
            if (i <= 0)
                break;
            this.mDischargeAmountScreenOff = (i + this.mDischargeAmountScreenOff);
            this.mDischargeAmountScreenOffSinceCharge = (i + this.mDischargeAmountScreenOffSinceCharge);
            break;
            label97: this.mDischargeScreenOnUnplugLevel = 0;
        }
    }

    public void updateKernelWakelocksLocked()
    {
        Map localMap = readKernelWakelockStats();
        if (localMap == null)
            Slog.w("BatteryStatsImpl", "Couldn't get kernel wake lock stats");
        while (true)
        {
            return;
            Iterator localIterator1 = localMap.entrySet().iterator();
            while (localIterator1.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator1.next();
                String str = (String)localEntry.getKey();
                KernelWakelockStats localKernelWakelockStats = (KernelWakelockStats)localEntry.getValue();
                SamplingTimer localSamplingTimer2 = (SamplingTimer)this.mKernelWakelockStats.get(str);
                if (localSamplingTimer2 == null)
                {
                    localSamplingTimer2 = new SamplingTimer(this.mUnpluggables, this.mOnBatteryInternal, true);
                    this.mKernelWakelockStats.put(str, localSamplingTimer2);
                }
                localSamplingTimer2.updateCurrentReportedCount(localKernelWakelockStats.mCount);
                localSamplingTimer2.updateCurrentReportedTotalTime(localKernelWakelockStats.mTotalTime);
                localSamplingTimer2.setUpdateVersion(sKernelWakelockUpdateVersion);
            }
            if (localMap.size() != this.mKernelWakelockStats.size())
            {
                Iterator localIterator2 = this.mKernelWakelockStats.entrySet().iterator();
                while (localIterator2.hasNext())
                {
                    SamplingTimer localSamplingTimer1 = (SamplingTimer)((Map.Entry)localIterator2.next()).getValue();
                    if (localSamplingTimer1.getUpdateVersion() != sKernelWakelockUpdateVersion)
                        localSamplingTimer1.setStale();
                }
            }
        }
    }

    public void writeAsyncLocked()
    {
        writeLocked(false);
    }

    void writeHistory(Parcel paramParcel, boolean paramBoolean)
    {
        paramParcel.writeLong(this.mHistoryBaseTime + this.mLastHistoryTime);
        paramParcel.writeInt(this.mHistoryBuffer.dataSize());
        paramParcel.appendFrom(this.mHistoryBuffer, 0, this.mHistoryBuffer.dataSize());
        if (paramBoolean)
            writeOldHistory(paramParcel);
    }

    void writeLocked(boolean paramBoolean)
    {
        if (this.mFile == null)
            Slog.w("BatteryStats", "writeLocked: no file associated with this instance");
        while (true)
        {
            return;
            if (!this.mShuttingDown)
            {
                Parcel localParcel = Parcel.obtain();
                writeSummaryToParcel(localParcel);
                this.mLastWriteTime = SystemClock.elapsedRealtime();
                if (this.mPendingWrite != null)
                    this.mPendingWrite.recycle();
                this.mPendingWrite = localParcel;
                if (paramBoolean)
                    commitPendingDataToDisk();
                else
                    new Thread("BatteryStats-Write")
                    {
                        public void run()
                        {
                            Process.setThreadPriority(10);
                            BatteryStatsImpl.this.commitPendingDataToDisk();
                        }
                    }
                    .start();
            }
        }
    }

    void writeOldHistory(Parcel paramParcel)
    {
    }

    public void writeSummaryToParcel(Parcel paramParcel)
    {
        updateKernelWakelocksLocked();
        long l1 = 1000L * SystemClock.uptimeMillis();
        long l2 = 1000L * SystemClock.elapsedRealtime();
        long l3 = getBatteryUptimeLocked(l1);
        long l4 = getBatteryRealtimeLocked(l2);
        paramParcel.writeInt(61);
        writeHistory(paramParcel, true);
        paramParcel.writeInt(this.mStartCount);
        paramParcel.writeLong(computeBatteryUptime(l1, 0));
        paramParcel.writeLong(computeBatteryRealtime(l2, 0));
        paramParcel.writeLong(computeUptime(l1, 0));
        paramParcel.writeLong(computeRealtime(l2, 0));
        paramParcel.writeInt(this.mDischargeUnplugLevel);
        paramParcel.writeInt(this.mDischargeCurrentLevel);
        paramParcel.writeInt(getLowDischargeAmountSinceCharge());
        paramParcel.writeInt(getHighDischargeAmountSinceCharge());
        paramParcel.writeInt(getDischargeAmountScreenOnSinceCharge());
        paramParcel.writeInt(getDischargeAmountScreenOffSinceCharge());
        this.mScreenOnTimer.writeSummaryFromParcelLocked(paramParcel, l4);
        for (int i = 0; i < 5; i++)
            this.mScreenBrightnessTimer[i].writeSummaryFromParcelLocked(paramParcel, l4);
        this.mInputEventCounter.writeSummaryFromParcelLocked(paramParcel);
        this.mPhoneOnTimer.writeSummaryFromParcelLocked(paramParcel, l4);
        for (int j = 0; j < 5; j++)
            this.mPhoneSignalStrengthsTimer[j].writeSummaryFromParcelLocked(paramParcel, l4);
        this.mPhoneSignalScanningTimer.writeSummaryFromParcelLocked(paramParcel, l4);
        for (int k = 0; k < 16; k++)
            this.mPhoneDataConnectionsTimer[k].writeSummaryFromParcelLocked(paramParcel, l4);
        this.mWifiOnTimer.writeSummaryFromParcelLocked(paramParcel, l4);
        this.mGlobalWifiRunningTimer.writeSummaryFromParcelLocked(paramParcel, l4);
        this.mBluetoothOnTimer.writeSummaryFromParcelLocked(paramParcel, l4);
        paramParcel.writeInt(this.mKernelWakelockStats.size());
        Iterator localIterator1 = this.mKernelWakelockStats.entrySet().iterator();
        while (localIterator1.hasNext())
        {
            Map.Entry localEntry6 = (Map.Entry)localIterator1.next();
            if ((Timer)localEntry6.getValue() != null)
            {
                paramParcel.writeInt(1);
                paramParcel.writeString((String)localEntry6.getKey());
                ((SamplingTimer)localEntry6.getValue()).writeSummaryFromParcelLocked(paramParcel, l4);
            }
            else
            {
                paramParcel.writeInt(0);
            }
        }
        paramParcel.writeInt(sNumSpeedSteps);
        int m = this.mUidStats.size();
        paramParcel.writeInt(m);
        for (int n = 0; n < m; n++)
        {
            paramParcel.writeInt(this.mUidStats.keyAt(n));
            Uid localUid = (Uid)this.mUidStats.valueAt(n);
            label512: label536: label560: label584: label608: label621: Iterator localIterator6;
            if (localUid.mWifiRunningTimer != null)
            {
                paramParcel.writeInt(1);
                localUid.mWifiRunningTimer.writeSummaryFromParcelLocked(paramParcel, l4);
                if (localUid.mFullWifiLockTimer == null)
                    break label788;
                paramParcel.writeInt(1);
                localUid.mFullWifiLockTimer.writeSummaryFromParcelLocked(paramParcel, l4);
                if (localUid.mScanWifiLockTimer == null)
                    break label796;
                paramParcel.writeInt(1);
                localUid.mScanWifiLockTimer.writeSummaryFromParcelLocked(paramParcel, l4);
                if (localUid.mWifiMulticastTimer == null)
                    break label804;
                paramParcel.writeInt(1);
                localUid.mWifiMulticastTimer.writeSummaryFromParcelLocked(paramParcel, l4);
                if (localUid.mAudioTurnedOnTimer == null)
                    break label812;
                paramParcel.writeInt(1);
                localUid.mAudioTurnedOnTimer.writeSummaryFromParcelLocked(paramParcel, l4);
                if (localUid.mVideoTurnedOnTimer == null)
                    break label820;
                paramParcel.writeInt(1);
                localUid.mVideoTurnedOnTimer.writeSummaryFromParcelLocked(paramParcel, l4);
                if (localUid.mUserActivityCounters != null)
                    break label828;
                paramParcel.writeInt(0);
                int i2 = localUid.mWakelockStats.size();
                paramParcel.writeInt(i2);
                if (i2 <= 0)
                    break label885;
                localIterator6 = localUid.mWakelockStats.entrySet().iterator();
            }
            while (true)
            {
                label657: if (!localIterator6.hasNext())
                    break label885;
                Map.Entry localEntry5 = (Map.Entry)localIterator6.next();
                paramParcel.writeString((String)localEntry5.getKey());
                BatteryStatsImpl.Uid.Wakelock localWakelock = (BatteryStatsImpl.Uid.Wakelock)localEntry5.getValue();
                if (localWakelock.mTimerFull != null)
                {
                    paramParcel.writeInt(1);
                    localWakelock.mTimerFull.writeSummaryFromParcelLocked(paramParcel, l4);
                    label729: if (localWakelock.mTimerPartial == null)
                        break label869;
                    paramParcel.writeInt(1);
                    localWakelock.mTimerPartial.writeSummaryFromParcelLocked(paramParcel, l4);
                }
                while (true)
                {
                    if (localWakelock.mTimerWindow == null)
                        break label877;
                    paramParcel.writeInt(1);
                    localWakelock.mTimerWindow.writeSummaryFromParcelLocked(paramParcel, l4);
                    break label657;
                    paramParcel.writeInt(0);
                    break;
                    label788: paramParcel.writeInt(0);
                    break label512;
                    label796: paramParcel.writeInt(0);
                    break label536;
                    label804: paramParcel.writeInt(0);
                    break label560;
                    label812: paramParcel.writeInt(0);
                    break label584;
                    label820: paramParcel.writeInt(0);
                    break label608;
                    label828: paramParcel.writeInt(1);
                    for (int i1 = 0; i1 < 7; i1++)
                        localUid.mUserActivityCounters[i1].writeSummaryFromParcelLocked(paramParcel);
                    break label621;
                    paramParcel.writeInt(0);
                    break label729;
                    label869: paramParcel.writeInt(0);
                }
                label877: paramParcel.writeInt(0);
            }
            label885: int i3 = localUid.mSensorStats.size();
            paramParcel.writeInt(i3);
            if (i3 > 0)
            {
                Iterator localIterator5 = localUid.mSensorStats.entrySet().iterator();
                while (localIterator5.hasNext())
                {
                    Map.Entry localEntry4 = (Map.Entry)localIterator5.next();
                    paramParcel.writeInt(((Integer)localEntry4.getKey()).intValue());
                    BatteryStatsImpl.Uid.Sensor localSensor = (BatteryStatsImpl.Uid.Sensor)localEntry4.getValue();
                    if (localSensor.mTimer != null)
                    {
                        paramParcel.writeInt(1);
                        localSensor.mTimer.writeSummaryFromParcelLocked(paramParcel, l4);
                    }
                    else
                    {
                        paramParcel.writeInt(0);
                    }
                }
            }
            int i4 = localUid.mProcessStats.size();
            paramParcel.writeInt(i4);
            if (i4 > 0)
            {
                Iterator localIterator4 = localUid.mProcessStats.entrySet().iterator();
                while (localIterator4.hasNext())
                {
                    Map.Entry localEntry3 = (Map.Entry)localIterator4.next();
                    paramParcel.writeString((String)localEntry3.getKey());
                    BatteryStatsImpl.Uid.Proc localProc = (BatteryStatsImpl.Uid.Proc)localEntry3.getValue();
                    paramParcel.writeLong(localProc.mUserTime);
                    paramParcel.writeLong(localProc.mSystemTime);
                    paramParcel.writeInt(localProc.mStarts);
                    int i7 = localProc.mSpeedBins.length;
                    paramParcel.writeInt(i7);
                    int i8 = 0;
                    if (i8 < i7)
                    {
                        if (localProc.mSpeedBins[i8] != null)
                        {
                            paramParcel.writeInt(1);
                            localProc.mSpeedBins[i8].writeSummaryFromParcelLocked(paramParcel);
                        }
                        while (true)
                        {
                            i8++;
                            break;
                            paramParcel.writeInt(0);
                        }
                    }
                    localProc.writeExcessivePowerToParcelLocked(paramParcel);
                }
            }
            int i5 = localUid.mPackageStats.size();
            paramParcel.writeInt(i5);
            if (i5 > 0)
            {
                Iterator localIterator2 = localUid.mPackageStats.entrySet().iterator();
                while (localIterator2.hasNext())
                {
                    Map.Entry localEntry1 = (Map.Entry)localIterator2.next();
                    paramParcel.writeString((String)localEntry1.getKey());
                    BatteryStatsImpl.Uid.Pkg localPkg = (BatteryStatsImpl.Uid.Pkg)localEntry1.getValue();
                    paramParcel.writeInt(localPkg.mWakeups);
                    int i6 = localPkg.mServiceStats.size();
                    paramParcel.writeInt(i6);
                    if (i6 > 0)
                    {
                        Iterator localIterator3 = localPkg.mServiceStats.entrySet().iterator();
                        while (localIterator3.hasNext())
                        {
                            Map.Entry localEntry2 = (Map.Entry)localIterator3.next();
                            paramParcel.writeString((String)localEntry2.getKey());
                            BatteryStatsImpl.Uid.Pkg.Serv localServ = (BatteryStatsImpl.Uid.Pkg.Serv)localEntry2.getValue();
                            paramParcel.writeLong(localServ.getStartTimeToNowLocked(l3));
                            paramParcel.writeInt(localServ.mStarts);
                            paramParcel.writeInt(localServ.mLaunches);
                        }
                    }
                }
            }
            paramParcel.writeLong(localUid.getTcpBytesReceived(0));
            paramParcel.writeLong(localUid.getTcpBytesSent(0));
        }
    }

    public void writeSyncLocked()
    {
        writeLocked(true);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        writeToParcelLocked(paramParcel, true, paramInt);
    }

    void writeToParcelLocked(Parcel paramParcel, boolean paramBoolean, int paramInt)
    {
        updateKernelWakelocksLocked();
        long l1 = 1000L * SystemClock.uptimeMillis();
        long l2 = 1000L * SystemClock.elapsedRealtime();
        long l3 = getBatteryUptimeLocked(l1);
        long l4 = getBatteryRealtimeLocked(l2);
        paramParcel.writeInt(-1166707595);
        writeHistory(paramParcel, false);
        paramParcel.writeInt(this.mStartCount);
        paramParcel.writeLong(this.mBatteryUptime);
        paramParcel.writeLong(this.mBatteryRealtime);
        this.mScreenOnTimer.writeToParcel(paramParcel, l4);
        for (int i = 0; i < 5; i++)
            this.mScreenBrightnessTimer[i].writeToParcel(paramParcel, l4);
        this.mInputEventCounter.writeToParcel(paramParcel);
        this.mPhoneOnTimer.writeToParcel(paramParcel, l4);
        for (int j = 0; j < 5; j++)
            this.mPhoneSignalStrengthsTimer[j].writeToParcel(paramParcel, l4);
        this.mPhoneSignalScanningTimer.writeToParcel(paramParcel, l4);
        for (int k = 0; k < 16; k++)
            this.mPhoneDataConnectionsTimer[k].writeToParcel(paramParcel, l4);
        this.mWifiOnTimer.writeToParcel(paramParcel, l4);
        this.mGlobalWifiRunningTimer.writeToParcel(paramParcel, l4);
        this.mBluetoothOnTimer.writeToParcel(paramParcel, l4);
        paramParcel.writeLong(this.mUptime);
        paramParcel.writeLong(this.mUptimeStart);
        paramParcel.writeLong(this.mRealtime);
        paramParcel.writeLong(this.mRealtimeStart);
        int m;
        Iterator localIterator;
        if (this.mOnBattery)
        {
            m = 1;
            paramParcel.writeInt(m);
            paramParcel.writeLong(l3);
            paramParcel.writeLong(this.mTrackBatteryUptimeStart);
            paramParcel.writeLong(l4);
            paramParcel.writeLong(this.mTrackBatteryRealtimeStart);
            paramParcel.writeLong(this.mUnpluggedBatteryUptime);
            paramParcel.writeLong(this.mUnpluggedBatteryRealtime);
            paramParcel.writeInt(this.mDischargeUnplugLevel);
            paramParcel.writeInt(this.mDischargeCurrentLevel);
            paramParcel.writeInt(this.mLowDischargeAmountSinceCharge);
            paramParcel.writeInt(this.mHighDischargeAmountSinceCharge);
            paramParcel.writeInt(this.mDischargeAmountScreenOn);
            paramParcel.writeInt(this.mDischargeAmountScreenOnSinceCharge);
            paramParcel.writeInt(this.mDischargeAmountScreenOff);
            paramParcel.writeInt(this.mDischargeAmountScreenOffSinceCharge);
            paramParcel.writeLong(this.mLastWriteTime);
            paramParcel.writeLong(getMobileTcpBytesReceived(3));
            paramParcel.writeLong(getMobileTcpBytesSent(3));
            paramParcel.writeLong(getTotalTcpBytesReceived(3));
            paramParcel.writeLong(getTotalTcpBytesSent(3));
            paramParcel.writeLong(getRadioDataUptime());
            paramParcel.writeInt(getBluetoothPingCount());
            if (paramBoolean)
            {
                paramParcel.writeInt(this.mKernelWakelockStats.size());
                localIterator = this.mKernelWakelockStats.entrySet().iterator();
            }
        }
        else
        {
            while (true)
            {
                if (!localIterator.hasNext())
                    break label560;
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                SamplingTimer localSamplingTimer = (SamplingTimer)localEntry.getValue();
                if (localSamplingTimer != null)
                {
                    paramParcel.writeInt(1);
                    paramParcel.writeString((String)localEntry.getKey());
                    Timer.writeTimerToParcel(paramParcel, localSamplingTimer, l4);
                    continue;
                    m = 0;
                    break;
                }
                paramParcel.writeInt(0);
            }
        }
        paramParcel.writeInt(0);
        label560: paramParcel.writeInt(sNumSpeedSteps);
        if (paramBoolean)
        {
            int n = this.mUidStats.size();
            paramParcel.writeInt(n);
            for (int i1 = 0; i1 < n; i1++)
            {
                paramParcel.writeInt(this.mUidStats.keyAt(i1));
                ((Uid)this.mUidStats.valueAt(i1)).writeToParcelLocked(paramParcel, l4);
            }
        }
        paramParcel.writeInt(0);
    }

    public void writeToParcelWithoutUids(Parcel paramParcel, int paramInt)
    {
        writeToParcelLocked(paramParcel, false, paramInt);
    }

    public final class Uid extends BatteryStats.Uid
    {
        boolean mAudioTurnedOn;
        BatteryStatsImpl.StopwatchTimer mAudioTurnedOnTimer;
        long mCurrentTcpBytesReceived;
        long mCurrentTcpBytesSent;
        boolean mFullWifiLockOut;
        BatteryStatsImpl.StopwatchTimer mFullWifiLockTimer;
        long mLoadedTcpBytesReceived;
        long mLoadedTcpBytesSent;
        final HashMap<String, Pkg> mPackageStats = new HashMap();
        final SparseArray<BatteryStats.Uid.Pid> mPids = new SparseArray();
        final HashMap<String, Proc> mProcessStats = new HashMap();
        boolean mScanWifiLockOut;
        BatteryStatsImpl.StopwatchTimer mScanWifiLockTimer;
        final HashMap<Integer, Sensor> mSensorStats = new HashMap();
        long mStartedTcpBytesReceived = -1L;
        long mStartedTcpBytesSent = -1L;
        long mTcpBytesReceivedAtLastUnplug;
        long mTcpBytesSentAtLastUnplug;
        final int mUid;
        BatteryStatsImpl.Counter[] mUserActivityCounters;
        boolean mVideoTurnedOn;
        BatteryStatsImpl.StopwatchTimer mVideoTurnedOnTimer;
        final HashMap<String, Wakelock> mWakelockStats = new HashMap();
        boolean mWifiMulticastEnabled;
        BatteryStatsImpl.StopwatchTimer mWifiMulticastTimer;
        boolean mWifiRunning;
        BatteryStatsImpl.StopwatchTimer mWifiRunningTimer;

        public Uid(int arg2)
        {
            int i;
            this.mUid = i;
            this.mWifiRunningTimer = new BatteryStatsImpl.StopwatchTimer(this, 4, BatteryStatsImpl.this.mWifiRunningTimers, BatteryStatsImpl.this.mUnpluggables);
            this.mFullWifiLockTimer = new BatteryStatsImpl.StopwatchTimer(this, 5, BatteryStatsImpl.this.mFullWifiLockTimers, BatteryStatsImpl.this.mUnpluggables);
            this.mScanWifiLockTimer = new BatteryStatsImpl.StopwatchTimer(this, 6, BatteryStatsImpl.this.mScanWifiLockTimers, BatteryStatsImpl.this.mUnpluggables);
            this.mWifiMulticastTimer = new BatteryStatsImpl.StopwatchTimer(this, 7, BatteryStatsImpl.this.mWifiMulticastTimers, BatteryStatsImpl.this.mUnpluggables);
            this.mAudioTurnedOnTimer = new BatteryStatsImpl.StopwatchTimer(this, 7, null, BatteryStatsImpl.this.mUnpluggables);
            this.mVideoTurnedOnTimer = new BatteryStatsImpl.StopwatchTimer(this, 8, null, BatteryStatsImpl.this.mUnpluggables);
        }

        public long computeCurrentTcpBytesReceived()
        {
            long l1 = 0L;
            long l2 = BatteryStatsImpl.this.getNetworkStatsDetailGroupedByUid().getTotal(null, this.mUid).rxBytes;
            long l3 = this.mCurrentTcpBytesReceived;
            if (this.mStartedTcpBytesReceived >= l1)
                l1 = l2 - this.mStartedTcpBytesReceived;
            return l1 + l3;
        }

        public long computeCurrentTcpBytesSent()
        {
            long l1 = 0L;
            long l2 = BatteryStatsImpl.this.getNetworkStatsDetailGroupedByUid().getTotal(null, this.mUid).txBytes;
            long l3 = this.mCurrentTcpBytesSent;
            if (this.mStartedTcpBytesSent >= l1)
                l1 = l2 - this.mStartedTcpBytesSent;
            return l1 + l3;
        }

        public long getAudioTurnedOnTime(long paramLong, int paramInt)
        {
            if (this.mAudioTurnedOnTimer == null);
            for (long l = 0L; ; l = this.mAudioTurnedOnTimer.getTotalTimeLocked(paramLong, paramInt))
                return l;
        }

        public BatteryStatsImpl getBatteryStats()
        {
            return BatteryStatsImpl.this;
        }

        public long getFullWifiLockTime(long paramLong, int paramInt)
        {
            if (this.mFullWifiLockTimer == null);
            for (long l = 0L; ; l = this.mFullWifiLockTimer.getTotalTimeLocked(paramLong, paramInt))
                return l;
        }

        public Map<String, ? extends BatteryStats.Uid.Pkg> getPackageStats()
        {
            return this.mPackageStats;
        }

        public Pkg getPackageStatsLocked(String paramString)
        {
            Pkg localPkg = (Pkg)this.mPackageStats.get(paramString);
            if (localPkg == null)
            {
                localPkg = new Pkg();
                this.mPackageStats.put(paramString, localPkg);
            }
            return localPkg;
        }

        public SparseArray<? extends BatteryStats.Uid.Pid> getPidStats()
        {
            return this.mPids;
        }

        public BatteryStats.Uid.Pid getPidStatsLocked(int paramInt)
        {
            BatteryStats.Uid.Pid localPid = (BatteryStats.Uid.Pid)this.mPids.get(paramInt);
            if (localPid == null)
            {
                localPid = new BatteryStats.Uid.Pid(this);
                this.mPids.put(paramInt, localPid);
            }
            return localPid;
        }

        public Map<String, ? extends BatteryStats.Uid.Proc> getProcessStats()
        {
            return this.mProcessStats;
        }

        public Proc getProcessStatsLocked(String paramString)
        {
            Proc localProc = (Proc)this.mProcessStats.get(paramString);
            if (localProc == null)
            {
                localProc = new Proc();
                this.mProcessStats.put(paramString, localProc);
            }
            return localProc;
        }

        public long getScanWifiLockTime(long paramLong, int paramInt)
        {
            if (this.mScanWifiLockTimer == null);
            for (long l = 0L; ; l = this.mScanWifiLockTimer.getTotalTimeLocked(paramLong, paramInt))
                return l;
        }

        public Map<Integer, ? extends BatteryStats.Uid.Sensor> getSensorStats()
        {
            return this.mSensorStats;
        }

        public BatteryStatsImpl.StopwatchTimer getSensorTimerLocked(int paramInt, boolean paramBoolean)
        {
            Sensor localSensor = (Sensor)this.mSensorStats.get(Integer.valueOf(paramInt));
            BatteryStatsImpl.StopwatchTimer localStopwatchTimer;
            if (localSensor == null)
                if (!paramBoolean)
                    localStopwatchTimer = null;
            while (true)
            {
                return localStopwatchTimer;
                localSensor = new Sensor(paramInt);
                this.mSensorStats.put(Integer.valueOf(paramInt), localSensor);
                localStopwatchTimer = localSensor.mTimer;
                if (localStopwatchTimer == null)
                {
                    ArrayList localArrayList = (ArrayList)BatteryStatsImpl.this.mSensorTimers.get(paramInt);
                    if (localArrayList == null)
                    {
                        localArrayList = new ArrayList();
                        BatteryStatsImpl.this.mSensorTimers.put(paramInt, localArrayList);
                    }
                    localStopwatchTimer = new BatteryStatsImpl.StopwatchTimer(this, 3, localArrayList, BatteryStatsImpl.this.mUnpluggables);
                    localSensor.mTimer = localStopwatchTimer;
                }
            }
        }

        public BatteryStatsImpl.Uid.Pkg.Serv getServiceStatsLocked(String paramString1, String paramString2)
        {
            Pkg localPkg = getPackageStatsLocked(paramString1);
            BatteryStatsImpl.Uid.Pkg.Serv localServ = (BatteryStatsImpl.Uid.Pkg.Serv)localPkg.mServiceStats.get(paramString2);
            if (localServ == null)
            {
                localServ = localPkg.newServiceStatsLocked();
                localPkg.mServiceStats.put(paramString2, localServ);
            }
            return localServ;
        }

        public long getTcpBytesReceived(int paramInt)
        {
            long l;
            if (paramInt == 1)
                l = this.mLoadedTcpBytesReceived;
            while (true)
            {
                return l;
                l = computeCurrentTcpBytesReceived();
                if (paramInt == 3)
                    l -= this.mTcpBytesReceivedAtLastUnplug;
                else if (paramInt == 0)
                    l += this.mLoadedTcpBytesReceived;
            }
        }

        public long getTcpBytesSent(int paramInt)
        {
            long l;
            if (paramInt == 1)
                l = this.mLoadedTcpBytesSent;
            while (true)
            {
                return l;
                l = computeCurrentTcpBytesSent();
                if (paramInt == 3)
                    l -= this.mTcpBytesSentAtLastUnplug;
                else if (paramInt == 0)
                    l += this.mLoadedTcpBytesSent;
            }
        }

        public int getUid()
        {
            return this.mUid;
        }

        public int getUserActivityCount(int paramInt1, int paramInt2)
        {
            if (this.mUserActivityCounters == null);
            for (int i = 0; ; i = this.mUserActivityCounters[paramInt1].getCountLocked(paramInt2))
                return i;
        }

        public long getVideoTurnedOnTime(long paramLong, int paramInt)
        {
            if (this.mVideoTurnedOnTimer == null);
            for (long l = 0L; ; l = this.mVideoTurnedOnTimer.getTotalTimeLocked(paramLong, paramInt))
                return l;
        }

        public BatteryStatsImpl.StopwatchTimer getWakeTimerLocked(String paramString, int paramInt)
        {
            Wakelock localWakelock = (Wakelock)this.mWakelockStats.get(paramString);
            if (localWakelock == null)
            {
                int i = this.mWakelockStats.size();
                if ((i > 30) && ((this.mUid != 1000) || (i > 50)))
                {
                    paramString = "*overflow*";
                    localWakelock = (Wakelock)this.mWakelockStats.get(paramString);
                }
                if (localWakelock == null)
                {
                    localWakelock = new Wakelock();
                    this.mWakelockStats.put(paramString, localWakelock);
                }
            }
            Object localObject;
            switch (paramInt)
            {
            default:
                throw new IllegalArgumentException("type=" + paramInt);
            case 0:
                BatteryStatsImpl.StopwatchTimer localStopwatchTimer3 = localWakelock.mTimerPartial;
                if (localStopwatchTimer3 == null)
                {
                    localStopwatchTimer3 = new BatteryStatsImpl.StopwatchTimer(this, 0, BatteryStatsImpl.this.mPartialTimers, BatteryStatsImpl.this.mUnpluggables);
                    localWakelock.mTimerPartial = localStopwatchTimer3;
                }
                localObject = localStopwatchTimer3;
            case 1:
            case 2:
            }
            while (true)
            {
                return localObject;
                BatteryStatsImpl.StopwatchTimer localStopwatchTimer2 = localWakelock.mTimerFull;
                if (localStopwatchTimer2 == null)
                {
                    localStopwatchTimer2 = new BatteryStatsImpl.StopwatchTimer(this, 1, BatteryStatsImpl.this.mFullTimers, BatteryStatsImpl.this.mUnpluggables);
                    localWakelock.mTimerFull = localStopwatchTimer2;
                }
                localObject = localStopwatchTimer2;
                continue;
                BatteryStatsImpl.StopwatchTimer localStopwatchTimer1 = localWakelock.mTimerWindow;
                if (localStopwatchTimer1 == null)
                {
                    localStopwatchTimer1 = new BatteryStatsImpl.StopwatchTimer(this, 2, BatteryStatsImpl.this.mWindowTimers, BatteryStatsImpl.this.mUnpluggables);
                    localWakelock.mTimerWindow = localStopwatchTimer1;
                }
                localObject = localStopwatchTimer1;
            }
        }

        public Map<String, ? extends BatteryStats.Uid.Wakelock> getWakelockStats()
        {
            return this.mWakelockStats;
        }

        public long getWifiMulticastTime(long paramLong, int paramInt)
        {
            if (this.mWifiMulticastTimer == null);
            for (long l = 0L; ; l = this.mWifiMulticastTimer.getTotalTimeLocked(paramLong, paramInt))
                return l;
        }

        public long getWifiRunningTime(long paramLong, int paramInt)
        {
            if (this.mWifiRunningTimer == null);
            for (long l = 0L; ; l = this.mWifiRunningTimer.getTotalTimeLocked(paramLong, paramInt))
                return l;
        }

        public boolean hasUserActivity()
        {
            if (this.mUserActivityCounters != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        void initUserActivityLocked()
        {
            this.mUserActivityCounters = new BatteryStatsImpl.Counter[7];
            for (int i = 0; i < 7; i++)
                this.mUserActivityCounters[i] = new BatteryStatsImpl.Counter(BatteryStatsImpl.this.mUnpluggables);
        }

        public void noteAudioTurnedOffLocked()
        {
            if (this.mAudioTurnedOn)
            {
                this.mAudioTurnedOn = false;
                this.mAudioTurnedOnTimer.stopRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteAudioTurnedOnLocked()
        {
            if (!this.mAudioTurnedOn)
            {
                this.mAudioTurnedOn = true;
                if (this.mAudioTurnedOnTimer == null)
                    this.mAudioTurnedOnTimer = new BatteryStatsImpl.StopwatchTimer(this, 7, null, BatteryStatsImpl.this.mUnpluggables);
                this.mAudioTurnedOnTimer.startRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteFullWifiLockAcquiredLocked()
        {
            if (!this.mFullWifiLockOut)
            {
                this.mFullWifiLockOut = true;
                if (this.mFullWifiLockTimer == null)
                    this.mFullWifiLockTimer = new BatteryStatsImpl.StopwatchTimer(this, 5, BatteryStatsImpl.this.mFullWifiLockTimers, BatteryStatsImpl.this.mUnpluggables);
                this.mFullWifiLockTimer.startRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteFullWifiLockReleasedLocked()
        {
            if (this.mFullWifiLockOut)
            {
                this.mFullWifiLockOut = false;
                this.mFullWifiLockTimer.stopRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteScanWifiLockAcquiredLocked()
        {
            if (!this.mScanWifiLockOut)
            {
                this.mScanWifiLockOut = true;
                if (this.mScanWifiLockTimer == null)
                    this.mScanWifiLockTimer = new BatteryStatsImpl.StopwatchTimer(this, 6, BatteryStatsImpl.this.mScanWifiLockTimers, BatteryStatsImpl.this.mUnpluggables);
                this.mScanWifiLockTimer.startRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteScanWifiLockReleasedLocked()
        {
            if (this.mScanWifiLockOut)
            {
                this.mScanWifiLockOut = false;
                this.mScanWifiLockTimer.stopRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteStartGps()
        {
            BatteryStatsImpl.StopwatchTimer localStopwatchTimer = getSensorTimerLocked(-10000, true);
            if (localStopwatchTimer != null)
                localStopwatchTimer.startRunningLocked(BatteryStatsImpl.this);
        }

        public void noteStartSensor(int paramInt)
        {
            BatteryStatsImpl.StopwatchTimer localStopwatchTimer = getSensorTimerLocked(paramInt, true);
            if (localStopwatchTimer != null)
                localStopwatchTimer.startRunningLocked(BatteryStatsImpl.this);
        }

        public void noteStartWakeLocked(int paramInt1, String paramString, int paramInt2)
        {
            BatteryStatsImpl.StopwatchTimer localStopwatchTimer = getWakeTimerLocked(paramString, paramInt2);
            if (localStopwatchTimer != null)
                localStopwatchTimer.startRunningLocked(BatteryStatsImpl.this);
            if ((paramInt1 >= 0) && (paramInt2 == 0))
            {
                BatteryStats.Uid.Pid localPid = getPidStatsLocked(paramInt1);
                if (localPid.mWakeStart == 0L)
                    localPid.mWakeStart = SystemClock.elapsedRealtime();
            }
        }

        public void noteStopGps()
        {
            BatteryStatsImpl.StopwatchTimer localStopwatchTimer = getSensorTimerLocked(-10000, false);
            if (localStopwatchTimer != null)
                localStopwatchTimer.stopRunningLocked(BatteryStatsImpl.this);
        }

        public void noteStopSensor(int paramInt)
        {
            BatteryStatsImpl.StopwatchTimer localStopwatchTimer = getSensorTimerLocked(paramInt, false);
            if (localStopwatchTimer != null)
                localStopwatchTimer.stopRunningLocked(BatteryStatsImpl.this);
        }

        public void noteStopWakeLocked(int paramInt1, String paramString, int paramInt2)
        {
            BatteryStatsImpl.StopwatchTimer localStopwatchTimer = getWakeTimerLocked(paramString, paramInt2);
            if (localStopwatchTimer != null)
                localStopwatchTimer.stopRunningLocked(BatteryStatsImpl.this);
            if ((paramInt1 >= 0) && (paramInt2 == 0))
            {
                BatteryStats.Uid.Pid localPid = (BatteryStats.Uid.Pid)this.mPids.get(paramInt1);
                if ((localPid != null) && (localPid.mWakeStart != 0L))
                {
                    localPid.mWakeSum += SystemClock.elapsedRealtime() - localPid.mWakeStart;
                    localPid.mWakeStart = 0L;
                }
            }
        }

        public void noteUserActivityLocked(int paramInt)
        {
            if (this.mUserActivityCounters == null)
                initUserActivityLocked();
            if (paramInt < 0)
                paramInt = 0;
            while (true)
            {
                this.mUserActivityCounters[paramInt].stepAtomic();
                return;
                if (paramInt >= 7)
                    paramInt = 6;
            }
        }

        public void noteVideoTurnedOffLocked()
        {
            if (this.mVideoTurnedOn)
            {
                this.mVideoTurnedOn = false;
                this.mVideoTurnedOnTimer.stopRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteVideoTurnedOnLocked()
        {
            if (!this.mVideoTurnedOn)
            {
                this.mVideoTurnedOn = true;
                if (this.mVideoTurnedOnTimer == null)
                    this.mVideoTurnedOnTimer = new BatteryStatsImpl.StopwatchTimer(this, 8, null, BatteryStatsImpl.this.mUnpluggables);
                this.mVideoTurnedOnTimer.startRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteWifiMulticastDisabledLocked()
        {
            if (this.mWifiMulticastEnabled)
            {
                this.mWifiMulticastEnabled = false;
                this.mWifiMulticastTimer.stopRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteWifiMulticastEnabledLocked()
        {
            if (!this.mWifiMulticastEnabled)
            {
                this.mWifiMulticastEnabled = true;
                if (this.mWifiMulticastTimer == null)
                    this.mWifiMulticastTimer = new BatteryStatsImpl.StopwatchTimer(this, 7, BatteryStatsImpl.this.mWifiMulticastTimers, BatteryStatsImpl.this.mUnpluggables);
                this.mWifiMulticastTimer.startRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteWifiRunningLocked()
        {
            if (!this.mWifiRunning)
            {
                this.mWifiRunning = true;
                if (this.mWifiRunningTimer == null)
                    this.mWifiRunningTimer = new BatteryStatsImpl.StopwatchTimer(this, 4, BatteryStatsImpl.this.mWifiRunningTimers, BatteryStatsImpl.this.mUnpluggables);
                this.mWifiRunningTimer.startRunningLocked(BatteryStatsImpl.this);
            }
        }

        public void noteWifiStoppedLocked()
        {
            if (this.mWifiRunning)
            {
                this.mWifiRunning = false;
                this.mWifiRunningTimer.stopRunningLocked(BatteryStatsImpl.this);
            }
        }

        void readFromParcelLocked(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, Parcel paramParcel)
        {
            int i = paramParcel.readInt();
            this.mWakelockStats.clear();
            for (int j = 0; j < i; j++)
            {
                String str3 = paramParcel.readString();
                Wakelock localWakelock = new Wakelock();
                localWakelock.readFromParcelLocked(paramArrayList, paramParcel);
                this.mWakelockStats.put(str3, localWakelock);
            }
            int k = paramParcel.readInt();
            this.mSensorStats.clear();
            for (int m = 0; m < k; m++)
            {
                int i5 = paramParcel.readInt();
                Sensor localSensor = new Sensor(i5);
                localSensor.readFromParcelLocked(BatteryStatsImpl.this.mUnpluggables, paramParcel);
                this.mSensorStats.put(Integer.valueOf(i5), localSensor);
            }
            int n = paramParcel.readInt();
            this.mProcessStats.clear();
            for (int i1 = 0; i1 < n; i1++)
            {
                String str2 = paramParcel.readString();
                Proc localProc = new Proc();
                localProc.readFromParcelLocked(paramParcel);
                this.mProcessStats.put(str2, localProc);
            }
            int i2 = paramParcel.readInt();
            this.mPackageStats.clear();
            for (int i3 = 0; i3 < i2; i3++)
            {
                String str1 = paramParcel.readString();
                Pkg localPkg = new Pkg();
                localPkg.readFromParcelLocked(paramParcel);
                this.mPackageStats.put(str1, localPkg);
            }
            this.mLoadedTcpBytesReceived = paramParcel.readLong();
            this.mLoadedTcpBytesSent = paramParcel.readLong();
            this.mCurrentTcpBytesReceived = paramParcel.readLong();
            this.mCurrentTcpBytesSent = paramParcel.readLong();
            this.mTcpBytesReceivedAtLastUnplug = paramParcel.readLong();
            this.mTcpBytesSentAtLastUnplug = paramParcel.readLong();
            this.mWifiRunning = false;
            if (paramParcel.readInt() != 0)
            {
                this.mWifiRunningTimer = new BatteryStatsImpl.StopwatchTimer(this, 4, BatteryStatsImpl.this.mWifiRunningTimers, BatteryStatsImpl.this.mUnpluggables, paramParcel);
                this.mFullWifiLockOut = false;
                if (paramParcel.readInt() == 0)
                    break label605;
                this.mFullWifiLockTimer = new BatteryStatsImpl.StopwatchTimer(this, 5, BatteryStatsImpl.this.mFullWifiLockTimers, BatteryStatsImpl.this.mUnpluggables, paramParcel);
                label391: this.mScanWifiLockOut = false;
                if (paramParcel.readInt() == 0)
                    break label613;
                this.mScanWifiLockTimer = new BatteryStatsImpl.StopwatchTimer(this, 6, BatteryStatsImpl.this.mScanWifiLockTimers, BatteryStatsImpl.this.mUnpluggables, paramParcel);
                label432: this.mWifiMulticastEnabled = false;
                if (paramParcel.readInt() == 0)
                    break label621;
                this.mWifiMulticastTimer = new BatteryStatsImpl.StopwatchTimer(this, 7, BatteryStatsImpl.this.mWifiMulticastTimers, BatteryStatsImpl.this.mUnpluggables, paramParcel);
                label473: this.mAudioTurnedOn = false;
                if (paramParcel.readInt() == 0)
                    break label629;
                this.mAudioTurnedOnTimer = new BatteryStatsImpl.StopwatchTimer(this, 7, null, BatteryStatsImpl.this.mUnpluggables, paramParcel);
                label508: this.mVideoTurnedOn = false;
                if (paramParcel.readInt() == 0)
                    break label637;
            }
            label605: label613: label621: label629: label637: for (this.mVideoTurnedOnTimer = new BatteryStatsImpl.StopwatchTimer(this, 8, null, BatteryStatsImpl.this.mUnpluggables, paramParcel); ; this.mVideoTurnedOnTimer = null)
            {
                if (paramParcel.readInt() == 0)
                    break label645;
                this.mUserActivityCounters = new BatteryStatsImpl.Counter[7];
                for (int i4 = 0; i4 < 7; i4++)
                    this.mUserActivityCounters[i4] = new BatteryStatsImpl.Counter(BatteryStatsImpl.this.mUnpluggables, paramParcel);
                this.mWifiRunningTimer = null;
                break;
                this.mFullWifiLockTimer = null;
                break label391;
                this.mScanWifiLockTimer = null;
                break label432;
                this.mWifiMulticastTimer = null;
                break label473;
                this.mAudioTurnedOnTimer = null;
                break label508;
            }
            label645: this.mUserActivityCounters = null;
        }

        public void reportExcessiveCpuLocked(String paramString, long paramLong1, long paramLong2)
        {
            Proc localProc = getProcessStatsLocked(paramString);
            if (localProc != null)
                localProc.addExcessiveCpu(paramLong1, paramLong2);
        }

        public void reportExcessiveWakeLocked(String paramString, long paramLong1, long paramLong2)
        {
            Proc localProc = getProcessStatsLocked(paramString);
            if (localProc != null)
                localProc.addExcessiveWake(paramLong1, paramLong2);
        }

        boolean reset()
        {
            boolean bool1 = false;
            int m;
            boolean bool7;
            label62: boolean bool6;
            label97: boolean bool5;
            label132: boolean bool4;
            if (this.mWifiRunningTimer != null)
            {
                if (!this.mWifiRunningTimer.reset(BatteryStatsImpl.this, false))
                {
                    m = 1;
                    bool1 = 0x0 | m | this.mWifiRunning;
                }
            }
            else
            {
                if (this.mFullWifiLockTimer != null)
                {
                    if (this.mFullWifiLockTimer.reset(BatteryStatsImpl.this, false))
                        break label272;
                    bool7 = true;
                    bool1 = bool1 | bool7 | this.mFullWifiLockOut;
                }
                if (this.mScanWifiLockTimer != null)
                {
                    if (this.mScanWifiLockTimer.reset(BatteryStatsImpl.this, false))
                        break label278;
                    bool6 = true;
                    bool1 = bool1 | bool6 | this.mScanWifiLockOut;
                }
                if (this.mWifiMulticastTimer != null)
                {
                    if (this.mWifiMulticastTimer.reset(BatteryStatsImpl.this, false))
                        break label284;
                    bool5 = true;
                    bool1 = bool1 | bool5 | this.mWifiMulticastEnabled;
                }
                if (this.mAudioTurnedOnTimer != null)
                {
                    if (this.mAudioTurnedOnTimer.reset(BatteryStatsImpl.this, false))
                        break label290;
                    bool4 = true;
                    label167: bool1 = bool1 | bool4 | this.mAudioTurnedOn;
                }
                if (this.mVideoTurnedOnTimer != null)
                    if (this.mVideoTurnedOnTimer.reset(BatteryStatsImpl.this, false))
                        break label296;
            }
            label272: label278: label284: label290: label296: for (boolean bool3 = true; ; bool3 = false)
            {
                bool1 = bool1 | bool3 | this.mVideoTurnedOn;
                this.mLoadedTcpBytesSent = 0L;
                this.mLoadedTcpBytesReceived = 0L;
                this.mCurrentTcpBytesSent = 0L;
                this.mCurrentTcpBytesReceived = 0L;
                if (this.mUserActivityCounters == null)
                    break label302;
                for (int k = 0; k < 7; k++)
                    this.mUserActivityCounters[k].reset(false);
                m = 0;
                break;
                bool7 = false;
                break label62;
                bool6 = false;
                break label97;
                bool5 = false;
                break label132;
                bool4 = false;
                break label167;
            }
            label302: if (this.mWakelockStats.size() > 0)
            {
                Iterator localIterator5 = this.mWakelockStats.entrySet().iterator();
                while (localIterator5.hasNext())
                    if (((Wakelock)((Map.Entry)localIterator5.next()).getValue()).reset())
                        localIterator5.remove();
                    else
                        bool1 = true;
            }
            if (this.mSensorStats.size() > 0)
            {
                Iterator localIterator4 = this.mSensorStats.entrySet().iterator();
                while (localIterator4.hasNext())
                    if (((Sensor)((Map.Entry)localIterator4.next()).getValue()).reset())
                        localIterator4.remove();
                    else
                        bool1 = true;
            }
            if (this.mProcessStats.size() > 0)
            {
                Iterator localIterator3 = this.mProcessStats.entrySet().iterator();
                while (localIterator3.hasNext())
                    ((Proc)((Map.Entry)localIterator3.next()).getValue()).detach();
                this.mProcessStats.clear();
            }
            if (this.mPids.size() > 0)
                for (int j = 0; (!bool1) && (j < this.mPids.size()); j++)
                    if (((BatteryStats.Uid.Pid)this.mPids.valueAt(j)).mWakeStart != 0L)
                        bool1 = true;
            if (this.mPackageStats.size() > 0)
            {
                Iterator localIterator1 = this.mPackageStats.entrySet().iterator();
                while (localIterator1.hasNext())
                {
                    Pkg localPkg = (Pkg)((Map.Entry)localIterator1.next()).getValue();
                    localPkg.detach();
                    if (localPkg.mServiceStats.size() > 0)
                    {
                        Iterator localIterator2 = localPkg.mServiceStats.entrySet().iterator();
                        while (localIterator2.hasNext())
                            ((BatteryStatsImpl.Uid.Pkg.Serv)((Map.Entry)localIterator2.next()).getValue()).detach();
                    }
                }
                this.mPackageStats.clear();
            }
            this.mPids.clear();
            if (!bool1)
            {
                if (this.mWifiRunningTimer != null)
                    this.mWifiRunningTimer.detach();
                if (this.mFullWifiLockTimer != null)
                    this.mFullWifiLockTimer.detach();
                if (this.mScanWifiLockTimer != null)
                    this.mScanWifiLockTimer.detach();
                if (this.mWifiMulticastTimer != null)
                    this.mWifiMulticastTimer.detach();
                if (this.mAudioTurnedOnTimer != null)
                    this.mAudioTurnedOnTimer.detach();
                if (this.mVideoTurnedOnTimer != null)
                    this.mVideoTurnedOnTimer.detach();
                if (this.mUserActivityCounters != null)
                    for (int i = 0; i < 7; i++)
                        this.mUserActivityCounters[i].detach();
            }
            if (!bool1);
            for (boolean bool2 = true; ; bool2 = false)
                return bool2;
        }

        void writeToParcelLocked(Parcel paramParcel, long paramLong)
        {
            paramParcel.writeInt(this.mWakelockStats.size());
            Iterator localIterator1 = this.mWakelockStats.entrySet().iterator();
            while (localIterator1.hasNext())
            {
                Map.Entry localEntry4 = (Map.Entry)localIterator1.next();
                paramParcel.writeString((String)localEntry4.getKey());
                ((Wakelock)localEntry4.getValue()).writeToParcelLocked(paramParcel, paramLong);
            }
            paramParcel.writeInt(this.mSensorStats.size());
            Iterator localIterator2 = this.mSensorStats.entrySet().iterator();
            while (localIterator2.hasNext())
            {
                Map.Entry localEntry3 = (Map.Entry)localIterator2.next();
                paramParcel.writeInt(((Integer)localEntry3.getKey()).intValue());
                ((Sensor)localEntry3.getValue()).writeToParcelLocked(paramParcel, paramLong);
            }
            paramParcel.writeInt(this.mProcessStats.size());
            Iterator localIterator3 = this.mProcessStats.entrySet().iterator();
            while (localIterator3.hasNext())
            {
                Map.Entry localEntry2 = (Map.Entry)localIterator3.next();
                paramParcel.writeString((String)localEntry2.getKey());
                ((Proc)localEntry2.getValue()).writeToParcelLocked(paramParcel);
            }
            paramParcel.writeInt(this.mPackageStats.size());
            Iterator localIterator4 = this.mPackageStats.entrySet().iterator();
            while (localIterator4.hasNext())
            {
                Map.Entry localEntry1 = (Map.Entry)localIterator4.next();
                paramParcel.writeString((String)localEntry1.getKey());
                ((Pkg)localEntry1.getValue()).writeToParcelLocked(paramParcel);
            }
            paramParcel.writeLong(this.mLoadedTcpBytesReceived);
            paramParcel.writeLong(this.mLoadedTcpBytesSent);
            paramParcel.writeLong(computeCurrentTcpBytesReceived());
            paramParcel.writeLong(computeCurrentTcpBytesSent());
            paramParcel.writeLong(this.mTcpBytesReceivedAtLastUnplug);
            paramParcel.writeLong(this.mTcpBytesSentAtLastUnplug);
            if (this.mWifiRunningTimer != null)
            {
                paramParcel.writeInt(1);
                this.mWifiRunningTimer.writeToParcel(paramParcel, paramLong);
                if (this.mFullWifiLockTimer == null)
                    break label538;
                paramParcel.writeInt(1);
                this.mFullWifiLockTimer.writeToParcel(paramParcel, paramLong);
                label407: if (this.mScanWifiLockTimer == null)
                    break label546;
                paramParcel.writeInt(1);
                this.mScanWifiLockTimer.writeToParcel(paramParcel, paramLong);
                label428: if (this.mWifiMulticastTimer == null)
                    break label554;
                paramParcel.writeInt(1);
                this.mWifiMulticastTimer.writeToParcel(paramParcel, paramLong);
                label449: if (this.mAudioTurnedOnTimer == null)
                    break label562;
                paramParcel.writeInt(1);
                this.mAudioTurnedOnTimer.writeToParcel(paramParcel, paramLong);
                label470: if (this.mVideoTurnedOnTimer == null)
                    break label570;
                paramParcel.writeInt(1);
                this.mVideoTurnedOnTimer.writeToParcel(paramParcel, paramLong);
            }
            while (true)
            {
                if (this.mUserActivityCounters == null)
                    break label578;
                paramParcel.writeInt(1);
                for (int i = 0; i < 7; i++)
                    this.mUserActivityCounters[i].writeToParcel(paramParcel);
                paramParcel.writeInt(0);
                break;
                label538: paramParcel.writeInt(0);
                break label407;
                label546: paramParcel.writeInt(0);
                break label428;
                label554: paramParcel.writeInt(0);
                break label449;
                label562: paramParcel.writeInt(0);
                break label470;
                label570: paramParcel.writeInt(0);
            }
            label578: paramParcel.writeInt(0);
        }

        public final class Pkg extends BatteryStats.Uid.Pkg
            implements BatteryStatsImpl.Unpluggable
        {
            int mLastWakeups;
            int mLoadedWakeups;
            final HashMap<String, Serv> mServiceStats = new HashMap();
            int mUnpluggedWakeups;
            int mWakeups;

            Pkg()
            {
                BatteryStatsImpl.this.mUnpluggables.add(this);
            }

            void detach()
            {
                BatteryStatsImpl.this.mUnpluggables.remove(this);
            }

            public BatteryStatsImpl getBatteryStats()
            {
                return BatteryStatsImpl.this;
            }

            public Map<String, ? extends BatteryStats.Uid.Pkg.Serv> getServiceStats()
            {
                return this.mServiceStats;
            }

            public int getWakeups(int paramInt)
            {
                int i;
                if (paramInt == 1)
                    i = this.mLastWakeups;
                while (true)
                {
                    return i;
                    i = this.mWakeups;
                    if (paramInt == 2)
                        i -= this.mLoadedWakeups;
                    else if (paramInt == 3)
                        i -= this.mUnpluggedWakeups;
                }
            }

            public void incWakeupsLocked()
            {
                this.mWakeups = (1 + this.mWakeups);
            }

            final Serv newServiceStatsLocked()
            {
                return new Serv();
            }

            public void plug(long paramLong1, long paramLong2)
            {
            }

            void readFromParcelLocked(Parcel paramParcel)
            {
                this.mWakeups = paramParcel.readInt();
                this.mLoadedWakeups = paramParcel.readInt();
                this.mLastWakeups = 0;
                this.mUnpluggedWakeups = paramParcel.readInt();
                int i = paramParcel.readInt();
                this.mServiceStats.clear();
                for (int j = 0; j < i; j++)
                {
                    String str = paramParcel.readString();
                    Serv localServ = new Serv();
                    this.mServiceStats.put(str, localServ);
                    localServ.readFromParcelLocked(paramParcel);
                }
            }

            public void unplug(long paramLong1, long paramLong2)
            {
                this.mUnpluggedWakeups = this.mWakeups;
            }

            void writeToParcelLocked(Parcel paramParcel)
            {
                paramParcel.writeInt(this.mWakeups);
                paramParcel.writeInt(this.mLoadedWakeups);
                paramParcel.writeInt(this.mUnpluggedWakeups);
                paramParcel.writeInt(this.mServiceStats.size());
                Iterator localIterator = this.mServiceStats.entrySet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    paramParcel.writeString((String)localEntry.getKey());
                    ((Serv)localEntry.getValue()).writeToParcelLocked(paramParcel);
                }
            }

            public final class Serv extends BatteryStats.Uid.Pkg.Serv
                implements BatteryStatsImpl.Unpluggable
            {
                int mLastLaunches;
                long mLastStartTime;
                int mLastStarts;
                boolean mLaunched;
                long mLaunchedSince;
                long mLaunchedTime;
                int mLaunches;
                int mLoadedLaunches;
                long mLoadedStartTime;
                int mLoadedStarts;
                boolean mRunning;
                long mRunningSince;
                long mStartTime;
                int mStarts;
                int mUnpluggedLaunches;
                long mUnpluggedStartTime;
                int mUnpluggedStarts;

                Serv()
                {
                    super();
                    BatteryStatsImpl.this.mUnpluggables.add(this);
                }

                void detach()
                {
                    BatteryStatsImpl.this.mUnpluggables.remove(this);
                }

                public BatteryStatsImpl getBatteryStats()
                {
                    return BatteryStatsImpl.this;
                }

                long getLaunchTimeToNowLocked(long paramLong)
                {
                    if (!this.mLaunched);
                    for (long l = this.mLaunchedTime; ; l = paramLong + this.mLaunchedTime - this.mLaunchedSince)
                        return l;
                }

                public int getLaunches(int paramInt)
                {
                    int i;
                    if (paramInt == 1)
                        i = this.mLastLaunches;
                    while (true)
                    {
                        return i;
                        i = this.mLaunches;
                        if (paramInt == 2)
                            i -= this.mLoadedLaunches;
                        else if (paramInt == 3)
                            i -= this.mUnpluggedLaunches;
                    }
                }

                public long getStartTime(long paramLong, int paramInt)
                {
                    long l;
                    if (paramInt == 1)
                        l = this.mLastStartTime;
                    while (true)
                    {
                        return l;
                        l = getStartTimeToNowLocked(paramLong);
                        if (paramInt == 2)
                            l -= this.mLoadedStartTime;
                        else if (paramInt == 3)
                            l -= this.mUnpluggedStartTime;
                    }
                }

                long getStartTimeToNowLocked(long paramLong)
                {
                    if (!this.mRunning);
                    for (long l = this.mStartTime; ; l = paramLong + this.mStartTime - this.mRunningSince)
                        return l;
                }

                public int getStarts(int paramInt)
                {
                    int i;
                    if (paramInt == 1)
                        i = this.mLastStarts;
                    while (true)
                    {
                        return i;
                        i = this.mStarts;
                        if (paramInt == 2)
                            i -= this.mLoadedStarts;
                        else if (paramInt == 3)
                            i -= this.mUnpluggedStarts;
                    }
                }

                public void plug(long paramLong1, long paramLong2)
                {
                }

                void readFromParcelLocked(Parcel paramParcel)
                {
                    boolean bool1 = true;
                    this.mStartTime = paramParcel.readLong();
                    this.mRunningSince = paramParcel.readLong();
                    boolean bool2;
                    if (paramParcel.readInt() != 0)
                    {
                        bool2 = bool1;
                        this.mRunning = bool2;
                        this.mStarts = paramParcel.readInt();
                        this.mLaunchedTime = paramParcel.readLong();
                        this.mLaunchedSince = paramParcel.readLong();
                        if (paramParcel.readInt() == 0)
                            break label145;
                    }
                    while (true)
                    {
                        this.mLaunched = bool1;
                        this.mLaunches = paramParcel.readInt();
                        this.mLoadedStartTime = paramParcel.readLong();
                        this.mLoadedStarts = paramParcel.readInt();
                        this.mLoadedLaunches = paramParcel.readInt();
                        this.mLastStartTime = 0L;
                        this.mLastStarts = 0;
                        this.mLastLaunches = 0;
                        this.mUnpluggedStartTime = paramParcel.readLong();
                        this.mUnpluggedStarts = paramParcel.readInt();
                        this.mUnpluggedLaunches = paramParcel.readInt();
                        return;
                        bool2 = false;
                        break;
                        label145: bool1 = false;
                    }
                }

                public void startLaunchedLocked()
                {
                    if (!this.mLaunched)
                    {
                        this.mLaunches = (1 + this.mLaunches);
                        this.mLaunchedSince = BatteryStatsImpl.this.getBatteryUptimeLocked();
                        this.mLaunched = true;
                    }
                }

                public void startRunningLocked()
                {
                    if (!this.mRunning)
                    {
                        this.mStarts = (1 + this.mStarts);
                        this.mRunningSince = BatteryStatsImpl.this.getBatteryUptimeLocked();
                        this.mRunning = true;
                    }
                }

                public void stopLaunchedLocked()
                {
                    if (this.mLaunched)
                    {
                        long l = BatteryStatsImpl.this.getBatteryUptimeLocked() - this.mLaunchedSince;
                        if (l <= 0L)
                            break label48;
                        this.mLaunchedTime = (l + this.mLaunchedTime);
                    }
                    while (true)
                    {
                        this.mLaunched = false;
                        return;
                        label48: this.mLaunches = (-1 + this.mLaunches);
                    }
                }

                public void stopRunningLocked()
                {
                    if (this.mRunning)
                    {
                        long l = BatteryStatsImpl.this.getBatteryUptimeLocked() - this.mRunningSince;
                        if (l <= 0L)
                            break label48;
                        this.mStartTime = (l + this.mStartTime);
                    }
                    while (true)
                    {
                        this.mRunning = false;
                        return;
                        label48: this.mStarts = (-1 + this.mStarts);
                    }
                }

                public void unplug(long paramLong1, long paramLong2)
                {
                    this.mUnpluggedStartTime = getStartTimeToNowLocked(paramLong1);
                    this.mUnpluggedStarts = this.mStarts;
                    this.mUnpluggedLaunches = this.mLaunches;
                }

                void writeToParcelLocked(Parcel paramParcel)
                {
                    int i = 1;
                    paramParcel.writeLong(this.mStartTime);
                    paramParcel.writeLong(this.mRunningSince);
                    int j;
                    if (this.mRunning)
                    {
                        j = i;
                        paramParcel.writeInt(j);
                        paramParcel.writeInt(this.mStarts);
                        paramParcel.writeLong(this.mLaunchedTime);
                        paramParcel.writeLong(this.mLaunchedSince);
                        if (!this.mLaunched)
                            break label130;
                    }
                    while (true)
                    {
                        paramParcel.writeInt(i);
                        paramParcel.writeInt(this.mLaunches);
                        paramParcel.writeLong(this.mLoadedStartTime);
                        paramParcel.writeInt(this.mLoadedStarts);
                        paramParcel.writeInt(this.mLoadedLaunches);
                        paramParcel.writeLong(this.mUnpluggedStartTime);
                        paramParcel.writeInt(this.mUnpluggedStarts);
                        paramParcel.writeInt(this.mUnpluggedLaunches);
                        return;
                        j = 0;
                        break;
                        label130: i = 0;
                    }
                }
            }
        }

        public final class Proc extends BatteryStats.Uid.Proc
            implements BatteryStatsImpl.Unpluggable
        {
            ArrayList<BatteryStats.Uid.Proc.ExcessivePower> mExcessivePower;
            long mForegroundTime;
            long mLastForegroundTime;
            int mLastStarts;
            long mLastSystemTime;
            long mLastUserTime;
            long mLoadedForegroundTime;
            int mLoadedStarts;
            long mLoadedSystemTime;
            long mLoadedUserTime;
            BatteryStatsImpl.SamplingCounter[] mSpeedBins;
            int mStarts;
            long mSystemTime;
            long mUnpluggedForegroundTime;
            int mUnpluggedStarts;
            long mUnpluggedSystemTime;
            long mUnpluggedUserTime;
            long mUserTime;

            Proc()
            {
                BatteryStatsImpl.this.mUnpluggables.add(this);
                this.mSpeedBins = new BatteryStatsImpl.SamplingCounter[BatteryStatsImpl.this.getCpuSpeedSteps()];
            }

            public void addCpuTimeLocked(int paramInt1, int paramInt2)
            {
                this.mUserTime += paramInt1;
                this.mSystemTime += paramInt2;
            }

            public void addExcessiveCpu(long paramLong1, long paramLong2)
            {
                if (this.mExcessivePower == null)
                    this.mExcessivePower = new ArrayList();
                BatteryStats.Uid.Proc.ExcessivePower localExcessivePower = new BatteryStats.Uid.Proc.ExcessivePower();
                localExcessivePower.type = 2;
                localExcessivePower.overTime = paramLong1;
                localExcessivePower.usedTime = paramLong2;
                this.mExcessivePower.add(localExcessivePower);
            }

            public void addExcessiveWake(long paramLong1, long paramLong2)
            {
                if (this.mExcessivePower == null)
                    this.mExcessivePower = new ArrayList();
                BatteryStats.Uid.Proc.ExcessivePower localExcessivePower = new BatteryStats.Uid.Proc.ExcessivePower();
                localExcessivePower.type = 1;
                localExcessivePower.overTime = paramLong1;
                localExcessivePower.usedTime = paramLong2;
                this.mExcessivePower.add(localExcessivePower);
            }

            public void addForegroundTimeLocked(long paramLong)
            {
                this.mForegroundTime = (paramLong + this.mForegroundTime);
            }

            public void addSpeedStepTimes(long[] paramArrayOfLong)
            {
                for (int i = 0; (i < this.mSpeedBins.length) && (i < paramArrayOfLong.length); i++)
                    if (paramArrayOfLong[i] != 0L)
                    {
                        BatteryStatsImpl.SamplingCounter localSamplingCounter = this.mSpeedBins[i];
                        if (localSamplingCounter == null)
                        {
                            BatteryStatsImpl.SamplingCounter[] arrayOfSamplingCounter = this.mSpeedBins;
                            localSamplingCounter = new BatteryStatsImpl.SamplingCounter(BatteryStatsImpl.this.mUnpluggables);
                            arrayOfSamplingCounter[i] = localSamplingCounter;
                        }
                        localSamplingCounter.addCountAtomic(paramArrayOfLong[i]);
                    }
            }

            public int countExcessivePowers()
            {
                if (this.mExcessivePower != null);
                for (int i = this.mExcessivePower.size(); ; i = 0)
                    return i;
            }

            void detach()
            {
                BatteryStatsImpl.this.mUnpluggables.remove(this);
                for (int i = 0; i < this.mSpeedBins.length; i++)
                {
                    BatteryStatsImpl.SamplingCounter localSamplingCounter = this.mSpeedBins[i];
                    if (localSamplingCounter != null)
                    {
                        BatteryStatsImpl.this.mUnpluggables.remove(localSamplingCounter);
                        this.mSpeedBins[i] = null;
                    }
                }
            }

            public BatteryStatsImpl getBatteryStats()
            {
                return BatteryStatsImpl.this;
            }

            public BatteryStats.Uid.Proc.ExcessivePower getExcessivePower(int paramInt)
            {
                if (this.mExcessivePower != null);
                for (BatteryStats.Uid.Proc.ExcessivePower localExcessivePower = (BatteryStats.Uid.Proc.ExcessivePower)this.mExcessivePower.get(paramInt); ; localExcessivePower = null)
                    return localExcessivePower;
            }

            public long getForegroundTime(int paramInt)
            {
                long l;
                if (paramInt == 1)
                    l = this.mLastForegroundTime;
                while (true)
                {
                    return l;
                    l = this.mForegroundTime;
                    if (paramInt == 2)
                        l -= this.mLoadedForegroundTime;
                    else if (paramInt == 3)
                        l -= this.mUnpluggedForegroundTime;
                }
            }

            public int getStarts(int paramInt)
            {
                int i;
                if (paramInt == 1)
                    i = this.mLastStarts;
                while (true)
                {
                    return i;
                    i = this.mStarts;
                    if (paramInt == 2)
                        i -= this.mLoadedStarts;
                    else if (paramInt == 3)
                        i -= this.mUnpluggedStarts;
                }
            }

            public long getSystemTime(int paramInt)
            {
                long l;
                if (paramInt == 1)
                    l = this.mLastSystemTime;
                while (true)
                {
                    return l;
                    l = this.mSystemTime;
                    if (paramInt == 2)
                        l -= this.mLoadedSystemTime;
                    else if (paramInt == 3)
                        l -= this.mUnpluggedSystemTime;
                }
            }

            public long getTimeAtCpuSpeedStep(int paramInt1, int paramInt2)
            {
                long l = 0L;
                if (paramInt1 < this.mSpeedBins.length)
                {
                    BatteryStatsImpl.SamplingCounter localSamplingCounter = this.mSpeedBins[paramInt1];
                    if (localSamplingCounter != null)
                        l = localSamplingCounter.getCountLocked(paramInt2);
                }
                return l;
            }

            public long getUserTime(int paramInt)
            {
                long l;
                if (paramInt == 1)
                    l = this.mLastUserTime;
                while (true)
                {
                    return l;
                    l = this.mUserTime;
                    if (paramInt == 2)
                        l -= this.mLoadedUserTime;
                    else if (paramInt == 3)
                        l -= this.mUnpluggedUserTime;
                }
            }

            public void incStartsLocked()
            {
                this.mStarts = (1 + this.mStarts);
            }

            public void plug(long paramLong1, long paramLong2)
            {
            }

            boolean readExcessivePowerFromParcelLocked(Parcel paramParcel)
            {
                boolean bool = true;
                int i = paramParcel.readInt();
                if (i == 0)
                    this.mExcessivePower = null;
                while (true)
                {
                    return bool;
                    if (i > 10000)
                    {
                        Slog.w("BatteryStatsImpl", "File corrupt: too many excessive power entries " + i);
                        bool = false;
                    }
                    else
                    {
                        this.mExcessivePower = new ArrayList();
                        for (int j = 0; j < i; j++)
                        {
                            BatteryStats.Uid.Proc.ExcessivePower localExcessivePower = new BatteryStats.Uid.Proc.ExcessivePower();
                            localExcessivePower.type = paramParcel.readInt();
                            localExcessivePower.overTime = paramParcel.readLong();
                            localExcessivePower.usedTime = paramParcel.readLong();
                            this.mExcessivePower.add(localExcessivePower);
                        }
                    }
                }
            }

            void readFromParcelLocked(Parcel paramParcel)
            {
                this.mUserTime = paramParcel.readLong();
                this.mSystemTime = paramParcel.readLong();
                this.mForegroundTime = paramParcel.readLong();
                this.mStarts = paramParcel.readInt();
                this.mLoadedUserTime = paramParcel.readLong();
                this.mLoadedSystemTime = paramParcel.readLong();
                this.mLoadedForegroundTime = paramParcel.readLong();
                this.mLoadedStarts = paramParcel.readInt();
                this.mLastUserTime = 0L;
                this.mLastSystemTime = 0L;
                this.mLastForegroundTime = 0L;
                this.mLastStarts = 0;
                this.mUnpluggedUserTime = paramParcel.readLong();
                this.mUnpluggedSystemTime = paramParcel.readLong();
                this.mUnpluggedForegroundTime = paramParcel.readLong();
                this.mUnpluggedStarts = paramParcel.readInt();
                int i = paramParcel.readInt();
                int j = BatteryStatsImpl.this.getCpuSpeedSteps();
                if (i >= j)
                    j = i;
                this.mSpeedBins = new BatteryStatsImpl.SamplingCounter[j];
                for (int k = 0; k < i; k++)
                    if (paramParcel.readInt() != 0)
                        this.mSpeedBins[k] = new BatteryStatsImpl.SamplingCounter(BatteryStatsImpl.this.mUnpluggables, paramParcel);
                readExcessivePowerFromParcelLocked(paramParcel);
            }

            public void unplug(long paramLong1, long paramLong2)
            {
                this.mUnpluggedUserTime = this.mUserTime;
                this.mUnpluggedSystemTime = this.mSystemTime;
                this.mUnpluggedStarts = this.mStarts;
                this.mUnpluggedForegroundTime = this.mForegroundTime;
            }

            void writeExcessivePowerToParcelLocked(Parcel paramParcel)
            {
                if (this.mExcessivePower == null)
                    paramParcel.writeInt(0);
                while (true)
                {
                    return;
                    int i = this.mExcessivePower.size();
                    paramParcel.writeInt(i);
                    for (int j = 0; j < i; j++)
                    {
                        BatteryStats.Uid.Proc.ExcessivePower localExcessivePower = (BatteryStats.Uid.Proc.ExcessivePower)this.mExcessivePower.get(j);
                        paramParcel.writeInt(localExcessivePower.type);
                        paramParcel.writeLong(localExcessivePower.overTime);
                        paramParcel.writeLong(localExcessivePower.usedTime);
                    }
                }
            }

            void writeToParcelLocked(Parcel paramParcel)
            {
                paramParcel.writeLong(this.mUserTime);
                paramParcel.writeLong(this.mSystemTime);
                paramParcel.writeLong(this.mForegroundTime);
                paramParcel.writeInt(this.mStarts);
                paramParcel.writeLong(this.mLoadedUserTime);
                paramParcel.writeLong(this.mLoadedSystemTime);
                paramParcel.writeLong(this.mLoadedForegroundTime);
                paramParcel.writeInt(this.mLoadedStarts);
                paramParcel.writeLong(this.mUnpluggedUserTime);
                paramParcel.writeLong(this.mUnpluggedSystemTime);
                paramParcel.writeLong(this.mUnpluggedForegroundTime);
                paramParcel.writeInt(this.mUnpluggedStarts);
                paramParcel.writeInt(this.mSpeedBins.length);
                int i = 0;
                if (i < this.mSpeedBins.length)
                {
                    BatteryStatsImpl.SamplingCounter localSamplingCounter = this.mSpeedBins[i];
                    if (localSamplingCounter != null)
                    {
                        paramParcel.writeInt(1);
                        localSamplingCounter.writeToParcel(paramParcel);
                    }
                    while (true)
                    {
                        i++;
                        break;
                        paramParcel.writeInt(0);
                    }
                }
                writeExcessivePowerToParcelLocked(paramParcel);
            }
        }

        public final class Sensor extends BatteryStats.Uid.Sensor
        {
            final int mHandle;
            BatteryStatsImpl.StopwatchTimer mTimer;

            public Sensor(int arg2)
            {
                int i;
                this.mHandle = i;
            }

            private BatteryStatsImpl.StopwatchTimer readTimerFromParcel(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, Parcel paramParcel)
            {
                if (paramParcel.readInt() == 0);
                ArrayList localArrayList;
                for (BatteryStatsImpl.StopwatchTimer localStopwatchTimer = null; ; localStopwatchTimer = new BatteryStatsImpl.StopwatchTimer(BatteryStatsImpl.Uid.this, 0, localArrayList, paramArrayList, paramParcel))
                {
                    return localStopwatchTimer;
                    localArrayList = (ArrayList)BatteryStatsImpl.this.mSensorTimers.get(this.mHandle);
                    if (localArrayList == null)
                    {
                        localArrayList = new ArrayList();
                        BatteryStatsImpl.this.mSensorTimers.put(this.mHandle, localArrayList);
                    }
                }
            }

            public int getHandle()
            {
                return this.mHandle;
            }

            public BatteryStatsImpl.Timer getSensorTime()
            {
                return this.mTimer;
            }

            void readFromParcelLocked(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, Parcel paramParcel)
            {
                this.mTimer = readTimerFromParcel(paramArrayList, paramParcel);
            }

            boolean reset()
            {
                boolean bool = true;
                if (this.mTimer.reset(BatteryStatsImpl.this, bool))
                    this.mTimer = null;
                while (true)
                {
                    return bool;
                    bool = false;
                }
            }

            void writeToParcelLocked(Parcel paramParcel, long paramLong)
            {
                BatteryStatsImpl.Timer.writeTimerToParcel(paramParcel, this.mTimer, paramLong);
            }
        }

        public final class Wakelock extends BatteryStats.Uid.Wakelock
        {
            BatteryStatsImpl.StopwatchTimer mTimerFull;
            BatteryStatsImpl.StopwatchTimer mTimerPartial;
            BatteryStatsImpl.StopwatchTimer mTimerWindow;

            public Wakelock()
            {
            }

            private BatteryStatsImpl.StopwatchTimer readTimerFromParcel(int paramInt, ArrayList<BatteryStatsImpl.StopwatchTimer> paramArrayList, ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList1, Parcel paramParcel)
            {
                if (paramParcel.readInt() == 0);
                for (BatteryStatsImpl.StopwatchTimer localStopwatchTimer = null; ; localStopwatchTimer = new BatteryStatsImpl.StopwatchTimer(BatteryStatsImpl.Uid.this, paramInt, paramArrayList, paramArrayList1, paramParcel))
                    return localStopwatchTimer;
            }

            public BatteryStatsImpl.Timer getWakeTime(int paramInt)
            {
                BatteryStatsImpl.StopwatchTimer localStopwatchTimer;
                switch (paramInt)
                {
                default:
                    throw new IllegalArgumentException("type = " + paramInt);
                case 1:
                    localStopwatchTimer = this.mTimerFull;
                case 0:
                case 2:
                }
                while (true)
                {
                    return localStopwatchTimer;
                    localStopwatchTimer = this.mTimerPartial;
                    continue;
                    localStopwatchTimer = this.mTimerWindow;
                }
            }

            void readFromParcelLocked(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, Parcel paramParcel)
            {
                this.mTimerPartial = readTimerFromParcel(0, BatteryStatsImpl.this.mPartialTimers, paramArrayList, paramParcel);
                this.mTimerFull = readTimerFromParcel(1, BatteryStatsImpl.this.mFullTimers, paramArrayList, paramParcel);
                this.mTimerWindow = readTimerFromParcel(2, BatteryStatsImpl.this.mWindowTimers, paramArrayList, paramParcel);
            }

            boolean reset()
            {
                boolean bool1 = true;
                boolean bool2 = false;
                boolean bool5;
                boolean bool4;
                label65: boolean bool3;
                if (this.mTimerFull != null)
                {
                    if (!this.mTimerFull.reset(BatteryStatsImpl.this, false))
                    {
                        bool5 = bool1;
                        bool2 = false | bool5;
                    }
                }
                else
                {
                    if (this.mTimerPartial != null)
                    {
                        if (this.mTimerPartial.reset(BatteryStatsImpl.this, false))
                            break label174;
                        bool4 = bool1;
                        bool2 |= bool4;
                    }
                    if (this.mTimerWindow != null)
                    {
                        if (this.mTimerWindow.reset(BatteryStatsImpl.this, false))
                            break label180;
                        bool3 = bool1;
                        label97: bool2 |= bool3;
                    }
                    if (!bool2)
                    {
                        if (this.mTimerFull != null)
                        {
                            this.mTimerFull.detach();
                            this.mTimerFull = null;
                        }
                        if (this.mTimerPartial != null)
                        {
                            this.mTimerPartial.detach();
                            this.mTimerPartial = null;
                        }
                        if (this.mTimerWindow != null)
                        {
                            this.mTimerWindow.detach();
                            this.mTimerWindow = null;
                        }
                    }
                    if (bool2)
                        break label185;
                }
                while (true)
                {
                    return bool1;
                    bool5 = false;
                    break;
                    label174: bool4 = false;
                    break label65;
                    label180: bool3 = false;
                    break label97;
                    label185: bool1 = false;
                }
            }

            void writeToParcelLocked(Parcel paramParcel, long paramLong)
            {
                BatteryStatsImpl.Timer.writeTimerToParcel(paramParcel, this.mTimerPartial, paramLong);
                BatteryStatsImpl.Timer.writeTimerToParcel(paramParcel, this.mTimerFull, paramLong);
                BatteryStatsImpl.Timer.writeTimerToParcel(paramParcel, this.mTimerWindow, paramLong);
            }
        }
    }

    private class KernelWakelockStats
    {
        public int mCount;
        public long mTotalTime;
        public int mVersion;

        KernelWakelockStats(int paramLong, long arg3, int arg5)
        {
            this.mCount = paramLong;
            this.mTotalTime = ???;
            int i;
            this.mVersion = i;
        }
    }

    public static final class StopwatchTimer extends BatteryStatsImpl.Timer
    {
        long mAcquireTime;
        boolean mInList;
        int mNesting;
        long mTimeout;
        final ArrayList<StopwatchTimer> mTimerPool;
        final BatteryStatsImpl.Uid mUid;
        long mUpdateTime;

        StopwatchTimer(BatteryStatsImpl.Uid paramUid, int paramInt, ArrayList<StopwatchTimer> paramArrayList, ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList1)
        {
            super(paramArrayList1);
            this.mUid = paramUid;
            this.mTimerPool = paramArrayList;
        }

        StopwatchTimer(BatteryStatsImpl.Uid paramUid, int paramInt, ArrayList<StopwatchTimer> paramArrayList, ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList1, Parcel paramParcel)
        {
            super(paramArrayList1, paramParcel);
            this.mUid = paramUid;
            this.mTimerPool = paramArrayList;
            this.mUpdateTime = paramParcel.readLong();
        }

        private static void refreshTimersLocked(BatteryStatsImpl paramBatteryStatsImpl, ArrayList<StopwatchTimer> paramArrayList)
        {
            long l1 = paramBatteryStatsImpl.getBatteryRealtimeLocked(1000L * SystemClock.elapsedRealtime());
            int i = paramArrayList.size();
            for (int j = i - 1; j >= 0; j--)
            {
                StopwatchTimer localStopwatchTimer = (StopwatchTimer)paramArrayList.get(j);
                long l2 = l1 - localStopwatchTimer.mUpdateTime;
                if (l2 > 0L)
                    localStopwatchTimer.mTotalTime += l2 / i;
                localStopwatchTimer.mUpdateTime = l1;
            }
        }

        protected int computeCurrentCountLocked()
        {
            return this.mCount;
        }

        protected long computeRunTimeLocked(long paramLong)
        {
            long l1 = 0L;
            if ((this.mTimeout > l1) && (paramLong > this.mUpdateTime + this.mTimeout))
                paramLong = this.mUpdateTime + this.mTimeout;
            long l2 = this.mTotalTime;
            long l3;
            if (this.mNesting > 0)
            {
                l3 = paramLong - this.mUpdateTime;
                if (this.mTimerPool == null)
                    break label84;
            }
            label84: for (int i = this.mTimerPool.size(); ; i = 1)
            {
                l1 = l3 / i;
                return l1 + l2;
            }
        }

        void detach()
        {
            super.detach();
            if (this.mTimerPool != null)
                this.mTimerPool.remove(this);
        }

        boolean isRunningLocked()
        {
            if (this.mNesting > 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void logState(Printer paramPrinter, String paramString)
        {
            super.logState(paramPrinter, paramString);
            paramPrinter.println(paramString + "mNesting=" + this.mNesting + "mUpdateTime=" + this.mUpdateTime + " mAcquireTime=" + this.mAcquireTime);
        }

        public void plug(long paramLong1, long paramLong2)
        {
            if (this.mNesting > 0)
            {
                super.plug(paramLong1, paramLong2);
                this.mUpdateTime = paramLong2;
            }
        }

        void readSummaryFromParcelLocked(Parcel paramParcel)
        {
            super.readSummaryFromParcelLocked(paramParcel);
            this.mNesting = 0;
        }

        boolean reset(BatteryStatsImpl paramBatteryStatsImpl, boolean paramBoolean)
        {
            boolean bool1 = true;
            boolean bool2;
            if (this.mNesting <= 0)
            {
                bool2 = bool1;
                if ((!bool2) || (!paramBoolean))
                    break label67;
            }
            while (true)
            {
                super.reset(paramBatteryStatsImpl, bool1);
                if (this.mNesting > 0)
                    this.mUpdateTime = paramBatteryStatsImpl.getBatteryRealtimeLocked(1000L * SystemClock.elapsedRealtime());
                this.mAcquireTime = this.mTotalTime;
                return bool2;
                bool2 = false;
                break;
                label67: bool1 = false;
            }
        }

        void setTimeout(long paramLong)
        {
            this.mTimeout = paramLong;
        }

        void startRunningLocked(BatteryStatsImpl paramBatteryStatsImpl)
        {
            int i = this.mNesting;
            this.mNesting = (i + 1);
            if (i == 0)
            {
                this.mUpdateTime = paramBatteryStatsImpl.getBatteryRealtimeLocked(1000L * SystemClock.elapsedRealtime());
                if (this.mTimerPool != null)
                {
                    refreshTimersLocked(paramBatteryStatsImpl, this.mTimerPool);
                    this.mTimerPool.add(this);
                }
                this.mCount = (1 + this.mCount);
                this.mAcquireTime = this.mTotalTime;
            }
        }

        void stopRunningLocked(BatteryStatsImpl paramBatteryStatsImpl)
        {
            if (this.mNesting == 0);
            label107: 
            while (true)
            {
                return;
                int i = -1 + this.mNesting;
                this.mNesting = i;
                if (i == 0)
                {
                    if (this.mTimerPool != null)
                    {
                        refreshTimersLocked(paramBatteryStatsImpl, this.mTimerPool);
                        this.mTimerPool.remove(this);
                    }
                    while (true)
                    {
                        if (this.mTotalTime != this.mAcquireTime)
                            break label107;
                        this.mCount = (-1 + this.mCount);
                        break;
                        long l = paramBatteryStatsImpl.getBatteryRealtimeLocked(1000L * SystemClock.elapsedRealtime());
                        this.mNesting = 1;
                        this.mTotalTime = computeRunTimeLocked(l);
                        this.mNesting = 0;
                    }
                }
            }
        }

        public void writeToParcel(Parcel paramParcel, long paramLong)
        {
            super.writeToParcel(paramParcel, paramLong);
            paramParcel.writeLong(this.mUpdateTime);
        }
    }

    public static final class SamplingTimer extends BatteryStatsImpl.Timer
    {
        int mCurrentReportedCount;
        long mCurrentReportedTotalTime;
        boolean mInDischarge;
        boolean mTrackingReportedValues;
        int mUnpluggedReportedCount;
        long mUnpluggedReportedTotalTime;
        int mUpdateVersion;

        SamplingTimer(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, boolean paramBoolean, Parcel paramParcel)
        {
            super(paramArrayList, paramParcel);
            this.mCurrentReportedCount = paramParcel.readInt();
            this.mUnpluggedReportedCount = paramParcel.readInt();
            this.mCurrentReportedTotalTime = paramParcel.readLong();
            this.mUnpluggedReportedTotalTime = paramParcel.readLong();
            if (paramParcel.readInt() == i);
            while (true)
            {
                this.mTrackingReportedValues = i;
                this.mInDischarge = paramBoolean;
                return;
                i = 0;
            }
        }

        SamplingTimer(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, boolean paramBoolean1, boolean paramBoolean2)
        {
            super(paramArrayList);
            this.mTrackingReportedValues = paramBoolean2;
            this.mInDischarge = paramBoolean1;
        }

        protected int computeCurrentCountLocked()
        {
            int i = this.mCount;
            if ((this.mInDischarge) && (this.mTrackingReportedValues));
            for (int j = this.mCurrentReportedCount - this.mUnpluggedReportedCount; ; j = 0)
                return j + i;
        }

        protected long computeRunTimeLocked(long paramLong)
        {
            long l1 = this.mTotalTime;
            if ((this.mInDischarge) && (this.mTrackingReportedValues));
            for (long l2 = this.mCurrentReportedTotalTime - this.mUnpluggedReportedTotalTime; ; l2 = 0L)
                return l2 + l1;
        }

        public int getUpdateVersion()
        {
            return this.mUpdateVersion;
        }

        public void logState(Printer paramPrinter, String paramString)
        {
            super.logState(paramPrinter, paramString);
            paramPrinter.println(paramString + "mCurrentReportedCount=" + this.mCurrentReportedCount + " mUnpluggedReportedCount=" + this.mUnpluggedReportedCount + " mCurrentReportedTotalTime=" + this.mCurrentReportedTotalTime + " mUnpluggedReportedTotalTime=" + this.mUnpluggedReportedTotalTime);
        }

        public void plug(long paramLong1, long paramLong2)
        {
            super.plug(paramLong1, paramLong2);
            this.mInDischarge = false;
        }

        void readSummaryFromParcelLocked(Parcel paramParcel)
        {
            int i = 1;
            super.readSummaryFromParcelLocked(paramParcel);
            long l = paramParcel.readLong();
            this.mCurrentReportedTotalTime = l;
            this.mUnpluggedReportedTotalTime = l;
            int j = paramParcel.readInt();
            this.mCurrentReportedCount = j;
            this.mUnpluggedReportedCount = j;
            if (paramParcel.readInt() == i);
            while (true)
            {
                this.mTrackingReportedValues = i;
                return;
                i = 0;
            }
        }

        boolean reset(BatteryStatsImpl paramBatteryStatsImpl, boolean paramBoolean)
        {
            super.reset(paramBatteryStatsImpl, paramBoolean);
            setStale();
            return true;
        }

        public void setStale()
        {
            this.mTrackingReportedValues = false;
            this.mUnpluggedReportedTotalTime = 0L;
            this.mUnpluggedReportedCount = 0;
        }

        public void setUpdateVersion(int paramInt)
        {
            this.mUpdateVersion = paramInt;
        }

        public void unplug(long paramLong1, long paramLong2)
        {
            super.unplug(paramLong1, paramLong2);
            if (this.mTrackingReportedValues)
            {
                this.mUnpluggedReportedTotalTime = this.mCurrentReportedTotalTime;
                this.mUnpluggedReportedCount = this.mCurrentReportedCount;
            }
            this.mInDischarge = true;
        }

        public void updateCurrentReportedCount(int paramInt)
        {
            if ((this.mInDischarge) && (this.mUnpluggedReportedCount == 0))
            {
                this.mUnpluggedReportedCount = paramInt;
                this.mTrackingReportedValues = true;
            }
            this.mCurrentReportedCount = paramInt;
        }

        public void updateCurrentReportedTotalTime(long paramLong)
        {
            if ((this.mInDischarge) && (this.mUnpluggedReportedTotalTime == 0L))
            {
                this.mUnpluggedReportedTotalTime = paramLong;
                this.mTrackingReportedValues = true;
            }
            this.mCurrentReportedTotalTime = paramLong;
        }

        void writeSummaryFromParcelLocked(Parcel paramParcel, long paramLong)
        {
            super.writeSummaryFromParcelLocked(paramParcel, paramLong);
            paramParcel.writeLong(this.mCurrentReportedTotalTime);
            paramParcel.writeInt(this.mCurrentReportedCount);
            if (this.mTrackingReportedValues);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                return;
            }
        }

        public void writeToParcel(Parcel paramParcel, long paramLong)
        {
            super.writeToParcel(paramParcel, paramLong);
            paramParcel.writeInt(this.mCurrentReportedCount);
            paramParcel.writeInt(this.mUnpluggedReportedCount);
            paramParcel.writeLong(this.mCurrentReportedTotalTime);
            paramParcel.writeLong(this.mUnpluggedReportedTotalTime);
            if (this.mTrackingReportedValues);
            for (int i = 1; ; i = 0)
            {
                paramParcel.writeInt(i);
                return;
            }
        }
    }

    public static abstract class Timer extends BatteryStats.Timer
        implements BatteryStatsImpl.Unpluggable
    {
        int mCount;
        int mLastCount;
        long mLastTime;
        int mLoadedCount;
        long mLoadedTime;
        long mTotalTime;
        final int mType;
        final ArrayList<BatteryStatsImpl.Unpluggable> mUnpluggables;
        int mUnpluggedCount;
        long mUnpluggedTime;

        Timer(int paramInt, ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList)
        {
            this.mType = paramInt;
            this.mUnpluggables = paramArrayList;
            paramArrayList.add(this);
        }

        Timer(int paramInt, ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, Parcel paramParcel)
        {
            this.mType = paramInt;
            this.mUnpluggables = paramArrayList;
            this.mCount = paramParcel.readInt();
            this.mLoadedCount = paramParcel.readInt();
            this.mLastCount = 0;
            this.mUnpluggedCount = paramParcel.readInt();
            this.mTotalTime = paramParcel.readLong();
            this.mLoadedTime = paramParcel.readLong();
            this.mLastTime = 0L;
            this.mUnpluggedTime = paramParcel.readLong();
            paramArrayList.add(this);
        }

        public static void writeTimerToParcel(Parcel paramParcel, Timer paramTimer, long paramLong)
        {
            if (paramTimer == null)
                paramParcel.writeInt(0);
            while (true)
            {
                return;
                paramParcel.writeInt(1);
                paramTimer.writeToParcel(paramParcel, paramLong);
            }
        }

        protected abstract int computeCurrentCountLocked();

        protected abstract long computeRunTimeLocked(long paramLong);

        void detach()
        {
            this.mUnpluggables.remove(this);
        }

        public int getCountLocked(int paramInt)
        {
            int i;
            if (paramInt == 1)
                i = this.mLastCount;
            while (true)
            {
                return i;
                i = computeCurrentCountLocked();
                if (paramInt == 3)
                    i -= this.mUnpluggedCount;
                else if (paramInt != 0)
                    i -= this.mLoadedCount;
            }
        }

        public long getTotalTimeLocked(long paramLong, int paramInt)
        {
            long l;
            if (paramInt == 1)
                l = this.mLastTime;
            while (true)
            {
                return l;
                l = computeRunTimeLocked(paramLong);
                if (paramInt == 3)
                    l -= this.mUnpluggedTime;
                else if (paramInt != 0)
                    l -= this.mLoadedTime;
            }
        }

        public void logState(Printer paramPrinter, String paramString)
        {
            paramPrinter.println(paramString + " mCount=" + this.mCount + " mLoadedCount=" + this.mLoadedCount + " mLastCount=" + this.mLastCount + " mUnpluggedCount=" + this.mUnpluggedCount);
            paramPrinter.println(paramString + "mTotalTime=" + this.mTotalTime + " mLoadedTime=" + this.mLoadedTime);
            paramPrinter.println(paramString + "mLastTime=" + this.mLastTime + " mUnpluggedTime=" + this.mUnpluggedTime);
        }

        public void plug(long paramLong1, long paramLong2)
        {
            this.mTotalTime = computeRunTimeLocked(paramLong2);
            this.mCount = computeCurrentCountLocked();
        }

        void readSummaryFromParcelLocked(Parcel paramParcel)
        {
            long l = 1000L * paramParcel.readLong();
            this.mLoadedTime = l;
            this.mTotalTime = l;
            this.mLastTime = 0L;
            this.mUnpluggedTime = this.mTotalTime;
            int i = paramParcel.readInt();
            this.mLoadedCount = i;
            this.mCount = i;
            this.mLastCount = 0;
            this.mUnpluggedCount = this.mCount;
        }

        boolean reset(BatteryStatsImpl paramBatteryStatsImpl, boolean paramBoolean)
        {
            this.mLastTime = 0L;
            this.mLoadedTime = 0L;
            this.mTotalTime = 0L;
            this.mLastCount = 0;
            this.mLoadedCount = 0;
            this.mCount = 0;
            if (paramBoolean)
                detach();
            return true;
        }

        public void unplug(long paramLong1, long paramLong2)
        {
            this.mUnpluggedTime = computeRunTimeLocked(paramLong2);
            this.mUnpluggedCount = this.mCount;
        }

        void writeSummaryFromParcelLocked(Parcel paramParcel, long paramLong)
        {
            paramParcel.writeLong((500L + computeRunTimeLocked(paramLong)) / 1000L);
            paramParcel.writeInt(this.mCount);
        }

        public void writeToParcel(Parcel paramParcel, long paramLong)
        {
            paramParcel.writeInt(this.mCount);
            paramParcel.writeInt(this.mLoadedCount);
            paramParcel.writeInt(this.mUnpluggedCount);
            paramParcel.writeLong(computeRunTimeLocked(paramLong));
            paramParcel.writeLong(this.mLoadedTime);
            paramParcel.writeLong(this.mUnpluggedTime);
        }
    }

    public static class SamplingCounter extends BatteryStatsImpl.Counter
    {
        SamplingCounter(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList)
        {
            super();
        }

        SamplingCounter(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, Parcel paramParcel)
        {
            super(paramParcel);
        }

        public void addCountAtomic(long paramLong)
        {
            this.mCount.addAndGet((int)paramLong);
        }
    }

    public static class Counter extends BatteryStats.Counter
        implements BatteryStatsImpl.Unpluggable
    {
        final AtomicInteger mCount = new AtomicInteger();
        int mLastCount;
        int mLoadedCount;
        int mPluggedCount;
        final ArrayList<BatteryStatsImpl.Unpluggable> mUnpluggables;
        int mUnpluggedCount;

        Counter(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList)
        {
            this.mUnpluggables = paramArrayList;
            paramArrayList.add(this);
        }

        Counter(ArrayList<BatteryStatsImpl.Unpluggable> paramArrayList, Parcel paramParcel)
        {
            this.mUnpluggables = paramArrayList;
            this.mPluggedCount = paramParcel.readInt();
            this.mCount.set(this.mPluggedCount);
            this.mLoadedCount = paramParcel.readInt();
            this.mLastCount = 0;
            this.mUnpluggedCount = paramParcel.readInt();
            paramArrayList.add(this);
        }

        public static void writeCounterToParcel(Parcel paramParcel, Counter paramCounter)
        {
            if (paramCounter == null)
                paramParcel.writeInt(0);
            while (true)
            {
                return;
                paramParcel.writeInt(1);
                paramCounter.writeToParcel(paramParcel);
            }
        }

        void detach()
        {
            this.mUnpluggables.remove(this);
        }

        public int getCountLocked(int paramInt)
        {
            int i;
            if (paramInt == 1)
                i = this.mLastCount;
            while (true)
            {
                return i;
                i = this.mCount.get();
                if (paramInt == 3)
                    i -= this.mUnpluggedCount;
                else if (paramInt != 0)
                    i -= this.mLoadedCount;
            }
        }

        public void logState(Printer paramPrinter, String paramString)
        {
            paramPrinter.println(paramString + "mCount=" + this.mCount.get() + " mLoadedCount=" + this.mLoadedCount + " mLastCount=" + this.mLastCount + " mUnpluggedCount=" + this.mUnpluggedCount + " mPluggedCount=" + this.mPluggedCount);
        }

        public void plug(long paramLong1, long paramLong2)
        {
            this.mPluggedCount = this.mCount.get();
        }

        void readSummaryFromParcelLocked(Parcel paramParcel)
        {
            this.mLoadedCount = paramParcel.readInt();
            this.mCount.set(this.mLoadedCount);
            this.mLastCount = 0;
            int i = this.mLoadedCount;
            this.mPluggedCount = i;
            this.mUnpluggedCount = i;
        }

        void reset(boolean paramBoolean)
        {
            this.mCount.set(0);
            this.mUnpluggedCount = 0;
            this.mPluggedCount = 0;
            this.mLastCount = 0;
            this.mLoadedCount = 0;
            if (paramBoolean)
                detach();
        }

        void stepAtomic()
        {
            this.mCount.incrementAndGet();
        }

        public void unplug(long paramLong1, long paramLong2)
        {
            this.mUnpluggedCount = this.mPluggedCount;
            this.mCount.set(this.mPluggedCount);
        }

        void writeSummaryFromParcelLocked(Parcel paramParcel)
        {
            paramParcel.writeInt(this.mCount.get());
        }

        public void writeToParcel(Parcel paramParcel)
        {
            paramParcel.writeInt(this.mCount.get());
            paramParcel.writeInt(this.mLoadedCount);
            paramParcel.writeInt(this.mUnpluggedCount);
        }
    }

    public static abstract interface Unpluggable
    {
        public abstract void plug(long paramLong1, long paramLong2);

        public abstract void unplug(long paramLong1, long paramLong2);
    }

    final class MyHandler extends Handler
    {
        MyHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            BatteryStatsImpl.BatteryCallback localBatteryCallback = BatteryStatsImpl.this.mCallback;
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            }
            do
                while (true)
                {
                    return;
                    if (localBatteryCallback != null)
                        localBatteryCallback.batteryNeedsCpuUpdate();
                }
            while (localBatteryCallback == null);
            if (paramMessage.arg1 != 0);
            for (boolean bool = true; ; bool = false)
            {
                localBatteryCallback.batteryPowerChanged(bool);
                break;
            }
        }
    }

    public static abstract interface BatteryCallback
    {
        public abstract void batteryNeedsCpuUpdate();

        public abstract void batteryPowerChanged(boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.BatteryStatsImpl
 * JD-Core Version:        0.6.2
 */