package com.android.internal.os;

class ZygoteSecurityException extends RuntimeException
{
    ZygoteSecurityException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.ZygoteSecurityException
 * JD-Core Version:        0.6.2
 */