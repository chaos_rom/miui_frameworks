package com.android.internal.os;

import android.os.Process;
import android.util.Slog;
import dalvik.system.Zygote;
import java.io.DataOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import libcore.io.IoUtils;

public class WrapperInit
{
    private static final String TAG = "AndroidRuntime";

    public static void execApplication(String paramString1, String paramString2, int paramInt, FileDescriptor paramFileDescriptor, String[] paramArrayOfString)
    {
        StringBuilder localStringBuilder = new StringBuilder(paramString1);
        localStringBuilder.append(" /system/bin/app_process /system/bin --application");
        if (paramString2 != null)
            localStringBuilder.append(" '--nice-name=").append(paramString2).append("'");
        localStringBuilder.append(" com.android.internal.os.WrapperInit ");
        if (paramFileDescriptor != null);
        for (int i = paramFileDescriptor.getInt$(); ; i = 0)
        {
            localStringBuilder.append(i);
            localStringBuilder.append(' ');
            localStringBuilder.append(paramInt);
            Zygote.appendQuotedShellArgs(localStringBuilder, paramArrayOfString);
            Zygote.execShell(localStringBuilder.toString());
            return;
        }
    }

    public static void execStandalone(String paramString1, String paramString2, String paramString3, String[] paramArrayOfString)
    {
        StringBuilder localStringBuilder = new StringBuilder(paramString1);
        localStringBuilder.append(" /system/bin/dalvikvm -classpath '").append(paramString2);
        localStringBuilder.append("' ").append(paramString3);
        Zygote.appendQuotedShellArgs(localStringBuilder, paramArrayOfString);
        Zygote.execShell(localStringBuilder.toString());
    }

    public static void main(String[] paramArrayOfString)
    {
        try
        {
            int i = Integer.parseInt(paramArrayOfString[0], 10);
            int j = Integer.parseInt(paramArrayOfString[1], 10);
            if (i != 0);
            try
            {
                FileDescriptor localFileDescriptor = ZygoteInit.createFileDescriptor(i);
                DataOutputStream localDataOutputStream = new DataOutputStream(new FileOutputStream(localFileDescriptor));
                localDataOutputStream.writeInt(Process.myPid());
                localDataOutputStream.close();
                IoUtils.closeQuietly(localFileDescriptor);
                ZygoteInit.preload();
                String[] arrayOfString = new String[-2 + paramArrayOfString.length];
                System.arraycopy(paramArrayOfString, 2, arrayOfString, 0, arrayOfString.length);
                RuntimeInit.wrapperInit(j, arrayOfString);
            }
            catch (IOException localIOException)
            {
                while (true)
                    Slog.d("AndroidRuntime", "Could not write pid of wrapped process to Zygote pipe.", localIOException);
            }
        }
        catch (ZygoteInit.MethodAndArgsCaller localMethodAndArgsCaller)
        {
            localMethodAndArgsCaller.run();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.WrapperInit
 * JD-Core Version:        0.6.2
 */