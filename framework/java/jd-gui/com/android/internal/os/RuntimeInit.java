package com.android.internal.os;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.ApplicationErrorReport.CrashInfo;
import android.app.IActivityManager;
import android.ddm.DdmRegister;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Debug;
import android.os.IBinder;
import android.os.Process;
import android.os.SystemProperties;
import android.util.Slog;
import com.android.internal.logging.AndroidConfig;
import com.android.server.NetworkManagementSocketTagger;
import dalvik.system.VMRuntime;
import java.io.PrintStream;
import java.util.TimeZone;
import java.util.logging.LogManager;
import org.apache.harmony.luni.internal.util.TimezoneGetter;

public class RuntimeInit
{
    private static final boolean DEBUG = false;
    private static final String TAG = "AndroidRuntime";
    private static boolean initialized;
    private static IBinder mApplicationObject;
    private static volatile boolean mCrashing = false;

    static
    {
        DdmRegister.registerHandlers();
    }

    private static void applicationInit(int paramInt, String[] paramArrayOfString)
        throws ZygoteInit.MethodAndArgsCaller
    {
        nativeSetExitWithoutCleanup(true);
        VMRuntime.getRuntime().setTargetHeapUtilization(0.75F);
        VMRuntime.getRuntime().setTargetSdkVersion(paramInt);
        try
        {
            Arguments localArguments = new Arguments(paramArrayOfString);
            invokeStaticMain(localArguments.startClass, localArguments.startArgs);
            return;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
                Slog.e("AndroidRuntime", localIllegalArgumentException.getMessage());
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public static String callGetDefaultUserAgent()
    {
        return getDefaultUserAgent();
    }

    private static final void commonInit()
    {
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtHandler(null));
        TimezoneGetter.setInstance(new TimezoneGetter()
        {
            public String getId()
            {
                return SystemProperties.get("persist.sys.timezone");
            }
        });
        TimeZone.setDefault(null);
        LogManager.getLogManager().reset();
        new AndroidConfig();
        System.setProperty("http.agent", getDefaultUserAgent());
        NetworkManagementSocketTagger.install();
        if (SystemProperties.get("ro.kernel.android.tracing").equals("1"))
        {
            Slog.i("AndroidRuntime", "NOTE: emulator trace profiling enabled");
            Debug.enableEmulatorTraceOutput();
        }
        initialized = true;
    }

    public static final IBinder getApplicationObject()
    {
        return mApplicationObject;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private static String getDefaultUserAgent()
    {
        StringBuilder localStringBuilder = new StringBuilder(64);
        localStringBuilder.append("Dalvik/");
        localStringBuilder.append(System.getProperty("java.vm.version"));
        localStringBuilder.append(" (Linux; U; Android ");
        String str1 = Build.VERSION.RELEASE;
        if (str1.length() > 0);
        while (true)
        {
            localStringBuilder.append(str1);
            if ("REL".equals(Build.VERSION.CODENAME))
            {
                String str3 = Build.MODEL;
                if (str3.length() > 0)
                {
                    localStringBuilder.append("; ");
                    localStringBuilder.append(str3);
                }
            }
            String str2 = Injector.appendMiuiVersion(localStringBuilder, Build.ID);
            if (str2.length() > 0)
            {
                localStringBuilder.append(" Build/");
                localStringBuilder.append(str2);
            }
            localStringBuilder.append(")");
            return localStringBuilder.toString();
            str1 = "1.0";
        }
    }

    // ERROR //
    private static void invokeStaticMain(String paramString, String[] paramArrayOfString)
        throws ZygoteInit.MethodAndArgsCaller
    {
        // Byte code:
        //     0: aload_0
        //     1: invokestatic 247	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
        //     4: astore_3
        //     5: iconst_1
        //     6: anewarray 243	java/lang/Class
        //     9: astore 6
        //     11: aload 6
        //     13: iconst_0
        //     14: ldc 248
        //     16: aastore
        //     17: aload_3
        //     18: ldc 250
        //     20: aload 6
        //     22: invokevirtual 254	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //     25: astore 7
        //     27: aload 7
        //     29: invokevirtual 259	java/lang/reflect/Method:getModifiers	()I
        //     32: istore 8
        //     34: iload 8
        //     36: invokestatic 265	java/lang/reflect/Modifier:isStatic	(I)Z
        //     39: ifeq +11 -> 50
        //     42: iload 8
        //     44: invokestatic 268	java/lang/reflect/Modifier:isPublic	(I)Z
        //     47: ifne +125 -> 172
        //     50: new 270	java/lang/RuntimeException
        //     53: dup
        //     54: new 183	java/lang/StringBuilder
        //     57: dup
        //     58: invokespecial 271	java/lang/StringBuilder:<init>	()V
        //     61: ldc_w 273
        //     64: invokevirtual 191	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     67: aload_0
        //     68: invokevirtual 191	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     71: invokevirtual 233	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     74: invokespecial 276	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     77: athrow
        //     78: astore_2
        //     79: new 270	java/lang/RuntimeException
        //     82: dup
        //     83: new 183	java/lang/StringBuilder
        //     86: dup
        //     87: invokespecial 271	java/lang/StringBuilder:<init>	()V
        //     90: ldc_w 278
        //     93: invokevirtual 191	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     96: aload_0
        //     97: invokevirtual 191	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     100: invokevirtual 233	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     103: aload_2
        //     104: invokespecial 281	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     107: athrow
        //     108: astore 5
        //     110: new 270	java/lang/RuntimeException
        //     113: dup
        //     114: new 183	java/lang/StringBuilder
        //     117: dup
        //     118: invokespecial 271	java/lang/StringBuilder:<init>	()V
        //     121: ldc_w 283
        //     124: invokevirtual 191	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     127: aload_0
        //     128: invokevirtual 191	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     131: invokevirtual 233	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     134: aload 5
        //     136: invokespecial 281	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     139: athrow
        //     140: astore 4
        //     142: new 270	java/lang/RuntimeException
        //     145: dup
        //     146: new 183	java/lang/StringBuilder
        //     149: dup
        //     150: invokespecial 271	java/lang/StringBuilder:<init>	()V
        //     153: ldc_w 285
        //     156: invokevirtual 191	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     159: aload_0
        //     160: invokevirtual 191	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     163: invokevirtual 233	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     166: aload 4
        //     168: invokespecial 281	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     171: athrow
        //     172: new 50	com/android/internal/os/ZygoteInit$MethodAndArgsCaller
        //     175: dup
        //     176: aload 7
        //     178: aload_1
        //     179: invokespecial 288	com/android/internal/os/ZygoteInit$MethodAndArgsCaller:<init>	(Ljava/lang/reflect/Method;[Ljava/lang/String;)V
        //     182: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     0	5	78	java/lang/ClassNotFoundException
        //     5	27	108	java/lang/NoSuchMethodException
        //     5	27	140	java/lang/SecurityException
    }

    public static final void main(String[] paramArrayOfString)
    {
        if ((paramArrayOfString.length == 2) && (paramArrayOfString[1].equals("application")))
            redirectLogStreams();
        commonInit();
        nativeFinishInit();
    }

    private static final native void nativeFinishInit();

    private static final native void nativeSetExitWithoutCleanup(boolean paramBoolean);

    private static final native void nativeZygoteInit();

    public static void redirectLogStreams()
    {
        System.out.close();
        System.setOut(new AndroidPrintStream(4, "System.out"));
        System.err.close();
        System.setErr(new AndroidPrintStream(5, "System.err"));
    }

    public static final void setApplicationObject(IBinder paramIBinder)
    {
        mApplicationObject = paramIBinder;
    }

    public static void wrapperInit(int paramInt, String[] paramArrayOfString)
        throws ZygoteInit.MethodAndArgsCaller
    {
        applicationInit(paramInt, paramArrayOfString);
    }

    public static void wtf(String paramString, Throwable paramThrowable)
    {
        try
        {
            if (ActivityManagerNative.getDefault().handleApplicationWtf(mApplicationObject, paramString, new ApplicationErrorReport.CrashInfo(paramThrowable)))
            {
                Process.killProcess(Process.myPid());
                System.exit(10);
            }
            return;
        }
        catch (Throwable localThrowable)
        {
            while (true)
                Slog.e("AndroidRuntime", "Error reporting WTF", localThrowable);
        }
    }

    public static final void zygoteInit(int paramInt, String[] paramArrayOfString)
        throws ZygoteInit.MethodAndArgsCaller
    {
        redirectLogStreams();
        commonInit();
        nativeZygoteInit();
        applicationInit(paramInt, paramArrayOfString);
    }

    static class Arguments
    {
        String[] startArgs;
        String startClass;

        Arguments(String[] paramArrayOfString)
            throws IllegalArgumentException
        {
            parseArgs(paramArrayOfString);
        }

        private void parseArgs(String[] paramArrayOfString)
            throws IllegalArgumentException
        {
            for (int i = 0; ; i++)
            {
                String str;
                if (i < paramArrayOfString.length)
                {
                    str = paramArrayOfString[i];
                    if (!str.equals("--"))
                        break label42;
                    i++;
                }
                label42: 
                while (!str.startsWith("--"))
                {
                    if (i != paramArrayOfString.length)
                        break;
                    throw new IllegalArgumentException("Missing classname argument to RuntimeInit!");
                }
            }
            int j = i + 1;
            this.startClass = paramArrayOfString[i];
            this.startArgs = new String[paramArrayOfString.length - j];
            System.arraycopy(paramArrayOfString, j, this.startArgs, 0, this.startArgs.length);
        }
    }

    private static class UncaughtHandler
        implements Thread.UncaughtExceptionHandler
    {
        // ERROR //
        public void uncaughtException(Thread paramThread, Throwable paramThrowable)
        {
            // Byte code:
            //     0: invokestatic 23	com/android/internal/os/RuntimeInit:access$000	()Z
            //     3: istore 7
            //     5: iload 7
            //     7: ifeq +15 -> 22
            //     10: invokestatic 29	android/os/Process:myPid	()I
            //     13: invokestatic 33	android/os/Process:killProcess	(I)V
            //     16: bipush 10
            //     18: invokestatic 38	java/lang/System:exit	(I)V
            //     21: return
            //     22: iconst_1
            //     23: invokestatic 42	com/android/internal/os/RuntimeInit:access$002	(Z)Z
            //     26: pop
            //     27: invokestatic 46	com/android/internal/os/RuntimeInit:access$100	()Landroid/os/IBinder;
            //     30: ifnonnull +65 -> 95
            //     33: ldc 48
            //     35: new 50	java/lang/StringBuilder
            //     38: dup
            //     39: invokespecial 51	java/lang/StringBuilder:<init>	()V
            //     42: ldc 53
            //     44: invokevirtual 57	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     47: aload_1
            //     48: invokevirtual 63	java/lang/Thread:getName	()Ljava/lang/String;
            //     51: invokevirtual 57	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     54: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     57: aload_2
            //     58: invokestatic 72	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     61: pop
            //     62: invokestatic 78	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
            //     65: invokestatic 46	com/android/internal/os/RuntimeInit:access$100	()Landroid/os/IBinder;
            //     68: new 80	android/app/ApplicationErrorReport$CrashInfo
            //     71: dup
            //     72: aload_2
            //     73: invokespecial 83	android/app/ApplicationErrorReport$CrashInfo:<init>	(Ljava/lang/Throwable;)V
            //     76: invokeinterface 89 3 0
            //     81: invokestatic 29	android/os/Process:myPid	()I
            //     84: invokestatic 33	android/os/Process:killProcess	(I)V
            //     87: bipush 10
            //     89: invokestatic 38	java/lang/System:exit	(I)V
            //     92: goto -71 -> 21
            //     95: ldc 48
            //     97: new 50	java/lang/StringBuilder
            //     100: dup
            //     101: invokespecial 51	java/lang/StringBuilder:<init>	()V
            //     104: ldc 91
            //     106: invokevirtual 57	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     109: aload_1
            //     110: invokevirtual 63	java/lang/Thread:getName	()Ljava/lang/String;
            //     113: invokevirtual 57	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     116: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     119: aload_2
            //     120: invokestatic 72	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     123: pop
            //     124: goto -62 -> 62
            //     127: astore 4
            //     129: ldc 48
            //     131: ldc 93
            //     133: aload 4
            //     135: invokestatic 72	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     138: pop
            //     139: invokestatic 29	android/os/Process:myPid	()I
            //     142: invokestatic 33	android/os/Process:killProcess	(I)V
            //     145: bipush 10
            //     147: invokestatic 38	java/lang/System:exit	(I)V
            //     150: goto -129 -> 21
            //     153: astore_3
            //     154: invokestatic 29	android/os/Process:myPid	()I
            //     157: invokestatic 33	android/os/Process:killProcess	(I)V
            //     160: bipush 10
            //     162: invokestatic 38	java/lang/System:exit	(I)V
            //     165: aload_3
            //     166: athrow
            //     167: astore 5
            //     169: goto -30 -> 139
            //
            // Exception table:
            //     from	to	target	type
            //     0	5	127	java/lang/Throwable
            //     22	81	127	java/lang/Throwable
            //     95	124	127	java/lang/Throwable
            //     0	5	153	finally
            //     22	81	153	finally
            //     95	124	153	finally
            //     129	139	153	finally
            //     129	139	167	java/lang/Throwable
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String appendMiuiVersion(StringBuilder paramStringBuilder, String paramString)
        {
            String str = Build.VERSION.INCREMENTAL;
            if (str.length() > 0)
            {
                paramStringBuilder.append(" MIUI/");
                paramStringBuilder.append(str);
            }
            return "";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.RuntimeInit
 * JD-Core Version:        0.6.2
 */