package com.android.internal.os;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class HandlerCaller
{
    static final int ARGS_POOL_MAX_SIZE = 10;
    private static final boolean DEBUG = false;
    private static final String TAG = "HandlerCaller";
    SomeArgs mArgsPool;
    int mArgsPoolSize;
    final Callback mCallback;
    public final Context mContext;
    final Handler mH;
    final Looper mMainLooper;

    public HandlerCaller(Context paramContext, Looper paramLooper, Callback paramCallback)
    {
        this.mContext = paramContext;
        this.mMainLooper = paramLooper;
        this.mH = new MyHandler(this.mMainLooper);
        this.mCallback = paramCallback;
    }

    public HandlerCaller(Context paramContext, Callback paramCallback)
    {
        this.mContext = paramContext;
        this.mMainLooper = paramContext.getMainLooper();
        this.mH = new MyHandler(this.mMainLooper);
        this.mCallback = paramCallback;
    }

    public void executeOrSendMessage(Message paramMessage)
    {
        if (Looper.myLooper() == this.mMainLooper)
        {
            this.mCallback.executeMessage(paramMessage);
            paramMessage.recycle();
        }
        while (true)
        {
            return;
            this.mH.sendMessage(paramMessage);
        }
    }

    public boolean hasMessages(int paramInt)
    {
        return this.mH.hasMessages(paramInt);
    }

    public SomeArgs obtainArgs()
    {
        SomeArgs localSomeArgs;
        synchronized (this.mH)
        {
            localSomeArgs = this.mArgsPool;
            if (localSomeArgs != null)
            {
                this.mArgsPool = localSomeArgs.next;
                localSomeArgs.next = null;
                this.mArgsPoolSize = (-1 + this.mArgsPoolSize);
            }
            else
            {
                localSomeArgs = new SomeArgs();
            }
        }
        return localSomeArgs;
    }

    public Message obtainMessage(int paramInt)
    {
        return this.mH.obtainMessage(paramInt);
    }

    public Message obtainMessageBO(int paramInt, boolean paramBoolean, Object paramObject)
    {
        Handler localHandler = this.mH;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
            return localHandler.obtainMessage(paramInt, i, 0, paramObject);
    }

    public Message obtainMessageBOO(int paramInt, boolean paramBoolean, Object paramObject1, Object paramObject2)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.arg1 = paramObject1;
        localSomeArgs.arg2 = paramObject2;
        Handler localHandler = this.mH;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
            return localHandler.obtainMessage(paramInt, i, 0, localSomeArgs);
    }

    public Message obtainMessageI(int paramInt1, int paramInt2)
    {
        return this.mH.obtainMessage(paramInt1, paramInt2, 0);
    }

    public Message obtainMessageII(int paramInt1, int paramInt2, int paramInt3)
    {
        return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3);
    }

    public Message obtainMessageIIII(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.argi1 = paramInt2;
        localSomeArgs.argi2 = paramInt3;
        localSomeArgs.argi3 = paramInt4;
        localSomeArgs.argi4 = paramInt5;
        return this.mH.obtainMessage(paramInt1, 0, 0, localSomeArgs);
    }

    public Message obtainMessageIIIIII(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.argi1 = paramInt2;
        localSomeArgs.argi2 = paramInt3;
        localSomeArgs.argi3 = paramInt4;
        localSomeArgs.argi4 = paramInt5;
        localSomeArgs.argi5 = paramInt6;
        localSomeArgs.argi6 = paramInt7;
        return this.mH.obtainMessage(paramInt1, 0, 0, localSomeArgs);
    }

    public Message obtainMessageIIIIO(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Object paramObject)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.arg1 = paramObject;
        localSomeArgs.argi1 = paramInt2;
        localSomeArgs.argi2 = paramInt3;
        localSomeArgs.argi3 = paramInt4;
        localSomeArgs.argi4 = paramInt5;
        return this.mH.obtainMessage(paramInt1, 0, 0, localSomeArgs);
    }

    public Message obtainMessageIIO(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
        return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject);
    }

    public Message obtainMessageIIOO(int paramInt1, int paramInt2, int paramInt3, Object paramObject1, Object paramObject2)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.arg1 = paramObject1;
        localSomeArgs.arg2 = paramObject2;
        return this.mH.obtainMessage(paramInt1, paramInt2, paramInt3, localSomeArgs);
    }

    public Message obtainMessageIO(int paramInt1, int paramInt2, Object paramObject)
    {
        return this.mH.obtainMessage(paramInt1, paramInt2, 0, paramObject);
    }

    public Message obtainMessageIOO(int paramInt1, int paramInt2, Object paramObject1, Object paramObject2)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.arg1 = paramObject1;
        localSomeArgs.arg2 = paramObject2;
        return this.mH.obtainMessage(paramInt1, paramInt2, 0, localSomeArgs);
    }

    public Message obtainMessageO(int paramInt, Object paramObject)
    {
        return this.mH.obtainMessage(paramInt, 0, 0, paramObject);
    }

    public Message obtainMessageOO(int paramInt, Object paramObject1, Object paramObject2)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.arg1 = paramObject1;
        localSomeArgs.arg2 = paramObject2;
        return this.mH.obtainMessage(paramInt, 0, 0, localSomeArgs);
    }

    public Message obtainMessageOOO(int paramInt, Object paramObject1, Object paramObject2, Object paramObject3)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.arg1 = paramObject1;
        localSomeArgs.arg2 = paramObject2;
        localSomeArgs.arg3 = paramObject3;
        return this.mH.obtainMessage(paramInt, 0, 0, localSomeArgs);
    }

    public Message obtainMessageOOOO(int paramInt, Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4)
    {
        SomeArgs localSomeArgs = obtainArgs();
        localSomeArgs.arg1 = paramObject1;
        localSomeArgs.arg2 = paramObject2;
        localSomeArgs.arg3 = paramObject3;
        localSomeArgs.arg4 = paramObject4;
        return this.mH.obtainMessage(paramInt, 0, 0, localSomeArgs);
    }

    public void recycleArgs(SomeArgs paramSomeArgs)
    {
        synchronized (this.mH)
        {
            if (this.mArgsPoolSize < 10)
            {
                paramSomeArgs.next = this.mArgsPool;
                this.mArgsPool = paramSomeArgs;
                this.mArgsPoolSize = (1 + this.mArgsPoolSize);
            }
            return;
        }
    }

    public void removeMessages(int paramInt)
    {
        this.mH.removeMessages(paramInt);
    }

    public void removeMessages(int paramInt, Object paramObject)
    {
        this.mH.removeMessages(paramInt, paramObject);
    }

    public void sendMessage(Message paramMessage)
    {
        this.mH.sendMessage(paramMessage);
    }

    public static abstract interface Callback
    {
        public abstract void executeMessage(Message paramMessage);
    }

    class MyHandler extends Handler
    {
        MyHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            HandlerCaller.this.mCallback.executeMessage(paramMessage);
        }
    }

    public static class SomeArgs
    {
        public Object arg1;
        public Object arg2;
        public Object arg3;
        public Object arg4;
        public int argi1;
        public int argi2;
        public int argi3;
        public int argi4;
        public int argi5;
        public int argi6;
        SomeArgs next;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.HandlerCaller
 * JD-Core Version:        0.6.2
 */