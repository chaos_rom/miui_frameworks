package com.android.internal.os;

import android.os.Process;
import android.os.SystemClock;
import android.util.Slog;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;

public class ProcessStats
{
    private static final boolean DEBUG = false;
    private static final int[] LOAD_AVERAGE_FORMAT = arrayOfInt4;
    private static final int[] PROCESS_FULL_STATS_FORMAT;
    static final int PROCESS_FULL_STAT_MAJOR_FAULTS = 2;
    static final int PROCESS_FULL_STAT_MINOR_FAULTS = 1;
    static final int PROCESS_FULL_STAT_STIME = 4;
    static final int PROCESS_FULL_STAT_UTIME = 3;
    static final int PROCESS_FULL_STAT_VSIZE = 5;
    private static final int[] PROCESS_STATS_FORMAT;
    static final int PROCESS_STAT_MAJOR_FAULTS = 1;
    static final int PROCESS_STAT_MINOR_FAULTS = 0;
    static final int PROCESS_STAT_STIME = 3;
    static final int PROCESS_STAT_UTIME = 2;
    private static final int[] SYSTEM_CPU_FORMAT;
    private static final String TAG = "ProcessStats";
    private static final boolean localLOGV;
    private static final Comparator<Stats> sLoadComparator = new Comparator()
    {
        public final int compare(ProcessStats.Stats paramAnonymousStats1, ProcessStats.Stats paramAnonymousStats2)
        {
            int i = -1;
            int j = paramAnonymousStats1.rel_utime + paramAnonymousStats1.rel_stime;
            int k = paramAnonymousStats2.rel_utime + paramAnonymousStats2.rel_stime;
            if (j != k)
                if (j <= k);
            while (true)
            {
                return i;
                i = 1;
                continue;
                if (paramAnonymousStats1.added != paramAnonymousStats2.added)
                {
                    if (!paramAnonymousStats1.added)
                        i = 1;
                }
                else if (paramAnonymousStats1.removed != paramAnonymousStats2.removed)
                {
                    if (!paramAnonymousStats1.added)
                        i = 1;
                }
                else
                    i = 0;
            }
        }
    };
    private long mBaseIdleTime;
    private long mBaseIoWaitTime;
    private long mBaseIrqTime;
    private long mBaseSoftIrqTime;
    private long mBaseSystemTime;
    private long mBaseUserTime;
    private byte[] mBuffer = new byte[256];
    private long[] mCpuSpeedTimes;
    private long[] mCpuSpeeds;
    private int[] mCurPids;
    private int[] mCurThreadPids;
    private long mCurrentSampleRealTime;
    private long mCurrentSampleTime;
    private boolean mFirst = true;
    private final boolean mIncludeThreads;
    private long mLastSampleRealTime;
    private long mLastSampleTime;
    private float mLoad1 = 0.0F;
    private float mLoad15 = 0.0F;
    private float mLoad5 = 0.0F;
    private final float[] mLoadAverageData = new float[3];
    private final ArrayList<Stats> mProcStats = new ArrayList();
    private final long[] mProcessFullStatsData = new long[6];
    private final String[] mProcessFullStatsStringData = new String[6];
    private final long[] mProcessStatsData = new long[4];
    private long[] mRelCpuSpeedTimes;
    private int mRelIdleTime;
    private int mRelIoWaitTime;
    private int mRelIrqTime;
    private int mRelSoftIrqTime;
    private int mRelSystemTime;
    private int mRelUserTime;
    private final long[] mSinglePidStatsData = new long[4];
    private final long[] mSystemCpuData = new long[7];
    private final ArrayList<Stats> mWorkingProcs = new ArrayList();
    private boolean mWorkingProcsSorted;

    static
    {
        int[] arrayOfInt1 = new int[15];
        arrayOfInt1[0] = 32;
        arrayOfInt1[1] = 544;
        arrayOfInt1[2] = 32;
        arrayOfInt1[3] = 32;
        arrayOfInt1[4] = 32;
        arrayOfInt1[5] = 32;
        arrayOfInt1[6] = 32;
        arrayOfInt1[7] = 32;
        arrayOfInt1[8] = 32;
        arrayOfInt1[9] = 8224;
        arrayOfInt1[10] = 32;
        arrayOfInt1[11] = 8224;
        arrayOfInt1[12] = 32;
        arrayOfInt1[13] = 8224;
        arrayOfInt1[14] = 8224;
        PROCESS_STATS_FORMAT = arrayOfInt1;
        int[] arrayOfInt2 = new int[22];
        arrayOfInt2[0] = 32;
        arrayOfInt2[1] = 4640;
        arrayOfInt2[2] = 32;
        arrayOfInt2[3] = 32;
        arrayOfInt2[4] = 32;
        arrayOfInt2[5] = 32;
        arrayOfInt2[6] = 32;
        arrayOfInt2[7] = 32;
        arrayOfInt2[8] = 32;
        arrayOfInt2[9] = 8224;
        arrayOfInt2[10] = 32;
        arrayOfInt2[11] = 8224;
        arrayOfInt2[12] = 32;
        arrayOfInt2[13] = 8224;
        arrayOfInt2[14] = 8224;
        arrayOfInt2[15] = 32;
        arrayOfInt2[16] = 32;
        arrayOfInt2[17] = 32;
        arrayOfInt2[18] = 32;
        arrayOfInt2[19] = 32;
        arrayOfInt2[20] = 32;
        arrayOfInt2[21] = 8224;
        PROCESS_FULL_STATS_FORMAT = arrayOfInt2;
        int[] arrayOfInt3 = new int[8];
        arrayOfInt3[0] = 288;
        arrayOfInt3[1] = 8224;
        arrayOfInt3[2] = 8224;
        arrayOfInt3[3] = 8224;
        arrayOfInt3[4] = 8224;
        arrayOfInt3[5] = 8224;
        arrayOfInt3[6] = 8224;
        arrayOfInt3[7] = 8224;
        SYSTEM_CPU_FORMAT = arrayOfInt3;
        int[] arrayOfInt4 = new int[3];
        arrayOfInt4[0] = 16416;
        arrayOfInt4[1] = 16416;
        arrayOfInt4[2] = 16416;
    }

    public ProcessStats(boolean paramBoolean)
    {
        this.mIncludeThreads = paramBoolean;
    }

    private int[] collectStats(String paramString, int paramInt, boolean paramBoolean, int[] paramArrayOfInt, ArrayList<Stats> paramArrayList)
    {
        int[] arrayOfInt = Process.getPids(paramString, paramArrayOfInt);
        if (arrayOfInt == null);
        int j;
        int k;
        int m;
        int n;
        for (int i = 0; ; i = arrayOfInt.length)
        {
            j = paramArrayList.size();
            k = 0;
            m = 0;
            if (m < i)
            {
                n = arrayOfInt[m];
                if (n >= 0)
                    break;
            }
            while (k < j)
            {
                Stats localStats1 = (Stats)paramArrayList.get(k);
                localStats1.rel_utime = 0;
                localStats1.rel_stime = 0;
                localStats1.rel_minfaults = 0;
                localStats1.rel_majfaults = 0;
                localStats1.removed = true;
                localStats1.working = true;
                paramArrayList.remove(k);
                j--;
            }
        }
        Stats localStats2;
        label144: long l1;
        long[] arrayOfLong2;
        if (k < j)
        {
            localStats2 = (Stats)paramArrayList.get(k);
            if ((localStats2 == null) || (localStats2.pid != n))
                break label512;
            localStats2.added = false;
            localStats2.working = false;
            k++;
            if (localStats2.interesting)
            {
                l1 = SystemClock.uptimeMillis();
                arrayOfLong2 = this.mProcessStatsData;
                if (Process.readProcFile(localStats2.statFile.toString(), PROCESS_STATS_FORMAT, null, arrayOfLong2, null))
                    break label226;
            }
        }
        while (true)
        {
            m++;
            break;
            localStats2 = null;
            break label144;
            label226: long l2 = arrayOfLong2[0];
            long l3 = arrayOfLong2[1];
            long l4 = arrayOfLong2[2];
            long l5 = arrayOfLong2[3];
            if ((l4 == localStats2.base_utime) && (l5 == localStats2.base_stime))
            {
                localStats2.rel_utime = 0;
                localStats2.rel_stime = 0;
                localStats2.rel_minfaults = 0;
                localStats2.rel_majfaults = 0;
                if (localStats2.active)
                    localStats2.active = false;
            }
            else
            {
                if (!localStats2.active)
                    localStats2.active = true;
                if (paramInt < 0)
                {
                    String str = localStats2.cmdlineFile;
                    getName(localStats2, str);
                    if (localStats2.threadStats != null)
                        this.mCurThreadPids = collectStats(localStats2.threadsDir, n, false, this.mCurThreadPids, localStats2.threadStats);
                }
                long l6 = l1 - localStats2.base_uptime;
                localStats2.rel_uptime = l6;
                localStats2.base_uptime = l1;
                int i1 = (int)(l4 - localStats2.base_utime);
                localStats2.rel_utime = i1;
                int i2 = (int)(l5 - localStats2.base_stime);
                localStats2.rel_stime = i2;
                localStats2.base_utime = l4;
                localStats2.base_stime = l5;
                int i3 = (int)(l2 - localStats2.base_minfaults);
                localStats2.rel_minfaults = i3;
                int i4 = (int)(l3 - localStats2.base_majfaults);
                localStats2.rel_majfaults = i4;
                localStats2.base_minfaults = l2;
                localStats2.base_majfaults = l3;
                localStats2.working = true;
                continue;
                label512: if ((localStats2 == null) || (localStats2.pid > n))
                {
                    Stats localStats3 = new Stats(n, paramInt, this.mIncludeThreads);
                    paramArrayList.add(k, localStats3);
                    k++;
                    j++;
                    String[] arrayOfString = this.mProcessFullStatsStringData;
                    long[] arrayOfLong1 = this.mProcessFullStatsData;
                    localStats3.base_uptime = SystemClock.uptimeMillis();
                    if (Process.readProcFile(localStats3.statFile.toString(), PROCESS_FULL_STATS_FORMAT, arrayOfString, arrayOfLong1, null))
                    {
                        localStats3.interesting = true;
                        localStats3.baseName = arrayOfString[0];
                        localStats3.base_minfaults = arrayOfLong1[1];
                        localStats3.base_majfaults = arrayOfLong1[2];
                        localStats3.base_utime = arrayOfLong1[3];
                        localStats3.base_stime = arrayOfLong1[4];
                        label651: if (paramInt >= 0)
                            break label811;
                        getName(localStats3, localStats3.cmdlineFile);
                        if (localStats3.threadStats != null)
                            this.mCurThreadPids = collectStats(localStats3.threadsDir, n, true, this.mCurThreadPids, localStats3.threadStats);
                    }
                    while (true)
                    {
                        localStats3.rel_utime = 0;
                        localStats3.rel_stime = 0;
                        localStats3.rel_minfaults = 0;
                        localStats3.rel_majfaults = 0;
                        localStats3.added = true;
                        if ((paramBoolean) || (!localStats3.interesting))
                            break;
                        localStats3.working = true;
                        break;
                        Slog.w("ProcessStats", "Skipping unknown process pid " + n);
                        localStats3.baseName = "<unknown>";
                        localStats3.base_stime = 0L;
                        localStats3.base_utime = 0L;
                        localStats3.base_majfaults = 0L;
                        localStats3.base_minfaults = 0L;
                        break label651;
                        label811: if (localStats3.interesting)
                        {
                            localStats3.name = localStats3.baseName;
                            localStats3.nameWidth = onMeasureProcessName(localStats3.name);
                        }
                    }
                }
                localStats2.rel_utime = 0;
                localStats2.rel_stime = 0;
                localStats2.rel_minfaults = 0;
                localStats2.rel_majfaults = 0;
                localStats2.removed = true;
                localStats2.working = true;
                paramArrayList.remove(k);
                j--;
                m--;
            }
        }
        return arrayOfInt;
    }

    private long[] getCpuSpeedTimes(long[] paramArrayOfLong)
    {
        long[] arrayOfLong1 = paramArrayOfLong;
        long[] arrayOfLong2 = this.mCpuSpeeds;
        if (paramArrayOfLong == null)
        {
            arrayOfLong1 = new long[20];
            arrayOfLong2 = new long[20];
        }
        int i = 0;
        String str1 = readFile("/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state", '\000');
        StringTokenizer localStringTokenizer;
        if (str1 != null)
            localStringTokenizer = new StringTokenizer(str1, "\n ");
        while (true)
        {
            String str2;
            if (localStringTokenizer.hasMoreElements())
                str2 = localStringTokenizer.nextToken();
            try
            {
                arrayOfLong2[i] = Long.parseLong(str2);
                arrayOfLong1[i] = Long.parseLong(localStringTokenizer.nextToken());
                i++;
                if (i == 20)
                {
                    if (paramArrayOfLong == null)
                    {
                        paramArrayOfLong = new long[i];
                        this.mCpuSpeeds = new long[i];
                        System.arraycopy(arrayOfLong2, 0, this.mCpuSpeeds, 0, i);
                        System.arraycopy(arrayOfLong1, 0, paramArrayOfLong, 0, i);
                    }
                    return paramArrayOfLong;
                }
            }
            catch (NumberFormatException localNumberFormatException)
            {
                Slog.i("ProcessStats", "Unable to parse time_in_state");
            }
        }
    }

    private void getName(Stats paramStats, String paramString)
    {
        Object localObject = paramStats.name;
        if ((paramStats.name == null) || (paramStats.name.equals("app_process")) || (paramStats.name.equals("<pre-initialized>")))
        {
            String str = readFile(paramString, '\000');
            if ((str != null) && (str.length() > 1))
            {
                localObject = str;
                int i = ((String)localObject).lastIndexOf("/");
                if ((i > 0) && (i < -1 + ((String)localObject).length()))
                    localObject = ((String)localObject).substring(i + 1);
            }
            if (localObject == null)
                localObject = paramStats.baseName;
        }
        if ((paramStats.name == null) || (!((String)localObject).equals(paramStats.name)))
        {
            paramStats.name = ((String)localObject);
            paramStats.nameWidth = onMeasureProcessName(paramStats.name);
        }
    }

    private void printProcessCPU(PrintWriter paramPrintWriter, String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9)
    {
        paramPrintWriter.print(paramString1);
        if (paramInt2 == 0)
            paramInt2 = 1;
        printRatio(paramPrintWriter, paramInt7 + (paramInt6 + (paramInt5 + (paramInt3 + paramInt4))), paramInt2);
        paramPrintWriter.print("% ");
        if (paramInt1 >= 0)
        {
            paramPrintWriter.print(paramInt1);
            paramPrintWriter.print("/");
        }
        paramPrintWriter.print(paramString2);
        paramPrintWriter.print(": ");
        printRatio(paramPrintWriter, paramInt3, paramInt2);
        paramPrintWriter.print("% user + ");
        printRatio(paramPrintWriter, paramInt4, paramInt2);
        paramPrintWriter.print("% kernel");
        if (paramInt5 > 0)
        {
            paramPrintWriter.print(" + ");
            printRatio(paramPrintWriter, paramInt5, paramInt2);
            paramPrintWriter.print("% iowait");
        }
        if (paramInt6 > 0)
        {
            paramPrintWriter.print(" + ");
            printRatio(paramPrintWriter, paramInt6, paramInt2);
            paramPrintWriter.print("% irq");
        }
        if (paramInt7 > 0)
        {
            paramPrintWriter.print(" + ");
            printRatio(paramPrintWriter, paramInt7, paramInt2);
            paramPrintWriter.print("% softirq");
        }
        if ((paramInt8 > 0) || (paramInt9 > 0))
        {
            paramPrintWriter.print(" / faults:");
            if (paramInt8 > 0)
            {
                paramPrintWriter.print(" ");
                paramPrintWriter.print(paramInt8);
                paramPrintWriter.print(" minor");
            }
            if (paramInt9 > 0)
            {
                paramPrintWriter.print(" ");
                paramPrintWriter.print(paramInt9);
                paramPrintWriter.print(" major");
            }
        }
        paramPrintWriter.println();
    }

    private void printRatio(PrintWriter paramPrintWriter, long paramLong1, long paramLong2)
    {
        long l1 = 1000L * paramLong1 / paramLong2;
        long l2 = l1 / 10L;
        paramPrintWriter.print(l2);
        if (l2 < 10L)
        {
            long l3 = l1 - l2 * 10L;
            if (l3 != 0L)
            {
                paramPrintWriter.print('.');
                paramPrintWriter.print(l3);
            }
        }
    }

    // ERROR //
    private String readFile(String paramString, char paramChar)
    {
        // Byte code:
        //     0: invokestatic 401	android/os/StrictMode:allowThreadDiskReads	()Landroid/os/StrictMode$ThreadPolicy;
        //     3: astore_3
        //     4: aconst_null
        //     5: astore 4
        //     7: new 403	java/io/FileInputStream
        //     10: dup
        //     11: aload_1
        //     12: invokespecial 405	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     15: astore 5
        //     17: aload 5
        //     19: aload_0
        //     20: getfield 132	com/android/internal/os/ProcessStats:mBuffer	[B
        //     23: invokevirtual 409	java/io/FileInputStream:read	([B)I
        //     26: istore 13
        //     28: aload 5
        //     30: invokevirtual 412	java/io/FileInputStream:close	()V
        //     33: iload 13
        //     35: ifle +63 -> 98
        //     38: iconst_0
        //     39: istore 15
        //     41: iload 15
        //     43: iload 13
        //     45: if_icmpge +14 -> 59
        //     48: aload_0
        //     49: getfield 132	com/android/internal/os/ProcessStats:mBuffer	[B
        //     52: iload 15
        //     54: baload
        //     55: iload_2
        //     56: if_icmpne +36 -> 92
        //     59: new 107	java/lang/String
        //     62: dup
        //     63: aload_0
        //     64: getfield 132	com/android/internal/os/ProcessStats:mBuffer	[B
        //     67: iconst_0
        //     68: iload 15
        //     70: invokespecial 415	java/lang/String:<init>	([BII)V
        //     73: astore 7
        //     75: aload 5
        //     77: ifnull +8 -> 85
        //     80: aload 5
        //     82: invokevirtual 412	java/io/FileInputStream:close	()V
        //     85: aload_3
        //     86: invokestatic 419	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     89: aload 7
        //     91: areturn
        //     92: iinc 15 1
        //     95: goto -54 -> 41
        //     98: aload 5
        //     100: ifnull +8 -> 108
        //     103: aload 5
        //     105: invokevirtual 412	java/io/FileInputStream:close	()V
        //     108: aload_3
        //     109: invokestatic 419	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     112: aconst_null
        //     113: astore 7
        //     115: goto -26 -> 89
        //     118: astore 18
        //     120: aload 4
        //     122: ifnull +8 -> 130
        //     125: aload 4
        //     127: invokevirtual 412	java/io/FileInputStream:close	()V
        //     130: aload_3
        //     131: invokestatic 419	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     134: goto -22 -> 112
        //     137: astore 17
        //     139: aload 4
        //     141: ifnull +8 -> 149
        //     144: aload 4
        //     146: invokevirtual 412	java/io/FileInputStream:close	()V
        //     149: aload_3
        //     150: invokestatic 419	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     153: goto -41 -> 112
        //     156: astore 11
        //     158: aload 4
        //     160: ifnull +8 -> 168
        //     163: aload 4
        //     165: invokevirtual 412	java/io/FileInputStream:close	()V
        //     168: aload_3
        //     169: invokestatic 419	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
        //     172: aload 11
        //     174: athrow
        //     175: astore 16
        //     177: goto -92 -> 85
        //     180: astore 14
        //     182: goto -74 -> 108
        //     185: astore 8
        //     187: goto -57 -> 130
        //     190: astore 10
        //     192: goto -43 -> 149
        //     195: astore 12
        //     197: goto -29 -> 168
        //     200: astore 11
        //     202: aload 5
        //     204: astore 4
        //     206: goto -48 -> 158
        //     209: astore 9
        //     211: aload 5
        //     213: astore 4
        //     215: goto -76 -> 139
        //     218: astore 6
        //     220: aload 5
        //     222: astore 4
        //     224: goto -104 -> 120
        //
        // Exception table:
        //     from	to	target	type
        //     7	17	118	java/io/FileNotFoundException
        //     7	17	137	java/io/IOException
        //     7	17	156	finally
        //     80	85	175	java/io/IOException
        //     103	108	180	java/io/IOException
        //     125	130	185	java/io/IOException
        //     144	149	190	java/io/IOException
        //     163	168	195	java/io/IOException
        //     17	75	200	finally
        //     17	75	209	java/io/IOException
        //     17	75	218	java/io/FileNotFoundException
    }

    final void buildWorkingProcs()
    {
        if (!this.mWorkingProcsSorted)
        {
            this.mWorkingProcs.clear();
            int i = this.mProcStats.size();
            for (int j = 0; j < i; j++)
            {
                Stats localStats1 = (Stats)this.mProcStats.get(j);
                if (localStats1.working)
                {
                    this.mWorkingProcs.add(localStats1);
                    if ((localStats1.threadStats != null) && (localStats1.threadStats.size() > 1))
                    {
                        localStats1.workingThreads.clear();
                        int k = localStats1.threadStats.size();
                        for (int m = 0; m < k; m++)
                        {
                            Stats localStats2 = (Stats)localStats1.threadStats.get(m);
                            if (localStats2.working)
                                localStats1.workingThreads.add(localStats2);
                        }
                        Collections.sort(localStats1.workingThreads, sLoadComparator);
                    }
                }
            }
            Collections.sort(this.mWorkingProcs, sLoadComparator);
            this.mWorkingProcsSorted = true;
        }
    }

    public final int countStats()
    {
        return this.mProcStats.size();
    }

    public final int countWorkingStats()
    {
        buildWorkingProcs();
        return this.mWorkingProcs.size();
    }

    public long getCpuTimeForPid(int paramInt)
    {
        String str = "/proc/" + paramInt + "/stat";
        long[] arrayOfLong = this.mSinglePidStatsData;
        if (Process.readProcFile(str, PROCESS_STATS_FORMAT, null, arrayOfLong, null));
        for (long l = arrayOfLong[2] + arrayOfLong[3]; ; l = 0L)
            return l;
    }

    public long[] getLastCpuSpeedTimes()
    {
        if (this.mCpuSpeedTimes == null)
        {
            this.mCpuSpeedTimes = getCpuSpeedTimes(null);
            this.mRelCpuSpeedTimes = new long[this.mCpuSpeedTimes.length];
            for (int j = 0; j < this.mCpuSpeedTimes.length; j++)
                this.mRelCpuSpeedTimes[j] = 1L;
        }
        getCpuSpeedTimes(this.mRelCpuSpeedTimes);
        for (int i = 0; i < this.mCpuSpeedTimes.length; i++)
        {
            long l = this.mRelCpuSpeedTimes[i];
            long[] arrayOfLong = this.mRelCpuSpeedTimes;
            arrayOfLong[i] -= this.mCpuSpeedTimes[i];
            this.mCpuSpeedTimes[i] = l;
        }
        return this.mRelCpuSpeedTimes;
    }

    public final int getLastIdleTime()
    {
        return this.mRelIdleTime;
    }

    public final int getLastIoWaitTime()
    {
        return this.mRelIoWaitTime;
    }

    public final int getLastIrqTime()
    {
        return this.mRelIrqTime;
    }

    public final int getLastSoftIrqTime()
    {
        return this.mRelSoftIrqTime;
    }

    public final int getLastSystemTime()
    {
        return this.mRelSystemTime;
    }

    public final int getLastUserTime()
    {
        return this.mRelUserTime;
    }

    public final Stats getStats(int paramInt)
    {
        return (Stats)this.mProcStats.get(paramInt);
    }

    public final float getTotalCpuPercent()
    {
        int i = this.mRelUserTime + this.mRelSystemTime + this.mRelIrqTime + this.mRelIdleTime;
        if (i <= 0);
        for (float f = 0.0F; ; f = 100.0F * (this.mRelUserTime + this.mRelSystemTime + this.mRelIrqTime) / i)
            return f;
    }

    public final Stats getWorkingStats(int paramInt)
    {
        return (Stats)this.mWorkingProcs.get(paramInt);
    }

    public void init()
    {
        this.mFirst = true;
        update();
    }

    public void onLoadChanged(float paramFloat1, float paramFloat2, float paramFloat3)
    {
    }

    public int onMeasureProcessName(String paramString)
    {
        return 0;
    }

    public final String printCurrentLoad()
    {
        StringWriter localStringWriter = new StringWriter();
        PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
        localPrintWriter.print("Load: ");
        localPrintWriter.print(this.mLoad1);
        localPrintWriter.print(" / ");
        localPrintWriter.print(this.mLoad5);
        localPrintWriter.print(" / ");
        localPrintWriter.println(this.mLoad15);
        return localStringWriter.toString();
    }

    public final String printCurrentState(long paramLong)
    {
        buildWorkingProcs();
        StringWriter localStringWriter = new StringWriter();
        PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
        localPrintWriter.print("CPU usage from ");
        long l3;
        label117: int i;
        int j;
        if (paramLong > this.mLastSampleTime)
        {
            localPrintWriter.print(paramLong - this.mLastSampleTime);
            localPrintWriter.print("ms to ");
            localPrintWriter.print(paramLong - this.mCurrentSampleTime);
            localPrintWriter.print("ms ago");
            long l1 = this.mCurrentSampleTime - this.mLastSampleTime;
            long l2 = this.mCurrentSampleRealTime - this.mLastSampleRealTime;
            if (l2 <= 0L)
                break label445;
            l3 = 100L * l1 / l2;
            if (l3 != 100L)
            {
                localPrintWriter.print(" with ");
                localPrintWriter.print(l3);
                localPrintWriter.print("% awake");
            }
            localPrintWriter.println(":");
            i = this.mRelUserTime + this.mRelSystemTime + this.mRelIoWaitTime + this.mRelIrqTime + this.mRelSoftIrqTime + this.mRelIdleTime;
            j = this.mWorkingProcs.size();
        }
        for (int k = 0; ; k++)
        {
            if (k >= j)
                break label505;
            Stats localStats1 = (Stats)this.mWorkingProcs.get(k);
            String str1;
            label234: int n;
            label317: Stats localStats2;
            String str2;
            if (localStats1.added)
            {
                str1 = " +";
                printProcessCPU(localPrintWriter, str1, localStats1.pid, localStats1.name, (int)(5L + localStats1.rel_uptime) / 10, localStats1.rel_utime, localStats1.rel_stime, 0, 0, 0, localStats1.rel_minfaults, localStats1.rel_majfaults);
                if ((localStats1.removed) || (localStats1.workingThreads == null))
                    continue;
                int m = localStats1.workingThreads.size();
                n = 0;
                if (n >= m)
                    continue;
                localStats2 = (Stats)localStats1.workingThreads.get(n);
                if (!localStats2.added)
                    break label475;
                str2 = "     +";
            }
            while (true)
            {
                printProcessCPU(localPrintWriter, str2, localStats2.pid, localStats2.name, (int)(5L + localStats1.rel_uptime) / 10, localStats2.rel_utime, localStats2.rel_stime, 0, 0, 0, 0, 0);
                n++;
                break label317;
                localPrintWriter.print(this.mLastSampleTime - paramLong);
                localPrintWriter.print("ms to ");
                localPrintWriter.print(this.mCurrentSampleTime - paramLong);
                localPrintWriter.print("ms later");
                break;
                label445: l3 = 0L;
                break label117;
                if (localStats1.removed)
                {
                    str1 = " -";
                    break label234;
                }
                str1 = "    ";
                break label234;
                label475: if (localStats2.removed)
                    str2 = "     -";
                else
                    str2 = "        ";
            }
        }
        label505: printProcessCPU(localPrintWriter, "", -1, "TOTAL", i, this.mRelUserTime, this.mRelSystemTime, this.mRelIoWaitTime, this.mRelIrqTime, this.mRelSoftIrqTime, 0, 0);
        return localStringWriter.toString();
    }

    public void update()
    {
        this.mLastSampleTime = this.mCurrentSampleTime;
        this.mCurrentSampleTime = SystemClock.uptimeMillis();
        this.mLastSampleRealTime = this.mCurrentSampleRealTime;
        this.mCurrentSampleRealTime = SystemClock.elapsedRealtime();
        long[] arrayOfLong = this.mSystemCpuData;
        if (Process.readProcFile("/proc/stat", SYSTEM_CPU_FORMAT, null, arrayOfLong, null))
        {
            long l1 = arrayOfLong[0] + arrayOfLong[1];
            long l2 = arrayOfLong[2];
            long l3 = arrayOfLong[3];
            long l4 = arrayOfLong[4];
            long l5 = arrayOfLong[5];
            long l6 = arrayOfLong[6];
            this.mRelUserTime = ((int)(l1 - this.mBaseUserTime));
            this.mRelSystemTime = ((int)(l2 - this.mBaseSystemTime));
            this.mRelIoWaitTime = ((int)(l4 - this.mBaseIoWaitTime));
            this.mRelIrqTime = ((int)(l5 - this.mBaseIrqTime));
            this.mRelSoftIrqTime = ((int)(l6 - this.mBaseSoftIrqTime));
            this.mRelIdleTime = ((int)(l3 - this.mBaseIdleTime));
            this.mBaseUserTime = l1;
            this.mBaseSystemTime = l2;
            this.mBaseIoWaitTime = l4;
            this.mBaseIrqTime = l5;
            this.mBaseSoftIrqTime = l6;
            this.mBaseIdleTime = l3;
        }
        this.mCurPids = collectStats("/proc", -1, this.mFirst, this.mCurPids, this.mProcStats);
        float[] arrayOfFloat = this.mLoadAverageData;
        if (Process.readProcFile("/proc/loadavg", LOAD_AVERAGE_FORMAT, null, null, arrayOfFloat))
        {
            float f1 = arrayOfFloat[0];
            float f2 = arrayOfFloat[1];
            float f3 = arrayOfFloat[2];
            if ((f1 != this.mLoad1) || (f2 != this.mLoad5) || (f3 != this.mLoad15))
            {
                this.mLoad1 = f1;
                this.mLoad5 = f2;
                this.mLoad15 = f3;
                onLoadChanged(f1, f2, f3);
            }
        }
        this.mWorkingProcsSorted = false;
        this.mFirst = false;
    }

    public static class Stats
    {
        public boolean active;
        public boolean added;
        public String baseName;
        public long base_majfaults;
        public long base_minfaults;
        public long base_stime;
        public long base_uptime;
        public long base_utime;
        final String cmdlineFile;
        public boolean interesting;
        public String name;
        public int nameWidth;
        public final int pid;
        public int rel_majfaults;
        public int rel_minfaults;
        public int rel_stime;
        public long rel_uptime;
        public int rel_utime;
        public boolean removed;
        final String statFile;
        final ArrayList<Stats> threadStats;
        final String threadsDir;
        public boolean working;
        final ArrayList<Stats> workingThreads;

        Stats(int paramInt1, int paramInt2, boolean paramBoolean)
        {
            this.pid = paramInt1;
            if (paramInt2 < 0)
            {
                File localFile = new File("/proc", Integer.toString(this.pid));
                this.statFile = new File(localFile, "stat").toString();
                this.cmdlineFile = new File(localFile, "cmdline").toString();
                this.threadsDir = new File(localFile, "task").toString();
                if (paramBoolean)
                {
                    this.threadStats = new ArrayList();
                    this.workingThreads = new ArrayList();
                }
            }
            while (true)
            {
                return;
                this.threadStats = null;
                this.workingThreads = null;
                continue;
                this.statFile = new File(new File(new File(new File("/proc", Integer.toString(paramInt2)), "task"), Integer.toString(this.pid)), "stat").toString();
                this.cmdlineFile = null;
                this.threadsDir = null;
                this.threadStats = null;
                this.workingThreads = null;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.ProcessStats
 * JD-Core Version:        0.6.2
 */