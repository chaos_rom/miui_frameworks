package com.android.internal.os;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.LocalServerSocket;
import android.os.Debug;
import android.os.FileUtils;
import android.os.Process;
import android.os.SystemClock;
import android.util.EventLog;
import android.util.Log;
import dalvik.system.VMRuntime;
import dalvik.system.Zygote;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class ZygoteInit
{
    private static final String ANDROID_SOCKET_ENV = "ANDROID_SOCKET_zygote";
    static final int GC_LOOP_COUNT = 10;
    private static final int LOG_BOOT_PROGRESS_PRELOAD_END = 3030;
    private static final int LOG_BOOT_PROGRESS_PRELOAD_START = 3020;
    private static final String PRELOADED_CLASSES = "preloaded-classes";
    private static final int PRELOAD_GC_THRESHOLD = 50000;
    private static final boolean PRELOAD_RESOURCES = true;
    private static final int ROOT_GID = 0;
    private static final int ROOT_UID = 0;
    private static final String TAG = "Zygote";
    private static final int UNPRIVILEGED_GID = 9999;
    private static final int UNPRIVILEGED_UID = 9999;
    public static final String USAGE_STRING = " <\"start-system-server\"|\"\" for startSystemServer>";
    private static final boolean ZYGOTE_FORK_MODE;
    private static Resources mResources;
    private static LocalServerSocket sServerSocket;

    private static ZygoteConnection acceptCommandPeer()
    {
        try
        {
            ZygoteConnection localZygoteConnection = new ZygoteConnection(sServerSocket.accept());
            return localZygoteConnection;
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException("IOException during accept()", localIOException);
        }
    }

    static native long capgetPermitted(int paramInt)
        throws IOException;

    static void closeServerSocket()
    {
        try
        {
            if (sServerSocket != null)
                sServerSocket.close();
            sServerSocket = null;
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.e("Zygote", "Zygote:    error closing sockets", localIOException);
        }
    }

    static native FileDescriptor createFileDescriptor(int paramInt)
        throws IOException;

    static void gc()
    {
        VMRuntime localVMRuntime = VMRuntime.getRuntime();
        System.gc();
        localVMRuntime.runFinalizationSync();
        System.gc();
        localVMRuntime.runFinalizationSync();
        System.gc();
        localVMRuntime.runFinalizationSync();
    }

    static native int getpgid(int paramInt)
        throws IOException;

    private static void handleSystemServerProcess(ZygoteConnection.Arguments paramArguments)
        throws ZygoteInit.MethodAndArgsCaller
    {
        closeServerSocket();
        FileUtils.setUMask(63);
        if (paramArguments.niceName != null)
            Process.setArgV0(paramArguments.niceName);
        if (paramArguments.invokeWith != null)
            WrapperInit.execApplication(paramArguments.invokeWith, paramArguments.niceName, paramArguments.targetSdkVersion, null, paramArguments.remainingArgs);
        while (true)
        {
            return;
            RuntimeInit.zygoteInit(paramArguments.targetSdkVersion, paramArguments.remainingArgs);
        }
    }

    // ERROR //
    static void invokeStaticMain(java.lang.ClassLoader paramClassLoader, String paramString, String[] paramArrayOfString)
        throws ZygoteInit.MethodAndArgsCaller
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokevirtual 159	java/lang/ClassLoader:loadClass	(Ljava/lang/String;)Ljava/lang/Class;
        //     5: astore 4
        //     7: iconst_1
        //     8: anewarray 161	java/lang/Class
        //     11: astore 7
        //     13: aload 7
        //     15: iconst_0
        //     16: ldc 162
        //     18: aastore
        //     19: aload 4
        //     21: ldc 164
        //     23: aload 7
        //     25: invokevirtual 168	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //     28: astore 8
        //     30: aload 8
        //     32: invokevirtual 174	java/lang/reflect/Method:getModifiers	()I
        //     35: istore 9
        //     37: iload 9
        //     39: invokestatic 180	java/lang/reflect/Modifier:isStatic	(I)Z
        //     42: ifeq +11 -> 53
        //     45: iload 9
        //     47: invokestatic 183	java/lang/reflect/Modifier:isPublic	(I)Z
        //     50: ifne +121 -> 171
        //     53: new 66	java/lang/RuntimeException
        //     56: dup
        //     57: new 185	java/lang/StringBuilder
        //     60: dup
        //     61: invokespecial 186	java/lang/StringBuilder:<init>	()V
        //     64: ldc 188
        //     66: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     69: aload_1
        //     70: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     73: invokevirtual 196	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     76: invokespecial 198	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     79: athrow
        //     80: astore_3
        //     81: new 66	java/lang/RuntimeException
        //     84: dup
        //     85: new 185	java/lang/StringBuilder
        //     88: dup
        //     89: invokespecial 186	java/lang/StringBuilder:<init>	()V
        //     92: ldc 200
        //     94: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     97: aload_1
        //     98: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     101: invokevirtual 196	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     104: aload_3
        //     105: invokespecial 71	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     108: athrow
        //     109: astore 6
        //     111: new 66	java/lang/RuntimeException
        //     114: dup
        //     115: new 185	java/lang/StringBuilder
        //     118: dup
        //     119: invokespecial 186	java/lang/StringBuilder:<init>	()V
        //     122: ldc 202
        //     124: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     127: aload_1
        //     128: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     131: invokevirtual 196	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     134: aload 6
        //     136: invokespecial 71	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     139: athrow
        //     140: astore 5
        //     142: new 66	java/lang/RuntimeException
        //     145: dup
        //     146: new 185	java/lang/StringBuilder
        //     149: dup
        //     150: invokespecial 186	java/lang/StringBuilder:<init>	()V
        //     153: ldc 204
        //     155: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     158: aload_1
        //     159: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     162: invokevirtual 196	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     165: aload 5
        //     167: invokespecial 71	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     170: athrow
        //     171: new 6	com/android/internal/os/ZygoteInit$MethodAndArgsCaller
        //     174: dup
        //     175: aload 8
        //     177: aload_2
        //     178: invokespecial 207	com/android/internal/os/ZygoteInit$MethodAndArgsCaller:<init>	(Ljava/lang/reflect/Method;[Ljava/lang/String;)V
        //     181: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     0	7	80	java/lang/ClassNotFoundException
        //     7	30	109	java/lang/NoSuchMethodException
        //     7	30	140	java/lang/SecurityException
    }

    public static void main(String[] paramArrayOfString)
    {
        do
            try
            {
                SamplingProfilerIntegration.start();
                registerZygoteSocket();
                EventLog.writeEvent(3020, SystemClock.uptimeMillis());
                preload();
                EventLog.writeEvent(3030, SystemClock.uptimeMillis());
                SamplingProfilerIntegration.writeZygoteSnapshot();
                gc();
                if (paramArrayOfString.length != 2)
                    throw new RuntimeException(paramArrayOfString[0] + " <\"start-system-server\"|\"\" for startSystemServer>");
            }
            catch (MethodAndArgsCaller localMethodAndArgsCaller)
            {
                localMethodAndArgsCaller.run();
                while (true)
                {
                    return;
                    if (!paramArrayOfString[1].equals("start-system-server"))
                        break;
                    startSystemServer();
                    Log.i("Zygote", "Accepting command socket connections");
                    runSelectLoopMode();
                    closeServerSocket();
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                Log.e("Zygote", "Zygote died with exception", localRuntimeException);
                closeServerSocket();
                throw localRuntimeException;
            }
        while (paramArrayOfString[1].equals(""));
        throw new RuntimeException(paramArrayOfString[0] + " <\"start-system-server\"|\"\" for startSystemServer>");
    }

    static void preload()
    {
        preloadClasses();
        preloadResources();
    }

    // ERROR //
    private static void preloadClasses()
    {
        // Byte code:
        //     0: invokestatic 94	dalvik/system/VMRuntime:getRuntime	()Ldalvik/system/VMRuntime;
        //     3: astore_0
        //     4: invokestatic 275	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
        //     7: ldc 21
        //     9: invokevirtual 279	java/lang/ClassLoader:getResourceAsStream	(Ljava/lang/String;)Ljava/io/InputStream;
        //     12: astore_1
        //     13: aload_1
        //     14: ifnonnull +13 -> 27
        //     17: ldc 32
        //     19: ldc_w 281
        //     22: invokestatic 283	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     25: pop
        //     26: return
        //     27: ldc 32
        //     29: ldc_w 285
        //     32: invokestatic 256	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     35: pop
        //     36: invokestatic 222	android/os/SystemClock:uptimeMillis	()J
        //     39: lstore_3
        //     40: sipush 9999
        //     43: invokestatic 289	com/android/internal/os/ZygoteInit:setEffectiveGroup	(I)V
        //     46: sipush 9999
        //     49: invokestatic 292	com/android/internal/os/ZygoteInit:setEffectiveUser	(I)V
        //     52: aload_0
        //     53: invokevirtual 296	dalvik/system/VMRuntime:getTargetHeapUtilization	()F
        //     56: fstore 5
        //     58: aload_0
        //     59: ldc_w 297
        //     62: invokevirtual 301	dalvik/system/VMRuntime:setTargetHeapUtilization	(F)F
        //     65: pop
        //     66: invokestatic 98	java/lang/System:gc	()V
        //     69: aload_0
        //     70: invokevirtual 101	dalvik/system/VMRuntime:runFinalizationSync	()V
        //     73: invokestatic 306	android/os/Debug:startAllocCounting	()V
        //     76: new 308	java/io/BufferedReader
        //     79: dup
        //     80: new 310	java/io/InputStreamReader
        //     83: dup
        //     84: aload_1
        //     85: invokespecial 313	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
        //     88: sipush 256
        //     91: invokespecial 316	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
        //     94: astore 7
        //     96: iconst_0
        //     97: istore 8
        //     99: aload 7
        //     101: invokevirtual 319	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     104: astore 14
        //     106: aload 14
        //     108: ifnull +238 -> 346
        //     111: aload 14
        //     113: invokevirtual 322	java/lang/String:trim	()Ljava/lang/String;
        //     116: astore 17
        //     118: aload 17
        //     120: ldc_w 324
        //     123: invokevirtual 328	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     126: ifne -27 -> 99
        //     129: aload 17
        //     131: ldc_w 263
        //     134: invokevirtual 246	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     137: istore 18
        //     139: iload 18
        //     141: ifne -42 -> 99
        //     144: aload 17
        //     146: invokestatic 331	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
        //     149: pop
        //     150: invokestatic 334	android/os/Debug:getGlobalAllocSize	()I
        //     153: ldc 23
        //     155: if_icmple +13 -> 168
        //     158: invokestatic 98	java/lang/System:gc	()V
        //     161: aload_0
        //     162: invokevirtual 101	dalvik/system/VMRuntime:runFinalizationSync	()V
        //     165: invokestatic 337	android/os/Debug:resetGlobalAllocSize	()V
        //     168: iinc 8 1
        //     171: goto -72 -> 99
        //     174: astore 21
        //     176: ldc 32
        //     178: new 185	java/lang/StringBuilder
        //     181: dup
        //     182: invokespecial 186	java/lang/StringBuilder:<init>	()V
        //     185: ldc_w 339
        //     188: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     191: aload 17
        //     193: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     196: invokevirtual 196	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     199: invokestatic 342	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     202: pop
        //     203: goto -104 -> 99
        //     206: astore 11
        //     208: ldc 32
        //     210: ldc_w 344
        //     213: aload 11
        //     215: invokestatic 85	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     218: pop
        //     219: aload_1
        //     220: invokestatic 350	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     223: aload_0
        //     224: fload 5
        //     226: invokevirtual 301	dalvik/system/VMRuntime:setTargetHeapUtilization	(F)F
        //     229: pop
        //     230: invokestatic 353	android/os/Debug:stopAllocCounting	()V
        //     233: iconst_0
        //     234: invokestatic 292	com/android/internal/os/ZygoteInit:setEffectiveUser	(I)V
        //     237: iconst_0
        //     238: invokestatic 289	com/android/internal/os/ZygoteInit:setEffectiveGroup	(I)V
        //     241: goto -215 -> 26
        //     244: astore 19
        //     246: ldc 32
        //     248: new 185	java/lang/StringBuilder
        //     251: dup
        //     252: invokespecial 186	java/lang/StringBuilder:<init>	()V
        //     255: ldc_w 355
        //     258: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     261: aload 17
        //     263: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     266: ldc_w 357
        //     269: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     272: invokevirtual 196	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     275: aload 19
        //     277: invokestatic 85	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     280: pop
        //     281: aload 19
        //     283: instanceof 359
        //     286: ifeq +36 -> 322
        //     289: aload 19
        //     291: checkcast 359	java/lang/Error
        //     294: athrow
        //     295: astore 9
        //     297: aload_1
        //     298: invokestatic 350	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     301: aload_0
        //     302: fload 5
        //     304: invokevirtual 301	dalvik/system/VMRuntime:setTargetHeapUtilization	(F)F
        //     307: pop
        //     308: invokestatic 353	android/os/Debug:stopAllocCounting	()V
        //     311: iconst_0
        //     312: invokestatic 292	com/android/internal/os/ZygoteInit:setEffectiveUser	(I)V
        //     315: iconst_0
        //     316: invokestatic 289	com/android/internal/os/ZygoteInit:setEffectiveGroup	(I)V
        //     319: aload 9
        //     321: athrow
        //     322: aload 19
        //     324: instanceof 66
        //     327: ifeq +9 -> 336
        //     330: aload 19
        //     332: checkcast 66	java/lang/RuntimeException
        //     335: athrow
        //     336: new 66	java/lang/RuntimeException
        //     339: dup
        //     340: aload 19
        //     342: invokespecial 362	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     345: athrow
        //     346: ldc 32
        //     348: new 185	java/lang/StringBuilder
        //     351: dup
        //     352: invokespecial 186	java/lang/StringBuilder:<init>	()V
        //     355: ldc_w 364
        //     358: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     361: iload 8
        //     363: invokevirtual 367	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     366: ldc_w 369
        //     369: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     372: invokestatic 222	android/os/SystemClock:uptimeMillis	()J
        //     375: lload_3
        //     376: lsub
        //     377: invokevirtual 372	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     380: ldc_w 374
        //     383: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     386: invokevirtual 196	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     389: invokestatic 256	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     392: pop
        //     393: aload_1
        //     394: invokestatic 350	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     397: aload_0
        //     398: fload 5
        //     400: invokevirtual 301	dalvik/system/VMRuntime:setTargetHeapUtilization	(F)F
        //     403: pop
        //     404: invokestatic 353	android/os/Debug:stopAllocCounting	()V
        //     407: iconst_0
        //     408: invokestatic 292	com/android/internal/os/ZygoteInit:setEffectiveUser	(I)V
        //     411: iconst_0
        //     412: invokestatic 289	com/android/internal/os/ZygoteInit:setEffectiveGroup	(I)V
        //     415: goto -389 -> 26
        //
        // Exception table:
        //     from	to	target	type
        //     144	168	174	java/lang/ClassNotFoundException
        //     76	139	206	java/io/IOException
        //     144	168	206	java/io/IOException
        //     176	203	206	java/io/IOException
        //     246	295	206	java/io/IOException
        //     322	393	206	java/io/IOException
        //     144	168	244	java/lang/Throwable
        //     76	139	295	finally
        //     144	168	295	finally
        //     176	203	295	finally
        //     208	219	295	finally
        //     246	295	295	finally
        //     322	393	295	finally
    }

    private static int preloadColorStateLists(VMRuntime paramVMRuntime, TypedArray paramTypedArray)
    {
        int i = paramTypedArray.length();
        for (int j = 0; j < i; j++)
        {
            if (Debug.getGlobalAllocSize() > 50000)
            {
                System.gc();
                paramVMRuntime.runFinalizationSync();
                Debug.resetGlobalAllocSize();
            }
            int k = paramTypedArray.getResourceId(j, 0);
            if (k != 0)
                mResources.getColorStateList(k);
        }
        return i;
    }

    private static int preloadDrawables(VMRuntime paramVMRuntime, TypedArray paramTypedArray)
    {
        int i = paramTypedArray.length();
        for (int j = 0; j < i; j++)
        {
            if (Debug.getGlobalAllocSize() > 50000)
            {
                System.gc();
                paramVMRuntime.runFinalizationSync();
                Debug.resetGlobalAllocSize();
            }
            int k = paramTypedArray.getResourceId(j, 0);
            if ((k != 0) && ((0xBFFFFFFF & mResources.getDrawable(k).getChangingConfigurations()) != 0))
                Log.w("Zygote", "Preloaded drawable resource #0x" + Integer.toHexString(k) + " (" + paramTypedArray.getString(j) + ") that varies with configuration!!");
        }
        return i;
    }

    private static void preloadResources()
    {
        VMRuntime localVMRuntime = VMRuntime.getRuntime();
        Debug.startAllocCounting();
        try
        {
            System.gc();
            localVMRuntime.runFinalizationSync();
            mResources = Resources.getSystem();
            mResources.startPreloading();
            Log.i("Zygote", "Preloading resources...");
            long l1 = SystemClock.uptimeMillis();
            TypedArray localTypedArray1 = mResources.obtainTypedArray(17235973);
            int i = preloadDrawables(localVMRuntime, localTypedArray1);
            localTypedArray1.recycle();
            Log.i("Zygote", "...preloaded " + i + " resources in " + (SystemClock.uptimeMillis() - l1) + "ms.");
            long l2 = SystemClock.uptimeMillis();
            TypedArray localTypedArray2 = mResources.obtainTypedArray(17235974);
            int j = preloadColorStateLists(localVMRuntime, localTypedArray2);
            localTypedArray2.recycle();
            Log.i("Zygote", "...preloaded " + j + " resources in " + (SystemClock.uptimeMillis() - l2) + "ms.");
            mResources.finishPreloading();
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                Log.w("Zygote", "Failure preloading resources", localRuntimeException);
                Debug.stopAllocCounting();
            }
        }
        finally
        {
            Debug.stopAllocCounting();
        }
    }

    // ERROR //
    private static void registerZygoteSocket()
    {
        // Byte code:
        //     0: getstatic 55	com/android/internal/os/ZygoteInit:sServerSocket	Landroid/net/LocalServerSocket;
        //     3: ifnonnull +26 -> 29
        //     6: ldc 11
        //     8: invokestatic 454	java/lang/System:getenv	(Ljava/lang/String;)Ljava/lang/String;
        //     11: invokestatic 458	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     14: istore_1
        //     15: new 57	android/net/LocalServerSocket
        //     18: dup
        //     19: iload_1
        //     20: invokestatic 460	com/android/internal/os/ZygoteInit:createFileDescriptor	(I)Ljava/io/FileDescriptor;
        //     23: invokespecial 463	android/net/LocalServerSocket:<init>	(Ljava/io/FileDescriptor;)V
        //     26: putstatic 55	com/android/internal/os/ZygoteInit:sServerSocket	Landroid/net/LocalServerSocket;
        //     29: return
        //     30: astore_0
        //     31: new 66	java/lang/RuntimeException
        //     34: dup
        //     35: ldc_w 465
        //     38: aload_0
        //     39: invokespecial 71	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     42: athrow
        //     43: astore_2
        //     44: new 66	java/lang/RuntimeException
        //     47: dup
        //     48: new 185	java/lang/StringBuilder
        //     51: dup
        //     52: invokespecial 186	java/lang/StringBuilder:<init>	()V
        //     55: ldc_w 467
        //     58: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     61: iload_1
        //     62: invokevirtual 367	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     65: ldc_w 469
        //     68: invokevirtual 192	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     71: invokevirtual 196	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     74: aload_2
        //     75: invokespecial 71	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     78: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     6	15	30	java/lang/RuntimeException
        //     15	29	43	java/io/IOException
    }

    static native void reopenStdio(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, FileDescriptor paramFileDescriptor3)
        throws IOException;

    private static void runForkMode()
        throws ZygoteInit.MethodAndArgsCaller
    {
        while (true)
        {
            ZygoteConnection localZygoteConnection = acceptCommandPeer();
            int i = Zygote.fork();
            if (i == 0)
                try
                {
                    sServerSocket.close();
                    sServerSocket = null;
                    localZygoteConnection.run();
                    return;
                }
                catch (IOException localIOException)
                {
                    while (true)
                    {
                        Log.e("Zygote", "Zygote Child: error closing sockets", localIOException);
                        sServerSocket = null;
                    }
                }
                finally
                {
                    sServerSocket = null;
                }
            if (i <= 0)
                break;
            localZygoteConnection.closeSocket();
        }
        throw new RuntimeException("Error invoking fork()");
    }

    private static void runSelectLoopMode()
        throws ZygoteInit.MethodAndArgsCaller
    {
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        FileDescriptor[] arrayOfFileDescriptor = new FileDescriptor[4];
        localArrayList1.add(sServerSocket.getFileDescriptor());
        localArrayList2.add(null);
        int i = 10;
        while (true)
        {
            if (i <= 0)
            {
                gc();
                i = 10;
            }
            int j;
            try
            {
                while (true)
                {
                    arrayOfFileDescriptor = (FileDescriptor[])localArrayList1.toArray(arrayOfFileDescriptor);
                    j = selectReadable(arrayOfFileDescriptor);
                    if (j >= 0)
                        break;
                    throw new RuntimeException("Error in select()");
                    i--;
                }
            }
            catch (IOException localIOException)
            {
                throw new RuntimeException("Error in select()", localIOException);
            }
            if (j == 0)
            {
                ZygoteConnection localZygoteConnection = acceptCommandPeer();
                localArrayList2.add(localZygoteConnection);
                localArrayList1.add(localZygoteConnection.getFileDesciptor());
            }
            else if (((ZygoteConnection)localArrayList2.get(j)).runOnce())
            {
                localArrayList2.remove(j);
                localArrayList1.remove(j);
            }
        }
    }

    static native int selectReadable(FileDescriptor[] paramArrayOfFileDescriptor)
        throws IOException;

    static native void setCapabilities(long paramLong1, long paramLong2)
        throws IOException;

    static native void setCloseOnExec(FileDescriptor paramFileDescriptor, boolean paramBoolean)
        throws IOException;

    private static void setEffectiveGroup(int paramInt)
    {
        int i = setregid(0, paramInt);
        if (i != 0)
            Log.e("Zygote", "setregid() failed. errno: " + i);
    }

    private static void setEffectiveUser(int paramInt)
    {
        int i = setreuid(0, paramInt);
        if (i != 0)
            Log.e("Zygote", "setreuid() failed. errno: " + i);
    }

    static native int setpgid(int paramInt1, int paramInt2);

    static native int setregid(int paramInt1, int paramInt2);

    static native int setreuid(int paramInt1, int paramInt2);

    // ERROR //
    private static boolean startSystemServer()
        throws ZygoteInit.MethodAndArgsCaller, RuntimeException
    {
        // Byte code:
        //     0: bipush 7
        //     2: anewarray 242	java/lang/String
        //     5: astore_0
        //     6: aload_0
        //     7: iconst_0
        //     8: ldc_w 543
        //     11: aastore
        //     12: aload_0
        //     13: iconst_1
        //     14: ldc_w 545
        //     17: aastore
        //     18: aload_0
        //     19: iconst_2
        //     20: ldc_w 547
        //     23: aastore
        //     24: aload_0
        //     25: iconst_3
        //     26: ldc_w 549
        //     29: aastore
        //     30: aload_0
        //     31: iconst_4
        //     32: ldc_w 551
        //     35: aastore
        //     36: aload_0
        //     37: iconst_5
        //     38: ldc_w 553
        //     41: aastore
        //     42: aload_0
        //     43: bipush 6
        //     45: ldc_w 555
        //     48: aastore
        //     49: new 114	com/android/internal/os/ZygoteConnection$Arguments
        //     52: dup
        //     53: aload_0
        //     54: invokespecial 557	com/android/internal/os/ZygoteConnection$Arguments:<init>	([Ljava/lang/String;)V
        //     57: astore_1
        //     58: aload_1
        //     59: invokestatic 560	com/android/internal/os/ZygoteConnection:applyDebuggerSystemProperty	(Lcom/android/internal/os/ZygoteConnection$Arguments;)V
        //     62: aload_1
        //     63: invokestatic 563	com/android/internal/os/ZygoteConnection:applyInvokeWithSystemProperty	(Lcom/android/internal/os/ZygoteConnection$Arguments;)V
        //     66: aload_1
        //     67: getfield 566	com/android/internal/os/ZygoteConnection$Arguments:uid	I
        //     70: aload_1
        //     71: getfield 569	com/android/internal/os/ZygoteConnection$Arguments:gid	I
        //     74: aload_1
        //     75: getfield 573	com/android/internal/os/ZygoteConnection$Arguments:gids	[I
        //     78: aload_1
        //     79: getfield 576	com/android/internal/os/ZygoteConnection$Arguments:debugFlags	I
        //     82: aconst_null
        //     83: checkcast 578	[[I
        //     86: aload_1
        //     87: getfield 582	com/android/internal/os/ZygoteConnection$Arguments:permittedCapabilities	J
        //     90: aload_1
        //     91: getfield 585	com/android/internal/os/ZygoteConnection$Arguments:effectiveCapabilities	J
        //     94: invokestatic 589	dalvik/system/Zygote:forkSystemServer	(II[II[[IJJ)I
        //     97: istore_3
        //     98: iload_3
        //     99: ifne +7 -> 106
        //     102: aload_1
        //     103: invokestatic 591	com/android/internal/os/ZygoteInit:handleSystemServerProcess	(Lcom/android/internal/os/ZygoteConnection$Arguments;)V
        //     106: iconst_1
        //     107: ireturn
        //     108: astore_2
        //     109: new 66	java/lang/RuntimeException
        //     112: dup
        //     113: aload_2
        //     114: invokespecial 362	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     117: athrow
        //     118: astore_2
        //     119: goto -10 -> 109
        //
        // Exception table:
        //     from	to	target	type
        //     49	58	108	java/lang/IllegalArgumentException
        //     58	98	118	java/lang/IllegalArgumentException
    }

    public static class MethodAndArgsCaller extends Exception
        implements Runnable
    {
        private final String[] mArgs;
        private final Method mMethod;

        public MethodAndArgsCaller(Method paramMethod, String[] paramArrayOfString)
        {
            this.mMethod = paramMethod;
            this.mArgs = paramArrayOfString;
        }

        public void run()
        {
            try
            {
                Method localMethod = this.mMethod;
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = this.mArgs;
                localMethod.invoke(null, arrayOfObject);
                return;
            }
            catch (IllegalAccessException localIllegalAccessException)
            {
                throw new RuntimeException(localIllegalAccessException);
            }
            catch (InvocationTargetException localInvocationTargetException)
            {
                Throwable localThrowable = localInvocationTargetException.getCause();
                if ((localThrowable instanceof RuntimeException))
                    throw ((RuntimeException)localThrowable);
                if ((localThrowable instanceof Error))
                    throw ((Error)localThrowable);
                throw new RuntimeException(localInvocationTargetException);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.ZygoteInit
 * JD-Core Version:        0.6.2
 */