package com.android.internal.os;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PkgUsageStats
    implements Parcelable
{
    public static final Parcelable.Creator<PkgUsageStats> CREATOR = new Parcelable.Creator()
    {
        public PkgUsageStats createFromParcel(Parcel paramAnonymousParcel)
        {
            return new PkgUsageStats(paramAnonymousParcel);
        }

        public PkgUsageStats[] newArray(int paramAnonymousInt)
        {
            return new PkgUsageStats[paramAnonymousInt];
        }
    };
    public Map<String, Long> componentResumeTimes;
    public int launchCount;
    public String packageName;
    public long usageTime;

    public PkgUsageStats(Parcel paramParcel)
    {
        this.packageName = paramParcel.readString();
        this.launchCount = paramParcel.readInt();
        this.usageTime = paramParcel.readLong();
        int i = paramParcel.readInt();
        this.componentResumeTimes = new HashMap(i);
        for (int j = 0; j < i; j++)
        {
            String str = paramParcel.readString();
            long l = paramParcel.readLong();
            this.componentResumeTimes.put(str, Long.valueOf(l));
        }
    }

    public PkgUsageStats(PkgUsageStats paramPkgUsageStats)
    {
        this.packageName = paramPkgUsageStats.packageName;
        this.launchCount = paramPkgUsageStats.launchCount;
        this.usageTime = paramPkgUsageStats.usageTime;
        this.componentResumeTimes = new HashMap(paramPkgUsageStats.componentResumeTimes);
    }

    public PkgUsageStats(String paramString, int paramInt, long paramLong, Map<String, Long> paramMap)
    {
        this.packageName = paramString;
        this.launchCount = paramInt;
        this.usageTime = paramLong;
        this.componentResumeTimes = new HashMap(paramMap);
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        return "PkgUsageStats{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.packageName + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.packageName);
        paramParcel.writeInt(this.launchCount);
        paramParcel.writeLong(this.usageTime);
        paramParcel.writeInt(this.componentResumeTimes.size());
        Iterator localIterator = this.componentResumeTimes.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            paramParcel.writeString((String)localEntry.getKey());
            paramParcel.writeLong(((Long)localEntry.getValue()).longValue());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.PkgUsageStats
 * JD-Core Version:        0.6.2
 */