package com.android.internal.os;

import android.os.IBinder;
import android.os.SystemClock;
import android.util.EventLog;
import java.lang.ref.WeakReference;

public class BinderInternal
{
    static WeakReference<GcWatcher> mGcWatcher = new WeakReference(new GcWatcher());
    static long mLastGcTime;

    public static final native void disableBackgroundScheduling(boolean paramBoolean);

    static void forceBinderGc()
    {
        forceGc("Binder");
    }

    public static void forceGc(String paramString)
    {
        EventLog.writeEvent(2741, paramString);
        Runtime.getRuntime().gc();
    }

    public static final native IBinder getContextObject();

    public static long getLastGcTime()
    {
        return mLastGcTime;
    }

    static final native void handleGc();

    public static final native void joinThreadPool();

    static final class GcWatcher
    {
        protected void finalize()
            throws Throwable
        {
            BinderInternal.handleGc();
            BinderInternal.mLastGcTime = SystemClock.uptimeMillis();
            BinderInternal.mGcWatcher = new WeakReference(new GcWatcher());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.os.BinderInternal
 * JD-Core Version:        0.6.2
 */