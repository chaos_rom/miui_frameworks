package com.android.internal.content;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import java.util.ArrayList;

public class SelectionBuilder
{
    private StringBuilder mSelection = new StringBuilder();
    private ArrayList<String> mSelectionArgs = new ArrayList();

    public SelectionBuilder append(String paramString, Object[] paramArrayOfObject)
    {
        if (TextUtils.isEmpty(paramString))
        {
            if ((paramArrayOfObject != null) && (paramArrayOfObject.length > 0))
                throw new IllegalArgumentException("Valid selection required when including arguments");
        }
        else
        {
            if (this.mSelection.length() > 0)
                this.mSelection.append(" AND ");
            this.mSelection.append("(").append(paramString).append(")");
            if (paramArrayOfObject != null)
            {
                int i = paramArrayOfObject.length;
                for (int j = 0; j < i; j++)
                {
                    Object localObject = paramArrayOfObject[j];
                    this.mSelectionArgs.add(String.valueOf(localObject));
                }
            }
        }
        return this;
    }

    public int delete(SQLiteDatabase paramSQLiteDatabase, String paramString)
    {
        return paramSQLiteDatabase.delete(paramString, getSelection(), getSelectionArgs());
    }

    public String getSelection()
    {
        return this.mSelection.toString();
    }

    public String[] getSelectionArgs()
    {
        return (String[])this.mSelectionArgs.toArray(new String[this.mSelectionArgs.size()]);
    }

    public Cursor query(SQLiteDatabase paramSQLiteDatabase, String paramString1, String[] paramArrayOfString, String paramString2)
    {
        return query(paramSQLiteDatabase, paramString1, paramArrayOfString, null, null, paramString2, null);
    }

    public Cursor query(SQLiteDatabase paramSQLiteDatabase, String paramString1, String[] paramArrayOfString, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        return paramSQLiteDatabase.query(paramString1, paramArrayOfString, getSelection(), getSelectionArgs(), paramString2, paramString3, paramString4, paramString5);
    }

    public SelectionBuilder reset()
    {
        this.mSelection.setLength(0);
        this.mSelectionArgs.clear();
        return this;
    }

    public int update(SQLiteDatabase paramSQLiteDatabase, String paramString, ContentValues paramContentValues)
    {
        return paramSQLiteDatabase.update(paramString, paramContentValues, getSelection(), getSelectionArgs());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.content.SelectionBuilder
 * JD-Core Version:        0.6.2
 */