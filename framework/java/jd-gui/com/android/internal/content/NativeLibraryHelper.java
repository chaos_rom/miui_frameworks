package com.android.internal.content;

import android.os.Build;
import android.util.Slog;
import java.io.File;

public class NativeLibraryHelper
{
    private static final boolean DEBUG_NATIVE = false;
    private static final String TAG = "NativeHelper";

    public static int copyNativeBinariesIfNeededLI(File paramFile1, File paramFile2)
    {
        String str1 = Build.CPU_ABI;
        String str2 = Build.CPU_ABI2;
        return nativeCopyNativeBinaries(paramFile1.getPath(), paramFile2.getPath(), str1, str2);
    }

    private static native int nativeCopyNativeBinaries(String paramString1, String paramString2, String paramString3, String paramString4);

    private static native long nativeSumNativeBinaries(String paramString1, String paramString2, String paramString3);

    public static boolean removeNativeBinariesFromDirLI(File paramFile)
    {
        boolean bool = false;
        if (paramFile.exists())
        {
            File[] arrayOfFile = paramFile.listFiles();
            if (arrayOfFile != null)
            {
                int i = 0;
                if (i < arrayOfFile.length)
                {
                    if (!arrayOfFile[i].delete())
                        Slog.w("NativeHelper", "Could not delete native binary: " + arrayOfFile[i].getPath());
                    while (true)
                    {
                        i++;
                        break;
                        bool = true;
                    }
                }
            }
        }
        return bool;
    }

    public static boolean removeNativeBinariesLI(String paramString)
    {
        return removeNativeBinariesFromDirLI(new File(paramString));
    }

    public static long sumNativeBinariesLI(File paramFile)
    {
        String str1 = Build.CPU_ABI;
        String str2 = Build.CPU_ABI2;
        return nativeSumNativeBinaries(paramFile.getPath(), str1, str2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.content.NativeLibraryHelper
 * JD-Core Version:        0.6.2
 */