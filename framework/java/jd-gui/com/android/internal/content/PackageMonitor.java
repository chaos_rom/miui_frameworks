package com.android.internal.content;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import java.util.HashSet;

public abstract class PackageMonitor extends BroadcastReceiver
{
    public static final int PACKAGE_PERMANENT_CHANGE = 3;
    public static final int PACKAGE_TEMPORARY_CHANGE = 2;
    public static final int PACKAGE_UNCHANGED = 0;
    public static final int PACKAGE_UPDATING = 1;
    static Handler sBackgroundHandler;
    static HandlerThread sBackgroundThread;
    static final IntentFilter sExternalFilt;
    static final Object sLock;
    static final IntentFilter sNonDataFilt;
    static final IntentFilter sPackageFilt = new IntentFilter();
    String[] mAppearingPackages;
    int mChangeType;
    String[] mDisappearingPackages;
    String[] mModifiedPackages;
    Context mRegisteredContext;
    Handler mRegisteredHandler;
    boolean mSomePackagesChanged;
    String[] mTempArray = new String[1];
    final HashSet<String> mUpdatingPackages = new HashSet();

    static
    {
        sNonDataFilt = new IntentFilter();
        sExternalFilt = new IntentFilter();
        sLock = new Object();
        sPackageFilt.addAction("android.intent.action.PACKAGE_ADDED");
        sPackageFilt.addAction("android.intent.action.PACKAGE_REMOVED");
        sPackageFilt.addAction("android.intent.action.PACKAGE_CHANGED");
        sPackageFilt.addAction("android.intent.action.QUERY_PACKAGE_RESTART");
        sPackageFilt.addAction("android.intent.action.PACKAGE_RESTARTED");
        sPackageFilt.addAction("android.intent.action.UID_REMOVED");
        sPackageFilt.addDataScheme("package");
        sNonDataFilt.addAction("android.intent.action.UID_REMOVED");
        sExternalFilt.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
        sExternalFilt.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
    }

    public boolean anyPackagesAppearing()
    {
        if (this.mAppearingPackages != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean anyPackagesDisappearing()
    {
        if (this.mDisappearingPackages != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean didSomePackagesChange()
    {
        return this.mSomePackagesChanged;
    }

    String getPackageName(Intent paramIntent)
    {
        Uri localUri = paramIntent.getData();
        if (localUri != null);
        for (String str = localUri.getSchemeSpecificPart(); ; str = null)
            return str;
    }

    public Handler getRegisteredHandler()
    {
        return this.mRegisteredHandler;
    }

    public int isPackageAppearing(String paramString)
    {
        int j;
        if (this.mAppearingPackages != null)
        {
            j = -1 + this.mAppearingPackages.length;
            if (j >= 0)
                if (!paramString.equals(this.mAppearingPackages[j]));
        }
        for (int i = this.mChangeType; ; i = 0)
        {
            return i;
            j--;
            break;
        }
    }

    public int isPackageDisappearing(String paramString)
    {
        int j;
        if (this.mDisappearingPackages != null)
        {
            j = -1 + this.mDisappearingPackages.length;
            if (j >= 0)
                if (!paramString.equals(this.mDisappearingPackages[j]));
        }
        for (int i = this.mChangeType; ; i = 0)
        {
            return i;
            j--;
            break;
        }
    }

    public boolean isPackageModified(String paramString)
    {
        int i;
        if (this.mModifiedPackages != null)
        {
            i = -1 + this.mModifiedPackages.length;
            if (i >= 0)
                if (!paramString.equals(this.mModifiedPackages[i]));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    boolean isPackageUpdating(String paramString)
    {
        synchronized (this.mUpdatingPackages)
        {
            boolean bool = this.mUpdatingPackages.contains(paramString);
            return bool;
        }
    }

    public void onBeginPackageChanges()
    {
    }

    public void onFinishPackageChanges()
    {
    }

    public boolean onHandleForceStop(Intent paramIntent, String[] paramArrayOfString, int paramInt, boolean paramBoolean)
    {
        return false;
    }

    public void onPackageAdded(String paramString, int paramInt)
    {
    }

    public void onPackageAppeared(String paramString, int paramInt)
    {
    }

    public void onPackageChanged(String paramString, int paramInt, String[] paramArrayOfString)
    {
    }

    public void onPackageDisappeared(String paramString, int paramInt)
    {
    }

    public void onPackageModified(String paramString)
    {
    }

    public void onPackageRemoved(String paramString, int paramInt)
    {
    }

    public void onPackageUpdateFinished(String paramString, int paramInt)
    {
    }

    public void onPackageUpdateStarted(String paramString, int paramInt)
    {
    }

    public void onPackagesAvailable(String[] paramArrayOfString)
    {
    }

    public void onPackagesUnavailable(String[] paramArrayOfString)
    {
    }

    public void onReceive(Context paramContext, Intent paramIntent)
    {
        onBeginPackageChanges();
        this.mAppearingPackages = null;
        this.mDisappearingPackages = null;
        this.mSomePackagesChanged = false;
        String str1 = paramIntent.getAction();
        String str4;
        int n;
        if ("android.intent.action.PACKAGE_ADDED".equals(str1))
        {
            str4 = getPackageName(paramIntent);
            n = paramIntent.getIntExtra("android.intent.extra.UID", 0);
            this.mSomePackagesChanged = true;
            if (str4 != null)
            {
                this.mAppearingPackages = this.mTempArray;
                this.mTempArray[0] = str4;
                if (!paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false))
                    break label168;
                this.mModifiedPackages = this.mTempArray;
                this.mChangeType = 1;
                onPackageUpdateFinished(str4, n);
                onPackageModified(str4);
                onPackageAppeared(str4, this.mChangeType);
                if (this.mChangeType != 1);
            }
        }
        while (true)
        {
            synchronized (this.mUpdatingPackages)
            {
                this.mUpdatingPackages.remove(str4);
                if (this.mSomePackagesChanged)
                    onSomePackagesChanged();
                onFinishPackageChanges();
                return;
                label168: this.mChangeType = 3;
                onPackageAdded(str4, n);
            }
            if ("android.intent.action.PACKAGE_REMOVED".equals(str1))
            {
                String str3 = getPackageName(paramIntent);
                int m = paramIntent.getIntExtra("android.intent.extra.UID", 0);
                if (str3 != null)
                {
                    this.mDisappearingPackages = this.mTempArray;
                    this.mTempArray[0] = str3;
                    if (paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false))
                        this.mChangeType = 1;
                    while (true)
                    {
                        synchronized (this.mUpdatingPackages)
                        {
                            onPackageUpdateStarted(str3, m);
                        }
                        localObject1 = finally;
                        throw localObject1;
                        this.mChangeType = 3;
                        this.mSomePackagesChanged = true;
                        onPackageRemoved(str3, m);
                    }
                }
            }
            else if ("android.intent.action.PACKAGE_CHANGED".equals(str1))
            {
                String str2 = getPackageName(paramIntent);
                int k = paramIntent.getIntExtra("android.intent.extra.UID", 0);
                String[] arrayOfString4 = paramIntent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
                if (str2 != null)
                {
                    this.mModifiedPackages = this.mTempArray;
                    this.mTempArray[0] = str2;
                    onPackageChanged(str2, k, arrayOfString4);
                    onPackageModified(str2);
                }
            }
            else if ("android.intent.action.QUERY_PACKAGE_RESTART".equals(str1))
            {
                this.mDisappearingPackages = paramIntent.getStringArrayExtra("android.intent.extra.PACKAGES");
                this.mChangeType = 2;
                if (onHandleForceStop(paramIntent, this.mDisappearingPackages, paramIntent.getIntExtra("android.intent.extra.UID", 0), false))
                    setResultCode(-1);
            }
            else if ("android.intent.action.PACKAGE_RESTARTED".equals(str1))
            {
                String[] arrayOfString3 = new String[1];
                arrayOfString3[0] = getPackageName(paramIntent);
                this.mDisappearingPackages = arrayOfString3;
                this.mChangeType = 2;
                onHandleForceStop(paramIntent, this.mDisappearingPackages, paramIntent.getIntExtra("android.intent.extra.UID", 0), true);
            }
            else if ("android.intent.action.UID_REMOVED".equals(str1))
            {
                onUidRemoved(paramIntent.getIntExtra("android.intent.extra.UID", 0));
            }
            else if ("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE".equals(str1))
            {
                String[] arrayOfString2 = paramIntent.getStringArrayExtra("android.intent.extra.changed_package_list");
                this.mAppearingPackages = arrayOfString2;
                this.mChangeType = 2;
                this.mSomePackagesChanged = true;
                if (arrayOfString2 != null)
                {
                    onPackagesAvailable(arrayOfString2);
                    for (int j = 0; j < arrayOfString2.length; j++)
                        onPackageAppeared(arrayOfString2[j], 2);
                }
            }
            else if ("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE".equals(str1))
            {
                String[] arrayOfString1 = paramIntent.getStringArrayExtra("android.intent.extra.changed_package_list");
                this.mDisappearingPackages = arrayOfString1;
                this.mChangeType = 2;
                this.mSomePackagesChanged = true;
                if (arrayOfString1 != null)
                {
                    onPackagesUnavailable(arrayOfString1);
                    for (int i = 0; i < arrayOfString1.length; i++)
                        onPackageDisappeared(arrayOfString1[i], 2);
                }
            }
        }
    }

    public void onSomePackagesChanged()
    {
    }

    public void onUidRemoved(int paramInt)
    {
    }

    public void register(Context paramContext, Looper paramLooper, boolean paramBoolean)
    {
        if (this.mRegisteredContext != null)
            throw new IllegalStateException("Already registered");
        this.mRegisteredContext = paramContext;
        if (paramLooper == null);
        while (true)
        {
            synchronized (sLock)
            {
                if (sBackgroundThread == null)
                {
                    sBackgroundThread = new HandlerThread("PackageMonitor", 10);
                    sBackgroundThread.start();
                    sBackgroundHandler = new Handler(sBackgroundThread.getLooper());
                }
                this.mRegisteredHandler = sBackgroundHandler;
                paramContext.registerReceiver(this, sPackageFilt, null, this.mRegisteredHandler);
                paramContext.registerReceiver(this, sNonDataFilt, null, this.mRegisteredHandler);
                if (paramBoolean)
                    paramContext.registerReceiver(this, sExternalFilt, null, this.mRegisteredHandler);
                return;
            }
            this.mRegisteredHandler = new Handler(paramLooper);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public void register(Context paramContext, boolean paramBoolean)
    {
        register(paramContext, null, paramBoolean);
    }

    public void unregister()
    {
        if (this.mRegisteredContext == null)
            throw new IllegalStateException("Not registered");
        this.mRegisteredContext.unregisterReceiver(this);
        this.mRegisteredContext = null;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.content.PackageMonitor
 * JD-Core Version:        0.6.2
 */