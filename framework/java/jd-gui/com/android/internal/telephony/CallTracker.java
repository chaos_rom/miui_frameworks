package com.android.internal.telephony;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class CallTracker extends Handler
{
    private static final boolean DBG_POLL = false;
    protected static final int EVENT_CALL_STATE_CHANGE = 2;
    protected static final int EVENT_CALL_WAITING_INFO_CDMA = 15;
    protected static final int EVENT_CONFERENCE_RESULT = 11;
    protected static final int EVENT_ECT_RESULT = 13;
    protected static final int EVENT_EXIT_ECM_RESPONSE_CDMA = 14;
    protected static final int EVENT_GET_LAST_CALL_FAIL_CAUSE = 5;
    protected static final int EVENT_OPERATION_COMPLETE = 4;
    protected static final int EVENT_POLL_CALLS_RESULT = 1;
    protected static final int EVENT_RADIO_AVAILABLE = 9;
    protected static final int EVENT_RADIO_NOT_AVAILABLE = 10;
    protected static final int EVENT_REPOLL_AFTER_DELAY = 3;
    protected static final int EVENT_SEPARATE_RESULT = 12;
    protected static final int EVENT_SWITCH_RESULT = 8;
    protected static final int EVENT_THREE_WAY_DIAL_L2_RESULT_CDMA = 16;
    static final int POLL_DELAY_MSEC = 250;
    public CommandsInterface cm;
    protected Message lastRelevantPoll;
    protected boolean needsPoll;
    protected int pendingOperations;

    private boolean checkNoOperationsPending()
    {
        if (this.pendingOperations == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected String checkForTestEmergencyNumber(String paramString)
    {
        String str = SystemProperties.get("ril.test.emergencynumber");
        if (!TextUtils.isEmpty(str))
        {
            String[] arrayOfString = str.split(":");
            log("checkForTestEmergencyNumber: values.length=" + arrayOfString.length);
            if ((arrayOfString.length == 2) && (arrayOfString[0].equals(PhoneNumberUtils.stripSeparators(paramString))))
            {
                this.cm.testingEmergencyCall();
                log("checkForTestEmergencyNumber: remap " + paramString + " to " + arrayOfString[1]);
                paramString = arrayOfString[1];
            }
        }
        return paramString;
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("CallTracker:");
        paramPrintWriter.println(" pendingOperations=" + this.pendingOperations);
        paramPrintWriter.println(" needsPoll=" + this.needsPoll);
        paramPrintWriter.println(" lastRelevantPoll=" + this.lastRelevantPoll);
    }

    public abstract void handleMessage(Message paramMessage);

    protected abstract void handlePollCalls(AsyncResult paramAsyncResult);

    protected void handleRadioAvailable()
    {
        pollCallsWhenSafe();
    }

    protected boolean isCommandExceptionRadioNotAvailable(Throwable paramThrowable)
    {
        if ((paramThrowable != null) && ((paramThrowable instanceof CommandException)) && (((CommandException)paramThrowable).getCommandError() == CommandException.Error.RADIO_NOT_AVAILABLE));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected abstract void log(String paramString);

    protected Message obtainNoPollCompleteMessage(int paramInt)
    {
        this.pendingOperations = (1 + this.pendingOperations);
        this.lastRelevantPoll = null;
        return obtainMessage(paramInt);
    }

    protected void pollCallsAfterDelay()
    {
        Message localMessage = obtainMessage();
        localMessage.what = 3;
        sendMessageDelayed(localMessage, 250L);
    }

    protected void pollCallsWhenSafe()
    {
        this.needsPoll = true;
        if (checkNoOperationsPending())
        {
            this.lastRelevantPoll = obtainMessage(1);
            this.cm.getCurrentCalls(this.lastRelevantPoll);
        }
    }

    public abstract void registerForVoiceCallEnded(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForVoiceCallStarted(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void unregisterForVoiceCallEnded(Handler paramHandler);

    public abstract void unregisterForVoiceCallStarted(Handler paramHandler);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.CallTracker
 * JD-Core Version:        0.6.2
 */