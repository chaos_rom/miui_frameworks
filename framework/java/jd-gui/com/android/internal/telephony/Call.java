package com.android.internal.telephony;

import android.util.Log;
import java.util.List;

public abstract class Call
{
    protected final String LOG_TAG = "Call";
    protected boolean isGeneric = false;
    public State state = State.IDLE;

    public abstract List<Connection> getConnections();

    public long getEarliestConnectTime()
    {
        long l1 = 9223372036854775807L;
        List localList = getConnections();
        if (localList.size() == 0);
        for (long l2 = 0L; ; l2 = l1)
        {
            return l2;
            int i = 0;
            int j = localList.size();
            while (i < j)
            {
                long l3 = ((Connection)localList.get(i)).getConnectTime();
                if (l3 < l1)
                    l1 = l3;
                i++;
            }
        }
    }

    public Connection getEarliestConnection()
    {
        long l1 = 9223372036854775807L;
        Object localObject1 = null;
        List localList = getConnections();
        if (localList.size() == 0);
        for (Object localObject2 = null; ; localObject2 = localObject1)
        {
            return localObject2;
            int i = 0;
            int j = localList.size();
            while (i < j)
            {
                Connection localConnection = (Connection)localList.get(i);
                long l2 = localConnection.getCreateTime();
                if (l2 < l1)
                {
                    localObject1 = localConnection;
                    l1 = l2;
                }
                i++;
            }
        }
    }

    public long getEarliestCreateTime()
    {
        long l1 = 9223372036854775807L;
        List localList = getConnections();
        if (localList.size() == 0);
        for (long l2 = 0L; ; l2 = l1)
        {
            return l2;
            int i = 0;
            int j = localList.size();
            while (i < j)
            {
                long l3 = ((Connection)localList.get(i)).getCreateTime();
                if (l3 < l1)
                    l1 = l3;
                i++;
            }
        }
    }

    public Connection getLatestConnection()
    {
        List localList = getConnections();
        Object localObject;
        if (localList.size() == 0)
            localObject = null;
        while (true)
        {
            return localObject;
            long l1 = 0L;
            localObject = null;
            int i = 0;
            int j = localList.size();
            while (i < j)
            {
                Connection localConnection = (Connection)localList.get(i);
                long l2 = localConnection.getCreateTime();
                if (l2 > l1)
                {
                    localObject = localConnection;
                    l1 = l2;
                }
                i++;
            }
        }
    }

    public abstract Phone getPhone();

    public State getState()
    {
        return this.state;
    }

    public abstract void hangup()
        throws CallStateException;

    public void hangupIfAlive()
    {
        if (getState().isAlive());
        try
        {
            hangup();
            return;
        }
        catch (CallStateException localCallStateException)
        {
            while (true)
                Log.w("Call", " hangupIfActive: caught " + localCallStateException);
        }
    }

    public boolean hasConnection(Connection paramConnection)
    {
        if (paramConnection.getCall() == this);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasConnections()
    {
        boolean bool = false;
        List localList = getConnections();
        if (localList == null);
        while (true)
        {
            return bool;
            if (localList.size() > 0)
                bool = true;
        }
    }

    public boolean isDialingOrAlerting()
    {
        return getState().isDialing();
    }

    public boolean isGeneric()
    {
        return this.isGeneric;
    }

    public boolean isIdle()
    {
        if (!getState().isAlive());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public abstract boolean isMultiparty();

    public boolean isRinging()
    {
        return getState().isRinging();
    }

    public void setGeneric(boolean paramBoolean)
    {
        this.isGeneric = paramBoolean;
    }

    public static enum State
    {
        static
        {
            ACTIVE = new State("ACTIVE", 1);
            HOLDING = new State("HOLDING", 2);
            DIALING = new State("DIALING", 3);
            ALERTING = new State("ALERTING", 4);
            INCOMING = new State("INCOMING", 5);
            WAITING = new State("WAITING", 6);
            DISCONNECTED = new State("DISCONNECTED", 7);
            DISCONNECTING = new State("DISCONNECTING", 8);
            State[] arrayOfState = new State[9];
            arrayOfState[0] = IDLE;
            arrayOfState[1] = ACTIVE;
            arrayOfState[2] = HOLDING;
            arrayOfState[3] = DIALING;
            arrayOfState[4] = ALERTING;
            arrayOfState[5] = INCOMING;
            arrayOfState[6] = WAITING;
            arrayOfState[7] = DISCONNECTED;
            arrayOfState[8] = DISCONNECTING;
        }

        public boolean isAlive()
        {
            if ((this != IDLE) && (this != DISCONNECTED) && (this != DISCONNECTING));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isDialing()
        {
            if ((this == DIALING) || (this == ALERTING));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isRinging()
        {
            if ((this == INCOMING) || (this == WAITING));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.Call
 * JD-Core Version:        0.6.2
 */