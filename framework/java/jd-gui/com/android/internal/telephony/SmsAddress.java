package com.android.internal.telephony;

public abstract class SmsAddress
{
    public static final int TON_ABBREVIATED = 6;
    public static final int TON_ALPHANUMERIC = 5;
    public static final int TON_INTERNATIONAL = 1;
    public static final int TON_NATIONAL = 2;
    public static final int TON_NETWORK = 3;
    public static final int TON_SUBSCRIBER = 4;
    public static final int TON_UNKNOWN;
    public String address;
    public byte[] origBytes;
    public int ton;

    public boolean couldBeEmailGateway()
    {
        if (this.address.length() <= 4);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String getAddressString()
    {
        return this.address;
    }

    public boolean isAlphanumeric()
    {
        if (this.ton == 5);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isNetworkSpecific()
    {
        if (this.ton == 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.SmsAddress
 * JD-Core Version:        0.6.2
 */