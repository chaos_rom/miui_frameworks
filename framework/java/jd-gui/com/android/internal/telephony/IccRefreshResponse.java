package com.android.internal.telephony;

public class IccRefreshResponse
{
    public static final int REFRESH_RESULT_FILE_UPDATE = 0;
    public static final int REFRESH_RESULT_INIT = 1;
    public static final int REFRESH_RESULT_RESET = 2;
    public String aid;
    public int efId;
    public int refreshResult;

    public String toString()
    {
        return "{" + this.refreshResult + ", " + this.aid + ", " + this.efId + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccRefreshResponse
 * JD-Core Version:        0.6.2
 */