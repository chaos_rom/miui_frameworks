package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.Binder;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemProperties;
import android.provider.Telephony.Sms;
import android.telephony.PhoneNumberUtils;
import android.telephony.ServiceState;
import android.telephony.SmsCbMessage;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Window;
import com.android.internal.util.HexDump;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import miui.provider.ExtraTelephony;

public abstract class SMSDispatcher extends Handler
{
    private static final int DESTINATION_PORT_COLUMN = 2;
    protected static final int EVENT_NEW_SMS = 1;
    static final int EVENT_SEND_CONFIRMED_SMS = 5;
    private static final int EVENT_SEND_LIMIT_REACHED_CONFIRMATION = 4;
    private static final int EVENT_SEND_RETRY = 3;
    protected static final int EVENT_SEND_SMS_COMPLETE = 2;
    static final int EVENT_STOP_SENDING = 7;
    private static final int MAX_SEND_RETRIES = 3;
    private static final int MO_MSG_QUEUE_LIMIT = 5;
    private static final int PDU_COLUMN = 0;
    private static final String[] PDU_PROJECTION;
    private static final String[] PDU_SEQUENCE_PORT_PROJECTION = arrayOfString2;
    public static final String RECEIVE_EMERGENCY_BROADCAST_PERMISSION = "android.permission.RECEIVE_EMERGENCY_BROADCAST";
    public static final String RECEIVE_SMS_PERMISSION = "android.permission.RECEIVE_SMS";
    private static final String SEND_NEXT_MSG_EXTRA = "SendNextMsg";
    private static final int SEND_RETRY_DELAY = 2000;
    private static final String SEND_SMS_NO_CONFIRMATION_PERMISSION = "android.permission.SEND_SMS_NO_CONFIRMATION";
    private static final int SEQUENCE_COLUMN = 1;
    private static final int SINGLE_PART_SMS = 1;
    static final String TAG = "SMS";
    private static final int WAKE_LOCK_TIMEOUT = 5000;
    protected static final Uri mRawUri = Uri.withAppendedPath(Telephony.Sms.CONTENT_URI, "raw");
    private static int sConcatenatedRef = new Random().nextInt(256);
    protected final ArrayList<SmsTracker> deliveryPendingList;
    protected final CommandsInterface mCm;
    protected final Context mContext;
    private int mPendingTrackerCount;
    protected final Phone mPhone;
    protected int mRemainingMessages;
    protected final ContentResolver mResolver;
    private final BroadcastReceiver mResultReceiver;
    protected boolean mSmsCapable;
    protected boolean mSmsReceiveDisabled;
    protected boolean mSmsSendDisabled;
    protected final SmsStorageMonitor mStorageMonitor;
    protected final TelephonyManager mTelephonyManager;
    private final SmsUsageMonitor mUsageMonitor;
    private PowerManager.WakeLock mWakeLock;
    protected final WapPushOverSms mWapPush;

    static
    {
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "pdu";
        PDU_PROJECTION = arrayOfString1;
        String[] arrayOfString2 = new String[3];
        arrayOfString2[0] = "pdu";
        arrayOfString2[1] = "sequence";
        arrayOfString2[2] = "destination_port";
    }

    protected SMSDispatcher(PhoneBase paramPhoneBase, SmsStorageMonitor paramSmsStorageMonitor, SmsUsageMonitor paramSmsUsageMonitor)
    {
        this.mSmsCapable = bool1;
        this.mRemainingMessages = -1;
        this.deliveryPendingList = new ArrayList();
        this.mResultReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                int i = 1;
                int j = getResultCode();
                if ((j == -1) || (j == i));
                while (true)
                {
                    SMSDispatcher.this.acknowledgeLastIncomingSms(i, j, null);
                    return;
                    i = 0;
                }
            }
        };
        this.mPhone = paramPhoneBase;
        this.mWapPush = new WapPushOverSms(paramPhoneBase, this);
        this.mContext = paramPhoneBase.getContext();
        this.mResolver = this.mContext.getContentResolver();
        this.mCm = paramPhoneBase.mCM;
        this.mStorageMonitor = paramSmsStorageMonitor;
        this.mUsageMonitor = paramSmsUsageMonitor;
        this.mTelephonyManager = ((TelephonyManager)this.mContext.getSystemService("phone"));
        createWakelock();
        this.mSmsCapable = this.mContext.getResources().getBoolean(17891369);
        boolean bool2;
        if (!SystemProperties.getBoolean("telephony.sms.receive", this.mSmsCapable))
        {
            bool2 = bool1;
            this.mSmsReceiveDisabled = bool2;
            if (SystemProperties.getBoolean("telephony.sms.send", this.mSmsCapable))
                break label244;
        }
        while (true)
        {
            this.mSmsSendDisabled = bool1;
            Log.d("SMS", "SMSDispatcher: ctor mSmsCapable=" + this.mSmsCapable + " format=" + getFormat() + " mSmsReceiveDisabled=" + this.mSmsReceiveDisabled + " mSmsSendDisabled=" + this.mSmsSendDisabled);
            return;
            bool2 = false;
            break;
            label244: bool1 = false;
        }
    }

    private void createWakelock()
    {
        this.mWakeLock = ((PowerManager)this.mContext.getSystemService("power")).newWakeLock(1, "SMSDispatcher");
        this.mWakeLock.setReferenceCounted(true);
    }

    private boolean denyIfQueueLimitReached(SmsTracker paramSmsTracker)
    {
        if (this.mPendingTrackerCount >= 5);
        while (true)
        {
            try
            {
                paramSmsTracker.mSentIntent.send(5);
                bool = true;
                return bool;
            }
            catch (PendingIntent.CanceledException localCanceledException)
            {
                Log.e("SMS", "failed to send back RESULT_ERROR_LIMIT_EXCEEDED");
                continue;
            }
            this.mPendingTrackerCount = (1 + this.mPendingTrackerCount);
            boolean bool = false;
        }
    }

    private CharSequence getAppLabel(String paramString)
    {
        PackageManager localPackageManager = this.mContext.getPackageManager();
        try
        {
            CharSequence localCharSequence = localPackageManager.getApplicationInfo(paramString, 0).loadLabel(localPackageManager);
            paramString = localCharSequence;
            return paramString;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
                Log.e("SMS", "PackageManager Name Not Found for package " + paramString);
        }
    }

    protected static int getNextConcatenatedRef()
    {
        sConcatenatedRef = 1 + sConcatenatedRef;
        return sConcatenatedRef;
    }

    protected static void handleNotInService(int paramInt, PendingIntent paramPendingIntent)
    {
        if (paramPendingIntent != null)
        {
            if (paramInt == 3);
            try
            {
                paramPendingIntent.send(2);
                return;
                paramPendingIntent.send(4);
            }
            catch (PendingIntent.CanceledException localCanceledException)
            {
            }
        }
    }

    private void notifyAndAcknowledgeLastIncomingSms(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        if (!paramBoolean)
        {
            Intent localIntent = new Intent("android.provider.Telephony.SMS_REJECTED");
            localIntent.putExtra("result", paramInt);
            this.mWakeLock.acquire(5000L);
            this.mContext.sendBroadcast(localIntent, "android.permission.RECEIVE_SMS");
        }
        acknowledgeLastIncomingSms(paramBoolean, paramInt, paramMessage);
    }

    private void sendMultipartSms(SmsTracker paramSmsTracker)
    {
        HashMap localHashMap = paramSmsTracker.mData;
        String str1 = (String)localHashMap.get("destination");
        String str2 = (String)localHashMap.get("scaddress");
        ArrayList localArrayList1 = (ArrayList)localHashMap.get("parts");
        ArrayList localArrayList2 = (ArrayList)localHashMap.get("sentIntents");
        ArrayList localArrayList3 = (ArrayList)localHashMap.get("deliveryIntents");
        int i = this.mPhone.getServiceState().getState();
        if (i != 0)
        {
            int j = 0;
            int k = localArrayList1.size();
            while (j < k)
            {
                PendingIntent localPendingIntent = null;
                if ((localArrayList2 != null) && (localArrayList2.size() > j))
                    localPendingIntent = (PendingIntent)localArrayList2.get(j);
                handleNotInService(i, localPendingIntent);
                j++;
            }
        }
        sendMultipartText(str1, str2, localArrayList1, localArrayList2, localArrayList3);
    }

    protected abstract void acknowledgeLastIncomingSms(boolean paramBoolean, int paramInt, Message paramMessage);

    protected abstract SmsMessageBase.TextEncodingDetails calculateLength(CharSequence paramCharSequence, boolean paramBoolean);

    public void dispatch(Intent paramIntent, String paramString)
    {
        this.mWakeLock.acquire(5000L);
        this.mContext.sendOrderedBroadcast(paramIntent, paramString, this.mResultReceiver, this, -1, null, null);
    }

    public void dispatch(Intent paramIntent, String paramString, BroadcastReceiver paramBroadcastReceiver)
    {
        this.mWakeLock.acquire(5000L);
        this.mContext.sendOrderedBroadcast(paramIntent, paramString, paramBroadcastReceiver, this, -1, null, null);
    }

    protected void dispatchBroadcastMessage(SmsCbMessage paramSmsCbMessage)
    {
        if (paramSmsCbMessage.isEmergencyMessage())
        {
            Intent localIntent1 = new Intent("android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED");
            localIntent1.putExtra("message", paramSmsCbMessage);
            Log.d("SMS", "Dispatching emergency SMS CB");
            dispatch(localIntent1, "android.permission.RECEIVE_EMERGENCY_BROADCAST");
        }
        while (true)
        {
            return;
            Intent localIntent2 = new Intent("android.provider.Telephony.SMS_CB_RECEIVED");
            localIntent2.putExtra("message", paramSmsCbMessage);
            Log.d("SMS", "Dispatching SMS CB");
            dispatch(localIntent2, "android.permission.RECEIVE_SMS");
        }
    }

    public abstract int dispatchMessage(SmsMessageBase paramSmsMessageBase);

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected int dispatchNormalMessage(SmsMessageBase paramSmsMessageBase)
    {
        int i = -1;
        SmsHeader localSmsHeader = paramSmsMessageBase.getUserDataHeader();
        byte[][] arrayOfByte;
        if ((localSmsHeader == null) || (localSmsHeader.concatRef == null))
        {
            arrayOfByte = new byte[1][];
            arrayOfByte[0] = paramSmsMessageBase.getPdu();
            if ((localSmsHeader != null) && (localSmsHeader.portAddrs != null))
                if (localSmsHeader.portAddrs.destPort == 2948)
                    i = this.mWapPush.dispatchWapPdu(paramSmsMessageBase.getUserData(), paramSmsMessageBase.getOriginatingAddress());
        }
        while (true)
        {
            return i;
            dispatchPortAddressedPdus(arrayOfByte, localSmsHeader.portAddrs.destPort);
            continue;
            dispatchPdus(arrayOfByte);
            continue;
            SmsHeader.ConcatRef localConcatRef = localSmsHeader.concatRef;
            SmsHeader.PortAddrs localPortAddrs = localSmsHeader.portAddrs;
            byte[] arrayOfByte1 = paramSmsMessageBase.getPdu();
            String str = paramSmsMessageBase.getOriginatingAddress();
            int j = localConcatRef.refNumber;
            int k = localConcatRef.seqNumber;
            int m = localConcatRef.msgCount;
            long l = paramSmsMessageBase.getTimestampMillis();
            if (localPortAddrs != null)
                i = localPortAddrs.destPort;
            i = processMessagePart(arrayOfByte1, str, j, k, m, l, i, false);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void dispatchPdus(byte[][] paramArrayOfByte)
    {
        if (Injector.checkFireWallForSms(this, paramArrayOfByte));
        while (true)
        {
            return;
            Intent localIntent = new Intent("android.provider.Telephony.SMS_RECEIVED");
            localIntent.putExtra("pdus", paramArrayOfByte);
            localIntent.putExtra("format", getFormat());
            dispatch(localIntent, "android.permission.RECEIVE_SMS");
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void dispatchPortAddressedPdus(byte[][] paramArrayOfByte, int paramInt)
    {
        if (Injector.checkFireWallForSms(this, paramArrayOfByte));
        while (true)
        {
            return;
            Intent localIntent = new Intent("android.intent.action.DATA_SMS_RECEIVED", Uri.parse("sms://localhost:" + paramInt));
            localIntent.putExtra("pdus", paramArrayOfByte);
            localIntent.putExtra("format", getFormat());
            dispatch(localIntent, "android.permission.RECEIVE_SMS");
        }
    }

    public abstract void dispose();

    protected void finalize()
    {
        Log.d("SMS", "SMSDispatcher finalized");
    }

    protected abstract String getFormat();

    public void handleMessage(Message paramMessage)
    {
        int i = 1;
        switch (paramMessage.what)
        {
        case 6:
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 7:
        }
        while (true)
        {
            return;
            AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
            if (localAsyncResult.exception != null)
            {
                Log.e("SMS", "Exception processing incoming SMS. Exception:" + localAsyncResult.exception);
                continue;
            }
            SmsMessage localSmsMessage = (SmsMessage)localAsyncResult.result;
            while (true)
            {
                try
                {
                    int j = dispatchMessage(localSmsMessage.mWrappedSmsMessage);
                    if (j == -1)
                        break;
                    if (j != i)
                        break label167;
                    notifyAndAcknowledgeLastIncomingSms(i, j, null);
                }
                catch (RuntimeException localRuntimeException)
                {
                    Log.e("SMS", "Exception dispatching message", localRuntimeException);
                    notifyAndAcknowledgeLastIncomingSms(false, 2, null);
                }
                break;
                label167: i = 0;
            }
            handleSendComplete((AsyncResult)paramMessage.obj);
            continue;
            sendSms((SmsTracker)paramMessage.obj);
            continue;
            handleReachSentLimit((SmsTracker)paramMessage.obj);
            continue;
            SmsTracker localSmsTracker2 = (SmsTracker)paramMessage.obj;
            if (localSmsTracker2.isMultipart())
                sendMultipartSms(localSmsTracker2);
            while (true)
            {
                this.mPendingTrackerCount = (-1 + this.mPendingTrackerCount);
                break;
                sendSms(localSmsTracker2);
            }
            SmsTracker localSmsTracker1 = (SmsTracker)paramMessage.obj;
            if (localSmsTracker1.mSentIntent != null);
            try
            {
                localSmsTracker1.mSentIntent.send(5);
                this.mPendingTrackerCount = (-1 + this.mPendingTrackerCount);
            }
            catch (PendingIntent.CanceledException localCanceledException)
            {
                while (true)
                    Log.e("SMS", "failed to send RESULT_ERROR_LIMIT_EXCEEDED");
            }
        }
    }

    protected void handleReachSentLimit(SmsTracker paramSmsTracker)
    {
        if (denyIfQueueLimitReached(paramSmsTracker));
        while (true)
        {
            return;
            CharSequence localCharSequence = getAppLabel(paramSmsTracker.mAppPackage);
            Resources localResources = Resources.getSystem();
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = localCharSequence;
            Spanned localSpanned = Html.fromHtml(localResources.getString(17040407, arrayOfObject));
            ConfirmDialogListener localConfirmDialogListener = new ConfirmDialogListener(paramSmsTracker);
            AlertDialog localAlertDialog = new AlertDialog.Builder(this.mContext).setTitle(17040406).setIcon(17301642).setMessage(localSpanned).setPositiveButton(localResources.getString(17040408), localConfirmDialogListener).setNegativeButton(localResources.getString(17040409), localConfirmDialogListener).setOnCancelListener(localConfirmDialogListener).create();
            localAlertDialog.getWindow().setType(2003);
            localAlertDialog.show();
        }
    }

    protected void handleSendComplete(AsyncResult paramAsyncResult)
    {
        SmsTracker localSmsTracker = (SmsTracker)paramAsyncResult.userObj;
        PendingIntent localPendingIntent = localSmsTracker.mSentIntent;
        if (paramAsyncResult.exception == null)
        {
            if (localSmsTracker.mDeliveryIntent != null)
            {
                localSmsTracker.mMessageRef = ((SmsResponse)paramAsyncResult.result).messageRef;
                this.deliveryPendingList.add(localSmsTracker);
            }
            if (localPendingIntent != null)
                try
                {
                    if (this.mRemainingMessages > -1)
                        this.mRemainingMessages = (-1 + this.mRemainingMessages);
                    if (this.mRemainingMessages == 0)
                    {
                        Intent localIntent2 = new Intent();
                        localIntent2.putExtra("SendNextMsg", true);
                        localPendingIntent.send(this.mContext, -1, localIntent2);
                    }
                    else
                    {
                        localPendingIntent.send(-1);
                    }
                }
                catch (PendingIntent.CanceledException localCanceledException2)
                {
                }
        }
        else
        {
            int i = this.mPhone.getServiceState().getState();
            if (i != 0)
            {
                handleNotInService(i, localSmsTracker.mSentIntent);
            }
            else if ((((CommandException)paramAsyncResult.exception).getCommandError() == CommandException.Error.SMS_FAIL_RETRY) && (localSmsTracker.mRetryCount < 3))
            {
                localSmsTracker.mRetryCount = (1 + localSmsTracker.mRetryCount);
                sendMessageDelayed(obtainMessage(3, localSmsTracker), 2000L);
            }
            else if (localSmsTracker.mSentIntent != null)
            {
                int j = 1;
                if (((CommandException)paramAsyncResult.exception).getCommandError() == CommandException.Error.FDN_CHECK_FAILURE)
                    j = 6;
                try
                {
                    Intent localIntent1 = new Intent();
                    if (paramAsyncResult.result != null)
                        localIntent1.putExtra("errorCode", ((SmsResponse)paramAsyncResult.result).errorCode);
                    if (this.mRemainingMessages > -1)
                        this.mRemainingMessages = (-1 + this.mRemainingMessages);
                    if (this.mRemainingMessages == 0)
                        localIntent1.putExtra("SendNextMsg", true);
                    localSmsTracker.mSentIntent.send(this.mContext, j, localIntent1);
                }
                catch (PendingIntent.CanceledException localCanceledException1)
                {
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected int processMessagePart(byte[] paramArrayOfByte, String paramString, int paramInt1, int paramInt2, int paramInt3, long paramLong, int paramInt4, boolean paramBoolean)
    {
        ((byte[][])null);
        Cursor localCursor = null;
        while (true)
        {
            int i;
            byte[][] arrayOfByte1;
            int k;
            ByteArrayOutputStream localByteArrayOutputStream1;
            try
            {
                String str1 = Integer.toString(paramInt1);
                String str2 = Integer.toString(paramInt2);
                ContentResolver localContentResolver = this.mResolver;
                Uri localUri = mRawUri;
                String[] arrayOfString1 = PDU_PROJECTION;
                String[] arrayOfString2 = new String[3];
                arrayOfString2[0] = paramString;
                arrayOfString2[1] = str1;
                arrayOfString2[2] = str2;
                localCursor = localContentResolver.query(localUri, arrayOfString1, "address=? AND reference_number=? AND sequence=?", arrayOfString2, null);
                if (localCursor.moveToNext())
                {
                    Log.w("SMS", "Discarding duplicate message segment from address=" + paramString + " refNumber=" + str1 + " seqNumber=" + str2);
                    byte[] arrayOfByte5 = HexDump.hexStringToByteArray(localCursor.getString(0));
                    if (!Arrays.equals(arrayOfByte5, paramArrayOfByte))
                        Log.e("SMS", "Warning: dup message segment PDU of length " + paramArrayOfByte.length + " is different from existing PDU of length " + arrayOfByte5.length);
                    i = 1;
                    return i;
                }
                localCursor.close();
                String[] arrayOfString3 = new String[2];
                arrayOfString3[0] = paramString;
                arrayOfString3[1] = str1;
                localCursor = this.mResolver.query(mRawUri, PDU_SEQUENCE_PORT_PROJECTION, "address=? AND reference_number=?", arrayOfString3, null);
                int j = localCursor.getCount();
                if (j != paramInt3 - 1)
                {
                    ContentValues localContentValues = new ContentValues();
                    localContentValues.put("date", Long.valueOf(paramLong));
                    localContentValues.put("pdu", HexDump.toHexString(paramArrayOfByte));
                    localContentValues.put("address", paramString);
                    localContentValues.put("reference_number", Integer.valueOf(paramInt1));
                    localContentValues.put("count", Integer.valueOf(paramInt3));
                    localContentValues.put("sequence", Integer.valueOf(paramInt2));
                    if (paramInt4 != -1)
                        localContentValues.put("destination_port", Integer.valueOf(paramInt4));
                    this.mResolver.insert(mRawUri, localContentValues);
                    i = 1;
                    if (localCursor == null)
                        continue;
                    continue;
                }
                arrayOfByte1 = new byte[paramInt3][];
                k = 0;
                if (k < j)
                {
                    localCursor.moveToNext();
                    int i2 = localCursor.getInt(1);
                    if (!paramBoolean)
                        i2--;
                    arrayOfByte1[i2] = HexDump.hexStringToByteArray(localCursor.getString(0));
                    if ((i2 != 0) || (localCursor.isNull(2)))
                        break label796;
                    paramInt4 = localCursor.getInt(2);
                    break label796;
                }
                if (paramBoolean)
                {
                    arrayOfByte1[paramInt2] = paramArrayOfByte;
                    this.mResolver.delete(mRawUri, "address=? AND reference_number=?", arrayOfString3);
                    if (localCursor != null)
                        localCursor.close();
                    if (!paramBoolean)
                        break label686;
                    localByteArrayOutputStream1 = new ByteArrayOutputStream();
                    int n = 0;
                    if (n < paramInt3)
                    {
                        localByteArrayOutputStream1.write(arrayOfByte1[n], 0, arrayOfByte1[n].length);
                        n++;
                        continue;
                    }
                }
                else
                {
                    int m = paramInt2 - 1;
                    arrayOfByte1[m] = paramArrayOfByte;
                    continue;
                }
            }
            catch (SQLException localSQLException)
            {
                Log.e("SMS", "Can't access multipart SMS database", localSQLException);
                i = 2;
                if (localCursor == null)
                    continue;
                continue;
            }
            finally
            {
                if (localCursor != null)
                    localCursor.close();
            }
            byte[] arrayOfByte2 = localByteArrayOutputStream1.toByteArray();
            label796: if (paramInt4 == 2948)
            {
                i = this.mWapPush.dispatchWapPdu(arrayOfByte2, paramString);
            }
            else
            {
                byte[][] arrayOfByte3 = new byte[1][];
                arrayOfByte3[0] = arrayOfByte2;
                dispatchPortAddressedPdus(arrayOfByte3, paramInt4);
                i = -1;
                continue;
                label686: if (paramInt4 != -1)
                {
                    if (paramInt4 == 2948)
                    {
                        ByteArrayOutputStream localByteArrayOutputStream2 = new ByteArrayOutputStream();
                        for (int i1 = 0; i1 < paramInt3; i1++)
                        {
                            byte[] arrayOfByte4 = SmsMessage.createFromPdu(arrayOfByte1[i1], getFormat()).getUserData();
                            localByteArrayOutputStream2.write(arrayOfByte4, 0, arrayOfByte4.length);
                        }
                        i = this.mWapPush.dispatchWapPdu(localByteArrayOutputStream2.toByteArray(), paramString);
                    }
                    else
                    {
                        dispatchPortAddressedPdus(arrayOfByte1, paramInt4);
                    }
                }
                else
                {
                    while (true)
                    {
                        i = -1;
                        break;
                        dispatchPdus(arrayOfByte1);
                    }
                    k++;
                }
            }
        }
    }

    protected abstract void sendData(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfByte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2);

    protected void sendMultipartText(String paramString1, String paramString2, ArrayList<String> paramArrayList, ArrayList<PendingIntent> paramArrayList1, ArrayList<PendingIntent> paramArrayList2)
    {
        int i = 0xFF & getNextConcatenatedRef();
        int j = paramArrayList.size();
        int k = 0;
        this.mRemainingMessages = j;
        SmsMessageBase.TextEncodingDetails[] arrayOfTextEncodingDetails = new SmsMessageBase.TextEncodingDetails[j];
        for (int m = 0; m < j; m++)
        {
            SmsMessageBase.TextEncodingDetails localTextEncodingDetails = calculateLength((CharSequence)paramArrayList.get(m), false);
            if ((k != localTextEncodingDetails.codeUnitSize) && ((k == 0) || (k == 1)))
                k = localTextEncodingDetails.codeUnitSize;
            arrayOfTextEncodingDetails[m] = localTextEncodingDetails;
        }
        int n = 0;
        if (n < j)
        {
            SmsHeader.ConcatRef localConcatRef = new SmsHeader.ConcatRef();
            localConcatRef.refNumber = i;
            localConcatRef.seqNumber = (n + 1);
            localConcatRef.msgCount = j;
            localConcatRef.isEightBits = true;
            SmsHeader localSmsHeader = new SmsHeader();
            localSmsHeader.concatRef = localConcatRef;
            if (k == 1)
            {
                localSmsHeader.languageTable = arrayOfTextEncodingDetails[n].languageTable;
                localSmsHeader.languageShiftTable = arrayOfTextEncodingDetails[n].languageShiftTable;
            }
            PendingIntent localPendingIntent1 = null;
            if ((paramArrayList1 != null) && (paramArrayList1.size() > n))
                localPendingIntent1 = (PendingIntent)paramArrayList1.get(n);
            PendingIntent localPendingIntent2 = null;
            if ((paramArrayList2 != null) && (paramArrayList2.size() > n))
                localPendingIntent2 = (PendingIntent)paramArrayList2.get(n);
            String str = (String)paramArrayList.get(n);
            if (n == j - 1);
            for (boolean bool = true; ; bool = false)
            {
                sendNewSubmitPdu(paramString1, paramString2, str, localSmsHeader, k, localPendingIntent1, localPendingIntent2, bool);
                n++;
                break;
            }
        }
    }

    protected abstract void sendNewSubmitPdu(String paramString1, String paramString2, String paramString3, SmsHeader paramSmsHeader, int paramInt, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean);

    protected void sendRawPdu(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString)
    {
        if ((!this.mSmsSendDisabled) || (paramPendingIntent1 != null));
        try
        {
            paramPendingIntent1.send(4);
            label16: Log.d("SMS", "Device does not support sending sms.");
            while (true)
            {
                return;
                if (paramArrayOfByte2 == null)
                {
                    if (paramPendingIntent1 != null)
                        try
                        {
                            paramPendingIntent1.send(3);
                        }
                        catch (PendingIntent.CanceledException localCanceledException2)
                        {
                        }
                }
                else
                {
                    HashMap localHashMap = new HashMap();
                    localHashMap.put("smsc", paramArrayOfByte1);
                    localHashMap.put("pdu", paramArrayOfByte2);
                    String[] arrayOfString = this.mContext.getPackageManager().getPackagesForUid(Binder.getCallingUid());
                    if ((arrayOfString == null) || (arrayOfString.length == 0))
                    {
                        Log.e("SMS", "Can't get calling app package name: refusing to send SMS");
                        if (paramPendingIntent1 != null)
                            try
                            {
                                paramPendingIntent1.send(1);
                            }
                            catch (PendingIntent.CanceledException localCanceledException1)
                            {
                                Log.e("SMS", "failed to send error result");
                            }
                    }
                    else
                    {
                        String str = arrayOfString[0];
                        SmsTracker localSmsTracker = new SmsTracker(localHashMap, paramPendingIntent1, paramPendingIntent2, str, PhoneNumberUtils.extractNetworkPortion(paramString));
                        if (!this.mUsageMonitor.check(str, 1))
                        {
                            sendMessage(obtainMessage(4, localSmsTracker));
                        }
                        else
                        {
                            int i = this.mPhone.getServiceState().getState();
                            if (i != 0)
                                handleNotInService(i, localSmsTracker.mSentIntent);
                            else
                                sendSms(localSmsTracker);
                        }
                    }
                }
            }
        }
        catch (PendingIntent.CanceledException localCanceledException3)
        {
            break label16;
        }
    }

    protected abstract void sendSms(SmsTracker paramSmsTracker);

    protected abstract void sendText(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2);

    private final class ConfirmDialogListener
        implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener
    {
        private final SMSDispatcher.SmsTracker mTracker;

        ConfirmDialogListener(SMSDispatcher.SmsTracker arg2)
        {
            Object localObject;
            this.mTracker = localObject;
        }

        public void onCancel(DialogInterface paramDialogInterface)
        {
            Log.d("SMS", "dialog dismissed: don't send SMS");
            SMSDispatcher.this.sendMessage(SMSDispatcher.this.obtainMessage(7, this.mTracker));
        }

        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
            if (paramInt == -1)
            {
                Log.d("SMS", "CONFIRM sending SMS");
                SMSDispatcher.this.sendMessage(SMSDispatcher.this.obtainMessage(5, this.mTracker));
            }
            while (true)
            {
                return;
                if (paramInt == -2)
                {
                    Log.d("SMS", "DENY sending SMS");
                    SMSDispatcher.this.sendMessage(SMSDispatcher.this.obtainMessage(7, this.mTracker));
                }
            }
        }
    }

    protected static final class SmsTracker
    {
        public final String mAppPackage;
        public final HashMap<String, Object> mData;
        public final PendingIntent mDeliveryIntent;
        public final String mDestAddress;
        public int mMessageRef;
        public int mRetryCount;
        public final PendingIntent mSentIntent;

        public SmsTracker(HashMap<String, Object> paramHashMap, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString1, String paramString2)
        {
            this.mData = paramHashMap;
            this.mSentIntent = paramPendingIntent1;
            this.mDeliveryIntent = paramPendingIntent2;
            this.mRetryCount = 0;
            this.mAppPackage = paramString1;
            this.mDestAddress = paramString2;
        }

        protected boolean isMultipart()
        {
            return this.mData.containsKey("parts");
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean checkFireWallForSms(SMSDispatcher paramSMSDispatcher, byte[][] paramArrayOfByte)
        {
            boolean bool = true;
            if (ExtraTelephony.checkFirewallForSms(paramSMSDispatcher.mContext, paramArrayOfByte))
                paramSMSDispatcher.acknowledgeLastIncomingSms(bool, -1, null);
            while (true)
            {
                return bool;
                bool = false;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.SMSDispatcher
 * JD-Core Version:        0.6.2
 */