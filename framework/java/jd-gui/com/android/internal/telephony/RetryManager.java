package com.android.internal.telephony;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class RetryManager
{
    public static final boolean DBG = true;
    public static final String LOG_TAG = "GSM";
    public static final boolean VDBG;
    private String mConfig;
    private int mMaxRetryCount;
    private ArrayList<RetryRec> mRetryArray = new ArrayList();
    private int mRetryCount;
    private boolean mRetryForever;
    private Random rng = new Random();

    private void log(String paramString)
    {
        Log.d("GSM", "[RM] " + paramString);
    }

    private int nextRandomizationTime(int paramInt)
    {
        int i = ((RetryRec)this.mRetryArray.get(paramInt)).mRandomizationTime;
        if (i == 0);
        for (int j = 0; ; j = this.rng.nextInt(i))
            return j;
    }

    private Pair<Boolean, Integer> parseNonNegativeInt(String paramString1, String paramString2)
    {
        try
        {
            int i = Integer.parseInt(paramString2);
            localPair = new Pair(Boolean.valueOf(validateNonNegativeInt(paramString1, i)), Integer.valueOf(i));
            return localPair;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
            {
                Log.e("GSM", paramString1 + " bad value: " + paramString2, localNumberFormatException);
                Pair localPair = new Pair(Boolean.valueOf(false), Integer.valueOf(0));
            }
        }
    }

    private boolean validateNonNegativeInt(String paramString, int paramInt)
    {
        if (paramInt < 0)
            Log.e("GSM", paramString + " bad value: is < 0");
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public boolean configure(int paramInt1, int paramInt2, int paramInt3)
    {
        boolean bool = false;
        if (!validateNonNegativeInt("maxRetryCount", paramInt1));
        while (true)
        {
            return bool;
            if ((validateNonNegativeInt("retryTime", paramInt2)) && (validateNonNegativeInt("randomizationTime", paramInt3)))
            {
                this.mMaxRetryCount = paramInt1;
                resetRetryCount();
                this.mRetryArray.clear();
                this.mRetryArray.add(new RetryRec(paramInt2, paramInt3));
                bool = true;
            }
        }
    }

    public boolean configure(String paramString)
    {
        if ((paramString.startsWith("\"")) && (paramString.endsWith("\"")))
            paramString = paramString.substring(1, -1 + paramString.length());
        this.mConfig = paramString;
        int i;
        String[] arrayOfString1;
        int j;
        String[] arrayOfString2;
        Pair localPair4;
        boolean bool;
        if (!TextUtils.isEmpty(paramString))
        {
            i = 0;
            this.mMaxRetryCount = 0;
            resetRetryCount();
            this.mRetryArray.clear();
            arrayOfString1 = paramString.split(",");
            j = 0;
            if (j < arrayOfString1.length)
            {
                arrayOfString2 = arrayOfString1[j].split("=", 2);
                arrayOfString2[0] = arrayOfString2[0].trim();
                if (arrayOfString2.length > 1)
                {
                    arrayOfString2[1] = arrayOfString2[1].trim();
                    if (TextUtils.equals(arrayOfString2[0], "default_randomization"))
                    {
                        localPair4 = parseNonNegativeInt(arrayOfString2[0], arrayOfString2[1]);
                        if (!((Boolean)localPair4.first).booleanValue())
                            bool = false;
                    }
                }
            }
        }
        while (true)
        {
            label164: return bool;
            i = ((Integer)localPair4.second).intValue();
            while (true)
            {
                j++;
                break;
                if (!TextUtils.equals(arrayOfString2[0], "max_retries"))
                    break label267;
                if (TextUtils.equals("infinite", arrayOfString2[1]))
                {
                    this.mRetryForever = true;
                }
                else
                {
                    Pair localPair3 = parseNonNegativeInt(arrayOfString2[0], arrayOfString2[1]);
                    if (!((Boolean)localPair3.first).booleanValue())
                    {
                        bool = false;
                        break label164;
                    }
                    this.mMaxRetryCount = ((Integer)localPair3.second).intValue();
                }
            }
            label267: Log.e("GSM", "Unrecognized configuration name value pair: " + arrayOfString1[j]);
            bool = false;
            continue;
            String[] arrayOfString3 = arrayOfString1[j].split(":", 2);
            arrayOfString3[0] = arrayOfString3[0].trim();
            RetryRec localRetryRec = new RetryRec(0, 0);
            Pair localPair1 = parseNonNegativeInt("delayTime", arrayOfString3[0]);
            if (!((Boolean)localPair1.first).booleanValue())
            {
                bool = false;
            }
            else
            {
                localRetryRec.mDelayTime = ((Integer)localPair1.second).intValue();
                Pair localPair2;
                if (arrayOfString3.length > 1)
                {
                    arrayOfString3[1] = arrayOfString3[1].trim();
                    localPair2 = parseNonNegativeInt("randomizationTime", arrayOfString3[1]);
                    if (!((Boolean)localPair2.first).booleanValue())
                        bool = false;
                }
                else
                {
                    for (localRetryRec.mRandomizationTime = ((Integer)localPair2.second).intValue(); ; localRetryRec.mRandomizationTime = i)
                    {
                        this.mRetryArray.add(localRetryRec);
                        break;
                    }
                    if (this.mRetryArray.size() > this.mMaxRetryCount)
                        this.mMaxRetryCount = this.mRetryArray.size();
                    bool = true;
                    continue;
                    bool = false;
                }
            }
        }
    }

    public int getRetryCount()
    {
        log("getRetryCount: " + this.mRetryCount);
        return this.mRetryCount;
    }

    public int getRetryTimer()
    {
        int i;
        if (this.mRetryCount < this.mRetryArray.size())
        {
            i = this.mRetryCount;
            if ((i < 0) || (i >= this.mRetryArray.size()))
                break label94;
        }
        label94: for (int j = ((RetryRec)this.mRetryArray.get(i)).mDelayTime + nextRandomizationTime(i); ; j = 0)
        {
            log("getRetryTimer: " + j);
            return j;
            i = -1 + this.mRetryArray.size();
            break;
        }
    }

    public void increaseRetryCount()
    {
        this.mRetryCount = (1 + this.mRetryCount);
        if (this.mRetryCount > this.mMaxRetryCount)
            this.mRetryCount = this.mMaxRetryCount;
        log("increaseRetryCount: " + this.mRetryCount);
    }

    public boolean isRetryForever()
    {
        log("isRetryForever: " + this.mRetryForever);
        return this.mRetryForever;
    }

    public boolean isRetryNeeded()
    {
        if ((this.mRetryForever) || (this.mRetryCount < this.mMaxRetryCount));
        for (boolean bool = true; ; bool = false)
        {
            log("isRetryNeeded: " + bool);
            return bool;
        }
    }

    public void resetRetryCount()
    {
        this.mRetryCount = 0;
        log("resetRetryCount: " + this.mRetryCount);
    }

    public void retryForeverUsingLastTimeout()
    {
        this.mRetryCount = this.mMaxRetryCount;
        this.mRetryForever = true;
        log("retryForeverUsingLastTimeout: " + this.mRetryForever + ", " + this.mRetryCount);
    }

    public void setRetryCount(int paramInt)
    {
        this.mRetryCount = paramInt;
        if (this.mRetryCount > this.mMaxRetryCount)
            this.mRetryCount = this.mMaxRetryCount;
        if (this.mRetryCount < 0)
            this.mRetryCount = 0;
        log("setRetryCount: " + this.mRetryCount);
    }

    public void setRetryForever(boolean paramBoolean)
    {
        this.mRetryForever = paramBoolean;
        log("setRetryForever: " + this.mRetryForever);
    }

    public String toString()
    {
        String str = "RetryManager: forever=" + this.mRetryForever + ", maxRetry=" + this.mMaxRetryCount + ", retry=" + this.mRetryCount + ",\n        " + this.mConfig;
        Iterator localIterator = this.mRetryArray.iterator();
        while (localIterator.hasNext())
        {
            RetryRec localRetryRec = (RetryRec)localIterator.next();
            str = str + "\n        " + localRetryRec.mDelayTime + ":" + localRetryRec.mRandomizationTime;
        }
        return str;
    }

    private static class RetryRec
    {
        int mDelayTime;
        int mRandomizationTime;

        RetryRec(int paramInt1, int paramInt2)
        {
            this.mDelayTime = paramInt1;
            this.mRandomizationTime = paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.RetryManager
 * JD-Core Version:        0.6.2
 */