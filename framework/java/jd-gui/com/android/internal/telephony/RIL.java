package com.android.internal.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.LocalSocket;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.cdma.CdmaCallWaitingNotification;
import com.android.internal.telephony.cdma.CdmaInformationRecords;
import com.android.internal.telephony.cdma.CdmaInformationRecords.CdmaDisplayInfoRec;
import com.android.internal.telephony.cdma.CdmaInformationRecords.CdmaLineControlInfoRec;
import com.android.internal.telephony.cdma.CdmaInformationRecords.CdmaNumberInfoRec;
import com.android.internal.telephony.cdma.CdmaInformationRecords.CdmaRedirectingNumberInfoRec;
import com.android.internal.telephony.cdma.CdmaInformationRecords.CdmaSignalInfoRec;
import com.android.internal.telephony.cdma.CdmaInformationRecords.CdmaT53AudioControlInfoRec;
import com.android.internal.telephony.cdma.CdmaInformationRecords.CdmaT53ClirInfoRec;
import com.android.internal.telephony.gsm.SmsBroadcastConfigInfo;
import com.android.internal.telephony.gsm.SuppServiceNotification;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public final class RIL extends BaseCommands
    implements CommandsInterface
{
    private static final int CDMA_BROADCAST_SMS_NO_OF_SERVICE_CATEGORIES = 31;
    private static final int CDMA_BSI_NO_OF_INTS_STRUCT = 3;
    private static final int DEFAULT_WAKE_LOCK_TIMEOUT = 60000;
    static final int EVENT_SEND = 1;
    static final int EVENT_WAKE_LOCK_TIMEOUT = 2;
    static final String LOG_TAG = "RILJ";
    static final int RESPONSE_SOLICITED = 0;
    static final int RESPONSE_UNSOLICITED = 1;
    static final boolean RILJ_LOGD = true;
    static final boolean RILJ_LOGV = false;
    static final int RIL_MAX_COMMAND_BYTES = 8192;
    static final String SOCKET_NAME_RIL = "rild";
    static final int SOCKET_OPEN_RETRY_MILLIS = 4000;
    BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (paramAnonymousIntent.getAction().equals("android.intent.action.SCREEN_ON"))
                RIL.this.sendScreenState(true);
            while (true)
            {
                return;
                if (paramAnonymousIntent.getAction().equals("android.intent.action.SCREEN_OFF"))
                    RIL.this.sendScreenState(false);
                else
                    Log.w("RILJ", "RIL received unexpected Intent: " + paramAnonymousIntent.getAction());
            }
        }
    };
    Object mLastNITZTimeInfo;
    RILReceiver mReceiver;
    Thread mReceiverThread;
    int mRequestMessagesPending;
    int mRequestMessagesWaiting;
    ArrayList<RILRequest> mRequestsList = new ArrayList();
    RILSender mSender;
    HandlerThread mSenderThread;
    private int mSetPreferredNetworkType;
    LocalSocket mSocket;
    AtomicBoolean mTestingEmergencyCall = new AtomicBoolean(false);
    PowerManager.WakeLock mWakeLock;
    int mWakeLockTimeout;

    public RIL(Context paramContext, int paramInt1, int paramInt2)
    {
        super(paramContext);
        riljLog("RIL(context, preferredNetworkType=" + paramInt1 + " cdmaSubscription=" + paramInt2 + ")");
        this.mCdmaSubscription = paramInt2;
        this.mPreferredNetworkType = paramInt1;
        this.mPhoneType = 0;
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "RILJ");
        this.mWakeLock.setReferenceCounted(false);
        this.mWakeLockTimeout = SystemProperties.getInt("ro.ril.wake_lock_timeout", 60000);
        this.mRequestMessagesPending = 0;
        this.mRequestMessagesWaiting = 0;
        this.mSenderThread = new HandlerThread("RILSender");
        this.mSenderThread.start();
        this.mSender = new RILSender(this.mSenderThread.getLooper());
        if (!((ConnectivityManager)paramContext.getSystemService("connectivity")).isNetworkSupported(0))
            riljLog("Not starting RILReceiver: wifi-only");
        while (true)
        {
            return;
            riljLog("Starting RILReceiver");
            this.mReceiver = new RILReceiver();
            this.mReceiverThread = new Thread(this.mReceiver, "RILReceiver");
            this.mReceiverThread.start();
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("android.intent.action.SCREEN_ON");
            localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
            paramContext.registerReceiver(this.mIntentReceiver, localIntentFilter);
        }
    }

    private void acquireWakeLock()
    {
        synchronized (this.mWakeLock)
        {
            this.mWakeLock.acquire();
            this.mRequestMessagesPending = (1 + this.mRequestMessagesPending);
            this.mSender.removeMessages(2);
            Message localMessage = this.mSender.obtainMessage(2);
            this.mSender.sendMessageDelayed(localMessage, this.mWakeLockTimeout);
            return;
        }
    }

    private void clearRequestsList(int paramInt, boolean paramBoolean)
    {
        while (true)
        {
            synchronized (this.mRequestsList)
            {
                int i = this.mRequestsList.size();
                if (paramBoolean)
                {
                    Log.d("RILJ", "WAKE_LOCK_TIMEOUT    mReqPending=" + this.mRequestMessagesPending + " mRequestList=" + i);
                    break label180;
                    if (j < i)
                    {
                        RILRequest localRILRequest = (RILRequest)this.mRequestsList.get(j);
                        if (paramBoolean)
                            Log.d("RILJ", j + ": [" + localRILRequest.mSerial + "] " + requestToString(localRILRequest.mRequest));
                        localRILRequest.onError(paramInt, null);
                        localRILRequest.release();
                        j++;
                        continue;
                    }
                    this.mRequestsList.clear();
                    this.mRequestMessagesWaiting = 0;
                    return;
                }
            }
            label180: int j = 0;
        }
    }

    private RILRequest findAndRemoveRequestFromList(int paramInt)
    {
        ArrayList localArrayList = this.mRequestsList;
        for (int i = 0; ; i++)
        {
            RILRequest localRILRequest;
            try
            {
                int j = this.mRequestsList.size();
                if (i < j)
                {
                    localRILRequest = (RILRequest)this.mRequestsList.get(i);
                    if (localRILRequest.mSerial != paramInt)
                        continue;
                    this.mRequestsList.remove(i);
                    if (this.mRequestMessagesWaiting > 0)
                        this.mRequestMessagesWaiting = (-1 + this.mRequestMessagesWaiting);
                }
                else
                {
                    localRILRequest = null;
                }
            }
            finally
            {
            }
        }
    }

    private DataCallState getDataCallState(Parcel paramParcel, int paramInt)
    {
        DataCallState localDataCallState = new DataCallState();
        localDataCallState.version = paramInt;
        if (paramInt < 5)
        {
            localDataCallState.cid = paramParcel.readInt();
            localDataCallState.active = paramParcel.readInt();
            localDataCallState.type = paramParcel.readString();
            String str4 = paramParcel.readString();
            if (!TextUtils.isEmpty(str4))
                localDataCallState.addresses = str4.split(" ");
        }
        while (true)
        {
            return localDataCallState;
            localDataCallState.status = paramParcel.readInt();
            localDataCallState.suggestedRetryTime = paramParcel.readInt();
            localDataCallState.cid = paramParcel.readInt();
            localDataCallState.active = paramParcel.readInt();
            localDataCallState.type = paramParcel.readString();
            localDataCallState.ifname = paramParcel.readString();
            if ((localDataCallState.status == DataConnection.FailCause.NONE.getErrorCode()) && (TextUtils.isEmpty(localDataCallState.ifname)))
                throw new RuntimeException("getDataCallState, no ifname");
            String str1 = paramParcel.readString();
            if (!TextUtils.isEmpty(str1))
                localDataCallState.addresses = str1.split(" ");
            String str2 = paramParcel.readString();
            if (!TextUtils.isEmpty(str2))
                localDataCallState.dnses = str2.split(" ");
            String str3 = paramParcel.readString();
            if (!TextUtils.isEmpty(str3))
                localDataCallState.gateways = str3.split(" ");
        }
    }

    private CommandsInterface.RadioState getRadioStateFromInt(int paramInt)
    {
        CommandsInterface.RadioState localRadioState;
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Unrecognized RIL_RadioState: " + paramInt);
        case 0:
            localRadioState = CommandsInterface.RadioState.RADIO_OFF;
        case 1:
        case 10:
        }
        while (true)
        {
            return localRadioState;
            localRadioState = CommandsInterface.RadioState.RADIO_UNAVAILABLE;
            continue;
            localRadioState = CommandsInterface.RadioState.RADIO_ON;
        }
    }

    private void notifyRegistrantsCdmaInfoRec(CdmaInformationRecords paramCdmaInformationRecords)
    {
        if ((paramCdmaInformationRecords.record instanceof CdmaInformationRecords.CdmaDisplayInfoRec))
            if (this.mDisplayInfoRegistrants != null)
            {
                unsljLogRet(1027, paramCdmaInformationRecords.record);
                this.mDisplayInfoRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaInformationRecords.record, null));
            }
        while (true)
        {
            return;
            if ((paramCdmaInformationRecords.record instanceof CdmaInformationRecords.CdmaSignalInfoRec))
            {
                if (this.mSignalInfoRegistrants != null)
                {
                    unsljLogRet(1027, paramCdmaInformationRecords.record);
                    this.mSignalInfoRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaInformationRecords.record, null));
                }
            }
            else if ((paramCdmaInformationRecords.record instanceof CdmaInformationRecords.CdmaNumberInfoRec))
            {
                if (this.mNumberInfoRegistrants != null)
                {
                    unsljLogRet(1027, paramCdmaInformationRecords.record);
                    this.mNumberInfoRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaInformationRecords.record, null));
                }
            }
            else if ((paramCdmaInformationRecords.record instanceof CdmaInformationRecords.CdmaRedirectingNumberInfoRec))
            {
                if (this.mRedirNumInfoRegistrants != null)
                {
                    unsljLogRet(1027, paramCdmaInformationRecords.record);
                    this.mRedirNumInfoRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaInformationRecords.record, null));
                }
            }
            else if ((paramCdmaInformationRecords.record instanceof CdmaInformationRecords.CdmaLineControlInfoRec))
            {
                if (this.mLineControlInfoRegistrants != null)
                {
                    unsljLogRet(1027, paramCdmaInformationRecords.record);
                    this.mLineControlInfoRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaInformationRecords.record, null));
                }
            }
            else if ((paramCdmaInformationRecords.record instanceof CdmaInformationRecords.CdmaT53ClirInfoRec))
            {
                if (this.mT53ClirInfoRegistrants != null)
                {
                    unsljLogRet(1027, paramCdmaInformationRecords.record);
                    this.mT53ClirInfoRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaInformationRecords.record, null));
                }
            }
            else if (((paramCdmaInformationRecords.record instanceof CdmaInformationRecords.CdmaT53AudioControlInfoRec)) && (this.mT53AudCntrlInfoRegistrants != null))
            {
                unsljLogRet(1027, paramCdmaInformationRecords.record);
                this.mT53AudCntrlInfoRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaInformationRecords.record, null));
            }
        }
    }

    private void notifyRegistrantsRilConnectionChanged(int paramInt)
    {
        this.mRilVersion = paramInt;
        if (this.mRilConnectedRegistrants != null)
            this.mRilConnectedRegistrants.notifyRegistrants(new AsyncResult(null, new Integer(paramInt), null));
    }

    private void processResponse(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        if (i == 1)
            processUnsolicited(paramParcel);
        while (true)
        {
            releaseWakeLockIfDone();
            return;
            if (i == 0)
                processSolicited(paramParcel);
        }
    }

    private void processSolicited(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        int j = paramParcel.readInt();
        RILRequest localRILRequest = findAndRemoveRequestFromList(i);
        if (localRILRequest == null)
            Log.w("RILJ", "Unexpected solicited response! sn: " + i + " error: " + j);
        while (true)
        {
            return;
            Object localObject1 = null;
            if ((j == 0) || (paramParcel.dataAvail() > 0))
            {
                try
                {
                    switch (localRILRequest.mRequest)
                    {
                    default:
                        throw new RuntimeException("Unrecognized solicited response: " + localRILRequest.mRequest);
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                    case 69:
                    case 70:
                    case 71:
                    case 72:
                    case 73:
                    case 74:
                    case 75:
                    case 76:
                    case 77:
                    case 78:
                    case 79:
                    case 80:
                    case 81:
                    case 82:
                    case 83:
                    case 84:
                    case 85:
                    case 87:
                    case 88:
                    case 89:
                    case 90:
                    case 91:
                    case 92:
                    case 93:
                    case 94:
                    case 86:
                    case 95:
                    case 96:
                    case 97:
                    case 98:
                    case 100:
                    case 101:
                    case 99:
                    case 102:
                    case 103:
                    case 104:
                    case 105:
                    case 106:
                    case 107:
                    case 108:
                    }
                }
                catch (Throwable localThrowable)
                {
                    Log.w("RILJ", localRILRequest.serialString() + "< " + requestToString(localRILRequest.mRequest) + " exception, possible invalid RIL response", localThrowable);
                    if (localRILRequest.mResult != null)
                    {
                        AsyncResult.forMessage(localRILRequest.mResult, null, localThrowable);
                        localRILRequest.mResult.sendToTarget();
                    }
                    localRILRequest.release();
                }
                continue;
                Object localObject3 = responseIccCardStatus(paramParcel);
                localObject1 = localObject3;
            }
            else
            {
                while (true)
                {
                    if (j == 0)
                        break label1780;
                    localRILRequest.onError(j, localObject1);
                    localRILRequest.release();
                    break;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseCallList(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseString(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    if ((this.mTestingEmergencyCall.getAndSet(false)) && (this.mEmergencyCallbackModeRegistrant != null))
                    {
                        riljLog("testing emergency call, notify ECM Registrants");
                        this.mEmergencyCallbackModeRegistrant.notifyRegistrant();
                    }
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseSignalStrength(paramParcel);
                    continue;
                    localObject1 = responseStrings(paramParcel);
                    continue;
                    localObject1 = responseStrings(paramParcel);
                    continue;
                    localObject1 = responseStrings(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseSMS(paramParcel);
                    continue;
                    localObject1 = responseSMS(paramParcel);
                    continue;
                    localObject1 = responseSetupDataCall(paramParcel);
                    continue;
                    localObject1 = responseICC_IO(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseCallForward(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseString(paramParcel);
                    continue;
                    localObject1 = responseString(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseOperatorInfos(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseString(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseDataCallList(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseRaw(paramParcel);
                    continue;
                    localObject1 = responseStrings(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseString(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseString(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseGetPreferredNetworkType(paramParcel);
                    continue;
                    localObject1 = responseCellList(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseSMS(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseGmsBroadcastConfig(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseCdmaBroadcastConfig(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseStrings(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseStrings(paramParcel);
                    continue;
                    localObject1 = responseString(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseInts(paramParcel);
                    continue;
                    localObject1 = responseString(paramParcel);
                    continue;
                    localObject1 = responseVoid(paramParcel);
                    continue;
                    localObject1 = responseICC_IO(paramParcel);
                    continue;
                    Object localObject2 = responseInts(paramParcel);
                    localObject1 = localObject2;
                }
                label1780: riljLog(localRILRequest.serialString() + "< " + requestToString(localRILRequest.mRequest) + " " + retToString(localRILRequest.mRequest, localObject1));
                if (localRILRequest.mResult != null)
                {
                    AsyncResult.forMessage(localRILRequest.mResult, localObject1, null);
                    localRILRequest.mResult.sendToTarget();
                }
                localRILRequest.release();
            }
        }
    }

    private void processUnsolicited(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        switch (i)
        {
        case 1007:
        default:
            try
            {
                throw new RuntimeException("Unrecognized unsol response: " + i);
            }
            catch (Throwable localThrowable)
            {
                Log.e("RILJ", "Exception processing unsol response: " + i + "Exception:" + localThrowable.toString());
            }
        case 1000:
        case 1001:
        case 1002:
        case 1003:
        case 1004:
        case 1005:
        case 1006:
        case 1008:
        case 1009:
        case 1010:
        case 1011:
        case 1012:
        case 1013:
        case 1014:
        case 1015:
        case 1016:
        case 1017:
        case 1018:
        case 1023:
        case 1019:
        case 1020:
        case 1021:
        case 1022:
        case 1024:
        case 1025:
        case 1026:
        case 1027:
        case 1028:
        case 1029:
        case 1030:
        case 1031:
        case 1032:
        case 1033:
        case 1034:
        case 1035:
        }
        while (true)
        {
            return;
            Object localObject3 = responseVoid(paramParcel);
            Object localObject2 = localObject3;
            while (true)
                switch (i)
                {
                case 1007:
                default:
                    break;
                case 1000:
                    CommandsInterface.RadioState localRadioState = getRadioStateFromInt(paramParcel.readInt());
                    unsljLogMore(i, localRadioState.toString());
                    switchToRadioState(localRadioState);
                    break;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseString(paramParcel);
                    continue;
                    localObject2 = responseString(paramParcel);
                    continue;
                    localObject2 = responseInts(paramParcel);
                    continue;
                    localObject2 = responseStrings(paramParcel);
                    continue;
                    localObject2 = responseString(paramParcel);
                    continue;
                    localObject2 = responseSignalStrength(paramParcel);
                    continue;
                    localObject2 = responseDataCallList(paramParcel);
                    continue;
                    localObject2 = responseSuppServiceNotification(paramParcel);
                    continue;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseString(paramParcel);
                    continue;
                    localObject2 = responseString(paramParcel);
                    continue;
                    localObject2 = responseInts(paramParcel);
                    continue;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseSimRefresh(paramParcel);
                    continue;
                    localObject2 = responseCallRing(paramParcel);
                    continue;
                    localObject2 = responseInts(paramParcel);
                    continue;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseCdmaSms(paramParcel);
                    continue;
                    localObject2 = responseRaw(paramParcel);
                    continue;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseCdmaCallWaiting(paramParcel);
                    continue;
                    localObject2 = responseInts(paramParcel);
                    continue;
                    localObject2 = responseCdmaInformationRecord(paramParcel);
                    continue;
                    localObject2 = responseRaw(paramParcel);
                    continue;
                    localObject2 = responseInts(paramParcel);
                    continue;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseInts(paramParcel);
                    continue;
                    localObject2 = responseInts(paramParcel);
                    continue;
                    localObject2 = responseVoid(paramParcel);
                    continue;
                    localObject2 = responseInts(paramParcel);
                    continue;
                    Object localObject1 = responseInts(paramParcel);
                    localObject2 = localObject1;
                case 1001:
                case 1002:
                case 1003:
                case 1004:
                case 1005:
                case 1006:
                case 1008:
                case 1009:
                case 1010:
                case 1011:
                case 1012:
                case 1013:
                case 1014:
                case 1015:
                case 1016:
                case 1017:
                case 1018:
                case 1023:
                case 1019:
                case 1020:
                case 1021:
                case 1022:
                case 1024:
                case 1025:
                case 1026:
                case 1027:
                case 1028:
                case 1029:
                case 1030:
                case 1035:
                case 1031:
                case 1032:
                case 1033:
                case 1034:
                }
            unsljLog(i);
            this.mCallStateRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
            continue;
            unsljLog(i);
            this.mVoiceNetworkStateRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
            continue;
            unsljLog(i);
            String[] arrayOfString2 = new String[2];
            arrayOfString2[1] = ((String)localObject2);
            SmsMessage localSmsMessage2 = SmsMessage.newFromCMT(arrayOfString2);
            if (this.mGsmSmsRegistrant != null)
            {
                Registrant localRegistrant16 = this.mGsmSmsRegistrant;
                AsyncResult localAsyncResult24 = new AsyncResult(null, localSmsMessage2, null);
                localRegistrant16.notifyRegistrant(localAsyncResult24);
                continue;
                unsljLogRet(i, localObject2);
                if (this.mSmsStatusRegistrant != null)
                {
                    Registrant localRegistrant15 = this.mSmsStatusRegistrant;
                    AsyncResult localAsyncResult23 = new AsyncResult(null, localObject2, null);
                    localRegistrant15.notifyRegistrant(localAsyncResult23);
                    continue;
                    unsljLogRet(i, localObject2);
                    int[] arrayOfInt = (int[])localObject2;
                    if (arrayOfInt.length == 1)
                    {
                        if (this.mSmsOnSimRegistrant != null)
                        {
                            Registrant localRegistrant14 = this.mSmsOnSimRegistrant;
                            AsyncResult localAsyncResult22 = new AsyncResult(null, arrayOfInt, null);
                            localRegistrant14.notifyRegistrant(localAsyncResult22);
                        }
                    }
                    else
                    {
                        riljLog(" NEW_SMS_ON_SIM ERROR with wrong length " + arrayOfInt.length);
                        continue;
                        String[] arrayOfString1 = (String[])localObject2;
                        if (arrayOfString1.length < 2)
                        {
                            arrayOfString1 = new String[2];
                            arrayOfString1[0] = ((String[])(String[])localObject2)[0];
                            arrayOfString1[1] = null;
                        }
                        unsljLogMore(i, arrayOfString1[0]);
                        if (this.mUSSDRegistrant != null)
                        {
                            Registrant localRegistrant13 = this.mUSSDRegistrant;
                            AsyncResult localAsyncResult21 = new AsyncResult(null, arrayOfString1, null);
                            localRegistrant13.notifyRegistrant(localAsyncResult21);
                            continue;
                            unsljLogRet(i, localObject2);
                            long l = paramParcel.readLong();
                            Object[] arrayOfObject = new Object[2];
                            arrayOfObject[0] = localObject2;
                            arrayOfObject[1] = Long.valueOf(l);
                            if (SystemProperties.getBoolean("telephony.test.ignore.nitz", false))
                            {
                                riljLog("ignoring UNSOL_NITZ_TIME_RECEIVED");
                            }
                            else if (this.mNITZTimeRegistrant != null)
                            {
                                Registrant localRegistrant12 = this.mNITZTimeRegistrant;
                                AsyncResult localAsyncResult20 = new AsyncResult(null, arrayOfObject, null);
                                localRegistrant12.notifyRegistrant(localAsyncResult20);
                            }
                            else
                            {
                                this.mLastNITZTimeInfo = arrayOfObject;
                                continue;
                                if (this.mSignalStrengthRegistrant != null)
                                {
                                    Registrant localRegistrant11 = this.mSignalStrengthRegistrant;
                                    AsyncResult localAsyncResult19 = new AsyncResult(null, localObject2, null);
                                    localRegistrant11.notifyRegistrant(localAsyncResult19);
                                    continue;
                                    unsljLogRet(i, localObject2);
                                    RegistrantList localRegistrantList8 = this.mDataNetworkStateRegistrants;
                                    AsyncResult localAsyncResult18 = new AsyncResult(null, localObject2, null);
                                    localRegistrantList8.notifyRegistrants(localAsyncResult18);
                                    continue;
                                    unsljLogRet(i, localObject2);
                                    if (this.mSsnRegistrant != null)
                                    {
                                        Registrant localRegistrant10 = this.mSsnRegistrant;
                                        AsyncResult localAsyncResult17 = new AsyncResult(null, localObject2, null);
                                        localRegistrant10.notifyRegistrant(localAsyncResult17);
                                        continue;
                                        unsljLog(i);
                                        if (this.mCatSessionEndRegistrant != null)
                                        {
                                            Registrant localRegistrant9 = this.mCatSessionEndRegistrant;
                                            AsyncResult localAsyncResult16 = new AsyncResult(null, localObject2, null);
                                            localRegistrant9.notifyRegistrant(localAsyncResult16);
                                            continue;
                                            unsljLogRet(i, localObject2);
                                            if (this.mCatProCmdRegistrant != null)
                                            {
                                                Registrant localRegistrant8 = this.mCatProCmdRegistrant;
                                                AsyncResult localAsyncResult15 = new AsyncResult(null, localObject2, null);
                                                localRegistrant8.notifyRegistrant(localAsyncResult15);
                                                continue;
                                                unsljLogRet(i, localObject2);
                                                if (this.mCatEventRegistrant != null)
                                                {
                                                    Registrant localRegistrant7 = this.mCatEventRegistrant;
                                                    AsyncResult localAsyncResult14 = new AsyncResult(null, localObject2, null);
                                                    localRegistrant7.notifyRegistrant(localAsyncResult14);
                                                    continue;
                                                    unsljLogRet(i, localObject2);
                                                    if (this.mCatCallSetUpRegistrant != null)
                                                    {
                                                        Registrant localRegistrant6 = this.mCatCallSetUpRegistrant;
                                                        AsyncResult localAsyncResult13 = new AsyncResult(null, localObject2, null);
                                                        localRegistrant6.notifyRegistrant(localAsyncResult13);
                                                        continue;
                                                        unsljLog(i);
                                                        if (this.mIccSmsFullRegistrant != null)
                                                        {
                                                            this.mIccSmsFullRegistrant.notifyRegistrant();
                                                            continue;
                                                            unsljLogRet(i, localObject2);
                                                            if (this.mIccRefreshRegistrants != null)
                                                            {
                                                                RegistrantList localRegistrantList7 = this.mIccRefreshRegistrants;
                                                                AsyncResult localAsyncResult12 = new AsyncResult(null, localObject2, null);
                                                                localRegistrantList7.notifyRegistrants(localAsyncResult12);
                                                                continue;
                                                                unsljLogRet(i, localObject2);
                                                                if (this.mRingRegistrant != null)
                                                                {
                                                                    Registrant localRegistrant5 = this.mRingRegistrant;
                                                                    AsyncResult localAsyncResult11 = new AsyncResult(null, localObject2, null);
                                                                    localRegistrant5.notifyRegistrant(localAsyncResult11);
                                                                    continue;
                                                                    unsljLogvRet(i, localObject2);
                                                                    if (this.mRestrictedStateRegistrant != null)
                                                                    {
                                                                        Registrant localRegistrant4 = this.mRestrictedStateRegistrant;
                                                                        AsyncResult localAsyncResult10 = new AsyncResult(null, localObject2, null);
                                                                        localRegistrant4.notifyRegistrant(localAsyncResult10);
                                                                        continue;
                                                                        unsljLog(i);
                                                                        if (this.mIccStatusChangedRegistrants != null)
                                                                        {
                                                                            this.mIccStatusChangedRegistrants.notifyRegistrants();
                                                                            continue;
                                                                            unsljLog(i);
                                                                            SmsMessage localSmsMessage1 = (SmsMessage)localObject2;
                                                                            if (this.mCdmaSmsRegistrant != null)
                                                                            {
                                                                                Registrant localRegistrant3 = this.mCdmaSmsRegistrant;
                                                                                AsyncResult localAsyncResult9 = new AsyncResult(null, localSmsMessage1, null);
                                                                                localRegistrant3.notifyRegistrant(localAsyncResult9);
                                                                                continue;
                                                                                unsljLog(i);
                                                                                if (this.mGsmBroadcastSmsRegistrant != null)
                                                                                {
                                                                                    Registrant localRegistrant2 = this.mGsmBroadcastSmsRegistrant;
                                                                                    AsyncResult localAsyncResult8 = new AsyncResult(null, localObject2, null);
                                                                                    localRegistrant2.notifyRegistrant(localAsyncResult8);
                                                                                    continue;
                                                                                    unsljLog(i);
                                                                                    if (this.mIccSmsFullRegistrant != null)
                                                                                    {
                                                                                        this.mIccSmsFullRegistrant.notifyRegistrant();
                                                                                        continue;
                                                                                        unsljLog(i);
                                                                                        if (this.mEmergencyCallbackModeRegistrant != null)
                                                                                        {
                                                                                            this.mEmergencyCallbackModeRegistrant.notifyRegistrant();
                                                                                            continue;
                                                                                            unsljLogRet(i, localObject2);
                                                                                            if (this.mCallWaitingInfoRegistrants != null)
                                                                                            {
                                                                                                RegistrantList localRegistrantList6 = this.mCallWaitingInfoRegistrants;
                                                                                                AsyncResult localAsyncResult7 = new AsyncResult(null, localObject2, null);
                                                                                                localRegistrantList6.notifyRegistrants(localAsyncResult7);
                                                                                                continue;
                                                                                                unsljLogRet(i, localObject2);
                                                                                                if (this.mOtaProvisionRegistrants != null)
                                                                                                {
                                                                                                    RegistrantList localRegistrantList5 = this.mOtaProvisionRegistrants;
                                                                                                    AsyncResult localAsyncResult6 = new AsyncResult(null, localObject2, null);
                                                                                                    localRegistrantList5.notifyRegistrants(localAsyncResult6);
                                                                                                    continue;
                                                                                                    try
                                                                                                    {
                                                                                                        ArrayList localArrayList = (ArrayList)localObject2;
                                                                                                        Iterator localIterator = localArrayList.iterator();
                                                                                                        while (localIterator.hasNext())
                                                                                                        {
                                                                                                            CdmaInformationRecords localCdmaInformationRecords = (CdmaInformationRecords)localIterator.next();
                                                                                                            unsljLogRet(i, localCdmaInformationRecords);
                                                                                                            notifyRegistrantsCdmaInfoRec(localCdmaInformationRecords);
                                                                                                        }
                                                                                                    }
                                                                                                    catch (ClassCastException localClassCastException)
                                                                                                    {
                                                                                                        Log.e("RILJ", "Unexpected exception casting to listInfoRecs", localClassCastException);
                                                                                                    }
                                                                                                    continue;
                                                                                                    unsljLogvRet(i, IccUtils.bytesToHexString((byte[])localObject2));
                                                                                                    if (this.mUnsolOemHookRawRegistrant != null)
                                                                                                    {
                                                                                                        Registrant localRegistrant1 = this.mUnsolOemHookRawRegistrant;
                                                                                                        AsyncResult localAsyncResult5 = new AsyncResult(null, localObject2, null);
                                                                                                        localRegistrant1.notifyRegistrant(localAsyncResult5);
                                                                                                        continue;
                                                                                                        unsljLogvRet(i, localObject2);
                                                                                                        if (this.mRingbackToneRegistrants != null)
                                                                                                        {
                                                                                                            if (((int[])(int[])localObject2)[0] == 1);
                                                                                                            for (boolean bool = true; ; bool = false)
                                                                                                            {
                                                                                                                this.mRingbackToneRegistrants.notifyRegistrants(new AsyncResult(null, Boolean.valueOf(bool), null));
                                                                                                                break;
                                                                                                            }
                                                                                                            unsljLogRet(i, localObject2);
                                                                                                            if (this.mResendIncallMuteRegistrants != null)
                                                                                                            {
                                                                                                                RegistrantList localRegistrantList4 = this.mResendIncallMuteRegistrants;
                                                                                                                AsyncResult localAsyncResult4 = new AsyncResult(null, localObject2, null);
                                                                                                                localRegistrantList4.notifyRegistrants(localAsyncResult4);
                                                                                                                continue;
                                                                                                                unsljLogRet(i, localObject2);
                                                                                                                if (this.mVoiceRadioTechChangedRegistrants != null)
                                                                                                                {
                                                                                                                    RegistrantList localRegistrantList3 = this.mVoiceRadioTechChangedRegistrants;
                                                                                                                    AsyncResult localAsyncResult3 = new AsyncResult(null, localObject2, null);
                                                                                                                    localRegistrantList3.notifyRegistrants(localAsyncResult3);
                                                                                                                    continue;
                                                                                                                    unsljLogRet(i, localObject2);
                                                                                                                    if (this.mCdmaSubscriptionChangedRegistrants != null)
                                                                                                                    {
                                                                                                                        RegistrantList localRegistrantList2 = this.mCdmaSubscriptionChangedRegistrants;
                                                                                                                        AsyncResult localAsyncResult2 = new AsyncResult(null, localObject2, null);
                                                                                                                        localRegistrantList2.notifyRegistrants(localAsyncResult2);
                                                                                                                        continue;
                                                                                                                        unsljLogRet(i, localObject2);
                                                                                                                        if (this.mCdmaPrlChangedRegistrants != null)
                                                                                                                        {
                                                                                                                            RegistrantList localRegistrantList1 = this.mCdmaPrlChangedRegistrants;
                                                                                                                            AsyncResult localAsyncResult1 = new AsyncResult(null, localObject2, null);
                                                                                                                            localRegistrantList1.notifyRegistrants(localAsyncResult1);
                                                                                                                            continue;
                                                                                                                            unsljLogRet(i, localObject2);
                                                                                                                            if (this.mExitEmergencyCallbackModeRegistrants != null)
                                                                                                                            {
                                                                                                                                this.mExitEmergencyCallbackModeRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
                                                                                                                                continue;
                                                                                                                                unsljLogRet(i, localObject2);
                                                                                                                                setRadioPower(false, null);
                                                                                                                                setPreferredNetworkType(this.mPreferredNetworkType, null);
                                                                                                                                setCdmaSubscriptionSource(this.mCdmaSubscription, null);
                                                                                                                                notifyRegistrantsRilConnectionChanged(((int[])(int[])localObject2)[0]);
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static int readRilMessage(InputStream paramInputStream, byte[] paramArrayOfByte)
        throws IOException
    {
        int i = 0;
        int j = 4;
        int k = paramInputStream.read(paramArrayOfByte, i, j);
        int m;
        if (k < 0)
        {
            Log.e("RILJ", "Hit EOS reading message length");
            m = -1;
        }
        while (true)
        {
            return m;
            i += k;
            j -= k;
            if (j > 0)
                break;
            m = (0xFF & paramArrayOfByte[0]) << 24 | (0xFF & paramArrayOfByte[1]) << 16 | (0xFF & paramArrayOfByte[2]) << 8 | 0xFF & paramArrayOfByte[3];
            int n = 0;
            int i1 = m;
            do
            {
                int i2 = paramInputStream.read(paramArrayOfByte, n, i1);
                if (i2 < 0)
                {
                    Log.e("RILJ", "Hit EOS reading message.    messageLength=" + m + " remaining=" + i1);
                    m = -1;
                    break;
                }
                n += i2;
                i1 -= i2;
            }
            while (i1 > 0);
        }
    }

    private void releaseWakeLockIfDone()
    {
        synchronized (this.mWakeLock)
        {
            if ((this.mWakeLock.isHeld()) && (this.mRequestMessagesPending == 0) && (this.mRequestMessagesWaiting == 0))
            {
                this.mSender.removeMessages(2);
                this.mWakeLock.release();
            }
            return;
        }
    }

    static String requestToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "<unknown request>";
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
        case 26:
        case 27:
        case 28:
        case 29:
        case 30:
        case 31:
        case 32:
        case 33:
        case 34:
        case 35:
        case 36:
        case 37:
        case 38:
        case 39:
        case 40:
        case 41:
        case 42:
        case 43:
        case 44:
        case 45:
        case 46:
        case 47:
        case 48:
        case 49:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:
        case 58:
        case 59:
        case 60:
        case 61:
        case 62:
        case 63:
        case 64:
        case 65:
        case 66:
        case 67:
        case 68:
        case 69:
        case 70:
        case 71:
        case 72:
        case 73:
        case 74:
        case 75:
        case 76:
        case 77:
        case 78:
        case 79:
        case 80:
        case 81:
        case 82:
        case 83:
        case 84:
        case 85:
        case 87:
        case 88:
        case 89:
        case 90:
        case 92:
        case 93:
        case 91:
        case 86:
        case 94:
        case 95:
        case 96:
        case 97:
        case 98:
        case 100:
        case 101:
        case 99:
        case 102:
        case 103:
        case 104:
        case 105:
        case 106:
        case 107:
        case 108:
        }
        while (true)
        {
            return str;
            str = "GET_SIM_STATUS";
            continue;
            str = "ENTER_SIM_PIN";
            continue;
            str = "ENTER_SIM_PUK";
            continue;
            str = "ENTER_SIM_PIN2";
            continue;
            str = "ENTER_SIM_PUK2";
            continue;
            str = "CHANGE_SIM_PIN";
            continue;
            str = "CHANGE_SIM_PIN2";
            continue;
            str = "ENTER_NETWORK_DEPERSONALIZATION";
            continue;
            str = "GET_CURRENT_CALLS";
            continue;
            str = "DIAL";
            continue;
            str = "GET_IMSI";
            continue;
            str = "HANGUP";
            continue;
            str = "HANGUP_WAITING_OR_BACKGROUND";
            continue;
            str = "HANGUP_FOREGROUND_RESUME_BACKGROUND";
            continue;
            str = "REQUEST_SWITCH_WAITING_OR_HOLDING_AND_ACTIVE";
            continue;
            str = "CONFERENCE";
            continue;
            str = "UDUB";
            continue;
            str = "LAST_CALL_FAIL_CAUSE";
            continue;
            str = "SIGNAL_STRENGTH";
            continue;
            str = "VOICE_REGISTRATION_STATE";
            continue;
            str = "DATA_REGISTRATION_STATE";
            continue;
            str = "OPERATOR";
            continue;
            str = "RADIO_POWER";
            continue;
            str = "DTMF";
            continue;
            str = "SEND_SMS";
            continue;
            str = "SEND_SMS_EXPECT_MORE";
            continue;
            str = "SETUP_DATA_CALL";
            continue;
            str = "SIM_IO";
            continue;
            str = "SEND_USSD";
            continue;
            str = "CANCEL_USSD";
            continue;
            str = "GET_CLIR";
            continue;
            str = "SET_CLIR";
            continue;
            str = "QUERY_CALL_FORWARD_STATUS";
            continue;
            str = "SET_CALL_FORWARD";
            continue;
            str = "QUERY_CALL_WAITING";
            continue;
            str = "SET_CALL_WAITING";
            continue;
            str = "SMS_ACKNOWLEDGE";
            continue;
            str = "GET_IMEI";
            continue;
            str = "GET_IMEISV";
            continue;
            str = "ANSWER";
            continue;
            str = "DEACTIVATE_DATA_CALL";
            continue;
            str = "QUERY_FACILITY_LOCK";
            continue;
            str = "SET_FACILITY_LOCK";
            continue;
            str = "CHANGE_BARRING_PASSWORD";
            continue;
            str = "QUERY_NETWORK_SELECTION_MODE";
            continue;
            str = "SET_NETWORK_SELECTION_AUTOMATIC";
            continue;
            str = "SET_NETWORK_SELECTION_MANUAL";
            continue;
            str = "QUERY_AVAILABLE_NETWORKS ";
            continue;
            str = "DTMF_START";
            continue;
            str = "DTMF_STOP";
            continue;
            str = "BASEBAND_VERSION";
            continue;
            str = "SEPARATE_CONNECTION";
            continue;
            str = "SET_MUTE";
            continue;
            str = "GET_MUTE";
            continue;
            str = "QUERY_CLIP";
            continue;
            str = "LAST_DATA_CALL_FAIL_CAUSE";
            continue;
            str = "DATA_CALL_LIST";
            continue;
            str = "RESET_RADIO";
            continue;
            str = "OEM_HOOK_RAW";
            continue;
            str = "OEM_HOOK_STRINGS";
            continue;
            str = "SCREEN_STATE";
            continue;
            str = "SET_SUPP_SVC_NOTIFICATION";
            continue;
            str = "WRITE_SMS_TO_SIM";
            continue;
            str = "DELETE_SMS_ON_SIM";
            continue;
            str = "SET_BAND_MODE";
            continue;
            str = "QUERY_AVAILABLE_BAND_MODE";
            continue;
            str = "REQUEST_STK_GET_PROFILE";
            continue;
            str = "REQUEST_STK_SET_PROFILE";
            continue;
            str = "REQUEST_STK_SEND_ENVELOPE_COMMAND";
            continue;
            str = "REQUEST_STK_SEND_TERMINAL_RESPONSE";
            continue;
            str = "REQUEST_STK_HANDLE_CALL_SETUP_REQUESTED_FROM_SIM";
            continue;
            str = "REQUEST_EXPLICIT_CALL_TRANSFER";
            continue;
            str = "REQUEST_SET_PREFERRED_NETWORK_TYPE";
            continue;
            str = "REQUEST_GET_PREFERRED_NETWORK_TYPE";
            continue;
            str = "REQUEST_GET_NEIGHBORING_CELL_IDS";
            continue;
            str = "REQUEST_SET_LOCATION_UPDATES";
            continue;
            str = "RIL_REQUEST_CDMA_SET_SUBSCRIPTION_SOURCE";
            continue;
            str = "RIL_REQUEST_CDMA_SET_ROAMING_PREFERENCE";
            continue;
            str = "RIL_REQUEST_CDMA_QUERY_ROAMING_PREFERENCE";
            continue;
            str = "RIL_REQUEST_SET_TTY_MODE";
            continue;
            str = "RIL_REQUEST_QUERY_TTY_MODE";
            continue;
            str = "RIL_REQUEST_CDMA_SET_PREFERRED_VOICE_PRIVACY_MODE";
            continue;
            str = "RIL_REQUEST_CDMA_QUERY_PREFERRED_VOICE_PRIVACY_MODE";
            continue;
            str = "RIL_REQUEST_CDMA_FLASH";
            continue;
            str = "RIL_REQUEST_CDMA_BURST_DTMF";
            continue;
            str = "RIL_REQUEST_CDMA_SEND_SMS";
            continue;
            str = "RIL_REQUEST_CDMA_SMS_ACKNOWLEDGE";
            continue;
            str = "RIL_REQUEST_GSM_GET_BROADCAST_CONFIG";
            continue;
            str = "RIL_REQUEST_GSM_SET_BROADCAST_CONFIG";
            continue;
            str = "RIL_REQUEST_CDMA_GET_BROADCAST_CONFIG";
            continue;
            str = "RIL_REQUEST_CDMA_SET_BROADCAST_CONFIG";
            continue;
            str = "RIL_REQUEST_GSM_BROADCAST_ACTIVATION";
            continue;
            str = "RIL_REQUEST_CDMA_VALIDATE_AND_WRITE_AKEY";
            continue;
            str = "RIL_REQUEST_CDMA_BROADCAST_ACTIVATION";
            continue;
            str = "RIL_REQUEST_CDMA_SUBSCRIPTION";
            continue;
            str = "RIL_REQUEST_CDMA_WRITE_SMS_TO_RUIM";
            continue;
            str = "RIL_REQUEST_CDMA_DELETE_SMS_ON_RUIM";
            continue;
            str = "RIL_REQUEST_DEVICE_IDENTITY";
            continue;
            str = "RIL_REQUEST_GET_SMSC_ADDRESS";
            continue;
            str = "RIL_REQUEST_SET_SMSC_ADDRESS";
            continue;
            str = "REQUEST_EXIT_EMERGENCY_CALLBACK_MODE";
            continue;
            str = "RIL_REQUEST_REPORT_SMS_MEMORY_STATUS";
            continue;
            str = "RIL_REQUEST_REPORT_STK_SERVICE_IS_RUNNING";
            continue;
            str = "RIL_REQUEST_CDMA_GET_SUBSCRIPTION_SOURCE";
            continue;
            str = "RIL_REQUEST_ISIM_AUTHENTICATION";
            continue;
            str = "RIL_REQUEST_ACKNOWLEDGE_INCOMING_GSM_SMS_WITH_PDU";
            continue;
            str = "RIL_REQUEST_STK_SEND_ENVELOPE_WITH_STATUS";
            continue;
            str = "RIL_REQUEST_VOICE_RADIO_TECH";
        }
    }

    private Object responseCallForward(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        CallForwardInfo[] arrayOfCallForwardInfo = new CallForwardInfo[i];
        for (int j = 0; j < i; j++)
        {
            arrayOfCallForwardInfo[j] = new CallForwardInfo();
            arrayOfCallForwardInfo[j].status = paramParcel.readInt();
            arrayOfCallForwardInfo[j].reason = paramParcel.readInt();
            arrayOfCallForwardInfo[j].serviceClass = paramParcel.readInt();
            arrayOfCallForwardInfo[j].toa = paramParcel.readInt();
            arrayOfCallForwardInfo[j].number = paramParcel.readString();
            arrayOfCallForwardInfo[j].timeSeconds = paramParcel.readInt();
        }
        return arrayOfCallForwardInfo;
    }

    private Object responseCallList(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        ArrayList localArrayList = new ArrayList(i);
        int j = 0;
        if (j < i)
        {
            DriverCall localDriverCall = new DriverCall();
            localDriverCall.state = DriverCall.stateFromCLCC(paramParcel.readInt());
            localDriverCall.index = paramParcel.readInt();
            localDriverCall.TOA = paramParcel.readInt();
            boolean bool1;
            label72: boolean bool2;
            label89: boolean bool3;
            label115: boolean bool4;
            if (paramParcel.readInt() != 0)
            {
                bool1 = true;
                localDriverCall.isMpty = bool1;
                if (paramParcel.readInt() == 0)
                    break label433;
                bool2 = true;
                localDriverCall.isMT = bool2;
                localDriverCall.als = paramParcel.readInt();
                if (paramParcel.readInt() != 0)
                    break label439;
                bool3 = false;
                localDriverCall.isVoice = bool3;
                if (paramParcel.readInt() == 0)
                    break label445;
                bool4 = true;
                label132: localDriverCall.isVoicePrivacy = bool4;
                localDriverCall.number = paramParcel.readString();
                localDriverCall.numberPresentation = DriverCall.presentationFromCLIP(paramParcel.readInt());
                localDriverCall.name = paramParcel.readString();
                localDriverCall.namePresentation = paramParcel.readInt();
                if (paramParcel.readInt() != 1)
                    break label451;
                localDriverCall.uusInfo = new UUSInfo();
                localDriverCall.uusInfo.setType(paramParcel.readInt());
                localDriverCall.uusInfo.setDcs(paramParcel.readInt());
                byte[] arrayOfByte = paramParcel.createByteArray();
                localDriverCall.uusInfo.setUserData(arrayOfByte);
                Object[] arrayOfObject = new Object[3];
                arrayOfObject[0] = Integer.valueOf(localDriverCall.uusInfo.getType());
                arrayOfObject[1] = Integer.valueOf(localDriverCall.uusInfo.getDcs());
                arrayOfObject[2] = Integer.valueOf(localDriverCall.uusInfo.getUserData().length);
                riljLogv(String.format("Incoming UUS : type=%d, dcs=%d, length=%d", arrayOfObject));
                riljLogv("Incoming UUS : data (string)=" + new String(localDriverCall.uusInfo.getUserData()));
                riljLogv("Incoming UUS : data (hex): " + IccUtils.bytesToHexString(localDriverCall.uusInfo.getUserData()));
                label374: localDriverCall.number = PhoneNumberUtils.stringFromStringAndTOA(localDriverCall.number, localDriverCall.TOA);
                localArrayList.add(localDriverCall);
                if (!localDriverCall.isVoicePrivacy)
                    break label461;
                this.mVoicePrivacyOnRegistrants.notifyRegistrants();
                riljLog("InCall VoicePrivacy is enabled");
            }
            while (true)
            {
                j++;
                break;
                bool1 = false;
                break label72;
                label433: bool2 = false;
                break label89;
                label439: bool3 = true;
                break label115;
                label445: bool4 = false;
                break label132;
                label451: riljLogv("Incoming UUS : NOT present!");
                break label374;
                label461: this.mVoicePrivacyOffRegistrants.notifyRegistrants();
                riljLog("InCall VoicePrivacy is disabled");
            }
        }
        Collections.sort(localArrayList);
        if ((i == 0) && (this.mTestingEmergencyCall.getAndSet(false)) && (this.mEmergencyCallbackModeRegistrant != null))
        {
            riljLog("responseCallList: call ended, testing emergency call, notify ECM Registrants");
            this.mEmergencyCallbackModeRegistrant.notifyRegistrant();
        }
        return localArrayList;
    }

    private Object responseCallRing(Parcel paramParcel)
    {
        char[] arrayOfChar = new char[4];
        arrayOfChar[0] = ((char)paramParcel.readInt());
        arrayOfChar[1] = ((char)paramParcel.readInt());
        arrayOfChar[2] = ((char)paramParcel.readInt());
        arrayOfChar[3] = ((char)paramParcel.readInt());
        return arrayOfChar;
    }

    private Object responseCdmaBroadcastConfig(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        if (i == 0)
        {
            arrayOfInt = new int[94];
            arrayOfInt[0] = 31;
            for (int m = 1; m < 94; m += 3)
            {
                arrayOfInt[(m + 0)] = (m / 3);
                arrayOfInt[(m + 1)] = 1;
                arrayOfInt[(m + 2)] = 0;
            }
        }
        int j = 1 + i * 3;
        int[] arrayOfInt = new int[j];
        arrayOfInt[0] = i;
        for (int k = 1; k < j; k++)
            arrayOfInt[k] = paramParcel.readInt();
        return arrayOfInt;
    }

    private Object responseCdmaCallWaiting(Parcel paramParcel)
    {
        CdmaCallWaitingNotification localCdmaCallWaitingNotification = new CdmaCallWaitingNotification();
        localCdmaCallWaitingNotification.number = paramParcel.readString();
        localCdmaCallWaitingNotification.numberPresentation = CdmaCallWaitingNotification.presentationFromCLIP(paramParcel.readInt());
        localCdmaCallWaitingNotification.name = paramParcel.readString();
        localCdmaCallWaitingNotification.namePresentation = localCdmaCallWaitingNotification.numberPresentation;
        localCdmaCallWaitingNotification.isPresent = paramParcel.readInt();
        localCdmaCallWaitingNotification.signalType = paramParcel.readInt();
        localCdmaCallWaitingNotification.alertPitch = paramParcel.readInt();
        localCdmaCallWaitingNotification.signal = paramParcel.readInt();
        localCdmaCallWaitingNotification.numberType = paramParcel.readInt();
        localCdmaCallWaitingNotification.numberPlan = paramParcel.readInt();
        return localCdmaCallWaitingNotification;
    }

    private ArrayList<CdmaInformationRecords> responseCdmaInformationRecord(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        ArrayList localArrayList = new ArrayList(i);
        for (int j = 0; j < i; j++)
            localArrayList.add(new CdmaInformationRecords(paramParcel));
        return localArrayList;
    }

    private Object responseCdmaSms(Parcel paramParcel)
    {
        return SmsMessage.newFromParcel(paramParcel);
    }

    private Object responseCellList(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        ArrayList localArrayList = new ArrayList();
        String str = SystemProperties.get("gsm.network.type", "unknown");
        int j;
        if (str.equals("GPRS"))
            j = 1;
        while (j != 0)
        {
            for (int k = 0; k < i; k++)
                localArrayList.add(new NeighboringCellInfo(paramParcel.readInt(), paramParcel.readString(), j));
            if (str.equals("EDGE"))
                j = 2;
            else if (str.equals("UMTS"))
                j = 3;
            else if (str.equals("HSDPA"))
                j = 8;
            else if (str.equals("HSUPA"))
                j = 9;
            else if (str.equals("HSPA"))
                j = 10;
            else
                j = 0;
        }
        return localArrayList;
    }

    private Object responseDataCallList(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        int j = paramParcel.readInt();
        riljLog("responseDataCallList ver=" + i + " num=" + j);
        ArrayList localArrayList = new ArrayList(j);
        for (int k = 0; k < j; k++)
            localArrayList.add(getDataCallState(paramParcel, i));
        return localArrayList;
    }

    private Object responseGetPreferredNetworkType(Parcel paramParcel)
    {
        int[] arrayOfInt = (int[])responseInts(paramParcel);
        if (arrayOfInt.length >= 1)
            this.mPreferredNetworkType = arrayOfInt[0];
        return arrayOfInt;
    }

    private Object responseGmsBroadcastConfig(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        ArrayList localArrayList = new ArrayList(i);
        int j = 0;
        if (j < i)
        {
            int k = paramParcel.readInt();
            int m = paramParcel.readInt();
            int n = paramParcel.readInt();
            int i1 = paramParcel.readInt();
            if (paramParcel.readInt() == 1);
            for (boolean bool = true; ; bool = false)
            {
                localArrayList.add(new SmsBroadcastConfigInfo(k, m, n, i1, bool));
                j++;
                break;
            }
        }
        return localArrayList;
    }

    private Object responseICC_IO(Parcel paramParcel)
    {
        return new IccIoResult(paramParcel.readInt(), paramParcel.readInt(), paramParcel.readString());
    }

    private Object responseIccCardStatus(Parcel paramParcel)
    {
        IccCardStatus localIccCardStatus = new IccCardStatus();
        localIccCardStatus.setCardState(paramParcel.readInt());
        localIccCardStatus.setUniversalPinState(paramParcel.readInt());
        localIccCardStatus.setGsmUmtsSubscriptionAppIndex(paramParcel.readInt());
        localIccCardStatus.setCdmaSubscriptionAppIndex(paramParcel.readInt());
        localIccCardStatus.setImsSubscriptionAppIndex(paramParcel.readInt());
        int i = paramParcel.readInt();
        if (i > 8)
            i = 8;
        localIccCardStatus.setNumApplications(i);
        for (int j = 0; j < i; j++)
        {
            IccCardApplication localIccCardApplication = new IccCardApplication();
            localIccCardApplication.app_type = localIccCardApplication.AppTypeFromRILInt(paramParcel.readInt());
            localIccCardApplication.app_state = localIccCardApplication.AppStateFromRILInt(paramParcel.readInt());
            localIccCardApplication.perso_substate = localIccCardApplication.PersoSubstateFromRILInt(paramParcel.readInt());
            localIccCardApplication.aid = paramParcel.readString();
            localIccCardApplication.app_label = paramParcel.readString();
            localIccCardApplication.pin1_replaced = paramParcel.readInt();
            localIccCardApplication.pin1 = localIccCardApplication.PinStateFromRILInt(paramParcel.readInt());
            localIccCardApplication.pin2 = localIccCardApplication.PinStateFromRILInt(paramParcel.readInt());
            localIccCardStatus.addApplication(localIccCardApplication);
        }
        return localIccCardStatus;
    }

    private Object responseInts(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        int[] arrayOfInt = new int[i];
        for (int j = 0; j < i; j++)
            arrayOfInt[j] = paramParcel.readInt();
        return arrayOfInt;
    }

    private Object responseOperatorInfos(Parcel paramParcel)
    {
        String[] arrayOfString = (String[])responseStrings(paramParcel);
        if (arrayOfString.length % 4 != 0)
            throw new RuntimeException("RIL_REQUEST_QUERY_AVAILABLE_NETWORKS: invalid response. Got " + arrayOfString.length + " strings, expected multible of 4");
        ArrayList localArrayList = new ArrayList(arrayOfString.length / 4);
        for (int i = 0; i < arrayOfString.length; i += 4)
            localArrayList.add(new OperatorInfo(arrayOfString[(i + 0)], arrayOfString[(i + 1)], arrayOfString[(i + 2)], arrayOfString[(i + 3)]));
        return localArrayList;
    }

    private Object responseRaw(Parcel paramParcel)
    {
        return paramParcel.createByteArray();
    }

    private Object responseSMS(Parcel paramParcel)
    {
        return new SmsResponse(paramParcel.readInt(), paramParcel.readString(), paramParcel.readInt());
    }

    private Object responseSetupDataCall(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        int j = paramParcel.readInt();
        DataCallState localDataCallState;
        if (i < 5)
        {
            localDataCallState = new DataCallState();
            localDataCallState.version = i;
            localDataCallState.cid = Integer.parseInt(paramParcel.readString());
            localDataCallState.ifname = paramParcel.readString();
            if (TextUtils.isEmpty(localDataCallState.ifname))
                throw new RuntimeException("RIL_REQUEST_SETUP_DATA_CALL response, no ifname");
            String str1 = paramParcel.readString();
            if (!TextUtils.isEmpty(str1))
                localDataCallState.addresses = str1.split(" ");
            if (j >= 4)
            {
                String str3 = paramParcel.readString();
                riljLog("responseSetupDataCall got dnses=" + str3);
                if (!TextUtils.isEmpty(str3))
                    localDataCallState.dnses = str3.split(" ");
            }
            if (j >= 5)
            {
                String str2 = paramParcel.readString();
                riljLog("responseSetupDataCall got gateways=" + str2);
                if (!TextUtils.isEmpty(str2))
                    localDataCallState.gateways = str2.split(" ");
            }
        }
        while (true)
        {
            return localDataCallState;
            if (j != 1)
                throw new RuntimeException("RIL_REQUEST_SETUP_DATA_CALL response expecting 1 RIL_Data_Call_response_v5 got " + j);
            localDataCallState = getDataCallState(paramParcel, i);
        }
    }

    private Object responseSignalStrength(Parcel paramParcel)
    {
        int[] arrayOfInt = new int[12];
        for (int i = 0; i < 12; i++)
            arrayOfInt[i] = paramParcel.readInt();
        return arrayOfInt;
    }

    private Object responseSimRefresh(Parcel paramParcel)
    {
        IccRefreshResponse localIccRefreshResponse = new IccRefreshResponse();
        localIccRefreshResponse.refreshResult = paramParcel.readInt();
        localIccRefreshResponse.efId = paramParcel.readInt();
        localIccRefreshResponse.aid = paramParcel.readString();
        return localIccRefreshResponse;
    }

    private Object responseString(Parcel paramParcel)
    {
        return paramParcel.readString();
    }

    private Object responseStrings(Parcel paramParcel)
    {
        return paramParcel.readStringArray();
    }

    private Object responseSuppServiceNotification(Parcel paramParcel)
    {
        SuppServiceNotification localSuppServiceNotification = new SuppServiceNotification();
        localSuppServiceNotification.notificationType = paramParcel.readInt();
        localSuppServiceNotification.code = paramParcel.readInt();
        localSuppServiceNotification.index = paramParcel.readInt();
        localSuppServiceNotification.type = paramParcel.readInt();
        localSuppServiceNotification.number = paramParcel.readString();
        return localSuppServiceNotification;
    }

    static String responseToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "<unknown reponse>";
        case 1000:
        case 1001:
        case 1002:
        case 1003:
        case 1004:
        case 1005:
        case 1006:
        case 1007:
        case 1008:
        case 1009:
        case 1010:
        case 1011:
        case 1012:
        case 1013:
        case 1014:
        case 1015:
        case 1016:
        case 1017:
        case 1018:
        case 1019:
        case 1020:
        case 1021:
        case 1022:
        case 1023:
        case 1024:
        case 1025:
        case 1026:
        case 1027:
        case 1028:
        case 1029:
        case 1030:
        case 1031:
        case 1032:
        case 1033:
        case 1034:
        case 1035:
        }
        while (true)
        {
            return str;
            str = "UNSOL_RESPONSE_RADIO_STATE_CHANGED";
            continue;
            str = "UNSOL_RESPONSE_CALL_STATE_CHANGED";
            continue;
            str = "UNSOL_RESPONSE_VOICE_NETWORK_STATE_CHANGED";
            continue;
            str = "UNSOL_RESPONSE_NEW_SMS";
            continue;
            str = "UNSOL_RESPONSE_NEW_SMS_STATUS_REPORT";
            continue;
            str = "UNSOL_RESPONSE_NEW_SMS_ON_SIM";
            continue;
            str = "UNSOL_ON_USSD";
            continue;
            str = "UNSOL_ON_USSD_REQUEST";
            continue;
            str = "UNSOL_NITZ_TIME_RECEIVED";
            continue;
            str = "UNSOL_SIGNAL_STRENGTH";
            continue;
            str = "UNSOL_DATA_CALL_LIST_CHANGED";
            continue;
            str = "UNSOL_SUPP_SVC_NOTIFICATION";
            continue;
            str = "UNSOL_STK_SESSION_END";
            continue;
            str = "UNSOL_STK_PROACTIVE_COMMAND";
            continue;
            str = "UNSOL_STK_EVENT_NOTIFY";
            continue;
            str = "UNSOL_STK_CALL_SETUP";
            continue;
            str = "UNSOL_SIM_SMS_STORAGE_FULL";
            continue;
            str = "UNSOL_SIM_REFRESH";
            continue;
            str = "UNSOL_CALL_RING";
            continue;
            str = "UNSOL_RESPONSE_SIM_STATUS_CHANGED";
            continue;
            str = "UNSOL_RESPONSE_CDMA_NEW_SMS";
            continue;
            str = "UNSOL_RESPONSE_NEW_BROADCAST_SMS";
            continue;
            str = "UNSOL_CDMA_RUIM_SMS_STORAGE_FULL";
            continue;
            str = "UNSOL_RESTRICTED_STATE_CHANGED";
            continue;
            str = "UNSOL_ENTER_EMERGENCY_CALLBACK_MODE";
            continue;
            str = "UNSOL_CDMA_CALL_WAITING";
            continue;
            str = "UNSOL_CDMA_OTA_PROVISION_STATUS";
            continue;
            str = "UNSOL_CDMA_INFO_REC";
            continue;
            str = "UNSOL_OEM_HOOK_RAW";
            continue;
            str = "UNSOL_RINGBACK_TONG";
            continue;
            str = "UNSOL_RESEND_INCALL_MUTE";
            continue;
            str = "CDMA_SUBSCRIPTION_SOURCE_CHANGED";
            continue;
            str = "UNSOL_CDMA_PRL_CHANGED";
            continue;
            str = "UNSOL_EXIT_EMERGENCY_CALLBACK_MODE";
            continue;
            str = "UNSOL_RIL_CONNECTED";
            continue;
            str = "UNSOL_VOICE_RADIO_TECH_CHANGED";
        }
    }

    private Object responseVoid(Parcel paramParcel)
    {
        return null;
    }

    private String retToString(int paramInt, Object paramObject)
    {
        String str;
        if (paramObject == null)
            str = "";
        while (true)
        {
            return str;
            int[] arrayOfInt;
            int m;
            StringBuilder localStringBuilder5;
            int n;
            switch (paramInt)
            {
            default:
                if (!(paramObject instanceof int[]))
                    break label163;
                arrayOfInt = (int[])paramObject;
                m = arrayOfInt.length;
                localStringBuilder5 = new StringBuilder("{");
                if (m > 0)
                {
                    n = 0 + 1;
                    localStringBuilder5.append(arrayOfInt[0]);
                }
                break;
            case 11:
            case 38:
            case 39:
                while (true)
                    if (n < m)
                    {
                        StringBuilder localStringBuilder6 = localStringBuilder5.append(", ");
                        int i1 = n + 1;
                        localStringBuilder6.append(arrayOfInt[n]);
                        n = i1;
                        continue;
                        str = "";
                        break;
                    }
            }
            localStringBuilder5.append("}");
            str = localStringBuilder5.toString();
            continue;
            label163: if ((paramObject instanceof String[]))
            {
                String[] arrayOfString = (String[])paramObject;
                int i = arrayOfString.length;
                StringBuilder localStringBuilder3 = new StringBuilder("{");
                if (i > 0)
                {
                    int j = 0 + 1;
                    localStringBuilder3.append(arrayOfString[0]);
                    while (j < i)
                    {
                        StringBuilder localStringBuilder4 = localStringBuilder3.append(", ");
                        int k = j + 1;
                        localStringBuilder4.append(arrayOfString[j]);
                        j = k;
                    }
                }
                localStringBuilder3.append("}");
                str = localStringBuilder3.toString();
            }
            else if (paramInt == 9)
            {
                ArrayList localArrayList2 = (ArrayList)paramObject;
                StringBuilder localStringBuilder2 = new StringBuilder(" ");
                Iterator localIterator2 = localArrayList2.iterator();
                while (localIterator2.hasNext())
                {
                    DriverCall localDriverCall = (DriverCall)localIterator2.next();
                    localStringBuilder2.append("[").append(localDriverCall).append("] ");
                }
                str = localStringBuilder2.toString();
            }
            else if (paramInt == 75)
            {
                ArrayList localArrayList1 = (ArrayList)paramObject;
                StringBuilder localStringBuilder1 = new StringBuilder(" ");
                Iterator localIterator1 = localArrayList1.iterator();
                while (localIterator1.hasNext())
                    localStringBuilder1.append((NeighboringCellInfo)localIterator1.next()).append(" ");
                str = localStringBuilder1.toString();
            }
            else
            {
                str = paramObject.toString();
            }
        }
    }

    private void riljLog(String paramString)
    {
        Log.d("RILJ", paramString);
    }

    private void riljLogv(String paramString)
    {
        Log.v("RILJ", paramString);
    }

    private void send(RILRequest paramRILRequest)
    {
        if (this.mSocket == null)
        {
            paramRILRequest.onError(1, null);
            paramRILRequest.release();
        }
        while (true)
        {
            return;
            Message localMessage = this.mSender.obtainMessage(1, paramRILRequest);
            acquireWakeLock();
            localMessage.sendToTarget();
        }
    }

    private void sendScreenState(boolean paramBoolean)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(61, null);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        while (true)
        {
            localParcel.writeInt(i);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + ": " + paramBoolean);
            send(localRILRequest);
            return;
            i = 0;
        }
    }

    private void switchToRadioState(CommandsInterface.RadioState paramRadioState)
    {
        setRadioState(paramRadioState);
    }

    private int translateStatus(int paramInt)
    {
        int i = 1;
        switch (paramInt & 0x7)
        {
        case 1:
        case 2:
        case 4:
        case 6:
        default:
        case 3:
        case 5:
        case 7:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 3;
            continue;
            i = 2;
        }
    }

    private void unsljLog(int paramInt)
    {
        riljLog("[UNSL]< " + responseToString(paramInt));
    }

    private void unsljLogMore(int paramInt, String paramString)
    {
        riljLog("[UNSL]< " + responseToString(paramInt) + " " + paramString);
    }

    private void unsljLogRet(int paramInt, Object paramObject)
    {
        riljLog("[UNSL]< " + responseToString(paramInt) + " " + retToString(paramInt, paramObject));
    }

    private void unsljLogvRet(int paramInt, Object paramObject)
    {
        riljLogv("[UNSL]< " + responseToString(paramInt) + " " + retToString(paramInt, paramObject));
    }

    public void acceptCall(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(40, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void acknowledgeIncomingGsmSmsWithPdu(boolean paramBoolean, String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(106, paramMessage);
        localRILRequest.mp.writeInt(2);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        for (String str = "1"; ; str = "0")
        {
            localParcel.writeString(str);
            localRILRequest.mp.writeString(paramString);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + ' ' + paramBoolean + " [" + paramString + ']');
            send(localRILRequest);
            return;
        }
    }

    public void acknowledgeLastIncomingCdmaSms(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(88, paramMessage);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        for (int i = 0; ; i = 1)
        {
            localParcel.writeInt(i);
            localRILRequest.mp.writeInt(paramInt);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramBoolean + " " + paramInt);
            send(localRILRequest);
            return;
        }
    }

    public void acknowledgeLastIncomingGsmSms(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(37, paramMessage);
        localRILRequest.mp.writeInt(2);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel.writeInt(i);
            localRILRequest.mp.writeInt(paramInt);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramBoolean + " " + paramInt);
            send(localRILRequest);
            return;
        }
    }

    public void cancelPendingUssd(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(30, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void changeBarringPassword(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(44, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(3);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(paramString3);
        send(localRILRequest);
    }

    public void changeIccPin(String paramString1, String paramString2, Message paramMessage)
    {
        changeIccPinForApp(paramString1, paramString2, null, paramMessage);
    }

    public void changeIccPin2(String paramString1, String paramString2, Message paramMessage)
    {
        changeIccPin2ForApp(paramString1, paramString2, null, paramMessage);
    }

    public void changeIccPin2ForApp(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(7, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(3);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(paramString3);
        send(localRILRequest);
    }

    public void changeIccPinForApp(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(6, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(3);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(paramString3);
        send(localRILRequest);
    }

    public void conference(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(16, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void deactivateDataCall(int paramInt1, int paramInt2, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(41, paramMessage);
        localRILRequest.mp.writeInt(2);
        localRILRequest.mp.writeString(Integer.toString(paramInt1));
        localRILRequest.mp.writeString(Integer.toString(paramInt2));
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramInt1 + " " + paramInt2);
        send(localRILRequest);
    }

    public void deleteSmsOnRuim(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(97, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        send(localRILRequest);
    }

    public void deleteSmsOnSim(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(64, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        send(localRILRequest);
    }

    public void dial(String paramString, int paramInt, Message paramMessage)
    {
        dial(paramString, paramInt, null, paramMessage);
    }

    public void dial(String paramString, int paramInt, UUSInfo paramUUSInfo, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(10, paramMessage);
        localRILRequest.mp.writeString(paramString);
        localRILRequest.mp.writeInt(paramInt);
        localRILRequest.mp.writeInt(0);
        if (paramUUSInfo == null)
            localRILRequest.mp.writeInt(0);
        while (true)
        {
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
            send(localRILRequest);
            return;
            localRILRequest.mp.writeInt(1);
            localRILRequest.mp.writeInt(paramUUSInfo.getType());
            localRILRequest.mp.writeInt(paramUUSInfo.getDcs());
            localRILRequest.mp.writeByteArray(paramUUSInfo.getUserData());
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("RIL:");
        paramPrintWriter.println(" mSocket=" + this.mSocket);
        paramPrintWriter.println(" mSenderThread=" + this.mSenderThread);
        paramPrintWriter.println(" mSender=" + this.mSender);
        paramPrintWriter.println(" mReceiverThread=" + this.mReceiverThread);
        paramPrintWriter.println(" mReceiver=" + this.mReceiver);
        paramPrintWriter.println(" mWakeLock=" + this.mWakeLock);
        paramPrintWriter.println(" mWakeLockTimeout=" + this.mWakeLockTimeout);
        synchronized (this.mRequestsList)
        {
            paramPrintWriter.println(" mRequestMessagesPending=" + this.mRequestMessagesPending);
            paramPrintWriter.println(" mRequestMessagesWaiting=" + this.mRequestMessagesWaiting);
            int i = this.mRequestsList.size();
            paramPrintWriter.println(" mRequestList count=" + i);
            for (int j = 0; j < i; j++)
            {
                RILRequest localRILRequest = (RILRequest)this.mRequestsList.get(j);
                paramPrintWriter.println("    [" + localRILRequest.mSerial + "] " + requestToString(localRILRequest.mRequest));
            }
            paramPrintWriter.println(" mLastNITZTimeInfo=" + this.mLastNITZTimeInfo);
            paramPrintWriter.println(" mTestingEmergencyCall=" + this.mTestingEmergencyCall.get());
            return;
        }
    }

    public void exitEmergencyCallbackMode(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(99, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void explicitCallTransfer(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(72, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getAvailableNetworks(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(48, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getBasebandVersion(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(51, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getCDMASubscription(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(95, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getCLIR(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(31, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getCdmaBroadcastConfig(Message paramMessage)
    {
        send(RILRequest.obtain(92, paramMessage));
    }

    public void getCdmaSubscriptionSource(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(104, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getCurrentCalls(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(9, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getDataCallList(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(57, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getDataRegistrationState(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(21, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getDeviceIdentity(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(98, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getGsmBroadcastConfig(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(89, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getIMEI(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(38, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getIMEISV(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(39, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getIMSI(Message paramMessage)
    {
        getIMSIForApp(null, paramMessage);
    }

    public void getIMSIForApp(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(11, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeString(paramString);
        riljLog(localRILRequest.serialString() + "> getIMSI: " + requestToString(localRILRequest.mRequest) + " aid: " + paramString);
        send(localRILRequest);
    }

    public void getIccCardStatus(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(1, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getLastCallFailCause(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(18, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getLastDataCallFailCause(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(56, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getLastPdpFailCause(Message paramMessage)
    {
        getLastDataCallFailCause(paramMessage);
    }

    public void getMute(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(54, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getNeighboringCids(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(75, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getNetworkSelectionMode(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(45, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getOperator(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(22, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    @Deprecated
    public void getPDPContextList(Message paramMessage)
    {
        getDataCallList(paramMessage);
    }

    public void getPreferredNetworkType(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(74, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getPreferredVoicePrivacy(Message paramMessage)
    {
        send(RILRequest.obtain(83, paramMessage));
    }

    public void getSignalStrength(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(19, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getSmscAddress(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(100, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getVoiceRadioTechnology(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(108, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void getVoiceRegistrationState(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(20, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void handleCallSetupRequestFromSim(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(71, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        int[] arrayOfInt = new int[i];
        if (paramBoolean);
        while (true)
        {
            arrayOfInt[0] = i;
            localRILRequest.mp.writeIntArray(arrayOfInt);
            send(localRILRequest);
            return;
            i = 0;
        }
    }

    public void hangupConnection(int paramInt, Message paramMessage)
    {
        riljLog("hangupConnection: gsmIndex=" + paramInt);
        RILRequest localRILRequest = RILRequest.obtain(12, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramInt);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        send(localRILRequest);
    }

    public void hangupForegroundResumeBackground(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(14, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void hangupWaitingOrBackground(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(13, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void iccIO(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, String paramString2, String paramString3, Message paramMessage)
    {
        iccIOForApp(paramInt1, paramInt2, paramString1, paramInt3, paramInt4, paramInt5, paramString2, paramString3, null, paramMessage);
    }

    public void iccIOForApp(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, String paramString2, String paramString3, String paramString4, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(28, paramMessage);
        localRILRequest.mp.writeInt(paramInt1);
        localRILRequest.mp.writeInt(paramInt2);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeInt(paramInt3);
        localRILRequest.mp.writeInt(paramInt4);
        localRILRequest.mp.writeInt(paramInt5);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(paramString3);
        localRILRequest.mp.writeString(paramString4);
        riljLog(localRILRequest.serialString() + "> iccIO: " + requestToString(localRILRequest.mRequest) + " 0x" + Integer.toHexString(paramInt1) + " 0x" + Integer.toHexString(paramInt2) + " " + " path: " + paramString1 + "," + paramInt3 + "," + paramInt4 + "," + paramInt5 + " aid: " + paramString4);
        send(localRILRequest);
    }

    public void invokeOemRilRequestRaw(byte[] paramArrayOfByte, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(59, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + "[" + IccUtils.bytesToHexString(paramArrayOfByte) + "]");
        localRILRequest.mp.writeByteArray(paramArrayOfByte);
        send(localRILRequest);
    }

    public void invokeOemRilRequestStrings(String[] paramArrayOfString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(60, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeStringArray(paramArrayOfString);
        send(localRILRequest);
    }

    protected void onRadioAvailable()
    {
        sendScreenState(true);
    }

    public void queryAvailableBandMode(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(66, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void queryCLIP(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(55, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void queryCallForwardStatus(int paramInt1, int paramInt2, String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(33, paramMessage);
        localRILRequest.mp.writeInt(2);
        localRILRequest.mp.writeInt(paramInt1);
        localRILRequest.mp.writeInt(paramInt2);
        localRILRequest.mp.writeInt(PhoneNumberUtils.toaFromString(paramString));
        localRILRequest.mp.writeString(paramString);
        localRILRequest.mp.writeInt(0);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramInt1 + " " + paramInt2);
        send(localRILRequest);
    }

    public void queryCallWaiting(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(35, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramInt);
        send(localRILRequest);
    }

    public void queryCdmaRoamingPreference(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(79, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void queryFacilityLock(String paramString1, String paramString2, int paramInt, Message paramMessage)
    {
        queryFacilityLockForApp(paramString1, paramString2, paramInt, null, paramMessage);
    }

    public void queryFacilityLockForApp(String paramString1, String paramString2, int paramInt, String paramString3, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(42, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(4);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(Integer.toString(paramInt));
        localRILRequest.mp.writeString(paramString3);
        send(localRILRequest);
    }

    public void queryTTYMode(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(81, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void rejectCall(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(17, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void reportSmsMemoryStatus(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(102, paramMessage);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        while (true)
        {
            localParcel.writeInt(i);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + ": " + paramBoolean);
            send(localRILRequest);
            return;
            i = 0;
        }
    }

    public void reportStkServiceIsRunning(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(103, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void requestIsimAuthentication(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(105, paramMessage);
        localRILRequest.mp.writeString(paramString);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void resetRadio(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(58, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void sendBurstDtmf(String paramString, int paramInt1, int paramInt2, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(85, paramMessage);
        localRILRequest.mp.writeInt(3);
        localRILRequest.mp.writeString(paramString);
        localRILRequest.mp.writeString(Integer.toString(paramInt1));
        localRILRequest.mp.writeString(Integer.toString(paramInt2));
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " : " + paramString);
        send(localRILRequest);
    }

    public void sendCDMAFeatureCode(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(84, paramMessage);
        localRILRequest.mp.writeString(paramString);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " : " + paramString);
        send(localRILRequest);
    }

    public void sendCdmaSms(byte[] paramArrayOfByte, Message paramMessage)
    {
        DataInputStream localDataInputStream = new DataInputStream(new ByteArrayInputStream(paramArrayOfByte));
        RILRequest localRILRequest = RILRequest.obtain(87, paramMessage);
        try
        {
            localRILRequest.mp.writeInt(localDataInputStream.readInt());
            localRILRequest.mp.writeByte((byte)localDataInputStream.readInt());
            localRILRequest.mp.writeInt(localDataInputStream.readInt());
            localRILRequest.mp.writeInt(localDataInputStream.read());
            localRILRequest.mp.writeInt(localDataInputStream.read());
            localRILRequest.mp.writeInt(localDataInputStream.read());
            localRILRequest.mp.writeInt(localDataInputStream.read());
            byte b1 = (byte)localDataInputStream.read();
            localRILRequest.mp.writeByte(b1);
            byte b2 = 0;
            while (b2 < b1)
            {
                localRILRequest.mp.writeByte(localDataInputStream.readByte());
                int i;
                b2 += 1;
            }
            localRILRequest.mp.writeInt(localDataInputStream.read());
            localRILRequest.mp.writeByte((byte)localDataInputStream.read());
            byte b3 = (byte)localDataInputStream.read();
            localRILRequest.mp.writeByte(b3);
            byte b4 = 0;
            while (b4 < b3)
            {
                localRILRequest.mp.writeByte(localDataInputStream.readByte());
                int j;
                b4 += 1;
            }
            int k = localDataInputStream.read();
            localRILRequest.mp.writeInt(k);
            for (int m = 0; m < k; m++)
                localRILRequest.mp.writeByte(localDataInputStream.readByte());
        }
        catch (IOException localIOException)
        {
            riljLog("sendSmsCdma: conversion from input stream to object failed: " + localIOException);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
            send(localRILRequest);
        }
    }

    public void sendDtmf(char paramChar, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(24, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeString(Character.toString(paramChar));
        send(localRILRequest);
    }

    public void sendEnvelope(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(69, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeString(paramString);
        send(localRILRequest);
    }

    public void sendEnvelopeWithStatus(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(107, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + '[' + paramString + ']');
        localRILRequest.mp.writeString(paramString);
        send(localRILRequest);
    }

    public void sendSMS(String paramString1, String paramString2, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(25, paramMessage);
        localRILRequest.mp.writeInt(2);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void sendTerminalResponse(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(70, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeString(paramString);
        send(localRILRequest);
    }

    public void sendUSSD(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(29, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramString);
        localRILRequest.mp.writeString(paramString);
        send(localRILRequest);
    }

    public void separateConnection(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(52, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramInt);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        send(localRILRequest);
    }

    public void setBandMode(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(65, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramInt);
        send(localRILRequest);
    }

    public void setCLIR(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(32, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramInt);
        send(localRILRequest);
    }

    public void setCallForward(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(34, paramMessage);
        localRILRequest.mp.writeInt(paramInt1);
        localRILRequest.mp.writeInt(paramInt2);
        localRILRequest.mp.writeInt(paramInt3);
        localRILRequest.mp.writeInt(PhoneNumberUtils.toaFromString(paramString));
        localRILRequest.mp.writeString(paramString);
        localRILRequest.mp.writeInt(paramInt4);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramInt1 + " " + paramInt2 + " " + paramInt3 + paramInt4);
        send(localRILRequest);
    }

    public void setCallWaiting(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(36, paramMessage);
        localRILRequest.mp.writeInt(2);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localParcel.writeInt(i);
            localRILRequest.mp.writeInt(paramInt);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramBoolean + ", " + paramInt);
            send(localRILRequest);
            return;
        }
    }

    public void setCdmaBroadcastActivation(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(94, paramMessage);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean)
            i = 0;
        localParcel.writeInt(i);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void setCdmaBroadcastConfig(int[] paramArrayOfInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(93, paramMessage);
        for (int i = 0; i < paramArrayOfInt.length; i++)
            localRILRequest.mp.writeInt(paramArrayOfInt[i]);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void setCdmaRoamingPreference(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(78, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " : " + paramInt);
        send(localRILRequest);
    }

    public void setCdmaSubscriptionSource(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(77, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " : " + paramInt);
        send(localRILRequest);
    }

    public void setCurrentPreferredNetworkType()
    {
        riljLog("setCurrentPreferredNetworkType: " + this.mSetPreferredNetworkType);
        setPreferredNetworkType(this.mSetPreferredNetworkType, null);
    }

    public void setFacilityLock(String paramString1, boolean paramBoolean, String paramString2, int paramInt, Message paramMessage)
    {
        setFacilityLockForApp(paramString1, paramBoolean, paramString2, paramInt, null, paramMessage);
    }

    public void setFacilityLockForApp(String paramString1, boolean paramBoolean, String paramString2, int paramInt, String paramString3, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(43, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(5);
        localRILRequest.mp.writeString(paramString1);
        if (paramBoolean);
        for (String str = "1"; ; str = "0")
        {
            localRILRequest.mp.writeString(str);
            localRILRequest.mp.writeString(paramString2);
            localRILRequest.mp.writeString(Integer.toString(paramInt));
            localRILRequest.mp.writeString(paramString3);
            send(localRILRequest);
            return;
        }
    }

    public void setGsmBroadcastActivation(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(91, paramMessage);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean)
            i = 0;
        localParcel.writeInt(i);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void setGsmBroadcastConfig(SmsBroadcastConfigInfo[] paramArrayOfSmsBroadcastConfigInfo, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(90, paramMessage);
        int i = paramArrayOfSmsBroadcastConfigInfo.length;
        localRILRequest.mp.writeInt(i);
        int j = 0;
        if (j < i)
        {
            localRILRequest.mp.writeInt(paramArrayOfSmsBroadcastConfigInfo[j].getFromServiceId());
            localRILRequest.mp.writeInt(paramArrayOfSmsBroadcastConfigInfo[j].getToServiceId());
            localRILRequest.mp.writeInt(paramArrayOfSmsBroadcastConfigInfo[j].getFromCodeScheme());
            localRILRequest.mp.writeInt(paramArrayOfSmsBroadcastConfigInfo[j].getToCodeScheme());
            Parcel localParcel = localRILRequest.mp;
            if (paramArrayOfSmsBroadcastConfigInfo[j].isSelected());
            for (int m = 1; ; m = 0)
            {
                localParcel.writeInt(m);
                j++;
                break;
            }
        }
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " with " + i + " configs : ");
        for (int k = 0; k < i; k++)
            riljLog(paramArrayOfSmsBroadcastConfigInfo[k].toString());
        send(localRILRequest);
    }

    public void setLocationUpdates(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(76, paramMessage);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        while (true)
        {
            localParcel.writeInt(i);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + ": " + paramBoolean);
            send(localRILRequest);
            return;
            i = 0;
        }
    }

    public void setMute(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(53, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramBoolean);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        while (true)
        {
            localParcel.writeInt(i);
            send(localRILRequest);
            return;
            i = 0;
        }
    }

    public void setNetworkSelectionModeAutomatic(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(46, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void setNetworkSelectionModeManual(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(47, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramString);
        localRILRequest.mp.writeString(paramString);
        send(localRILRequest);
    }

    public void setOnNITZTime(Handler paramHandler, int paramInt, Object paramObject)
    {
        super.setOnNITZTime(paramHandler, paramInt, paramObject);
        if (this.mLastNITZTimeInfo != null)
        {
            this.mNITZTimeRegistrant.notifyRegistrant(new AsyncResult(null, this.mLastNITZTimeInfo, null));
            this.mLastNITZTimeInfo = null;
        }
    }

    public void setPhoneType(int paramInt)
    {
        riljLog("setPhoneType=" + paramInt + " old value=" + this.mPhoneType);
        this.mPhoneType = paramInt;
    }

    public void setPreferredNetworkType(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(73, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        this.mSetPreferredNetworkType = paramInt;
        this.mPreferredNetworkType = paramInt;
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " : " + paramInt);
        send(localRILRequest);
    }

    public void setPreferredVoicePrivacy(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(82, paramMessage);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        while (true)
        {
            localParcel.writeInt(i);
            send(localRILRequest);
            return;
            i = 0;
        }
    }

    public void setRadioPower(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(23, paramMessage);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        StringBuilder localStringBuilder;
        if (paramBoolean)
        {
            localParcel.writeInt(i);
            localStringBuilder = new StringBuilder().append(localRILRequest.serialString()).append("> ").append(requestToString(localRILRequest.mRequest));
            if (!paramBoolean)
                break label105;
        }
        label105: for (String str = " on"; ; str = " off")
        {
            riljLog(str);
            send(localRILRequest);
            return;
            i = 0;
            break;
        }
    }

    public void setSmscAddress(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(101, paramMessage);
        localRILRequest.mp.writeString(paramString);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " : " + paramString);
        send(localRILRequest);
    }

    public void setSuppServiceNotifications(boolean paramBoolean, Message paramMessage)
    {
        int i = 1;
        RILRequest localRILRequest = RILRequest.obtain(62, paramMessage);
        localRILRequest.mp.writeInt(i);
        Parcel localParcel = localRILRequest.mp;
        if (paramBoolean);
        while (true)
        {
            localParcel.writeInt(i);
            riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
            send(localRILRequest);
            return;
            i = 0;
        }
    }

    public void setTTYMode(int paramInt, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(80, paramMessage);
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeInt(paramInt);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " : " + paramInt);
        send(localRILRequest);
    }

    public void setupDataCall(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(27, paramMessage);
        localRILRequest.mp.writeInt(7);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(paramString3);
        localRILRequest.mp.writeString(paramString4);
        localRILRequest.mp.writeString(paramString5);
        localRILRequest.mp.writeString(paramString6);
        localRILRequest.mp.writeString(paramString7);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest) + " " + paramString1 + " " + paramString2 + " " + paramString3 + " " + paramString4 + " " + paramString5 + " " + paramString6 + " " + paramString7);
        send(localRILRequest);
    }

    public void startDtmf(char paramChar, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(49, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeString(Character.toString(paramChar));
        send(localRILRequest);
    }

    public void stopDtmf(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(50, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void supplyIccPin(String paramString, Message paramMessage)
    {
        supplyIccPinForApp(paramString, null, paramMessage);
    }

    public void supplyIccPin2(String paramString, Message paramMessage)
    {
        supplyIccPin2ForApp(paramString, null, paramMessage);
    }

    public void supplyIccPin2ForApp(String paramString1, String paramString2, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(4, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(2);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        send(localRILRequest);
    }

    public void supplyIccPinForApp(String paramString1, String paramString2, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(2, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(2);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        send(localRILRequest);
    }

    public void supplyIccPuk(String paramString1, String paramString2, Message paramMessage)
    {
        supplyIccPukForApp(paramString1, paramString2, null, paramMessage);
    }

    public void supplyIccPuk2(String paramString1, String paramString2, Message paramMessage)
    {
        supplyIccPuk2ForApp(paramString1, paramString2, null, paramMessage);
    }

    public void supplyIccPuk2ForApp(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(5, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(3);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(paramString3);
        send(localRILRequest);
    }

    public void supplyIccPukForApp(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(3, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(3);
        localRILRequest.mp.writeString(paramString1);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(paramString3);
        send(localRILRequest);
    }

    public void supplyNetworkDepersonalization(String paramString, Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(8, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        localRILRequest.mp.writeInt(1);
        localRILRequest.mp.writeString(paramString);
        send(localRILRequest);
    }

    public void switchWaitingOrHoldingAndActive(Message paramMessage)
    {
        RILRequest localRILRequest = RILRequest.obtain(15, paramMessage);
        riljLog(localRILRequest.serialString() + "> " + requestToString(localRILRequest.mRequest));
        send(localRILRequest);
    }

    public void testingEmergencyCall()
    {
        riljLog("testingEmergencyCall");
        this.mTestingEmergencyCall.set(true);
    }

    public void writeSmsToRuim(int paramInt, String paramString, Message paramMessage)
    {
        int i = translateStatus(paramInt);
        RILRequest localRILRequest = RILRequest.obtain(96, paramMessage);
        localRILRequest.mp.writeInt(i);
        localRILRequest.mp.writeString(paramString);
        send(localRILRequest);
    }

    public void writeSmsToSim(int paramInt, String paramString1, String paramString2, Message paramMessage)
    {
        int i = translateStatus(paramInt);
        RILRequest localRILRequest = RILRequest.obtain(63, paramMessage);
        localRILRequest.mp.writeInt(i);
        localRILRequest.mp.writeString(paramString2);
        localRILRequest.mp.writeString(paramString1);
        send(localRILRequest);
    }

    class RILReceiver
        implements Runnable
    {
        byte[] buffer = new byte[8192];

        RILReceiver()
        {
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: iconst_0
            //     1: istore_1
            //     2: aconst_null
            //     3: astore_2
            //     4: new 31	android/net/LocalSocket
            //     7: dup
            //     8: invokespecial 32	android/net/LocalSocket:<init>	()V
            //     11: astore_3
            //     12: aload_3
            //     13: new 34	android/net/LocalSocketAddress
            //     16: dup
            //     17: ldc 36
            //     19: getstatic 42	android/net/LocalSocketAddress$Namespace:RESERVED	Landroid/net/LocalSocketAddress$Namespace;
            //     22: invokespecial 45	android/net/LocalSocketAddress:<init>	(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V
            //     25: invokevirtual 49	android/net/LocalSocket:connect	(Landroid/net/LocalSocketAddress;)V
            //     28: iconst_0
            //     29: istore_1
            //     30: aload_0
            //     31: getfield 17	com/android/internal/telephony/RIL$RILReceiver:this$0	Lcom/android/internal/telephony/RIL;
            //     34: aload_3
            //     35: putfield 53	com/android/internal/telephony/RIL:mSocket	Landroid/net/LocalSocket;
            //     38: ldc 55
            //     40: ldc 57
            //     42: invokestatic 63	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     45: pop
            //     46: iconst_0
            //     47: istore 12
            //     49: aload_0
            //     50: getfield 17	com/android/internal/telephony/RIL$RILReceiver:this$0	Lcom/android/internal/telephony/RIL;
            //     53: getfield 53	com/android/internal/telephony/RIL:mSocket	Landroid/net/LocalSocket;
            //     56: invokevirtual 67	android/net/LocalSocket:getInputStream	()Ljava/io/InputStream;
            //     59: astore 19
            //     61: aload 19
            //     63: aload_0
            //     64: getfield 22	com/android/internal/telephony/RIL$RILReceiver:buffer	[B
            //     67: invokestatic 71	com/android/internal/telephony/RIL:access$300	(Ljava/io/InputStream;[B)I
            //     70: istore 20
            //     72: iload 20
            //     74: istore 12
            //     76: iload 12
            //     78: ifge +160 -> 238
            //     81: ldc 55
            //     83: ldc 73
            //     85: invokestatic 63	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     88: pop
            //     89: aload_0
            //     90: getfield 17	com/android/internal/telephony/RIL$RILReceiver:this$0	Lcom/android/internal/telephony/RIL;
            //     93: getstatic 79	com/android/internal/telephony/CommandsInterface$RadioState:RADIO_UNAVAILABLE	Lcom/android/internal/telephony/CommandsInterface$RadioState;
            //     96: invokevirtual 83	com/android/internal/telephony/RIL:setRadioState	(Lcom/android/internal/telephony/CommandsInterface$RadioState;)V
            //     99: aload_0
            //     100: getfield 17	com/android/internal/telephony/RIL$RILReceiver:this$0	Lcom/android/internal/telephony/RIL;
            //     103: getfield 53	com/android/internal/telephony/RIL:mSocket	Landroid/net/LocalSocket;
            //     106: invokevirtual 86	android/net/LocalSocket:close	()V
            //     109: aload_0
            //     110: getfield 17	com/android/internal/telephony/RIL$RILReceiver:this$0	Lcom/android/internal/telephony/RIL;
            //     113: aconst_null
            //     114: putfield 53	com/android/internal/telephony/RIL:mSocket	Landroid/net/LocalSocket;
            //     117: invokestatic 91	com/android/internal/telephony/RILRequest:resetSerial	()V
            //     120: aload_0
            //     121: getfield 17	com/android/internal/telephony/RIL$RILReceiver:this$0	Lcom/android/internal/telephony/RIL;
            //     124: iconst_1
            //     125: iconst_0
            //     126: invokestatic 95	com/android/internal/telephony/RIL:access$500	(Lcom/android/internal/telephony/RIL;IZ)V
            //     129: goto -127 -> 2
            //     132: astore 5
            //     134: ldc 55
            //     136: ldc 97
            //     138: aload 5
            //     140: invokestatic 101	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     143: pop
            //     144: aload_0
            //     145: getfield 17	com/android/internal/telephony/RIL$RILReceiver:this$0	Lcom/android/internal/telephony/RIL;
            //     148: bipush 255
            //     150: invokestatic 105	com/android/internal/telephony/RIL:access$600	(Lcom/android/internal/telephony/RIL;I)V
            //     153: return
            //     154: astore 22
            //     156: aload_2
            //     157: ifnull +7 -> 164
            //     160: aload_2
            //     161: invokevirtual 86	android/net/LocalSocket:close	()V
            //     164: iload_1
            //     165: bipush 8
            //     167: if_icmpne +45 -> 212
            //     170: ldc 55
            //     172: new 107	java/lang/StringBuilder
            //     175: dup
            //     176: invokespecial 108	java/lang/StringBuilder:<init>	()V
            //     179: ldc 110
            //     181: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     184: iload_1
            //     185: invokevirtual 117	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     188: ldc 119
            //     190: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     193: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     196: invokestatic 125	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     199: pop
            //     200: ldc2_w 126
            //     203: invokestatic 133	java/lang/Thread:sleep	(J)V
            //     206: iinc 1 1
            //     209: goto -207 -> 2
            //     212: iload_1
            //     213: ifle -13 -> 200
            //     216: iload_1
            //     217: bipush 8
            //     219: if_icmpge -19 -> 200
            //     222: ldc 55
            //     224: ldc 135
            //     226: invokestatic 63	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     229: pop
            //     230: goto -30 -> 200
            //     233: astore 5
            //     235: goto -101 -> 134
            //     238: invokestatic 141	android/os/Parcel:obtain	()Landroid/os/Parcel;
            //     241: astore 21
            //     243: aload 21
            //     245: aload_0
            //     246: getfield 22	com/android/internal/telephony/RIL$RILReceiver:buffer	[B
            //     249: iconst_0
            //     250: iload 12
            //     252: invokevirtual 145	android/os/Parcel:unmarshall	([BII)V
            //     255: aload 21
            //     257: iconst_0
            //     258: invokevirtual 149	android/os/Parcel:setDataPosition	(I)V
            //     261: aload_0
            //     262: getfield 17	com/android/internal/telephony/RIL$RILReceiver:this$0	Lcom/android/internal/telephony/RIL;
            //     265: aload 21
            //     267: invokestatic 153	com/android/internal/telephony/RIL:access$400	(Lcom/android/internal/telephony/RIL;Landroid/os/Parcel;)V
            //     270: aload 21
            //     272: invokevirtual 156	android/os/Parcel:recycle	()V
            //     275: goto -214 -> 61
            //     278: astore 17
            //     280: ldc 55
            //     282: ldc 158
            //     284: aload 17
            //     286: invokestatic 160	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     289: pop
            //     290: goto -209 -> 81
            //     293: astore 13
            //     295: ldc 55
            //     297: new 107	java/lang/StringBuilder
            //     300: dup
            //     301: invokespecial 108	java/lang/StringBuilder:<init>	()V
            //     304: ldc 162
            //     306: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     309: iload 12
            //     311: invokevirtual 117	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     314: ldc 164
            //     316: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     319: aload 13
            //     321: invokevirtual 165	java/lang/Throwable:toString	()Ljava/lang/String;
            //     324: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     327: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     330: invokestatic 125	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     333: pop
            //     334: goto -253 -> 81
            //     337: astore 10
            //     339: goto -175 -> 164
            //     342: astore 8
            //     344: goto -138 -> 206
            //     347: astore 16
            //     349: goto -240 -> 109
            //     352: astore 4
            //     354: aload_3
            //     355: astore_2
            //     356: goto -200 -> 156
            //
            // Exception table:
            //     from	to	target	type
            //     12	28	132	java/lang/Throwable
            //     30	46	132	java/lang/Throwable
            //     81	99	132	java/lang/Throwable
            //     99	109	132	java/lang/Throwable
            //     109	129	132	java/lang/Throwable
            //     280	334	132	java/lang/Throwable
            //     4	12	154	java/io/IOException
            //     4	12	233	java/lang/Throwable
            //     160	164	233	java/lang/Throwable
            //     170	200	233	java/lang/Throwable
            //     200	206	233	java/lang/Throwable
            //     222	230	233	java/lang/Throwable
            //     49	72	278	java/io/IOException
            //     238	275	278	java/io/IOException
            //     49	72	293	java/lang/Throwable
            //     238	275	293	java/lang/Throwable
            //     160	164	337	java/io/IOException
            //     200	206	342	java/lang/InterruptedException
            //     99	109	347	java/io/IOException
            //     12	28	352	java/io/IOException
        }
    }

    class RILSender extends Handler
        implements Runnable
    {
        byte[] dataLength = new byte[4];

        public RILSender(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            RILRequest localRILRequest1 = (RILRequest)paramMessage.obj;
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                int k = 0;
                try
                {
                    localLocalSocket = RIL.this.mSocket;
                    if (localLocalSocket == null)
                    {
                        localRILRequest1.onError(1, null);
                        localRILRequest1.release();
                        if (RIL.this.mRequestMessagesPending > 0)
                        {
                            RIL localRIL5 = RIL.this;
                            localRIL5.mRequestMessagesPending = (-1 + localRIL5.mRequestMessagesPending);
                        }
                        RIL.this.releaseWakeLockIfDone();
                        continue;
                    }
                }
                catch (IOException localIOException)
                {
                }
                catch (RuntimeException localRuntimeException)
                {
                    while (true)
                    {
                        LocalSocket localLocalSocket;
                        RIL localRIL3;
                        RIL localRIL4;
                        byte[] arrayOfByte1;
                        RIL localRIL2;
                        Log.e("RILJ", "Uncaught exception ", localRuntimeException);
                        if ((RIL.this.findAndRemoveRequestFromList(localRILRequest1.mSerial) != null) || (k == 0))
                        {
                            localRILRequest1.onError(2, null);
                            localRILRequest1.release();
                        }
                        RIL localRIL1 = RIL.this;
                        continue;
                        byte[] arrayOfByte2 = this.dataLength;
                        this.dataLength[1] = 0;
                        arrayOfByte2[0] = 0;
                        this.dataLength[2] = ((byte)(0xFF & arrayOfByte1.length >> 8));
                        this.dataLength[3] = ((byte)(0xFF & arrayOfByte1.length));
                        localLocalSocket.getOutputStream().write(this.dataLength);
                        localLocalSocket.getOutputStream().write(arrayOfByte1);
                        localRIL1 = RIL.this;
                    }
                }
                finally
                {
                    RIL.this.releaseWakeLockIfDone();
                }
                synchronized (RIL.this.mWakeLock)
                {
                    if (RIL.this.mWakeLock.isHeld())
                        if (RIL.this.mRequestMessagesWaiting != 0)
                        {
                            Log.d("RILJ", "NOTE: mReqWaiting is NOT 0 but" + RIL.this.mRequestMessagesWaiting + " at TIMEOUT, reset!" + " There still msg waitng for response");
                            RIL.this.mRequestMessagesWaiting = 0;
                        }
                }
                synchronized (RIL.this.mRequestsList)
                {
                    int i = RIL.this.mRequestsList.size();
                    Log.d("RILJ", "WAKE_LOCK_TIMEOUT    mRequestList=" + i);
                    for (int j = 0; j < i; j++)
                    {
                        RILRequest localRILRequest2 = (RILRequest)RIL.this.mRequestsList.get(j);
                        Log.d("RILJ", j + ": [" + localRILRequest2.mSerial + "] " + RIL.requestToString(localRILRequest2.mRequest));
                    }
                    if (RIL.this.mRequestMessagesPending != 0)
                    {
                        Log.e("RILJ", "ERROR: mReqPending is NOT 0 but" + RIL.this.mRequestMessagesPending + " at TIMEOUT, reset!");
                        RIL.this.mRequestMessagesPending = 0;
                    }
                    RIL.this.mWakeLock.release();
                    continue;
                    localObject1 = finally;
                    throw localObject1;
                }
            }
        }

        public void run()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.RIL
 * JD-Core Version:        0.6.2
 */