package com.android.internal.telephony.sip;

import android.content.Context;
import android.net.sip.SipProfile.Builder;
import android.util.Log;
import com.android.internal.telephony.PhoneNotifier;
import java.text.ParseException;

public class SipPhoneFactory
{
    public static SipPhone makePhone(String paramString, Context paramContext, PhoneNotifier paramPhoneNotifier)
    {
        try
        {
            localSipPhone = new SipPhone(paramContext, paramPhoneNotifier, new SipProfile.Builder(paramString).build());
            return localSipPhone;
        }
        catch (ParseException localParseException)
        {
            while (true)
            {
                Log.w("SipPhoneFactory", "makePhone", localParseException);
                SipPhone localSipPhone = null;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.sip.SipPhoneFactory
 * JD-Core Version:        0.6.2
 */