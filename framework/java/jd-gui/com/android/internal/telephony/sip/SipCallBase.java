package com.android.internal.telephony.sip;

import com.android.internal.telephony.Call;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

abstract class SipCallBase extends Call
{
    protected List<Connection> connections = new ArrayList();

    void clearDisconnected()
    {
        Iterator localIterator = this.connections.iterator();
        while (localIterator.hasNext())
            if (((Connection)localIterator.next()).getState() == Call.State.DISCONNECTED)
                localIterator.remove();
        if (this.connections.isEmpty())
            setState(Call.State.IDLE);
    }

    public List<Connection> getConnections()
    {
        return this.connections;
    }

    public boolean isMultiparty()
    {
        int i = 1;
        if (this.connections.size() > i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    protected abstract void setState(Call.State paramState);

    public String toString()
    {
        return this.state.toString() + ":" + super.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.sip.SipCallBase
 * JD-Core Version:        0.6.2
 */