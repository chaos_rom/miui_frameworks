package com.android.internal.telephony.sip;

import android.net.sip.SipAudioCall;
import android.os.SystemClock;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.Connection.DisconnectCause;
import com.android.internal.telephony.Connection.PostDialState;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.UUSInfo;

abstract class SipConnectionBase extends Connection
{
    private static final String LOG_TAG = "SIP_CONN";
    private long connectTime;
    private long connectTimeReal;
    private long createTime;
    private String dialString;
    private long disconnectTime;
    private long duration = -1L;
    private long holdingStartTime;
    private boolean isIncoming;
    private Connection.DisconnectCause mCause = Connection.DisconnectCause.NOT_DISCONNECTED;
    private SipAudioCall mSipAudioCall;
    private int nextPostDialChar;
    private Connection.PostDialState postDialState = Connection.PostDialState.NOT_STARTED;
    private String postDialString;

    SipConnectionBase(String paramString)
    {
        this.dialString = paramString;
        this.postDialString = PhoneNumberUtils.extractPostDialPortion(paramString);
        this.isIncoming = false;
        this.createTime = System.currentTimeMillis();
    }

    private void log(String paramString)
    {
        Log.d("SIP_CONN", "[SipConn] " + paramString);
    }

    public void cancelPostDial()
    {
    }

    public long getConnectTime()
    {
        return this.connectTime;
    }

    public long getCreateTime()
    {
        return this.createTime;
    }

    public Connection.DisconnectCause getDisconnectCause()
    {
        return this.mCause;
    }

    public long getDisconnectTime()
    {
        return this.disconnectTime;
    }

    public long getDurationMillis()
    {
        long l = 0L;
        if (this.connectTimeReal == l);
        while (true)
        {
            return l;
            if (this.duration < l)
                l = SystemClock.elapsedRealtime() - this.connectTimeReal;
            else
                l = this.duration;
        }
    }

    public long getHoldDurationMillis()
    {
        if (getState() != Call.State.HOLDING);
        for (long l = 0L; ; l = SystemClock.elapsedRealtime() - this.holdingStartTime)
            return l;
    }

    public int getNumberPresentation()
    {
        return Connection.PRESENTATION_ALLOWED;
    }

    protected abstract Phone getPhone();

    public Connection.PostDialState getPostDialState()
    {
        return this.postDialState;
    }

    public String getRemainingPostDialString()
    {
        if ((this.postDialState == Connection.PostDialState.CANCELLED) || (this.postDialState == Connection.PostDialState.COMPLETE) || (this.postDialString == null) || (this.postDialString.length() <= this.nextPostDialChar));
        for (String str = ""; ; str = this.postDialString.substring(this.nextPostDialChar))
            return str;
    }

    public UUSInfo getUUSInfo()
    {
        return null;
    }

    public void proceedAfterWaitChar()
    {
    }

    public void proceedAfterWildChar(String paramString)
    {
    }

    void setDisconnectCause(Connection.DisconnectCause paramDisconnectCause)
    {
        this.mCause = paramDisconnectCause;
    }

    protected void setState(Call.State paramState)
    {
        switch (1.$SwitchMap$com$android$internal$telephony$Call$State[paramState.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            if (this.connectTime == 0L)
            {
                this.connectTimeReal = SystemClock.elapsedRealtime();
                this.connectTime = System.currentTimeMillis();
                continue;
                this.duration = getDurationMillis();
                this.disconnectTime = System.currentTimeMillis();
                continue;
                this.holdingStartTime = SystemClock.elapsedRealtime();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.sip.SipConnectionBase
 * JD-Core Version:        0.6.2
 */