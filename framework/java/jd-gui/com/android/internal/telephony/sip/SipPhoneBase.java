package com.android.internal.telephony.sip;

import android.content.Context;
import android.net.LinkProperties;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.util.Log;
import com.android.internal.telephony.Call;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.DataConnection;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccPhoneBookInterfaceManager;
import com.android.internal.telephony.IccSmsInterfaceManager;
import com.android.internal.telephony.MmiCode;
import com.android.internal.telephony.OperatorInfo;
import com.android.internal.telephony.Phone.DataActivityState;
import com.android.internal.telephony.Phone.DataState;
import com.android.internal.telephony.Phone.State;
import com.android.internal.telephony.Phone.SuppService;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.PhoneNotifier;
import com.android.internal.telephony.PhoneSubInfo;
import com.android.internal.telephony.UUSInfo;
import java.util.ArrayList;
import java.util.List;

abstract class SipPhoneBase extends PhoneBase
{
    private static final String LOG_TAG = "SipPhone";
    private RegistrantList mRingbackRegistrants = new RegistrantList();
    private Phone.State state = Phone.State.IDLE;

    public SipPhoneBase(Context paramContext, PhoneNotifier paramPhoneNotifier)
    {
        super(paramPhoneNotifier, paramContext, new SipCommandInterface(paramContext), false);
    }

    static void migrate(RegistrantList paramRegistrantList1, RegistrantList paramRegistrantList2)
    {
        paramRegistrantList2.removeCleared();
        int i = 0;
        int j = paramRegistrantList2.size();
        while (i < j)
        {
            paramRegistrantList1.add((Registrant)paramRegistrantList2.get(i));
            i++;
        }
    }

    public void activateCellBroadcastSms(int paramInt, Message paramMessage)
    {
        Log.e("SipPhone", "Error! This functionality is not implemented for SIP.");
    }

    public boolean canDial()
    {
        boolean bool = false;
        int i = getServiceState().getState();
        Log.v("SipPhone", "canDial(): serviceState = " + i);
        if (i == 3);
        while (true)
        {
            return bool;
            String str = SystemProperties.get("ro.telephony.disable-call", "false");
            Log.v("SipPhone", "canDial(): disableCall = " + str);
            if (!str.equals("true"))
            {
                Log.v("SipPhone", "canDial(): ringingCall: " + getRingingCall().getState());
                Log.v("SipPhone", "canDial(): foregndCall: " + getForegroundCall().getState());
                Log.v("SipPhone", "canDial(): backgndCall: " + getBackgroundCall().getState());
                if ((!getRingingCall().isRinging()) && ((!getForegroundCall().getState().isAlive()) || (!getBackgroundCall().getState().isAlive())))
                    bool = true;
            }
        }
    }

    public Connection dial(String paramString, UUSInfo paramUUSInfo)
        throws CallStateException
    {
        return dial(paramString);
    }

    public boolean disableDataConnectivity()
    {
        return false;
    }

    public void disableLocationUpdates()
    {
    }

    public boolean enableDataConnectivity()
    {
        return false;
    }

    public void enableLocationUpdates()
    {
    }

    public void getAvailableNetworks(Message paramMessage)
    {
    }

    public abstract Call getBackgroundCall();

    public boolean getCallForwardingIndicator()
    {
        return false;
    }

    public void getCallForwardingOption(int paramInt, Message paramMessage)
    {
    }

    public void getCallWaiting(Message paramMessage)
    {
        AsyncResult.forMessage(paramMessage, null, null);
        paramMessage.sendToTarget();
    }

    public void getCellBroadcastSmsConfig(Message paramMessage)
    {
        Log.e("SipPhone", "Error! This functionality is not implemented for SIP.");
    }

    public CellLocation getCellLocation()
    {
        return null;
    }

    public List<DataConnection> getCurrentDataConnectionList()
    {
        return null;
    }

    public Phone.DataActivityState getDataActivityState()
    {
        return Phone.DataActivityState.NONE;
    }

    public void getDataCallList(Message paramMessage)
    {
    }

    public Phone.DataState getDataConnectionState()
    {
        return Phone.DataState.DISCONNECTED;
    }

    public Phone.DataState getDataConnectionState(String paramString)
    {
        return Phone.DataState.DISCONNECTED;
    }

    public boolean getDataRoamingEnabled()
    {
        return false;
    }

    public String getDeviceId()
    {
        return null;
    }

    public String getDeviceSvn()
    {
        return null;
    }

    public String getEsn()
    {
        Log.e("SipPhone", "[SipPhone] getEsn() is a CDMA method");
        return "0";
    }

    public abstract Call getForegroundCall();

    public IccCard getIccCard()
    {
        return null;
    }

    public IccFileHandler getIccFileHandler()
    {
        return null;
    }

    public IccPhoneBookInterfaceManager getIccPhoneBookInterfaceManager()
    {
        return null;
    }

    public boolean getIccRecordsLoaded()
    {
        return false;
    }

    public String getIccSerialNumber()
    {
        return null;
    }

    public IccSmsInterfaceManager getIccSmsInterfaceManager()
    {
        return null;
    }

    public String getImei()
    {
        return null;
    }

    public String getLine1AlphaTag()
    {
        return null;
    }

    public String getLine1Number()
    {
        return null;
    }

    public LinkProperties getLinkProperties(String paramString)
    {
        return null;
    }

    public String getMeid()
    {
        Log.e("SipPhone", "[SipPhone] getMeid() is a CDMA method");
        return "0";
    }

    public boolean getMessageWaitingIndicator()
    {
        return false;
    }

    public void getNeighboringCids(Message paramMessage)
    {
    }

    public void getOutgoingCallerIdDisplay(Message paramMessage)
    {
        AsyncResult.forMessage(paramMessage, null, null);
        paramMessage.sendToTarget();
    }

    public List<? extends MmiCode> getPendingMmiCodes()
    {
        return new ArrayList(0);
    }

    public PhoneSubInfo getPhoneSubInfo()
    {
        return null;
    }

    public int getPhoneType()
    {
        return 3;
    }

    public abstract Call getRingingCall();

    public ServiceState getServiceState()
    {
        ServiceState localServiceState = new ServiceState();
        localServiceState.setState(0);
        return localServiceState;
    }

    public SignalStrength getSignalStrength()
    {
        return new SignalStrength();
    }

    public Phone.State getState()
    {
        return this.state;
    }

    public String getSubscriberId()
    {
        return null;
    }

    public String getVoiceMailAlphaTag()
    {
        return null;
    }

    public String getVoiceMailNumber()
    {
        return null;
    }

    public boolean handleInCallMmiCommands(String paramString)
        throws CallStateException
    {
        return false;
    }

    public boolean handlePinMmi(String paramString)
    {
        return false;
    }

    public boolean isDataConnectivityPossible()
    {
        return false;
    }

    boolean isInCall()
    {
        Call.State localState1 = getForegroundCall().getState();
        Call.State localState2 = getBackgroundCall().getState();
        Call.State localState3 = getRingingCall().getState();
        if ((localState1.isAlive()) || (localState2.isAlive()) || (localState3.isAlive()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void migrateFrom(SipPhoneBase paramSipPhoneBase)
    {
        migrate(this.mRingbackRegistrants, paramSipPhoneBase.mRingbackRegistrants);
        migrate(this.mPreciseCallStateRegistrants, paramSipPhoneBase.mPreciseCallStateRegistrants);
        migrate(this.mNewRingingConnectionRegistrants, paramSipPhoneBase.mNewRingingConnectionRegistrants);
        migrate(this.mIncomingRingRegistrants, paramSipPhoneBase.mIncomingRingRegistrants);
        migrate(this.mDisconnectRegistrants, paramSipPhoneBase.mDisconnectRegistrants);
        migrate(this.mServiceStateRegistrants, paramSipPhoneBase.mServiceStateRegistrants);
        migrate(this.mMmiCompleteRegistrants, paramSipPhoneBase.mMmiCompleteRegistrants);
        migrate(this.mMmiRegistrants, paramSipPhoneBase.mMmiRegistrants);
        migrate(this.mUnknownConnectionRegistrants, paramSipPhoneBase.mUnknownConnectionRegistrants);
        migrate(this.mSuppServiceFailedRegistrants, paramSipPhoneBase.mSuppServiceFailedRegistrants);
    }

    public boolean needsOtaServiceProvisioning()
    {
        return false;
    }

    public void notifyCallForwardingIndicator()
    {
        this.mNotifier.notifyCallForwardingChanged(this);
    }

    void notifyDisconnect(Connection paramConnection)
    {
        this.mDisconnectRegistrants.notifyResult(paramConnection);
    }

    void notifyNewRingingConnection(Connection paramConnection)
    {
        super.notifyNewRingingConnectionP(paramConnection);
    }

    void notifyPhoneStateChanged()
    {
        this.mNotifier.notifyPhoneState(this);
    }

    void notifyPreciseCallStateChanged()
    {
        super.notifyPreciseCallStateChangedP();
    }

    void notifyServiceStateChanged(ServiceState paramServiceState)
    {
        super.notifyServiceStateChangedP(paramServiceState);
    }

    void notifySuppServiceFailed(Phone.SuppService paramSuppService)
    {
        this.mSuppServiceFailedRegistrants.notifyResult(paramSuppService);
    }

    void notifyUnknownConnection()
    {
        this.mUnknownConnectionRegistrants.notifyResult(this);
    }

    public void registerForRingbackTone(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mRingbackRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForSuppServiceNotification(Handler paramHandler, int paramInt, Object paramObject)
    {
    }

    public void saveClirSetting(int paramInt)
    {
    }

    public void selectNetworkManually(OperatorInfo paramOperatorInfo, Message paramMessage)
    {
    }

    public void sendUssdResponse(String paramString)
    {
    }

    public void setCallForwardingOption(int paramInt1, int paramInt2, String paramString, int paramInt3, Message paramMessage)
    {
    }

    public void setCallWaiting(boolean paramBoolean, Message paramMessage)
    {
        Log.e("SipPhone", "call waiting not supported");
    }

    public void setCellBroadcastSmsConfig(int[] paramArrayOfInt, Message paramMessage)
    {
        Log.e("SipPhone", "Error! This functionality is not implemented for SIP.");
    }

    public void setDataRoamingEnabled(boolean paramBoolean)
    {
    }

    public void setLine1Number(String paramString1, String paramString2, Message paramMessage)
    {
        AsyncResult.forMessage(paramMessage, null, null);
        paramMessage.sendToTarget();
    }

    public void setNetworkSelectionModeAutomatic(Message paramMessage)
    {
    }

    public void setOnPostDialCharacter(Handler paramHandler, int paramInt, Object paramObject)
    {
    }

    public void setOutgoingCallerIdDisplay(int paramInt, Message paramMessage)
    {
        AsyncResult.forMessage(paramMessage, null, null);
        paramMessage.sendToTarget();
    }

    public void setRadioPower(boolean paramBoolean)
    {
    }

    public void setVoiceMailNumber(String paramString1, String paramString2, Message paramMessage)
    {
        AsyncResult.forMessage(paramMessage, null, null);
        paramMessage.sendToTarget();
    }

    protected void startRingbackTone()
    {
        AsyncResult localAsyncResult = new AsyncResult(null, Boolean.TRUE, null);
        this.mRingbackRegistrants.notifyRegistrants(localAsyncResult);
    }

    protected void stopRingbackTone()
    {
        AsyncResult localAsyncResult = new AsyncResult(null, Boolean.FALSE, null);
        this.mRingbackRegistrants.notifyRegistrants(localAsyncResult);
    }

    public void unregisterForRingbackTone(Handler paramHandler)
    {
        this.mRingbackRegistrants.remove(paramHandler);
    }

    public void unregisterForSuppServiceNotification(Handler paramHandler)
    {
    }

    boolean updateCurrentCarrierInProvider()
    {
        return false;
    }

    void updatePhoneState()
    {
        Phone.State localState = this.state;
        if (getRingingCall().isRinging())
            this.state = Phone.State.RINGING;
        while (true)
        {
            if (this.state != localState)
            {
                Log.d("SipPhone", " ^^^ new phone state: " + this.state);
                notifyPhoneStateChanged();
            }
            return;
            if ((getForegroundCall().isIdle()) && (getBackgroundCall().isIdle()))
                this.state = Phone.State.IDLE;
            else
                this.state = Phone.State.OFFHOOK;
        }
    }

    public void updateServiceLocation()
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.sip.SipPhoneBase
 * JD-Core Version:        0.6.2
 */