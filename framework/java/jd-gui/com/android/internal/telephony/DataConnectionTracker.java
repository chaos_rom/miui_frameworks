package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.telephony.ServiceState;
import android.text.TextUtils;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class DataConnectionTracker extends Handler
{
    public static String ACTION_DATA_CONNECTION_TRACKER_MESSENGER = "com.android.internal.telephony";
    protected static final int APN_CBS_ID = 7;
    protected static final int APN_DEFAULT_ID = 0;
    protected static final int APN_DELAY_MILLIS = 0;
    protected static final int APN_DUN_ID = 3;
    protected static final int APN_FOTA_ID = 6;
    protected static final int APN_HIPRI_ID = 4;
    protected static final int APN_IMS_ID = 5;
    protected static final int APN_INVALID_ID = -1;
    protected static final int APN_MMS_ID = 1;
    protected static final int APN_NUM_TYPES = 8;
    protected static final String APN_RESTORE_DELAY_PROP_NAME = "android.telephony.apn-restore";
    protected static final int APN_SUPL_ID = 2;
    public static final String APN_TYPE_KEY = "apnType";
    protected static final int BASE = 270336;
    public static final int CMD_SET_DEPENDENCY_MET = 270367;
    public static final int CMD_SET_POLICY_DATA_ENABLE = 270368;
    public static final int CMD_SET_USER_DATA_ENABLE = 270365;
    protected static final int DATA_STALL_ALARM_AGGRESSIVE_DELAY_IN_MS_DEFAULT = 60000;
    protected static final int DATA_STALL_ALARM_NON_AGGRESSIVE_DELAY_IN_MS_DEFAULT = 360000;
    protected static final String DATA_STALL_ALARM_TAG_EXTRA = "data.stall.alram.tag";
    protected static final int DATA_STALL_NO_RECV_POLL_LIMIT = 1;
    protected static final boolean DBG = true;
    protected static final String DEFALUT_DATA_ON_BOOT_PROP = "net.def_data_on_boot";
    protected static final String DEFAULT_DATA_RETRY_CONFIG = "default_randomization=2000,5000,10000,20000,40000,80000:5000,160000:5000,320000:5000,640000:5000,1280000:5000,1800000:5000";
    protected static final int DEFAULT_MAX_PDP_RESET_FAIL = 3;
    public static final int DISABLED = 0;
    public static final int ENABLED = 1;
    protected static final int EVENT_APN_CHANGED = 270355;
    protected static final int EVENT_CDMA_DATA_DETACHED = 270356;
    protected static final int EVENT_CDMA_OTA_PROVISION = 270361;
    protected static final int EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED = 270357;
    public static final int EVENT_CLEAN_UP_ALL_CONNECTIONS = 270366;
    public static final int EVENT_CLEAN_UP_CONNECTION = 270360;
    protected static final int EVENT_DATA_CONNECTION_ATTACHED = 270352;
    protected static final int EVENT_DATA_CONNECTION_DETACHED = 270345;
    protected static final int EVENT_DATA_SETUP_COMPLETE = 270336;
    protected static final int EVENT_DATA_STALL_ALARM = 270353;
    protected static final int EVENT_DATA_STATE_CHANGED = 270340;
    protected static final int EVENT_DISCONNECT_DONE = 270351;
    protected static final int EVENT_DO_RECOVERY = 270354;
    protected static final int EVENT_ENABLE_NEW_APN = 270349;
    protected static final int EVENT_LINK_STATE_CHANGED = 270346;
    protected static final int EVENT_POLL_PDP = 270341;
    protected static final int EVENT_PS_RESTRICT_DISABLED = 270359;
    protected static final int EVENT_PS_RESTRICT_ENABLED = 270358;
    protected static final int EVENT_RADIO_AVAILABLE = 270337;
    protected static final int EVENT_RADIO_OFF_OR_NOT_AVAILABLE = 270342;
    protected static final int EVENT_RECORDS_LOADED = 270338;
    protected static final int EVENT_RESET_DONE = 270364;
    protected static final int EVENT_RESTART_RADIO = 270362;
    protected static final int EVENT_RESTORE_DEFAULT_APN = 270350;
    protected static final int EVENT_ROAMING_OFF = 270348;
    protected static final int EVENT_ROAMING_ON = 270347;
    protected static final int EVENT_SET_INTERNAL_DATA_ENABLE = 270363;
    protected static final int EVENT_TRY_SETUP_DATA = 270339;
    protected static final int EVENT_VOICE_CALL_ENDED = 270344;
    protected static final int EVENT_VOICE_CALL_STARTED = 270343;
    public static String EXTRA_MESSENGER = "EXTRA_MESSENGER";
    protected static final String FAIL_DATA_SETUP_COUNTER = "fail_data_setup_counter";
    protected static final String FAIL_DATA_SETUP_FAIL_CAUSE = "fail_data_setup_fail_cause";
    protected static final String INTENT_RECONNECT_ALARM_EXTRA_REASON = "reconnect_alarm_extra_reason";
    protected static final String INTENT_SET_FAIL_DATA_SETUP_COUNTER = "com.android.internal.telephony.dataconnectiontracker.intent_set_fail_data_setup_counter";
    protected static final int NO_RECV_POLL_LIMIT = 24;
    protected static final String NULL_IP = "0.0.0.0";
    protected static final int NUMBER_SENT_PACKETS_OF_HANG = 10;
    protected static final int POLL_LONGEST_RTT = 120000;
    protected static final int POLL_NETSTAT_MILLIS = 1000;
    protected static final int POLL_NETSTAT_SCREEN_OFF_MILLIS = 600000;
    protected static final int POLL_NETSTAT_SLOW_MILLIS = 5000;
    protected static final int RESTORE_DEFAULT_APN_DELAY = 60000;
    protected static final String SECONDARY_DATA_RETRY_CONFIG = "max_retries=3, 5000, 5000, 5000";
    protected static final boolean VDBG;
    protected static boolean sPolicyDataEnabled = true;
    private boolean[] dataEnabled = new boolean[8];
    private int enabledCount = 0;
    protected ApnSetting mActiveApn;
    protected Activity mActivity = Activity.NONE;
    protected ArrayList<ApnSetting> mAllApns = null;
    protected ConcurrentHashMap<String, ApnContext> mApnContexts = new ConcurrentHashMap();
    protected HashMap<String, Integer> mApnToDataConnectionId = new HashMap();
    protected boolean mAutoAttachOnCreation = false;
    protected int mCidActive;
    protected HashMap<Integer, DataConnectionAc> mDataConnectionAsyncChannels = new HashMap();
    protected Handler mDataConnectionTracker = null;
    protected HashMap<Integer, DataConnection> mDataConnections = new HashMap();
    protected Object mDataEnabledLock = new Object();
    private final DataRoamingSettingObserver mDataRoamingSettingObserver;
    protected PendingIntent mDataStallAlarmIntent = null;
    protected int mDataStallAlarmTag = (int)SystemClock.elapsedRealtime();
    protected TxRxSum mDataStallTxRxSum = new TxRxSum(0L, 0L);
    protected int mFailDataSetupCounter = 0;
    protected DataConnection.FailCause mFailDataSetupFailCause = DataConnection.FailCause.ERROR_UNSPECIFIED;
    protected BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            int i = 1;
            String str = paramAnonymousIntent.getAction();
            DataConnectionTracker.this.log("onReceive: action=" + str);
            if (str.equals("android.intent.action.SCREEN_ON"))
            {
                DataConnectionTracker.this.mIsScreenOn = i;
                DataConnectionTracker.this.stopNetStatPoll();
                DataConnectionTracker.this.startNetStatPoll();
                DataConnectionTracker.this.restartDataStallAlarm();
            }
            while (true)
            {
                return;
                if (str.equals("android.intent.action.SCREEN_OFF"))
                {
                    DataConnectionTracker.this.mIsScreenOn = false;
                    DataConnectionTracker.this.stopNetStatPoll();
                    DataConnectionTracker.this.startNetStatPoll();
                    DataConnectionTracker.this.restartDataStallAlarm();
                }
                else if (str.startsWith(DataConnectionTracker.this.getActionIntentReconnectAlarm()))
                {
                    DataConnectionTracker.this.log("Reconnect alarm. Previous state was " + DataConnectionTracker.this.mState);
                    DataConnectionTracker.this.onActionIntentReconnectAlarm(paramAnonymousIntent);
                }
                else if (str.equals(DataConnectionTracker.this.getActionIntentDataStallAlarm()))
                {
                    DataConnectionTracker.this.onActionIntentDataStallAlarm(paramAnonymousIntent);
                }
                else
                {
                    if (str.equals("android.net.wifi.STATE_CHANGE"))
                    {
                        NetworkInfo localNetworkInfo = (NetworkInfo)paramAnonymousIntent.getParcelableExtra("networkInfo");
                        DataConnectionTracker localDataConnectionTracker = DataConnectionTracker.this;
                        if ((localNetworkInfo != null) && (localNetworkInfo.isConnected()));
                        while (true)
                        {
                            localDataConnectionTracker.mIsWifiConnected = i;
                            break;
                            i = 0;
                        }
                    }
                    if (str.equals("android.net.wifi.WIFI_STATE_CHANGED"))
                    {
                        if (paramAnonymousIntent.getIntExtra("wifi_state", 4) == 3);
                        for (int j = i; ; j = 0)
                        {
                            if (j != 0)
                                break label299;
                            DataConnectionTracker.this.mIsWifiConnected = false;
                            break;
                        }
                    }
                    else
                    {
                        label299: if (str.equals("com.android.internal.telephony.dataconnectiontracker.intent_set_fail_data_setup_counter"))
                        {
                            DataConnectionTracker.this.mFailDataSetupCounter = paramAnonymousIntent.getIntExtra("fail_data_setup_counter", i);
                            DataConnectionTracker.this.mFailDataSetupFailCause = DataConnection.FailCause.fromInt(paramAnonymousIntent.getIntExtra("fail_data_setup_fail_cause", DataConnection.FailCause.ERROR_UNSPECIFIED.getErrorCode()));
                            DataConnectionTracker.this.log("set mFailDataSetupCounter=" + DataConnectionTracker.this.mFailDataSetupCounter + " mFailDataSetupFailCause=" + DataConnectionTracker.this.mFailDataSetupFailCause);
                        }
                    }
                }
            }
        }
    };
    protected boolean mInternalDataEnabled = true;
    protected boolean mIsDisposed = false;
    protected boolean mIsPsRestricted = false;
    protected boolean mIsScreenOn = true;
    protected boolean mIsWifiConnected = false;
    protected boolean mNetStatPollEnabled = false;
    protected int mNetStatPollPeriod;
    protected int mNoRecvPollCount = 0;
    protected PhoneBase mPhone;
    protected ApnSetting mPreferredApn = null;
    protected PendingIntent mReconnectIntent = null;
    protected String mRequestedApnType = "default";
    protected long mRxPkts;
    protected long mSentSinceLastRecv;
    protected State mState = State.IDLE;
    protected long mTxPkts;
    protected AtomicInteger mUniqueIdGenerator = new AtomicInteger(0);
    protected boolean mUserDataEnabled = true;

    protected DataConnectionTracker(PhoneBase paramPhoneBase)
    {
        log("DCT.constructor");
        this.mPhone = paramPhoneBase;
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction(getActionIntentReconnectAlarm());
        localIntentFilter.addAction("android.intent.action.SCREEN_ON");
        localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
        localIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
        localIntentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        localIntentFilter.addAction("com.android.internal.telephony.dataconnectiontracker.intent_set_fail_data_setup_counter");
        if (Settings.Secure.getInt(this.mPhone.getContext().getContentResolver(), "mobile_data", 1) == 1);
        for (boolean bool = true; ; bool = false)
        {
            this.mUserDataEnabled = bool;
            this.mPhone.getContext().registerReceiver(this.mIntentReceiver, localIntentFilter, null, this.mPhone);
            this.dataEnabled[0] = SystemProperties.getBoolean("net.def_data_on_boot", true);
            if (this.dataEnabled[0] != 0)
                this.enabledCount = (1 + this.enabledCount);
            this.mAutoAttachOnCreation = PreferenceManager.getDefaultSharedPreferences(this.mPhone.getContext()).getBoolean("disabled_on_boot_key", false);
            this.mDataRoamingSettingObserver = new DataRoamingSettingObserver(this.mPhone);
            this.mDataRoamingSettingObserver.register(this.mPhone.getContext());
            return;
        }
    }

    private void handleDataOnRoamingChange()
    {
        if (this.mPhone.getServiceState().getRoaming())
        {
            if (getDataOnRoamingEnabled())
                resetAllRetryCounts();
            sendMessage(obtainMessage(270347));
        }
    }

    private void notifyApnIdDisconnected(String paramString, int paramInt)
    {
        this.mPhone.notifyDataConnection(paramString, apnIdToType(paramInt), Phone.DataState.DISCONNECTED);
    }

    private void notifyApnIdUpToCurrent(String paramString, int paramInt)
    {
        switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$State[this.mState.ordinal()])
        {
        case 1:
        case 2:
        default:
        case 3:
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return;
            this.mPhone.notifyDataConnection(paramString, apnIdToType(paramInt), Phone.DataState.CONNECTING);
            continue;
            this.mPhone.notifyDataConnection(paramString, apnIdToType(paramInt), Phone.DataState.CONNECTING);
            this.mPhone.notifyDataConnection(paramString, apnIdToType(paramInt), Phone.DataState.CONNECTED);
        }
    }

    protected String apnIdToType(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            log("Unknown id (" + paramInt + ") in apnIdToType");
            str = "default";
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            return str;
            str = "default";
            continue;
            str = "mms";
            continue;
            str = "supl";
            continue;
            str = "dun";
            continue;
            str = "hipri";
            continue;
            str = "ims";
            continue;
            str = "fota";
            continue;
            str = "cbs";
        }
    }

    protected int apnTypeToId(String paramString)
    {
        int i;
        if (TextUtils.equals(paramString, "default"))
            i = 0;
        while (true)
        {
            return i;
            if (TextUtils.equals(paramString, "mms"))
                i = 1;
            else if (TextUtils.equals(paramString, "supl"))
                i = 2;
            else if (TextUtils.equals(paramString, "dun"))
                i = 3;
            else if (TextUtils.equals(paramString, "hipri"))
                i = 4;
            else if (TextUtils.equals(paramString, "ims"))
                i = 5;
            else if (TextUtils.equals(paramString, "fota"))
                i = 6;
            else if (TextUtils.equals(paramString, "cbs"))
                i = 7;
            else
                i = -1;
        }
    }

    protected void broadcastMessenger()
    {
        Intent localIntent = new Intent(ACTION_DATA_CONNECTION_TRACKER_MESSENGER);
        localIntent.putExtra(EXTRA_MESSENGER, new Messenger(this));
        this.mPhone.getContext().sendBroadcast(localIntent);
    }

    public void cleanUpAllConnections(String paramString)
    {
        Message localMessage = obtainMessage(270366);
        localMessage.obj = paramString;
        sendMessage(localMessage);
    }

    /** @deprecated */
    public int disableApnType(String paramString)
    {
        int i = 3;
        try
        {
            log("disableApnType(" + paramString + ")");
            int j = apnTypeToId(paramString);
            if (j == -1);
            while (true)
            {
                return i;
                if (isApnIdEnabled(j))
                {
                    setEnabled(j, false);
                    if (isApnTypeActive("default"))
                    {
                        int k = this.dataEnabled[0];
                        if (k != 0)
                            i = 0;
                        else
                            i = 1;
                    }
                    else
                    {
                        i = 1;
                    }
                }
            }
        }
        finally
        {
        }
    }

    public void dispose()
    {
        log("DCT.dispose");
        Iterator localIterator = this.mDataConnectionAsyncChannels.values().iterator();
        while (localIterator.hasNext())
            ((DataConnectionAc)localIterator.next()).disconnect();
        this.mDataConnectionAsyncChannels.clear();
        this.mIsDisposed = true;
        this.mPhone.getContext().unregisterReceiver(this.mIntentReceiver);
        this.mDataRoamingSettingObserver.unregister(this.mPhone.getContext());
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("DataConnectionTracker:");
        paramPrintWriter.println(" mInternalDataEnabled=" + this.mInternalDataEnabled);
        paramPrintWriter.println(" mUserDataEnabled=" + this.mUserDataEnabled);
        paramPrintWriter.println(" sPolicyDataEnabed=" + sPolicyDataEnabled);
        paramPrintWriter.println(" dataEnabled:");
        for (int i = 0; i < this.dataEnabled.length; i++)
        {
            Object[] arrayOfObject4 = new Object[2];
            arrayOfObject4[0] = Integer.valueOf(i);
            arrayOfObject4[1] = Boolean.valueOf(this.dataEnabled[i]);
            paramPrintWriter.printf("    dataEnabled[%d]=%b\n", arrayOfObject4);
        }
        paramPrintWriter.flush();
        paramPrintWriter.println(" enabledCount=" + this.enabledCount);
        paramPrintWriter.println(" mRequestedApnType=" + this.mRequestedApnType);
        paramPrintWriter.println(" mPhone=" + this.mPhone.getPhoneName());
        paramPrintWriter.println(" mActivity=" + this.mActivity);
        paramPrintWriter.println(" mState=" + this.mState);
        paramPrintWriter.println(" mTxPkts=" + this.mTxPkts);
        paramPrintWriter.println(" mRxPkts=" + this.mRxPkts);
        paramPrintWriter.println(" mNetStatPollPeriod=" + this.mNetStatPollPeriod);
        paramPrintWriter.println(" mNetStatPollEnabled=" + this.mNetStatPollEnabled);
        paramPrintWriter.println(" mDataStallTxRxSum=" + this.mDataStallTxRxSum);
        paramPrintWriter.println(" mDataStallAlarmTag=" + this.mDataStallAlarmTag);
        paramPrintWriter.println(" mSentSinceLastRecv=" + this.mSentSinceLastRecv);
        paramPrintWriter.println(" mNoRecvPollCount=" + this.mNoRecvPollCount);
        paramPrintWriter.println(" mIsWifiConnected=" + this.mIsWifiConnected);
        paramPrintWriter.println(" mReconnectIntent=" + this.mReconnectIntent);
        paramPrintWriter.println(" mCidActive=" + this.mCidActive);
        paramPrintWriter.println(" mAutoAttachOnCreation=" + this.mAutoAttachOnCreation);
        paramPrintWriter.println(" mIsScreenOn=" + this.mIsScreenOn);
        paramPrintWriter.println(" mUniqueIdGenerator=" + this.mUniqueIdGenerator);
        paramPrintWriter.flush();
        paramPrintWriter.println(" ***************************************");
        Set localSet1 = this.mDataConnections.entrySet();
        paramPrintWriter.println(" mDataConnections: count=" + localSet1.size());
        Iterator localIterator1 = localSet1.iterator();
        while (localIterator1.hasNext())
        {
            Map.Entry localEntry2 = (Map.Entry)localIterator1.next();
            Object[] arrayOfObject3 = new Object[1];
            arrayOfObject3[0] = localEntry2.getKey();
            paramPrintWriter.printf(" *** mDataConnection[%d] \n", arrayOfObject3);
            ((DataConnection)localEntry2.getValue()).dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        }
        paramPrintWriter.println(" ***************************************");
        paramPrintWriter.flush();
        Set localSet2 = this.mApnToDataConnectionId.entrySet();
        paramPrintWriter.println(" mApnToDataConnectonId size=" + localSet2.size());
        Iterator localIterator2 = localSet2.iterator();
        while (localIterator2.hasNext())
        {
            Map.Entry localEntry1 = (Map.Entry)localIterator2.next();
            Object[] arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = localEntry1.getKey();
            arrayOfObject2[1] = localEntry1.getValue();
            paramPrintWriter.printf(" mApnToDataConnectonId[%s]=%d\n", arrayOfObject2);
        }
        paramPrintWriter.println(" ***************************************");
        paramPrintWriter.flush();
        if (this.mApnContexts != null)
        {
            Set localSet3 = this.mApnContexts.entrySet();
            paramPrintWriter.println(" mApnContexts size=" + localSet3.size());
            Iterator localIterator3 = localSet3.iterator();
            while (localIterator3.hasNext())
                ((ApnContext)((Map.Entry)localIterator3.next()).getValue()).dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
            paramPrintWriter.println(" ***************************************");
        }
        while (true)
        {
            paramPrintWriter.flush();
            paramPrintWriter.println(" mActiveApn=" + this.mActiveApn);
            if (this.mAllApns == null)
                break label1312;
            paramPrintWriter.println(" mAllApns size=" + this.mAllApns.size());
            for (int j = 0; j < this.mAllApns.size(); j++)
            {
                Object[] arrayOfObject1 = new Object[2];
                arrayOfObject1[0] = Integer.valueOf(j);
                arrayOfObject1[1] = this.mAllApns.get(j);
                paramPrintWriter.printf(" mAllApns[%d]: %s\n", arrayOfObject1);
            }
            paramPrintWriter.println(" mApnContexts=null");
        }
        paramPrintWriter.flush();
        while (true)
        {
            paramPrintWriter.println(" mPreferredApn=" + this.mPreferredApn);
            paramPrintWriter.println(" mIsPsRestricted=" + this.mIsPsRestricted);
            paramPrintWriter.println(" mIsDisposed=" + this.mIsDisposed);
            paramPrintWriter.println(" mIntentReceiver=" + this.mIntentReceiver);
            paramPrintWriter.println(" mDataRoamingSettingObserver=" + this.mDataRoamingSettingObserver);
            paramPrintWriter.flush();
            return;
            label1312: paramPrintWriter.println(" mAllApns=null");
        }
    }

    /** @deprecated */
    public int enableApnType(String paramString)
    {
        int i = 1;
        try
        {
            int j = apnTypeToId(paramString);
            if (j == -1)
                i = 3;
            while (true)
            {
                return i;
                log("enableApnType(" + paramString + "), isApnTypeActive = " + isApnTypeActive(paramString) + ", isApnIdEnabled =" + isApnIdEnabled(j) + " and state = " + this.mState);
                if (!isApnTypeAvailable(paramString))
                {
                    log("type not available");
                    i = 2;
                }
                else if (isApnIdEnabled(j))
                {
                    i = 0;
                }
                else
                {
                    setEnabled(j, true);
                }
            }
        }
        finally
        {
        }
    }

    protected ApnSetting fetchDunApn()
    {
        ApnSetting localApnSetting;
        if (SystemProperties.getBoolean("net.tethering.noprovisioning", false))
        {
            log("fetchDunApn: net.tethering.noprovisioning=true ret: null");
            localApnSetting = null;
        }
        while (true)
        {
            return localApnSetting;
            Context localContext = this.mPhone.getContext();
            localApnSetting = ApnSetting.fromString(Settings.Secure.getString(localContext.getContentResolver(), "tether_dun_apn"));
            if (localApnSetting == null)
                localApnSetting = ApnSetting.fromString(localContext.getResources().getString(17039385));
        }
    }

    protected abstract String getActionIntentDataStallAlarm();

    protected abstract String getActionIntentReconnectAlarm();

    public String getActiveApnString(String paramString)
    {
        String str = null;
        if (this.mActiveApn != null)
            str = this.mActiveApn.apn;
        return str;
    }

    public String[] getActiveApnTypes()
    {
        String[] arrayOfString;
        if (this.mActiveApn != null)
            arrayOfString = this.mActiveApn.types;
        while (true)
        {
            return arrayOfString;
            arrayOfString = new String[1];
            arrayOfString[0] = "default";
        }
    }

    public Activity getActivity()
    {
        return this.mActivity;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean getAnyDataEnabled()
    {
        synchronized (this.mDataEnabledLock)
        {
            if ((this.mInternalDataEnabled) && (this.mUserDataEnabled) && (sPolicyDataEnabled) && (this.enabledCount != 0))
            {
                bool1 = true;
                boolean bool2 = Injector.getAnyDataEnabled(this, bool1);
                if (!bool2)
                    log("getAnyDataEnabled " + bool2);
                return bool2;
            }
            boolean bool1 = false;
        }
    }

    public boolean getDataOnRoamingEnabled()
    {
        boolean bool = false;
        try
        {
            int i = Settings.Secure.getInt(this.mPhone.getContext().getContentResolver(), "data_roaming");
            if (i != 0)
                bool = true;
            label25: return bool;
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label25;
        }
    }

    protected LinkCapabilities getLinkCapabilities(String paramString)
    {
        if (isApnIdEnabled(apnTypeToId(paramString)));
        for (LinkCapabilities localLinkCapabilities = ((DataConnectionAc)this.mDataConnectionAsyncChannels.get(Integer.valueOf(0))).getLinkCapabilitiesSync(); ; localLinkCapabilities = new LinkCapabilities())
            return localLinkCapabilities;
    }

    protected LinkProperties getLinkProperties(String paramString)
    {
        if (isApnIdEnabled(apnTypeToId(paramString)));
        for (LinkProperties localLinkProperties = ((DataConnectionAc)this.mDataConnectionAsyncChannels.get(Integer.valueOf(0))).getLinkPropertiesSync(); ; localLinkProperties = new LinkProperties())
            return localLinkProperties;
    }

    protected String getReryConfig(boolean paramBoolean)
    {
        int i = this.mPhone.getServiceState().getNetworkType();
        String str;
        if ((i == 4) || (i == 7) || (i == 5) || (i == 6) || (i == 12) || (i == 14))
            str = SystemProperties.get("ro.cdma.data_retry_config");
        while (true)
        {
            return str;
            if (paramBoolean)
                str = SystemProperties.get("ro.gsm.data_retry_config");
            else
                str = SystemProperties.get("ro.gsm.2nd_data_retry_config");
        }
    }

    public abstract State getState(String paramString);

    protected abstract void gotoIdleAndNotifyDataConnection(String paramString);

    public void handleMessage(Message paramMessage)
    {
        boolean bool1 = false;
        switch (paramMessage.what)
        {
        default:
            Log.e("DATA", "Unidentified event msg=" + paramMessage);
        case 69636:
        case 270349:
        case 270339:
        case 270353:
        case 270348:
        case 270347:
        case 270337:
        case 270342:
        case 270336:
        case 270351:
        case 270343:
        case 270344:
        case 270366:
        case 270360:
        case 270363:
        case 270364:
            while (true)
            {
                return;
                log("DISCONNECTED_CONNECTED: msg=" + paramMessage);
                DataConnectionAc localDataConnectionAc = (DataConnectionAc)paramMessage.obj;
                this.mDataConnectionAsyncChannels.remove(Integer.valueOf(localDataConnectionAc.dataConnection.getDataConnectionId()));
                localDataConnectionAc.disconnected();
                continue;
                onEnableApn(paramMessage.arg1, paramMessage.arg2);
                continue;
                String str2 = null;
                if ((paramMessage.obj instanceof String))
                    str2 = (String)paramMessage.obj;
                onTrySetupData(str2);
                continue;
                onDataStallAlarm(paramMessage.arg1);
                continue;
                if (!getDataOnRoamingEnabled())
                    resetAllRetryCounts();
                onRoamingOff();
                continue;
                onRoamingOn();
                continue;
                onRadioAvailable();
                continue;
                onRadioOffOrNotAvailable();
                continue;
                this.mCidActive = paramMessage.arg1;
                onDataSetupComplete((AsyncResult)paramMessage.obj);
                continue;
                log("DataConnectoinTracker.handleMessage: EVENT_DISCONNECT_DONE msg=" + paramMessage);
                onDisconnectDone(paramMessage.arg1, (AsyncResult)paramMessage.obj);
                continue;
                onVoiceCallStarted();
                continue;
                onVoiceCallEnded();
                continue;
                onCleanUpAllConnections((String)paramMessage.obj);
                continue;
                if (paramMessage.arg1 == 0);
                while (true)
                {
                    onCleanUpConnection(bool1, paramMessage.arg2, (String)paramMessage.obj);
                    break;
                    bool1 = true;
                }
                if (paramMessage.arg1 == 1);
                for (boolean bool5 = true; ; bool5 = false)
                {
                    onSetInternalDataEnabled(bool5);
                    break;
                }
                log("EVENT_RESET_DONE");
                onResetDone((AsyncResult)paramMessage.obj);
            }
        case 270365:
            if (paramMessage.arg1 == 1);
            for (boolean bool4 = true; ; bool4 = false)
            {
                log("CMD_SET_USER_DATA_ENABLE enabled=" + bool4);
                onSetUserDataEnabled(bool4);
                break;
            }
        case 270367:
            if (paramMessage.arg1 == 1);
            for (boolean bool3 = true; ; bool3 = false)
            {
                log("CMD_SET_DEPENDENCY_MET met=" + bool3);
                Bundle localBundle = paramMessage.getData();
                if (localBundle == null)
                    break;
                String str1 = (String)localBundle.get("apnType");
                if (str1 == null)
                    break;
                onSetDependencyMet(str1, bool3);
                break;
            }
        case 270368:
        }
        if (paramMessage.arg1 == 1);
        for (boolean bool2 = true; ; bool2 = false)
        {
            onSetPolicyDataEnabled(bool2);
            break;
        }
    }

    /** @deprecated */
    protected boolean isApnIdEnabled(int paramInt)
    {
        if (paramInt != -1);
        try
        {
            int i = this.dataEnabled[paramInt];
            return i;
            int j = 0;
        }
        finally
        {
        }
    }

    public boolean isApnTypeActive(String paramString)
    {
        boolean bool = true;
        if ("dun".equals(paramString))
        {
            ApnSetting localApnSetting = fetchDunApn();
            if (localApnSetting != null)
                if ((this.mActiveApn == null) || (!localApnSetting.toString().equals(this.mActiveApn.toString())));
        }
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if ((this.mActiveApn == null) || (!this.mActiveApn.canHandleType(paramString)))
                bool = false;
        }
    }

    protected abstract boolean isApnTypeAvailable(String paramString);

    public boolean isApnTypeEnabled(String paramString)
    {
        if (paramString == null);
        for (boolean bool = false; ; bool = isApnIdEnabled(apnTypeToId(paramString)))
            return bool;
    }

    protected abstract boolean isDataAllowed();

    protected abstract boolean isDataPossible(String paramString);

    protected boolean isDataSetupCompleteOk(AsyncResult paramAsyncResult)
    {
        boolean bool = false;
        if (paramAsyncResult.exception != null)
            log("isDataSetupCompleteOk return false, ar.result=" + paramAsyncResult.result);
        while (true)
        {
            return bool;
            if (this.mFailDataSetupCounter <= 0)
            {
                log("isDataSetupCompleteOk return true");
                bool = true;
            }
            else
            {
                paramAsyncResult.result = this.mFailDataSetupFailCause;
                log("isDataSetupCompleteOk return false mFailDataSetupCounter=" + this.mFailDataSetupCounter + " mFailDataSetupFailCause=" + this.mFailDataSetupFailCause);
                this.mFailDataSetupCounter = (-1 + this.mFailDataSetupCounter);
            }
        }
    }

    public abstract boolean isDisconnected();

    protected boolean isEmergency()
    {
        while (true)
        {
            synchronized (this.mDataEnabledLock)
            {
                if (!this.mPhone.isInEcm())
                    if (this.mPhone.isInEmergencyCall())
                    {
                        break label68;
                        log("isEmergency: result=" + bool);
                        return bool;
                    }
                    else
                    {
                        bool = false;
                    }
            }
            label68: boolean bool = true;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    protected boolean isMmsDataEnabled()
    {
        int i = 1;
        if ((this.mRequestedApnType.equals("mms")) && (Settings.System.getInt(this.mPhone.getContext().getContentResolver(), "always_enable_mms", i) == i));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    protected abstract void log(String paramString);

    protected abstract void loge(String paramString);

    protected void notifyDataConnection(String paramString)
    {
        for (int i = 0; i < 8; i++)
            if (this.dataEnabled[i] != 0)
                this.mPhone.notifyDataConnection(paramString, apnIdToType(i));
        notifyOffApnsOfAvailability(paramString);
    }

    protected void notifyOffApnsOfAvailability(String paramString)
    {
        log("notifyOffApnsOfAvailability - reason= " + paramString);
        for (int i = 0; i < 8; i++)
            if (!isApnIdEnabled(i))
                notifyApnIdDisconnected(paramString, i);
    }

    protected void onActionIntentDataStallAlarm(Intent paramIntent)
    {
        Message localMessage = obtainMessage(270353, paramIntent.getAction());
        localMessage.arg1 = paramIntent.getIntExtra("data.stall.alram.tag", 0);
        sendMessage(localMessage);
    }

    protected void onActionIntentReconnectAlarm(Intent paramIntent)
    {
        String str = paramIntent.getStringExtra("reconnect_alarm_extra_reason");
        if (this.mState == State.FAILED)
        {
            Message localMessage = obtainMessage(270360);
            localMessage.arg1 = 0;
            localMessage.arg2 = 0;
            localMessage.obj = str;
            sendMessage(localMessage);
        }
        sendMessage(obtainMessage(270339));
    }

    protected abstract void onCleanUpAllConnections(String paramString);

    protected abstract void onCleanUpConnection(boolean paramBoolean, int paramInt, String paramString);

    protected abstract void onDataSetupComplete(AsyncResult paramAsyncResult);

    protected void onDataStallAlarm(int paramInt)
    {
        loge("onDataStallAlarm: not impleted tag=" + paramInt);
    }

    protected abstract void onDisconnectDone(int paramInt, AsyncResult paramAsyncResult);

    protected void onEnableApn(int paramInt1, int paramInt2)
    {
        log("EVENT_APN_ENABLE_REQUEST apnId=" + paramInt1 + ", apnType=" + apnIdToType(paramInt1) + ", enabled=" + paramInt2 + ", dataEnabled = " + this.dataEnabled[paramInt1] + ", enabledCount = " + this.enabledCount + ", isApnTypeActive = " + isApnTypeActive(apnIdToType(paramInt1)));
        if (paramInt2 == 1);
        while (true)
        {
            try
            {
                if (this.dataEnabled[paramInt1] == 0)
                {
                    this.dataEnabled[paramInt1] = true;
                    this.enabledCount = (1 + this.enabledCount);
                }
                String str = apnIdToType(paramInt1);
                if (!isApnTypeActive(str))
                {
                    this.mRequestedApnType = str;
                    onEnableNewApn();
                    return;
                }
            }
            finally
            {
            }
            notifyApnIdUpToCurrent("apnSwitched", paramInt1);
            continue;
            int i = 0;
            try
            {
                if (this.dataEnabled[paramInt1] != 0)
                {
                    this.dataEnabled[paramInt1] = false;
                    this.enabledCount = (-1 + this.enabledCount);
                    i = 1;
                }
                if (i == 0)
                    continue;
                if ((this.enabledCount == 0) || (paramInt1 == 3))
                {
                    this.mRequestedApnType = "default";
                    onCleanUpConnection(true, paramInt1, "dataDisabled");
                }
                notifyApnIdDisconnected("dataDisabled", paramInt1);
                if ((this.dataEnabled[0] != 1) || (isApnTypeActive("default")))
                    continue;
                this.mRequestedApnType = "default";
                onEnableNewApn();
            }
            finally
            {
            }
        }
    }

    protected void onEnableNewApn()
    {
    }

    protected abstract void onRadioAvailable();

    protected abstract void onRadioOffOrNotAvailable();

    protected void onResetDone(AsyncResult paramAsyncResult)
    {
        log("EVENT_RESET_DONE");
        String str = null;
        if ((paramAsyncResult.userObj instanceof String))
            str = (String)paramAsyncResult.userObj;
        gotoIdleAndNotifyDataConnection(str);
    }

    protected abstract void onRoamingOff();

    protected abstract void onRoamingOn();

    protected void onSetDependencyMet(String paramString, boolean paramBoolean)
    {
    }

    protected void onSetInternalDataEnabled(boolean paramBoolean)
    {
        synchronized (this.mDataEnabledLock)
        {
            this.mInternalDataEnabled = paramBoolean;
            if (paramBoolean)
            {
                log("onSetInternalDataEnabled: changed to enabled, try to setup data call");
                resetAllRetryCounts();
                onTrySetupData("dataEnabled");
                return;
            }
            log("onSetInternalDataEnabled: changed to disabled, cleanUpAllConnections");
            cleanUpAllConnections(null);
        }
    }

    protected void onSetPolicyDataEnabled(boolean paramBoolean)
    {
        synchronized (this.mDataEnabledLock)
        {
            boolean bool = getAnyDataEnabled();
            if (sPolicyDataEnabled != paramBoolean)
            {
                sPolicyDataEnabled = paramBoolean;
                if (bool != getAnyDataEnabled())
                {
                    if (bool)
                        break label53;
                    resetAllRetryCounts();
                    onTrySetupData("dataEnabled");
                }
            }
            return;
            label53: onCleanUpAllConnections("dataDisabled");
        }
    }

    // ERROR //
    protected void onSetUserDataEnabled(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 263	com/android/internal/telephony/DataConnectionTracker:mDataEnabledLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: invokevirtual 1110	com/android/internal/telephony/DataConnectionTracker:getAnyDataEnabled	()Z
        //     11: istore 4
        //     13: aload_0
        //     14: getfield 267	com/android/internal/telephony/DataConnectionTracker:mUserDataEnabled	Z
        //     17: iload_1
        //     18: if_icmpeq +96 -> 114
        //     21: aload_0
        //     22: iload_1
        //     23: putfield 267	com/android/internal/telephony/DataConnectionTracker:mUserDataEnabled	Z
        //     26: aload_0
        //     27: getfield 365	com/android/internal/telephony/DataConnectionTracker:mPhone	Lcom/android/internal/telephony/PhoneBase;
        //     30: invokevirtual 389	com/android/internal/telephony/PhoneBase:getContext	()Landroid/content/Context;
        //     33: invokevirtual 395	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     36: astore 5
        //     38: iload_1
        //     39: ifeq +103 -> 142
        //     42: iconst_1
        //     43: istore 6
        //     45: aload 5
        //     47: ldc_w 397
        //     50: iload 6
        //     52: invokestatic 1114	android/provider/Settings$Secure:putInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
        //     55: pop
        //     56: aload_0
        //     57: invokevirtual 447	com/android/internal/telephony/DataConnectionTracker:getDataOnRoamingEnabled	()Z
        //     60: ifne +28 -> 88
        //     63: aload_0
        //     64: getfield 365	com/android/internal/telephony/DataConnectionTracker:mPhone	Lcom/android/internal/telephony/PhoneBase;
        //     67: invokevirtual 438	com/android/internal/telephony/PhoneBase:getServiceState	()Landroid/telephony/ServiceState;
        //     70: invokevirtual 444	android/telephony/ServiceState:getRoaming	()Z
        //     73: iconst_1
        //     74: if_icmpne +14 -> 88
        //     77: iload_1
        //     78: ifeq +39 -> 117
        //     81: aload_0
        //     82: ldc_w 1116
        //     85: invokevirtual 1050	com/android/internal/telephony/DataConnectionTracker:notifyOffApnsOfAvailability	(Ljava/lang/String;)V
        //     88: iload 4
        //     90: aload_0
        //     91: invokevirtual 1110	com/android/internal/telephony/DataConnectionTracker:getAnyDataEnabled	()Z
        //     94: if_icmpeq +20 -> 114
        //     97: iload 4
        //     99: ifne +33 -> 132
        //     102: aload_0
        //     103: invokevirtual 450	com/android/internal/telephony/DataConnectionTracker:resetAllRetryCounts	()V
        //     106: aload_0
        //     107: ldc_w 1104
        //     110: invokevirtual 926	com/android/internal/telephony/DataConnectionTracker:onTrySetupData	(Ljava/lang/String;)Z
        //     113: pop
        //     114: aload_2
        //     115: monitorexit
        //     116: return
        //     117: aload_0
        //     118: ldc_w 1096
        //     121: invokevirtual 1050	com/android/internal/telephony/DataConnectionTracker:notifyOffApnsOfAvailability	(Ljava/lang/String;)V
        //     124: goto -36 -> 88
        //     127: astore_3
        //     128: aload_2
        //     129: monitorexit
        //     130: aload_3
        //     131: athrow
        //     132: aload_0
        //     133: ldc_w 1096
        //     136: invokevirtual 962	com/android/internal/telephony/DataConnectionTracker:onCleanUpAllConnections	(Ljava/lang/String;)V
        //     139: goto -25 -> 114
        //     142: iconst_0
        //     143: istore 6
        //     145: goto -100 -> 45
        //
        // Exception table:
        //     from	to	target	type
        //     7	130	127	finally
        //     132	139	127	finally
    }

    protected abstract boolean onTrySetupData(String paramString);

    protected abstract void onVoiceCallEnded();

    protected abstract void onVoiceCallStarted();

    protected void resetAllRetryCounts()
    {
        Iterator localIterator1 = this.mApnContexts.values().iterator();
        while (localIterator1.hasNext())
            ((ApnContext)localIterator1.next()).setRetryCount(0);
        Iterator localIterator2 = this.mDataConnections.values().iterator();
        while (localIterator2.hasNext())
            ((DataConnection)localIterator2.next()).resetRetryCount();
    }

    protected abstract void restartDataStallAlarm();

    protected abstract void restartRadio();

    public void setDataOnRoamingEnabled(boolean paramBoolean)
    {
        ContentResolver localContentResolver;
        if (getDataOnRoamingEnabled() != paramBoolean)
        {
            localContentResolver = this.mPhone.getContext().getContentResolver();
            if (!paramBoolean)
                break label35;
        }
        label35: for (int i = 1; ; i = 0)
        {
            Settings.Secure.putInt(localContentResolver, "data_roaming", i);
            return;
        }
    }

    protected void setEnabled(int paramInt, boolean paramBoolean)
    {
        log("setEnabled(" + paramInt + ", " + paramBoolean + ") with old state = " + this.dataEnabled[paramInt] + " and enabledCount = " + this.enabledCount);
        Message localMessage = obtainMessage(270349);
        localMessage.arg1 = paramInt;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localMessage.arg2 = i;
            sendMessage(localMessage);
            return;
        }
    }

    public boolean setInternalDataEnabled(boolean paramBoolean)
    {
        log("setInternalDataEnabled(" + paramBoolean + ")");
        Message localMessage = obtainMessage(270363);
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localMessage.arg1 = i;
            sendMessage(localMessage);
            return true;
        }
    }

    protected abstract void setState(State paramState);

    protected abstract void startNetStatPoll();

    protected abstract void stopNetStatPoll();

    public class TxRxSum
    {
        public long rxPkts;
        public long txPkts;

        public TxRxSum()
        {
            reset();
        }

        public TxRxSum(long arg2, long arg4)
        {
            this.txPkts = ???;
            Object localObject;
            this.rxPkts = localObject;
        }

        public TxRxSum(TxRxSum arg2)
        {
            Object localObject;
            this.txPkts = localObject.txPkts;
            this.rxPkts = localObject.rxPkts;
        }

        public void reset()
        {
            this.txPkts = -1L;
            this.rxPkts = -1L;
        }

        public String toString()
        {
            return "{txSum=" + this.txPkts + " rxSum=" + this.rxPkts + "}";
        }

        public void updateTxRxSum()
        {
            int i = 0;
            int j = 0;
            long l1 = 0L;
            long l2 = 0L;
            Iterator localIterator = DataConnectionTracker.this.mApnContexts.values().iterator();
            while (localIterator.hasNext())
            {
                ApnContext localApnContext = (ApnContext)localIterator.next();
                if (localApnContext.getState() == DataConnectionTracker.State.CONNECTED)
                {
                    DataConnectionAc localDataConnectionAc = localApnContext.getDataConnectionAc();
                    if (localDataConnectionAc != null)
                    {
                        LinkProperties localLinkProperties = localDataConnectionAc.getLinkPropertiesSync();
                        if (localLinkProperties != null)
                        {
                            String str = localLinkProperties.getInterfaceName();
                            if (str != null)
                            {
                                long l3 = TrafficStats.getTxPackets(str);
                                if (l3 > 0L)
                                {
                                    i = 1;
                                    l1 += l3;
                                }
                                long l4 = TrafficStats.getRxPackets(str);
                                if (l4 > 0L)
                                {
                                    j = 1;
                                    l2 += l4;
                                }
                            }
                        }
                    }
                }
            }
            if (i != 0)
                this.txPkts = l1;
            if (j != 0)
                this.rxPkts = l2;
        }
    }

    private class DataRoamingSettingObserver extends ContentObserver
    {
        public DataRoamingSettingObserver(Handler arg2)
        {
            super();
        }

        public void onChange(boolean paramBoolean)
        {
            DataConnectionTracker.this.handleDataOnRoamingChange();
        }

        public void register(Context paramContext)
        {
            paramContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("data_roaming"), false, this);
        }

        public void unregister(Context paramContext)
        {
            paramContext.getContentResolver().unregisterContentObserver(this);
        }
    }

    public static enum Activity
    {
        static
        {
            DATAIN = new Activity("DATAIN", 1);
            DATAOUT = new Activity("DATAOUT", 2);
            DATAINANDOUT = new Activity("DATAINANDOUT", 3);
            DORMANT = new Activity("DORMANT", 4);
            Activity[] arrayOfActivity = new Activity[5];
            arrayOfActivity[0] = NONE;
            arrayOfActivity[1] = DATAIN;
            arrayOfActivity[2] = DATAOUT;
            arrayOfActivity[3] = DATAINANDOUT;
            arrayOfActivity[4] = DORMANT;
        }
    }

    public static enum State
    {
        static
        {
            CONNECTING = new State("CONNECTING", 2);
            SCANNING = new State("SCANNING", 3);
            CONNECTED = new State("CONNECTED", 4);
            DISCONNECTING = new State("DISCONNECTING", 5);
            FAILED = new State("FAILED", 6);
            State[] arrayOfState = new State[7];
            arrayOfState[0] = IDLE;
            arrayOfState[1] = INITING;
            arrayOfState[2] = CONNECTING;
            arrayOfState[3] = SCANNING;
            arrayOfState[4] = CONNECTED;
            arrayOfState[5] = DISCONNECTING;
            arrayOfState[6] = FAILED;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean getAnyDataEnabled(DataConnectionTracker paramDataConnectionTracker, boolean paramBoolean)
        {
            if ((paramDataConnectionTracker.isMmsDataEnabled()) || (paramBoolean));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.DataConnectionTracker
 * JD-Core Version:        0.6.2
 */