package com.android.internal.telephony;

import android.app.PendingIntent;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.net.ProxyProperties;
import android.os.AsyncResult;
import android.os.Message;
import android.os.SystemProperties;
import android.telephony.ServiceState;
import android.text.TextUtils;
import android.util.TimeUtils;
import com.android.internal.util.AsyncChannel;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class DataConnection extends StateMachine
{
    protected static final int BASE = 262144;
    private static final int CMD_TO_STRING_COUNT = 7;
    protected static final boolean DBG = true;
    protected static final int EVENT_CONNECT = 262144;
    protected static final int EVENT_DEACTIVATE_DONE = 262147;
    protected static final int EVENT_DISCONNECT = 262148;
    protected static final int EVENT_DISCONNECT_ALL = 262150;
    protected static final int EVENT_GET_LAST_FAIL_DONE = 262146;
    protected static final int EVENT_LOG_BAD_DNS_ADDRESS = 50100;
    protected static final int EVENT_RIL_CONNECTED = 262149;
    protected static final int EVENT_SETUP_DATA_CONNECTION_DONE = 262145;
    protected static final String NULL_IP = "0.0.0.0";
    protected static final boolean VDBG;
    protected static int mCount;
    protected static Object mCountLock = new Object();
    private static String[] sCmdToString = new String[7];
    protected int cid;
    protected long createTime;
    protected FailCause lastFailCause;
    protected long lastFailTime;
    protected AsyncChannel mAc;
    private DcActivatingState mActivatingState = new DcActivatingState(null);
    private DcActiveState mActiveState = new DcActiveState(null);
    protected ApnSetting mApn;
    protected List<ApnContext> mApnList = null;
    protected LinkCapabilities mCapabilities = new LinkCapabilities();
    private DataConnectionTracker mDataConnectionTracker = null;
    private DcDefaultState mDefaultState = new DcDefaultState(null);
    private DcDisconnectionErrorCreatingConnection mDisconnectingErrorCreatingConnection = new DcDisconnectionErrorCreatingConnection(null);
    private DcDisconnectingState mDisconnectingState = new DcDisconnectingState(null);
    private int mId;
    private DcInactiveState mInactiveState = new DcInactiveState(null);
    protected LinkProperties mLinkProperties = new LinkProperties();
    PendingIntent mReconnectIntent = null;
    protected int mRefCount;
    private RetryManager mRetryMgr;
    protected int mRetryOverride = -1;
    protected int mRilVersion = -1;
    protected int mTag;
    protected PhoneBase phone;
    Object userData;

    static
    {
        sCmdToString[0] = "EVENT_CONNECT";
        sCmdToString[1] = "EVENT_SETUP_DATA_CONNECTION_DONE";
        sCmdToString[2] = "EVENT_GET_LAST_FAIL_DONE";
        sCmdToString[3] = "EVENT_DEACTIVATE_DONE";
        sCmdToString[4] = "EVENT_DISCONNECT";
        sCmdToString[5] = "EVENT_RIL_CONNECTED";
        sCmdToString[6] = "EVENT_DISCONNECT_ALL";
    }

    protected DataConnection(PhoneBase paramPhoneBase, String paramString, int paramInt, RetryManager paramRetryManager, DataConnectionTracker paramDataConnectionTracker)
    {
        super(paramString);
        setProcessedMessagesSize(100);
        log("DataConnection constructor E");
        this.phone = paramPhoneBase;
        this.mDataConnectionTracker = paramDataConnectionTracker;
        this.mId = paramInt;
        this.mRetryMgr = paramRetryManager;
        this.cid = -1;
        setDbg(false);
        addState(this.mDefaultState);
        addState(this.mInactiveState, this.mDefaultState);
        addState(this.mActivatingState, this.mDefaultState);
        addState(this.mActiveState, this.mDefaultState);
        addState(this.mDisconnectingState, this.mDefaultState);
        addState(this.mDisconnectingErrorCreatingConnection, this.mDefaultState);
        setInitialState(this.mInactiveState);
        this.mApnList = new ArrayList();
        log("DataConnection constructor X");
    }

    protected static String cmdToString(int paramInt)
    {
        int i = paramInt - 262144;
        if ((i >= 0) && (i < sCmdToString.length));
        for (String str = sCmdToString[i]; ; str = null)
            return str;
    }

    private int getSuggestedRetryTime(AsyncResult paramAsyncResult)
    {
        int i = -1;
        if (paramAsyncResult.exception == null)
            i = ((DataCallState)paramAsyncResult.result).suggestedRetryTime;
        return i;
    }

    private void notifyConnectCompleted(ConnectionParams paramConnectionParams, FailCause paramFailCause)
    {
        Message localMessage = paramConnectionParams.onCompletedMsg;
        if (localMessage == null)
            return;
        long l = System.currentTimeMillis();
        localMessage.arg1 = this.cid;
        if (paramFailCause == FailCause.NONE)
        {
            this.createTime = l;
            AsyncResult.forMessage(localMessage);
        }
        while (true)
        {
            log("notifyConnectionCompleted at " + l + " cause=" + paramFailCause);
            localMessage.sendToTarget();
            break;
            this.lastFailCause = paramFailCause;
            this.lastFailTime = l;
            AsyncResult.forMessage(localMessage, paramFailCause, new CallSetupException(this.mRetryOverride));
        }
    }

    private void notifyDisconnectCompleted(DisconnectParams paramDisconnectParams, boolean paramBoolean)
    {
        ApnContext localApnContext1 = null;
        String str = null;
        if (paramDisconnectParams.onCompletedMsg != null)
        {
            Message localMessage2 = paramDisconnectParams.onCompletedMsg;
            if ((localMessage2.obj instanceof ApnContext))
                localApnContext1 = (ApnContext)localMessage2.obj;
            str = paramDisconnectParams.reason;
            AsyncResult.forMessage(localMessage2);
            localMessage2.sendToTarget();
        }
        if (paramBoolean)
        {
            Iterator localIterator = this.mApnList.iterator();
            while (localIterator.hasNext())
            {
                ApnContext localApnContext2 = (ApnContext)localIterator.next();
                if (localApnContext2 != localApnContext1)
                {
                    if (str != null)
                        localApnContext2.setReason(str);
                    Message localMessage1 = this.mDataConnectionTracker.obtainMessage(270351, localApnContext2);
                    AsyncResult.forMessage(localMessage1);
                    localMessage1.sendToTarget();
                }
            }
        }
        log("NotifyDisconnectCompleted DisconnectParams=" + paramDisconnectParams);
    }

    private DataCallState.SetupResult onSetupConnectionCompleted(AsyncResult paramAsyncResult)
    {
        DataCallState localDataCallState = (DataCallState)paramAsyncResult.result;
        ConnectionParams localConnectionParams = (ConnectionParams)paramAsyncResult.userObj;
        DataCallState.SetupResult localSetupResult;
        if (paramAsyncResult.exception != null)
        {
            log("onSetupConnectionCompleted failed, ar.exception=" + paramAsyncResult.exception + " response=" + localDataCallState);
            if (((paramAsyncResult.exception instanceof CommandException)) && (((CommandException)paramAsyncResult.exception).getCommandError() == CommandException.Error.RADIO_NOT_AVAILABLE))
            {
                localSetupResult = DataCallState.SetupResult.ERR_BadCommand;
                localSetupResult.mFailCause = FailCause.RADIO_NOT_AVAILABLE;
            }
        }
        while (true)
        {
            return localSetupResult;
            if ((localDataCallState == null) || (localDataCallState.version < 4))
            {
                localSetupResult = DataCallState.SetupResult.ERR_GetLastErrorFromRil;
            }
            else
            {
                localSetupResult = DataCallState.SetupResult.ERR_RilError;
                localSetupResult.mFailCause = FailCause.fromInt(localDataCallState.status);
                continue;
                if (localConnectionParams.tag != this.mTag)
                {
                    log("BUG: onSetupConnectionCompleted is stale cp.tag=" + localConnectionParams.tag + ", mtag=" + this.mTag);
                    localSetupResult = DataCallState.SetupResult.ERR_Stale;
                }
                else if (localDataCallState.status != 0)
                {
                    localSetupResult = DataCallState.SetupResult.ERR_RilError;
                    localSetupResult.mFailCause = FailCause.fromInt(localDataCallState.status);
                }
                else
                {
                    log("onSetupConnectionCompleted received DataCallState: " + localDataCallState);
                    this.cid = localDataCallState.cid;
                    localSetupResult = updateLinkProperty(localDataCallState).setupResult;
                }
            }
        }
    }

    private DataCallState.SetupResult setLinkProperties(DataCallState paramDataCallState, LinkProperties paramLinkProperties)
    {
        String str = "net." + paramDataCallState.ifname + ".";
        String[] arrayOfString = new String[2];
        arrayOfString[0] = SystemProperties.get(str + "dns1");
        arrayOfString[1] = SystemProperties.get(str + "dns2");
        return paramDataCallState.setLinkProperties(paramLinkProperties, isDnsOk(arrayOfString));
    }

    private void tearDownData(Object paramObject)
    {
        int i = 0;
        DisconnectParams localDisconnectParams;
        if ((paramObject != null) && ((paramObject instanceof DisconnectParams)))
        {
            localDisconnectParams = (DisconnectParams)paramObject;
            if (!TextUtils.equals(localDisconnectParams.reason, "radioTurnedOff"))
                break label88;
            i = 1;
        }
        label134: 
        while (true)
        {
            if (this.phone.mCM.getRadioState().isOn())
            {
                log("tearDownData radio is on, call deactivateDataCall");
                this.phone.mCM.deactivateDataCall(this.cid, i, obtainMessage(262147, paramObject));
            }
            while (true)
            {
                return;
                label88: if (!TextUtils.equals(localDisconnectParams.reason, "pdpReset"))
                    break label134;
                i = 2;
                break;
                log("tearDownData radio is off sendMessage EVENT_DEACTIVATE_DONE immediately");
                sendMessage(obtainMessage(262147, new AsyncResult(paramObject, null, null)));
            }
        }
    }

    private UpdateLinkPropertyResult updateLinkProperty(DataCallState paramDataCallState)
    {
        UpdateLinkPropertyResult localUpdateLinkPropertyResult = new UpdateLinkPropertyResult(this.mLinkProperties);
        if (paramDataCallState == null);
        while (true)
        {
            return localUpdateLinkPropertyResult;
            localUpdateLinkPropertyResult.newLp = new LinkProperties();
            localUpdateLinkPropertyResult.setupResult = setLinkProperties(paramDataCallState, localUpdateLinkPropertyResult.newLp);
            if (localUpdateLinkPropertyResult.setupResult != DataCallState.SetupResult.SUCCESS)
            {
                log("updateLinkProperty failed : " + localUpdateLinkPropertyResult.setupResult);
            }
            else
            {
                localUpdateLinkPropertyResult.newLp.setHttpProxy(this.mLinkProperties.getHttpProxy());
                if (!localUpdateLinkPropertyResult.oldLp.equals(localUpdateLinkPropertyResult.newLp))
                {
                    log("updateLinkProperty old LP=" + localUpdateLinkPropertyResult.oldLp);
                    log("updateLinkProperty new LP=" + localUpdateLinkPropertyResult.newLp);
                }
                this.mLinkProperties = localUpdateLinkPropertyResult.newLp;
            }
        }
    }

    public void bringUp(Message paramMessage, ApnSetting paramApnSetting)
    {
        sendMessage(obtainMessage(262144, new ConnectionParams(paramApnSetting, paramMessage)));
    }

    protected void clearSettings()
    {
        log("clearSettings");
        this.createTime = -1L;
        this.lastFailTime = -1L;
        this.lastFailCause = FailCause.NONE;
        this.mRetryOverride = -1;
        this.mRefCount = 0;
        this.cid = -1;
        this.mLinkProperties = new LinkProperties();
        this.mApn = null;
    }

    public boolean configureRetry(int paramInt1, int paramInt2, int paramInt3)
    {
        return this.mRetryMgr.configure(paramInt1, paramInt2, paramInt3);
    }

    public boolean configureRetry(String paramString)
    {
        return this.mRetryMgr.configure(paramString);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.print("DataConnection ");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" mApnList=" + this.mApnList);
        paramPrintWriter.flush();
        paramPrintWriter.println(" mDataConnectionTracker=" + this.mDataConnectionTracker);
        paramPrintWriter.println(" mApn=" + this.mApn);
        paramPrintWriter.println(" mTag=" + this.mTag);
        paramPrintWriter.flush();
        paramPrintWriter.println(" phone=" + this.phone);
        paramPrintWriter.println(" mRilVersion=" + this.mRilVersion);
        paramPrintWriter.println(" cid=" + this.cid);
        paramPrintWriter.flush();
        paramPrintWriter.println(" mLinkProperties=" + this.mLinkProperties);
        paramPrintWriter.flush();
        paramPrintWriter.println(" mCapabilities=" + this.mCapabilities);
        paramPrintWriter.println(" createTime=" + TimeUtils.logTimeOfDay(this.createTime));
        paramPrintWriter.println(" lastFailTime=" + TimeUtils.logTimeOfDay(this.lastFailTime));
        paramPrintWriter.println(" lastFailCause=" + this.lastFailCause);
        paramPrintWriter.flush();
        paramPrintWriter.println(" mRetryOverride=" + this.mRetryOverride);
        paramPrintWriter.println(" mRefCount=" + this.mRefCount);
        paramPrintWriter.println(" userData=" + this.userData);
        if (this.mRetryMgr != null)
            paramPrintWriter.println(" " + this.mRetryMgr);
        paramPrintWriter.flush();
    }

    public int getDataConnectionId()
    {
        return this.mId;
    }

    protected String getMessageInfo(Message paramMessage)
    {
        String str = cmdToString(paramMessage.what);
        if (str == null)
            str = DataConnectionAc.cmdToString(paramMessage.what);
        return str;
    }

    public int getRetryCount()
    {
        return this.mRetryMgr.getRetryCount();
    }

    public int getRetryTimer()
    {
        return this.mRetryMgr.getRetryTimer();
    }

    protected int getRilRadioTechnology(int paramInt)
    {
        if (this.mRilVersion < 6);
        for (int i = paramInt; ; i = 2 + this.phone.getServiceState().getRilRadioTechnology())
            return i;
    }

    public void increaseRetryCount()
    {
        this.mRetryMgr.increaseRetryCount();
    }

    protected abstract boolean isDnsOk(String[] paramArrayOfString);

    public boolean isRetryForever()
    {
        return this.mRetryMgr.isRetryForever();
    }

    public boolean isRetryNeeded()
    {
        return this.mRetryMgr.isRetryNeeded();
    }

    protected abstract void log(String paramString);

    protected abstract void onConnect(ConnectionParams paramConnectionParams);

    public void resetRetryCount()
    {
        this.mRetryMgr.resetRetryCount();
    }

    public void retryForeverUsingLastTimeout()
    {
        this.mRetryMgr.retryForeverUsingLastTimeout();
    }

    public void setRetryCount(int paramInt)
    {
        log("setRetryCount: " + paramInt);
        this.mRetryMgr.setRetryCount(paramInt);
    }

    public void tearDown(String paramString, Message paramMessage)
    {
        sendMessage(obtainMessage(262148, new DisconnectParams(paramString, paramMessage)));
    }

    public void tearDownAll(String paramString, Message paramMessage)
    {
        sendMessage(obtainMessage(262150, new DisconnectParams(paramString, paramMessage)));
    }

    public abstract String toString();

    private class DcDisconnectionErrorCreatingConnection extends State
    {
        private DcDisconnectionErrorCreatingConnection()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 262147:
            }
            DataConnection.ConnectionParams localConnectionParams = (DataConnection.ConnectionParams)((AsyncResult)paramMessage.obj).userObj;
            if (localConnectionParams.tag == DataConnection.this.mTag)
            {
                DataConnection.this.log("DcDisconnectionErrorCreatingConnection msg.what=EVENT_DEACTIVATE_DONE");
                DataConnection.this.mInactiveState.setEnterNotificationParams(localConnectionParams, DataConnection.FailCause.UNACCEPTABLE_NETWORK_PARAMETER, -1);
                DataConnection.this.transitionTo(DataConnection.this.mInactiveState);
            }
            while (true)
            {
                bool = true;
                break;
                DataConnection.this.log("DcDisconnectionErrorCreatingConnection EVENT_DEACTIVATE_DONE stale dp.tag=" + localConnectionParams.tag + ", mTag=" + DataConnection.this.mTag);
            }
        }
    }

    private class DcDisconnectingState extends State
    {
        private DcDisconnectingState()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            case 262145:
            case 262146:
            default:
            case 262144:
                for (bool = false; ; bool = true)
                {
                    return bool;
                    DataConnection.this.log("DcDisconnectingState msg.what=EVENT_CONNECT. Defer. RefCount = " + DataConnection.this.mRefCount);
                    DataConnection.this.deferMessage(paramMessage);
                }
            case 262147:
            }
            DataConnection.this.log("DcDisconnectingState msg.what=EVENT_DEACTIVATE_DONE");
            AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
            DataConnection.DisconnectParams localDisconnectParams = (DataConnection.DisconnectParams)localAsyncResult.userObj;
            if (localDisconnectParams.tag == DataConnection.this.mTag)
            {
                DataConnection.this.mInactiveState.setEnterNotificationParams((DataConnection.DisconnectParams)localAsyncResult.userObj);
                DataConnection.this.transitionTo(DataConnection.this.mInactiveState);
            }
            while (true)
            {
                bool = true;
                break;
                DataConnection.this.log("DcDisconnectState EVENT_DEACTIVATE_DONE stale dp.tag=" + localDisconnectParams.tag + " mTag=" + DataConnection.this.mTag);
            }
        }
    }

    private class DcActiveState extends State
    {
        private DataConnection.ConnectionParams mConnectionParams = null;
        private DataConnection.FailCause mFailCause = null;

        private DcActiveState()
        {
        }

        public void enter()
        {
            if ((this.mConnectionParams != null) && (this.mFailCause != null))
                DataConnection.this.notifyConnectCompleted(this.mConnectionParams, this.mFailCause);
        }

        public void exit()
        {
            this.mConnectionParams = null;
            this.mFailCause = null;
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 262144:
            case 262148:
            case 262150:
            }
            while (true)
            {
                return bool;
                DataConnection localDataConnection2 = DataConnection.this;
                localDataConnection2.mRefCount = (1 + localDataConnection2.mRefCount);
                DataConnection.this.log("DcActiveState msg.what=EVENT_CONNECT RefCount=" + DataConnection.this.mRefCount);
                if (paramMessage.obj != null)
                    DataConnection.this.notifyConnectCompleted((DataConnection.ConnectionParams)paramMessage.obj, DataConnection.FailCause.NONE);
                bool = true;
                continue;
                DataConnection localDataConnection1 = DataConnection.this;
                localDataConnection1.mRefCount = (-1 + localDataConnection1.mRefCount);
                DataConnection.this.log("DcActiveState msg.what=EVENT_DISCONNECT RefCount=" + DataConnection.this.mRefCount);
                if (DataConnection.this.mRefCount == 0)
                {
                    DataConnection.DisconnectParams localDisconnectParams2 = (DataConnection.DisconnectParams)paramMessage.obj;
                    localDisconnectParams2.tag = DataConnection.this.mTag;
                    DataConnection.this.tearDownData(localDisconnectParams2);
                    DataConnection.this.transitionTo(DataConnection.this.mDisconnectingState);
                }
                while (true)
                {
                    bool = true;
                    break;
                    if (paramMessage.obj != null)
                        DataConnection.this.notifyDisconnectCompleted((DataConnection.DisconnectParams)paramMessage.obj, false);
                }
                DataConnection.this.log("DcActiveState msg.what=EVENT_DISCONNECT_ALL RefCount=" + DataConnection.this.mRefCount);
                DataConnection.this.mRefCount = 0;
                DataConnection.DisconnectParams localDisconnectParams1 = (DataConnection.DisconnectParams)paramMessage.obj;
                localDisconnectParams1.tag = DataConnection.this.mTag;
                DataConnection.this.tearDownData(localDisconnectParams1);
                DataConnection.this.transitionTo(DataConnection.this.mDisconnectingState);
                bool = true;
            }
        }

        public void setEnterNotificationParams(DataConnection.ConnectionParams paramConnectionParams, DataConnection.FailCause paramFailCause)
        {
            this.mConnectionParams = paramConnectionParams;
            this.mFailCause = paramFailCause;
        }
    }

    private class DcActivatingState extends State
    {
        private DcActivatingState()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
            case 262144:
                for (bool = false; ; bool = true)
                {
                    return bool;
                    DataConnection.this.log("DcActivatingState deferring msg.what=EVENT_CONNECT refCount = " + DataConnection.this.mRefCount);
                    DataConnection.this.deferMessage(paramMessage);
                }
            case 262145:
                DataConnection.this.log("DcActivatingState msg.what=EVENT_SETUP_DATA_CONNECTION_DONE");
                AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
                DataConnection.ConnectionParams localConnectionParams2 = (DataConnection.ConnectionParams)localAsyncResult2.userObj;
                DataCallState.SetupResult localSetupResult = DataConnection.this.onSetupConnectionCompleted(localAsyncResult2);
                DataConnection.this.log("DcActivatingState onSetupConnectionCompleted result=" + localSetupResult);
                switch (DataConnection.1.$SwitchMap$com$android$internal$telephony$DataCallState$SetupResult[localSetupResult.ordinal()])
                {
                default:
                    throw new RuntimeException("Unknown SetupResult, should not happen");
                case 1:
                    DataConnection.this.mActiveState.setEnterNotificationParams(localConnectionParams2, DataConnection.FailCause.NONE);
                    DataConnection.this.transitionTo(DataConnection.this.mActiveState);
                case 6:
                case 2:
                case 3:
                case 4:
                case 5:
                }
                while (true)
                {
                    bool = true;
                    break;
                    DataConnection.this.mInactiveState.setEnterNotificationParams(localConnectionParams2, localSetupResult.mFailCause, -1);
                    DataConnection.this.transitionTo(DataConnection.this.mInactiveState);
                    continue;
                    DataConnection.this.tearDownData(localConnectionParams2);
                    DataConnection.this.transitionTo(DataConnection.this.mDisconnectingErrorCreatingConnection);
                    continue;
                    DataConnection.this.phone.mCM.getLastDataCallFailCause(DataConnection.this.obtainMessage(262146, localConnectionParams2));
                    continue;
                    DataConnection.this.mInactiveState.setEnterNotificationParams(localConnectionParams2, localSetupResult.mFailCause, DataConnection.this.getSuggestedRetryTime(localAsyncResult2));
                    DataConnection.this.transitionTo(DataConnection.this.mInactiveState);
                }
            case 262146:
            }
            AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
            DataConnection.ConnectionParams localConnectionParams1 = (DataConnection.ConnectionParams)localAsyncResult1.userObj;
            DataConnection.FailCause localFailCause = DataConnection.FailCause.UNKNOWN;
            if (localConnectionParams1.tag == DataConnection.this.mTag)
            {
                DataConnection.this.log("DcActivatingState msg.what=EVENT_GET_LAST_FAIL_DONE");
                if (localAsyncResult1.exception == null)
                    localFailCause = DataConnection.FailCause.fromInt(((int[])(int[])localAsyncResult1.result)[0]);
                DataConnection.this.mInactiveState.setEnterNotificationParams(localConnectionParams1, localFailCause, -1);
                DataConnection.this.transitionTo(DataConnection.this.mInactiveState);
            }
            while (true)
            {
                bool = true;
                break;
                DataConnection.this.log("DcActivatingState EVENT_GET_LAST_FAIL_DONE is stale cp.tag=" + localConnectionParams1.tag + ", mTag=" + DataConnection.this.mTag);
            }
        }
    }

    private class DcInactiveState extends State
    {
        private DataConnection.ConnectionParams mConnectionParams = null;
        private DataConnection.DisconnectParams mDisconnectParams = null;
        private DataConnection.FailCause mFailCause = null;

        private DcInactiveState()
        {
        }

        public void enter()
        {
            DataConnection localDataConnection = DataConnection.this;
            localDataConnection.mTag = (1 + localDataConnection.mTag);
            if ((this.mConnectionParams != null) && (this.mFailCause != null))
                DataConnection.this.notifyConnectCompleted(this.mConnectionParams, this.mFailCause);
            if (this.mDisconnectParams != null)
                DataConnection.this.notifyDisconnectCompleted(this.mDisconnectParams, true);
            DataConnection.this.clearSettings();
        }

        public void exit()
        {
            this.mConnectionParams = null;
            this.mFailCause = null;
            this.mDisconnectParams = null;
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 266254:
            case 262144:
            case 262148:
            case 262150:
            }
            while (true)
            {
                return bool;
                DataConnection.this.log("DcInactiveState: msg.what=RSP_RESET, ignore we're already reset");
                DataConnection.this.mAc.replyToMessage(paramMessage, 266255);
                bool = true;
                continue;
                DataConnection.ConnectionParams localConnectionParams = (DataConnection.ConnectionParams)paramMessage.obj;
                localConnectionParams.tag = DataConnection.this.mTag;
                DataConnection.this.log("DcInactiveState msg.what=EVENT_CONNECT.RefCount = " + DataConnection.this.mRefCount);
                DataConnection.this.mRefCount = 1;
                DataConnection.this.onConnect(localConnectionParams);
                DataConnection.this.transitionTo(DataConnection.this.mActivatingState);
                bool = true;
                continue;
                DataConnection.this.log("DcInactiveState: msg.what=EVENT_DISCONNECT");
                DataConnection.this.notifyDisconnectCompleted((DataConnection.DisconnectParams)paramMessage.obj, false);
                bool = true;
                continue;
                DataConnection.this.log("DcInactiveState: msg.what=EVENT_DISCONNECT_ALL");
                DataConnection.this.notifyDisconnectCompleted((DataConnection.DisconnectParams)paramMessage.obj, false);
                bool = true;
            }
        }

        public void setEnterNotificationParams(DataConnection.ConnectionParams paramConnectionParams, DataConnection.FailCause paramFailCause, int paramInt)
        {
            this.mConnectionParams = paramConnectionParams;
            this.mFailCause = paramFailCause;
            DataConnection.this.mRetryOverride = paramInt;
        }

        public void setEnterNotificationParams(DataConnection.DisconnectParams paramDisconnectParams)
        {
            this.mDisconnectParams = paramDisconnectParams;
        }
    }

    private class DcDefaultState extends State
    {
        private DcDefaultState()
        {
        }

        public void enter()
        {
            DataConnection.this.phone.mCM.registerForRilConnected(DataConnection.this.getHandler(), 262149, null);
        }

        public void exit()
        {
            DataConnection.this.phone.mCM.unregisterForRilConnected(DataConnection.this.getHandler());
        }

        public boolean processMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                DataConnection.this.log("DcDefaultState: shouldn't happen but ignore msg.what=0x" + Integer.toHexString(paramMessage.what));
            case 69633:
            case 69635:
            case 69636:
            case 266240:
            case 266242:
            case 266244:
            case 266246:
            case 266248:
            case 266252:
            case 266250:
            case 266254:
            case 266256:
            case 266258:
            case 266260:
            case 266262:
            case 266264:
            case 266266:
            case 262144:
            case 262148:
            case 262150:
            case 262149:
            }
            while (true)
            {
                return true;
                if (DataConnection.this.mAc != null)
                {
                    DataConnection.this.mAc.replyToMessage(paramMessage, 69634, 3);
                }
                else
                {
                    DataConnection.this.mAc = new AsyncChannel();
                    DataConnection.this.mAc.connected(null, DataConnection.this.getHandler(), paramMessage.replyTo);
                    DataConnection.this.mAc.replyToMessage(paramMessage, 69634, 0, DataConnection.this.mId, "hi");
                    continue;
                    DataConnection.this.mAc.disconnect();
                    continue;
                    DataConnection.this.mAc = null;
                    continue;
                    int i;
                    label351: AsyncChannel localAsyncChannel;
                    if (DataConnection.this.getCurrentState() == DataConnection.this.mInactiveState)
                    {
                        i = 1;
                        localAsyncChannel = DataConnection.this.mAc;
                        if (i == 0)
                            break label387;
                    }
                    label387: for (int j = 1; ; j = 0)
                    {
                        localAsyncChannel.replyToMessage(paramMessage, 266241, j);
                        break;
                        i = 0;
                        break label351;
                    }
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266243, DataConnection.this.cid);
                    continue;
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266245, DataConnection.this.mApn);
                    continue;
                    LinkProperties localLinkProperties = new LinkProperties(DataConnection.this.mLinkProperties);
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266247, localLinkProperties);
                    continue;
                    ProxyProperties localProxyProperties = (ProxyProperties)paramMessage.obj;
                    DataConnection.this.mLinkProperties.setHttpProxy(localProxyProperties);
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266249);
                    continue;
                    DataCallState localDataCallState = (DataCallState)paramMessage.obj;
                    DataConnection.UpdateLinkPropertyResult localUpdateLinkPropertyResult = DataConnection.this.updateLinkProperty(localDataCallState);
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266253, localUpdateLinkPropertyResult);
                    continue;
                    LinkCapabilities localLinkCapabilities = new LinkCapabilities(DataConnection.this.mCapabilities);
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266251, localLinkCapabilities);
                    continue;
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266255);
                    DataConnection.this.transitionTo(DataConnection.this.mInactiveState);
                    continue;
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266257, DataConnection.this.mRefCount);
                    continue;
                    ApnContext localApnContext2 = (ApnContext)paramMessage.obj;
                    if (!DataConnection.this.mApnList.contains(localApnContext2))
                        DataConnection.this.mApnList.add(localApnContext2);
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266259);
                    continue;
                    ApnContext localApnContext1 = (ApnContext)paramMessage.obj;
                    DataConnection.this.mApnList.remove(localApnContext1);
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266261);
                    continue;
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266263, new ArrayList(DataConnection.this.mApnList));
                    continue;
                    PendingIntent localPendingIntent = (PendingIntent)paramMessage.obj;
                    DataConnection.this.mReconnectIntent = localPendingIntent;
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266265);
                    continue;
                    DataConnection.this.mAc.replyToMessage(paramMessage, 266267, DataConnection.this.mReconnectIntent);
                    continue;
                    DataConnection.this.log("DcDefaultState: msg.what=EVENT_CONNECT, fail not expected");
                    DataConnection.ConnectionParams localConnectionParams = (DataConnection.ConnectionParams)paramMessage.obj;
                    DataConnection.this.notifyConnectCompleted(localConnectionParams, DataConnection.FailCause.UNKNOWN);
                    continue;
                    DataConnection.this.log("DcDefaultState deferring msg.what=EVENT_DISCONNECT" + DataConnection.this.mRefCount);
                    DataConnection.this.deferMessage(paramMessage);
                    continue;
                    DataConnection.this.log("DcDefaultState deferring msg.what=EVENT_DISCONNECT_ALL" + DataConnection.this.mRefCount);
                    DataConnection.this.deferMessage(paramMessage);
                    continue;
                    AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
                    if (localAsyncResult.exception == null)
                    {
                        DataConnection.this.mRilVersion = ((Integer)localAsyncResult.result).intValue();
                        DataConnection.this.log("DcDefaultState: msg.what=EVENT_RIL_CONNECTED mRilVersion=" + DataConnection.this.mRilVersion);
                    }
                    else
                    {
                        DataConnection.this.log("Unexpected exception on EVENT_RIL_CONNECTED");
                        DataConnection.this.mRilVersion = -1;
                    }
                }
            }
        }
    }

    public static class UpdateLinkPropertyResult
    {
        public LinkProperties newLp;
        public LinkProperties oldLp;
        public DataCallState.SetupResult setupResult = DataCallState.SetupResult.SUCCESS;

        public UpdateLinkPropertyResult(LinkProperties paramLinkProperties)
        {
            this.oldLp = paramLinkProperties;
            this.newLp = paramLinkProperties;
        }
    }

    public static class CallSetupException extends Exception
    {
        private int mRetryOverride = -1;

        CallSetupException(int paramInt)
        {
            this.mRetryOverride = paramInt;
        }

        public int getRetryOverride()
        {
            return this.mRetryOverride;
        }
    }

    public static enum FailCause
    {
        private static final HashMap<Integer, FailCause> sErrorCodeToFailCauseMap;
        private final int mErrorCode;

        static
        {
            INSUFFICIENT_RESOURCES = new FailCause("INSUFFICIENT_RESOURCES", 2, 26);
            MISSING_UNKNOWN_APN = new FailCause("MISSING_UNKNOWN_APN", 3, 27);
            UNKNOWN_PDP_ADDRESS_TYPE = new FailCause("UNKNOWN_PDP_ADDRESS_TYPE", 4, 28);
            USER_AUTHENTICATION = new FailCause("USER_AUTHENTICATION", 5, 29);
            ACTIVATION_REJECT_GGSN = new FailCause("ACTIVATION_REJECT_GGSN", 6, 30);
            ACTIVATION_REJECT_UNSPECIFIED = new FailCause("ACTIVATION_REJECT_UNSPECIFIED", 7, 31);
            SERVICE_OPTION_NOT_SUPPORTED = new FailCause("SERVICE_OPTION_NOT_SUPPORTED", 8, 32);
            SERVICE_OPTION_NOT_SUBSCRIBED = new FailCause("SERVICE_OPTION_NOT_SUBSCRIBED", 9, 33);
            SERVICE_OPTION_OUT_OF_ORDER = new FailCause("SERVICE_OPTION_OUT_OF_ORDER", 10, 34);
            NSAPI_IN_USE = new FailCause("NSAPI_IN_USE", 11, 35);
            ONLY_IPV4_ALLOWED = new FailCause("ONLY_IPV4_ALLOWED", 12, 50);
            ONLY_IPV6_ALLOWED = new FailCause("ONLY_IPV6_ALLOWED", 13, 51);
            ONLY_SINGLE_BEARER_ALLOWED = new FailCause("ONLY_SINGLE_BEARER_ALLOWED", 14, 52);
            PROTOCOL_ERRORS = new FailCause("PROTOCOL_ERRORS", 15, 111);
            REGISTRATION_FAIL = new FailCause("REGISTRATION_FAIL", 16, -1);
            GPRS_REGISTRATION_FAIL = new FailCause("GPRS_REGISTRATION_FAIL", 17, -2);
            SIGNAL_LOST = new FailCause("SIGNAL_LOST", 18, -3);
            PREF_RADIO_TECH_CHANGED = new FailCause("PREF_RADIO_TECH_CHANGED", 19, -4);
            RADIO_POWER_OFF = new FailCause("RADIO_POWER_OFF", 20, -5);
            TETHERED_CALL_ACTIVE = new FailCause("TETHERED_CALL_ACTIVE", 21, -6);
            ERROR_UNSPECIFIED = new FailCause("ERROR_UNSPECIFIED", 22, 65535);
            UNKNOWN = new FailCause("UNKNOWN", 23, 65536);
            RADIO_NOT_AVAILABLE = new FailCause("RADIO_NOT_AVAILABLE", 24, 65537);
            UNACCEPTABLE_NETWORK_PARAMETER = new FailCause("UNACCEPTABLE_NETWORK_PARAMETER", 25, 65538);
            CONNECTION_TO_DATACONNECTIONAC_BROKEN = new FailCause("CONNECTION_TO_DATACONNECTIONAC_BROKEN", 26, 65539);
            FailCause[] arrayOfFailCause1 = new FailCause[27];
            arrayOfFailCause1[0] = NONE;
            arrayOfFailCause1[1] = OPERATOR_BARRED;
            arrayOfFailCause1[2] = INSUFFICIENT_RESOURCES;
            arrayOfFailCause1[3] = MISSING_UNKNOWN_APN;
            arrayOfFailCause1[4] = UNKNOWN_PDP_ADDRESS_TYPE;
            arrayOfFailCause1[5] = USER_AUTHENTICATION;
            arrayOfFailCause1[6] = ACTIVATION_REJECT_GGSN;
            arrayOfFailCause1[7] = ACTIVATION_REJECT_UNSPECIFIED;
            arrayOfFailCause1[8] = SERVICE_OPTION_NOT_SUPPORTED;
            arrayOfFailCause1[9] = SERVICE_OPTION_NOT_SUBSCRIBED;
            arrayOfFailCause1[10] = SERVICE_OPTION_OUT_OF_ORDER;
            arrayOfFailCause1[11] = NSAPI_IN_USE;
            arrayOfFailCause1[12] = ONLY_IPV4_ALLOWED;
            arrayOfFailCause1[13] = ONLY_IPV6_ALLOWED;
            arrayOfFailCause1[14] = ONLY_SINGLE_BEARER_ALLOWED;
            arrayOfFailCause1[15] = PROTOCOL_ERRORS;
            arrayOfFailCause1[16] = REGISTRATION_FAIL;
            arrayOfFailCause1[17] = GPRS_REGISTRATION_FAIL;
            arrayOfFailCause1[18] = SIGNAL_LOST;
            arrayOfFailCause1[19] = PREF_RADIO_TECH_CHANGED;
            arrayOfFailCause1[20] = RADIO_POWER_OFF;
            arrayOfFailCause1[21] = TETHERED_CALL_ACTIVE;
            arrayOfFailCause1[22] = ERROR_UNSPECIFIED;
            arrayOfFailCause1[23] = UNKNOWN;
            arrayOfFailCause1[24] = RADIO_NOT_AVAILABLE;
            arrayOfFailCause1[25] = UNACCEPTABLE_NETWORK_PARAMETER;
            arrayOfFailCause1[26] = CONNECTION_TO_DATACONNECTIONAC_BROKEN;
            $VALUES = arrayOfFailCause1;
            sErrorCodeToFailCauseMap = new HashMap();
            for (FailCause localFailCause : values())
                sErrorCodeToFailCauseMap.put(Integer.valueOf(localFailCause.getErrorCode()), localFailCause);
        }

        private FailCause(int paramInt)
        {
            this.mErrorCode = paramInt;
        }

        public static FailCause fromInt(int paramInt)
        {
            FailCause localFailCause = (FailCause)sErrorCodeToFailCauseMap.get(Integer.valueOf(paramInt));
            if (localFailCause == null)
                localFailCause = UNKNOWN;
            return localFailCause;
        }

        int getErrorCode()
        {
            return this.mErrorCode;
        }

        public boolean isEventLoggable()
        {
            if ((this == OPERATOR_BARRED) || (this == INSUFFICIENT_RESOURCES) || (this == UNKNOWN_PDP_ADDRESS_TYPE) || (this == USER_AUTHENTICATION) || (this == ACTIVATION_REJECT_GGSN) || (this == ACTIVATION_REJECT_UNSPECIFIED) || (this == SERVICE_OPTION_NOT_SUBSCRIBED) || (this == SERVICE_OPTION_NOT_SUPPORTED) || (this == SERVICE_OPTION_OUT_OF_ORDER) || (this == NSAPI_IN_USE) || (this == PROTOCOL_ERRORS) || (this == UNACCEPTABLE_NETWORK_PARAMETER));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isPermanentFail()
        {
            if ((this == OPERATOR_BARRED) || (this == MISSING_UNKNOWN_APN) || (this == UNKNOWN_PDP_ADDRESS_TYPE) || (this == USER_AUTHENTICATION) || (this == SERVICE_OPTION_NOT_SUPPORTED) || (this == SERVICE_OPTION_NOT_SUBSCRIBED) || (this == NSAPI_IN_USE) || (this == PROTOCOL_ERRORS));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    protected static class DisconnectParams
    {
        public Message onCompletedMsg;
        public String reason;
        public int tag;

        public DisconnectParams(String paramString, Message paramMessage)
        {
            this.reason = paramString;
            this.onCompletedMsg = paramMessage;
        }
    }

    protected static class ConnectionParams
    {
        public ApnSetting apn;
        public Message onCompletedMsg;
        public int tag;

        public ConnectionParams(ApnSetting paramApnSetting, Message paramMessage)
        {
            this.apn = paramApnSetting;
            this.onCompletedMsg = paramMessage;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.DataConnection
 * JD-Core Version:        0.6.2
 */