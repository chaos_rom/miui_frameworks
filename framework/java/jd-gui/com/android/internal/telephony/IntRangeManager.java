package com.android.internal.telephony;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class IntRangeManager
{
    private static final int INITIAL_CLIENTS_ARRAY_SIZE = 4;
    private ArrayList<IntRange> mRanges = new ArrayList();

    private boolean tryAddSingleRange(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        startUpdate();
        addRange(paramInt1, paramInt2, paramBoolean);
        return finishUpdate();
    }

    protected abstract void addRange(int paramInt1, int paramInt2, boolean paramBoolean);

    /** @deprecated */
    public boolean disableRange(int paramInt1, int paramInt2, String paramString)
    {
        while (true)
        {
            int j;
            boolean bool;
            int i2;
            ClientRange localClientRange1;
            int n;
            int i1;
            ArrayList localArrayList2;
            IntRange localIntRange3;
            int i3;
            try
            {
                int i = this.mRanges.size();
                j = 0;
                if (j >= i)
                    break label622;
                IntRange localIntRange1 = (IntRange)this.mRanges.get(j);
                int k = localIntRange1.startId;
                if (paramInt1 < k)
                {
                    bool = false;
                    return bool;
                }
                if (paramInt2 > localIntRange1.endId)
                    break label616;
                ArrayList localArrayList1 = localIntRange1.clients;
                int m = localArrayList1.size();
                if (m != 1)
                    break label640;
                ClientRange localClientRange3 = (ClientRange)localArrayList1.get(0);
                if ((localClientRange3.startId != paramInt1) || (localClientRange3.endId != paramInt2) || (!localClientRange3.client.equals(paramString)))
                    break label634;
                if (!tryAddSingleRange(paramInt1, paramInt2, false))
                    break label628;
                this.mRanges.remove(j);
                bool = true;
                continue;
                if (i2 >= m)
                    break label616;
                localClientRange1 = (ClientRange)localArrayList1.get(i2);
                if ((localClientRange1.startId != paramInt1) || (localClientRange1.endId != paramInt2) || (!localClientRange1.client.equals(paramString)))
                    break label593;
                if (i2 == m - 1)
                {
                    if (localIntRange1.endId == n)
                    {
                        localArrayList1.remove(i2);
                        bool = true;
                        continue;
                    }
                    if (!tryAddSingleRange(n + 1, localIntRange1.endId, false))
                        break label653;
                    localArrayList1.remove(i2);
                    localIntRange1.endId = n;
                    bool = true;
                    continue;
                }
                IntRange localIntRange2 = new IntRange(localIntRange1, i2);
                if (i2 == 0)
                {
                    int i4 = ((ClientRange)localArrayList1.get(1)).startId;
                    if (i4 != localIntRange1.startId)
                    {
                        startUpdate();
                        i1 = 1;
                        addRange(localIntRange1.startId, i4 - 1, false);
                        localIntRange2.startId = i4;
                    }
                    n = ((ClientRange)localArrayList1.get(1)).endId;
                }
                localArrayList2 = new ArrayList();
                localIntRange3 = localIntRange2;
                i3 = i2 + 1;
                if (i3 < m)
                {
                    ClientRange localClientRange2 = (ClientRange)localArrayList1.get(i3);
                    if (localClientRange2.startId > n + 1)
                    {
                        if (i1 == 0)
                        {
                            startUpdate();
                            i1 = 1;
                        }
                        addRange(n + 1, -1 + localClientRange2.startId, false);
                        localIntRange3.endId = n;
                        localArrayList2.add(localIntRange3);
                        localIntRange3 = new IntRange(localClientRange2);
                        if (localClientRange2.endId <= n)
                            break label659;
                        n = localClientRange2.endId;
                        break label659;
                    }
                    localIntRange3.clients.add(localClientRange2);
                    continue;
                }
            }
            finally
            {
            }
            if (n < paramInt2)
            {
                if (i1 == 0)
                {
                    startUpdate();
                    i1 = 1;
                }
                addRange(n + 1, paramInt2, false);
                localIntRange3.endId = n;
            }
            localArrayList2.add(localIntRange3);
            if ((i1 != 0) && (!finishUpdate()))
            {
                bool = false;
            }
            else
            {
                this.mRanges.remove(j);
                this.mRanges.addAll(j, localArrayList2);
                bool = true;
                continue;
                label593: if (localClientRange1.endId > n)
                    n = localClientRange1.endId;
                i2++;
                continue;
                label616: j++;
                continue;
                label622: bool = false;
                continue;
                label628: bool = false;
                continue;
                label634: bool = false;
                continue;
                label640: n = -2147483648;
                i1 = 0;
                i2 = 0;
                continue;
                label653: bool = false;
                continue;
                label659: i3++;
            }
        }
    }

    /** @deprecated */
    public boolean enableRange(int paramInt1, int paramInt2, String paramString)
    {
        while (true)
        {
            int j;
            try
            {
                int i = this.mRanges.size();
                if (i == 0)
                {
                    if (tryAddSingleRange(paramInt1, paramInt2, true))
                    {
                        ArrayList localArrayList3 = this.mRanges;
                        IntRange localIntRange11 = new IntRange(paramInt1, paramInt2, paramString);
                        localArrayList3.add(localIntRange11);
                        bool = true;
                        return bool;
                    }
                    bool = false;
                    continue;
                }
                j = 0;
                if (j < i)
                {
                    IntRange localIntRange2 = (IntRange)this.mRanges.get(j);
                    if (paramInt1 < localIntRange2.startId)
                    {
                        if (paramInt2 + 1 < localIntRange2.startId)
                        {
                            if (!tryAddSingleRange(paramInt1, paramInt2, true))
                                break label900;
                            ArrayList localArrayList2 = this.mRanges;
                            IntRange localIntRange10 = new IntRange(paramInt1, paramInt2, paramString);
                            localArrayList2.add(j, localIntRange10);
                            bool = true;
                            continue;
                        }
                        if (paramInt2 > localIntRange2.endId)
                            break label912;
                        if (!tryAddSingleRange(paramInt1, -1 + localIntRange2.startId, true))
                            break label906;
                        localIntRange2.startId = paramInt1;
                        localIntRange2.clients.add(0, new ClientRange(paramInt1, paramInt2, paramString));
                        bool = true;
                        continue;
                        if (i3 < i)
                        {
                            IntRange localIntRange7 = (IntRange)this.mRanges.get(i3);
                            if (paramInt2 + 1 < localIntRange7.startId)
                            {
                                if (!tryAddSingleRange(paramInt1, paramInt2, true))
                                    break label927;
                                localIntRange2.startId = paramInt1;
                                localIntRange2.endId = paramInt2;
                                localIntRange2.clients.add(0, new ClientRange(paramInt1, paramInt2, paramString));
                                int i8 = j + 1;
                                int i9 = i8;
                                if (i9 >= i3)
                                    break label921;
                                IntRange localIntRange9 = (IntRange)this.mRanges.get(i8);
                                localIntRange2.clients.addAll(localIntRange9.clients);
                                this.mRanges.remove(localIntRange9);
                                i9++;
                                continue;
                            }
                            if (paramInt2 > localIntRange7.endId)
                                break label945;
                            if (!tryAddSingleRange(paramInt1, -1 + localIntRange7.startId, true))
                                break label939;
                            localIntRange2.startId = paramInt1;
                            localIntRange2.endId = localIntRange7.endId;
                            localIntRange2.clients.add(0, new ClientRange(paramInt1, paramInt2, paramString));
                            int i6 = j + 1;
                            int i7 = i6;
                            if (i7 > i3)
                                break label933;
                            IntRange localIntRange8 = (IntRange)this.mRanges.get(i6);
                            localIntRange2.clients.addAll(localIntRange8.clients);
                            this.mRanges.remove(localIntRange8);
                            i7++;
                            continue;
                        }
                        if (!tryAddSingleRange(paramInt1, paramInt2, true))
                            break label957;
                        localIntRange2.startId = paramInt1;
                        localIntRange2.endId = paramInt2;
                        localIntRange2.clients.add(0, new ClientRange(paramInt1, paramInt2, paramString));
                        int i4 = j + 1;
                        int i5 = i4;
                        if (i5 >= i)
                            break label951;
                        IntRange localIntRange6 = (IntRange)this.mRanges.get(i4);
                        localIntRange2.clients.addAll(localIntRange6.clients);
                        this.mRanges.remove(localIntRange6);
                        i5++;
                        continue;
                    }
                    if (paramInt1 + 1 > localIntRange2.endId)
                        break label1010;
                    if (paramInt2 > localIntRange2.endId)
                        break label963;
                    ClientRange localClientRange1 = new ClientRange(paramInt1, paramInt2, paramString);
                    localIntRange2.insert(localClientRange1);
                    bool = true;
                    continue;
                    if (m < i)
                    {
                        IntRange localIntRange5 = (IntRange)this.mRanges.get(m);
                        if (paramInt2 + 1 >= localIntRange5.startId)
                            break label976;
                    }
                    if (k == j)
                    {
                        if (!tryAddSingleRange(1 + localIntRange2.endId, paramInt2, true))
                            break label986;
                        localIntRange2.endId = paramInt2;
                        ClientRange localClientRange3 = new ClientRange(paramInt1, paramInt2, paramString);
                        localIntRange2.insert(localClientRange3);
                        bool = true;
                        continue;
                    }
                    IntRange localIntRange3 = (IntRange)this.mRanges.get(k);
                    if (paramInt2 > localIntRange3.endId)
                        break label992;
                    n = -1 + localIntRange3.startId;
                    if (!tryAddSingleRange(1 + localIntRange2.endId, n, true))
                        break label1004;
                    localIntRange2.endId = paramInt2;
                    ClientRange localClientRange2 = new ClientRange(paramInt1, paramInt2, paramString);
                    localIntRange2.insert(localClientRange2);
                    int i1 = j + 1;
                    int i2 = i1;
                    if (i2 >= k)
                        break label998;
                    IntRange localIntRange4 = (IntRange)this.mRanges.get(i1);
                    localIntRange2.clients.addAll(localIntRange4.clients);
                    this.mRanges.remove(localIntRange4);
                    i2++;
                    continue;
                }
                if (tryAddSingleRange(paramInt1, paramInt2, true))
                {
                    ArrayList localArrayList1 = this.mRanges;
                    IntRange localIntRange1 = new IntRange(paramInt1, paramInt2, paramString);
                    localArrayList1.add(localIntRange1);
                    bool = true;
                    continue;
                }
                bool = false;
                continue;
            }
            finally
            {
            }
            label900: boolean bool = false;
            continue;
            label906: bool = false;
            continue;
            label912: int i3 = j + 1;
            continue;
            label921: bool = true;
            continue;
            label927: bool = false;
            continue;
            label933: bool = true;
            continue;
            label939: bool = false;
            continue;
            label945: i3++;
            continue;
            label951: bool = true;
            continue;
            label957: bool = false;
            continue;
            label963: int k = j;
            int m = j + 1;
            continue;
            label976: k = m;
            m++;
            continue;
            label986: bool = false;
            continue;
            label992: int n = paramInt2;
            continue;
            label998: bool = true;
            continue;
            label1004: bool = false;
            continue;
            label1010: j++;
        }
    }

    protected abstract boolean finishUpdate();

    public boolean isEmpty()
    {
        return this.mRanges.isEmpty();
    }

    protected abstract void startUpdate();

    public boolean updateRanges()
    {
        startUpdate();
        Iterator localIterator = this.mRanges.iterator();
        if (localIterator.hasNext())
        {
            IntRange localIntRange1 = (IntRange)localIterator.next();
            int i = localIntRange1.startId;
            int j = localIntRange1.endId;
            while (localIterator.hasNext())
            {
                IntRange localIntRange2 = (IntRange)localIterator.next();
                if (localIntRange2.startId <= j + 1)
                {
                    if (localIntRange2.endId > j)
                        j = localIntRange2.endId;
                }
                else
                {
                    addRange(i, j, true);
                    i = localIntRange2.startId;
                    j = localIntRange2.endId;
                }
            }
            addRange(i, j, true);
        }
        return finishUpdate();
    }

    private class ClientRange
    {
        final String client;
        final int endId;
        final int startId;

        ClientRange(int paramInt1, int paramString, String arg4)
        {
            this.startId = paramInt1;
            this.endId = paramString;
            Object localObject;
            this.client = localObject;
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = false;
            if ((paramObject != null) && ((paramObject instanceof ClientRange)))
            {
                ClientRange localClientRange = (ClientRange)paramObject;
                if ((this.startId == localClientRange.startId) && (this.endId == localClientRange.endId) && (this.client.equals(localClientRange.client)))
                    bool = true;
            }
            return bool;
        }

        public int hashCode()
        {
            return 31 * (31 * this.startId + this.endId) + this.client.hashCode();
        }
    }

    private class IntRange
    {
        final ArrayList<IntRangeManager.ClientRange> clients;
        int endId;
        int startId;

        IntRange(int paramInt1, int paramString, String arg4)
        {
            this.startId = paramInt1;
            this.endId = paramString;
            this.clients = new ArrayList(4);
            String str;
            this.clients.add(new IntRangeManager.ClientRange(IntRangeManager.this, paramInt1, paramString, str));
        }

        IntRange(IntRangeManager.ClientRange arg2)
        {
            Object localObject;
            this.startId = localObject.startId;
            this.endId = localObject.endId;
            this.clients = new ArrayList(4);
            this.clients.add(localObject);
        }

        IntRange(IntRange paramInt, int arg3)
        {
            this.startId = paramInt.startId;
            this.endId = paramInt.endId;
            this.clients = new ArrayList(paramInt.clients.size());
            int i;
            for (int j = 0; j < i; j++)
                this.clients.add(paramInt.clients.get(j));
        }

        void insert(IntRangeManager.ClientRange paramClientRange)
        {
            int i = this.clients.size();
            int j = 0;
            if (j < i)
            {
                IntRangeManager.ClientRange localClientRange = (IntRangeManager.ClientRange)this.clients.get(j);
                if (paramClientRange.startId <= localClientRange.startId)
                    if (!paramClientRange.equals(localClientRange))
                        this.clients.add(j, paramClientRange);
            }
            while (true)
            {
                return;
                j++;
                break;
                this.clients.add(paramClientRange);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IntRangeManager
 * JD-Core Version:        0.6.2
 */