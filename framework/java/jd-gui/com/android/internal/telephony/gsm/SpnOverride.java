package com.android.internal.telephony.gsm;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import com.android.internal.util.XmlUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SpnOverride
{
    static final String LOG_TAG = "GSM";
    static final String PARTNER_SPN_OVERRIDE_PATH = "etc/spn-conf.xml";
    private HashMap<String, String> CarrierSpnMap = new HashMap();

    SpnOverride()
    {
        loadSpnOverrides();
    }

    private void loadSpnOverrides()
    {
        File localFile = new File(Environment.getRootDirectory(), "etc/spn-conf.xml");
        try
        {
            localFileReader = new FileReader(localFile);
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            try
            {
                FileReader localFileReader;
                XmlPullParser localXmlPullParser = Xml.newPullParser();
                localXmlPullParser.setInput(localFileReader);
                XmlUtils.beginDocument(localXmlPullParser, "spnOverrides");
                while (true)
                {
                    XmlUtils.nextElement(localXmlPullParser);
                    boolean bool = "spnOverride".equals(localXmlPullParser.getName());
                    if (!bool)
                        while (true)
                        {
                            return;
                            localFileNotFoundException = localFileNotFoundException;
                            Log.w("GSM", "Can't open " + Environment.getRootDirectory() + "/" + "etc/spn-conf.xml");
                        }
                    String str1 = localXmlPullParser.getAttributeValue(null, "numeric");
                    String str2 = localXmlPullParser.getAttributeValue(null, "spn");
                    this.CarrierSpnMap.put(str1, str2);
                }
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                while (true)
                    Log.w("GSM", "Exception in spn-conf parser " + localXmlPullParserException);
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.w("GSM", "Exception in spn-conf parser " + localIOException);
            }
        }
    }

    boolean containsCarrier(String paramString)
    {
        return this.CarrierSpnMap.containsKey(paramString);
    }

    String getSpn(String paramString)
    {
        return (String)this.CarrierSpnMap.get(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.SpnOverride
 * JD-Core Version:        0.6.2
 */