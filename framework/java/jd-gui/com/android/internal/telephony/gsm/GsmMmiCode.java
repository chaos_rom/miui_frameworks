package com.android.internal.telephony.gsm;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.CallForwardInfo;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandException.Error;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.MmiCode;
import com.android.internal.telephony.MmiCode.State;
import com.android.internal.telephony.PhoneBase;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class GsmMmiCode extends Handler
    implements MmiCode
{
    static final String ACTION_ACTIVATE = "*";
    static final String ACTION_DEACTIVATE = "#";
    static final String ACTION_ERASURE = "##";
    static final String ACTION_INTERROGATE = "*#";
    static final String ACTION_REGISTER = "**";
    static final char END_OF_USSD_COMMAND = '#';
    static final int EVENT_GET_CLIR_COMPLETE = 2;
    static final int EVENT_QUERY_CF_COMPLETE = 3;
    static final int EVENT_QUERY_COMPLETE = 5;
    static final int EVENT_SET_CFF_COMPLETE = 6;
    static final int EVENT_SET_COMPLETE = 1;
    static final int EVENT_USSD_CANCEL_COMPLETE = 7;
    static final int EVENT_USSD_COMPLETE = 4;
    static final String LOG_TAG = "GSM";
    static final int MATCH_GROUP_ACTION = 2;
    static final int MATCH_GROUP_DIALING_NUMBER = 12;
    static final int MATCH_GROUP_POUND_STRING = 1;
    static final int MATCH_GROUP_PWD_CONFIRM = 11;
    static final int MATCH_GROUP_SERVICE_CODE = 3;
    static final int MATCH_GROUP_SIA = 5;
    static final int MATCH_GROUP_SIB = 7;
    static final int MATCH_GROUP_SIC = 9;
    static final int MAX_LENGTH_SHORT_CODE = 2;
    static final String SC_BAIC = "35";
    static final String SC_BAICr = "351";
    static final String SC_BAOC = "33";
    static final String SC_BAOIC = "331";
    static final String SC_BAOICxH = "332";
    static final String SC_BA_ALL = "330";
    static final String SC_BA_MO = "333";
    static final String SC_BA_MT = "353";
    static final String SC_CFB = "67";
    static final String SC_CFNR = "62";
    static final String SC_CFNRy = "61";
    static final String SC_CFU = "21";
    static final String SC_CF_All = "002";
    static final String SC_CF_All_Conditional = "004";
    static final String SC_CLIP = "30";
    static final String SC_CLIR = "31";
    static final String SC_PIN = "04";
    static final String SC_PIN2 = "042";
    static final String SC_PUK = "05";
    static final String SC_PUK2 = "052";
    static final String SC_PWD = "03";
    static final String SC_WAIT = "43";
    static Pattern sPatternSuppService = Pattern.compile("((\\*|#|\\*#|\\*\\*|##)(\\d{2,3})(\\*([^*#]*)(\\*([^*#]*)(\\*([^*#]*)(\\*([^*#]*))?)?)?)?#)([^#]*)");
    private static String[] sTwoDigitNumberPattern;
    String action;
    Context context;
    String dialingNumber;
    private boolean isPendingUSSD;
    private boolean isUssdRequest;
    CharSequence message;
    GSMPhone phone;
    String poundString;
    String pwd;
    String sc;
    String sia;
    String sib;
    String sic;
    MmiCode.State state = MmiCode.State.PENDING;

    GsmMmiCode(GSMPhone paramGSMPhone)
    {
        super(paramGSMPhone.getHandler().getLooper());
        this.phone = paramGSMPhone;
        this.context = paramGSMPhone.getContext();
    }

    private CharSequence createQueryCallBarringResultMessage(int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder(this.context.getText(17039561));
        int i = 1;
        while (i <= 128)
        {
            if ((i & paramInt) != 0)
            {
                localStringBuilder.append("\n");
                localStringBuilder.append(serviceClassToCFString(i & paramInt));
            }
            i <<= 1;
        }
        return localStringBuilder;
    }

    private CharSequence createQueryCallWaitingResultMessage(int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder(this.context.getText(17039561));
        int i = 1;
        while (i <= 128)
        {
            if ((i & paramInt) != 0)
            {
                localStringBuilder.append("\n");
                localStringBuilder.append(serviceClassToCFString(i & paramInt));
            }
            i <<= 1;
        }
        return localStringBuilder;
    }

    private CharSequence getErrorMessage(AsyncResult paramAsyncResult)
    {
        if (((paramAsyncResult.exception instanceof CommandException)) && (((CommandException)paramAsyncResult.exception).getCommandError() == CommandException.Error.FDN_CHECK_FAILURE))
            Log.i("GSM", "FDN_CHECK_FAILURE");
        for (CharSequence localCharSequence = this.context.getText(17039559); ; localCharSequence = this.context.getText(17039558))
            return localCharSequence;
    }

    private CharSequence getScString()
    {
        Object localObject;
        if (this.sc != null)
            if (isServiceCodeCallBarring(this.sc))
                localObject = this.context.getText(17039580);
        while (true)
        {
            return localObject;
            if (isServiceCodeCallForwarding(this.sc))
                localObject = this.context.getText(17039578);
            else if (this.sc.equals("30"))
                localObject = this.context.getText(17039576);
            else if (this.sc.equals("31"))
                localObject = this.context.getText(17039577);
            else if (this.sc.equals("03"))
                localObject = this.context.getText(17039581);
            else if (this.sc.equals("43"))
                localObject = this.context.getText(17039579);
            else if (isPinCommand())
                localObject = this.context.getText(17039582);
            else
                localObject = "";
        }
    }

    private void handlePasswordError(int paramInt)
    {
        this.state = MmiCode.State.FAILED;
        StringBuilder localStringBuilder = new StringBuilder(getScString());
        localStringBuilder.append("\n");
        localStringBuilder.append(this.context.getText(paramInt));
        this.message = localStringBuilder;
        this.phone.onMMIDone(this);
    }

    private static boolean isEmptyOrNull(CharSequence paramCharSequence)
    {
        if ((paramCharSequence == null) || (paramCharSequence.length() == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    static boolean isServiceCodeCallBarring(String paramString)
    {
        if ((paramString != null) && ((paramString.equals("33")) || (paramString.equals("331")) || (paramString.equals("332")) || (paramString.equals("35")) || (paramString.equals("351")) || (paramString.equals("330")) || (paramString.equals("333")) || (paramString.equals("353"))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    static boolean isServiceCodeCallForwarding(String paramString)
    {
        if ((paramString != null) && ((paramString.equals("21")) || (paramString.equals("67")) || (paramString.equals("61")) || (paramString.equals("62")) || (paramString.equals("002")) || (paramString.equals("004"))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isShortCode(String paramString, GSMPhone paramGSMPhone)
    {
        boolean bool = false;
        if (paramString == null);
        while (true)
        {
            return bool;
            if ((paramString.length() != 0) && (!PhoneNumberUtils.isLocalEmergencyNumber(paramString, paramGSMPhone.getContext())))
                bool = isShortCodeUSSD(paramString, paramGSMPhone);
        }
    }

    private static boolean isShortCodeUSSD(String paramString, GSMPhone paramGSMPhone)
    {
        boolean bool = true;
        if (paramString != null)
            if ((!paramGSMPhone.isInCall()) || (paramString.length() > 2));
        while (true)
        {
            return bool;
            if ((paramString.length() > 2) || (paramString.charAt(-1 + paramString.length()) != '#'))
                bool = false;
        }
    }

    private static boolean isTwoDigitShortCode(Context paramContext, String paramString)
    {
        boolean bool = false;
        Log.d("GSM", "isTwoDigitShortCode");
        if ((paramString == null) || (paramString.length() != 2));
        while (true)
        {
            return bool;
            if (sTwoDigitNumberPattern == null)
                sTwoDigitNumberPattern = paramContext.getResources().getStringArray(17236011);
            String[] arrayOfString = sTwoDigitNumberPattern;
            int i = arrayOfString.length;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label127;
                String str = arrayOfString[j];
                Log.d("GSM", "Two Digit Number Pattern " + str);
                if (paramString.equals(str))
                {
                    Log.d("GSM", "Two Digit Number Pattern -true");
                    bool = true;
                    break;
                }
            }
            label127: Log.d("GSM", "Two Digit Number Pattern -false");
        }
    }

    private CharSequence makeCFQueryResultMessage(CallForwardInfo paramCallForwardInfo, int paramInt)
    {
        String[] arrayOfString = new String[3];
        arrayOfString[0] = "{0}";
        arrayOfString[1] = "{1}";
        arrayOfString[2] = "{2}";
        CharSequence[] arrayOfCharSequence = new CharSequence[3];
        int i;
        CharSequence localCharSequence;
        if (paramCallForwardInfo.reason == 2)
        {
            i = 1;
            if (paramCallForwardInfo.status != 1)
                break label176;
            if (i == 0)
                break label161;
            localCharSequence = this.context.getText(17039628);
            label65: arrayOfCharSequence[0] = serviceClassToCFString(paramInt & paramCallForwardInfo.serviceClass);
            arrayOfCharSequence[1] = PhoneNumberUtils.stringFromStringAndTOA(paramCallForwardInfo.number, paramCallForwardInfo.toa);
            arrayOfCharSequence[2] = Integer.toString(paramCallForwardInfo.timeSeconds);
            if ((paramCallForwardInfo.reason == 0) && ((paramInt & paramCallForwardInfo.serviceClass) == 1))
                if (paramCallForwardInfo.status != 1)
                    break label243;
        }
        label161: label176: label243: for (boolean bool = true; ; bool = false)
        {
            this.phone.mIccRecords.setVoiceCallForwardingFlag(1, bool);
            return TextUtils.replace(localCharSequence, arrayOfString, arrayOfCharSequence);
            i = 0;
            break;
            localCharSequence = this.context.getText(17039627);
            break label65;
            if ((paramCallForwardInfo.status == 0) && (isEmptyOrNull(paramCallForwardInfo.number)))
            {
                localCharSequence = this.context.getText(17039626);
                break label65;
            }
            if (i != 0)
            {
                localCharSequence = this.context.getText(17039630);
                break label65;
            }
            localCharSequence = this.context.getText(17039629);
            break label65;
        }
    }

    private static String makeEmptyNull(String paramString)
    {
        if ((paramString != null) && (paramString.length() == 0))
            paramString = null;
        return paramString;
    }

    static GsmMmiCode newFromDialString(String paramString, GSMPhone paramGSMPhone)
    {
        GsmMmiCode localGsmMmiCode = null;
        Matcher localMatcher = sPatternSuppService.matcher(paramString);
        if (localMatcher.matches())
        {
            localGsmMmiCode = new GsmMmiCode(paramGSMPhone);
            localGsmMmiCode.poundString = makeEmptyNull(localMatcher.group(1));
            localGsmMmiCode.action = makeEmptyNull(localMatcher.group(2));
            localGsmMmiCode.sc = makeEmptyNull(localMatcher.group(3));
            localGsmMmiCode.sia = makeEmptyNull(localMatcher.group(5));
            localGsmMmiCode.sib = makeEmptyNull(localMatcher.group(7));
            localGsmMmiCode.sic = makeEmptyNull(localMatcher.group(9));
            localGsmMmiCode.pwd = makeEmptyNull(localMatcher.group(11));
            localGsmMmiCode.dialingNumber = makeEmptyNull(localMatcher.group(12));
        }
        while (true)
        {
            return localGsmMmiCode;
            if (paramString.endsWith("#"))
            {
                localGsmMmiCode = new GsmMmiCode(paramGSMPhone);
                localGsmMmiCode.poundString = paramString;
            }
            else if (isTwoDigitShortCode(paramGSMPhone.getContext(), paramString))
            {
                localGsmMmiCode = null;
            }
            else if (isShortCode(paramString, paramGSMPhone))
            {
                localGsmMmiCode = new GsmMmiCode(paramGSMPhone);
                localGsmMmiCode.dialingNumber = paramString;
            }
        }
    }

    static GsmMmiCode newFromUssdUserInput(String paramString, GSMPhone paramGSMPhone)
    {
        GsmMmiCode localGsmMmiCode = new GsmMmiCode(paramGSMPhone);
        localGsmMmiCode.message = paramString;
        localGsmMmiCode.state = MmiCode.State.PENDING;
        localGsmMmiCode.isPendingUSSD = true;
        return localGsmMmiCode;
    }

    static GsmMmiCode newNetworkInitiatedUssd(String paramString, boolean paramBoolean, GSMPhone paramGSMPhone)
    {
        GsmMmiCode localGsmMmiCode = new GsmMmiCode(paramGSMPhone);
        localGsmMmiCode.message = paramString;
        localGsmMmiCode.isUssdRequest = paramBoolean;
        if (paramBoolean)
            localGsmMmiCode.isPendingUSSD = true;
        for (localGsmMmiCode.state = MmiCode.State.PENDING; ; localGsmMmiCode.state = MmiCode.State.COMPLETE)
            return localGsmMmiCode;
    }

    private void onGetClirComplete(AsyncResult paramAsyncResult)
    {
        StringBuilder localStringBuilder = new StringBuilder(getScString());
        localStringBuilder.append("\n");
        if (paramAsyncResult.exception != null)
        {
            this.state = MmiCode.State.FAILED;
            localStringBuilder.append(getErrorMessage(paramAsyncResult));
        }
        int[] arrayOfInt;
        while (true)
        {
            this.message = localStringBuilder;
            this.phone.onMMIDone(this);
            return;
            arrayOfInt = (int[])paramAsyncResult.result;
            switch (arrayOfInt[1])
            {
            default:
                break;
            case 0:
                localStringBuilder.append(this.context.getText(17039593));
                this.state = MmiCode.State.COMPLETE;
                break;
            case 1:
                localStringBuilder.append(this.context.getText(17039594));
                this.state = MmiCode.State.COMPLETE;
                break;
            case 2:
                localStringBuilder.append(this.context.getText(17039558));
                this.state = MmiCode.State.FAILED;
            case 3:
            case 4:
            }
        }
        switch (arrayOfInt[0])
        {
        default:
            localStringBuilder.append(this.context.getText(17039589));
        case 1:
        case 2:
        }
        while (true)
        {
            this.state = MmiCode.State.COMPLETE;
            break;
            localStringBuilder.append(this.context.getText(17039589));
            continue;
            localStringBuilder.append(this.context.getText(17039590));
        }
        switch (arrayOfInt[0])
        {
        default:
            localStringBuilder.append(this.context.getText(17039592));
        case 1:
        case 2:
        }
        while (true)
        {
            this.state = MmiCode.State.COMPLETE;
            break;
            localStringBuilder.append(this.context.getText(17039591));
            continue;
            localStringBuilder.append(this.context.getText(17039592));
        }
    }

    private void onQueryCfComplete(AsyncResult paramAsyncResult)
    {
        StringBuilder localStringBuilder = new StringBuilder(getScString());
        localStringBuilder.append("\n");
        if (paramAsyncResult.exception != null)
        {
            this.state = MmiCode.State.FAILED;
            localStringBuilder.append(getErrorMessage(paramAsyncResult));
            this.message = localStringBuilder;
            this.phone.onMMIDone(this);
            return;
        }
        CallForwardInfo[] arrayOfCallForwardInfo = (CallForwardInfo[])paramAsyncResult.result;
        if (arrayOfCallForwardInfo.length == 0)
        {
            localStringBuilder.append(this.context.getText(17039562));
            this.phone.mIccRecords.setVoiceCallForwardingFlag(1, false);
        }
        while (true)
        {
            this.state = MmiCode.State.COMPLETE;
            break;
            SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder();
            int i = 1;
            while (i <= 128)
            {
                int j = 0;
                int k = arrayOfCallForwardInfo.length;
                while (j < k)
                {
                    if ((i & arrayOfCallForwardInfo[j].serviceClass) != 0)
                    {
                        localSpannableStringBuilder.append(makeCFQueryResultMessage(arrayOfCallForwardInfo[j], i));
                        localSpannableStringBuilder.append("\n");
                    }
                    j++;
                }
                i <<= 1;
            }
            localStringBuilder.append(localSpannableStringBuilder);
        }
    }

    private void onQueryComplete(AsyncResult paramAsyncResult)
    {
        StringBuilder localStringBuilder = new StringBuilder(getScString());
        localStringBuilder.append("\n");
        if (paramAsyncResult.exception != null)
        {
            this.state = MmiCode.State.FAILED;
            localStringBuilder.append(getErrorMessage(paramAsyncResult));
            this.message = localStringBuilder;
            this.phone.onMMIDone(this);
            return;
        }
        int[] arrayOfInt = (int[])paramAsyncResult.result;
        if (arrayOfInt.length != 0)
            if (arrayOfInt[0] == 0)
                localStringBuilder.append(this.context.getText(17039562));
        while (true)
        {
            this.state = MmiCode.State.COMPLETE;
            break;
            if (this.sc.equals("43"))
            {
                localStringBuilder.append(createQueryCallWaitingResultMessage(arrayOfInt[1]));
            }
            else if (isServiceCodeCallBarring(this.sc))
            {
                localStringBuilder.append(createQueryCallBarringResultMessage(arrayOfInt[0]));
            }
            else if (arrayOfInt[0] == 1)
            {
                localStringBuilder.append(this.context.getText(17039560));
            }
            else
            {
                localStringBuilder.append(this.context.getText(17039558));
                continue;
                localStringBuilder.append(this.context.getText(17039558));
            }
        }
    }

    private void onSetComplete(AsyncResult paramAsyncResult)
    {
        StringBuilder localStringBuilder = new StringBuilder(getScString());
        localStringBuilder.append("\n");
        CommandException.Error localError;
        if (paramAsyncResult.exception != null)
        {
            this.state = MmiCode.State.FAILED;
            if ((paramAsyncResult.exception instanceof CommandException))
            {
                localError = ((CommandException)paramAsyncResult.exception).getCommandError();
                if (localError == CommandException.Error.PASSWORD_INCORRECT)
                    if (isPinCommand())
                        if ((this.sc.equals("05")) || (this.sc.equals("052")))
                            localStringBuilder.append(this.context.getText(17039568));
            }
        }
        while (true)
        {
            this.message = localStringBuilder;
            this.phone.onMMIDone(this);
            return;
            localStringBuilder.append(this.context.getText(17039567));
            continue;
            localStringBuilder.append(this.context.getText(17039565));
            continue;
            if (localError == CommandException.Error.SIM_PUK2)
            {
                localStringBuilder.append(this.context.getText(17039567));
                localStringBuilder.append("\n");
                localStringBuilder.append(this.context.getText(17039573));
            }
            else if (localError == CommandException.Error.FDN_CHECK_FAILURE)
            {
                Log.i("GSM", "FDN_CHECK_FAILURE");
                localStringBuilder.append(this.context.getText(17039559));
            }
            else
            {
                localStringBuilder.append(this.context.getText(17039558));
                continue;
                localStringBuilder.append(this.context.getText(17039558));
                continue;
                if (isActivate())
                {
                    this.state = MmiCode.State.COMPLETE;
                    localStringBuilder.append(this.context.getText(17039560));
                    if (this.sc.equals("31"))
                        this.phone.saveClirSetting(1);
                }
                else if (isDeactivate())
                {
                    this.state = MmiCode.State.COMPLETE;
                    localStringBuilder.append(this.context.getText(17039562));
                    if (this.sc.equals("31"))
                        this.phone.saveClirSetting(2);
                }
                else if (isRegister())
                {
                    this.state = MmiCode.State.COMPLETE;
                    localStringBuilder.append(this.context.getText(17039563));
                }
                else if (isErasure())
                {
                    this.state = MmiCode.State.COMPLETE;
                    localStringBuilder.append(this.context.getText(17039564));
                }
                else
                {
                    this.state = MmiCode.State.FAILED;
                    localStringBuilder.append(this.context.getText(17039558));
                }
            }
        }
    }

    static String scToBarringFacility(String paramString)
    {
        if (paramString == null)
            throw new RuntimeException("invalid call barring sc");
        String str;
        if (paramString.equals("33"))
            str = "AO";
        while (true)
        {
            return str;
            if (paramString.equals("331"))
            {
                str = "OI";
            }
            else if (paramString.equals("332"))
            {
                str = "OX";
            }
            else if (paramString.equals("35"))
            {
                str = "AI";
            }
            else if (paramString.equals("351"))
            {
                str = "IR";
            }
            else if (paramString.equals("330"))
            {
                str = "AB";
            }
            else if (paramString.equals("333"))
            {
                str = "AG";
            }
            else
            {
                if (!paramString.equals("353"))
                    break;
                str = "AC";
            }
        }
        throw new RuntimeException("invalid call barring sc");
    }

    private static int scToCallForwardReason(String paramString)
    {
        if (paramString == null)
            throw new RuntimeException("invalid call forward sc");
        int i;
        if (paramString.equals("002"))
            i = 4;
        while (true)
        {
            return i;
            if (paramString.equals("21"))
            {
                i = 0;
            }
            else if (paramString.equals("67"))
            {
                i = 1;
            }
            else if (paramString.equals("62"))
            {
                i = 3;
            }
            else if (paramString.equals("61"))
            {
                i = 2;
            }
            else
            {
                if (!paramString.equals("004"))
                    break;
                i = 5;
            }
        }
        throw new RuntimeException("invalid call forward sc");
    }

    private CharSequence serviceClassToCFString(int paramInt)
    {
        CharSequence localCharSequence;
        switch (paramInt)
        {
        default:
            localCharSequence = null;
        case 1:
        case 2:
        case 4:
        case 8:
        case 16:
        case 32:
        case 64:
        case 128:
        }
        while (true)
        {
            return localCharSequence;
            localCharSequence = this.context.getText(17039604);
            continue;
            localCharSequence = this.context.getText(17039605);
            continue;
            localCharSequence = this.context.getText(17039606);
            continue;
            localCharSequence = this.context.getText(17039607);
            continue;
            localCharSequence = this.context.getText(17039609);
            continue;
            localCharSequence = this.context.getText(17039608);
            continue;
            localCharSequence = this.context.getText(17039610);
            continue;
            localCharSequence = this.context.getText(17039611);
        }
    }

    private static int siToServiceClass(String paramString)
    {
        int i;
        if ((paramString == null) || (paramString.length() == 0))
            i = 0;
        while (true)
        {
            return i;
            switch (Integer.parseInt(paramString, 10))
            {
            default:
                throw new RuntimeException("unsupported MMI service code " + paramString);
            case 10:
                i = 13;
                break;
            case 11:
                i = 1;
                break;
            case 12:
                i = 12;
                break;
            case 13:
                i = 4;
                break;
            case 16:
                i = 8;
                break;
            case 19:
                i = 5;
                break;
            case 20:
                i = 48;
                break;
            case 21:
                i = 160;
                break;
            case 22:
                i = 80;
                break;
            case 24:
                i = 16;
                break;
            case 25:
                i = 32;
                break;
            case 26:
                i = 17;
                break;
            case 99:
                i = 64;
            }
        }
    }

    private static int siToTime(String paramString)
    {
        if ((paramString == null) || (paramString.length() == 0));
        for (int i = 0; ; i = Integer.parseInt(paramString, 10))
            return i;
    }

    public void cancel()
    {
        if ((this.state == MmiCode.State.COMPLETE) || (this.state == MmiCode.State.FAILED));
        while (true)
        {
            return;
            this.state = MmiCode.State.CANCELLED;
            if (this.isPendingUSSD)
                this.phone.mCM.cancelPendingUssd(obtainMessage(7, this));
            else
                this.phone.onMMIDone(this);
        }
    }

    int getCLIRMode()
    {
        int i;
        if ((this.sc != null) && (this.sc.equals("31")))
            if (isActivate())
                i = 2;
        while (true)
        {
            return i;
            if (isDeactivate())
                i = 1;
            else
                i = 0;
        }
    }

    public CharSequence getMessage()
    {
        return this.message;
    }

    public MmiCode.State getState()
    {
        return this.state;
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
        case 1:
        case 6:
        case 2:
        case 3:
        case 5:
        case 4:
        case 7:
        }
        while (true)
        {
            return;
            onSetComplete((AsyncResult)paramMessage.obj);
            continue;
            AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
            if ((localAsyncResult2.exception == null) && (paramMessage.arg1 == 1))
                if (paramMessage.arg2 != 1)
                    break label124;
            label124: for (boolean bool = true; ; bool = false)
            {
                this.phone.mIccRecords.setVoiceCallForwardingFlag(1, bool);
                onSetComplete(localAsyncResult2);
                break;
            }
            onGetClirComplete((AsyncResult)paramMessage.obj);
            continue;
            onQueryCfComplete((AsyncResult)paramMessage.obj);
            continue;
            onQueryComplete((AsyncResult)paramMessage.obj);
            continue;
            AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult1.exception != null)
            {
                this.state = MmiCode.State.FAILED;
                this.message = getErrorMessage(localAsyncResult1);
                this.phone.onMMIDone(this);
                continue;
                this.phone.onMMIDone(this);
            }
        }
    }

    boolean isActivate()
    {
        if ((this.action != null) && (this.action.equals("*")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isCancelable()
    {
        return this.isPendingUSSD;
    }

    boolean isDeactivate()
    {
        if ((this.action != null) && (this.action.equals("#")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isErasure()
    {
        if ((this.action != null) && (this.action.equals("##")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isInterrogate()
    {
        if ((this.action != null) && (this.action.equals("*#")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isMMI()
    {
        if (this.poundString != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPendingUSSD()
    {
        return this.isPendingUSSD;
    }

    boolean isPinCommand()
    {
        if ((this.sc != null) && ((this.sc.equals("04")) || (this.sc.equals("042")) || (this.sc.equals("05")) || (this.sc.equals("052"))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isRegister()
    {
        if ((this.action != null) && (this.action.equals("**")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isShortCode()
    {
        if ((this.poundString == null) && (this.dialingNumber != null) && (this.dialingNumber.length() <= 2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isTemporaryModeCLIR()
    {
        if ((this.sc != null) && (this.sc.equals("31")) && (this.dialingNumber != null) && ((isActivate()) || (isDeactivate())));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isUssdRequest()
    {
        return this.isUssdRequest;
    }

    void onUssdFinished(String paramString, boolean paramBoolean)
    {
        if (this.state == MmiCode.State.PENDING)
            if (paramString != null)
                break label53;
        label53: for (this.message = this.context.getText(17039566); ; this.message = paramString)
        {
            this.isUssdRequest = paramBoolean;
            if (!paramBoolean)
                this.state = MmiCode.State.COMPLETE;
            this.phone.onMMIDone(this);
            return;
        }
    }

    void onUssdFinishedError()
    {
        if (this.state == MmiCode.State.PENDING)
        {
            this.state = MmiCode.State.FAILED;
            this.message = this.context.getText(17039558);
            this.phone.onMMIDone(this);
        }
    }

    void processCode()
    {
        try
        {
            if (isShortCode())
            {
                Log.d("GSM", "isShortCode");
                sendUssd(this.dialingNumber);
                break label1111;
            }
            if (this.dialingNumber != null)
                throw new RuntimeException("Invalid or Unsupported MMI Code");
        }
        catch (RuntimeException localRuntimeException)
        {
            this.state = MmiCode.State.FAILED;
            this.message = this.context.getText(17039558);
            this.phone.onMMIDone(this);
        }
        int m;
        int n;
        int i2;
        int i3;
        int i4;
        String str1;
        String str2;
        int i;
        if ((this.sc != null) && (this.sc.equals("30")))
        {
            Log.d("GSM", "is CLIP");
            if (isInterrogate())
                this.phone.mCM.queryCLIP(obtainMessage(5, this));
            else
                throw new RuntimeException("Invalid or Unsupported MMI Code");
        }
        else if ((this.sc != null) && (this.sc.equals("31")))
        {
            Log.d("GSM", "is CLIR");
            if (isActivate())
                this.phone.mCM.setCLIR(1, obtainMessage(1, this));
            else if (isDeactivate())
                this.phone.mCM.setCLIR(2, obtainMessage(1, this));
            else if (isInterrogate())
                this.phone.mCM.getCLIR(obtainMessage(2, this));
            else
                throw new RuntimeException("Invalid or Unsupported MMI Code");
        }
        else if (isServiceCodeCallForwarding(this.sc))
        {
            Log.d("GSM", "is CF");
            String str8 = this.sia;
            m = siToServiceClass(this.sib);
            n = scToCallForwardReason(this.sc);
            int i1 = siToTime(this.sic);
            if (isInterrogate())
            {
                this.phone.mCM.queryCallForwardStatus(n, m, str8, obtainMessage(3, this));
            }
            else if (isActivate())
            {
                i2 = 1;
                break label1112;
                Log.d("GSM", "is CF setCallForward");
                this.phone.mCM.setCallForward(i2, n, m, str8, i1, obtainMessage(6, i3, i4, this));
            }
            else
            {
                if (isDeactivate())
                {
                    i2 = 0;
                    break label1112;
                }
                if (isRegister())
                {
                    i2 = 3;
                    break label1112;
                }
                if (isErasure())
                {
                    i2 = 4;
                    break label1112;
                }
                throw new RuntimeException("invalid action");
            }
        }
        else if (isServiceCodeCallBarring(this.sc))
        {
            String str6 = this.sia;
            int k = siToServiceClass(this.sib);
            String str7 = scToBarringFacility(this.sc);
            if (isInterrogate())
                this.phone.mCM.queryFacilityLock(str7, str6, k, obtainMessage(5, this));
            else if ((isActivate()) || (isDeactivate()))
                this.phone.mCM.setFacilityLock(str7, isActivate(), str6, k, obtainMessage(1, this));
            else
                throw new RuntimeException("Invalid or Unsupported MMI Code");
        }
        else if ((this.sc != null) && (this.sc.equals("03")))
        {
            String str3 = this.sib;
            String str4 = this.sic;
            if ((isActivate()) || (isRegister()))
            {
                this.action = "**";
                if (this.sia == null);
                for (String str5 = "AB"; str4.equals(this.pwd); str5 = scToBarringFacility(this.sia))
                {
                    this.phone.mCM.changeBarringPassword(str5, str3, str4, obtainMessage(1, this));
                    break label1111;
                }
                handlePasswordError(17039565);
            }
            else
            {
                throw new RuntimeException("Invalid or Unsupported MMI Code");
            }
        }
        else if ((this.sc != null) && (this.sc.equals("43")))
        {
            int j = siToServiceClass(this.sia);
            if ((isActivate()) || (isDeactivate()))
                this.phone.mCM.setCallWaiting(isActivate(), j, obtainMessage(1, this));
            else if (isInterrogate())
                this.phone.mCM.queryCallWaiting(j, obtainMessage(5, this));
            else
                throw new RuntimeException("Invalid or Unsupported MMI Code");
        }
        else if (isPinCommand())
        {
            str1 = this.sia;
            str2 = this.sib;
            i = str2.length();
            if (isRegister())
            {
                if (str2.equals(this.sic))
                    break label1168;
                handlePasswordError(17039569);
                break label1111;
            }
        }
        label1162: label1168: 
        while (true)
        {
            label883: handlePasswordError(17039570);
            label1111: label1112: 
            do
            {
                if ((this.sc.equals("04")) && (this.phone.getIccCard().getState() == IccCard.State.PUK_REQUIRED))
                {
                    handlePasswordError(17039572);
                }
                else if (this.sc.equals("04"))
                {
                    this.phone.mCM.changeIccPin(str1, str2, obtainMessage(1, this));
                }
                else if (this.sc.equals("042"))
                {
                    this.phone.mCM.changeIccPin2(str1, str2, obtainMessage(1, this));
                }
                else if (this.sc.equals("05"))
                {
                    this.phone.mCM.supplyIccPuk(str1, str2, obtainMessage(1, this));
                }
                else if (this.sc.equals("052"))
                {
                    this.phone.mCM.supplyIccPuk2(str1, str2, obtainMessage(1, this));
                    break label1111;
                    throw new RuntimeException("Invalid or Unsupported MMI Code");
                    if (this.poundString != null)
                        sendUssd(this.poundString);
                    else
                        throw new RuntimeException("Invalid or Unsupported MMI Code");
                }
                return;
                if (((n == 0) || (n == 4)) && (((m & 0x1) != 0) || (m == 0)));
                for (i3 = 1; ; i3 = 0)
                {
                    if ((i2 != 1) && (i2 != 3))
                        break label1162;
                    i4 = 1;
                    break;
                }
                i4 = 0;
                break;
                if (i < 4)
                    break label883;
            }
            while (i <= 8);
        }
    }

    void sendUssd(String paramString)
    {
        this.isPendingUSSD = true;
        this.phone.mCM.sendUSSD(paramString, obtainMessage(4, this));
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("GsmMmiCode {");
        localStringBuilder.append("State=" + getState());
        if (this.action != null)
            localStringBuilder.append(" action=" + this.action);
        if (this.sc != null)
            localStringBuilder.append(" sc=" + this.sc);
        if (this.sia != null)
            localStringBuilder.append(" sia=" + this.sia);
        if (this.sib != null)
            localStringBuilder.append(" sib=" + this.sib);
        if (this.sic != null)
            localStringBuilder.append(" sic=" + this.sic);
        if (this.poundString != null)
            localStringBuilder.append(" poundString=" + this.poundString);
        if (this.dialingNumber != null)
            localStringBuilder.append(" dialingNumber=" + this.dialingNumber);
        if (this.pwd != null)
            localStringBuilder.append(" pwd=" + this.pwd);
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmMmiCode
 * JD-Core Version:        0.6.2
 */