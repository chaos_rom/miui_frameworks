package com.android.internal.telephony.gsm;

import android.telephony.SmsCbCmasInfo;
import android.telephony.SmsCbEtwsInfo;
import java.util.Arrays;

class SmsCbHeader
{
    static final int FORMAT_ETWS_PRIMARY = 3;
    static final int FORMAT_GSM = 1;
    static final int FORMAT_UMTS = 2;
    private static final int MESSAGE_TYPE_CBS_MESSAGE = 1;
    static final int PDU_HEADER_LENGTH = 6;
    private static final int PDU_LENGTH_ETWS = 56;
    private static final int PDU_LENGTH_GSM = 88;
    private final int dataCodingScheme;
    private final int format;
    private final int geographicalScope;
    private final SmsCbCmasInfo mCmasInfo;
    private final SmsCbEtwsInfo mEtwsInfo;
    private final int messageIdentifier;
    private final int nrOfPages;
    private final int pageIndex;
    private final int serialNumber;

    public SmsCbHeader(byte[] paramArrayOfByte)
        throws IllegalArgumentException
    {
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length < 6))
            throw new IllegalArgumentException("Illegal PDU");
        boolean bool3;
        boolean bool4;
        label137: byte[] arrayOfByte;
        if (paramArrayOfByte.length <= 56)
        {
            this.format = 3;
            this.geographicalScope = ((0xC0 & paramArrayOfByte[0]) >> 6);
            this.serialNumber = ((0xFF & paramArrayOfByte[0]) << 8 | 0xFF & paramArrayOfByte[1]);
            this.messageIdentifier = ((0xFF & paramArrayOfByte[2]) << 8 | 0xFF & paramArrayOfByte[3]);
            this.dataCodingScheme = -1;
            this.pageIndex = -1;
            this.nrOfPages = -1;
            if ((0x1 & paramArrayOfByte[4]) != 0)
            {
                bool3 = true;
                if ((0x80 & paramArrayOfByte[5]) == 0)
                    break label196;
                bool4 = true;
                int i3 = (0xFE & paramArrayOfByte[4]) >> 1;
                if (paramArrayOfByte.length <= 6)
                    break label202;
                arrayOfByte = Arrays.copyOfRange(paramArrayOfByte, 6, paramArrayOfByte.length);
                label165: this.mEtwsInfo = new SmsCbEtwsInfo(i3, bool3, bool4, arrayOfByte);
                this.mCmasInfo = null;
            }
        }
        while (true)
        {
            return;
            bool3 = false;
            break;
            label196: bool4 = false;
            break label137;
            label202: arrayOfByte = null;
            break label165;
            int i2;
            if (paramArrayOfByte.length <= 88)
            {
                this.format = 1;
                this.geographicalScope = ((0xC0 & paramArrayOfByte[0]) >> 6);
                this.serialNumber = ((0xFF & paramArrayOfByte[0]) << 8 | 0xFF & paramArrayOfByte[1]);
                this.messageIdentifier = ((0xFF & paramArrayOfByte[2]) << 8 | 0xFF & paramArrayOfByte[3]);
                this.dataCodingScheme = (0xFF & paramArrayOfByte[4]);
                int i1 = (0xF0 & paramArrayOfByte[5]) >> 4;
                i2 = 0xF & paramArrayOfByte[5];
                if ((i1 == 0) || (i2 == 0) || (i1 > i2))
                {
                    i1 = 1;
                    i2 = 1;
                }
                this.pageIndex = i1;
            }
            for (this.nrOfPages = i2; ; this.nrOfPages = 1)
            {
                if (!isEtwsMessage())
                    break label513;
                boolean bool1 = isEtwsEmergencyUserAlert();
                boolean bool2 = isEtwsPopupAlert();
                this.mEtwsInfo = new SmsCbEtwsInfo(getEtwsWarningType(), bool1, bool2, null);
                this.mCmasInfo = null;
                break;
                this.format = 2;
                int i = paramArrayOfByte[0];
                if (i != 1)
                    throw new IllegalArgumentException("Unsupported message type " + i);
                this.messageIdentifier = ((0xFF & paramArrayOfByte[1]) << 8 | 0xFF & paramArrayOfByte[2]);
                this.geographicalScope = ((0xC0 & paramArrayOfByte[3]) >> 6);
                this.serialNumber = ((0xFF & paramArrayOfByte[3]) << 8 | 0xFF & paramArrayOfByte[4]);
                this.dataCodingScheme = (0xFF & paramArrayOfByte[5]);
                this.pageIndex = 1;
            }
            label513: if (isCmasMessage())
            {
                int j = getCmasMessageClass();
                int k = getCmasSeverity();
                int m = getCmasUrgency();
                int n = getCmasCertainty();
                this.mEtwsInfo = null;
                this.mCmasInfo = new SmsCbCmasInfo(j, -1, -1, k, m, n);
            }
            else
            {
                this.mEtwsInfo = null;
                this.mCmasInfo = null;
            }
        }
    }

    private int getCmasCertainty()
    {
        int i;
        switch (this.messageIdentifier)
        {
        default:
            i = -1;
        case 4371:
        case 4373:
        case 4375:
        case 4377:
        case 4372:
        case 4374:
        case 4376:
        case 4378:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 1;
        }
    }

    private int getCmasMessageClass()
    {
        int i;
        switch (this.messageIdentifier)
        {
        default:
            i = -1;
        case 4370:
        case 4371:
        case 4372:
        case 4373:
        case 4374:
        case 4375:
        case 4376:
        case 4377:
        case 4378:
        case 4379:
        case 4380:
        case 4381:
        case 4382:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 3;
            continue;
            i = 4;
            continue;
            i = 5;
            continue;
            i = 6;
        }
    }

    private int getCmasSeverity()
    {
        int i;
        switch (this.messageIdentifier)
        {
        default:
            i = -1;
        case 4371:
        case 4372:
        case 4373:
        case 4374:
        case 4375:
        case 4376:
        case 4377:
        case 4378:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 1;
        }
    }

    private int getCmasUrgency()
    {
        int i;
        switch (this.messageIdentifier)
        {
        default:
            i = -1;
        case 4371:
        case 4372:
        case 4375:
        case 4376:
        case 4373:
        case 4374:
        case 4377:
        case 4378:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 1;
        }
    }

    private int getEtwsWarningType()
    {
        return -4352 + this.messageIdentifier;
    }

    private boolean isCmasMessage()
    {
        if ((this.messageIdentifier >= 4370) && (this.messageIdentifier <= 4399));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isEtwsEmergencyUserAlert()
    {
        if ((0x2000 & this.serialNumber) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isEtwsMessage()
    {
        if ((0xFFF8 & this.messageIdentifier) == 4352);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isEtwsPopupAlert()
    {
        if ((0x1000 & this.serialNumber) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    SmsCbCmasInfo getCmasInfo()
    {
        return this.mCmasInfo;
    }

    int getDataCodingScheme()
    {
        return this.dataCodingScheme;
    }

    SmsCbEtwsInfo getEtwsInfo()
    {
        return this.mEtwsInfo;
    }

    int getGeographicalScope()
    {
        return this.geographicalScope;
    }

    int getNumberOfPages()
    {
        return this.nrOfPages;
    }

    int getPageIndex()
    {
        return this.pageIndex;
    }

    int getSerialNumber()
    {
        return this.serialNumber;
    }

    int getServiceCategory()
    {
        return this.messageIdentifier;
    }

    boolean isEmergencyMessage()
    {
        if ((this.messageIdentifier >= 4352) && (this.messageIdentifier <= 6399));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isEtwsPrimaryNotification()
    {
        if (this.format == 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isUmtsFormat()
    {
        if (this.format == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toString()
    {
        return "SmsCbHeader{GS=" + this.geographicalScope + ", serialNumber=0x" + Integer.toHexString(this.serialNumber) + ", messageIdentifier=0x" + Integer.toHexString(this.messageIdentifier) + ", DCS=0x" + Integer.toHexString(this.dataCodingScheme) + ", page " + this.pageIndex + " of " + this.nrOfPages + '}';
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.SmsCbHeader
 * JD-Core Version:        0.6.2
 */