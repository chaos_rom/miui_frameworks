package com.android.internal.telephony.gsm;

import android.telephony.SmsCbLocation;
import android.telephony.SmsCbMessage;
import android.util.Pair;
import com.android.internal.telephony.GsmAlphabet;
import java.io.UnsupportedEncodingException;

public class GsmSmsCbMessage
{
    private static final char CARRIAGE_RETURN = '\r';
    private static final String[] LANGUAGE_CODES_GROUP_0;
    private static final String[] LANGUAGE_CODES_GROUP_2 = arrayOfString2;
    private static final int PDU_BODY_PAGE_LENGTH = 82;

    static
    {
        String[] arrayOfString1 = new String[16];
        arrayOfString1[0] = "de";
        arrayOfString1[1] = "en";
        arrayOfString1[2] = "it";
        arrayOfString1[3] = "fr";
        arrayOfString1[4] = "es";
        arrayOfString1[5] = "nl";
        arrayOfString1[6] = "sv";
        arrayOfString1[7] = "da";
        arrayOfString1[8] = "pt";
        arrayOfString1[9] = "fi";
        arrayOfString1[10] = "no";
        arrayOfString1[11] = "el";
        arrayOfString1[12] = "tr";
        arrayOfString1[13] = "hu";
        arrayOfString1[14] = "pl";
        arrayOfString1[15] = null;
        LANGUAGE_CODES_GROUP_0 = arrayOfString1;
        String[] arrayOfString2 = new String[16];
        arrayOfString2[0] = "cs";
        arrayOfString2[1] = "he";
        arrayOfString2[2] = "ar";
        arrayOfString2[3] = "ru";
        arrayOfString2[4] = "is";
        arrayOfString2[5] = null;
        arrayOfString2[6] = null;
        arrayOfString2[7] = null;
        arrayOfString2[8] = null;
        arrayOfString2[9] = null;
        arrayOfString2[10] = null;
        arrayOfString2[11] = null;
        arrayOfString2[12] = null;
        arrayOfString2[13] = null;
        arrayOfString2[14] = null;
        arrayOfString2[15] = null;
    }

    public static SmsCbMessage createSmsCbMessage(SmsCbLocation paramSmsCbLocation, byte[][] paramArrayOfByte)
        throws IllegalArgumentException
    {
        return createSmsCbMessage(new SmsCbHeader(paramArrayOfByte[0]), paramSmsCbLocation, paramArrayOfByte);
    }

    static SmsCbMessage createSmsCbMessage(SmsCbHeader paramSmsCbHeader, SmsCbLocation paramSmsCbLocation, byte[][] paramArrayOfByte)
        throws IllegalArgumentException
    {
        SmsCbMessage localSmsCbMessage;
        if (paramSmsCbHeader.isEtwsPrimaryNotification())
        {
            localSmsCbMessage = new SmsCbMessage(1, paramSmsCbHeader.getGeographicalScope(), paramSmsCbHeader.getSerialNumber(), paramSmsCbLocation, paramSmsCbHeader.getServiceCategory(), null, "ETWS", 3, paramSmsCbHeader.getEtwsInfo(), paramSmsCbHeader.getCmasInfo());
            return localSmsCbMessage;
        }
        String str = null;
        StringBuilder localStringBuilder = new StringBuilder();
        int i = paramArrayOfByte.length;
        for (int j = 0; j < i; j++)
        {
            Pair localPair = parseBody(paramSmsCbHeader, paramArrayOfByte[j]);
            str = (String)localPair.first;
            localStringBuilder.append((String)localPair.second);
        }
        if (paramSmsCbHeader.isEmergencyMessage());
        for (int k = 3; ; k = 0)
        {
            localSmsCbMessage = new SmsCbMessage(1, paramSmsCbHeader.getGeographicalScope(), paramSmsCbHeader.getSerialNumber(), paramSmsCbLocation, paramSmsCbHeader.getServiceCategory(), str, localStringBuilder.toString(), k, paramSmsCbHeader.getEtwsInfo(), paramSmsCbHeader.getCmasInfo());
            break;
        }
    }

    private static Pair<String, String> parseBody(SmsCbHeader paramSmsCbHeader, byte[] paramArrayOfByte)
    {
        String str = null;
        boolean bool = false;
        int i = paramSmsCbHeader.getDataCodingScheme();
        int j;
        switch ((i & 0xF0) >> 4)
        {
        case 8:
        case 10:
        case 11:
        case 12:
        case 13:
        default:
            j = 1;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 9:
        case 14:
        case 15:
        }
        StringBuilder localStringBuilder;
        while (paramSmsCbHeader.isUmtsFormat())
        {
            int k = paramArrayOfByte[6];
            if (paramArrayOfByte.length < 7 + k * 83)
            {
                throw new IllegalArgumentException("Pdu length " + paramArrayOfByte.length + " does not match " + k + " pages");
                j = 1;
                str = LANGUAGE_CODES_GROUP_0[(i & 0xF)];
                continue;
                bool = true;
                if ((i & 0xF) == 1)
                {
                    j = 3;
                }
                else
                {
                    j = 1;
                    continue;
                    j = 1;
                    str = LANGUAGE_CODES_GROUP_2[(i & 0xF)];
                    continue;
                    j = 1;
                    continue;
                    switch ((i & 0xC) >> 2)
                    {
                    default:
                        j = 1;
                        break;
                    case 1:
                        j = 2;
                        break;
                    case 2:
                        j = 3;
                        continue;
                        throw new IllegalArgumentException("Unsupported GSM dataCodingScheme " + i);
                        if ((i & 0x4) >> 2 == 1)
                        {
                            j = 2;
                            continue;
                        }
                        j = 1;
                        break;
                    }
                }
            }
            else
            {
                localStringBuilder = new StringBuilder();
                for (int m = 0; m < k; m++)
                {
                    int n = 7 + m * 83;
                    int i1 = paramArrayOfByte[(n + 82)];
                    if (i1 > 82)
                        throw new IllegalArgumentException("Page length " + i1 + " exceeds maximum value " + 82);
                    Pair localPair2 = unpackBody(paramArrayOfByte, j, n, i1, bool, str);
                    str = (String)localPair2.first;
                    localStringBuilder.append((String)localPair2.second);
                }
            }
        }
        for (Pair localPair1 = new Pair(str, localStringBuilder.toString()); ; localPair1 = unpackBody(paramArrayOfByte, j, 6, paramArrayOfByte.length - 6, bool, str))
            return localPair1;
    }

    private static Pair<String, String> unpackBody(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, String paramString)
    {
        String str = null;
        int i;
        switch (paramInt1)
        {
        case 2:
        default:
            if (str != null)
            {
                i = -1 + str.length();
                label47: if (i >= 0)
                    if (str.charAt(i) == '\r')
                        break label202;
            }
            break;
        case 1:
        case 3:
        }
        for (str = str.substring(0, i + 1); ; str = "")
        {
            while (true)
            {
                return new Pair(paramString, str);
                str = GsmAlphabet.gsm7BitPackedToString(paramArrayOfByte, paramInt2, paramInt3 * 8 / 7);
                if ((!paramBoolean) || (str == null) || (str.length() <= 2))
                    break;
                paramString = str.substring(0, 2);
                str = str.substring(3);
                break;
                if ((paramBoolean) && (paramArrayOfByte.length >= paramInt2 + 2))
                {
                    paramString = GsmAlphabet.gsm7BitPackedToString(paramArrayOfByte, paramInt2, 2);
                    paramInt2 += 2;
                    paramInt3 -= 2;
                }
                try
                {
                    str = new String(paramArrayOfByte, paramInt2, 0xFFFE & paramInt3, "utf-16");
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException)
                {
                    throw new IllegalArgumentException("Error decoding UTF-16 message", localUnsupportedEncodingException);
                }
            }
            label202: i--;
            break label47;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmSmsCbMessage
 * JD-Core Version:        0.6.2
 */