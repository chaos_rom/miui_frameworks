package com.android.internal.telephony.gsm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.provider.Telephony.Carriers;
import android.telephony.CellLocation;
import android.telephony.PhoneNumberUtils;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallForwardInfo;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CallTracker;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.DataConnectionTracker.Activity;
import com.android.internal.telephony.DataConnectionTracker.State;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccPhoneBookInterfaceManager;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.IccSmsInterfaceManager;
import com.android.internal.telephony.IccVmNotSupportedException;
import com.android.internal.telephony.MmiCode;
import com.android.internal.telephony.OperatorInfo;
import com.android.internal.telephony.Phone.DataActivityState;
import com.android.internal.telephony.Phone.DataState;
import com.android.internal.telephony.Phone.State;
import com.android.internal.telephony.Phone.SuppService;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.PhoneNotifier;
import com.android.internal.telephony.PhoneProxy;
import com.android.internal.telephony.PhoneSubInfo;
import com.android.internal.telephony.SMSDispatcher;
import com.android.internal.telephony.ServiceStateTracker;
import com.android.internal.telephony.UUSInfo;
import com.android.internal.telephony.test.SimulatedRadioControl;
import com.android.internal.telephony.uicc.UiccController;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class GSMPhone extends PhoneBase
{
    public static final String CIPHERING_KEY = "ciphering_key";
    private static final boolean LOCAL_DEBUG = true;
    static final String LOG_TAG = "GSM";
    private static final boolean VDBG = false;
    public static final String VM_NUMBER = "vm_number_key";
    public static final String VM_SIM_IMSI = "vm_sim_imsi_key";
    Thread debugPortThread;
    ServerSocket debugSocket;
    GsmCallTracker mCT;
    private String mImei;
    private String mImeiSv;
    ArrayList<GsmMmiCode> mPendingMMIs = new ArrayList();
    Registrant mPostDialHandler;
    GsmServiceStateTracker mSST;
    SimPhoneBookInterfaceManager mSimPhoneBookIntManager;
    SimSmsInterfaceManager mSimSmsIntManager;
    RegistrantList mSsnRegistrants = new RegistrantList();
    PhoneSubInfo mSubInfo;
    private String mVmNumber;

    public GSMPhone(Context paramContext, CommandsInterface paramCommandsInterface, PhoneNotifier paramPhoneNotifier)
    {
        this(paramContext, paramCommandsInterface, paramPhoneNotifier, false);
    }

    public GSMPhone(Context paramContext, CommandsInterface paramCommandsInterface, PhoneNotifier paramPhoneNotifier, boolean paramBoolean)
    {
        super(paramPhoneNotifier, paramContext, paramCommandsInterface, paramBoolean);
        if ((paramCommandsInterface instanceof SimulatedRadioControl))
            this.mSimulatedRadioControl = ((SimulatedRadioControl)paramCommandsInterface);
        this.mCM.setPhoneType(1);
        this.mIccCard.set(UiccController.getInstance(this).getIccCard());
        this.mIccRecords = ((IccCard)this.mIccCard.get()).getIccRecords();
        this.mCT = new GsmCallTracker(this);
        this.mSST = new GsmServiceStateTracker(this);
        this.mSMS = new GsmSMSDispatcher(this, this.mSmsStorageMonitor, this.mSmsUsageMonitor);
        this.mDataConnectionTracker = new GsmDataConnectionTracker(this);
        if (!paramBoolean)
        {
            this.mSimPhoneBookIntManager = new SimPhoneBookInterfaceManager(this);
            this.mSimSmsIntManager = new SimSmsInterfaceManager(this, this.mSMS);
            this.mSubInfo = new PhoneSubInfo(this);
        }
        this.mCM.registerForAvailable(this, 1, null);
        registerForSimRecordEvents();
        this.mCM.registerForOffOrNotAvailable(this, 8, null);
        this.mCM.registerForOn(this, 5, null);
        this.mCM.setOnUSSD(this, 7, null);
        this.mCM.setOnSuppServiceNotification(this, 2, null);
        this.mSST.registerForNetworkAttached(this, 19, null);
        SystemProperties.set("gsm.current.phone-type", new Integer(1).toString());
    }

    private String getVmSimImsi()
    {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getString("vm_sim_imsi_key", null);
    }

    private boolean handleCallDeflectionIncallSupplementaryService(String paramString)
        throws CallStateException
    {
        int i = 1;
        if (paramString.length() > i)
            i = 0;
        while (true)
        {
            return i;
            if (getRingingCall().getState() != Call.State.IDLE)
            {
                Log.d("GSM", "MmiCode 0: rejectCall");
                try
                {
                    this.mCT.rejectCall();
                }
                catch (CallStateException localCallStateException)
                {
                    Log.d("GSM", "reject failed", localCallStateException);
                    notifySuppServiceFailed(Phone.SuppService.REJECT);
                }
            }
            else if (getBackgroundCall().getState() != Call.State.IDLE)
            {
                Log.d("GSM", "MmiCode 0: hangupWaitingOrBackground");
                this.mCT.hangupWaitingOrBackground();
            }
        }
    }

    private boolean handleCallHoldIncallSupplementaryService(String paramString)
        throws CallStateException
    {
        int i = 1;
        int j = paramString.length();
        if (j > 2)
            i = 0;
        while (true)
        {
            return i;
            GsmCall localGsmCall = getForegroundCall();
            if (j > i)
            {
                int k;
                try
                {
                    k = '\0*0' + paramString.charAt(1);
                    GsmConnection localGsmConnection = this.mCT.getConnectionByIndex(localGsmCall, k);
                    if ((localGsmConnection == null) || (k < i) || (k > 7))
                        break label130;
                    Log.d("GSM", "MmiCode 2: separate call " + k);
                    this.mCT.separate(localGsmConnection);
                }
                catch (CallStateException localCallStateException2)
                {
                    Log.d("GSM", "separate failed", localCallStateException2);
                    notifySuppServiceFailed(Phone.SuppService.SEPARATE);
                }
                continue;
                label130: Log.d("GSM", "separate: invalid call index " + k);
                notifySuppServiceFailed(Phone.SuppService.SEPARATE);
            }
            else
            {
                try
                {
                    if (getRingingCall().getState() == Call.State.IDLE)
                        break label222;
                    Log.d("GSM", "MmiCode 2: accept ringing call");
                    this.mCT.acceptCall();
                }
                catch (CallStateException localCallStateException1)
                {
                    Log.d("GSM", "switch failed", localCallStateException1);
                    notifySuppServiceFailed(Phone.SuppService.SWITCH);
                }
                continue;
                label222: Log.d("GSM", "MmiCode 2: switchWaitingOrHoldingAndActive");
                this.mCT.switchWaitingOrHoldingAndActive();
            }
        }
    }

    private boolean handleCallWaitingIncallSupplementaryService(String paramString)
        throws CallStateException
    {
        int i = 1;
        int j = paramString.length();
        if (j > 2)
            i = 0;
        while (true)
        {
            return i;
            GsmCall localGsmCall = getForegroundCall();
            if (j > i)
            {
                try
                {
                    int k = '\0*0' + paramString.charAt(1);
                    if ((k < i) || (k > 7))
                        continue;
                    Log.d("GSM", "MmiCode 1: hangupConnectionByIndex " + k);
                    this.mCT.hangupConnectionByIndex(localGsmCall, k);
                }
                catch (CallStateException localCallStateException)
                {
                    Log.d("GSM", "hangup failed", localCallStateException);
                    notifySuppServiceFailed(Phone.SuppService.HANGUP);
                }
            }
            else if (localGsmCall.getState() != Call.State.IDLE)
            {
                Log.d("GSM", "MmiCode 1: hangup foreground");
                this.mCT.hangup(localGsmCall);
            }
            else
            {
                Log.d("GSM", "MmiCode 1: switchWaitingOrHoldingAndActive");
                this.mCT.switchWaitingOrHoldingAndActive();
            }
        }
    }

    private boolean handleCcbsIncallSupplementaryService(String paramString)
        throws CallStateException
    {
        int i = 1;
        if (paramString.length() > i)
            i = 0;
        while (true)
        {
            return i;
            Log.i("GSM", "MmiCode 5: CCBS not supported!");
            notifySuppServiceFailed(Phone.SuppService.UNKNOWN);
        }
    }

    private void handleCfuQueryResult(CallForwardInfo[] paramArrayOfCallForwardInfo)
    {
        boolean bool = false;
        if ((paramArrayOfCallForwardInfo == null) || (paramArrayOfCallForwardInfo.length == 0))
            this.mIccRecords.setVoiceCallForwardingFlag(1, false);
        label76: 
        while (true)
        {
            return;
            int i = 0;
            int j = paramArrayOfCallForwardInfo.length;
            while (true)
            {
                if (i >= j)
                    break label76;
                if ((0x1 & paramArrayOfCallForwardInfo[i].serviceClass) != 0)
                {
                    IccRecords localIccRecords = this.mIccRecords;
                    if (paramArrayOfCallForwardInfo[i].status == 1)
                        bool = true;
                    localIccRecords.setVoiceCallForwardingFlag(1, bool);
                    break;
                }
                i++;
            }
        }
    }

    private boolean handleEctIncallSupplementaryService(String paramString)
        throws CallStateException
    {
        int i = 1;
        if (paramString.length() != i)
            i = 0;
        while (true)
        {
            return i;
            Log.d("GSM", "MmiCode 4: explicit call transfer");
            try
            {
                explicitCallTransfer();
            }
            catch (CallStateException localCallStateException)
            {
                Log.d("GSM", "transfer failed", localCallStateException);
                notifySuppServiceFailed(Phone.SuppService.TRANSFER);
            }
        }
    }

    private boolean handleMultipartyIncallSupplementaryService(String paramString)
        throws CallStateException
    {
        int i = 1;
        if (paramString.length() > i)
            i = 0;
        while (true)
        {
            return i;
            Log.d("GSM", "MmiCode 3: merge calls");
            try
            {
                conference();
            }
            catch (CallStateException localCallStateException)
            {
                Log.d("GSM", "conference failed", localCallStateException);
                notifySuppServiceFailed(Phone.SuppService.CONFERENCE);
            }
        }
    }

    private void handleSetSelectNetwork(AsyncResult paramAsyncResult)
    {
        if (!(paramAsyncResult.userObj instanceof NetworkSelectMessage))
            Log.d("GSM", "unexpected result from user object.");
        while (true)
        {
            return;
            NetworkSelectMessage localNetworkSelectMessage = (NetworkSelectMessage)paramAsyncResult.userObj;
            if (localNetworkSelectMessage.message != null)
            {
                Log.d("GSM", "sending original message to recipient");
                AsyncResult.forMessage(localNetworkSelectMessage.message, paramAsyncResult.result, paramAsyncResult.exception);
                localNetworkSelectMessage.message.sendToTarget();
            }
            SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
            localEditor.putString("network_selection_key", localNetworkSelectMessage.operatorNumeric);
            localEditor.putString("network_selection_name_key", localNetworkSelectMessage.operatorAlphaLong);
            if (!localEditor.commit())
                Log.e("GSM", "failed to commit network selection preference");
        }
    }

    private boolean isValidCommandInterfaceCFAction(int paramInt)
    {
        switch (paramInt)
        {
        case 2:
        default:
        case 0:
        case 1:
        case 3:
        case 4:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private boolean isValidCommandInterfaceCFReason(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private void onIncomingUSSD(int paramInt, String paramString)
    {
        int i = 1;
        int j;
        label19: GsmMmiCode localGsmMmiCode;
        int k;
        if (paramInt == i)
        {
            j = i;
            if ((paramInt == 0) || (paramInt == i))
                break label94;
            localGsmMmiCode = null;
            k = 0;
            int m = this.mPendingMMIs.size();
            label34: if (k < m)
            {
                if (!((GsmMmiCode)this.mPendingMMIs.get(k)).isPendingUSSD())
                    break label99;
                localGsmMmiCode = (GsmMmiCode)this.mPendingMMIs.get(k);
            }
            if (localGsmMmiCode == null)
                break label116;
            if (i == 0)
                break label105;
            localGsmMmiCode.onUssdFinishedError();
        }
        while (true)
        {
            return;
            j = 0;
            break;
            label94: i = 0;
            break label19;
            label99: k++;
            break label34;
            label105: localGsmMmiCode.onUssdFinished(paramString, j);
            continue;
            label116: if ((i == 0) && (paramString != null))
                onNetworkInitiatedUssd(GsmMmiCode.newNetworkInitiatedUssd(paramString, j, this));
        }
    }

    private void onNetworkInitiatedUssd(GsmMmiCode paramGsmMmiCode)
    {
        this.mMmiCompleteRegistrants.notifyRegistrants(new AsyncResult(null, paramGsmMmiCode, null));
    }

    private void processIccRecordEvents(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 1:
        case 0:
        }
        while (true)
        {
            return;
            notifyCallForwardingIndicator();
            continue;
            notifyMessageWaitingIndicator();
        }
    }

    private void registerForSimRecordEvents()
    {
        this.mIccRecords.registerForNetworkSelectionModeAutomatic(this, 28, null);
        this.mIccRecords.registerForNewSms(this, 29, null);
        this.mIccRecords.registerForRecordsEvents(this, 30, null);
        this.mIccRecords.registerForRecordsLoaded(this, 3, null);
    }

    private void setVmSimImsi(String paramString)
    {
        SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        localEditor.putString("vm_sim_imsi_key", paramString);
        localEditor.apply();
    }

    private void storeVoiceMailNumber(String paramString)
    {
        SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        localEditor.putString("vm_number_key", paramString);
        localEditor.apply();
        setVmSimImsi(getSubscriberId());
    }

    private void unregisterForSimRecordEvents()
    {
        this.mIccRecords.unregisterForNetworkSelectionModeAutomatic(this);
        this.mIccRecords.unregisterForNewSms(this);
        this.mIccRecords.unregisterForRecordsEvents(this);
        this.mIccRecords.unregisterForRecordsLoaded(this);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void acceptCall()
        throws CallStateException
    {
        removeMessages(15);
        this.mCT.acceptCall();
    }

    public void activateCellBroadcastSms(int paramInt, Message paramMessage)
    {
        Log.e("GSM", "[GSMPhone] activateCellBroadcastSms() is obsolete; use SmsManager");
        paramMessage.sendToTarget();
    }

    public boolean canConference()
    {
        return this.mCT.canConference();
    }

    public boolean canDial()
    {
        return this.mCT.canDial();
    }

    public boolean canTransfer()
    {
        return this.mCT.canTransfer();
    }

    public void clearDisconnected()
    {
        this.mCT.clearDisconnected();
    }

    public void conference()
        throws CallStateException
    {
        this.mCT.conference();
    }

    public Connection dial(String paramString)
        throws CallStateException
    {
        return dial(paramString, null);
    }

    public Connection dial(String paramString, UUSInfo paramUUSInfo)
        throws CallStateException
    {
        Connection localConnection = null;
        String str = PhoneNumberUtils.stripSeparators(paramString);
        if (handleInCallMmiCommands(str));
        while (true)
        {
            return localConnection;
            GsmMmiCode localGsmMmiCode = GsmMmiCode.newFromDialString(PhoneNumberUtils.extractNetworkPortionAlt(str), this);
            Log.d("GSM", "dialing w/ mmi '" + localGsmMmiCode + "'...");
            if (localGsmMmiCode == null)
            {
                localConnection = this.mCT.dial(str, paramUUSInfo);
            }
            else if (localGsmMmiCode.isTemporaryModeCLIR())
            {
                localConnection = this.mCT.dial(localGsmMmiCode.dialingNumber, localGsmMmiCode.getCLIRMode(), paramUUSInfo);
            }
            else
            {
                this.mPendingMMIs.add(localGsmMmiCode);
                this.mMmiRegistrants.notifyRegistrants(new AsyncResult(null, localGsmMmiCode, null));
                localGsmMmiCode.processCode();
            }
        }
    }

    public void disableLocationUpdates()
    {
        this.mSST.disableLocationUpdates();
    }

    public void dispose()
    {
        synchronized (PhoneProxy.lockForRadioTechnologyChange)
        {
            super.dispose();
            this.mCM.unregisterForAvailable(this);
            unregisterForSimRecordEvents();
            this.mCM.unregisterForOffOrNotAvailable(this);
            this.mCM.unregisterForOn(this);
            this.mSST.unregisterForNetworkAttached(this);
            this.mCM.unSetOnUSSD(this);
            this.mCM.unSetOnSuppServiceNotification(this);
            this.mPendingMMIs.clear();
            this.mCT.dispose();
            this.mDataConnectionTracker.dispose();
            this.mSST.dispose();
            this.mSimPhoneBookIntManager.dispose();
            this.mSimSmsIntManager.dispose();
            this.mSubInfo.dispose();
            return;
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("GSMPhone extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" mCT=" + this.mCT);
        paramPrintWriter.println(" mSST=" + this.mSST);
        paramPrintWriter.println(" mPendingMMIs=" + this.mPendingMMIs);
        paramPrintWriter.println(" mSimPhoneBookIntManager=" + this.mSimPhoneBookIntManager);
        paramPrintWriter.println(" mSimSmsIntManager=" + this.mSimSmsIntManager);
        paramPrintWriter.println(" mSubInfo=" + this.mSubInfo);
        paramPrintWriter.println(" mVmNumber=" + this.mVmNumber);
    }

    public void enableLocationUpdates()
    {
        this.mSST.enableLocationUpdates();
    }

    public void explicitCallTransfer()
        throws CallStateException
    {
        this.mCT.explicitCallTransfer();
    }

    protected void finalize()
    {
        Log.d("GSM", "GSMPhone finalized");
    }

    public void getAvailableNetworks(Message paramMessage)
    {
        this.mCM.getAvailableNetworks(paramMessage);
    }

    public GsmCall getBackgroundCall()
    {
        return this.mCT.backgroundCall;
    }

    public void getCallForwardingOption(int paramInt, Message paramMessage)
    {
        if (isValidCommandInterfaceCFReason(paramInt))
        {
            Log.d("GSM", "requesting call forwarding query.");
            if (paramInt != 0)
                break label45;
        }
        label45: for (Message localMessage = obtainMessage(13, paramMessage); ; localMessage = paramMessage)
        {
            this.mCM.queryCallForwardStatus(paramInt, 0, null, localMessage);
            return;
        }
    }

    public CallTracker getCallTracker()
    {
        return this.mCT;
    }

    public void getCallWaiting(Message paramMessage)
    {
        this.mCM.queryCallWaiting(0, paramMessage);
    }

    public void getCellBroadcastSmsConfig(Message paramMessage)
    {
        Log.e("GSM", "[GSMPhone] getCellBroadcastSmsConfig() is obsolete; use SmsManager");
        paramMessage.sendToTarget();
    }

    public CellLocation getCellLocation()
    {
        return this.mSST.cellLoc;
    }

    public Phone.DataActivityState getDataActivityState()
    {
        Phone.DataActivityState localDataActivityState = Phone.DataActivityState.NONE;
        if (this.mSST.getCurrentGprsState() == 0)
            switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$Activity[this.mDataConnectionTracker.getActivity().ordinal()])
            {
            default:
            case 1:
            case 2:
            case 3:
            }
        while (true)
        {
            return localDataActivityState;
            localDataActivityState = Phone.DataActivityState.DATAIN;
            continue;
            localDataActivityState = Phone.DataActivityState.DATAOUT;
            continue;
            localDataActivityState = Phone.DataActivityState.DATAINANDOUT;
        }
    }

    public void getDataCallList(Message paramMessage)
    {
        this.mCM.getDataCallList(paramMessage);
    }

    public Phone.DataState getDataConnectionState(String paramString)
    {
        Phone.DataState localDataState = Phone.DataState.DISCONNECTED;
        if (this.mSST == null)
            localDataState = Phone.DataState.DISCONNECTED;
        while (true)
        {
            return localDataState;
            if (this.mSST.getCurrentGprsState() != 0)
                localDataState = Phone.DataState.DISCONNECTED;
            else if ((!this.mDataConnectionTracker.isApnTypeEnabled(paramString)) || (!this.mDataConnectionTracker.isApnTypeActive(paramString)))
                localDataState = Phone.DataState.DISCONNECTED;
            else
                switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$State[this.mDataConnectionTracker.getState(paramString).ordinal()])
                {
                default:
                    break;
                case 1:
                case 2:
                    localDataState = Phone.DataState.DISCONNECTED;
                    break;
                case 3:
                case 4:
                    if ((this.mCT.state != Phone.State.IDLE) && (!this.mSST.isConcurrentVoiceAndDataAllowed()))
                        localDataState = Phone.DataState.SUSPENDED;
                    else
                        localDataState = Phone.DataState.CONNECTED;
                    break;
                case 5:
                case 6:
                case 7:
                    localDataState = Phone.DataState.CONNECTING;
                }
        }
    }

    public boolean getDataRoamingEnabled()
    {
        return this.mDataConnectionTracker.getDataOnRoamingEnabled();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public String getDeviceId()
    {
        this.mImei = Injector.checkEmptyImei(this, this.mImei);
        return this.mImei;
    }

    public String getDeviceSvn()
    {
        return this.mImeiSv;
    }

    public String getEsn()
    {
        Log.e("GSM", "[GSMPhone] getEsn() is a CDMA method");
        return "0";
    }

    public GsmCall getForegroundCall()
    {
        return this.mCT.foregroundCall;
    }

    public IccPhoneBookInterfaceManager getIccPhoneBookInterfaceManager()
    {
        return this.mSimPhoneBookIntManager;
    }

    public IccSmsInterfaceManager getIccSmsInterfaceManager()
    {
        return this.mSimSmsIntManager;
    }

    public String getImei()
    {
        return this.mImei;
    }

    public String getLine1AlphaTag()
    {
        return this.mIccRecords.getMsisdnAlphaTag();
    }

    public String getLine1Number()
    {
        return this.mIccRecords.getMsisdnNumber();
    }

    public String getMeid()
    {
        Log.e("GSM", "[GSMPhone] getMeid() is a CDMA method");
        return "0";
    }

    public String getMsisdn()
    {
        return this.mIccRecords.getMsisdnNumber();
    }

    public boolean getMute()
    {
        return this.mCT.getMute();
    }

    public void getNeighboringCids(Message paramMessage)
    {
        this.mCM.getNeighboringCids(paramMessage);
    }

    public void getOutgoingCallerIdDisplay(Message paramMessage)
    {
        this.mCM.getCLIR(paramMessage);
    }

    public List<? extends MmiCode> getPendingMmiCodes()
    {
        return this.mPendingMMIs;
    }

    public String getPhoneName()
    {
        return "GSM";
    }

    public PhoneSubInfo getPhoneSubInfo()
    {
        return this.mSubInfo;
    }

    public int getPhoneType()
    {
        return 1;
    }

    public GsmCall getRingingCall()
    {
        return this.mCT.ringingCall;
    }

    public ServiceState getServiceState()
    {
        return this.mSST.ss;
    }

    public ServiceStateTracker getServiceStateTracker()
    {
        return this.mSST;
    }

    public SignalStrength getSignalStrength()
    {
        return this.mSST.mSignalStrength;
    }

    public Phone.State getState()
    {
        return this.mCT.state;
    }

    public String getSubscriberId()
    {
        return this.mIccRecords.getIMSI();
    }

    public String getVoiceMailAlphaTag()
    {
        String str = this.mIccRecords.getVoiceMailAlphaTag();
        if ((str == null) || (str.length() == 0))
            str = this.mContext.getText(17039364).toString();
        return str;
    }

    public String getVoiceMailNumber()
    {
        String str = this.mIccRecords.getVoiceMailNumber();
        if (TextUtils.isEmpty(str))
            str = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("vm_number_key", null);
        return str;
    }

    public boolean handleInCallMmiCommands(String paramString)
        throws CallStateException
    {
        boolean bool;
        if (!isInCall())
            bool = false;
        while (true)
        {
            return bool;
            if (TextUtils.isEmpty(paramString))
            {
                bool = false;
            }
            else
            {
                bool = false;
                switch (paramString.charAt(0))
                {
                default:
                    break;
                case '0':
                    bool = handleCallDeflectionIncallSupplementaryService(paramString);
                    break;
                case '1':
                    bool = handleCallWaitingIncallSupplementaryService(paramString);
                    break;
                case '2':
                    bool = handleCallHoldIncallSupplementaryService(paramString);
                    break;
                case '3':
                    bool = handleMultipartyIncallSupplementaryService(paramString);
                    break;
                case '4':
                    bool = handleEctIncallSupplementaryService(paramString);
                    break;
                case '5':
                    bool = handleCcbsIncallSupplementaryService(paramString);
                }
            }
        }
    }

    public void handleMessage(Message paramMessage)
    {
        boolean bool = false;
        switch (paramMessage.what)
        {
        case 4:
        case 11:
        case 14:
        case 15:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
        case 26:
        case 27:
        default:
            super.handleMessage(paramMessage);
        case 5:
        case 1:
        case 19:
        case 3:
        case 6:
        case 9:
        case 10:
        case 7:
        case 8:
        case 2:
        case 12:
        case 20:
        case 13:
        case 29:
        case 28:
        case 30:
        case 16:
        case 17:
        case 18:
        }
        while (true)
        {
            return;
            this.mCM.getBasebandVersion(obtainMessage(6));
            this.mCM.getIMEI(obtainMessage(9));
            this.mCM.getIMEISV(obtainMessage(10));
            continue;
            syncClirSetting();
            continue;
            updateCurrentCarrierInProvider();
            String str1 = getVmSimImsi();
            String str2 = getSubscriberId();
            if ((str1 != null) && (str2 != null) && (!str2.equals(str1)))
            {
                storeVoiceMailNumber(null);
                setVmSimImsi(null);
                continue;
                AsyncResult localAsyncResult9 = (AsyncResult)paramMessage.obj;
                if (localAsyncResult9.exception == null)
                {
                    Log.d("GSM", "Baseband version: " + localAsyncResult9.result);
                    setSystemProperty("gsm.version.baseband", (String)localAsyncResult9.result);
                    continue;
                    AsyncResult localAsyncResult8 = (AsyncResult)paramMessage.obj;
                    if (localAsyncResult8.exception == null)
                    {
                        this.mImei = ((String)localAsyncResult8.result);
                        continue;
                        AsyncResult localAsyncResult7 = (AsyncResult)paramMessage.obj;
                        if (localAsyncResult7.exception == null)
                        {
                            this.mImeiSv = ((String)localAsyncResult7.result);
                            continue;
                            String[] arrayOfString = (String[])((AsyncResult)paramMessage.obj).result;
                            if (arrayOfString.length > 1)
                            {
                                try
                                {
                                    onIncomingUSSD(Integer.parseInt(arrayOfString[0]), arrayOfString[1]);
                                }
                                catch (NumberFormatException localNumberFormatException)
                                {
                                    Log.w("GSM", "error parsing USSD");
                                }
                                continue;
                                int i = 0;
                                int j = this.mPendingMMIs.size();
                                while (i < j)
                                {
                                    if (((GsmMmiCode)this.mPendingMMIs.get(i)).isPendingUSSD())
                                        ((GsmMmiCode)this.mPendingMMIs.get(i)).onUssdFinishedError();
                                    i++;
                                }
                                continue;
                                AsyncResult localAsyncResult6 = (AsyncResult)paramMessage.obj;
                                ((SuppServiceNotification)localAsyncResult6.result);
                                this.mSsnRegistrants.notifyRegistrants(localAsyncResult6);
                                continue;
                                AsyncResult localAsyncResult5 = (AsyncResult)paramMessage.obj;
                                if (localAsyncResult5.exception == null)
                                {
                                    IccRecords localIccRecords = this.mIccRecords;
                                    if (paramMessage.arg1 == 1)
                                        bool = true;
                                    localIccRecords.setVoiceCallForwardingFlag(1, bool);
                                }
                                Message localMessage4 = (Message)localAsyncResult5.userObj;
                                if (localMessage4 != null)
                                {
                                    AsyncResult.forMessage(localMessage4, localAsyncResult5.result, localAsyncResult5.exception);
                                    localMessage4.sendToTarget();
                                    continue;
                                    AsyncResult localAsyncResult4 = (AsyncResult)paramMessage.obj;
                                    if (IccVmNotSupportedException.class.isInstance(localAsyncResult4.exception))
                                    {
                                        storeVoiceMailNumber(this.mVmNumber);
                                        localAsyncResult4.exception = null;
                                    }
                                    Message localMessage3 = (Message)localAsyncResult4.userObj;
                                    if (localMessage3 != null)
                                    {
                                        AsyncResult.forMessage(localMessage3, localAsyncResult4.result, localAsyncResult4.exception);
                                        localMessage3.sendToTarget();
                                        continue;
                                        AsyncResult localAsyncResult3 = (AsyncResult)paramMessage.obj;
                                        if (localAsyncResult3.exception == null)
                                            handleCfuQueryResult((CallForwardInfo[])localAsyncResult3.result);
                                        Message localMessage2 = (Message)localAsyncResult3.userObj;
                                        if (localMessage2 != null)
                                        {
                                            AsyncResult.forMessage(localMessage2, localAsyncResult3.result, localAsyncResult3.exception);
                                            localMessage2.sendToTarget();
                                            continue;
                                            AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
                                            this.mSMS.dispatchMessage((SmsMessage)localAsyncResult2.result);
                                            continue;
                                            setNetworkSelectionModeAutomatic((Message)((AsyncResult)paramMessage.obj).result);
                                            continue;
                                            processIccRecordEvents(((Integer)((AsyncResult)paramMessage.obj).result).intValue());
                                            continue;
                                            handleSetSelectNetwork((AsyncResult)paramMessage.obj);
                                            continue;
                                            AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
                                            if (localAsyncResult1.exception == null)
                                                saveClirSetting(paramMessage.arg1);
                                            Message localMessage1 = (Message)localAsyncResult1.userObj;
                                            if (localMessage1 != null)
                                            {
                                                AsyncResult.forMessage(localMessage1, localAsyncResult1.result, localAsyncResult1.exception);
                                                localMessage1.sendToTarget();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean handlePinMmi(String paramString)
    {
        GsmMmiCode localGsmMmiCode = GsmMmiCode.newFromDialString(paramString, this);
        if ((localGsmMmiCode != null) && (localGsmMmiCode.isPinCommand()))
        {
            this.mPendingMMIs.add(localGsmMmiCode);
            this.mMmiRegistrants.notifyRegistrants(new AsyncResult(null, localGsmMmiCode, null));
            localGsmMmiCode.processCode();
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean isCfEnable(int paramInt)
    {
        int i = 1;
        if ((paramInt == i) || (paramInt == 3));
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public boolean isCspPlmnEnabled()
    {
        return this.mIccRecords.isCspPlmnEnabled();
    }

    boolean isInCall()
    {
        Call.State localState1 = getForegroundCall().getState();
        Call.State localState2 = getBackgroundCall().getState();
        Call.State localState3 = getRingingCall().getState();
        if ((localState1.isAlive()) || (localState2.isAlive()) || (localState3.isAlive()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void notifyCallForwardingIndicator()
    {
        this.mNotifier.notifyCallForwardingChanged(this);
    }

    void notifyDisconnect(Connection paramConnection)
    {
        this.mDisconnectRegistrants.notifyResult(paramConnection);
    }

    void notifyLocationChanged()
    {
        this.mNotifier.notifyCellLocation(this);
    }

    void notifyNewRingingConnection(Connection paramConnection)
    {
        super.notifyNewRingingConnectionP(paramConnection);
    }

    void notifyPhoneStateChanged()
    {
        this.mNotifier.notifyPhoneState(this);
    }

    void notifyPreciseCallStateChanged()
    {
        super.notifyPreciseCallStateChangedP();
    }

    void notifyServiceStateChanged(ServiceState paramServiceState)
    {
        super.notifyServiceStateChangedP(paramServiceState);
    }

    void notifySignalStrength()
    {
        this.mNotifier.notifySignalStrength(this);
    }

    void notifySuppServiceFailed(Phone.SuppService paramSuppService)
    {
        this.mSuppServiceFailedRegistrants.notifyResult(paramSuppService);
    }

    void notifyUnknownConnection()
    {
        this.mUnknownConnectionRegistrants.notifyResult(this);
    }

    void onMMIDone(GsmMmiCode paramGsmMmiCode)
    {
        if ((this.mPendingMMIs.remove(paramGsmMmiCode)) || (paramGsmMmiCode.isUssdRequest()))
            this.mMmiCompleteRegistrants.notifyRegistrants(new AsyncResult(null, paramGsmMmiCode, null));
    }

    public void registerForSuppServiceNotification(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSsnRegistrants.addUnique(paramHandler, paramInt, paramObject);
        if (this.mSsnRegistrants.size() == 1)
            this.mCM.setSuppServiceNotifications(true, null);
    }

    public void rejectCall()
        throws CallStateException
    {
        this.mCT.rejectCall();
    }

    public void removeReferences()
    {
        Log.d("GSM", "removeReferences");
        this.mSimulatedRadioControl = null;
        this.mSimPhoneBookIntManager = null;
        this.mSimSmsIntManager = null;
        this.mSubInfo = null;
        this.mCT = null;
        this.mSST = null;
        super.removeReferences();
    }

    public void saveClirSetting(int paramInt)
    {
        SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        localEditor.putInt("clir_key", paramInt);
        if (!localEditor.commit())
            Log.e("GSM", "failed to commit CLIR preference");
    }

    public void selectNetworkManually(OperatorInfo paramOperatorInfo, Message paramMessage)
    {
        NetworkSelectMessage localNetworkSelectMessage = new NetworkSelectMessage(null);
        localNetworkSelectMessage.message = paramMessage;
        localNetworkSelectMessage.operatorNumeric = paramOperatorInfo.getOperatorNumeric();
        localNetworkSelectMessage.operatorAlphaLong = paramOperatorInfo.getOperatorAlphaLong();
        Message localMessage = obtainMessage(16, localNetworkSelectMessage);
        this.mCM.setNetworkSelectionModeManual(paramOperatorInfo.getOperatorNumeric(), localMessage);
    }

    public void sendBurstDtmf(String paramString)
    {
        Log.e("GSM", "[GSMPhone] sendBurstDtmf() is a CDMA method");
    }

    public void sendDtmf(char paramChar)
    {
        if (!PhoneNumberUtils.is12Key(paramChar))
            Log.e("GSM", "sendDtmf called with invalid character '" + paramChar + "'");
        while (true)
        {
            return;
            if (this.mCT.state == Phone.State.OFFHOOK)
                this.mCM.sendDtmf(paramChar, null);
        }
    }

    public void sendUssdResponse(String paramString)
    {
        GsmMmiCode localGsmMmiCode = GsmMmiCode.newFromUssdUserInput(paramString, this);
        this.mPendingMMIs.add(localGsmMmiCode);
        this.mMmiRegistrants.notifyRegistrants(new AsyncResult(null, localGsmMmiCode, null));
        localGsmMmiCode.sendUssd(paramString);
    }

    public void setCallForwardingOption(int paramInt1, int paramInt2, String paramString, int paramInt3, Message paramMessage)
    {
        int i;
        if ((isValidCommandInterfaceCFAction(paramInt1)) && (isValidCommandInterfaceCFReason(paramInt2)))
        {
            if (paramInt2 != 0)
                break label68;
            if (!isCfEnable(paramInt1))
                break label62;
            i = 1;
        }
        label62: label68: for (Message localMessage = obtainMessage(12, i, 0, paramMessage); ; localMessage = paramMessage)
        {
            this.mCM.setCallForward(paramInt1, paramInt2, 1, paramString, paramInt3, localMessage);
            return;
            i = 0;
            break;
        }
    }

    public void setCallWaiting(boolean paramBoolean, Message paramMessage)
    {
        this.mCM.setCallWaiting(paramBoolean, 1, paramMessage);
    }

    public void setCellBroadcastSmsConfig(int[] paramArrayOfInt, Message paramMessage)
    {
        Log.e("GSM", "[GSMPhone] setCellBroadcastSmsConfig() is obsolete; use SmsManager");
        paramMessage.sendToTarget();
    }

    public void setDataRoamingEnabled(boolean paramBoolean)
    {
        this.mDataConnectionTracker.setDataOnRoamingEnabled(paramBoolean);
    }

    public void setLine1Number(String paramString1, String paramString2, Message paramMessage)
    {
        this.mIccRecords.setMsisdnNumber(paramString1, paramString2, paramMessage);
    }

    public void setMute(boolean paramBoolean)
    {
        this.mCT.setMute(paramBoolean);
    }

    public void setNetworkSelectionModeAutomatic(Message paramMessage)
    {
        NetworkSelectMessage localNetworkSelectMessage = new NetworkSelectMessage(null);
        localNetworkSelectMessage.message = paramMessage;
        localNetworkSelectMessage.operatorNumeric = "";
        localNetworkSelectMessage.operatorAlphaLong = "";
        Message localMessage = obtainMessage(17, localNetworkSelectMessage);
        Log.d("GSM", "wrapping and sending message to connect automatically");
        this.mCM.setNetworkSelectionModeAutomatic(localMessage);
    }

    public void setOnPostDialCharacter(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mPostDialHandler = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOutgoingCallerIdDisplay(int paramInt, Message paramMessage)
    {
        this.mCM.setCLIR(paramInt, obtainMessage(18, paramInt, 0, paramMessage));
    }

    public void setRadioPower(boolean paramBoolean)
    {
        this.mSST.setRadioPower(paramBoolean);
    }

    public final void setSystemProperty(String paramString1, String paramString2)
    {
        super.setSystemProperty(paramString1, paramString2);
    }

    public void setVoiceMailNumber(String paramString1, String paramString2, Message paramMessage)
    {
        this.mVmNumber = paramString2;
        Message localMessage = obtainMessage(20, 0, 0, paramMessage);
        this.mIccRecords.setVoiceMailNumber(paramString1, this.mVmNumber, localMessage);
    }

    public void startDtmf(char paramChar)
    {
        if (!PhoneNumberUtils.is12Key(paramChar))
            Log.e("GSM", "startDtmf called with invalid character '" + paramChar + "'");
        while (true)
        {
            return;
            this.mCM.startDtmf(paramChar, null);
        }
    }

    public void stopDtmf()
    {
        this.mCM.stopDtmf(null);
    }

    public void switchHoldingAndActive()
        throws CallStateException
    {
        this.mCT.switchWaitingOrHoldingAndActive();
    }

    protected void syncClirSetting()
    {
        int i = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("clir_key", -1);
        if (i >= 0)
            this.mCM.setCLIR(i, null);
    }

    public void unregisterForSuppServiceNotification(Handler paramHandler)
    {
        this.mSsnRegistrants.remove(paramHandler);
        if (this.mSsnRegistrants.size() == 0)
            this.mCM.setSuppServiceNotifications(false, null);
    }

    boolean updateCurrentCarrierInProvider()
    {
        if (this.mIccRecords != null);
        while (true)
        {
            try
            {
                Uri localUri = Uri.withAppendedPath(Telephony.Carriers.CONTENT_URI, "current");
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("numeric", this.mIccRecords.getOperatorNumeric());
                this.mContext.getContentResolver().insert(localUri, localContentValues);
                bool = true;
                return bool;
            }
            catch (SQLException localSQLException)
            {
                Log.e("GSM", "Can't store current operator", localSQLException);
            }
            boolean bool = false;
        }
    }

    public void updateServiceLocation()
    {
        this.mSST.enableSingleLocationUpdate();
    }

    private static class NetworkSelectMessage
    {
        public Message message;
        public String operatorAlphaLong;
        public String operatorNumeric;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String checkEmptyImei(GSMPhone paramGSMPhone, String paramString)
        {
            Context localContext = paramGSMPhone.getContext();
            if ((TextUtils.isEmpty(paramString)) && (!localContext.getPackageManager().hasSystemFeature("android.hardware.telephony")) && (localContext.getPackageManager().hasSystemFeature("android.hardware.wifi")))
            {
                WifiInfo localWifiInfo = ((WifiManager)localContext.getSystemService("wifi")).getConnectionInfo();
                if (localWifiInfo != null)
                    paramString = localWifiInfo.getMacAddress();
            }
            return paramString;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GSMPhone
 * JD-Core Version:        0.6.2
 */