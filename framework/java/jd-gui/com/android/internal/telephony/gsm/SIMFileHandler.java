package com.android.internal.telephony.gsm;

import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCardApplication.AppType;
import com.android.internal.telephony.IccConstants;
import com.android.internal.telephony.IccFileHandler;

public final class SIMFileHandler extends IccFileHandler
    implements IccConstants
{
    static final String LOG_TAG = "GSM";

    public SIMFileHandler(IccCard paramIccCard, String paramString, CommandsInterface paramCommandsInterface)
    {
        super(paramIccCard, paramString, paramCommandsInterface);
    }

    protected void finalize()
    {
        Log.d("GSM", "SIMFileHandler finalized");
    }

    protected String getEFPath(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getCommonIccEFPath(paramInt);
            if (str == null)
            {
                if ((this.mParentCard == null) || (!this.mParentCard.isApplicationOnIcc(IccCardApplication.AppType.APPTYPE_USIM)))
                    break label223;
                str = "3F007F105F3A";
            }
            break;
        case 28476:
        case 28472:
        case 28486:
        case 28589:
        case 28613:
        case 28615:
        case 28616:
        case 28617:
        case 28618:
        case 28619:
        case 28621:
        case 28433:
        case 28435:
        case 28436:
        case 28437:
        case 28438:
        case 28439:
        case 28440:
        case 20272:
        }
        while (true)
        {
            return str;
            str = "3F007F10";
            continue;
            str = "3F007F20";
            continue;
            str = "3F007F20";
            continue;
            str = "3F007F105F3A";
            continue;
            label223: Log.e("GSM", "Error: EF Path being returned in null");
        }
    }

    public void handleMessage(Message paramMessage)
    {
        super.handleMessage(paramMessage);
    }

    protected void logd(String paramString)
    {
        Log.d("GSM", "[SIMFileHandler] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("GSM", "[SIMFileHandler] " + paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.SIMFileHandler
 * JD-Core Version:        0.6.2
 */