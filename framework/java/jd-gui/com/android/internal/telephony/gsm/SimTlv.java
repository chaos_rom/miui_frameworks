package com.android.internal.telephony.gsm;

public class SimTlv
{
    int curDataLength;
    int curDataOffset;
    int curOffset;
    boolean hasValidTlvObject;
    byte[] record;
    int tlvLength;
    int tlvOffset;

    public SimTlv(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        this.record = paramArrayOfByte;
        this.tlvOffset = paramInt1;
        this.tlvLength = paramInt2;
        this.curOffset = paramInt1;
        this.hasValidTlvObject = parseCurrentTlvObject();
    }

    private boolean parseCurrentTlvObject()
    {
        boolean bool = false;
        try
        {
            if ((this.record[this.curOffset] != 0) && ((0xFF & this.record[this.curOffset]) != 255))
            {
                if ((0xFF & this.record[(1 + this.curOffset)]) < 128)
                    this.curDataLength = (0xFF & this.record[(1 + this.curOffset)]);
                for (this.curDataOffset = (2 + this.curOffset); this.curDataLength + this.curDataOffset <= this.tlvOffset + this.tlvLength; this.curDataOffset = (3 + this.curOffset))
                {
                    bool = true;
                    break;
                    if ((0xFF & this.record[(1 + this.curOffset)]) != 129)
                        break;
                    this.curDataLength = (0xFF & this.record[(2 + this.curOffset)]);
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
        {
        }
        return bool;
    }

    public byte[] getData()
    {
        byte[] arrayOfByte;
        if (!this.hasValidTlvObject)
            arrayOfByte = null;
        while (true)
        {
            return arrayOfByte;
            arrayOfByte = new byte[this.curDataLength];
            System.arraycopy(this.record, this.curDataOffset, arrayOfByte, 0, this.curDataLength);
        }
    }

    public int getTag()
    {
        if (!this.hasValidTlvObject);
        for (int i = 0; ; i = 0xFF & this.record[this.curOffset])
            return i;
    }

    public boolean isValidObject()
    {
        return this.hasValidTlvObject;
    }

    public boolean nextObject()
    {
        if (!this.hasValidTlvObject);
        for (boolean bool = false; ; bool = this.hasValidTlvObject)
        {
            return bool;
            this.curOffset = (this.curDataOffset + this.curDataLength);
            this.hasValidTlvObject = parseCurrentTlvObject();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.SimTlv
 * JD-Core Version:        0.6.2
 */