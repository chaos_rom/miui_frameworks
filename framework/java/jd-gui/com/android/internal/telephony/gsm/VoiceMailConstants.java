package com.android.internal.telephony.gsm;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import com.android.internal.util.XmlUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class VoiceMailConstants
{
    static final String LOG_TAG = "GSM";
    static final int NAME = 0;
    static final int NUMBER = 1;
    static final String PARTNER_VOICEMAIL_PATH = "etc/voicemail-conf.xml";
    static final int SIZE = 3;
    static final int TAG = 2;
    private HashMap<String, String[]> CarrierVmMap = new HashMap();

    VoiceMailConstants()
    {
        loadVoiceMail();
    }

    private void loadVoiceMail()
    {
        File localFile = new File(Environment.getRootDirectory(), "etc/voicemail-conf.xml");
        try
        {
            localFileReader = new FileReader(localFile);
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            try
            {
                FileReader localFileReader;
                XmlPullParser localXmlPullParser = Xml.newPullParser();
                localXmlPullParser.setInput(localFileReader);
                XmlUtils.beginDocument(localXmlPullParser, "voicemail");
                while (true)
                {
                    XmlUtils.nextElement(localXmlPullParser);
                    boolean bool = "voicemail".equals(localXmlPullParser.getName());
                    if (!bool)
                        while (true)
                        {
                            return;
                            localFileNotFoundException = localFileNotFoundException;
                            Log.w("GSM", "Can't open " + Environment.getRootDirectory() + "/" + "etc/voicemail-conf.xml");
                        }
                    String[] arrayOfString = new String[3];
                    String str = localXmlPullParser.getAttributeValue(null, "numeric");
                    arrayOfString[0] = localXmlPullParser.getAttributeValue(null, "carrier");
                    arrayOfString[1] = localXmlPullParser.getAttributeValue(null, "vmnumber");
                    arrayOfString[2] = localXmlPullParser.getAttributeValue(null, "vmtag");
                    this.CarrierVmMap.put(str, arrayOfString);
                }
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                while (true)
                    Log.w("GSM", "Exception in Voicemail parser " + localXmlPullParserException);
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.w("GSM", "Exception in Voicemail parser " + localIOException);
            }
        }
    }

    boolean containsCarrier(String paramString)
    {
        return this.CarrierVmMap.containsKey(paramString);
    }

    String getCarrierName(String paramString)
    {
        return ((String[])this.CarrierVmMap.get(paramString))[0];
    }

    String getVoiceMailNumber(String paramString)
    {
        return ((String[])this.CarrierVmMap.get(paramString))[1];
    }

    String getVoiceMailTag(String paramString)
    {
        return ((String[])this.CarrierVmMap.get(paramString))[2];
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.VoiceMailConstants
 * JD-Core Version:        0.6.2
 */