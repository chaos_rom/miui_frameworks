package com.android.internal.telephony.gsm;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.EventLog;
import android.util.Log;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CallTracker;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.Connection.DisconnectCause;
import com.android.internal.telephony.Phone.State;
import com.android.internal.telephony.Phone.SuppService;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.UUSInfo;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public final class GsmCallTracker extends CallTracker
{
    private static final boolean DBG_POLL = false;
    static final String LOG_TAG = "GSM";
    static final int MAX_CONNECTIONS = 7;
    static final int MAX_CONNECTIONS_PER_CALL = 5;
    private static final boolean REPEAT_POLLING;
    GsmCall backgroundCall = new GsmCall(this);
    GsmConnection[] connections = new GsmConnection[7];
    boolean desiredMute = false;
    ArrayList<GsmConnection> droppedDuringPoll = new ArrayList(7);
    GsmCall foregroundCall = new GsmCall(this);
    boolean hangupPendingMO;
    GsmConnection pendingMO;
    GSMPhone phone;
    GsmCall ringingCall = new GsmCall(this);
    Phone.State state = Phone.State.IDLE;
    RegistrantList voiceCallEndedRegistrants = new RegistrantList();
    RegistrantList voiceCallStartedRegistrants = new RegistrantList();

    GsmCallTracker(GSMPhone paramGSMPhone)
    {
        this.phone = paramGSMPhone;
        this.cm = paramGSMPhone.mCM;
        this.cm.registerForCallStateChanged(this, 2, null);
        this.cm.registerForOn(this, 9, null);
        this.cm.registerForNotAvailable(this, 10, null);
    }

    private void dumpState()
    {
        Log.i("GSM", "Phone State:" + this.state);
        Log.i("GSM", "Ringing call: " + this.ringingCall.toString());
        List localList1 = this.ringingCall.getConnections();
        int i = 0;
        int j = localList1.size();
        while (i < j)
        {
            Log.i("GSM", localList1.get(i).toString());
            i++;
        }
        Log.i("GSM", "Foreground call: " + this.foregroundCall.toString());
        List localList2 = this.foregroundCall.getConnections();
        int k = 0;
        int m = localList2.size();
        while (k < m)
        {
            Log.i("GSM", localList2.get(k).toString());
            k++;
        }
        Log.i("GSM", "Background call: " + this.backgroundCall.toString());
        List localList3 = this.backgroundCall.getConnections();
        int n = 0;
        int i1 = localList3.size();
        while (n < i1)
        {
            Log.i("GSM", localList3.get(n).toString());
            n++;
        }
    }

    private void fakeHoldForegroundBeforeDial()
    {
        List localList = (List)this.foregroundCall.connections.clone();
        int i = 0;
        int j = localList.size();
        while (i < j)
        {
            ((GsmConnection)localList.get(i)).fakeHoldBeforeDial();
            i++;
        }
    }

    private Phone.SuppService getFailedService(int paramInt)
    {
        Phone.SuppService localSuppService;
        switch (paramInt)
        {
        case 9:
        case 10:
        default:
            localSuppService = Phone.SuppService.UNKNOWN;
        case 8:
        case 11:
        case 12:
        case 13:
        }
        while (true)
        {
            return localSuppService;
            localSuppService = Phone.SuppService.SWITCH;
            continue;
            localSuppService = Phone.SuppService.CONFERENCE;
            continue;
            localSuppService = Phone.SuppService.SEPARATE;
            continue;
            localSuppService = Phone.SuppService.TRANSFER;
        }
    }

    private void handleRadioNotAvailable()
    {
        pollCallsWhenSafe();
    }

    private void internalClearDisconnected()
    {
        this.ringingCall.clearDisconnected();
        this.foregroundCall.clearDisconnected();
        this.backgroundCall.clearDisconnected();
    }

    private Message obtainCompleteMessage()
    {
        return obtainCompleteMessage(4);
    }

    private Message obtainCompleteMessage(int paramInt)
    {
        this.pendingOperations = (1 + this.pendingOperations);
        this.lastRelevantPoll = null;
        this.needsPoll = true;
        return obtainMessage(paramInt);
    }

    private void operationComplete()
    {
        this.pendingOperations = (-1 + this.pendingOperations);
        if ((this.pendingOperations == 0) && (this.needsPoll))
        {
            this.lastRelevantPoll = obtainMessage(1);
            this.cm.getCurrentCalls(this.lastRelevantPoll);
        }
        while (true)
        {
            return;
            if (this.pendingOperations < 0)
            {
                Log.e("GSM", "GsmCallTracker.pendingOperations < 0");
                this.pendingOperations = 0;
            }
        }
    }

    private void updatePhoneState()
    {
        Phone.State localState = this.state;
        if (this.ringingCall.isRinging())
        {
            this.state = Phone.State.RINGING;
            if ((this.state != Phone.State.IDLE) || (localState == this.state))
                break label120;
            this.voiceCallEndedRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
        }
        while (true)
        {
            if (this.state != localState)
                this.phone.notifyPhoneStateChanged();
            return;
            if ((this.pendingMO != null) || (!this.foregroundCall.isIdle()) || (!this.backgroundCall.isIdle()))
            {
                this.state = Phone.State.OFFHOOK;
                break;
            }
            this.state = Phone.State.IDLE;
            break;
            label120: if ((localState == Phone.State.IDLE) && (localState != this.state))
                this.voiceCallStartedRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
        }
    }

    void acceptCall()
        throws CallStateException
    {
        if (this.ringingCall.getState() == Call.State.INCOMING)
        {
            Log.i("phone", "acceptCall: incoming...");
            setMute(false);
            this.cm.acceptCall(obtainCompleteMessage());
        }
        while (true)
        {
            return;
            if (this.ringingCall.getState() != Call.State.WAITING)
                break;
            setMute(false);
            switchWaitingOrHoldingAndActive();
        }
        throw new CallStateException("phone not ringing");
    }

    boolean canConference()
    {
        if ((this.foregroundCall.getState() == Call.State.ACTIVE) && (this.backgroundCall.getState() == Call.State.HOLDING) && (!this.backgroundCall.isFull()) && (!this.foregroundCall.isFull()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean canDial()
    {
        int i = this.phone.getServiceState().getState();
        String str = SystemProperties.get("ro.telephony.disable-call", "false");
        if ((i != 3) && (this.pendingMO == null) && (!this.ringingCall.isRinging()) && (!str.equals("true")) && ((!this.foregroundCall.getState().isAlive()) || (!this.backgroundCall.getState().isAlive())));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean canTransfer()
    {
        if ((this.foregroundCall.getState() == Call.State.ACTIVE) && (this.backgroundCall.getState() == Call.State.HOLDING));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void clearDisconnected()
    {
        internalClearDisconnected();
        updatePhoneState();
        this.phone.notifyPreciseCallStateChanged();
    }

    void conference()
        throws CallStateException
    {
        this.cm.conference(obtainCompleteMessage(11));
    }

    Connection dial(String paramString)
        throws CallStateException
    {
        return dial(paramString, 0, null);
    }

    Connection dial(String paramString, int paramInt)
        throws CallStateException
    {
        return dial(paramString, paramInt, null);
    }

    /** @deprecated */
    Connection dial(String paramString, int paramInt, UUSInfo paramUUSInfo)
        throws CallStateException
    {
        try
        {
            clearDisconnected();
            if (!canDial())
                throw new CallStateException("cannot dial in current state");
        }
        finally
        {
        }
        if (this.foregroundCall.getState() == Call.State.ACTIVE)
        {
            switchWaitingOrHoldingAndActive();
            fakeHoldForegroundBeforeDial();
        }
        if (this.foregroundCall.getState() != Call.State.IDLE)
            throw new CallStateException("cannot dial in current state");
        this.pendingMO = new GsmConnection(this.phone.getContext(), checkForTestEmergencyNumber(paramString), this, this.foregroundCall);
        this.hangupPendingMO = false;
        if ((this.pendingMO.address == null) || (this.pendingMO.address.length() == 0) || (this.pendingMO.address.indexOf('N') >= 0))
        {
            this.pendingMO.cause = Connection.DisconnectCause.INVALID_NUMBER;
            pollCallsWhenSafe();
        }
        while (true)
        {
            updatePhoneState();
            this.phone.notifyPreciseCallStateChanged();
            GsmConnection localGsmConnection = this.pendingMO;
            return localGsmConnection;
            setMute(false);
            this.cm.dial(this.pendingMO.address, paramInt, paramUUSInfo, obtainCompleteMessage());
        }
    }

    Connection dial(String paramString, UUSInfo paramUUSInfo)
        throws CallStateException
    {
        return dial(paramString, 0, paramUUSInfo);
    }

    public void dispose()
    {
        this.cm.unregisterForCallStateChanged(this);
        this.cm.unregisterForOn(this);
        this.cm.unregisterForNotAvailable(this);
        GsmConnection[] arrayOfGsmConnection = this.connections;
        int i = arrayOfGsmConnection.length;
        int j = 0;
        while (true)
            if (j < i)
            {
                GsmConnection localGsmConnection = arrayOfGsmConnection[j];
                if (localGsmConnection != null);
                try
                {
                    hangup(localGsmConnection);
                    j++;
                }
                catch (CallStateException localCallStateException2)
                {
                    while (true)
                        Log.e("GSM", "unexpected error on hangup during dispose");
                }
            }
        try
        {
            if (this.pendingMO != null)
                hangup(this.pendingMO);
            clearDisconnected();
            return;
        }
        catch (CallStateException localCallStateException1)
        {
            while (true)
                Log.e("GSM", "unexpected error on hangup during dispose");
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("GsmCallTracker extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println("connections: length=" + this.connections.length);
        for (int i = 0; i < this.connections.length; i++)
        {
            Object[] arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = Integer.valueOf(i);
            arrayOfObject2[1] = this.connections[i];
            paramPrintWriter.printf("    connections[%d]=%s\n", arrayOfObject2);
        }
        paramPrintWriter.println(" voiceCallEndedRegistrants=" + this.voiceCallEndedRegistrants);
        paramPrintWriter.println(" voiceCallStartedRegistrants=" + this.voiceCallStartedRegistrants);
        paramPrintWriter.println(" droppedDuringPoll: size=" + this.droppedDuringPoll.size());
        for (int j = 0; j < this.droppedDuringPoll.size(); j++)
        {
            Object[] arrayOfObject1 = new Object[2];
            arrayOfObject1[0] = Integer.valueOf(j);
            arrayOfObject1[1] = this.droppedDuringPoll.get(j);
            paramPrintWriter.printf("    droppedDuringPoll[%d]=%s\n", arrayOfObject1);
        }
        paramPrintWriter.println(" ringingCall=" + this.ringingCall);
        paramPrintWriter.println(" foregroundCall=" + this.foregroundCall);
        paramPrintWriter.println(" backgroundCall=" + this.backgroundCall);
        paramPrintWriter.println(" pendingMO=" + this.pendingMO);
        paramPrintWriter.println(" hangupPendingMO=" + this.hangupPendingMO);
        paramPrintWriter.println(" phone=" + this.phone);
        paramPrintWriter.println(" desiredMute=" + this.desiredMute);
        paramPrintWriter.println(" state=" + this.state);
    }

    void explicitCallTransfer()
        throws CallStateException
    {
        this.cm.explicitCallTransfer(obtainCompleteMessage(13));
    }

    protected void finalize()
    {
        Log.d("GSM", "GsmCallTracker finalized");
    }

    GsmConnection getConnectionByIndex(GsmCall paramGsmCall, int paramInt)
        throws CallStateException
    {
        int i = paramGsmCall.connections.size();
        int j = 0;
        GsmConnection localGsmConnection;
        if (j < i)
        {
            localGsmConnection = (GsmConnection)paramGsmCall.connections.get(j);
            if (localGsmConnection.getGSMIndex() != paramInt);
        }
        while (true)
        {
            return localGsmConnection;
            j++;
            break;
            localGsmConnection = null;
        }
    }

    boolean getMute()
    {
        return this.desiredMute;
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        case 6:
        case 7:
        default:
        case 1:
        case 4:
        case 8:
        case 11:
        case 12:
        case 13:
        case 5:
        case 2:
        case 3:
        case 9:
        case 10:
        }
        while (true)
        {
            return;
            ((AsyncResult)paramMessage.obj);
            if (paramMessage == this.lastRelevantPoll)
            {
                this.needsPoll = false;
                this.lastRelevantPoll = null;
                handlePollCalls((AsyncResult)paramMessage.obj);
                continue;
                ((AsyncResult)paramMessage.obj);
                operationComplete();
                continue;
                if (((AsyncResult)paramMessage.obj).exception != null)
                    this.phone.notifySuppServiceFailed(getFailedService(paramMessage.what));
                operationComplete();
                continue;
                AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
                operationComplete();
                int i;
                GsmCellLocation localGsmCellLocation;
                Object[] arrayOfObject;
                if (localAsyncResult.exception != null)
                {
                    i = 16;
                    Log.i("GSM", "Exception during getLastCallFailCause, assuming normal disconnect");
                    if ((i == 34) || (i == 41) || (i == 42) || (i == 44) || (i == 49) || (i == 58) || (i == 65535))
                    {
                        localGsmCellLocation = (GsmCellLocation)this.phone.getCellLocation();
                        arrayOfObject = new Object[3];
                        arrayOfObject[0] = Integer.valueOf(i);
                        if (localGsmCellLocation == null)
                            break label363;
                    }
                }
                label363: for (int j = localGsmCellLocation.getCid(); ; j = -1)
                {
                    arrayOfObject[1] = Integer.valueOf(j);
                    arrayOfObject[2] = Integer.valueOf(TelephonyManager.getDefault().getNetworkType());
                    EventLog.writeEvent(50106, arrayOfObject);
                    int k = 0;
                    int m = this.droppedDuringPoll.size();
                    while (k < m)
                    {
                        ((GsmConnection)this.droppedDuringPoll.get(k)).onRemoteDisconnect(i);
                        k++;
                    }
                    i = ((int[])(int[])localAsyncResult.result)[0];
                    break;
                }
                updatePhoneState();
                this.phone.notifyPreciseCallStateChanged();
                this.droppedDuringPoll.clear();
                continue;
                pollCallsWhenSafe();
                continue;
                handleRadioAvailable();
                continue;
                handleRadioNotAvailable();
            }
        }
    }

    /** @deprecated */
    // ERROR //
    protected void handlePollCalls(AsyncResult paramAsyncResult)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: getfield 490	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     6: ifnonnull +206 -> 212
        //     9: aload_1
        //     10: getfield 530	android/os/AsyncResult:result	Ljava/lang/Object;
        //     13: checkcast 135	java/util/List
        //     16: astore_3
        //     17: aconst_null
        //     18: astore 4
        //     20: iconst_0
        //     21: istore 5
        //     23: iconst_0
        //     24: istore 6
        //     26: iconst_0
        //     27: istore 7
        //     29: iconst_0
        //     30: istore 8
        //     32: aload_3
        //     33: invokeinterface 139 1 0
        //     38: istore 9
        //     40: iload 7
        //     42: aload_0
        //     43: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     46: arraylength
        //     47: if_icmpge +503 -> 550
        //     50: aload_0
        //     51: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     54: iload 7
        //     56: aaload
        //     57: astore 18
        //     59: aconst_null
        //     60: astore 19
        //     62: iload 8
        //     64: iload 9
        //     66: if_icmpge +31 -> 97
        //     69: aload_3
        //     70: iload 8
        //     72: invokeinterface 143 2 0
        //     77: checkcast 542	com/android/internal/telephony/DriverCall
        //     80: astore 19
        //     82: aload 19
        //     84: getfield 545	com/android/internal/telephony/DriverCall:index	I
        //     87: iload 7
        //     89: iconst_1
        //     90: iadd
        //     91: if_icmpne +155 -> 246
        //     94: iinc 8 1
        //     97: aload 18
        //     99: ifnonnull +295 -> 394
        //     102: aload 19
        //     104: ifnull +290 -> 394
        //     107: aload_0
        //     108: getfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     111: ifnull +155 -> 266
        //     114: aload_0
        //     115: getfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     118: aload 19
        //     120: invokevirtual 549	com/android/internal/telephony/gsm/GsmConnection:compareTo	(Lcom/android/internal/telephony/DriverCall;)Z
        //     123: ifeq +143 -> 266
        //     126: aload_0
        //     127: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     130: iload 7
        //     132: aload_0
        //     133: getfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     136: aastore
        //     137: aload_0
        //     138: getfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     141: iload 7
        //     143: putfield 550	com/android/internal/telephony/gsm/GsmConnection:index	I
        //     146: aload_0
        //     147: getfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     150: aload 19
        //     152: invokevirtual 553	com/android/internal/telephony/gsm/GsmConnection:update	(Lcom/android/internal/telephony/DriverCall;)Z
        //     155: pop
        //     156: aload_0
        //     157: aconst_null
        //     158: putfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     161: aload_0
        //     162: getfield 359	com/android/internal/telephony/gsm/GsmCallTracker:hangupPendingMO	Z
        //     165: ifeq +738 -> 903
        //     168: aload_0
        //     169: iconst_0
        //     170: putfield 359	com/android/internal/telephony/gsm/GsmCallTracker:hangupPendingMO	Z
        //     173: aload_0
        //     174: new 106	java/lang/StringBuilder
        //     177: dup
        //     178: invokespecial 107	java/lang/StringBuilder:<init>	()V
        //     181: ldc_w 555
        //     184: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     187: iload 7
        //     189: invokevirtual 415	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     192: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     195: invokevirtual 558	com/android/internal/telephony/gsm/GsmCallTracker:log	(Ljava/lang/String;)V
        //     198: aload_0
        //     199: aload_0
        //     200: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     203: iload 7
        //     205: aaload
        //     206: invokevirtual 397	com/android/internal/telephony/gsm/GsmCallTracker:hangup	(Lcom/android/internal/telephony/gsm/GsmConnection;)V
        //     209: aload_0
        //     210: monitorexit
        //     211: return
        //     212: aload_0
        //     213: aload_1
        //     214: getfield 490	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     217: invokevirtual 562	com/android/internal/telephony/gsm/GsmCallTracker:isCommandExceptionRadioNotAvailable	(Ljava/lang/Throwable;)Z
        //     220: ifeq +14 -> 234
        //     223: new 55	java/util/ArrayList
        //     226: dup
        //     227: invokespecial 563	java/util/ArrayList:<init>	()V
        //     230: astore_3
        //     231: goto -214 -> 17
        //     234: aload_0
        //     235: invokevirtual 566	com/android/internal/telephony/gsm/GsmCallTracker:pollCallsAfterDelay	()V
        //     238: goto -29 -> 209
        //     241: astore_2
        //     242: aload_0
        //     243: monitorexit
        //     244: aload_2
        //     245: athrow
        //     246: aconst_null
        //     247: astore 19
        //     249: goto -152 -> 97
        //     252: astore 29
        //     254: ldc 11
        //     256: ldc_w 568
        //     259: invokestatic 216	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     262: pop
        //     263: goto -54 -> 209
        //     266: aload_0
        //     267: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     270: astore 25
        //     272: new 44	com/android/internal/telephony/gsm/GsmConnection
        //     275: dup
        //     276: aload_0
        //     277: getfield 82	com/android/internal/telephony/gsm/GsmCallTracker:phone	Lcom/android/internal/telephony/gsm/GSMPhone;
        //     280: invokevirtual 350	com/android/internal/telephony/gsm/GSMPhone:getContext	()Landroid/content/Context;
        //     283: aload 19
        //     285: aload_0
        //     286: iload 7
        //     288: invokespecial 571	com/android/internal/telephony/gsm/GsmConnection:<init>	(Landroid/content/Context;Lcom/android/internal/telephony/DriverCall;Lcom/android/internal/telephony/gsm/GsmCallTracker;I)V
        //     291: astore 26
        //     293: aload 25
        //     295: iload 7
        //     297: aload 26
        //     299: aastore
        //     300: aload_0
        //     301: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     304: iload 7
        //     306: aaload
        //     307: invokevirtual 575	com/android/internal/telephony/gsm/GsmConnection:getCall	()Lcom/android/internal/telephony/gsm/GsmCall;
        //     310: aload_0
        //     311: getfield 67	com/android/internal/telephony/gsm/GsmCallTracker:ringingCall	Lcom/android/internal/telephony/gsm/GsmCall;
        //     314: if_acmpne +15 -> 329
        //     317: aload_0
        //     318: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     321: iload 7
        //     323: aaload
        //     324: astore 4
        //     326: goto +577 -> 903
        //     329: ldc 11
        //     331: new 106	java/lang/StringBuilder
        //     334: dup
        //     335: invokespecial 107	java/lang/StringBuilder:<init>	()V
        //     338: ldc_w 577
        //     341: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     344: aload 19
        //     346: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     349: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     352: invokestatic 126	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     355: pop
        //     356: aload 19
        //     358: getfield 580	com/android/internal/telephony/DriverCall:state	Lcom/android/internal/telephony/DriverCall$State;
        //     361: getstatic 585	com/android/internal/telephony/DriverCall$State:ALERTING	Lcom/android/internal/telephony/DriverCall$State;
        //     364: if_acmpeq +548 -> 912
        //     367: aload 19
        //     369: getfield 580	com/android/internal/telephony/DriverCall:state	Lcom/android/internal/telephony/DriverCall$State;
        //     372: getstatic 588	com/android/internal/telephony/DriverCall$State:DIALING	Lcom/android/internal/telephony/DriverCall$State;
        //     375: if_acmpeq +537 -> 912
        //     378: aload_0
        //     379: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     382: iload 7
        //     384: aaload
        //     385: invokestatic 594	java/lang/System:currentTimeMillis	()J
        //     388: putfield 598	com/android/internal/telephony/gsm/GsmConnection:connectTime	J
        //     391: goto +521 -> 912
        //     394: aload 18
        //     396: ifnull +29 -> 425
        //     399: aload 19
        //     401: ifnonnull +24 -> 425
        //     404: aload_0
        //     405: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     408: aload 18
        //     410: invokevirtual 601	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     413: pop
        //     414: aload_0
        //     415: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     418: iload 7
        //     420: aconst_null
        //     421: aastore
        //     422: goto +484 -> 906
        //     425: aload 18
        //     427: ifnull +91 -> 518
        //     430: aload 19
        //     432: ifnull +86 -> 518
        //     435: aload 18
        //     437: aload 19
        //     439: invokevirtual 549	com/android/internal/telephony/gsm/GsmConnection:compareTo	(Lcom/android/internal/telephony/DriverCall;)Z
        //     442: ifne +76 -> 518
        //     445: aload_0
        //     446: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     449: aload 18
        //     451: invokevirtual 601	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     454: pop
        //     455: aload_0
        //     456: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     459: astore 22
        //     461: new 44	com/android/internal/telephony/gsm/GsmConnection
        //     464: dup
        //     465: aload_0
        //     466: getfield 82	com/android/internal/telephony/gsm/GsmCallTracker:phone	Lcom/android/internal/telephony/gsm/GSMPhone;
        //     469: invokevirtual 350	com/android/internal/telephony/gsm/GSMPhone:getContext	()Landroid/content/Context;
        //     472: aload 19
        //     474: aload_0
        //     475: iload 7
        //     477: invokespecial 571	com/android/internal/telephony/gsm/GsmConnection:<init>	(Landroid/content/Context;Lcom/android/internal/telephony/DriverCall;Lcom/android/internal/telephony/gsm/GsmCallTracker;I)V
        //     480: astore 23
        //     482: aload 22
        //     484: iload 7
        //     486: aload 23
        //     488: aastore
        //     489: aload_0
        //     490: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     493: iload 7
        //     495: aaload
        //     496: invokevirtual 575	com/android/internal/telephony/gsm/GsmConnection:getCall	()Lcom/android/internal/telephony/gsm/GsmCall;
        //     499: aload_0
        //     500: getfield 67	com/android/internal/telephony/gsm/GsmCallTracker:ringingCall	Lcom/android/internal/telephony/gsm/GsmCall;
        //     503: if_acmpne +415 -> 918
        //     506: aload_0
        //     507: getfield 46	com/android/internal/telephony/gsm/GsmCallTracker:connections	[Lcom/android/internal/telephony/gsm/GsmConnection;
        //     510: iload 7
        //     512: aaload
        //     513: astore 4
        //     515: goto +403 -> 918
        //     518: aload 18
        //     520: ifnull +386 -> 906
        //     523: aload 19
        //     525: ifnull +381 -> 906
        //     528: aload 18
        //     530: aload 19
        //     532: invokevirtual 553	com/android/internal/telephony/gsm/GsmConnection:update	(Lcom/android/internal/telephony/DriverCall;)Z
        //     535: istore 20
        //     537: iload 5
        //     539: ifne +385 -> 924
        //     542: iload 20
        //     544: ifeq +386 -> 930
        //     547: goto +377 -> 924
        //     550: aload_0
        //     551: getfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     554: ifnull +57 -> 611
        //     557: ldc 11
        //     559: new 106	java/lang/StringBuilder
        //     562: dup
        //     563: invokespecial 107	java/lang/StringBuilder:<init>	()V
        //     566: ldc_w 603
        //     569: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     572: aload_0
        //     573: getfield 69	com/android/internal/telephony/gsm/GsmCallTracker:foregroundCall	Lcom/android/internal/telephony/gsm/GsmCall;
        //     576: invokevirtual 253	com/android/internal/telephony/gsm/GsmCall:getState	()Lcom/android/internal/telephony/Call$State;
        //     579: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     582: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     585: invokestatic 465	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     588: pop
        //     589: aload_0
        //     590: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     593: aload_0
        //     594: getfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     597: invokevirtual 601	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     600: pop
        //     601: aload_0
        //     602: aconst_null
        //     603: putfield 240	com/android/internal/telephony/gsm/GsmCallTracker:pendingMO	Lcom/android/internal/telephony/gsm/GsmConnection;
        //     606: aload_0
        //     607: iconst_0
        //     608: putfield 359	com/android/internal/telephony/gsm/GsmCallTracker:hangupPendingMO	Z
        //     611: aload 4
        //     613: ifnull +12 -> 625
        //     616: aload_0
        //     617: getfield 82	com/android/internal/telephony/gsm/GsmCallTracker:phone	Lcom/android/internal/telephony/gsm/GSMPhone;
        //     620: aload 4
        //     622: invokevirtual 607	com/android/internal/telephony/gsm/GSMPhone:notifyNewRingingConnection	(Lcom/android/internal/telephony/Connection;)V
        //     625: bipush 255
        //     627: aload_0
        //     628: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     631: invokevirtual 434	java/util/ArrayList:size	()I
        //     634: iadd
        //     635: istore 10
        //     637: iload 10
        //     639: iflt +196 -> 835
        //     642: aload_0
        //     643: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     646: iload 10
        //     648: invokevirtual 435	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     651: checkcast 44	com/android/internal/telephony/gsm/GsmConnection
        //     654: astore 11
        //     656: aload 11
        //     658: invokevirtual 610	com/android/internal/telephony/gsm/GsmConnection:isIncoming	()Z
        //     661: ifeq +110 -> 771
        //     664: aload 11
        //     666: invokevirtual 613	com/android/internal/telephony/gsm/GsmConnection:getConnectTime	()J
        //     669: lconst_0
        //     670: lcmp
        //     671: ifne +100 -> 771
        //     674: aload 11
        //     676: getfield 378	com/android/internal/telephony/gsm/GsmConnection:cause	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     679: getstatic 616	com/android/internal/telephony/Connection$DisconnectCause:LOCAL	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     682: if_acmpne +81 -> 763
        //     685: getstatic 619	com/android/internal/telephony/Connection$DisconnectCause:INCOMING_REJECTED	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     688: astore 14
        //     690: aload_0
        //     691: new 106	java/lang/StringBuilder
        //     694: dup
        //     695: invokespecial 107	java/lang/StringBuilder:<init>	()V
        //     698: ldc_w 621
        //     701: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     704: aload 11
        //     706: getfield 378	com/android/internal/telephony/gsm/GsmConnection:cause	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     709: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     712: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     715: invokevirtual 558	com/android/internal/telephony/gsm/GsmCallTracker:log	(Ljava/lang/String;)V
        //     718: aload_0
        //     719: new 106	java/lang/StringBuilder
        //     722: dup
        //     723: invokespecial 107	java/lang/StringBuilder:<init>	()V
        //     726: ldc_w 623
        //     729: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     732: aload 14
        //     734: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     737: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     740: invokevirtual 558	com/android/internal/telephony/gsm/GsmCallTracker:log	(Ljava/lang/String;)V
        //     743: aload_0
        //     744: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     747: iload 10
        //     749: invokevirtual 626	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     752: pop
        //     753: aload 11
        //     755: aload 14
        //     757: invokevirtual 630	com/android/internal/telephony/gsm/GsmConnection:onDisconnect	(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
        //     760: goto +176 -> 936
        //     763: getstatic 633	com/android/internal/telephony/Connection$DisconnectCause:INCOMING_MISSED	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     766: astore 14
        //     768: goto -78 -> 690
        //     771: aload 11
        //     773: getfield 378	com/android/internal/telephony/gsm/GsmConnection:cause	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     776: getstatic 616	com/android/internal/telephony/Connection$DisconnectCause:LOCAL	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     779: if_acmpne +24 -> 803
        //     782: aload_0
        //     783: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     786: iload 10
        //     788: invokevirtual 626	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     791: pop
        //     792: aload 11
        //     794: getstatic 616	com/android/internal/telephony/Connection$DisconnectCause:LOCAL	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     797: invokevirtual 630	com/android/internal/telephony/gsm/GsmConnection:onDisconnect	(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
        //     800: goto +136 -> 936
        //     803: aload 11
        //     805: getfield 378	com/android/internal/telephony/gsm/GsmConnection:cause	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     808: getstatic 375	com/android/internal/telephony/Connection$DisconnectCause:INVALID_NUMBER	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     811: if_acmpne +125 -> 936
        //     814: aload_0
        //     815: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     818: iload 10
        //     820: invokevirtual 626	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     823: pop
        //     824: aload 11
        //     826: getstatic 375	com/android/internal/telephony/Connection$DisconnectCause:INVALID_NUMBER	Lcom/android/internal/telephony/Connection$DisconnectCause;
        //     829: invokevirtual 630	com/android/internal/telephony/gsm/GsmConnection:onDisconnect	(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
        //     832: goto +104 -> 936
        //     835: aload_0
        //     836: getfield 60	com/android/internal/telephony/gsm/GsmCallTracker:droppedDuringPoll	Ljava/util/ArrayList;
        //     839: invokevirtual 434	java/util/ArrayList:size	()I
        //     842: ifle +17 -> 859
        //     845: aload_0
        //     846: getfield 91	com/android/internal/telephony/CallTracker:cm	Lcom/android/internal/telephony/CommandsInterface;
        //     849: aload_0
        //     850: iconst_5
        //     851: invokevirtual 636	com/android/internal/telephony/gsm/GsmCallTracker:obtainNoPollCompleteMessage	(I)Landroid/os/Message;
        //     854: invokeinterface 639 2 0
        //     859: iconst_0
        //     860: ifeq +82 -> 942
        //     863: aload_0
        //     864: invokevirtual 566	com/android/internal/telephony/gsm/GsmCallTracker:pollCallsAfterDelay	()V
        //     867: goto +75 -> 942
        //     870: aload_0
        //     871: invokespecial 323	com/android/internal/telephony/gsm/GsmCallTracker:internalClearDisconnected	()V
        //     874: aload_0
        //     875: invokespecial 325	com/android/internal/telephony/gsm/GsmCallTracker:updatePhoneState	()V
        //     878: iload 6
        //     880: ifeq +75 -> 955
        //     883: aload_0
        //     884: getfield 82	com/android/internal/telephony/gsm/GsmCallTracker:phone	Lcom/android/internal/telephony/gsm/GSMPhone;
        //     887: invokevirtual 642	com/android/internal/telephony/gsm/GSMPhone:notifyUnknownConnection	()V
        //     890: goto +65 -> 955
        //     893: aload_0
        //     894: getfield 82	com/android/internal/telephony/gsm/GsmCallTracker:phone	Lcom/android/internal/telephony/gsm/GSMPhone;
        //     897: invokevirtual 328	com/android/internal/telephony/gsm/GSMPhone:notifyPreciseCallStateChanged	()V
        //     900: goto -691 -> 209
        //     903: iconst_1
        //     904: istore 5
        //     906: iinc 7 1
        //     909: goto -869 -> 40
        //     912: iconst_1
        //     913: istore 6
        //     915: goto -12 -> 903
        //     918: iconst_1
        //     919: istore 5
        //     921: goto -15 -> 906
        //     924: iconst_1
        //     925: istore 5
        //     927: goto -21 -> 906
        //     930: iconst_0
        //     931: istore 5
        //     933: goto -6 -> 927
        //     936: iinc 10 255
        //     939: goto -302 -> 637
        //     942: aload 4
        //     944: ifnonnull -74 -> 870
        //     947: iload 5
        //     949: ifeq -75 -> 874
        //     952: goto -82 -> 870
        //     955: iload 5
        //     957: ifne -64 -> 893
        //     960: aload 4
        //     962: ifnull -753 -> 209
        //     965: goto -72 -> 893
        //
        // Exception table:
        //     from	to	target	type
        //     2	173	241	finally
        //     173	209	241	finally
        //     212	238	241	finally
        //     254	900	241	finally
        //     173	209	252	com/android/internal/telephony/CallStateException
    }

    void hangup(GsmCall paramGsmCall)
        throws CallStateException
    {
        if (paramGsmCall.getConnections().size() == 0)
            throw new CallStateException("no connections in call");
        if (paramGsmCall == this.ringingCall)
        {
            log("(ringing) hangup waiting or background");
            this.cm.hangupWaitingOrBackground(obtainCompleteMessage());
        }
        while (true)
        {
            paramGsmCall.onHangupLocal();
            this.phone.notifyPreciseCallStateChanged();
            return;
            if (paramGsmCall == this.foregroundCall)
            {
                if (paramGsmCall.isDialingOrAlerting())
                {
                    log("(foregnd) hangup dialing or alerting...");
                    hangup((GsmConnection)paramGsmCall.getConnections().get(0));
                }
                else
                {
                    hangupForegroundResumeBackground();
                }
            }
            else
            {
                if (paramGsmCall != this.backgroundCall)
                    break;
                if (this.ringingCall.isRinging())
                {
                    log("hangup all conns in background call");
                    hangupAllConnections(paramGsmCall);
                }
                else
                {
                    hangupWaitingOrBackground();
                }
            }
        }
        throw new RuntimeException("GsmCall " + paramGsmCall + "does not belong to GsmCallTracker " + this);
    }

    void hangup(GsmConnection paramGsmConnection)
        throws CallStateException
    {
        if (paramGsmConnection.owner != this)
            throw new CallStateException("GsmConnection " + paramGsmConnection + "does not belong to GsmCallTracker " + this);
        if (paramGsmConnection == this.pendingMO)
        {
            log("hangup: set hangupPendingMO to true");
            this.hangupPendingMO = true;
        }
        while (true)
        {
            paramGsmConnection.onHangupLocal();
            return;
            try
            {
                this.cm.hangupConnection(paramGsmConnection.getGSMIndex(), obtainCompleteMessage());
            }
            catch (CallStateException localCallStateException)
            {
                Log.w("GSM", "GsmCallTracker WARN: hangup() on absent connection " + paramGsmConnection);
            }
        }
    }

    void hangupAllConnections(GsmCall paramGsmCall)
        throws CallStateException
    {
        try
        {
            int i = paramGsmCall.connections.size();
            for (int j = 0; j < i; j++)
            {
                GsmConnection localGsmConnection = (GsmConnection)paramGsmCall.connections.get(j);
                this.cm.hangupConnection(localGsmConnection.getGSMIndex(), obtainCompleteMessage());
            }
        }
        catch (CallStateException localCallStateException)
        {
            Log.e("GSM", "hangupConnectionByIndex caught " + localCallStateException);
        }
    }

    void hangupConnectionByIndex(GsmCall paramGsmCall, int paramInt)
        throws CallStateException
    {
        int i = paramGsmCall.connections.size();
        for (int j = 0; j < i; j++)
            if (((GsmConnection)paramGsmCall.connections.get(j)).getGSMIndex() == paramInt)
            {
                this.cm.hangupConnection(paramInt, obtainCompleteMessage());
                return;
            }
        throw new CallStateException("no gsm index found");
    }

    void hangupForegroundResumeBackground()
    {
        log("hangupForegroundResumeBackground");
        this.cm.hangupForegroundResumeBackground(obtainCompleteMessage());
    }

    void hangupWaitingOrBackground()
    {
        log("hangupWaitingOrBackground");
        this.cm.hangupWaitingOrBackground(obtainCompleteMessage());
    }

    protected void log(String paramString)
    {
        Log.d("GSM", "[GsmCallTracker] " + paramString);
    }

    public void registerForVoiceCallEnded(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.voiceCallEndedRegistrants.add(localRegistrant);
    }

    public void registerForVoiceCallStarted(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.voiceCallStartedRegistrants.add(localRegistrant);
    }

    void rejectCall()
        throws CallStateException
    {
        if (this.ringingCall.getState().isRinging())
        {
            this.cm.rejectCall(obtainCompleteMessage());
            return;
        }
        throw new CallStateException("phone not ringing");
    }

    void separate(GsmConnection paramGsmConnection)
        throws CallStateException
    {
        if (paramGsmConnection.owner != this)
            throw new CallStateException("GsmConnection " + paramGsmConnection + "does not belong to GsmCallTracker " + this);
        try
        {
            this.cm.separateConnection(paramGsmConnection.getGSMIndex(), obtainCompleteMessage(12));
            return;
        }
        catch (CallStateException localCallStateException)
        {
            while (true)
                Log.w("GSM", "GsmCallTracker WARN: separate() on absent connection " + paramGsmConnection);
        }
    }

    void setMute(boolean paramBoolean)
    {
        this.desiredMute = paramBoolean;
        this.cm.setMute(this.desiredMute, null);
    }

    void switchWaitingOrHoldingAndActive()
        throws CallStateException
    {
        if (this.ringingCall.getState() == Call.State.INCOMING)
            throw new CallStateException("cannot be in the incoming state");
        this.cm.switchWaitingOrHoldingAndActive(obtainCompleteMessage(8));
    }

    public void unregisterForVoiceCallEnded(Handler paramHandler)
    {
        this.voiceCallEndedRegistrants.remove(paramHandler);
    }

    public void unregisterForVoiceCallStarted(Handler paramHandler)
    {
        this.voiceCallStartedRegistrants.remove(paramHandler);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmCallTracker
 * JD-Core Version:        0.6.2
 */