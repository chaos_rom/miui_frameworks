package com.android.internal.telephony.gsm;

import com.android.internal.telephony.IccServiceTable;

public final class UsimServiceTable extends IccServiceTable
{
    public UsimServiceTable(byte[] paramArrayOfByte)
    {
        super(paramArrayOfByte);
    }

    protected String getTag()
    {
        return "UsimServiceTable";
    }

    protected Object[] getValues()
    {
        return UsimService.values();
    }

    public boolean isAvailable(UsimService paramUsimService)
    {
        return super.isAvailable(paramUsimService.ordinal());
    }

    public static enum UsimService
    {
        static
        {
            FDN = new UsimService("FDN", 1);
            FDN_EXTENSION = new UsimService("FDN_EXTENSION", 2);
            SDN = new UsimService("SDN", 3);
            SDN_EXTENSION = new UsimService("SDN_EXTENSION", 4);
            BDN = new UsimService("BDN", 5);
            BDN_EXTENSION = new UsimService("BDN_EXTENSION", 6);
            OUTGOING_CALL_INFO = new UsimService("OUTGOING_CALL_INFO", 7);
            INCOMING_CALL_INFO = new UsimService("INCOMING_CALL_INFO", 8);
            SM_STORAGE = new UsimService("SM_STORAGE", 9);
            SM_STATUS_REPORTS = new UsimService("SM_STATUS_REPORTS", 10);
            SM_SERVICE_PARAMS = new UsimService("SM_SERVICE_PARAMS", 11);
            ADVICE_OF_CHARGE = new UsimService("ADVICE_OF_CHARGE", 12);
            CAP_CONFIG_PARAMS_2 = new UsimService("CAP_CONFIG_PARAMS_2", 13);
            CB_MESSAGE_ID = new UsimService("CB_MESSAGE_ID", 14);
            CB_MESSAGE_ID_RANGES = new UsimService("CB_MESSAGE_ID_RANGES", 15);
            GROUP_ID_LEVEL_1 = new UsimService("GROUP_ID_LEVEL_1", 16);
            GROUP_ID_LEVEL_2 = new UsimService("GROUP_ID_LEVEL_2", 17);
            SPN = new UsimService("SPN", 18);
            USER_PLMN_SELECT = new UsimService("USER_PLMN_SELECT", 19);
            MSISDN = new UsimService("MSISDN", 20);
            IMAGE = new UsimService("IMAGE", 21);
            LOCALISED_SERVICE_AREAS = new UsimService("LOCALISED_SERVICE_AREAS", 22);
            EMLPP = new UsimService("EMLPP", 23);
            EMLPP_AUTO_ANSWER = new UsimService("EMLPP_AUTO_ANSWER", 24);
            RFU = new UsimService("RFU", 25);
            GSM_ACCESS = new UsimService("GSM_ACCESS", 26);
            DATA_DL_VIA_SMS_PP = new UsimService("DATA_DL_VIA_SMS_PP", 27);
            DATA_DL_VIA_SMS_CB = new UsimService("DATA_DL_VIA_SMS_CB", 28);
            CALL_CONTROL_BY_USIM = new UsimService("CALL_CONTROL_BY_USIM", 29);
            MO_SMS_CONTROL_BY_USIM = new UsimService("MO_SMS_CONTROL_BY_USIM", 30);
            RUN_AT_COMMAND = new UsimService("RUN_AT_COMMAND", 31);
            IGNORED_1 = new UsimService("IGNORED_1", 32);
            ENABLED_SERVICES_TABLE = new UsimService("ENABLED_SERVICES_TABLE", 33);
            APN_CONTROL_LIST = new UsimService("APN_CONTROL_LIST", 34);
            DEPERSONALISATION_CONTROL_KEYS = new UsimService("DEPERSONALISATION_CONTROL_KEYS", 35);
            COOPERATIVE_NETWORK_LIST = new UsimService("COOPERATIVE_NETWORK_LIST", 36);
            GSM_SECURITY_CONTEXT = new UsimService("GSM_SECURITY_CONTEXT", 37);
            CPBCCH_INFO = new UsimService("CPBCCH_INFO", 38);
            INVESTIGATION_SCAN = new UsimService("INVESTIGATION_SCAN", 39);
            MEXE = new UsimService("MEXE", 40);
            OPERATOR_PLMN_SELECT = new UsimService("OPERATOR_PLMN_SELECT", 41);
            HPLMN_SELECT = new UsimService("HPLMN_SELECT", 42);
            EXTENSION_5 = new UsimService("EXTENSION_5", 43);
            PLMN_NETWORK_NAME = new UsimService("PLMN_NETWORK_NAME", 44);
            OPERATOR_PLMN_LIST = new UsimService("OPERATOR_PLMN_LIST", 45);
            MBDN = new UsimService("MBDN", 46);
            MWI_STATUS = new UsimService("MWI_STATUS", 47);
            CFI_STATUS = new UsimService("CFI_STATUS", 48);
            IGNORED_2 = new UsimService("IGNORED_2", 49);
            SERVICE_PROVIDER_DISPLAY_INFO = new UsimService("SERVICE_PROVIDER_DISPLAY_INFO", 50);
            MMS_NOTIFICATION = new UsimService("MMS_NOTIFICATION", 51);
            MMS_NOTIFICATION_EXTENSION = new UsimService("MMS_NOTIFICATION_EXTENSION", 52);
            GPRS_CALL_CONTROL_BY_USIM = new UsimService("GPRS_CALL_CONTROL_BY_USIM", 53);
            MMS_CONNECTIVITY_PARAMS = new UsimService("MMS_CONNECTIVITY_PARAMS", 54);
            NETWORK_INDICATION_OF_ALERTING = new UsimService("NETWORK_INDICATION_OF_ALERTING", 55);
            VGCS_GROUP_ID_LIST = new UsimService("VGCS_GROUP_ID_LIST", 56);
            VBS_GROUP_ID_LIST = new UsimService("VBS_GROUP_ID_LIST", 57);
            PSEUDONYM = new UsimService("PSEUDONYM", 58);
            IWLAN_USER_PLMN_SELECT = new UsimService("IWLAN_USER_PLMN_SELECT", 59);
            IWLAN_OPERATOR_PLMN_SELECT = new UsimService("IWLAN_OPERATOR_PLMN_SELECT", 60);
            USER_WSID_LIST = new UsimService("USER_WSID_LIST", 61);
            OPERATOR_WSID_LIST = new UsimService("OPERATOR_WSID_LIST", 62);
            VGCS_SECURITY = new UsimService("VGCS_SECURITY", 63);
            VBS_SECURITY = new UsimService("VBS_SECURITY", 64);
            WLAN_REAUTH_IDENTITY = new UsimService("WLAN_REAUTH_IDENTITY", 65);
            MM_STORAGE = new UsimService("MM_STORAGE", 66);
            GBA = new UsimService("GBA", 67);
            MBMS_SECURITY = new UsimService("MBMS_SECURITY", 68);
            DATA_DL_VIA_USSD = new UsimService("DATA_DL_VIA_USSD", 69);
            EQUIVALENT_HPLMN = new UsimService("EQUIVALENT_HPLMN", 70);
            TERMINAL_PROFILE_AFTER_UICC_ACTIVATION = new UsimService("TERMINAL_PROFILE_AFTER_UICC_ACTIVATION", 71);
            EQUIVALENT_HPLMN_PRESENTATION = new UsimService("EQUIVALENT_HPLMN_PRESENTATION", 72);
            LAST_RPLMN_SELECTION_INDICATION = new UsimService("LAST_RPLMN_SELECTION_INDICATION", 73);
            OMA_BCAST_PROFILE = new UsimService("OMA_BCAST_PROFILE", 74);
            GBA_LOCAL_KEY_ESTABLISHMENT = new UsimService("GBA_LOCAL_KEY_ESTABLISHMENT", 75);
            TERMINAL_APPLICATIONS = new UsimService("TERMINAL_APPLICATIONS", 76);
            SPN_ICON = new UsimService("SPN_ICON", 77);
            PLMN_NETWORK_NAME_ICON = new UsimService("PLMN_NETWORK_NAME_ICON", 78);
            USIM_IP_CONNECTION_PARAMS = new UsimService("USIM_IP_CONNECTION_PARAMS", 79);
            IWLAN_HOME_ID_LIST = new UsimService("IWLAN_HOME_ID_LIST", 80);
            IWLAN_EQUIVALENT_HPLMN_PRESENTATION = new UsimService("IWLAN_EQUIVALENT_HPLMN_PRESENTATION", 81);
            IWLAN_HPLMN_PRIORITY_INDICATION = new UsimService("IWLAN_HPLMN_PRIORITY_INDICATION", 82);
            IWLAN_LAST_REGISTERED_PLMN = new UsimService("IWLAN_LAST_REGISTERED_PLMN", 83);
            EPS_MOBILITY_MANAGEMENT_INFO = new UsimService("EPS_MOBILITY_MANAGEMENT_INFO", 84);
            ALLOWED_CSG_LISTS_AND_INDICATIONS = new UsimService("ALLOWED_CSG_LISTS_AND_INDICATIONS", 85);
            CALL_CONTROL_ON_EPS_PDN_CONNECTION_BY_USIM = new UsimService("CALL_CONTROL_ON_EPS_PDN_CONNECTION_BY_USIM", 86);
            HPLMN_DIRECT_ACCESS = new UsimService("HPLMN_DIRECT_ACCESS", 87);
            ECALL_DATA = new UsimService("ECALL_DATA", 88);
            OPERATOR_CSG_LISTS_AND_INDICATIONS = new UsimService("OPERATOR_CSG_LISTS_AND_INDICATIONS", 89);
            SM_OVER_IP = new UsimService("SM_OVER_IP", 90);
            CSG_DISPLAY_CONTROL = new UsimService("CSG_DISPLAY_CONTROL", 91);
            IMS_COMMUNICATION_CONTROL_BY_USIM = new UsimService("IMS_COMMUNICATION_CONTROL_BY_USIM", 92);
            EXTENDED_TERMINAL_APPLICATIONS = new UsimService("EXTENDED_TERMINAL_APPLICATIONS", 93);
            UICC_ACCESS_TO_IMS = new UsimService("UICC_ACCESS_TO_IMS", 94);
            NAS_CONFIG_BY_USIM = new UsimService("NAS_CONFIG_BY_USIM", 95);
            UsimService[] arrayOfUsimService = new UsimService[96];
            arrayOfUsimService[0] = PHONEBOOK;
            arrayOfUsimService[1] = FDN;
            arrayOfUsimService[2] = FDN_EXTENSION;
            arrayOfUsimService[3] = SDN;
            arrayOfUsimService[4] = SDN_EXTENSION;
            arrayOfUsimService[5] = BDN;
            arrayOfUsimService[6] = BDN_EXTENSION;
            arrayOfUsimService[7] = OUTGOING_CALL_INFO;
            arrayOfUsimService[8] = INCOMING_CALL_INFO;
            arrayOfUsimService[9] = SM_STORAGE;
            arrayOfUsimService[10] = SM_STATUS_REPORTS;
            arrayOfUsimService[11] = SM_SERVICE_PARAMS;
            arrayOfUsimService[12] = ADVICE_OF_CHARGE;
            arrayOfUsimService[13] = CAP_CONFIG_PARAMS_2;
            arrayOfUsimService[14] = CB_MESSAGE_ID;
            arrayOfUsimService[15] = CB_MESSAGE_ID_RANGES;
            arrayOfUsimService[16] = GROUP_ID_LEVEL_1;
            arrayOfUsimService[17] = GROUP_ID_LEVEL_2;
            arrayOfUsimService[18] = SPN;
            arrayOfUsimService[19] = USER_PLMN_SELECT;
            arrayOfUsimService[20] = MSISDN;
            arrayOfUsimService[21] = IMAGE;
            arrayOfUsimService[22] = LOCALISED_SERVICE_AREAS;
            arrayOfUsimService[23] = EMLPP;
            arrayOfUsimService[24] = EMLPP_AUTO_ANSWER;
            arrayOfUsimService[25] = RFU;
            arrayOfUsimService[26] = GSM_ACCESS;
            arrayOfUsimService[27] = DATA_DL_VIA_SMS_PP;
            arrayOfUsimService[28] = DATA_DL_VIA_SMS_CB;
            arrayOfUsimService[29] = CALL_CONTROL_BY_USIM;
            arrayOfUsimService[30] = MO_SMS_CONTROL_BY_USIM;
            arrayOfUsimService[31] = RUN_AT_COMMAND;
            arrayOfUsimService[32] = IGNORED_1;
            arrayOfUsimService[33] = ENABLED_SERVICES_TABLE;
            arrayOfUsimService[34] = APN_CONTROL_LIST;
            arrayOfUsimService[35] = DEPERSONALISATION_CONTROL_KEYS;
            arrayOfUsimService[36] = COOPERATIVE_NETWORK_LIST;
            arrayOfUsimService[37] = GSM_SECURITY_CONTEXT;
            arrayOfUsimService[38] = CPBCCH_INFO;
            arrayOfUsimService[39] = INVESTIGATION_SCAN;
            arrayOfUsimService[40] = MEXE;
            arrayOfUsimService[41] = OPERATOR_PLMN_SELECT;
            arrayOfUsimService[42] = HPLMN_SELECT;
            arrayOfUsimService[43] = EXTENSION_5;
            arrayOfUsimService[44] = PLMN_NETWORK_NAME;
            arrayOfUsimService[45] = OPERATOR_PLMN_LIST;
            arrayOfUsimService[46] = MBDN;
            arrayOfUsimService[47] = MWI_STATUS;
            arrayOfUsimService[48] = CFI_STATUS;
            arrayOfUsimService[49] = IGNORED_2;
            arrayOfUsimService[50] = SERVICE_PROVIDER_DISPLAY_INFO;
            arrayOfUsimService[51] = MMS_NOTIFICATION;
            arrayOfUsimService[52] = MMS_NOTIFICATION_EXTENSION;
            arrayOfUsimService[53] = GPRS_CALL_CONTROL_BY_USIM;
            arrayOfUsimService[54] = MMS_CONNECTIVITY_PARAMS;
            arrayOfUsimService[55] = NETWORK_INDICATION_OF_ALERTING;
            arrayOfUsimService[56] = VGCS_GROUP_ID_LIST;
            arrayOfUsimService[57] = VBS_GROUP_ID_LIST;
            arrayOfUsimService[58] = PSEUDONYM;
            arrayOfUsimService[59] = IWLAN_USER_PLMN_SELECT;
            arrayOfUsimService[60] = IWLAN_OPERATOR_PLMN_SELECT;
            arrayOfUsimService[61] = USER_WSID_LIST;
            arrayOfUsimService[62] = OPERATOR_WSID_LIST;
            arrayOfUsimService[63] = VGCS_SECURITY;
            arrayOfUsimService[64] = VBS_SECURITY;
            arrayOfUsimService[65] = WLAN_REAUTH_IDENTITY;
            arrayOfUsimService[66] = MM_STORAGE;
            arrayOfUsimService[67] = GBA;
            arrayOfUsimService[68] = MBMS_SECURITY;
            arrayOfUsimService[69] = DATA_DL_VIA_USSD;
            arrayOfUsimService[70] = EQUIVALENT_HPLMN;
            arrayOfUsimService[71] = TERMINAL_PROFILE_AFTER_UICC_ACTIVATION;
            arrayOfUsimService[72] = EQUIVALENT_HPLMN_PRESENTATION;
            arrayOfUsimService[73] = LAST_RPLMN_SELECTION_INDICATION;
            arrayOfUsimService[74] = OMA_BCAST_PROFILE;
            arrayOfUsimService[75] = GBA_LOCAL_KEY_ESTABLISHMENT;
            arrayOfUsimService[76] = TERMINAL_APPLICATIONS;
            arrayOfUsimService[77] = SPN_ICON;
            arrayOfUsimService[78] = PLMN_NETWORK_NAME_ICON;
            arrayOfUsimService[79] = USIM_IP_CONNECTION_PARAMS;
            arrayOfUsimService[80] = IWLAN_HOME_ID_LIST;
            arrayOfUsimService[81] = IWLAN_EQUIVALENT_HPLMN_PRESENTATION;
            arrayOfUsimService[82] = IWLAN_HPLMN_PRIORITY_INDICATION;
            arrayOfUsimService[83] = IWLAN_LAST_REGISTERED_PLMN;
            arrayOfUsimService[84] = EPS_MOBILITY_MANAGEMENT_INFO;
            arrayOfUsimService[85] = ALLOWED_CSG_LISTS_AND_INDICATIONS;
            arrayOfUsimService[86] = CALL_CONTROL_ON_EPS_PDN_CONNECTION_BY_USIM;
            arrayOfUsimService[87] = HPLMN_DIRECT_ACCESS;
            arrayOfUsimService[88] = ECALL_DATA;
            arrayOfUsimService[89] = OPERATOR_CSG_LISTS_AND_INDICATIONS;
            arrayOfUsimService[90] = SM_OVER_IP;
            arrayOfUsimService[91] = CSG_DISPLAY_CONTROL;
            arrayOfUsimService[92] = IMS_COMMUNICATION_CONTROL_BY_USIM;
            arrayOfUsimService[93] = EXTENDED_TERMINAL_APPLICATIONS;
            arrayOfUsimService[94] = UICC_ACCESS_TO_IMS;
            arrayOfUsimService[95] = NAS_CONFIG_BY_USIM;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.UsimServiceTable
 * JD-Core Version:        0.6.2
 */