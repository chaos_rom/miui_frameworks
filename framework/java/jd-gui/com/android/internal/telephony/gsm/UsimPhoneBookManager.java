package com.android.internal.telephony.gsm;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.AdnRecord;
import com.android.internal.telephony.AdnRecordCache;
import com.android.internal.telephony.IccConstants;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UsimPhoneBookManager extends Handler
    implements IccConstants
{
    private static final boolean DBG = true;
    private static final int EVENT_EMAIL_LOAD_DONE = 4;
    private static final int EVENT_IAP_LOAD_DONE = 3;
    private static final int EVENT_PBR_LOAD_DONE = 1;
    private static final int EVENT_USIM_ADN_LOAD_DONE = 2;
    private static final String LOG_TAG = "GSM";
    private static final int USIM_EFAAS_TAG = 199;
    private static final int USIM_EFADN_TAG = 192;
    private static final int USIM_EFANR_TAG = 196;
    private static final int USIM_EFCCP1_TAG = 203;
    private static final int USIM_EFEMAIL_TAG = 202;
    private static final int USIM_EFEXT1_TAG = 194;
    private static final int USIM_EFGRP_TAG = 198;
    private static final int USIM_EFGSD_TAG = 200;
    private static final int USIM_EFIAP_TAG = 193;
    private static final int USIM_EFPBC_TAG = 197;
    private static final int USIM_EFSNE_TAG = 195;
    private static final int USIM_EFUID_TAG = 201;
    private static final int USIM_TYPE1_TAG = 168;
    private static final int USIM_TYPE2_TAG = 169;
    private static final int USIM_TYPE3_TAG = 170;
    private AdnRecordCache mAdnCache;
    private ArrayList<byte[]> mEmailFileRecord;
    private boolean mEmailPresentInIap = false;
    private int mEmailTagNumberInIap = 0;
    private Map<Integer, ArrayList<String>> mEmailsForAdnRec;
    private IccFileHandler mFh;
    private ArrayList<byte[]> mIapFileRecord;
    private Boolean mIsPbrPresent;
    private Object mLock = new Object();
    private PbrFile mPbrFile;
    private ArrayList<AdnRecord> mPhoneBookRecords;
    private boolean mRefreshCache = false;

    public UsimPhoneBookManager(IccFileHandler paramIccFileHandler, AdnRecordCache paramAdnRecordCache)
    {
        this.mFh = paramIccFileHandler;
        this.mPhoneBookRecords = new ArrayList();
        this.mPbrFile = null;
        this.mIsPbrPresent = Boolean.valueOf(true);
        this.mAdnCache = paramAdnRecordCache;
    }

    private void createPbrFile(ArrayList<byte[]> paramArrayList)
    {
        if (paramArrayList == null)
        {
            this.mPbrFile = null;
            this.mIsPbrPresent = Boolean.valueOf(false);
        }
        while (true)
        {
            return;
            this.mPbrFile = new PbrFile(paramArrayList);
        }
    }

    private void log(String paramString)
    {
        Log.d("GSM", paramString);
    }

    private void readAdnFileAndWait(int paramInt)
    {
        Map localMap = (Map)this.mPbrFile.mFileIds.get(Integer.valueOf(paramInt));
        if ((localMap == null) || (localMap.isEmpty()));
        while (true)
        {
            return;
            int i = 0;
            if (localMap.containsKey(Integer.valueOf(194)))
                i = ((Integer)localMap.get(Integer.valueOf(194))).intValue();
            this.mAdnCache.requestLoadAllAdnLike(((Integer)localMap.get(Integer.valueOf(192))).intValue(), i, obtainMessage(2));
            try
            {
                this.mLock.wait();
            }
            catch (InterruptedException localInterruptedException)
            {
                Log.e("GSM", "Interrupted Exception in readAdnFileAndWait");
            }
        }
    }

    private void readEmailFileAndWait(int paramInt)
    {
        Map localMap = (Map)this.mPbrFile.mFileIds.get(Integer.valueOf(paramInt));
        if (localMap == null);
        while (true)
        {
            return;
            if (localMap.containsKey(Integer.valueOf(202)))
            {
                ((Integer)localMap.get(Integer.valueOf(202))).intValue();
                if (this.mEmailPresentInIap)
                {
                    readIapFileAndWait(((Integer)localMap.get(Integer.valueOf(193))).intValue());
                    if (this.mIapFileRecord == null)
                        Log.e("GSM", "Error: IAP file is empty");
                }
                else
                {
                    this.mFh.loadEFLinearFixedAll(((Integer)localMap.get(Integer.valueOf(202))).intValue(), obtainMessage(4));
                    try
                    {
                        this.mLock.wait();
                        if (this.mEmailFileRecord == null)
                            Log.e("GSM", "Error: Email file is empty");
                    }
                    catch (InterruptedException localInterruptedException)
                    {
                        while (true)
                            Log.e("GSM", "Interrupted Exception in readEmailFileAndWait");
                        updatePhoneAdnRecord();
                    }
                }
            }
        }
    }

    private String readEmailRecord(int paramInt)
    {
        try
        {
            byte[] arrayOfByte = (byte[])this.mEmailFileRecord.get(paramInt);
            str = IccUtils.adnStringFieldToString(arrayOfByte, 0, -2 + arrayOfByte.length);
            return str;
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
            while (true)
                String str = null;
        }
    }

    private void readIapFileAndWait(int paramInt)
    {
        this.mFh.loadEFLinearFixedAll(paramInt, obtainMessage(3));
        try
        {
            this.mLock.wait();
            return;
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
                Log.e("GSM", "Interrupted Exception in readIapFileAndWait");
        }
    }

    private void readPbrFileAndWait()
    {
        this.mFh.loadEFLinearFixedAll(20272, obtainMessage(1));
        try
        {
            this.mLock.wait();
            return;
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
                Log.e("GSM", "Interrupted Exception in readAdnFileAndWait");
        }
    }

    private void refreshCache()
    {
        if (this.mPbrFile == null);
        while (true)
        {
            return;
            this.mPhoneBookRecords.clear();
            int i = this.mPbrFile.mFileIds.size();
            for (int j = 0; j < i; j++)
                readAdnFileAndWait(j);
        }
    }

    private void updatePhoneAdnRecord()
    {
        if (this.mEmailFileRecord == null);
        int i;
        String[] arrayOfString2;
        AdnRecord localAdnRecord2;
        int k;
        label156: 
        do
        {
            return;
            i = this.mPhoneBookRecords.size();
            if (this.mIapFileRecord != null)
            {
                int m = 0;
                while (m < i)
                    try
                    {
                        byte[] arrayOfByte = (byte[])this.mIapFileRecord.get(m);
                        int n = arrayOfByte[this.mEmailTagNumberInIap];
                        if (n != -1)
                        {
                            arrayOfString2 = new String[1];
                            arrayOfString2[0] = readEmailRecord(n - 1);
                            localAdnRecord2 = (AdnRecord)this.mPhoneBookRecords.get(m);
                            if (localAdnRecord2 == null)
                                break label190;
                            localAdnRecord2.setEmails(arrayOfString2);
                            this.mPhoneBookRecords.set(m, localAdnRecord2);
                        }
                        m++;
                    }
                    catch (IndexOutOfBoundsException localIndexOutOfBoundsException2)
                    {
                        Log.e("GSM", "Error: Improper ICC card: No IAP record for ADN, continuing");
                    }
            }
            int j = this.mPhoneBookRecords.size();
            if (this.mEmailsForAdnRec == null)
                parseType1EmailFile(j);
            k = 0;
        }
        while (k >= i);
        while (true)
        {
            ArrayList localArrayList;
            try
            {
                localArrayList = (ArrayList)this.mEmailsForAdnRec.get(Integer.valueOf(k));
                if (localArrayList != null)
                    break label213;
                k++;
                break label156;
                label190: localAdnRecord2 = new AdnRecord("", "", arrayOfString2);
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException1)
            {
            }
            break;
            label213: AdnRecord localAdnRecord1 = (AdnRecord)this.mPhoneBookRecords.get(k);
            String[] arrayOfString1 = new String[localArrayList.size()];
            System.arraycopy(localArrayList.toArray(), 0, arrayOfString1, 0, localArrayList.size());
            localAdnRecord1.setEmails(arrayOfString1);
            this.mPhoneBookRecords.set(k, localAdnRecord1);
        }
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return;
            AsyncResult localAsyncResult4 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult4.exception == null)
                createPbrFile((ArrayList)localAsyncResult4.result);
            synchronized (this.mLock)
            {
                this.mLock.notify();
            }
            log("Loading USIM ADN records done");
            AsyncResult localAsyncResult3 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult3.exception == null)
                this.mPhoneBookRecords.addAll((ArrayList)localAsyncResult3.result);
            synchronized (this.mLock)
            {
                this.mLock.notify();
            }
            log("Loading USIM IAP records done");
            AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult2.exception == null)
                this.mIapFileRecord = ((ArrayList)localAsyncResult2.result);
            synchronized (this.mLock)
            {
                this.mLock.notify();
            }
            log("Loading USIM Email records done");
            AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult1.exception == null)
                this.mEmailFileRecord = ((ArrayList)localAsyncResult1.result);
            synchronized (this.mLock)
            {
                this.mLock.notify();
            }
        }
    }

    public void invalidateCache()
    {
        this.mRefreshCache = true;
    }

    // ERROR //
    public ArrayList<AdnRecord> loadEfFilesFromUsim()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: aload_0
        //     3: getfield 86	com/android/internal/telephony/gsm/UsimPhoneBookManager:mLock	Ljava/lang/Object;
        //     6: astore_2
        //     7: aload_2
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 99	com/android/internal/telephony/gsm/UsimPhoneBookManager:mPhoneBookRecords	Ljava/util/ArrayList;
        //     13: invokevirtual 307	java/util/ArrayList:isEmpty	()Z
        //     16: ifne +29 -> 45
        //     19: aload_0
        //     20: getfield 92	com/android/internal/telephony/gsm/UsimPhoneBookManager:mRefreshCache	Z
        //     23: ifeq +12 -> 35
        //     26: aload_0
        //     27: iconst_0
        //     28: putfield 92	com/android/internal/telephony/gsm/UsimPhoneBookManager:mRefreshCache	Z
        //     31: aload_0
        //     32: invokespecial 309	com/android/internal/telephony/gsm/UsimPhoneBookManager:refreshCache	()V
        //     35: aload_0
        //     36: getfield 99	com/android/internal/telephony/gsm/UsimPhoneBookManager:mPhoneBookRecords	Ljava/util/ArrayList;
        //     39: astore_1
        //     40: aload_2
        //     41: monitorexit
        //     42: goto +93 -> 135
        //     45: aload_0
        //     46: getfield 109	com/android/internal/telephony/gsm/UsimPhoneBookManager:mIsPbrPresent	Ljava/lang/Boolean;
        //     49: invokevirtual 312	java/lang/Boolean:booleanValue	()Z
        //     52: ifne +13 -> 65
        //     55: aload_2
        //     56: monitorexit
        //     57: goto +78 -> 135
        //     60: astore_3
        //     61: aload_2
        //     62: monitorexit
        //     63: aload_3
        //     64: athrow
        //     65: aload_0
        //     66: getfield 101	com/android/internal/telephony/gsm/UsimPhoneBookManager:mPbrFile	Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;
        //     69: ifnonnull +7 -> 76
        //     72: aload_0
        //     73: invokespecial 314	com/android/internal/telephony/gsm/UsimPhoneBookManager:readPbrFileAndWait	()V
        //     76: aload_0
        //     77: getfield 101	com/android/internal/telephony/gsm/UsimPhoneBookManager:mPbrFile	Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;
        //     80: ifnonnull +8 -> 88
        //     83: aload_2
        //     84: monitorexit
        //     85: goto +50 -> 135
        //     88: aload_0
        //     89: getfield 101	com/android/internal/telephony/gsm/UsimPhoneBookManager:mPbrFile	Lcom/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile;
        //     92: getfield 136	com/android/internal/telephony/gsm/UsimPhoneBookManager$PbrFile:mFileIds	Ljava/util/HashMap;
        //     95: invokevirtual 228	java/util/HashMap:size	()I
        //     98: istore 4
        //     100: iconst_0
        //     101: istore 5
        //     103: iload 5
        //     105: iload 4
        //     107: if_icmpge +21 -> 128
        //     110: aload_0
        //     111: iload 5
        //     113: invokespecial 230	com/android/internal/telephony/gsm/UsimPhoneBookManager:readAdnFileAndWait	(I)V
        //     116: aload_0
        //     117: iload 5
        //     119: invokespecial 316	com/android/internal/telephony/gsm/UsimPhoneBookManager:readEmailFileAndWait	(I)V
        //     122: iinc 5 1
        //     125: goto -22 -> 103
        //     128: aload_2
        //     129: monitorexit
        //     130: aload_0
        //     131: getfield 99	com/android/internal/telephony/gsm/UsimPhoneBookManager:mPhoneBookRecords	Ljava/util/ArrayList;
        //     134: astore_1
        //     135: aload_1
        //     136: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     9	63	60	finally
        //     65	130	60	finally
    }

    void parseType1EmailFile(int paramInt)
    {
        this.mEmailsForAdnRec = new HashMap();
        int i = 0;
        if (i < paramInt);
        while (true)
        {
            int j;
            try
            {
                byte[] arrayOfByte = (byte[])this.mEmailFileRecord.get(i);
                j = arrayOfByte[(-1 + arrayOfByte.length)];
                if (j != -1)
                    break label66;
                i++;
            }
            catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
            {
                Log.e("GSM", "Error: Improper ICC card: No email record for ADN, continuing");
            }
            return;
            label66: String str = readEmailRecord(i);
            if ((str != null) && (!str.equals("")))
            {
                ArrayList localArrayList = (ArrayList)this.mEmailsForAdnRec.get(Integer.valueOf(j - 1));
                if (localArrayList == null)
                    localArrayList = new ArrayList();
                localArrayList.add(str);
                this.mEmailsForAdnRec.put(Integer.valueOf(j - 1), localArrayList);
            }
        }
    }

    public void reset()
    {
        this.mPhoneBookRecords.clear();
        this.mIapFileRecord = null;
        this.mEmailFileRecord = null;
        this.mPbrFile = null;
        this.mIsPbrPresent = Boolean.valueOf(true);
        this.mRefreshCache = false;
    }

    private class PbrFile
    {
        HashMap<Integer, Map<Integer, Integer>> mFileIds = new HashMap();

        PbrFile()
        {
            int i = 0;
            Object localObject;
            Iterator localIterator = localObject.iterator();
            while (localIterator.hasNext())
            {
                byte[] arrayOfByte = (byte[])localIterator.next();
                parseTag(new SimTlv(arrayOfByte, 0, arrayOfByte.length), i);
                i++;
            }
        }

        void parseEf(SimTlv paramSimTlv, Map<Integer, Integer> paramMap, int paramInt)
        {
            int i = 0;
            int j = paramSimTlv.getTag();
            if ((paramInt == 169) && (j == 202))
            {
                UsimPhoneBookManager.access$002(UsimPhoneBookManager.this, true);
                UsimPhoneBookManager.access$102(UsimPhoneBookManager.this, i);
            }
            switch (j)
            {
            default:
            case 192:
            case 193:
            case 194:
            case 195:
            case 196:
            case 197:
            case 198:
            case 199:
            case 200:
            case 201:
            case 202:
            case 203:
            }
            while (true)
            {
                i++;
                if (paramSimTlv.nextObject())
                    break;
                return;
                byte[] arrayOfByte = paramSimTlv.getData();
                int k = (0xFF & arrayOfByte[0]) << 8 | 0xFF & arrayOfByte[1];
                paramMap.put(Integer.valueOf(j), Integer.valueOf(k));
            }
        }

        void parseTag(SimTlv paramSimTlv, int paramInt)
        {
            HashMap localHashMap = new HashMap();
            while (true)
            {
                int i = paramSimTlv.getTag();
                switch (i)
                {
                default:
                case 168:
                case 169:
                case 170:
                }
                while (!paramSimTlv.nextObject())
                {
                    this.mFileIds.put(Integer.valueOf(paramInt), localHashMap);
                    return;
                    byte[] arrayOfByte = paramSimTlv.getData();
                    parseEf(new SimTlv(arrayOfByte, 0, arrayOfByte.length), localHashMap, i);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.UsimPhoneBookManager
 * JD-Core Version:        0.6.2
 */