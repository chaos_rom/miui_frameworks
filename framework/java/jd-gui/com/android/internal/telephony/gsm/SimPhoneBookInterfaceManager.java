package com.android.internal.telephony.gsm;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccPhoneBookInterfaceManager;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.PhoneBase;
import java.util.concurrent.atomic.AtomicBoolean;

public class SimPhoneBookInterfaceManager extends IccPhoneBookInterfaceManager
{
    static final String LOG_TAG = "GSM";

    public SimPhoneBookInterfaceManager(GSMPhone paramGSMPhone)
    {
        super(paramGSMPhone);
        this.adnCache = paramGSMPhone.mIccRecords.getAdnCache();
    }

    public void dispose()
    {
        super.dispose();
    }

    protected void finalize()
    {
        try
        {
            super.finalize();
            Log.d("GSM", "SimPhoneBookInterfaceManager finalized");
            return;
        }
        catch (Throwable localThrowable)
        {
            while (true)
                Log.e("GSM", "Error while finalizing:", localThrowable);
        }
    }

    public int[] getAdnRecordsSize(int paramInt)
    {
        logd("getAdnRecordsSize: efid=" + paramInt);
        synchronized (this.mLock)
        {
            checkThread();
            this.recordSize = new int[3];
            AtomicBoolean localAtomicBoolean = new AtomicBoolean(false);
            Message localMessage = this.mBaseHandler.obtainMessage(1, localAtomicBoolean);
            this.phone.getIccFileHandler().getEFLinearRecordSize(paramInt, localMessage);
            waitForResult(localAtomicBoolean);
            return this.recordSize;
        }
    }

    protected void logd(String paramString)
    {
        Log.d("GSM", "[SimPbInterfaceManager] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("GSM", "[SimPbInterfaceManager] " + paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.SimPhoneBookInterfaceManager
 * JD-Core Version:        0.6.2
 */