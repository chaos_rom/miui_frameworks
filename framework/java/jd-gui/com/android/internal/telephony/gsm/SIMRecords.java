package com.android.internal.telephony.gsm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.os.AsyncResult;
import android.os.Message;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.AdnRecord;
import com.android.internal.telephony.AdnRecordCache;
import com.android.internal.telephony.AdnRecordLoader;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.IccRefreshResponse;
import com.android.internal.telephony.IccUtils;
import com.android.internal.telephony.IccVmFixedException;
import com.android.internal.telephony.IccVmNotSupportedException;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.SmsMessageBase;
import java.util.ArrayList;
import java.util.Iterator;

public class SIMRecords extends IccRecords
{
    static final int CFF_LINE1_MASK = 15;
    static final int CFF_LINE1_RESET = 240;
    static final int CFF_UNCONDITIONAL_ACTIVE = 10;
    static final int CFF_UNCONDITIONAL_DEACTIVE = 5;
    private static final int CPHS_SST_MBN_ENABLED = 48;
    private static final int CPHS_SST_MBN_MASK = 48;
    private static final boolean CRASH_RIL = false;
    protected static final boolean DBG = true;
    protected static final int EVENT_GET_AD_DONE = 9;
    private static final int EVENT_GET_ALL_SMS_DONE = 18;
    private static final int EVENT_GET_CFF_DONE = 24;
    private static final int EVENT_GET_CFIS_DONE = 32;
    private static final int EVENT_GET_CPHS_MAILBOX_DONE = 11;
    private static final int EVENT_GET_CSP_CPHS_DONE = 33;
    protected static final int EVENT_GET_ICCID_DONE = 4;
    protected static final int EVENT_GET_IMSI_DONE = 3;
    private static final int EVENT_GET_INFO_CPHS_DONE = 26;
    private static final int EVENT_GET_MBDN_DONE = 6;
    private static final int EVENT_GET_MBI_DONE = 5;
    protected static final int EVENT_GET_MSISDN_DONE = 10;
    private static final int EVENT_GET_MWIS_DONE = 7;
    private static final int EVENT_GET_PNN_DONE = 15;
    private static final int EVENT_GET_SMS_DONE = 22;
    private static final int EVENT_GET_SPDI_DONE = 13;
    private static final int EVENT_GET_SPN_DONE = 12;
    protected static final int EVENT_GET_SST_DONE = 17;
    private static final int EVENT_GET_VOICE_MAIL_INDICATOR_CPHS_DONE = 8;
    private static final int EVENT_MARK_SMS_READ_DONE = 19;
    private static final int EVENT_RADIO_OFF_OR_NOT_AVAILABLE = 2;
    private static final int EVENT_SET_CPHS_MAILBOX_DONE = 25;
    private static final int EVENT_SET_MBDN_DONE = 20;
    private static final int EVENT_SET_MSISDN_DONE = 30;
    private static final int EVENT_SIM_REFRESH = 31;
    private static final int EVENT_SMS_ON_SIM = 21;
    private static final int EVENT_UPDATE_DONE = 14;
    protected static final String LOG_TAG = "GSM";
    private static final String[] MCCMNC_CODES_HAVING_3DIGITS_MNC = arrayOfString;
    static final int SPN_RULE_SHOW_PLMN = 2;
    static final int SPN_RULE_SHOW_SPN = 1;
    static final int TAG_FULL_NETWORK_NAME = 67;
    static final int TAG_SHORT_NETWORK_NAME = 69;
    static final int TAG_SPDI = 163;
    static final int TAG_SPDI_PLMN_LIST = 128;
    private boolean callForwardingEnabled;
    byte[] efCPHS_MWI = null;
    byte[] efMWIS = null;
    private String imsi;
    private byte[] mCphsInfo = null;
    boolean mCspPlmnEnabled = true;
    byte[] mEfCff = null;
    byte[] mEfCfis = null;
    SpnOverride mSpnOverride;
    UsimServiceTable mUsimServiceTable;
    VoiceMailConstants mVmConfig;
    String pnnHomeName = null;
    ArrayList<String> spdiNetworks = null;
    int spnDisplayCondition;
    private Get_Spn_Fsm_State spnState;

    static
    {
        String[] arrayOfString = new String[122];
        arrayOfString[0] = "405025";
        arrayOfString[1] = "405026";
        arrayOfString[2] = "405027";
        arrayOfString[3] = "405028";
        arrayOfString[4] = "405029";
        arrayOfString[5] = "405030";
        arrayOfString[6] = "405031";
        arrayOfString[7] = "405032";
        arrayOfString[8] = "405033";
        arrayOfString[9] = "405034";
        arrayOfString[10] = "405035";
        arrayOfString[11] = "405036";
        arrayOfString[12] = "405037";
        arrayOfString[13] = "405038";
        arrayOfString[14] = "405039";
        arrayOfString[15] = "405040";
        arrayOfString[16] = "405041";
        arrayOfString[17] = "405042";
        arrayOfString[18] = "405043";
        arrayOfString[19] = "405044";
        arrayOfString[20] = "405045";
        arrayOfString[21] = "405046";
        arrayOfString[22] = "405047";
        arrayOfString[23] = "405750";
        arrayOfString[24] = "405751";
        arrayOfString[25] = "405752";
        arrayOfString[26] = "405753";
        arrayOfString[27] = "405754";
        arrayOfString[28] = "405755";
        arrayOfString[29] = "405756";
        arrayOfString[30] = "405799";
        arrayOfString[31] = "405800";
        arrayOfString[32] = "405801";
        arrayOfString[33] = "405802";
        arrayOfString[34] = "405803";
        arrayOfString[35] = "405804";
        arrayOfString[36] = "405805";
        arrayOfString[37] = "405806";
        arrayOfString[38] = "405807";
        arrayOfString[39] = "405808";
        arrayOfString[40] = "405809";
        arrayOfString[41] = "405810";
        arrayOfString[42] = "405811";
        arrayOfString[43] = "405812";
        arrayOfString[44] = "405813";
        arrayOfString[45] = "405814";
        arrayOfString[46] = "405815";
        arrayOfString[47] = "405816";
        arrayOfString[48] = "405817";
        arrayOfString[49] = "405818";
        arrayOfString[50] = "405819";
        arrayOfString[51] = "405820";
        arrayOfString[52] = "405821";
        arrayOfString[53] = "405822";
        arrayOfString[54] = "405823";
        arrayOfString[55] = "405824";
        arrayOfString[56] = "405825";
        arrayOfString[57] = "405826";
        arrayOfString[58] = "405827";
        arrayOfString[59] = "405828";
        arrayOfString[60] = "405829";
        arrayOfString[61] = "405830";
        arrayOfString[62] = "405831";
        arrayOfString[63] = "405832";
        arrayOfString[64] = "405833";
        arrayOfString[65] = "405834";
        arrayOfString[66] = "405835";
        arrayOfString[67] = "405836";
        arrayOfString[68] = "405837";
        arrayOfString[69] = "405838";
        arrayOfString[70] = "405839";
        arrayOfString[71] = "405840";
        arrayOfString[72] = "405841";
        arrayOfString[73] = "405842";
        arrayOfString[74] = "405843";
        arrayOfString[75] = "405844";
        arrayOfString[76] = "405845";
        arrayOfString[77] = "405846";
        arrayOfString[78] = "405847";
        arrayOfString[79] = "405848";
        arrayOfString[80] = "405849";
        arrayOfString[81] = "405850";
        arrayOfString[82] = "405851";
        arrayOfString[83] = "405852";
        arrayOfString[84] = "405853";
        arrayOfString[85] = "405875";
        arrayOfString[86] = "405876";
        arrayOfString[87] = "405877";
        arrayOfString[88] = "405878";
        arrayOfString[89] = "405879";
        arrayOfString[90] = "405880";
        arrayOfString[91] = "405881";
        arrayOfString[92] = "405882";
        arrayOfString[93] = "405883";
        arrayOfString[94] = "405884";
        arrayOfString[95] = "405885";
        arrayOfString[96] = "405886";
        arrayOfString[97] = "405908";
        arrayOfString[98] = "405909";
        arrayOfString[99] = "405910";
        arrayOfString[100] = "405911";
        arrayOfString[101] = "405912";
        arrayOfString[102] = "405913";
        arrayOfString[103] = "405914";
        arrayOfString[104] = "405915";
        arrayOfString[105] = "405916";
        arrayOfString[106] = "405917";
        arrayOfString[107] = "405918";
        arrayOfString[108] = "405919";
        arrayOfString[109] = "405920";
        arrayOfString[110] = "405921";
        arrayOfString[111] = "405922";
        arrayOfString[112] = "405923";
        arrayOfString[113] = "405924";
        arrayOfString[114] = "405925";
        arrayOfString[115] = "405926";
        arrayOfString[116] = "405927";
        arrayOfString[117] = "405928";
        arrayOfString[118] = "405929";
        arrayOfString[119] = "405930";
        arrayOfString[120] = "405931";
        arrayOfString[121] = "405932";
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public SIMRecords(IccCard paramIccCard, Context paramContext, CommandsInterface paramCommandsInterface)
    {
        super(paramIccCard, paramContext, paramCommandsInterface);
        this.adnCache = new AdnRecordCache(this.mFh);
        this.mVmConfig = new VoiceMailConstants();
        this.mSpnOverride = new MiuiSpnOverrideImpl();
        this.recordsRequested = false;
        this.recordsToLoad = 0;
        this.mCi.registerForOffOrNotAvailable(this, 2, null);
        this.mCi.setOnSmsOnSim(this, 21, null);
        this.mCi.registerForIccRefresh(this, 31, null);
        onRadioOffOrNotAvailable();
    }

    private void getSpnFsm(boolean paramBoolean, AsyncResult paramAsyncResult)
    {
        if (paramBoolean)
            if ((this.spnState == Get_Spn_Fsm_State.READ_SPN_3GPP) || (this.spnState == Get_Spn_Fsm_State.READ_SPN_CPHS) || (this.spnState == Get_Spn_Fsm_State.READ_SPN_SHORT_CPHS) || (this.spnState == Get_Spn_Fsm_State.INIT))
                this.spnState = Get_Spn_Fsm_State.INIT;
        while (true)
        {
            return;
            this.spnState = Get_Spn_Fsm_State.INIT;
            switch (1.$SwitchMap$com$android$internal$telephony$gsm$SIMRecords$Get_Spn_Fsm_State[this.spnState.ordinal()])
            {
            default:
                this.spnState = Get_Spn_Fsm_State.IDLE;
                break;
            case 1:
                this.spn = null;
                this.mFh.loadEFTransparent(28486, obtainMessage(12));
                this.recordsToLoad = (1 + this.recordsToLoad);
                this.spnState = Get_Spn_Fsm_State.READ_SPN_3GPP;
                break;
            case 2:
                if ((paramAsyncResult != null) && (paramAsyncResult.exception == null))
                {
                    byte[] arrayOfByte3 = (byte[])paramAsyncResult.result;
                    this.spnDisplayCondition = (0xFF & arrayOfByte3[0]);
                    this.spn = IccUtils.adnStringFieldToString(arrayOfByte3, 1, -1 + arrayOfByte3.length);
                    log("Load EF_SPN: " + this.spn + " spnDisplayCondition: " + this.spnDisplayCondition);
                    SystemProperties.set("gsm.sim.operator.alpha", this.spn);
                    this.spnState = Get_Spn_Fsm_State.IDLE;
                }
                else
                {
                    this.mFh.loadEFTransparent(28436, obtainMessage(12));
                    this.recordsToLoad = (1 + this.recordsToLoad);
                    this.spnState = Get_Spn_Fsm_State.READ_SPN_CPHS;
                    this.spnDisplayCondition = -1;
                }
                break;
            case 3:
                if ((paramAsyncResult != null) && (paramAsyncResult.exception == null))
                {
                    byte[] arrayOfByte2 = (byte[])paramAsyncResult.result;
                    this.spn = IccUtils.adnStringFieldToString(arrayOfByte2, 0, -1 + arrayOfByte2.length);
                    log("Load EF_SPN_CPHS: " + this.spn);
                    SystemProperties.set("gsm.sim.operator.alpha", this.spn);
                    this.spnState = Get_Spn_Fsm_State.IDLE;
                }
                else
                {
                    this.mFh.loadEFTransparent(28440, obtainMessage(12));
                    this.recordsToLoad = (1 + this.recordsToLoad);
                    this.spnState = Get_Spn_Fsm_State.READ_SPN_SHORT_CPHS;
                }
                break;
            case 4:
            }
        }
        if ((paramAsyncResult != null) && (paramAsyncResult.exception == null))
        {
            byte[] arrayOfByte1 = (byte[])paramAsyncResult.result;
            this.spn = IccUtils.adnStringFieldToString(arrayOfByte1, 0, -1 + arrayOfByte1.length);
            log("Load EF_SPN_SHORT_CPHS: " + this.spn);
            SystemProperties.set("gsm.sim.operator.alpha", this.spn);
        }
        while (true)
        {
            this.spnState = Get_Spn_Fsm_State.IDLE;
            break;
            log("No SPN loaded in either CHPS or 3GPP");
        }
    }

    private void handleEfCspData(byte[] paramArrayOfByte)
    {
        int i = paramArrayOfByte.length / 2;
        this.mCspPlmnEnabled = true;
        int j = 0;
        if (j < i)
            if (paramArrayOfByte[(j * 2)] == -64)
            {
                log("[CSP] found ValueAddedServicesGroup, value " + paramArrayOfByte[(1 + j * 2)]);
                if ((0x80 & paramArrayOfByte[(1 + j * 2)]) == 128)
                    this.mCspPlmnEnabled = true;
            }
        while (true)
        {
            return;
            this.mCspPlmnEnabled = false;
            log("[CSP] Set Automatic Network Selection");
            this.mNetworkSelectionModeAutomaticRegistrants.notifyRegistrants();
            continue;
            j++;
            break;
            log("[CSP] Value Added Service Group (0xC0), not found!");
        }
    }

    private void handleFileUpdate(int paramInt)
    {
        switch (paramInt)
        {
        default:
            this.adnCache.reset();
            fetchSimRecords();
        case 28615:
        case 28439:
        case 28437:
        }
        while (true)
        {
            return;
            this.recordsToLoad = (1 + this.recordsToLoad);
            new AdnRecordLoader(this.mFh).loadFromEF(28615, 28616, this.mailboxIndex, obtainMessage(6));
            continue;
            this.recordsToLoad = (1 + this.recordsToLoad);
            new AdnRecordLoader(this.mFh).loadFromEF(28439, 28490, 1, obtainMessage(11));
            continue;
            this.recordsToLoad = (1 + this.recordsToLoad);
            log("[CSP] SIM Refresh for EF_CSP_CPHS");
            this.mFh.loadEFTransparent(28437, obtainMessage(33));
        }
    }

    private void handleSimRefresh(IccRefreshResponse paramIccRefreshResponse)
    {
        if (paramIccRefreshResponse == null)
            log("handleSimRefresh received without input");
        while (true)
        {
            return;
            if ((paramIccRefreshResponse.aid == null) || (paramIccRefreshResponse.aid.equals(this.mParentCard.getAid())))
                switch (paramIccRefreshResponse.refreshResult)
                {
                default:
                    log("handleSimRefresh with unknown operation");
                    break;
                case 0:
                    log("handleSimRefresh with SIM_FILE_UPDATED");
                    handleFileUpdate(paramIccRefreshResponse.efId);
                    break;
                case 1:
                    log("handleSimRefresh with SIM_REFRESH_INIT");
                    this.adnCache.reset();
                    fetchSimRecords();
                    break;
                case 2:
                    log("handleSimRefresh with SIM_REFRESH_RESET");
                    this.mCi.setRadioPower(false, null);
                }
        }
    }

    private void handleSms(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte[0] != 0)
            Log.d("ENF", "status : " + paramArrayOfByte[0]);
        if (paramArrayOfByte[0] == 3)
        {
            int i = paramArrayOfByte.length;
            byte[] arrayOfByte = new byte[i - 1];
            System.arraycopy(paramArrayOfByte, 1, arrayOfByte, 0, i - 1);
            dispatchGsmMessage(SmsMessage.createFromPdu(arrayOfByte));
        }
    }

    private void handleSmses(ArrayList paramArrayList)
    {
        int i = paramArrayList.size();
        for (int j = 0; j < i; j++)
        {
            byte[] arrayOfByte1 = (byte[])paramArrayList.get(j);
            if (arrayOfByte1[0] != 0)
                Log.i("ENF", "status " + j + ": " + arrayOfByte1[0]);
            if (arrayOfByte1[0] == 3)
            {
                int k = arrayOfByte1.length;
                byte[] arrayOfByte2 = new byte[k - 1];
                System.arraycopy(arrayOfByte1, 1, arrayOfByte2, 0, k - 1);
                dispatchGsmMessage(SmsMessage.createFromPdu(arrayOfByte2));
                arrayOfByte1[0] = 1;
            }
        }
    }

    private boolean isCphsMailboxEnabled()
    {
        boolean bool1 = true;
        boolean bool2 = false;
        if (this.mCphsInfo == null)
            return bool2;
        if ((0x30 & this.mCphsInfo[bool1]) == 48);
        while (true)
        {
            bool2 = bool1;
            break;
            bool1 = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private boolean isOnMatchingPlmn(String paramString)
    {
        boolean bool = false;
        if (paramString == null)
            break label40;
        while (true)
        {
            return bool;
            if (Injector.isMatchingOperator(this, paramString, getOperatorNumeric()))
            {
                bool = true;
            }
            else if (this.spdiNetworks != null)
            {
                Iterator localIterator = this.spdiNetworks.iterator();
                label40: if (localIterator.hasNext())
                {
                    if (!paramString.equals((String)localIterator.next()))
                        break;
                    bool = true;
                }
            }
        }
    }

    private void parseEfSpdi(byte[] paramArrayOfByte)
    {
        SimTlv localSimTlv = new SimTlv(paramArrayOfByte, 0, paramArrayOfByte.length);
        byte[] arrayOfByte = null;
        if (localSimTlv.isValidObject())
        {
            if (localSimTlv.getTag() == 163)
                localSimTlv = new SimTlv(localSimTlv.getData(), 0, localSimTlv.getData().length);
            if (localSimTlv.getTag() == 128)
                arrayOfByte = localSimTlv.getData();
        }
        else
        {
            if (arrayOfByte != null)
                break label77;
        }
        while (true)
        {
            return;
            localSimTlv.nextObject();
            break;
            label77: this.spdiNetworks = new ArrayList(arrayOfByte.length / 3);
            for (int i = 0; i + 2 < arrayOfByte.length; i += 3)
            {
                String str = IccUtils.bcdToString(arrayOfByte, i, 3);
                if (str.length() >= 5)
                {
                    log("EF_SPDI network: " + str);
                    this.spdiNetworks.add(str);
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void setSpnFromConfig(String paramString)
    {
        if (this.mSpnOverride.containsCarrier(paramString))
        {
            this.spn = this.mSpnOverride.getSpn(paramString);
            Injector.updateSpnDisplayCondition(this);
        }
    }

    private void setVoiceMailByCountry(String paramString)
    {
        if (this.mVmConfig.containsCarrier(paramString))
        {
            this.isVoiceMailFixed = true;
            this.voiceMailNum = this.mVmConfig.getVoiceMailNumber(paramString);
            this.voiceMailTag = this.mVmConfig.getVoiceMailTag(paramString);
        }
    }

    protected int dispatchGsmMessage(SmsMessageBase paramSmsMessageBase)
    {
        this.mNewSmsRegistrants.notifyResult(paramSmsMessageBase);
        return 0;
    }

    public void dispose()
    {
        log("Disposing SIMRecords " + this);
        this.mCi.unregisterForOffOrNotAvailable(this);
        this.mCi.unregisterForIccRefresh(this);
        this.mCi.unSetOnSmsOnSim(this);
        super.dispose();
    }

    protected void fetchSimRecords()
    {
        this.recordsRequested = true;
        log("fetchSimRecords " + this.recordsToLoad);
        this.mCi.getIMSIForApp(this.mParentCard.getAid(), obtainMessage(3));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(12258, obtainMessage(4));
        this.recordsToLoad = (1 + this.recordsToLoad);
        new AdnRecordLoader(this.mFh).loadFromEF(28480, 28490, 1, obtainMessage(10));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFLinearFixed(28617, 1, obtainMessage(5));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28589, obtainMessage(9));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFLinearFixed(28618, 1, obtainMessage(7));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28433, obtainMessage(8));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFLinearFixed(28619, 1, obtainMessage(32));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28435, obtainMessage(24));
        this.recordsToLoad = (1 + this.recordsToLoad);
        getSpnFsm(true, null);
        this.mFh.loadEFTransparent(28621, obtainMessage(13));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFLinearFixed(28613, 1, obtainMessage(15));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28472, obtainMessage(17));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28438, obtainMessage(26));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28437, obtainMessage(33));
        this.recordsToLoad = (1 + this.recordsToLoad);
        log("fetchSimRecords " + this.recordsToLoad + " requested: " + this.recordsRequested);
    }

    protected void finalize()
    {
        log("finalized");
    }

    public int getDisplayRule(String paramString)
    {
        int i;
        if ((this.spn == null) || (this.spnDisplayCondition == -1))
            i = 2;
        while (true)
        {
            return i;
            if (isOnMatchingPlmn(paramString))
            {
                i = 1;
                if ((0x1 & this.spnDisplayCondition) == 1)
                    i |= 2;
            }
            else
            {
                i = 2;
                if ((0x2 & this.spnDisplayCondition) == 0)
                    i |= 1;
            }
        }
    }

    public String getIMSI()
    {
        return this.imsi;
    }

    public String getMsisdnAlphaTag()
    {
        return this.msisdnTag;
    }

    public String getMsisdnNumber()
    {
        return this.msisdn;
    }

    public String getOperatorNumeric()
    {
        String str = null;
        if (this.imsi == null)
            log("getOperatorNumeric: IMSI == null");
        while (true)
        {
            return str;
            if ((this.mncLength == -1) || (this.mncLength == 0))
                log("getSIMOperatorNumeric: bad mncLength");
            else
                str = this.imsi.substring(0, 3 + this.mncLength);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    String getSpn()
    {
        return this.spn;
    }

    public UsimServiceTable getUsimServiceTable()
    {
        return this.mUsimServiceTable;
    }

    public boolean getVoiceCallForwardingFlag()
    {
        return this.callForwardingEnabled;
    }

    public String getVoiceMailAlphaTag()
    {
        return this.voiceMailTag;
    }

    public String getVoiceMailNumber()
    {
        return this.voiceMailNum;
    }

    // ERROR //
    public void handleMessage(Message paramMessage)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: aload_0
        //     3: getfield 836	com/android/internal/telephony/IccRecords:mDestroyed	Z
        //     6: ifeq +53 -> 59
        //     9: aload_0
        //     10: new 502	java/lang/StringBuilder
        //     13: dup
        //     14: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     17: ldc_w 838
        //     20: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     23: aload_1
        //     24: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     27: ldc_w 840
        //     30: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     33: aload_1
        //     34: getfield 845	android/os/Message:what	I
        //     37: invokevirtual 514	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     40: ldc_w 847
        //     43: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: ldc_w 849
        //     49: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     52: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     55: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     58: return
        //     59: aload_1
        //     60: getfield 845	android/os/Message:what	I
        //     63: tableswitch	default:+141 -> 204, 2:+157->220, 3:+182->245, 4:+1304->1367, 5:+507->570, 6:+687->750, 7:+1063->1126, 8:+1209->1272, 9:+1379->1442, 10:+944->1007, 11:+687->750, 12:+2383->2446, 13:+2508->2571, 14:+2549->2612, 15:+2581->2644, 16:+141->204, 17:+2944->3007, 18:+2677->2740, 19:+2711->2774, 20:+3086->3149, 21:+2744->2807, 22:+2876->2939, 23:+141->204, 24:+2400->2463, 25:+3283->3346, 26:+3019->3082, 27:+141->204, 28:+141->204, 29:+141->204, 30:+1011->1074, 31:+3397->3460, 32:+3459->3522, 33:+3557->3620
        //     205: aload_1
        //     206: invokespecial 854	com/android/internal/telephony/IccRecords:handleMessage	(Landroid/os/Message;)V
        //     209: iload_2
        //     210: ifeq -152 -> 58
        //     213: aload_0
        //     214: invokevirtual 857	com/android/internal/telephony/gsm/SIMRecords:onRecordLoaded	()V
        //     217: goto -159 -> 58
        //     220: aload_0
        //     221: invokevirtual 441	com/android/internal/telephony/gsm/SIMRecords:onRadioOffOrNotAvailable	()V
        //     224: goto -15 -> 209
        //     227: astore 4
        //     229: aload_0
        //     230: ldc_w 859
        //     233: aload 4
        //     235: invokevirtual 863	com/android/internal/telephony/gsm/SIMRecords:logw	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     238: iload_2
        //     239: ifeq -181 -> 58
        //     242: goto -29 -> 213
        //     245: iconst_1
        //     246: istore_2
        //     247: aload_1
        //     248: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     251: checkcast 483	android/os/AsyncResult
        //     254: astore 87
        //     256: aload 87
        //     258: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     261: ifnull +45 -> 306
        //     264: aload_0
        //     265: new 502	java/lang/StringBuilder
        //     268: dup
        //     269: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     272: ldc_w 868
        //     275: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     278: aload 87
        //     280: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     283: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     286: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     289: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     292: goto -83 -> 209
        //     295: astore_3
        //     296: iload_2
        //     297: ifeq +7 -> 304
        //     300: aload_0
        //     301: invokevirtual 857	com/android/internal/telephony/gsm/SIMRecords:onRecordLoaded	()V
        //     304: aload_3
        //     305: athrow
        //     306: aload_0
        //     307: aload 87
        //     309: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     312: checkcast 122	java/lang/String
        //     315: putfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     318: aload_0
        //     319: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     322: ifnull +59 -> 381
        //     325: aload_0
        //     326: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     329: invokevirtual 711	java/lang/String:length	()I
        //     332: bipush 6
        //     334: if_icmplt +15 -> 349
        //     337: aload_0
        //     338: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     341: invokevirtual 711	java/lang/String:length	()I
        //     344: bipush 15
        //     346: if_icmple +35 -> 381
        //     349: aload_0
        //     350: new 502	java/lang/StringBuilder
        //     353: dup
        //     354: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     357: ldc_w 870
        //     360: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     363: aload_0
        //     364: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     367: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     370: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     373: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     376: aload_0
        //     377: aconst_null
        //     378: putfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     381: aload_0
        //     382: ldc_w 872
        //     385: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     388: aload_0
        //     389: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     392: ifeq +11 -> 403
        //     395: aload_0
        //     396: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     399: iconst_2
        //     400: if_icmpne +72 -> 472
        //     403: aload_0
        //     404: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     407: ifnull +65 -> 472
        //     410: aload_0
        //     411: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     414: invokevirtual 711	java/lang/String:length	()I
        //     417: bipush 6
        //     419: if_icmplt +53 -> 472
        //     422: aload_0
        //     423: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     426: iconst_0
        //     427: bipush 6
        //     429: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     432: astore 90
        //     434: getstatic 368	com/android/internal/telephony/gsm/SIMRecords:MCCMNC_CODES_HAVING_3DIGITS_MNC	[Ljava/lang/String;
        //     437: astore 91
        //     439: aload 91
        //     441: arraylength
        //     442: istore 92
        //     444: iconst_0
        //     445: istore 93
        //     447: iload 93
        //     449: iload 92
        //     451: if_icmpge +21 -> 472
        //     454: aload 91
        //     456: iload 93
        //     458: aaload
        //     459: aload 90
        //     461: invokevirtual 595	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     464: ifeq +3416 -> 3880
        //     467: aload_0
        //     468: iconst_3
        //     469: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     472: aload_0
        //     473: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     476: istore 88
        //     478: iload 88
        //     480: ifne +22 -> 502
        //     483: aload_0
        //     484: aload_0
        //     485: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     488: iconst_0
        //     489: iconst_3
        //     490: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     493: invokestatic 877	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     496: invokestatic 883	com/android/internal/telephony/MccTable:smallestDigitsMccForMnc	(I)I
        //     499: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     502: aload_0
        //     503: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     506: ifeq +33 -> 539
        //     509: aload_0
        //     510: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     513: bipush 255
        //     515: if_icmpeq +24 -> 539
        //     518: aload_0
        //     519: getfield 887	com/android/internal/telephony/IccRecords:mContext	Landroid/content/Context;
        //     522: aload_0
        //     523: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     526: iconst_0
        //     527: iconst_3
        //     528: aload_0
        //     529: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     532: iadd
        //     533: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     536: invokestatic 891	com/android/internal/telephony/MccTable:updateMccMncConfiguration	(Landroid/content/Context;Ljava/lang/String;)V
        //     539: aload_0
        //     540: getfield 586	com/android/internal/telephony/IccRecords:mParentCard	Lcom/android/internal/telephony/IccCard;
        //     543: ldc_w 893
        //     546: aconst_null
        //     547: invokevirtual 896	com/android/internal/telephony/IccCard:broadcastIccStateChangedIntent	(Ljava/lang/String;Ljava/lang/String;)V
        //     550: goto -341 -> 209
        //     553: astore 89
        //     555: aload_0
        //     556: iconst_0
        //     557: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     560: aload_0
        //     561: ldc_w 898
        //     564: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     567: goto -65 -> 502
        //     570: iconst_1
        //     571: istore_2
        //     572: aload_1
        //     573: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     576: checkcast 483	android/os/AsyncResult
        //     579: astore 84
        //     581: aload 84
        //     583: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     586: checkcast 492	[B
        //     589: checkcast 492	[B
        //     592: astore 85
        //     594: iconst_0
        //     595: istore 86
        //     597: aload 84
        //     599: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     602: ifnonnull +70 -> 672
        //     605: aload_0
        //     606: new 502	java/lang/StringBuilder
        //     609: dup
        //     610: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     613: ldc_w 900
        //     616: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     619: aload 85
        //     621: invokestatic 904	com/android/internal/telephony/IccUtils:bytesToHexString	([B)Ljava/lang/String;
        //     624: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     627: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     630: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     633: aload_0
        //     634: sipush 255
        //     637: aload 85
        //     639: iconst_0
        //     640: baload
        //     641: iand
        //     642: putfield 567	com/android/internal/telephony/IccRecords:mailboxIndex	I
        //     645: aload_0
        //     646: getfield 567	com/android/internal/telephony/IccRecords:mailboxIndex	I
        //     649: ifeq +23 -> 672
        //     652: aload_0
        //     653: getfield 567	com/android/internal/telephony/IccRecords:mailboxIndex	I
        //     656: sipush 255
        //     659: if_icmpeq +13 -> 672
        //     662: aload_0
        //     663: ldc_w 906
        //     666: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     669: iconst_1
        //     670: istore 86
        //     672: aload_0
        //     673: iconst_1
        //     674: aload_0
        //     675: getfield 422	com/android/internal/telephony/IccRecords:recordsToLoad	I
        //     678: iadd
        //     679: putfield 422	com/android/internal/telephony/IccRecords:recordsToLoad	I
        //     682: iload 86
        //     684: ifeq +36 -> 720
        //     687: new 563	com/android/internal/telephony/AdnRecordLoader
        //     690: dup
        //     691: aload_0
        //     692: getfield 398	com/android/internal/telephony/IccRecords:mFh	Lcom/android/internal/telephony/IccFileHandler;
        //     695: invokespecial 564	com/android/internal/telephony/AdnRecordLoader:<init>	(Lcom/android/internal/telephony/IccFileHandler;)V
        //     698: sipush 28615
        //     701: sipush 28616
        //     704: aload_0
        //     705: getfield 567	com/android/internal/telephony/IccRecords:mailboxIndex	I
        //     708: aload_0
        //     709: bipush 6
        //     711: invokevirtual 475	com/android/internal/telephony/gsm/SIMRecords:obtainMessage	(I)Landroid/os/Message;
        //     714: invokevirtual 571	com/android/internal/telephony/AdnRecordLoader:loadFromEF	(IIILandroid/os/Message;)V
        //     717: goto -508 -> 209
        //     720: new 563	com/android/internal/telephony/AdnRecordLoader
        //     723: dup
        //     724: aload_0
        //     725: getfield 398	com/android/internal/telephony/IccRecords:mFh	Lcom/android/internal/telephony/IccFileHandler;
        //     728: invokespecial 564	com/android/internal/telephony/AdnRecordLoader:<init>	(Lcom/android/internal/telephony/IccFileHandler;)V
        //     731: sipush 28439
        //     734: sipush 28490
        //     737: iconst_1
        //     738: aload_0
        //     739: bipush 11
        //     741: invokevirtual 475	com/android/internal/telephony/gsm/SIMRecords:obtainMessage	(I)Landroid/os/Message;
        //     744: invokevirtual 571	com/android/internal/telephony/AdnRecordLoader:loadFromEF	(IIILandroid/os/Message;)V
        //     747: goto -538 -> 209
        //     750: aload_0
        //     751: aconst_null
        //     752: putfield 741	com/android/internal/telephony/IccRecords:voiceMailNum	Ljava/lang/String;
        //     755: aload_0
        //     756: aconst_null
        //     757: putfield 747	com/android/internal/telephony/IccRecords:voiceMailTag	Ljava/lang/String;
        //     760: iconst_1
        //     761: istore_2
        //     762: aload_1
        //     763: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     766: checkcast 483	android/os/AsyncResult
        //     769: astore 78
        //     771: aload 78
        //     773: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     776: ifnull +95 -> 871
        //     779: new 502	java/lang/StringBuilder
        //     782: dup
        //     783: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     786: ldc_w 908
        //     789: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     792: astore 82
        //     794: aload_1
        //     795: getfield 845	android/os/Message:what	I
        //     798: bipush 11
        //     800: if_icmpne +3086 -> 3886
        //     803: ldc_w 910
        //     806: astore 83
        //     808: aload_0
        //     809: aload 82
        //     811: aload 83
        //     813: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     816: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     819: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     822: aload_1
        //     823: getfield 845	android/os/Message:what	I
        //     826: bipush 6
        //     828: if_icmpne -619 -> 209
        //     831: aload_0
        //     832: iconst_1
        //     833: aload_0
        //     834: getfield 422	com/android/internal/telephony/IccRecords:recordsToLoad	I
        //     837: iadd
        //     838: putfield 422	com/android/internal/telephony/IccRecords:recordsToLoad	I
        //     841: new 563	com/android/internal/telephony/AdnRecordLoader
        //     844: dup
        //     845: aload_0
        //     846: getfield 398	com/android/internal/telephony/IccRecords:mFh	Lcom/android/internal/telephony/IccFileHandler;
        //     849: invokespecial 564	com/android/internal/telephony/AdnRecordLoader:<init>	(Lcom/android/internal/telephony/IccFileHandler;)V
        //     852: sipush 28439
        //     855: sipush 28490
        //     858: iconst_1
        //     859: aload_0
        //     860: bipush 11
        //     862: invokevirtual 475	com/android/internal/telephony/gsm/SIMRecords:obtainMessage	(I)Landroid/os/Message;
        //     865: invokevirtual 571	com/android/internal/telephony/AdnRecordLoader:loadFromEF	(IIILandroid/os/Message;)V
        //     868: goto -659 -> 209
        //     871: aload 78
        //     873: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     876: checkcast 912	com/android/internal/telephony/AdnRecord
        //     879: astore 79
        //     881: new 502	java/lang/StringBuilder
        //     884: dup
        //     885: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     888: ldc_w 914
        //     891: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     894: aload 79
        //     896: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     899: astore 80
        //     901: aload_1
        //     902: getfield 845	android/os/Message:what	I
        //     905: bipush 11
        //     907: if_icmpne +2987 -> 3894
        //     910: ldc_w 916
        //     913: astore 81
        //     915: aload_0
        //     916: aload 80
        //     918: aload 81
        //     920: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     923: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     926: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     929: aload 79
        //     931: invokevirtual 919	com/android/internal/telephony/AdnRecord:isEmpty	()Z
        //     934: ifeq +52 -> 986
        //     937: aload_1
        //     938: getfield 845	android/os/Message:what	I
        //     941: bipush 6
        //     943: if_icmpne +43 -> 986
        //     946: aload_0
        //     947: iconst_1
        //     948: aload_0
        //     949: getfield 422	com/android/internal/telephony/IccRecords:recordsToLoad	I
        //     952: iadd
        //     953: putfield 422	com/android/internal/telephony/IccRecords:recordsToLoad	I
        //     956: new 563	com/android/internal/telephony/AdnRecordLoader
        //     959: dup
        //     960: aload_0
        //     961: getfield 398	com/android/internal/telephony/IccRecords:mFh	Lcom/android/internal/telephony/IccFileHandler;
        //     964: invokespecial 564	com/android/internal/telephony/AdnRecordLoader:<init>	(Lcom/android/internal/telephony/IccFileHandler;)V
        //     967: sipush 28439
        //     970: sipush 28490
        //     973: iconst_1
        //     974: aload_0
        //     975: bipush 11
        //     977: invokevirtual 475	com/android/internal/telephony/gsm/SIMRecords:obtainMessage	(I)Landroid/os/Message;
        //     980: invokevirtual 571	com/android/internal/telephony/AdnRecordLoader:loadFromEF	(IIILandroid/os/Message;)V
        //     983: goto -774 -> 209
        //     986: aload_0
        //     987: aload 79
        //     989: invokevirtual 922	com/android/internal/telephony/AdnRecord:getNumber	()Ljava/lang/String;
        //     992: putfield 741	com/android/internal/telephony/IccRecords:voiceMailNum	Ljava/lang/String;
        //     995: aload_0
        //     996: aload 79
        //     998: invokevirtual 925	com/android/internal/telephony/AdnRecord:getAlphaTag	()Ljava/lang/String;
        //     1001: putfield 747	com/android/internal/telephony/IccRecords:voiceMailTag	Ljava/lang/String;
        //     1004: goto -795 -> 209
        //     1007: iconst_1
        //     1008: istore_2
        //     1009: aload_1
        //     1010: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     1013: checkcast 483	android/os/AsyncResult
        //     1016: astore 76
        //     1018: aload 76
        //     1020: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     1023: ifnull +13 -> 1036
        //     1026: aload_0
        //     1027: ldc_w 927
        //     1030: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     1033: goto -824 -> 209
        //     1036: aload 76
        //     1038: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     1041: checkcast 912	com/android/internal/telephony/AdnRecord
        //     1044: astore 77
        //     1046: aload_0
        //     1047: aload 77
        //     1049: invokevirtual 922	com/android/internal/telephony/AdnRecord:getNumber	()Ljava/lang/String;
        //     1052: putfield 807	com/android/internal/telephony/IccRecords:msisdn	Ljava/lang/String;
        //     1055: aload_0
        //     1056: aload 77
        //     1058: invokevirtual 925	com/android/internal/telephony/AdnRecord:getAlphaTag	()Ljava/lang/String;
        //     1061: putfield 803	com/android/internal/telephony/IccRecords:msisdnTag	Ljava/lang/String;
        //     1064: aload_0
        //     1065: ldc_w 929
        //     1068: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     1071: goto -862 -> 209
        //     1074: iconst_0
        //     1075: istore_2
        //     1076: aload_1
        //     1077: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     1080: checkcast 483	android/os/AsyncResult
        //     1083: astore 75
        //     1085: aload 75
        //     1087: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     1090: ifnull -881 -> 209
        //     1093: aload 75
        //     1095: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     1098: checkcast 842	android/os/Message
        //     1101: invokestatic 936	android/os/AsyncResult:forMessage	(Landroid/os/Message;)Landroid/os/AsyncResult;
        //     1104: aload 75
        //     1106: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     1109: putfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     1112: aload 75
        //     1114: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     1117: checkcast 842	android/os/Message
        //     1120: invokevirtual 939	android/os/Message:sendToTarget	()V
        //     1123: goto -914 -> 209
        //     1126: iconst_1
        //     1127: istore_2
        //     1128: aload_1
        //     1129: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     1132: checkcast 483	android/os/AsyncResult
        //     1135: astore 72
        //     1137: aload 72
        //     1139: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     1142: checkcast 492	[B
        //     1145: checkcast 492	[B
        //     1148: astore 73
        //     1150: aload 72
        //     1152: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     1155: ifnonnull -946 -> 209
        //     1158: aload_0
        //     1159: new 502	java/lang/StringBuilder
        //     1162: dup
        //     1163: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     1166: ldc_w 941
        //     1169: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1172: aload 73
        //     1174: invokestatic 904	com/android/internal/telephony/IccUtils:bytesToHexString	([B)Ljava/lang/String;
        //     1177: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1180: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1183: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     1186: aload_0
        //     1187: aload 73
        //     1189: putfield 382	com/android/internal/telephony/gsm/SIMRecords:efMWIS	[B
        //     1192: sipush 255
        //     1195: aload 73
        //     1197: iconst_0
        //     1198: baload
        //     1199: iand
        //     1200: sipush 255
        //     1203: if_icmpne +13 -> 1216
        //     1206: aload_0
        //     1207: ldc_w 943
        //     1210: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     1213: goto -1004 -> 209
        //     1216: iconst_1
        //     1217: aload 73
        //     1219: iconst_0
        //     1220: baload
        //     1221: iand
        //     1222: ifeq +2680 -> 3902
        //     1225: iconst_1
        //     1226: istore 74
        //     1228: aload_0
        //     1229: sipush 255
        //     1232: aload 73
        //     1234: iconst_1
        //     1235: baload
        //     1236: iand
        //     1237: putfield 946	com/android/internal/telephony/IccRecords:countVoiceMessages	I
        //     1240: iload 74
        //     1242: ifeq +16 -> 1258
        //     1245: aload_0
        //     1246: getfield 946	com/android/internal/telephony/IccRecords:countVoiceMessages	I
        //     1249: ifne +9 -> 1258
        //     1252: aload_0
        //     1253: bipush 255
        //     1255: putfield 946	com/android/internal/telephony/IccRecords:countVoiceMessages	I
        //     1258: aload_0
        //     1259: getfield 949	com/android/internal/telephony/IccRecords:mRecordsEventsRegistrants	Landroid/os/RegistrantList;
        //     1262: iconst_0
        //     1263: invokestatic 953	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     1266: invokevirtual 754	android/os/RegistrantList:notifyResult	(Ljava/lang/Object;)V
        //     1269: goto -1060 -> 209
        //     1272: iconst_1
        //     1273: istore_2
        //     1274: aload_1
        //     1275: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     1278: checkcast 483	android/os/AsyncResult
        //     1281: astore 69
        //     1283: aload 69
        //     1285: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     1288: checkcast 492	[B
        //     1291: checkcast 492	[B
        //     1294: astore 70
        //     1296: aload 69
        //     1298: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     1301: ifnonnull -1092 -> 209
        //     1304: aload_0
        //     1305: aload 70
        //     1307: putfield 384	com/android/internal/telephony/gsm/SIMRecords:efCPHS_MWI	[B
        //     1310: aload_0
        //     1311: getfield 382	com/android/internal/telephony/gsm/SIMRecords:efMWIS	[B
        //     1314: ifnonnull -1105 -> 209
        //     1317: bipush 15
        //     1319: aload 70
        //     1321: iconst_0
        //     1322: baload
        //     1323: iand
        //     1324: istore 71
        //     1326: iload 71
        //     1328: bipush 10
        //     1330: if_icmpne +23 -> 1353
        //     1333: aload_0
        //     1334: bipush 255
        //     1336: putfield 946	com/android/internal/telephony/IccRecords:countVoiceMessages	I
        //     1339: aload_0
        //     1340: getfield 949	com/android/internal/telephony/IccRecords:mRecordsEventsRegistrants	Landroid/os/RegistrantList;
        //     1343: iconst_0
        //     1344: invokestatic 953	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     1347: invokevirtual 754	android/os/RegistrantList:notifyResult	(Ljava/lang/Object;)V
        //     1350: goto -1141 -> 209
        //     1353: iload 71
        //     1355: iconst_5
        //     1356: if_icmpne -17 -> 1339
        //     1359: aload_0
        //     1360: iconst_0
        //     1361: putfield 946	com/android/internal/telephony/IccRecords:countVoiceMessages	I
        //     1364: goto -25 -> 1339
        //     1367: iconst_1
        //     1368: istore_2
        //     1369: aload_1
        //     1370: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     1373: checkcast 483	android/os/AsyncResult
        //     1376: astore 67
        //     1378: aload 67
        //     1380: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     1383: checkcast 492	[B
        //     1386: checkcast 492	[B
        //     1389: astore 68
        //     1391: aload 67
        //     1393: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     1396: ifnonnull -1187 -> 209
        //     1399: aload_0
        //     1400: aload 68
        //     1402: iconst_0
        //     1403: aload 68
        //     1405: arraylength
        //     1406: invokestatic 708	com/android/internal/telephony/IccUtils:bcdToString	([BII)Ljava/lang/String;
        //     1409: putfield 956	com/android/internal/telephony/IccRecords:iccid	Ljava/lang/String;
        //     1412: aload_0
        //     1413: new 502	java/lang/StringBuilder
        //     1416: dup
        //     1417: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     1420: ldc_w 958
        //     1423: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1426: aload_0
        //     1427: getfield 956	com/android/internal/telephony/IccRecords:iccid	Ljava/lang/String;
        //     1430: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1433: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1436: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     1439: goto -1230 -> 209
        //     1442: iconst_1
        //     1443: istore_2
        //     1444: aload_1
        //     1445: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     1448: checkcast 483	android/os/AsyncResult
        //     1451: astore 40
        //     1453: aload 40
        //     1455: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     1458: checkcast 492	[B
        //     1461: checkcast 492	[B
        //     1464: astore 41
        //     1466: aload 40
        //     1468: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     1471: astore 42
        //     1473: aload 42
        //     1475: ifnull +180 -> 1655
        //     1478: aload_0
        //     1479: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1482: bipush 255
        //     1484: if_icmpeq +18 -> 1502
        //     1487: aload_0
        //     1488: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1491: ifeq +11 -> 1502
        //     1494: aload_0
        //     1495: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1498: iconst_2
        //     1499: if_icmpne +72 -> 1571
        //     1502: aload_0
        //     1503: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1506: ifnull +65 -> 1571
        //     1509: aload_0
        //     1510: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1513: invokevirtual 711	java/lang/String:length	()I
        //     1516: bipush 6
        //     1518: if_icmplt +53 -> 1571
        //     1521: aload_0
        //     1522: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1525: iconst_0
        //     1526: bipush 6
        //     1528: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     1531: astore 63
        //     1533: getstatic 368	com/android/internal/telephony/gsm/SIMRecords:MCCMNC_CODES_HAVING_3DIGITS_MNC	[Ljava/lang/String;
        //     1536: astore 64
        //     1538: aload 64
        //     1540: arraylength
        //     1541: istore 65
        //     1543: iconst_0
        //     1544: istore 66
        //     1546: iload 66
        //     1548: iload 65
        //     1550: if_icmpge +21 -> 1571
        //     1553: aload 64
        //     1555: iload 66
        //     1557: aaload
        //     1558: aload 63
        //     1560: invokevirtual 595	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1563: ifeq +2363 -> 3926
        //     1566: aload_0
        //     1567: iconst_3
        //     1568: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1571: aload_0
        //     1572: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1575: ifeq +12 -> 1587
        //     1578: aload_0
        //     1579: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1582: bipush 255
        //     1584: if_icmpne +33 -> 1617
        //     1587: aload_0
        //     1588: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1591: astore 61
        //     1593: aload 61
        //     1595: ifnull +2157 -> 3752
        //     1598: aload_0
        //     1599: aload_0
        //     1600: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1603: iconst_0
        //     1604: iconst_3
        //     1605: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     1608: invokestatic 877	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     1611: invokestatic 883	com/android/internal/telephony/MccTable:smallestDigitsMccForMnc	(I)I
        //     1614: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1617: aload_0
        //     1618: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1621: ifnull -1412 -> 209
        //     1624: aload_0
        //     1625: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1628: ifeq -1419 -> 209
        //     1631: aload_0
        //     1632: getfield 887	com/android/internal/telephony/IccRecords:mContext	Landroid/content/Context;
        //     1635: aload_0
        //     1636: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1639: iconst_0
        //     1640: iconst_3
        //     1641: aload_0
        //     1642: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1645: iadd
        //     1646: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     1649: invokestatic 891	com/android/internal/telephony/MccTable:updateMccMncConfiguration	(Landroid/content/Context;Ljava/lang/String;)V
        //     1652: goto -1443 -> 209
        //     1655: aload_0
        //     1656: new 502	java/lang/StringBuilder
        //     1659: dup
        //     1660: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     1663: ldc_w 960
        //     1666: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1669: aload 41
        //     1671: invokestatic 904	com/android/internal/telephony/IccUtils:bytesToHexString	([B)Ljava/lang/String;
        //     1674: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1677: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1680: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     1683: aload 41
        //     1685: arraylength
        //     1686: iconst_3
        //     1687: if_icmpge +187 -> 1874
        //     1690: aload_0
        //     1691: ldc_w 962
        //     1694: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     1697: aload_0
        //     1698: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1701: bipush 255
        //     1703: if_icmpeq +18 -> 1721
        //     1706: aload_0
        //     1707: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1710: ifeq +11 -> 1721
        //     1713: aload_0
        //     1714: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1717: iconst_2
        //     1718: if_icmpne +72 -> 1790
        //     1721: aload_0
        //     1722: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1725: ifnull +65 -> 1790
        //     1728: aload_0
        //     1729: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1732: invokevirtual 711	java/lang/String:length	()I
        //     1735: bipush 6
        //     1737: if_icmplt +53 -> 1790
        //     1740: aload_0
        //     1741: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1744: iconst_0
        //     1745: bipush 6
        //     1747: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     1750: astore 57
        //     1752: getstatic 368	com/android/internal/telephony/gsm/SIMRecords:MCCMNC_CODES_HAVING_3DIGITS_MNC	[Ljava/lang/String;
        //     1755: astore 58
        //     1757: aload 58
        //     1759: arraylength
        //     1760: istore 59
        //     1762: iconst_0
        //     1763: istore 60
        //     1765: iload 60
        //     1767: iload 59
        //     1769: if_icmpge +21 -> 1790
        //     1772: aload 58
        //     1774: iload 60
        //     1776: aaload
        //     1777: aload 57
        //     1779: invokevirtual 595	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1782: ifeq +2150 -> 3932
        //     1785: aload_0
        //     1786: iconst_3
        //     1787: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1790: aload_0
        //     1791: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1794: ifeq +12 -> 1806
        //     1797: aload_0
        //     1798: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1801: bipush 255
        //     1803: if_icmpne +33 -> 1836
        //     1806: aload_0
        //     1807: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1810: astore 55
        //     1812: aload 55
        //     1814: ifnull +1970 -> 3784
        //     1817: aload_0
        //     1818: aload_0
        //     1819: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1822: iconst_0
        //     1823: iconst_3
        //     1824: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     1827: invokestatic 877	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     1830: invokestatic 883	com/android/internal/telephony/MccTable:smallestDigitsMccForMnc	(I)I
        //     1833: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1836: aload_0
        //     1837: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1840: ifnull -1631 -> 209
        //     1843: aload_0
        //     1844: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1847: ifeq -1638 -> 209
        //     1850: aload_0
        //     1851: getfield 887	com/android/internal/telephony/IccRecords:mContext	Landroid/content/Context;
        //     1854: aload_0
        //     1855: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1858: iconst_0
        //     1859: iconst_3
        //     1860: aload_0
        //     1861: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1864: iadd
        //     1865: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     1868: invokestatic 891	com/android/internal/telephony/MccTable:updateMccMncConfiguration	(Landroid/content/Context;Ljava/lang/String;)V
        //     1871: goto -1662 -> 209
        //     1874: aload 41
        //     1876: arraylength
        //     1877: iconst_3
        //     1878: if_icmpne +187 -> 2065
        //     1881: aload_0
        //     1882: ldc_w 964
        //     1885: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     1888: aload_0
        //     1889: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1892: bipush 255
        //     1894: if_icmpeq +18 -> 1912
        //     1897: aload_0
        //     1898: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1901: ifeq +11 -> 1912
        //     1904: aload_0
        //     1905: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1908: iconst_2
        //     1909: if_icmpne +72 -> 1981
        //     1912: aload_0
        //     1913: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1916: ifnull +65 -> 1981
        //     1919: aload_0
        //     1920: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1923: invokevirtual 711	java/lang/String:length	()I
        //     1926: bipush 6
        //     1928: if_icmplt +53 -> 1981
        //     1931: aload_0
        //     1932: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     1935: iconst_0
        //     1936: bipush 6
        //     1938: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     1941: astore 51
        //     1943: getstatic 368	com/android/internal/telephony/gsm/SIMRecords:MCCMNC_CODES_HAVING_3DIGITS_MNC	[Ljava/lang/String;
        //     1946: astore 52
        //     1948: aload 52
        //     1950: arraylength
        //     1951: istore 53
        //     1953: iconst_0
        //     1954: istore 54
        //     1956: iload 54
        //     1958: iload 53
        //     1960: if_icmpge +21 -> 1981
        //     1963: aload 52
        //     1965: iload 54
        //     1967: aaload
        //     1968: aload 51
        //     1970: invokevirtual 595	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1973: ifeq +1965 -> 3938
        //     1976: aload_0
        //     1977: iconst_3
        //     1978: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1981: aload_0
        //     1982: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1985: ifeq +12 -> 1997
        //     1988: aload_0
        //     1989: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     1992: bipush 255
        //     1994: if_icmpne +33 -> 2027
        //     1997: aload_0
        //     1998: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2001: astore 49
        //     2003: aload 49
        //     2005: ifnull +1811 -> 3816
        //     2008: aload_0
        //     2009: aload_0
        //     2010: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2013: iconst_0
        //     2014: iconst_3
        //     2015: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     2018: invokestatic 877	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     2021: invokestatic 883	com/android/internal/telephony/MccTable:smallestDigitsMccForMnc	(I)I
        //     2024: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2027: aload_0
        //     2028: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2031: ifnull -1822 -> 209
        //     2034: aload_0
        //     2035: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2038: ifeq -1829 -> 209
        //     2041: aload_0
        //     2042: getfield 887	com/android/internal/telephony/IccRecords:mContext	Landroid/content/Context;
        //     2045: aload_0
        //     2046: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2049: iconst_0
        //     2050: iconst_3
        //     2051: aload_0
        //     2052: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2055: iadd
        //     2056: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     2059: invokestatic 891	com/android/internal/telephony/MccTable:updateMccMncConfiguration	(Landroid/content/Context;Ljava/lang/String;)V
        //     2062: goto -1853 -> 209
        //     2065: aload_0
        //     2066: bipush 15
        //     2068: aload 41
        //     2070: iconst_3
        //     2071: baload
        //     2072: iand
        //     2073: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2076: aload_0
        //     2077: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2080: bipush 15
        //     2082: if_icmpne +8 -> 2090
        //     2085: aload_0
        //     2086: iconst_0
        //     2087: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2090: aload_0
        //     2091: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2094: bipush 255
        //     2096: if_icmpeq +18 -> 2114
        //     2099: aload_0
        //     2100: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2103: ifeq +11 -> 2114
        //     2106: aload_0
        //     2107: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2110: iconst_2
        //     2111: if_icmpne +72 -> 2183
        //     2114: aload_0
        //     2115: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2118: ifnull +65 -> 2183
        //     2121: aload_0
        //     2122: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2125: invokevirtual 711	java/lang/String:length	()I
        //     2128: bipush 6
        //     2130: if_icmplt +53 -> 2183
        //     2133: aload_0
        //     2134: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2137: iconst_0
        //     2138: bipush 6
        //     2140: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     2143: astore 45
        //     2145: getstatic 368	com/android/internal/telephony/gsm/SIMRecords:MCCMNC_CODES_HAVING_3DIGITS_MNC	[Ljava/lang/String;
        //     2148: astore 46
        //     2150: aload 46
        //     2152: arraylength
        //     2153: istore 47
        //     2155: iconst_0
        //     2156: istore 48
        //     2158: iload 48
        //     2160: iload 47
        //     2162: if_icmpge +21 -> 2183
        //     2165: aload 46
        //     2167: iload 48
        //     2169: aaload
        //     2170: aload 45
        //     2172: invokevirtual 595	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     2175: ifeq +1769 -> 3944
        //     2178: aload_0
        //     2179: iconst_3
        //     2180: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2183: aload_0
        //     2184: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2187: ifeq +12 -> 2199
        //     2190: aload_0
        //     2191: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2194: bipush 255
        //     2196: if_icmpne +33 -> 2229
        //     2199: aload_0
        //     2200: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2203: astore 43
        //     2205: aload 43
        //     2207: ifnull +1641 -> 3848
        //     2210: aload_0
        //     2211: aload_0
        //     2212: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2215: iconst_0
        //     2216: iconst_3
        //     2217: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     2220: invokestatic 877	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     2223: invokestatic 883	com/android/internal/telephony/MccTable:smallestDigitsMccForMnc	(I)I
        //     2226: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2229: aload_0
        //     2230: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2233: ifnull -2024 -> 209
        //     2236: aload_0
        //     2237: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2240: ifeq -2031 -> 209
        //     2243: aload_0
        //     2244: getfield 887	com/android/internal/telephony/IccRecords:mContext	Landroid/content/Context;
        //     2247: aload_0
        //     2248: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2251: iconst_0
        //     2252: iconst_3
        //     2253: aload_0
        //     2254: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2257: iadd
        //     2258: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     2261: invokestatic 891	com/android/internal/telephony/MccTable:updateMccMncConfiguration	(Landroid/content/Context;Ljava/lang/String;)V
        //     2264: goto -2055 -> 209
        //     2267: astore 33
        //     2269: aload_0
        //     2270: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2273: bipush 255
        //     2275: if_icmpeq +18 -> 2293
        //     2278: aload_0
        //     2279: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2282: ifeq +11 -> 2293
        //     2285: aload_0
        //     2286: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2289: iconst_2
        //     2290: if_icmpne +72 -> 2362
        //     2293: aload_0
        //     2294: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2297: ifnull +65 -> 2362
        //     2300: aload_0
        //     2301: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2304: invokevirtual 711	java/lang/String:length	()I
        //     2307: bipush 6
        //     2309: if_icmplt +53 -> 2362
        //     2312: aload_0
        //     2313: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2316: iconst_0
        //     2317: bipush 6
        //     2319: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     2322: astore 36
        //     2324: getstatic 368	com/android/internal/telephony/gsm/SIMRecords:MCCMNC_CODES_HAVING_3DIGITS_MNC	[Ljava/lang/String;
        //     2327: astore 37
        //     2329: aload 37
        //     2331: arraylength
        //     2332: istore 38
        //     2334: iconst_0
        //     2335: istore 39
        //     2337: iload 39
        //     2339: iload 38
        //     2341: if_icmpge +21 -> 2362
        //     2344: aload 37
        //     2346: iload 39
        //     2348: aaload
        //     2349: aload 36
        //     2351: invokevirtual 595	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     2354: ifeq +1566 -> 3920
        //     2357: aload_0
        //     2358: iconst_3
        //     2359: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2362: aload_0
        //     2363: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2366: ifeq +12 -> 2378
        //     2369: aload_0
        //     2370: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2373: bipush 255
        //     2375: if_icmpne +33 -> 2408
        //     2378: aload_0
        //     2379: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2382: astore 34
        //     2384: aload 34
        //     2386: ifnull +1334 -> 3720
        //     2389: aload_0
        //     2390: aload_0
        //     2391: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2394: iconst_0
        //     2395: iconst_3
        //     2396: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     2399: invokestatic 877	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     2402: invokestatic 883	com/android/internal/telephony/MccTable:smallestDigitsMccForMnc	(I)I
        //     2405: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2408: aload_0
        //     2409: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2412: ifnull +31 -> 2443
        //     2415: aload_0
        //     2416: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2419: ifeq +24 -> 2443
        //     2422: aload_0
        //     2423: getfield 887	com/android/internal/telephony/IccRecords:mContext	Landroid/content/Context;
        //     2426: aload_0
        //     2427: getfield 799	com/android/internal/telephony/gsm/SIMRecords:imsi	Ljava/lang/String;
        //     2430: iconst_0
        //     2431: iconst_3
        //     2432: aload_0
        //     2433: getfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     2436: iadd
        //     2437: invokevirtual 818	java/lang/String:substring	(II)Ljava/lang/String;
        //     2440: invokestatic 891	com/android/internal/telephony/MccTable:updateMccMncConfiguration	(Landroid/content/Context;Ljava/lang/String;)V
        //     2443: aload 33
        //     2445: athrow
        //     2446: iconst_1
        //     2447: istore_2
        //     2448: aload_0
        //     2449: iconst_0
        //     2450: aload_1
        //     2451: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     2454: checkcast 483	android/os/AsyncResult
        //     2457: invokespecial 784	com/android/internal/telephony/gsm/SIMRecords:getSpnFsm	(ZLandroid/os/AsyncResult;)V
        //     2460: goto -2251 -> 209
        //     2463: iconst_1
        //     2464: istore_2
        //     2465: aload_1
        //     2466: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     2469: checkcast 483	android/os/AsyncResult
        //     2472: astore 30
        //     2474: aload 30
        //     2476: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     2479: checkcast 492	[B
        //     2482: checkcast 492	[B
        //     2485: astore 31
        //     2487: aload 30
        //     2489: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2492: ifnonnull -2283 -> 209
        //     2495: aload_0
        //     2496: new 502	java/lang/StringBuilder
        //     2499: dup
        //     2500: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     2503: ldc_w 966
        //     2506: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2509: aload 31
        //     2511: invokestatic 904	com/android/internal/telephony/IccUtils:bytesToHexString	([B)Ljava/lang/String;
        //     2514: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2517: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2520: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     2523: aload_0
        //     2524: aload 31
        //     2526: putfield 386	com/android/internal/telephony/gsm/SIMRecords:mEfCff	[B
        //     2529: aload_0
        //     2530: getfield 388	com/android/internal/telephony/gsm/SIMRecords:mEfCfis	[B
        //     2533: ifnonnull -2324 -> 209
        //     2536: bipush 15
        //     2538: aload 31
        //     2540: iconst_0
        //     2541: baload
        //     2542: iand
        //     2543: bipush 10
        //     2545: if_icmpne +1363 -> 3908
        //     2548: iconst_1
        //     2549: istore 32
        //     2551: aload_0
        //     2552: iload 32
        //     2554: putfield 826	com/android/internal/telephony/gsm/SIMRecords:callForwardingEnabled	Z
        //     2557: aload_0
        //     2558: getfield 949	com/android/internal/telephony/IccRecords:mRecordsEventsRegistrants	Landroid/os/RegistrantList;
        //     2561: iconst_1
        //     2562: invokestatic 953	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     2565: invokevirtual 754	android/os/RegistrantList:notifyResult	(Ljava/lang/Object;)V
        //     2568: goto -2359 -> 209
        //     2571: iconst_1
        //     2572: istore_2
        //     2573: aload_1
        //     2574: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     2577: checkcast 483	android/os/AsyncResult
        //     2580: astore 28
        //     2582: aload 28
        //     2584: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     2587: checkcast 492	[B
        //     2590: checkcast 492	[B
        //     2593: astore 29
        //     2595: aload 28
        //     2597: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2600: ifnonnull -2391 -> 209
        //     2603: aload_0
        //     2604: aload 29
        //     2606: invokespecial 968	com/android/internal/telephony/gsm/SIMRecords:parseEfSpdi	([B)V
        //     2609: goto -2400 -> 209
        //     2612: aload_1
        //     2613: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     2616: checkcast 483	android/os/AsyncResult
        //     2619: astore 27
        //     2621: aload 27
        //     2623: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2626: ifnull -2417 -> 209
        //     2629: aload_0
        //     2630: ldc_w 970
        //     2633: aload 27
        //     2635: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2638: invokevirtual 863	com/android/internal/telephony/gsm/SIMRecords:logw	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     2641: goto -2432 -> 209
        //     2644: iconst_1
        //     2645: istore_2
        //     2646: aload_1
        //     2647: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     2650: checkcast 483	android/os/AsyncResult
        //     2653: astore 23
        //     2655: aload 23
        //     2657: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     2660: checkcast 492	[B
        //     2663: checkcast 492	[B
        //     2666: astore 24
        //     2668: aload 23
        //     2670: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2673: ifnonnull -2464 -> 209
        //     2676: new 687	com/android/internal/telephony/gsm/SimTlv
        //     2679: dup
        //     2680: aload 24
        //     2682: iconst_0
        //     2683: aload 24
        //     2685: arraylength
        //     2686: invokespecial 690	com/android/internal/telephony/gsm/SimTlv:<init>	([BII)V
        //     2689: astore 25
        //     2691: aload 25
        //     2693: invokevirtual 693	com/android/internal/telephony/gsm/SimTlv:isValidObject	()Z
        //     2696: ifeq -2487 -> 209
        //     2699: aload 25
        //     2701: invokevirtual 696	com/android/internal/telephony/gsm/SimTlv:getTag	()I
        //     2704: bipush 67
        //     2706: if_icmpne +25 -> 2731
        //     2709: aload_0
        //     2710: aload 25
        //     2712: invokevirtual 700	com/android/internal/telephony/gsm/SimTlv:getData	()[B
        //     2715: iconst_0
        //     2716: aload 25
        //     2718: invokevirtual 700	com/android/internal/telephony/gsm/SimTlv:getData	()[B
        //     2721: arraylength
        //     2722: invokestatic 973	com/android/internal/telephony/IccUtils:networkNameToString	([BII)Ljava/lang/String;
        //     2725: putfield 392	com/android/internal/telephony/gsm/SIMRecords:pnnHomeName	Ljava/lang/String;
        //     2728: goto -2519 -> 209
        //     2731: aload 25
        //     2733: invokevirtual 703	com/android/internal/telephony/gsm/SimTlv:nextObject	()Z
        //     2736: pop
        //     2737: goto -46 -> 2691
        //     2740: iconst_1
        //     2741: istore_2
        //     2742: aload_1
        //     2743: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     2746: checkcast 483	android/os/AsyncResult
        //     2749: astore 22
        //     2751: aload 22
        //     2753: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2756: ifnonnull -2547 -> 209
        //     2759: aload_0
        //     2760: aload 22
        //     2762: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     2765: checkcast 646	java/util/ArrayList
        //     2768: invokespecial 975	com/android/internal/telephony/gsm/SIMRecords:handleSmses	(Ljava/util/ArrayList;)V
        //     2771: goto -2562 -> 209
        //     2774: ldc_w 618
        //     2777: new 502	java/lang/StringBuilder
        //     2780: dup
        //     2781: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     2784: ldc_w 977
        //     2787: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2790: aload_1
        //     2791: getfield 980	android/os/Message:arg1	I
        //     2794: invokevirtual 514	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2797: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2800: invokestatic 660	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     2803: pop
        //     2804: goto -2595 -> 209
        //     2807: iconst_0
        //     2808: istore_2
        //     2809: aload_1
        //     2810: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     2813: checkcast 483	android/os/AsyncResult
        //     2816: astore 19
        //     2818: aload 19
        //     2820: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     2823: checkcast 981	[I
        //     2826: checkcast 981	[I
        //     2829: astore 20
        //     2831: aload 19
        //     2833: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2836: ifnonnull +10 -> 2846
        //     2839: aload 20
        //     2841: arraylength
        //     2842: iconst_1
        //     2843: if_icmpeq +46 -> 2889
        //     2846: aload_0
        //     2847: new 502	java/lang/StringBuilder
        //     2850: dup
        //     2851: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     2854: ldc_w 983
        //     2857: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2860: aload 19
        //     2862: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2865: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     2868: ldc_w 985
        //     2871: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2874: aload 20
        //     2876: arraylength
        //     2877: invokevirtual 514	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2880: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2883: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     2886: goto -2677 -> 209
        //     2889: aload_0
        //     2890: new 502	java/lang/StringBuilder
        //     2893: dup
        //     2894: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     2897: ldc_w 987
        //     2900: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2903: aload 20
        //     2905: iconst_0
        //     2906: iaload
        //     2907: invokevirtual 514	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2910: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2913: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     2916: aload_0
        //     2917: getfield 398	com/android/internal/telephony/IccRecords:mFh	Lcom/android/internal/telephony/IccFileHandler;
        //     2920: sipush 28476
        //     2923: aload 20
        //     2925: iconst_0
        //     2926: iaload
        //     2927: aload_0
        //     2928: bipush 22
        //     2930: invokevirtual 475	com/android/internal/telephony/gsm/SIMRecords:obtainMessage	(I)Landroid/os/Message;
        //     2933: invokevirtual 782	com/android/internal/telephony/IccFileHandler:loadEFLinearFixed	(IILandroid/os/Message;)V
        //     2936: goto -2727 -> 209
        //     2939: iconst_0
        //     2940: istore_2
        //     2941: aload_1
        //     2942: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     2945: checkcast 483	android/os/AsyncResult
        //     2948: astore 18
        //     2950: aload 18
        //     2952: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2955: ifnonnull +21 -> 2976
        //     2958: aload_0
        //     2959: aload 18
        //     2961: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     2964: checkcast 492	[B
        //     2967: checkcast 492	[B
        //     2970: invokespecial 989	com/android/internal/telephony/gsm/SIMRecords:handleSms	([B)V
        //     2973: goto -2764 -> 209
        //     2976: aload_0
        //     2977: new 502	java/lang/StringBuilder
        //     2980: dup
        //     2981: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     2984: ldc_w 991
        //     2987: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2990: aload 18
        //     2992: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     2995: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     2998: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3001: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     3004: goto -2795 -> 209
        //     3007: iconst_1
        //     3008: istore_2
        //     3009: aload_1
        //     3010: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     3013: checkcast 483	android/os/AsyncResult
        //     3016: astore 16
        //     3018: aload 16
        //     3020: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     3023: checkcast 492	[B
        //     3026: checkcast 492	[B
        //     3029: astore 17
        //     3031: aload 16
        //     3033: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3036: ifnonnull -2827 -> 209
        //     3039: aload_0
        //     3040: new 993	com/android/internal/telephony/gsm/UsimServiceTable
        //     3043: dup
        //     3044: aload 17
        //     3046: invokespecial 995	com/android/internal/telephony/gsm/UsimServiceTable:<init>	([B)V
        //     3049: putfield 823	com/android/internal/telephony/gsm/SIMRecords:mUsimServiceTable	Lcom/android/internal/telephony/gsm/UsimServiceTable;
        //     3052: aload_0
        //     3053: new 502	java/lang/StringBuilder
        //     3056: dup
        //     3057: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     3060: ldc_w 997
        //     3063: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3066: aload_0
        //     3067: getfield 823	com/android/internal/telephony/gsm/SIMRecords:mUsimServiceTable	Lcom/android/internal/telephony/gsm/UsimServiceTable;
        //     3070: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     3073: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3076: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3079: goto -2870 -> 209
        //     3082: iconst_1
        //     3083: istore_2
        //     3084: aload_1
        //     3085: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     3088: checkcast 483	android/os/AsyncResult
        //     3091: astore 15
        //     3093: aload 15
        //     3095: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3098: ifnonnull -2889 -> 209
        //     3101: aload_0
        //     3102: aload 15
        //     3104: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     3107: checkcast 492	[B
        //     3110: checkcast 492	[B
        //     3113: putfield 378	com/android/internal/telephony/gsm/SIMRecords:mCphsInfo	[B
        //     3116: aload_0
        //     3117: new 502	java/lang/StringBuilder
        //     3120: dup
        //     3121: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     3124: ldc_w 999
        //     3127: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3130: aload_0
        //     3131: getfield 378	com/android/internal/telephony/gsm/SIMRecords:mCphsInfo	[B
        //     3134: invokestatic 904	com/android/internal/telephony/IccUtils:bytesToHexString	([B)Ljava/lang/String;
        //     3137: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3140: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3143: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3146: goto -2937 -> 209
        //     3149: iconst_0
        //     3150: istore_2
        //     3151: aload_1
        //     3152: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     3155: checkcast 483	android/os/AsyncResult
        //     3158: astore 12
        //     3160: aload 12
        //     3162: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3165: ifnonnull +19 -> 3184
        //     3168: aload_0
        //     3169: aload_0
        //     3170: getfield 1002	com/android/internal/telephony/IccRecords:newVoiceMailNum	Ljava/lang/String;
        //     3173: putfield 741	com/android/internal/telephony/IccRecords:voiceMailNum	Ljava/lang/String;
        //     3176: aload_0
        //     3177: aload_0
        //     3178: getfield 1005	com/android/internal/telephony/IccRecords:newVoiceMailTag	Ljava/lang/String;
        //     3181: putfield 747	com/android/internal/telephony/IccRecords:voiceMailTag	Ljava/lang/String;
        //     3184: aload_0
        //     3185: invokespecial 1007	com/android/internal/telephony/gsm/SIMRecords:isCphsMailboxEnabled	()Z
        //     3188: ifeq +117 -> 3305
        //     3191: new 912	com/android/internal/telephony/AdnRecord
        //     3194: dup
        //     3195: aload_0
        //     3196: getfield 747	com/android/internal/telephony/IccRecords:voiceMailTag	Ljava/lang/String;
        //     3199: aload_0
        //     3200: getfield 741	com/android/internal/telephony/IccRecords:voiceMailNum	Ljava/lang/String;
        //     3203: invokespecial 1009	com/android/internal/telephony/AdnRecord:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     3206: astore 13
        //     3208: aload 12
        //     3210: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3213: checkcast 842	android/os/Message
        //     3216: astore 14
        //     3218: aload 12
        //     3220: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3223: ifnonnull +47 -> 3270
        //     3226: aload 12
        //     3228: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3231: ifnull +39 -> 3270
        //     3234: aload 12
        //     3236: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3239: checkcast 842	android/os/Message
        //     3242: invokestatic 936	android/os/AsyncResult:forMessage	(Landroid/os/Message;)Landroid/os/AsyncResult;
        //     3245: aconst_null
        //     3246: putfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3249: aload 12
        //     3251: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3254: checkcast 842	android/os/Message
        //     3257: invokevirtual 939	android/os/Message:sendToTarget	()V
        //     3260: aload_0
        //     3261: ldc_w 1011
        //     3264: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3267: aconst_null
        //     3268: astore 14
        //     3270: new 563	com/android/internal/telephony/AdnRecordLoader
        //     3273: dup
        //     3274: aload_0
        //     3275: getfield 398	com/android/internal/telephony/IccRecords:mFh	Lcom/android/internal/telephony/IccFileHandler;
        //     3278: invokespecial 564	com/android/internal/telephony/AdnRecordLoader:<init>	(Lcom/android/internal/telephony/IccFileHandler;)V
        //     3281: aload 13
        //     3283: sipush 28439
        //     3286: sipush 28490
        //     3289: iconst_1
        //     3290: aconst_null
        //     3291: aload_0
        //     3292: bipush 25
        //     3294: aload 14
        //     3296: invokevirtual 1014	com/android/internal/telephony/gsm/SIMRecords:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     3299: invokevirtual 1018	com/android/internal/telephony/AdnRecordLoader:updateEF	(Lcom/android/internal/telephony/AdnRecord;IIILjava/lang/String;Landroid/os/Message;)V
        //     3302: goto -3093 -> 209
        //     3305: aload 12
        //     3307: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3310: ifnull -3101 -> 209
        //     3313: aload 12
        //     3315: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3318: checkcast 842	android/os/Message
        //     3321: invokestatic 936	android/os/AsyncResult:forMessage	(Landroid/os/Message;)Landroid/os/AsyncResult;
        //     3324: aload 12
        //     3326: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3329: putfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3332: aload 12
        //     3334: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3337: checkcast 842	android/os/Message
        //     3340: invokevirtual 939	android/os/Message:sendToTarget	()V
        //     3343: goto -3134 -> 209
        //     3346: iconst_0
        //     3347: istore_2
        //     3348: aload_1
        //     3349: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     3352: checkcast 483	android/os/AsyncResult
        //     3355: astore 11
        //     3357: aload 11
        //     3359: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3362: ifnonnull +67 -> 3429
        //     3365: aload_0
        //     3366: aload_0
        //     3367: getfield 1002	com/android/internal/telephony/IccRecords:newVoiceMailNum	Ljava/lang/String;
        //     3370: putfield 741	com/android/internal/telephony/IccRecords:voiceMailNum	Ljava/lang/String;
        //     3373: aload_0
        //     3374: aload_0
        //     3375: getfield 1005	com/android/internal/telephony/IccRecords:newVoiceMailTag	Ljava/lang/String;
        //     3378: putfield 747	com/android/internal/telephony/IccRecords:voiceMailTag	Ljava/lang/String;
        //     3381: aload 11
        //     3383: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3386: ifnull -3177 -> 209
        //     3389: aload_0
        //     3390: ldc_w 1020
        //     3393: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3396: aload 11
        //     3398: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3401: checkcast 842	android/os/Message
        //     3404: invokestatic 936	android/os/AsyncResult:forMessage	(Landroid/os/Message;)Landroid/os/AsyncResult;
        //     3407: aload 11
        //     3409: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3412: putfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3415: aload 11
        //     3417: getfield 932	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     3420: checkcast 842	android/os/Message
        //     3423: invokevirtual 939	android/os/Message:sendToTarget	()V
        //     3426: goto -3217 -> 209
        //     3429: aload_0
        //     3430: new 502	java/lang/StringBuilder
        //     3433: dup
        //     3434: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     3437: ldc_w 1022
        //     3440: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3443: aload 11
        //     3445: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3448: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     3451: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3454: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3457: goto -76 -> 3381
        //     3460: iconst_0
        //     3461: istore_2
        //     3462: aload_1
        //     3463: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     3466: checkcast 483	android/os/AsyncResult
        //     3469: astore 10
        //     3471: aload_0
        //     3472: new 502	java/lang/StringBuilder
        //     3475: dup
        //     3476: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     3479: ldc_w 1024
        //     3482: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3485: aload 10
        //     3487: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3490: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     3493: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3496: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3499: aload 10
        //     3501: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3504: ifnonnull -3295 -> 209
        //     3507: aload_0
        //     3508: aload 10
        //     3510: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     3513: checkcast 579	com/android/internal/telephony/IccRefreshResponse
        //     3516: invokespecial 1026	com/android/internal/telephony/gsm/SIMRecords:handleSimRefresh	(Lcom/android/internal/telephony/IccRefreshResponse;)V
        //     3519: goto -3310 -> 209
        //     3522: iconst_1
        //     3523: istore_2
        //     3524: aload_1
        //     3525: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     3528: checkcast 483	android/os/AsyncResult
        //     3531: astore 7
        //     3533: aload 7
        //     3535: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     3538: checkcast 492	[B
        //     3541: checkcast 492	[B
        //     3544: astore 8
        //     3546: aload 7
        //     3548: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3551: ifnonnull -3342 -> 209
        //     3554: aload_0
        //     3555: new 502	java/lang/StringBuilder
        //     3558: dup
        //     3559: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     3562: ldc_w 1028
        //     3565: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3568: aload 8
        //     3570: invokestatic 904	com/android/internal/telephony/IccUtils:bytesToHexString	([B)Ljava/lang/String;
        //     3573: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3576: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3579: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3582: aload_0
        //     3583: aload 8
        //     3585: putfield 388	com/android/internal/telephony/gsm/SIMRecords:mEfCfis	[B
        //     3588: iconst_1
        //     3589: aload 8
        //     3591: iconst_1
        //     3592: baload
        //     3593: iand
        //     3594: ifeq +320 -> 3914
        //     3597: iconst_1
        //     3598: istore 9
        //     3600: aload_0
        //     3601: iload 9
        //     3603: putfield 826	com/android/internal/telephony/gsm/SIMRecords:callForwardingEnabled	Z
        //     3606: aload_0
        //     3607: getfield 949	com/android/internal/telephony/IccRecords:mRecordsEventsRegistrants	Landroid/os/RegistrantList;
        //     3610: iconst_1
        //     3611: invokestatic 953	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     3614: invokevirtual 754	android/os/RegistrantList:notifyResult	(Ljava/lang/Object;)V
        //     3617: goto -3408 -> 209
        //     3620: iconst_1
        //     3621: istore_2
        //     3622: aload_1
        //     3623: getfield 866	android/os/Message:obj	Ljava/lang/Object;
        //     3626: checkcast 483	android/os/AsyncResult
        //     3629: astore 5
        //     3631: aload 5
        //     3633: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3636: ifnull +34 -> 3670
        //     3639: aload_0
        //     3640: new 502	java/lang/StringBuilder
        //     3643: dup
        //     3644: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     3647: ldc_w 1030
        //     3650: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3653: aload 5
        //     3655: getfield 487	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     3658: invokevirtual 760	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     3661: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3664: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     3667: goto -3458 -> 209
        //     3670: aload 5
        //     3672: getfield 491	android/os/AsyncResult:result	Ljava/lang/Object;
        //     3675: checkcast 492	[B
        //     3678: checkcast 492	[B
        //     3681: astore 6
        //     3683: aload_0
        //     3684: new 502	java/lang/StringBuilder
        //     3687: dup
        //     3688: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     3691: ldc_w 1032
        //     3694: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3697: aload 6
        //     3699: invokestatic 904	com/android/internal/telephony/IccUtils:bytesToHexString	([B)Ljava/lang/String;
        //     3702: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3705: invokevirtual 518	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3708: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3711: aload_0
        //     3712: aload 6
        //     3714: invokespecial 1034	com/android/internal/telephony/gsm/SIMRecords:handleEfCspData	([B)V
        //     3717: goto -3508 -> 209
        //     3720: aload_0
        //     3721: iconst_0
        //     3722: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3725: aload_0
        //     3726: ldc_w 964
        //     3729: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3732: goto -1324 -> 2408
        //     3735: astore 35
        //     3737: aload_0
        //     3738: iconst_0
        //     3739: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3742: aload_0
        //     3743: ldc_w 898
        //     3746: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     3749: goto -1341 -> 2408
        //     3752: aload_0
        //     3753: iconst_0
        //     3754: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3757: aload_0
        //     3758: ldc_w 964
        //     3761: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3764: goto -2147 -> 1617
        //     3767: astore 62
        //     3769: aload_0
        //     3770: iconst_0
        //     3771: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3774: aload_0
        //     3775: ldc_w 898
        //     3778: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     3781: goto -2164 -> 1617
        //     3784: aload_0
        //     3785: iconst_0
        //     3786: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3789: aload_0
        //     3790: ldc_w 964
        //     3793: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3796: goto -1960 -> 1836
        //     3799: astore 56
        //     3801: aload_0
        //     3802: iconst_0
        //     3803: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3806: aload_0
        //     3807: ldc_w 898
        //     3810: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     3813: goto -1977 -> 1836
        //     3816: aload_0
        //     3817: iconst_0
        //     3818: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3821: aload_0
        //     3822: ldc_w 964
        //     3825: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3828: goto -1801 -> 2027
        //     3831: astore 50
        //     3833: aload_0
        //     3834: iconst_0
        //     3835: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3838: aload_0
        //     3839: ldc_w 898
        //     3842: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     3845: goto -1818 -> 2027
        //     3848: aload_0
        //     3849: iconst_0
        //     3850: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3853: aload_0
        //     3854: ldc_w 964
        //     3857: invokevirtual 522	com/android/internal/telephony/gsm/SIMRecords:log	(Ljava/lang/String;)V
        //     3860: goto -1631 -> 2229
        //     3863: astore 44
        //     3865: aload_0
        //     3866: iconst_0
        //     3867: putfield 812	com/android/internal/telephony/IccRecords:mncLength	I
        //     3870: aload_0
        //     3871: ldc_w 898
        //     3874: invokevirtual 852	com/android/internal/telephony/gsm/SIMRecords:loge	(Ljava/lang/String;)V
        //     3877: goto -1648 -> 2229
        //     3880: iinc 93 1
        //     3883: goto -3436 -> 447
        //     3886: ldc_w 1036
        //     3889: astore 83
        //     3891: goto -3083 -> 808
        //     3894: ldc_w 1038
        //     3897: astore 81
        //     3899: goto -2984 -> 915
        //     3902: iconst_0
        //     3903: istore 74
        //     3905: goto -2677 -> 1228
        //     3908: iconst_0
        //     3909: istore 32
        //     3911: goto -1360 -> 2551
        //     3914: iconst_0
        //     3915: istore 9
        //     3917: goto -317 -> 3600
        //     3920: iinc 39 1
        //     3923: goto -1586 -> 2337
        //     3926: iinc 66 1
        //     3929: goto -2383 -> 1546
        //     3932: iinc 60 1
        //     3935: goto -2170 -> 1765
        //     3938: iinc 54 1
        //     3941: goto -1985 -> 1956
        //     3944: iinc 48 1
        //     3947: goto -1789 -> 2158
        //
        // Exception table:
        //     from	to	target	type
        //     59	209	227	java/lang/RuntimeException
        //     220	224	227	java/lang/RuntimeException
        //     247	292	227	java/lang/RuntimeException
        //     306	478	227	java/lang/RuntimeException
        //     483	502	227	java/lang/RuntimeException
        //     502	1439	227	java/lang/RuntimeException
        //     1478	1593	227	java/lang/RuntimeException
        //     1598	1617	227	java/lang/RuntimeException
        //     1617	1652	227	java/lang/RuntimeException
        //     1697	1812	227	java/lang/RuntimeException
        //     1817	1836	227	java/lang/RuntimeException
        //     1836	1871	227	java/lang/RuntimeException
        //     1888	2003	227	java/lang/RuntimeException
        //     2008	2027	227	java/lang/RuntimeException
        //     2027	2062	227	java/lang/RuntimeException
        //     2090	2205	227	java/lang/RuntimeException
        //     2210	2229	227	java/lang/RuntimeException
        //     2229	2384	227	java/lang/RuntimeException
        //     2389	2408	227	java/lang/RuntimeException
        //     2408	3877	227	java/lang/RuntimeException
        //     59	209	295	finally
        //     220	224	295	finally
        //     229	238	295	finally
        //     247	292	295	finally
        //     306	478	295	finally
        //     483	502	295	finally
        //     502	1439	295	finally
        //     1478	1593	295	finally
        //     1598	1617	295	finally
        //     1617	1652	295	finally
        //     1697	1812	295	finally
        //     1817	1836	295	finally
        //     1836	1871	295	finally
        //     1888	2003	295	finally
        //     2008	2027	295	finally
        //     2027	2062	295	finally
        //     2090	2205	295	finally
        //     2210	2229	295	finally
        //     2229	2384	295	finally
        //     2389	2408	295	finally
        //     2408	3877	295	finally
        //     483	502	553	java/lang/NumberFormatException
        //     1444	1473	2267	finally
        //     1655	1697	2267	finally
        //     1874	1888	2267	finally
        //     2065	2090	2267	finally
        //     2389	2408	3735	java/lang/NumberFormatException
        //     1598	1617	3767	java/lang/NumberFormatException
        //     1817	1836	3799	java/lang/NumberFormatException
        //     2008	2027	3831	java/lang/NumberFormatException
        //     2210	2229	3863	java/lang/NumberFormatException
    }

    public boolean isCspPlmnEnabled()
    {
        return this.mCspPlmnEnabled;
    }

    protected void log(String paramString)
    {
        Log.d("GSM", "[SIMRecords] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("GSM", "[SIMRecords] " + paramString);
    }

    protected void logv(String paramString)
    {
        Log.v("GSM", "[SIMRecords] " + paramString);
    }

    protected void logw(String paramString, Throwable paramThrowable)
    {
        Log.w("GSM", "[SIMRecords] " + paramString, paramThrowable);
    }

    protected void onAllRecordsLoaded()
    {
        String str = getOperatorNumeric();
        log("SIMRecords: onAllRecordsLoaded set 'gsm.sim.operator.numeric' to operator='" + str + "'");
        SystemProperties.set("gsm.sim.operator.numeric", str);
        if (this.imsi != null)
            SystemProperties.set("gsm.sim.operator.iso-country", MccTable.countryCodeForMcc(Integer.parseInt(this.imsi.substring(0, 3))));
        while (true)
        {
            setVoiceMailByCountry(str);
            setSpnFromConfig(str);
            this.recordsLoadedRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
            this.mParentCard.broadcastIccStateChangedIntent("LOADED", null);
            return;
            loge("onAllRecordsLoaded: imsi is NULL!");
        }
    }

    protected void onRadioOffOrNotAvailable()
    {
        this.imsi = null;
        this.msisdn = null;
        this.voiceMailNum = null;
        this.countVoiceMessages = 0;
        this.mncLength = -1;
        this.iccid = null;
        this.spnDisplayCondition = -1;
        this.efMWIS = null;
        this.efCPHS_MWI = null;
        this.spdiNetworks = null;
        this.pnnHomeName = null;
        this.adnCache.reset();
        log("SIMRecords: onRadioOffOrNotAvailable set 'gsm.sim.operator.numeric' to operator=null");
        SystemProperties.set("gsm.sim.operator.numeric", null);
        SystemProperties.set("gsm.sim.operator.alpha", null);
        SystemProperties.set("gsm.sim.operator.iso-country", null);
        this.recordsRequested = false;
    }

    public void onReady()
    {
        this.mParentCard.broadcastIccStateChangedIntent("READY", null);
        fetchSimRecords();
    }

    protected void onRecordLoaded()
    {
        this.recordsToLoad = (-1 + this.recordsToLoad);
        log("onRecordLoaded " + this.recordsToLoad + " requested: " + this.recordsRequested);
        if ((this.recordsToLoad == 0) && (this.recordsRequested == true))
            onAllRecordsLoaded();
        while (true)
        {
            return;
            if (this.recordsToLoad < 0)
            {
                loge("recordsToLoad <0, programmer error suspected");
                this.recordsToLoad = 0;
            }
        }
    }

    public void onRefresh(boolean paramBoolean, int[] paramArrayOfInt)
    {
        if (paramBoolean)
            fetchSimRecords();
    }

    public void setMsisdnNumber(String paramString1, String paramString2, Message paramMessage)
    {
        this.msisdn = paramString2;
        this.msisdnTag = paramString1;
        log("Set MSISDN: " + this.msisdnTag + " " + "xxxxxxx");
        AdnRecord localAdnRecord = new AdnRecord(this.msisdnTag, this.msisdn);
        new AdnRecordLoader(this.mFh).updateEF(localAdnRecord, 28480, 28490, 1, null, obtainMessage(30, paramMessage));
    }

    public void setVoiceCallForwardingFlag(int paramInt, boolean paramBoolean)
    {
        if (paramInt != 1)
            return;
        this.callForwardingEnabled = paramBoolean;
        this.mRecordsEventsRegistrants.notifyResult(Integer.valueOf(1));
        while (true)
        {
            try
            {
                if (this.mEfCfis != null)
                {
                    if (!paramBoolean)
                        break label150;
                    byte[] arrayOfByte2 = this.mEfCfis;
                    arrayOfByte2[1] = ((byte)(0x1 | arrayOfByte2[1]));
                    this.mFh.updateEFLinearFixed(28619, 1, this.mEfCfis, null, obtainMessage(14, Integer.valueOf(28619)));
                }
                if (this.mEfCff == null)
                    break;
                if (!paramBoolean)
                    break label172;
                this.mEfCff[0] = ((byte)(0xA | 0xF0 & this.mEfCff[0]));
                this.mFh.updateEFTransparent(28435, this.mEfCff, obtainMessage(14, Integer.valueOf(28435)));
            }
            catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
            {
                logw("Error saving call fowarding flag to SIM. Probably malformed SIM record", localArrayIndexOutOfBoundsException);
            }
            break;
            label150: byte[] arrayOfByte1 = this.mEfCfis;
            arrayOfByte1[1] = ((byte)(0xFE & arrayOfByte1[1]));
            continue;
            label172: this.mEfCff[0] = ((byte)(0x5 | 0xF0 & this.mEfCff[0]));
        }
    }

    public void setVoiceMailNumber(String paramString1, String paramString2, Message paramMessage)
    {
        if (this.isVoiceMailFixed)
        {
            AsyncResult.forMessage(paramMessage).exception = new IccVmFixedException("Voicemail number is fixed by operator");
            paramMessage.sendToTarget();
        }
        while (true)
        {
            return;
            this.newVoiceMailNum = paramString2;
            this.newVoiceMailTag = paramString1;
            AdnRecord localAdnRecord = new AdnRecord(this.newVoiceMailTag, this.newVoiceMailNum);
            if ((this.mailboxIndex != 0) && (this.mailboxIndex != 255))
            {
                new AdnRecordLoader(this.mFh).updateEF(localAdnRecord, 28615, 28616, this.mailboxIndex, null, obtainMessage(20, paramMessage));
            }
            else if (isCphsMailboxEnabled())
            {
                new AdnRecordLoader(this.mFh).updateEF(localAdnRecord, 28439, 28490, 1, null, obtainMessage(25, paramMessage));
            }
            else
            {
                AsyncResult.forMessage(paramMessage).exception = new IccVmNotSupportedException("Update SIM voice mailbox error");
                paramMessage.sendToTarget();
            }
        }
    }

    public void setVoiceMessageWaiting(int paramInt1, int paramInt2)
    {
        int i = 0;
        if (paramInt1 != 1)
            return;
        if (paramInt2 < 0)
        {
            paramInt2 = -1;
            label15: this.countVoiceMessages = paramInt2;
            this.mRecordsEventsRegistrants.notifyResult(Integer.valueOf(0));
        }
        while (true)
        {
            try
            {
                if (this.efMWIS != null)
                {
                    byte[] arrayOfByte2 = this.efMWIS;
                    int m = 0xFE & this.efMWIS[0];
                    if (this.countVoiceMessages != 0)
                        break label213;
                    arrayOfByte2[0] = ((byte)(i | m));
                    if (paramInt2 >= 0)
                        break label218;
                    this.efMWIS[1] = 0;
                    this.mFh.updateEFLinearFixed(28618, 1, this.efMWIS, null, obtainMessage(14, Integer.valueOf(28618)));
                }
                if (this.efCPHS_MWI == null)
                    break;
                byte[] arrayOfByte1 = this.efCPHS_MWI;
                int j = 0xF0 & this.efCPHS_MWI[0];
                if (this.countVoiceMessages != 0)
                    break label229;
                k = 5;
                arrayOfByte1[0] = ((byte)(k | j));
                this.mFh.updateEFTransparent(28433, this.efCPHS_MWI, obtainMessage(14, Integer.valueOf(28433)));
            }
            catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
            {
                logw("Error saving voice mail state to SIM. Probably malformed SIM record", localArrayIndexOutOfBoundsException);
            }
            break;
            if (paramInt2 <= 255)
                break label15;
            paramInt2 = 255;
            break label15;
            label213: i = 1;
            continue;
            label218: this.efMWIS[1] = ((byte)paramInt2);
            continue;
            label229: int k = 10;
        }
    }

    private static enum Get_Spn_Fsm_State
    {
        static
        {
            Get_Spn_Fsm_State[] arrayOfGet_Spn_Fsm_State = new Get_Spn_Fsm_State[5];
            arrayOfGet_Spn_Fsm_State[0] = IDLE;
            arrayOfGet_Spn_Fsm_State[1] = INIT;
            arrayOfGet_Spn_Fsm_State[2] = READ_SPN_3GPP;
            arrayOfGet_Spn_Fsm_State[3] = READ_SPN_CPHS;
            arrayOfGet_Spn_Fsm_State[4] = READ_SPN_SHORT_CPHS;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean isMatchingOperator(SIMRecords paramSIMRecords, String paramString1, String paramString2)
        {
            boolean bool = true;
            if (paramString1.equals(paramString2));
            while (true)
            {
                return bool;
                if (!MiuiSpnOverride.getInstance().getEquivalentOperatorNumeric(paramString1).equals(MiuiSpnOverride.getInstance().getEquivalentOperatorNumeric(paramSIMRecords.getOperatorNumeric())))
                    bool = false;
            }
        }

        static void updateSpnDisplayCondition(SIMRecords paramSIMRecords)
        {
            if (!TextUtils.isEmpty(paramSIMRecords.getSpn()))
                paramSIMRecords.spnDisplayCondition = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.SIMRecords
 * JD-Core Version:        0.6.2
 */