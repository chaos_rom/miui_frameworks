package com.android.internal.telephony.gsm;

import com.android.internal.telephony.Call;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.DriverCall;
import com.android.internal.telephony.DriverCall.State;
import com.android.internal.telephony.Phone;
import java.util.ArrayList;
import java.util.List;

class GsmCall extends Call
{
    ArrayList<Connection> connections = new ArrayList();
    GsmCallTracker owner;

    GsmCall(GsmCallTracker paramGsmCallTracker)
    {
        this.owner = paramGsmCallTracker;
    }

    static Call.State stateFromDCState(DriverCall.State paramState)
    {
        Call.State localState;
        switch (1.$SwitchMap$com$android$internal$telephony$DriverCall$State[paramState.ordinal()])
        {
        default:
            throw new RuntimeException("illegal call state:" + paramState);
        case 1:
            localState = Call.State.ACTIVE;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return localState;
            localState = Call.State.HOLDING;
            continue;
            localState = Call.State.DIALING;
            continue;
            localState = Call.State.ALERTING;
            continue;
            localState = Call.State.INCOMING;
            continue;
            localState = Call.State.WAITING;
        }
    }

    void attach(Connection paramConnection, DriverCall paramDriverCall)
    {
        this.connections.add(paramConnection);
        this.state = stateFromDCState(paramDriverCall.state);
    }

    void attachFake(Connection paramConnection, Call.State paramState)
    {
        this.connections.add(paramConnection);
        this.state = paramState;
    }

    void clearDisconnected()
    {
        for (int i = -1 + this.connections.size(); i >= 0; i--)
            if (((GsmConnection)this.connections.get(i)).getState() == Call.State.DISCONNECTED)
                this.connections.remove(i);
        if (this.connections.size() == 0)
            this.state = Call.State.IDLE;
    }

    void connectionDisconnected(GsmConnection paramGsmConnection)
    {
        int i;
        int j;
        int k;
        if (this.state != Call.State.DISCONNECTED)
        {
            i = 1;
            j = 0;
            k = this.connections.size();
        }
        while (true)
        {
            if (j < k)
            {
                if (((Connection)this.connections.get(j)).getState() != Call.State.DISCONNECTED)
                    i = 0;
            }
            else
            {
                if (i != 0)
                    this.state = Call.State.DISCONNECTED;
                return;
            }
            j++;
        }
    }

    void detach(GsmConnection paramGsmConnection)
    {
        this.connections.remove(paramGsmConnection);
        if (this.connections.size() == 0)
            this.state = Call.State.IDLE;
    }

    public void dispose()
    {
    }

    public List<Connection> getConnections()
    {
        return this.connections;
    }

    public Phone getPhone()
    {
        return this.owner.phone;
    }

    public void hangup()
        throws CallStateException
    {
        this.owner.hangup(this);
    }

    boolean isFull()
    {
        if (this.connections.size() == 5);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isMultiparty()
    {
        int i = 1;
        if (this.connections.size() > i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    void onHangupLocal()
    {
        int i = 0;
        int j = this.connections.size();
        while (i < j)
        {
            ((GsmConnection)this.connections.get(i)).onHangupLocal();
            i++;
        }
        this.state = Call.State.DISCONNECTING;
    }

    public String toString()
    {
        return this.state.toString();
    }

    boolean update(GsmConnection paramGsmConnection, DriverCall paramDriverCall)
    {
        boolean bool = false;
        Call.State localState = stateFromDCState(paramDriverCall.state);
        if (localState != this.state)
        {
            this.state = localState;
            bool = true;
        }
        return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmCall
 * JD-Core Version:        0.6.2
 */