package com.android.internal.telephony.gsm;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Registrant;
import android.os.SystemClock;
import android.telephony.PhoneNumberUtils;
import android.telephony.ServiceState;
import android.util.Log;
import com.android.internal.telephony.Call;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CallTracker;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.Connection.DisconnectCause;
import com.android.internal.telephony.Connection.PostDialState;
import com.android.internal.telephony.DriverCall;
import com.android.internal.telephony.DriverCall.State;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.telephony.RestrictedState;
import com.android.internal.telephony.ServiceStateTracker;
import com.android.internal.telephony.UUSInfo;

public class GsmConnection extends Connection
{
    static final int EVENT_DTMF_DONE = 1;
    static final int EVENT_NEXT_POST_DIAL = 3;
    static final int EVENT_PAUSE_DONE = 2;
    static final int EVENT_WAKE_LOCK_TIMEOUT = 4;
    static final String LOG_TAG = "GSM";
    static final int PAUSE_DELAY_FIRST_MILLIS = 100;
    static final int PAUSE_DELAY_MILLIS = 3000;
    static final int WAKE_LOCK_TIMEOUT_MILLIS = 60000;
    String address;
    Connection.DisconnectCause cause = Connection.DisconnectCause.NOT_DISCONNECTED;
    long connectTime;
    long connectTimeReal;
    long createTime;
    String dialString;
    long disconnectTime;
    boolean disconnected;
    long duration;
    Handler h;
    long holdingStartTime;
    int index;
    boolean isIncoming;
    private PowerManager.WakeLock mPartialWakeLock;
    int nextPostDialChar;
    int numberPresentation = Connection.PRESENTATION_ALLOWED;
    GsmCallTracker owner;
    GsmCall parent;
    Connection.PostDialState postDialState = Connection.PostDialState.NOT_STARTED;
    String postDialString;
    UUSInfo uusInfo;

    GsmConnection(Context paramContext, DriverCall paramDriverCall, GsmCallTracker paramGsmCallTracker, int paramInt)
    {
        createWakeLock(paramContext);
        acquireWakeLock();
        this.owner = paramGsmCallTracker;
        this.h = new MyHandler(this.owner.getLooper());
        this.address = paramDriverCall.number;
        this.isIncoming = paramDriverCall.isMT;
        this.createTime = System.currentTimeMillis();
        this.numberPresentation = paramDriverCall.numberPresentation;
        this.uusInfo = paramDriverCall.uusInfo;
        this.index = paramInt;
        this.parent = parentFromDCState(paramDriverCall.state);
        this.parent.attach(this, paramDriverCall);
    }

    GsmConnection(Context paramContext, String paramString, GsmCallTracker paramGsmCallTracker, GsmCall paramGsmCall)
    {
        createWakeLock(paramContext);
        acquireWakeLock();
        this.owner = paramGsmCallTracker;
        this.h = new MyHandler(this.owner.getLooper());
        this.dialString = paramString;
        this.address = PhoneNumberUtils.extractNetworkPortionAlt(paramString);
        this.postDialString = PhoneNumberUtils.extractPostDialPortion(paramString);
        this.index = -1;
        this.isIncoming = false;
        this.createTime = System.currentTimeMillis();
        this.parent = paramGsmCall;
        paramGsmCall.attachFake(this, Call.State.DIALING);
    }

    private void acquireWakeLock()
    {
        log("acquireWakeLock");
        this.mPartialWakeLock.acquire();
    }

    private void createWakeLock(Context paramContext)
    {
        this.mPartialWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "GSM");
    }

    static boolean equalsHandlesNulls(Object paramObject1, Object paramObject2)
    {
        boolean bool;
        if (paramObject1 == null)
            if (paramObject2 == null)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = paramObject1.equals(paramObject2);
        }
    }

    private boolean isConnectingInOrOut()
    {
        if ((this.parent == null) || (this.parent == this.owner.ringingCall) || (this.parent.state == Call.State.DIALING) || (this.parent.state == Call.State.ALERTING));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void log(String paramString)
    {
        Log.d("GSM", "[GSMConn] " + paramString);
    }

    private void onStartedHolding()
    {
        this.holdingStartTime = SystemClock.elapsedRealtime();
    }

    private GsmCall parentFromDCState(DriverCall.State paramState)
    {
        GsmCall localGsmCall;
        switch (1.$SwitchMap$com$android$internal$telephony$DriverCall$State[paramState.ordinal()])
        {
        default:
            throw new RuntimeException("illegal call state: " + paramState);
        case 1:
        case 2:
        case 3:
            localGsmCall = this.owner.foregroundCall;
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return localGsmCall;
            localGsmCall = this.owner.backgroundCall;
            continue;
            localGsmCall = this.owner.ringingCall;
        }
    }

    private void processNextPostDialChar()
    {
        if (this.postDialState == Connection.PostDialState.CANCELLED);
        while (true)
        {
            return;
            char c;
            if ((this.postDialString == null) || (this.postDialString.length() <= this.nextPostDialChar))
            {
                setPostDialState(Connection.PostDialState.COMPLETE);
                c = '\000';
            }
            do
            {
                Registrant localRegistrant = this.owner.phone.mPostDialHandler;
                if (localRegistrant == null)
                    break;
                Message localMessage = localRegistrant.messageForRegistrant();
                if (localMessage == null)
                    break;
                Connection.PostDialState localPostDialState = this.postDialState;
                AsyncResult localAsyncResult = AsyncResult.forMessage(localMessage);
                localAsyncResult.result = this;
                localAsyncResult.userObj = localPostDialState;
                localMessage.arg1 = c;
                localMessage.sendToTarget();
                break;
                setPostDialState(Connection.PostDialState.STARTED);
                String str = this.postDialString;
                int i = this.nextPostDialChar;
                this.nextPostDialChar = (i + 1);
                c = str.charAt(i);
            }
            while (processPostDialChar(c));
            this.h.obtainMessage(3).sendToTarget();
            Log.e("GSM", "processNextPostDialChar: c=" + c + " isn't valid!");
        }
    }

    private boolean processPostDialChar(char paramChar)
    {
        int i = 1;
        if (PhoneNumberUtils.is12Key(paramChar))
            this.owner.cm.sendDtmf(paramChar, this.h.obtainMessage(i));
        while (true)
        {
            return i;
            if (paramChar == ',')
            {
                if (this.nextPostDialChar == i)
                    this.h.sendMessageDelayed(this.h.obtainMessage(2), 100L);
                else
                    this.h.sendMessageDelayed(this.h.obtainMessage(2), 3000L);
            }
            else if (paramChar == ';')
                setPostDialState(Connection.PostDialState.WAIT);
            else if (paramChar == 'N')
                setPostDialState(Connection.PostDialState.WILD);
            else
                int j = 0;
        }
    }

    private void releaseWakeLock()
    {
        synchronized (this.mPartialWakeLock)
        {
            if (this.mPartialWakeLock.isHeld())
            {
                log("releaseWakeLock");
                this.mPartialWakeLock.release();
            }
            return;
        }
    }

    private void setPostDialState(Connection.PostDialState paramPostDialState)
    {
        if ((this.postDialState != Connection.PostDialState.STARTED) && (paramPostDialState == Connection.PostDialState.STARTED))
        {
            acquireWakeLock();
            Message localMessage = this.h.obtainMessage(4);
            this.h.sendMessageDelayed(localMessage, 60000L);
        }
        while (true)
        {
            this.postDialState = paramPostDialState;
            return;
            if ((this.postDialState == Connection.PostDialState.STARTED) && (paramPostDialState != Connection.PostDialState.STARTED))
            {
                this.h.removeMessages(4);
                releaseWakeLock();
            }
        }
    }

    public void cancelPostDial()
    {
        setPostDialState(Connection.PostDialState.CANCELLED);
    }

    boolean compareTo(DriverCall paramDriverCall)
    {
        boolean bool = true;
        if ((!this.isIncoming) && (!paramDriverCall.isMT));
        while (true)
        {
            return bool;
            String str = PhoneNumberUtils.stringFromStringAndTOA(paramDriverCall.number, paramDriverCall.TOA);
            if ((this.isIncoming != paramDriverCall.isMT) || (!equalsHandlesNulls(this.address, str)))
                bool = false;
        }
    }

    Connection.DisconnectCause disconnectCauseFromCode(int paramInt)
    {
        GSMPhone localGSMPhone;
        int i;
        Connection.DisconnectCause localDisconnectCause;
        switch (paramInt)
        {
        default:
            localGSMPhone = this.owner.phone;
            i = localGSMPhone.getServiceState().getState();
            if (i == 3)
                localDisconnectCause = Connection.DisconnectCause.POWER_OFF;
            break;
        case 17:
        case 34:
        case 41:
        case 42:
        case 44:
        case 49:
        case 58:
        case 68:
        case 240:
        case 241:
        case 1:
        }
        while (true)
        {
            return localDisconnectCause;
            localDisconnectCause = Connection.DisconnectCause.BUSY;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CONGESTION;
            continue;
            localDisconnectCause = Connection.DisconnectCause.LIMIT_EXCEEDED;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CALL_BARRED;
            continue;
            localDisconnectCause = Connection.DisconnectCause.FDN_BLOCKED;
            continue;
            localDisconnectCause = Connection.DisconnectCause.UNOBTAINABLE_NUMBER;
            continue;
            if ((i == 1) || (i == 2))
                localDisconnectCause = Connection.DisconnectCause.OUT_OF_SERVICE;
            else if (localGSMPhone.getIccCard().getState() != IccCard.State.READY)
                localDisconnectCause = Connection.DisconnectCause.ICC_ERROR;
            else if (paramInt == 65535)
            {
                if (localGSMPhone.mSST.mRestrictedState.isCsRestricted())
                    localDisconnectCause = Connection.DisconnectCause.CS_RESTRICTED;
                else if (localGSMPhone.mSST.mRestrictedState.isCsEmergencyRestricted())
                    localDisconnectCause = Connection.DisconnectCause.CS_RESTRICTED_EMERGENCY;
                else if (localGSMPhone.mSST.mRestrictedState.isCsNormalRestricted())
                    localDisconnectCause = Connection.DisconnectCause.CS_RESTRICTED_NORMAL;
                else
                    localDisconnectCause = Connection.DisconnectCause.ERROR_UNSPECIFIED;
            }
            else if (paramInt == 16)
                localDisconnectCause = Connection.DisconnectCause.NORMAL;
            else
                localDisconnectCause = Connection.DisconnectCause.ERROR_UNSPECIFIED;
        }
    }

    public void dispose()
    {
    }

    void fakeHoldBeforeDial()
    {
        if (this.parent != null)
            this.parent.detach(this);
        this.parent = this.owner.backgroundCall;
        this.parent.attachFake(this, Call.State.HOLDING);
        onStartedHolding();
    }

    protected void finalize()
    {
        if (this.mPartialWakeLock.isHeld())
            Log.e("GSM", "[GSMConn] UNEXPECTED; mPartialWakeLock is held when finalizing.");
        releaseWakeLock();
    }

    public String getAddress()
    {
        return this.address;
    }

    public GsmCall getCall()
    {
        return this.parent;
    }

    public long getConnectTime()
    {
        return this.connectTime;
    }

    public long getCreateTime()
    {
        return this.createTime;
    }

    public Connection.DisconnectCause getDisconnectCause()
    {
        return this.cause;
    }

    public long getDisconnectTime()
    {
        return this.disconnectTime;
    }

    public long getDurationMillis()
    {
        long l = 0L;
        if (this.connectTimeReal == l);
        while (true)
        {
            return l;
            if (this.duration == l)
                l = SystemClock.elapsedRealtime() - this.connectTimeReal;
            else
                l = this.duration;
        }
    }

    int getGSMIndex()
        throws CallStateException
    {
        if (this.index >= 0)
            return 1 + this.index;
        throw new CallStateException("GSM index not yet assigned");
    }

    public long getHoldDurationMillis()
    {
        if (getState() != Call.State.HOLDING);
        for (long l = 0L; ; l = SystemClock.elapsedRealtime() - this.holdingStartTime)
            return l;
    }

    public int getNumberPresentation()
    {
        return this.numberPresentation;
    }

    public Connection.PostDialState getPostDialState()
    {
        return this.postDialState;
    }

    public String getRemainingPostDialString()
    {
        if ((this.postDialState == Connection.PostDialState.CANCELLED) || (this.postDialState == Connection.PostDialState.COMPLETE) || (this.postDialString == null) || (this.postDialString.length() <= this.nextPostDialChar));
        for (String str = ""; ; str = this.postDialString.substring(this.nextPostDialChar))
            return str;
    }

    public Call.State getState()
    {
        if (this.disconnected);
        for (Call.State localState = Call.State.DISCONNECTED; ; localState = super.getState())
            return localState;
    }

    public UUSInfo getUUSInfo()
    {
        return this.uusInfo;
    }

    public void hangup()
        throws CallStateException
    {
        if (!this.disconnected)
        {
            this.owner.hangup(this);
            return;
        }
        throw new CallStateException("disconnected");
    }

    public boolean isIncoming()
    {
        return this.isIncoming;
    }

    void onConnectedInOrOut()
    {
        this.connectTime = System.currentTimeMillis();
        this.connectTimeReal = SystemClock.elapsedRealtime();
        this.duration = 0L;
        log("onConnectedInOrOut: connectTime=" + this.connectTime);
        if (!this.isIncoming)
            processNextPostDialChar();
        releaseWakeLock();
    }

    void onDisconnect(Connection.DisconnectCause paramDisconnectCause)
    {
        this.cause = paramDisconnectCause;
        if (!this.disconnected)
        {
            this.index = -1;
            this.disconnectTime = System.currentTimeMillis();
            this.duration = (SystemClock.elapsedRealtime() - this.connectTimeReal);
            this.disconnected = true;
            this.owner.phone.notifyDisconnect(this);
            if (this.parent != null)
                this.parent.connectionDisconnected(this);
        }
        releaseWakeLock();
    }

    void onHangupLocal()
    {
        this.cause = Connection.DisconnectCause.LOCAL;
    }

    void onRemoteDisconnect(int paramInt)
    {
        onDisconnect(disconnectCauseFromCode(paramInt));
    }

    public void proceedAfterWaitChar()
    {
        if (this.postDialState != Connection.PostDialState.WAIT)
            Log.w("GSM", "GsmConnection.proceedAfterWaitChar(): Expected getPostDialState() to be WAIT but was " + this.postDialState);
        while (true)
        {
            return;
            setPostDialState(Connection.PostDialState.STARTED);
            processNextPostDialChar();
        }
    }

    public void proceedAfterWildChar(String paramString)
    {
        if (this.postDialState != Connection.PostDialState.WILD)
            Log.w("GSM", "GsmConnection.proceedAfterWaitChar(): Expected getPostDialState() to be WILD but was " + this.postDialState);
        while (true)
        {
            return;
            setPostDialState(Connection.PostDialState.STARTED);
            StringBuilder localStringBuilder = new StringBuilder(paramString);
            localStringBuilder.append(this.postDialString.substring(this.nextPostDialChar));
            this.postDialString = localStringBuilder.toString();
            this.nextPostDialChar = 0;
            log("proceedAfterWildChar: new postDialString is " + this.postDialString);
            processNextPostDialChar();
        }
    }

    public void separate()
        throws CallStateException
    {
        if (!this.disconnected)
        {
            this.owner.separate(this);
            return;
        }
        throw new CallStateException("disconnected");
    }

    boolean update(DriverCall paramDriverCall)
    {
        boolean bool1 = true;
        int i = 0;
        boolean bool2 = isConnectingInOrOut();
        boolean bool3;
        boolean bool5;
        StringBuilder localStringBuilder;
        if (getState() == Call.State.HOLDING)
        {
            bool3 = bool1;
            GsmCall localGsmCall = parentFromDCState(paramDriverCall.state);
            if (!equalsHandlesNulls(this.address, paramDriverCall.number))
            {
                log("update: phone # changed!");
                this.address = paramDriverCall.number;
                i = 1;
            }
            if (localGsmCall == this.parent)
                break label249;
            if (this.parent != null)
                this.parent.detach(this);
            localGsmCall.attach(this, paramDriverCall);
            this.parent = localGsmCall;
            bool5 = true;
            localStringBuilder = new StringBuilder().append("update: parent=").append(this.parent).append(", hasNewParent=");
            if (localGsmCall == this.parent)
                break label281;
        }
        while (true)
        {
            log(bool1 + ", wasConnectingInOrOut=" + bool2 + ", wasHolding=" + bool3 + ", isConnectingInOrOut=" + isConnectingInOrOut() + ", changed=" + bool5);
            if ((bool2) && (!isConnectingInOrOut()))
                onConnectedInOrOut();
            if ((bool5) && (!bool3) && (getState() == Call.State.HOLDING))
                onStartedHolding();
            return bool5;
            bool3 = false;
            break;
            label249: boolean bool4 = this.parent.update(this, paramDriverCall);
            if ((i != 0) || (bool4));
            for (bool5 = bool1; ; bool5 = false)
                break;
            label281: bool1 = false;
        }
    }

    class MyHandler extends Handler
    {
        MyHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return;
                GsmConnection.this.processNextPostDialChar();
                continue;
                GsmConnection.this.releaseWakeLock();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmConnection
 * JD-Core Version:        0.6.2
 */