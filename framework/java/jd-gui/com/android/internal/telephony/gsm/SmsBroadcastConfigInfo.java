package com.android.internal.telephony.gsm;

public final class SmsBroadcastConfigInfo
{
    private int fromCodeScheme;
    private int fromServiceId;
    private boolean selected;
    private int toCodeScheme;
    private int toServiceId;

    public SmsBroadcastConfigInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
    {
        this.fromServiceId = paramInt1;
        this.toServiceId = paramInt2;
        this.fromCodeScheme = paramInt3;
        this.toCodeScheme = paramInt4;
        this.selected = paramBoolean;
    }

    public int getFromCodeScheme()
    {
        return this.fromCodeScheme;
    }

    public int getFromServiceId()
    {
        return this.fromServiceId;
    }

    public int getToCodeScheme()
    {
        return this.toCodeScheme;
    }

    public int getToServiceId()
    {
        return this.toServiceId;
    }

    public boolean isSelected()
    {
        return this.selected;
    }

    public void setFromCodeScheme(int paramInt)
    {
        this.fromCodeScheme = paramInt;
    }

    public void setFromServiceId(int paramInt)
    {
        this.fromServiceId = paramInt;
    }

    public void setSelected(boolean paramBoolean)
    {
        this.selected = paramBoolean;
    }

    public void setToCodeScheme(int paramInt)
    {
        this.toCodeScheme = paramInt;
    }

    public void setToServiceId(int paramInt)
    {
        this.toServiceId = paramInt;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder().append("SmsBroadcastConfigInfo: Id [").append(this.fromServiceId).append(',').append(this.toServiceId).append("] Code [").append(this.fromCodeScheme).append(',').append(this.toCodeScheme).append("] ");
        if (this.selected);
        for (String str = "ENABLED"; ; str = "DISABLED")
            return str;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.SmsBroadcastConfigInfo
 * JD-Core Version:        0.6.2
 */