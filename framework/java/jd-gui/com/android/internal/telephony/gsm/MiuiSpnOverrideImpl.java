package com.android.internal.telephony.gsm;

import android.app.ActivityThread;
import android.app.Application;
import android.content.res.Resources;
import android.provider.Settings.System;
import android.text.TextUtils;

class MiuiSpnOverrideImpl extends SpnOverride
{
    private static final String SETTING_PREFIX = "MOBILE_OPERATOR_NAME_";
    private final String[] mNumericEquivalencies = Resources.getSystem().getStringArray(101056519);
    private final String[] mNumericValues = Resources.getSystem().getStringArray(101056512);

    private String getFromSettings(String paramString)
    {
        return Settings.System.getString(ActivityThread.currentApplication().getContentResolver(), "MOBILE_OPERATOR_NAME_" + paramString);
    }

    private int getIndex(String paramString)
    {
        int i = 0;
        if (i < this.mNumericValues.length)
            if (!this.mNumericValues[i].equals(paramString));
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    boolean containsCarrier(String paramString)
    {
        if ((getIndex(paramString) >= 0) || (super.containsCarrier(paramString)) || (!TextUtils.isEmpty(getFromSettings(paramString))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    String getEquivalentOperatorNumeric(String paramString)
    {
        int i = getIndex(paramString);
        if (i >= 0)
            paramString = this.mNumericEquivalencies[i];
        return paramString;
    }

    String getSpn(String paramString)
    {
        String str = getFromSettings(paramString);
        if (!TextUtils.isEmpty(str));
        while (true)
        {
            return str;
            int i = getIndex(paramString);
            if (i >= 0)
                str = Resources.getSystem().getStringArray(101056513)[i];
            else
                str = super.getSpn(paramString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.MiuiSpnOverrideImpl
 * JD-Core Version:        0.6.2
 */