package com.android.internal.telephony.gsm;

import android.os.Message;
import android.telephony.ServiceState;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import com.android.internal.telephony.ApnSetting;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.DataConnection;
import com.android.internal.telephony.DataConnection.ConnectionParams;
import com.android.internal.telephony.DataConnection.FailCause;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.RetryManager;
import com.android.internal.util.IState;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GsmDataConnection extends DataConnection
{
    private static final String LOG_TAG = "GSM";
    protected int mProfileId = 0;

    private GsmDataConnection(PhoneBase paramPhoneBase, String paramString, int paramInt, RetryManager paramRetryManager, DataConnectionTracker paramDataConnectionTracker)
    {
        super(paramPhoneBase, paramString, paramInt, paramRetryManager, paramDataConnectionTracker);
    }

    private boolean isIpAddress(String paramString)
    {
        if (paramString == null);
        for (boolean bool = false; ; bool = Patterns.IP_ADDRESS.matcher(paramString).matches())
            return bool;
    }

    static GsmDataConnection makeDataConnection(PhoneBase paramPhoneBase, int paramInt, RetryManager paramRetryManager, DataConnectionTracker paramDataConnectionTracker)
    {
        synchronized (mCountLock)
        {
            mCount = 1 + mCount;
            GsmDataConnection localGsmDataConnection = new GsmDataConnection(paramPhoneBase, "GsmDC-" + mCount, paramInt, paramRetryManager, paramDataConnectionTracker);
            localGsmDataConnection.start();
            localGsmDataConnection.log("Made " + localGsmDataConnection.getName());
            return localGsmDataConnection;
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("GsmDataConnection extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" mProfileId=" + this.mProfileId);
    }

    public int getProfileId()
    {
        return this.mProfileId;
    }

    protected boolean isDnsOk(String[] paramArrayOfString)
    {
        boolean bool = false;
        if (("0.0.0.0".equals(paramArrayOfString[0])) && ("0.0.0.0".equals(paramArrayOfString[1])) && (!this.phone.isDnsCheckDisabled()) && ((!this.mApn.types[0].equals("mms")) || (!isIpAddress(this.mApn.mmsProxy))))
        {
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[bool] = this.mApn.types[0];
            arrayOfObject[1] = "mms";
            arrayOfObject[2] = this.mApn.mmsProxy;
            arrayOfObject[3] = Boolean.valueOf(isIpAddress(this.mApn.mmsProxy));
            log(String.format("isDnsOk: return false apn.types[0]=%s APN_TYPE_MMS=%s isIpAddress(%s)=%s", arrayOfObject));
        }
        while (true)
        {
            return bool;
            bool = true;
        }
    }

    protected void log(String paramString)
    {
        Log.d("GSM", "[" + getName() + "] " + paramString);
    }

    protected void onConnect(DataConnection.ConnectionParams paramConnectionParams)
    {
        this.mApn = paramConnectionParams.apn;
        log("Connecting to carrier: '" + this.mApn.carrier + "' APN: '" + this.mApn.apn + "' proxy: '" + this.mApn.proxy + "' port: '" + this.mApn.port);
        this.createTime = -1L;
        this.lastFailTime = -1L;
        this.lastFailCause = DataConnection.FailCause.NONE;
        Message localMessage = obtainMessage(262145, paramConnectionParams);
        localMessage.obj = paramConnectionParams;
        int i = this.mApn.authType;
        if (i == -1)
        {
            if (TextUtils.isEmpty(this.mApn.user))
                i = 0;
        }
        else
            if (!this.phone.getServiceState().getRoaming())
                break label228;
        label228: for (String str = this.mApn.roamingProtocol; ; str = this.mApn.protocol)
        {
            this.phone.mCM.setupDataCall(Integer.toString(getRilRadioTechnology(1)), Integer.toString(this.mProfileId), this.mApn.apn, this.mApn.user, this.mApn.password, Integer.toString(i), str, localMessage);
            return;
            i = 3;
            break;
        }
    }

    public void setProfileId(int paramInt)
    {
        this.mProfileId = paramInt;
    }

    public String toString()
    {
        return "{" + getName() + ": State=" + getCurrentState().getName() + " apnSetting=" + this.mApn + " apnList= " + this.mApnList + " RefCount=" + this.mRefCount + " cid=" + this.cid + " create=" + this.createTime + " lastFail=" + this.lastFailTime + " lastFailCause=" + this.lastFailCause + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmDataConnection
 * JD-Core Version:        0.6.2
 */