package com.android.internal.telephony.gsm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.LinkAddress;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.net.LinkProperties.CompareResult;
import android.net.NetworkConfig;
import android.net.NetworkUtils;
import android.net.ProxyProperties;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.provider.Telephony.Carriers;
import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Log;
import com.android.internal.telephony.ApnContext;
import com.android.internal.telephony.ApnSetting;
import com.android.internal.telephony.CallTracker;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.CommandsInterface.RadioState;
import com.android.internal.telephony.DataCallState;
import com.android.internal.telephony.DataConnection;
import com.android.internal.telephony.DataConnection.CallSetupException;
import com.android.internal.telephony.DataConnection.FailCause;
import com.android.internal.telephony.DataConnection.UpdateLinkPropertyResult;
import com.android.internal.telephony.DataConnectionAc;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.DataConnectionTracker.Activity;
import com.android.internal.telephony.DataConnectionTracker.State;
import com.android.internal.telephony.DataConnectionTracker.TxRxSum;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.Phone.DataState;
import com.android.internal.telephony.Phone.State;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.RetryManager;
import com.android.internal.telephony.ServiceStateTracker;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import miui.net.FirewallManager;

public final class GsmDataConnectionTracker extends DataConnectionTracker
{
    static final String APN_ID = "apn_id";
    private static final boolean DATA_STALL_NOT_SUSPECTED = false;
    private static final boolean DATA_STALL_SUSPECTED = true;
    private static final String INTENT_DATA_STALL_ALARM = "com.android.internal.telephony.gprs-data-stall";
    private static final String INTENT_RECONNECT_ALARM = "com.android.internal.telephony.gprs-reconnect";
    private static final String INTENT_RECONNECT_ALARM_EXTRA_RETRY_COUNT = "reconnect_alaram_extra_retry_count";
    private static final String INTENT_RECONNECT_ALARM_EXTRA_TYPE = "reconnect_alarm_extra_type";
    private static final int POLL_PDP_MILLIS = 5000;
    static final Uri PREFERAPN_NO_UPDATE_URI = Uri.parse("content://telephony/carriers/preferapn_no_update");
    private static final boolean RADIO_TESTS;
    protected final String LOG_TAG = "GSM";
    private boolean canSetPreferApn = false;
    private ApnChangeObserver mApnObserver;
    private Runnable mPollNetStat = new Runnable()
    {
        public void run()
        {
            GsmDataConnectionTracker.this.updateDataActivity();
            if (GsmDataConnectionTracker.access$200(GsmDataConnectionTracker.this))
                GsmDataConnectionTracker.access$302(GsmDataConnectionTracker.this, Settings.Secure.getInt(GsmDataConnectionTracker.this.mResolver, "pdp_watchdog_poll_interval_ms", 1000));
            while (true)
            {
                if (GsmDataConnectionTracker.access$600(GsmDataConnectionTracker.this))
                    GsmDataConnectionTracker.access$800(GsmDataConnectionTracker.this).postDelayed(this, GsmDataConnectionTracker.access$700(GsmDataConnectionTracker.this));
                return;
                GsmDataConnectionTracker.access$502(GsmDataConnectionTracker.this, Settings.Secure.getInt(GsmDataConnectionTracker.this.mResolver, "pdp_watchdog_long_poll_interval_ms", 600000));
            }
        }
    };
    private boolean mReregisterOnReconnectFailure = false;
    private ContentResolver mResolver;

    public GsmDataConnectionTracker(PhoneBase paramPhoneBase)
    {
        super(paramPhoneBase);
        log("GsmDCT.constructor");
        paramPhoneBase.mCM.registerForAvailable(this, 270337, null);
        paramPhoneBase.mCM.registerForOffOrNotAvailable(this, 270342, null);
        paramPhoneBase.mIccRecords.registerForRecordsLoaded(this, 270338, null);
        paramPhoneBase.mCM.registerForDataNetworkStateChanged(this, 270340, null);
        paramPhoneBase.getCallTracker().registerForVoiceCallEnded(this, 270344, null);
        paramPhoneBase.getCallTracker().registerForVoiceCallStarted(this, 270343, null);
        paramPhoneBase.getServiceStateTracker().registerForDataConnectionAttached(this, 270352, null);
        paramPhoneBase.getServiceStateTracker().registerForDataConnectionDetached(this, 270345, null);
        paramPhoneBase.getServiceStateTracker().registerForRoamingOn(this, 270347, null);
        paramPhoneBase.getServiceStateTracker().registerForRoamingOff(this, 270348, null);
        paramPhoneBase.getServiceStateTracker().registerForPsRestrictedEnabled(this, 270358, null);
        paramPhoneBase.getServiceStateTracker().registerForPsRestrictedDisabled(this, 270359, null);
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("com.android.internal.telephony.gprs-data-stall");
        paramPhoneBase.getContext().registerReceiver(this.mIntentReceiver, localIntentFilter, null, paramPhoneBase);
        this.mDataConnectionTracker = this;
        this.mResolver = this.mPhone.getContext().getContentResolver();
        this.mApnObserver = new ApnChangeObserver();
        paramPhoneBase.getContext().getContentResolver().registerContentObserver(Telephony.Carriers.CONTENT_URI, true, this.mApnObserver);
        initApnContextsAndDataConnection();
        broadcastMessenger();
    }

    private ApnContext addApnContext(String paramString)
    {
        ApnContext localApnContext = new ApnContext(paramString, "GSM");
        localApnContext.setDependencyMet(false);
        this.mApnContexts.put(paramString, localApnContext);
        return localApnContext;
    }

    private String apnListToString(ArrayList<ApnSetting> paramArrayList)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = 0;
        int j = paramArrayList.size();
        while (i < j)
        {
            localStringBuilder.append('[').append(((ApnSetting)paramArrayList.get(i)).toString()).append(']');
            i++;
        }
        return localStringBuilder.toString();
    }

    private void applyNewState(ApnContext paramApnContext, boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = 0;
        int j = 0;
        log("applyNewState(" + paramApnContext.getApnType() + ", " + paramBoolean1 + "(" + paramApnContext.isEnabled() + "), " + paramBoolean2 + "(" + paramApnContext.getDependencyMet() + "))");
        if (paramApnContext.isReady())
        {
            if ((paramBoolean1) && (paramBoolean2))
                return;
            if (!paramBoolean1)
            {
                paramApnContext.setReason("dataDisabled");
                i = 1;
            }
        }
        while ((!paramBoolean1) || (!paramBoolean2))
            while (true)
            {
                paramApnContext.setEnabled(paramBoolean1);
                paramApnContext.setDependencyMet(paramBoolean2);
                if (i != 0)
                    cleanUpConnection(true, paramApnContext);
                if (j != 0)
                {
                    trySetupData(paramApnContext);
                    continue;
                    paramApnContext.setReason("dependencyUnmet");
                }
            }
        if (paramApnContext.isEnabled())
            paramApnContext.setReason("dependencyMet");
        while (true)
        {
            if (paramApnContext.getState() == DataConnectionTracker.State.FAILED)
                paramApnContext.setState(DataConnectionTracker.State.IDLE);
            j = 1;
            break;
            paramApnContext.setReason("dataEnabled");
        }
    }

    private ArrayList<ApnSetting> buildWaitingApns(String paramString)
    {
        ArrayList localArrayList = new ArrayList();
        if (paramString.equals("dun"))
        {
            ApnSetting localApnSetting2 = fetchDunApn();
            if (localApnSetting2 != null)
            {
                localArrayList.add(localApnSetting2);
                log("buildWaitingApns: X added APN_TYPE_DUN apnList=" + localArrayList);
            }
        }
        while (true)
        {
            return localArrayList;
            String str = this.mPhone.mIccRecords.getOperatorNumeric();
            int i = this.mPhone.getServiceState().getRilRadioTechnology();
            if ((this.canSetPreferApn) && (this.mPreferredApn != null) && (this.mPreferredApn.canHandleType(paramString)))
            {
                log("buildWaitingApns: Preferred APN:" + str + ":" + this.mPreferredApn.numeric + ":" + this.mPreferredApn);
                if (this.mPreferredApn.numeric.equals(str))
                {
                    if ((this.mPreferredApn.bearer == 0) || (this.mPreferredApn.bearer == i))
                    {
                        localArrayList.add(this.mPreferredApn);
                        log("buildWaitingApns: X added preferred apnList=" + localArrayList);
                        continue;
                    }
                    log("buildWaitingApns: no preferred APN");
                    setPreferredApn(-1);
                }
            }
            else
            {
                for (this.mPreferredApn = null; this.mAllApns != null; this.mPreferredApn = null)
                {
                    Iterator localIterator = this.mAllApns.iterator();
                    while (localIterator.hasNext())
                    {
                        ApnSetting localApnSetting1 = (ApnSetting)localIterator.next();
                        if ((localApnSetting1.canHandleType(paramString)) && ((localApnSetting1.bearer == 0) || (localApnSetting1.bearer == i)))
                        {
                            log("apn info : " + localApnSetting1.toString());
                            localArrayList.add(localApnSetting1);
                        }
                    }
                    log("buildWaitingApns: no preferred APN");
                    setPreferredApn(-1);
                }
                loge("mAllApns is empty!");
                log("buildWaitingApns: X apnList=" + localArrayList);
            }
        }
    }

    private void cancelReconnectAlarm(DataConnectionAc paramDataConnectionAc)
    {
        if (paramDataConnectionAc == null);
        while (true)
        {
            return;
            PendingIntent localPendingIntent = paramDataConnectionAc.getReconnectIntentSync();
            if (localPendingIntent != null)
            {
                ((AlarmManager)this.mPhone.getContext().getSystemService("alarm")).cancel(localPendingIntent);
                paramDataConnectionAc.setReconnectIntentSync(null);
            }
        }
    }

    private DataConnection checkForConnectionForApnContext(ApnContext paramApnContext)
    {
        String str = paramApnContext.getApnType();
        ApnSetting localApnSetting1 = null;
        if ("dun".equals(str))
            localApnSetting1 = fetchDunApn();
        Object localObject1 = null;
        Iterator localIterator = this.mApnContexts.values().iterator();
        ApnContext localApnContext;
        Object localObject2;
        ApnSetting localApnSetting2;
        while (true)
            if (localIterator.hasNext())
            {
                localApnContext = (ApnContext)localIterator.next();
                localObject2 = localApnContext.getDataConnection();
                if (localObject2 != null)
                {
                    localApnSetting2 = localApnContext.getApnSetting();
                    if (localApnSetting1 != null)
                    {
                        if (!localApnSetting1.equals(localApnSetting2))
                            continue;
                        switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$State[localApnContext.getState().ordinal()])
                        {
                        case 2:
                        default:
                            break;
                        case 1:
                            log("checkForConnectionForApnContext: apnContext=" + paramApnContext + " found conn=" + localObject2);
                        case 3:
                        }
                    }
                }
            }
        while (true)
        {
            return localObject2;
            localObject1 = localObject2;
            break;
            if ((localApnSetting2 == null) || (!localApnSetting2.canHandleType(str)))
                break;
            switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$State[localApnContext.getState().ordinal()])
            {
            case 2:
            default:
                break;
            case 1:
                log("checkForConnectionForApnContext: apnContext=" + paramApnContext + " found conn=" + localObject2);
                break;
            case 3:
                localObject1 = localObject2;
                break;
                if (localObject1 != null)
                {
                    log("checkForConnectionForApnContext: apnContext=" + paramApnContext + " found conn=" + localObject1);
                    localObject2 = localObject1;
                }
                else
                {
                    log("checkForConnectionForApnContext: apnContext=" + paramApnContext + " NO conn");
                    localObject2 = null;
                }
                break;
            }
        }
    }

    private void cleanUpConnection(boolean paramBoolean, ApnContext paramApnContext)
    {
        if (paramApnContext == null)
        {
            log("cleanUpConnection: apn context is null");
            return;
        }
        DataConnectionAc localDataConnectionAc = paramApnContext.getDataConnectionAc();
        log("cleanUpConnection: E tearDown=" + paramBoolean + " reason=" + paramApnContext.getReason() + " apnContext=" + paramApnContext);
        if (paramBoolean)
            if (paramApnContext.isDisconnected())
            {
                paramApnContext.setState(DataConnectionTracker.State.IDLE);
                if (!paramApnContext.isReady())
                {
                    paramApnContext.setDataConnection(null);
                    paramApnContext.setDataConnectionAc(null);
                }
            }
        while (true)
        {
            if ((localDataConnectionAc != null) && (localDataConnectionAc.getApnListSync().isEmpty()))
                cancelReconnectAlarm(localDataConnectionAc);
            log("cleanUpConnection: X tearDown=" + paramBoolean + " reason=" + paramApnContext.getReason() + " apnContext=" + paramApnContext + " dc=" + paramApnContext.getDataConnection());
            break;
            if (localDataConnectionAc != null)
            {
                if (paramApnContext.getState() != DataConnectionTracker.State.DISCONNECTING)
                {
                    int i = 0;
                    if ("dun".equals(paramApnContext.getApnType()))
                    {
                        ApnSetting localApnSetting = fetchDunApn();
                        if ((localApnSetting != null) && (localApnSetting.equals(paramApnContext.getApnSetting())))
                        {
                            log("tearing down dedicated DUN connection");
                            i = 1;
                        }
                    }
                    StringBuilder localStringBuilder = new StringBuilder().append("cleanUpConnection: tearing down");
                    String str;
                    label271: Message localMessage;
                    if (i != 0)
                    {
                        str = " all";
                        log(str);
                        localMessage = obtainMessage(270351, paramApnContext);
                        if (i == 0)
                            break label331;
                        paramApnContext.getDataConnection().tearDownAll(paramApnContext.getReason(), localMessage);
                    }
                    while (true)
                    {
                        paramApnContext.setState(DataConnectionTracker.State.DISCONNECTING);
                        break;
                        str = "";
                        break label271;
                        label331: paramApnContext.getDataConnection().tearDown(paramApnContext.getReason(), localMessage);
                    }
                }
            }
            else
            {
                paramApnContext.setState(DataConnectionTracker.State.IDLE);
                this.mPhone.notifyDataConnection(paramApnContext.getReason(), paramApnContext.getApnType());
                continue;
                if (localDataConnectionAc != null)
                    localDataConnectionAc.resetSync();
                paramApnContext.setState(DataConnectionTracker.State.IDLE);
                this.mPhone.notifyDataConnection(paramApnContext.getReason(), paramApnContext.getApnType());
                paramApnContext.setDataConnection(null);
                paramApnContext.setDataConnectionAc(null);
            }
        }
    }

    private void configureRetry(DataConnection paramDataConnection, boolean paramBoolean, int paramInt)
    {
        log("configureRetry: forDefault=" + paramBoolean + " retryCount=" + paramInt + " dc=" + paramDataConnection);
        if (paramDataConnection == null)
            return;
        if (!paramDataConnection.configureRetry(getReryConfig(paramBoolean)))
        {
            if (!paramBoolean)
                break label103;
            if (!paramDataConnection.configureRetry("default_randomization=2000,5000,10000,20000,40000,80000:5000,160000:5000,320000:5000,640000:5000,1280000:5000,1800000:5000"))
            {
                loge("configureRetry: Could not configure using DEFAULT_DATA_RETRY_CONFIG=default_randomization=2000,5000,10000,20000,40000,80000:5000,160000:5000,320000:5000,640000:5000,1280000:5000,1800000:5000");
                paramDataConnection.configureRetry(20, 2000, 1000);
            }
        }
        while (true)
        {
            paramDataConnection.setRetryCount(paramInt);
            break;
            label103: if (!paramDataConnection.configureRetry("max_retries=3, 5000, 5000, 5000"))
            {
                loge("configureRetry: Could note configure using SECONDARY_DATA_RETRY_CONFIG=max_retries=3, 5000, 5000, 5000");
                paramDataConnection.configureRetry("max_retries=3, 333, 333, 333");
            }
        }
    }

    private void createAllApnList()
    {
        this.mAllApns = new ArrayList();
        String str1 = this.mPhone.mIccRecords.getOperatorNumeric();
        if (str1 != null)
        {
            String str2 = "numeric = '" + str1 + "'";
            String str3 = str2 + " and carrier_enabled = 1";
            log("createAllApnList: selection=" + str3);
            Cursor localCursor = this.mPhone.getContext().getContentResolver().query(Telephony.Carriers.CONTENT_URI, null, str3, null, null);
            if (localCursor != null)
            {
                if (localCursor.getCount() > 0)
                    this.mAllApns = createApnList(localCursor);
                localCursor.close();
            }
        }
        if (this.mAllApns.isEmpty())
        {
            log("createAllApnList: No APN found for carrier: " + str1);
            this.mPreferredApn = null;
        }
        while (true)
        {
            log("createAllApnList: X mAllApns=" + this.mAllApns);
            return;
            this.mPreferredApn = getPreferredApn();
            if ((this.mPreferredApn != null) && (!this.mPreferredApn.numeric.equals(str1)))
            {
                this.mPreferredApn = null;
                setPreferredApn(-1);
            }
            log("createAllApnList: mPreferredApn=" + this.mPreferredApn);
        }
    }

    private ArrayList<ApnSetting> createApnList(Cursor paramCursor)
    {
        ArrayList localArrayList = new ArrayList();
        String[] arrayOfString;
        int i;
        String str1;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        int j;
        String str11;
        String str12;
        if (paramCursor.moveToFirst())
        {
            arrayOfString = parseTypes(paramCursor.getString(paramCursor.getColumnIndexOrThrow("type")));
            i = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("_id"));
            str1 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("numeric"));
            str2 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("name"));
            str3 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("apn"));
            str4 = NetworkUtils.trimV4AddrZeros(paramCursor.getString(paramCursor.getColumnIndexOrThrow("proxy")));
            str5 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("port"));
            str6 = NetworkUtils.trimV4AddrZeros(paramCursor.getString(paramCursor.getColumnIndexOrThrow("mmsc")));
            str7 = NetworkUtils.trimV4AddrZeros(paramCursor.getString(paramCursor.getColumnIndexOrThrow("mmsproxy")));
            str8 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("mmsport"));
            str9 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("user"));
            str10 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("password"));
            j = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("authtype"));
            str11 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("protocol"));
            str12 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("roaming_protocol"));
            if (paramCursor.getInt(paramCursor.getColumnIndexOrThrow("carrier_enabled")) != 1)
                break label399;
        }
        label399: for (boolean bool = true; ; bool = false)
        {
            localArrayList.add(new ApnSetting(i, str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, j, arrayOfString, str11, str12, bool, paramCursor.getInt(paramCursor.getColumnIndexOrThrow("bearer"))));
            if (paramCursor.moveToNext())
                break;
            log("createApnList: X result=" + localArrayList);
            return localArrayList;
        }
    }

    private GsmDataConnection createDataConnection()
    {
        log("createDataConnection E");
        RetryManager localRetryManager = new RetryManager();
        int i = this.mUniqueIdGenerator.getAndIncrement();
        GsmDataConnection localGsmDataConnection = GsmDataConnection.makeDataConnection(this.mPhone, i, localRetryManager, this);
        this.mDataConnections.put(Integer.valueOf(i), localGsmDataConnection);
        DataConnectionAc localDataConnectionAc = new DataConnectionAc(localGsmDataConnection, "GSM");
        int j = localDataConnectionAc.fullyConnectSync(this.mPhone.getContext(), this, localGsmDataConnection.getHandler());
        if (j == 0)
            this.mDataConnectionAsyncChannels.put(Integer.valueOf(localDataConnectionAc.dataConnection.getDataConnectionId()), localDataConnectionAc);
        while (true)
        {
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("com.android.internal.telephony.gprs-reconnect." + i);
            this.mPhone.getContext().registerReceiver(this.mIntentReceiver, localIntentFilter, null, this.mPhone);
            log("createDataConnection() X id=" + i + " dc=" + localGsmDataConnection);
            return localGsmDataConnection;
            loge("createDataConnection: Could not connect to dcac.mDc=" + localDataConnectionAc.dataConnection + " status=" + j);
        }
    }

    private boolean dataConnectionNotInUse(DataConnectionAc paramDataConnectionAc)
    {
        log("dataConnectionNotInUse: check if dcac is inuse dc=" + paramDataConnectionAc.dataConnection);
        Iterator localIterator1 = this.mApnContexts.values().iterator();
        while (localIterator1.hasNext())
        {
            ApnContext localApnContext2 = (ApnContext)localIterator1.next();
            if (localApnContext2.getDataConnectionAc() == paramDataConnectionAc)
                log("dataConnectionNotInUse: in use by apnContext=" + localApnContext2);
        }
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            Iterator localIterator2 = paramDataConnectionAc.getApnListSync().iterator();
            while (localIterator2.hasNext())
            {
                ApnContext localApnContext1 = (ApnContext)localIterator2.next();
                log("dataConnectionNotInUse: removing apnContext=" + localApnContext1);
                paramDataConnectionAc.removeApnContextSync(localApnContext1);
            }
            log("dataConnectionNotInUse: not in use return true");
        }
    }

    private void destroyDataConnections()
    {
        if (this.mDataConnections != null)
        {
            log("destroyDataConnections: clear mDataConnectionList");
            this.mDataConnections.clear();
        }
        while (true)
        {
            return;
            log("destroyDataConnections: mDataConnecitonList is empty, ignore");
        }
    }

    private void doRecovery()
    {
        if (getOverallState() == DataConnectionTracker.State.CONNECTED)
        {
            int i = getRecoveryAction();
            switch (i)
            {
            default:
                throw new RuntimeException("doRecovery: Invalid recoveryAction=" + i);
            case 0:
                EventLog.writeEvent(50118, this.mSentSinceLastRecv);
                log("doRecovery() get data call list");
                this.mPhone.mCM.getDataCallList(obtainMessage(270340));
                putRecoveryAction(1);
            case 1:
            case 2:
            case 3:
            case 4:
            }
        }
        while (true)
        {
            return;
            EventLog.writeEvent(50119, this.mSentSinceLastRecv);
            log("doRecovery() cleanup all connections");
            cleanUpAllConnections(true, "pdpReset");
            putRecoveryAction(2);
            continue;
            EventLog.writeEvent(50120, this.mSentSinceLastRecv);
            log("doRecovery() re-register");
            this.mPhone.getServiceStateTracker().reRegisterNetwork(null);
            putRecoveryAction(3);
            continue;
            EventLog.writeEvent(50121, this.mSentSinceLastRecv);
            log("restarting radio");
            putRecoveryAction(4);
            restartRadio();
            continue;
            EventLog.writeEvent(50122, -1);
            log("restarting radio with gsm.radioreset to true");
            SystemProperties.set("gsm.radioreset", "true");
            try
            {
                Thread.sleep(1000L);
                label254: restartRadio();
                putRecoveryAction(0);
            }
            catch (InterruptedException localInterruptedException)
            {
                break label254;
            }
        }
    }

    private List<ApnContext> findApnContextToClean(Collection<DataConnectionAc> paramCollection)
    {
        Object localObject;
        if (paramCollection == null)
            localObject = null;
        while (true)
        {
            return localObject;
            log("findApnContextToClean(ar): E dcacs=" + paramCollection);
            localObject = new ArrayList();
            Iterator localIterator1 = this.mApnContexts.values().iterator();
            while (localIterator1.hasNext())
            {
                ApnContext localApnContext = (ApnContext)localIterator1.next();
                if (localApnContext.getState() == DataConnectionTracker.State.CONNECTED)
                {
                    int i = 0;
                    Iterator localIterator2 = paramCollection.iterator();
                    while (localIterator2.hasNext())
                        if ((DataConnectionAc)localIterator2.next() == localApnContext.getDataConnectionAc())
                            i = 1;
                    if (i == 0)
                    {
                        log("findApnContextToClean(ar): Connected apn not found in the list (" + localApnContext.toString() + ")");
                        if (localApnContext.getDataConnectionAc() != null)
                            ((ArrayList)localObject).addAll(localApnContext.getDataConnectionAc().getApnListSync());
                        else
                            ((ArrayList)localObject).add(localApnContext);
                    }
                }
            }
            log("findApnContextToClean(ar): X list=" + localObject);
        }
    }

    private DataConnectionAc findDataConnectionAcByCid(int paramInt)
    {
        Iterator localIterator = this.mDataConnectionAsyncChannels.values().iterator();
        DataConnectionAc localDataConnectionAc;
        do
        {
            if (!localIterator.hasNext())
                break;
            localDataConnectionAc = (DataConnectionAc)localIterator.next();
        }
        while (localDataConnectionAc.getCidSync() != paramInt);
        while (true)
        {
            return localDataConnectionAc;
            localDataConnectionAc = null;
        }
    }

    private GsmDataConnection findFreeDataConnection()
    {
        Iterator localIterator = this.mDataConnectionAsyncChannels.values().iterator();
        DataConnection localDataConnection;
        while (localIterator.hasNext())
        {
            DataConnectionAc localDataConnectionAc = (DataConnectionAc)localIterator.next();
            if ((localDataConnectionAc.isInactiveSync()) && (dataConnectionNotInUse(localDataConnectionAc)))
            {
                localDataConnection = localDataConnectionAc.dataConnection;
                log("findFreeDataConnection: found free GsmDataConnection= dcac=" + localDataConnectionAc + " dc=" + localDataConnection);
            }
        }
        for (GsmDataConnection localGsmDataConnection = (GsmDataConnection)localDataConnection; ; localGsmDataConnection = null)
        {
            return localGsmDataConnection;
            log("findFreeDataConnection: NO free GsmDataConnection");
        }
    }

    private int getCellLocationId()
    {
        int i = -1;
        CellLocation localCellLocation = this.mPhone.getCellLocation();
        if (localCellLocation != null)
        {
            if (!(localCellLocation instanceof GsmCellLocation))
                break label32;
            i = ((GsmCellLocation)localCellLocation).getCid();
        }
        while (true)
        {
            return i;
            label32: if ((localCellLocation instanceof CdmaCellLocation))
                i = ((CdmaCellLocation)localCellLocation).getBaseStationId();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static boolean getPolicyDataEnabled()
    {
        return sPolicyDataEnabled;
    }

    private ApnSetting getPreferredApn()
    {
        Object localObject = null;
        if (this.mAllApns.isEmpty())
            log("getPreferredApn: X not found mAllApns.isEmpty");
        while (true)
        {
            return localObject;
            ContentResolver localContentResolver = this.mPhone.getContext().getContentResolver();
            Uri localUri = PREFERAPN_NO_UPDATE_URI;
            String[] arrayOfString = new String[3];
            arrayOfString[0] = "_id";
            arrayOfString[1] = "name";
            arrayOfString[2] = "apn";
            Cursor localCursor = localContentResolver.query(localUri, arrayOfString, null, null, "name ASC");
            if (localCursor != null);
            for (this.canSetPreferApn = true; ; this.canSetPreferApn = false)
            {
                if ((!this.canSetPreferApn) || (localCursor.getCount() <= 0))
                    break label230;
                localCursor.moveToFirst();
                int i = localCursor.getInt(localCursor.getColumnIndexOrThrow("_id"));
                Iterator localIterator = this.mAllApns.iterator();
                ApnSetting localApnSetting;
                do
                {
                    if (!localIterator.hasNext())
                        break;
                    localApnSetting = (ApnSetting)localIterator.next();
                }
                while ((localApnSetting.id != i) || (!localApnSetting.canHandleType(this.mRequestedApnType)));
                log("getPreferredApn: X found apnSetting" + localApnSetting);
                localCursor.close();
                localObject = localApnSetting;
                break;
            }
            label230: if (localCursor != null)
                localCursor.close();
            log("getPreferredApn: X not found");
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private boolean isDataAllowed(ApnContext paramApnContext)
    {
        if ((paramApnContext.isReady()) && (Injector.isDataAllowed(this, paramApnContext)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void notifyApnIdUpToCurrent(String paramString1, ApnContext paramApnContext, String paramString2)
    {
        switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$State[paramApnContext.getState().ordinal()])
        {
        case 4:
        case 5:
        default:
        case 3:
        case 6:
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            this.mPhone.notifyDataConnection(paramString1, paramString2, Phone.DataState.CONNECTING);
            continue;
            this.mPhone.notifyDataConnection(paramString1, paramString2, Phone.DataState.CONNECTING);
            this.mPhone.notifyDataConnection(paramString1, paramString2, Phone.DataState.CONNECTED);
        }
    }

    private void notifyDefaultData(ApnContext paramApnContext)
    {
        log("notifyDefaultData: type=" + paramApnContext.getApnType() + ", reason:" + paramApnContext.getReason());
        paramApnContext.setState(DataConnectionTracker.State.CONNECTED);
        this.mPhone.notifyDataConnection(paramApnContext.getReason(), paramApnContext.getApnType());
        startNetStatPoll();
        startDataStallAlarm(false);
        paramApnContext.setRetryCount(0);
    }

    private void notifyNoData(DataConnection.FailCause paramFailCause, ApnContext paramApnContext)
    {
        log("notifyNoData: type=" + paramApnContext.getApnType());
        paramApnContext.setState(DataConnectionTracker.State.FAILED);
        if ((paramFailCause.isPermanentFail()) && (!paramApnContext.getApnType().equals("default")))
            this.mPhone.notifyDataConnectionFailed(paramApnContext.getReason(), paramApnContext.getApnType());
    }

    private void onApnChanged()
    {
        boolean bool1 = true;
        DataConnectionTracker.State localState = getOverallState();
        boolean bool2;
        if ((localState == DataConnectionTracker.State.IDLE) || (localState == DataConnectionTracker.State.FAILED))
        {
            bool2 = bool1;
            if ((this.mPhone instanceof GSMPhone))
                ((GSMPhone)this.mPhone).updateCurrentCarrierInProvider();
            log("onApnChanged: createAllApnList and cleanUpAllConnections");
            createAllApnList();
            if (bool2)
                break label84;
        }
        while (true)
        {
            cleanUpAllConnections(bool1, "apnChanged");
            if (bool2)
                setupDataOnReadyApns("apnChanged");
            return;
            bool2 = false;
            break;
            label84: bool1 = false;
        }
    }

    private void onDataConnectionAttached()
    {
        log("onDataConnectionAttached");
        if (getOverallState() == DataConnectionTracker.State.CONNECTED)
        {
            log("onDataConnectionAttached: start polling notify attached");
            startNetStatPoll();
            startDataStallAlarm(false);
            notifyDataConnection("dataAttached");
        }
        while (true)
        {
            setupDataOnReadyApns("dataAttached");
            return;
            notifyOffApnsOfAvailability("dataAttached");
        }
    }

    private void onDataStateChanged(AsyncResult paramAsyncResult)
    {
        log("onDataStateChanged(ar): E");
        ArrayList localArrayList1 = (ArrayList)paramAsyncResult.result;
        if (paramAsyncResult.exception != null)
            log("onDataStateChanged(ar): exception; likely radio not available, ignore");
        while (true)
        {
            return;
            log("onDataStateChanged(ar): DataCallState size=" + localArrayList1.size());
            HashMap localHashMap = new HashMap();
            Iterator localIterator1 = localArrayList1.iterator();
            while (localIterator1.hasNext())
            {
                DataCallState localDataCallState2 = (DataCallState)localIterator1.next();
                DataConnectionAc localDataConnectionAc2 = findDataConnectionAcByCid(localDataCallState2.cid);
                if (localDataConnectionAc2 != null)
                    localHashMap.put(localDataCallState2, localDataConnectionAc2);
            }
            List localList = findApnContextToClean(localHashMap.values());
            Iterator localIterator2 = localArrayList1.iterator();
            while (localIterator2.hasNext())
            {
                DataCallState localDataCallState1 = (DataCallState)localIterator2.next();
                DataConnectionAc localDataConnectionAc1 = (DataConnectionAc)localHashMap.get(localDataCallState1);
                if (localDataConnectionAc1 == null)
                {
                    loge("onDataStateChanged(ar): No associated DataConnection ignore");
                }
                else
                {
                    Collection localCollection = localDataConnectionAc1.getApnListSync();
                    ArrayList localArrayList2 = new ArrayList();
                    Iterator localIterator4 = localCollection.iterator();
                    while (localIterator4.hasNext())
                    {
                        ApnContext localApnContext2 = (ApnContext)localIterator4.next();
                        if ((localApnContext2.getState() == DataConnectionTracker.State.CONNECTED) || (localApnContext2.getState() == DataConnectionTracker.State.CONNECTING) || (localApnContext2.getState() == DataConnectionTracker.State.INITING))
                            localArrayList2.add(localApnContext2);
                    }
                    if (localArrayList2.size() == 0)
                    {
                        log("onDataStateChanged(ar): no connected apns");
                    }
                    else
                    {
                        log("onDataStateChanged(ar): Found ConnId=" + localDataCallState1.cid + " newState=" + localDataCallState1.toString());
                        if (localDataCallState1.active == 0)
                        {
                            log("onDataStateChanged(ar): inactive, cleanup apns=" + localArrayList2);
                            localList.addAll(localArrayList2);
                        }
                        else
                        {
                            DataConnection.UpdateLinkPropertyResult localUpdateLinkPropertyResult = localDataConnectionAc1.updateLinkPropertiesDataCallStateSync(localDataCallState1);
                            if (localUpdateLinkPropertyResult.oldLp.equals(localUpdateLinkPropertyResult.newLp))
                            {
                                log("onDataStateChanged(ar): no change");
                            }
                            else if (localUpdateLinkPropertyResult.oldLp.isIdenticalInterfaceName(localUpdateLinkPropertyResult.newLp))
                            {
                                if ((!localUpdateLinkPropertyResult.oldLp.isIdenticalDnses(localUpdateLinkPropertyResult.newLp)) || (!localUpdateLinkPropertyResult.oldLp.isIdenticalRoutes(localUpdateLinkPropertyResult.newLp)) || (!localUpdateLinkPropertyResult.oldLp.isIdenticalHttpProxy(localUpdateLinkPropertyResult.newLp)) || (!localUpdateLinkPropertyResult.oldLp.isIdenticalAddresses(localUpdateLinkPropertyResult.newLp)))
                                {
                                    LinkProperties.CompareResult localCompareResult = localUpdateLinkPropertyResult.oldLp.compareAddresses(localUpdateLinkPropertyResult.newLp);
                                    log("onDataStateChanged: oldLp=" + localUpdateLinkPropertyResult.oldLp + " newLp=" + localUpdateLinkPropertyResult.newLp + " car=" + localCompareResult);
                                    int j = 0;
                                    Iterator localIterator5 = localCompareResult.added.iterator();
                                    while (true)
                                    {
                                        if (!localIterator5.hasNext())
                                            break label657;
                                        LinkAddress localLinkAddress = (LinkAddress)localIterator5.next();
                                        Iterator localIterator7 = localCompareResult.removed.iterator();
                                        if (localIterator7.hasNext())
                                        {
                                            if (!NetworkUtils.addressTypeMatches(((LinkAddress)localIterator7.next()).getAddress(), localLinkAddress.getAddress()))
                                                break;
                                            j = 1;
                                        }
                                    }
                                    label657: if (j != 0)
                                    {
                                        log("onDataStateChanged(ar): addr change, cleanup apns=" + localArrayList2 + " oldLp=" + localUpdateLinkPropertyResult.oldLp + " newLp=" + localUpdateLinkPropertyResult.newLp);
                                        localList.addAll(localArrayList2);
                                    }
                                    else
                                    {
                                        log("onDataStateChanged(ar): simple change");
                                        Iterator localIterator6 = localArrayList2.iterator();
                                        while (localIterator6.hasNext())
                                        {
                                            ApnContext localApnContext1 = (ApnContext)localIterator6.next();
                                            this.mPhone.notifyDataConnection("linkPropertiesChanged", localApnContext1.getApnType());
                                        }
                                    }
                                }
                                else
                                {
                                    log("onDataStateChanged(ar): no changes");
                                }
                            }
                            else
                            {
                                log("onDataStateChanged(ar): interface change, cleanup apns=" + localArrayList2);
                                localList.addAll(localArrayList2);
                            }
                        }
                    }
                }
            }
            if (localList.size() != 0)
            {
                int i = getCellLocationId();
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = Integer.valueOf(i);
                arrayOfObject[1] = Integer.valueOf(TelephonyManager.getDefault().getNetworkType());
                EventLog.writeEvent(50109, arrayOfObject);
            }
            log("onDataStateChange(ar): apnsToCleanup=" + localList);
            Iterator localIterator3 = localList.iterator();
            while (localIterator3.hasNext())
                cleanUpConnection(true, (ApnContext)localIterator3.next());
            log("onDataStateChanged(ar): X");
        }
    }

    private void onRecordsLoaded()
    {
        log("onRecordsLoaded: createAllApnList");
        createAllApnList();
        if (this.mPhone.mCM.getRadioState().isOn())
        {
            log("onRecordsLoaded: notifying data availability");
            notifyOffApnsOfAvailability("simLoaded");
        }
        setupDataOnReadyApns("simLoaded");
    }

    private String[] parseTypes(String paramString)
    {
        String[] arrayOfString;
        if ((paramString == null) || (paramString.equals("")))
        {
            arrayOfString = new String[1];
            arrayOfString[0] = "*";
        }
        while (true)
        {
            return arrayOfString;
            arrayOfString = paramString.split(",");
        }
    }

    private void reconnectAfterFail(DataConnection.FailCause paramFailCause, ApnContext paramApnContext, int paramInt)
    {
        if (paramApnContext == null)
            loge("reconnectAfterFail: apnContext == null, impossible");
        while (true)
        {
            return;
            log("reconnectAfterFail: lastFailCause=" + paramFailCause + " retryOverride=" + paramInt + " apnContext=" + paramApnContext);
            if ((paramApnContext.getState() == DataConnectionTracker.State.FAILED) && (paramApnContext.getDataConnection() != null))
                if (!paramApnContext.getDataConnection().isRetryNeeded())
                {
                    if (!paramApnContext.getApnType().equals("default"))
                        this.mPhone.notifyDataConnection("apnFailed", paramApnContext.getApnType());
                    else if (this.mReregisterOnReconnectFailure)
                        paramApnContext.getDataConnection().retryForeverUsingLastTimeout();
                }
                else
                {
                    int i = paramInt;
                    if (i < 0)
                    {
                        i = paramApnContext.getDataConnection().getRetryTimer();
                        paramApnContext.getDataConnection().increaseRetryCount();
                        log("reconnectAfterFail: increaseRetryCount=" + paramApnContext.getDataConnection().getRetryCount() + " nextReconnectDelay=" + i);
                    }
                    startAlarmForReconnect(i, paramApnContext);
                    if (!shouldPostNotification(paramFailCause))
                    {
                        log("reconnectAfterFail: NOT Posting GPRS Unavailable notification -- likely transient error");
                        continue;
                        log("reconnectAfterFail: activate failed, Reregistering to network");
                        this.mReregisterOnReconnectFailure = true;
                        this.mPhone.getServiceStateTracker().reRegisterNetwork(null);
                        paramApnContext.setRetryCount(0);
                    }
                    else
                    {
                        notifyNoData(paramFailCause, paramApnContext);
                    }
                }
        }
    }

    private void resetPollStats()
    {
        this.mTxPkts = -1L;
        this.mRxPkts = -1L;
        this.mNetStatPollPeriod = 1000;
    }

    private boolean retryAfterDisconnected(String paramString)
    {
        boolean bool = true;
        if ("radioTurnedOff".equals(paramString))
            bool = false;
        return bool;
    }

    private void setPreferredApn(int paramInt)
    {
        if (!this.canSetPreferApn)
            log("setPreferredApn: X !canSEtPreferApn");
        while (true)
        {
            return;
            log("setPreferredApn: delete");
            ContentResolver localContentResolver = this.mPhone.getContext().getContentResolver();
            localContentResolver.delete(PREFERAPN_NO_UPDATE_URI, null, null);
            if (paramInt >= 0)
            {
                log("setPreferredApn: insert");
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("apn_id", Integer.valueOf(paramInt));
                localContentResolver.insert(PREFERAPN_NO_UPDATE_URI, localContentValues);
            }
        }
    }

    private boolean setupData(ApnContext paramApnContext)
    {
        boolean bool = false;
        log("setupData: apnContext=" + paramApnContext);
        int i = getApnProfileID(paramApnContext.getApnType());
        ApnSetting localApnSetting = paramApnContext.getNextWaitingApn();
        if (localApnSetting == null)
            log("setupData: return for no apn found!");
        while (true)
        {
            return bool;
            GsmDataConnection localGsmDataConnection = (GsmDataConnection)checkForConnectionForApnContext(paramApnContext);
            if (localGsmDataConnection == null)
            {
                localGsmDataConnection = findReadyDataConnection(localApnSetting);
                if (localGsmDataConnection == null)
                {
                    log("setupData: No ready GsmDataConnection found!");
                    localGsmDataConnection = findFreeDataConnection();
                }
                if (localGsmDataConnection == null)
                    localGsmDataConnection = createDataConnection();
                if (localGsmDataConnection == null)
                    log("setupData: No free GsmDataConnection found!");
            }
            else
            {
                localApnSetting = ((DataConnectionAc)this.mDataConnectionAsyncChannels.get(Integer.valueOf(localGsmDataConnection.getDataConnectionId()))).getApnSettingSync();
                DataConnectionAc localDataConnectionAc = (DataConnectionAc)this.mDataConnectionAsyncChannels.get(Integer.valueOf(localGsmDataConnection.getDataConnectionId()));
                localGsmDataConnection.setProfileId(i);
                int j = localDataConnectionAc.getRefCountSync();
                log("setupData: init dc and apnContext refCount=" + j);
                if (j == 0)
                    configureRetry(localGsmDataConnection, localApnSetting.canHandleType("default"), paramApnContext.getRetryCount());
                paramApnContext.setDataConnectionAc(localDataConnectionAc);
                paramApnContext.setDataConnection(localGsmDataConnection);
                paramApnContext.setApnSetting(localApnSetting);
                paramApnContext.setState(DataConnectionTracker.State.INITING);
                this.mPhone.notifyDataConnection(paramApnContext.getReason(), paramApnContext.getApnType());
                if (paramApnContext.getDataConnectionAc().getReconnectIntentSync() != null)
                {
                    log("setupData: data reconnection pending");
                    paramApnContext.setState(DataConnectionTracker.State.FAILED);
                    this.mPhone.notifyDataConnection(paramApnContext.getReason(), paramApnContext.getApnType());
                    bool = true;
                }
                else
                {
                    Message localMessage = obtainMessage();
                    localMessage.what = 270336;
                    localMessage.obj = paramApnContext;
                    localGsmDataConnection.bringUp(localMessage, localApnSetting);
                    log("setupData: initing!");
                    bool = true;
                }
            }
        }
    }

    private void setupDataOnReadyApns(String paramString)
    {
        Iterator localIterator1 = this.mDataConnectionAsyncChannels.values().iterator();
        while (localIterator1.hasNext())
        {
            DataConnectionAc localDataConnectionAc = (DataConnectionAc)localIterator1.next();
            if (localDataConnectionAc.getReconnectIntentSync() != null)
                cancelReconnectAlarm(localDataConnectionAc);
            if (localDataConnectionAc.dataConnection != null)
            {
                Collection localCollection = localDataConnectionAc.getApnListSync();
                boolean bool = false;
                Iterator localIterator3 = localCollection.iterator();
                while (localIterator3.hasNext())
                    if (((ApnContext)localIterator3.next()).getApnType().equals("default"))
                        bool = true;
                configureRetry(localDataConnectionAc.dataConnection, bool, 0);
            }
        }
        resetAllRetryCounts();
        Iterator localIterator2 = this.mApnContexts.values().iterator();
        while (localIterator2.hasNext())
        {
            ApnContext localApnContext = (ApnContext)localIterator2.next();
            if (localApnContext.getState() == DataConnectionTracker.State.FAILED)
                localApnContext.setState(DataConnectionTracker.State.IDLE);
            if ((localApnContext.isReady()) && (localApnContext.getState() == DataConnectionTracker.State.IDLE))
            {
                localApnContext.setReason(paramString);
                trySetupData(localApnContext);
            }
        }
    }

    private boolean shouldPostNotification(DataConnection.FailCause paramFailCause)
    {
        if (paramFailCause != DataConnection.FailCause.UNKNOWN);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void startAlarmForReconnect(int paramInt, ApnContext paramApnContext)
    {
        DataConnectionAc localDataConnectionAc = paramApnContext.getDataConnectionAc();
        if ((localDataConnectionAc == null) || (localDataConnectionAc.dataConnection == null))
            loge("startAlarmForReconnect: null dcac or dc.");
        while (true)
        {
            return;
            AlarmManager localAlarmManager = (AlarmManager)this.mPhone.getContext().getSystemService("alarm");
            Intent localIntent = new Intent("com.android.internal.telephony.gprs-reconnect." + localDataConnectionAc.dataConnection.getDataConnectionId());
            String str = paramApnContext.getReason();
            localIntent.putExtra("reconnect_alarm_extra_reason", str);
            int i = localDataConnectionAc.dataConnection.getDataConnectionId();
            localIntent.putExtra("reconnect_alarm_extra_type", i);
            int j = localDataConnectionAc.dataConnection.getRetryCount();
            localIntent.putExtra("reconnect_alaram_extra_retry_count", j);
            log("startAlarmForReconnect: next attempt in " + paramInt / 1000 + "s" + " reason='" + str + "' connectionId=" + i + " retryCount=" + j);
            PendingIntent localPendingIntent = PendingIntent.getBroadcast(this.mPhone.getContext(), 0, localIntent, 134217728);
            localDataConnectionAc.setReconnectIntentSync(localPendingIntent);
            localAlarmManager.set(2, SystemClock.elapsedRealtime() + paramInt, localPendingIntent);
        }
    }

    private void startDataStallAlarm(boolean paramBoolean)
    {
        int i = getRecoveryAction();
        if ((this.mIsScreenOn) || (paramBoolean) || (RecoveryAction.isAggressiveRecovery(i)));
        for (int j = Settings.Secure.getInt(this.mResolver, "data_stall_alarm_aggressive_delay_in_ms", 60000); ; j = Settings.Secure.getInt(this.mResolver, "data_stall_alarm_non_aggressive_delay_in_ms", 360000))
        {
            this.mDataStallAlarmTag = (1 + this.mDataStallAlarmTag);
            AlarmManager localAlarmManager = (AlarmManager)this.mPhone.getContext().getSystemService("alarm");
            Intent localIntent = new Intent("com.android.internal.telephony.gprs-data-stall");
            localIntent.putExtra("data.stall.alram.tag", this.mDataStallAlarmTag);
            this.mDataStallAlarmIntent = PendingIntent.getBroadcast(this.mPhone.getContext(), 0, localIntent, 134217728);
            localAlarmManager.set(2, SystemClock.elapsedRealtime() + j, this.mDataStallAlarmIntent);
            return;
        }
    }

    private void startDelayedRetry(DataConnection.FailCause paramFailCause, ApnContext paramApnContext, int paramInt)
    {
        notifyNoData(paramFailCause, paramApnContext);
        reconnectAfterFail(paramFailCause, paramApnContext, paramInt);
    }

    private void stopDataStallAlarm()
    {
        AlarmManager localAlarmManager = (AlarmManager)this.mPhone.getContext().getSystemService("alarm");
        this.mDataStallAlarmTag = (1 + this.mDataStallAlarmTag);
        if (this.mDataStallAlarmIntent != null)
        {
            localAlarmManager.cancel(this.mDataStallAlarmIntent);
            this.mDataStallAlarmIntent = null;
        }
    }

    private boolean trySetupData(ApnContext paramApnContext)
    {
        boolean bool = false;
        log("trySetupData for type:" + paramApnContext.getApnType() + " due to " + paramApnContext.getReason());
        log("trySetupData with mIsPsRestricted=" + this.mIsPsRestricted);
        if (this.mPhone.getSimulatedRadioControl() != null)
        {
            paramApnContext.setState(DataConnectionTracker.State.CONNECTED);
            this.mPhone.notifyDataConnection(paramApnContext.getReason(), paramApnContext.getApnType());
            log("trySetupData: (fix?) We're on the simulator; assuming data is connected");
            bool = true;
        }
        while (true)
        {
            return bool;
            this.mPhone.getServiceStateTracker().getDesiredPowerState();
            if (((paramApnContext.getState() == DataConnectionTracker.State.IDLE) || (paramApnContext.getState() == DataConnectionTracker.State.SCANNING)) && (isDataAllowed(paramApnContext)) && (getAnyDataEnabled()) && (!isEmergency()))
            {
                if (paramApnContext.getState() == DataConnectionTracker.State.IDLE)
                {
                    ArrayList localArrayList = buildWaitingApns(paramApnContext.getApnType());
                    if (localArrayList.isEmpty())
                    {
                        log("trySetupData: No APN found");
                        notifyNoData(DataConnection.FailCause.MISSING_UNKNOWN_APN, paramApnContext);
                        notifyOffApnsOfAvailability(paramApnContext.getReason());
                    }
                    else
                    {
                        paramApnContext.setWaitingApns(localArrayList);
                        log("trySetupData: Create from mAllApns : " + apnListToString(this.mAllApns));
                    }
                }
                else
                {
                    log("Setup watingApns : " + apnListToString(paramApnContext.getWaitingApns()));
                    bool = setupData(paramApnContext);
                    notifyOffApnsOfAvailability(paramApnContext.getReason());
                }
            }
            else
            {
                if ((!paramApnContext.getApnType().equals("default")) && ((paramApnContext.getState() == DataConnectionTracker.State.IDLE) || (paramApnContext.getState() == DataConnectionTracker.State.SCANNING)))
                    this.mPhone.notifyDataConnectionFailed(paramApnContext.getReason(), paramApnContext.getApnType());
                notifyOffApnsOfAvailability(paramApnContext.getReason());
            }
        }
    }

    private boolean trySetupData(String paramString1, String paramString2)
    {
        StringBuilder localStringBuilder = new StringBuilder().append("trySetupData: ").append(paramString2).append(" due to ");
        if (paramString1 == null);
        for (String str = "(unspecified)"; ; str = paramString1)
        {
            log(str + " isPsRestricted=" + this.mIsPsRestricted);
            if (paramString2 == null)
                paramString2 = "default";
            ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString2);
            if (localApnContext == null)
            {
                log("trySetupData new apn context for type:" + paramString2);
                localApnContext = new ApnContext(paramString2, "GSM");
                this.mApnContexts.put(paramString2, localApnContext);
            }
            localApnContext.setReason(paramString1);
            return trySetupData(localApnContext);
        }
    }

    private void updateDataActivity()
    {
        DataConnectionTracker.TxRxSum localTxRxSum1 = new DataConnectionTracker.TxRxSum(this, this.mTxPkts, this.mRxPkts);
        DataConnectionTracker.TxRxSum localTxRxSum2 = new DataConnectionTracker.TxRxSum(this);
        localTxRxSum2.updateTxRxSum();
        this.mTxPkts = localTxRxSum2.txPkts;
        this.mRxPkts = localTxRxSum2.rxPkts;
        long l1;
        long l2;
        DataConnectionTracker.Activity localActivity;
        if ((this.mNetStatPollEnabled) && ((localTxRxSum1.txPkts > 0L) || (localTxRxSum1.rxPkts > 0L)))
        {
            l1 = this.mTxPkts - localTxRxSum1.txPkts;
            l2 = this.mRxPkts - localTxRxSum1.rxPkts;
            if ((l1 <= 0L) || (l2 <= 0L))
                break label140;
            localActivity = DataConnectionTracker.Activity.DATAINANDOUT;
        }
        while (true)
        {
            if ((this.mActivity != localActivity) && (this.mIsScreenOn))
            {
                this.mActivity = localActivity;
                this.mPhone.notifyDataActivity();
            }
            return;
            label140: if ((l1 > 0L) && (l2 == 0L))
                localActivity = DataConnectionTracker.Activity.DATAOUT;
            else if ((l1 == 0L) && (l2 > 0L))
                localActivity = DataConnectionTracker.Activity.DATAIN;
            else
                localActivity = DataConnectionTracker.Activity.NONE;
        }
    }

    private void updateDataStallInfo()
    {
        DataConnectionTracker.TxRxSum localTxRxSum = new DataConnectionTracker.TxRxSum(this, this.mDataStallTxRxSum);
        this.mDataStallTxRxSum.updateTxRxSum();
        long l1 = this.mDataStallTxRxSum.txPkts - localTxRxSum.txPkts;
        long l2 = this.mDataStallTxRxSum.rxPkts - localTxRxSum.rxPkts;
        if ((l1 > 0L) && (l2 > 0L))
        {
            this.mSentSinceLastRecv = 0L;
            putRecoveryAction(0);
        }
        while (true)
        {
            return;
            if ((l1 > 0L) && (l2 == 0L))
            {
                if (this.mPhone.getState() == Phone.State.IDLE);
                for (this.mSentSinceLastRecv = (l1 + this.mSentSinceLastRecv); ; this.mSentSinceLastRecv = 0L)
                {
                    log("updateDataStallInfo: OUT sent=" + l1 + " mSentSinceLastRecv=" + this.mSentSinceLastRecv);
                    break;
                }
            }
            if ((l1 == 0L) && (l2 > 0L))
            {
                this.mSentSinceLastRecv = 0L;
                putRecoveryAction(0);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    String callApnIdToType(int paramInt)
    {
        return apnIdToType(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean callIsDataAllowed()
    {
        return isDataAllowed();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean callIsMmsDataEnabled()
    {
        return isMmsDataEnabled();
    }

    protected void cleanUpAllConnections(boolean paramBoolean, String paramString)
    {
        log("cleanUpAllConnections: tearDown=" + paramBoolean + " reason=" + paramString);
        Iterator localIterator = this.mApnContexts.values().iterator();
        while (localIterator.hasNext())
        {
            ApnContext localApnContext = (ApnContext)localIterator.next();
            localApnContext.setReason(paramString);
            cleanUpConnection(paramBoolean, localApnContext);
        }
        stopNetStatPoll();
        stopDataStallAlarm();
        this.mRequestedApnType = "default";
    }

    /** @deprecated */
    public int disableApnType(String paramString)
    {
        try
        {
            log("disableApnType:" + paramString);
            ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
            int i;
            if (localApnContext != null)
            {
                setEnabled(apnTypeToId(paramString), false);
                if ((localApnContext.getState() != DataConnectionTracker.State.IDLE) && (localApnContext.getState() != DataConnectionTracker.State.FAILED))
                {
                    log("diableApnType: return APN_REQUEST_STARTED");
                    i = 1;
                }
            }
            while (true)
            {
                return i;
                log("disableApnType: return APN_ALREADY_INACTIVE");
                i = 4;
                continue;
                log("disableApnType: no apn context was found, return APN_REQUEST_FAILED");
                i = 3;
            }
        }
        finally
        {
        }
    }

    public void dispose()
    {
        log("GsmDCT.dispose");
        cleanUpAllConnections(false, null);
        super.dispose();
        this.mPhone.mCM.unregisterForAvailable(this);
        this.mPhone.mCM.unregisterForOffOrNotAvailable(this);
        this.mPhone.mIccRecords.unregisterForRecordsLoaded(this);
        this.mPhone.mCM.unregisterForDataNetworkStateChanged(this);
        this.mPhone.getCallTracker().unregisterForVoiceCallEnded(this);
        this.mPhone.getCallTracker().unregisterForVoiceCallStarted(this);
        this.mPhone.getServiceStateTracker().unregisterForDataConnectionAttached(this);
        this.mPhone.getServiceStateTracker().unregisterForDataConnectionDetached(this);
        this.mPhone.getServiceStateTracker().unregisterForRoamingOn(this);
        this.mPhone.getServiceStateTracker().unregisterForRoamingOff(this);
        this.mPhone.getServiceStateTracker().unregisterForPsRestrictedEnabled(this);
        this.mPhone.getServiceStateTracker().unregisterForPsRestrictedDisabled(this);
        this.mPhone.getContext().getContentResolver().unregisterContentObserver(this.mApnObserver);
        this.mApnContexts.clear();
        destroyDataConnections();
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("GsmDataConnectionTracker extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" RADIO_TESTS=false");
        paramPrintWriter.println(" mReregisterOnReconnectFailure=" + this.mReregisterOnReconnectFailure);
        paramPrintWriter.println(" mResolver=" + this.mResolver);
        paramPrintWriter.println(" canSetPreferApn=" + this.canSetPreferApn);
        paramPrintWriter.println(" mApnObserver=" + this.mApnObserver);
        paramPrintWriter.println(" getOverallState=" + getOverallState());
    }

    /** @deprecated */
    public int enableApnType(String paramString)
    {
        int i = 1;
        try
        {
            ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
            if ((localApnContext == null) || (!isApnTypeAvailable(paramString)))
            {
                log("enableApnType: " + paramString + " is type not available");
                i = 2;
            }
            while (true)
            {
                return i;
                log("enableApnType: " + paramString + " mState(" + localApnContext.getState() + ")");
                if (localApnContext.getState() == DataConnectionTracker.State.CONNECTED)
                {
                    log("enableApnType: return APN_ALREADY_ACTIVE");
                    i = 0;
                }
                else
                {
                    setEnabled(apnTypeToId(paramString), true);
                    log("enableApnType: new apn request for type " + paramString + " return APN_REQUEST_STARTED");
                }
            }
        }
        finally
        {
        }
    }

    protected void finalize()
    {
        log("finalize");
    }

    protected GsmDataConnection findReadyDataConnection(ApnSetting paramApnSetting)
    {
        GsmDataConnection localGsmDataConnection = null;
        if (paramApnSetting == null);
        label6: Iterator localIterator;
        while (!localIterator.hasNext())
        {
            return localGsmDataConnection;
            log("findReadyDataConnection: apn string <" + paramApnSetting + ">" + " dcacs.size=" + this.mDataConnectionAsyncChannels.size());
            localIterator = this.mDataConnectionAsyncChannels.values().iterator();
        }
        DataConnectionAc localDataConnectionAc = (DataConnectionAc)localIterator.next();
        ApnSetting localApnSetting = localDataConnectionAc.getApnSettingSync();
        StringBuilder localStringBuilder = new StringBuilder().append("findReadyDataConnection: dc apn string <");
        if (localApnSetting != null);
        for (String str = localApnSetting.toString(); ; str = "null")
        {
            log(str + ">");
            if ((localApnSetting == null) || (!TextUtils.equals(localApnSetting.toString(), paramApnSetting.toString())))
                break;
            DataConnection localDataConnection = localDataConnectionAc.dataConnection;
            log("findReadyDataConnection: found ready GsmDataConnection= dcac=" + localDataConnectionAc + " dc=" + localDataConnection);
            localGsmDataConnection = (GsmDataConnection)localDataConnection;
            break label6;
        }
    }

    protected String getActionIntentDataStallAlarm()
    {
        return "com.android.internal.telephony.gprs-data-stall";
    }

    protected String getActionIntentReconnectAlarm()
    {
        return "com.android.internal.telephony.gprs-reconnect";
    }

    public String getActiveApnString(String paramString)
    {
        log("get active apn string for type:" + paramString);
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
        ApnSetting localApnSetting;
        if (localApnContext != null)
        {
            localApnSetting = localApnContext.getApnSetting();
            if (localApnSetting == null);
        }
        for (String str = localApnSetting.apn; ; str = null)
            return str;
    }

    public String[] getActiveApnTypes()
    {
        log("get all active apn types");
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mApnContexts.values().iterator();
        while (localIterator.hasNext())
        {
            ApnContext localApnContext = (ApnContext)localIterator.next();
            if (localApnContext.isReady())
                localArrayList.add(localApnContext.getApnType());
        }
        return (String[])localArrayList.toArray(new String[0]);
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean getAnyDataEnabled()
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_1
        //     2: aload_0
        //     3: getfield 1679	com/android/internal/telephony/DataConnectionTracker:mDataEnabledLock	Ljava/lang/Object;
        //     6: astore_2
        //     7: aload_2
        //     8: monitorenter
        //     9: aload_0
        //     10: invokevirtual 1529	com/android/internal/telephony/gsm/GsmDataConnectionTracker:isMmsDataEnabled	()Z
        //     13: ifne +28 -> 41
        //     16: aload_0
        //     17: getfield 1682	com/android/internal/telephony/DataConnectionTracker:mInternalDataEnabled	Z
        //     20: ifeq +16 -> 36
        //     23: aload_0
        //     24: getfield 1685	com/android/internal/telephony/DataConnectionTracker:mUserDataEnabled	Z
        //     27: ifeq +9 -> 36
        //     30: getstatic 925	com/android/internal/telephony/gsm/GsmDataConnectionTracker:sPolicyDataEnabled	Z
        //     33: ifne +8 -> 41
        //     36: aload_2
        //     37: monitorexit
        //     38: goto +58 -> 96
        //     41: aload_0
        //     42: getfield 259	com/android/internal/telephony/DataConnectionTracker:mApnContexts	Ljava/util/concurrent/ConcurrentHashMap;
        //     45: invokevirtual 477	java/util/concurrent/ConcurrentHashMap:values	()Ljava/util/Collection;
        //     48: invokeinterface 480 1 0
        //     53: astore 4
        //     55: aload 4
        //     57: invokeinterface 435 1 0
        //     62: ifeq +32 -> 94
        //     65: aload_0
        //     66: aload 4
        //     68: invokeinterface 439 1 0
        //     73: checkcast 248	com/android/internal/telephony/ApnContext
        //     76: invokespecial 1418	com/android/internal/telephony/gsm/GsmDataConnectionTracker:isDataAllowed	(Lcom/android/internal/telephony/ApnContext;)Z
        //     79: ifeq -24 -> 55
        //     82: iconst_1
        //     83: istore_1
        //     84: aload_2
        //     85: monitorexit
        //     86: goto +10 -> 96
        //     89: astore_3
        //     90: aload_2
        //     91: monitorexit
        //     92: aload_3
        //     93: athrow
        //     94: aload_2
        //     95: monitorexit
        //     96: iload_1
        //     97: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     9	92	89	finally
        //     94	96	89	finally
    }

    protected int getApnProfileID(String paramString)
    {
        int i;
        if (TextUtils.equals(paramString, "ims"))
            i = 2;
        while (true)
        {
            return i;
            if (TextUtils.equals(paramString, "fota"))
                i = 3;
            else if (TextUtils.equals(paramString, "cbs"))
                i = 4;
            else
                i = 0;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean getInternalDataEnabled()
    {
        return this.mInternalDataEnabled;
    }

    protected LinkCapabilities getLinkCapabilities(String paramString)
    {
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
        DataConnectionAc localDataConnectionAc;
        if (localApnContext != null)
        {
            localDataConnectionAc = localApnContext.getDataConnectionAc();
            if (localDataConnectionAc != null)
                log("get active pdp is not null, return link Capabilities for " + paramString);
        }
        for (LinkCapabilities localLinkCapabilities = localDataConnectionAc.getLinkCapabilitiesSync(); ; localLinkCapabilities = new LinkCapabilities())
        {
            return localLinkCapabilities;
            log("return new LinkCapabilities");
        }
    }

    protected LinkProperties getLinkProperties(String paramString)
    {
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
        DataConnectionAc localDataConnectionAc;
        if (localApnContext != null)
        {
            localDataConnectionAc = localApnContext.getDataConnectionAc();
            if (localDataConnectionAc != null)
                log("return link properites for " + paramString);
        }
        for (LinkProperties localLinkProperties = localDataConnectionAc.getLinkPropertiesSync(); ; localLinkProperties = new LinkProperties())
        {
            return localLinkProperties;
            log("return new LinkProperties");
        }
    }

    public DataConnectionTracker.State getOverallState()
    {
        int i = 0;
        int j = 1;
        int k = 0;
        Iterator localIterator = this.mApnContexts.values().iterator();
        DataConnectionTracker.State localState;
        while (localIterator.hasNext())
        {
            ApnContext localApnContext = (ApnContext)localIterator.next();
            if (localApnContext.isEnabled())
            {
                k = 1;
                switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$State[localApnContext.getState().ordinal()])
                {
                default:
                    break;
                case 1:
                case 2:
                    log("overall state is CONNECTED");
                    localState = DataConnectionTracker.State.CONNECTED;
                case 3:
                case 4:
                case 5:
                case 6:
                }
            }
        }
        while (true)
        {
            return localState;
            i = 1;
            j = 0;
            break;
            j = 0;
            break;
            if (k == 0)
            {
                log("overall state is IDLE");
                localState = DataConnectionTracker.State.IDLE;
            }
            else if (i != 0)
            {
                log("overall state is CONNECTING");
                localState = DataConnectionTracker.State.CONNECTING;
            }
            else if (j == 0)
            {
                log("overall state is IDLE");
                localState = DataConnectionTracker.State.IDLE;
            }
            else
            {
                log("overall state is FAILED");
                localState = DataConnectionTracker.State.FAILED;
            }
        }
    }

    public int getRecoveryAction()
    {
        return Settings.System.getInt(this.mPhone.getContext().getContentResolver(), "radio.data.stall.recovery.action", 0);
    }

    public DataConnectionTracker.State getState(String paramString)
    {
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
        if (localApnContext != null);
        for (DataConnectionTracker.State localState = localApnContext.getState(); ; localState = DataConnectionTracker.State.FAILED)
            return localState;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean getUserDataEnabled()
    {
        return this.mUserDataEnabled;
    }

    protected void gotoIdleAndNotifyDataConnection(String paramString)
    {
        log("gotoIdleAndNotifyDataConnection: reason=" + paramString);
        notifyDataConnection(paramString);
        this.mActiveApn = null;
    }

    public void handleMessage(Message paramMessage)
    {
        boolean bool = false;
        log("handleMessage msg=" + paramMessage);
        if ((!this.mPhone.mIsTheCurrentActivePhone) || (this.mIsDisposed))
            loge("handleMessage: Ignore GSM msgs since GSM phone is inactive");
        while (true)
        {
            return;
            switch (paramMessage.what)
            {
            case 270342:
            case 270343:
            case 270344:
            case 270346:
            case 270347:
            case 270348:
            case 270349:
            case 270350:
            case 270351:
            case 270353:
            case 270356:
            case 270357:
            default:
                super.handleMessage(paramMessage);
                break;
            case 270338:
                onRecordsLoaded();
                break;
            case 270345:
                onDataConnectionDetached();
                break;
            case 270352:
                onDataConnectionAttached();
                break;
            case 270340:
                onDataStateChanged((AsyncResult)paramMessage.obj);
                break;
            case 270341:
                onPollPdp();
                break;
            case 270354:
                doRecovery();
                break;
            case 270355:
                onApnChanged();
                break;
            case 270358:
                log("EVENT_PS_RESTRICT_ENABLED " + this.mIsPsRestricted);
                stopNetStatPoll();
                stopDataStallAlarm();
                this.mIsPsRestricted = true;
                break;
            case 270359:
                log("EVENT_PS_RESTRICT_DISABLED " + this.mIsPsRestricted);
                this.mIsPsRestricted = false;
                if (isConnected())
                {
                    startNetStatPoll();
                    startDataStallAlarm(false);
                }
                else
                {
                    if (this.mState == DataConnectionTracker.State.FAILED)
                    {
                        cleanUpAllConnections(false, "psRestrictEnabled");
                        resetAllRetryCounts();
                        this.mReregisterOnReconnectFailure = false;
                    }
                    trySetupData("psRestrictEnabled", "default");
                }
                break;
            case 270339:
                if ((paramMessage.obj instanceof ApnContext))
                    onTrySetupData((ApnContext)paramMessage.obj);
                else if ((paramMessage.obj instanceof String))
                    onTrySetupData((String)paramMessage.obj);
                else
                    loge("EVENT_TRY_SETUP request w/o apnContext or String");
                break;
            case 270360:
                if (paramMessage.arg1 == 0);
                while (true)
                {
                    log("EVENT_CLEAN_UP_CONNECTION tearDown=" + bool);
                    if (!(paramMessage.obj instanceof ApnContext))
                        break label480;
                    cleanUpConnection(bool, (ApnContext)paramMessage.obj);
                    break;
                    bool = true;
                }
                label480: loge("EVENT_CLEAN_UP_CONNECTION request w/o apn context");
            }
        }
    }

    protected void initApnContextsAndDataConnection()
    {
        boolean bool = SystemProperties.getBoolean("net.def_data_on_boot", true);
        String[] arrayOfString = this.mPhone.getContext().getResources().getStringArray(17235987);
        int i = arrayOfString.length;
        int j = 0;
        while (j < i)
        {
            NetworkConfig localNetworkConfig = new NetworkConfig(arrayOfString[j]);
            ApnContext localApnContext1;
            switch (localNetworkConfig.type)
            {
            case 1:
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                j++;
                break;
            case 0:
                localApnContext1 = addApnContext("default");
                localApnContext1.setEnabled(bool);
            case 2:
            case 3:
            case 4:
            case 5:
            case 10:
            case 11:
            case 12:
                while (localApnContext1 != null)
                {
                    onSetDependencyMet(localApnContext1.getApnType(), localNetworkConfig.dependencyMet);
                    break;
                    localApnContext1 = addApnContext("mms");
                    continue;
                    localApnContext1 = addApnContext("supl");
                    continue;
                    localApnContext1 = addApnContext("dun");
                    continue;
                    ApnContext localApnContext2 = addApnContext("hipri");
                    ApnContext localApnContext3 = (ApnContext)this.mApnContexts.get("default");
                    if (localApnContext3 == null)
                        break;
                    applyNewState(localApnContext2, localApnContext2.isEnabled(), localApnContext3.getDependencyMet());
                    break;
                    localApnContext1 = addApnContext("fota");
                    continue;
                    localApnContext1 = addApnContext("ims");
                    continue;
                    localApnContext1 = addApnContext("cbs");
                }
            }
        }
    }

    public boolean isApnTypeActive(String paramString)
    {
        boolean bool = false;
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
        if (localApnContext == null);
        while (true)
        {
            return bool;
            if (localApnContext.getDataConnection() != null)
                bool = true;
        }
    }

    protected boolean isApnTypeAvailable(String paramString)
    {
        boolean bool = true;
        if ((paramString.equals("dun")) && (fetchDunApn() != null));
        while (true)
        {
            return bool;
            if (this.mAllApns != null)
            {
                Iterator localIterator = this.mAllApns.iterator();
                while (true)
                    if (localIterator.hasNext())
                        if (((ApnSetting)localIterator.next()).canHandleType(paramString))
                            break;
            }
            bool = false;
        }
    }

    public boolean isApnTypeEnabled(String paramString)
    {
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
        if (localApnContext == null);
        for (boolean bool = false; ; bool = localApnContext.isEnabled())
            return bool;
    }

    protected boolean isConnected()
    {
        Iterator localIterator = this.mApnContexts.values().iterator();
        do
            if (!localIterator.hasNext())
                break;
        while (((ApnContext)localIterator.next()).getState() != DataConnectionTracker.State.CONNECTED);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected boolean isDataAllowed()
    {
        while (true)
        {
            synchronized (this.mDataEnabledLock)
            {
                boolean bool1 = this.mInternalDataEnabled;
                int i = this.mPhone.getServiceStateTracker().getCurrentDataConnectionState();
                boolean bool2 = this.mPhone.getServiceStateTracker().getDesiredPowerState();
                if (((i == 0) || (this.mAutoAttachOnCreation)) && (this.mPhone.mIccRecords.getRecordsLoaded()) && ((this.mPhone.getState() == Phone.State.IDLE) || (this.mPhone.getServiceStateTracker().isConcurrentVoiceAndDataAllowed())) && (bool1) && ((!this.mPhone.getServiceState().getRoaming()) || (getDataOnRoamingEnabled())) && (!this.mIsPsRestricted) && (bool2))
                {
                    bool3 = true;
                    if (!bool3)
                    {
                        String str1 = "";
                        if ((i != 0) && (!this.mAutoAttachOnCreation))
                            str1 = str1 + " - gprs= " + i;
                        if (!this.mPhone.mIccRecords.getRecordsLoaded())
                            str1 = str1 + " - SIM not loaded";
                        if ((this.mPhone.getState() != Phone.State.IDLE) && (!this.mPhone.getServiceStateTracker().isConcurrentVoiceAndDataAllowed()))
                        {
                            String str2 = str1 + " - PhoneState= " + this.mPhone.getState();
                            str1 = str2 + " - Concurrent voice and data not allowed";
                        }
                        if (!bool1)
                            str1 = str1 + " - mInternalDataEnabled= false";
                        if ((this.mPhone.getServiceState().getRoaming()) && (!getDataOnRoamingEnabled()))
                            str1 = str1 + " - Roaming and data roaming not enabled";
                        if (this.mIsPsRestricted)
                            str1 = str1 + " - mIsPsRestricted= true";
                        if (!bool2)
                            str1 = str1 + " - desiredPowerState= false";
                        log("isDataAllowed: not allowed due to" + str1);
                    }
                    return bool3;
                }
            }
            boolean bool3 = false;
        }
    }

    protected boolean isDataPossible(String paramString)
    {
        boolean bool1 = false;
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(paramString);
        if (localApnContext == null)
            return bool1;
        boolean bool2 = localApnContext.isEnabled();
        DataConnectionTracker.State localState = localApnContext.getState();
        boolean bool3;
        label48: boolean bool4;
        if ((!bool2) || (localState != DataConnectionTracker.State.FAILED))
        {
            bool3 = true;
            bool4 = isDataAllowed();
            if ((!bool4) || (!bool3))
                break label145;
        }
        label145: for (boolean bool5 = true; ; bool5 = false)
        {
            Object[] arrayOfObject = new Object[6];
            arrayOfObject[bool1] = paramString;
            arrayOfObject[1] = Boolean.valueOf(bool5);
            arrayOfObject[2] = Boolean.valueOf(bool4);
            arrayOfObject[3] = Boolean.valueOf(bool3);
            arrayOfObject[4] = Boolean.valueOf(bool2);
            arrayOfObject[5] = localState;
            log(String.format("isDataPossible(%s): possible=%b isDataAllowed=%b apnTypePossible=%b apnContextisEnabled=%b apnContextState()=%s", arrayOfObject));
            bool1 = bool5;
            break;
            bool3 = false;
            break label48;
        }
    }

    public boolean isDisconnected()
    {
        Iterator localIterator = this.mApnContexts.values().iterator();
        do
            if (!localIterator.hasNext())
                break;
        while (((ApnContext)localIterator.next()).isDisconnected());
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    protected void log(String paramString)
    {
        Log.d("GSM", "[GsmDCT] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("GSM", "[GsmDCT] " + paramString);
    }

    protected void notifyDataConnection(String paramString)
    {
        log("notifyDataConnection: reason=" + paramString);
        Iterator localIterator = this.mApnContexts.values().iterator();
        while (localIterator.hasNext())
        {
            ApnContext localApnContext = (ApnContext)localIterator.next();
            if (localApnContext.isReady())
            {
                log("notifyDataConnection: type:" + localApnContext.getApnType());
                PhoneBase localPhoneBase = this.mPhone;
                if (paramString != null);
                for (String str = paramString; ; str = localApnContext.getReason())
                {
                    localPhoneBase.notifyDataConnection(str, localApnContext.getApnType());
                    break;
                }
            }
        }
        notifyOffApnsOfAvailability(paramString);
    }

    protected void notifyOffApnsOfAvailability(String paramString)
    {
        Iterator localIterator = this.mApnContexts.values().iterator();
        while (localIterator.hasNext())
        {
            ApnContext localApnContext = (ApnContext)localIterator.next();
            if (!localApnContext.isReady())
            {
                log("notifyOffApnOfAvailability type:" + localApnContext.getApnType());
                PhoneBase localPhoneBase = this.mPhone;
                if (paramString != null);
                for (String str = paramString; ; str = localApnContext.getReason())
                {
                    localPhoneBase.notifyDataConnection(str, localApnContext.getApnType(), Phone.DataState.DISCONNECTED);
                    break;
                }
            }
            log("notifyOffApnsOfAvailability skipped apn due to isReady==false: " + localApnContext.toString());
        }
    }

    protected void onActionIntentReconnectAlarm(Intent paramIntent)
    {
        String str = paramIntent.getStringExtra("reconnect_alarm_extra_reason");
        int i = paramIntent.getIntExtra("reconnect_alarm_extra_type", -1);
        int j = paramIntent.getIntExtra("reconnect_alaram_extra_retry_count", 0);
        DataConnectionAc localDataConnectionAc = (DataConnectionAc)this.mDataConnectionAsyncChannels.get(Integer.valueOf(i));
        log("onActionIntentReconnectAlarm: mState=" + this.mState + " reason=" + str + " connectionId=" + i + " retryCount=" + j);
        if (localDataConnectionAc != null)
        {
            Iterator localIterator = localDataConnectionAc.getApnListSync().iterator();
            while (localIterator.hasNext())
            {
                ApnContext localApnContext = (ApnContext)localIterator.next();
                localApnContext.setDataConnectionAc(null);
                localApnContext.setDataConnection(null);
                localApnContext.setReason(str);
                localApnContext.setRetryCount(j);
                if (localApnContext.getState() == DataConnectionTracker.State.FAILED)
                    localApnContext.setState(DataConnectionTracker.State.IDLE);
                sendMessage(obtainMessage(270339, localApnContext));
            }
            localDataConnectionAc.setReconnectIntentSync(null);
        }
    }

    protected void onCleanUpAllConnections(String paramString)
    {
        cleanUpAllConnections(true, paramString);
    }

    protected void onCleanUpConnection(boolean paramBoolean, int paramInt, String paramString)
    {
        log("onCleanUpConnection");
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(apnIdToType(paramInt));
        if (localApnContext != null)
        {
            localApnContext.setReason(paramString);
            cleanUpConnection(paramBoolean, localApnContext);
        }
    }

    protected void onDataConnectionDetached()
    {
        log("onDataConnectionDetached: stop polling and notify detached");
        stopNetStatPoll();
        stopDataStallAlarm();
        notifyDataConnection("dataDetached");
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onDataSetupComplete(AsyncResult paramAsyncResult)
    {
        DataConnection.FailCause localFailCause = DataConnection.FailCause.UNKNOWN;
        int i = 0;
        ApnContext localApnContext;
        DataConnectionAc localDataConnectionAc;
        if ((paramAsyncResult.userObj instanceof ApnContext))
        {
            localApnContext = (ApnContext)paramAsyncResult.userObj;
            if (!isDataSetupCompleteOk(paramAsyncResult))
                break label402;
            localDataConnectionAc = localApnContext.getDataConnectionAc();
            if (localDataConnectionAc != null)
                break label135;
            log("onDataSetupComplete: no connection to DC, handle as error");
            localFailCause = DataConnection.FailCause.CONNECTION_TO_DATACONNECTIONAC_BROKEN;
            i = 1;
            if (i != 0)
            {
                if (!localApnContext.getWaitingApns().isEmpty())
                    break label665;
                if (localApnContext.getWaitingApnsPermFailCount() != 0)
                    break label602;
                log("onDataSetupComplete: All APN's had permanent failures, stop retrying");
                localApnContext.setState(DataConnectionTracker.State.FAILED);
                this.mPhone.notifyDataConnection("apnFailed", localApnContext.getApnType());
                localApnContext.setDataConnection(null);
                localApnContext.setDataConnectionAc(null);
            }
        }
        while (true)
        {
            while (true)
            {
                return;
                throw new RuntimeException("onDataSetupComplete: No apnContext");
                label135: localApnContext.getDataConnection();
                ApnSetting localApnSetting2 = localApnContext.getApnSetting();
                StringBuilder localStringBuilder = new StringBuilder().append("onDataSetupComplete: success apn=");
                String str2;
                if (localApnSetting2 == null)
                {
                    str2 = "unknown";
                    log(str2);
                    if ((localApnSetting2 == null) || (localApnSetting2.proxy == null) || (localApnSetting2.proxy.length() == 0));
                }
                try
                {
                    String str3 = localApnSetting2.port;
                    if (TextUtils.isEmpty(str3))
                        str3 = "8080";
                    localDataConnectionAc.setLinkPropertiesHttpProxySync(new ProxyProperties(localApnSetting2.proxy, Integer.parseInt(str3), null));
                    Injector.onDataSetupComplete(localApnContext);
                    if (TextUtils.equals(localApnContext.getApnType(), "default"))
                    {
                        SystemProperties.set("gsm.defaultpdpcontext.active", "true");
                        if ((this.canSetPreferApn) && (this.mPreferredApn == null))
                        {
                            log("onDataSetupComplete: PREFERED APN is null");
                            this.mPreferredApn = localApnSetting2;
                            if (this.mPreferredApn != null)
                                setPreferredApn(this.mPreferredApn.id);
                        }
                        notifyDefaultData(localApnContext);
                        break;
                        str2 = localApnSetting2.apn;
                    }
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    while (true)
                    {
                        loge("onDataSetupComplete: NumberFormatException making ProxyProperties (" + localApnSetting2.port + "): " + localNumberFormatException);
                        continue;
                        SystemProperties.set("gsm.defaultpdpcontext.active", "false");
                    }
                }
            }
            label402: localFailCause = (DataConnection.FailCause)paramAsyncResult.result;
            ApnSetting localApnSetting1 = localApnContext.getApnSetting();
            Object[] arrayOfObject1 = new Object[2];
            if (localApnSetting1 == null);
            for (String str1 = "unknown"; ; str1 = localApnSetting1.apn)
            {
                arrayOfObject1[0] = str1;
                arrayOfObject1[1] = localFailCause;
                log(String.format("onDataSetupComplete: error apn=%s cause=%s", arrayOfObject1));
                if (localFailCause.isEventLoggable())
                {
                    int k = getCellLocationId();
                    Object[] arrayOfObject3 = new Object[3];
                    arrayOfObject3[0] = Integer.valueOf(localFailCause.ordinal());
                    arrayOfObject3[1] = Integer.valueOf(k);
                    arrayOfObject3[2] = Integer.valueOf(TelephonyManager.getDefault().getNetworkType());
                    EventLog.writeEvent(50105, arrayOfObject3);
                }
                if (localFailCause.isPermanentFail())
                    localApnContext.decWaitingApnsPermFailCount();
                localApnContext.removeWaitingApn(localApnContext.getApnSetting());
                Object[] arrayOfObject2 = new Object[2];
                arrayOfObject2[0] = Integer.valueOf(localApnContext.getWaitingApns().size());
                arrayOfObject2[1] = Integer.valueOf(localApnContext.getWaitingApnsPermFailCount());
                log(String.format("onDataSetupComplete: WaitingApns.size=%d WaitingApnsPermFailureCountDown=%d", arrayOfObject2));
                i = 1;
                break;
            }
            label602: log("onDataSetupComplete: Not all permanent failures, retry");
            int j = -1;
            if ((paramAsyncResult.exception instanceof DataConnection.CallSetupException))
                j = ((DataConnection.CallSetupException)paramAsyncResult.exception).getRetryOverride();
            if (j == 2147483647)
            {
                log("No retry is suggested.");
            }
            else
            {
                startDelayedRetry(localFailCause, localApnContext, j);
                continue;
                label665: log("onDataSetupComplete: Try next APN");
                localApnContext.setState(DataConnectionTracker.State.SCANNING);
                startAlarmForReconnect(APN_DELAY_MILLIS, localApnContext);
            }
        }
    }

    protected void onDataStallAlarm(int paramInt)
    {
        if (this.mDataStallAlarmTag != paramInt)
            log("onDataStallAlarm: ignore, tag=" + paramInt + " expecting " + this.mDataStallAlarmTag);
        while (true)
        {
            return;
            updateDataStallInfo();
            int i = Settings.Secure.getInt(this.mResolver, "pdp_watchdog_trigger_packet_count", 10);
            boolean bool = false;
            if (this.mSentSinceLastRecv >= i)
            {
                log("onDataStallAlarm: tag=" + paramInt + " do recovery action=" + getRecoveryAction());
                bool = true;
                sendMessage(obtainMessage(270354));
            }
            startDataStallAlarm(bool);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onDisconnectDone(int paramInt, AsyncResult paramAsyncResult)
    {
        ApnContext localApnContext;
        if ((paramAsyncResult.userObj instanceof ApnContext))
        {
            localApnContext = (ApnContext)paramAsyncResult.userObj;
            log("onDisconnectDone: EVENT_DISCONNECT_DONE apnContext=" + localApnContext);
            localApnContext.setState(DataConnectionTracker.State.IDLE);
            Injector.onDisconnectDone(localApnContext);
            this.mPhone.notifyDataConnection(localApnContext.getReason(), localApnContext.getApnType());
            if ((!isDisconnected()) || (!this.mPhone.getServiceStateTracker().processPendingRadioPowerOffAfterDataOff()))
                break label114;
            localApnContext.setApnSetting(null);
            localApnContext.setDataConnection(null);
            localApnContext.setDataConnectionAc(null);
        }
        while (true)
        {
            return;
            loge("onDisconnectDone: Invalid ar in onDisconnectDone, ignore");
            continue;
            label114: if ((localApnContext.isReady()) && (retryAfterDisconnected(localApnContext.getReason())))
            {
                SystemProperties.set("gsm.defaultpdpcontext.active", "false");
                startAlarmForReconnect(APN_DELAY_MILLIS, localApnContext);
            }
            else
            {
                localApnContext.setApnSetting(null);
                localApnContext.setDataConnection(null);
                localApnContext.setDataConnectionAc(null);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onEnableApn(int paramInt1, int paramInt2)
    {
        int i = 1;
        this.mRequestedApnType = Injector.getApnType(this, paramInt2, paramInt1);
        ApnContext localApnContext = (ApnContext)this.mApnContexts.get(apnIdToType(paramInt1));
        if (localApnContext == null)
        {
            loge("onEnableApn(" + paramInt1 + ", " + paramInt2 + "): NO ApnContext");
            return;
        }
        log("onEnableApn: apnContext=" + localApnContext + " call applyNewState");
        if (paramInt2 == i);
        while (true)
        {
            applyNewState(localApnContext, i, localApnContext.getDependencyMet());
            break;
            i = 0;
        }
    }

    protected void onPollPdp()
    {
        if (getOverallState() == DataConnectionTracker.State.CONNECTED)
        {
            this.mPhone.mCM.getDataCallList(obtainMessage(270340));
            sendMessageDelayed(obtainMessage(270341), 5000L);
        }
    }

    protected void onRadioAvailable()
    {
        log("onRadioAvailable");
        if (this.mPhone.getSimulatedRadioControl() != null)
        {
            notifyDataConnection(null);
            log("onRadioAvailable: We're on the simulator; assuming data is connected");
        }
        if (this.mPhone.mIccRecords.getRecordsLoaded())
            notifyOffApnsOfAvailability(null);
        if (getOverallState() != DataConnectionTracker.State.IDLE)
            cleanUpConnection(true, null);
    }

    protected void onRadioOffOrNotAvailable()
    {
        resetAllRetryCounts();
        this.mReregisterOnReconnectFailure = false;
        if (this.mPhone.getSimulatedRadioControl() != null)
            log("We're on the simulator; assuming radio off is meaningless");
        while (true)
        {
            notifyOffApnsOfAvailability(null);
            return;
            log("onRadioOffOrNotAvailable: is off and clean up all connections");
            cleanUpAllConnections(false, "radioTurnedOff");
        }
    }

    protected void onRoamingOff()
    {
        log("onRoamingOff");
        if (!this.mUserDataEnabled);
        while (true)
        {
            return;
            if (!getDataOnRoamingEnabled())
            {
                notifyOffApnsOfAvailability("roamingOff");
                setupDataOnReadyApns("roamingOff");
            }
            else
            {
                notifyDataConnection("roamingOff");
            }
        }
    }

    protected void onRoamingOn()
    {
        if (!this.mUserDataEnabled);
        while (true)
        {
            return;
            if (getDataOnRoamingEnabled())
            {
                log("onRoamingOn: setup data on roaming");
                setupDataOnReadyApns("roamingOn");
                notifyDataConnection("roamingOn");
            }
            else
            {
                log("onRoamingOn: Tear down data connection on roaming.");
                cleanUpAllConnections(true, "roamingOn");
                notifyOffApnsOfAvailability("roamingOn");
            }
        }
    }

    protected void onSetDependencyMet(String paramString, boolean paramBoolean)
    {
        if ("hipri".equals(paramString));
        while (true)
        {
            return;
            ApnContext localApnContext1 = (ApnContext)this.mApnContexts.get(paramString);
            if (localApnContext1 == null)
            {
                loge("onSetDependencyMet: ApnContext not found in onSetDependencyMet(" + paramString + ", " + paramBoolean + ")");
            }
            else
            {
                applyNewState(localApnContext1, localApnContext1.isEnabled(), paramBoolean);
                if ("default".equals(paramString))
                {
                    ApnContext localApnContext2 = (ApnContext)this.mApnContexts.get("hipri");
                    if (localApnContext2 != null)
                        applyNewState(localApnContext2, localApnContext2.isEnabled(), paramBoolean);
                }
            }
        }
    }

    protected boolean onTrySetupData(ApnContext paramApnContext)
    {
        log("onTrySetupData: apnContext=" + paramApnContext);
        return trySetupData(paramApnContext);
    }

    protected boolean onTrySetupData(String paramString)
    {
        log("onTrySetupData: reason=" + paramString);
        setupDataOnReadyApns(paramString);
        return true;
    }

    protected void onVoiceCallEnded()
    {
        log("onVoiceCallEnded");
        if (isConnected())
        {
            if (this.mPhone.getServiceStateTracker().isConcurrentVoiceAndDataAllowed())
                break label51;
            startNetStatPoll();
            startDataStallAlarm(false);
            notifyDataConnection("2GVoiceCallEnded");
        }
        while (true)
        {
            setupDataOnReadyApns("2GVoiceCallEnded");
            return;
            label51: resetPollStats();
        }
    }

    protected void onVoiceCallStarted()
    {
        log("onVoiceCallStarted");
        if ((isConnected()) && (!this.mPhone.getServiceStateTracker().isConcurrentVoiceAndDataAllowed()))
        {
            log("onVoiceCallStarted stop polling");
            stopNetStatPoll();
            stopDataStallAlarm();
            notifyDataConnection("2GVoiceCallStarted");
        }
    }

    public void putRecoveryAction(int paramInt)
    {
        Settings.System.putInt(this.mPhone.getContext().getContentResolver(), "radio.data.stall.recovery.action", paramInt);
    }

    protected void restartDataStallAlarm()
    {
        if (!isConnected());
        while (true)
        {
            return;
            if (RecoveryAction.isAggressiveRecovery(getRecoveryAction()))
            {
                log("data stall recovery action is pending. not resetting the alarm.");
            }
            else
            {
                stopDataStallAlarm();
                startDataStallAlarm(false);
            }
        }
    }

    protected void restartRadio()
    {
        log("restartRadio: ************TURN OFF RADIO**************");
        cleanUpAllConnections(true, "radioTurnedOff");
        this.mPhone.getServiceStateTracker().powerOffRadioSafely(this);
        SystemProperties.set("net.ppp.reset-by-timeout", String.valueOf(1 + Integer.parseInt(SystemProperties.get("net.ppp.reset-by-timeout", "0"))));
    }

    protected void setState(DataConnectionTracker.State paramState)
    {
        log("setState should not be used in GSM" + paramState);
    }

    protected void startNetStatPoll()
    {
        if ((getOverallState() == DataConnectionTracker.State.CONNECTED) && (!this.mNetStatPollEnabled))
        {
            log("startNetStatPoll");
            resetPollStats();
            this.mNetStatPollEnabled = true;
            this.mPollNetStat.run();
        }
    }

    protected void stopNetStatPoll()
    {
        this.mNetStatPollEnabled = false;
        removeCallbacks(this.mPollNetStat);
        log("stopNetStatPoll");
    }

    private static class RecoveryAction
    {
        public static final int CLEANUP = 1;
        public static final int GET_DATA_CALL_LIST = 0;
        public static final int RADIO_RESTART = 3;
        public static final int RADIO_RESTART_WITH_PROP = 4;
        public static final int REREGISTER = 2;

        private static boolean isAggressiveRecovery(int paramInt)
        {
            int i = 1;
            if ((paramInt == i) || (paramInt == 2) || (paramInt == 3) || (paramInt == 4));
            while (true)
            {
                return i;
                i = 0;
            }
        }
    }

    private class ApnChangeObserver extends ContentObserver
    {
        public ApnChangeObserver()
        {
            super();
        }

        public void onChange(boolean paramBoolean)
        {
            GsmDataConnectionTracker.this.sendMessage(GsmDataConnectionTracker.this.obtainMessage(270355));
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String getApnType(GsmDataConnectionTracker paramGsmDataConnectionTracker, int paramInt1, int paramInt2)
        {
            if (paramInt1 == 1);
            for (String str = paramGsmDataConnectionTracker.callApnIdToType(paramInt2); ; str = "default")
                return str;
        }

        static boolean isDataAllowed(GsmDataConnectionTracker paramGsmDataConnectionTracker, ApnContext paramApnContext)
        {
            if (((!paramGsmDataConnectionTracker.getInternalDataEnabled()) || (!paramGsmDataConnectionTracker.getUserDataEnabled()) || (!GsmDataConnectionTracker.getPolicyDataEnabled())) && ((!"mms".equals(paramApnContext.getApnType())) || (!paramGsmDataConnectionTracker.callIsMmsDataEnabled())));
            for (boolean bool = false; ; bool = paramGsmDataConnectionTracker.callIsDataAllowed())
                return bool;
        }

        static void onDataSetupComplete(ApnContext paramApnContext)
        {
            DataConnectionAc localDataConnectionAc = paramApnContext.getDataConnectionAc();
            ApnSetting localApnSetting = paramApnContext.getApnSetting();
            FirewallManager.getInstance().onDataConnected(0, FirewallManager.encodeApnSetting(localApnSetting), localDataConnectionAc.getLinkPropertiesSync().getInterfaceName());
        }

        static void onDisconnectDone(ApnContext paramApnContext)
        {
            if (paramApnContext.getApnSetting() != null)
                FirewallManager.getInstance().onDataDisconnected(0, FirewallManager.encodeApnSetting(paramApnContext.getApnSetting()));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmDataConnectionTracker
 * JD-Core Version:        0.6.2
 */