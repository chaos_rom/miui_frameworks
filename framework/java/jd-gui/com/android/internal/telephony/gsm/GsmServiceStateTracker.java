package com.android.internal.telephony.gsm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RegistrantList;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Log;
import android.util.TimeUtils;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.CommandsInterface.RadioState;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.RestrictedState;
import com.android.internal.telephony.ServiceStateTracker;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

final class GsmServiceStateTracker extends ServiceStateTracker
{
    static final int CS_DISABLED = 1004;
    static final int CS_EMERGENCY_ENABLED = 1006;
    static final int CS_ENABLED = 1003;
    static final int CS_NORMAL_ENABLED = 1005;
    static final int CS_NOTIFICATION = 999;
    static final boolean DBG = true;
    static final int DEFAULT_GPRS_CHECK_PERIOD_MILLIS = 60000;
    static final String LOG_TAG = "GSM";
    static final int PS_DISABLED = 1002;
    static final int PS_ENABLED = 1001;
    static final int PS_NOTIFICATION = 888;
    private static final String WAKELOCK_TAG = "ServiceStateTracker";
    GsmCellLocation cellLoc;
    private ContentResolver cr;
    private String curPlmn = null;
    private String curSpn = null;
    private int curSpnRule = 0;
    private int gprsState = 1;
    private ContentObserver mAutoTimeObserver = new ContentObserver(new Handler())
    {
        public void onChange(boolean paramAnonymousBoolean)
        {
            Log.i("GsmServiceStateTracker", "Auto time state changed");
            GsmServiceStateTracker.this.revertToNitzTime();
        }
    };
    private ContentObserver mAutoTimeZoneObserver = new ContentObserver(new Handler())
    {
        public void onChange(boolean paramAnonymousBoolean)
        {
            Log.i("GsmServiceStateTracker", "Auto time zone state changed");
            GsmServiceStateTracker.this.revertToNitzTimeZone();
        }
    };
    private boolean mDataRoaming = false;
    private boolean mEmergencyOnly = false;
    private boolean mGotCountryCode = false;
    private boolean mGsmRoaming = false;
    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (paramAnonymousIntent.getAction().equals("android.intent.action.LOCALE_CHANGED"))
                GsmServiceStateTracker.this.updateSpnDisplay();
        }
    };
    private int mMaxDataCalls = 1;
    private boolean mNeedFixZoneAfterNitz = false;
    private boolean mNeedToRegForSimLoaded;
    private int mNewMaxDataCalls = 1;
    private int mNewReasonDataDenied = -1;
    private boolean mNitzUpdatedTime = false;
    private Notification mNotification;
    int mPreferredNetworkType;
    private int mReasonDataDenied = -1;
    private boolean mReportedGprsNoReg = false;
    long mSavedAtTime;
    long mSavedTime;
    String mSavedTimeZone;
    private boolean mStartedGprsRegCheck = false;
    private PowerManager.WakeLock mWakeLock;
    private boolean mZoneDst;
    private int mZoneOffset;
    private long mZoneTime;
    GsmCellLocation newCellLoc;
    private int newGPRSState = 1;
    GSMPhone phone;

    public GsmServiceStateTracker(GSMPhone paramGSMPhone)
    {
        this.phone = paramGSMPhone;
        this.cm = paramGSMPhone.mCM;
        this.ss = new ServiceState();
        this.newSS = new ServiceState();
        this.cellLoc = new GsmCellLocation();
        this.newCellLoc = new GsmCellLocation();
        this.mSignalStrength = new SignalStrength();
        this.mWakeLock = ((PowerManager)paramGSMPhone.getContext().getSystemService("power")).newWakeLock(1, "ServiceStateTracker");
        this.cm.registerForAvailable(this, 13, null);
        this.cm.registerForRadioStateChanged(this, 1, null);
        this.cm.registerForVoiceNetworkStateChanged(this, 2, null);
        this.cm.setOnNITZTime(this, 11, null);
        this.cm.setOnSignalStrengthUpdate(this, 12, null);
        this.cm.setOnRestrictedStateChanged(this, 23, null);
        paramGSMPhone.getIccCard().registerForReady(this, 17, null);
        if (Settings.System.getInt(paramGSMPhone.getContext().getContentResolver(), "airplane_mode_on", 0) <= 0)
            bool = true;
        this.mDesiredPowerState = bool;
        this.cr = paramGSMPhone.getContext().getContentResolver();
        this.cr.registerContentObserver(Settings.System.getUriFor("auto_time"), true, this.mAutoTimeObserver);
        this.cr.registerContentObserver(Settings.System.getUriFor("auto_time_zone"), true, this.mAutoTimeZoneObserver);
        setSignalStrengthDefaultValues();
        this.mNeedToRegForSimLoaded = true;
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.LOCALE_CHANGED");
        paramGSMPhone.getContext().registerReceiver(this.mIntentReceiver, localIntentFilter);
        paramGSMPhone.notifyOtaspChanged(3);
    }

    private static String displayNameFor(int paramInt)
    {
        int i = paramInt / 1000 / 60;
        char[] arrayOfChar = new char[9];
        arrayOfChar[0] = 'G';
        arrayOfChar[1] = 'M';
        arrayOfChar[2] = 'T';
        if (i < 0)
        {
            arrayOfChar[3] = '-';
            i = -i;
        }
        while (true)
        {
            int j = i / 60;
            int k = i % 60;
            arrayOfChar[4] = ((char)(48 + j / 10));
            arrayOfChar[5] = ((char)(48 + j % 10));
            arrayOfChar[6] = ':';
            arrayOfChar[7] = ((char)(48 + k / 10));
            arrayOfChar[8] = ((char)(48 + k % 10));
            return new String(arrayOfChar);
            arrayOfChar[3] = '+';
        }
    }

    private TimeZone findTimeZone(int paramInt, boolean paramBoolean, long paramLong)
    {
        int i = paramInt;
        if (paramBoolean)
            i += 4480;
        String[] arrayOfString = TimeZone.getAvailableIDs(i);
        Object localObject = null;
        Date localDate = new Date(paramLong);
        int j = arrayOfString.length;
        for (int k = 0; ; k++)
            if (k < j)
            {
                TimeZone localTimeZone = TimeZone.getTimeZone(arrayOfString[k]);
                if ((localTimeZone.getOffset(paramLong) == paramInt) && (localTimeZone.inDaylightTime(localDate) == paramBoolean))
                    localObject = localTimeZone;
            }
            else
            {
                return localObject;
            }
    }

    private boolean getAutoTime()
    {
        boolean bool = true;
        try
        {
            int i = Settings.System.getInt(this.phone.getContext().getContentResolver(), "auto_time");
            if (i > 0);
            while (true)
            {
                label22: return bool;
                bool = false;
            }
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label22;
        }
    }

    private boolean getAutoTimeZone()
    {
        boolean bool = true;
        try
        {
            int i = Settings.System.getInt(this.phone.getContext().getContentResolver(), "auto_time_zone");
            if (i > 0);
            while (true)
            {
                label23: return bool;
                bool = false;
            }
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label23;
        }
    }

    private TimeZone getNitzTimeZone(int paramInt, boolean paramBoolean, long paramLong)
    {
        TimeZone localTimeZone = findTimeZone(paramInt, paramBoolean, paramLong);
        boolean bool;
        StringBuilder localStringBuilder;
        if (localTimeZone == null)
        {
            if (!paramBoolean)
            {
                bool = true;
                localTimeZone = findTimeZone(paramInt, bool, paramLong);
            }
        }
        else
        {
            localStringBuilder = new StringBuilder().append("getNitzTimeZone returning ");
            if (localTimeZone != null)
                break label78;
        }
        label78: for (Object localObject = localTimeZone; ; localObject = localTimeZone.getID())
        {
            log(localObject);
            return localTimeZone;
            bool = false;
            break;
        }
    }

    private boolean isGprsConsistent(int paramInt1, int paramInt2)
    {
        if ((paramInt2 != 0) || (paramInt1 == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isRoamingBetweenOperators(boolean paramBoolean, ServiceState paramServiceState)
    {
        boolean bool1 = true;
        String str1 = SystemProperties.get("gsm.sim.operator.alpha", "empty");
        String str2 = paramServiceState.getOperatorAlphaLong();
        String str3 = paramServiceState.getOperatorAlphaShort();
        boolean bool2;
        if ((str2 != null) && (str1.equals(str2)))
            bool2 = bool1;
        while (true)
        {
            boolean bool3;
            label61: String str4;
            String str5;
            int i;
            if ((str3 != null) && (str1.equals(str3)))
            {
                bool3 = bool1;
                str4 = SystemProperties.get("gsm.sim.operator.numeric", "");
                str5 = paramServiceState.getOperatorNumeric();
                i = 1;
            }
            try
            {
                boolean bool4 = str4.substring(0, 3).equals(str5.substring(0, 3));
                i = bool4;
                label104: if ((paramBoolean) && ((i == 0) || ((!bool2) && (!bool3))));
                while (true)
                {
                    return bool1;
                    bool2 = false;
                    break;
                    bool3 = false;
                    break label61;
                    bool1 = false;
                }
            }
            catch (Exception localException)
            {
                break label104;
            }
        }
    }

    private void onRestrictedStateChanged(AsyncResult paramAsyncResult)
    {
        boolean bool1 = true;
        RestrictedState localRestrictedState = new RestrictedState();
        log("onRestrictedStateChanged: E rs " + this.mRestrictedState);
        boolean bool2;
        boolean bool3;
        if (paramAsyncResult.exception == null)
        {
            int i = ((int[])(int[])paramAsyncResult.result)[0];
            if (((i & 0x1) == 0) && ((i & 0x4) == 0))
                break label245;
            bool2 = bool1;
            localRestrictedState.setCsEmergencyRestricted(bool2);
            if (this.phone.getIccCard().getState() == IccCard.State.READY)
            {
                if (((i & 0x2) == 0) && ((i & 0x4) == 0))
                    break label251;
                bool3 = bool1;
                label114: localRestrictedState.setCsNormalRestricted(bool3);
                if ((i & 0x10) == 0)
                    break label257;
                label128: localRestrictedState.setPsRestricted(bool1);
            }
            log("onRestrictedStateChanged: new rs " + localRestrictedState);
            if ((this.mRestrictedState.isPsRestricted()) || (!localRestrictedState.isPsRestricted()))
                break label262;
            this.mPsRestrictEnabledRegistrants.notifyRegistrants();
            setNotification(1001);
            label188: if (!this.mRestrictedState.isCsRestricted())
                break label330;
            if (localRestrictedState.isCsRestricted())
                break label296;
            setNotification(1004);
        }
        while (true)
        {
            this.mRestrictedState = localRestrictedState;
            log("onRestrictedStateChanged: X rs " + this.mRestrictedState);
            return;
            label245: bool2 = false;
            break;
            label251: bool3 = false;
            break label114;
            label257: bool1 = false;
            break label128;
            label262: if ((!this.mRestrictedState.isPsRestricted()) || (localRestrictedState.isPsRestricted()))
                break label188;
            this.mPsRestrictDisabledRegistrants.notifyRegistrants();
            setNotification(1002);
            break label188;
            label296: if (!localRestrictedState.isCsNormalRestricted())
            {
                setNotification(1006);
            }
            else if (!localRestrictedState.isCsEmergencyRestricted())
            {
                setNotification(1005);
                continue;
                label330: if ((this.mRestrictedState.isCsEmergencyRestricted()) && (!this.mRestrictedState.isCsNormalRestricted()))
                {
                    if (!localRestrictedState.isCsRestricted())
                        setNotification(1004);
                    else if (localRestrictedState.isCsRestricted())
                        setNotification(1003);
                    else if (localRestrictedState.isCsNormalRestricted())
                        setNotification(1005);
                }
                else if ((!this.mRestrictedState.isCsEmergencyRestricted()) && (this.mRestrictedState.isCsNormalRestricted()))
                {
                    if (!localRestrictedState.isCsRestricted())
                        setNotification(1004);
                    else if (localRestrictedState.isCsRestricted())
                        setNotification(1003);
                    else if (localRestrictedState.isCsEmergencyRestricted())
                        setNotification(1006);
                }
                else if (localRestrictedState.isCsRestricted())
                    setNotification(1003);
                else if (localRestrictedState.isCsEmergencyRestricted())
                    setNotification(1006);
                else if (localRestrictedState.isCsNormalRestricted())
                    setNotification(1005);
            }
        }
    }

    private void onSignalStrengthResult(AsyncResult paramAsyncResult)
    {
        SignalStrength localSignalStrength = this.mSignalStrength;
        int i = 99;
        int j = -1;
        int k = -1;
        int m = -1;
        int n = 2147483647;
        int i1 = -1;
        if (paramAsyncResult.exception != null)
            setSignalStrengthDefaultValues();
        while (true)
        {
            this.mSignalStrength = new SignalStrength(i, -1, -1, -1, -1, -1, -1, j, k, m, n, i1, true);
            if (!this.mSignalStrength.equals(localSignalStrength));
            try
            {
                this.phone.notifySignalStrength();
                return;
                int[] arrayOfInt = (int[])paramAsyncResult.result;
                if (arrayOfInt.length != 0)
                {
                    i = arrayOfInt[0];
                    j = arrayOfInt[7];
                    k = arrayOfInt[8];
                    m = arrayOfInt[9];
                    n = arrayOfInt[10];
                    i1 = arrayOfInt[11];
                    continue;
                }
                loge("Bogus signal strength response");
                i = 99;
            }
            catch (NullPointerException localNullPointerException)
            {
                while (true)
                    log("onSignalStrengthResult() Phone already destroyed: " + localNullPointerException + "SignalStrength not notified");
            }
        }
    }

    private void pollState()
    {
        this.pollingContext = new int[1];
        this.pollingContext[0] = 0;
        switch (4.$SwitchMap$com$android$internal$telephony$CommandsInterface$RadioState[this.cm.getRadioState().ordinal()])
        {
        default:
            int[] arrayOfInt1 = this.pollingContext;
            arrayOfInt1[0] = (1 + arrayOfInt1[0]);
            this.cm.getOperator(obtainMessage(6, this.pollingContext));
            int[] arrayOfInt2 = this.pollingContext;
            arrayOfInt2[0] = (1 + arrayOfInt2[0]);
            this.cm.getDataRegistrationState(obtainMessage(5, this.pollingContext));
            int[] arrayOfInt3 = this.pollingContext;
            arrayOfInt3[0] = (1 + arrayOfInt3[0]);
            this.cm.getVoiceRegistrationState(obtainMessage(4, this.pollingContext));
            int[] arrayOfInt4 = this.pollingContext;
            arrayOfInt4[0] = (1 + arrayOfInt4[0]);
            this.cm.getNetworkSelectionMode(obtainMessage(14, this.pollingContext));
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            this.newSS.setStateOutOfService();
            this.newCellLoc.setStateInvalid();
            setSignalStrengthDefaultValues();
            this.mGotCountryCode = false;
            this.mNitzUpdatedTime = false;
            pollStateDone();
            continue;
            this.newSS.setStateOff();
            this.newCellLoc.setStateInvalid();
            setSignalStrengthDefaultValues();
            this.mGotCountryCode = false;
            this.mNitzUpdatedTime = false;
            pollStateDone();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void pollStateDone()
    {
        log("Poll ServiceState done:    oldSS=[" + this.ss + "] newSS=[" + this.newSS + "] oldGprs=" + this.gprsState + " newData=" + this.newGPRSState + " oldMaxDataCalls=" + this.mMaxDataCalls + " mNewMaxDataCalls=" + this.mNewMaxDataCalls + " oldReasonDataDenied=" + this.mReasonDataDenied + " mNewReasonDataDenied=" + this.mNewReasonDataDenied + " oldType=" + ServiceState.rilRadioTechnologyToString(this.mRilRadioTechnology) + " newType=" + ServiceState.rilRadioTechnologyToString(this.mNewRilRadioTechnology));
        int i;
        label192: int j;
        label208: int k;
        label224: int m;
        label238: int n;
        label255: int i1;
        label278: int i2;
        label301: int i3;
        label318: String str1;
        String str2;
        label805: String str5;
        if ((this.ss.getState() != 0) && (this.newSS.getState() == 0))
        {
            i = 1;
            if ((this.ss.getState() != 0) || (this.newSS.getState() == 0))
                break label997;
            if ((this.gprsState == 0) || (this.newGPRSState != 0))
                break label1000;
            j = 1;
            if ((this.gprsState != 0) || (this.newGPRSState == 0))
                break label1005;
            k = 1;
            if (this.mRilRadioTechnology == this.mNewRilRadioTechnology)
                break label1010;
            m = 1;
            if (this.newSS.equals(this.ss))
                break label1016;
            n = 1;
            if ((this.ss.getRoaming()) || (!this.newSS.getRoaming()))
                break label1022;
            i1 = 1;
            if ((!this.ss.getRoaming()) || (this.newSS.getRoaming()))
                break label1028;
            i2 = 1;
            if (this.newCellLoc.equals(this.cellLoc))
                break label1034;
            i3 = 1;
            if ((this.ss.getState() != this.newSS.getState()) || (this.gprsState != this.newGPRSState))
            {
                Object[] arrayOfObject1 = new Object[4];
                arrayOfObject1[0] = Integer.valueOf(this.ss.getState());
                arrayOfObject1[1] = Integer.valueOf(this.gprsState);
                arrayOfObject1[2] = Integer.valueOf(this.newSS.getState());
                arrayOfObject1[3] = Integer.valueOf(this.newGPRSState);
                EventLog.writeEvent(50114, arrayOfObject1);
            }
            ServiceState localServiceState = this.ss;
            this.ss = this.newSS;
            this.newSS = localServiceState;
            this.newSS.setStateOutOfService();
            GsmCellLocation localGsmCellLocation1 = this.cellLoc;
            this.cellLoc = this.newCellLoc;
            this.newCellLoc = localGsmCellLocation1;
            if (m != 0)
            {
                int i5 = -1;
                GsmCellLocation localGsmCellLocation2 = (GsmCellLocation)this.phone.getCellLocation();
                if (localGsmCellLocation2 != null)
                    i5 = localGsmCellLocation2.getCid();
                Object[] arrayOfObject2 = new Object[3];
                arrayOfObject2[0] = Integer.valueOf(i5);
                arrayOfObject2[1] = Integer.valueOf(this.mRilRadioTechnology);
                arrayOfObject2[2] = Integer.valueOf(this.mNewRilRadioTechnology);
                EventLog.writeEvent(50112, arrayOfObject2);
                log("RAT switched " + ServiceState.rilRadioTechnologyToString(this.mRilRadioTechnology) + " -> " + ServiceState.rilRadioTechnologyToString(this.mNewRilRadioTechnology) + " at cell " + i5);
            }
            this.gprsState = this.newGPRSState;
            this.mReasonDataDenied = this.mNewReasonDataDenied;
            this.mMaxDataCalls = this.mNewMaxDataCalls;
            this.mRilRadioTechnology = this.mNewRilRadioTechnology;
            this.mNewRilRadioTechnology = 0;
            this.newSS.setStateOutOfService();
            if (m != 0)
                this.phone.setSystemProperty("gsm.network.type", ServiceState.rilRadioTechnologyToString(this.mRilRadioTechnology));
            if (i != 0)
            {
                this.mNetworkAttachedRegistrants.notifyRegistrants();
                log("pollStateDone: registering current mNitzUpdatedTime=" + this.mNitzUpdatedTime + " changing to false");
                this.mNitzUpdatedTime = false;
            }
            if (n != 0)
            {
                updateSpnDisplay();
                this.phone.setSystemProperty("gsm.operator.alpha", this.ss.getOperatorAlphaLong());
                str1 = SystemProperties.get("gsm.operator.numeric", "");
                str2 = this.ss.getOperatorNumeric();
                this.phone.setSystemProperty("gsm.operator.numeric", str2);
                if (!TextUtils.isEmpty(str2))
                    break label1040;
                log("operatorNumeric is null");
                this.phone.setSystemProperty("gsm.operator.iso-country", "");
                this.mGotCountryCode = false;
                this.mNitzUpdatedTime = false;
                GSMPhone localGSMPhone = this.phone;
                if (!this.ss.getRoaming())
                    break label1780;
                str5 = "true";
                label826: localGSMPhone.setSystemProperty("gsm.operator.isroaming", str5);
                this.phone.notifyServiceStateChanged(this.ss);
            }
            if (j != 0)
                this.mAttachedRegistrants.notifyRegistrants();
            if (k != 0)
                this.mDetachedRegistrants.notifyRegistrants();
            if (m != 0)
                this.phone.notifyDataConnection("nwTypeChanged");
            if (i1 != 0)
                this.mRoamingOnRegistrants.notifyRegistrants();
            if (i2 != 0)
                this.mRoamingOffRegistrants.notifyRegistrants();
            if (i3 != 0)
                this.phone.notifyLocationChanged();
            if (isGprsConsistent(this.gprsState, this.ss.getState()))
                break label1788;
            if ((!this.mStartedGprsRegCheck) && (!this.mReportedGprsNoReg))
            {
                this.mStartedGprsRegCheck = true;
                int i4 = Settings.Secure.getInt(this.phone.getContext().getContentResolver(), "gprs_register_check_period_ms", 60000);
                sendMessageDelayed(obtainMessage(22), i4);
            }
        }
        while (true)
        {
            while (true)
            {
                return;
                i = 0;
                break;
                label997: break label192;
                label1000: j = 0;
                break label208;
                label1005: k = 0;
                break label224;
                label1010: m = 0;
                break label238;
                label1016: n = 0;
                break label255;
                label1022: i1 = 0;
                break label278;
                label1028: i2 = 0;
                break label301;
                label1034: i3 = 0;
                break label318;
                label1040: Object localObject = "";
                String str3 = str2.substring(0, 3);
                try
                {
                    String str6 = MccTable.countryCodeForMcc(Integer.parseInt(str3));
                    localObject = str6;
                    this.phone.setSystemProperty("gsm.operator.iso-country", (String)localObject);
                    this.mGotCountryCode = true;
                    if ((!this.mNitzUpdatedTime) && (!str3.equals("000")) && (!TextUtils.isEmpty((CharSequence)localObject)) && (getAutoTimeZone()))
                    {
                        if ((SystemProperties.getBoolean("telephony.test.ignore.nitz", false)) && ((1L & SystemClock.uptimeMillis()) == 0L))
                        {
                            bool = true;
                            localArrayList = TimeUtils.getTimeZonesWithUniqueOffsets((String)localObject);
                            if ((localArrayList.size() != 1) && (!bool))
                                break label1629;
                            TimeZone localTimeZone2 = (TimeZone)localArrayList.get(0);
                            log("pollStateDone: no nitz but one TZ for iso-cc=" + (String)localObject + " with zone.getID=" + localTimeZone2.getID() + " testOneUniqueOffsetPath=" + bool);
                            setAndBroadcastNetworkSetTimeZone(localTimeZone2.getID());
                        }
                    }
                    else
                    {
                        if (!shouldFixTimeZoneNow(this.phone, str2, str1, this.mNeedFixZoneAfterNitz))
                            break label805;
                        String str4 = SystemProperties.get("persist.sys.timezone");
                        log("pollStateDone: fix time zone zoneName='" + str4 + "' mZoneOffset=" + this.mZoneOffset + " mZoneDst=" + this.mZoneDst + " iso-cc='" + (String)localObject + "' iso-cc-idx=" + Arrays.binarySearch(GMT_COUNTRY_CODES, localObject));
                        if ((this.mZoneOffset != 0) || (this.mZoneDst) || (str4 == null) || (str4.length() <= 0) || (Arrays.binarySearch(GMT_COUNTRY_CODES, localObject) >= 0))
                            break label1702;
                        localTimeZone1 = TimeZone.getDefault();
                        if (this.mNeedFixZoneAfterNitz)
                        {
                            long l1 = System.currentTimeMillis();
                            l2 = localTimeZone1.getOffset(l1);
                            log("pollStateDone: tzOffset=" + l2 + " ltod=" + TimeUtils.logTimeOfDay(l1));
                            if (!getAutoTime())
                                break label1688;
                            long l3 = l1 - l2;
                            log("pollStateDone: adj ltod=" + TimeUtils.logTimeOfDay(l3));
                            setAndBroadcastNetworkSetTime(l3);
                        }
                        log("pollStateDone: using default TimeZone");
                        this.mNeedFixZoneAfterNitz = false;
                        if (localTimeZone1 == null)
                            break label1770;
                        log("pollStateDone: zone != null zone.getID=" + localTimeZone1.getID());
                        if (getAutoTimeZone())
                            setAndBroadcastNetworkSetTimeZone(localTimeZone1.getID());
                        saveNitzTimeZone(localTimeZone1.getID());
                    }
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    while (true)
                        loge("pollStateDone: countryCodeForMcc error" + localNumberFormatException);
                }
                catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException)
                {
                    while (true)
                    {
                        ArrayList localArrayList;
                        TimeZone localTimeZone1;
                        long l2;
                        loge("pollStateDone: countryCodeForMcc error" + localStringIndexOutOfBoundsException);
                        continue;
                        boolean bool = false;
                        continue;
                        label1629: log("pollStateDone: there are " + localArrayList.size() + " unique offsets for iso-cc='" + (String)localObject + " testOneUniqueOffsetPath=" + bool + "', do nothing");
                        continue;
                        label1688: this.mSavedTime -= l2;
                        continue;
                        label1702: if (((String)localObject).equals(""))
                        {
                            localTimeZone1 = getNitzTimeZone(this.mZoneOffset, this.mZoneDst, this.mZoneTime);
                            log("pollStateDone: using NITZ TimeZone");
                        }
                        else
                        {
                            localTimeZone1 = TimeUtils.getTimeZone(this.mZoneOffset, this.mZoneDst, this.mZoneTime, (String)localObject);
                            log("pollStateDone: using getTimeZone(off, dst, time, iso)");
                        }
                    }
                    label1770: log("pollStateDone: zone == null");
                }
            }
            break label805;
            label1780: str5 = "false";
            break label826;
            label1788: this.mReportedGprsNoReg = false;
        }
    }

    private void queueNextSignalStrengthPoll()
    {
        if (this.dontPollSignalStrength);
        while (true)
        {
            return;
            Message localMessage = obtainMessage();
            localMessage.what = 10;
            sendMessageDelayed(localMessage, 20000L);
        }
    }

    private boolean regCodeIsRoaming(int paramInt)
    {
        if (5 == paramInt);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private int regCodeToServiceState(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        case 6:
        case 7:
        case 8:
        case 9:
        case 11:
        default:
            loge("regCodeToServiceState: unexpected service state " + paramInt);
        case 0:
        case 2:
        case 3:
        case 4:
        case 10:
        case 12:
        case 13:
        case 14:
        case 1:
        case 5:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 0;
        }
    }

    private void revertToNitzTime()
    {
        if (Settings.System.getInt(this.phone.getContext().getContentResolver(), "auto_time", 0) == 0);
        while (true)
        {
            return;
            log("Reverting to NITZ Time: mSavedTime=" + this.mSavedTime + " mSavedAtTime=" + this.mSavedAtTime);
            if ((this.mSavedTime != 0L) && (this.mSavedAtTime != 0L))
                setAndBroadcastNetworkSetTime(this.mSavedTime + (SystemClock.elapsedRealtime() - this.mSavedAtTime));
        }
    }

    private void revertToNitzTimeZone()
    {
        if (Settings.System.getInt(this.phone.getContext().getContentResolver(), "auto_time_zone", 0) == 0);
        while (true)
        {
            return;
            log("Reverting to NITZ TimeZone: tz='" + this.mSavedTimeZone);
            if (this.mSavedTimeZone != null)
                setAndBroadcastNetworkSetTimeZone(this.mSavedTimeZone);
        }
    }

    private void saveNitzTime(long paramLong)
    {
        this.mSavedTime = paramLong;
        this.mSavedAtTime = SystemClock.elapsedRealtime();
    }

    private void saveNitzTimeZone(String paramString)
    {
        this.mSavedTimeZone = paramString;
    }

    private void setAndBroadcastNetworkSetTime(long paramLong)
    {
        log("setAndBroadcastNetworkSetTime: time=" + paramLong + "ms");
        SystemClock.setCurrentTimeMillis(paramLong);
        Intent localIntent = new Intent("android.intent.action.NETWORK_SET_TIME");
        localIntent.addFlags(536870912);
        localIntent.putExtra("time", paramLong);
        this.phone.getContext().sendStickyBroadcast(localIntent);
    }

    private void setAndBroadcastNetworkSetTimeZone(String paramString)
    {
        log("setAndBroadcastNetworkSetTimeZone: setTimeZone=" + paramString);
        ((AlarmManager)this.phone.getContext().getSystemService("alarm")).setTimeZone(paramString);
        Intent localIntent = new Intent("android.intent.action.NETWORK_SET_TIMEZONE");
        localIntent.addFlags(536870912);
        localIntent.putExtra("time-zone", paramString);
        this.phone.getContext().sendStickyBroadcast(localIntent);
        log("setAndBroadcastNetworkSetTimeZone: call alarm.setTimeZone and broadcast zoneId=" + paramString);
    }

    private void setNotification(int paramInt)
    {
        log("setNotification: create notification " + paramInt);
        Context localContext = this.phone.getContext();
        this.mNotification = new Notification();
        this.mNotification.when = System.currentTimeMillis();
        this.mNotification.flags = 16;
        this.mNotification.icon = 17301642;
        Intent localIntent = new Intent();
        this.mNotification.contentIntent = PendingIntent.getActivity(localContext, 0, localIntent, 268435456);
        Object localObject = "";
        CharSequence localCharSequence = localContext.getText(17039595);
        int i = 999;
        NotificationManager localNotificationManager;
        switch (paramInt)
        {
        case 1004:
        default:
            log("setNotification: put notification " + localCharSequence + " / " + localObject);
            this.mNotification.tickerText = localCharSequence;
            this.mNotification.setLatestEventInfo(localContext, localCharSequence, (CharSequence)localObject, this.mNotification.contentIntent);
            localNotificationManager = (NotificationManager)localContext.getSystemService("notification");
            if ((paramInt == 1002) || (paramInt == 1004))
                localNotificationManager.cancel(i);
            break;
        case 1001:
        case 1002:
        case 1003:
        case 1005:
        case 1006:
        }
        while (true)
        {
            return;
            i = 888;
            localObject = localContext.getText(17039596);
            break;
            i = 888;
            break;
            localObject = localContext.getText(17039599);
            break;
            localObject = localContext.getText(17039598);
            break;
            localObject = localContext.getText(17039597);
            break;
            localNotificationManager.notify(i, this.mNotification);
        }
    }

    private void setSignalStrengthDefaultValues()
    {
        this.mSignalStrength = new SignalStrength(99, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2147483647, -1, true);
    }

    // ERROR //
    private void setTimeFromNITZString(String paramString, long paramLong)
    {
        // Byte code:
        //     0: invokestatic 854	android/os/SystemClock:elapsedRealtime	()J
        //     3: lstore 4
        //     5: aload_0
        //     6: new 334	java/lang/StringBuilder
        //     9: dup
        //     10: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     13: ldc_w 973
        //     16: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     19: aload_1
        //     20: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     23: ldc_w 975
        //     26: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     29: lload_2
        //     30: invokevirtual 780	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     33: ldc_w 977
        //     36: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: lload 4
        //     41: invokevirtual 780	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     44: ldc_w 979
        //     47: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: lload 4
        //     52: lload_2
        //     53: lsub
        //     54: invokevirtual 780	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     57: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokevirtual 351	com/android/internal/telephony/gsm/GsmServiceStateTracker:log	(Ljava/lang/String;)V
        //     63: ldc_w 981
        //     66: invokestatic 313	java/util/TimeZone:getTimeZone	(Ljava/lang/String;)Ljava/util/TimeZone;
        //     69: invokestatic 987	java/util/Calendar:getInstance	(Ljava/util/TimeZone;)Ljava/util/Calendar;
        //     72: astore 7
        //     74: aload 7
        //     76: invokevirtual 990	java/util/Calendar:clear	()V
        //     79: aload 7
        //     81: bipush 16
        //     83: iconst_0
        //     84: invokevirtual 994	java/util/Calendar:set	(II)V
        //     87: aload_1
        //     88: ldc_w 996
        //     91: invokevirtual 1000	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     94: astore 8
        //     96: aload 7
        //     98: iconst_1
        //     99: sipush 2000
        //     102: aload 8
        //     104: iconst_0
        //     105: aaload
        //     106: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     109: iadd
        //     110: invokevirtual 994	java/util/Calendar:set	(II)V
        //     113: aload 7
        //     115: iconst_2
        //     116: bipush 255
        //     118: aload 8
        //     120: iconst_1
        //     121: aaload
        //     122: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     125: iadd
        //     126: invokevirtual 994	java/util/Calendar:set	(II)V
        //     129: aload 7
        //     131: iconst_5
        //     132: aload 8
        //     134: iconst_2
        //     135: aaload
        //     136: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     139: invokevirtual 994	java/util/Calendar:set	(II)V
        //     142: aload 7
        //     144: bipush 10
        //     146: aload 8
        //     148: iconst_3
        //     149: aaload
        //     150: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     153: invokevirtual 994	java/util/Calendar:set	(II)V
        //     156: aload 7
        //     158: bipush 12
        //     160: aload 8
        //     162: iconst_4
        //     163: aaload
        //     164: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     167: invokevirtual 994	java/util/Calendar:set	(II)V
        //     170: aload 7
        //     172: bipush 13
        //     174: aload 8
        //     176: iconst_5
        //     177: aaload
        //     178: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     181: invokevirtual 994	java/util/Calendar:set	(II)V
        //     184: aload_1
        //     185: bipush 45
        //     187: invokevirtual 1003	java/lang/String:indexOf	(I)I
        //     190: bipush 255
        //     192: if_icmpne +643 -> 835
        //     195: iconst_1
        //     196: istore 9
        //     198: aload 8
        //     200: bipush 6
        //     202: aaload
        //     203: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     206: istore 10
        //     208: aload 8
        //     210: arraylength
        //     211: bipush 8
        //     213: if_icmplt +628 -> 841
        //     216: aload 8
        //     218: bipush 7
        //     220: aaload
        //     221: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     224: istore 11
        //     226: goto +587 -> 813
        //     229: sipush 1000
        //     232: bipush 60
        //     234: bipush 15
        //     236: iload 12
        //     238: iload 10
        //     240: imul
        //     241: imul
        //     242: imul
        //     243: imul
        //     244: istore 13
        //     246: aconst_null
        //     247: astore 14
        //     249: aload 8
        //     251: arraylength
        //     252: bipush 9
        //     254: if_icmplt +20 -> 274
        //     257: aload 8
        //     259: bipush 8
        //     261: aaload
        //     262: bipush 33
        //     264: bipush 47
        //     266: invokevirtual 1007	java/lang/String:replace	(CC)Ljava/lang/String;
        //     269: invokestatic 313	java/util/TimeZone:getTimeZone	(Ljava/lang/String;)Ljava/util/TimeZone;
        //     272: astore 14
        //     274: ldc_w 640
        //     277: invokestatic 739	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     280: astore 15
        //     282: aload 14
        //     284: ifnonnull +51 -> 335
        //     287: aload_0
        //     288: getfield 113	com/android/internal/telephony/gsm/GsmServiceStateTracker:mGotCountryCode	Z
        //     291: ifeq +44 -> 335
        //     294: aload 15
        //     296: ifnull +564 -> 860
        //     299: aload 15
        //     301: invokevirtual 766	java/lang/String:length	()I
        //     304: ifle +556 -> 860
        //     307: iload 11
        //     309: ifeq +545 -> 854
        //     312: iconst_1
        //     313: istore 29
        //     315: aload 7
        //     317: invokevirtual 1010	java/util/Calendar:getTimeInMillis	()J
        //     320: lstore 30
        //     322: iload 13
        //     324: iload 29
        //     326: lload 30
        //     328: aload 15
        //     330: invokestatic 819	android/util/TimeUtils:getTimeZone	(IZJLjava/lang/String;)Ljava/util/TimeZone;
        //     333: astore 14
        //     335: aload 14
        //     337: ifnull +29 -> 366
        //     340: aload_0
        //     341: getfield 745	com/android/internal/telephony/gsm/GsmServiceStateTracker:mZoneOffset	I
        //     344: iload 13
        //     346: if_icmpne +20 -> 366
        //     349: aload_0
        //     350: getfield 749	com/android/internal/telephony/gsm/GsmServiceStateTracker:mZoneDst	Z
        //     353: istore 23
        //     355: iload 11
        //     357: ifeq +139 -> 496
        //     360: iconst_1
        //     361: istore 24
        //     363: goto +461 -> 824
        //     366: aload_0
        //     367: iconst_1
        //     368: putfield 111	com/android/internal/telephony/gsm/GsmServiceStateTracker:mNeedFixZoneAfterNitz	Z
        //     371: aload_0
        //     372: iload 13
        //     374: putfield 745	com/android/internal/telephony/gsm/GsmServiceStateTracker:mZoneOffset	I
        //     377: iload 11
        //     379: ifeq +123 -> 502
        //     382: iconst_1
        //     383: istore 16
        //     385: aload_0
        //     386: iload 16
        //     388: putfield 749	com/android/internal/telephony/gsm/GsmServiceStateTracker:mZoneDst	Z
        //     391: aload_0
        //     392: aload 7
        //     394: invokevirtual 1010	java/util/Calendar:getTimeInMillis	()J
        //     397: putfield 812	com/android/internal/telephony/gsm/GsmServiceStateTracker:mZoneTime	J
        //     400: aload 14
        //     402: ifnull +28 -> 430
        //     405: aload_0
        //     406: invokespecial 695	com/android/internal/telephony/gsm/GsmServiceStateTracker:getAutoTimeZone	()Z
        //     409: ifeq +12 -> 421
        //     412: aload_0
        //     413: aload 14
        //     415: invokevirtual 354	java/util/TimeZone:getID	()Ljava/lang/String;
        //     418: invokespecial 730	com/android/internal/telephony/gsm/GsmServiceStateTracker:setAndBroadcastNetworkSetTimeZone	(Ljava/lang/String;)V
        //     421: aload_0
        //     422: aload 14
        //     424: invokevirtual 354	java/util/TimeZone:getID	()Ljava/lang/String;
        //     427: invokespecial 800	com/android/internal/telephony/gsm/GsmServiceStateTracker:saveNitzTimeZone	(Ljava/lang/String;)V
        //     430: ldc_w 1012
        //     433: invokestatic 739	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     436: astore 17
        //     438: aload 17
        //     440: ifnull +68 -> 508
        //     443: aload 17
        //     445: ldc_w 1014
        //     448: invokevirtual 380	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     451: ifeq +57 -> 508
        //     454: aload_0
        //     455: ldc_w 1016
        //     458: invokevirtual 351	com/android/internal/telephony/gsm/GsmServiceStateTracker:log	(Ljava/lang/String;)V
        //     461: goto +373 -> 834
        //     464: aload 7
        //     466: invokevirtual 1010	java/util/Calendar:getTimeInMillis	()J
        //     469: lstore 26
        //     471: aload_0
        //     472: iload 13
        //     474: iload 25
        //     476: lload 26
        //     478: invokespecial 814	com/android/internal/telephony/gsm/GsmServiceStateTracker:getNitzTimeZone	(IZJ)Ljava/util/TimeZone;
        //     481: astore 28
        //     483: aload 28
        //     485: astore 14
        //     487: goto -152 -> 335
        //     490: iconst_0
        //     491: istore 25
        //     493: goto -29 -> 464
        //     496: iconst_0
        //     497: istore 24
        //     499: goto +325 -> 824
        //     502: iconst_0
        //     503: istore 16
        //     505: goto -120 -> 385
        //     508: aload_0
        //     509: getfield 198	com/android/internal/telephony/gsm/GsmServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     512: invokevirtual 1021	android/os/PowerManager$WakeLock:acquire	()V
        //     515: aload_0
        //     516: invokespecial 788	com/android/internal/telephony/gsm/GsmServiceStateTracker:getAutoTime	()Z
        //     519: ifeq +244 -> 763
        //     522: invokestatic 854	android/os/SystemClock:elapsedRealtime	()J
        //     525: lload_2
        //     526: lsub
        //     527: lstore 19
        //     529: lload 19
        //     531: lconst_0
        //     532: lcmp
        //     533: ifge +77 -> 610
        //     536: aload_0
        //     537: new 334	java/lang/StringBuilder
        //     540: dup
        //     541: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     544: ldc_w 1023
        //     547: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     550: aload_1
        //     551: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     554: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     557: invokevirtual 351	com/android/internal/telephony/gsm/GsmServiceStateTracker:log	(Ljava/lang/String;)V
        //     560: aload_0
        //     561: getfield 198	com/android/internal/telephony/gsm/GsmServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     564: invokevirtual 1026	android/os/PowerManager$WakeLock:release	()V
        //     567: goto +267 -> 834
        //     570: astore 6
        //     572: aload_0
        //     573: new 334	java/lang/StringBuilder
        //     576: dup
        //     577: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     580: ldc_w 1028
        //     583: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     586: aload_1
        //     587: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     590: ldc_w 1030
        //     593: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     596: aload 6
        //     598: invokevirtual 344	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     601: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     604: invokevirtual 481	com/android/internal/telephony/gsm/GsmServiceStateTracker:loge	(Ljava/lang/String;)V
        //     607: goto +227 -> 834
        //     610: lload 19
        //     612: ldc2_w 1031
        //     615: lcmp
        //     616: ifle +48 -> 664
        //     619: aload_0
        //     620: new 334	java/lang/StringBuilder
        //     623: dup
        //     624: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     627: ldc_w 1034
        //     630: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     633: lload 19
        //     635: ldc2_w 1035
        //     638: ldiv
        //     639: invokevirtual 780	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     642: ldc_w 1038
        //     645: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     648: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     651: invokevirtual 351	com/android/internal/telephony/gsm/GsmServiceStateTracker:log	(Ljava/lang/String;)V
        //     654: aload_0
        //     655: getfield 198	com/android/internal/telephony/gsm/GsmServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     658: invokevirtual 1026	android/os/PowerManager$WakeLock:release	()V
        //     661: goto +173 -> 834
        //     664: lload 19
        //     666: l2i
        //     667: istore 21
        //     669: aload 7
        //     671: bipush 14
        //     673: iload 21
        //     675: invokevirtual 1041	java/util/Calendar:add	(II)V
        //     678: aload_0
        //     679: new 334	java/lang/StringBuilder
        //     682: dup
        //     683: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     686: ldc_w 1043
        //     689: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     692: aload 7
        //     694: invokevirtual 1047	java/util/Calendar:getTime	()Ljava/util/Date;
        //     697: invokevirtual 344	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     700: ldc_w 1049
        //     703: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     706: lload 19
        //     708: invokevirtual 780	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     711: ldc_w 1051
        //     714: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     717: aload 7
        //     719: invokevirtual 1010	java/util/Calendar:getTimeInMillis	()J
        //     722: invokestatic 775	java/lang/System:currentTimeMillis	()J
        //     725: lsub
        //     726: invokevirtual 780	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     729: ldc_w 1053
        //     732: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     735: aload_1
        //     736: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     739: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     742: invokevirtual 351	com/android/internal/telephony/gsm/GsmServiceStateTracker:log	(Ljava/lang/String;)V
        //     745: aload_0
        //     746: aload 7
        //     748: invokevirtual 1010	java/util/Calendar:getTimeInMillis	()J
        //     751: invokespecial 793	com/android/internal/telephony/gsm/GsmServiceStateTracker:setAndBroadcastNetworkSetTime	(J)V
        //     754: ldc 35
        //     756: ldc_w 1055
        //     759: invokestatic 1061	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     762: pop
        //     763: ldc_w 1063
        //     766: aload 7
        //     768: invokevirtual 1010	java/util/Calendar:getTimeInMillis	()J
        //     771: invokestatic 1065	java/lang/String:valueOf	(J)Ljava/lang/String;
        //     774: invokestatic 1067	android/os/SystemProperties:set	(Ljava/lang/String;Ljava/lang/String;)V
        //     777: aload_0
        //     778: aload 7
        //     780: invokevirtual 1010	java/util/Calendar:getTimeInMillis	()J
        //     783: invokespecial 1069	com/android/internal/telephony/gsm/GsmServiceStateTracker:saveNitzTime	(J)V
        //     786: aload_0
        //     787: iconst_1
        //     788: putfield 115	com/android/internal/telephony/gsm/GsmServiceStateTracker:mNitzUpdatedTime	Z
        //     791: aload_0
        //     792: getfield 198	com/android/internal/telephony/gsm/GsmServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     795: invokevirtual 1026	android/os/PowerManager$WakeLock:release	()V
        //     798: goto +36 -> 834
        //     801: astore 18
        //     803: aload_0
        //     804: getfield 198	com/android/internal/telephony/gsm/GsmServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     807: invokevirtual 1026	android/os/PowerManager$WakeLock:release	()V
        //     810: aload 18
        //     812: athrow
        //     813: iload 9
        //     815: ifeq +32 -> 847
        //     818: iconst_1
        //     819: istore 12
        //     821: goto -592 -> 229
        //     824: iload 23
        //     826: iload 24
        //     828: if_icmpeq -428 -> 400
        //     831: goto -465 -> 366
        //     834: return
        //     835: iconst_0
        //     836: istore 9
        //     838: goto -640 -> 198
        //     841: iconst_0
        //     842: istore 11
        //     844: goto -31 -> 813
        //     847: bipush 255
        //     849: istore 12
        //     851: goto -622 -> 229
        //     854: iconst_0
        //     855: istore 29
        //     857: goto -542 -> 315
        //     860: iload 11
        //     862: ifeq -372 -> 490
        //     865: iconst_1
        //     866: istore 25
        //     868: goto -404 -> 464
        //
        // Exception table:
        //     from	to	target	type
        //     63	483	570	java/lang/RuntimeException
        //     560	567	570	java/lang/RuntimeException
        //     654	661	570	java/lang/RuntimeException
        //     791	813	570	java/lang/RuntimeException
        //     508	560	801	finally
        //     619	654	801	finally
        //     669	791	801	finally
    }

    private static void sloge(String paramString)
    {
        Log.e("GSM", "[GsmSST] " + paramString);
    }

    private static int twoDigitsAt(String paramString, int paramInt)
    {
        int i = Character.digit(paramString.charAt(paramInt), 10);
        int j = Character.digit(paramString.charAt(paramInt + 1), 10);
        if ((i < 0) || (j < 0))
            throw new RuntimeException("invalid format");
        return j + i * 10;
    }

    public void dispose()
    {
        this.cm.unregisterForAvailable(this);
        this.cm.unregisterForRadioStateChanged(this);
        this.cm.unregisterForVoiceNetworkStateChanged(this);
        this.phone.getIccCard().unregisterForReady(this);
        this.phone.mIccRecords.unregisterForRecordsLoaded(this);
        this.cm.unSetOnSignalStrengthUpdate(this);
        this.cm.unSetOnRestrictedStateChanged(this);
        this.cm.unSetOnNITZTime(this);
        this.cr.unregisterContentObserver(this.mAutoTimeObserver);
        this.cr.unregisterContentObserver(this.mAutoTimeZoneObserver);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("GsmServiceStateTracker extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" phone=" + this.phone);
        paramPrintWriter.println(" cellLoc=" + this.cellLoc);
        paramPrintWriter.println(" newCellLoc=" + this.newCellLoc);
        paramPrintWriter.println(" mPreferredNetworkType=" + this.mPreferredNetworkType);
        paramPrintWriter.println(" gprsState=" + this.gprsState);
        paramPrintWriter.println(" newGPRSState=" + this.newGPRSState);
        paramPrintWriter.println(" mMaxDataCalls=" + this.mMaxDataCalls);
        paramPrintWriter.println(" mNewMaxDataCalls=" + this.mNewMaxDataCalls);
        paramPrintWriter.println(" mReasonDataDenied=" + this.mReasonDataDenied);
        paramPrintWriter.println(" mNewReasonDataDenied=" + this.mNewReasonDataDenied);
        paramPrintWriter.println(" mGsmRoaming=" + this.mGsmRoaming);
        paramPrintWriter.println(" mDataRoaming=" + this.mDataRoaming);
        paramPrintWriter.println(" mEmergencyOnly=" + this.mEmergencyOnly);
        paramPrintWriter.println(" mNeedFixZoneAfterNitz=" + this.mNeedFixZoneAfterNitz);
        paramPrintWriter.println(" mZoneOffset=" + this.mZoneOffset);
        paramPrintWriter.println(" mZoneDst=" + this.mZoneDst);
        paramPrintWriter.println(" mZoneTime=" + this.mZoneTime);
        paramPrintWriter.println(" mGotCountryCode=" + this.mGotCountryCode);
        paramPrintWriter.println(" mNitzUpdatedTime=" + this.mNitzUpdatedTime);
        paramPrintWriter.println(" mSavedTimeZone=" + this.mSavedTimeZone);
        paramPrintWriter.println(" mSavedTime=" + this.mSavedTime);
        paramPrintWriter.println(" mSavedAtTime=" + this.mSavedAtTime);
        paramPrintWriter.println(" mNeedToRegForSimLoaded=" + this.mNeedToRegForSimLoaded);
        paramPrintWriter.println(" mStartedGprsRegCheck=" + this.mStartedGprsRegCheck);
        paramPrintWriter.println(" mReportedGprsNoReg=" + this.mReportedGprsNoReg);
        paramPrintWriter.println(" mNotification=" + this.mNotification);
        paramPrintWriter.println(" mWakeLock=" + this.mWakeLock);
        paramPrintWriter.println(" curSpn=" + this.curSpn);
        paramPrintWriter.println(" curPlmn=" + this.curPlmn);
        paramPrintWriter.println(" curSpnRule=" + this.curSpnRule);
    }

    protected void finalize()
    {
        log("finalize");
    }

    public int getCurrentDataConnectionState()
    {
        return this.gprsState;
    }

    int getCurrentGprsState()
    {
        return this.gprsState;
    }

    protected Phone getPhone()
    {
        return this.phone;
    }

    public void handleMessage(Message paramMessage)
    {
        if (!this.phone.mIsTheCurrentActivePhone)
            Log.e("GSM", "Received message " + paramMessage + "[" + paramMessage.what + "] while being destroyed. Ignoring.");
        while (true)
        {
            return;
            switch (paramMessage.what)
            {
            case 13:
            case 7:
            case 8:
            case 9:
            default:
                super.handleMessage(paramMessage);
                break;
            case 17:
                this.cm.setCurrentPreferredNetworkType();
                if (this.mNeedToRegForSimLoaded)
                {
                    this.phone.mIccRecords.registerForRecordsLoaded(this, 16, null);
                    this.mNeedToRegForSimLoaded = false;
                }
                if (!this.phone.getContext().getResources().getBoolean(17891373))
                    this.phone.restoreSavedNetworkSelection(null);
                pollState();
                queueNextSignalStrengthPoll();
                break;
            case 1:
                setPowerStateToDesired();
                pollState();
                break;
            case 2:
                pollState();
                break;
            case 3:
                if (this.cm.getRadioState().isOn())
                {
                    onSignalStrengthResult((AsyncResult)paramMessage.obj);
                    queueNextSignalStrengthPoll();
                }
                break;
            case 15:
                AsyncResult localAsyncResult6 = (AsyncResult)paramMessage.obj;
                String[] arrayOfString;
                int j;
                int k;
                if (localAsyncResult6.exception == null)
                {
                    arrayOfString = (String[])localAsyncResult6.result;
                    j = -1;
                    k = -1;
                    if (arrayOfString.length < 3);
                }
                try
                {
                    if ((arrayOfString[1] != null) && (arrayOfString[1].length() > 0))
                        j = Integer.parseInt(arrayOfString[1], 16);
                    if ((arrayOfString[2] != null) && (arrayOfString[2].length() > 0))
                    {
                        int m = Integer.parseInt(arrayOfString[2], 16);
                        k = m;
                    }
                    this.cellLoc.setLacAndCid(j, k);
                    this.phone.notifyLocationChanged();
                    disableSingleLocationUpdate();
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    while (true)
                        Log.w("GSM", "error parsing location: " + localNumberFormatException);
                }
            case 4:
            case 5:
            case 6:
            case 14:
                AsyncResult localAsyncResult5 = (AsyncResult)paramMessage.obj;
                handlePollStateResult(paramMessage.what, localAsyncResult5);
                break;
            case 10:
                this.cm.getSignalStrength(obtainMessage(3));
                break;
            case 11:
                AsyncResult localAsyncResult4 = (AsyncResult)paramMessage.obj;
                setTimeFromNITZString((String)((Object[])(Object[])localAsyncResult4.result)[0], ((Long)((Object[])(Object[])localAsyncResult4.result)[1]).longValue());
                break;
            case 12:
                AsyncResult localAsyncResult3 = (AsyncResult)paramMessage.obj;
                this.dontPollSignalStrength = true;
                onSignalStrengthResult(localAsyncResult3);
                break;
            case 16:
                updateSpnDisplay();
                break;
            case 18:
                if (((AsyncResult)paramMessage.obj).exception == null)
                    this.cm.getVoiceRegistrationState(obtainMessage(15, null));
                break;
            case 20:
                Message localMessage2 = obtainMessage(21, ((AsyncResult)paramMessage.obj).userObj);
                this.cm.setPreferredNetworkType(this.mPreferredNetworkType, localMessage2);
                break;
            case 21:
                AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
                if (localAsyncResult2.userObj != null)
                {
                    AsyncResult.forMessage((Message)localAsyncResult2.userObj).exception = localAsyncResult2.exception;
                    ((Message)localAsyncResult2.userObj).sendToTarget();
                }
                break;
            case 19:
                AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
                if (localAsyncResult1.exception == null);
                for (this.mPreferredNetworkType = ((int[])(int[])localAsyncResult1.result)[0]; ; this.mPreferredNetworkType = 7)
                {
                    Message localMessage1 = obtainMessage(20, localAsyncResult1.userObj);
                    this.cm.setPreferredNetworkType(7, localMessage1);
                    break;
                }
            case 22:
                GsmCellLocation localGsmCellLocation;
                Object[] arrayOfObject;
                if ((this.ss != null) && (!isGprsConsistent(this.gprsState, this.ss.getState())))
                {
                    localGsmCellLocation = (GsmCellLocation)this.phone.getCellLocation();
                    arrayOfObject = new Object[2];
                    arrayOfObject[0] = this.ss.getOperatorNumeric();
                    if (localGsmCellLocation == null)
                        break label862;
                }
                for (int i = localGsmCellLocation.getCid(); ; i = -1)
                {
                    arrayOfObject[1] = Integer.valueOf(i);
                    EventLog.writeEvent(50107, arrayOfObject);
                    this.mReportedGprsNoReg = true;
                    this.mStartedGprsRegCheck = false;
                    break;
                }
            case 23:
                label862: log("EVENT_RESTRICTED_STATE_CHANGED");
                onRestrictedStateChanged((AsyncResult)paramMessage.obj);
            }
        }
    }

    // ERROR //
    protected void handlePollStateResult(int paramInt, AsyncResult paramAsyncResult)
    {
        // Byte code:
        //     0: aload_2
        //     1: getfield 1275	android/os/AsyncResult:userObj	Ljava/lang/Object;
        //     4: aload_0
        //     5: getfield 489	com/android/internal/telephony/ServiceStateTracker:pollingContext	[I
        //     8: if_acmpeq +4 -> 12
        //     11: return
        //     12: aload_2
        //     13: getfield 408	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     16: ifnull +198 -> 214
        //     19: aconst_null
        //     20: astore 23
        //     22: aload_2
        //     23: getfield 408	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     26: instanceof 1293
        //     29: ifeq +18 -> 47
        //     32: aload_2
        //     33: getfield 408	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     36: checkcast 1293	com/android/internal/telephony/CommandException
        //     39: checkcast 1293	com/android/internal/telephony/CommandException
        //     42: invokevirtual 1297	com/android/internal/telephony/CommandException:getCommandError	()Lcom/android/internal/telephony/CommandException$Error;
        //     45: astore 23
        //     47: aload 23
        //     49: getstatic 1303	com/android/internal/telephony/CommandException$Error:RADIO_NOT_AVAILABLE	Lcom/android/internal/telephony/CommandException$Error;
        //     52: if_acmpne +10 -> 62
        //     55: aload_0
        //     56: invokevirtual 1306	com/android/internal/telephony/gsm/GsmServiceStateTracker:cancelPollState	()V
        //     59: goto -48 -> 11
        //     62: aload_0
        //     63: getfield 152	com/android/internal/telephony/ServiceStateTracker:cm	Lcom/android/internal/telephony/CommandsInterface;
        //     66: invokeinterface 496 1 0
        //     71: invokevirtual 1237	com/android/internal/telephony/CommandsInterface$RadioState:isOn	()Z
        //     74: ifne +10 -> 84
        //     77: aload_0
        //     78: invokevirtual 1306	com/android/internal/telephony/gsm/GsmServiceStateTracker:cancelPollState	()V
        //     81: goto -70 -> 11
        //     84: aload 23
        //     86: getstatic 1309	com/android/internal/telephony/CommandException$Error:OP_NOT_ALLOWED_BEFORE_REG_NW	Lcom/android/internal/telephony/CommandException$Error;
        //     89: if_acmpeq +30 -> 119
        //     92: aload_0
        //     93: new 334	java/lang/StringBuilder
        //     96: dup
        //     97: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     100: ldc_w 1311
        //     103: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     106: aload_2
        //     107: getfield 408	android/os/AsyncResult:exception	Ljava/lang/Throwable;
        //     110: invokevirtual 344	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     113: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     116: invokevirtual 481	com/android/internal/telephony/gsm/GsmServiceStateTracker:loge	(Ljava/lang/String;)V
        //     119: aload_0
        //     120: getfield 489	com/android/internal/telephony/ServiceStateTracker:pollingContext	[I
        //     123: astore 6
        //     125: aload 6
        //     127: iconst_0
        //     128: bipush 255
        //     130: aload 6
        //     132: iconst_0
        //     133: iaload
        //     134: iadd
        //     135: iastore
        //     136: aload_0
        //     137: getfield 489	com/android/internal/telephony/ServiceStateTracker:pollingContext	[I
        //     140: iconst_0
        //     141: iaload
        //     142: ifne -131 -> 11
        //     145: aload_0
        //     146: getfield 105	com/android/internal/telephony/gsm/GsmServiceStateTracker:mGsmRoaming	Z
        //     149: ifne +10 -> 159
        //     152: aload_0
        //     153: getfield 107	com/android/internal/telephony/gsm/GsmServiceStateTracker:mDataRoaming	Z
        //     156: ifeq +675 -> 831
        //     159: iconst_1
        //     160: istore 7
        //     162: aload_0
        //     163: getfield 105	com/android/internal/telephony/gsm/GsmServiceStateTracker:mGsmRoaming	Z
        //     166: ifeq +21 -> 187
        //     169: aload_0
        //     170: aload_0
        //     171: getfield 105	com/android/internal/telephony/gsm/GsmServiceStateTracker:mGsmRoaming	Z
        //     174: aload_0
        //     175: getfield 162	com/android/internal/telephony/ServiceStateTracker:newSS	Landroid/telephony/ServiceState;
        //     178: invokespecial 1313	com/android/internal/telephony/gsm/GsmServiceStateTracker:isRoamingBetweenOperators	(ZLandroid/telephony/ServiceState;)Z
        //     181: ifne +6 -> 187
        //     184: iconst_0
        //     185: istore 7
        //     187: aload_0
        //     188: getfield 162	com/android/internal/telephony/ServiceStateTracker:newSS	Landroid/telephony/ServiceState;
        //     191: iload 7
        //     193: invokevirtual 1316	android/telephony/ServiceState:setRoaming	(Z)V
        //     196: aload_0
        //     197: getfield 162	com/android/internal/telephony/ServiceStateTracker:newSS	Landroid/telephony/ServiceState;
        //     200: aload_0
        //     201: getfield 109	com/android/internal/telephony/gsm/GsmServiceStateTracker:mEmergencyOnly	Z
        //     204: invokevirtual 1319	android/telephony/ServiceState:setEmergencyOnly	(Z)V
        //     207: aload_0
        //     208: invokespecial 528	com/android/internal/telephony/gsm/GsmServiceStateTracker:pollStateDone	()V
        //     211: goto -200 -> 11
        //     214: iload_1
        //     215: lookupswitch	default:+41->256, 4:+44->259, 5:+344->559, 6:+527->742, 14:+573->788
        //     257: impdep2
        //     258: dneg
        //     259: aload_2
        //     260: getfield 412	android/os/AsyncResult:result	Ljava/lang/Object;
        //     263: checkcast 1243	[Ljava/lang/String;
        //     266: checkcast 1243	[Ljava/lang/String;
        //     269: astore 15
        //     271: bipush 255
        //     273: istore 16
        //     275: bipush 255
        //     277: istore 17
        //     279: bipush 255
        //     281: istore 18
        //     283: bipush 255
        //     285: istore 19
        //     287: aload 15
        //     289: arraylength
        //     290: istore 20
        //     292: iload 20
        //     294: ifle +118 -> 412
        //     297: aload 15
        //     299: iconst_0
        //     300: aaload
        //     301: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     304: istore 18
        //     306: aload 15
        //     308: arraylength
        //     309: iconst_3
        //     310: if_icmplt +59 -> 369
        //     313: aload 15
        //     315: iconst_1
        //     316: aaload
        //     317: ifnull +24 -> 341
        //     320: aload 15
        //     322: iconst_1
        //     323: aaload
        //     324: invokevirtual 766	java/lang/String:length	()I
        //     327: ifle +14 -> 341
        //     330: aload 15
        //     332: iconst_1
        //     333: aaload
        //     334: bipush 16
        //     336: invokestatic 1245	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     339: istore 16
        //     341: aload 15
        //     343: iconst_2
        //     344: aaload
        //     345: ifnull +24 -> 369
        //     348: aload 15
        //     350: iconst_2
        //     351: aaload
        //     352: invokevirtual 766	java/lang/String:length	()I
        //     355: ifle +14 -> 369
        //     358: aload 15
        //     360: iconst_2
        //     361: aaload
        //     362: bipush 16
        //     364: invokestatic 1245	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     367: istore 17
        //     369: aload 15
        //     371: arraylength
        //     372: bipush 14
        //     374: if_icmple +38 -> 412
        //     377: aload 15
        //     379: bipush 14
        //     381: aaload
        //     382: ifnull +30 -> 412
        //     385: aload 15
        //     387: bipush 14
        //     389: aaload
        //     390: invokevirtual 766	java/lang/String:length	()I
        //     393: ifle +19 -> 412
        //     396: aload 15
        //     398: bipush 14
        //     400: aaload
        //     401: bipush 16
        //     403: invokestatic 1245	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     406: istore 22
        //     408: iload 22
        //     410: istore 19
        //     412: aload_0
        //     413: aload_0
        //     414: iload 18
        //     416: invokespecial 1321	com/android/internal/telephony/gsm/GsmServiceStateTracker:regCodeIsRoaming	(I)Z
        //     419: putfield 105	com/android/internal/telephony/gsm/GsmServiceStateTracker:mGsmRoaming	Z
        //     422: aload_0
        //     423: getfield 162	com/android/internal/telephony/ServiceStateTracker:newSS	Landroid/telephony/ServiceState;
        //     426: aload_0
        //     427: iload 18
        //     429: invokespecial 1323	com/android/internal/telephony/gsm/GsmServiceStateTracker:regCodeToServiceState	(I)I
        //     432: invokevirtual 1326	android/telephony/ServiceState:setState	(I)V
        //     435: iload 18
        //     437: bipush 10
        //     439: if_icmpeq +24 -> 463
        //     442: iload 18
        //     444: bipush 12
        //     446: if_icmpeq +17 -> 463
        //     449: iload 18
        //     451: bipush 13
        //     453: if_icmpeq +10 -> 463
        //     456: iload 18
        //     458: bipush 14
        //     460: if_icmpne +91 -> 551
        //     463: aload_0
        //     464: iconst_1
        //     465: putfield 109	com/android/internal/telephony/gsm/GsmServiceStateTracker:mEmergencyOnly	Z
        //     468: aload_0
        //     469: getfield 169	com/android/internal/telephony/gsm/GsmServiceStateTracker:newCellLoc	Landroid/telephony/gsm/GsmCellLocation;
        //     472: iload 16
        //     474: iload 17
        //     476: invokevirtual 1248	android/telephony/gsm/GsmCellLocation:setLacAndCid	(II)V
        //     479: aload_0
        //     480: getfield 169	com/android/internal/telephony/gsm/GsmServiceStateTracker:newCellLoc	Landroid/telephony/gsm/GsmCellLocation;
        //     483: iload 19
        //     485: invokevirtual 1329	android/telephony/gsm/GsmCellLocation:setPsc	(I)V
        //     488: goto -369 -> 119
        //     491: astore 8
        //     493: aload_0
        //     494: new 334	java/lang/StringBuilder
        //     497: dup
        //     498: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     501: ldc_w 1331
        //     504: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     507: aload 8
        //     509: invokevirtual 344	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     512: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     515: invokevirtual 481	com/android/internal/telephony/gsm/GsmServiceStateTracker:loge	(Ljava/lang/String;)V
        //     518: goto -399 -> 119
        //     521: astore 21
        //     523: aload_0
        //     524: new 334	java/lang/StringBuilder
        //     527: dup
        //     528: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     531: ldc_w 1333
        //     534: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     537: aload 21
        //     539: invokevirtual 344	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     542: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     545: invokevirtual 481	com/android/internal/telephony/gsm/GsmServiceStateTracker:loge	(Ljava/lang/String;)V
        //     548: goto -136 -> 412
        //     551: aload_0
        //     552: iconst_0
        //     553: putfield 109	com/android/internal/telephony/gsm/GsmServiceStateTracker:mEmergencyOnly	Z
        //     556: goto -88 -> 468
        //     559: aload_2
        //     560: getfield 412	android/os/AsyncResult:result	Ljava/lang/Object;
        //     563: checkcast 1243	[Ljava/lang/String;
        //     566: checkcast 1243	[Ljava/lang/String;
        //     569: astore 10
        //     571: iconst_0
        //     572: istore 11
        //     574: bipush 255
        //     576: istore 12
        //     578: aload_0
        //     579: bipush 255
        //     581: putfield 103	com/android/internal/telephony/gsm/GsmServiceStateTracker:mNewReasonDataDenied	I
        //     584: aload_0
        //     585: iconst_1
        //     586: putfield 99	com/android/internal/telephony/gsm/GsmServiceStateTracker:mNewMaxDataCalls	I
        //     589: aload 10
        //     591: arraylength
        //     592: istore 13
        //     594: iload 13
        //     596: ifle +78 -> 674
        //     599: aload 10
        //     601: iconst_0
        //     602: aaload
        //     603: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     606: istore 12
        //     608: aload 10
        //     610: arraylength
        //     611: iconst_4
        //     612: if_icmplt +19 -> 631
        //     615: aload 10
        //     617: iconst_3
        //     618: aaload
        //     619: ifnull +12 -> 631
        //     622: aload 10
        //     624: iconst_3
        //     625: aaload
        //     626: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     629: istore 11
        //     631: aload 10
        //     633: arraylength
        //     634: iconst_5
        //     635: if_icmplt +20 -> 655
        //     638: iload 12
        //     640: iconst_3
        //     641: if_icmpne +14 -> 655
        //     644: aload_0
        //     645: aload 10
        //     647: iconst_4
        //     648: aaload
        //     649: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     652: putfield 103	com/android/internal/telephony/gsm/GsmServiceStateTracker:mNewReasonDataDenied	I
        //     655: aload 10
        //     657: arraylength
        //     658: bipush 6
        //     660: if_icmplt +14 -> 674
        //     663: aload_0
        //     664: aload 10
        //     666: iconst_5
        //     667: aaload
        //     668: invokestatic 686	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     671: putfield 99	com/android/internal/telephony/gsm/GsmServiceStateTracker:mNewMaxDataCalls	I
        //     674: aload_0
        //     675: aload_0
        //     676: iload 12
        //     678: invokespecial 1323	com/android/internal/telephony/gsm/GsmServiceStateTracker:regCodeToServiceState	(I)I
        //     681: putfield 95	com/android/internal/telephony/gsm/GsmServiceStateTracker:newGPRSState	I
        //     684: aload_0
        //     685: aload_0
        //     686: iload 12
        //     688: invokespecial 1321	com/android/internal/telephony/gsm/GsmServiceStateTracker:regCodeIsRoaming	(I)Z
        //     691: putfield 107	com/android/internal/telephony/gsm/GsmServiceStateTracker:mDataRoaming	Z
        //     694: aload_0
        //     695: iload 11
        //     697: putfield 571	com/android/internal/telephony/ServiceStateTracker:mNewRilRadioTechnology	I
        //     700: aload_0
        //     701: getfield 162	com/android/internal/telephony/ServiceStateTracker:newSS	Landroid/telephony/ServiceState;
        //     704: iload 11
        //     706: invokevirtual 1336	android/telephony/ServiceState:setRadioTechnology	(I)V
        //     709: goto -590 -> 119
        //     712: astore 14
        //     714: aload_0
        //     715: new 334	java/lang/StringBuilder
        //     718: dup
        //     719: invokespecial 335	java/lang/StringBuilder:<init>	()V
        //     722: ldc_w 1338
        //     725: invokevirtual 341	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     728: aload 14
        //     730: invokevirtual 344	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     733: invokevirtual 348	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     736: invokevirtual 481	com/android/internal/telephony/gsm/GsmServiceStateTracker:loge	(Ljava/lang/String;)V
        //     739: goto -65 -> 674
        //     742: aload_2
        //     743: getfield 412	android/os/AsyncResult:result	Ljava/lang/Object;
        //     746: checkcast 1243	[Ljava/lang/String;
        //     749: checkcast 1243	[Ljava/lang/String;
        //     752: astore 9
        //     754: aload 9
        //     756: ifnull -637 -> 119
        //     759: aload 9
        //     761: arraylength
        //     762: iconst_3
        //     763: if_icmplt -644 -> 119
        //     766: aload_0
        //     767: getfield 162	com/android/internal/telephony/ServiceStateTracker:newSS	Landroid/telephony/ServiceState;
        //     770: aload 9
        //     772: iconst_0
        //     773: aaload
        //     774: aload 9
        //     776: iconst_1
        //     777: aaload
        //     778: aload 9
        //     780: iconst_2
        //     781: aaload
        //     782: invokevirtual 1342	android/telephony/ServiceState:setOperatorName	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //     785: goto -666 -> 119
        //     788: aload_2
        //     789: getfield 412	android/os/AsyncResult:result	Ljava/lang/Object;
        //     792: checkcast 414	[I
        //     795: checkcast 414	[I
        //     798: astore_3
        //     799: aload_0
        //     800: getfield 162	com/android/internal/telephony/ServiceStateTracker:newSS	Landroid/telephony/ServiceState;
        //     803: astore 4
        //     805: aload_3
        //     806: iconst_0
        //     807: iaload
        //     808: iconst_1
        //     809: if_icmpne +16 -> 825
        //     812: iconst_1
        //     813: istore 5
        //     815: aload 4
        //     817: iload 5
        //     819: invokevirtual 1345	android/telephony/ServiceState:setIsManualSelection	(Z)V
        //     822: goto -703 -> 119
        //     825: iconst_0
        //     826: istore 5
        //     828: goto -13 -> 815
        //     831: iconst_0
        //     832: istore 7
        //     834: goto -672 -> 162
        //
        // Exception table:
        //     from	to	target	type
        //     259	292	491	java/lang/RuntimeException
        //     297	408	491	java/lang/RuntimeException
        //     412	488	491	java/lang/RuntimeException
        //     523	594	491	java/lang/RuntimeException
        //     599	674	491	java/lang/RuntimeException
        //     674	822	491	java/lang/RuntimeException
        //     297	408	521	java/lang/NumberFormatException
        //     599	674	712	java/lang/NumberFormatException
    }

    protected void hangupAndPowerOff()
    {
        if (this.phone.isInCall())
        {
            this.phone.mCT.ringingCall.hangupIfAlive();
            this.phone.mCT.backgroundCall.hangupIfAlive();
            this.phone.mCT.foregroundCall.hangupIfAlive();
        }
        this.cm.setRadioPower(false, null);
    }

    public boolean isConcurrentVoiceAndDataAllowed()
    {
        if (this.mRilRadioTechnology >= 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void log(String paramString)
    {
        Log.d("GSM", "[GsmSST] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("GSM", "[GsmSST] " + paramString);
    }

    protected void setPowerStateToDesired()
    {
        if ((this.mDesiredPowerState) && (this.cm.getRadioState() == CommandsInterface.RadioState.RADIO_OFF))
            this.cm.setRadioPower(true, null);
        while (true)
        {
            return;
            if ((!this.mDesiredPowerState) && (this.cm.getRadioState().isOn()))
                powerOffRadioSafely(this.phone.mDataConnectionTracker);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void updateSpnDisplay()
    {
        int i = this.phone.mIccRecords.getDisplayRule(this.ss.getOperatorNumeric());
        String str1 = Injector.getSpn(this, this.phone.mIccRecords.getServiceProviderName());
        String str2 = Injector.getPlmn(this, this.ss.getOperatorAlphaLong());
        if ((this.mEmergencyOnly) && (this.cm.getRadioState().isOn()))
        {
            str2 = Resources.getSystem().getText(17040144).toString();
            log("updateSpnDisplay: emergency only and radio is on plmn='" + str2 + "'");
        }
        boolean bool1;
        if ((i != this.curSpnRule) || (!TextUtils.equals(str1, this.curSpn)) || (!TextUtils.equals(str2, this.curPlmn)))
        {
            if ((this.mEmergencyOnly) || (TextUtils.isEmpty(str1)) || ((i & 0x1) != 1))
                break label352;
            bool1 = true;
            if ((TextUtils.isEmpty(str2)) || ((!this.mEmergencyOnly) && ((i & 0x2) != 2)))
                break label358;
        }
        label352: label358: for (boolean bool2 = true; ; bool2 = false)
        {
            String str3 = "updateSpnDisplay: changed sending intent rule=" + i + " showPlmn='%b' plmn='%s' showSpn='%b' spn='%s'";
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = Boolean.valueOf(bool2);
            arrayOfObject[1] = str2;
            arrayOfObject[2] = Boolean.valueOf(bool1);
            arrayOfObject[3] = str1;
            log(String.format(str3, arrayOfObject));
            Intent localIntent = new Intent("android.provider.Telephony.SPN_STRINGS_UPDATED");
            localIntent.addFlags(536870912);
            localIntent.putExtra("showSpn", bool1);
            localIntent.putExtra("spn", str1);
            localIntent.putExtra("showPlmn", bool2);
            localIntent.putExtra("plmn", str2);
            this.phone.getContext().sendStickyBroadcast(localIntent);
            this.curSpnRule = i;
            this.curSpn = str1;
            this.curPlmn = str2;
            return;
            bool1 = false;
            break;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String getPlmn(GsmServiceStateTracker paramGsmServiceStateTracker, String paramString)
        {
            String str = ((SIMRecords)paramGsmServiceStateTracker.phone.mIccRecords).mSpnOverride.getSpn(paramGsmServiceStateTracker.ss.getOperatorNumeric());
            if (TextUtils.isEmpty(str));
            while (true)
            {
                return paramString;
                paramString = str;
            }
        }

        static String getSpn(GsmServiceStateTracker paramGsmServiceStateTracker, String paramString)
        {
            String str = ((SIMRecords)paramGsmServiceStateTracker.phone.mIccRecords).mSpnOverride.getSpn(paramGsmServiceStateTracker.phone.mIccRecords.getOperatorNumeric());
            if (TextUtils.isEmpty(str));
            while (true)
            {
                return paramString;
                paramString = str;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmServiceStateTracker
 * JD-Core Version:        0.6.2
 */