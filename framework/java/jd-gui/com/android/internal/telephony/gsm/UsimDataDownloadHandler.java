package com.android.internal.telephony.gsm;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccIoResult;
import com.android.internal.telephony.IccUtils;
import com.android.internal.telephony.cat.ComprehensionTlvTag;

public class UsimDataDownloadHandler extends Handler
{
    private static final int BER_SMS_PP_DOWNLOAD_TAG = 209;
    private static final int DEV_ID_NETWORK = 131;
    private static final int DEV_ID_UICC = 129;
    private static final int EVENT_SEND_ENVELOPE_RESPONSE = 2;
    private static final int EVENT_START_DATA_DOWNLOAD = 1;
    private static final String TAG = "UsimDataDownloadHandler";
    private final CommandsInterface mCI;

    public UsimDataDownloadHandler(CommandsInterface paramCommandsInterface)
    {
        this.mCI = paramCommandsInterface;
    }

    private void acknowledgeSmsWithError(int paramInt)
    {
        this.mCI.acknowledgeLastIncomingGsmSms(false, paramInt, null);
    }

    private static int getEnvelopeBodyLength(int paramInt1, int paramInt2)
    {
        int i = paramInt2 + 5;
        if (paramInt2 > 127);
        for (int j = 2; ; j = 1)
        {
            int k = i + j;
            if (paramInt1 != 0)
                k = paramInt1 + (k + 2);
            return k;
        }
    }

    private void handleDataDownload(SmsMessage paramSmsMessage)
    {
        int i = paramSmsMessage.getDataCodingScheme();
        int j = paramSmsMessage.getProtocolIdentifier();
        byte[] arrayOfByte1 = paramSmsMessage.getPdu();
        int k = 0xFF & arrayOfByte1[0];
        int m = k + 1;
        int n = arrayOfByte1.length - m;
        int i1 = getEnvelopeBodyLength(k, n);
        int i2 = i1 + 1;
        int i3;
        byte[] arrayOfByte2;
        int i4;
        int i5;
        if (i1 > 127)
        {
            i3 = 2;
            arrayOfByte2 = new byte[i2 + i3];
            i4 = 0 + 1;
            arrayOfByte2[0] = -47;
            if (i1 <= 127)
                break label398;
            i5 = i4 + 1;
            arrayOfByte2[i4] = -127;
        }
        while (true)
        {
            int i6 = i5 + 1;
            arrayOfByte2[i5] = ((byte)i1);
            int i7 = i6 + 1;
            arrayOfByte2[i6] = ((byte)(0x80 | ComprehensionTlvTag.DEVICE_IDENTITIES.value()));
            int i8 = i7 + 1;
            arrayOfByte2[i7] = 2;
            int i9 = i8 + 1;
            arrayOfByte2[i8] = -125;
            int i10 = i9 + 1;
            arrayOfByte2[i9] = -127;
            int i16;
            if (k != 0)
            {
                int i15 = i10 + 1;
                arrayOfByte2[i10] = ((byte)ComprehensionTlvTag.ADDRESS.value());
                i16 = i15 + 1;
                arrayOfByte2[i15] = ((byte)k);
                System.arraycopy(arrayOfByte1, 1, arrayOfByte2, i16, k);
            }
            for (int i11 = i16 + k; ; i11 = i10)
            {
                int i12 = i11 + 1;
                arrayOfByte2[i11] = ((byte)(0x80 | ComprehensionTlvTag.SMS_TPDU.value()));
                int i13;
                if (n > 127)
                {
                    i13 = i12 + 1;
                    arrayOfByte2[i12] = -127;
                }
                while (true)
                {
                    int i14 = i13 + 1;
                    arrayOfByte2[i13] = ((byte)n);
                    System.arraycopy(arrayOfByte1, m, arrayOfByte2, i14, n);
                    if (i14 + n != arrayOfByte2.length)
                    {
                        Log.e("UsimDataDownloadHandler", "startDataDownload() calculated incorrect envelope length, aborting.");
                        acknowledgeSmsWithError(255);
                    }
                    while (true)
                    {
                        return;
                        i3 = 1;
                        break;
                        String str = IccUtils.bytesToHexString(arrayOfByte2);
                        CommandsInterface localCommandsInterface = this.mCI;
                        int[] arrayOfInt = new int[2];
                        arrayOfInt[0] = i;
                        arrayOfInt[1] = j;
                        localCommandsInterface.sendEnvelopeWithStatus(str, obtainMessage(2, arrayOfInt));
                    }
                    i13 = i12;
                }
            }
            label398: i5 = i4;
        }
    }

    private static boolean is7bitDcs(int paramInt)
    {
        if (((paramInt & 0x8C) == 0) || ((paramInt & 0xF4) == 240));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void sendSmsAckForEnvelopeResponse(IccIoResult paramIccIoResult, int paramInt1, int paramInt2)
    {
        int i = paramIccIoResult.sw1;
        int j = paramIccIoResult.sw2;
        boolean bool;
        byte[] arrayOfByte1;
        if (((i == 144) && (j == 0)) || (i == 145))
        {
            Log.d("UsimDataDownloadHandler", "USIM data download succeeded: " + paramIccIoResult.toString());
            bool = true;
            arrayOfByte1 = paramIccIoResult.payload;
            if ((arrayOfByte1 != null) && (arrayOfByte1.length != 0))
                break label222;
            if (!bool)
                break label212;
            this.mCI.acknowledgeLastIncomingGsmSms(true, 0, null);
        }
        while (true)
        {
            return;
            if ((i == 147) && (j == 0))
            {
                Log.e("UsimDataDownloadHandler", "USIM data download failed: Toolkit busy");
                acknowledgeSmsWithError(212);
            }
            else
            {
                if ((i == 98) || (i == 99))
                {
                    Log.e("UsimDataDownloadHandler", "USIM data download failed: " + paramIccIoResult.toString());
                    bool = false;
                    break;
                }
                Log.e("UsimDataDownloadHandler", "Unexpected SW1/SW2 response from UICC: " + paramIccIoResult.toString());
                bool = false;
                break;
                label212: acknowledgeSmsWithError(213);
            }
        }
        label222: byte[] arrayOfByte2;
        int i1;
        label259: int i3;
        int i7;
        if (bool)
        {
            arrayOfByte2 = new byte[5 + arrayOfByte1.length];
            int i8 = 0 + 1;
            arrayOfByte2[0] = 0;
            i1 = i8 + 1;
            arrayOfByte2[i8] = 7;
            int i2 = i1 + 1;
            arrayOfByte2[i1] = ((byte)paramInt2);
            i3 = i2 + 1;
            arrayOfByte2[i2] = ((byte)paramInt1);
            if (!is7bitDcs(paramInt1))
                break label407;
            int i6 = 8 * arrayOfByte1.length / 7;
            i7 = i3 + 1;
            arrayOfByte2[i3] = ((byte)i6);
        }
        label407: int i4;
        for (int i5 = i7; ; i5 = i4)
        {
            System.arraycopy(arrayOfByte1, 0, arrayOfByte2, i5, arrayOfByte1.length);
            this.mCI.acknowledgeIncomingGsmSmsWithPdu(bool, IccUtils.bytesToHexString(arrayOfByte2), null);
            break;
            arrayOfByte2 = new byte[6 + arrayOfByte1.length];
            int k = 0 + 1;
            arrayOfByte2[0] = 0;
            int m = k + 1;
            arrayOfByte2[k] = -43;
            int n = m + 1;
            arrayOfByte2[m] = 7;
            i1 = n;
            break label259;
            i4 = i3 + 1;
            arrayOfByte2[i3] = ((byte)arrayOfByte1.length);
        }
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
            Log.e("UsimDataDownloadHandler", "Ignoring unexpected message, what=" + paramMessage.what);
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            handleDataDownload((SmsMessage)paramMessage.obj);
            continue;
            AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
            if (localAsyncResult.exception != null)
            {
                Log.e("UsimDataDownloadHandler", "UICC Send Envelope failure, exception: " + localAsyncResult.exception);
                acknowledgeSmsWithError(213);
            }
            else
            {
                int[] arrayOfInt = (int[])localAsyncResult.userObj;
                sendSmsAckForEnvelopeResponse((IccIoResult)localAsyncResult.result, arrayOfInt[0], arrayOfInt[1]);
            }
        }
    }

    public int startDataDownload(SmsMessage paramSmsMessage)
    {
        if (sendMessage(obtainMessage(1, paramSmsMessage)));
        for (int i = -1; ; i = 2)
        {
            return i;
            Log.e("UsimDataDownloadHandler", "startDataDownload failed to send message to start data download.");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.UsimDataDownloadHandler
 * JD-Core Version:        0.6.2
 */