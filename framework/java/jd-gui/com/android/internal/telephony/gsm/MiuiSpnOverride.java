package com.android.internal.telephony.gsm;

public class MiuiSpnOverride
{
    public static final String EQ_OPERATOR_CM = "46000";
    public static final String EQ_OPERATOR_CT = "46003";
    public static final String EQ_OPERATOR_CU = "46001";
    public static final String MCC_CHINA = "460";
    public static final String MCC_NONE = "000";
    public static final String MCC_TAIWAN = "466";
    private static final MiuiSpnOverrideImpl sImpl = new MiuiSpnOverrideImpl();
    static final MiuiSpnOverride sInstance = new MiuiSpnOverride();

    public static MiuiSpnOverride getInstance()
    {
        return sInstance;
    }

    public boolean containsCarrier(String paramString)
    {
        return sImpl.containsCarrier(paramString);
    }

    public String getEquivalentOperatorNumeric(String paramString)
    {
        return sImpl.getEquivalentOperatorNumeric(paramString);
    }

    public String getSpn(String paramString)
    {
        return sImpl.getSpn(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.MiuiSpnOverride
 * JD-Core Version:        0.6.2
 */