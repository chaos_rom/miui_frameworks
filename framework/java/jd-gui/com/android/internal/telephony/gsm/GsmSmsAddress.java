package com.android.internal.telephony.gsm;

import android.telephony.PhoneNumberUtils;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.SmsAddress;

public class GsmSmsAddress extends SmsAddress
{
    static final int OFFSET_ADDRESS_LENGTH = 0;
    static final int OFFSET_ADDRESS_VALUE = 2;
    static final int OFFSET_TOA = 1;

    public GsmSmsAddress(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        this.origBytes = new byte[paramInt2];
        System.arraycopy(paramArrayOfByte, paramInt1, this.origBytes, 0, paramInt2);
        int i = 0xFF & this.origBytes[0];
        int j = 0xFF & this.origBytes[1];
        this.ton = (0x7 & j >> 4);
        if ((j & 0x80) != 128)
            throw new RuntimeException("Invalid TOA - high bit must be set");
        if (isAlphanumeric())
        {
            int n = i * 4 / 7;
            this.address = GsmAlphabet.gsm7BitPackedToString(this.origBytes, 2, n);
        }
        while (true)
        {
            return;
            int k = this.origBytes[(paramInt2 - 1)];
            if ((i & 0x1) == 1)
            {
                byte[] arrayOfByte = this.origBytes;
                int m = paramInt2 - 1;
                arrayOfByte[m] = ((byte)(0xF0 | arrayOfByte[m]));
            }
            this.address = PhoneNumberUtils.calledPartyBCDToString(this.origBytes, 1, paramInt2 - 1);
            this.origBytes[(paramInt2 - 1)] = k;
        }
    }

    public String getAddressString()
    {
        return this.address;
    }

    public boolean isAlphanumeric()
    {
        if (this.ton == 5);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isCphsVoiceMessageClear()
    {
        if ((isCphsVoiceMessageIndicatorAddress()) && ((0xFF & this.origBytes[2]) == 16));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isCphsVoiceMessageIndicatorAddress()
    {
        boolean bool = true;
        if (((0xFF & this.origBytes[0]) == 4) && (isAlphanumeric()) && ((0xF & this.origBytes[bool]) == 0));
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public boolean isCphsVoiceMessageSet()
    {
        if ((isCphsVoiceMessageIndicatorAddress()) && ((0xFF & this.origBytes[2]) == 17));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isNetworkSpecific()
    {
        if (this.ton == 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmSmsAddress
 * JD-Core Version:        0.6.2
 */