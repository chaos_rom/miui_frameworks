package com.android.internal.telephony.gsm;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Intent;
import android.os.AsyncResult;
import android.os.Message;
import android.os.SystemProperties;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsCbLocation;
import android.telephony.SmsMessage.MessageClass;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccUtils;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.SMSDispatcher;
import com.android.internal.telephony.SMSDispatcher.SmsTracker;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.SmsMessageBase.SubmitPduBase;
import com.android.internal.telephony.SmsMessageBase.TextEncodingDetails;
import com.android.internal.telephony.SmsStorageMonitor;
import com.android.internal.telephony.SmsUsageMonitor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public final class GsmSMSDispatcher extends SMSDispatcher
{
    private static final int EVENT_NEW_BROADCAST_SMS = 101;
    private static final int EVENT_NEW_SMS_STATUS_REPORT = 100;
    private static final int EVENT_WRITE_SMS_COMPLETE = 102;
    private static final String TAG = "GSM";
    private final UsimDataDownloadHandler mDataDownloadHandler = new UsimDataDownloadHandler(this.mCm);
    private final HashMap<SmsCbConcatInfo, byte[][]> mSmsCbPageMap = new HashMap();

    public GsmSMSDispatcher(PhoneBase paramPhoneBase, SmsStorageMonitor paramSmsStorageMonitor, SmsUsageMonitor paramSmsUsageMonitor)
    {
        super(paramPhoneBase, paramSmsStorageMonitor, paramSmsUsageMonitor);
        this.mCm.setOnNewGsmSms(this, 1, null);
        this.mCm.setOnSmsStatus(this, 100, null);
        this.mCm.setOnNewGsmBroadcastSms(this, 101, null);
    }

    private void handleBroadcastSms(AsyncResult paramAsyncResult)
    {
        while (true)
        {
            int m;
            try
            {
                arrayOfByte = (byte[])paramAsyncResult.result;
                SmsCbHeader localSmsCbHeader = new SmsCbHeader(arrayOfByte);
                String str = SystemProperties.get("gsm.operator.numeric");
                GsmCellLocation localGsmCellLocation = (GsmCellLocation)this.mPhone.getCellLocation();
                int i = localGsmCellLocation.getLac();
                int j = localGsmCellLocation.getCid();
                SmsCbConcatInfo localSmsCbConcatInfo;
                switch (localSmsCbHeader.getGeographicalScope())
                {
                case 1:
                default:
                    localSmsCbLocation = new SmsCbLocation(str);
                    int k = localSmsCbHeader.getNumberOfPages();
                    if (k <= 1)
                        continue;
                    localSmsCbConcatInfo = new SmsCbConcatInfo(localSmsCbHeader, localSmsCbLocation);
                    localObject = (byte[][])this.mSmsCbPageMap.get(localSmsCbConcatInfo);
                    if (localObject == null)
                    {
                        localObject = new byte[k][];
                        this.mSmsCbPageMap.put(localSmsCbConcatInfo, localObject);
                    }
                    localObject[(-1 + localSmsCbHeader.getPageIndex())] = arrayOfByte;
                    m = 0;
                    if (m >= localObject.length)
                        continue;
                    if (localObject[m] != null)
                        break label346;
                    break;
                case 2:
                    localSmsCbLocation = new SmsCbLocation(str, i, -1);
                    break;
                case 0:
                case 3:
                }
                SmsCbLocation localSmsCbLocation = new SmsCbLocation(str, i, j);
                continue;
                this.mSmsCbPageMap.remove(localSmsCbConcatInfo);
                dispatchBroadcastMessage(GsmSmsCbMessage.createSmsCbMessage(localSmsCbHeader, localSmsCbLocation, (byte[][])localObject));
                Iterator localIterator = this.mSmsCbPageMap.keySet().iterator();
                if (localIterator.hasNext())
                {
                    if (((SmsCbConcatInfo)localIterator.next()).matchesLocation(str, i, j))
                        continue;
                    localIterator.remove();
                    continue;
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                byte[] arrayOfByte;
                Log.e("GSM", "Error in decoding SMS CB pdu", localRuntimeException);
                break label345;
                Object localObject = new byte[1][];
                localObject[0] = arrayOfByte;
                continue;
            }
            label345: return;
            label346: m++;
        }
    }

    private void handleStatusReport(AsyncResult paramAsyncResult)
    {
        String str = (String)paramAsyncResult.result;
        SmsMessage localSmsMessage = SmsMessage.newFromCDS(str);
        int i;
        int j;
        int k;
        int m;
        if (localSmsMessage != null)
        {
            i = localSmsMessage.getStatus();
            j = localSmsMessage.messageRef;
            k = 0;
            m = this.deliveryPendingList.size();
        }
        while (true)
        {
            PendingIntent localPendingIntent;
            Intent localIntent;
            if (k < m)
            {
                SMSDispatcher.SmsTracker localSmsTracker = (SMSDispatcher.SmsTracker)this.deliveryPendingList.get(k);
                if (localSmsTracker.mMessageRef != j)
                    break label155;
                if ((i >= 64) || (i < 32))
                    this.deliveryPendingList.remove(k);
                localPendingIntent = localSmsTracker.mDeliveryIntent;
                localIntent = new Intent();
                localIntent.putExtra("pdu", IccUtils.hexStringToBytes(str));
                localIntent.putExtra("format", "3gpp");
            }
            try
            {
                localPendingIntent.send(this.mContext, -1, localIntent);
                label147: acknowledgeLastIncomingSms(true, 1, null);
                return;
                label155: k++;
            }
            catch (PendingIntent.CanceledException localCanceledException)
            {
                break label147;
            }
        }
    }

    private static int resultToCause(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        case 0:
        case 2:
        default:
            i = 255;
        case -1:
        case 1:
        case 3:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 211;
        }
    }

    protected void acknowledgeLastIncomingSms(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        this.mCm.acknowledgeLastIncomingGsmSms(paramBoolean, resultToCause(paramInt), paramMessage);
    }

    protected SmsMessageBase.TextEncodingDetails calculateLength(CharSequence paramCharSequence, boolean paramBoolean)
    {
        return SmsMessage.calculateLength(paramCharSequence, paramBoolean);
    }

    public int dispatchMessage(SmsMessageBase paramSmsMessageBase)
    {
        int i = 1;
        if (paramSmsMessageBase == null)
        {
            Log.e("GSM", "dispatchMessage: message is null");
            i = 2;
        }
        while (true)
        {
            return i;
            SmsMessage localSmsMessage = (SmsMessage)paramSmsMessageBase;
            if (localSmsMessage.isTypeZero())
            {
                Log.d("GSM", "Received short message type 0, Don't display or store it. Send Ack");
            }
            else if (localSmsMessage.isUsimDataDownload())
            {
                UsimServiceTable localUsimServiceTable = this.mPhone.getUsimServiceTable();
                if ((localUsimServiceTable != null) && (localUsimServiceTable.isAvailable(UsimServiceTable.UsimService.DATA_DL_VIA_SMS_PP)))
                {
                    Log.d("GSM", "Received SMS-PP data download, sending to UICC.");
                    i = this.mDataDownloadHandler.startDataDownload(localSmsMessage);
                }
                else
                {
                    Log.d("GSM", "DATA_DL_VIA_SMS_PP service not available, storing message to UICC.");
                    String str = IccUtils.bytesToHexString(PhoneNumberUtils.networkPortionToCalledPartyBCDWithLength(localSmsMessage.getServiceCenterAddress()));
                    this.mCm.writeSmsToSim(3, str, IccUtils.bytesToHexString(localSmsMessage.getPdu()), obtainMessage(102));
                    i = -1;
                }
            }
            else if (this.mSmsReceiveDisabled)
            {
                Log.d("GSM", "Received short message on device which doesn't support SMS service. Ignored.");
            }
            else
            {
                boolean bool = false;
                if (localSmsMessage.isMWISetMessage())
                {
                    this.mPhone.setVoiceMessageWaiting(i, -1);
                    bool = localSmsMessage.isMwiDontStore();
                }
                while (true)
                {
                    if (bool)
                        break label252;
                    if ((this.mStorageMonitor.isStorageAvailable()) || (localSmsMessage.getMessageClass() == SmsMessage.MessageClass.CLASS_0))
                        break label254;
                    i = 3;
                    break;
                    if (localSmsMessage.isMWIClearMessage())
                    {
                        this.mPhone.setVoiceMessageWaiting(i, 0);
                        bool = localSmsMessage.isMwiDontStore();
                    }
                }
                label252: continue;
                label254: i = dispatchNormalMessage(paramSmsMessageBase);
            }
        }
    }

    public void dispose()
    {
        this.mCm.unSetOnNewGsmSms(this);
        this.mCm.unSetOnSmsStatus(this);
        this.mCm.unSetOnNewGsmBroadcastSms(this);
    }

    protected String getFormat()
    {
        return "3gpp";
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
            super.handleMessage(paramMessage);
        case 100:
        case 101:
        case 102:
        }
        while (true)
        {
            return;
            handleStatusReport((AsyncResult)paramMessage.obj);
            continue;
            handleBroadcastSms((AsyncResult)paramMessage.obj);
            continue;
            AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
            if (localAsyncResult.exception == null)
            {
                Log.d("GSM", "Successfully wrote SMS-PP message to UICC");
                this.mCm.acknowledgeLastIncomingGsmSms(true, 0, null);
            }
            else
            {
                Log.d("GSM", "Failed to write SMS-PP message to UICC", localAsyncResult.exception);
                this.mCm.acknowledgeLastIncomingGsmSms(false, 255, null);
            }
        }
    }

    protected void sendData(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfByte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        boolean bool;
        if (paramPendingIntent2 != null)
        {
            bool = true;
            SmsMessage.SubmitPdu localSubmitPdu = SmsMessage.getSubmitPdu(paramString2, paramString1, paramInt, paramArrayOfByte, bool);
            if (localSubmitPdu == null)
                break label51;
            sendRawPdu(localSubmitPdu.encodedScAddress, localSubmitPdu.encodedMessage, paramPendingIntent1, paramPendingIntent2, paramString1);
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label51: Log.e("GSM", "GsmSMSDispatcher.sendData(): getSubmitPdu() returned null");
        }
    }

    protected void sendNewSubmitPdu(String paramString1, String paramString2, String paramString3, SmsHeader paramSmsHeader, int paramInt, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean)
    {
        boolean bool;
        if (paramPendingIntent2 != null)
        {
            bool = true;
            SmsMessage.SubmitPdu localSubmitPdu = SmsMessage.getSubmitPdu(paramString2, paramString1, paramString3, bool, SmsHeader.toByteArray(paramSmsHeader), paramInt, paramSmsHeader.languageTable, paramSmsHeader.languageShiftTable);
            if (localSubmitPdu == null)
                break label66;
            sendRawPdu(localSubmitPdu.encodedScAddress, localSubmitPdu.encodedMessage, paramPendingIntent1, paramPendingIntent2, paramString1);
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label66: Log.e("GSM", "GsmSMSDispatcher.sendNewSubmitPdu(): getSubmitPdu() returned null");
        }
    }

    protected void sendSms(SMSDispatcher.SmsTracker paramSmsTracker)
    {
        HashMap localHashMap = paramSmsTracker.mData;
        byte[] arrayOfByte1 = (byte[])localHashMap.get("smsc");
        byte[] arrayOfByte2 = (byte[])localHashMap.get("pdu");
        Message localMessage = obtainMessage(2, paramSmsTracker);
        this.mCm.sendSMS(IccUtils.bytesToHexString(arrayOfByte1), IccUtils.bytesToHexString(arrayOfByte2), localMessage);
    }

    protected void sendText(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        boolean bool;
        if (paramPendingIntent2 != null)
        {
            bool = true;
            SmsMessage.SubmitPdu localSubmitPdu = SmsMessage.getSubmitPdu(paramString2, paramString1, paramString3, bool);
            if (localSubmitPdu == null)
                break label49;
            sendRawPdu(localSubmitPdu.encodedScAddress, localSubmitPdu.encodedMessage, paramPendingIntent1, paramPendingIntent2, paramString1);
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label49: Log.e("GSM", "GsmSMSDispatcher.sendText(): getSubmitPdu() returned null");
        }
    }

    private static final class SmsCbConcatInfo
    {
        private final SmsCbHeader mHeader;
        private final SmsCbLocation mLocation;

        public SmsCbConcatInfo(SmsCbHeader paramSmsCbHeader, SmsCbLocation paramSmsCbLocation)
        {
            this.mHeader = paramSmsCbHeader;
            this.mLocation = paramSmsCbLocation;
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = false;
            if ((paramObject instanceof SmsCbConcatInfo))
            {
                SmsCbConcatInfo localSmsCbConcatInfo = (SmsCbConcatInfo)paramObject;
                if ((this.mHeader.getSerialNumber() == localSmsCbConcatInfo.mHeader.getSerialNumber()) && (this.mLocation.equals(localSmsCbConcatInfo.mLocation)))
                    bool = true;
            }
            return bool;
        }

        public int hashCode()
        {
            return 31 * this.mHeader.getSerialNumber() + this.mLocation.hashCode();
        }

        public boolean matchesLocation(String paramString, int paramInt1, int paramInt2)
        {
            return this.mLocation.isInLocationArea(paramString, paramInt1, paramInt2);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.GsmSMSDispatcher
 * JD-Core Version:        0.6.2
 */