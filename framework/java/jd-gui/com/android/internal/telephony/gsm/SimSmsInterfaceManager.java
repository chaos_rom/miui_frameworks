package com.android.internal.telephony.gsm;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccSmsInterfaceManager;
import com.android.internal.telephony.IccUtils;
import com.android.internal.telephony.IntRangeManager;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.SMSDispatcher;
import com.android.internal.telephony.SmsRawData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class SimSmsInterfaceManager extends IccSmsInterfaceManager
{
    static final boolean DBG = true;
    private static final int EVENT_LOAD_DONE = 1;
    private static final int EVENT_SET_BROADCAST_ACTIVATION_DONE = 3;
    private static final int EVENT_SET_BROADCAST_CONFIG_DONE = 4;
    private static final int EVENT_UPDATE_DONE = 2;
    static final String LOG_TAG = "GSM";
    private static final int SMS_CB_CODE_SCHEME_MAX = 255;
    private static final int SMS_CB_CODE_SCHEME_MIN;
    private CellBroadcastRangeManager mCellBroadcastRangeManager = new CellBroadcastRangeManager();
    private HashMap<Integer, HashSet<String>> mCellBroadcastSubscriptions = new HashMap();
    Handler mHandler = new Handler()
    {
        // ERROR //
        public void handleMessage(Message paramAnonymousMessage)
        {
            // Byte code:
            //     0: iconst_1
            //     1: istore_2
            //     2: aload_1
            //     3: getfield 23	android/os/Message:what	I
            //     6: tableswitch	default:+30 -> 36, 1:+102->108, 2:+31->37, 3:+212->218, 4:+212->218
            //     37: aload_1
            //     38: getfield 27	android/os/Message:obj	Ljava/lang/Object;
            //     41: checkcast 29	android/os/AsyncResult
            //     44: astore 12
            //     46: aload_0
            //     47: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     50: invokestatic 33	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;)Ljava/lang/Object;
            //     53: astore 13
            //     55: aload 13
            //     57: monitorenter
            //     58: aload_0
            //     59: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     62: astore 15
            //     64: aload 12
            //     66: getfield 37	android/os/AsyncResult:exception	Ljava/lang/Throwable;
            //     69: ifnonnull +34 -> 103
            //     72: aload 15
            //     74: iload_2
            //     75: invokestatic 41	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$102	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;Z)Z
            //     78: pop
            //     79: aload_0
            //     80: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     83: invokestatic 33	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;)Ljava/lang/Object;
            //     86: invokevirtual 46	java/lang/Object:notifyAll	()V
            //     89: aload 13
            //     91: monitorexit
            //     92: goto -56 -> 36
            //     95: astore 14
            //     97: aload 13
            //     99: monitorexit
            //     100: aload 14
            //     102: athrow
            //     103: iconst_0
            //     104: istore_2
            //     105: goto -33 -> 72
            //     108: aload_1
            //     109: getfield 27	android/os/Message:obj	Ljava/lang/Object;
            //     112: checkcast 29	android/os/AsyncResult
            //     115: astore 8
            //     117: aload_0
            //     118: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     121: invokestatic 33	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;)Ljava/lang/Object;
            //     124: astore 9
            //     126: aload 9
            //     128: monitorenter
            //     129: aload 8
            //     131: getfield 37	android/os/AsyncResult:exception	Ljava/lang/Throwable;
            //     134: ifnonnull +50 -> 184
            //     137: aload_0
            //     138: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     141: aload_0
            //     142: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     145: aload 8
            //     147: getfield 49	android/os/AsyncResult:result	Ljava/lang/Object;
            //     150: checkcast 51	java/util/ArrayList
            //     153: invokestatic 55	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$300	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;
            //     156: invokestatic 59	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$202	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;Ljava/util/List;)Ljava/util/List;
            //     159: pop
            //     160: aload_0
            //     161: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     164: invokestatic 33	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;)Ljava/lang/Object;
            //     167: invokevirtual 46	java/lang/Object:notifyAll	()V
            //     170: aload 9
            //     172: monitorexit
            //     173: goto -137 -> 36
            //     176: astore 10
            //     178: aload 9
            //     180: monitorexit
            //     181: aload 10
            //     183: athrow
            //     184: aload_0
            //     185: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     188: ldc 61
            //     190: invokevirtual 65	com/android/internal/telephony/gsm/SimSmsInterfaceManager:log	(Ljava/lang/String;)V
            //     193: aload_0
            //     194: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     197: invokestatic 69	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$200	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;)Ljava/util/List;
            //     200: ifnull -40 -> 160
            //     203: aload_0
            //     204: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     207: invokestatic 69	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$200	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;)Ljava/util/List;
            //     210: invokeinterface 74 1 0
            //     215: goto -55 -> 160
            //     218: aload_1
            //     219: getfield 27	android/os/Message:obj	Ljava/lang/Object;
            //     222: checkcast 29	android/os/AsyncResult
            //     225: astore_3
            //     226: aload_0
            //     227: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     230: invokestatic 33	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;)Ljava/lang/Object;
            //     233: astore 4
            //     235: aload 4
            //     237: monitorenter
            //     238: aload_0
            //     239: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     242: astore 6
            //     244: aload_3
            //     245: getfield 37	android/os/AsyncResult:exception	Ljava/lang/Throwable;
            //     248: ifnonnull +34 -> 282
            //     251: aload 6
            //     253: iload_2
            //     254: invokestatic 41	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$102	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;Z)Z
            //     257: pop
            //     258: aload_0
            //     259: getfield 12	com/android/internal/telephony/gsm/SimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;
            //     262: invokestatic 33	com/android/internal/telephony/gsm/SimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/gsm/SimSmsInterfaceManager;)Ljava/lang/Object;
            //     265: invokevirtual 46	java/lang/Object:notifyAll	()V
            //     268: aload 4
            //     270: monitorexit
            //     271: goto -235 -> 36
            //     274: astore 5
            //     276: aload 4
            //     278: monitorexit
            //     279: aload 5
            //     281: athrow
            //     282: iconst_0
            //     283: istore_2
            //     284: goto -33 -> 251
            //
            // Exception table:
            //     from	to	target	type
            //     58	100	95	finally
            //     129	181	176	finally
            //     184	215	176	finally
            //     238	279	274	finally
        }
    };
    private final Object mLock = new Object();
    private List<SmsRawData> mSms;
    private boolean mSuccess;

    public SimSmsInterfaceManager(GSMPhone paramGSMPhone, SMSDispatcher paramSMSDispatcher)
    {
        super(paramGSMPhone);
        this.mDispatcher = paramSMSDispatcher;
    }

    private boolean setCellBroadcastActivation(boolean paramBoolean)
    {
        log("Calling setCellBroadcastActivation(" + paramBoolean + ')');
        synchronized (this.mLock)
        {
            Message localMessage = this.mHandler.obtainMessage(3);
            this.mSuccess = false;
            this.mPhone.mCM.setGsmBroadcastActivation(paramBoolean, localMessage);
            try
            {
                this.mLock.wait();
                return this.mSuccess;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    log("interrupted while trying to set cell broadcast activation");
            }
        }
    }

    private boolean setCellBroadcastConfig(SmsBroadcastConfigInfo[] paramArrayOfSmsBroadcastConfigInfo)
    {
        log("Calling setGsmBroadcastConfig with " + paramArrayOfSmsBroadcastConfigInfo.length + " configurations");
        synchronized (this.mLock)
        {
            Message localMessage = this.mHandler.obtainMessage(4);
            this.mSuccess = false;
            this.mPhone.mCM.setGsmBroadcastConfig(paramArrayOfSmsBroadcastConfigInfo, localMessage);
            try
            {
                this.mLock.wait();
                return this.mSuccess;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    log("interrupted while trying to set cell broadcast config");
            }
        }
    }

    public boolean copyMessageToIccEf(int paramInt, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        log("copyMessageToIccEf: status=" + paramInt + " ==> " + "pdu=(" + Arrays.toString(paramArrayOfByte1) + "), smsm=(" + Arrays.toString(paramArrayOfByte2) + ")");
        enforceReceiveAndSend("Copying message to SIM");
        synchronized (this.mLock)
        {
            this.mSuccess = false;
            Message localMessage = this.mHandler.obtainMessage(2);
            this.mPhone.mCM.writeSmsToSim(paramInt, IccUtils.bytesToHexString(paramArrayOfByte2), IccUtils.bytesToHexString(paramArrayOfByte1), localMessage);
            try
            {
                this.mLock.wait();
                return this.mSuccess;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    log("interrupted while trying to update by index");
            }
        }
    }

    public boolean disableCellBroadcast(int paramInt)
    {
        return disableCellBroadcastRange(paramInt, paramInt);
    }

    public boolean disableCellBroadcastRange(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        log("disableCellBroadcastRange");
        Context localContext = this.mPhone.getContext();
        localContext.enforceCallingPermission("android.permission.RECEIVE_SMS", "Disabling cell broadcast SMS");
        String str = localContext.getPackageManager().getNameForUid(Binder.getCallingUid());
        if (!this.mCellBroadcastRangeManager.disableRange(paramInt1, paramInt2, str))
            log("Failed to remove cell broadcast subscription for MID range " + paramInt1 + " to " + paramInt2 + " from client " + str);
        while (true)
        {
            return bool;
            log("Removed cell broadcast subscription for MID range " + paramInt1 + " to " + paramInt2 + " from client " + str);
            if (!this.mCellBroadcastRangeManager.isEmpty())
                bool = true;
            setCellBroadcastActivation(bool);
            bool = true;
        }
    }

    public void dispose()
    {
    }

    public boolean enableCellBroadcast(int paramInt)
    {
        return enableCellBroadcastRange(paramInt, paramInt);
    }

    public boolean enableCellBroadcastRange(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        log("enableCellBroadcastRange");
        Context localContext = this.mPhone.getContext();
        localContext.enforceCallingPermission("android.permission.RECEIVE_SMS", "Enabling cell broadcast SMS");
        String str = localContext.getPackageManager().getNameForUid(Binder.getCallingUid());
        if (!this.mCellBroadcastRangeManager.enableRange(paramInt1, paramInt2, str))
            log("Failed to add cell broadcast subscription for MID range " + paramInt1 + " to " + paramInt2 + " from client " + str);
        while (true)
        {
            return bool;
            log("Added cell broadcast subscription for MID range " + paramInt1 + " to " + paramInt2 + " from client " + str);
            if (!this.mCellBroadcastRangeManager.isEmpty())
                bool = true;
            setCellBroadcastActivation(bool);
            bool = true;
        }
    }

    protected void finalize()
    {
        try
        {
            super.finalize();
            Log.d("GSM", "SimSmsInterfaceManager finalized");
            return;
        }
        catch (Throwable localThrowable)
        {
            while (true)
                Log.e("GSM", "Error while finalizing:", localThrowable);
        }
    }

    public List<SmsRawData> getAllMessagesFromIccEf()
    {
        log("getAllMessagesFromEF");
        this.mPhone.getContext().enforceCallingPermission("android.permission.RECEIVE_SMS", "Reading messages from SIM");
        synchronized (this.mLock)
        {
            Message localMessage = this.mHandler.obtainMessage(1);
            this.mPhone.getIccFileHandler().loadEFLinearFixedAll(28476, localMessage);
            try
            {
                this.mLock.wait();
                return this.mSms;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    log("interrupted while trying to load from the SIM");
            }
        }
    }

    protected void log(String paramString)
    {
        Log.d("GSM", "[SimSmsInterfaceManager] " + paramString);
    }

    public boolean updateMessageOnIccEf(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        log("updateMessageOnIccEf: index=" + paramInt1 + " status=" + paramInt2 + " ==> " + "(" + Arrays.toString(paramArrayOfByte) + ")");
        enforceReceiveAndSend("Updating message on SIM");
        Message localMessage;
        synchronized (this.mLock)
        {
            this.mSuccess = false;
            localMessage = this.mHandler.obtainMessage(2);
            if (paramInt2 == 0)
                this.mPhone.mCM.deleteSmsOnSim(paramInt1, localMessage);
        }
        try
        {
            while (true)
            {
                this.mLock.wait();
                return this.mSuccess;
                byte[] arrayOfByte = makeSmsRecordData(paramInt2, paramArrayOfByte);
                this.mPhone.getIccFileHandler().updateEFLinearFixed(28476, paramInt1, arrayOfByte, null, localMessage);
            }
            localObject2 = finally;
            throw localObject2;
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
                log("interrupted while trying to update by index");
        }
    }

    class CellBroadcastRangeManager extends IntRangeManager
    {
        private ArrayList<SmsBroadcastConfigInfo> mConfigList = new ArrayList();

        CellBroadcastRangeManager()
        {
        }

        protected void addRange(int paramInt1, int paramInt2, boolean paramBoolean)
        {
            this.mConfigList.add(new SmsBroadcastConfigInfo(paramInt1, paramInt2, 0, 255, paramBoolean));
        }

        protected boolean finishUpdate()
        {
            if (this.mConfigList.isEmpty());
            SmsBroadcastConfigInfo[] arrayOfSmsBroadcastConfigInfo;
            for (boolean bool = true; ; bool = SimSmsInterfaceManager.this.setCellBroadcastConfig(arrayOfSmsBroadcastConfigInfo))
            {
                return bool;
                arrayOfSmsBroadcastConfigInfo = (SmsBroadcastConfigInfo[])this.mConfigList.toArray(new SmsBroadcastConfigInfo[this.mConfigList.size()]);
            }
        }

        protected void startUpdate()
        {
            this.mConfigList.clear();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.gsm.SimSmsInterfaceManager
 * JD-Core Version:        0.6.2
 */