package com.android.internal.telephony;

public class WapPushManagerParams
{
    public static final int APP_QUERY_FAILED = 2;
    public static final int APP_TYPE_ACTIVITY = 0;
    public static final int APP_TYPE_SERVICE = 1;
    public static final int EXCEPTION_CAUGHT = 16;
    public static final int FURTHER_PROCESSING = 32768;
    public static final int INVALID_RECEIVER_NAME = 8;
    public static final int MESSAGE_HANDLED = 1;
    public static final int SIGNATURE_NO_MATCH = 4;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.WapPushManagerParams
 * JD-Core Version:        0.6.2
 */