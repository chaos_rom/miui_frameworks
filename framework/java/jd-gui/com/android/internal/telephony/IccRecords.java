package com.android.internal.telephony;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import com.android.internal.telephony.gsm.UsimServiceTable;
import com.android.internal.telephony.ims.IsimRecords;

public abstract class IccRecords extends Handler
    implements IccConstants
{
    protected static final boolean DBG = true;
    public static final int EVENT_CFI = 1;
    public static final int EVENT_GET_ICC_RECORD_DONE = 100;
    public static final int EVENT_MWI = 0;
    protected static final int EVENT_SET_MSISDN_DONE = 30;
    public static final int EVENT_SPN = 2;
    protected static final int SPN_RULE_SHOW_PLMN = 2;
    protected static final int SPN_RULE_SHOW_SPN = 1;
    protected static final int UNINITIALIZED = -1;
    protected static final int UNKNOWN;
    protected AdnRecordCache adnCache;
    protected int countVoiceMessages = 0;
    public String iccid;
    protected boolean isVoiceMailFixed = false;
    protected CommandsInterface mCi;
    protected Context mContext;
    protected boolean mDestroyed = false;
    protected IccFileHandler mFh;
    protected RegistrantList mNetworkSelectionModeAutomaticRegistrants = new RegistrantList();
    protected RegistrantList mNewSmsRegistrants = new RegistrantList();
    protected IccCard mParentCard;
    protected RegistrantList mRecordsEventsRegistrants = new RegistrantList();
    protected int mailboxIndex = 0;
    protected int mncLength = -1;
    protected String msisdn = null;
    protected String msisdnTag = null;
    protected String newVoiceMailNum = null;
    protected String newVoiceMailTag = null;
    protected RegistrantList recordsLoadedRegistrants = new RegistrantList();
    protected boolean recordsRequested = false;
    protected int recordsToLoad;
    protected String spn;
    protected String voiceMailNum = null;
    protected String voiceMailTag = null;

    public IccRecords(IccCard paramIccCard, Context paramContext, CommandsInterface paramCommandsInterface)
    {
        this.mContext = paramContext;
        this.mCi = paramCommandsInterface;
        this.mFh = paramIccCard.getIccFileHandler();
        this.mParentCard = paramIccCard;
    }

    public void dispose()
    {
        this.mDestroyed = true;
        this.mParentCard = null;
        this.mFh = null;
        this.mCi = null;
        this.mContext = null;
    }

    public AdnRecordCache getAdnCache()
    {
        return this.adnCache;
    }

    public abstract int getDisplayRule(String paramString);

    public String getIMSI()
    {
        return null;
    }

    public IccCard getIccCard()
    {
        return this.mParentCard;
    }

    public IsimRecords getIsimRecords()
    {
        return null;
    }

    public String getMsisdnAlphaTag()
    {
        return this.msisdnTag;
    }

    public String getMsisdnNumber()
    {
        return this.msisdn;
    }

    public String getOperatorNumeric()
    {
        return null;
    }

    public boolean getRecordsLoaded()
    {
        boolean bool = true;
        if ((this.recordsToLoad == 0) && (this.recordsRequested == bool));
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public String getServiceProviderName()
    {
        return this.spn;
    }

    public UsimServiceTable getUsimServiceTable()
    {
        return null;
    }

    public boolean getVoiceCallForwardingFlag()
    {
        return false;
    }

    public String getVoiceMailAlphaTag()
    {
        return this.voiceMailTag;
    }

    public String getVoiceMailNumber()
    {
        return this.voiceMailNum;
    }

    public int getVoiceMessageCount()
    {
        return this.countVoiceMessages;
    }

    public boolean getVoiceMessageWaiting()
    {
        if (this.countVoiceMessages != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
            super.handleMessage(paramMessage);
        case 100:
        }
        while (true)
        {
            return;
            try
            {
                AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
                IccRecordLoaded localIccRecordLoaded = (IccRecordLoaded)localAsyncResult.userObj;
                log(localIccRecordLoaded.getEfName() + " LOADED");
                if (localAsyncResult.exception != null)
                    loge("Record Load Exception: " + localAsyncResult.exception);
                while (true)
                {
                    onRecordLoaded();
                    break;
                    localIccRecordLoaded.onRecordLoaded(localAsyncResult);
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                while (true)
                    loge("Exception parsing SIM record: " + localRuntimeException);
            }
            finally
            {
                onRecordLoaded();
            }
        }
    }

    public boolean isCspPlmnEnabled()
    {
        return false;
    }

    public boolean isProvisioned()
    {
        return true;
    }

    protected abstract void log(String paramString);

    protected abstract void loge(String paramString);

    protected abstract void onAllRecordsLoaded();

    protected abstract void onRadioOffOrNotAvailable();

    public abstract void onReady();

    protected abstract void onRecordLoaded();

    public abstract void onRefresh(boolean paramBoolean, int[] paramArrayOfInt);

    public void registerForNetworkSelectionModeAutomatic(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mNetworkSelectionModeAutomaticRegistrants.add(localRegistrant);
    }

    public void registerForNewSms(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mNewSmsRegistrants.add(localRegistrant);
    }

    public void registerForRecordsEvents(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mRecordsEventsRegistrants.add(localRegistrant);
    }

    public void registerForRecordsLoaded(Handler paramHandler, int paramInt, Object paramObject)
    {
        if (this.mDestroyed);
        while (true)
        {
            return;
            Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
            this.recordsLoadedRegistrants.add(localRegistrant);
            if ((this.recordsToLoad == 0) && (this.recordsRequested == true))
                localRegistrant.notifyRegistrant(new AsyncResult(null, null, null));
        }
    }

    public void setMsisdnNumber(String paramString1, String paramString2, Message paramMessage)
    {
        this.msisdn = paramString2;
        this.msisdnTag = paramString1;
        log("Set MSISDN: " + this.msisdnTag + " " + this.msisdn);
        AdnRecord localAdnRecord = new AdnRecord(this.msisdnTag, this.msisdn);
        new AdnRecordLoader(this.mFh).updateEF(localAdnRecord, 28480, 28490, 1, null, obtainMessage(30, paramMessage));
    }

    public void setVoiceCallForwardingFlag(int paramInt, boolean paramBoolean)
    {
    }

    public abstract void setVoiceMailNumber(String paramString1, String paramString2, Message paramMessage);

    public abstract void setVoiceMessageWaiting(int paramInt1, int paramInt2);

    public void unregisterForNetworkSelectionModeAutomatic(Handler paramHandler)
    {
        this.mNetworkSelectionModeAutomaticRegistrants.remove(paramHandler);
    }

    public void unregisterForNewSms(Handler paramHandler)
    {
        this.mNewSmsRegistrants.remove(paramHandler);
    }

    public void unregisterForRecordsEvents(Handler paramHandler)
    {
        this.mRecordsEventsRegistrants.remove(paramHandler);
    }

    public void unregisterForRecordsLoaded(Handler paramHandler)
    {
        this.recordsLoadedRegistrants.remove(paramHandler);
    }

    public static abstract interface IccRecordLoaded
    {
        public abstract String getEfName();

        public abstract void onRecordLoaded(AsyncResult paramAsyncResult);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccRecords
 * JD-Core Version:        0.6.2
 */