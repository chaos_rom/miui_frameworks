package com.android.internal.telephony;

import android.app.PendingIntent;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.net.ProxyProperties;
import android.os.Message;
import android.util.Log;
import com.android.internal.util.AsyncChannel;
import java.util.ArrayList;
import java.util.Collection;

public class DataConnectionAc extends AsyncChannel
{
    public static final int BASE = 266240;
    private static final int CMD_TO_STRING_COUNT = 28;
    private static final boolean DBG = false;
    public static final int REQ_ADD_APNCONTEXT = 266258;
    public static final int REQ_GET_APNCONTEXT_LIST = 266262;
    public static final int REQ_GET_APNSETTING = 266244;
    public static final int REQ_GET_CID = 266242;
    public static final int REQ_GET_LINK_CAPABILITIES = 266250;
    public static final int REQ_GET_LINK_PROPERTIES = 266246;
    public static final int REQ_GET_RECONNECT_INTENT = 266266;
    public static final int REQ_GET_REFCOUNT = 266256;
    public static final int REQ_IS_INACTIVE = 266240;
    public static final int REQ_REMOVE_APNCONTEXT = 266260;
    public static final int REQ_RESET = 266254;
    public static final int REQ_SET_LINK_PROPERTIES_HTTP_PROXY = 266248;
    public static final int REQ_SET_RECONNECT_INTENT = 266264;
    public static final int REQ_UPDATE_LINK_PROPERTIES_DATA_CALL_STATE = 266252;
    public static final int RSP_ADD_APNCONTEXT = 266259;
    public static final int RSP_GET_APNCONTEXT_LIST = 266263;
    public static final int RSP_GET_APNSETTING = 266245;
    public static final int RSP_GET_CID = 266243;
    public static final int RSP_GET_LINK_CAPABILITIES = 266251;
    public static final int RSP_GET_LINK_PROPERTIES = 266247;
    public static final int RSP_GET_RECONNECT_INTENT = 266267;
    public static final int RSP_GET_REFCOUNT = 266257;
    public static final int RSP_IS_INACTIVE = 266241;
    public static final int RSP_REMOVE_APNCONTEXT = 266261;
    public static final int RSP_RESET = 266255;
    public static final int RSP_SET_LINK_PROPERTIES_HTTP_PROXY = 266249;
    public static final int RSP_SET_RECONNECT_INTENT = 266265;
    public static final int RSP_UPDATE_LINK_PROPERTIES_DATA_CALL_STATE = 266253;
    private static String[] sCmdToString = new String[28];
    public DataConnection dataConnection;
    private String mLogTag;

    static
    {
        sCmdToString[0] = "REQ_IS_INACTIVE";
        sCmdToString[1] = "RSP_IS_INACTIVE";
        sCmdToString[2] = "REQ_GET_CID";
        sCmdToString[3] = "RSP_GET_CID";
        sCmdToString[4] = "REQ_GET_APNSETTING";
        sCmdToString[5] = "RSP_GET_APNSETTING";
        sCmdToString[6] = "REQ_GET_LINK_PROPERTIES";
        sCmdToString[7] = "RSP_GET_LINK_PROPERTIES";
        sCmdToString[8] = "REQ_SET_LINK_PROPERTIES_HTTP_PROXY";
        sCmdToString[9] = "RSP_SET_LINK_PROPERTIES_HTTP_PROXY";
        sCmdToString[10] = "REQ_GET_LINK_CAPABILITIES";
        sCmdToString[11] = "RSP_GET_LINK_CAPABILITIES";
        sCmdToString[12] = "REQ_UPDATE_LINK_PROPERTIES_DATA_CALL_STATE";
        sCmdToString[13] = "RSP_UPDATE_LINK_PROPERTIES_DATA_CALL_STATE";
        sCmdToString[14] = "REQ_RESET";
        sCmdToString[15] = "RSP_RESET";
        sCmdToString[16] = "REQ_GET_REFCOUNT";
        sCmdToString[17] = "RSP_GET_REFCOUNT";
        sCmdToString[18] = "REQ_ADD_APNCONTEXT";
        sCmdToString[19] = "RSP_ADD_APNCONTEXT";
        sCmdToString[20] = "REQ_REMOVE_APNCONTEXT";
        sCmdToString[21] = "RSP_REMOVE_APNCONTEXT";
        sCmdToString[22] = "REQ_GET_APNCONTEXT_LIST";
        sCmdToString[23] = "RSP_GET_APNCONTEXT_LIST";
        sCmdToString[24] = "REQ_SET_RECONNECT_INTENT";
        sCmdToString[25] = "RSP_SET_RECONNECT_INTENT";
        sCmdToString[26] = "REQ_GET_RECONNECT_INTENT";
        sCmdToString[27] = "RSP_GET_RECONNECT_INTENT";
    }

    public DataConnectionAc(DataConnection paramDataConnection, String paramString)
    {
        this.dataConnection = paramDataConnection;
        this.mLogTag = paramString;
    }

    protected static String cmdToString(int paramInt)
    {
        int i = paramInt - 266240;
        if ((i >= 0) && (i < sCmdToString.length));
        for (String str = sCmdToString[i]; ; str = AsyncChannel.cmdToString(i + 266240))
            return str;
    }

    private void log(String paramString)
    {
        Log.d(this.mLogTag, "DataConnectionAc " + paramString);
    }

    public void addApnContextSync(ApnContext paramApnContext)
    {
        Message localMessage = sendMessageSynchronously(266258, paramApnContext);
        if ((localMessage != null) && (localMessage.what == 266259));
        while (true)
        {
            return;
            log("addApnContext error response=" + localMessage);
        }
    }

    public Collection<ApnContext> getApnListSync()
    {
        Message localMessage = sendMessageSynchronously(266262);
        if ((localMessage != null) && (localMessage.what == 266263));
        for (Object localObject = rspApnList(localMessage); ; localObject = new ArrayList())
        {
            return localObject;
            log("getApnList error response=" + localMessage);
        }
    }

    public ApnSetting getApnSettingSync()
    {
        Message localMessage = sendMessageSynchronously(266244);
        if ((localMessage != null) && (localMessage.what == 266245));
        for (ApnSetting localApnSetting = rspApnSetting(localMessage); ; localApnSetting = null)
        {
            return localApnSetting;
            log("getApnSetting error response=" + localMessage);
        }
    }

    public int getCidSync()
    {
        Message localMessage = sendMessageSynchronously(266242);
        if ((localMessage != null) && (localMessage.what == 266243));
        for (int i = rspCid(localMessage); ; i = -1)
        {
            return i;
            log("rspCid error response=" + localMessage);
        }
    }

    public LinkCapabilities getLinkCapabilitiesSync()
    {
        Message localMessage = sendMessageSynchronously(266250);
        if ((localMessage != null) && (localMessage.what == 266251));
        for (LinkCapabilities localLinkCapabilities = rspLinkCapabilities(localMessage); ; localLinkCapabilities = null)
        {
            return localLinkCapabilities;
            log("getLinkCapabilities error response=" + localMessage);
        }
    }

    public LinkProperties getLinkPropertiesSync()
    {
        Message localMessage = sendMessageSynchronously(266246);
        if ((localMessage != null) && (localMessage.what == 266247));
        for (LinkProperties localLinkProperties = rspLinkProperties(localMessage); ; localLinkProperties = null)
        {
            return localLinkProperties;
            log("getLinkProperties error response=" + localMessage);
        }
    }

    public PendingIntent getReconnectIntentSync()
    {
        Message localMessage = sendMessageSynchronously(266266);
        if ((localMessage != null) && (localMessage.what == 266267));
        for (PendingIntent localPendingIntent = rspReconnectIntent(localMessage); ; localPendingIntent = null)
        {
            return localPendingIntent;
            log("getReconnectIntent error response=" + localMessage);
        }
    }

    public int getRefCountSync()
    {
        Message localMessage = sendMessageSynchronously(266256);
        if ((localMessage != null) && (localMessage.what == 266257));
        for (int i = rspRefCount(localMessage); ; i = -1)
        {
            return i;
            log("rspRefCount error response=" + localMessage);
        }
    }

    public boolean isInactiveSync()
    {
        Message localMessage = sendMessageSynchronously(266240);
        if ((localMessage != null) && (localMessage.what == 266241));
        for (boolean bool = rspIsInactive(localMessage); ; bool = false)
        {
            return bool;
            log("rspIsInactive error response=" + localMessage);
        }
    }

    public void removeApnContextSync(ApnContext paramApnContext)
    {
        Message localMessage = sendMessageSynchronously(266260, paramApnContext);
        if ((localMessage != null) && (localMessage.what == 266261));
        while (true)
        {
            return;
            log("removeApnContext error response=" + localMessage);
        }
    }

    public void reqAddApnContext(ApnContext paramApnContext)
    {
        sendMessageSynchronously(266258, paramApnContext);
    }

    public void reqApnSetting()
    {
        sendMessage(266244);
    }

    public void reqCid()
    {
        sendMessage(266242);
    }

    public void reqGetApnList(ApnContext paramApnContext)
    {
        sendMessageSynchronously(266262);
    }

    public void reqGetReconnectIntent()
    {
        sendMessageSynchronously(266266);
    }

    public void reqIsInactive()
    {
        sendMessage(266240);
    }

    public void reqLinkCapabilities()
    {
        sendMessage(266250);
    }

    public void reqLinkProperties()
    {
        sendMessage(266246);
    }

    public void reqRefCount()
    {
        sendMessage(266256);
    }

    public void reqRemomveApnContext(ApnContext paramApnContext)
    {
        sendMessageSynchronously(266260, paramApnContext);
    }

    public void reqReset()
    {
        sendMessage(266254);
    }

    public void reqSetLinkPropertiesHttpProxy(ProxyProperties paramProxyProperties)
    {
        sendMessage(266248, paramProxyProperties);
    }

    public void reqSetReconnectIntent(PendingIntent paramPendingIntent)
    {
        sendMessageSynchronously(266264, paramPendingIntent);
    }

    public void reqUpdateLinkPropertiesDataCallState(DataCallState paramDataCallState)
    {
        sendMessage(266252, paramDataCallState);
    }

    public void resetSync()
    {
        Message localMessage = sendMessageSynchronously(266254);
        if ((localMessage != null) && (localMessage.what == 266255));
        while (true)
        {
            return;
            log("restSync error response=" + localMessage);
        }
    }

    public Collection<ApnContext> rspApnList(Message paramMessage)
    {
        Object localObject = (Collection)paramMessage.obj;
        if (localObject == null)
            localObject = new ArrayList();
        return localObject;
    }

    public ApnSetting rspApnSetting(Message paramMessage)
    {
        return (ApnSetting)paramMessage.obj;
    }

    public int rspCid(Message paramMessage)
    {
        return paramMessage.arg1;
    }

    public boolean rspIsInactive(Message paramMessage)
    {
        int i = 1;
        if (paramMessage.arg1 == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public LinkCapabilities rspLinkCapabilities(Message paramMessage)
    {
        return (LinkCapabilities)paramMessage.obj;
    }

    public LinkProperties rspLinkProperties(Message paramMessage)
    {
        return (LinkProperties)paramMessage.obj;
    }

    public PendingIntent rspReconnectIntent(Message paramMessage)
    {
        return (PendingIntent)paramMessage.obj;
    }

    public int rspRefCount(Message paramMessage)
    {
        return paramMessage.arg1;
    }

    public DataConnection.UpdateLinkPropertyResult rspUpdateLinkPropertiesDataCallState(Message paramMessage)
    {
        return (DataConnection.UpdateLinkPropertyResult)paramMessage.obj;
    }

    public void setLinkPropertiesHttpProxySync(ProxyProperties paramProxyProperties)
    {
        Message localMessage = sendMessageSynchronously(266248, paramProxyProperties);
        if ((localMessage != null) && (localMessage.what == 266249));
        while (true)
        {
            return;
            log("setLinkPropertiesHttpPoxy error response=" + localMessage);
        }
    }

    public void setReconnectIntentSync(PendingIntent paramPendingIntent)
    {
        Message localMessage = sendMessageSynchronously(266264, paramPendingIntent);
        if ((localMessage != null) && (localMessage.what == 266265));
        while (true)
        {
            return;
            log("setReconnectIntent error response=" + localMessage);
        }
    }

    public String toString()
    {
        return this.dataConnection.getName();
    }

    public DataConnection.UpdateLinkPropertyResult updateLinkPropertiesDataCallStateSync(DataCallState paramDataCallState)
    {
        Message localMessage = sendMessageSynchronously(266252, paramDataCallState);
        if ((localMessage != null) && (localMessage.what == 266253));
        for (DataConnection.UpdateLinkPropertyResult localUpdateLinkPropertyResult = rspUpdateLinkPropertiesDataCallState(localMessage); ; localUpdateLinkPropertyResult = new DataConnection.UpdateLinkPropertyResult(new LinkProperties()))
        {
            return localUpdateLinkPropertyResult;
            log("getLinkProperties error response=" + localMessage);
        }
    }

    public static enum LinkPropertyChangeAction
    {
        static
        {
            CHANGED = new LinkPropertyChangeAction("CHANGED", 1);
            RESET = new LinkPropertyChangeAction("RESET", 2);
            LinkPropertyChangeAction[] arrayOfLinkPropertyChangeAction = new LinkPropertyChangeAction[3];
            arrayOfLinkPropertyChangeAction[0] = NONE;
            arrayOfLinkPropertyChangeAction[1] = CHANGED;
            arrayOfLinkPropertyChangeAction[2] = RESET;
        }

        public static LinkPropertyChangeAction fromInt(int paramInt)
        {
            LinkPropertyChangeAction localLinkPropertyChangeAction;
            if (paramInt == NONE.ordinal())
                localLinkPropertyChangeAction = NONE;
            while (true)
            {
                return localLinkPropertyChangeAction;
                if (paramInt == CHANGED.ordinal())
                {
                    localLinkPropertyChangeAction = CHANGED;
                }
                else
                {
                    if (paramInt != RESET.ordinal())
                        break;
                    localLinkPropertyChangeAction = RESET;
                }
            }
            throw new RuntimeException("LinkPropertyChangeAction.fromInt: bad value=" + paramInt);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.DataConnectionAc
 * JD-Core Version:        0.6.2
 */