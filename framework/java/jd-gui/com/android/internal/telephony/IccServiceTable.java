package com.android.internal.telephony;

import android.util.Log;

public abstract class IccServiceTable
{
    protected final byte[] mServiceTable;

    protected IccServiceTable(byte[] paramArrayOfByte)
    {
        this.mServiceTable = paramArrayOfByte;
    }

    protected abstract String getTag();

    protected abstract Object[] getValues();

    protected boolean isAvailable(int paramInt)
    {
        int i = 1;
        boolean bool = false;
        int j = paramInt / 8;
        if (j >= this.mServiceTable.length)
        {
            Log.e(getTag(), "isAvailable for service " + (paramInt + 1) + " fails, max service is " + 8 * this.mServiceTable.length);
            return bool;
        }
        int k = paramInt % 8;
        if ((this.mServiceTable[j] & i << k) != 0);
        while (true)
        {
            bool = i;
            break;
            i = 0;
        }
    }

    public String toString()
    {
        Object[] arrayOfObject = getValues();
        int i = this.mServiceTable.length;
        StringBuilder localStringBuilder = new StringBuilder(getTag()).append('[').append(i * 8).append("]={ ");
        int j = 0;
        for (int k = 0; k < i; k++)
        {
            int m = this.mServiceTable[k];
            int n = 0;
            if (n < 8)
            {
                label93: int i1;
                if ((m & 1 << n) != 0)
                {
                    if (j == 0)
                        break label125;
                    localStringBuilder.append(", ");
                    i1 = n + k * 8;
                    if (i1 >= arrayOfObject.length)
                        break label131;
                    localStringBuilder.append(arrayOfObject[i1]);
                }
                while (true)
                {
                    n++;
                    break;
                    label125: j = 1;
                    break label93;
                    label131: localStringBuilder.append('#').append(i1 + 1);
                }
            }
        }
        return " }";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccServiceTable
 * JD-Core Version:        0.6.2
 */