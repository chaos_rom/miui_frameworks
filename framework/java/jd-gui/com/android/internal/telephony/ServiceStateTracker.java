package com.android.internal.telephony;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.util.TimeUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class ServiceStateTracker extends Handler
{
    protected static final boolean DBG = true;
    public static final int DEFAULT_GPRS_CHECK_PERIOD_MILLIS = 60000;
    protected static final int EVENT_CDMA_PRL_VERSION_CHANGED = 40;
    protected static final int EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED = 39;
    protected static final int EVENT_CHECK_REPORT_GPRS = 22;
    protected static final int EVENT_ERI_FILE_LOADED = 36;
    protected static final int EVENT_GET_LOC_DONE = 15;
    protected static final int EVENT_GET_LOC_DONE_CDMA = 31;
    protected static final int EVENT_GET_PREFERRED_NETWORK_TYPE = 19;
    protected static final int EVENT_GET_SIGNAL_STRENGTH = 3;
    protected static final int EVENT_GET_SIGNAL_STRENGTH_CDMA = 29;
    protected static final int EVENT_LOCATION_UPDATES_ENABLED = 18;
    protected static final int EVENT_NETWORK_STATE_CHANGED = 2;
    protected static final int EVENT_NETWORK_STATE_CHANGED_CDMA = 30;
    protected static final int EVENT_NITZ_TIME = 11;
    protected static final int EVENT_NV_LOADED = 33;
    protected static final int EVENT_NV_READY = 35;
    protected static final int EVENT_OTA_PROVISION_STATUS_CHANGE = 37;
    protected static final int EVENT_POLL_SIGNAL_STRENGTH = 10;
    protected static final int EVENT_POLL_SIGNAL_STRENGTH_CDMA = 28;
    protected static final int EVENT_POLL_STATE_CDMA_SUBSCRIPTION = 34;
    protected static final int EVENT_POLL_STATE_GPRS = 5;
    protected static final int EVENT_POLL_STATE_NETWORK_SELECTION_MODE = 14;
    protected static final int EVENT_POLL_STATE_OPERATOR = 6;
    protected static final int EVENT_POLL_STATE_OPERATOR_CDMA = 25;
    protected static final int EVENT_POLL_STATE_REGISTRATION = 4;
    protected static final int EVENT_POLL_STATE_REGISTRATION_CDMA = 24;
    protected static final int EVENT_RADIO_AVAILABLE = 13;
    protected static final int EVENT_RADIO_ON = 41;
    protected static final int EVENT_RADIO_STATE_CHANGED = 1;
    protected static final int EVENT_RESET_PREFERRED_NETWORK_TYPE = 21;
    protected static final int EVENT_RESTRICTED_STATE_CHANGED = 23;
    protected static final int EVENT_RUIM_READY = 26;
    protected static final int EVENT_RUIM_RECORDS_LOADED = 27;
    protected static final int EVENT_SET_PREFERRED_NETWORK_TYPE = 20;
    protected static final int EVENT_SET_RADIO_POWER_OFF = 38;
    protected static final int EVENT_SIGNAL_STRENGTH_UPDATE = 12;
    protected static final int EVENT_SIGNAL_STRENGTH_UPDATE_CDMA = 32;
    protected static final int EVENT_SIM_READY = 17;
    protected static final int EVENT_SIM_RECORDS_LOADED = 16;
    protected static final String[] GMT_COUNTRY_CODES = arrayOfString;
    public static final int OTASP_NEEDED = 2;
    public static final int OTASP_NOT_NEEDED = 3;
    public static final int OTASP_UNINITIALIZED = 0;
    public static final int OTASP_UNKNOWN = 1;
    protected static final int POLL_PERIOD_MILLIS = 20000;
    protected static final String REGISTRATION_DENIED_AUTH = "Authentication Failure";
    protected static final String REGISTRATION_DENIED_GEN = "General";
    protected static final String TIMEZONE_PROPERTY = "persist.sys.timezone";
    protected CommandsInterface cm;
    protected boolean dontPollSignalStrength = false;
    protected RegistrantList mAttachedRegistrants = new RegistrantList();
    protected boolean mDesiredPowerState;
    protected RegistrantList mDetachedRegistrants = new RegistrantList();
    protected RegistrantList mNetworkAttachedRegistrants = new RegistrantList();
    protected int mNewRilRadioTechnology = 0;
    private boolean mPendingRadioPowerOffAfterDataOff = false;
    private int mPendingRadioPowerOffAfterDataOffTag = 0;
    protected RegistrantList mPsRestrictDisabledRegistrants = new RegistrantList();
    protected RegistrantList mPsRestrictEnabledRegistrants = new RegistrantList();
    public RestrictedState mRestrictedState = new RestrictedState();
    protected int mRilRadioTechnology = 0;
    protected RegistrantList mRoamingOffRegistrants = new RegistrantList();
    protected RegistrantList mRoamingOnRegistrants = new RegistrantList();
    public SignalStrength mSignalStrength;
    private boolean mWantContinuousLocationUpdates;
    private boolean mWantSingleLocationUpdate;
    protected ServiceState newSS;
    protected int[] pollingContext;
    public ServiceState ss;

    static
    {
        String[] arrayOfString = new String[20];
        arrayOfString[0] = "bf";
        arrayOfString[1] = "ci";
        arrayOfString[2] = "eh";
        arrayOfString[3] = "fo";
        arrayOfString[4] = "gb";
        arrayOfString[5] = "gh";
        arrayOfString[6] = "gm";
        arrayOfString[7] = "gn";
        arrayOfString[8] = "gw";
        arrayOfString[9] = "ie";
        arrayOfString[10] = "lr";
        arrayOfString[11] = "is";
        arrayOfString[12] = "ma";
        arrayOfString[13] = "ml";
        arrayOfString[14] = "mr";
        arrayOfString[15] = "pt";
        arrayOfString[16] = "sl";
        arrayOfString[17] = "sn";
        arrayOfString[18] = "st";
        arrayOfString[19] = "tg";
    }

    protected void cancelPollState()
    {
        this.pollingContext = new int[1];
    }

    public void disableLocationUpdates()
    {
        this.mWantContinuousLocationUpdates = false;
        if ((!this.mWantSingleLocationUpdate) && (!this.mWantContinuousLocationUpdates))
            this.cm.setLocationUpdates(false, null);
    }

    protected void disableSingleLocationUpdate()
    {
        this.mWantSingleLocationUpdate = false;
        if ((!this.mWantSingleLocationUpdate) && (!this.mWantContinuousLocationUpdates))
            this.cm.setLocationUpdates(false, null);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("ServiceStateTracker:");
        paramPrintWriter.println(" ss=" + this.ss);
        paramPrintWriter.println(" newSS=" + this.newSS);
        paramPrintWriter.println(" mSignalStrength=" + this.mSignalStrength);
        paramPrintWriter.println(" mRestrictedState=" + this.mRestrictedState);
        paramPrintWriter.println(" pollingContext=" + this.pollingContext);
        paramPrintWriter.println(" mDesiredPowerState=" + this.mDesiredPowerState);
        paramPrintWriter.println(" mRilRadioTechnology=" + this.mRilRadioTechnology);
        paramPrintWriter.println(" mNewRilRadioTechnology=" + this.mNewRilRadioTechnology);
        paramPrintWriter.println(" dontPollSignalStrength=" + this.dontPollSignalStrength);
        paramPrintWriter.println(" mPendingRadioPowerOffAfterDataOff=" + this.mPendingRadioPowerOffAfterDataOff);
        paramPrintWriter.println(" mPendingRadioPowerOffAfterDataOffTag=" + this.mPendingRadioPowerOffAfterDataOffTag);
    }

    public void enableLocationUpdates()
    {
        if ((this.mWantSingleLocationUpdate) || (this.mWantContinuousLocationUpdates));
        while (true)
        {
            return;
            this.mWantContinuousLocationUpdates = true;
            this.cm.setLocationUpdates(true, obtainMessage(18));
        }
    }

    public void enableSingleLocationUpdate()
    {
        if ((this.mWantSingleLocationUpdate) || (this.mWantContinuousLocationUpdates));
        while (true)
        {
            return;
            this.mWantSingleLocationUpdate = true;
            this.cm.setLocationUpdates(true, obtainMessage(18));
        }
    }

    public abstract int getCurrentDataConnectionState();

    public boolean getDesiredPowerState()
    {
        return this.mDesiredPowerState;
    }

    protected abstract Phone getPhone();

    // ERROR //
    public void handleMessage(Message paramMessage)
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 308	android/os/Message:what	I
        //     4: tableswitch	default:+20 -> 24, 38:+48->52
        //     25: new 241	java/lang/StringBuilder
        //     28: dup
        //     29: invokespecial 242	java/lang/StringBuilder:<init>	()V
        //     32: ldc_w 310
        //     35: invokevirtual 248	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     38: aload_1
        //     39: getfield 308	android/os/Message:what	I
        //     42: invokevirtual 281	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: invokevirtual 257	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     48: invokevirtual 313	com/android/internal/telephony/ServiceStateTracker:log	(Ljava/lang/String;)V
        //     51: return
        //     52: aload_0
        //     53: monitorenter
        //     54: aload_0
        //     55: getfield 210	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOff	Z
        //     58: ifeq +50 -> 108
        //     61: aload_1
        //     62: getfield 316	android/os/Message:arg1	I
        //     65: aload_0
        //     66: getfield 212	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOffTag	I
        //     69: if_icmpne +39 -> 108
        //     72: aload_0
        //     73: ldc_w 318
        //     76: invokevirtual 313	com/android/internal/telephony/ServiceStateTracker:log	(Ljava/lang/String;)V
        //     79: aload_0
        //     80: invokevirtual 321	com/android/internal/telephony/ServiceStateTracker:hangupAndPowerOff	()V
        //     83: aload_0
        //     84: iconst_1
        //     85: aload_0
        //     86: getfield 212	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOffTag	I
        //     89: iadd
        //     90: putfield 212	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOffTag	I
        //     93: aload_0
        //     94: iconst_0
        //     95: putfield 210	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOff	Z
        //     98: aload_0
        //     99: monitorexit
        //     100: goto -49 -> 51
        //     103: astore_2
        //     104: aload_0
        //     105: monitorexit
        //     106: aload_2
        //     107: athrow
        //     108: aload_0
        //     109: new 241	java/lang/StringBuilder
        //     112: dup
        //     113: invokespecial 242	java/lang/StringBuilder:<init>	()V
        //     116: ldc_w 323
        //     119: invokevirtual 248	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     122: aload_1
        //     123: getfield 316	android/os/Message:arg1	I
        //     126: invokevirtual 281	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     129: ldc_w 325
        //     132: invokevirtual 248	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     135: aload_0
        //     136: getfield 212	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOffTag	I
        //     139: invokevirtual 281	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     142: invokevirtual 257	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     145: invokevirtual 313	com/android/internal/telephony/ServiceStateTracker:log	(Ljava/lang/String;)V
        //     148: goto -50 -> 98
        //
        // Exception table:
        //     from	to	target	type
        //     54	106	103	finally
        //     108	148	103	finally
    }

    protected abstract void handlePollStateResult(int paramInt, AsyncResult paramAsyncResult);

    protected abstract void hangupAndPowerOff();

    public abstract boolean isConcurrentVoiceAndDataAllowed();

    protected abstract void log(String paramString);

    protected abstract void loge(String paramString);

    // ERROR //
    public void powerOffRadioSafely(DataConnectionTracker paramDataConnectionTracker)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 210	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOff	Z
        //     6: ifne +28 -> 34
        //     9: aload_1
        //     10: invokevirtual 336	com/android/internal/telephony/DataConnectionTracker:isDisconnected	()Z
        //     13: ifeq +24 -> 37
        //     16: aload_1
        //     17: ldc_w 338
        //     20: invokevirtual 341	com/android/internal/telephony/DataConnectionTracker:cleanUpAllConnections	(Ljava/lang/String;)V
        //     23: aload_0
        //     24: ldc_w 343
        //     27: invokevirtual 313	com/android/internal/telephony/ServiceStateTracker:log	(Ljava/lang/String;)V
        //     30: aload_0
        //     31: invokevirtual 321	com/android/internal/telephony/ServiceStateTracker:hangupAndPowerOff	()V
        //     34: aload_0
        //     35: monitorexit
        //     36: return
        //     37: aload_1
        //     38: ldc_w 338
        //     41: invokevirtual 341	com/android/internal/telephony/DataConnectionTracker:cleanUpAllConnections	(Ljava/lang/String;)V
        //     44: aload_0
        //     45: invokestatic 347	android/os/Message:obtain	(Landroid/os/Handler;)Landroid/os/Message;
        //     48: astore_3
        //     49: aload_3
        //     50: bipush 38
        //     52: putfield 308	android/os/Message:what	I
        //     55: iconst_1
        //     56: aload_0
        //     57: getfield 212	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOffTag	I
        //     60: iadd
        //     61: istore 4
        //     63: aload_0
        //     64: iload 4
        //     66: putfield 212	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOffTag	I
        //     69: aload_3
        //     70: iload 4
        //     72: putfield 316	android/os/Message:arg1	I
        //     75: aload_0
        //     76: aload_3
        //     77: ldc2_w 348
        //     80: invokevirtual 353	com/android/internal/telephony/ServiceStateTracker:sendMessageDelayed	(Landroid/os/Message;J)Z
        //     83: ifeq +23 -> 106
        //     86: aload_0
        //     87: ldc_w 355
        //     90: invokevirtual 313	com/android/internal/telephony/ServiceStateTracker:log	(Ljava/lang/String;)V
        //     93: aload_0
        //     94: iconst_1
        //     95: putfield 210	com/android/internal/telephony/ServiceStateTracker:mPendingRadioPowerOffAfterDataOff	Z
        //     98: goto -64 -> 34
        //     101: astore_2
        //     102: aload_0
        //     103: monitorexit
        //     104: aload_2
        //     105: athrow
        //     106: aload_0
        //     107: ldc_w 357
        //     110: invokevirtual 313	com/android/internal/telephony/ServiceStateTracker:log	(Ljava/lang/String;)V
        //     113: aload_0
        //     114: invokevirtual 321	com/android/internal/telephony/ServiceStateTracker:hangupAndPowerOff	()V
        //     117: goto -83 -> 34
        //
        // Exception table:
        //     from	to	target	type
        //     2	104	101	finally
        //     106	117	101	finally
    }

    public boolean processPendingRadioPowerOffAfterDataOff()
    {
        boolean bool = false;
        try
        {
            if (this.mPendingRadioPowerOffAfterDataOff)
            {
                log("Process pending request to turn radio off.");
                this.mPendingRadioPowerOffAfterDataOffTag = (1 + this.mPendingRadioPowerOffAfterDataOffTag);
                hangupAndPowerOff();
                this.mPendingRadioPowerOffAfterDataOff = false;
                bool = true;
            }
            else;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
        return bool;
    }

    public void reRegisterNetwork(Message paramMessage)
    {
        this.cm.getPreferredNetworkType(obtainMessage(19, paramMessage));
    }

    public void registerForDataConnectionAttached(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mAttachedRegistrants.add(localRegistrant);
        if (getCurrentDataConnectionState() == 0)
            localRegistrant.notifyRegistrant();
    }

    public void registerForDataConnectionDetached(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mDetachedRegistrants.add(localRegistrant);
        if (getCurrentDataConnectionState() != 0)
            localRegistrant.notifyRegistrant();
    }

    public void registerForNetworkAttached(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mNetworkAttachedRegistrants.add(localRegistrant);
        if (this.ss.getState() == 0)
            localRegistrant.notifyRegistrant();
    }

    public void registerForPsRestrictedDisabled(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mPsRestrictDisabledRegistrants.add(localRegistrant);
        if (this.mRestrictedState.isPsRestricted())
            localRegistrant.notifyRegistrant();
    }

    public void registerForPsRestrictedEnabled(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mPsRestrictEnabledRegistrants.add(localRegistrant);
        if (this.mRestrictedState.isPsRestricted())
            localRegistrant.notifyRegistrant();
    }

    public void registerForRoamingOff(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mRoamingOffRegistrants.add(localRegistrant);
        if (!this.ss.getRoaming())
            localRegistrant.notifyRegistrant();
    }

    public void registerForRoamingOn(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mRoamingOnRegistrants.add(localRegistrant);
        if (this.ss.getRoaming())
            localRegistrant.notifyRegistrant();
    }

    protected abstract void setPowerStateToDesired();

    public void setRadioPower(boolean paramBoolean)
    {
        this.mDesiredPowerState = paramBoolean;
        setPowerStateToDesired();
    }

    protected boolean shouldFixTimeZoneNow(PhoneBase paramPhoneBase, String paramString1, String paramString2, boolean paramBoolean)
    {
        boolean bool1 = true;
        try
        {
            i = Integer.parseInt(paramString1.substring(0, 3));
        }
        catch (Exception localException1)
        {
            try
            {
                int k = Integer.parseInt(paramString2.substring(0, 3));
                j = k;
                localIccCard = paramPhoneBase.getIccCard();
                if ((localIccCard != null) && (localIccCard.getState().iccCardExist()))
                {
                    bool2 = bool1;
                    if (((!bool2) || (i == j)) && (!paramBoolean))
                        break label274;
                    long l = System.currentTimeMillis();
                    StringBuilder localStringBuilder = new StringBuilder().append("shouldFixTimeZoneNow: retVal=").append(bool1).append(" iccCard=").append(localIccCard).append(" iccCard.state=");
                    if (localIccCard != null)
                        break label280;
                    str = "null";
                    log(str + " iccCardExist=" + bool2 + " operatorNumeric=" + paramString1 + " mcc=" + i + " prevOperatorNumeric=" + paramString2 + " prevMcc=" + j + " needToFixTimeZone=" + paramBoolean + " ltod=" + TimeUtils.logTimeOfDay(l));
                    while (true)
                    {
                        return bool1;
                        localException1 = localException1;
                        log("shouldFixTimeZoneNow: no mcc, operatorNumeric=" + paramString1 + " retVal=false");
                        bool1 = false;
                    }
                }
            }
            catch (Exception localException2)
            {
                while (true)
                {
                    int i;
                    IccCard localIccCard;
                    int j = i + 1;
                    continue;
                    boolean bool2 = false;
                    continue;
                    label274: bool1 = false;
                    continue;
                    label280: String str = localIccCard.getState().toString();
                }
            }
        }
    }

    public void unregisterForDataConnectionAttached(Handler paramHandler)
    {
        this.mAttachedRegistrants.remove(paramHandler);
    }

    public void unregisterForDataConnectionDetached(Handler paramHandler)
    {
        this.mDetachedRegistrants.remove(paramHandler);
    }

    public void unregisterForNetworkAttached(Handler paramHandler)
    {
        this.mNetworkAttachedRegistrants.remove(paramHandler);
    }

    public void unregisterForPsRestrictedDisabled(Handler paramHandler)
    {
        this.mPsRestrictDisabledRegistrants.remove(paramHandler);
    }

    public void unregisterForPsRestrictedEnabled(Handler paramHandler)
    {
        this.mPsRestrictEnabledRegistrants.remove(paramHandler);
    }

    public void unregisterForRoamingOff(Handler paramHandler)
    {
        this.mRoamingOffRegistrants.remove(paramHandler);
    }

    public void unregisterForRoamingOn(Handler paramHandler)
    {
        this.mRoamingOnRegistrants.remove(paramHandler);
    }

    protected abstract void updateSpnDisplay();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.ServiceStateTracker
 * JD-Core Version:        0.6.2
 */