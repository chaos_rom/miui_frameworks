package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.text.TextUtils;

public abstract interface IExtendedNetworkService extends IInterface
{
    public abstract void clearMmiString()
        throws RemoteException;

    public abstract CharSequence getMmiRunningText()
        throws RemoteException;

    public abstract CharSequence getUserMessage(CharSequence paramCharSequence)
        throws RemoteException;

    public abstract void setMmiString(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IExtendedNetworkService
    {
        private static final String DESCRIPTOR = "com.android.internal.telephony.IExtendedNetworkService";
        static final int TRANSACTION_clearMmiString = 4;
        static final int TRANSACTION_getMmiRunningText = 2;
        static final int TRANSACTION_getUserMessage = 3;
        static final int TRANSACTION_setMmiString = 1;

        public Stub()
        {
            attachInterface(this, "com.android.internal.telephony.IExtendedNetworkService");
        }

        public static IExtendedNetworkService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.telephony.IExtendedNetworkService");
                if ((localIInterface != null) && ((localIInterface instanceof IExtendedNetworkService)))
                    localObject = (IExtendedNetworkService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("com.android.internal.telephony.IExtendedNetworkService");
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IExtendedNetworkService");
                setMmiString(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("com.android.internal.telephony.IExtendedNetworkService");
                CharSequence localCharSequence3 = getMmiRunningText();
                paramParcel2.writeNoException();
                if (localCharSequence3 != null)
                {
                    paramParcel2.writeInt(i);
                    TextUtils.writeToParcel(localCharSequence3, paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IExtendedNetworkService");
                    if (paramParcel1.readInt() != 0);
                    for (CharSequence localCharSequence1 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel1); ; localCharSequence1 = null)
                    {
                        CharSequence localCharSequence2 = getUserMessage(localCharSequence1);
                        paramParcel2.writeNoException();
                        if (localCharSequence2 == null)
                            break label213;
                        paramParcel2.writeInt(i);
                        TextUtils.writeToParcel(localCharSequence2, paramParcel2, i);
                        break;
                    }
                    label213: paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IExtendedNetworkService");
                    clearMmiString();
                    paramParcel2.writeNoException();
                }
            }
        }

        private static class Proxy
            implements IExtendedNetworkService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void clearMmiString()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IExtendedNetworkService");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.telephony.IExtendedNetworkService";
            }

            public CharSequence getMmiRunningText()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IExtendedNetworkService");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localCharSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(localParcel2);
                        return localCharSequence;
                    }
                    CharSequence localCharSequence = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public CharSequence getUserMessage(CharSequence paramCharSequence)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.telephony.IExtendedNetworkService");
                        if (paramCharSequence != null)
                        {
                            localParcel1.writeInt(1);
                            TextUtils.writeToParcel(paramCharSequence, localParcel1, 0);
                            this.mRemote.transact(3, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localCharSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(localParcel2);
                                return localCharSequence;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    CharSequence localCharSequence = null;
                }
            }

            public void setMmiString(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.IExtendedNetworkService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IExtendedNetworkService
 * JD-Core Version:        0.6.2
 */