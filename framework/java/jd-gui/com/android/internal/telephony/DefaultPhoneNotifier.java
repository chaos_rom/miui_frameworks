package com.android.internal.telephony;

import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;

public class DefaultPhoneNotifier
    implements PhoneNotifier
{
    private static final boolean DBG = true;
    static final String LOG_TAG = "GSM";
    private ITelephonyRegistry mRegistry = ITelephonyRegistry.Stub.asInterface(ServiceManager.getService("telephony.registry"));

    public static int convertCallState(Phone.State paramState)
    {
        int i;
        switch (1.$SwitchMap$com$android$internal$telephony$Phone$State[paramState.ordinal()])
        {
        default:
            i = 0;
        case 1:
        case 2:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 2;
        }
    }

    public static Phone.State convertCallState(int paramInt)
    {
        Phone.State localState;
        switch (paramInt)
        {
        default:
            localState = Phone.State.IDLE;
        case 1:
        case 2:
        }
        while (true)
        {
            return localState;
            localState = Phone.State.RINGING;
            continue;
            localState = Phone.State.OFFHOOK;
        }
    }

    public static int convertDataActivityState(Phone.DataActivityState paramDataActivityState)
    {
        int i;
        switch (1.$SwitchMap$com$android$internal$telephony$Phone$DataActivityState[paramDataActivityState.ordinal()])
        {
        default:
            i = 0;
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 3;
            continue;
            i = 4;
        }
    }

    public static Phone.DataActivityState convertDataActivityState(int paramInt)
    {
        Phone.DataActivityState localDataActivityState;
        switch (paramInt)
        {
        default:
            localDataActivityState = Phone.DataActivityState.NONE;
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return localDataActivityState;
            localDataActivityState = Phone.DataActivityState.DATAIN;
            continue;
            localDataActivityState = Phone.DataActivityState.DATAOUT;
            continue;
            localDataActivityState = Phone.DataActivityState.DATAINANDOUT;
            continue;
            localDataActivityState = Phone.DataActivityState.DORMANT;
        }
    }

    public static int convertDataState(Phone.DataState paramDataState)
    {
        int i;
        switch (1.$SwitchMap$com$android$internal$telephony$Phone$DataState[paramDataState.ordinal()])
        {
        default:
            i = 0;
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 3;
        }
    }

    public static Phone.DataState convertDataState(int paramInt)
    {
        Phone.DataState localDataState;
        switch (paramInt)
        {
        default:
            localDataState = Phone.DataState.DISCONNECTED;
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return localDataState;
            localDataState = Phone.DataState.CONNECTING;
            continue;
            localDataState = Phone.DataState.CONNECTED;
            continue;
            localDataState = Phone.DataState.SUSPENDED;
        }
    }

    private void doNotifyDataConnection(Phone paramPhone, String paramString1, String paramString2, Phone.DataState paramDataState)
    {
        TelephonyManager localTelephonyManager = TelephonyManager.getDefault();
        LinkProperties localLinkProperties = null;
        LinkCapabilities localLinkCapabilities = null;
        boolean bool1 = false;
        if (paramDataState == Phone.DataState.CONNECTED)
        {
            localLinkProperties = paramPhone.getLinkProperties(paramString2);
            localLinkCapabilities = paramPhone.getLinkCapabilities(paramString2);
        }
        ServiceState localServiceState = paramPhone.getServiceState();
        if (localServiceState != null)
            bool1 = localServiceState.getRoaming();
        try
        {
            ITelephonyRegistry localITelephonyRegistry = this.mRegistry;
            int i = convertDataState(paramDataState);
            boolean bool2 = paramPhone.isDataConnectivityPossible(paramString2);
            String str = paramPhone.getActiveApnHost(paramString2);
            if (localTelephonyManager != null);
            for (int j = localTelephonyManager.getNetworkType(); ; j = 0)
            {
                localITelephonyRegistry.notifyDataConnection(i, bool2, paramString1, str, paramString2, localLinkProperties, localLinkCapabilities, j, bool1);
                label126: return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label126;
        }
    }

    private void log(String paramString)
    {
        Log.d("GSM", "[PhoneNotifier] " + paramString);
    }

    public void notifyCallForwardingChanged(Phone paramPhone)
    {
        try
        {
            this.mRegistry.notifyCallForwardingChanged(paramPhone.getCallForwardingIndicator());
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    public void notifyCellInfo(Phone paramPhone, CellInfo paramCellInfo)
    {
        try
        {
            this.mRegistry.notifyCellInfo(paramCellInfo);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void notifyCellLocation(Phone paramPhone)
    {
        Bundle localBundle = new Bundle();
        paramPhone.getCellLocation().fillInNotifierBundle(localBundle);
        try
        {
            this.mRegistry.notifyCellLocation(localBundle);
            label28: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label28;
        }
    }

    public void notifyDataActivity(Phone paramPhone)
    {
        try
        {
            this.mRegistry.notifyDataActivity(convertDataActivityState(paramPhone.getDataActivityState()));
            label18: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }

    public void notifyDataConnection(Phone paramPhone, String paramString1, String paramString2, Phone.DataState paramDataState)
    {
        doNotifyDataConnection(paramPhone, paramString1, paramString2, paramDataState);
    }

    public void notifyDataConnectionFailed(Phone paramPhone, String paramString1, String paramString2)
    {
        try
        {
            this.mRegistry.notifyDataConnectionFailed(paramString1, paramString2);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void notifyMessageWaitingChanged(Phone paramPhone)
    {
        try
        {
            this.mRegistry.notifyMessageWaitingChanged(paramPhone.getMessageWaitingIndicator());
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    public void notifyOtaspChanged(Phone paramPhone, int paramInt)
    {
        try
        {
            this.mRegistry.notifyOtaspChanged(paramInt);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void notifyPhoneState(Phone paramPhone)
    {
        Call localCall = paramPhone.getRingingCall();
        String str = "";
        if ((localCall != null) && (localCall.getEarliestConnection() != null))
            str = localCall.getEarliestConnection().getAddress();
        try
        {
            this.mRegistry.notifyCallState(convertCallState(paramPhone.getState()), str);
            label48: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label48;
        }
    }

    public void notifyServiceState(Phone paramPhone)
    {
        ServiceState localServiceState = paramPhone.getServiceState();
        if (localServiceState == null)
        {
            localServiceState = new ServiceState();
            localServiceState.setStateOutOfService();
        }
        try
        {
            this.mRegistry.notifyServiceState(localServiceState);
            label33: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label33;
        }
    }

    public void notifySignalStrength(Phone paramPhone)
    {
        try
        {
            this.mRegistry.notifySignalStrength(paramPhone.getSignalStrength());
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.DefaultPhoneNotifier
 * JD-Core Version:        0.6.2
 */