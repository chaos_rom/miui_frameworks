package com.android.internal.telephony;

public class IccIoResult
{
    public byte[] payload;
    public int sw1;
    public int sw2;

    public IccIoResult(int paramInt1, int paramInt2, String paramString)
    {
        this(paramInt1, paramInt2, IccUtils.hexStringToBytes(paramString));
    }

    public IccIoResult(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        this.sw1 = paramInt1;
        this.sw2 = paramInt2;
        this.payload = paramArrayOfByte;
    }

    public IccException getException()
    {
        Object localObject;
        if (success())
            localObject = null;
        while (true)
        {
            return localObject;
            switch (this.sw1)
            {
            default:
                localObject = new IccException("sw1:" + this.sw1 + " sw2:" + this.sw2);
                break;
            case 148:
                if (this.sw2 == 8)
                    localObject = new IccFileTypeMismatch();
                else
                    localObject = new IccFileNotFound();
                break;
            }
        }
    }

    public boolean success()
    {
        if ((this.sw1 == 144) || (this.sw1 == 145) || (this.sw1 == 158) || (this.sw1 == 159));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toString()
    {
        return "IccIoResponse sw1:0x" + Integer.toHexString(this.sw1) + " sw2:0x" + Integer.toHexString(this.sw2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccIoResult
 * JD-Core Version:        0.6.2
 */