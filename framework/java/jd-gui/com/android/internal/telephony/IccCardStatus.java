package com.android.internal.telephony;

import java.util.ArrayList;

public class IccCardStatus
{
    public static final int CARD_MAX_APPS = 8;
    private ArrayList<IccCardApplication> mApplications = new ArrayList(8);
    private CardState mCardState;
    private int mCdmaSubscriptionAppIndex;
    private int mGsmUmtsSubscriptionAppIndex;
    private int mImsSubscriptionAppIndex;
    private int mNumApplications;
    private PinState mUniversalPinState;

    public void addApplication(IccCardApplication paramIccCardApplication)
    {
        this.mApplications.add(paramIccCardApplication);
    }

    public IccCardApplication getApplication(int paramInt)
    {
        return (IccCardApplication)this.mApplications.get(paramInt);
    }

    public CardState getCardState()
    {
        return this.mCardState;
    }

    public int getCdmaSubscriptionAppIndex()
    {
        return this.mCdmaSubscriptionAppIndex;
    }

    public int getGsmUmtsSubscriptionAppIndex()
    {
        return this.mGsmUmtsSubscriptionAppIndex;
    }

    public int getImsSubscriptionAppIndex()
    {
        return this.mImsSubscriptionAppIndex;
    }

    public int getNumApplications()
    {
        return this.mNumApplications;
    }

    public PinState getUniversalPinState()
    {
        return this.mUniversalPinState;
    }

    public void setCardState(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Unrecognized RIL_CardState: " + paramInt);
        case 0:
            this.mCardState = CardState.CARDSTATE_ABSENT;
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            this.mCardState = CardState.CARDSTATE_PRESENT;
            continue;
            this.mCardState = CardState.CARDSTATE_ERROR;
        }
    }

    public void setCdmaSubscriptionAppIndex(int paramInt)
    {
        this.mCdmaSubscriptionAppIndex = paramInt;
    }

    public void setGsmUmtsSubscriptionAppIndex(int paramInt)
    {
        this.mGsmUmtsSubscriptionAppIndex = paramInt;
    }

    public void setImsSubscriptionAppIndex(int paramInt)
    {
        this.mImsSubscriptionAppIndex = paramInt;
    }

    public void setNumApplications(int paramInt)
    {
        this.mNumApplications = paramInt;
    }

    public void setUniversalPinState(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Unrecognized RIL_PinState: " + paramInt);
        case 0:
            this.mUniversalPinState = PinState.PINSTATE_UNKNOWN;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return;
            this.mUniversalPinState = PinState.PINSTATE_ENABLED_NOT_VERIFIED;
            continue;
            this.mUniversalPinState = PinState.PINSTATE_ENABLED_VERIFIED;
            continue;
            this.mUniversalPinState = PinState.PINSTATE_DISABLED;
            continue;
            this.mUniversalPinState = PinState.PINSTATE_ENABLED_BLOCKED;
            continue;
            this.mUniversalPinState = PinState.PINSTATE_ENABLED_PERM_BLOCKED;
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("IccCardState {").append(this.mCardState).append(",").append(this.mUniversalPinState).append(",num_apps=").append(this.mNumApplications).append(",gsm_id=").append(this.mGsmUmtsSubscriptionAppIndex);
        if ((this.mGsmUmtsSubscriptionAppIndex >= 0) && (this.mGsmUmtsSubscriptionAppIndex < 8))
        {
            Object localObject2 = getApplication(this.mGsmUmtsSubscriptionAppIndex);
            if (localObject2 == null)
                localObject2 = "null";
            localStringBuilder.append(localObject2);
        }
        localStringBuilder.append(",cmda_id=").append(this.mCdmaSubscriptionAppIndex);
        if ((this.mCdmaSubscriptionAppIndex >= 0) && (this.mCdmaSubscriptionAppIndex < 8))
        {
            Object localObject1 = getApplication(this.mCdmaSubscriptionAppIndex);
            if (localObject1 == null)
                localObject1 = "null";
            localStringBuilder.append(localObject1);
        }
        localStringBuilder.append(",ism_id=").append(this.mImsSubscriptionAppIndex);
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }

    public static enum PinState
    {
        static
        {
            PINSTATE_ENABLED_NOT_VERIFIED = new PinState("PINSTATE_ENABLED_NOT_VERIFIED", 1);
            PINSTATE_ENABLED_VERIFIED = new PinState("PINSTATE_ENABLED_VERIFIED", 2);
            PINSTATE_DISABLED = new PinState("PINSTATE_DISABLED", 3);
            PINSTATE_ENABLED_BLOCKED = new PinState("PINSTATE_ENABLED_BLOCKED", 4);
            PINSTATE_ENABLED_PERM_BLOCKED = new PinState("PINSTATE_ENABLED_PERM_BLOCKED", 5);
            PinState[] arrayOfPinState = new PinState[6];
            arrayOfPinState[0] = PINSTATE_UNKNOWN;
            arrayOfPinState[1] = PINSTATE_ENABLED_NOT_VERIFIED;
            arrayOfPinState[2] = PINSTATE_ENABLED_VERIFIED;
            arrayOfPinState[3] = PINSTATE_DISABLED;
            arrayOfPinState[4] = PINSTATE_ENABLED_BLOCKED;
            arrayOfPinState[5] = PINSTATE_ENABLED_PERM_BLOCKED;
        }

        boolean isPermBlocked()
        {
            if (this == PINSTATE_ENABLED_PERM_BLOCKED);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean isPinRequired()
        {
            if (this == PINSTATE_ENABLED_NOT_VERIFIED);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean isPukRequired()
        {
            if (this == PINSTATE_ENABLED_BLOCKED);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    public static enum CardState
    {
        static
        {
            CARDSTATE_ERROR = new CardState("CARDSTATE_ERROR", 2);
            CardState[] arrayOfCardState = new CardState[3];
            arrayOfCardState[0] = CARDSTATE_ABSENT;
            arrayOfCardState[1] = CARDSTATE_PRESENT;
            arrayOfCardState[2] = CARDSTATE_ERROR;
        }

        boolean isCardPresent()
        {
            if (this == CARDSTATE_PRESENT);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccCardStatus
 * JD-Core Version:        0.6.2
 */