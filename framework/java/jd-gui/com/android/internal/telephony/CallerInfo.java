package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Country;
import android.location.CountryDetector;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.android.i18n.phonenumbers.NumberParseException;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import com.android.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.android.i18n.phonenumbers.geocoding.PhoneNumberOfflineGeocoder;
import java.util.Locale;
import miui.telephony.ExtraCallerInfo;

public class CallerInfo
{
    public static final String PAYPHONE_NUMBER = "-3";
    public static final String PRIVATE_NUMBER = "-2";
    private static final String TAG = "CallerInfo";
    public static final String UNKNOWN_NUMBER = "-1";
    private static final boolean VDBG = Log.isLoggable("CallerInfo", 2);
    public Drawable cachedPhoto;
    public Bitmap cachedPhotoIcon;
    public String cnapName;
    public boolean contactExists;
    public Uri contactRefUri;
    public Uri contactRingtoneUri;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public ExtraCallerInfo extra = new ExtraCallerInfo();
    public String geoDescription;
    public boolean isCachedPhotoCurrent;
    private boolean mIsEmergency = false;
    private boolean mIsVoiceMail = false;
    public String name;
    public int namePresentation;
    public boolean needUpdate;
    public String normalizedNumber;
    public String numberLabel;
    public int numberPresentation;
    public int numberType;
    public long person_id;
    public String phoneLabel;
    public String phoneNumber;
    public int photoResource;
    public boolean shouldSendToVoicemail;

    static CallerInfo doSecondaryLookupIfNecessary(Context paramContext, String paramString, CallerInfo paramCallerInfo)
    {
        if ((!paramCallerInfo.contactExists) && (PhoneNumberUtils.isUriNumber(paramString)))
        {
            String str = PhoneNumberUtils.getUsernameFromUriNumber(paramString);
            if (PhoneNumberUtils.isGlobalPhoneNumber(str))
                paramCallerInfo = getCallerInfo(paramContext, Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)));
        }
        return paramCallerInfo;
    }

    public static String getCallerId(Context paramContext, String paramString)
    {
        CallerInfo localCallerInfo = getCallerInfo(paramContext, paramString);
        Object localObject = null;
        String str;
        if (localCallerInfo != null)
        {
            str = localCallerInfo.name;
            if (TextUtils.isEmpty(str))
                break label31;
        }
        label31: for (localObject = str; ; localObject = paramString)
            return localObject;
    }

    public static CallerInfo getCallerInfo(Context paramContext, Uri paramUri)
    {
        return getCallerInfo(paramContext, paramUri, paramContext.getContentResolver().query(paramUri, null, null, null, null));
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static CallerInfo getCallerInfo(Context paramContext, Uri paramUri, Cursor paramCursor)
    {
        CallerInfo localCallerInfo = new CallerInfo();
        localCallerInfo.photoResource = 0;
        localCallerInfo.phoneLabel = null;
        localCallerInfo.numberType = 0;
        localCallerInfo.numberLabel = null;
        localCallerInfo.cachedPhoto = null;
        localCallerInfo.isCachedPhotoCurrent = false;
        localCallerInfo.contactExists = false;
        if (VDBG)
            Log.v("CallerInfo", "getCallerInfo() based on cursor...");
        if (paramCursor != null)
            if (paramCursor.moveToFirst())
            {
                int i = paramCursor.getColumnIndex("display_name");
                if (i != -1)
                    localCallerInfo.name = paramCursor.getString(i);
                Injector.setContactRef(paramUri);
                int j = Injector.getColumnIndex(paramCursor, "number");
                if (j != -1)
                    localCallerInfo.phoneNumber = paramCursor.getString(j);
                int k = Injector.getColumnIndex(paramCursor, "normalized_number");
                if (k != -1)
                    localCallerInfo.normalizedNumber = paramCursor.getString(k);
                int m = Injector.getColumnIndex(paramCursor, "label");
                if (m != -1)
                {
                    int i3 = Injector.getColumnIndex(paramCursor, "type");
                    if (i3 != -1)
                    {
                        localCallerInfo.numberType = paramCursor.getInt(i3);
                        localCallerInfo.numberLabel = paramCursor.getString(m);
                        localCallerInfo.phoneLabel = ContactsContract.CommonDataKinds.Phone.getDisplayLabel(paramContext, localCallerInfo.numberType, localCallerInfo.numberLabel).toString();
                    }
                }
                int n = getColumnIndexForPersonId(paramUri, paramCursor);
                if (n == -1)
                    break label415;
                localCallerInfo.person_id = paramCursor.getLong(n);
                if (VDBG)
                    Log.v("CallerInfo", "==> got info.person_id: " + localCallerInfo.person_id);
                int i1 = paramCursor.getColumnIndex("custom_ringtone");
                if ((i1 == -1) || (paramCursor.getString(i1) == null))
                    break label444;
                localCallerInfo.contactRingtoneUri = Uri.parse(paramCursor.getString(i1));
                label333: int i2 = paramCursor.getColumnIndex("send_to_voicemail");
                if ((i2 == -1) || (paramCursor.getInt(i2) != 1))
                    break label452;
            }
        label415: label444: label452: for (boolean bool = true; ; bool = false)
        {
            localCallerInfo.shouldSendToVoicemail = bool;
            localCallerInfo.contactExists = true;
            localCallerInfo.extra = ExtraCallerInfo.getExtraCallerInfo(paramContext, localCallerInfo, paramCursor);
            paramCursor.close();
            localCallerInfo.needUpdate = false;
            localCallerInfo.name = normalize(localCallerInfo.name);
            localCallerInfo.contactRefUri = paramUri;
            return localCallerInfo;
            Log.w("CallerInfo", "Couldn't find person_id column for " + paramUri);
            break;
            localCallerInfo.contactRingtoneUri = null;
            break label333;
        }
    }

    public static CallerInfo getCallerInfo(Context paramContext, String paramString)
    {
        if (VDBG)
            Log.v("CallerInfo", "getCallerInfo() based on number...");
        CallerInfo localCallerInfo;
        if (TextUtils.isEmpty(paramString))
            localCallerInfo = null;
        while (true)
        {
            return localCallerInfo;
            if (PhoneNumberUtils.isLocalEmergencyNumber(paramString, paramContext))
            {
                localCallerInfo = new CallerInfo().markAsEmergency(paramContext);
            }
            else if (PhoneNumberUtils.isVoiceMailNumber(paramString))
            {
                localCallerInfo = new CallerInfo().markAsVoiceMail();
            }
            else
            {
                localCallerInfo = doSecondaryLookupIfNecessary(paramContext, paramString, getCallerInfo(paramContext, Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(paramString))));
                if (TextUtils.isEmpty(localCallerInfo.phoneNumber))
                    localCallerInfo.phoneNumber = paramString;
            }
        }
    }

    private static int getColumnIndexForPersonId(Uri paramUri, Cursor paramCursor)
    {
        if (VDBG)
            Log.v("CallerInfo", "- getColumnIndexForPersonId: contactRef URI = '" + paramUri + "'...");
        String str1 = paramUri.toString();
        String str2 = null;
        if (str1.startsWith("content://com.android.contacts/data/phones"))
        {
            if (VDBG)
                Log.v("CallerInfo", "'data/phones' URI; using RawContacts.CONTACT_ID");
            str2 = "contact_id";
            if (str2 == null)
                break label238;
        }
        label238: for (int i = paramCursor.getColumnIndex(str2); ; i = -1)
        {
            if (VDBG)
                Log.v("CallerInfo", "==> Using column '" + str2 + "' (columnIndex = " + i + ") for person_id lookup...");
            return i;
            if (str1.startsWith("content://com.android.contacts/data"))
            {
                if (VDBG)
                    Log.v("CallerInfo", "'data' URI; using Data.CONTACT_ID");
                str2 = "contact_id";
                break;
            }
            if (str1.startsWith("content://com.android.contacts/phone_lookup"))
            {
                if (VDBG)
                    Log.v("CallerInfo", "'phone_lookup' URI; using PhoneLookup._ID");
                str2 = "_id";
                break;
            }
            Log.w("CallerInfo", "Unexpected prefix for contactRef '" + str1 + "'");
            break;
        }
    }

    private static String getCurrentCountryIso(Context paramContext, Locale paramLocale)
    {
        CountryDetector localCountryDetector = (CountryDetector)paramContext.getSystemService("country_detector");
        String str;
        if (localCountryDetector != null)
            str = localCountryDetector.detectCountry().getCountryIso();
        while (true)
        {
            return str;
            str = paramLocale.getCountry();
            Log.w("CallerInfo", "No CountryDetector; falling back to countryIso based on locale: " + str);
        }
    }

    private static String getGeoDescription(Context paramContext, String paramString)
    {
        String str1 = null;
        if (VDBG)
            Log.v("CallerInfo", "getGeoDescription('" + paramString + "')...");
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return str1;
            PhoneNumberUtil localPhoneNumberUtil = PhoneNumberUtil.getInstance();
            PhoneNumberOfflineGeocoder localPhoneNumberOfflineGeocoder = PhoneNumberOfflineGeocoder.getInstance();
            Locale localLocale = paramContext.getResources().getConfiguration().locale;
            String str2 = getCurrentCountryIso(paramContext, localLocale);
            Phonenumber.PhoneNumber localPhoneNumber = null;
            try
            {
                if (VDBG)
                    Log.v("CallerInfo", "parsing '" + paramString + "' for countryIso '" + str2 + "'...");
                localPhoneNumber = localPhoneNumberUtil.parse(paramString, str2);
                if (VDBG)
                    Log.v("CallerInfo", "- parsed number: " + localPhoneNumber);
                if (localPhoneNumber == null)
                    continue;
                str1 = localPhoneNumberOfflineGeocoder.getDescriptionForNumber(localPhoneNumber, localLocale);
                if (!VDBG)
                    continue;
                Log.v("CallerInfo", "- got description: '" + str1 + "'");
            }
            catch (NumberParseException localNumberParseException)
            {
                while (true)
                    Log.w("CallerInfo", "getGeoDescription: NumberParseException for incoming number '" + paramString + "'");
            }
        }
    }

    private static String normalize(String paramString)
    {
        if ((paramString == null) || (paramString.length() > 0));
        while (true)
        {
            return paramString;
            paramString = null;
        }
    }

    public boolean isEmergencyNumber()
    {
        return this.mIsEmergency;
    }

    public boolean isVoiceMailNumber()
    {
        return this.mIsVoiceMail;
    }

    CallerInfo markAsEmergency(Context paramContext)
    {
        this.phoneNumber = paramContext.getString(17040117);
        this.photoResource = 17302609;
        this.mIsEmergency = true;
        return this;
    }

    CallerInfo markAsVoiceMail()
    {
        this.mIsVoiceMail = true;
        try
        {
            this.phoneNumber = TelephonyManager.getDefault().getVoiceMailAlphaTag();
            return this;
        }
        catch (SecurityException localSecurityException)
        {
            while (true)
                Log.e("CallerInfo", "Cannot access VoiceMail.", localSecurityException);
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder1 = new StringBuilder(128).append(super.toString() + " { ");
        StringBuilder localStringBuilder2 = new StringBuilder().append("name ");
        String str1;
        StringBuilder localStringBuilder3;
        StringBuilder localStringBuilder4;
        if (this.name == null)
        {
            str1 = "null";
            localStringBuilder3 = localStringBuilder1.append(str1);
            localStringBuilder4 = new StringBuilder().append(", phoneNumber ");
            if (this.phoneNumber != null)
                break label135;
        }
        label135: for (String str2 = "null"; ; str2 = "non-null")
        {
            return localStringBuilder4.append(str2).toString() + " }";
            str1 = "non-null";
            break;
        }
    }

    public void updateGeoDescription(Context paramContext, String paramString)
    {
        if (TextUtils.isEmpty(this.phoneNumber));
        for (String str = paramString; ; str = this.phoneNumber)
        {
            this.geoDescription = getGeoDescription(paramContext, str);
            return;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        private static Uri mContactRef;

        static int getColumnIndex(Cursor paramCursor, String paramString)
        {
            return miui.telephony.CallerInfo.getColumnIndex(mContactRef, paramString, paramCursor);
        }

        static void setContactRef(Uri paramUri)
        {
            mContactRef = paramUri;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.CallerInfo
 * JD-Core Version:        0.6.2
 */