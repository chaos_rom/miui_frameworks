package com.android.internal.telephony;

import android.os.RemoteException;
import android.os.ServiceManager;
import java.util.List;

public class IccPhoneBookInterfaceManagerProxy extends IIccPhoneBook.Stub
{
    private IccPhoneBookInterfaceManager mIccPhoneBookInterfaceManager;

    public IccPhoneBookInterfaceManagerProxy(IccPhoneBookInterfaceManager paramIccPhoneBookInterfaceManager)
    {
        this.mIccPhoneBookInterfaceManager = paramIccPhoneBookInterfaceManager;
        if (ServiceManager.getService("simphonebook") == null)
            ServiceManager.addService("simphonebook", this);
    }

    public int getAdnCapacity()
    {
        return this.mIccPhoneBookInterfaceManager.getAdnCapacity();
    }

    public List<AdnRecord> getAdnRecordsInEf(int paramInt)
        throws RemoteException
    {
        return this.mIccPhoneBookInterfaceManager.getAdnRecordsInEf(paramInt);
    }

    public int[] getAdnRecordsSize(int paramInt)
        throws RemoteException
    {
        return this.mIccPhoneBookInterfaceManager.getAdnRecordsSize(paramInt);
    }

    public int getFreeAdn()
    {
        return this.mIccPhoneBookInterfaceManager.getFreeAdn();
    }

    public void setmIccPhoneBookInterfaceManager(IccPhoneBookInterfaceManager paramIccPhoneBookInterfaceManager)
    {
        this.mIccPhoneBookInterfaceManager = paramIccPhoneBookInterfaceManager;
    }

    public boolean updateAdnRecordsInEfByIndex(int paramInt1, String paramString1, String paramString2, int paramInt2, String paramString3)
        throws RemoteException
    {
        return this.mIccPhoneBookInterfaceManager.updateAdnRecordsInEfByIndex(paramInt1, paramString1, paramString2, paramInt2, paramString3);
    }

    public boolean updateAdnRecordsInEfBySearch(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
        throws RemoteException
    {
        return this.mIccPhoneBookInterfaceManager.updateAdnRecordsInEfBySearch(paramInt, paramString1, paramString2, paramString3, paramString4, paramString5);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccPhoneBookInterfaceManagerProxy
 * JD-Core Version:        0.6.2
 */