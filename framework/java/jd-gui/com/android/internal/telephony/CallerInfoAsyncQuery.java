package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.AsyncQueryHandler;
import android.content.AsyncQueryHandler.WorkerArgs;
import android.content.AsyncQueryHandler.WorkerHandler;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.location.Country;
import android.location.CountryDetector;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import miui.telephony.PhoneNumberUtils.PhoneNumber;

public class CallerInfoAsyncQuery
{
    private static final boolean DBG = false;
    private static final boolean ENABLE_UNKNOWN_NUMBER_GEO_DESCRIPTION = true;
    private static final int EVENT_ADD_LISTENER = 2;
    private static final int EVENT_EMERGENCY_NUMBER = 4;
    private static final int EVENT_END_OF_QUEUE = 3;
    private static final int EVENT_NEW_QUERY = 1;
    private static final int EVENT_VOICEMAIL_NUMBER = 5;
    private static final String LOG_TAG = "CallerInfoAsyncQuery";
    private CallerInfoAsyncQueryHandler mHandler;

    private void allocate(Context paramContext, Uri paramUri)
    {
        if ((paramContext == null) || (paramUri == null))
            throw new QueryPoolException("Bad context or query uri.");
        this.mHandler = new CallerInfoAsyncQueryHandler(paramContext, null);
        CallerInfoAsyncQueryHandler.access$302(this.mHandler, paramContext);
        CallerInfoAsyncQueryHandler.access$402(this.mHandler, paramUri);
    }

    private void release()
    {
        CallerInfoAsyncQueryHandler.access$302(this.mHandler, null);
        CallerInfoAsyncQueryHandler.access$402(this.mHandler, null);
        CallerInfoAsyncQueryHandler.access$502(this.mHandler, null);
        this.mHandler = null;
    }

    private static String sanitizeUriToString(Uri paramUri)
    {
        int i;
        if (paramUri != null)
        {
            str = paramUri.toString();
            i = str.lastIndexOf('/');
            if (i <= 0);
        }
        for (String str = str.substring(0, i) + "/xxxxxxx"; ; str = "")
            return str;
    }

    public static CallerInfoAsyncQuery startQuery(int paramInt, Context paramContext, Uri paramUri, OnQueryCompleteListener paramOnQueryCompleteListener, Object paramObject)
    {
        CallerInfoAsyncQuery localCallerInfoAsyncQuery = new CallerInfoAsyncQuery();
        localCallerInfoAsyncQuery.allocate(paramContext, paramUri);
        CookieWrapper localCookieWrapper = new CookieWrapper(null);
        localCookieWrapper.listener = paramOnQueryCompleteListener;
        localCookieWrapper.cookie = paramObject;
        localCookieWrapper.event = 1;
        localCallerInfoAsyncQuery.mHandler.startQuery(paramInt, localCookieWrapper, paramUri, null, null, null, null);
        return localCallerInfoAsyncQuery;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static CallerInfoAsyncQuery startQuery(int paramInt, Context paramContext, String paramString, OnQueryCompleteListener paramOnQueryCompleteListener, Object paramObject)
    {
        Uri localUri;
        String str;
        String[] arrayOfString;
        CallerInfoAsyncQuery localCallerInfoAsyncQuery;
        CookieWrapper localCookieWrapper;
        if (PhoneNumberUtils.isUriNumber(paramString))
        {
            localUri = ContactsContract.Data.CONTENT_URI;
            str = "upper(data1)=? AND mimetype='vnd.android.cursor.item/sip_address'";
            arrayOfString = new String[1];
            arrayOfString[0] = paramString.toUpperCase();
            localCallerInfoAsyncQuery = new CallerInfoAsyncQuery();
            localCallerInfoAsyncQuery.allocate(paramContext, localUri);
            localCookieWrapper = new CookieWrapper(null);
            localCookieWrapper.listener = paramOnQueryCompleteListener;
            localCookieWrapper.cookie = paramObject;
            localCookieWrapper.number = paramString;
            if (!PhoneNumberUtils.isLocalEmergencyNumber(paramString, paramContext))
                break label136;
            localCookieWrapper.event = 4;
        }
        while (true)
        {
            localCallerInfoAsyncQuery.mHandler.startQuery(paramInt, localCookieWrapper, localUri, null, str, arrayOfString, null);
            return localCallerInfoAsyncQuery;
            localUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(Injector.getPhoneNumber(paramString)));
            str = null;
            arrayOfString = null;
            break;
            label136: if (PhoneNumberUtils.isVoiceMailNumber(paramString))
                localCookieWrapper.event = 5;
            else
                localCookieWrapper.event = 1;
        }
    }

    public void addQueryListener(int paramInt, OnQueryCompleteListener paramOnQueryCompleteListener, Object paramObject)
    {
        CookieWrapper localCookieWrapper = new CookieWrapper(null);
        localCookieWrapper.listener = paramOnQueryCompleteListener;
        localCookieWrapper.cookie = paramObject;
        localCookieWrapper.event = 2;
        this.mHandler.startQuery(paramInt, localCookieWrapper, null, null, null, null, null);
    }

    private class CallerInfoAsyncQueryHandler extends AsyncQueryHandler
    {
        private CallerInfo mCallerInfo;
        private Context mQueryContext;
        private Uri mQueryUri;

        private CallerInfoAsyncQueryHandler(Context arg2)
        {
            super();
        }

        protected Handler createHandler(Looper paramLooper)
        {
            return new CallerInfoWorkerHandler(paramLooper);
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        protected void onQueryComplete(int paramInt, Object paramObject, Cursor paramCursor)
        {
            CallerInfoAsyncQuery.CookieWrapper localCookieWrapper1 = (CallerInfoAsyncQuery.CookieWrapper)paramObject;
            if (localCookieWrapper1 == null);
            while (true)
            {
                return;
                if (localCookieWrapper1.event != 3)
                    break;
                CallerInfoAsyncQuery.this.release();
            }
            if (this.mCallerInfo == null)
            {
                if ((this.mQueryContext == null) || (this.mQueryUri == null))
                    throw new CallerInfoAsyncQuery.QueryPoolException("Bad context or query uri, or CallerInfoAsyncQuery already released.");
                if (localCookieWrapper1.event != 4)
                    break label148;
                this.mCallerInfo = new CallerInfo().markAsEmergency(this.mQueryContext);
            }
            while (true)
            {
                CallerInfoAsyncQuery.CookieWrapper localCookieWrapper2 = new CallerInfoAsyncQuery.CookieWrapper(null);
                localCookieWrapper2.event = 3;
                startQuery(paramInt, localCookieWrapper2, null, null, null, null, null);
                if (localCookieWrapper1.listener == null)
                    break;
                localCookieWrapper1.listener.onQueryComplete(paramInt, localCookieWrapper1.cookie, this.mCallerInfo);
                break;
                label148: if (localCookieWrapper1.event == 5)
                {
                    this.mCallerInfo = new CallerInfo().markAsVoiceMail();
                }
                else
                {
                    this.mCallerInfo = CallerInfo.getCallerInfo(this.mQueryContext, this.mQueryUri, paramCursor);
                    CallerInfo localCallerInfo = CallerInfo.doSecondaryLookupIfNecessary(this.mQueryContext, localCookieWrapper1.number, this.mCallerInfo);
                    if (localCallerInfo != this.mCallerInfo)
                        this.mCallerInfo = localCallerInfo;
                    miui.telephony.CallerInfo.doSpNumberQuery(this.mQueryContext, localCookieWrapper1.number, this.mCallerInfo);
                    if (TextUtils.isEmpty(this.mCallerInfo.name))
                        this.mCallerInfo.updateGeoDescription(this.mQueryContext, localCookieWrapper1.number);
                    if (!TextUtils.isEmpty(localCookieWrapper1.number))
                    {
                        CountryDetector localCountryDetector = (CountryDetector)this.mQueryContext.getSystemService("country_detector");
                        this.mCallerInfo.phoneNumber = PhoneNumberUtils.formatNumber(localCookieWrapper1.number, this.mCallerInfo.normalizedNumber, localCountryDetector.detectCountry().getCountryIso());
                    }
                }
            }
        }

        protected class CallerInfoWorkerHandler extends AsyncQueryHandler.WorkerHandler
        {
            public CallerInfoWorkerHandler(Looper arg2)
            {
                super(localLooper);
            }

            public void handleMessage(Message paramMessage)
            {
                AsyncQueryHandler.WorkerArgs localWorkerArgs = (AsyncQueryHandler.WorkerArgs)paramMessage.obj;
                CallerInfoAsyncQuery.CookieWrapper localCookieWrapper = (CallerInfoAsyncQuery.CookieWrapper)localWorkerArgs.cookie;
                if (localCookieWrapper == null)
                    super.handleMessage(paramMessage);
                while (true)
                {
                    return;
                    switch (localCookieWrapper.event)
                    {
                    default:
                        break;
                    case 1:
                        super.handleMessage(paramMessage);
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        Message localMessage = localWorkerArgs.handler.obtainMessage(paramMessage.what);
                        localMessage.obj = localWorkerArgs;
                        localMessage.arg1 = paramMessage.arg1;
                        localMessage.sendToTarget();
                    }
                }
            }
        }
    }

    public static class QueryPoolException extends SQLException
    {
        public QueryPoolException(String paramString)
        {
            super();
        }
    }

    private static final class CookieWrapper
    {
        public Object cookie;
        public int event;
        public CallerInfoAsyncQuery.OnQueryCompleteListener listener;
        public String number;
    }

    public static abstract interface OnQueryCompleteListener
    {
        public abstract void onQueryComplete(int paramInt, Object paramObject, CallerInfo paramCallerInfo);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static class Injector
    {
        static String getPhoneNumber(String paramString)
        {
            PhoneNumberUtils.PhoneNumber localPhoneNumber = PhoneNumberUtils.PhoneNumber.parse(paramString);
            if (localPhoneNumber != null)
                paramString = localPhoneNumber.getNumberWithoutPrefix(true);
            return paramString;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.CallerInfoAsyncQuery
 * JD-Core Version:        0.6.2
 */