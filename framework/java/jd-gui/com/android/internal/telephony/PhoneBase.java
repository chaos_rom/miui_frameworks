package com.android.internal.telephony;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.net.wifi.WifiManager;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.telephony.ServiceState;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.gsm.UsimServiceTable;
import com.android.internal.telephony.ims.IsimRecords;
import com.android.internal.telephony.test.SimulatedRadioControl;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicReference;

public abstract class PhoneBase extends Handler
    implements Phone
{
    public static final String CLIR_KEY = "clir_key";
    public static final String DATA_DISABLED_ON_BOOT_KEY = "disabled_on_boot_key";
    public static final String DNS_SERVER_CHECK_DISABLED_KEY = "dns_server_check_disabled_key";
    protected static final int EVENT_CALL_RING = 14;
    protected static final int EVENT_CALL_RING_CONTINUE = 15;
    protected static final int EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED = 27;
    protected static final int EVENT_EMERGENCY_CALLBACK_MODE_ENTER = 25;
    protected static final int EVENT_EXIT_EMERGENCY_CALLBACK_RESPONSE = 26;
    protected static final int EVENT_GET_BASEBAND_VERSION_DONE = 6;
    protected static final int EVENT_GET_CALL_FORWARD_DONE = 13;
    protected static final int EVENT_GET_DEVICE_IDENTITY_DONE = 21;
    protected static final int EVENT_GET_IMEISV_DONE = 10;
    protected static final int EVENT_GET_IMEI_DONE = 9;
    protected static final int EVENT_GET_SIM_STATUS_DONE = 11;
    protected static final int EVENT_ICC_RECORD_EVENTS = 30;
    protected static final int EVENT_MMI_DONE = 4;
    protected static final int EVENT_NEW_ICC_SMS = 29;
    protected static final int EVENT_NV_READY = 23;
    protected static final int EVENT_RADIO_AVAILABLE = 1;
    protected static final int EVENT_RADIO_OFF_OR_NOT_AVAILABLE = 8;
    protected static final int EVENT_RADIO_ON = 5;
    protected static final int EVENT_REGISTERED_TO_NETWORK = 19;
    protected static final int EVENT_RUIM_RECORDS_LOADED = 22;
    protected static final int EVENT_SET_CALL_FORWARD_DONE = 12;
    protected static final int EVENT_SET_CLIR_COMPLETE = 18;
    protected static final int EVENT_SET_ENHANCED_VP = 24;
    protected static final int EVENT_SET_NETWORK_AUTOMATIC = 28;
    protected static final int EVENT_SET_NETWORK_AUTOMATIC_COMPLETE = 17;
    protected static final int EVENT_SET_NETWORK_MANUAL_COMPLETE = 16;
    protected static final int EVENT_SET_VM_NUMBER_DONE = 20;
    protected static final int EVENT_SIM_RECORDS_LOADED = 3;
    protected static final int EVENT_SSN = 2;
    protected static final int EVENT_USSD = 7;
    private static final boolean LOCAL_DEBUG = true;
    private static final String LOG_TAG = "PHONE";
    public static final String NETWORK_SELECTION_KEY = "network_selection_key";
    public static final String NETWORK_SELECTION_NAME_KEY = "network_selection_name_key";
    public CommandsInterface mCM;
    int mCallRingContinueToken;
    int mCallRingDelay;
    protected final Context mContext;
    public DataConnectionTracker mDataConnectionTracker;
    protected final RegistrantList mDisconnectRegistrants = new RegistrantList();
    boolean mDnsCheckDisabled;
    boolean mDoesRilSendMultipleCallRing;
    protected AtomicReference<IccCard> mIccCard = new AtomicReference();
    public IccRecords mIccRecords;
    protected final RegistrantList mIncomingRingRegistrants = new RegistrantList();
    public boolean mIsTheCurrentActivePhone = true;
    boolean mIsVoiceCapable = true;
    protected Looper mLooper;
    protected final RegistrantList mMmiCompleteRegistrants = new RegistrantList();
    protected final RegistrantList mMmiRegistrants = new RegistrantList();
    protected final RegistrantList mNewRingingConnectionRegistrants = new RegistrantList();
    protected PhoneNotifier mNotifier;
    protected final RegistrantList mPreciseCallStateRegistrants = new RegistrantList();
    public SMSDispatcher mSMS;
    protected final RegistrantList mServiceStateRegistrants = new RegistrantList();
    protected SimulatedRadioControl mSimulatedRadioControl;
    public SmsStorageMonitor mSmsStorageMonitor;
    public SmsUsageMonitor mSmsUsageMonitor;
    protected final RegistrantList mSuppServiceFailedRegistrants = new RegistrantList();
    boolean mUnitTestMode;
    protected final RegistrantList mUnknownConnectionRegistrants = new RegistrantList();

    protected PhoneBase(PhoneNotifier paramPhoneNotifier, Context paramContext, CommandsInterface paramCommandsInterface)
    {
        this(paramPhoneNotifier, paramContext, paramCommandsInterface, false);
    }

    protected PhoneBase(PhoneNotifier paramPhoneNotifier, Context paramContext, CommandsInterface paramCommandsInterface, boolean paramBoolean)
    {
        this.mNotifier = paramPhoneNotifier;
        this.mContext = paramContext;
        this.mLooper = Looper.myLooper();
        this.mCM = paramCommandsInterface;
        setPropertiesByCarrier();
        setUnitTestMode(paramBoolean);
        this.mDnsCheckDisabled = PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("dns_server_check_disabled_key", false);
        this.mCM.setOnCallRing(this, 14, null);
        this.mIsVoiceCapable = this.mContext.getResources().getBoolean(17891368);
        this.mDoesRilSendMultipleCallRing = SystemProperties.getBoolean("ro.telephony.call_ring.multiple", true);
        Log.d("PHONE", "mDoesRilSendMultipleCallRing=" + this.mDoesRilSendMultipleCallRing);
        this.mCallRingDelay = SystemProperties.getInt("ro.telephony.call_ring.delay", 3000);
        Log.d("PHONE", "mCallRingDelay=" + this.mCallRingDelay);
        this.mSmsStorageMonitor = new SmsStorageMonitor(this);
        this.mSmsUsageMonitor = new SmsUsageMonitor(paramContext);
    }

    private void checkCorrectThread(Handler paramHandler)
    {
        if (paramHandler.getLooper() != this.mLooper)
            throw new RuntimeException("com.android.internal.telephony.Phone must be used from within one thread");
    }

    private String getSavedNetworkSelection()
    {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getString("network_selection_key", "");
    }

    private static void logUnexpectedCdmaMethodCall(String paramString)
    {
        Log.e("PHONE", "Error! " + paramString + "() in PhoneBase should not be " + "called, CDMAPhone inactive.");
    }

    private static void logUnexpectedGsmMethodCall(String paramString)
    {
        Log.e("PHONE", "Error! " + paramString + "() in PhoneBase should not be " + "called, GSMPhone inactive.");
    }

    private void notifyIncomingRing()
    {
        if (!this.mIsVoiceCapable);
        while (true)
        {
            return;
            AsyncResult localAsyncResult = new AsyncResult(null, this, null);
            this.mIncomingRingRegistrants.notifyRegistrants(localAsyncResult);
        }
    }

    private void sendIncomingCallRingNotification(int paramInt)
    {
        if ((this.mIsVoiceCapable) && (!this.mDoesRilSendMultipleCallRing) && (paramInt == this.mCallRingContinueToken))
        {
            Log.d("PHONE", "Sending notifyIncomingRing");
            notifyIncomingRing();
            sendMessageDelayed(obtainMessage(15, paramInt, 0), this.mCallRingDelay);
        }
        while (true)
        {
            return;
            Log.d("PHONE", "Ignoring ring notification request, mDoesRilSendMultipleCallRing=" + this.mDoesRilSendMultipleCallRing + " token=" + paramInt + " mCallRingContinueToken=" + this.mCallRingContinueToken + " mIsVoiceCapable=" + this.mIsVoiceCapable);
        }
    }

    private void setPropertiesByCarrier()
    {
        String str1 = SystemProperties.get("ro.carrier");
        if ((str1 == null) || (str1.length() == 0) || ("unknown".equals(str1)));
        label166: label170: 
        while (true)
        {
            return;
            CharSequence[] arrayOfCharSequence = this.mContext.getResources().getTextArray(17236019);
            for (int i = 0; ; i += 3)
            {
                while (true)
                {
                    if (i >= arrayOfCharSequence.length)
                        break label170;
                    if (!str1.equals(arrayOfCharSequence[i].toString()))
                        break label166;
                    String str2 = arrayOfCharSequence[(i + 1)].toString();
                    String str3 = str2.substring(0, 2);
                    String str4 = "";
                    if (str2.length() >= 5)
                        str4 = str2.substring(3, 5);
                    MccTable.setSystemLocale(this.mContext, str3, str4);
                    if (str4.isEmpty())
                        break;
                    try
                    {
                        Settings.Secure.getInt(this.mContext.getContentResolver(), "wifi_country_code");
                    }
                    catch (Settings.SettingNotFoundException localSettingNotFoundException)
                    {
                        ((WifiManager)this.mContext.getSystemService("wifi")).setCountryCode(str4, false);
                    }
                }
                break;
            }
        }
    }

    public int disableApnType(String paramString)
    {
        return this.mDataConnectionTracker.disableApnType(paramString);
    }

    public void disableDnsCheck(boolean paramBoolean)
    {
        this.mDnsCheckDisabled = paramBoolean;
        SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        localEditor.putBoolean("dns_server_check_disabled_key", paramBoolean);
        localEditor.apply();
    }

    public void dispose()
    {
        synchronized (PhoneProxy.lockForRadioTechnologyChange)
        {
            this.mCM.unSetOnCallRing(this);
            this.mDataConnectionTracker.cleanUpAllConnections(null);
            this.mIsTheCurrentActivePhone = false;
            this.mSmsStorageMonitor.dispose();
            this.mSmsUsageMonitor.dispose();
            return;
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("PhoneBase:");
        paramPrintWriter.println(" mCM=" + this.mCM);
        paramPrintWriter.println(" mDnsCheckDisabled=" + this.mDnsCheckDisabled);
        paramPrintWriter.println(" mDataConnectionTracker=" + this.mDataConnectionTracker);
        paramPrintWriter.println(" mDoesRilSendMultipleCallRing=" + this.mDoesRilSendMultipleCallRing);
        paramPrintWriter.println(" mCallRingContinueToken=" + this.mCallRingContinueToken);
        paramPrintWriter.println(" mCallRingDelay=" + this.mCallRingDelay);
        paramPrintWriter.println(" mIsTheCurrentActivePhone=" + this.mIsTheCurrentActivePhone);
        paramPrintWriter.println(" mIsVoiceCapable=" + this.mIsVoiceCapable);
        paramPrintWriter.println(" mIccRecords=" + this.mIccRecords);
        paramPrintWriter.println(" mIccCard=" + this.mIccCard.get());
        paramPrintWriter.println(" mSmsStorageMonitor=" + this.mSmsStorageMonitor);
        paramPrintWriter.println(" mSmsUsageMonitor=" + this.mSmsUsageMonitor);
        paramPrintWriter.println(" mSMS=" + this.mSMS);
        paramPrintWriter.flush();
        paramPrintWriter.println(" mLooper=" + this.mLooper);
        paramPrintWriter.println(" mContext=" + this.mContext);
        paramPrintWriter.println(" mNotifier=" + this.mNotifier);
        paramPrintWriter.println(" mSimulatedRadioControl=" + this.mSimulatedRadioControl);
        paramPrintWriter.println(" mUnitTestMode=" + this.mUnitTestMode);
        paramPrintWriter.println(" isDnsCheckDisabled()=" + isDnsCheckDisabled());
        paramPrintWriter.println(" getUnitTestMode()=" + getUnitTestMode());
        paramPrintWriter.println(" getState()=" + getState());
        paramPrintWriter.println(" getIccSerialNumber()=" + getIccSerialNumber());
        paramPrintWriter.println(" getIccRecordsLoaded()=" + getIccRecordsLoaded());
        paramPrintWriter.println(" getMessageWaitingIndicator()=" + getMessageWaitingIndicator());
        paramPrintWriter.println(" getCallForwardingIndicator()=" + getCallForwardingIndicator());
        paramPrintWriter.println(" isInEmergencyCall()=" + isInEmergencyCall());
        paramPrintWriter.flush();
        paramPrintWriter.println(" isInEcm()=" + isInEcm());
        paramPrintWriter.println(" getPhoneName()=" + getPhoneName());
        paramPrintWriter.println(" getPhoneType()=" + getPhoneType());
        paramPrintWriter.println(" getVoiceMessageCount()=" + getVoiceMessageCount());
        paramPrintWriter.println(" getActiveApnTypes()=" + getActiveApnTypes());
        paramPrintWriter.println(" isDataConnectivityPossible()=" + isDataConnectivityPossible());
        paramPrintWriter.println(" needsOtaServiceProvisioning=" + needsOtaServiceProvisioning());
    }

    public int enableApnType(String paramString)
    {
        return this.mDataConnectionTracker.enableApnType(paramString);
    }

    public void enableEnhancedVoicePrivacy(boolean paramBoolean, Message paramMessage)
    {
        logUnexpectedCdmaMethodCall("enableEnhancedVoicePrivacy");
    }

    public void exitEmergencyCallbackMode()
    {
        logUnexpectedCdmaMethodCall("exitEmergencyCallbackMode");
    }

    public String getActiveApnHost(String paramString)
    {
        return this.mDataConnectionTracker.getActiveApnString(paramString);
    }

    public String[] getActiveApnTypes()
    {
        return this.mDataConnectionTracker.getActiveApnTypes();
    }

    public boolean getCallForwardingIndicator()
    {
        return this.mIccRecords.getVoiceCallForwardingFlag();
    }

    public CallTracker getCallTracker()
    {
        return null;
    }

    public int getCdmaEriIconIndex()
    {
        logUnexpectedCdmaMethodCall("getCdmaEriIconIndex");
        return -1;
    }

    public int getCdmaEriIconMode()
    {
        logUnexpectedCdmaMethodCall("getCdmaEriIconMode");
        return -1;
    }

    public String getCdmaEriText()
    {
        logUnexpectedCdmaMethodCall("getCdmaEriText");
        return "GSM nw, no ERI";
    }

    public String getCdmaMin()
    {
        logUnexpectedCdmaMethodCall("getCdmaMin");
        return null;
    }

    public String getCdmaPrlVersion()
    {
        logUnexpectedCdmaMethodCall("getCdmaPrlVersion");
        return null;
    }

    public Context getContext()
    {
        return this.mContext;
    }

    public Phone.DataState getDataConnectionState()
    {
        return getDataConnectionState("default");
    }

    public void getEnhancedVoicePrivacy(Message paramMessage)
    {
        logUnexpectedCdmaMethodCall("getEnhancedVoicePrivacy");
    }

    public Handler getHandler()
    {
        return this;
    }

    public IccCard getIccCard()
    {
        return (IccCard)this.mIccCard.get();
    }

    public IccFileHandler getIccFileHandler()
    {
        IccCard localIccCard = (IccCard)this.mIccCard.get();
        if (localIccCard == null);
        for (IccFileHandler localIccFileHandler = null; ; localIccFileHandler = localIccCard.getIccFileHandler())
            return localIccFileHandler;
    }

    public boolean getIccRecordsLoaded()
    {
        return this.mIccRecords.getRecordsLoaded();
    }

    public String getIccSerialNumber()
    {
        return this.mIccRecords.iccid;
    }

    public IsimRecords getIsimRecords()
    {
        Log.e("PHONE", "getIsimRecords() is only supported on LTE devices");
        return null;
    }

    public LinkCapabilities getLinkCapabilities(String paramString)
    {
        return this.mDataConnectionTracker.getLinkCapabilities(paramString);
    }

    public LinkProperties getLinkProperties(String paramString)
    {
        return this.mDataConnectionTracker.getLinkProperties(paramString);
    }

    public int getLteOnCdmaMode()
    {
        return this.mCM.getLteOnCdmaMode();
    }

    public boolean getMessageWaitingIndicator()
    {
        return this.mIccRecords.getVoiceMessageWaiting();
    }

    public String getMsisdn()
    {
        logUnexpectedGsmMethodCall("getMsisdn");
        return null;
    }

    public abstract String getPhoneName();

    public abstract int getPhoneType();

    public void getPreferredNetworkType(Message paramMessage)
    {
        this.mCM.getPreferredNetworkType(paramMessage);
    }

    public ServiceStateTracker getServiceStateTracker()
    {
        return null;
    }

    public SimulatedRadioControl getSimulatedRadioControl()
    {
        return this.mSimulatedRadioControl;
    }

    public void getSmscAddress(Message paramMessage)
    {
        this.mCM.getSmscAddress(paramMessage);
    }

    public abstract Phone.State getState();

    public boolean getUnitTestMode()
    {
        return this.mUnitTestMode;
    }

    public UsimServiceTable getUsimServiceTable()
    {
        return this.mIccRecords.getUsimServiceTable();
    }

    public int getVoiceMessageCount()
    {
        return 0;
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
            throw new RuntimeException("unexpected event not handled");
        case 14:
            Log.d("PHONE", "Event EVENT_CALL_RING Received state=" + getState());
            if (((AsyncResult)paramMessage.obj).exception == null)
            {
                Phone.State localState = getState();
                if ((this.mDoesRilSendMultipleCallRing) || ((localState != Phone.State.RINGING) && (localState != Phone.State.IDLE)))
                    break label129;
                this.mCallRingContinueToken = (1 + this.mCallRingContinueToken);
                sendIncomingCallRingNotification(this.mCallRingContinueToken);
            }
            break;
        case 15:
        }
        while (true)
        {
            return;
            label129: notifyIncomingRing();
            continue;
            Log.d("PHONE", "Event EVENT_CALL_RING_CONTINUE Received stat=" + getState());
            if (getState() == Phone.State.RINGING)
                sendIncomingCallRingNotification(paramMessage.arg1);
        }
    }

    public void invokeOemRilRequestRaw(byte[] paramArrayOfByte, Message paramMessage)
    {
        this.mCM.invokeOemRilRequestRaw(paramArrayOfByte, paramMessage);
    }

    public void invokeOemRilRequestStrings(String[] paramArrayOfString, Message paramMessage)
    {
        this.mCM.invokeOemRilRequestStrings(paramArrayOfString, paramMessage);
    }

    public boolean isCspPlmnEnabled()
    {
        logUnexpectedGsmMethodCall("isCspPlmnEnabled");
        return false;
    }

    public boolean isDataConnectivityPossible()
    {
        return isDataConnectivityPossible("default");
    }

    public boolean isDataConnectivityPossible(String paramString)
    {
        if ((this.mDataConnectionTracker != null) && (this.mDataConnectionTracker.isDataPossible(paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isDnsCheckDisabled()
    {
        return this.mDnsCheckDisabled;
    }

    public boolean isInEcm()
    {
        return false;
    }

    public boolean isInEmergencyCall()
    {
        return false;
    }

    public boolean isMinInfoReady()
    {
        logUnexpectedCdmaMethodCall("isMinInfoReady");
        return false;
    }

    public boolean isOtaSpNumber(String paramString)
    {
        return false;
    }

    public boolean needsOtaServiceProvisioning()
    {
        return false;
    }

    public void notifyCallForwardingIndicator()
    {
        Log.e("PHONE", "Error! This function should never be executed, inactive CDMAPhone.");
    }

    public void notifyDataActivity()
    {
        this.mNotifier.notifyDataActivity(this);
    }

    public void notifyDataConnection(String paramString)
    {
        for (String str : getActiveApnTypes())
            this.mNotifier.notifyDataConnection(this, paramString, str, getDataConnectionState(str));
    }

    public void notifyDataConnection(String paramString1, String paramString2)
    {
        this.mNotifier.notifyDataConnection(this, paramString1, paramString2, getDataConnectionState(paramString2));
    }

    public void notifyDataConnection(String paramString1, String paramString2, Phone.DataState paramDataState)
    {
        this.mNotifier.notifyDataConnection(this, paramString1, paramString2, paramDataState);
    }

    public void notifyDataConnectionFailed(String paramString1, String paramString2)
    {
        this.mNotifier.notifyDataConnectionFailed(this, paramString1, paramString2);
    }

    protected void notifyDisconnectP(Connection paramConnection)
    {
        AsyncResult localAsyncResult = new AsyncResult(null, paramConnection, null);
        this.mDisconnectRegistrants.notifyRegistrants(localAsyncResult);
    }

    public void notifyMessageWaitingIndicator()
    {
        if (!this.mIsVoiceCapable);
        while (true)
        {
            return;
            this.mNotifier.notifyMessageWaitingChanged(this);
        }
    }

    protected void notifyNewRingingConnectionP(Connection paramConnection)
    {
        if (!this.mIsVoiceCapable);
        while (true)
        {
            return;
            AsyncResult localAsyncResult = new AsyncResult(null, paramConnection, null);
            this.mNewRingingConnectionRegistrants.notifyRegistrants(localAsyncResult);
        }
    }

    public void notifyOtaspChanged(int paramInt)
    {
        this.mNotifier.notifyOtaspChanged(this, paramInt);
    }

    protected void notifyPreciseCallStateChangedP()
    {
        AsyncResult localAsyncResult = new AsyncResult(null, this, null);
        this.mPreciseCallStateRegistrants.notifyRegistrants(localAsyncResult);
    }

    protected void notifyServiceStateChangedP(ServiceState paramServiceState)
    {
        AsyncResult localAsyncResult = new AsyncResult(null, paramServiceState, null);
        this.mServiceStateRegistrants.notifyRegistrants(localAsyncResult);
        this.mNotifier.notifyServiceState(this);
    }

    public void queryAvailableBandMode(Message paramMessage)
    {
        this.mCM.queryAvailableBandMode(paramMessage);
    }

    public void queryCdmaRoamingPreference(Message paramMessage)
    {
        this.mCM.queryCdmaRoamingPreference(paramMessage);
    }

    public void queryTTYMode(Message paramMessage)
    {
        this.mCM.queryTTYMode(paramMessage);
    }

    public void registerFoT53ClirlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerFoT53ClirlInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForCallWaiting(Handler paramHandler, int paramInt, Object paramObject)
    {
        logUnexpectedCdmaMethodCall("registerForCallWaiting");
    }

    public void registerForCdmaOtaStatusChange(Handler paramHandler, int paramInt, Object paramObject)
    {
        logUnexpectedCdmaMethodCall("registerForCdmaOtaStatusChange");
    }

    public void registerForDisconnect(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mDisconnectRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForDisplayInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForDisplayInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForEcmTimerReset(Handler paramHandler, int paramInt, Object paramObject)
    {
        logUnexpectedCdmaMethodCall("registerForEcmTimerReset");
    }

    public void registerForInCallVoicePrivacyOff(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForInCallVoicePrivacyOff(paramHandler, paramInt, paramObject);
    }

    public void registerForInCallVoicePrivacyOn(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForInCallVoicePrivacyOn(paramHandler, paramInt, paramObject);
    }

    public void registerForIncomingRing(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mIncomingRingRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForLineControlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForLineControlInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForMmiComplete(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mMmiCompleteRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForMmiInitiate(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mMmiRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForNewRingingConnection(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mNewRingingConnectionRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForNumberInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForNumberInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForPreciseCallStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mPreciseCallStateRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForRedirectedNumberInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForRedirectedNumberInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForResendIncallMute(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForResendIncallMute(paramHandler, paramInt, paramObject);
    }

    public void registerForRingbackTone(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForRingbackTone(paramHandler, paramInt, paramObject);
    }

    public void registerForServiceStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mServiceStateRegistrants.add(paramHandler, paramInt, paramObject);
    }

    public void registerForSignalInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForSignalInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForSubscriptionInfoReady(Handler paramHandler, int paramInt, Object paramObject)
    {
        logUnexpectedCdmaMethodCall("registerForSubscriptionInfoReady");
    }

    public void registerForSuppServiceFailed(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mSuppServiceFailedRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForT53AudioControlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForT53AudioControlInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForUnknownConnection(Handler paramHandler, int paramInt, Object paramObject)
    {
        checkCorrectThread(paramHandler);
        this.mUnknownConnectionRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void removeReferences()
    {
        this.mSmsStorageMonitor = null;
        this.mSmsUsageMonitor = null;
        this.mSMS = null;
        this.mIccRecords = null;
        this.mIccCard.set(null);
        this.mDataConnectionTracker = null;
    }

    public void requestIsimAuthentication(String paramString, Message paramMessage)
    {
        Log.e("PHONE", "requestIsimAuthentication() is only supported on LTE devices");
    }

    public void restoreSavedNetworkSelection(Message paramMessage)
    {
        String str = getSavedNetworkSelection();
        if (TextUtils.isEmpty(str))
            this.mCM.setNetworkSelectionModeAutomatic(paramMessage);
        while (true)
        {
            return;
            this.mCM.setNetworkSelectionModeManual(str, paramMessage);
        }
    }

    public void sendBurstDtmf(String paramString, int paramInt1, int paramInt2, Message paramMessage)
    {
        logUnexpectedCdmaMethodCall("sendBurstDtmf");
    }

    public void setBandMode(int paramInt, Message paramMessage)
    {
        this.mCM.setBandMode(paramInt, paramMessage);
    }

    public void setCdmaRoamingPreference(int paramInt, Message paramMessage)
    {
        this.mCM.setCdmaRoamingPreference(paramInt, paramMessage);
    }

    public void setCdmaSubscription(int paramInt, Message paramMessage)
    {
        this.mCM.setCdmaSubscriptionSource(paramInt, paramMessage);
    }

    public void setEchoSuppressionEnabled(boolean paramBoolean)
    {
    }

    public void setOnEcbModeExitResponse(Handler paramHandler, int paramInt, Object paramObject)
    {
        logUnexpectedCdmaMethodCall("setOnEcbModeExitResponse");
    }

    public void setPreferredNetworkType(int paramInt, Message paramMessage)
    {
        this.mCM.setPreferredNetworkType(paramInt, paramMessage);
    }

    public void setSmscAddress(String paramString, Message paramMessage)
    {
        this.mCM.setSmscAddress(paramString, paramMessage);
    }

    public void setSystemProperty(String paramString1, String paramString2)
    {
        if (getUnitTestMode());
        while (true)
        {
            return;
            SystemProperties.set(paramString1, paramString2);
        }
    }

    public void setTTYMode(int paramInt, Message paramMessage)
    {
        this.mCM.setTTYMode(paramInt, paramMessage);
    }

    public void setUnitTestMode(boolean paramBoolean)
    {
        this.mUnitTestMode = paramBoolean;
    }

    public void setVoiceMessageWaiting(int paramInt1, int paramInt2)
    {
        this.mIccRecords.setVoiceMessageWaiting(paramInt1, paramInt2);
    }

    public void unregisterForCallWaiting(Handler paramHandler)
    {
        logUnexpectedCdmaMethodCall("unregisterForCallWaiting");
    }

    public void unregisterForCdmaOtaStatusChange(Handler paramHandler)
    {
        logUnexpectedCdmaMethodCall("unregisterForCdmaOtaStatusChange");
    }

    public void unregisterForDisconnect(Handler paramHandler)
    {
        this.mDisconnectRegistrants.remove(paramHandler);
    }

    public void unregisterForDisplayInfo(Handler paramHandler)
    {
        this.mCM.unregisterForDisplayInfo(paramHandler);
    }

    public void unregisterForEcmTimerReset(Handler paramHandler)
    {
        logUnexpectedCdmaMethodCall("unregisterForEcmTimerReset");
    }

    public void unregisterForInCallVoicePrivacyOff(Handler paramHandler)
    {
        this.mCM.unregisterForInCallVoicePrivacyOff(paramHandler);
    }

    public void unregisterForInCallVoicePrivacyOn(Handler paramHandler)
    {
        this.mCM.unregisterForInCallVoicePrivacyOn(paramHandler);
    }

    public void unregisterForIncomingRing(Handler paramHandler)
    {
        this.mIncomingRingRegistrants.remove(paramHandler);
    }

    public void unregisterForLineControlInfo(Handler paramHandler)
    {
        this.mCM.unregisterForLineControlInfo(paramHandler);
    }

    public void unregisterForMmiComplete(Handler paramHandler)
    {
        checkCorrectThread(paramHandler);
        this.mMmiCompleteRegistrants.remove(paramHandler);
    }

    public void unregisterForMmiInitiate(Handler paramHandler)
    {
        this.mMmiRegistrants.remove(paramHandler);
    }

    public void unregisterForNewRingingConnection(Handler paramHandler)
    {
        this.mNewRingingConnectionRegistrants.remove(paramHandler);
    }

    public void unregisterForNumberInfo(Handler paramHandler)
    {
        this.mCM.unregisterForNumberInfo(paramHandler);
    }

    public void unregisterForPreciseCallStateChanged(Handler paramHandler)
    {
        this.mPreciseCallStateRegistrants.remove(paramHandler);
    }

    public void unregisterForRedirectedNumberInfo(Handler paramHandler)
    {
        this.mCM.unregisterForRedirectedNumberInfo(paramHandler);
    }

    public void unregisterForResendIncallMute(Handler paramHandler)
    {
        this.mCM.unregisterForResendIncallMute(paramHandler);
    }

    public void unregisterForRingbackTone(Handler paramHandler)
    {
        this.mCM.unregisterForRingbackTone(paramHandler);
    }

    public void unregisterForServiceStateChanged(Handler paramHandler)
    {
        this.mServiceStateRegistrants.remove(paramHandler);
    }

    public void unregisterForSignalInfo(Handler paramHandler)
    {
        this.mCM.unregisterForSignalInfo(paramHandler);
    }

    public void unregisterForSubscriptionInfoReady(Handler paramHandler)
    {
        logUnexpectedCdmaMethodCall("unregisterForSubscriptionInfoReady");
    }

    public void unregisterForSuppServiceFailed(Handler paramHandler)
    {
        this.mSuppServiceFailedRegistrants.remove(paramHandler);
    }

    public void unregisterForT53AudioControlInfo(Handler paramHandler)
    {
        this.mCM.unregisterForT53AudioControlInfo(paramHandler);
    }

    public void unregisterForT53ClirInfo(Handler paramHandler)
    {
        this.mCM.unregisterForT53ClirInfo(paramHandler);
    }

    public void unregisterForUnknownConnection(Handler paramHandler)
    {
        this.mUnknownConnectionRegistrants.remove(paramHandler);
    }

    public void unsetOnEcbModeExitResponse(Handler paramHandler)
    {
        logUnexpectedCdmaMethodCall("unsetOnEcbModeExitResponse");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.PhoneBase
 * JD-Core Version:        0.6.2
 */