package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

public class SmsUsageMonitor
{
    private static final boolean DBG = true;
    private static final int DEFAULT_SMS_CHECK_PERIOD = 1800000;
    private static final int DEFAULT_SMS_MAX_COUNT = 30;
    private static final String TAG = "SmsUsageMonitor";
    private static final boolean VDBG;
    private final int mCheckPeriod;
    private final int mMaxAllowed;
    private final HashMap<String, ArrayList<Long>> mSmsStamp = new HashMap();

    public SmsUsageMonitor(Context paramContext)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        this.mMaxAllowed = Settings.Secure.getInt(localContentResolver, "sms_outgoing_check_max_count", 30);
        this.mCheckPeriod = Settings.Secure.getInt(localContentResolver, "sms_outgoing_check_interval_ms", 1800000);
    }

    private boolean isUnderLimit(ArrayList<Long> paramArrayList, int paramInt)
    {
        Long localLong = Long.valueOf(System.currentTimeMillis());
        long l = localLong.longValue() - this.mCheckPeriod;
        while ((!paramArrayList.isEmpty()) && (((Long)paramArrayList.get(0)).longValue() < l))
            paramArrayList.remove(0);
        if (paramInt + paramArrayList.size() <= this.mMaxAllowed)
            for (int i = 0; i < paramInt; i++)
                paramArrayList.add(localLong);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static void log(String paramString)
    {
        Log.d("SmsUsageMonitor", paramString);
    }

    // ERROR //
    private void removeExpiredTimestamps()
    {
        // Byte code:
        //     0: invokestatic 60	java/lang/System:currentTimeMillis	()J
        //     3: aload_0
        //     4: getfield 52	com/android/internal/telephony/SmsUsageMonitor:mCheckPeriod	I
        //     7: i2l
        //     8: lsub
        //     9: lstore_1
        //     10: aload_0
        //     11: getfield 32	com/android/internal/telephony/SmsUsageMonitor:mSmsStamp	Ljava/util/HashMap;
        //     14: astore_3
        //     15: aload_3
        //     16: monitorenter
        //     17: aload_0
        //     18: getfield 32	com/android/internal/telephony/SmsUsageMonitor:mSmsStamp	Ljava/util/HashMap;
        //     21: invokevirtual 103	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     24: invokeinterface 109 1 0
        //     29: astore 5
        //     31: aload 5
        //     33: invokeinterface 114 1 0
        //     38: ifeq +72 -> 110
        //     41: aload 5
        //     43: invokeinterface 118 1 0
        //     48: checkcast 120	java/util/Map$Entry
        //     51: invokeinterface 123 1 0
        //     56: checkcast 71	java/util/ArrayList
        //     59: astore 6
        //     61: aload 6
        //     63: invokevirtual 75	java/util/ArrayList:isEmpty	()Z
        //     66: ifne +27 -> 93
        //     69: aload 6
        //     71: bipush 255
        //     73: aload 6
        //     75: invokevirtual 86	java/util/ArrayList:size	()I
        //     78: iadd
        //     79: invokevirtual 79	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     82: checkcast 62	java/lang/Long
        //     85: invokevirtual 69	java/lang/Long:longValue	()J
        //     88: lload_1
        //     89: lcmp
        //     90: ifge -59 -> 31
        //     93: aload 5
        //     95: invokeinterface 125 1 0
        //     100: goto -69 -> 31
        //     103: astore 4
        //     105: aload_3
        //     106: monitorexit
        //     107: aload 4
        //     109: athrow
        //     110: aload_3
        //     111: monitorexit
        //     112: return
        //
        // Exception table:
        //     from	to	target	type
        //     17	107	103	finally
        //     110	112	103	finally
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean check(String paramString, int paramInt)
    {
        boolean bool;
        if ("com.android.mms".equals(paramString))
            bool = true;
        while (true)
        {
            return bool;
            synchronized (this.mSmsStamp)
            {
                removeExpiredTimestamps();
                ArrayList localArrayList = (ArrayList)this.mSmsStamp.get(paramString);
                if (localArrayList == null)
                {
                    localArrayList = new ArrayList();
                    this.mSmsStamp.put(paramString, localArrayList);
                }
                bool = isUnderLimit(localArrayList, paramInt);
            }
        }
    }

    void dispose()
    {
        this.mSmsStamp.clear();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.SmsUsageMonitor
 * JD-Core Version:        0.6.2
 */