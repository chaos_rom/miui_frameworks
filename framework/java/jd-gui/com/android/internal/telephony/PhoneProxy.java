package com.android.internal.telephony;

import android.app.ActivityManagerNative;
import android.content.Context;
import android.content.Intent;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.util.Log;
import com.android.internal.telephony.gsm.UsimServiceTable;
import com.android.internal.telephony.ims.IsimRecords;
import com.android.internal.telephony.test.SimulatedRadioControl;
import java.util.List;

public class PhoneProxy extends Handler
    implements Phone
{
    private static final int EVENT_RADIO_ON = 2;
    private static final int EVENT_REQUEST_VOICE_RADIO_TECH_DONE = 3;
    private static final int EVENT_RIL_CONNECTED = 4;
    private static final int EVENT_VOICE_RADIO_TECH_CHANGED = 1;
    private static final String LOG_TAG = "PHONE";
    public static final Object lockForRadioTechnologyChange = new Object();
    private Phone mActivePhone;
    private CommandsInterface mCommandsInterface;
    private IccPhoneBookInterfaceManagerProxy mIccPhoneBookInterfaceManagerProxy;
    private IccSmsInterfaceManagerProxy mIccSmsInterfaceManagerProxy;
    private PhoneSubInfoProxy mPhoneSubInfoProxy;
    private boolean mResetModemOnRadioTechnologyChange = false;
    private int mRilVersion;

    public PhoneProxy(Phone paramPhone)
    {
        this.mActivePhone = paramPhone;
        this.mResetModemOnRadioTechnologyChange = SystemProperties.getBoolean("persist.radio.reset_on_switch", false);
        this.mIccSmsInterfaceManagerProxy = new IccSmsInterfaceManagerProxy(paramPhone.getIccSmsInterfaceManager());
        this.mIccPhoneBookInterfaceManagerProxy = new IccPhoneBookInterfaceManagerProxy(paramPhone.getIccPhoneBookInterfaceManager());
        this.mPhoneSubInfoProxy = new PhoneSubInfoProxy(paramPhone.getPhoneSubInfo());
        this.mCommandsInterface = ((PhoneBase)this.mActivePhone).mCM;
        this.mCommandsInterface.registerForRilConnected(this, 4, null);
        this.mCommandsInterface.registerForOn(this, 2, null);
        this.mCommandsInterface.registerForVoiceRadioTechChanged(this, 1, null);
    }

    private void deleteAndCreatePhone(int paramInt)
    {
        String str1 = "Unknown";
        Phone localPhone = this.mActivePhone;
        if (localPhone != null)
            str1 = ((PhoneBase)localPhone).getPhoneName();
        StringBuilder localStringBuilder = new StringBuilder().append("Switching Voice Phone : ").append(str1).append(" >>> ");
        String str2;
        if (ServiceState.isGsm(paramInt))
        {
            str2 = "GSM";
            logd(str2);
            if (localPhone != null)
            {
                CallManager.getInstance().unregisterPhone(localPhone);
                logd("Disposing old phone..");
                localPhone.dispose();
            }
            if (!ServiceState.isCdma(paramInt))
                break label139;
            this.mActivePhone = PhoneFactory.getCdmaPhone();
        }
        while (true)
        {
            if (localPhone != null)
                localPhone.removeReferences();
            if (this.mActivePhone != null)
                CallManager.getInstance().registerPhone(this.mActivePhone);
            return;
            str2 = "CDMA";
            break;
            label139: if (ServiceState.isGsm(paramInt))
                this.mActivePhone = PhoneFactory.getGsmPhone();
        }
    }

    private static void logd(String paramString)
    {
        Log.d("PHONE", "[PhoneProxy] " + paramString);
    }

    private void loge(String paramString)
    {
        Log.e("PHONE", "[PhoneProxy] " + paramString);
    }

    private void logw(String paramString)
    {
        Log.w("PHONE", "[PhoneProxy] " + paramString);
    }

    private void updatePhoneObject(int paramInt)
    {
        if (this.mActivePhone != null)
        {
            if ((this.mRilVersion != 6) || (getLteOnCdmaMode() != 1))
                break label156;
            if (this.mActivePhone.getPhoneType() == 2)
                logd("LTE ON CDMA property is set. Use CDMA Phone newVoiceRadioTech = " + paramInt + " Active Phone = " + this.mActivePhone.getPhoneName());
        }
        while (true)
        {
            return;
            logd("LTE ON CDMA property is set. Switch to CDMALTEPhone newVoiceRadioTech = " + paramInt + " Active Phone = " + this.mActivePhone.getPhoneName());
            paramInt = 6;
            while (true)
                if (paramInt == 0)
                {
                    logd("Ignoring voice radio technology changed message. newVoiceRadioTech = Unknown. Active Phone = " + this.mActivePhone.getPhoneName());
                    break;
                    label156: if (((ServiceState.isCdma(paramInt)) && (this.mActivePhone.getPhoneType() == 2)) || ((ServiceState.isGsm(paramInt)) && (this.mActivePhone.getPhoneType() == 1)))
                    {
                        logd("Ignoring voice radio technology changed message. newVoiceRadioTech = " + paramInt + " Active Phone = " + this.mActivePhone.getPhoneName());
                        break;
                    }
                }
            boolean bool = false;
            if ((this.mResetModemOnRadioTechnologyChange) && (this.mCommandsInterface.getRadioState().isOn()))
            {
                bool = true;
                logd("Setting Radio Power to Off");
                this.mCommandsInterface.setRadioPower(false, null);
            }
            deleteAndCreatePhone(paramInt);
            if ((this.mResetModemOnRadioTechnologyChange) && (bool))
            {
                logd("Resetting Radio");
                this.mCommandsInterface.setRadioPower(bool, null);
            }
            this.mIccSmsInterfaceManagerProxy.setmIccSmsInterfaceManager(this.mActivePhone.getIccSmsInterfaceManager());
            this.mIccPhoneBookInterfaceManagerProxy.setmIccPhoneBookInterfaceManager(this.mActivePhone.getIccPhoneBookInterfaceManager());
            this.mPhoneSubInfoProxy.setmPhoneSubInfo(this.mActivePhone.getPhoneSubInfo());
            this.mCommandsInterface = ((PhoneBase)this.mActivePhone).mCM;
            Intent localIntent = new Intent("android.intent.action.RADIO_TECHNOLOGY");
            localIntent.addFlags(536870912);
            localIntent.putExtra("phoneName", this.mActivePhone.getPhoneName());
            ActivityManagerNative.broadcastStickyIntent(localIntent, null);
        }
    }

    public void acceptCall()
        throws CallStateException
    {
        this.mActivePhone.acceptCall();
    }

    public void activateCellBroadcastSms(int paramInt, Message paramMessage)
    {
        this.mActivePhone.activateCellBroadcastSms(paramInt, paramMessage);
    }

    public boolean canConference()
    {
        return this.mActivePhone.canConference();
    }

    public boolean canTransfer()
    {
        return this.mActivePhone.canTransfer();
    }

    public void clearDisconnected()
    {
        this.mActivePhone.clearDisconnected();
    }

    public void conference()
        throws CallStateException
    {
        this.mActivePhone.conference();
    }

    public Connection dial(String paramString)
        throws CallStateException
    {
        return this.mActivePhone.dial(paramString);
    }

    public Connection dial(String paramString, UUSInfo paramUUSInfo)
        throws CallStateException
    {
        return this.mActivePhone.dial(paramString, paramUUSInfo);
    }

    public int disableApnType(String paramString)
    {
        return this.mActivePhone.disableApnType(paramString);
    }

    public void disableDnsCheck(boolean paramBoolean)
    {
        this.mActivePhone.disableDnsCheck(paramBoolean);
    }

    public void disableLocationUpdates()
    {
        this.mActivePhone.disableLocationUpdates();
    }

    public void dispose()
    {
        this.mCommandsInterface.unregisterForOn(this);
        this.mCommandsInterface.unregisterForVoiceRadioTechChanged(this);
        this.mCommandsInterface.unregisterForRilConnected(this);
    }

    public int enableApnType(String paramString)
    {
        return this.mActivePhone.enableApnType(paramString);
    }

    public void enableEnhancedVoicePrivacy(boolean paramBoolean, Message paramMessage)
    {
        this.mActivePhone.enableEnhancedVoicePrivacy(paramBoolean, paramMessage);
    }

    public void enableLocationUpdates()
    {
        this.mActivePhone.enableLocationUpdates();
    }

    public void exitEmergencyCallbackMode()
    {
        this.mActivePhone.exitEmergencyCallbackMode();
    }

    public void explicitCallTransfer()
        throws CallStateException
    {
        this.mActivePhone.explicitCallTransfer();
    }

    public String getActiveApnHost(String paramString)
    {
        return this.mActivePhone.getActiveApnHost(paramString);
    }

    public String[] getActiveApnTypes()
    {
        return this.mActivePhone.getActiveApnTypes();
    }

    public Phone getActivePhone()
    {
        return this.mActivePhone;
    }

    public void getAvailableNetworks(Message paramMessage)
    {
        this.mActivePhone.getAvailableNetworks(paramMessage);
    }

    public Call getBackgroundCall()
    {
        return this.mActivePhone.getBackgroundCall();
    }

    public boolean getCallForwardingIndicator()
    {
        return this.mActivePhone.getCallForwardingIndicator();
    }

    public void getCallForwardingOption(int paramInt, Message paramMessage)
    {
        this.mActivePhone.getCallForwardingOption(paramInt, paramMessage);
    }

    public void getCallWaiting(Message paramMessage)
    {
        this.mActivePhone.getCallWaiting(paramMessage);
    }

    public int getCdmaEriIconIndex()
    {
        return this.mActivePhone.getCdmaEriIconIndex();
    }

    public int getCdmaEriIconMode()
    {
        return this.mActivePhone.getCdmaEriIconMode();
    }

    public String getCdmaEriText()
    {
        return this.mActivePhone.getCdmaEriText();
    }

    public String getCdmaMin()
    {
        return this.mActivePhone.getCdmaMin();
    }

    public String getCdmaPrlVersion()
    {
        return this.mActivePhone.getCdmaPrlVersion();
    }

    public void getCellBroadcastSmsConfig(Message paramMessage)
    {
        this.mActivePhone.getCellBroadcastSmsConfig(paramMessage);
    }

    public CellLocation getCellLocation()
    {
        return this.mActivePhone.getCellLocation();
    }

    public Context getContext()
    {
        return this.mActivePhone.getContext();
    }

    public Phone.DataActivityState getDataActivityState()
    {
        return this.mActivePhone.getDataActivityState();
    }

    public void getDataCallList(Message paramMessage)
    {
        this.mActivePhone.getDataCallList(paramMessage);
    }

    public Phone.DataState getDataConnectionState()
    {
        return this.mActivePhone.getDataConnectionState("default");
    }

    public Phone.DataState getDataConnectionState(String paramString)
    {
        return this.mActivePhone.getDataConnectionState(paramString);
    }

    public boolean getDataRoamingEnabled()
    {
        return this.mActivePhone.getDataRoamingEnabled();
    }

    public String getDeviceId()
    {
        return this.mActivePhone.getDeviceId();
    }

    public String getDeviceSvn()
    {
        return this.mActivePhone.getDeviceSvn();
    }

    public void getEnhancedVoicePrivacy(Message paramMessage)
    {
        this.mActivePhone.getEnhancedVoicePrivacy(paramMessage);
    }

    public String getEsn()
    {
        return this.mActivePhone.getEsn();
    }

    public Call getForegroundCall()
    {
        return this.mActivePhone.getForegroundCall();
    }

    public IccCard getIccCard()
    {
        return this.mActivePhone.getIccCard();
    }

    public IccPhoneBookInterfaceManager getIccPhoneBookInterfaceManager()
    {
        return this.mActivePhone.getIccPhoneBookInterfaceManager();
    }

    public boolean getIccRecordsLoaded()
    {
        return this.mActivePhone.getIccRecordsLoaded();
    }

    public String getIccSerialNumber()
    {
        return this.mActivePhone.getIccSerialNumber();
    }

    public IccSmsInterfaceManager getIccSmsInterfaceManager()
    {
        return this.mActivePhone.getIccSmsInterfaceManager();
    }

    public String getImei()
    {
        return this.mActivePhone.getImei();
    }

    public IsimRecords getIsimRecords()
    {
        return this.mActivePhone.getIsimRecords();
    }

    public String getLine1AlphaTag()
    {
        return this.mActivePhone.getLine1AlphaTag();
    }

    public String getLine1Number()
    {
        return this.mActivePhone.getLine1Number();
    }

    public LinkCapabilities getLinkCapabilities(String paramString)
    {
        return this.mActivePhone.getLinkCapabilities(paramString);
    }

    public LinkProperties getLinkProperties(String paramString)
    {
        return this.mActivePhone.getLinkProperties(paramString);
    }

    public int getLteOnCdmaMode()
    {
        return this.mActivePhone.getLteOnCdmaMode();
    }

    public String getMeid()
    {
        return this.mActivePhone.getMeid();
    }

    public boolean getMessageWaitingIndicator()
    {
        return this.mActivePhone.getMessageWaitingIndicator();
    }

    public String getMsisdn()
    {
        return this.mActivePhone.getMsisdn();
    }

    public boolean getMute()
    {
        return this.mActivePhone.getMute();
    }

    public void getNeighboringCids(Message paramMessage)
    {
        this.mActivePhone.getNeighboringCids(paramMessage);
    }

    public void getOutgoingCallerIdDisplay(Message paramMessage)
    {
        this.mActivePhone.getOutgoingCallerIdDisplay(paramMessage);
    }

    public List<? extends MmiCode> getPendingMmiCodes()
    {
        return this.mActivePhone.getPendingMmiCodes();
    }

    public String getPhoneName()
    {
        return this.mActivePhone.getPhoneName();
    }

    public PhoneSubInfo getPhoneSubInfo()
    {
        return this.mActivePhone.getPhoneSubInfo();
    }

    public int getPhoneType()
    {
        return this.mActivePhone.getPhoneType();
    }

    public void getPreferredNetworkType(Message paramMessage)
    {
        this.mActivePhone.getPreferredNetworkType(paramMessage);
    }

    public Call getRingingCall()
    {
        return this.mActivePhone.getRingingCall();
    }

    public ServiceState getServiceState()
    {
        return this.mActivePhone.getServiceState();
    }

    public SignalStrength getSignalStrength()
    {
        return this.mActivePhone.getSignalStrength();
    }

    public SimulatedRadioControl getSimulatedRadioControl()
    {
        return this.mActivePhone.getSimulatedRadioControl();
    }

    public void getSmscAddress(Message paramMessage)
    {
        this.mActivePhone.getSmscAddress(paramMessage);
    }

    public Phone.State getState()
    {
        return this.mActivePhone.getState();
    }

    public String getSubscriberId()
    {
        return this.mActivePhone.getSubscriberId();
    }

    public boolean getUnitTestMode()
    {
        return this.mActivePhone.getUnitTestMode();
    }

    public UsimServiceTable getUsimServiceTable()
    {
        return this.mActivePhone.getUsimServiceTable();
    }

    public String getVoiceMailAlphaTag()
    {
        return this.mActivePhone.getVoiceMailAlphaTag();
    }

    public String getVoiceMailNumber()
    {
        return this.mActivePhone.getVoiceMailNumber();
    }

    public int getVoiceMessageCount()
    {
        return this.mActivePhone.getVoiceMessageCount();
    }

    public boolean handleInCallMmiCommands(String paramString)
        throws CallStateException
    {
        return this.mActivePhone.handleInCallMmiCommands(paramString);
    }

    public void handleMessage(Message paramMessage)
    {
        AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
        switch (paramMessage.what)
        {
        default:
            loge("Error! This handler was not registered for this message type. Message: " + paramMessage.what);
        case 2:
        case 4:
        case 1:
        case 3:
        }
        while (true)
        {
            super.handleMessage(paramMessage);
            return;
            this.mCommandsInterface.getVoiceRadioTechnology(obtainMessage(3));
            continue;
            if ((localAsyncResult.exception == null) && (localAsyncResult.result != null))
            {
                this.mRilVersion = ((Integer)localAsyncResult.result).intValue();
            }
            else
            {
                logd("Unexpected exception on EVENT_RIL_CONNECTED");
                this.mRilVersion = -1;
                continue;
                if (localAsyncResult.exception == null)
                {
                    if ((localAsyncResult.result != null) && (((int[])localAsyncResult.result).length != 0))
                        updatePhoneObject(((int[])(int[])localAsyncResult.result)[0]);
                    else
                        loge("Voice Radio Technology event " + paramMessage.what + " has no tech!");
                }
                else
                    loge("Voice Radio Technology event " + paramMessage.what + " exception!" + localAsyncResult.exception);
            }
        }
    }

    public boolean handlePinMmi(String paramString)
    {
        return this.mActivePhone.handlePinMmi(paramString);
    }

    public void invokeOemRilRequestRaw(byte[] paramArrayOfByte, Message paramMessage)
    {
        this.mActivePhone.invokeOemRilRequestRaw(paramArrayOfByte, paramMessage);
    }

    public void invokeOemRilRequestStrings(String[] paramArrayOfString, Message paramMessage)
    {
        this.mActivePhone.invokeOemRilRequestStrings(paramArrayOfString, paramMessage);
    }

    public boolean isCspPlmnEnabled()
    {
        return this.mActivePhone.isCspPlmnEnabled();
    }

    public boolean isDataConnectivityPossible()
    {
        return this.mActivePhone.isDataConnectivityPossible("default");
    }

    public boolean isDataConnectivityPossible(String paramString)
    {
        return this.mActivePhone.isDataConnectivityPossible(paramString);
    }

    public boolean isDnsCheckDisabled()
    {
        return this.mActivePhone.isDnsCheckDisabled();
    }

    public boolean isMinInfoReady()
    {
        return this.mActivePhone.isMinInfoReady();
    }

    public boolean isOtaSpNumber(String paramString)
    {
        return this.mActivePhone.isOtaSpNumber(paramString);
    }

    public boolean needsOtaServiceProvisioning()
    {
        return this.mActivePhone.needsOtaServiceProvisioning();
    }

    public void notifyDataActivity()
    {
        this.mActivePhone.notifyDataActivity();
    }

    public void queryAvailableBandMode(Message paramMessage)
    {
        this.mActivePhone.queryAvailableBandMode(paramMessage);
    }

    public void queryCdmaRoamingPreference(Message paramMessage)
    {
        this.mActivePhone.queryCdmaRoamingPreference(paramMessage);
    }

    public void queryTTYMode(Message paramMessage)
    {
        this.mActivePhone.queryTTYMode(paramMessage);
    }

    public void registerFoT53ClirlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerFoT53ClirlInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForCallWaiting(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForCallWaiting(paramHandler, paramInt, paramObject);
    }

    public void registerForCdmaOtaStatusChange(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForCdmaOtaStatusChange(paramHandler, paramInt, paramObject);
    }

    public void registerForDisconnect(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForDisconnect(paramHandler, paramInt, paramObject);
    }

    public void registerForDisplayInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForDisplayInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForEcmTimerReset(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForEcmTimerReset(paramHandler, paramInt, paramObject);
    }

    public void registerForInCallVoicePrivacyOff(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForInCallVoicePrivacyOff(paramHandler, paramInt, paramObject);
    }

    public void registerForInCallVoicePrivacyOn(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForInCallVoicePrivacyOn(paramHandler, paramInt, paramObject);
    }

    public void registerForIncomingRing(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForIncomingRing(paramHandler, paramInt, paramObject);
    }

    public void registerForLineControlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForLineControlInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForMmiComplete(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForMmiComplete(paramHandler, paramInt, paramObject);
    }

    public void registerForMmiInitiate(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForMmiInitiate(paramHandler, paramInt, paramObject);
    }

    public void registerForNewRingingConnection(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForNewRingingConnection(paramHandler, paramInt, paramObject);
    }

    public void registerForNumberInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForNumberInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForPreciseCallStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForPreciseCallStateChanged(paramHandler, paramInt, paramObject);
    }

    public void registerForRedirectedNumberInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForRedirectedNumberInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForResendIncallMute(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForResendIncallMute(paramHandler, paramInt, paramObject);
    }

    public void registerForRingbackTone(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForRingbackTone(paramHandler, paramInt, paramObject);
    }

    public void registerForServiceStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForServiceStateChanged(paramHandler, paramInt, paramObject);
    }

    public void registerForSignalInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForSignalInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForSubscriptionInfoReady(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForSubscriptionInfoReady(paramHandler, paramInt, paramObject);
    }

    public void registerForSuppServiceFailed(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForSuppServiceFailed(paramHandler, paramInt, paramObject);
    }

    public void registerForSuppServiceNotification(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForSuppServiceNotification(paramHandler, paramInt, paramObject);
    }

    public void registerForT53AudioControlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForT53AudioControlInfo(paramHandler, paramInt, paramObject);
    }

    public void registerForUnknownConnection(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.registerForUnknownConnection(paramHandler, paramInt, paramObject);
    }

    public void rejectCall()
        throws CallStateException
    {
        this.mActivePhone.rejectCall();
    }

    public void removeReferences()
    {
        this.mActivePhone = null;
        this.mCommandsInterface = null;
    }

    public void requestIsimAuthentication(String paramString, Message paramMessage)
    {
        this.mActivePhone.requestIsimAuthentication(paramString, paramMessage);
    }

    public void selectNetworkManually(OperatorInfo paramOperatorInfo, Message paramMessage)
    {
        this.mActivePhone.selectNetworkManually(paramOperatorInfo, paramMessage);
    }

    public void sendBurstDtmf(String paramString, int paramInt1, int paramInt2, Message paramMessage)
    {
        this.mActivePhone.sendBurstDtmf(paramString, paramInt1, paramInt2, paramMessage);
    }

    public void sendDtmf(char paramChar)
    {
        this.mActivePhone.sendDtmf(paramChar);
    }

    public void sendUssdResponse(String paramString)
    {
        this.mActivePhone.sendUssdResponse(paramString);
    }

    public void setBandMode(int paramInt, Message paramMessage)
    {
        this.mActivePhone.setBandMode(paramInt, paramMessage);
    }

    public void setCallForwardingOption(int paramInt1, int paramInt2, String paramString, int paramInt3, Message paramMessage)
    {
        this.mActivePhone.setCallForwardingOption(paramInt1, paramInt2, paramString, paramInt3, paramMessage);
    }

    public void setCallWaiting(boolean paramBoolean, Message paramMessage)
    {
        this.mActivePhone.setCallWaiting(paramBoolean, paramMessage);
    }

    public void setCdmaRoamingPreference(int paramInt, Message paramMessage)
    {
        this.mActivePhone.setCdmaRoamingPreference(paramInt, paramMessage);
    }

    public void setCdmaSubscription(int paramInt, Message paramMessage)
    {
        this.mActivePhone.setCdmaSubscription(paramInt, paramMessage);
    }

    public void setCellBroadcastSmsConfig(int[] paramArrayOfInt, Message paramMessage)
    {
        this.mActivePhone.setCellBroadcastSmsConfig(paramArrayOfInt, paramMessage);
    }

    public void setDataRoamingEnabled(boolean paramBoolean)
    {
        this.mActivePhone.setDataRoamingEnabled(paramBoolean);
    }

    public void setEchoSuppressionEnabled(boolean paramBoolean)
    {
        this.mActivePhone.setEchoSuppressionEnabled(paramBoolean);
    }

    public void setLine1Number(String paramString1, String paramString2, Message paramMessage)
    {
        this.mActivePhone.setLine1Number(paramString1, paramString2, paramMessage);
    }

    public void setMute(boolean paramBoolean)
    {
        this.mActivePhone.setMute(paramBoolean);
    }

    public void setNetworkSelectionModeAutomatic(Message paramMessage)
    {
        this.mActivePhone.setNetworkSelectionModeAutomatic(paramMessage);
    }

    public void setOnEcbModeExitResponse(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.setOnEcbModeExitResponse(paramHandler, paramInt, paramObject);
    }

    public void setOnPostDialCharacter(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mActivePhone.setOnPostDialCharacter(paramHandler, paramInt, paramObject);
    }

    public void setOutgoingCallerIdDisplay(int paramInt, Message paramMessage)
    {
        this.mActivePhone.setOutgoingCallerIdDisplay(paramInt, paramMessage);
    }

    public void setPreferredNetworkType(int paramInt, Message paramMessage)
    {
        this.mActivePhone.setPreferredNetworkType(paramInt, paramMessage);
    }

    public void setRadioPower(boolean paramBoolean)
    {
        this.mActivePhone.setRadioPower(paramBoolean);
    }

    public void setSmscAddress(String paramString, Message paramMessage)
    {
        this.mActivePhone.setSmscAddress(paramString, paramMessage);
    }

    public void setTTYMode(int paramInt, Message paramMessage)
    {
        this.mActivePhone.setTTYMode(paramInt, paramMessage);
    }

    public void setUnitTestMode(boolean paramBoolean)
    {
        this.mActivePhone.setUnitTestMode(paramBoolean);
    }

    public void setVoiceMailNumber(String paramString1, String paramString2, Message paramMessage)
    {
        this.mActivePhone.setVoiceMailNumber(paramString1, paramString2, paramMessage);
    }

    public void setVoiceMessageWaiting(int paramInt1, int paramInt2)
    {
        this.mActivePhone.setVoiceMessageWaiting(paramInt1, paramInt2);
    }

    public void startDtmf(char paramChar)
    {
        this.mActivePhone.startDtmf(paramChar);
    }

    public void stopDtmf()
    {
        this.mActivePhone.stopDtmf();
    }

    public void switchHoldingAndActive()
        throws CallStateException
    {
        this.mActivePhone.switchHoldingAndActive();
    }

    public void unregisterForCallWaiting(Handler paramHandler)
    {
        this.mActivePhone.unregisterForCallWaiting(paramHandler);
    }

    public void unregisterForCdmaOtaStatusChange(Handler paramHandler)
    {
        this.mActivePhone.unregisterForCdmaOtaStatusChange(paramHandler);
    }

    public void unregisterForDisconnect(Handler paramHandler)
    {
        this.mActivePhone.unregisterForDisconnect(paramHandler);
    }

    public void unregisterForDisplayInfo(Handler paramHandler)
    {
        this.mActivePhone.unregisterForDisplayInfo(paramHandler);
    }

    public void unregisterForEcmTimerReset(Handler paramHandler)
    {
        this.mActivePhone.unregisterForEcmTimerReset(paramHandler);
    }

    public void unregisterForInCallVoicePrivacyOff(Handler paramHandler)
    {
        this.mActivePhone.unregisterForInCallVoicePrivacyOff(paramHandler);
    }

    public void unregisterForInCallVoicePrivacyOn(Handler paramHandler)
    {
        this.mActivePhone.unregisterForInCallVoicePrivacyOn(paramHandler);
    }

    public void unregisterForIncomingRing(Handler paramHandler)
    {
        this.mActivePhone.unregisterForIncomingRing(paramHandler);
    }

    public void unregisterForLineControlInfo(Handler paramHandler)
    {
        this.mActivePhone.unregisterForLineControlInfo(paramHandler);
    }

    public void unregisterForMmiComplete(Handler paramHandler)
    {
        this.mActivePhone.unregisterForMmiComplete(paramHandler);
    }

    public void unregisterForMmiInitiate(Handler paramHandler)
    {
        this.mActivePhone.unregisterForMmiInitiate(paramHandler);
    }

    public void unregisterForNewRingingConnection(Handler paramHandler)
    {
        this.mActivePhone.unregisterForNewRingingConnection(paramHandler);
    }

    public void unregisterForNumberInfo(Handler paramHandler)
    {
        this.mActivePhone.unregisterForNumberInfo(paramHandler);
    }

    public void unregisterForPreciseCallStateChanged(Handler paramHandler)
    {
        this.mActivePhone.unregisterForPreciseCallStateChanged(paramHandler);
    }

    public void unregisterForRedirectedNumberInfo(Handler paramHandler)
    {
        this.mActivePhone.unregisterForRedirectedNumberInfo(paramHandler);
    }

    public void unregisterForResendIncallMute(Handler paramHandler)
    {
        this.mActivePhone.unregisterForResendIncallMute(paramHandler);
    }

    public void unregisterForRingbackTone(Handler paramHandler)
    {
        this.mActivePhone.unregisterForRingbackTone(paramHandler);
    }

    public void unregisterForServiceStateChanged(Handler paramHandler)
    {
        this.mActivePhone.unregisterForServiceStateChanged(paramHandler);
    }

    public void unregisterForSignalInfo(Handler paramHandler)
    {
        this.mActivePhone.unregisterForSignalInfo(paramHandler);
    }

    public void unregisterForSubscriptionInfoReady(Handler paramHandler)
    {
        this.mActivePhone.unregisterForSubscriptionInfoReady(paramHandler);
    }

    public void unregisterForSuppServiceFailed(Handler paramHandler)
    {
        this.mActivePhone.unregisterForSuppServiceFailed(paramHandler);
    }

    public void unregisterForSuppServiceNotification(Handler paramHandler)
    {
        this.mActivePhone.unregisterForSuppServiceNotification(paramHandler);
    }

    public void unregisterForT53AudioControlInfo(Handler paramHandler)
    {
        this.mActivePhone.unregisterForT53AudioControlInfo(paramHandler);
    }

    public void unregisterForT53ClirInfo(Handler paramHandler)
    {
        this.mActivePhone.unregisterForT53ClirInfo(paramHandler);
    }

    public void unregisterForUnknownConnection(Handler paramHandler)
    {
        this.mActivePhone.unregisterForUnknownConnection(paramHandler);
    }

    public void unsetOnEcbModeExitResponse(Handler paramHandler)
    {
        this.mActivePhone.unsetOnEcbModeExitResponse(paramHandler);
    }

    public void updateServiceLocation()
    {
        this.mActivePhone.updateServiceLocation();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.PhoneProxy
 * JD-Core Version:        0.6.2
 */