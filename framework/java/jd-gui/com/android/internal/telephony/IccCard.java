package com.android.internal.telephony;

import android.app.ActivityManagerNative;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.Registrant;
import android.os.RegistrantList;
import android.util.Log;
import android.view.Window;
import com.android.internal.telephony.cat.CatService;
import com.android.internal.telephony.cdma.CDMALTEPhone;
import com.android.internal.telephony.cdma.CdmaLteUiccFileHandler;
import com.android.internal.telephony.cdma.CdmaLteUiccRecords;
import com.android.internal.telephony.cdma.CdmaSubscriptionSourceManager;
import com.android.internal.telephony.cdma.RuimFileHandler;
import com.android.internal.telephony.cdma.RuimRecords;
import com.android.internal.telephony.gsm.SIMFileHandler;
import com.android.internal.telephony.gsm.SIMRecords;

public class IccCard
{
    public static final boolean CARD_IS_3GPP = true;
    public static final boolean CARD_IS_NOT_3GPP = false;
    private static final int EVENT_CARD_ADDED = 14;
    private static final int EVENT_CARD_REMOVED = 13;
    protected static final int EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED = 15;
    private static final int EVENT_CHANGE_FACILITY_FDN_DONE = 11;
    private static final int EVENT_CHANGE_FACILITY_LOCK_DONE = 8;
    private static final int EVENT_CHANGE_ICC_PASSWORD_DONE = 9;
    private static final int EVENT_GET_ICC_STATUS_DONE = 2;
    protected static final int EVENT_ICC_LOCKED = 1;
    protected static final int EVENT_ICC_READY = 6;
    private static final int EVENT_ICC_STATUS_CHANGED = 12;
    private static final int EVENT_PINPUK_DONE = 4;
    private static final int EVENT_QUERY_FACILITY_FDN_DONE = 10;
    private static final int EVENT_QUERY_FACILITY_LOCK_DONE = 7;
    protected static final int EVENT_RADIO_OFF_OR_NOT_AVAILABLE = 3;
    protected static final int EVENT_RADIO_ON = 16;
    private static final int EVENT_REPOLL_STATUS_DONE = 5;
    public static final String INTENT_KEY_ICC_STATE = "ss";
    public static final String INTENT_KEY_LOCKED_REASON = "reason";
    public static final String INTENT_VALUE_ABSENT_ON_PERM_DISABLED = "PERM_DISABLED";
    public static final String INTENT_VALUE_ICC_ABSENT = "ABSENT";
    public static final String INTENT_VALUE_ICC_IMSI = "IMSI";
    public static final String INTENT_VALUE_ICC_LOADED = "LOADED";
    public static final String INTENT_VALUE_ICC_LOCKED = "LOCKED";
    public static final String INTENT_VALUE_ICC_NOT_READY = "NOT_READY";
    public static final String INTENT_VALUE_ICC_READY = "READY";
    public static final String INTENT_VALUE_LOCKED_NETWORK = "NETWORK";
    public static final String INTENT_VALUE_LOCKED_ON_PIN = "PIN";
    public static final String INTENT_VALUE_LOCKED_ON_PUK = "PUK";
    protected boolean is3gpp = true;
    protected boolean isSubscriptionFromIccCard = true;
    private RegistrantList mAbsentRegistrants = new RegistrantList();
    private CatService mCatService;
    protected CdmaSubscriptionSourceManager mCdmaSSM = null;
    protected boolean mDbg;
    private boolean mDesiredFdnEnabled;
    private boolean mDesiredPinLocked;
    protected Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            if (!IccCard.this.mPhone.mIsTheCurrentActivePhone)
                Log.e(IccCard.this.mLogTag, "Received message " + paramAnonymousMessage + "[" + paramAnonymousMessage.what + "] while being destroyed. Ignoring.");
            while (true)
            {
                return;
                switch (paramAnonymousMessage.what)
                {
                default:
                    Log.e(IccCard.this.mLogTag, "[IccCard] Unknown Event " + paramAnonymousMessage.what);
                    break;
                case 3:
                    IccCard.this.mState = null;
                    IccCard.this.updateStateProperty();
                    IccCard.this.broadcastIccStateChangedIntent("NOT_READY", null);
                    break;
                case 16:
                    if (!IccCard.this.is3gpp)
                        IccCard.this.handleCdmaSubscriptionSource();
                    IccCard.this.mPhone.mCM.getIccCardStatus(obtainMessage(2));
                    break;
                case 15:
                    IccCard.this.handleCdmaSubscriptionSource();
                    break;
                case 6:
                    if (IccCard.this.isSubscriptionFromIccCard)
                    {
                        IccCard.this.mPhone.mCM.queryFacilityLock("SC", "", 7, obtainMessage(7));
                        IccCard.this.mPhone.mCM.queryFacilityLock("FD", "", 7, obtainMessage(10));
                    }
                    break;
                case 1:
                    IccCard.this.mPhone.mCM.queryFacilityLock("SC", "", 7, obtainMessage(7));
                    break;
                case 2:
                    AsyncResult localAsyncResult8 = (AsyncResult)paramAnonymousMessage.obj;
                    IccCard.this.getIccCardStatusDone(localAsyncResult8);
                    break;
                case 4:
                    AsyncResult localAsyncResult7 = (AsyncResult)paramAnonymousMessage.obj;
                    AsyncResult.forMessage((Message)localAsyncResult7.userObj).exception = localAsyncResult7.exception;
                    IccCard.this.mPhone.mCM.getIccCardStatus(obtainMessage(5, localAsyncResult7.userObj));
                    break;
                case 5:
                    AsyncResult localAsyncResult6 = (AsyncResult)paramAnonymousMessage.obj;
                    IccCard.this.getIccCardStatusDone(localAsyncResult6);
                    ((Message)localAsyncResult6.userObj).sendToTarget();
                    break;
                case 7:
                    AsyncResult localAsyncResult5 = (AsyncResult)paramAnonymousMessage.obj;
                    IccCard.this.onQueryFacilityLock(localAsyncResult5);
                    break;
                case 10:
                    AsyncResult localAsyncResult4 = (AsyncResult)paramAnonymousMessage.obj;
                    IccCard.this.onQueryFdnEnabled(localAsyncResult4);
                    break;
                case 8:
                    AsyncResult localAsyncResult3 = (AsyncResult)paramAnonymousMessage.obj;
                    if (localAsyncResult3.exception == null)
                    {
                        IccCard.access$502(IccCard.this, IccCard.this.mDesiredPinLocked);
                        if (IccCard.this.mDbg)
                            IccCard.this.log("EVENT_CHANGE_FACILITY_LOCK_DONE: mIccPinLocked= " + IccCard.this.mIccPinLocked);
                    }
                    while (true)
                    {
                        AsyncResult.forMessage((Message)localAsyncResult3.userObj).exception = localAsyncResult3.exception;
                        ((Message)localAsyncResult3.userObj).sendToTarget();
                        break;
                        Log.e(IccCard.this.mLogTag, "Error change facility lock with exception " + localAsyncResult3.exception);
                    }
                case 11:
                    AsyncResult localAsyncResult2 = (AsyncResult)paramAnonymousMessage.obj;
                    if (localAsyncResult2.exception == null)
                    {
                        IccCard.access$702(IccCard.this, IccCard.this.mDesiredFdnEnabled);
                        if (IccCard.this.mDbg)
                            IccCard.this.log("EVENT_CHANGE_FACILITY_FDN_DONE: mIccFdnEnabled=" + IccCard.this.mIccFdnEnabled);
                    }
                    while (true)
                    {
                        AsyncResult.forMessage((Message)localAsyncResult2.userObj).exception = localAsyncResult2.exception;
                        ((Message)localAsyncResult2.userObj).sendToTarget();
                        break;
                        Log.e(IccCard.this.mLogTag, "Error change facility fdn with exception " + localAsyncResult2.exception);
                    }
                case 9:
                    AsyncResult localAsyncResult1 = (AsyncResult)paramAnonymousMessage.obj;
                    if (localAsyncResult1.exception != null)
                        Log.e(IccCard.this.mLogTag, "Error in change sim password with exception" + localAsyncResult1.exception);
                    AsyncResult.forMessage((Message)localAsyncResult1.userObj).exception = localAsyncResult1.exception;
                    ((Message)localAsyncResult1.userObj).sendToTarget();
                    break;
                case 12:
                    Log.d(IccCard.this.mLogTag, "Received Event EVENT_ICC_STATUS_CHANGED");
                    IccCard.this.mPhone.mCM.getIccCardStatus(obtainMessage(2));
                    break;
                case 13:
                    IccCard.this.onIccSwap(false);
                    break;
                case 14:
                    IccCard.this.onIccSwap(true);
                }
            }
        }
    };
    protected IccCardStatus mIccCardStatus = null;
    private boolean mIccFdnEnabled = false;
    private IccFileHandler mIccFileHandler;
    private boolean mIccPinLocked = true;
    private IccRecords mIccRecords;
    protected String mLogTag;
    private RegistrantList mNetworkLockedRegistrants = new RegistrantList();
    protected PhoneBase mPhone;
    private RegistrantList mPinLockedRegistrants = new RegistrantList();
    protected RegistrantList mReadyRegistrants = new RegistrantList();
    protected RegistrantList mRuimReadyRegistrants = new RegistrantList();
    protected State mState = null;
    private final Object mStateMonitor = new Object();

    public IccCard(PhoneBase paramPhoneBase, String paramString, Boolean paramBoolean1, Boolean paramBoolean2)
    {
        this.mLogTag = paramString;
        this.mDbg = paramBoolean2.booleanValue();
        StringBuilder localStringBuilder;
        if (this.mDbg)
        {
            localStringBuilder = new StringBuilder().append("[IccCard] Creating card type ");
            if (!paramBoolean1.booleanValue())
                break label372;
        }
        label372: for (String str = "3gpp"; ; str = "3gpp2")
        {
            log(str);
            this.mPhone = paramPhoneBase;
            this.is3gpp = paramBoolean1.booleanValue();
            this.mCdmaSSM = CdmaSubscriptionSourceManager.getInstance(this.mPhone.getContext(), this.mPhone.mCM, this.mHandler, 15, null);
            if ((paramPhoneBase.mCM.getLteOnCdmaMode() != 1) || (!(paramPhoneBase instanceof CDMALTEPhone)))
                break;
            this.mIccFileHandler = new CdmaLteUiccFileHandler(this, "", this.mPhone.mCM);
            this.mIccRecords = new CdmaLteUiccRecords(this, this.mPhone.mContext, this.mPhone.mCM);
            this.mCatService = CatService.getInstance(this.mPhone.mCM, this.mIccRecords, this.mPhone.mContext, this.mIccFileHandler, this);
            this.mPhone.mCM.registerForOffOrNotAvailable(this.mHandler, 3, null);
            this.mPhone.mCM.registerForOn(this.mHandler, 16, null);
            this.mPhone.mCM.registerForIccStatusChanged(this.mHandler, 12, null);
            return;
        }
        Object localObject1;
        if (paramBoolean1.booleanValue())
        {
            localObject1 = new SIMFileHandler(this, "", this.mPhone.mCM);
            label405: this.mIccFileHandler = ((IccFileHandler)localObject1);
            if (!paramBoolean1.booleanValue())
                break label473;
        }
        label473: for (Object localObject2 = new SIMRecords(this, this.mPhone.mContext, this.mPhone.mCM); ; localObject2 = new RuimRecords(this, this.mPhone.mContext, this.mPhone.mCM))
        {
            this.mIccRecords = ((IccRecords)localObject2);
            break;
            localObject1 = new RuimFileHandler(this, "", this.mPhone.mCM);
            break label405;
        }
    }

    private State getAppState(int paramInt)
    {
        IccCardApplication localIccCardApplication;
        State localState;
        if ((paramInt >= 0) && (paramInt < 8))
        {
            localIccCardApplication = this.mIccCardStatus.getApplication(paramInt);
            if (localIccCardApplication != null)
                break label77;
            Log.e(this.mLogTag, "[IccCard] Subscription Application in not present");
            localState = State.ABSENT;
        }
        while (true)
        {
            return localState;
            Log.e(this.mLogTag, "[IccCard] Invalid Subscription Application index:" + paramInt);
            localState = State.ABSENT;
            continue;
            label77: if (localIccCardApplication.pin1.isPermBlocked())
                localState = State.PERM_DISABLED;
            else if (localIccCardApplication.app_state.isPinRequired())
                localState = State.PIN_REQUIRED;
            else if (localIccCardApplication.app_state.isPukRequired())
                localState = State.PUK_REQUIRED;
            else if (localIccCardApplication.app_state.isSubscriptionPersoEnabled())
                localState = State.NETWORK_LOCKED;
            else if (localIccCardApplication.app_state.isAppReady())
                localState = State.READY;
            else if (localIccCardApplication.app_state.isAppNotReady())
                localState = State.NOT_READY;
            else
                localState = State.NOT_READY;
        }
    }

    private State getConsolidatedState(State paramState1, State paramState2, State paramState3)
    {
        if (paramState2 == State.ABSENT);
        while (true)
        {
            return paramState1;
            if (paramState1 == State.ABSENT)
                paramState1 = paramState2;
            else if ((paramState1 == State.READY) && (paramState2 == State.READY))
                paramState1 = State.READY;
            else if (((paramState2 == State.NOT_READY) && (paramState1 == State.READY)) || ((paramState1 == State.NOT_READY) && (paramState2 == State.READY)))
                paramState1 = State.NOT_READY;
            else if (paramState2 != State.NOT_READY)
                if (paramState1 == State.NOT_READY)
                    paramState1 = paramState2;
                else
                    paramState1 = paramState3;
        }
    }

    private void getIccCardStatusDone(AsyncResult paramAsyncResult)
    {
        if (paramAsyncResult.exception != null)
            Log.e(this.mLogTag, "Error getting ICC status. RIL_REQUEST_GET_ICC_STATUS should never return an error", paramAsyncResult.exception);
        while (true)
        {
            return;
            handleIccCardStatus((IccCardStatus)paramAsyncResult.result);
        }
    }

    private void handleCdmaSubscriptionSource()
    {
        if (this.mCdmaSSM != null)
        {
            int i = this.mCdmaSSM.getCdmaSubscriptionSource();
            Log.d(this.mLogTag, "Received Cdma subscription source: " + i);
            if (i != 0)
                break label71;
        }
        label71: for (boolean bool = true; ; bool = false)
        {
            if (bool != this.isSubscriptionFromIccCard)
            {
                this.isSubscriptionFromIccCard = bool;
                handleIccCardStatus(this.mIccCardStatus);
            }
            return;
        }
    }

    private void handleIccCardStatus(IccCardStatus paramIccCardStatus)
    {
        State localState1 = getRuimState();
        State localState2 = this.mState;
        this.mIccCardStatus = paramIccCardStatus;
        State localState3 = getIccCardState();
        label406: label412: label546: 
        while (true)
        {
            synchronized (this.mStateMonitor)
            {
                this.mState = localState3;
                updateStateProperty();
                if ((localState2 != State.READY) && (localState3 == State.READY))
                {
                    this.mHandler.sendMessage(this.mHandler.obtainMessage(6));
                    this.mReadyRegistrants.notifyRegistrants();
                    if ((localState1 != State.READY) && (getRuimState() == State.READY))
                        this.mRuimReadyRegistrants.notifyRegistrants();
                    if (((localState2 != State.PIN_REQUIRED) && (localState3 == State.PIN_REQUIRED)) || ((localState2 != State.PUK_REQUIRED) && (localState3 == State.PUK_REQUIRED)))
                    {
                        i = 1;
                        if ((localState2 == State.ABSENT) || (localState3 != State.ABSENT))
                            break label406;
                        j = 1;
                        if ((localState2 == State.NETWORK_LOCKED) || (localState3 != State.NETWORK_LOCKED))
                            break label412;
                        k = 1;
                        if ((localState2 == State.PERM_DISABLED) || (localState3 != State.PERM_DISABLED))
                            break label418;
                        m = 1;
                        if ((localState2 == null) || (!localState2.iccCardExist()) || (localState3 != State.ABSENT))
                            break label424;
                        n = 1;
                        if ((localState2 != State.ABSENT) || (localState3 == null) || (!localState3.iccCardExist()))
                            break label430;
                        i1 = 1;
                        if (i == 0)
                            break label443;
                        if (this.mDbg)
                            log("Notify SIM pin or puk locked.");
                        this.mPinLockedRegistrants.notifyRegistrants();
                        if (localState3 != State.PIN_REQUIRED)
                            break label436;
                        str = "PIN";
                        broadcastIccStateChangedIntent("LOCKED", str);
                        if (n == 0)
                            break label546;
                        this.mHandler.sendMessage(this.mHandler.obtainMessage(13, null));
                        if ((localState2 != State.READY) && (localState3 == State.READY) && ((this.is3gpp) || (this.isSubscriptionFromIccCard)))
                        {
                            if (!(this.mIccFileHandler instanceof CdmaLteUiccFileHandler))
                                this.mIccFileHandler.setAid(getAid());
                            this.mIccRecords.onReady();
                        }
                    }
                }
                else
                {
                    if (!localState3.isPinLocked())
                        continue;
                    this.mHandler.sendMessage(this.mHandler.obtainMessage(1));
                }
            }
            int i = 0;
            continue;
            int j = 0;
            continue;
            int k = 0;
            continue;
            label418: int m = 0;
            continue;
            label424: int n = 0;
            continue;
            label430: int i1 = 0;
            continue;
            label436: String str = "PUK";
            continue;
            label443: if (j != 0)
            {
                if (this.mDbg)
                    log("Notify SIM missing.");
                this.mAbsentRegistrants.notifyRegistrants();
                broadcastIccStateChangedIntent("ABSENT", null);
            }
            else if (k != 0)
            {
                if (this.mDbg)
                    log("Notify SIM network locked.");
                this.mNetworkLockedRegistrants.notifyRegistrants();
                broadcastIccStateChangedIntent("LOCKED", "NETWORK");
            }
            else if (m != 0)
            {
                if (this.mDbg)
                    log("Notify SIM permanently disabled.");
                broadcastIccStateChangedIntent("ABSENT", "PERM_DISABLED");
                continue;
                if (i1 != 0)
                    this.mHandler.sendMessage(this.mHandler.obtainMessage(14, null));
            }
        }
    }

    private void log(String paramString)
    {
        Log.d(this.mLogTag, "[IccCard] " + paramString);
    }

    private void onIccSwap(boolean paramBoolean)
    {
        DialogInterface.OnClickListener local1 = new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
                if (paramAnonymousInt == -1)
                {
                    if (IccCard.this.mDbg)
                        IccCard.this.log("Reboot due to SIM swap");
                    ((PowerManager)IccCard.this.mPhone.getContext().getSystemService("power")).reboot("SIM is added.");
                }
            }
        };
        Resources localResources = Resources.getSystem();
        String str1;
        if (paramBoolean)
        {
            str1 = localResources.getString(17040413);
            if (!paramBoolean)
                break label112;
        }
        label112: for (String str2 = localResources.getString(17040414); ; str2 = localResources.getString(17040411))
        {
            String str3 = localResources.getString(17040415);
            AlertDialog localAlertDialog = new AlertDialog.Builder(this.mPhone.getContext()).setTitle(str1).setMessage(str2).setPositiveButton(str3, local1).create();
            localAlertDialog.getWindow().setType(2003);
            localAlertDialog.show();
            return;
            str1 = localResources.getString(17040410);
            break;
        }
    }

    private void onQueryFacilityLock(AsyncResult paramAsyncResult)
    {
        if (paramAsyncResult.exception != null)
            if (this.mDbg)
                log("Error in querying facility lock:" + paramAsyncResult.exception);
        while (true)
        {
            return;
            int[] arrayOfInt = (int[])paramAsyncResult.result;
            if (arrayOfInt.length != 0)
            {
                if (arrayOfInt[0] != 0);
                for (boolean bool = true; ; bool = false)
                {
                    this.mIccPinLocked = bool;
                    if (!this.mDbg)
                        break;
                    log("Query facility lock : " + this.mIccPinLocked);
                    break;
                }
            }
            Log.e(this.mLogTag, "[IccCard] Bogus facility lock response");
        }
    }

    private void onQueryFdnEnabled(AsyncResult paramAsyncResult)
    {
        if (paramAsyncResult.exception != null)
            if (this.mDbg)
                log("Error in querying facility lock:" + paramAsyncResult.exception);
        while (true)
        {
            return;
            int[] arrayOfInt = (int[])paramAsyncResult.result;
            if (arrayOfInt.length != 0)
            {
                if (arrayOfInt[0] != 0);
                for (boolean bool = true; ; bool = false)
                {
                    this.mIccFdnEnabled = bool;
                    if (!this.mDbg)
                        break;
                    log("Query facility lock : " + this.mIccFdnEnabled);
                    break;
                }
            }
            Log.e(this.mLogTag, "[IccCard] Bogus facility lock response");
        }
    }

    public void broadcastIccStateChangedIntent(String paramString1, String paramString2)
    {
        Intent localIntent = new Intent("android.intent.action.SIM_STATE_CHANGED");
        localIntent.addFlags(536870912);
        localIntent.putExtra("phoneName", this.mPhone.getPhoneName());
        localIntent.putExtra("ss", paramString1);
        localIntent.putExtra("reason", paramString2);
        if (this.mDbg)
            log("Broadcasting intent ACTION_SIM_STATE_CHANGED " + paramString1 + " reason " + paramString2);
        ActivityManagerNative.broadcastStickyIntent(localIntent, "android.permission.READ_PHONE_STATE");
    }

    public void changeIccFdnPassword(String paramString1, String paramString2, Message paramMessage)
    {
        this.mPhone.mCM.changeIccPin2(paramString1, paramString2, this.mHandler.obtainMessage(9, paramMessage));
    }

    public void changeIccLockPassword(String paramString1, String paramString2, Message paramMessage)
    {
        this.mPhone.mCM.changeIccPin(paramString1, paramString2, this.mHandler.obtainMessage(9, paramMessage));
    }

    public void dispose()
    {
        StringBuilder localStringBuilder;
        if (this.mDbg)
        {
            localStringBuilder = new StringBuilder().append("[IccCard] Disposing card type ");
            if (!this.is3gpp)
                break label124;
        }
        label124: for (String str = "3gpp"; ; str = "3gpp2")
        {
            log(str);
            this.mPhone.mCM.unregisterForIccStatusChanged(this.mHandler);
            this.mPhone.mCM.unregisterForOffOrNotAvailable(this.mHandler);
            this.mPhone.mCM.unregisterForOn(this.mHandler);
            this.mCatService.dispose();
            this.mCdmaSSM.dispose(this.mHandler);
            this.mIccRecords.dispose();
            this.mIccFileHandler.dispose();
            return;
        }
    }

    protected void finalize()
    {
        StringBuilder localStringBuilder;
        if (this.mDbg)
        {
            localStringBuilder = new StringBuilder().append("[IccCard] Finalized card type ");
            if (!this.is3gpp)
                break label44;
        }
        label44: for (String str = "3gpp"; ; str = "3gpp2")
        {
            log(str);
            return;
        }
    }

    public String getAid()
    {
        String str1 = "";
        String str2;
        if (this.mIccCardStatus == null)
        {
            str2 = str1;
            return str2;
        }
        int i = getCurrentApplicationIndex();
        if ((i >= 0) && (i < 8))
        {
            IccCardApplication localIccCardApplication = this.mIccCardStatus.getApplication(i);
            if (localIccCardApplication != null)
                str1 = localIccCardApplication.aid;
        }
        while (true)
        {
            str2 = str1;
            break;
            Log.e(this.mLogTag, "[IccCard] getAid: no current application index=" + i);
            continue;
            Log.e(this.mLogTag, "[IccCard] getAid: Invalid Subscription Application index=" + i);
        }
    }

    protected int getCurrentApplicationIndex()
    {
        if (this.is3gpp);
        for (int i = this.mIccCardStatus.getGsmUmtsSubscriptionAppIndex(); ; i = this.mIccCardStatus.getCdmaSubscriptionAppIndex())
            return i;
    }

    public State getIccCardState()
    {
        Object localObject;
        if ((!this.is3gpp) && (!this.isSubscriptionFromIccCard))
            localObject = State.READY;
        while (true)
        {
            return localObject;
            if (this.mIccCardStatus == null)
            {
                Log.e(this.mLogTag, "[IccCard] IccCardStatus is null");
                localObject = State.ABSENT;
            }
            else if (!this.mIccCardStatus.getCardState().isCardPresent())
            {
                localObject = State.ABSENT;
            }
            else
            {
                CommandsInterface.RadioState localRadioState = this.mPhone.mCM.getRadioState();
                if ((localRadioState == CommandsInterface.RadioState.RADIO_OFF) || (localRadioState == CommandsInterface.RadioState.RADIO_UNAVAILABLE))
                {
                    localObject = State.NOT_READY;
                }
                else if (localRadioState == CommandsInterface.RadioState.RADIO_ON)
                {
                    localObject = getAppState(this.mIccCardStatus.getCdmaSubscriptionAppIndex());
                    State localState = getAppState(this.mIccCardStatus.getGsmUmtsSubscriptionAppIndex());
                    if (this.mDbg)
                        log("USIM=" + localState + " CSIM=" + localObject);
                    if (this.mPhone.getLteOnCdmaMode() == 1)
                        localObject = getConsolidatedState((State)localObject, localState, (State)localObject);
                    else if (this.is3gpp)
                        localObject = localState;
                }
                else
                {
                    localObject = State.ABSENT;
                }
            }
        }
    }

    public boolean getIccFdnEnabled()
    {
        return this.mIccFdnEnabled;
    }

    public IccFileHandler getIccFileHandler()
    {
        return this.mIccFileHandler;
    }

    public boolean getIccLockEnabled()
    {
        return this.mIccPinLocked;
    }

    public IccRecords getIccRecords()
    {
        return this.mIccRecords;
    }

    public State getRuimState()
    {
        if (this.mIccCardStatus != null);
        for (State localState = getAppState(this.mIccCardStatus.getCdmaSubscriptionAppIndex()); ; localState = State.UNKNOWN)
            return localState;
    }

    public String getServiceProviderName()
    {
        return this.mPhone.mIccRecords.getServiceProviderName();
    }

    public State getState()
    {
        State localState;
        if (this.mState == null)
            switch (3.$SwitchMap$com$android$internal$telephony$CommandsInterface$RadioState[this.mPhone.mCM.getRadioState().ordinal()])
            {
            default:
                if ((!this.is3gpp) && (!this.isSubscriptionFromIccCard))
                    localState = State.READY;
                break;
            case 1:
            case 2:
            }
        while (true)
        {
            return localState;
            localState = State.UNKNOWN;
            continue;
            localState = this.mState;
            continue;
            localState = State.UNKNOWN;
        }
    }

    public boolean hasIccCard()
    {
        if (this.mIccCardStatus == null);
        for (boolean bool = false; ; bool = this.mIccCardStatus.getCardState().isCardPresent())
            return bool;
    }

    public boolean isApplicationOnIcc(IccCardApplication.AppType paramAppType)
    {
        boolean bool = false;
        if (this.mIccCardStatus == null);
        label57: 
        while (true)
        {
            return bool;
            for (int i = 0; ; i++)
            {
                if (i >= this.mIccCardStatus.getNumApplications())
                    break label57;
                IccCardApplication localIccCardApplication = this.mIccCardStatus.getApplication(i);
                if ((localIccCardApplication != null) && (localIccCardApplication.app_type == paramAppType))
                {
                    bool = true;
                    break;
                }
            }
        }
    }

    public void registerForAbsent(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mAbsentRegistrants.add(localRegistrant);
        if (getState() == State.ABSENT)
            localRegistrant.notifyRegistrant();
    }

    public void registerForLocked(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mPinLockedRegistrants.add(localRegistrant);
        if (getState().isPinLocked())
            localRegistrant.notifyRegistrant();
    }

    public void registerForNetworkLocked(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mNetworkLockedRegistrants.add(localRegistrant);
        if (getState() == State.NETWORK_LOCKED)
            localRegistrant.notifyRegistrant();
    }

    public void registerForReady(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        synchronized (this.mStateMonitor)
        {
            this.mReadyRegistrants.add(localRegistrant);
            if (getState() == State.READY)
                localRegistrant.notifyRegistrant(new AsyncResult(null, null, null));
            return;
        }
    }

    public void registerForRuimReady(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        synchronized (this.mStateMonitor)
        {
            this.mRuimReadyRegistrants.add(localRegistrant);
            if ((getState() == State.READY) && (getRuimState() == State.READY))
                localRegistrant.notifyRegistrant(new AsyncResult(null, null, null));
            return;
        }
    }

    public void setIccFdnEnabled(boolean paramBoolean, String paramString, Message paramMessage)
    {
        this.mDesiredFdnEnabled = paramBoolean;
        this.mPhone.mCM.setFacilityLock("FD", paramBoolean, paramString, 15, this.mHandler.obtainMessage(11, paramMessage));
    }

    public void setIccLockEnabled(boolean paramBoolean, String paramString, Message paramMessage)
    {
        this.mDesiredPinLocked = paramBoolean;
        this.mPhone.mCM.setFacilityLock("SC", paramBoolean, paramString, 7, this.mHandler.obtainMessage(8, paramMessage));
    }

    public void supplyNetworkDepersonalization(String paramString, Message paramMessage)
    {
        this.mPhone.mCM.supplyNetworkDepersonalization(paramString, this.mHandler.obtainMessage(4, paramMessage));
    }

    public void supplyPin(String paramString, Message paramMessage)
    {
        this.mPhone.mCM.supplyIccPin(paramString, this.mHandler.obtainMessage(4, paramMessage));
    }

    public void supplyPin2(String paramString, Message paramMessage)
    {
        this.mPhone.mCM.supplyIccPin2(paramString, this.mHandler.obtainMessage(4, paramMessage));
    }

    public void supplyPuk(String paramString1, String paramString2, Message paramMessage)
    {
        this.mPhone.mCM.supplyIccPuk(paramString1, paramString2, this.mHandler.obtainMessage(4, paramMessage));
    }

    public void supplyPuk2(String paramString1, String paramString2, Message paramMessage)
    {
        this.mPhone.mCM.supplyIccPuk2(paramString1, paramString2, this.mHandler.obtainMessage(4, paramMessage));
    }

    public void unregisterForAbsent(Handler paramHandler)
    {
        this.mAbsentRegistrants.remove(paramHandler);
    }

    public void unregisterForLocked(Handler paramHandler)
    {
        this.mPinLockedRegistrants.remove(paramHandler);
    }

    public void unregisterForNetworkLocked(Handler paramHandler)
    {
        this.mNetworkLockedRegistrants.remove(paramHandler);
    }

    public void unregisterForReady(Handler paramHandler)
    {
        synchronized (this.mStateMonitor)
        {
            this.mReadyRegistrants.remove(paramHandler);
            return;
        }
    }

    public void unregisterForRuimReady(Handler paramHandler)
    {
        synchronized (this.mStateMonitor)
        {
            this.mRuimReadyRegistrants.remove(paramHandler);
            return;
        }
    }

    protected void updateStateProperty()
    {
        this.mPhone.setSystemProperty("gsm.sim.state", getState().toString());
    }

    public static enum State
    {
        static
        {
            ABSENT = new State("ABSENT", 1);
            PIN_REQUIRED = new State("PIN_REQUIRED", 2);
            PUK_REQUIRED = new State("PUK_REQUIRED", 3);
            NETWORK_LOCKED = new State("NETWORK_LOCKED", 4);
            READY = new State("READY", 5);
            NOT_READY = new State("NOT_READY", 6);
            PERM_DISABLED = new State("PERM_DISABLED", 7);
            State[] arrayOfState = new State[8];
            arrayOfState[0] = UNKNOWN;
            arrayOfState[1] = ABSENT;
            arrayOfState[2] = PIN_REQUIRED;
            arrayOfState[3] = PUK_REQUIRED;
            arrayOfState[4] = NETWORK_LOCKED;
            arrayOfState[5] = READY;
            arrayOfState[6] = NOT_READY;
            arrayOfState[7] = PERM_DISABLED;
        }

        public boolean iccCardExist()
        {
            if ((this == PIN_REQUIRED) || (this == PUK_REQUIRED) || (this == NETWORK_LOCKED) || (this == READY) || (this == PERM_DISABLED));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isPinLocked()
        {
            if ((this == PIN_REQUIRED) || (this == PUK_REQUIRED));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccCard
 * JD-Core Version:        0.6.2
 */