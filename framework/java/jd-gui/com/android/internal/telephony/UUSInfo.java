package com.android.internal.telephony;

public class UUSInfo
{
    public static final int UUS_DCS_IA5c = 4;
    public static final int UUS_DCS_OSIHLP = 1;
    public static final int UUS_DCS_RMCF = 3;
    public static final int UUS_DCS_USP = 0;
    public static final int UUS_DCS_X244 = 2;
    public static final int UUS_TYPE1_IMPLICIT = 0;
    public static final int UUS_TYPE1_NOT_REQUIRED = 2;
    public static final int UUS_TYPE1_REQUIRED = 1;
    public static final int UUS_TYPE2_NOT_REQUIRED = 4;
    public static final int UUS_TYPE2_REQUIRED = 3;
    public static final int UUS_TYPE3_NOT_REQUIRED = 6;
    public static final int UUS_TYPE3_REQUIRED = 5;
    private byte[] uusData;
    private int uusDcs;
    private int uusType;

    public UUSInfo()
    {
        this.uusType = 0;
        this.uusDcs = 4;
        this.uusData = null;
    }

    public UUSInfo(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        this.uusType = paramInt1;
        this.uusDcs = paramInt2;
        this.uusData = paramArrayOfByte;
    }

    public int getDcs()
    {
        return this.uusDcs;
    }

    public int getType()
    {
        return this.uusType;
    }

    public byte[] getUserData()
    {
        return this.uusData;
    }

    public void setDcs(int paramInt)
    {
        this.uusDcs = paramInt;
    }

    public void setType(int paramInt)
    {
        this.uusType = paramInt;
    }

    public void setUserData(byte[] paramArrayOfByte)
    {
        this.uusData = paramArrayOfByte;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.UUSInfo
 * JD-Core Version:        0.6.2
 */