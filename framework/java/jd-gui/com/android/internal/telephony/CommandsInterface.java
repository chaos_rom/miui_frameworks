package com.android.internal.telephony;

import android.os.Handler;
import android.os.Message;
import com.android.internal.telephony.gsm.SmsBroadcastConfigInfo;

public abstract interface CommandsInterface
{
    public static final String CB_FACILITY_BAIC = "AI";
    public static final String CB_FACILITY_BAICr = "IR";
    public static final String CB_FACILITY_BAOC = "AO";
    public static final String CB_FACILITY_BAOIC = "OI";
    public static final String CB_FACILITY_BAOICxH = "OX";
    public static final String CB_FACILITY_BA_ALL = "AB";
    public static final String CB_FACILITY_BA_FD = "FD";
    public static final String CB_FACILITY_BA_MO = "AG";
    public static final String CB_FACILITY_BA_MT = "AC";
    public static final String CB_FACILITY_BA_SIM = "SC";
    public static final int CDMA_SMS_FAIL_CAUSE_ENCODING_PROBLEM = 96;
    public static final int CDMA_SMS_FAIL_CAUSE_INVALID_TELESERVICE_ID = 4;
    public static final int CDMA_SMS_FAIL_CAUSE_OTHER_TERMINAL_PROBLEM = 39;
    public static final int CDMA_SMS_FAIL_CAUSE_RESOURCE_SHORTAGE = 35;
    public static final int CF_ACTION_DISABLE = 0;
    public static final int CF_ACTION_ENABLE = 1;
    public static final int CF_ACTION_ERASURE = 4;
    public static final int CF_ACTION_REGISTRATION = 3;
    public static final int CF_REASON_ALL = 4;
    public static final int CF_REASON_ALL_CONDITIONAL = 5;
    public static final int CF_REASON_BUSY = 1;
    public static final int CF_REASON_NOT_REACHABLE = 3;
    public static final int CF_REASON_NO_REPLY = 2;
    public static final int CF_REASON_UNCONDITIONAL = 0;
    public static final int CLIR_DEFAULT = 0;
    public static final int CLIR_INVOCATION = 1;
    public static final int CLIR_SUPPRESSION = 2;
    public static final int GSM_SMS_FAIL_CAUSE_MEMORY_CAPACITY_EXCEEDED = 211;
    public static final int GSM_SMS_FAIL_CAUSE_UNSPECIFIED_ERROR = 255;
    public static final int GSM_SMS_FAIL_CAUSE_USIM_APP_TOOLKIT_BUSY = 212;
    public static final int GSM_SMS_FAIL_CAUSE_USIM_DATA_DOWNLOAD_ERROR = 213;
    public static final int SERVICE_CLASS_DATA = 2;
    public static final int SERVICE_CLASS_DATA_ASYNC = 32;
    public static final int SERVICE_CLASS_DATA_SYNC = 16;
    public static final int SERVICE_CLASS_FAX = 4;
    public static final int SERVICE_CLASS_MAX = 128;
    public static final int SERVICE_CLASS_NONE = 0;
    public static final int SERVICE_CLASS_PACKET = 64;
    public static final int SERVICE_CLASS_PAD = 128;
    public static final int SERVICE_CLASS_SMS = 8;
    public static final int SERVICE_CLASS_VOICE = 1;
    public static final int USSD_MODE_NOTIFY = 0;
    public static final int USSD_MODE_REQUEST = 1;

    public abstract void acceptCall(Message paramMessage);

    public abstract void acknowledgeIncomingGsmSmsWithPdu(boolean paramBoolean, String paramString, Message paramMessage);

    public abstract void acknowledgeLastIncomingCdmaSms(boolean paramBoolean, int paramInt, Message paramMessage);

    public abstract void acknowledgeLastIncomingGsmSms(boolean paramBoolean, int paramInt, Message paramMessage);

    public abstract void cancelPendingUssd(Message paramMessage);

    public abstract void changeBarringPassword(String paramString1, String paramString2, String paramString3, Message paramMessage);

    public abstract void changeIccPin(String paramString1, String paramString2, Message paramMessage);

    public abstract void changeIccPin2(String paramString1, String paramString2, Message paramMessage);

    public abstract void changeIccPin2ForApp(String paramString1, String paramString2, String paramString3, Message paramMessage);

    public abstract void changeIccPinForApp(String paramString1, String paramString2, String paramString3, Message paramMessage);

    public abstract void conference(Message paramMessage);

    public abstract void deactivateDataCall(int paramInt1, int paramInt2, Message paramMessage);

    public abstract void deleteSmsOnRuim(int paramInt, Message paramMessage);

    public abstract void deleteSmsOnSim(int paramInt, Message paramMessage);

    public abstract void dial(String paramString, int paramInt, Message paramMessage);

    public abstract void dial(String paramString, int paramInt, UUSInfo paramUUSInfo, Message paramMessage);

    public abstract void exitEmergencyCallbackMode(Message paramMessage);

    public abstract void explicitCallTransfer(Message paramMessage);

    public abstract void getAvailableNetworks(Message paramMessage);

    public abstract void getBasebandVersion(Message paramMessage);

    public abstract void getCDMASubscription(Message paramMessage);

    public abstract void getCLIR(Message paramMessage);

    public abstract void getCdmaBroadcastConfig(Message paramMessage);

    public abstract void getCdmaSubscriptionSource(Message paramMessage);

    public abstract void getCurrentCalls(Message paramMessage);

    public abstract void getDataCallList(Message paramMessage);

    public abstract void getDataRegistrationState(Message paramMessage);

    public abstract void getDeviceIdentity(Message paramMessage);

    public abstract void getGsmBroadcastConfig(Message paramMessage);

    public abstract void getIMEI(Message paramMessage);

    public abstract void getIMEISV(Message paramMessage);

    public abstract void getIMSI(Message paramMessage);

    public abstract void getIMSIForApp(String paramString, Message paramMessage);

    public abstract void getIccCardStatus(Message paramMessage);

    public abstract void getLastCallFailCause(Message paramMessage);

    public abstract void getLastDataCallFailCause(Message paramMessage);

    @Deprecated
    public abstract void getLastPdpFailCause(Message paramMessage);

    public abstract int getLteOnCdmaMode();

    public abstract void getMute(Message paramMessage);

    public abstract void getNeighboringCids(Message paramMessage);

    public abstract void getNetworkSelectionMode(Message paramMessage);

    public abstract void getOperator(Message paramMessage);

    @Deprecated
    public abstract void getPDPContextList(Message paramMessage);

    public abstract void getPreferredNetworkType(Message paramMessage);

    public abstract void getPreferredVoicePrivacy(Message paramMessage);

    public abstract RadioState getRadioState();

    public abstract void getSignalStrength(Message paramMessage);

    public abstract void getSmscAddress(Message paramMessage);

    public abstract void getVoiceRadioTechnology(Message paramMessage);

    public abstract void getVoiceRegistrationState(Message paramMessage);

    public abstract void handleCallSetupRequestFromSim(boolean paramBoolean, Message paramMessage);

    public abstract void hangupConnection(int paramInt, Message paramMessage);

    public abstract void hangupForegroundResumeBackground(Message paramMessage);

    public abstract void hangupWaitingOrBackground(Message paramMessage);

    public abstract void iccIO(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, String paramString2, String paramString3, Message paramMessage);

    public abstract void iccIOForApp(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, String paramString2, String paramString3, String paramString4, Message paramMessage);

    public abstract void invokeOemRilRequestRaw(byte[] paramArrayOfByte, Message paramMessage);

    public abstract void invokeOemRilRequestStrings(String[] paramArrayOfString, Message paramMessage);

    public abstract void queryAvailableBandMode(Message paramMessage);

    public abstract void queryCLIP(Message paramMessage);

    public abstract void queryCallForwardStatus(int paramInt1, int paramInt2, String paramString, Message paramMessage);

    public abstract void queryCallWaiting(int paramInt, Message paramMessage);

    public abstract void queryCdmaRoamingPreference(Message paramMessage);

    public abstract void queryFacilityLock(String paramString1, String paramString2, int paramInt, Message paramMessage);

    public abstract void queryFacilityLockForApp(String paramString1, String paramString2, int paramInt, String paramString3, Message paramMessage);

    public abstract void queryTTYMode(Message paramMessage);

    public abstract void registerFoT53ClirlInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForAvailable(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForCallStateChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForCallWaitingInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForCdmaOtaProvision(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForCdmaPrlChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForCdmaSubscriptionChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForDataNetworkStateChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForDisplayInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForExitEmergencyCallbackMode(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForIccRefresh(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForIccStatusChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForInCallVoicePrivacyOff(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForInCallVoicePrivacyOn(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForLineControlInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForNotAvailable(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForNumberInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForOffOrNotAvailable(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForOn(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForRadioStateChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForRedirectedNumberInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForResendIncallMute(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForRilConnected(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForRingbackTone(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForSignalInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForT53AudioControlInfo(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForVoiceNetworkStateChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void registerForVoiceRadioTechChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void rejectCall(Message paramMessage);

    public abstract void reportSmsMemoryStatus(boolean paramBoolean, Message paramMessage);

    public abstract void reportStkServiceIsRunning(Message paramMessage);

    public abstract void requestIsimAuthentication(String paramString, Message paramMessage);

    public abstract void resetRadio(Message paramMessage);

    public abstract void sendBurstDtmf(String paramString, int paramInt1, int paramInt2, Message paramMessage);

    public abstract void sendCDMAFeatureCode(String paramString, Message paramMessage);

    public abstract void sendCdmaSms(byte[] paramArrayOfByte, Message paramMessage);

    public abstract void sendDtmf(char paramChar, Message paramMessage);

    public abstract void sendEnvelope(String paramString, Message paramMessage);

    public abstract void sendEnvelopeWithStatus(String paramString, Message paramMessage);

    public abstract void sendSMS(String paramString1, String paramString2, Message paramMessage);

    public abstract void sendTerminalResponse(String paramString, Message paramMessage);

    public abstract void sendUSSD(String paramString, Message paramMessage);

    public abstract void separateConnection(int paramInt, Message paramMessage);

    public abstract void setBandMode(int paramInt, Message paramMessage);

    public abstract void setCLIR(int paramInt, Message paramMessage);

    public abstract void setCallForward(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, Message paramMessage);

    public abstract void setCallWaiting(boolean paramBoolean, int paramInt, Message paramMessage);

    public abstract void setCdmaBroadcastActivation(boolean paramBoolean, Message paramMessage);

    public abstract void setCdmaBroadcastConfig(int[] paramArrayOfInt, Message paramMessage);

    public abstract void setCdmaRoamingPreference(int paramInt, Message paramMessage);

    public abstract void setCdmaSubscriptionSource(int paramInt, Message paramMessage);

    public abstract void setCurrentPreferredNetworkType();

    public abstract void setEmergencyCallbackMode(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setFacilityLock(String paramString1, boolean paramBoolean, String paramString2, int paramInt, Message paramMessage);

    public abstract void setFacilityLockForApp(String paramString1, boolean paramBoolean, String paramString2, int paramInt, String paramString3, Message paramMessage);

    public abstract void setGsmBroadcastActivation(boolean paramBoolean, Message paramMessage);

    public abstract void setGsmBroadcastConfig(SmsBroadcastConfigInfo[] paramArrayOfSmsBroadcastConfigInfo, Message paramMessage);

    public abstract void setLocationUpdates(boolean paramBoolean, Message paramMessage);

    public abstract void setMute(boolean paramBoolean, Message paramMessage);

    public abstract void setNetworkSelectionModeAutomatic(Message paramMessage);

    public abstract void setNetworkSelectionModeManual(String paramString, Message paramMessage);

    public abstract void setOnCallRing(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnCatCallSetUp(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnCatEvent(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnCatProactiveCmd(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnCatSessionEnd(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnIccRefresh(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnIccSmsFull(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnNITZTime(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnNewCdmaSms(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnNewGsmBroadcastSms(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnNewGsmSms(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnRestrictedStateChanged(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnSignalStrengthUpdate(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnSmsOnSim(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnSmsStatus(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnSuppServiceNotification(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setOnUSSD(Handler paramHandler, int paramInt, Object paramObject);

    public abstract void setPhoneType(int paramInt);

    public abstract void setPreferredNetworkType(int paramInt, Message paramMessage);

    public abstract void setPreferredVoicePrivacy(boolean paramBoolean, Message paramMessage);

    public abstract void setRadioPower(boolean paramBoolean, Message paramMessage);

    public abstract void setSmscAddress(String paramString, Message paramMessage);

    public abstract void setSuppServiceNotifications(boolean paramBoolean, Message paramMessage);

    public abstract void setTTYMode(int paramInt, Message paramMessage);

    public abstract void setupDataCall(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Message paramMessage);

    public abstract void startDtmf(char paramChar, Message paramMessage);

    public abstract void stopDtmf(Message paramMessage);

    public abstract void supplyIccPin(String paramString, Message paramMessage);

    public abstract void supplyIccPin2(String paramString, Message paramMessage);

    public abstract void supplyIccPin2ForApp(String paramString1, String paramString2, Message paramMessage);

    public abstract void supplyIccPinForApp(String paramString1, String paramString2, Message paramMessage);

    public abstract void supplyIccPuk(String paramString1, String paramString2, Message paramMessage);

    public abstract void supplyIccPuk2(String paramString1, String paramString2, Message paramMessage);

    public abstract void supplyIccPuk2ForApp(String paramString1, String paramString2, String paramString3, Message paramMessage);

    public abstract void supplyIccPukForApp(String paramString1, String paramString2, String paramString3, Message paramMessage);

    public abstract void supplyNetworkDepersonalization(String paramString, Message paramMessage);

    public abstract void switchWaitingOrHoldingAndActive(Message paramMessage);

    public abstract void testingEmergencyCall();

    public abstract void unSetOnCallRing(Handler paramHandler);

    public abstract void unSetOnCatCallSetUp(Handler paramHandler);

    public abstract void unSetOnCatEvent(Handler paramHandler);

    public abstract void unSetOnCatProactiveCmd(Handler paramHandler);

    public abstract void unSetOnCatSessionEnd(Handler paramHandler);

    public abstract void unSetOnIccSmsFull(Handler paramHandler);

    public abstract void unSetOnNITZTime(Handler paramHandler);

    public abstract void unSetOnNewCdmaSms(Handler paramHandler);

    public abstract void unSetOnNewGsmBroadcastSms(Handler paramHandler);

    public abstract void unSetOnNewGsmSms(Handler paramHandler);

    public abstract void unSetOnRestrictedStateChanged(Handler paramHandler);

    public abstract void unSetOnSignalStrengthUpdate(Handler paramHandler);

    public abstract void unSetOnSmsOnSim(Handler paramHandler);

    public abstract void unSetOnSmsStatus(Handler paramHandler);

    public abstract void unSetOnSuppServiceNotification(Handler paramHandler);

    public abstract void unSetOnUSSD(Handler paramHandler);

    public abstract void unregisterForAvailable(Handler paramHandler);

    public abstract void unregisterForCallStateChanged(Handler paramHandler);

    public abstract void unregisterForCallWaitingInfo(Handler paramHandler);

    public abstract void unregisterForCdmaOtaProvision(Handler paramHandler);

    public abstract void unregisterForCdmaPrlChanged(Handler paramHandler);

    public abstract void unregisterForCdmaSubscriptionChanged(Handler paramHandler);

    public abstract void unregisterForDataNetworkStateChanged(Handler paramHandler);

    public abstract void unregisterForDisplayInfo(Handler paramHandler);

    public abstract void unregisterForExitEmergencyCallbackMode(Handler paramHandler);

    public abstract void unregisterForIccRefresh(Handler paramHandler);

    public abstract void unregisterForIccStatusChanged(Handler paramHandler);

    public abstract void unregisterForInCallVoicePrivacyOff(Handler paramHandler);

    public abstract void unregisterForInCallVoicePrivacyOn(Handler paramHandler);

    public abstract void unregisterForLineControlInfo(Handler paramHandler);

    public abstract void unregisterForNotAvailable(Handler paramHandler);

    public abstract void unregisterForNumberInfo(Handler paramHandler);

    public abstract void unregisterForOffOrNotAvailable(Handler paramHandler);

    public abstract void unregisterForOn(Handler paramHandler);

    public abstract void unregisterForRadioStateChanged(Handler paramHandler);

    public abstract void unregisterForRedirectedNumberInfo(Handler paramHandler);

    public abstract void unregisterForResendIncallMute(Handler paramHandler);

    public abstract void unregisterForRilConnected(Handler paramHandler);

    public abstract void unregisterForRingbackTone(Handler paramHandler);

    public abstract void unregisterForSignalInfo(Handler paramHandler);

    public abstract void unregisterForT53AudioControlInfo(Handler paramHandler);

    public abstract void unregisterForT53ClirInfo(Handler paramHandler);

    public abstract void unregisterForVoiceNetworkStateChanged(Handler paramHandler);

    public abstract void unregisterForVoiceRadioTechChanged(Handler paramHandler);

    public abstract void unsetOnIccRefresh(Handler paramHandler);

    public abstract void writeSmsToRuim(int paramInt, String paramString, Message paramMessage);

    public abstract void writeSmsToSim(int paramInt, String paramString1, String paramString2, Message paramMessage);

    public static enum RadioState
    {
        static
        {
            RADIO_ON = new RadioState("RADIO_ON", 2);
            RadioState[] arrayOfRadioState = new RadioState[3];
            arrayOfRadioState[0] = RADIO_OFF;
            arrayOfRadioState[1] = RADIO_UNAVAILABLE;
            arrayOfRadioState[2] = RADIO_ON;
        }

        public boolean isAvailable()
        {
            if (this != RADIO_UNAVAILABLE);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isOn()
        {
            if (this == RADIO_ON);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.CommandsInterface
 * JD-Core Version:        0.6.2
 */