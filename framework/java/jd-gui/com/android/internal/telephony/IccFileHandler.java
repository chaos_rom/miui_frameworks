package com.android.internal.telephony;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;

public abstract class IccFileHandler extends Handler
    implements IccConstants
{
    protected static final int COMMAND_GET_RESPONSE = 192;
    protected static final int COMMAND_READ_BINARY = 176;
    protected static final int COMMAND_READ_RECORD = 178;
    protected static final int COMMAND_SEEK = 162;
    protected static final int COMMAND_UPDATE_BINARY = 214;
    protected static final int COMMAND_UPDATE_RECORD = 220;
    protected static final int EF_TYPE_CYCLIC = 3;
    protected static final int EF_TYPE_LINEAR_FIXED = 1;
    protected static final int EF_TYPE_TRANSPARENT = 0;
    protected static final int EVENT_GET_BINARY_SIZE_DONE = 4;
    protected static final int EVENT_GET_EF_LINEAR_RECORD_SIZE_DONE = 8;
    protected static final int EVENT_GET_RECORD_SIZE_DONE = 6;
    protected static final int EVENT_READ_BINARY_DONE = 5;
    protected static final int EVENT_READ_ICON_DONE = 10;
    protected static final int EVENT_READ_IMG_DONE = 9;
    protected static final int EVENT_READ_RECORD_DONE = 7;
    protected static final int GET_RESPONSE_EF_IMG_SIZE_BYTES = 10;
    protected static final int GET_RESPONSE_EF_SIZE_BYTES = 15;
    protected static final int READ_RECORD_MODE_ABSOLUTE = 4;
    protected static final int RESPONSE_DATA_ACCESS_CONDITION_1 = 8;
    protected static final int RESPONSE_DATA_ACCESS_CONDITION_2 = 9;
    protected static final int RESPONSE_DATA_ACCESS_CONDITION_3 = 10;
    protected static final int RESPONSE_DATA_FILE_ID_1 = 4;
    protected static final int RESPONSE_DATA_FILE_ID_2 = 5;
    protected static final int RESPONSE_DATA_FILE_SIZE_1 = 2;
    protected static final int RESPONSE_DATA_FILE_SIZE_2 = 3;
    protected static final int RESPONSE_DATA_FILE_STATUS = 11;
    protected static final int RESPONSE_DATA_FILE_TYPE = 6;
    protected static final int RESPONSE_DATA_LENGTH = 12;
    protected static final int RESPONSE_DATA_RECORD_LENGTH = 14;
    protected static final int RESPONSE_DATA_RFU_1 = 0;
    protected static final int RESPONSE_DATA_RFU_2 = 1;
    protected static final int RESPONSE_DATA_RFU_3 = 7;
    protected static final int RESPONSE_DATA_STRUCTURE = 13;
    protected static final int TYPE_DF = 2;
    protected static final int TYPE_EF = 4;
    protected static final int TYPE_MF = 1;
    protected static final int TYPE_RFU;
    protected String mAid;
    protected final CommandsInterface mCi;
    protected final IccCard mParentCard;

    protected IccFileHandler(IccCard paramIccCard, String paramString, CommandsInterface paramCommandsInterface)
    {
        this.mParentCard = paramIccCard;
        this.mAid = paramString;
        this.mCi = paramCommandsInterface;
    }

    private void sendResult(Message paramMessage, Object paramObject, Throwable paramThrowable)
    {
        if (paramMessage == null);
        while (true)
        {
            return;
            AsyncResult.forMessage(paramMessage, paramObject, paramThrowable);
            paramMessage.sendToTarget();
        }
    }

    public void dispose()
    {
    }

    protected String getCommonIccEFPath(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = null;
        case 28474:
        case 28475:
        case 28480:
        case 28489:
        case 28490:
        case 28491:
        case 28492:
        case 12037:
        case 12258:
        case 20256:
        }
        while (true)
        {
            return str;
            str = "3F007F10";
            continue;
            str = "3F00";
            continue;
            str = "3F007F105F50";
        }
    }

    public void getEFLinearRecordSize(int paramInt, Message paramMessage)
    {
        Message localMessage = obtainMessage(8, new LoadLinearFixedContext(paramInt, paramMessage));
        this.mCi.iccIOForApp(192, paramInt, getEFPath(paramInt), 0, 0, 15, null, null, this.mAid, localMessage);
    }

    protected abstract String getEFPath(int paramInt);

    public void handleMessage(Message paramMessage)
    {
        Message localMessage = null;
        try
        {
            switch (paramMessage.what)
            {
            case 9:
                AsyncResult localAsyncResult7 = (AsyncResult)paramMessage.obj;
                LoadLinearFixedContext localLoadLinearFixedContext4 = (LoadLinearFixedContext)localAsyncResult7.userObj;
                IccIoResult localIccIoResult7 = (IccIoResult)localAsyncResult7.result;
                localMessage = localLoadLinearFixedContext4.onLoaded;
                if (localIccIoResult7.getException() != null)
                {
                    byte[] arrayOfByte7 = localIccIoResult7.payload;
                    Throwable localThrowable7 = localAsyncResult7.exception;
                    sendResult(localMessage, arrayOfByte7, localThrowable7);
                }
                break;
            case 10:
            case 8:
            case 6:
            case 4:
            case 7:
            case 5:
            }
        }
        catch (Exception localException)
        {
            if (localMessage != null)
            {
                sendResult(localMessage, null, localException);
                return;
                AsyncResult localAsyncResult6 = (AsyncResult)paramMessage.obj;
                localMessage = (Message)localAsyncResult6.userObj;
                IccIoResult localIccIoResult6 = (IccIoResult)localAsyncResult6.result;
                if (localIccIoResult6.getException() != null)
                {
                    byte[] arrayOfByte6 = localIccIoResult6.payload;
                    Throwable localThrowable6 = localAsyncResult6.exception;
                    sendResult(localMessage, arrayOfByte6, localThrowable6);
                    return;
                    AsyncResult localAsyncResult5 = (AsyncResult)paramMessage.obj;
                    LoadLinearFixedContext localLoadLinearFixedContext3 = (LoadLinearFixedContext)localAsyncResult5.userObj;
                    IccIoResult localIccIoResult5 = (IccIoResult)localAsyncResult5.result;
                    localMessage = localLoadLinearFixedContext3.onLoaded;
                    if (localAsyncResult5.exception != null)
                    {
                        Throwable localThrowable5 = localAsyncResult5.exception;
                        sendResult(localMessage, null, localThrowable5);
                    }
                    else
                    {
                        IccException localIccException5 = localIccIoResult5.getException();
                        if (localIccException5 != null)
                        {
                            sendResult(localMessage, null, localIccException5);
                        }
                        else
                        {
                            byte[] arrayOfByte5 = localIccIoResult5.payload;
                            if ((4 != arrayOfByte5[6]) || (1 != arrayOfByte5[13]))
                                throw new IccFileTypeMismatch();
                            int[] arrayOfInt = new int[3];
                            arrayOfInt[0] = (0xFF & arrayOfByte5[14]);
                            arrayOfInt[1] = (((0xFF & arrayOfByte5[2]) << 8) + (0xFF & arrayOfByte5[3]));
                            arrayOfInt[2] = (arrayOfInt[1] / arrayOfInt[0]);
                            sendResult(localMessage, arrayOfInt, null);
                            return;
                            AsyncResult localAsyncResult4 = (AsyncResult)paramMessage.obj;
                            LoadLinearFixedContext localLoadLinearFixedContext2 = (LoadLinearFixedContext)localAsyncResult4.userObj;
                            IccIoResult localIccIoResult4 = (IccIoResult)localAsyncResult4.result;
                            localMessage = localLoadLinearFixedContext2.onLoaded;
                            if (localAsyncResult4.exception != null)
                            {
                                Throwable localThrowable4 = localAsyncResult4.exception;
                                sendResult(localMessage, null, localThrowable4);
                            }
                            else
                            {
                                IccException localIccException4 = localIccIoResult4.getException();
                                if (localIccException4 != null)
                                {
                                    sendResult(localMessage, null, localIccException4);
                                }
                                else
                                {
                                    byte[] arrayOfByte4 = localIccIoResult4.payload;
                                    if (4 != arrayOfByte4[6])
                                        throw new IccFileTypeMismatch();
                                    if (1 != arrayOfByte4[13])
                                        throw new IccFileTypeMismatch();
                                    localLoadLinearFixedContext2.recordSize = (0xFF & arrayOfByte4[14]);
                                    localLoadLinearFixedContext2.countRecords = ((((0xFF & arrayOfByte4[2]) << 8) + (0xFF & arrayOfByte4[3])) / localLoadLinearFixedContext2.recordSize);
                                    if (localLoadLinearFixedContext2.loadAll)
                                        localLoadLinearFixedContext2.results = new ArrayList(localLoadLinearFixedContext2.countRecords);
                                    this.mCi.iccIOForApp(178, localLoadLinearFixedContext2.efid, getEFPath(localLoadLinearFixedContext2.efid), localLoadLinearFixedContext2.recordNum, 4, localLoadLinearFixedContext2.recordSize, null, null, this.mAid, obtainMessage(7, localLoadLinearFixedContext2));
                                    return;
                                    AsyncResult localAsyncResult3 = (AsyncResult)paramMessage.obj;
                                    localMessage = (Message)localAsyncResult3.userObj;
                                    IccIoResult localIccIoResult3 = (IccIoResult)localAsyncResult3.result;
                                    if (localAsyncResult3.exception != null)
                                    {
                                        Throwable localThrowable3 = localAsyncResult3.exception;
                                        sendResult(localMessage, null, localThrowable3);
                                    }
                                    else
                                    {
                                        IccException localIccException3 = localIccIoResult3.getException();
                                        if (localIccException3 != null)
                                        {
                                            sendResult(localMessage, null, localIccException3);
                                        }
                                        else
                                        {
                                            byte[] arrayOfByte3 = localIccIoResult3.payload;
                                            int i = paramMessage.arg1;
                                            if (4 != arrayOfByte3[6])
                                                throw new IccFileTypeMismatch();
                                            if (arrayOfByte3[13] != 0)
                                                throw new IccFileTypeMismatch();
                                            int j = ((0xFF & arrayOfByte3[2]) << 8) + (0xFF & arrayOfByte3[3]);
                                            this.mCi.iccIOForApp(176, i, getEFPath(i), 0, 0, j, null, null, this.mAid, obtainMessage(5, i, 0, localMessage));
                                            return;
                                            AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
                                            LoadLinearFixedContext localLoadLinearFixedContext1 = (LoadLinearFixedContext)localAsyncResult2.userObj;
                                            IccIoResult localIccIoResult2 = (IccIoResult)localAsyncResult2.result;
                                            localMessage = localLoadLinearFixedContext1.onLoaded;
                                            if (localAsyncResult2.exception != null)
                                            {
                                                Throwable localThrowable2 = localAsyncResult2.exception;
                                                sendResult(localMessage, null, localThrowable2);
                                            }
                                            else
                                            {
                                                IccException localIccException2 = localIccIoResult2.getException();
                                                if (localIccException2 != null)
                                                {
                                                    sendResult(localMessage, null, localIccException2);
                                                }
                                                else if (!localLoadLinearFixedContext1.loadAll)
                                                {
                                                    byte[] arrayOfByte2 = localIccIoResult2.payload;
                                                    sendResult(localMessage, arrayOfByte2, null);
                                                }
                                                else
                                                {
                                                    localLoadLinearFixedContext1.results.add(localIccIoResult2.payload);
                                                    localLoadLinearFixedContext1.recordNum = (1 + localLoadLinearFixedContext1.recordNum);
                                                    if (localLoadLinearFixedContext1.recordNum > localLoadLinearFixedContext1.countRecords)
                                                    {
                                                        ArrayList localArrayList = localLoadLinearFixedContext1.results;
                                                        sendResult(localMessage, localArrayList, null);
                                                    }
                                                    else
                                                    {
                                                        this.mCi.iccIOForApp(178, localLoadLinearFixedContext1.efid, getEFPath(localLoadLinearFixedContext1.efid), localLoadLinearFixedContext1.recordNum, 4, localLoadLinearFixedContext1.recordSize, null, null, this.mAid, obtainMessage(7, localLoadLinearFixedContext1));
                                                        return;
                                                        AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
                                                        localMessage = (Message)localAsyncResult1.userObj;
                                                        IccIoResult localIccIoResult1 = (IccIoResult)localAsyncResult1.result;
                                                        if (localAsyncResult1.exception != null)
                                                        {
                                                            Throwable localThrowable1 = localAsyncResult1.exception;
                                                            sendResult(localMessage, null, localThrowable1);
                                                        }
                                                        else
                                                        {
                                                            IccException localIccException1 = localIccIoResult1.getException();
                                                            if (localIccException1 != null)
                                                            {
                                                                sendResult(localMessage, null, localIccException1);
                                                            }
                                                            else
                                                            {
                                                                byte[] arrayOfByte1 = localIccIoResult1.payload;
                                                                sendResult(localMessage, arrayOfByte1, null);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                loge("uncaught exception" + localException);
            }
        }
    }

    public void loadEFImgLinearFixed(int paramInt, Message paramMessage)
    {
        Message localMessage = obtainMessage(9, new LoadLinearFixedContext(20256, paramInt, paramMessage));
        this.mCi.iccIOForApp(192, 20256, "img", paramInt, 4, 10, null, null, this.mAid, localMessage);
    }

    public void loadEFImgTransparent(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Message paramMessage)
    {
        Message localMessage = obtainMessage(10, paramInt1, 0, paramMessage);
        this.mCi.iccIOForApp(176, paramInt1, "img", paramInt2, paramInt3, paramInt4, null, null, this.mAid, localMessage);
    }

    public void loadEFLinearFixed(int paramInt1, int paramInt2, Message paramMessage)
    {
        Message localMessage = obtainMessage(6, new LoadLinearFixedContext(paramInt1, paramInt2, paramMessage));
        this.mCi.iccIOForApp(192, paramInt1, getEFPath(paramInt1), 0, 0, 15, null, null, this.mAid, localMessage);
    }

    public void loadEFLinearFixedAll(int paramInt, Message paramMessage)
    {
        Message localMessage = obtainMessage(6, new LoadLinearFixedContext(paramInt, paramMessage));
        this.mCi.iccIOForApp(192, paramInt, getEFPath(paramInt), 0, 0, 15, null, null, this.mAid, localMessage);
    }

    public void loadEFTransparent(int paramInt, Message paramMessage)
    {
        Message localMessage = obtainMessage(4, paramInt, 0, paramMessage);
        this.mCi.iccIOForApp(192, paramInt, getEFPath(paramInt), 0, 0, 15, null, null, this.mAid, localMessage);
    }

    protected abstract void logd(String paramString);

    protected abstract void loge(String paramString);

    protected void setAid(String paramString)
    {
        this.mAid = paramString;
    }

    public void updateEFLinearFixed(int paramInt1, int paramInt2, byte[] paramArrayOfByte, String paramString, Message paramMessage)
    {
        this.mCi.iccIOForApp(220, paramInt1, getEFPath(paramInt1), paramInt2, 4, paramArrayOfByte.length, IccUtils.bytesToHexString(paramArrayOfByte), paramString, this.mAid, paramMessage);
    }

    public void updateEFTransparent(int paramInt, byte[] paramArrayOfByte, Message paramMessage)
    {
        this.mCi.iccIOForApp(214, paramInt, getEFPath(paramInt), 0, 0, paramArrayOfByte.length, IccUtils.bytesToHexString(paramArrayOfByte), null, this.mAid, paramMessage);
    }

    static class LoadLinearFixedContext
    {
        int countRecords;
        int efid;
        boolean loadAll;
        Message onLoaded;
        int recordNum;
        int recordSize;
        ArrayList<byte[]> results;

        LoadLinearFixedContext(int paramInt1, int paramInt2, Message paramMessage)
        {
            this.efid = paramInt1;
            this.recordNum = paramInt2;
            this.onLoaded = paramMessage;
            this.loadAll = false;
        }

        LoadLinearFixedContext(int paramInt, Message paramMessage)
        {
            this.efid = paramInt;
            this.recordNum = 1;
            this.loadAll = true;
            this.onLoaded = paramMessage;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccFileHandler
 * JD-Core Version:        0.6.2
 */