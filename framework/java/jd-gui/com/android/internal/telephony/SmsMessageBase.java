package com.android.internal.telephony;

import android.provider.Telephony.Mms;
import android.telephony.SmsMessage.MessageClass;
import java.util.Arrays;

public abstract class SmsMessageBase
{
    private static final String LOG_TAG = "SMS";
    protected String emailBody;
    protected String emailFrom;
    protected int indexOnIcc = -1;
    protected boolean isEmail;
    protected boolean isMwi;
    protected byte[] mPdu;
    protected String messageBody;
    public int messageRef;
    protected boolean mwiDontStore;
    protected boolean mwiSense;
    protected SmsAddress originatingAddress;
    protected String pseudoSubject;
    protected String scAddress;
    protected long scTimeMillis;
    protected int statusOnIcc = -1;
    protected byte[] userData;
    protected SmsHeader userDataHeader;

    protected void extractEmailAddressFromMessageBody()
    {
        String[] arrayOfString = this.messageBody.split("( /)|( )", 2);
        if (arrayOfString.length < 2);
        while (true)
        {
            return;
            this.emailFrom = arrayOfString[0];
            this.emailBody = arrayOfString[1];
            this.isEmail = Telephony.Mms.isEmailAddress(this.emailFrom);
        }
    }

    public String getDisplayMessageBody()
    {
        if (this.isEmail);
        for (String str = this.emailBody; ; str = getMessageBody())
            return str;
    }

    public String getDisplayOriginatingAddress()
    {
        if (this.isEmail);
        for (String str = this.emailFrom; ; str = getOriginatingAddress())
            return str;
    }

    public String getEmailBody()
    {
        return this.emailBody;
    }

    public String getEmailFrom()
    {
        return this.emailFrom;
    }

    public int getIndexOnIcc()
    {
        return this.indexOnIcc;
    }

    public String getMessageBody()
    {
        return this.messageBody;
    }

    public abstract SmsMessage.MessageClass getMessageClass();

    public String getOriginatingAddress()
    {
        if (this.originatingAddress == null);
        for (String str = null; ; str = this.originatingAddress.getAddressString())
            return str;
    }

    public byte[] getPdu()
    {
        return this.mPdu;
    }

    public abstract int getProtocolIdentifier();

    public String getPseudoSubject()
    {
        if (this.pseudoSubject == null);
        for (String str = ""; ; str = this.pseudoSubject)
            return str;
    }

    public String getServiceCenterAddress()
    {
        return this.scAddress;
    }

    public abstract int getStatus();

    public int getStatusOnIcc()
    {
        return this.statusOnIcc;
    }

    public long getTimestampMillis()
    {
        return this.scTimeMillis;
    }

    public byte[] getUserData()
    {
        return this.userData;
    }

    public SmsHeader getUserDataHeader()
    {
        return this.userDataHeader;
    }

    public abstract boolean isCphsMwiMessage();

    public boolean isEmail()
    {
        return this.isEmail;
    }

    public abstract boolean isMWIClearMessage();

    public abstract boolean isMWISetMessage();

    public abstract boolean isMwiDontStore();

    public abstract boolean isReplace();

    public abstract boolean isReplyPathPresent();

    public abstract boolean isStatusReportMessage();

    protected void parseMessageBody()
    {
        if ((this.originatingAddress != null) && (this.originatingAddress.couldBeEmailGateway()))
            extractEmailAddressFromMessageBody();
    }

    public static abstract class SubmitPduBase
    {
        public byte[] encodedMessage;
        public byte[] encodedScAddress;

        public String toString()
        {
            return "SubmitPdu: encodedScAddress = " + Arrays.toString(this.encodedScAddress) + ", encodedMessage = " + Arrays.toString(this.encodedMessage);
        }
    }

    public static class TextEncodingDetails
    {
        public int codeUnitCount;
        public int codeUnitSize;
        public int codeUnitsRemaining;
        public int languageShiftTable;
        public int languageTable;
        public int msgCount;

        public String toString()
        {
            return "TextEncodingDetails { msgCount=" + this.msgCount + ", codeUnitCount=" + this.codeUnitCount + ", codeUnitsRemaining=" + this.codeUnitsRemaining + ", codeUnitSize=" + this.codeUnitSize + ", languageTable=" + this.languageTable + ", languageShiftTable=" + this.languageShiftTable + " }";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.SmsMessageBase
 * JD-Core Version:        0.6.2
 */