package com.android.internal.telephony;

import java.util.HashMap;

public class WspTypeDecoder
{
    public static final String CONTENT_TYPE_B_MMS = "application/vnd.wap.mms-message";
    public static final String CONTENT_TYPE_B_PUSH_CO = "application/vnd.wap.coc";
    public static final String CONTENT_TYPE_B_PUSH_SYNCML_NOTI = "application/vnd.syncml.notification";
    public static final int PARAMETER_ID_X_WAP_APPLICATION_ID = 47;
    public static final int PDU_TYPE_CONFIRMED_PUSH = 7;
    public static final int PDU_TYPE_PUSH = 6;
    private static final int Q_VALUE = 0;
    private static final int WAP_PDU_LENGTH_QUOTE = 31;
    private static final int WAP_PDU_SHORT_LENGTH_MAX = 30;
    private static final HashMap<Integer, String> WELL_KNOWN_MIME_TYPES = new HashMap();
    private static final HashMap<Integer, String> WELL_KNOWN_PARAMETERS = new HashMap();
    HashMap<String, String> contentParameters;
    int dataLength;
    String stringValue;
    long unsigned32bit;
    byte[] wspData;

    static
    {
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(0), "*/*");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(1), "text/*");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(2), "text/html");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(3), "text/plain");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(4), "text/x-hdml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(5), "text/x-ttml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(6), "text/x-vCalendar");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(7), "text/x-vCard");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(8), "text/vnd.wap.wml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(9), "text/vnd.wap.wmlscript");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(10), "text/vnd.wap.wta-event");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(11), "multipart/*");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(12), "multipart/mixed");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(13), "multipart/form-data");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(14), "multipart/byterantes");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(15), "multipart/alternative");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(16), "application/*");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(17), "application/java-vm");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(18), "application/x-www-form-urlencoded");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(19), "application/x-hdmlc");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(20), "application/vnd.wap.wmlc");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(21), "application/vnd.wap.wmlscriptc");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(22), "application/vnd.wap.wta-eventc");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(23), "application/vnd.wap.uaprof");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(24), "application/vnd.wap.wtls-ca-certificate");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(25), "application/vnd.wap.wtls-user-certificate");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(26), "application/x-x509-ca-cert");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(27), "application/x-x509-user-cert");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(28), "image/*");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(29), "image/gif");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(30), "image/jpeg");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(31), "image/tiff");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(32), "image/png");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(33), "image/vnd.wap.wbmp");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(34), "application/vnd.wap.multipart.*");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(35), "application/vnd.wap.multipart.mixed");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(36), "application/vnd.wap.multipart.form-data");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(37), "application/vnd.wap.multipart.byteranges");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(38), "application/vnd.wap.multipart.alternative");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(39), "application/xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(40), "text/xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(41), "application/vnd.wap.wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(42), "application/x-x968-cross-cert");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(43), "application/x-x968-ca-cert");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(44), "application/x-x968-user-cert");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(45), "text/vnd.wap.si");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(46), "application/vnd.wap.sic");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(47), "text/vnd.wap.sl");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(48), "application/vnd.wap.slc");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(49), "text/vnd.wap.co");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(50), "application/vnd.wap.coc");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(51), "application/vnd.wap.multipart.related");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(52), "application/vnd.wap.sia");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(53), "text/vnd.wap.connectivity-xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(54), "application/vnd.wap.connectivity-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(55), "application/pkcs7-mime");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(56), "application/vnd.wap.hashed-certificate");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(57), "application/vnd.wap.signed-certificate");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(58), "application/vnd.wap.cert-response");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(59), "application/xhtml+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(60), "application/wml+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(61), "text/css");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(62), "application/vnd.wap.mms-message");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(63), "application/vnd.wap.rollover-certificate");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(64), "application/vnd.wap.locc+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(65), "application/vnd.wap.loc+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(66), "application/vnd.syncml.dm+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(67), "application/vnd.syncml.dm+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(68), "application/vnd.syncml.notification");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(69), "application/vnd.wap.xhtml+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(70), "application/vnd.wv.csp.cir");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(71), "application/vnd.oma.dd+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(72), "application/vnd.oma.drm.message");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(73), "application/vnd.oma.drm.content");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(74), "application/vnd.oma.drm.rights+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(75), "application/vnd.oma.drm.rights+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(76), "application/vnd.wv.csp+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(77), "application/vnd.wv.csp+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(78), "application/vnd.syncml.ds.notification");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(79), "audio/*");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(80), "video/*");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(81), "application/vnd.oma.dd2+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(82), "application/mikey");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(83), "application/vnd.oma.dcd");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(84), "application/vnd.oma.dcdc");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(513), "application/vnd.uplanet.cacheop-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(514), "application/vnd.uplanet.signal");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(515), "application/vnd.uplanet.alert-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(516), "application/vnd.uplanet.list-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(517), "application/vnd.uplanet.listcmd-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(518), "application/vnd.uplanet.channel-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(519), "application/vnd.uplanet.provisioning-status-uri");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(520), "x-wap.multipart/vnd.uplanet.header-set");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(521), "application/vnd.uplanet.bearer-choice-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(522), "application/vnd.phonecom.mmc-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(523), "application/vnd.nokia.syncset+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(524), "image/x-up-wpng");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(768), "application/iota.mmc-wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(769), "application/iota.mmc-xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(770), "application/vnd.syncml+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(771), "application/vnd.syncml+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(772), "text/vnd.wap.emn+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(773), "text/calendar");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(774), "application/vnd.omads-email+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(775), "application/vnd.omads-file+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(776), "application/vnd.omads-folder+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(777), "text/directory;profile=vCard");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(778), "application/vnd.wap.emn+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(779), "application/vnd.nokia.ipdc-purchase-response");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(780), "application/vnd.motorola.screen3+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(781), "application/vnd.motorola.screen3+gzip");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(782), "application/vnd.cmcc.setting+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(783), "application/vnd.cmcc.bombing+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(784), "application/vnd.docomo.pf");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(785), "application/vnd.docomo.ub");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(786), "application/vnd.omaloc-supl-init");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(787), "application/vnd.oma.group-usage-list+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(788), "application/oma-directory+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(789), "application/vnd.docomo.pf2");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(790), "application/vnd.oma.drm.roap-trigger+wbxml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(791), "application/vnd.sbm.mid2");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(792), "application/vnd.wmf.bootstrap");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(793), "application/vnc.cmcc.dcd+xml");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(794), "application/vnd.sbm.cid");
        WELL_KNOWN_MIME_TYPES.put(Integer.valueOf(795), "application/vnd.oma.bcast.provisioningtrigger");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(0), "Q");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(1), "Charset");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(2), "Level");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(3), "Type");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(7), "Differences");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(8), "Padding");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(9), "Type");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(14), "Max-Age");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(16), "Secure");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(17), "SEC");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(18), "MAC");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(19), "Creation-date");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(20), "Modification-date");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(21), "Read-date");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(22), "Size");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(23), "Name");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(24), "Filename");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(25), "Start");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(26), "Start-info");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(27), "Comment");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(28), "Domain");
        WELL_KNOWN_PARAMETERS.put(Integer.valueOf(29), "Path");
    }

    public WspTypeDecoder(byte[] paramArrayOfByte)
    {
        this.wspData = paramArrayOfByte;
    }

    private boolean decodeNoValue(int paramInt)
    {
        int i = 1;
        if (this.wspData[paramInt] == 0)
            this.dataLength = i;
        while (true)
        {
            return i;
            i = 0;
        }
    }

    private void expandWellKnownMimeType()
    {
        if (this.stringValue == null)
        {
            int i = (int)this.unsigned32bit;
            this.stringValue = ((String)WELL_KNOWN_MIME_TYPES.get(Integer.valueOf(i)));
        }
        while (true)
        {
            return;
            this.unsigned32bit = -1L;
        }
    }

    private boolean readContentParameters(int paramInt1, int paramInt2, int paramInt3)
    {
        boolean bool = false;
        String str1;
        int j;
        int n;
        Object localObject;
        if (paramInt2 > 0)
        {
            int i = this.wspData[paramInt1];
            if (((i & 0x80) == 0) && (i > 31))
            {
                decodeTokenText(paramInt1);
                str1 = this.stringValue;
                j = 0 + this.dataLength;
                if (!decodeNoValue(paramInt1 + j))
                    break label245;
                n = j + this.dataLength;
                localObject = null;
                label74: this.contentParameters.put(str1, localObject);
                bool = readContentParameters(paramInt1 + n, paramInt2 - n, paramInt3 + n);
            }
        }
        while (true)
        {
            return bool;
            if (decodeIntegerValue(paramInt1))
            {
                j = 0 + this.dataLength;
                int k = (int)this.unsigned32bit;
                str1 = (String)WELL_KNOWN_PARAMETERS.get(Integer.valueOf(k));
                if (str1 == null)
                    str1 = "unassigned/0x" + Long.toHexString(k);
                if (k != 0)
                    break;
                if (decodeUintvarInteger(paramInt1 + j))
                {
                    int m = j + this.dataLength;
                    String str2 = String.valueOf(this.unsigned32bit);
                    this.contentParameters.put(str1, str2);
                    bool = readContentParameters(paramInt1 + m, paramInt2 - m, paramInt3 + m);
                    continue;
                    label245: if (decodeIntegerValue(paramInt1 + j))
                    {
                        n = j + this.dataLength;
                        int i1 = (int)this.unsigned32bit;
                        if (i1 == 0)
                        {
                            localObject = "";
                            break label74;
                        }
                        localObject = String.valueOf(i1);
                        break label74;
                    }
                    decodeTokenText(paramInt1 + j);
                    n = j + this.dataLength;
                    localObject = this.stringValue;
                    if (!((String)localObject).startsWith("\""))
                        break label74;
                    localObject = ((String)localObject).substring(1);
                    break label74;
                    this.dataLength = paramInt3;
                    bool = true;
                }
            }
        }
    }

    public boolean decodeConstrainedEncoding(int paramInt)
    {
        boolean bool = true;
        if (decodeShortInteger(paramInt) == bool)
            this.stringValue = null;
        while (true)
        {
            return bool;
            bool = decodeExtensionMedia(paramInt);
        }
    }

    public boolean decodeContentLength(int paramInt)
    {
        return decodeIntegerValue(paramInt);
    }

    public boolean decodeContentLocation(int paramInt)
    {
        return decodeTextString(paramInt);
    }

    public boolean decodeContentType(int paramInt)
    {
        boolean bool = true;
        this.contentParameters = new HashMap();
        try
        {
            if (!decodeValueLength(paramInt))
            {
                bool = decodeConstrainedEncoding(paramInt);
                if (bool)
                    expandWellKnownMimeType();
            }
            else
            {
                int i = (int)this.unsigned32bit;
                int j = getDecodedDataLength();
                if (decodeIntegerValue(paramInt + j) == bool)
                {
                    this.dataLength = (j + this.dataLength);
                    int m = this.dataLength;
                    this.stringValue = null;
                    expandWellKnownMimeType();
                    long l2 = this.unsigned32bit;
                    String str2 = this.stringValue;
                    if (!readContentParameters(paramInt + this.dataLength, i - (this.dataLength - j), 0))
                        break label256;
                    this.dataLength = (m + this.dataLength);
                    this.unsigned32bit = l2;
                    this.stringValue = str2;
                }
                else
                {
                    if (decodeExtensionMedia(paramInt + j) == bool)
                    {
                        this.dataLength = (j + this.dataLength);
                        int k = this.dataLength;
                        expandWellKnownMimeType();
                        long l1 = this.unsigned32bit;
                        String str1 = this.stringValue;
                        if (readContentParameters(paramInt + this.dataLength, i - (this.dataLength - j), 0))
                        {
                            this.dataLength = (k + this.dataLength);
                            this.unsigned32bit = l1;
                            this.stringValue = str1;
                            break label248;
                        }
                    }
                    bool = false;
                }
            }
            label248: return bool;
        }
        catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
        {
            while (true)
            {
                bool = false;
                continue;
                label256: bool = false;
            }
        }
    }

    public boolean decodeExtensionMedia(int paramInt)
    {
        boolean bool = false;
        int i = paramInt;
        this.dataLength = 0;
        this.stringValue = null;
        int j = this.wspData.length;
        if (i < j)
            bool = true;
        while ((i < j) && (this.wspData[i] != 0))
            i++;
        this.dataLength = (1 + (i - paramInt));
        this.stringValue = new String(this.wspData, paramInt, -1 + this.dataLength);
        return bool;
    }

    public boolean decodeIntegerValue(int paramInt)
    {
        boolean bool = true;
        if (decodeShortInteger(paramInt) == bool);
        while (true)
        {
            return bool;
            bool = decodeLongInteger(paramInt);
        }
    }

    public boolean decodeLongInteger(int paramInt)
    {
        int i = 0xFF & this.wspData[paramInt];
        if (i > 30);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.unsigned32bit = 0L;
            for (int j = 1; j <= i; j++)
                this.unsigned32bit = (this.unsigned32bit << 8 | 0xFF & this.wspData[(paramInt + j)]);
            this.dataLength = (i + 1);
        }
    }

    public boolean decodeShortInteger(int paramInt)
    {
        int i = 1;
        if ((0x80 & this.wspData[paramInt]) == 0)
            i = 0;
        while (true)
        {
            return i;
            this.unsigned32bit = (0x7F & this.wspData[paramInt]);
            this.dataLength = i;
        }
    }

    public boolean decodeTextString(int paramInt)
    {
        for (int i = paramInt; this.wspData[i] != 0; i++);
        this.dataLength = (1 + (i - paramInt));
        if (this.wspData[paramInt] == 127);
        for (this.stringValue = new String(this.wspData, paramInt + 1, -2 + this.dataLength); ; this.stringValue = new String(this.wspData, paramInt, -1 + this.dataLength))
            return true;
    }

    public boolean decodeTokenText(int paramInt)
    {
        for (int i = paramInt; this.wspData[i] != 0; i++);
        this.dataLength = (1 + (i - paramInt));
        this.stringValue = new String(this.wspData, paramInt, -1 + this.dataLength);
        return true;
    }

    public boolean decodeUintvarInteger(int paramInt)
    {
        int i = paramInt;
        this.unsigned32bit = 0L;
        if ((0x80 & this.wspData[i]) != 0)
            if (i - paramInt < 4);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.unsigned32bit = (this.unsigned32bit << 7 | 0x7F & this.wspData[i]);
            i++;
            break;
            this.unsigned32bit = (this.unsigned32bit << 7 | 0x7F & this.wspData[i]);
            this.dataLength = (1 + (i - paramInt));
        }
    }

    public boolean decodeValueLength(int paramInt)
    {
        int i = 1;
        if ((0xFF & this.wspData[paramInt]) > 31)
            i = 0;
        while (true)
        {
            return i;
            if (this.wspData[paramInt] < 31)
            {
                this.unsigned32bit = this.wspData[paramInt];
                this.dataLength = i;
            }
            else
            {
                decodeUintvarInteger(paramInt + 1);
                this.dataLength = (1 + this.dataLength);
            }
        }
    }

    public boolean decodeXWapApplicationId(int paramInt)
    {
        boolean bool = true;
        if (decodeIntegerValue(paramInt) == bool)
            this.stringValue = null;
        while (true)
        {
            return bool;
            bool = decodeTextString(paramInt);
        }
    }

    public boolean decodeXWapContentURI(int paramInt)
    {
        return decodeTextString(paramInt);
    }

    public boolean decodeXWapInitiatorURI(int paramInt)
    {
        return decodeTextString(paramInt);
    }

    public HashMap<String, String> getContentParameters()
    {
        return this.contentParameters;
    }

    public int getDecodedDataLength()
    {
        return this.dataLength;
    }

    public long getValue32()
    {
        return this.unsigned32bit;
    }

    public String getValueString()
    {
        return this.stringValue;
    }

    public boolean seekXWapApplicationId(int paramInt1, int paramInt2)
    {
        boolean bool = false;
        int i = paramInt1;
        while (true)
            if (i <= paramInt2)
                try
                {
                    if (decodeIntegerValue(i))
                    {
                        if ((int)getValue32() == 47)
                        {
                            this.unsigned32bit = (i + 1);
                            bool = true;
                            break label198;
                        }
                    }
                    else
                        if (!decodeTextString(i))
                            break label198;
                    int j = i + getDecodedDataLength();
                    if (j <= paramInt2)
                    {
                        int k = this.wspData[j];
                        if ((k >= 0) && (k <= 30))
                        {
                            i = j + (1 + this.wspData[j]);
                            continue;
                        }
                        if (k == 31)
                        {
                            if (j + 1 >= paramInt2)
                                break label198;
                            int n = j + 1;
                            if (!decodeUintvarInteger(n))
                                break label198;
                            i = n + getDecodedDataLength();
                            continue;
                        }
                        if ((31 < k) && (k <= 127))
                        {
                            if (!decodeTextString(j))
                                break label198;
                            int m = getDecodedDataLength();
                            i = j + m;
                            continue;
                        }
                        i = j + 1;
                    }
                }
                catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
                {
                }
        label198: return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.WspTypeDecoder
 * JD-Core Version:        0.6.2
 */