package com.android.internal.telephony;

public class RestrictedState
{
    private boolean mCsEmergencyRestricted;
    private boolean mCsNormalRestricted;
    private boolean mPsRestricted;

    public RestrictedState()
    {
        setPsRestricted(false);
        setCsNormalRestricted(false);
        setCsEmergencyRestricted(false);
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        try
        {
            localRestrictedState = (RestrictedState)paramObject;
            if (paramObject == null)
                return bool;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
            {
                RestrictedState localRestrictedState;
                continue;
                if ((this.mPsRestricted == localRestrictedState.mPsRestricted) && (this.mCsNormalRestricted == localRestrictedState.mCsNormalRestricted) && (this.mCsEmergencyRestricted == localRestrictedState.mCsEmergencyRestricted))
                    bool = true;
            }
        }
    }

    public boolean isCsEmergencyRestricted()
    {
        return this.mCsEmergencyRestricted;
    }

    public boolean isCsNormalRestricted()
    {
        return this.mCsNormalRestricted;
    }

    public boolean isCsRestricted()
    {
        if ((this.mCsNormalRestricted) && (this.mCsEmergencyRestricted));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPsRestricted()
    {
        return this.mPsRestricted;
    }

    public void setCsEmergencyRestricted(boolean paramBoolean)
    {
        this.mCsEmergencyRestricted = paramBoolean;
    }

    public void setCsNormalRestricted(boolean paramBoolean)
    {
        this.mCsNormalRestricted = paramBoolean;
    }

    public void setPsRestricted(boolean paramBoolean)
    {
        this.mPsRestricted = paramBoolean;
    }

    public String toString()
    {
        String str = "none";
        if ((this.mCsEmergencyRestricted) && (this.mCsNormalRestricted))
            str = "all";
        while (true)
        {
            return "Restricted State CS: " + str + " PS:" + this.mPsRestricted;
            if ((this.mCsEmergencyRestricted) && (!this.mCsNormalRestricted))
                str = "emergency";
            else if ((!this.mCsEmergencyRestricted) && (this.mCsNormalRestricted))
                str = "normal call";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.RestrictedState
 * JD-Core Version:        0.6.2
 */