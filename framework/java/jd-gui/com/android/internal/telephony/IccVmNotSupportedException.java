package com.android.internal.telephony;

public final class IccVmNotSupportedException extends IccException
{
    IccVmNotSupportedException()
    {
    }

    public IccVmNotSupportedException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccVmNotSupportedException
 * JD-Core Version:        0.6.2
 */