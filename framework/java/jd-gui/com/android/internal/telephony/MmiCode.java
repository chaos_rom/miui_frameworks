package com.android.internal.telephony;

public abstract interface MmiCode
{
    public abstract void cancel();

    public abstract CharSequence getMessage();

    public abstract State getState();

    public abstract boolean isCancelable();

    public abstract boolean isUssdRequest();

    public static enum State
    {
        static
        {
            CANCELLED = new State("CANCELLED", 1);
            COMPLETE = new State("COMPLETE", 2);
            FAILED = new State("FAILED", 3);
            State[] arrayOfState = new State[4];
            arrayOfState[0] = PENDING;
            arrayOfState[1] = CANCELLED;
            arrayOfState[2] = COMPLETE;
            arrayOfState[3] = FAILED;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.MmiCode
 * JD-Core Version:        0.6.2
 */