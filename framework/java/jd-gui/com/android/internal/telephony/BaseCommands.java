package com.android.internal.telephony;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class BaseCommands
    implements CommandsInterface
{
    static final String LOG_TAG = "RILB";
    private static final String sKernelCmdLine = getProcCmdLine();
    private static final String sLteOnCdmaProductType = SystemProperties.get("telephony.lteOnCdmaProductType", "");
    private static final Pattern sProductTypePattern = Pattern.compile("\\sproduct_type\\s*=\\s*(\\w+)");
    protected RegistrantList mAvailRegistrants = new RegistrantList();
    protected RegistrantList mCallStateRegistrants = new RegistrantList();
    protected RegistrantList mCallWaitingInfoRegistrants = new RegistrantList();
    protected Registrant mCatCallSetUpRegistrant;
    protected Registrant mCatEventRegistrant;
    protected Registrant mCatProCmdRegistrant;
    protected Registrant mCatSessionEndRegistrant;
    protected RegistrantList mCdmaPrlChangedRegistrants = new RegistrantList();
    protected Registrant mCdmaSmsRegistrant;
    protected int mCdmaSubscription;
    protected RegistrantList mCdmaSubscriptionChangedRegistrants = new RegistrantList();
    protected Context mContext;
    protected RegistrantList mDataNetworkStateRegistrants = new RegistrantList();
    protected RegistrantList mDisplayInfoRegistrants = new RegistrantList();
    protected Registrant mEmergencyCallbackModeRegistrant;
    protected RegistrantList mExitEmergencyCallbackModeRegistrants = new RegistrantList();
    protected Registrant mGsmBroadcastSmsRegistrant;
    protected Registrant mGsmSmsRegistrant;
    protected RegistrantList mIccRefreshRegistrants = new RegistrantList();
    protected Registrant mIccSmsFullRegistrant;
    protected RegistrantList mIccStatusChangedRegistrants = new RegistrantList();
    protected RegistrantList mLineControlInfoRegistrants = new RegistrantList();
    protected Registrant mNITZTimeRegistrant;
    protected RegistrantList mNotAvailRegistrants = new RegistrantList();
    protected RegistrantList mNumberInfoRegistrants = new RegistrantList();
    protected RegistrantList mOffOrNotAvailRegistrants = new RegistrantList();
    protected RegistrantList mOnRegistrants = new RegistrantList();
    protected RegistrantList mOtaProvisionRegistrants = new RegistrantList();
    protected int mPhoneType;
    protected int mPreferredNetworkType;
    protected RegistrantList mRadioStateChangedRegistrants = new RegistrantList();
    protected RegistrantList mRedirNumInfoRegistrants = new RegistrantList();
    protected RegistrantList mResendIncallMuteRegistrants = new RegistrantList();
    protected Registrant mRestrictedStateRegistrant;
    protected RegistrantList mRilConnectedRegistrants = new RegistrantList();
    protected int mRilVersion = -1;
    protected Registrant mRingRegistrant;
    protected RegistrantList mRingbackToneRegistrants = new RegistrantList();
    protected RegistrantList mSignalInfoRegistrants = new RegistrantList();
    protected Registrant mSignalStrengthRegistrant;
    protected Registrant mSmsOnSimRegistrant;
    protected Registrant mSmsStatusRegistrant;
    protected Registrant mSsnRegistrant;
    protected CommandsInterface.RadioState mState = CommandsInterface.RadioState.RADIO_UNAVAILABLE;
    protected Object mStateMonitor = new Object();
    protected RegistrantList mT53AudCntrlInfoRegistrants = new RegistrantList();
    protected RegistrantList mT53ClirInfoRegistrants = new RegistrantList();
    protected Registrant mUSSDRegistrant;
    protected Registrant mUnsolOemHookRawRegistrant;
    protected RegistrantList mVoiceNetworkStateRegistrants = new RegistrantList();
    protected RegistrantList mVoicePrivacyOffRegistrants = new RegistrantList();
    protected RegistrantList mVoicePrivacyOnRegistrants = new RegistrantList();
    protected RegistrantList mVoiceRadioTechChangedRegistrants = new RegistrantList();

    public BaseCommands(Context paramContext)
    {
        this.mContext = paramContext;
    }

    public static int getLteOnCdmaModeStatic()
    {
        String str = "";
        int i = SystemProperties.getInt("telephony.lteOnCdmaDevice", -1);
        int j = i;
        if (j == -1)
        {
            Matcher localMatcher = sProductTypePattern.matcher(sKernelCmdLine);
            if (!localMatcher.find())
                break label123;
            str = localMatcher.group(1);
            if (!sLteOnCdmaProductType.equals(str))
                break label118;
            j = 1;
        }
        while (true)
        {
            Log.d("RILB", "getLteOnCdmaMode=" + j + " curVal=" + i + " product_type='" + str + "' lteOnCdmaProductType='" + sLteOnCdmaProductType + "'");
            return j;
            label118: j = 0;
            continue;
            label123: j = 0;
        }
    }

    // ERROR //
    private static String getProcCmdLine()
    {
        // Byte code:
        //     0: ldc 95
        //     2: astore_0
        //     3: aconst_null
        //     4: astore_1
        //     5: new 240	java/io/FileInputStream
        //     8: dup
        //     9: ldc 242
        //     11: invokespecial 245	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     14: astore_2
        //     15: sipush 2048
        //     18: newarray byte
        //     20: astore 9
        //     22: aload_2
        //     23: aload 9
        //     25: invokevirtual 249	java/io/FileInputStream:read	([B)I
        //     28: istore 10
        //     30: iload 10
        //     32: ifle +20 -> 52
        //     35: new 203	java/lang/String
        //     38: dup
        //     39: aload 9
        //     41: iconst_0
        //     42: iload 10
        //     44: invokespecial 252	java/lang/String:<init>	([BII)V
        //     47: astore 11
        //     49: aload 11
        //     51: astore_0
        //     52: aload_2
        //     53: ifnull +7 -> 60
        //     56: aload_2
        //     57: invokevirtual 255	java/io/FileInputStream:close	()V
        //     60: ldc 10
        //     62: new 209	java/lang/StringBuilder
        //     65: dup
        //     66: invokespecial 210	java/lang/StringBuilder:<init>	()V
        //     69: ldc_w 257
        //     72: invokevirtual 216	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     75: aload_0
        //     76: invokevirtual 216	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     79: invokevirtual 230	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     82: invokestatic 236	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     85: pop
        //     86: aload_0
        //     87: areturn
        //     88: astore_3
        //     89: ldc 10
        //     91: new 209	java/lang/StringBuilder
        //     94: dup
        //     95: invokespecial 210	java/lang/StringBuilder:<init>	()V
        //     98: ldc_w 259
        //     101: invokevirtual 216	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     104: aload_3
        //     105: invokevirtual 262	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     108: invokevirtual 230	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     111: invokestatic 236	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     114: pop
        //     115: aload_1
        //     116: ifnull -56 -> 60
        //     119: aload_1
        //     120: invokevirtual 255	java/io/FileInputStream:close	()V
        //     123: goto -63 -> 60
        //     126: astore 7
        //     128: goto -68 -> 60
        //     131: astore 4
        //     133: aload_1
        //     134: ifnull +7 -> 141
        //     137: aload_1
        //     138: invokevirtual 255	java/io/FileInputStream:close	()V
        //     141: aload 4
        //     143: athrow
        //     144: astore 5
        //     146: goto -5 -> 141
        //     149: astore 12
        //     151: goto -91 -> 60
        //     154: astore 4
        //     156: aload_2
        //     157: astore_1
        //     158: goto -25 -> 133
        //     161: astore_3
        //     162: aload_2
        //     163: astore_1
        //     164: goto -75 -> 89
        //
        // Exception table:
        //     from	to	target	type
        //     5	15	88	java/io/IOException
        //     119	123	126	java/io/IOException
        //     5	15	131	finally
        //     89	115	131	finally
        //     137	141	144	java/io/IOException
        //     56	60	149	java/io/IOException
        //     15	49	154	finally
        //     15	49	161	java/io/IOException
    }

    public int getLteOnCdmaMode()
    {
        return getLteOnCdmaModeStatic();
    }

    public CommandsInterface.RadioState getRadioState()
    {
        return this.mState;
    }

    protected void onRadioAvailable()
    {
    }

    public void registerFoT53ClirlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mT53ClirInfoRegistrants.add(localRegistrant);
    }

    public void registerForAvailable(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        synchronized (this.mStateMonitor)
        {
            this.mAvailRegistrants.add(localRegistrant);
            if (this.mState.isAvailable())
                localRegistrant.notifyRegistrant(new AsyncResult(null, null, null));
            return;
        }
    }

    public void registerForCallStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mCallStateRegistrants.add(localRegistrant);
    }

    public void registerForCallWaitingInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mCallWaitingInfoRegistrants.add(localRegistrant);
    }

    public void registerForCdmaOtaProvision(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mOtaProvisionRegistrants.add(localRegistrant);
    }

    public void registerForCdmaPrlChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mCdmaPrlChangedRegistrants.add(localRegistrant);
    }

    public void registerForCdmaSubscriptionChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mCdmaSubscriptionChangedRegistrants.add(localRegistrant);
    }

    public void registerForDataNetworkStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mDataNetworkStateRegistrants.add(localRegistrant);
    }

    public void registerForDisplayInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mDisplayInfoRegistrants.add(localRegistrant);
    }

    public void registerForExitEmergencyCallbackMode(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mExitEmergencyCallbackModeRegistrants.add(localRegistrant);
    }

    public void registerForIccRefresh(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mIccRefreshRegistrants.add(localRegistrant);
    }

    public void registerForIccStatusChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mIccStatusChangedRegistrants.add(localRegistrant);
    }

    public void registerForInCallVoicePrivacyOff(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mVoicePrivacyOffRegistrants.add(localRegistrant);
    }

    public void registerForInCallVoicePrivacyOn(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mVoicePrivacyOnRegistrants.add(localRegistrant);
    }

    public void registerForLineControlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mLineControlInfoRegistrants.add(localRegistrant);
    }

    public void registerForNotAvailable(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        synchronized (this.mStateMonitor)
        {
            this.mNotAvailRegistrants.add(localRegistrant);
            if (!this.mState.isAvailable())
                localRegistrant.notifyRegistrant(new AsyncResult(null, null, null));
            return;
        }
    }

    public void registerForNumberInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mNumberInfoRegistrants.add(localRegistrant);
    }

    public void registerForOffOrNotAvailable(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        synchronized (this.mStateMonitor)
        {
            this.mOffOrNotAvailRegistrants.add(localRegistrant);
            if ((this.mState == CommandsInterface.RadioState.RADIO_OFF) || (!this.mState.isAvailable()))
                localRegistrant.notifyRegistrant(new AsyncResult(null, null, null));
            return;
        }
    }

    public void registerForOn(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        synchronized (this.mStateMonitor)
        {
            this.mOnRegistrants.add(localRegistrant);
            if (this.mState.isOn())
                localRegistrant.notifyRegistrant(new AsyncResult(null, null, null));
            return;
        }
    }

    public void registerForRadioStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        synchronized (this.mStateMonitor)
        {
            this.mRadioStateChangedRegistrants.add(localRegistrant);
            localRegistrant.notifyRegistrant();
            return;
        }
    }

    public void registerForRedirectedNumberInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mRedirNumInfoRegistrants.add(localRegistrant);
    }

    public void registerForResendIncallMute(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mResendIncallMuteRegistrants.add(localRegistrant);
    }

    public void registerForRilConnected(Handler paramHandler, int paramInt, Object paramObject)
    {
        Log.d("RILB", "registerForRilConnected h=" + paramHandler + " w=" + paramInt);
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mRilConnectedRegistrants.add(localRegistrant);
        if (this.mRilVersion != -1)
        {
            Log.d("RILB", "Notifying: ril connected mRilVersion=" + this.mRilVersion);
            localRegistrant.notifyRegistrant(new AsyncResult(null, new Integer(this.mRilVersion), null));
        }
    }

    public void registerForRingbackTone(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mRingbackToneRegistrants.add(localRegistrant);
    }

    public void registerForSignalInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mSignalInfoRegistrants.add(localRegistrant);
    }

    public void registerForT53AudioControlInfo(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mT53AudCntrlInfoRegistrants.add(localRegistrant);
    }

    public void registerForVoiceNetworkStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mVoiceNetworkStateRegistrants.add(localRegistrant);
    }

    public void registerForVoiceRadioTechChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mVoiceRadioTechChangedRegistrants.add(localRegistrant);
    }

    public void setCurrentPreferredNetworkType()
    {
    }

    public void setEmergencyCallbackMode(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mEmergencyCallbackModeRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnCallRing(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mRingRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnCatCallSetUp(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCatCallSetUpRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnCatEvent(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCatEventRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnCatProactiveCmd(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCatProCmdRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnCatSessionEnd(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCatSessionEndRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnIccRefresh(Handler paramHandler, int paramInt, Object paramObject)
    {
        registerForIccRefresh(paramHandler, paramInt, paramObject);
    }

    public void setOnIccSmsFull(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mIccSmsFullRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnNITZTime(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mNITZTimeRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnNewCdmaSms(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCdmaSmsRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnNewGsmBroadcastSms(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mGsmBroadcastSmsRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnNewGsmSms(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mGsmSmsRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnRestrictedStateChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mRestrictedStateRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnSignalStrengthUpdate(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSignalStrengthRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnSmsOnSim(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSmsOnSimRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnSmsStatus(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSmsStatusRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnSuppServiceNotification(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSsnRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnUSSD(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mUSSDRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnUnsolOemHookRaw(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mUnsolOemHookRawRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    protected void setRadioState(CommandsInterface.RadioState paramRadioState)
    {
        synchronized (this.mStateMonitor)
        {
            CommandsInterface.RadioState localRadioState = this.mState;
            this.mState = paramRadioState;
            if (localRadioState != this.mState)
            {
                this.mRadioStateChangedRegistrants.notifyRegistrants();
                if ((this.mState.isAvailable()) && (!localRadioState.isAvailable()))
                {
                    Log.d("RILB", "Notifying: radio available");
                    this.mAvailRegistrants.notifyRegistrants();
                    onRadioAvailable();
                }
                if ((!this.mState.isAvailable()) && (localRadioState.isAvailable()))
                {
                    Log.d("RILB", "Notifying: radio not available");
                    this.mNotAvailRegistrants.notifyRegistrants();
                }
                if ((this.mState.isOn()) && (!localRadioState.isOn()))
                {
                    Log.d("RILB", "Notifying: Radio On");
                    this.mOnRegistrants.notifyRegistrants();
                }
                if (((!this.mState.isOn()) || (!this.mState.isAvailable())) && (localRadioState.isOn()) && (localRadioState.isAvailable()))
                {
                    Log.d("RILB", "Notifying: radio off or not available");
                    this.mOffOrNotAvailRegistrants.notifyRegistrants();
                }
            }
        }
    }

    public void testingEmergencyCall()
    {
    }

    public void unSetOnCallRing(Handler paramHandler)
    {
        this.mRingRegistrant.clear();
    }

    public void unSetOnCatCallSetUp(Handler paramHandler)
    {
        this.mCatCallSetUpRegistrant.clear();
    }

    public void unSetOnCatEvent(Handler paramHandler)
    {
        this.mCatEventRegistrant.clear();
    }

    public void unSetOnCatProactiveCmd(Handler paramHandler)
    {
        this.mCatProCmdRegistrant.clear();
    }

    public void unSetOnCatSessionEnd(Handler paramHandler)
    {
        this.mCatSessionEndRegistrant.clear();
    }

    public void unSetOnIccSmsFull(Handler paramHandler)
    {
        this.mIccSmsFullRegistrant.clear();
    }

    public void unSetOnNITZTime(Handler paramHandler)
    {
        this.mNITZTimeRegistrant.clear();
    }

    public void unSetOnNewCdmaSms(Handler paramHandler)
    {
        this.mCdmaSmsRegistrant.clear();
    }

    public void unSetOnNewGsmBroadcastSms(Handler paramHandler)
    {
        this.mGsmBroadcastSmsRegistrant.clear();
    }

    public void unSetOnNewGsmSms(Handler paramHandler)
    {
        this.mGsmSmsRegistrant.clear();
    }

    public void unSetOnRestrictedStateChanged(Handler paramHandler)
    {
        this.mRestrictedStateRegistrant.clear();
    }

    public void unSetOnSignalStrengthUpdate(Handler paramHandler)
    {
        this.mSignalStrengthRegistrant.clear();
    }

    public void unSetOnSmsOnSim(Handler paramHandler)
    {
        this.mSmsOnSimRegistrant.clear();
    }

    public void unSetOnSmsStatus(Handler paramHandler)
    {
        this.mSmsStatusRegistrant.clear();
    }

    public void unSetOnSuppServiceNotification(Handler paramHandler)
    {
        this.mSsnRegistrant.clear();
    }

    public void unSetOnUSSD(Handler paramHandler)
    {
        this.mUSSDRegistrant.clear();
    }

    public void unSetOnUnsolOemHookRaw(Handler paramHandler)
    {
        this.mUnsolOemHookRawRegistrant.clear();
    }

    public void unregisterForAvailable(Handler paramHandler)
    {
        synchronized (this.mStateMonitor)
        {
            this.mAvailRegistrants.remove(paramHandler);
            return;
        }
    }

    public void unregisterForCallStateChanged(Handler paramHandler)
    {
        this.mCallStateRegistrants.remove(paramHandler);
    }

    public void unregisterForCallWaitingInfo(Handler paramHandler)
    {
        this.mCallWaitingInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForCdmaOtaProvision(Handler paramHandler)
    {
        this.mOtaProvisionRegistrants.remove(paramHandler);
    }

    public void unregisterForCdmaPrlChanged(Handler paramHandler)
    {
        this.mCdmaPrlChangedRegistrants.remove(paramHandler);
    }

    public void unregisterForCdmaSubscriptionChanged(Handler paramHandler)
    {
        this.mCdmaSubscriptionChangedRegistrants.remove(paramHandler);
    }

    public void unregisterForDataNetworkStateChanged(Handler paramHandler)
    {
        this.mDataNetworkStateRegistrants.remove(paramHandler);
    }

    public void unregisterForDisplayInfo(Handler paramHandler)
    {
        this.mDisplayInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForExitEmergencyCallbackMode(Handler paramHandler)
    {
        this.mExitEmergencyCallbackModeRegistrants.remove(paramHandler);
    }

    public void unregisterForIccRefresh(Handler paramHandler)
    {
        this.mIccRefreshRegistrants.remove(paramHandler);
    }

    public void unregisterForIccStatusChanged(Handler paramHandler)
    {
        this.mIccStatusChangedRegistrants.remove(paramHandler);
    }

    public void unregisterForInCallVoicePrivacyOff(Handler paramHandler)
    {
        this.mVoicePrivacyOffRegistrants.remove(paramHandler);
    }

    public void unregisterForInCallVoicePrivacyOn(Handler paramHandler)
    {
        this.mVoicePrivacyOnRegistrants.remove(paramHandler);
    }

    public void unregisterForLineControlInfo(Handler paramHandler)
    {
        this.mLineControlInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForNotAvailable(Handler paramHandler)
    {
        synchronized (this.mStateMonitor)
        {
            this.mNotAvailRegistrants.remove(paramHandler);
            return;
        }
    }

    public void unregisterForNumberInfo(Handler paramHandler)
    {
        this.mNumberInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForOffOrNotAvailable(Handler paramHandler)
    {
        synchronized (this.mStateMonitor)
        {
            this.mOffOrNotAvailRegistrants.remove(paramHandler);
            return;
        }
    }

    public void unregisterForOn(Handler paramHandler)
    {
        synchronized (this.mStateMonitor)
        {
            this.mOnRegistrants.remove(paramHandler);
            return;
        }
    }

    public void unregisterForRadioStateChanged(Handler paramHandler)
    {
        synchronized (this.mStateMonitor)
        {
            this.mRadioStateChangedRegistrants.remove(paramHandler);
            return;
        }
    }

    public void unregisterForRedirectedNumberInfo(Handler paramHandler)
    {
        this.mRedirNumInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForResendIncallMute(Handler paramHandler)
    {
        this.mResendIncallMuteRegistrants.remove(paramHandler);
    }

    public void unregisterForRilConnected(Handler paramHandler)
    {
        this.mRilConnectedRegistrants.remove(paramHandler);
    }

    public void unregisterForRingbackTone(Handler paramHandler)
    {
        this.mRingbackToneRegistrants.remove(paramHandler);
    }

    public void unregisterForSignalInfo(Handler paramHandler)
    {
        this.mSignalInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForT53AudioControlInfo(Handler paramHandler)
    {
        this.mT53AudCntrlInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForT53ClirInfo(Handler paramHandler)
    {
        this.mT53ClirInfoRegistrants.remove(paramHandler);
    }

    public void unregisterForVoiceNetworkStateChanged(Handler paramHandler)
    {
        this.mVoiceNetworkStateRegistrants.remove(paramHandler);
    }

    public void unregisterForVoiceRadioTechChanged(Handler paramHandler)
    {
        this.mVoiceRadioTechChangedRegistrants.remove(paramHandler);
    }

    public void unsetOnIccRefresh(Handler paramHandler)
    {
        unregisterForIccRefresh(paramHandler);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.BaseCommands
 * JD-Core Version:        0.6.2
 */