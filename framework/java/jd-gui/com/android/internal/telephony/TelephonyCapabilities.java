package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.util.Log;

public class TelephonyCapabilities
{
    private static final String LOG_TAG = "TelephonyCapabilities";

    public static int getDeviceIdLabel(Phone paramPhone)
    {
        int i;
        if (paramPhone.getPhoneType() == 1)
            i = 17039574;
        while (true)
        {
            return i;
            if (paramPhone.getPhoneType() == 2)
            {
                i = 17039575;
            }
            else
            {
                Log.w("TelephonyCapabilities", "getDeviceIdLabel: no known label for phone " + paramPhone.getPhoneName());
                i = 0;
            }
        }
    }

    public static boolean supportsAdn(int paramInt)
    {
        int i = 1;
        if (paramInt == i);
        while (true)
        {
            return i;
            i = 0;
        }
    }

    public static boolean supportsAnswerAndHold(Phone paramPhone)
    {
        int i = 1;
        if ((paramPhone.getPhoneType() == i) || (paramPhone.getPhoneType() == 3));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean supportsConferenceCallManagement(Phone paramPhone)
    {
        int i = 1;
        if ((paramPhone.getPhoneType() == i) || (paramPhone.getPhoneType() == 3));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean supportsEcm(Phone paramPhone)
    {
        if (paramPhone.getPhoneType() == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean supportsHoldAndUnhold(Phone paramPhone)
    {
        int i = 1;
        if ((paramPhone.getPhoneType() == i) || (paramPhone.getPhoneType() == 3));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean supportsNetworkSelection(Phone paramPhone)
    {
        int i = 1;
        if (paramPhone.getPhoneType() == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public static boolean supportsOtasp(Phone paramPhone)
    {
        if (paramPhone.getPhoneType() == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean supportsVoiceMessageCount(Phone paramPhone)
    {
        if (paramPhone.getPhoneType() == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.TelephonyCapabilities
 * JD-Core Version:        0.6.2
 */