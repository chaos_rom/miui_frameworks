package com.android.internal.telephony;

import android.app.PendingIntent;
import android.os.RemoteException;
import android.os.ServiceManager;
import java.util.List;

public class IccSmsInterfaceManagerProxy extends ISms.Stub
{
    private IccSmsInterfaceManager mIccSmsInterfaceManager;

    public IccSmsInterfaceManagerProxy(IccSmsInterfaceManager paramIccSmsInterfaceManager)
    {
        this.mIccSmsInterfaceManager = paramIccSmsInterfaceManager;
        if (ServiceManager.getService("isms") == null)
            ServiceManager.addService("isms", this);
    }

    public boolean copyMessageToIccEf(int paramInt, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
        throws RemoteException
    {
        return this.mIccSmsInterfaceManager.copyMessageToIccEf(paramInt, paramArrayOfByte1, paramArrayOfByte2);
    }

    public boolean disableCellBroadcast(int paramInt)
        throws RemoteException
    {
        return this.mIccSmsInterfaceManager.disableCellBroadcast(paramInt);
    }

    public boolean disableCellBroadcastRange(int paramInt1, int paramInt2)
        throws RemoteException
    {
        return this.mIccSmsInterfaceManager.disableCellBroadcastRange(paramInt1, paramInt2);
    }

    public boolean enableCellBroadcast(int paramInt)
        throws RemoteException
    {
        return this.mIccSmsInterfaceManager.enableCellBroadcast(paramInt);
    }

    public boolean enableCellBroadcastRange(int paramInt1, int paramInt2)
        throws RemoteException
    {
        return this.mIccSmsInterfaceManager.enableCellBroadcastRange(paramInt1, paramInt2);
    }

    public List<SmsRawData> getAllMessagesFromIccEf()
        throws RemoteException
    {
        return this.mIccSmsInterfaceManager.getAllMessagesFromIccEf();
    }

    public void sendData(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfByte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        this.mIccSmsInterfaceManager.sendData(paramString1, paramString2, paramInt, paramArrayOfByte, paramPendingIntent1, paramPendingIntent2);
    }

    public void sendMultipartText(String paramString1, String paramString2, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2)
        throws RemoteException
    {
        this.mIccSmsInterfaceManager.sendMultipartText(paramString1, paramString2, paramList, paramList1, paramList2);
    }

    public void sendText(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        this.mIccSmsInterfaceManager.sendText(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
    }

    public void setmIccSmsInterfaceManager(IccSmsInterfaceManager paramIccSmsInterfaceManager)
    {
        this.mIccSmsInterfaceManager = paramIccSmsInterfaceManager;
    }

    public boolean updateMessageOnIccEf(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
        throws RemoteException
    {
        return this.mIccSmsInterfaceManager.updateMessageOnIccEf(paramInt1, paramInt2, paramArrayOfByte);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccSmsInterfaceManagerProxy
 * JD-Core Version:        0.6.2
 */