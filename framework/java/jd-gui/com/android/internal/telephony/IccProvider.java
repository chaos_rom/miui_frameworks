package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.List;

public class IccProvider extends ContentProvider
{
    private static final String[] ADDRESS_BOOK_COLUMN_NAMES;
    private static final int ADN = 1;
    private static final boolean DBG = false;
    private static final int FDN = 2;
    private static final int SDN = 3;
    private static final String STR_EMAILS = "emails";
    private static final String STR_NUMBER = "number";
    private static final String STR_PIN2 = "pin2";
    private static final String STR_TAG = "tag";
    private static final String TAG = "IccProvider";
    private static final UriMatcher URL_MATCHER;

    static
    {
        String[] arrayOfString = new String[4];
        arrayOfString[0] = "name";
        arrayOfString[1] = "number";
        arrayOfString[2] = "emails";
        arrayOfString[3] = "_id";
        ADDRESS_BOOK_COLUMN_NAMES = arrayOfString;
        URL_MATCHER = new UriMatcher(-1);
        URL_MATCHER.addURI("icc", "adn", 1);
        URL_MATCHER.addURI("icc", "fdn", 2);
        URL_MATCHER.addURI("icc", "sdn", 3);
    }

    private boolean addIccRecordToEf(int paramInt, String paramString1, String paramString2, String[] paramArrayOfString, String paramString3)
    {
        boolean bool1 = false;
        try
        {
            IIccPhoneBook localIIccPhoneBook = IIccPhoneBook.Stub.asInterface(ServiceManager.getService("simphonebook"));
            if (localIIccPhoneBook != null)
            {
                boolean bool2 = localIIccPhoneBook.updateAdnRecordsInEfBySearch(paramInt, "", "", paramString1, paramString2, paramString3);
                bool1 = bool2;
            }
            label40: return bool1;
        }
        catch (SecurityException localSecurityException)
        {
            break label40;
        }
        catch (RemoteException localRemoteException)
        {
            break label40;
        }
    }

    private boolean deleteIccRecordFromEf(int paramInt, String paramString1, String paramString2, String[] paramArrayOfString, String paramString3)
    {
        boolean bool1 = false;
        try
        {
            IIccPhoneBook localIIccPhoneBook = IIccPhoneBook.Stub.asInterface(ServiceManager.getService("simphonebook"));
            if (localIIccPhoneBook != null)
            {
                boolean bool2 = localIIccPhoneBook.updateAdnRecordsInEfBySearch(paramInt, paramString1, paramString2, "", "", paramString3);
                bool1 = bool2;
            }
            label40: return bool1;
        }
        catch (SecurityException localSecurityException)
        {
            break label40;
        }
        catch (RemoteException localRemoteException)
        {
            break label40;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static UriMatcher getURL_MATCHER()
    {
        return URL_MATCHER;
    }

    private MatrixCursor loadFromEf(int paramInt)
    {
        Object localObject = null;
        try
        {
            IIccPhoneBook localIIccPhoneBook = IIccPhoneBook.Stub.asInterface(ServiceManager.getService("simphonebook"));
            if (localIIccPhoneBook != null)
            {
                List localList = localIIccPhoneBook.getAdnRecordsInEf(paramInt);
                localObject = localList;
            }
            label30: if (localObject != null)
            {
                int i = localObject.size();
                localMatrixCursor = new MatrixCursor(ADDRESS_BOOK_COLUMN_NAMES, i);
                for (int j = 0; j < i; j++)
                    loadRecord((AdnRecord)localObject.get(j), localMatrixCursor, j);
            }
            Log.w("IccProvider", "Cannot load ADN records");
            MatrixCursor localMatrixCursor = new MatrixCursor(ADDRESS_BOOK_COLUMN_NAMES);
            return localMatrixCursor;
        }
        catch (SecurityException localSecurityException)
        {
            break label30;
        }
        catch (RemoteException localRemoteException)
        {
            break label30;
        }
    }

    private void loadRecord(AdnRecord paramAdnRecord, MatrixCursor paramMatrixCursor, int paramInt)
    {
        if (!paramAdnRecord.isEmpty())
        {
            Object[] arrayOfObject = new Object[4];
            String str1 = paramAdnRecord.getAlphaTag();
            String str2 = paramAdnRecord.getNumber();
            arrayOfObject[0] = str1;
            arrayOfObject[1] = str2;
            String[] arrayOfString = paramAdnRecord.getEmails();
            if (arrayOfString != null)
            {
                StringBuilder localStringBuilder = new StringBuilder();
                int i = arrayOfString.length;
                for (int j = 0; j < i; j++)
                {
                    localStringBuilder.append(arrayOfString[j]);
                    localStringBuilder.append(",");
                }
                arrayOfObject[2] = localStringBuilder.toString();
            }
            arrayOfObject[3] = Integer.valueOf(paramInt);
            paramMatrixCursor.addRow(arrayOfObject);
        }
    }

    private void log(String paramString)
    {
        Log.d("IccProvider", "[IccProvider] " + paramString);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private String normalizeValue(String paramString)
    {
        int i = paramString.length();
        String str1 = paramString;
        if (i <= 1);
        for (String str2 = str1; ; str2 = str1)
        {
            return str2;
            if ((paramString.charAt(0) == '\'') && (paramString.charAt(i - 1) == '\''))
                str1 = paramString.substring(1, i - 1);
        }
    }

    private boolean updateIccRecordInEf(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        boolean bool1 = false;
        try
        {
            IIccPhoneBook localIIccPhoneBook = IIccPhoneBook.Stub.asInterface(ServiceManager.getService("simphonebook"));
            if (localIIccPhoneBook != null)
            {
                boolean bool2 = localIIccPhoneBook.updateAdnRecordsInEfBySearch(paramInt, paramString1, paramString2, paramString3, paramString4, paramString5);
                bool1 = bool2;
            }
            label40: return bool1;
        }
        catch (SecurityException localSecurityException)
        {
            break label40;
        }
        catch (RemoteException localRemoteException)
        {
            break label40;
        }
    }

    public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
    {
        int i;
        String str1;
        String str2;
        String str3;
        String[] arrayOfString1;
        int j;
        switch (URL_MATCHER.match(paramUri))
        {
        default:
            throw new UnsupportedOperationException("Cannot insert into URL: " + paramUri);
        case 1:
            i = 28474;
            str1 = null;
            str2 = null;
            str3 = null;
            arrayOfString1 = paramString.split("AND");
            j = arrayOfString1.length;
        case 2:
        }
        while (true)
        {
            j--;
            if (j < 0)
                break label244;
            String str4 = arrayOfString1[j];
            String[] arrayOfString2 = str4.split("=");
            if (arrayOfString2.length != 2)
            {
                Log.e("IccProvider", "resolve: bad whereClause parameter: " + str4);
                continue;
                i = 28475;
                break;
            }
            String str5 = arrayOfString2[0].trim();
            String str6 = arrayOfString2[1].trim();
            if ("tag".equals(str5))
                str1 = normalizeValue(str6);
            else if ("number".equals(str5))
                str2 = normalizeValue(str6);
            else if ((!"emails".equals(str5)) && ("pin2".equals(str5)))
                str3 = normalizeValue(str6);
        }
        label244: int k;
        if (TextUtils.isEmpty(str2))
            k = 0;
        while (true)
        {
            return k;
            if ((i == 28475) && (TextUtils.isEmpty(str3)))
                k = 0;
            else if (!deleteIccRecordFromEf(i, str1, str2, null, str3))
                k = 0;
            else
                k = 1;
        }
    }

    public String getType(Uri paramUri)
    {
        switch (URL_MATCHER.match(paramUri))
        {
        default:
            throw new IllegalArgumentException("Unknown URL " + paramUri);
        case 1:
        case 2:
        case 3:
        }
        return "vnd.android.cursor.dir/sim-contact";
    }

    public Uri insert(Uri paramUri, ContentValues paramContentValues)
    {
        Uri localUri = null;
        String str = null;
        int i = URL_MATCHER.match(paramUri);
        int j;
        switch (i)
        {
        default:
            throw new UnsupportedOperationException("Cannot insert into URL: " + paramUri);
        case 1:
            j = 28474;
        case 2:
        }
        while (!addIccRecordToEf(j, paramContentValues.getAsString("tag"), paramContentValues.getAsString("number"), null, str))
        {
            return localUri;
            j = 28475;
            str = paramContentValues.getAsString("pin2");
        }
        StringBuilder localStringBuilder = new StringBuilder("content://icc/");
        switch (i)
        {
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            localStringBuilder.append(0);
            localUri = Uri.parse(localStringBuilder.toString());
            break;
            localStringBuilder.append("adn/");
            continue;
            localStringBuilder.append("fdn/");
        }
    }

    public boolean onCreate()
    {
        return true;
    }

    public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        MatrixCursor localMatrixCursor;
        switch (URL_MATCHER.match(paramUri))
        {
        default:
            throw new IllegalArgumentException("Unknown URL " + paramUri);
        case 1:
            localMatrixCursor = loadFromEf(28474);
        case 2:
        case 3:
        }
        while (true)
        {
            return localMatrixCursor;
            localMatrixCursor = loadFromEf(28475);
            continue;
            localMatrixCursor = loadFromEf(28489);
        }
    }

    public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
    {
        String str = null;
        int i;
        switch (URL_MATCHER.match(paramUri))
        {
        default:
            throw new UnsupportedOperationException("Cannot insert into URL: " + paramUri);
        case 1:
            i = 28474;
            if (updateIccRecordInEf(i, paramContentValues.getAsString("tag"), paramContentValues.getAsString("number"), paramContentValues.getAsString("newTag"), paramContentValues.getAsString("newNumber"), str))
                break;
        case 2:
        }
        for (int j = 0; ; j = 1)
        {
            return j;
            i = 28475;
            str = paramContentValues.getAsString("pin2");
            break;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccProvider
 * JD-Core Version:        0.6.2
 */