package com.android.internal.telephony;

import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class DebugService
{
    private static String TAG = "DebugService";

    public DebugService()
    {
        log("DebugService:");
    }

    private static void log(String paramString)
    {
        Log.d(TAG, "DebugService " + paramString);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        log("dump: +");
        try
        {
            localPhoneProxy = (PhoneProxy)PhoneFactory.getDefaultPhone();
        }
        catch (Exception localException6)
        {
            try
            {
                PhoneProxy localPhoneProxy;
                localPhoneBase = (PhoneBase)localPhoneProxy.getActivePhone();
                paramPrintWriter.println();
                paramPrintWriter.println("++++++++++++++++++++++++++++++++");
                paramPrintWriter.flush();
            }
            catch (Exception localException6)
            {
                try
                {
                    localPhoneBase.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
                    paramPrintWriter.flush();
                    paramPrintWriter.println("++++++++++++++++++++++++++++++++");
                }
                catch (Exception localException6)
                {
                    try
                    {
                        localPhoneBase.mDataConnectionTracker.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
                        paramPrintWriter.flush();
                        paramPrintWriter.println("++++++++++++++++++++++++++++++++");
                    }
                    catch (Exception localException6)
                    {
                        try
                        {
                            localPhoneBase.getServiceStateTracker().dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
                            paramPrintWriter.flush();
                            paramPrintWriter.println("++++++++++++++++++++++++++++++++");
                        }
                        catch (Exception localException6)
                        {
                            try
                            {
                                localPhoneBase.getCallTracker().dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
                                paramPrintWriter.flush();
                                paramPrintWriter.println("++++++++++++++++++++++++++++++++");
                            }
                            catch (Exception localException6)
                            {
                                try
                                {
                                    while (true)
                                    {
                                        PhoneBase localPhoneBase;
                                        ((RIL)localPhoneBase.mCM).dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
                                        paramPrintWriter.flush();
                                        paramPrintWriter.println("++++++++++++++++++++++++++++++++");
                                        log("dump: -");
                                        while (true)
                                        {
                                            return;
                                            localException1 = localException1;
                                            paramPrintWriter.println("Telephony DebugService: Could not getDefaultPhone e=" + localException1);
                                            continue;
                                            localException2 = localException2;
                                            paramPrintWriter.println("Telephony DebugService: Could not PhoneBase e=" + localException2);
                                        }
                                        localException3 = localException3;
                                        localException3.printStackTrace();
                                        continue;
                                        localException4 = localException4;
                                        localException4.printStackTrace();
                                        continue;
                                        localException5 = localException5;
                                        localException5.printStackTrace();
                                    }
                                    localException6 = localException6;
                                    localException6.printStackTrace();
                                }
                                catch (Exception localException7)
                                {
                                    while (true)
                                        localException7.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.DebugService
 * JD-Core Version:        0.6.2
 */