package com.android.internal.telephony;

public class CallStateException extends Exception
{
    public CallStateException()
    {
    }

    public CallStateException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.CallStateException
 * JD-Core Version:        0.6.2
 */