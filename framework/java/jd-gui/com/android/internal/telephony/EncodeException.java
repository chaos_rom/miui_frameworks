package com.android.internal.telephony;

public class EncodeException extends Exception
{
    public EncodeException()
    {
    }

    public EncodeException(char paramChar)
    {
        super("Unencodable char: '" + paramChar + "'");
    }

    public EncodeException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.EncodeException
 * JD-Core Version:        0.6.2
 */