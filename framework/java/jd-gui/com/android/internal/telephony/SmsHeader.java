package com.android.internal.telephony;

import com.android.internal.util.HexDump;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class SmsHeader
{
    public static final int ELT_ID_APPLICATION_PORT_ADDRESSING_16_BIT = 5;
    public static final int ELT_ID_APPLICATION_PORT_ADDRESSING_8_BIT = 4;
    public static final int ELT_ID_CHARACTER_SIZE_WVG_OBJECT = 25;
    public static final int ELT_ID_COMPRESSION_CONTROL = 22;
    public static final int ELT_ID_CONCATENATED_16_BIT_REFERENCE = 8;
    public static final int ELT_ID_CONCATENATED_8_BIT_REFERENCE = 0;
    public static final int ELT_ID_ENHANCED_VOICE_MAIL_INFORMATION = 35;
    public static final int ELT_ID_EXTENDED_OBJECT = 20;
    public static final int ELT_ID_EXTENDED_OBJECT_DATA_REQUEST_CMD = 26;
    public static final int ELT_ID_HYPERLINK_FORMAT_ELEMENT = 33;
    public static final int ELT_ID_LARGE_ANIMATION = 14;
    public static final int ELT_ID_LARGE_PICTURE = 16;
    public static final int ELT_ID_NATIONAL_LANGUAGE_LOCKING_SHIFT = 37;
    public static final int ELT_ID_NATIONAL_LANGUAGE_SINGLE_SHIFT = 36;
    public static final int ELT_ID_OBJECT_DISTR_INDICATOR = 23;
    public static final int ELT_ID_PREDEFINED_ANIMATION = 13;
    public static final int ELT_ID_PREDEFINED_SOUND = 11;
    public static final int ELT_ID_REPLY_ADDRESS_ELEMENT = 34;
    public static final int ELT_ID_REUSED_EXTENDED_OBJECT = 21;
    public static final int ELT_ID_RFC_822_EMAIL_HEADER = 32;
    public static final int ELT_ID_SMALL_ANIMATION = 15;
    public static final int ELT_ID_SMALL_PICTURE = 17;
    public static final int ELT_ID_SMSC_CONTROL_PARAMS = 6;
    public static final int ELT_ID_SPECIAL_SMS_MESSAGE_INDICATION = 1;
    public static final int ELT_ID_STANDARD_WVG_OBJECT = 24;
    public static final int ELT_ID_TEXT_FORMATTING = 10;
    public static final int ELT_ID_UDH_SOURCE_INDICATION = 7;
    public static final int ELT_ID_USER_DEFINED_SOUND = 12;
    public static final int ELT_ID_USER_PROMPT_INDICATOR = 19;
    public static final int ELT_ID_VARIABLE_PICTURE = 18;
    public static final int ELT_ID_WIRELESS_CTRL_MSG_PROTOCOL = 9;
    public static final int PORT_WAP_PUSH = 2948;
    public static final int PORT_WAP_WSP = 9200;
    public ConcatRef concatRef;
    public int languageShiftTable;
    public int languageTable;
    public ArrayList<MiscElt> miscEltList = new ArrayList();
    public PortAddrs portAddrs;

    public static SmsHeader fromByteArray(byte[] paramArrayOfByte)
    {
        ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramArrayOfByte);
        SmsHeader localSmsHeader = new SmsHeader();
        while (localByteArrayInputStream.available() > 0)
        {
            int i = localByteArrayInputStream.read();
            int j = localByteArrayInputStream.read();
            switch (i)
            {
            default:
                MiscElt localMiscElt = new MiscElt();
                localMiscElt.id = i;
                localMiscElt.data = new byte[j];
                localByteArrayInputStream.read(localMiscElt.data, 0, j);
                localSmsHeader.miscEltList.add(localMiscElt);
                break;
            case 0:
                ConcatRef localConcatRef2 = new ConcatRef();
                localConcatRef2.refNumber = localByteArrayInputStream.read();
                localConcatRef2.msgCount = localByteArrayInputStream.read();
                localConcatRef2.seqNumber = localByteArrayInputStream.read();
                localConcatRef2.isEightBits = true;
                if ((localConcatRef2.msgCount != 0) && (localConcatRef2.seqNumber != 0) && (localConcatRef2.seqNumber <= localConcatRef2.msgCount))
                    localSmsHeader.concatRef = localConcatRef2;
                break;
            case 8:
                ConcatRef localConcatRef1 = new ConcatRef();
                localConcatRef1.refNumber = (localByteArrayInputStream.read() << 8 | localByteArrayInputStream.read());
                localConcatRef1.msgCount = localByteArrayInputStream.read();
                localConcatRef1.seqNumber = localByteArrayInputStream.read();
                localConcatRef1.isEightBits = false;
                if ((localConcatRef1.msgCount != 0) && (localConcatRef1.seqNumber != 0) && (localConcatRef1.seqNumber <= localConcatRef1.msgCount))
                    localSmsHeader.concatRef = localConcatRef1;
                break;
            case 4:
                PortAddrs localPortAddrs2 = new PortAddrs();
                localPortAddrs2.destPort = localByteArrayInputStream.read();
                localPortAddrs2.origPort = localByteArrayInputStream.read();
                localPortAddrs2.areEightBits = true;
                localSmsHeader.portAddrs = localPortAddrs2;
                break;
            case 5:
                PortAddrs localPortAddrs1 = new PortAddrs();
                localPortAddrs1.destPort = (localByteArrayInputStream.read() << 8 | localByteArrayInputStream.read());
                localPortAddrs1.origPort = (localByteArrayInputStream.read() << 8 | localByteArrayInputStream.read());
                localPortAddrs1.areEightBits = false;
                localSmsHeader.portAddrs = localPortAddrs1;
                break;
            case 36:
                localSmsHeader.languageShiftTable = localByteArrayInputStream.read();
                break;
            case 37:
                localSmsHeader.languageTable = localByteArrayInputStream.read();
            }
        }
        return localSmsHeader;
    }

    public static byte[] toByteArray(SmsHeader paramSmsHeader)
    {
        if ((paramSmsHeader.portAddrs == null) && (paramSmsHeader.concatRef == null) && (paramSmsHeader.miscEltList.isEmpty()) && (paramSmsHeader.languageShiftTable == 0) && (paramSmsHeader.languageTable == 0));
        ByteArrayOutputStream localByteArrayOutputStream;
        for (byte[] arrayOfByte = null; ; arrayOfByte = localByteArrayOutputStream.toByteArray())
        {
            return arrayOfByte;
            localByteArrayOutputStream = new ByteArrayOutputStream(140);
            ConcatRef localConcatRef = paramSmsHeader.concatRef;
            PortAddrs localPortAddrs;
            if (localConcatRef != null)
            {
                if (localConcatRef.isEightBits)
                {
                    localByteArrayOutputStream.write(0);
                    localByteArrayOutputStream.write(3);
                    localByteArrayOutputStream.write(localConcatRef.refNumber);
                    localByteArrayOutputStream.write(localConcatRef.msgCount);
                    localByteArrayOutputStream.write(localConcatRef.seqNumber);
                }
            }
            else
            {
                localPortAddrs = paramSmsHeader.portAddrs;
                if (localPortAddrs != null)
                {
                    if (!localPortAddrs.areEightBits)
                        break label305;
                    localByteArrayOutputStream.write(4);
                    localByteArrayOutputStream.write(2);
                    localByteArrayOutputStream.write(localPortAddrs.destPort);
                    localByteArrayOutputStream.write(localPortAddrs.origPort);
                }
            }
            while (true)
            {
                if (paramSmsHeader.languageShiftTable != 0)
                {
                    localByteArrayOutputStream.write(36);
                    localByteArrayOutputStream.write(1);
                    localByteArrayOutputStream.write(paramSmsHeader.languageShiftTable);
                }
                if (paramSmsHeader.languageTable != 0)
                {
                    localByteArrayOutputStream.write(37);
                    localByteArrayOutputStream.write(1);
                    localByteArrayOutputStream.write(paramSmsHeader.languageTable);
                }
                Iterator localIterator = paramSmsHeader.miscEltList.iterator();
                while (localIterator.hasNext())
                {
                    MiscElt localMiscElt = (MiscElt)localIterator.next();
                    localByteArrayOutputStream.write(localMiscElt.id);
                    localByteArrayOutputStream.write(localMiscElt.data.length);
                    localByteArrayOutputStream.write(localMiscElt.data, 0, localMiscElt.data.length);
                }
                localByteArrayOutputStream.write(8);
                localByteArrayOutputStream.write(4);
                localByteArrayOutputStream.write(localConcatRef.refNumber >>> 8);
                localByteArrayOutputStream.write(0xFF & localConcatRef.refNumber);
                break;
                label305: localByteArrayOutputStream.write(5);
                localByteArrayOutputStream.write(4);
                localByteArrayOutputStream.write(localPortAddrs.destPort >>> 8);
                localByteArrayOutputStream.write(0xFF & localPortAddrs.destPort);
                localByteArrayOutputStream.write(localPortAddrs.origPort >>> 8);
                localByteArrayOutputStream.write(0xFF & localPortAddrs.origPort);
            }
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("UserDataHeader ");
        localStringBuilder.append("{ ConcatRef ");
        if (this.concatRef == null)
        {
            localStringBuilder.append("unset");
            localStringBuilder.append(", PortAddrs ");
            if (this.portAddrs != null)
                break label391;
            localStringBuilder.append("unset");
        }
        while (true)
        {
            if (this.languageShiftTable != 0)
                localStringBuilder.append(", languageShiftTable=" + this.languageShiftTable);
            if (this.languageTable != 0)
                localStringBuilder.append(", languageTable=" + this.languageTable);
            Iterator localIterator = this.miscEltList.iterator();
            while (localIterator.hasNext())
            {
                MiscElt localMiscElt = (MiscElt)localIterator.next();
                localStringBuilder.append(", MiscElt ");
                localStringBuilder.append("{ id=" + localMiscElt.id);
                localStringBuilder.append(", length=" + localMiscElt.data.length);
                localStringBuilder.append(", data=" + HexDump.toHexString(localMiscElt.data));
                localStringBuilder.append(" }");
            }
            localStringBuilder.append("{ refNumber=" + this.concatRef.refNumber);
            localStringBuilder.append(", msgCount=" + this.concatRef.msgCount);
            localStringBuilder.append(", seqNumber=" + this.concatRef.seqNumber);
            localStringBuilder.append(", isEightBits=" + this.concatRef.isEightBits);
            localStringBuilder.append(" }");
            break;
            label391: localStringBuilder.append("{ destPort=" + this.portAddrs.destPort);
            localStringBuilder.append(", origPort=" + this.portAddrs.origPort);
            localStringBuilder.append(", areEightBits=" + this.portAddrs.areEightBits);
            localStringBuilder.append(" }");
        }
        localStringBuilder.append(" }");
        return localStringBuilder.toString();
    }

    public static class MiscElt
    {
        public byte[] data;
        public int id;
    }

    public static class ConcatRef
    {
        public boolean isEightBits;
        public int msgCount;
        public int refNumber;
        public int seqNumber;
    }

    public static class PortAddrs
    {
        public boolean areEightBits;
        public int destPort;
        public int origPort;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.SmsHeader
 * JD-Core Version:        0.6.2
 */