package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.util.SparseArray;
import com.android.internal.telephony.gsm.UsimPhoneBookManager;
import java.util.ArrayList;
import java.util.Iterator;

public final class AdnRecordCache extends Handler
    implements IccConstants
{
    static final int EVENT_LOAD_ALL_ADN_LIKE_DONE = 1;
    static final int EVENT_UPDATE_ADN_DONE = 2;
    SparseArray<ArrayList<AdnRecord>> adnLikeFiles = new SparseArray();
    SparseArray<ArrayList<Message>> adnLikeWaiters = new SparseArray();

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    private AdnCacheManager mAdnCacheManager;
    private IccFileHandler mFh;
    private UsimPhoneBookManager mUsimPhoneBookManager;
    SparseArray<Message> userWriteResponse = new SparseArray();

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public AdnRecordCache(IccFileHandler paramIccFileHandler)
    {
        this.mFh = paramIccFileHandler;
        this.mUsimPhoneBookManager = new UsimPhoneBookManager(this.mFh, this);
        this.mAdnCacheManager = new AdnCacheManager(this);
    }

    private void clearUserWriters()
    {
        int i = this.userWriteResponse.size();
        for (int j = 0; j < i; j++)
            sendErrorResponse((Message)this.userWriteResponse.valueAt(j), "AdnCace reset");
        this.userWriteResponse.clear();
    }

    private void clearWaiters()
    {
        int i = this.adnLikeWaiters.size();
        for (int j = 0; j < i; j++)
            notifyWaiters((ArrayList)this.adnLikeWaiters.valueAt(j), new AsyncResult(null, null, new RuntimeException("AdnCache reset")));
        this.adnLikeWaiters.clear();
    }

    private void notifyWaiters(ArrayList<Message> paramArrayList, AsyncResult paramAsyncResult)
    {
        if (paramArrayList == null);
        while (true)
        {
            return;
            int i = 0;
            int j = paramArrayList.size();
            while (i < j)
            {
                Message localMessage = (Message)paramArrayList.get(i);
                AsyncResult.forMessage(localMessage, paramAsyncResult.result, paramAsyncResult.exception);
                localMessage.sendToTarget();
                i++;
            }
        }
    }

    private void sendErrorResponse(Message paramMessage, String paramString)
    {
        if (paramMessage != null)
        {
            RuntimeException localRuntimeException = new RuntimeException(paramString);
            AsyncResult.forMessage(paramMessage).exception = localRuntimeException;
            paramMessage.sendToTarget();
        }
    }

    int extensionEfForEf(int paramInt)
    {
        int i = 28490;
        switch (paramInt)
        {
        default:
            i = -1;
        case 28474:
        case 28480:
        case 28615:
        case 28489:
        case 28475:
        case 20272:
        }
        while (true)
        {
            return i;
            i = 28616;
            continue;
            i = 28492;
            continue;
            i = 28491;
            continue;
            i = 0;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public int getAdnCapacity()
    {
        return this.mAdnCacheManager.getAdnCapacity();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public int getFreeAdn()
    {
        return this.mAdnCacheManager.getFreeAdn();
    }

    public ArrayList<AdnRecord> getRecordsIfLoaded(int paramInt)
    {
        return (ArrayList)this.adnLikeFiles.get(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
            int k = paramMessage.arg1;
            ArrayList localArrayList = (ArrayList)this.adnLikeWaiters.get(k);
            this.adnLikeWaiters.delete(k);
            if (localAsyncResult2.exception == null)
                this.mAdnCacheManager.handleLoadAllAdnLike(k, localAsyncResult2);
            notifyWaiters(localArrayList, localAsyncResult2);
            continue;
            AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
            int i = paramMessage.arg1;
            int j = paramMessage.arg2;
            AdnRecord localAdnRecord = (AdnRecord)localAsyncResult1.userObj;
            if (localAsyncResult1.exception == null)
            {
                ((ArrayList)this.adnLikeFiles.get(i)).set(j - 1, localAdnRecord);
                this.mUsimPhoneBookManager.invalidateCache();
            }
            Message localMessage = (Message)this.userWriteResponse.get(i);
            this.userWriteResponse.delete(i);
            AsyncResult.forMessage(localMessage, null, localAsyncResult1.exception);
            localMessage.sendToTarget();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void requestLoadAllAdnLike(int paramInt1, int paramInt2, Message paramMessage)
    {
        ArrayList localArrayList1;
        if (paramInt1 == 20272)
        {
            localArrayList1 = this.mUsimPhoneBookManager.loadEfFilesFromUsim();
            if (localArrayList1 == null)
                break label59;
            if (paramMessage != null)
            {
                this.mAdnCacheManager.handleAllAdnLikeLoaded(paramInt1, localArrayList1);
                AsyncResult.forMessage(paramMessage).result = localArrayList1;
                paramMessage.sendToTarget();
            }
        }
        while (true)
        {
            return;
            localArrayList1 = getRecordsIfLoaded(paramInt1);
            break;
            label59: ArrayList localArrayList2 = (ArrayList)this.adnLikeWaiters.get(paramInt1);
            if (localArrayList2 != null)
            {
                localArrayList2.add(paramMessage);
            }
            else
            {
                ArrayList localArrayList3 = new ArrayList();
                localArrayList3.add(paramMessage);
                this.adnLikeWaiters.put(paramInt1, localArrayList3);
                if (paramInt2 < 0)
                {
                    if (paramMessage != null)
                    {
                        AsyncResult.forMessage(paramMessage).exception = new RuntimeException("EF is not known ADN-like EF:" + paramInt1);
                        paramMessage.sendToTarget();
                    }
                }
                else
                    new AdnRecordLoader(this.mFh).loadAllFromEF(paramInt1, paramInt2, obtainMessage(1, paramInt1, 0));
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void reset()
    {
        this.adnLikeFiles.clear();
        this.mUsimPhoneBookManager.reset();
        clearWaiters();
        clearUserWriters();
        this.mAdnCacheManager.reset();
    }

    public void updateAdnByIndex(int paramInt1, AdnRecord paramAdnRecord, int paramInt2, String paramString, Message paramMessage)
    {
        int i = extensionEfForEf(paramInt1);
        if (i < 0)
            sendErrorResponse(paramMessage, "EF is not known ADN-like EF:" + paramInt1);
        while (true)
        {
            return;
            if ((Message)this.userWriteResponse.get(paramInt1) != null)
            {
                sendErrorResponse(paramMessage, "Have pending update for EF:" + paramInt1);
            }
            else
            {
                this.userWriteResponse.put(paramInt1, paramMessage);
                new AdnRecordLoader(this.mFh).updateEF(paramAdnRecord, paramInt1, i, paramInt2, paramString, obtainMessage(2, paramInt1, paramInt2, paramAdnRecord));
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void updateAdnBySearch(int paramInt, AdnRecord paramAdnRecord1, AdnRecord paramAdnRecord2, String paramString, Message paramMessage)
    {
        int i = extensionEfForEf(paramInt);
        if (i < 0)
            sendErrorResponse(paramMessage, "EF is not known ADN-like EF:" + paramInt);
        while (true)
        {
            return;
            if (paramInt == 20272);
            for (ArrayList localArrayList = this.mUsimPhoneBookManager.loadEfFilesFromUsim(); ; localArrayList = getRecordsIfLoaded(paramInt))
            {
                if (localArrayList != null)
                    break label97;
                sendErrorResponse(paramMessage, "Adn list not exist for EF:" + paramInt);
                break;
            }
            label97: int j = -1;
            int k = 1;
            Iterator localIterator = localArrayList.iterator();
            while (true)
            {
                if (localIterator.hasNext())
                {
                    if (paramAdnRecord1.isEqual((AdnRecord)localIterator.next()))
                        j = k;
                }
                else
                {
                    if (j != -1)
                        break label192;
                    sendErrorResponse(paramMessage, "Adn record don't exist for " + paramAdnRecord1);
                    this.mAdnCacheManager.handleNonExistentAdnRecord(paramInt);
                    break;
                }
                k++;
            }
            label192: if (paramInt == 20272)
            {
                AdnRecord localAdnRecord = (AdnRecord)localArrayList.get(j - 1);
                paramInt = localAdnRecord.efid;
                i = localAdnRecord.extRecord;
                j = localAdnRecord.recordNumber;
                paramAdnRecord2.efid = paramInt;
                paramAdnRecord2.extRecord = i;
                paramAdnRecord2.recordNumber = j;
            }
            this.mAdnCacheManager.handleUpdateAdnRecord(paramInt, paramAdnRecord1, paramAdnRecord2);
            if ((Message)this.userWriteResponse.get(paramInt) != null)
            {
                sendErrorResponse(paramMessage, "Have pending update for EF:" + paramInt);
            }
            else
            {
                this.userWriteResponse.put(paramInt, paramMessage);
                AdnRecordLoader localAdnRecordLoader = new AdnRecordLoader(this.mFh);
                Message localMessage = obtainMessage(2, paramInt, j, paramAdnRecord2);
                localAdnRecordLoader.updateEF(paramAdnRecord2, paramInt, i, j, paramString, localMessage);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.AdnRecordCache
 * JD-Core Version:        0.6.2
 */