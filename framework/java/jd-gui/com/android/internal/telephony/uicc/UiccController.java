package com.android.internal.telephony.uicc;

import android.util.Log;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.cdma.CDMALTEPhone;
import com.android.internal.telephony.cdma.CDMAPhone;
import com.android.internal.telephony.gsm.GSMPhone;

public class UiccController
{
    private static final boolean DBG = true;
    private static final String LOG_TAG = "RIL_UiccController";
    private static UiccController mInstance;
    private PhoneBase mCurrentPhone;
    private IccCard mIccCard;
    private boolean mIsCurrentCard3gpp;

    private UiccController(PhoneBase paramPhoneBase)
    {
        log("Creating UiccController");
        setNewPhone(paramPhoneBase);
    }

    /** @deprecated */
    public static UiccController getInstance(PhoneBase paramPhoneBase)
    {
        try
        {
            if (mInstance == null)
                mInstance = new UiccController(paramPhoneBase);
            while (true)
            {
                UiccController localUiccController = mInstance;
                return localUiccController;
                mInstance.setNewPhone(paramPhoneBase);
            }
        }
        finally
        {
        }
    }

    private void log(String paramString)
    {
        Log.d("RIL_UiccController", paramString);
    }

    private void setNewPhone(PhoneBase paramPhoneBase)
    {
        this.mCurrentPhone = paramPhoneBase;
        if ((paramPhoneBase instanceof GSMPhone))
        {
            log("New phone is GSMPhone");
            updateCurrentCard(true);
        }
        while (true)
        {
            return;
            if ((paramPhoneBase instanceof CDMALTEPhone))
            {
                log("New phone type is CDMALTEPhone");
                updateCurrentCard(true);
            }
            else if ((paramPhoneBase instanceof CDMAPhone))
            {
                log("New phone type is CDMAPhone");
                updateCurrentCard(false);
            }
            else
            {
                Log.e("RIL_UiccController", "Unhandled phone type. Critical error!");
            }
        }
    }

    private void updateCurrentCard(boolean paramBoolean)
    {
        if ((this.mIsCurrentCard3gpp == paramBoolean) && (this.mIccCard != null));
        while (true)
        {
            return;
            if (this.mIccCard != null)
            {
                this.mIccCard.dispose();
                this.mIccCard = null;
            }
            this.mIsCurrentCard3gpp = paramBoolean;
            this.mIccCard = new IccCard(this.mCurrentPhone, this.mCurrentPhone.getPhoneName(), Boolean.valueOf(paramBoolean), Boolean.valueOf(true));
        }
    }

    public IccCard getIccCard()
    {
        return this.mIccCard;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.uicc.UiccController
 * JD-Core Version:        0.6.2
 */