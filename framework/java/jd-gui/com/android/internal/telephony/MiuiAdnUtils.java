package com.android.internal.telephony;

import android.util.Log;
import java.io.UnsupportedEncodingException;

public class MiuiAdnUtils
{
    private static final String TAG = "MiuiAdnUtils";

    public static boolean encodeAlphaTag(byte[] paramArrayOfByte, String paramString, int paramInt)
    {
        int i = 1;
        try
        {
            GsmAlphabet.stringToGsm7BitPacked(paramString);
            byte[] arrayOfByte4 = GsmAlphabet.stringToGsm8BitPacked(paramString);
            arrayOfByte3 = arrayOfByte4;
            if (arrayOfByte3.length > paramInt)
            {
                System.arraycopy(arrayOfByte3, 0, paramArrayOfByte, 0, paramInt);
                return i;
            }
        }
        catch (EncodeException localEncodeException)
        {
            while (true)
            {
                byte[] arrayOfByte3;
                Log.w("MiuiAdnUtils", "[buildAdnString]    can't encode with GSM alphabet, try utf-16be");
                byte[] arrayOfByte1 = new byte[i];
                arrayOfByte1[0] = -128;
                try
                {
                    byte[] arrayOfByte2 = paramString.getBytes("utf-16be");
                    arrayOfByte3 = new byte[1 + arrayOfByte2.length];
                    System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, i);
                    System.arraycopy(arrayOfByte2, 0, arrayOfByte3, i, arrayOfByte2.length);
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException)
                {
                    Log.e("MiuiAdnUtils", "Implausible UnsupportedEncodingException ", localUnsupportedEncodingException);
                    int j = 0;
                }
                continue;
                paramInt = arrayOfByte3.length;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.MiuiAdnUtils
 * JD-Core Version:        0.6.2
 */