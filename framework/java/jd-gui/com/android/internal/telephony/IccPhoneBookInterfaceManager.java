package com.android.internal.telephony;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ServiceManager;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class IccPhoneBookInterfaceManager extends IIccPhoneBook.Stub
{
    protected static final boolean ALLOW_SIM_OP_IN_UI_THREAD = false;
    protected static final boolean DBG = true;
    protected static final int EVENT_GET_SIZE_DONE = 1;
    protected static final int EVENT_LOAD_DONE = 2;
    protected static final int EVENT_UPDATE_DONE = 3;
    protected AdnRecordCache adnCache;
    protected Handler mBaseHandler = new Handler()
    {
        private void notifyPending(AsyncResult paramAnonymousAsyncResult)
        {
            if (paramAnonymousAsyncResult.userObj == null);
            while (true)
            {
                return;
                ((AtomicBoolean)paramAnonymousAsyncResult.userObj).set(true);
                IccPhoneBookInterfaceManager.this.mLock.notifyAll();
            }
        }

        // ERROR //
        public void handleMessage(Message paramAnonymousMessage)
        {
            // Byte code:
            //     0: iconst_1
            //     1: istore_2
            //     2: aload_1
            //     3: getfield 45	android/os/Message:what	I
            //     6: tableswitch	default:+26 -> 32, 1:+27->33, 2:+228->234, 3:+162->168
            //     33: aload_1
            //     34: getfield 48	android/os/Message:obj	Ljava/lang/Object;
            //     37: checkcast 19	android/os/AsyncResult
            //     40: astore 10
            //     42: aload_0
            //     43: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     46: getfield 32	com/android/internal/telephony/IccPhoneBookInterfaceManager:mLock	Ljava/lang/Object;
            //     49: astore 11
            //     51: aload 11
            //     53: monitorenter
            //     54: aload 10
            //     56: getfield 52	android/os/AsyncResult:exception	Ljava/lang/Throwable;
            //     59: ifnonnull +89 -> 148
            //     62: aload_0
            //     63: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     66: aload 10
            //     68: getfield 55	android/os/AsyncResult:result	Ljava/lang/Object;
            //     71: checkcast 57	[I
            //     74: checkcast 57	[I
            //     77: putfield 60	com/android/internal/telephony/IccPhoneBookInterfaceManager:recordSize	[I
            //     80: aload_0
            //     81: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     84: new 62	java/lang/StringBuilder
            //     87: dup
            //     88: invokespecial 63	java/lang/StringBuilder:<init>	()V
            //     91: ldc 65
            //     93: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     96: aload_0
            //     97: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     100: getfield 60	com/android/internal/telephony/IccPhoneBookInterfaceManager:recordSize	[I
            //     103: iconst_0
            //     104: iaload
            //     105: invokevirtual 72	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     108: ldc 74
            //     110: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     113: aload_0
            //     114: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     117: getfield 60	com/android/internal/telephony/IccPhoneBookInterfaceManager:recordSize	[I
            //     120: iconst_1
            //     121: iaload
            //     122: invokevirtual 72	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     125: ldc 76
            //     127: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     130: aload_0
            //     131: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     134: getfield 60	com/android/internal/telephony/IccPhoneBookInterfaceManager:recordSize	[I
            //     137: iconst_2
            //     138: iaload
            //     139: invokevirtual 72	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     142: invokevirtual 80	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     145: invokevirtual 84	com/android/internal/telephony/IccPhoneBookInterfaceManager:logd	(Ljava/lang/String;)V
            //     148: aload_0
            //     149: aload 10
            //     151: invokespecial 86	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:notifyPending	(Landroid/os/AsyncResult;)V
            //     154: aload 11
            //     156: monitorexit
            //     157: goto -125 -> 32
            //     160: astore 12
            //     162: aload 11
            //     164: monitorexit
            //     165: aload 12
            //     167: athrow
            //     168: aload_1
            //     169: getfield 48	android/os/Message:obj	Ljava/lang/Object;
            //     172: checkcast 19	android/os/AsyncResult
            //     175: astore 6
            //     177: aload_0
            //     178: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     181: getfield 32	com/android/internal/telephony/IccPhoneBookInterfaceManager:mLock	Ljava/lang/Object;
            //     184: astore 7
            //     186: aload 7
            //     188: monitorenter
            //     189: aload_0
            //     190: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     193: astore 9
            //     195: aload 6
            //     197: getfield 52	android/os/AsyncResult:exception	Ljava/lang/Throwable;
            //     200: ifnonnull +29 -> 229
            //     203: aload 9
            //     205: iload_2
            //     206: putfield 90	com/android/internal/telephony/IccPhoneBookInterfaceManager:success	Z
            //     209: aload_0
            //     210: aload 6
            //     212: invokespecial 86	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:notifyPending	(Landroid/os/AsyncResult;)V
            //     215: aload 7
            //     217: monitorexit
            //     218: goto -186 -> 32
            //     221: astore 8
            //     223: aload 7
            //     225: monitorexit
            //     226: aload 8
            //     228: athrow
            //     229: iconst_0
            //     230: istore_2
            //     231: goto -28 -> 203
            //     234: aload_1
            //     235: getfield 48	android/os/Message:obj	Ljava/lang/Object;
            //     238: checkcast 19	android/os/AsyncResult
            //     241: astore_3
            //     242: aload_0
            //     243: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     246: getfield 32	com/android/internal/telephony/IccPhoneBookInterfaceManager:mLock	Ljava/lang/Object;
            //     249: astore 4
            //     251: aload 4
            //     253: monitorenter
            //     254: aload_3
            //     255: getfield 52	android/os/AsyncResult:exception	Ljava/lang/Throwable;
            //     258: ifnonnull +36 -> 294
            //     261: aload_0
            //     262: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     265: aload_3
            //     266: getfield 55	android/os/AsyncResult:result	Ljava/lang/Object;
            //     269: checkcast 92	java/util/List
            //     272: putfield 96	com/android/internal/telephony/IccPhoneBookInterfaceManager:records	Ljava/util/List;
            //     275: aload_0
            //     276: aload_3
            //     277: invokespecial 86	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:notifyPending	(Landroid/os/AsyncResult;)V
            //     280: aload 4
            //     282: monitorexit
            //     283: goto -251 -> 32
            //     286: astore 5
            //     288: aload 4
            //     290: monitorexit
            //     291: aload 5
            //     293: athrow
            //     294: aload_0
            //     295: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     298: ldc 98
            //     300: invokevirtual 84	com/android/internal/telephony/IccPhoneBookInterfaceManager:logd	(Ljava/lang/String;)V
            //     303: aload_0
            //     304: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     307: getfield 96	com/android/internal/telephony/IccPhoneBookInterfaceManager:records	Ljava/util/List;
            //     310: ifnull -35 -> 275
            //     313: aload_0
            //     314: getfield 12	com/android/internal/telephony/IccPhoneBookInterfaceManager$1:this$0	Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
            //     317: getfield 96	com/android/internal/telephony/IccPhoneBookInterfaceManager:records	Ljava/util/List;
            //     320: invokeinterface 101 1 0
            //     325: goto -50 -> 275
            //
            // Exception table:
            //     from	to	target	type
            //     54	165	160	finally
            //     189	226	221	finally
            //     254	291	286	finally
            //     294	325	286	finally
        }
    };
    protected final Object mLock = new Object();
    protected PhoneBase phone;
    protected int[] recordSize;
    protected List<AdnRecord> records;
    protected boolean success;

    public IccPhoneBookInterfaceManager(PhoneBase paramPhoneBase)
    {
        this.phone = paramPhoneBase;
    }

    private int updateEfForIccType(int paramInt)
    {
        if ((paramInt == 28474) && (this.phone.getIccCard().isApplicationOnIcc(IccCardApplication.AppType.APPTYPE_USIM)))
            paramInt = 20272;
        return paramInt;
    }

    protected void checkThread()
    {
        if (this.mBaseHandler.getLooper().equals(Looper.myLooper()))
        {
            loge("query() called on the main UI thread!");
            throw new IllegalStateException("You cannot call query on this provder from the main UI thread.");
        }
    }

    public void dispose()
    {
    }

    public int getAdnCapacity()
    {
        return this.adnCache.getAdnCapacity();
    }

    public List<AdnRecord> getAdnRecordsInEf(int paramInt)
    {
        if (this.phone.getContext().checkCallingOrSelfPermission("android.permission.READ_CONTACTS") != 0)
            throw new SecurityException("Requires android.permission.READ_CONTACTS permission");
        int i = updateEfForIccType(paramInt);
        logd("getAdnRecordsInEF: efid=" + i);
        synchronized (this.mLock)
        {
            checkThread();
            AtomicBoolean localAtomicBoolean = new AtomicBoolean(false);
            Message localMessage = this.mBaseHandler.obtainMessage(2, localAtomicBoolean);
            this.adnCache.requestLoadAllAdnLike(i, this.adnCache.extensionEfForEf(i), localMessage);
            waitForResult(localAtomicBoolean);
            return this.records;
        }
    }

    public abstract int[] getAdnRecordsSize(int paramInt);

    public int getFreeAdn()
    {
        return this.adnCache.getFreeAdn();
    }

    protected abstract void logd(String paramString);

    protected abstract void loge(String paramString);

    protected void publish()
    {
        ServiceManager.addService("simphonebook", this);
    }

    public boolean updateAdnRecordsInEfByIndex(int paramInt1, String paramString1, String paramString2, int paramInt2, String paramString3)
    {
        if (this.phone.getContext().checkCallingOrSelfPermission("android.permission.WRITE_CONTACTS") != 0)
            throw new SecurityException("Requires android.permission.WRITE_CONTACTS permission");
        logd("updateAdnRecordsInEfByIndex: efid=" + paramInt1 + " Index=" + paramInt2 + " ==> " + "(" + paramString1 + "," + paramString2 + ")" + " pin2=" + paramString3);
        synchronized (this.mLock)
        {
            checkThread();
            this.success = false;
            AtomicBoolean localAtomicBoolean = new AtomicBoolean(false);
            Message localMessage = this.mBaseHandler.obtainMessage(3, localAtomicBoolean);
            AdnRecord localAdnRecord = new AdnRecord(paramString1, paramString2);
            this.adnCache.updateAdnByIndex(paramInt1, localAdnRecord, paramInt2, paramString3, localMessage);
            waitForResult(localAtomicBoolean);
            return this.success;
        }
    }

    public boolean updateAdnRecordsInEfBySearch(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        if (this.phone.getContext().checkCallingOrSelfPermission("android.permission.WRITE_CONTACTS") != 0)
            throw new SecurityException("Requires android.permission.WRITE_CONTACTS permission");
        logd("updateAdnRecordsInEfBySearch: efid=" + paramInt + " (" + paramString1 + "," + paramString2 + ")" + "==>" + " (" + paramString3 + "," + paramString4 + ")" + " pin2=" + paramString5);
        int i = updateEfForIccType(paramInt);
        synchronized (this.mLock)
        {
            checkThread();
            this.success = false;
            AtomicBoolean localAtomicBoolean = new AtomicBoolean(false);
            Message localMessage = this.mBaseHandler.obtainMessage(3, localAtomicBoolean);
            AdnRecord localAdnRecord1 = new AdnRecord(paramString1, paramString2);
            AdnRecord localAdnRecord2 = new AdnRecord(paramString3, paramString4);
            this.adnCache.updateAdnBySearch(i, localAdnRecord1, localAdnRecord2, paramString5, localMessage);
            waitForResult(localAtomicBoolean);
            return this.success;
        }
    }

    protected void waitForResult(AtomicBoolean paramAtomicBoolean)
    {
        while (!paramAtomicBoolean.get())
            try
            {
                this.mLock.wait();
            }
            catch (InterruptedException localInterruptedException)
            {
                logd("interrupted while trying to update by search");
            }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccPhoneBookInterfaceManager
 * JD-Core Version:        0.6.2
 */