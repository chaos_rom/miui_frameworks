package com.android.internal.telephony;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.telephony.CellInfo;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;

public abstract interface IPhoneStateListener extends IInterface
{
    public abstract void onCallForwardingIndicatorChanged(boolean paramBoolean)
        throws RemoteException;

    public abstract void onCallStateChanged(int paramInt, String paramString)
        throws RemoteException;

    public abstract void onCellInfoChanged(CellInfo paramCellInfo)
        throws RemoteException;

    public abstract void onCellLocationChanged(Bundle paramBundle)
        throws RemoteException;

    public abstract void onDataActivity(int paramInt)
        throws RemoteException;

    public abstract void onDataConnectionStateChanged(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void onMessageWaitingIndicatorChanged(boolean paramBoolean)
        throws RemoteException;

    public abstract void onOtaspChanged(int paramInt)
        throws RemoteException;

    public abstract void onServiceStateChanged(ServiceState paramServiceState)
        throws RemoteException;

    public abstract void onSignalStrengthChanged(int paramInt)
        throws RemoteException;

    public abstract void onSignalStrengthsChanged(SignalStrength paramSignalStrength)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IPhoneStateListener
    {
        private static final String DESCRIPTOR = "com.android.internal.telephony.IPhoneStateListener";
        static final int TRANSACTION_onCallForwardingIndicatorChanged = 4;
        static final int TRANSACTION_onCallStateChanged = 6;
        static final int TRANSACTION_onCellInfoChanged = 11;
        static final int TRANSACTION_onCellLocationChanged = 5;
        static final int TRANSACTION_onDataActivity = 8;
        static final int TRANSACTION_onDataConnectionStateChanged = 7;
        static final int TRANSACTION_onMessageWaitingIndicatorChanged = 3;
        static final int TRANSACTION_onOtaspChanged = 10;
        static final int TRANSACTION_onServiceStateChanged = 1;
        static final int TRANSACTION_onSignalStrengthChanged = 2;
        static final int TRANSACTION_onSignalStrengthsChanged = 9;

        public Stub()
        {
            attachInterface(this, "com.android.internal.telephony.IPhoneStateListener");
        }

        public static IPhoneStateListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.telephony.IPhoneStateListener");
                if ((localIInterface != null) && ((localIInterface instanceof IPhoneStateListener)))
                    localObject = (IPhoneStateListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = false;
            boolean bool2 = true;
            switch (paramInt1)
            {
            default:
                bool2 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                while (true)
                {
                    return bool2;
                    paramParcel2.writeString("com.android.internal.telephony.IPhoneStateListener");
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    if (paramParcel1.readInt() != 0);
                    for (ServiceState localServiceState = (ServiceState)ServiceState.CREATOR.createFromParcel(paramParcel1); ; localServiceState = null)
                    {
                        onServiceStateChanged(localServiceState);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    onSignalStrengthChanged(paramParcel1.readInt());
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    if (paramParcel1.readInt() != 0)
                        bool1 = bool2;
                    onMessageWaitingIndicatorChanged(bool1);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    if (paramParcel1.readInt() != 0)
                        bool1 = bool2;
                    onCallForwardingIndicatorChanged(bool1);
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    if (paramParcel1.readInt() != 0);
                    for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                    {
                        onCellLocationChanged(localBundle);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    onCallStateChanged(paramParcel1.readInt(), paramParcel1.readString());
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    onDataConnectionStateChanged(paramParcel1.readInt(), paramParcel1.readInt());
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    onDataActivity(paramParcel1.readInt());
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    if (paramParcel1.readInt() != 0);
                    for (SignalStrength localSignalStrength = (SignalStrength)SignalStrength.CREATOR.createFromParcel(paramParcel1); ; localSignalStrength = null)
                    {
                        onSignalStrengthsChanged(localSignalStrength);
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
                    onOtaspChanged(paramParcel1.readInt());
                }
            case 11:
            }
            paramParcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (paramParcel1.readInt() != 0);
            for (CellInfo localCellInfo = (CellInfo)CellInfo.CREATOR.createFromParcel(paramParcel1); ; localCellInfo = null)
            {
                onCellInfoChanged(localCellInfo);
                break;
            }
        }

        private static class Proxy
            implements IPhoneStateListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.telephony.IPhoneStateListener";
            }

            public void onCallForwardingIndicatorChanged(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(4, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onCallStateChanged(int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    localParcel.writeInt(paramInt);
                    localParcel.writeString(paramString);
                    this.mRemote.transact(6, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onCellInfoChanged(CellInfo paramCellInfo)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    if (paramCellInfo != null)
                    {
                        localParcel.writeInt(1);
                        paramCellInfo.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(11, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onCellLocationChanged(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    if (paramBundle != null)
                    {
                        localParcel.writeInt(1);
                        paramBundle.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(5, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onDataActivity(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(8, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onDataConnectionStateChanged(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(7, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onMessageWaitingIndicatorChanged(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onOtaspChanged(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(10, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onServiceStateChanged(ServiceState paramServiceState)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    if (paramServiceState != null)
                    {
                        localParcel.writeInt(1);
                        paramServiceState.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onSignalStrengthChanged(int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onSignalStrengthsChanged(SignalStrength paramSignalStrength)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
                    if (paramSignalStrength != null)
                    {
                        localParcel.writeInt(1);
                        paramSignalStrength.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IPhoneStateListener
 * JD-Core Version:        0.6.2
 */