package com.android.internal.telephony;

import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.telephony.CellInfo;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;

public abstract interface ITelephonyRegistry extends IInterface
{
    public abstract void listen(String paramString, IPhoneStateListener paramIPhoneStateListener, int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract void notifyCallForwardingChanged(boolean paramBoolean)
        throws RemoteException;

    public abstract void notifyCallState(int paramInt, String paramString)
        throws RemoteException;

    public abstract void notifyCellInfo(CellInfo paramCellInfo)
        throws RemoteException;

    public abstract void notifyCellLocation(Bundle paramBundle)
        throws RemoteException;

    public abstract void notifyDataActivity(int paramInt)
        throws RemoteException;

    public abstract void notifyDataConnection(int paramInt1, boolean paramBoolean1, String paramString1, String paramString2, String paramString3, LinkProperties paramLinkProperties, LinkCapabilities paramLinkCapabilities, int paramInt2, boolean paramBoolean2)
        throws RemoteException;

    public abstract void notifyDataConnectionFailed(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void notifyMessageWaitingChanged(boolean paramBoolean)
        throws RemoteException;

    public abstract void notifyOtaspChanged(int paramInt)
        throws RemoteException;

    public abstract void notifyServiceState(ServiceState paramServiceState)
        throws RemoteException;

    public abstract void notifySignalStrength(SignalStrength paramSignalStrength)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ITelephonyRegistry
    {
        private static final String DESCRIPTOR = "com.android.internal.telephony.ITelephonyRegistry";
        static final int TRANSACTION_listen = 1;
        static final int TRANSACTION_notifyCallForwardingChanged = 6;
        static final int TRANSACTION_notifyCallState = 2;
        static final int TRANSACTION_notifyCellInfo = 12;
        static final int TRANSACTION_notifyCellLocation = 10;
        static final int TRANSACTION_notifyDataActivity = 7;
        static final int TRANSACTION_notifyDataConnection = 8;
        static final int TRANSACTION_notifyDataConnectionFailed = 9;
        static final int TRANSACTION_notifyMessageWaitingChanged = 5;
        static final int TRANSACTION_notifyOtaspChanged = 11;
        static final int TRANSACTION_notifyServiceState = 3;
        static final int TRANSACTION_notifySignalStrength = 4;

        public Stub()
        {
            attachInterface(this, "com.android.internal.telephony.ITelephonyRegistry");
        }

        public static ITelephonyRegistry asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.internal.telephony.ITelephonyRegistry");
                if ((localIInterface != null) && ((localIInterface instanceof ITelephonyRegistry)))
                    localObject = (ITelephonyRegistry)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("com.android.internal.telephony.ITelephonyRegistry");
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    String str4 = paramParcel1.readString();
                    IPhoneStateListener localIPhoneStateListener = IPhoneStateListener.Stub.asInterface(paramParcel1.readStrongBinder());
                    int k = paramParcel1.readInt();
                    if (paramParcel1.readInt() != 0);
                    for (boolean bool6 = true; ; bool6 = false)
                    {
                        listen(str4, localIPhoneStateListener, k, bool6);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    notifyCallState(paramParcel1.readInt(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramParcel1.readInt() != 0);
                    for (ServiceState localServiceState = (ServiceState)ServiceState.CREATOR.createFromParcel(paramParcel1); ; localServiceState = null)
                    {
                        notifyServiceState(localServiceState);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramParcel1.readInt() != 0);
                    for (SignalStrength localSignalStrength = (SignalStrength)SignalStrength.CREATOR.createFromParcel(paramParcel1); ; localSignalStrength = null)
                    {
                        notifySignalStrength(localSignalStrength);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramParcel1.readInt() != 0);
                    for (boolean bool5 = true; ; bool5 = false)
                    {
                        notifyMessageWaitingChanged(bool5);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramParcel1.readInt() != 0);
                    for (boolean bool4 = true; ; bool4 = false)
                    {
                        notifyCallForwardingChanged(bool4);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    notifyDataActivity(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    int i = paramParcel1.readInt();
                    boolean bool2;
                    label455: String str1;
                    String str2;
                    String str3;
                    LinkProperties localLinkProperties;
                    label494: LinkCapabilities localLinkCapabilities;
                    label515: int j;
                    if (paramParcel1.readInt() != 0)
                    {
                        bool2 = true;
                        str1 = paramParcel1.readString();
                        str2 = paramParcel1.readString();
                        str3 = paramParcel1.readString();
                        if (paramParcel1.readInt() == 0)
                            break label569;
                        localLinkProperties = (LinkProperties)LinkProperties.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label575;
                        localLinkCapabilities = (LinkCapabilities)LinkCapabilities.CREATOR.createFromParcel(paramParcel1);
                        j = paramParcel1.readInt();
                        if (paramParcel1.readInt() == 0)
                            break label581;
                    }
                    label569: label575: label581: for (boolean bool3 = true; ; bool3 = false)
                    {
                        notifyDataConnection(i, bool2, str1, str2, str3, localLinkProperties, localLinkCapabilities, j, bool3);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                        bool2 = false;
                        break label455;
                        localLinkProperties = null;
                        break label494;
                        localLinkCapabilities = null;
                        break label515;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    notifyDataConnectionFailed(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool1 = true;
                    continue;
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramParcel1.readInt() != 0);
                    for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                    {
                        notifyCellLocation(localBundle);
                        paramParcel2.writeNoException();
                        bool1 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
                    notifyOtaspChanged(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool1 = true;
                }
            case 12:
            }
            paramParcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            if (paramParcel1.readInt() != 0);
            for (CellInfo localCellInfo = (CellInfo)CellInfo.CREATOR.createFromParcel(paramParcel1); ; localCellInfo = null)
            {
                notifyCellInfo(localCellInfo);
                paramParcel2.writeNoException();
                bool1 = true;
                break;
            }
        }

        private static class Proxy
            implements ITelephonyRegistry
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "com.android.internal.telephony.ITelephonyRegistry";
            }

            public void listen(String paramString, IPhoneStateListener paramIPhoneStateListener, int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    localParcel1.writeString(paramString);
                    if (paramIPhoneStateListener != null);
                    for (IBinder localIBinder = paramIPhoneStateListener.asBinder(); ; localIBinder = null)
                    {
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        if (!paramBoolean)
                            break;
                        localParcel1.writeInt(i);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyCallForwardingChanged(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyCallState(int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyCellInfo(CellInfo paramCellInfo)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramCellInfo != null)
                    {
                        localParcel1.writeInt(1);
                        paramCellInfo.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(12, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyCellLocation(Bundle paramBundle)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramBundle != null)
                    {
                        localParcel1.writeInt(1);
                        paramBundle.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(10, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyDataActivity(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyDataConnection(int paramInt1, boolean paramBoolean1, String paramString1, String paramString2, String paramString3, LinkProperties paramLinkProperties, LinkCapabilities paramLinkCapabilities, int paramInt2, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                        localParcel1.writeInt(paramInt1);
                        int j;
                        if (paramBoolean1)
                        {
                            j = i;
                            localParcel1.writeInt(j);
                            localParcel1.writeString(paramString1);
                            localParcel1.writeString(paramString2);
                            localParcel1.writeString(paramString3);
                            if (paramLinkProperties != null)
                            {
                                localParcel1.writeInt(1);
                                paramLinkProperties.writeToParcel(localParcel1, 0);
                                if (paramLinkCapabilities == null)
                                    break label181;
                                localParcel1.writeInt(1);
                                paramLinkCapabilities.writeToParcel(localParcel1, 0);
                                localParcel1.writeInt(paramInt2);
                                if (!paramBoolean2)
                                    break label190;
                                localParcel1.writeInt(i);
                                this.mRemote.transact(8, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            j = 0;
                            continue;
                        }
                        localParcel1.writeInt(0);
                        continue;
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label181: localParcel1.writeInt(0);
                    continue;
                    label190: i = 0;
                }
            }

            public void notifyDataConnectionFailed(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyMessageWaitingChanged(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyOtaspChanged(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifyServiceState(ServiceState paramServiceState)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramServiceState != null)
                    {
                        localParcel1.writeInt(1);
                        paramServiceState.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void notifySignalStrength(SignalStrength paramSignalStrength)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
                    if (paramSignalStrength != null)
                    {
                        localParcel1.writeInt(1);
                        paramSignalStrength.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.ITelephonyRegistry
 * JD-Core Version:        0.6.2
 */