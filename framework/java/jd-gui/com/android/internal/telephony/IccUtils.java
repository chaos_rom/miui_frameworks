package com.android.internal.telephony;

import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.util.Log;
import java.io.UnsupportedEncodingException;

public class IccUtils
{
    static final String LOG_TAG = "IccUtils";

    public static String adnStringFieldToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        String str1;
        if (paramInt2 == 0)
            str1 = "";
        while (true)
        {
            return str1;
            if ((paramInt2 >= 1) && (paramArrayOfByte[paramInt1] == -128))
            {
                int n = (paramInt2 - 1) / 2;
                Object localObject2 = null;
                try
                {
                    String str3 = new String(paramArrayOfByte, paramInt1 + 1, n * 2, "utf-16be");
                    localObject2 = str3;
                    if (localObject2 == null)
                        break label122;
                    for (i1 = localObject2.length(); (i1 > 0) && (localObject2.charAt(i1 - 1) == 65535); i1--);
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException)
                {
                    int i1;
                    while (true)
                        Log.e("IccUtils", "implausible UnsupportedEncodingException", localUnsupportedEncodingException);
                    str1 = localObject2.substring(0, i1);
                }
                continue;
            }
            label122: int i = 0;
            int j = 0;
            int k = 0;
            StringBuilder localStringBuilder;
            if ((paramInt2 >= 3) && (paramArrayOfByte[paramInt1] == -127))
            {
                k = 0xFF & paramArrayOfByte[(paramInt1 + 1)];
                if (k > paramInt2 - 3)
                    k = paramInt2 - 3;
                j = (char)((0xFF & paramArrayOfByte[(paramInt1 + 2)]) << 7);
                paramInt1 += 3;
                i = 1;
                if (i != 0)
                    localStringBuilder = new StringBuilder();
            }
            else
            {
                while (true)
                {
                    if (k <= 0)
                        break label355;
                    if (paramArrayOfByte[paramInt1] < 0)
                    {
                        localStringBuilder.append(j + (0x7F & paramArrayOfByte[paramInt1]));
                        paramInt1++;
                        k--;
                    }
                    int m = 0;
                    while (true)
                        if ((m < k) && (paramArrayOfByte[(paramInt1 + m)] >= 0))
                        {
                            m++;
                            continue;
                            if ((paramInt2 < 4) || (paramArrayOfByte[paramInt1] != -126))
                                break;
                            k = 0xFF & paramArrayOfByte[(paramInt1 + 1)];
                            if (k > paramInt2 - 4)
                                k = paramInt2 - 4;
                            j = (char)((0xFF & paramArrayOfByte[(paramInt1 + 2)]) << 8 | 0xFF & paramArrayOfByte[(paramInt1 + 3)]);
                            paramInt1 += 4;
                            i = 1;
                            break;
                        }
                    localStringBuilder.append(GsmAlphabet.gsm8BitUnpackedToString(paramArrayOfByte, paramInt1, m));
                    paramInt1 += m;
                    k -= m;
                }
                label355: str1 = localStringBuilder.toString();
                continue;
            }
            Resources localResources = Resources.getSystem();
            Object localObject1 = "";
            try
            {
                String str2 = localResources.getString(17039392);
                localObject1 = str2;
                label387: str1 = GsmAlphabet.gsm8BitUnpackedToString(paramArrayOfByte, paramInt1, paramInt2, ((String)localObject1).trim());
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                break label387;
            }
        }
    }

    public static String bcdToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder(paramInt2 * 2);
        int i = paramInt1;
        int j;
        if (i < paramInt1 + paramInt2)
        {
            j = 0xF & paramArrayOfByte[i];
            if (j <= 9);
        }
        else
        {
            label38: return localStringBuilder.toString();
        }
        localStringBuilder.append((char)(j + 48));
        int k = 0xF & paramArrayOfByte[i] >> 4;
        if (k == 15);
        while (true)
        {
            i++;
            break;
            if (k > 9)
                break label38;
            localStringBuilder.append((char)(k + 48));
        }
    }

    private static int bitToRGB(int paramInt)
    {
        if (paramInt == 1);
        for (int i = -1; ; i = -16777216)
            return i;
    }

    public static String bytesToHexString(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte == null);
        StringBuilder localStringBuilder;
        for (String str = null; ; str = localStringBuilder.toString())
        {
            return str;
            localStringBuilder = new StringBuilder(2 * paramArrayOfByte.length);
            for (int i = 0; i < paramArrayOfByte.length; i++)
            {
                localStringBuilder.append("0123456789abcdef".charAt(0xF & paramArrayOfByte[i] >> 4));
                localStringBuilder.append("0123456789abcdef".charAt(0xF & paramArrayOfByte[i]));
            }
        }
    }

    public static int cdmaBcdByteToInt(byte paramByte)
    {
        int i = 0;
        if ((paramByte & 0xF0) <= 144)
            i = 10 * (0xF & paramByte >> 4);
        if ((paramByte & 0xF) <= 9)
            i += (paramByte & 0xF);
        return i;
    }

    public static String cdmaBcdToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder(paramInt2);
        int i = 0;
        for (int j = paramInt1; ; j++)
        {
            int m;
            if (i < paramInt2)
            {
                int k = 0xF & paramArrayOfByte[j];
                if (k > 9)
                    k = 0;
                localStringBuilder.append((char)(k + 48));
                m = i + 1;
                if (m != paramInt2);
            }
            else
            {
                return localStringBuilder.toString();
            }
            int n = 0xF & paramArrayOfByte[j] >> 4;
            if (n > 9)
                n = 0;
            localStringBuilder.append((char)(n + 48));
            i = m + 1;
        }
    }

    private static int[] getCLUT(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        if (paramArrayOfByte == null)
        {
            arrayOfInt = null;
            return arrayOfInt;
        }
        int[] arrayOfInt = new int[paramInt2];
        int i = paramInt1 + paramInt2 * 3;
        int j = paramInt1;
        int k = 0;
        while (true)
        {
            int m = k + 1;
            int n = j + 1;
            int i1 = 0xFF000000 | (0xFF & paramArrayOfByte[j]) << 16;
            int i2 = n + 1;
            int i3 = i1 | (0xFF & paramArrayOfByte[n]) << 8;
            int i4 = i2 + 1;
            arrayOfInt[k] = (i3 | 0xFF & paramArrayOfByte[i2]);
            if (i4 >= i)
                break;
            k = m;
            j = i4;
        }
    }

    public static int gsmBcdByteToInt(byte paramByte)
    {
        int i = 0;
        if ((paramByte & 0xF0) <= 144)
            i = 0xF & paramByte >> 4;
        if ((paramByte & 0xF) <= 9)
            i += 10 * (paramByte & 0xF);
        return i;
    }

    static int hexCharToInt(char paramChar)
    {
        int i;
        if ((paramChar >= '0') && (paramChar <= '9'))
            i = paramChar + '\0*0';
        while (true)
        {
            return i;
            if ((paramChar >= 'A') && (paramChar <= 'F'))
            {
                i = 10 + (paramChar + '\0'7');
            }
            else
            {
                if ((paramChar < 'a') || (paramChar > 'f'))
                    break;
                i = 10 + (paramChar + '\0#7');
            }
        }
        throw new RuntimeException("invalid hex char '" + paramChar + "'");
    }

    public static byte[] hexStringToBytes(String paramString)
    {
        byte[] arrayOfByte;
        if (paramString == null)
            arrayOfByte = null;
        while (true)
        {
            return arrayOfByte;
            int i = paramString.length();
            arrayOfByte = new byte[i / 2];
            for (int j = 0; j < i; j += 2)
                arrayOfByte[(j / 2)] = ((byte)(hexCharToInt(paramString.charAt(j)) << 4 | hexCharToInt(paramString.charAt(j + 1))));
        }
    }

    private static int[] mapTo2OrderBitColor(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        int[] arrayOfInt;
        if (8 % paramInt3 != 0)
        {
            Log.e("IccUtils", "not event number of color");
            arrayOfInt = mapToNon2OrderBitColor(paramArrayOfByte, paramInt1, paramInt2, paramArrayOfInt, paramInt3);
        }
        int m;
        label212: 
        while (true)
        {
            return arrayOfInt;
            int i = 1;
            int j;
            int k;
            switch (paramInt3)
            {
            case 3:
            case 5:
            case 6:
            case 7:
            default:
                arrayOfInt = new int[paramInt2];
                j = 0;
                k = 8 / paramInt3;
            case 1:
            case 2:
            case 4:
            case 8:
            }
            int n;
            for (m = paramInt1; ; m = n)
            {
                if (j >= paramInt2)
                    break label212;
                n = m + 1;
                int i1 = paramArrayOfByte[m];
                int i2 = 0;
                int i3 = j;
                while (true)
                    if (i2 < k)
                    {
                        int i4 = -1 + (k - i2);
                        int i5 = i3 + 1;
                        arrayOfInt[i3] = paramArrayOfInt[(i & i1 >> i4 * paramInt3)];
                        i2++;
                        i3 = i5;
                        continue;
                        i = 1;
                        break;
                        i = 3;
                        break;
                        i = 15;
                        break;
                        i = 255;
                        break;
                    }
                j = i3;
            }
        }
    }

    private static int[] mapToNon2OrderBitColor(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
    {
        if (8 % paramInt3 == 0)
            Log.e("IccUtils", "not odd number of color");
        for (int[] arrayOfInt = mapTo2OrderBitColor(paramArrayOfByte, paramInt1, paramInt2, paramArrayOfInt, paramInt3); ; arrayOfInt = new int[paramInt2])
            return arrayOfInt;
    }

    public static String networkNameToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
        String str;
        if (((0x80 & paramArrayOfByte[paramInt1]) != 128) || (paramInt2 < 1))
        {
            str = "";
            return str;
        }
        switch (0x7 & paramArrayOfByte[paramInt1] >>> 4)
        {
        default:
            str = "";
        case 0:
        case 1:
        }
        while ((0x40 & paramArrayOfByte[paramInt1]) != 0)
        {
            break;
            int i = 0x7 & paramArrayOfByte[paramInt1];
            int j = (8 * (paramInt2 - 1) - i) / 7;
            str = GsmAlphabet.gsm7BitPackedToString(paramArrayOfByte, paramInt1 + 1, j);
            continue;
            try
            {
                str = new String(paramArrayOfByte, paramInt1 + 1, paramInt2 - 1, "utf-16");
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                str = "";
                Log.e("IccUtils", "implausible UnsupportedEncodingException", localUnsupportedEncodingException);
            }
        }
    }

    public static Bitmap parseToBnW(byte[] paramArrayOfByte, int paramInt)
    {
        int i = 0 + 1;
        int j = 0xFF & paramArrayOfByte[0];
        int k = i + 1;
        int m = 0xFF & paramArrayOfByte[i];
        int n = j * m;
        int[] arrayOfInt = new int[n];
        int i1 = 7;
        int i2 = 0;
        int i3 = 0;
        int i4 = k;
        int i5;
        if (i3 < n)
        {
            if (i3 % 8 != 0)
                break label152;
            i5 = i4 + 1;
            i2 = paramArrayOfByte[i4];
            i1 = 7;
        }
        while (true)
        {
            int i6 = i3 + 1;
            int i7 = i1 - 1;
            arrayOfInt[i3] = bitToRGB(0x1 & i2 >> i1);
            i1 = i7;
            i3 = i6;
            i4 = i5;
            break;
            if (i3 != n)
                Log.e("IccUtils", "parse end and size error");
            return Bitmap.createBitmap(arrayOfInt, j, m, Bitmap.Config.ARGB_8888);
            label152: i5 = i4;
        }
    }

    public static Bitmap parseToRGB(byte[] paramArrayOfByte, int paramInt, boolean paramBoolean)
    {
        int i = 0 + 1;
        int j = 0xFF & paramArrayOfByte[0];
        int k = i + 1;
        int m = 0xFF & paramArrayOfByte[i];
        int n = k + 1;
        int i1 = 0xFF & paramArrayOfByte[k];
        int i2 = n + 1;
        int i3 = 0xFF & paramArrayOfByte[n];
        int i4 = i2 + 1;
        int i5 = (0xFF & paramArrayOfByte[i2]) << 8;
        int i6 = i4 + 1;
        int[] arrayOfInt1 = getCLUT(paramArrayOfByte, i5 | 0xFF & paramArrayOfByte[i4], i3);
        if (true == paramBoolean)
            arrayOfInt1[(i3 - 1)] = 0;
        if (8 % i1 == 0);
        for (int[] arrayOfInt2 = mapTo2OrderBitColor(paramArrayOfByte, i6, j * m, arrayOfInt1, i1); ; arrayOfInt2 = mapToNon2OrderBitColor(paramArrayOfByte, i6, j * m, arrayOfInt1, i1))
            return Bitmap.createBitmap(arrayOfInt2, j, m, Bitmap.Config.RGB_565);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccUtils
 * JD-Core Version:        0.6.2
 */