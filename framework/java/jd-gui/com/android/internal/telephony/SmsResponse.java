package com.android.internal.telephony;

public class SmsResponse
{
    String ackPdu;
    int errorCode;
    int messageRef;

    public SmsResponse(int paramInt1, String paramString, int paramInt2)
    {
        this.messageRef = paramInt1;
        this.ackPdu = paramString;
        this.errorCode = paramInt2;
    }

    public String toString()
    {
        return "{ messageRef = " + this.messageRef + ", errorCode = " + this.errorCode + ", ackPdu = " + this.ackPdu + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.SmsResponse
 * JD-Core Version:        0.6.2
 */