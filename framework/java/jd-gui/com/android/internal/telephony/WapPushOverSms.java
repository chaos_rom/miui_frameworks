package com.android.internal.telephony;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import miui.provider.ExtraTelephony;

public class WapPushOverSms
{
    private static final String LOG_TAG = "WAP PUSH";
    private final int BIND_RETRY_INTERVAL = 1000;
    private final int WAKE_LOCK_TIMEOUT = 5000;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    String mAddress;
    private final Context mContext;
    private SMSDispatcher mSmsDispatcher;
    private WapPushConnection mWapConn = null;
    private WspTypeDecoder pduDecoder;

    public WapPushOverSms(Phone paramPhone, SMSDispatcher paramSMSDispatcher)
    {
        this.mSmsDispatcher = paramSMSDispatcher;
        this.mContext = paramPhone.getContext();
        this.mWapConn = new WapPushConnection(this.mContext);
        this.mWapConn.bindWapPushManager();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public int dispatchWapPdu(byte[] paramArrayOfByte)
    {
        int i = 0 + 1;
        int j = 0xFF & paramArrayOfByte[0];
        int k = i + 1;
        int m = 0xFF & paramArrayOfByte[i];
        int i4;
        if ((m != 6) && (m != 7))
            i4 = 1;
        String str1;
        byte[] arrayOfByte1;
        byte[] arrayOfByte2;
        while (true)
        {
            return i4;
            WspTypeDecoder localWspTypeDecoder = new WspTypeDecoder(paramArrayOfByte);
            this.pduDecoder = localWspTypeDecoder;
            if (!this.pduDecoder.decodeUintvarInteger(k))
            {
                i4 = 2;
            }
            else
            {
                int n = (int)this.pduDecoder.getValue32();
                int i1 = 2 + this.pduDecoder.getDecodedDataLength();
                if (!this.pduDecoder.decodeContentType(i1))
                {
                    i4 = 2;
                }
                else
                {
                    str1 = this.pduDecoder.getValueString();
                    long l = this.pduDecoder.getValue32();
                    int i2 = i1 + this.pduDecoder.getDecodedDataLength();
                    arrayOfByte1 = new byte[n];
                    System.arraycopy(paramArrayOfByte, i1, arrayOfByte1, 0, arrayOfByte1.length);
                    label185: String str3;
                    String str4;
                    label264: int i6;
                    if ((str1 != null) && (str1.equals("application/vnd.wap.coc")))
                    {
                        arrayOfByte2 = paramArrayOfByte;
                        if (!this.pduDecoder.seekXWapApplicationId(i2, -1 + (i2 + n)))
                            break label444;
                        int i5 = (int)this.pduDecoder.getValue32();
                        this.pduDecoder.decodeXWapApplicationId(i5);
                        str3 = this.pduDecoder.getValueString();
                        if (str3 == null)
                            str3 = Integer.toString((int)this.pduDecoder.getValue32());
                        if (str1 != null)
                            break label336;
                        str4 = Long.toString(l);
                        i6 = 1;
                    }
                    try
                    {
                        IWapPushManager localIWapPushManager = this.mWapConn.getWapPushManager();
                        if (localIWapPushManager == null);
                        while (true)
                        {
                            if (i6 != 0)
                                break label444;
                            i4 = 1;
                            break;
                            int i3 = i1 + n;
                            arrayOfByte2 = new byte[paramArrayOfByte.length - i3];
                            System.arraycopy(paramArrayOfByte, i3, arrayOfByte2, 0, arrayOfByte2.length);
                            if (!Injector.checkFirewallForWapPush(this, arrayOfByte2))
                                break label185;
                            i4 = -1;
                            break;
                            label336: str4 = str1;
                            break label264;
                            Intent localIntent2 = new Intent();
                            localIntent2.putExtra("transactionId", j);
                            localIntent2.putExtra("pduType", m);
                            localIntent2.putExtra("header", arrayOfByte1);
                            localIntent2.putExtra("data", arrayOfByte2);
                            localIntent2.putExtra("contentTypeParameters", this.pduDecoder.getContentParameters());
                            int i7 = localIWapPushManager.processMessage(str3, str4, localIntent2);
                            if (((i7 & 0x1) > 0) && ((0x8000 & i7) == 0))
                                i6 = 0;
                        }
                    }
                    catch (RemoteException localRemoteException)
                    {
                    }
                    label444: if (str1 != null)
                        break;
                    i4 = 2;
                }
            }
        }
        if (str1.equals("application/vnd.wap.mms-message"));
        for (String str2 = "android.permission.RECEIVE_MMS"; ; str2 = "android.permission.RECEIVE_WAP_PUSH")
        {
            Intent localIntent1 = new Intent("android.provider.Telephony.WAP_PUSH_RECEIVED");
            localIntent1.setType(str1);
            localIntent1.putExtra("transactionId", j);
            localIntent1.putExtra("pduType", m);
            localIntent1.putExtra("header", arrayOfByte1);
            localIntent1.putExtra("data", arrayOfByte2);
            localIntent1.putExtra("contentTypeParameters", this.pduDecoder.getContentParameters());
            localIntent1.putExtra("address", this.mAddress);
            this.mSmsDispatcher.dispatch(localIntent1, str2);
            i4 = -1;
            break;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public int dispatchWapPdu(byte[] paramArrayOfByte, String paramString)
    {
        this.mAddress = paramString;
        return dispatchWapPdu(paramArrayOfByte);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Context getContext()
    {
        return this.mContext;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    SMSDispatcher getSmsDispatcher()
    {
        return this.mSmsDispatcher;
    }

    private class WapPushConnection
        implements ServiceConnection
    {
        private Context mOwner;
        private IWapPushManager mWapPushMan;

        public WapPushConnection(Context arg2)
        {
            Object localObject;
            this.mOwner = localObject;
        }

        private void rebindWapPushManager()
        {
            if (this.mWapPushMan != null);
            while (true)
            {
                return;
                new Thread()
                {
                    public void run()
                    {
                        while (WapPushOverSms.WapPushConnection.this.mWapPushMan == null)
                        {
                            WapPushOverSms.WapPushConnection.this.mOwner.bindService(new Intent(IWapPushManager.class.getName()), jdField_this, 1);
                            try
                            {
                                Thread.sleep(1000L);
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                            }
                        }
                    }
                }
                .start();
            }
        }

        public void bindWapPushManager()
        {
            if (this.mWapPushMan != null);
            while (true)
            {
                return;
                this.mOwner.bindService(new Intent(IWapPushManager.class.getName()), this, 1);
            }
        }

        public IWapPushManager getWapPushManager()
        {
            return this.mWapPushMan;
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            this.mWapPushMan = IWapPushManager.Stub.asInterface(paramIBinder);
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            this.mWapPushMan = null;
            rebindWapPushManager();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean checkFirewallForWapPush(WapPushOverSms paramWapPushOverSms, byte[] paramArrayOfByte)
        {
            boolean bool = true;
            if (ExtraTelephony.checkFirewallForWapPush(paramWapPushOverSms.getContext(), paramArrayOfByte))
                paramWapPushOverSms.getSmsDispatcher().acknowledgeLastIncomingSms(bool, -1, null);
            while (true)
            {
                return bool;
                bool = false;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.WapPushOverSms
 * JD-Core Version:        0.6.2
 */