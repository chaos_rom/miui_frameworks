package com.android.internal.telephony;

import android.os.ServiceManager;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class PhoneSubInfoProxy extends IPhoneSubInfo.Stub
{
    private PhoneSubInfo mPhoneSubInfo;

    public PhoneSubInfoProxy(PhoneSubInfo paramPhoneSubInfo)
    {
        this.mPhoneSubInfo = paramPhoneSubInfo;
        if (ServiceManager.getService("iphonesubinfo") == null)
            ServiceManager.addService("iphonesubinfo", this);
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mPhoneSubInfo.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }

    public String getCompleteVoiceMailNumber()
    {
        return this.mPhoneSubInfo.getCompleteVoiceMailNumber();
    }

    public String getDeviceId()
    {
        return this.mPhoneSubInfo.getDeviceId();
    }

    public String getDeviceSvn()
    {
        return this.mPhoneSubInfo.getDeviceSvn();
    }

    public String getIccSerialNumber()
    {
        return this.mPhoneSubInfo.getIccSerialNumber();
    }

    public String getIsimDomain()
    {
        return this.mPhoneSubInfo.getIsimDomain();
    }

    public String getIsimImpi()
    {
        return this.mPhoneSubInfo.getIsimImpi();
    }

    public String[] getIsimImpu()
    {
        return this.mPhoneSubInfo.getIsimImpu();
    }

    public String getLine1AlphaTag()
    {
        return this.mPhoneSubInfo.getLine1AlphaTag();
    }

    public String getLine1Number()
    {
        return this.mPhoneSubInfo.getLine1Number();
    }

    public String getMsisdn()
    {
        return this.mPhoneSubInfo.getMsisdn();
    }

    public String getSubscriberId()
    {
        return this.mPhoneSubInfo.getSubscriberId();
    }

    public String getVoiceMailAlphaTag()
    {
        return this.mPhoneSubInfo.getVoiceMailAlphaTag();
    }

    public String getVoiceMailNumber()
    {
        return this.mPhoneSubInfo.getVoiceMailNumber();
    }

    public void setmPhoneSubInfo(PhoneSubInfo paramPhoneSubInfo)
    {
        this.mPhoneSubInfo = paramPhoneSubInfo;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.PhoneSubInfoProxy
 * JD-Core Version:        0.6.2
 */