package com.android.internal.telephony;

import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class MiuiIccProvider extends IccProvider
{
    private static final int ADNCAPACITY = 5;
    private static final boolean DBG = false;
    private static final int FREEADN = 4;
    private static final String TAG = "IccProvider";

    static
    {
        getURL_MATCHER().addURI("icc", "freeadn", 4);
        getURL_MATCHER().addURI("icc", "adncapacity", 5);
    }

    private MatrixCursor getAdnCapacity()
    {
        int i = 0;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "count";
        MatrixCursor localMatrixCursor = new MatrixCursor(arrayOfString, 1);
        try
        {
            IIccPhoneBook localIIccPhoneBook = IIccPhoneBook.Stub.asInterface(ServiceManager.getService("simphonebook"));
            if (localIIccPhoneBook != null)
            {
                int j = localIIccPhoneBook.getAdnCapacity();
                i = j;
            }
            label49: Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Integer.valueOf(i);
            localMatrixCursor.addRow(arrayOfObject);
            return localMatrixCursor;
        }
        catch (SecurityException localSecurityException)
        {
            break label49;
        }
        catch (RemoteException localRemoteException)
        {
            break label49;
        }
    }

    private MatrixCursor getFreeAdn()
    {
        int i = 0;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "count";
        MatrixCursor localMatrixCursor = new MatrixCursor(arrayOfString, 1);
        try
        {
            IIccPhoneBook localIIccPhoneBook = IIccPhoneBook.Stub.asInterface(ServiceManager.getService("simphonebook"));
            if (localIIccPhoneBook != null)
            {
                int j = localIIccPhoneBook.getFreeAdn();
                i = j;
            }
            label49: Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Integer.valueOf(i);
            localMatrixCursor.addRow(arrayOfObject);
            return localMatrixCursor;
        }
        catch (SecurityException localSecurityException)
        {
            break label49;
        }
        catch (RemoteException localRemoteException)
        {
            break label49;
        }
    }

    private void log(String paramString)
    {
        Log.d("IccProvider", "[IccProvider] " + paramString);
    }

    public String getType(Uri paramUri)
    {
        switch (getURL_MATCHER().match(paramUri))
        {
        default:
        case 4:
        case 5:
        }
        for (String str = super.getType(paramUri); ; str = "vnd.android.cursor.dir/sim-contact")
            return str;
    }

    public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        Object localObject;
        switch (getURL_MATCHER().match(paramUri))
        {
        default:
            localObject = super.query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2);
        case 4:
        case 5:
        }
        while (true)
        {
            return localObject;
            localObject = getFreeAdn();
            continue;
            localObject = getAdnCapacity();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.MiuiIccProvider
 * JD-Core Version:        0.6.2
 */