package com.android.internal.telephony;

import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ApnContext
{
    protected static final boolean DBG = true;
    public final String LOG_TAG;
    private ApnSetting mApnSetting;
    private final String mApnType;
    DataConnection mDataConnection;
    DataConnectionAc mDataConnectionAc;
    AtomicBoolean mDataEnabled;
    AtomicBoolean mDependencyMet;
    String mReason;
    int mRetryCount;
    private DataConnectionTracker.State mState;
    private ArrayList<ApnSetting> mWaitingApns = null;
    private AtomicInteger mWaitingApnsPermanentFailureCountDown;

    public ApnContext(String paramString1, String paramString2)
    {
        this.mApnType = paramString1;
        this.mState = DataConnectionTracker.State.IDLE;
        setReason("dataEnabled");
        setRetryCount(0);
        this.mDataEnabled = new AtomicBoolean(false);
        this.mDependencyMet = new AtomicBoolean(true);
        this.mWaitingApnsPermanentFailureCountDown = new AtomicInteger(0);
        this.LOG_TAG = paramString2;
    }

    public void decWaitingApnsPermFailCount()
    {
        this.mWaitingApnsPermanentFailureCountDown.decrementAndGet();
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("ApnContext: " + toString());
    }

    /** @deprecated */
    public ApnSetting getApnSetting()
    {
        try
        {
            ApnSetting localApnSetting = this.mApnSetting;
            return localApnSetting;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getApnType()
    {
        return this.mApnType;
    }

    /** @deprecated */
    public DataConnection getDataConnection()
    {
        try
        {
            DataConnection localDataConnection = this.mDataConnection;
            return localDataConnection;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public DataConnectionAc getDataConnectionAc()
    {
        try
        {
            DataConnectionAc localDataConnectionAc = this.mDataConnectionAc;
            return localDataConnectionAc;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean getDependencyMet()
    {
        return this.mDependencyMet.get();
    }

    /** @deprecated */
    public ApnSetting getNextWaitingApn()
    {
        try
        {
            ArrayList localArrayList = this.mWaitingApns;
            ApnSetting localApnSetting = null;
            if ((localArrayList != null) && (!localArrayList.isEmpty()))
                localApnSetting = (ApnSetting)localArrayList.get(0);
            return localApnSetting;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public String getReason()
    {
        try
        {
            String str = this.mReason;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public int getRetryCount()
    {
        try
        {
            int i = this.mRetryCount;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public DataConnectionTracker.State getState()
    {
        try
        {
            DataConnectionTracker.State localState = this.mState;
            return localState;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public ArrayList<ApnSetting> getWaitingApns()
    {
        try
        {
            ArrayList localArrayList = this.mWaitingApns;
            return localArrayList;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getWaitingApnsPermFailCount()
    {
        return this.mWaitingApnsPermanentFailureCountDown.get();
    }

    public boolean isDisconnected()
    {
        DataConnectionTracker.State localState = getState();
        if ((localState == DataConnectionTracker.State.IDLE) || (localState == DataConnectionTracker.State.FAILED));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isEnabled()
    {
        return this.mDataEnabled.get();
    }

    public boolean isReady()
    {
        if ((this.mDataEnabled.get()) && (this.mDependencyMet.get()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void log(String paramString)
    {
        Log.d(this.LOG_TAG, "[ApnContext:" + this.mApnType + "] " + paramString);
    }

    /** @deprecated */
    public void removeWaitingApn(ApnSetting paramApnSetting)
    {
        try
        {
            if (this.mWaitingApns != null)
                this.mWaitingApns.remove(paramApnSetting);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setApnSetting(ApnSetting paramApnSetting)
    {
        try
        {
            this.mApnSetting = paramApnSetting;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setDataConnection(DataConnection paramDataConnection)
    {
        try
        {
            log("setDataConnection: old dc=" + this.mDataConnection + " new dc=" + paramDataConnection + " this=" + this);
            this.mDataConnection = paramDataConnection;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setDataConnectionAc(DataConnectionAc paramDataConnectionAc)
    {
        try
        {
            log("setDataConnectionAc: old dcac=" + this.mDataConnectionAc + " new dcac=" + paramDataConnectionAc);
            if (paramDataConnectionAc != null)
                paramDataConnectionAc.addApnContextSync(this);
            while (true)
            {
                this.mDataConnectionAc = paramDataConnectionAc;
                return;
                if (this.mDataConnectionAc != null)
                    this.mDataConnectionAc.removeApnContextSync(this);
            }
        }
        finally
        {
        }
    }

    public void setDependencyMet(boolean paramBoolean)
    {
        log("set mDependencyMet as " + paramBoolean + " current state is " + this.mDependencyMet.get());
        this.mDependencyMet.set(paramBoolean);
    }

    public void setEnabled(boolean paramBoolean)
    {
        log("set enabled as " + paramBoolean + ", current state is " + this.mDataEnabled.get());
        this.mDataEnabled.set(paramBoolean);
    }

    /** @deprecated */
    public void setReason(String paramString)
    {
        try
        {
            log("set reason as " + paramString + ",current state " + this.mState);
            this.mReason = paramString;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setRetryCount(int paramInt)
    {
        try
        {
            log("setRetryCount: " + paramInt);
            this.mRetryCount = paramInt;
            DataConnection localDataConnection = this.mDataConnection;
            if (localDataConnection != null)
                localDataConnection.setRetryCount(paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setState(DataConnectionTracker.State paramState)
    {
        try
        {
            log("setState: " + paramState + ", previous state:" + this.mState);
            this.mState = paramState;
            if ((this.mState == DataConnectionTracker.State.FAILED) && (this.mWaitingApns != null))
                this.mWaitingApns.clear();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void setWaitingApns(ArrayList<ApnSetting> paramArrayList)
    {
        try
        {
            this.mWaitingApns = paramArrayList;
            this.mWaitingApnsPermanentFailureCountDown.set(this.mWaitingApns.size());
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String toString()
    {
        return "{mApnType=" + this.mApnType + " mState=" + getState() + " mWaitingApns=" + this.mWaitingApns + " mWaitingApnsPermanentFailureCountDown=" + this.mWaitingApnsPermanentFailureCountDown + " mApnSetting=" + this.mApnSetting + " mDataConnectionAc=" + this.mDataConnectionAc + " mReason=" + this.mReason + " mRetryCount=" + this.mRetryCount + " mDataEnabled=" + this.mDataEnabled + " mDependencyMet=" + this.mDependencyMet + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.ApnContext
 * JD-Core Version:        0.6.2
 */