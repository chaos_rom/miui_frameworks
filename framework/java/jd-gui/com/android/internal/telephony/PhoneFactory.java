package com.android.internal.telephony;

import android.content.Context;
import android.os.Looper;
import com.android.internal.telephony.cdma.CDMALTEPhone;
import com.android.internal.telephony.cdma.CDMAPhone;
import com.android.internal.telephony.gsm.GSMPhone;
import com.android.internal.telephony.sip.SipPhone;
import com.android.internal.telephony.sip.SipPhoneFactory;

public class PhoneFactory
{
    static final String LOG_TAG = "PHONE";
    static final int SOCKET_OPEN_MAX_RETRY = 3;
    static final int SOCKET_OPEN_RETRY_MILLIS = 2000;
    static final int preferredCdmaSubscription = 1;
    private static CommandsInterface sCommandsInterface = null;
    private static Context sContext;
    private static Looper sLooper;
    private static boolean sMadeDefaults = false;
    private static PhoneNotifier sPhoneNotifier;
    private static Phone sProxyPhone = null;

    public static Phone getCdmaPhone()
    {
        synchronized (PhoneProxy.lockForRadioTechnologyChange)
        {
            switch (BaseCommands.getLteOnCdmaModeStatic())
            {
            default:
                localObject3 = new CDMAPhone(sContext, sCommandsInterface, sPhoneNotifier);
                return localObject3;
            case 1:
            }
            Object localObject3 = new CDMALTEPhone(sContext, sCommandsInterface, sPhoneNotifier);
        }
    }

    public static Phone getDefaultPhone()
    {
        if (sLooper != Looper.myLooper())
            throw new RuntimeException("PhoneFactory.getDefaultPhone must be called from Looper thread");
        if (!sMadeDefaults)
            throw new IllegalStateException("Default phones haven't been made yet!");
        return sProxyPhone;
    }

    public static Phone getGsmPhone()
    {
        synchronized (PhoneProxy.lockForRadioTechnologyChange)
        {
            GSMPhone localGSMPhone = new GSMPhone(sContext, sCommandsInterface, sPhoneNotifier);
            return localGSMPhone;
        }
    }

    public static int getPhoneType(int paramInt)
    {
        int i = 2;
        switch (paramInt)
        {
        case 9:
        default:
            i = 1;
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 10:
        case 0:
        case 1:
        case 2:
        case 3:
        case 11:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            if (BaseCommands.getLteOnCdmaModeStatic() != 1)
                i = 1;
        }
    }

    // ERROR //
    public static void makeDefaultPhone(Context paramContext)
    {
        // Byte code:
        //     0: ldc 99
        //     2: monitorenter
        //     3: getstatic 35	com/android/internal/telephony/PhoneFactory:sMadeDefaults	Z
        //     6: ifne +256 -> 262
        //     9: invokestatic 73	android/os/Looper:myLooper	()Landroid/os/Looper;
        //     12: putstatic 67	com/android/internal/telephony/PhoneFactory:sLooper	Landroid/os/Looper;
        //     15: aload_0
        //     16: putstatic 56	com/android/internal/telephony/PhoneFactory:sContext	Landroid/content/Context;
        //     19: getstatic 67	com/android/internal/telephony/PhoneFactory:sLooper	Landroid/os/Looper;
        //     22: ifnonnull +19 -> 41
        //     25: new 75	java/lang/RuntimeException
        //     28: dup
        //     29: ldc 101
        //     31: invokespecial 80	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     34: athrow
        //     35: astore_1
        //     36: ldc 99
        //     38: monitorexit
        //     39: aload_1
        //     40: athrow
        //     41: iconst_0
        //     42: istore_2
        //     43: iconst_0
        //     44: istore_3
        //     45: iinc 2 1
        //     48: new 103	android/net/LocalServerSocket
        //     51: dup
        //     52: ldc 105
        //     54: invokespecial 106	android/net/LocalServerSocket:<init>	(Ljava/lang/String;)V
        //     57: pop
        //     58: iload_3
        //     59: ifne +207 -> 266
        //     62: new 108	com/android/internal/telephony/DefaultPhoneNotifier
        //     65: dup
        //     66: invokespecial 109	com/android/internal/telephony/DefaultPhoneNotifier:<init>	()V
        //     69: putstatic 58	com/android/internal/telephony/PhoneFactory:sPhoneNotifier	Lcom/android/internal/telephony/PhoneNotifier;
        //     72: iconst_0
        //     73: istore 6
        //     75: invokestatic 52	com/android/internal/telephony/BaseCommands:getLteOnCdmaModeStatic	()I
        //     78: iconst_1
        //     79: if_icmpne +7 -> 86
        //     82: bipush 7
        //     84: istore 6
        //     86: aload_0
        //     87: invokevirtual 115	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     90: ldc 117
        //     92: iload 6
        //     94: invokestatic 123	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     97: istore 7
        //     99: ldc 8
        //     101: new 125	java/lang/StringBuilder
        //     104: dup
        //     105: invokespecial 126	java/lang/StringBuilder:<init>	()V
        //     108: ldc 128
        //     110: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     113: iload 7
        //     115: invokestatic 138	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     118: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     121: invokevirtual 141	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     124: invokestatic 147	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     127: pop
        //     128: invokestatic 52	com/android/internal/telephony/BaseCommands:getLteOnCdmaModeStatic	()I
        //     131: tableswitch	default:+21 -> 152, 0:+164->295, 1:+178->309
        //     153: invokevirtual 115	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     156: ldc 149
        //     158: iconst_1
        //     159: invokestatic 123	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     162: istore 9
        //     164: ldc 8
        //     166: ldc 151
        //     168: invokestatic 147	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     171: pop
        //     172: ldc 8
        //     174: new 125	java/lang/StringBuilder
        //     177: dup
        //     178: invokespecial 126	java/lang/StringBuilder:<init>	()V
        //     181: ldc 153
        //     183: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     186: iload 9
        //     188: invokevirtual 156	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     191: invokevirtual 141	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     194: invokestatic 147	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     197: pop
        //     198: new 158	com/android/internal/telephony/RIL
        //     201: dup
        //     202: aload_0
        //     203: iload 7
        //     205: iload 9
        //     207: invokespecial 161	com/android/internal/telephony/RIL:<init>	(Landroid/content/Context;II)V
        //     210: putstatic 33	com/android/internal/telephony/PhoneFactory:sCommandsInterface	Lcom/android/internal/telephony/CommandsInterface;
        //     213: iload 7
        //     215: invokestatic 163	com/android/internal/telephony/PhoneFactory:getPhoneType	(I)I
        //     218: istore 12
        //     220: iload 12
        //     222: iconst_1
        //     223: if_icmpne +100 -> 323
        //     226: ldc 8
        //     228: ldc 165
        //     230: invokestatic 147	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     233: pop
        //     234: new 42	com/android/internal/telephony/PhoneProxy
        //     237: dup
        //     238: new 88	com/android/internal/telephony/gsm/GSMPhone
        //     241: dup
        //     242: aload_0
        //     243: getstatic 33	com/android/internal/telephony/PhoneFactory:sCommandsInterface	Lcom/android/internal/telephony/CommandsInterface;
        //     246: getstatic 58	com/android/internal/telephony/PhoneFactory:sPhoneNotifier	Lcom/android/internal/telephony/PhoneNotifier;
        //     249: invokespecial 89	com/android/internal/telephony/gsm/GSMPhone:<init>	(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V
        //     252: invokespecial 168	com/android/internal/telephony/PhoneProxy:<init>	(Lcom/android/internal/telephony/Phone;)V
        //     255: putstatic 31	com/android/internal/telephony/PhoneFactory:sProxyPhone	Lcom/android/internal/telephony/Phone;
        //     258: iconst_1
        //     259: putstatic 35	com/android/internal/telephony/PhoneFactory:sMadeDefaults	Z
        //     262: ldc 99
        //     264: monitorexit
        //     265: return
        //     266: iload_2
        //     267: iconst_3
        //     268: if_icmple +13 -> 281
        //     271: new 75	java/lang/RuntimeException
        //     274: dup
        //     275: ldc 170
        //     277: invokespecial 80	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     280: athrow
        //     281: ldc2_w 171
        //     284: invokestatic 178	java/lang/Thread:sleep	(J)V
        //     287: goto -244 -> 43
        //     290: astore 5
        //     292: goto -249 -> 43
        //     295: iconst_1
        //     296: istore 9
        //     298: ldc 8
        //     300: ldc 180
        //     302: invokestatic 147	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     305: pop
        //     306: goto -134 -> 172
        //     309: iconst_0
        //     310: istore 9
        //     312: ldc 8
        //     314: ldc 182
        //     316: invokestatic 147	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     319: pop
        //     320: goto -148 -> 172
        //     323: iload 12
        //     325: iconst_2
        //     326: if_icmpne -68 -> 258
        //     329: invokestatic 52	com/android/internal/telephony/BaseCommands:getLteOnCdmaModeStatic	()I
        //     332: tableswitch	default:+20 -> 352, 1:+55->387
        //     353: iconst_5
        //     354: ldc 184
        //     356: invokestatic 147	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     359: pop
        //     360: new 42	com/android/internal/telephony/PhoneProxy
        //     363: dup
        //     364: new 54	com/android/internal/telephony/cdma/CDMAPhone
        //     367: dup
        //     368: aload_0
        //     369: getstatic 33	com/android/internal/telephony/PhoneFactory:sCommandsInterface	Lcom/android/internal/telephony/CommandsInterface;
        //     372: getstatic 58	com/android/internal/telephony/PhoneFactory:sPhoneNotifier	Lcom/android/internal/telephony/PhoneNotifier;
        //     375: invokespecial 61	com/android/internal/telephony/cdma/CDMAPhone:<init>	(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V
        //     378: invokespecial 168	com/android/internal/telephony/PhoneProxy:<init>	(Lcom/android/internal/telephony/Phone;)V
        //     381: putstatic 31	com/android/internal/telephony/PhoneFactory:sProxyPhone	Lcom/android/internal/telephony/Phone;
        //     384: goto -126 -> 258
        //     387: ldc 8
        //     389: ldc 186
        //     391: invokestatic 147	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     394: pop
        //     395: new 42	com/android/internal/telephony/PhoneProxy
        //     398: dup
        //     399: new 63	com/android/internal/telephony/cdma/CDMALTEPhone
        //     402: dup
        //     403: aload_0
        //     404: getstatic 33	com/android/internal/telephony/PhoneFactory:sCommandsInterface	Lcom/android/internal/telephony/CommandsInterface;
        //     407: getstatic 58	com/android/internal/telephony/PhoneFactory:sPhoneNotifier	Lcom/android/internal/telephony/PhoneNotifier;
        //     410: invokespecial 64	com/android/internal/telephony/cdma/CDMALTEPhone:<init>	(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V
        //     413: invokespecial 168	com/android/internal/telephony/PhoneProxy:<init>	(Lcom/android/internal/telephony/Phone;)V
        //     416: putstatic 31	com/android/internal/telephony/PhoneFactory:sProxyPhone	Lcom/android/internal/telephony/Phone;
        //     419: goto -161 -> 258
        //     422: astore 18
        //     424: iconst_1
        //     425: istore_3
        //     426: goto -368 -> 58
        //
        // Exception table:
        //     from	to	target	type
        //     3	39	35	finally
        //     48	58	35	finally
        //     62	281	35	finally
        //     281	287	35	finally
        //     298	419	35	finally
        //     281	287	290	java/lang/InterruptedException
        //     48	58	422	java/io/IOException
    }

    public static void makeDefaultPhones(Context paramContext)
    {
        makeDefaultPhone(paramContext);
    }

    public static SipPhone makeSipPhone(String paramString)
    {
        return SipPhoneFactory.makePhone(paramString, sContext, sPhoneNotifier);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.PhoneFactory
 * JD-Core Version:        0.6.2
 */