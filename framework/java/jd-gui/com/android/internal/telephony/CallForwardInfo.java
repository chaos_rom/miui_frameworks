package com.android.internal.telephony;

import android.telephony.PhoneNumberUtils;

public class CallForwardInfo
{
    public String number;
    public int reason;
    public int serviceClass;
    public int status;
    public int timeSeconds;
    public int toa;

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder().append(super.toString());
        if (this.status == 0);
        for (String str = " not active "; ; str = " active ")
            return str + " reason: " + this.reason + " serviceClass: " + this.serviceClass + " \"" + PhoneNumberUtils.stringFromStringAndTOA(this.number, this.toa) + "\" " + this.timeSeconds + " seconds";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.CallForwardInfo
 * JD-Core Version:        0.6.2
 */