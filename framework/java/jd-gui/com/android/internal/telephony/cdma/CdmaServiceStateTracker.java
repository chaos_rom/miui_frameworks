package com.android.internal.telephony.cdma;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.cdma.CdmaCellLocation;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Log;
import android.util.TimeUtils;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandException.Error;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.CommandsInterface.RadioState;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.ServiceStateTracker;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

public class CdmaServiceStateTracker extends ServiceStateTracker
{
    static final String LOG_TAG = "CDMA";
    private static final int NITZ_UPDATE_DIFF_DEFAULT = 2000;
    private static final int NITZ_UPDATE_SPACING_DEFAULT = 600000;
    private static final String UNACTIVATED_MIN2_VALUE = "000000";
    private static final String UNACTIVATED_MIN_VALUE = "1111110111";
    private static final String WAKELOCK_TAG = "ServiceStateTracker";
    protected RegistrantList cdmaForSubscriptionInfoReadyRegistrants = new RegistrantList();
    CdmaCellLocation cellLoc;
    private ContentResolver cr;
    private String currentCarrier = null;
    private boolean isEriTextLoaded = false;
    protected boolean isSubscriptionFromRuim = false;
    private ContentObserver mAutoTimeObserver = new ContentObserver(new Handler())
    {
        public void onChange(boolean paramAnonymousBoolean)
        {
            CdmaServiceStateTracker.this.log("Auto time state changed");
            CdmaServiceStateTracker.this.revertToNitzTime();
        }
    };
    private ContentObserver mAutoTimeZoneObserver = new ContentObserver(new Handler())
    {
        public void onChange(boolean paramAnonymousBoolean)
        {
            CdmaServiceStateTracker.this.log("Auto time zone state changed");
            CdmaServiceStateTracker.this.revertToNitzTimeZone();
        }
    };
    private boolean mCdmaRoaming = false;
    private CdmaSubscriptionSourceManager mCdmaSSM;
    protected String mCurPlmn = null;
    int mCurrentOtaspMode = 0;
    protected int mDataConnectionState = 1;
    private int mDefaultRoamingIndicator;
    protected boolean mGotCountryCode = false;
    protected int[] mHomeNetworkId = null;
    protected int[] mHomeSystemId = null;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    BroadcastReceiver mIntentReceiver = new LocaleChangedIntentReceiver();
    private boolean mIsInPrl;
    protected boolean mIsMinInfoReady = false;
    protected String mMdn;
    protected String mMin;
    protected boolean mNeedFixZone = false;
    private boolean mNeedToRegForRuimLoaded = false;
    protected int mNewDataConnectionState = 1;
    private int mNitzUpdateDiff = SystemProperties.getInt("ro.nitz_update_diff", 2000);
    private int mNitzUpdateSpacing = SystemProperties.getInt("ro.nitz_update_spacing", 600000);
    protected String mPrlVersion;
    private String mRegistrationDeniedReason;
    protected int mRegistrationState = -1;
    private int mRoamingIndicator;
    long mSavedAtTime;
    long mSavedTime;
    String mSavedTimeZone;
    private PowerManager.WakeLock mWakeLock;
    private boolean mZoneDst;
    private int mZoneOffset;
    private long mZoneTime;
    CdmaCellLocation newCellLoc;
    CDMAPhone phone;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public CdmaServiceStateTracker(CDMAPhone paramCDMAPhone)
    {
        this.phone = paramCDMAPhone;
        this.cr = paramCDMAPhone.getContext().getContentResolver();
        this.cm = paramCDMAPhone.mCM;
        this.ss = new ServiceState();
        this.newSS = new ServiceState();
        this.cellLoc = new CdmaCellLocation();
        this.newCellLoc = new CdmaCellLocation();
        this.mSignalStrength = new SignalStrength();
        this.mCdmaSSM = CdmaSubscriptionSourceManager.getInstance(paramCDMAPhone.getContext(), this.cm, this, 39, null);
        if (this.mCdmaSSM.getCdmaSubscriptionSource() == 0);
        for (boolean bool2 = true; ; bool2 = false)
        {
            this.isSubscriptionFromRuim = bool2;
            this.mWakeLock = ((PowerManager)paramCDMAPhone.getContext().getSystemService("power")).newWakeLock(1, "ServiceStateTracker");
            this.cm.registerForRadioStateChanged(this, 1, null);
            this.cm.registerForVoiceNetworkStateChanged(this, 30, null);
            this.cm.setOnNITZTime(this, 11, null);
            this.cm.setOnSignalStrengthUpdate(this, 12, null);
            this.cm.registerForCdmaPrlChanged(this, 40, null);
            paramCDMAPhone.registerForEriFileLoaded(this, 36, null);
            this.cm.registerForCdmaOtaProvision(this, 37, null);
            if (Settings.System.getInt(this.cr, "airplane_mode_on", 0) <= 0)
                bool1 = true;
            this.mDesiredPowerState = bool1;
            this.cr.registerContentObserver(Settings.System.getUriFor("auto_time"), true, this.mAutoTimeObserver);
            this.cr.registerContentObserver(Settings.System.getUriFor("auto_time_zone"), true, this.mAutoTimeZoneObserver);
            setSignalStrengthDefaultValues();
            this.mNeedToRegForRuimLoaded = true;
            monitorLocaleChange();
            return;
        }
    }

    private TimeZone findTimeZone(int paramInt, boolean paramBoolean, long paramLong)
    {
        int i = paramInt;
        if (paramBoolean)
            i += 4480;
        String[] arrayOfString = TimeZone.getAvailableIDs(i);
        Object localObject = null;
        Date localDate = new Date(paramLong);
        int j = arrayOfString.length;
        for (int k = 0; ; k++)
            if (k < j)
            {
                TimeZone localTimeZone = TimeZone.getTimeZone(arrayOfString[k]);
                if ((localTimeZone.getOffset(paramLong) == paramInt) && (localTimeZone.inDaylightTime(localDate) == paramBoolean))
                    localObject = localTimeZone;
            }
            else
            {
                return localObject;
            }
    }

    private boolean getAutoTime()
    {
        boolean bool = true;
        try
        {
            int i = Settings.System.getInt(this.cr, "auto_time");
            if (i > 0);
            while (true)
            {
                label17: return bool;
                bool = false;
            }
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label17;
        }
    }

    private boolean getAutoTimeZone()
    {
        boolean bool = true;
        try
        {
            int i = Settings.System.getInt(this.cr, "auto_time_zone");
            if (i > 0);
            while (true)
            {
                label17: return bool;
                bool = false;
            }
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label17;
        }
    }

    private TimeZone getNitzTimeZone(int paramInt, boolean paramBoolean, long paramLong)
    {
        TimeZone localTimeZone = findTimeZone(paramInt, paramBoolean, paramLong);
        boolean bool;
        StringBuilder localStringBuilder;
        if (localTimeZone == null)
        {
            if (!paramBoolean)
            {
                bool = true;
                localTimeZone = findTimeZone(paramInt, bool, paramLong);
            }
        }
        else
        {
            localStringBuilder = new StringBuilder().append("getNitzTimeZone returning ");
            if (localTimeZone != null)
                break label78;
        }
        label78: for (Object localObject = localTimeZone; ; localObject = localTimeZone.getID())
        {
            log(localObject);
            return localTimeZone;
            bool = false;
            break;
        }
    }

    private void getSubscriptionInfoAndStartPollingThreads()
    {
        this.cm.getCDMASubscription(obtainMessage(34));
        pollState();
    }

    private void handleCdmaSubscriptionSource(int paramInt)
    {
        log("Subscription Source : " + paramInt);
        boolean bool;
        if (paramInt == 0)
        {
            bool = true;
            this.isSubscriptionFromRuim = bool;
            saveCdmaSubscriptionSource(paramInt);
            if (this.isSubscriptionFromRuim)
                break label64;
            sendMessage(obtainMessage(35));
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label64: this.phone.getIccCard().registerForReady(this, 26, null);
        }
    }

    private boolean isHomeSid(int paramInt)
    {
        int i;
        if (this.mHomeSystemId != null)
        {
            i = 0;
            if (i < this.mHomeSystemId.length)
                if (paramInt != this.mHomeSystemId[i]);
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    private boolean isRoamIndForHomeSystem(String paramString)
    {
        boolean bool = false;
        String str = SystemProperties.get("ro.cdma.homesystem");
        String[] arrayOfString;
        int i;
        if (!TextUtils.isEmpty(str))
        {
            arrayOfString = str.split(",");
            i = arrayOfString.length;
        }
        for (int j = 0; ; j++)
            if (j < i)
            {
                if (arrayOfString[j].equals(paramString))
                    bool = true;
            }
            else
                return bool;
    }

    private boolean isRoamingBetweenOperators(boolean paramBoolean, ServiceState paramServiceState)
    {
        boolean bool1 = true;
        String str1 = SystemProperties.get("gsm.sim.operator.alpha", "empty");
        String str2 = paramServiceState.getOperatorAlphaLong();
        String str3 = paramServiceState.getOperatorAlphaShort();
        boolean bool2;
        boolean bool3;
        if ((str2 != null) && (str1.equals(str2)))
        {
            bool2 = bool1;
            if ((str3 == null) || (!str1.equals(str3)))
                break label83;
            bool3 = bool1;
            label61: if ((!paramBoolean) || (bool2) || (bool3))
                break label89;
        }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            label83: bool3 = false;
            break label61;
            label89: bool1 = false;
        }
    }

    private void queueNextSignalStrengthPoll()
    {
        if (this.dontPollSignalStrength);
        while (true)
        {
            return;
            Message localMessage = obtainMessage();
            localMessage.what = 10;
            sendMessageDelayed(localMessage, 20000L);
        }
    }

    private boolean regCodeIsRoaming(int paramInt)
    {
        if (5 == paramInt);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void revertToNitzTime()
    {
        if (Settings.System.getInt(this.cr, "auto_time", 0) == 0);
        while (true)
        {
            return;
            log("revertToNitzTime: mSavedTime=" + this.mSavedTime + " mSavedAtTime=" + this.mSavedAtTime);
            if ((this.mSavedTime != 0L) && (this.mSavedAtTime != 0L))
                setAndBroadcastNetworkSetTime(this.mSavedTime + (SystemClock.elapsedRealtime() - this.mSavedAtTime));
        }
    }

    private void revertToNitzTimeZone()
    {
        if (Settings.System.getInt(this.phone.getContext().getContentResolver(), "auto_time_zone", 0) == 0);
        while (true)
        {
            return;
            log("revertToNitzTimeZone: tz='" + this.mSavedTimeZone);
            if (this.mSavedTimeZone != null)
                setAndBroadcastNetworkSetTimeZone(this.mSavedTimeZone);
        }
    }

    private void saveCdmaSubscriptionSource(int paramInt)
    {
        log("Storing cdma subscription source: " + paramInt);
        Settings.Secure.putInt(this.phone.getContext().getContentResolver(), "subscription_mode", paramInt);
    }

    private void saveNitzTimeZone(String paramString)
    {
        this.mSavedTimeZone = paramString;
    }

    private void setAndBroadcastNetworkSetTime(long paramLong)
    {
        log("setAndBroadcastNetworkSetTime: time=" + paramLong + "ms");
        SystemClock.setCurrentTimeMillis(paramLong);
        Intent localIntent = new Intent("android.intent.action.NETWORK_SET_TIME");
        localIntent.addFlags(536870912);
        localIntent.putExtra("time", paramLong);
        this.phone.getContext().sendStickyBroadcast(localIntent);
    }

    private void setAndBroadcastNetworkSetTimeZone(String paramString)
    {
        log("setAndBroadcastNetworkSetTimeZone: setTimeZone=" + paramString);
        ((AlarmManager)this.phone.getContext().getSystemService("alarm")).setTimeZone(paramString);
        Intent localIntent = new Intent("android.intent.action.NETWORK_SET_TIMEZONE");
        localIntent.addFlags(536870912);
        localIntent.putExtra("time-zone", paramString);
        this.phone.getContext().sendStickyBroadcast(localIntent);
    }

    // ERROR //
    private void setTimeFromNITZString(String paramString, long paramLong)
    {
        // Byte code:
        //     0: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     3: lstore 4
        //     5: aload_0
        //     6: new 332	java/lang/StringBuilder
        //     9: dup
        //     10: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     13: ldc_w 539
        //     16: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     19: aload_1
        //     20: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     23: ldc_w 406
        //     26: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     29: lload_2
        //     30: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     33: ldc_w 541
        //     36: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: lload 4
        //     41: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     44: ldc_w 543
        //     47: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: lload 4
        //     52: lload_2
        //     53: lsub
        //     54: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     57: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     63: ldc_w 545
        //     66: invokestatic 311	java/util/TimeZone:getTimeZone	(Ljava/lang/String;)Ljava/util/TimeZone;
        //     69: invokestatic 550	java/util/Calendar:getInstance	(Ljava/util/TimeZone;)Ljava/util/Calendar;
        //     72: astore 7
        //     74: aload 7
        //     76: invokevirtual 553	java/util/Calendar:clear	()V
        //     79: aload 7
        //     81: bipush 16
        //     83: iconst_0
        //     84: invokevirtual 557	java/util/Calendar:set	(II)V
        //     87: aload_1
        //     88: ldc_w 559
        //     91: invokevirtual 412	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     94: astore 8
        //     96: aload 7
        //     98: iconst_1
        //     99: sipush 2000
        //     102: aload 8
        //     104: iconst_0
        //     105: aaload
        //     106: invokestatic 565	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     109: iadd
        //     110: invokevirtual 557	java/util/Calendar:set	(II)V
        //     113: aload 7
        //     115: iconst_2
        //     116: bipush 255
        //     118: aload 8
        //     120: iconst_1
        //     121: aaload
        //     122: invokestatic 565	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     125: iadd
        //     126: invokevirtual 557	java/util/Calendar:set	(II)V
        //     129: aload 7
        //     131: iconst_5
        //     132: aload 8
        //     134: iconst_2
        //     135: aaload
        //     136: invokestatic 565	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     139: invokevirtual 557	java/util/Calendar:set	(II)V
        //     142: aload 7
        //     144: bipush 10
        //     146: aload 8
        //     148: iconst_3
        //     149: aaload
        //     150: invokestatic 565	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     153: invokevirtual 557	java/util/Calendar:set	(II)V
        //     156: aload 7
        //     158: bipush 12
        //     160: aload 8
        //     162: iconst_4
        //     163: aaload
        //     164: invokestatic 565	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     167: invokevirtual 557	java/util/Calendar:set	(II)V
        //     170: aload 7
        //     172: bipush 13
        //     174: aload 8
        //     176: iconst_5
        //     177: aaload
        //     178: invokestatic 565	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     181: invokevirtual 557	java/util/Calendar:set	(II)V
        //     184: aload_1
        //     185: bipush 45
        //     187: invokevirtual 569	java/lang/String:indexOf	(I)I
        //     190: bipush 255
        //     192: if_icmpne +1077 -> 1269
        //     195: iconst_1
        //     196: istore 9
        //     198: aload 8
        //     200: bipush 6
        //     202: aaload
        //     203: invokestatic 565	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     206: istore 10
        //     208: aload 8
        //     210: arraylength
        //     211: bipush 8
        //     213: if_icmplt +1062 -> 1275
        //     216: aload 8
        //     218: bipush 7
        //     220: aaload
        //     221: invokestatic 565	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     224: istore 11
        //     226: goto +1021 -> 1247
        //     229: sipush 1000
        //     232: bipush 60
        //     234: bipush 15
        //     236: iload 12
        //     238: iload 10
        //     240: imul
        //     241: imul
        //     242: imul
        //     243: imul
        //     244: istore 13
        //     246: aconst_null
        //     247: astore 14
        //     249: aload 8
        //     251: arraylength
        //     252: bipush 9
        //     254: if_icmplt +20 -> 274
        //     257: aload 8
        //     259: bipush 8
        //     261: aaload
        //     262: bipush 33
        //     264: bipush 47
        //     266: invokevirtual 573	java/lang/String:replace	(CC)Ljava/lang/String;
        //     269: invokestatic 311	java/util/TimeZone:getTimeZone	(Ljava/lang/String;)Ljava/util/TimeZone;
        //     272: astore 14
        //     274: ldc_w 575
        //     277: invokestatic 398	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     280: astore 15
        //     282: aload 14
        //     284: ifnonnull +51 -> 335
        //     287: aload_0
        //     288: getfield 130	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mGotCountryCode	Z
        //     291: ifeq +44 -> 335
        //     294: aload 15
        //     296: ifnull +998 -> 1294
        //     299: aload 15
        //     301: invokevirtual 578	java/lang/String:length	()I
        //     304: ifle +990 -> 1294
        //     307: iload 11
        //     309: ifeq +979 -> 1288
        //     312: iconst_1
        //     313: istore 45
        //     315: aload 7
        //     317: invokevirtual 581	java/util/Calendar:getTimeInMillis	()J
        //     320: lstore 46
        //     322: iload 13
        //     324: iload 45
        //     326: lload 46
        //     328: aload 15
        //     330: invokestatic 586	android/util/TimeUtils:getTimeZone	(IZJLjava/lang/String;)Ljava/util/TimeZone;
        //     333: astore 14
        //     335: aload 14
        //     337: ifnull +29 -> 366
        //     340: aload_0
        //     341: getfield 588	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mZoneOffset	I
        //     344: iload 13
        //     346: if_icmpne +20 -> 366
        //     349: aload_0
        //     350: getfield 590	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mZoneDst	Z
        //     353: istore 40
        //     355: iload 11
        //     357: ifeq +954 -> 1311
        //     360: iconst_1
        //     361: istore 41
        //     363: goto +895 -> 1258
        //     366: aload_0
        //     367: iconst_1
        //     368: putfield 128	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mNeedFixZone	Z
        //     371: aload_0
        //     372: iload 13
        //     374: putfield 588	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mZoneOffset	I
        //     377: iload 11
        //     379: ifeq +938 -> 1317
        //     382: iconst_1
        //     383: istore 16
        //     385: aload_0
        //     386: iload 16
        //     388: putfield 590	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mZoneDst	Z
        //     391: aload_0
        //     392: aload 7
        //     394: invokevirtual 581	java/util/Calendar:getTimeInMillis	()J
        //     397: putfield 592	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mZoneTime	J
        //     400: new 332	java/lang/StringBuilder
        //     403: dup
        //     404: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     407: ldc_w 594
        //     410: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     413: iload 13
        //     415: invokevirtual 372	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     418: ldc_w 596
        //     421: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     424: iload 11
        //     426: invokevirtual 372	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     429: ldc_w 598
        //     432: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     435: astore 17
        //     437: aload 14
        //     439: ifnull +147 -> 586
        //     442: aload 14
        //     444: invokevirtual 353	java/util/TimeZone:getID	()Ljava/lang/String;
        //     447: astore 18
        //     449: aload_0
        //     450: aload 17
        //     452: aload 18
        //     454: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     457: ldc_w 600
        //     460: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     463: aload 15
        //     465: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     468: ldc_w 602
        //     471: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     474: aload_0
        //     475: getfield 130	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mGotCountryCode	Z
        //     478: invokevirtual 605	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     481: ldc_w 607
        //     484: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     487: aload_0
        //     488: getfield 128	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mNeedFixZone	Z
        //     491: invokevirtual 605	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     494: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     497: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     500: aload 14
        //     502: ifnull +28 -> 530
        //     505: aload_0
        //     506: invokespecial 609	com/android/internal/telephony/cdma/CdmaServiceStateTracker:getAutoTimeZone	()Z
        //     509: ifeq +12 -> 521
        //     512: aload_0
        //     513: aload 14
        //     515: invokevirtual 353	java/util/TimeZone:getID	()Ljava/lang/String;
        //     518: invokespecial 477	com/android/internal/telephony/cdma/CdmaServiceStateTracker:setAndBroadcastNetworkSetTimeZone	(Ljava/lang/String;)V
        //     521: aload_0
        //     522: aload 14
        //     524: invokevirtual 353	java/util/TimeZone:getID	()Ljava/lang/String;
        //     527: invokespecial 611	com/android/internal/telephony/cdma/CdmaServiceStateTracker:saveNitzTimeZone	(Ljava/lang/String;)V
        //     530: ldc_w 613
        //     533: invokestatic 398	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     536: astore 19
        //     538: aload 19
        //     540: ifnull +54 -> 594
        //     543: aload 19
        //     545: ldc_w 615
        //     548: invokevirtual 416	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     551: ifeq +43 -> 594
        //     554: aload_0
        //     555: ldc_w 617
        //     558: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     561: goto +707 -> 1268
        //     564: aload 7
        //     566: invokevirtual 581	java/util/Calendar:getTimeInMillis	()J
        //     569: lstore 43
        //     571: aload_0
        //     572: iload 13
        //     574: iload 42
        //     576: lload 43
        //     578: invokespecial 619	com/android/internal/telephony/cdma/CdmaServiceStateTracker:getNitzTimeZone	(IZJ)Ljava/util/TimeZone;
        //     581: astore 14
        //     583: goto -248 -> 335
        //     586: ldc_w 621
        //     589: astore 18
        //     591: goto -142 -> 449
        //     594: aload_0
        //     595: getfield 232	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     598: invokevirtual 626	android/os/PowerManager$WakeLock:acquire	()V
        //     601: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     604: lload_2
        //     605: lsub
        //     606: lstore 23
        //     608: lload 23
        //     610: lconst_0
        //     611: lcmp
        //     612: ifge +121 -> 733
        //     615: aload_0
        //     616: new 332	java/lang/StringBuilder
        //     619: dup
        //     620: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     623: ldc_w 628
        //     626: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     629: aload_1
        //     630: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     633: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     636: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     639: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     642: lstore 38
        //     644: aload_0
        //     645: new 332	java/lang/StringBuilder
        //     648: dup
        //     649: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     652: ldc_w 630
        //     655: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     658: lload 38
        //     660: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     663: ldc_w 632
        //     666: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     669: lload 38
        //     671: lload 4
        //     673: lsub
        //     674: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     677: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     680: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     683: aload_0
        //     684: getfield 232	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     687: invokevirtual 635	android/os/PowerManager$WakeLock:release	()V
        //     690: goto +578 -> 1268
        //     693: astore 6
        //     695: aload_0
        //     696: new 332	java/lang/StringBuilder
        //     699: dup
        //     700: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     703: ldc_w 637
        //     706: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     709: aload_1
        //     710: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     713: ldc_w 639
        //     716: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     719: aload 6
        //     721: invokevirtual 342	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     724: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     727: invokevirtual 642	com/android/internal/telephony/cdma/CdmaServiceStateTracker:loge	(Ljava/lang/String;)V
        //     730: goto +538 -> 1268
        //     733: lload 23
        //     735: ldc2_w 643
        //     738: lcmp
        //     739: ifle +92 -> 831
        //     742: aload_0
        //     743: new 332	java/lang/StringBuilder
        //     746: dup
        //     747: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     750: ldc_w 646
        //     753: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     756: lload 23
        //     758: ldc2_w 647
        //     761: ldiv
        //     762: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     765: ldc_w 650
        //     768: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     771: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     774: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     777: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     780: lstore 36
        //     782: aload_0
        //     783: new 332	java/lang/StringBuilder
        //     786: dup
        //     787: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     790: ldc_w 630
        //     793: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     796: lload 36
        //     798: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     801: ldc_w 632
        //     804: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     807: lload 36
        //     809: lload 4
        //     811: lsub
        //     812: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     815: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     818: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     821: aload_0
        //     822: getfield 232	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     825: invokevirtual 635	android/os/PowerManager$WakeLock:release	()V
        //     828: goto +440 -> 1268
        //     831: lload 23
        //     833: l2i
        //     834: istore 25
        //     836: aload 7
        //     838: bipush 14
        //     840: iload 25
        //     842: invokevirtual 653	java/util/Calendar:add	(II)V
        //     845: aload_0
        //     846: invokespecial 655	com/android/internal/telephony/cdma/CdmaServiceStateTracker:getAutoTime	()Z
        //     849: ifeq +155 -> 1004
        //     852: aload 7
        //     854: invokevirtual 581	java/util/Calendar:getTimeInMillis	()J
        //     857: invokestatic 660	java/lang/System:currentTimeMillis	()J
        //     860: lsub
        //     861: lstore 28
        //     863: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     866: aload_0
        //     867: getfield 461	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mSavedAtTime	J
        //     870: lsub
        //     871: lstore 30
        //     873: aload_0
        //     874: getfield 173	com/android/internal/telephony/cdma/CdmaServiceStateTracker:cr	Landroid/content/ContentResolver;
        //     877: ldc_w 662
        //     880: aload_0
        //     881: getfield 109	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mNitzUpdateSpacing	I
        //     884: invokestatic 663	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     887: istore 32
        //     889: aload_0
        //     890: getfield 173	com/android/internal/telephony/cdma/CdmaServiceStateTracker:cr	Landroid/content/ContentResolver;
        //     893: ldc_w 665
        //     896: aload_0
        //     897: getfield 113	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mNitzUpdateDiff	I
        //     900: invokestatic 663	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     903: istore 33
        //     905: aload_0
        //     906: getfield 461	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mSavedAtTime	J
        //     909: lconst_0
        //     910: lcmp
        //     911: ifeq +24 -> 935
        //     914: lload 30
        //     916: iload 32
        //     918: i2l
        //     919: lcmp
        //     920: ifgt +15 -> 935
        //     923: lload 28
        //     925: invokestatic 671	java/lang/Math:abs	(J)J
        //     928: iload 33
        //     930: i2l
        //     931: lcmp
        //     932: ifle +163 -> 1095
        //     935: aload_0
        //     936: new 332	java/lang/StringBuilder
        //     939: dup
        //     940: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     943: ldc_w 673
        //     946: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     949: aload 7
        //     951: invokevirtual 677	java/util/Calendar:getTime	()Ljava/util/Date;
        //     954: invokevirtual 342	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     957: ldc_w 679
        //     960: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     963: lload 23
        //     965: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     968: ldc_w 681
        //     971: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     974: lload 28
        //     976: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     979: ldc_w 683
        //     982: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     985: aload_1
        //     986: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     989: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     992: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     995: aload_0
        //     996: aload 7
        //     998: invokevirtual 581	java/util/Calendar:getTimeInMillis	()J
        //     1001: invokespecial 470	com/android/internal/telephony/cdma/CdmaServiceStateTracker:setAndBroadcastNetworkSetTime	(J)V
        //     1004: aload_0
        //     1005: ldc_w 685
        //     1008: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     1011: ldc_w 687
        //     1014: aload 7
        //     1016: invokevirtual 581	java/util/Calendar:getTimeInMillis	()J
        //     1019: invokestatic 691	java/lang/String:valueOf	(J)Ljava/lang/String;
        //     1022: invokestatic 694	android/os/SystemProperties:set	(Ljava/lang/String;Ljava/lang/String;)V
        //     1025: aload_0
        //     1026: aload 7
        //     1028: invokevirtual 581	java/util/Calendar:getTimeInMillis	()J
        //     1031: putfield 454	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mSavedTime	J
        //     1034: aload_0
        //     1035: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     1038: putfield 461	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mSavedAtTime	J
        //     1041: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     1044: lstore 26
        //     1046: aload_0
        //     1047: new 332	java/lang/StringBuilder
        //     1050: dup
        //     1051: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     1054: ldc_w 630
        //     1057: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1060: lload 26
        //     1062: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1065: ldc_w 632
        //     1068: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1071: lload 26
        //     1073: lload 4
        //     1075: lsub
        //     1076: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1079: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1082: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     1085: aload_0
        //     1086: getfield 232	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     1089: invokevirtual 635	android/os/PowerManager$WakeLock:release	()V
        //     1092: goto +176 -> 1268
        //     1095: aload_0
        //     1096: new 332	java/lang/StringBuilder
        //     1099: dup
        //     1100: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     1103: ldc_w 696
        //     1106: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1109: lload 30
        //     1111: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1114: ldc_w 698
        //     1117: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1120: lload 28
        //     1122: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1125: ldc_w 492
        //     1128: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1131: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1134: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     1137: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     1140: lstore 34
        //     1142: aload_0
        //     1143: new 332	java/lang/StringBuilder
        //     1146: dup
        //     1147: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     1150: ldc_w 630
        //     1153: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1156: lload 34
        //     1158: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1161: ldc_w 632
        //     1164: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1167: lload 34
        //     1169: lload 4
        //     1171: lsub
        //     1172: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1175: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1178: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     1181: aload_0
        //     1182: getfield 232	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     1185: invokevirtual 635	android/os/PowerManager$WakeLock:release	()V
        //     1188: goto +80 -> 1268
        //     1191: astore 20
        //     1193: invokestatic 467	android/os/SystemClock:elapsedRealtime	()J
        //     1196: lstore 21
        //     1198: aload_0
        //     1199: new 332	java/lang/StringBuilder
        //     1202: dup
        //     1203: invokespecial 333	java/lang/StringBuilder:<init>	()V
        //     1206: ldc_w 630
        //     1209: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1212: lload 21
        //     1214: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1217: ldc_w 632
        //     1220: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1223: lload 21
        //     1225: lload 4
        //     1227: lsub
        //     1228: invokevirtual 457	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1231: invokevirtual 346	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1234: invokevirtual 350	com/android/internal/telephony/cdma/CdmaServiceStateTracker:log	(Ljava/lang/String;)V
        //     1237: aload_0
        //     1238: getfield 232	com/android/internal/telephony/cdma/CdmaServiceStateTracker:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     1241: invokevirtual 635	android/os/PowerManager$WakeLock:release	()V
        //     1244: aload 20
        //     1246: athrow
        //     1247: iload 9
        //     1249: ifeq +32 -> 1281
        //     1252: iconst_1
        //     1253: istore 12
        //     1255: goto -1026 -> 229
        //     1258: iload 40
        //     1260: iload 41
        //     1262: if_icmpeq -862 -> 400
        //     1265: goto -899 -> 366
        //     1268: return
        //     1269: iconst_0
        //     1270: istore 9
        //     1272: goto -1074 -> 198
        //     1275: iconst_0
        //     1276: istore 11
        //     1278: goto -31 -> 1247
        //     1281: bipush 255
        //     1283: istore 12
        //     1285: goto -1056 -> 229
        //     1288: iconst_0
        //     1289: istore 45
        //     1291: goto -976 -> 315
        //     1294: iload 11
        //     1296: ifeq +9 -> 1305
        //     1299: iconst_1
        //     1300: istore 42
        //     1302: goto -738 -> 564
        //     1305: iconst_0
        //     1306: istore 42
        //     1308: goto -744 -> 564
        //     1311: iconst_0
        //     1312: istore 41
        //     1314: goto -56 -> 1258
        //     1317: iconst_0
        //     1318: istore 16
        //     1320: goto -935 -> 385
        //
        // Exception table:
        //     from	to	target	type
        //     63	591	693	java/lang/RuntimeException
        //     639	690	693	java/lang/RuntimeException
        //     777	828	693	java/lang/RuntimeException
        //     1041	1092	693	java/lang/RuntimeException
        //     1137	1247	693	java/lang/RuntimeException
        //     594	639	1191	finally
        //     742	777	1191	finally
        //     836	1041	1191	finally
        //     1095	1137	1191	finally
    }

    public void dispose()
    {
        this.cm.unregisterForRadioStateChanged(this);
        this.cm.unregisterForVoiceNetworkStateChanged(this);
        this.phone.getIccCard().unregisterForReady(this);
        this.cm.unregisterForCdmaOtaProvision(this);
        this.phone.unregisterForEriFileLoaded(this);
        this.phone.mIccRecords.unregisterForRecordsLoaded(this);
        this.cm.unSetOnSignalStrengthUpdate(this);
        this.cm.unSetOnNITZTime(this);
        this.cr.unregisterContentObserver(this.mAutoTimeObserver);
        this.cr.unregisterContentObserver(this.mAutoTimeZoneObserver);
        this.mCdmaSSM.dispose(this);
        this.cm.unregisterForCdmaPrlChanged(this);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("CdmaServiceStateTracker extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" phone=" + this.phone);
        paramPrintWriter.println(" cellLoc=" + this.cellLoc);
        paramPrintWriter.println(" newCellLoc=" + this.newCellLoc);
        paramPrintWriter.println(" mCurrentOtaspMode=" + this.mCurrentOtaspMode);
        paramPrintWriter.println(" mCdmaRoaming=" + this.mCdmaRoaming);
        paramPrintWriter.println(" mRoamingIndicator=" + this.mRoamingIndicator);
        paramPrintWriter.println(" mIsInPrl=" + this.mIsInPrl);
        paramPrintWriter.println(" mDefaultRoamingIndicator=" + this.mDefaultRoamingIndicator);
        paramPrintWriter.println(" mDataConnectionState=" + this.mDataConnectionState);
        paramPrintWriter.println(" mNewDataConnectionState=" + this.mNewDataConnectionState);
        paramPrintWriter.println(" mRegistrationState=" + this.mRegistrationState);
        paramPrintWriter.println(" mNeedFixZone=" + this.mNeedFixZone);
        paramPrintWriter.println(" mZoneOffset=" + this.mZoneOffset);
        paramPrintWriter.println(" mZoneDst=" + this.mZoneDst);
        paramPrintWriter.println(" mZoneTime=" + this.mZoneTime);
        paramPrintWriter.println(" mGotCountryCode=" + this.mGotCountryCode);
        paramPrintWriter.println(" mSavedTimeZone=" + this.mSavedTimeZone);
        paramPrintWriter.println(" mSavedTime=" + this.mSavedTime);
        paramPrintWriter.println(" mSavedAtTime=" + this.mSavedAtTime);
        paramPrintWriter.println(" mNeedToRegForRuimLoaded=" + this.mNeedToRegForRuimLoaded);
        paramPrintWriter.println(" mWakeLock=" + this.mWakeLock);
        paramPrintWriter.println(" mCurPlmn=" + this.mCurPlmn);
        paramPrintWriter.println(" mMdn=" + this.mMdn);
        paramPrintWriter.println(" mHomeSystemId=" + this.mHomeSystemId);
        paramPrintWriter.println(" mHomeNetworkId=" + this.mHomeNetworkId);
        paramPrintWriter.println(" mMin=" + this.mMin);
        paramPrintWriter.println(" mPrlVersion=" + this.mPrlVersion);
        paramPrintWriter.println(" mIsMinInfoReady=" + this.mIsMinInfoReady);
        paramPrintWriter.println(" isEriTextLoaded=" + this.isEriTextLoaded);
        paramPrintWriter.println(" isSubscriptionFromRuim=" + this.isSubscriptionFromRuim);
        paramPrintWriter.println(" mCdmaSSM=" + this.mCdmaSSM);
        paramPrintWriter.println(" mRegistrationDeniedReason=" + this.mRegistrationDeniedReason);
        paramPrintWriter.println(" currentCarrier=" + this.currentCarrier);
    }

    protected void finalize()
    {
        log("CdmaServiceStateTracker finalized");
    }

    protected void fixTimeZone(String paramString)
    {
        String str = SystemProperties.get("persist.sys.timezone");
        log("fixTimeZone zoneName='" + str + "' mZoneOffset=" + this.mZoneOffset + " mZoneDst=" + this.mZoneDst + " iso-cc='" + paramString + "' iso-cc-idx=" + Arrays.binarySearch(GMT_COUNTRY_CODES, paramString));
        TimeZone localTimeZone;
        long l2;
        if ((this.mZoneOffset == 0) && (!this.mZoneDst) && (str != null) && (str.length() > 0) && (Arrays.binarySearch(GMT_COUNTRY_CODES, paramString) < 0))
        {
            localTimeZone = TimeZone.getDefault();
            if (this.mNeedFixZone)
            {
                long l1 = System.currentTimeMillis();
                l2 = localTimeZone.getOffset(l1);
                log("fixTimeZone: tzOffset=" + l2 + " ltod=" + TimeUtils.logTimeOfDay(l1));
                if (getAutoTime())
                {
                    long l3 = l1 - l2;
                    log("fixTimeZone: adj ltod=" + TimeUtils.logTimeOfDay(l3));
                    setAndBroadcastNetworkSetTime(l3);
                }
            }
            else
            {
                log("fixTimeZone: using default TimeZone");
                label237: this.mNeedFixZone = false;
                if (localTimeZone == null)
                    break label412;
                log("fixTimeZone: zone != null zone.getID=" + localTimeZone.getID());
                if (!getAutoTimeZone())
                    break label402;
                setAndBroadcastNetworkSetTimeZone(localTimeZone.getID());
                label288: saveNitzTimeZone(localTimeZone.getID());
            }
        }
        while (true)
        {
            return;
            this.mSavedTime -= l2;
            log("fixTimeZone: adj mSavedTime=" + this.mSavedTime);
            break;
            if (paramString.equals(""))
            {
                localTimeZone = getNitzTimeZone(this.mZoneOffset, this.mZoneDst, this.mZoneTime);
                log("fixTimeZone: using NITZ TimeZone");
                break label237;
            }
            localTimeZone = TimeUtils.getTimeZone(this.mZoneOffset, this.mZoneDst, this.mZoneTime, paramString);
            log("fixTimeZone: using getTimeZone(off, dst, time, iso)");
            break label237;
            label402: log("fixTimeZone: skip changing zone as getAutoTimeZone was false");
            break label288;
            label412: log("fixTimeZone: zone == null, do nothing for zone");
        }
    }

    public String getCdmaMin()
    {
        return this.mMin;
    }

    public int getCurrentDataConnectionState()
    {
        return this.mDataConnectionState;
    }

    String getImsi()
    {
        String str1 = SystemProperties.get("gsm.sim.operator.numeric", "");
        if ((!TextUtils.isEmpty(str1)) && (getCdmaMin() != null));
        for (String str2 = str1 + getCdmaMin(); ; str2 = null)
            return str2;
    }

    public String getMdnNumber()
    {
        return this.mMdn;
    }

    int getOtasp()
    {
        int i;
        if ((this.mMin == null) || (this.mMin.length() < 6))
        {
            log("getOtasp: bad mMin='" + this.mMin + "'");
            i = 1;
        }
        while (true)
        {
            log("getOtasp: state=" + i);
            return i;
            if ((this.mMin.equals("1111110111")) || (this.mMin.substring(0, 6).equals("000000")) || (SystemProperties.getBoolean("test_cdma_setup", false)))
                i = 2;
            else
                i = 3;
        }
    }

    protected Phone getPhone()
    {
        return this.phone;
    }

    public String getPrlVersion()
    {
        return this.mPrlVersion;
    }

    public void handleMessage(Message paramMessage)
    {
        if (!this.phone.mIsTheCurrentActivePhone)
            loge("Received message " + paramMessage + "[" + paramMessage.what + "]" + " while being destroyed. Ignoring.");
        while (true)
        {
            return;
            switch (paramMessage.what)
            {
            case 2:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 28:
            case 29:
            case 32:
            case 33:
            case 38:
            default:
                super.handleMessage(paramMessage);
                break;
            case 39:
                handleCdmaSubscriptionSource(this.mCdmaSSM.getCdmaSubscriptionSource());
                break;
            case 26:
                if (this.mNeedToRegForRuimLoaded)
                {
                    this.phone.mIccRecords.registerForRecordsLoaded(this, 27, null);
                    this.mNeedToRegForRuimLoaded = false;
                }
                if (this.phone.getLteOnCdmaMode() == 1)
                {
                    log("Receive EVENT_RUIM_READY");
                    pollState();
                }
                while (true)
                {
                    this.phone.prepareEri();
                    break;
                    log("Receive EVENT_RUIM_READY and Send Request getCDMASubscription.");
                    getSubscriptionInfoAndStartPollingThreads();
                }
            case 35:
                getSubscriptionInfoAndStartPollingThreads();
                break;
            case 1:
                if (this.cm.getRadioState() == CommandsInterface.RadioState.RADIO_ON)
                {
                    handleCdmaSubscriptionSource(this.mCdmaSSM.getCdmaSubscriptionSource());
                    queueNextSignalStrengthPoll();
                }
                setPowerStateToDesired();
                pollState();
                break;
            case 30:
                pollState();
                break;
            case 3:
                if (this.cm.getRadioState().isOn())
                {
                    onSignalStrengthResult((AsyncResult)paramMessage.obj);
                    queueNextSignalStrengthPoll();
                }
                break;
            case 31:
            case 24:
            case 25:
            case 34:
            case 10:
            case 11:
            case 12:
            case 27:
            case 18:
            case 36:
            case 37:
            case 40:
            }
        }
        AsyncResult localAsyncResult7 = (AsyncResult)paramMessage.obj;
        String[] arrayOfString2;
        int j;
        int k;
        int m;
        int n;
        int i1;
        if (localAsyncResult7.exception == null)
        {
            arrayOfString2 = (String[])localAsyncResult7.result;
            j = -1;
            k = 2147483647;
            m = 2147483647;
            n = -1;
            i1 = -1;
            if (arrayOfString2.length <= 9);
        }
        while (true)
        {
            try
            {
                if (arrayOfString2[4] != null)
                    j = Integer.parseInt(arrayOfString2[4]);
                if (arrayOfString2[5] != null)
                    k = Integer.parseInt(arrayOfString2[5]);
                if (arrayOfString2[6] == null)
                    break label1067;
                m = Integer.parseInt(arrayOfString2[6]);
                break label1067;
                if (arrayOfString2[8] != null)
                    n = Integer.parseInt(arrayOfString2[8]);
                if (arrayOfString2[9] != null)
                {
                    int i2 = Integer.parseInt(arrayOfString2[9]);
                    i1 = i2;
                }
                this.cellLoc.setCellLocationData(j, k, m, n, i1);
                this.phone.notifyLocationChanged();
                disableSingleLocationUpdate();
            }
            catch (NumberFormatException localNumberFormatException)
            {
                loge("error parsing cell location data: " + localNumberFormatException);
                continue;
            }
            AsyncResult localAsyncResult6 = (AsyncResult)paramMessage.obj;
            handlePollStateResult(paramMessage.what, localAsyncResult6);
            break;
            AsyncResult localAsyncResult5 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult5.exception != null)
                break;
            String[] arrayOfString1 = (String[])localAsyncResult5.result;
            if ((arrayOfString1 != null) && (arrayOfString1.length >= 5))
            {
                this.mMdn = arrayOfString1[0];
                parseSidNid(arrayOfString1[1], arrayOfString1[2]);
                this.mMin = arrayOfString1[3];
                this.mPrlVersion = arrayOfString1[4];
                log("GET_CDMA_SUBSCRIPTION: MDN=" + this.mMdn);
                this.mIsMinInfoReady = true;
                updateOtaspState();
                this.phone.getIccCard().broadcastIccStateChangedIntent("IMSI", null);
                break;
            }
            log("GET_CDMA_SUBSCRIPTION: error parsing cdmaSubscription params num=" + arrayOfString1.length);
            break;
            this.cm.getSignalStrength(obtainMessage(3));
            break;
            AsyncResult localAsyncResult4 = (AsyncResult)paramMessage.obj;
            setTimeFromNITZString((String)((Object[])(Object[])localAsyncResult4.result)[0], ((Long)((Object[])(Object[])localAsyncResult4.result)[1]).longValue());
            break;
            AsyncResult localAsyncResult3 = (AsyncResult)paramMessage.obj;
            this.dontPollSignalStrength = true;
            onSignalStrengthResult(localAsyncResult3);
            break;
            updateSpnDisplay();
            break;
            if (((AsyncResult)paramMessage.obj).exception != null)
                break;
            this.cm.getVoiceRegistrationState(obtainMessage(31, null));
            break;
            log("[CdmaServiceStateTracker] ERI file has been loaded, repolling.");
            pollState();
            break;
            AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult2.exception != null)
                break;
            int i = ((int[])(int[])localAsyncResult2.result)[0];
            if ((i != 8) && (i != 10))
                break;
            log("EVENT_OTA_PROVISION_STATUS_CHANGE: Complete, Reload MDN");
            this.cm.getCDMASubscription(obtainMessage(34));
            break;
            AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult1.exception != null)
                break;
            this.mPrlVersion = Integer.toString(((int[])(int[])localAsyncResult1.result)[0]);
            break;
            label1067: if ((k == 0) && (m == 0))
            {
                k = 2147483647;
                m = 2147483647;
            }
        }
    }

    protected void handlePollStateResult(int paramInt, AsyncResult paramAsyncResult)
    {
        if (paramAsyncResult.userObj != this.pollingContext);
        label119: 
        do
        {
            CommandException.Error localError;
            while (true)
            {
                return;
                if (paramAsyncResult.exception == null)
                    break label415;
                localError = null;
                if ((paramAsyncResult.exception instanceof CommandException))
                    localError = ((CommandException)paramAsyncResult.exception).getCommandError();
                if (localError == CommandException.Error.RADIO_NOT_AVAILABLE)
                {
                    cancelPollState();
                }
                else
                {
                    if (this.cm.getRadioState().isOn())
                        break;
                    cancelPollState();
                }
            }
            if (localError != CommandException.Error.OP_NOT_ALLOWED_BEFORE_REG_NW)
                loge("handlePollStateResult: RIL returned an error where it must succeed" + paramAsyncResult.exception);
            int[] arrayOfInt = this.pollingContext;
            arrayOfInt[0] = (-1 + arrayOfInt[0]);
        }
        while (this.pollingContext[0] != 0);
        boolean bool1 = false;
        if ((!isSidsAllZeros()) && (isHomeSid(this.newSS.getSystemId())))
            bool1 = true;
        label198: boolean bool2;
        if (this.isSubscriptionFromRuim)
        {
            this.newSS.setRoaming(isRoamingBetweenOperators(this.mCdmaRoaming, this.newSS));
            this.newSS.setCdmaDefaultRoamingIndicator(this.mDefaultRoamingIndicator);
            this.newSS.setCdmaRoamingIndicator(this.mRoamingIndicator);
            bool2 = true;
            if (TextUtils.isEmpty(this.mPrlVersion))
                bool2 = false;
            if (bool2)
                break label466;
            this.newSS.setCdmaRoamingIndicator(1);
        }
        while (true)
        {
            while (true)
            {
                int i = this.newSS.getCdmaRoamingIndicator();
                this.newSS.setCdmaEriIconIndex(this.phone.mEriManager.getCdmaEriIconIndex(i, this.mDefaultRoamingIndicator));
                this.newSS.setCdmaEriIconMode(this.phone.mEriManager.getCdmaEriIconMode(i, this.mDefaultRoamingIndicator));
                log("Set CDMA Roaming Indicator to: " + this.newSS.getCdmaRoamingIndicator() + ". mCdmaRoaming = " + this.mCdmaRoaming + ", isPrlLoaded = " + bool2 + ". namMatch = " + bool1 + " , mIsInPrl = " + this.mIsInPrl + ", mRoamingIndicator = " + this.mRoamingIndicator + ", mDefaultRoamingIndicator= " + this.mDefaultRoamingIndicator);
                pollStateDone();
                break;
                try
                {
                    label415: handlePollStateResultMessage(paramInt, paramAsyncResult);
                }
                catch (RuntimeException localRuntimeException)
                {
                    loge("handlePollStateResult: Exception while polling service state. Probably malformed RIL response." + localRuntimeException);
                }
            }
            break label119;
            this.newSS.setRoaming(this.mCdmaRoaming);
            break label198;
            label466: if (!isSidsAllZeros())
                if ((!bool1) && (!this.mIsInPrl))
                    this.newSS.setCdmaRoamingIndicator(this.mDefaultRoamingIndicator);
                else if ((bool1) && (!this.mIsInPrl))
                    this.newSS.setCdmaRoamingIndicator(2);
                else if ((!bool1) && (this.mIsInPrl))
                    this.newSS.setCdmaRoamingIndicator(this.mRoamingIndicator);
                else if (this.mRoamingIndicator <= 2)
                    this.newSS.setCdmaRoamingIndicator(1);
                else
                    this.newSS.setCdmaRoamingIndicator(this.mRoamingIndicator);
        }
    }

    protected void handlePollStateResultMessage(int paramInt, AsyncResult paramAsyncResult)
    {
        String[] arrayOfString2;
        int i;
        int j;
        int k;
        int m;
        int n;
        int i1;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        switch (paramInt)
        {
        default:
            loge("handlePollStateResultMessage: RIL response handle in wrong phone! Expected CDMA RIL request and get GSM RIL request.");
            return;
        case 24:
            arrayOfString2 = (String[])paramAsyncResult.result;
            i = 4;
            j = -1;
            k = -1;
            m = 2147483647;
            n = 2147483647;
            i1 = 0;
            i2 = 0;
            i3 = 0;
            i4 = -1;
            i5 = 0;
            i6 = 0;
            i7 = 0;
            if (arrayOfString2.length < 14)
                break;
        case 25:
        }
        while (true)
        {
            try
            {
                if (arrayOfString2[0] != null)
                    i = Integer.parseInt(arrayOfString2[0]);
                if (arrayOfString2[3] != null)
                    j = Integer.parseInt(arrayOfString2[3]);
                if (arrayOfString2[4] != null)
                    k = Integer.parseInt(arrayOfString2[4]);
                if (arrayOfString2[5] != null)
                    m = Integer.parseInt(arrayOfString2[5]);
                if (arrayOfString2[6] == null)
                    break label734;
                n = Integer.parseInt(arrayOfString2[6]);
                break label734;
                if (arrayOfString2[7] != null)
                    i1 = Integer.parseInt(arrayOfString2[7]);
                if (arrayOfString2[8] != null)
                    i2 = Integer.parseInt(arrayOfString2[8]);
                if (arrayOfString2[9] != null)
                    i3 = Integer.parseInt(arrayOfString2[9]);
                if (arrayOfString2[10] != null)
                    i4 = Integer.parseInt(arrayOfString2[10]);
                if (arrayOfString2[11] != null)
                    i5 = Integer.parseInt(arrayOfString2[11]);
                if (arrayOfString2[12] != null)
                    i6 = Integer.parseInt(arrayOfString2[12]);
                if (arrayOfString2[13] != null)
                {
                    int i8 = Integer.parseInt(arrayOfString2[13]);
                    i7 = i8;
                }
                this.mRegistrationState = i;
                if ((!regCodeIsRoaming(i)) || (isRoamIndForHomeSystem(arrayOfString2[10])))
                    break label544;
                bool1 = true;
                this.mCdmaRoaming = bool1;
                this.newSS.setState(regCodeToServiceState(i));
                setCdmaTechnology(j);
                this.newSS.setCssIndicator(i1);
                this.newSS.setSystemAndNetworkId(i2, i3);
                this.mRoamingIndicator = i4;
                if (i5 != 0)
                    break label550;
                bool2 = false;
                this.mIsInPrl = bool2;
                this.mDefaultRoamingIndicator = i6;
                this.newCellLoc.setCellLocationData(k, m, n, i2, i3);
                if (i7 != 0)
                    break label556;
                this.mRegistrationDeniedReason = "General";
                if (this.mRegistrationState != 3)
                    break;
                log("Registration denied, " + this.mRegistrationDeniedReason);
            }
            catch (NumberFormatException localNumberFormatException)
            {
                loge("EVENT_POLL_STATE_REGISTRATION_CDMA: error parsing: " + localNumberFormatException);
                continue;
            }
            throw new RuntimeException("Warning! Wrong number of parameters returned from RIL_REQUEST_REGISTRATION_STATE: expected 14 or more strings and got " + arrayOfString2.length + " strings");
            label544: boolean bool1 = false;
            continue;
            label550: boolean bool2 = true;
            continue;
            label556: if (i7 == 1)
            {
                this.mRegistrationDeniedReason = "Authentication Failure";
            }
            else
            {
                this.mRegistrationDeniedReason = "";
                continue;
                String[] arrayOfString1 = (String[])paramAsyncResult.result;
                if ((arrayOfString1 != null) && (arrayOfString1.length >= 3))
                {
                    if ((arrayOfString1[2] == null) || (arrayOfString1[2].length() < 5) || ("00000".equals(arrayOfString1[2])))
                    {
                        arrayOfString1[2] = SystemProperties.get(CDMAPhone.PROPERTY_CDMA_HOME_OPERATOR_NUMERIC, "00000");
                        log("RIL_REQUEST_OPERATOR.response[2], the numeric,    is bad. Using SystemProperties '" + CDMAPhone.PROPERTY_CDMA_HOME_OPERATOR_NUMERIC + "'= " + arrayOfString1[2]);
                    }
                    if (!this.isSubscriptionFromRuim)
                    {
                        this.newSS.setOperatorName(null, arrayOfString1[1], arrayOfString1[2]);
                        break;
                    }
                    this.newSS.setOperatorName(arrayOfString1[0], arrayOfString1[1], arrayOfString1[2]);
                    break;
                }
                log("EVENT_POLL_STATE_OPERATOR_CDMA: error parsing opNames");
                break;
                label734: if ((m == 0) && (n == 0))
                {
                    m = 2147483647;
                    n = 2147483647;
                }
            }
        }
    }

    protected void hangupAndPowerOff()
    {
        this.phone.mCT.ringingCall.hangupIfAlive();
        this.phone.mCT.backgroundCall.hangupIfAlive();
        this.phone.mCT.foregroundCall.hangupIfAlive();
        this.cm.setRadioPower(false, null);
    }

    public boolean isConcurrentVoiceAndDataAllowed()
    {
        return false;
    }

    public boolean isMinInfoReady()
    {
        return this.mIsMinInfoReady;
    }

    protected boolean isSidsAllZeros()
    {
        int i;
        if (this.mHomeSystemId != null)
        {
            i = 0;
            if (i < this.mHomeSystemId.length)
                if (this.mHomeSystemId[i] == 0);
        }
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            i++;
            break;
        }
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[CdmaSST] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("CDMA", "[CdmaSST] " + paramString);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void monitorLocaleChange()
    {
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.LOCALE_CHANGED");
        this.phone.getContext().registerReceiver(this.mIntentReceiver, localIntentFilter);
    }

    protected void onSignalStrengthResult(AsyncResult paramAsyncResult)
    {
        if (paramAsyncResult.exception != null)
            setSignalStrengthDefaultValues();
        try
        {
            this.phone.notifySignalStrength();
            return;
            int[] arrayOfInt = (int[])paramAsyncResult.result;
            int i;
            label47: int j;
            label59: int k;
            label71: int m;
            if (arrayOfInt[2] > 0)
            {
                i = -arrayOfInt[2];
                if (arrayOfInt[3] <= 0)
                    break label141;
                j = -arrayOfInt[3];
                if (arrayOfInt[4] <= 0)
                    break label149;
                k = -arrayOfInt[4];
                if (arrayOfInt[5] <= 0)
                    break label156;
                m = -arrayOfInt[5];
                label83: if ((arrayOfInt[6] <= 0) || (arrayOfInt[6] > 8))
                    break label163;
            }
            label141: label149: label156: label163: for (int n = arrayOfInt[6]; ; n = -1)
            {
                this.mSignalStrength = new SignalStrength(99, -1, i, j, k, m, n, false);
                break;
                i = -120;
                break label47;
                j = -160;
                break label59;
                k = -120;
                break label71;
                m = -1;
                break label83;
            }
        }
        catch (NullPointerException localNullPointerException)
        {
            while (true)
                loge("onSignalStrengthResult() Phone already destroyed: " + localNullPointerException + "SignalStrength not notified");
        }
    }

    protected void parseSidNid(String paramString1, String paramString2)
    {
        if (paramString1 != null)
        {
            String[] arrayOfString2 = paramString1.split(",");
            this.mHomeSystemId = new int[arrayOfString2.length];
            int j = 0;
            while (true)
                if (j < arrayOfString2.length)
                    try
                    {
                        this.mHomeSystemId[j] = Integer.parseInt(arrayOfString2[j]);
                        j++;
                    }
                    catch (NumberFormatException localNumberFormatException2)
                    {
                        while (true)
                            loge("error parsing system id: " + localNumberFormatException2);
                    }
        }
        log("CDMA_SUBSCRIPTION: SID=" + paramString1);
        if (paramString2 != null)
        {
            String[] arrayOfString1 = paramString2.split(",");
            this.mHomeNetworkId = new int[arrayOfString1.length];
            int i = 0;
            while (true)
                if (i < arrayOfString1.length)
                    try
                    {
                        this.mHomeNetworkId[i] = Integer.parseInt(arrayOfString1[i]);
                        i++;
                    }
                    catch (NumberFormatException localNumberFormatException1)
                    {
                        while (true)
                            loge("CDMA_SUBSCRIPTION: error parsing network id: " + localNumberFormatException1);
                    }
        }
        log("CDMA_SUBSCRIPTION: NID=" + paramString2);
    }

    protected void pollState()
    {
        this.pollingContext = new int[1];
        this.pollingContext[0] = 0;
        switch (3.$SwitchMap$com$android$internal$telephony$CommandsInterface$RadioState[this.cm.getRadioState().ordinal()])
        {
        default:
            int[] arrayOfInt1 = this.pollingContext;
            arrayOfInt1[0] = (1 + arrayOfInt1[0]);
            this.cm.getOperator(obtainMessage(25, this.pollingContext));
            int[] arrayOfInt2 = this.pollingContext;
            arrayOfInt2[0] = (1 + arrayOfInt2[0]);
            this.cm.getVoiceRegistrationState(obtainMessage(24, this.pollingContext));
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            this.newSS.setStateOutOfService();
            this.newCellLoc.setStateInvalid();
            setSignalStrengthDefaultValues();
            this.mGotCountryCode = false;
            pollStateDone();
            continue;
            this.newSS.setStateOff();
            this.newCellLoc.setStateInvalid();
            setSignalStrengthDefaultValues();
            this.mGotCountryCode = false;
            pollStateDone();
        }
    }

    protected void pollStateDone()
    {
        log("pollStateDone: oldSS=[" + this.ss + "] newSS=[" + this.newSS + "]");
        int i;
        label88: int j;
        label104: int k;
        label120: int m;
        label134: int n;
        label148: int i1;
        label165: int i2;
        label188: int i3;
        label211: int i4;
        label228: String str5;
        label475: String str1;
        String str2;
        label563: CDMAPhone localCDMAPhone;
        if ((this.ss.getState() != 0) && (this.newSS.getState() == 0))
        {
            i = 1;
            if ((this.ss.getState() != 0) || (this.newSS.getState() == 0))
                break label691;
            if ((this.mDataConnectionState == 0) || (this.mNewDataConnectionState != 0))
                break label694;
            j = 1;
            if ((this.mDataConnectionState != 0) || (this.mNewDataConnectionState == 0))
                break label699;
            k = 1;
            if (this.mDataConnectionState == this.mNewDataConnectionState)
                break label704;
            m = 1;
            if (this.mRilRadioTechnology == this.mNewRilRadioTechnology)
                break label710;
            n = 1;
            if (this.newSS.equals(this.ss))
                break label716;
            i1 = 1;
            if ((this.ss.getRoaming()) || (!this.newSS.getRoaming()))
                break label722;
            i2 = 1;
            if ((!this.ss.getRoaming()) || (this.newSS.getRoaming()))
                break label728;
            i3 = 1;
            if (this.newCellLoc.equals(this.cellLoc))
                break label734;
            i4 = 1;
            if ((this.ss.getState() != this.newSS.getState()) || (this.mDataConnectionState != this.mNewDataConnectionState))
            {
                Object[] arrayOfObject = new Object[4];
                arrayOfObject[0] = Integer.valueOf(this.ss.getState());
                arrayOfObject[1] = Integer.valueOf(this.mDataConnectionState);
                arrayOfObject[2] = Integer.valueOf(this.newSS.getState());
                arrayOfObject[3] = Integer.valueOf(this.mNewDataConnectionState);
                EventLog.writeEvent(50116, arrayOfObject);
            }
            ServiceState localServiceState = this.ss;
            this.ss = this.newSS;
            this.newSS = localServiceState;
            this.newSS.setStateOutOfService();
            CdmaCellLocation localCdmaCellLocation = this.cellLoc;
            this.cellLoc = this.newCellLoc;
            this.newCellLoc = localCdmaCellLocation;
            this.mDataConnectionState = this.mNewDataConnectionState;
            this.mRilRadioTechnology = this.mNewRilRadioTechnology;
            this.mNewRilRadioTechnology = 0;
            this.newSS.setStateOutOfService();
            if (n != 0)
                this.phone.setSystemProperty("gsm.network.type", ServiceState.rilRadioTechnologyToString(this.mRilRadioTechnology));
            if (i != 0)
                this.mNetworkAttachedRegistrants.notifyRegistrants();
            if (i1 != 0)
            {
                if ((this.cm.getRadioState().isOn()) && (!this.isSubscriptionFromRuim))
                {
                    if (this.ss.getState() != 0)
                        break label740;
                    str5 = this.phone.getCdmaEriText();
                    this.ss.setOperatorAlphaLong(str5);
                }
                this.phone.setSystemProperty("gsm.operator.alpha", this.ss.getOperatorAlphaLong());
                str1 = SystemProperties.get("gsm.operator.numeric", "");
                str2 = this.ss.getOperatorNumeric();
                this.phone.setSystemProperty("gsm.operator.numeric", str2);
                if (str2 != null)
                    break label761;
                log("operatorNumeric is null");
                this.phone.setSystemProperty("gsm.operator.iso-country", "");
                this.mGotCountryCode = false;
                localCDMAPhone = this.phone;
                if (!this.ss.getRoaming())
                    break label898;
            }
        }
        label898: for (String str3 = "true"; ; str3 = "false")
            while (true)
            {
                localCDMAPhone.setSystemProperty("gsm.operator.isroaming", str3);
                updateSpnDisplay();
                this.phone.notifyServiceStateChanged(this.ss);
                if (j != 0)
                    this.mAttachedRegistrants.notifyRegistrants();
                if (k != 0)
                    this.mDetachedRegistrants.notifyRegistrants();
                if ((m != 0) || (n != 0))
                    this.phone.notifyDataConnection(null);
                if (i2 != 0)
                    this.mRoamingOnRegistrants.notifyRegistrants();
                if (i3 != 0)
                    this.mRoamingOffRegistrants.notifyRegistrants();
                if (i4 != 0)
                    this.phone.notifyLocationChanged();
                return;
                i = 0;
                break;
                label691: break label88;
                label694: j = 0;
                break label104;
                label699: k = 0;
                break label120;
                label704: m = 0;
                break label134;
                label710: n = 0;
                break label148;
                label716: i1 = 0;
                break label165;
                label722: i2 = 0;
                break label188;
                label728: i3 = 0;
                break label211;
                label734: i4 = 0;
                break label228;
                label740: str5 = this.phone.getContext().getText(17039625).toString();
                break label475;
                label761: Object localObject = "";
                str2.substring(0, 3);
                try
                {
                    String str4 = MccTable.countryCodeForMcc(Integer.parseInt(str2.substring(0, 3)));
                    localObject = str4;
                    this.phone.setSystemProperty("gsm.operator.iso-country", (String)localObject);
                    this.mGotCountryCode = true;
                    if (!shouldFixTimeZoneNow(this.phone, str2, str1, this.mNeedFixZone))
                        break label563;
                    fixTimeZone((String)localObject);
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    while (true)
                        loge("pollStateDone: countryCodeForMcc error" + localNumberFormatException);
                }
                catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException)
                {
                    while (true)
                        loge("pollStateDone: countryCodeForMcc error" + localStringIndexOutOfBoundsException);
                }
            }
    }

    protected int radioTechnologyToDataServiceState(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        case 9:
        case 10:
        case 11:
        default:
            loge("radioTechnologyToDataServiceState: Wrong radioTechnology code.");
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 12:
        case 13:
        }
        while (true)
        {
            return i;
            i = 0;
        }
    }

    protected int regCodeToServiceState(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        default:
            loge("regCodeToServiceState: unexpected service state " + paramInt);
        case 0:
        case 2:
        case 3:
        case 4:
        case 1:
        case 5:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 0;
        }
    }

    public void registerForSubscriptionInfoReady(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.cdmaForSubscriptionInfoReadyRegistrants.add(localRegistrant);
        if (isMinInfoReady())
            localRegistrant.notifyRegistrant();
    }

    protected void setCdmaTechnology(int paramInt)
    {
        this.mNewDataConnectionState = radioTechnologyToDataServiceState(paramInt);
        this.newSS.setRadioTechnology(paramInt);
        this.mNewRilRadioTechnology = paramInt;
    }

    protected void setPowerStateToDesired()
    {
        if ((this.mDesiredPowerState) && (this.cm.getRadioState() == CommandsInterface.RadioState.RADIO_OFF))
            this.cm.setRadioPower(true, null);
        while (true)
        {
            return;
            if ((!this.mDesiredPowerState) && (this.cm.getRadioState().isOn()))
                powerOffRadioSafely(this.phone.mDataConnectionTracker);
        }
    }

    protected void setSignalStrengthDefaultValues()
    {
        this.mSignalStrength = new SignalStrength(99, -1, -1, -1, -1, -1, -1, false);
    }

    public void unregisterForSubscriptionInfoReady(Handler paramHandler)
    {
        this.cdmaForSubscriptionInfoReadyRegistrants.remove(paramHandler);
    }

    protected void updateOtaspState()
    {
        int i = getOtasp();
        int j = this.mCurrentOtaspMode;
        this.mCurrentOtaspMode = i;
        if (this.cdmaForSubscriptionInfoReadyRegistrants != null)
        {
            log("CDMA_SUBSCRIPTION: call notifyRegistrants()");
            this.cdmaForSubscriptionInfoReadyRegistrants.notifyRegistrants();
        }
        if (j != this.mCurrentOtaspMode)
        {
            log("CDMA_SUBSCRIPTION: call notifyOtaspChanged old otaspMode=" + j + " new otaspMode=" + this.mCurrentOtaspMode);
            this.phone.notifyOtaspChanged(this.mCurrentOtaspMode);
        }
    }

    protected void updateSpnDisplay()
    {
        String str = this.ss.getOperatorAlphaLong();
        if (!TextUtils.equals(str, this.mCurPlmn))
            if (str == null)
                break label133;
        label133: for (boolean bool = true; ; bool = false)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = Boolean.valueOf(bool);
            arrayOfObject[1] = str;
            log(String.format("updateSpnDisplay: changed sending intent showPlmn='%b' plmn='%s'", arrayOfObject));
            Intent localIntent = new Intent("android.provider.Telephony.SPN_STRINGS_UPDATED");
            localIntent.addFlags(536870912);
            localIntent.putExtra("showSpn", false);
            localIntent.putExtra("spn", "");
            localIntent.putExtra("showPlmn", bool);
            localIntent.putExtra("plmn", str);
            this.phone.getContext().sendStickyBroadcast(localIntent);
            this.mCurPlmn = str;
            return;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    class LocaleChangedIntentReceiver extends BroadcastReceiver
    {
        LocaleChangedIntentReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if (!CdmaServiceStateTracker.this.phone.mIsTheCurrentActivePhone)
                Log.e("CDMA", "Received Intent " + paramIntent + " while being destroyed. Ignoring.");
            while (true)
            {
                return;
                if (paramIntent.getAction().equals("android.intent.action.LOCALE_CHANGED"))
                    CdmaServiceStateTracker.this.updateSpnDisplay();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaServiceStateTracker
 * JD-Core Version:        0.6.2
 */