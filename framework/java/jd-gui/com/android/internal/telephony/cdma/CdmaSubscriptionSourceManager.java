package com.android.internal.telephony.cdma;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import android.provider.Settings.Secure;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import java.util.concurrent.atomic.AtomicInteger;

public class CdmaSubscriptionSourceManager extends Handler
{
    private static final int EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED = 1;
    private static final int EVENT_GET_CDMA_SUBSCRIPTION_SOURCE = 2;
    private static final int EVENT_RADIO_ON = 3;
    static final String LOG_TAG = "CDMA";
    public static final int PREFERRED_CDMA_SUBSCRIPTION = 1;
    public static final int SUBSCRIPTION_FROM_NV = 1;
    public static final int SUBSCRIPTION_FROM_RUIM = 0;
    public static final int SUBSCRIPTION_SOURCE_UNKNOWN = -1;
    private static CdmaSubscriptionSourceManager sInstance;
    private static int sReferenceCount = 0;
    private static final Object sReferenceCountMonitor = new Object();
    private CommandsInterface mCM;
    private AtomicInteger mCdmaSubscriptionSource = new AtomicInteger(1);
    private RegistrantList mCdmaSubscriptionSourceChangedRegistrants = new RegistrantList();
    private Context mContext;

    private CdmaSubscriptionSourceManager(Context paramContext, CommandsInterface paramCommandsInterface)
    {
        this.mContext = paramContext;
        this.mCM = paramCommandsInterface;
        this.mCM.registerForCdmaSubscriptionChanged(this, 1, null);
        this.mCM.registerForOn(this, 3, null);
        int i = getDefaultCdmaSubscriptionSource();
        this.mCdmaSubscriptionSource.set(i);
    }

    private int getDefaultCdmaSubscriptionSource()
    {
        return Settings.Secure.getInt(this.mContext.getContentResolver(), "subscription_mode", 1);
    }

    public static CdmaSubscriptionSourceManager getInstance(Context paramContext, CommandsInterface paramCommandsInterface, Handler paramHandler, int paramInt, Object paramObject)
    {
        synchronized (sReferenceCountMonitor)
        {
            if (sInstance == null)
                sInstance = new CdmaSubscriptionSourceManager(paramContext, paramCommandsInterface);
            sReferenceCount = 1 + sReferenceCount;
            sInstance.registerForCdmaSubscriptionSourceChanged(paramHandler, paramInt, paramObject);
            return sInstance;
        }
    }

    private void handleGetCdmaSubscriptionSource(AsyncResult paramAsyncResult)
    {
        if ((paramAsyncResult.exception == null) && (paramAsyncResult.result != null))
        {
            int i = ((int[])(int[])paramAsyncResult.result)[0];
            if (i != this.mCdmaSubscriptionSource.get())
            {
                log("Subscription Source Changed : " + this.mCdmaSubscriptionSource + " >> " + i);
                this.mCdmaSubscriptionSource.set(i);
                this.mCdmaSubscriptionSourceChangedRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
            }
        }
        while (true)
        {
            return;
            logw("Unable to get CDMA Subscription Source, Exception: " + paramAsyncResult.exception + ", result: " + paramAsyncResult.result);
        }
    }

    private void log(String paramString)
    {
        Log.d("CDMA", "[CdmaSSM] " + paramString);
    }

    private void loge(String paramString)
    {
        Log.e("CDMA", "[CdmaSSM] " + paramString);
    }

    private void logw(String paramString)
    {
        Log.w("CDMA", "[CdmaSSM] " + paramString);
    }

    private void registerForCdmaSubscriptionSourceChanged(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mCdmaSubscriptionSourceChangedRegistrants.add(localRegistrant);
    }

    public void dispose(Handler paramHandler)
    {
        this.mCdmaSubscriptionSourceChangedRegistrants.remove(paramHandler);
        synchronized (sReferenceCountMonitor)
        {
            sReferenceCount = -1 + sReferenceCount;
            if (sReferenceCount <= 0)
            {
                this.mCM.unregisterForCdmaSubscriptionChanged(this);
                this.mCM.unregisterForOn(this);
                sInstance = null;
            }
            return;
        }
    }

    public int getCdmaSubscriptionSource()
    {
        return this.mCdmaSubscriptionSource.get();
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
            super.handleMessage(paramMessage);
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            log("CDMA_SUBSCRIPTION_SOURCE event = " + paramMessage.what);
            handleGetCdmaSubscriptionSource((AsyncResult)paramMessage.obj);
            continue;
            this.mCM.getCdmaSubscriptionSource(obtainMessage(2));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaSubscriptionSourceManager
 * JD-Core Version:        0.6.2
 */