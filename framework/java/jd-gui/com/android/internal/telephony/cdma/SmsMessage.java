package com.android.internal.telephony.cdma;

import android.os.Parcel;
import android.os.SystemProperties;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsCbLocation;
import android.telephony.SmsCbMessage;
import android.telephony.SmsMessage.MessageClass;
import android.telephony.cdma.CdmaSmsCbProgramData;
import android.util.Log;
import com.android.internal.telephony.SmsAddress;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsHeader.PortAddrs;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.SmsMessageBase.SubmitPduBase;
import com.android.internal.telephony.SmsMessageBase.TextEncodingDetails;
import com.android.internal.telephony.cdma.sms.BearerData;
import com.android.internal.telephony.cdma.sms.BearerData.TimeStamp;
import com.android.internal.telephony.cdma.sms.CdmaSmsAddress;
import com.android.internal.telephony.cdma.sms.CdmaSmsSubaddress;
import com.android.internal.telephony.cdma.sms.SmsEnvelope;
import com.android.internal.telephony.cdma.sms.UserData;
import com.android.internal.util.BitwiseInputStream;
import com.android.internal.util.HexDump;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class SmsMessage extends SmsMessageBase
{
    private static final byte BEARER_DATA = 8;
    private static final byte BEARER_REPLY_OPTION = 6;
    private static final byte CAUSE_CODES = 7;
    private static final byte DESTINATION_ADDRESS = 4;
    private static final byte DESTINATION_SUB_ADDRESS = 5;
    private static final String LOGGABLE_TAG = "CDMA:SMS";
    static final String LOG_TAG = "CDMA";
    private static final byte ORIGINATING_ADDRESS = 2;
    private static final byte ORIGINATING_SUB_ADDRESS = 3;
    private static final int RETURN_ACK = 1;
    private static final int RETURN_NO_ACK = 0;
    private static final byte SERVICE_CATEGORY = 1;
    private static final byte TELESERVICE_IDENTIFIER;
    private BearerData mBearerData;
    private SmsEnvelope mEnvelope;
    private int status;

    public static SmsMessageBase.TextEncodingDetails calculateLength(CharSequence paramCharSequence, boolean paramBoolean)
    {
        return BearerData.calcTextEncodingDetails(paramCharSequence, paramBoolean);
    }

    private byte convertDtmfToAscii(byte paramByte)
    {
        byte b;
        switch (paramByte)
        {
        default:
            b = 32;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        }
        while (true)
        {
            return b;
            b = 68;
            continue;
            b = 49;
            continue;
            b = 50;
            continue;
            b = 51;
            continue;
            b = 52;
            continue;
            b = 53;
            continue;
            b = 54;
            continue;
            b = 55;
            continue;
            b = 56;
            continue;
            b = 57;
            continue;
            b = 48;
            continue;
            b = 42;
            continue;
            b = 35;
            continue;
            b = 65;
            continue;
            b = 66;
            continue;
            b = 67;
        }
    }

    public static SmsMessage createFromEfRecord(int paramInt, byte[] paramArrayOfByte)
    {
        SmsMessage localSmsMessage;
        try
        {
            localSmsMessage = new SmsMessage();
            localSmsMessage.indexOnIcc = paramInt;
            if ((0x1 & paramArrayOfByte[0]) == 0)
            {
                Log.w("CDMA", "SMS parsing failed: Trying to parse a free record");
                localSmsMessage = null;
            }
            else
            {
                localSmsMessage.statusOnIcc = (0x7 & paramArrayOfByte[0]);
                int i = paramArrayOfByte[1];
                byte[] arrayOfByte = new byte[i];
                System.arraycopy(paramArrayOfByte, 2, arrayOfByte, 0, i);
                localSmsMessage.parsePduFromEfRecord(arrayOfByte);
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            Log.e("CDMA", "SMS PDU parsing failed: ", localRuntimeException);
            localSmsMessage = null;
        }
        return localSmsMessage;
    }

    public static SmsMessage createFromPdu(byte[] paramArrayOfByte)
    {
        SmsMessage localSmsMessage = new SmsMessage();
        try
        {
            localSmsMessage.parsePdu(paramArrayOfByte);
            return localSmsMessage;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                Log.e("CDMA", "SMS PDU parsing failed: ", localRuntimeException);
                localSmsMessage = null;
            }
        }
    }

    private void createPdu()
    {
        SmsEnvelope localSmsEnvelope = this.mEnvelope;
        CdmaSmsAddress localCdmaSmsAddress = localSmsEnvelope.origAddress;
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(100);
        DataOutputStream localDataOutputStream = new DataOutputStream(new BufferedOutputStream(localByteArrayOutputStream));
        try
        {
            localDataOutputStream.writeInt(localSmsEnvelope.messageType);
            localDataOutputStream.writeInt(localSmsEnvelope.teleService);
            localDataOutputStream.writeInt(localSmsEnvelope.serviceCategory);
            localDataOutputStream.writeByte(localCdmaSmsAddress.digitMode);
            localDataOutputStream.writeByte(localCdmaSmsAddress.numberMode);
            localDataOutputStream.writeByte(localCdmaSmsAddress.ton);
            localDataOutputStream.writeByte(localCdmaSmsAddress.numberPlan);
            localDataOutputStream.writeByte(localCdmaSmsAddress.numberOfDigits);
            localDataOutputStream.write(localCdmaSmsAddress.origBytes, 0, localCdmaSmsAddress.origBytes.length);
            localDataOutputStream.writeInt(localSmsEnvelope.bearerReply);
            localDataOutputStream.writeByte(localSmsEnvelope.replySeqNo);
            localDataOutputStream.writeByte(localSmsEnvelope.errorClass);
            localDataOutputStream.writeByte(localSmsEnvelope.causeCode);
            localDataOutputStream.writeInt(localSmsEnvelope.bearerData.length);
            localDataOutputStream.write(localSmsEnvelope.bearerData, 0, localSmsEnvelope.bearerData.length);
            localDataOutputStream.close();
            this.mPdu = localByteArrayOutputStream.toByteArray();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.e("CDMA", "createPdu: conversion from object to byte array failed: " + localIOException);
        }
    }

    /** @deprecated */
    static int getNextMessageId()
    {
        try
        {
            int i = SystemProperties.getInt("persist.radio.cdma.msgid", 1);
            String str = Integer.toString(1 + i % 65535);
            SystemProperties.set("persist.radio.cdma.msgid", str);
            if (Log.isLoggable("CDMA:SMS", 2))
            {
                Log.d("CDMA", "next persist.radio.cdma.msgid = " + str);
                Log.d("CDMA", "readback gets " + SystemProperties.get("persist.radio.cdma.msgid"));
            }
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static SubmitPdu getSubmitPdu(String paramString, UserData paramUserData, boolean paramBoolean)
    {
        return privateGetSubmitPdu(paramString, paramBoolean, paramUserData);
    }

    public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfByte, boolean paramBoolean)
    {
        SmsHeader.PortAddrs localPortAddrs = new SmsHeader.PortAddrs();
        localPortAddrs.destPort = paramInt;
        localPortAddrs.origPort = 0;
        localPortAddrs.areEightBits = false;
        SmsHeader localSmsHeader = new SmsHeader();
        localSmsHeader.portAddrs = localPortAddrs;
        UserData localUserData = new UserData();
        localUserData.userDataHeader = localSmsHeader;
        localUserData.msgEncoding = 0;
        localUserData.msgEncodingSet = true;
        localUserData.payload = paramArrayOfByte;
        return privateGetSubmitPdu(paramString2, paramBoolean, localUserData);
    }

    public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, SmsHeader paramSmsHeader)
    {
        if ((paramString3 == null) || (paramString2 == null));
        UserData localUserData;
        for (SubmitPdu localSubmitPdu = null; ; localSubmitPdu = privateGetSubmitPdu(paramString2, paramBoolean, localUserData))
        {
            return localSubmitPdu;
            localUserData = new UserData();
            localUserData.payloadStr = paramString3;
            localUserData.userDataHeader = paramSmsHeader;
        }
    }

    public static int getTPLayerLengthForPDU(String paramString)
    {
        Log.w("CDMA", "getTPLayerLengthForPDU: is not supported in CDMA mode.");
        return 0;
    }

    public static SmsMessage newFromParcel(Parcel paramParcel)
    {
        SmsMessage localSmsMessage = new SmsMessage();
        SmsEnvelope localSmsEnvelope = new SmsEnvelope();
        CdmaSmsAddress localCdmaSmsAddress = new CdmaSmsAddress();
        CdmaSmsSubaddress localCdmaSmsSubaddress = new CdmaSmsSubaddress();
        localSmsEnvelope.teleService = paramParcel.readInt();
        if (paramParcel.readByte() != 0)
            localSmsEnvelope.messageType = 1;
        byte[] arrayOfByte1;
        while (true)
        {
            localSmsEnvelope.serviceCategory = paramParcel.readInt();
            int i = paramParcel.readInt();
            localCdmaSmsAddress.digitMode = ((byte)(i & 0xFF));
            localCdmaSmsAddress.numberMode = ((byte)(0xFF & paramParcel.readInt()));
            localCdmaSmsAddress.ton = paramParcel.readInt();
            localCdmaSmsAddress.numberPlan = ((byte)(0xFF & paramParcel.readInt()));
            int j = paramParcel.readByte();
            localCdmaSmsAddress.numberOfDigits = j;
            arrayOfByte1 = new byte[j];
            for (int k = 0; k < j; k++)
            {
                arrayOfByte1[k] = paramParcel.readByte();
                if (i == 0)
                    arrayOfByte1[k] = localSmsMessage.convertDtmfToAscii(arrayOfByte1[k]);
            }
            if (localSmsEnvelope.teleService == 0)
                localSmsEnvelope.messageType = 2;
            else
                localSmsEnvelope.messageType = 0;
        }
        localCdmaSmsAddress.origBytes = arrayOfByte1;
        localCdmaSmsSubaddress.type = paramParcel.readInt();
        localCdmaSmsSubaddress.odd = paramParcel.readByte();
        int m = paramParcel.readByte();
        if (m < 0)
            m = 0;
        byte[] arrayOfByte2 = new byte[m];
        int n = 0;
        while (n < m)
        {
            arrayOfByte2[n] = paramParcel.readByte();
            n += 1;
        }
        localCdmaSmsSubaddress.origBytes = arrayOfByte2;
        int i1 = paramParcel.readInt();
        if (i1 < 0)
            i1 = 0;
        byte[] arrayOfByte3 = new byte[i1];
        for (int i2 = 0; i2 < i1; i2++)
            arrayOfByte3[i2] = paramParcel.readByte();
        localSmsEnvelope.bearerData = arrayOfByte3;
        localSmsEnvelope.origAddress = localCdmaSmsAddress;
        localSmsEnvelope.origSubaddress = localCdmaSmsSubaddress;
        localSmsMessage.originatingAddress = localCdmaSmsAddress;
        localSmsMessage.mEnvelope = localSmsEnvelope;
        localSmsMessage.createPdu();
        return localSmsMessage;
    }

    private void parsePdu(byte[] paramArrayOfByte)
    {
        DataInputStream localDataInputStream = new DataInputStream(new ByteArrayInputStream(paramArrayOfByte));
        SmsEnvelope localSmsEnvelope = new SmsEnvelope();
        CdmaSmsAddress localCdmaSmsAddress = new CdmaSmsAddress();
        try
        {
            localSmsEnvelope.messageType = localDataInputStream.readInt();
            localSmsEnvelope.teleService = localDataInputStream.readInt();
            localSmsEnvelope.serviceCategory = localDataInputStream.readInt();
            localCdmaSmsAddress.digitMode = localDataInputStream.readByte();
            localCdmaSmsAddress.numberMode = localDataInputStream.readByte();
            localCdmaSmsAddress.ton = localDataInputStream.readByte();
            localCdmaSmsAddress.numberPlan = localDataInputStream.readByte();
            int i = localDataInputStream.readByte();
            localCdmaSmsAddress.numberOfDigits = i;
            localCdmaSmsAddress.origBytes = new byte[i];
            localDataInputStream.read(localCdmaSmsAddress.origBytes, 0, i);
            localSmsEnvelope.bearerReply = localDataInputStream.readInt();
            localSmsEnvelope.replySeqNo = localDataInputStream.readByte();
            localSmsEnvelope.errorClass = localDataInputStream.readByte();
            localSmsEnvelope.causeCode = localDataInputStream.readByte();
            int j = localDataInputStream.readInt();
            localSmsEnvelope.bearerData = new byte[j];
            localDataInputStream.read(localSmsEnvelope.bearerData, 0, j);
            localDataInputStream.close();
            this.originatingAddress = localCdmaSmsAddress;
            localSmsEnvelope.origAddress = localCdmaSmsAddress;
            this.mEnvelope = localSmsEnvelope;
            this.mPdu = paramArrayOfByte;
            parseSms();
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Log.e("CDMA", "createFromPdu: conversion from byte array to object failed: " + localException);
        }
    }

    private void parsePduFromEfRecord(byte[] paramArrayOfByte)
    {
        ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramArrayOfByte);
        DataInputStream localDataInputStream = new DataInputStream(localByteArrayInputStream);
        SmsEnvelope localSmsEnvelope = new SmsEnvelope();
        CdmaSmsAddress localCdmaSmsAddress = new CdmaSmsAddress();
        CdmaSmsSubaddress localCdmaSmsSubaddress = new CdmaSmsSubaddress();
        int j;
        byte[] arrayOfByte1;
        try
        {
            localSmsEnvelope.messageType = localDataInputStream.readByte();
            if (localDataInputStream.available() > 0)
            {
                int i = localDataInputStream.readByte();
                j = localDataInputStream.readByte();
                arrayOfByte1 = new byte[j];
                switch (i)
                {
                default:
                    throw new Exception("unsupported parameterId (" + i + ")");
                case 0:
                case 1:
                case 2:
                case 4:
                case 3:
                case 5:
                case 6:
                case 7:
                case 8:
                }
            }
        }
        catch (Exception localException)
        {
            Log.e("CDMA", "parsePduFromEfRecord: conversion from pdu to SmsMessage failed" + localException);
        }
        while (true)
        {
            this.originatingAddress = localCdmaSmsAddress;
            localSmsEnvelope.origAddress = localCdmaSmsAddress;
            localSmsEnvelope.origSubaddress = localCdmaSmsSubaddress;
            this.mEnvelope = localSmsEnvelope;
            this.mPdu = paramArrayOfByte;
            parseSms();
            return;
            localSmsEnvelope.teleService = localDataInputStream.readUnsignedShort();
            Log.i("CDMA", "teleservice = " + localSmsEnvelope.teleService);
            break;
            localSmsEnvelope.serviceCategory = localDataInputStream.readUnsignedShort();
            break;
            localDataInputStream.read(arrayOfByte1, 0, j);
            BitwiseInputStream localBitwiseInputStream4 = new BitwiseInputStream(arrayOfByte1);
            localCdmaSmsAddress.digitMode = localBitwiseInputStream4.read(1);
            localCdmaSmsAddress.numberMode = localBitwiseInputStream4.read(1);
            int n = 0;
            if (localCdmaSmsAddress.digitMode == 1)
            {
                n = localBitwiseInputStream4.read(3);
                localCdmaSmsAddress.ton = n;
                if (localCdmaSmsAddress.numberMode == 0)
                    localCdmaSmsAddress.numberPlan = localBitwiseInputStream4.read(4);
            }
            localCdmaSmsAddress.numberOfDigits = localBitwiseInputStream4.read(8);
            byte[] arrayOfByte3 = new byte[localCdmaSmsAddress.numberOfDigits];
            if (localCdmaSmsAddress.digitMode == 0)
                for (int i2 = 0; i2 < localCdmaSmsAddress.numberOfDigits; i2++)
                    arrayOfByte3[i2] = convertDtmfToAscii((byte)(0xF & localBitwiseInputStream4.read(4)));
            if (localCdmaSmsAddress.digitMode == 1)
            {
                if (localCdmaSmsAddress.numberMode == 0)
                    for (int i1 = 0; i1 < localCdmaSmsAddress.numberOfDigits; i1++)
                        arrayOfByte3[i1] = ((byte)(0xFF & localBitwiseInputStream4.read(8)));
                if (localCdmaSmsAddress.numberMode == 1)
                    if (n == 2)
                        Log.e("CDMA", "TODO: Originating Addr is email id");
            }
            while (true)
            {
                localCdmaSmsAddress.origBytes = arrayOfByte3;
                Log.i("CDMA", "Originating Addr=" + localCdmaSmsAddress.toString());
                break;
                Log.e("CDMA", "TODO: Originating Addr is data network address");
                continue;
                Log.e("CDMA", "Originating Addr is of incorrect type");
                continue;
                Log.e("CDMA", "Incorrect Digit mode");
            }
            localDataInputStream.read(arrayOfByte1, 0, j);
            BitwiseInputStream localBitwiseInputStream3 = new BitwiseInputStream(arrayOfByte1);
            localCdmaSmsSubaddress.type = localBitwiseInputStream3.read(3);
            localCdmaSmsSubaddress.odd = localBitwiseInputStream3.readByteArray(1)[0];
            int k = localBitwiseInputStream3.read(8);
            byte[] arrayOfByte2 = new byte[k];
            for (int m = 0; m < k; m++)
                arrayOfByte2[m] = convertDtmfToAscii((byte)(0xFF & localBitwiseInputStream3.read(4)));
            localCdmaSmsSubaddress.origBytes = arrayOfByte2;
            break;
            localDataInputStream.read(arrayOfByte1, 0, j);
            BitwiseInputStream localBitwiseInputStream2 = new BitwiseInputStream(arrayOfByte1);
            localSmsEnvelope.bearerReply = localBitwiseInputStream2.read(6);
            break;
            localDataInputStream.read(arrayOfByte1, 0, j);
            BitwiseInputStream localBitwiseInputStream1 = new BitwiseInputStream(arrayOfByte1);
            localSmsEnvelope.replySeqNo = localBitwiseInputStream1.readByteArray(6)[0];
            localSmsEnvelope.errorClass = localBitwiseInputStream1.readByteArray(2)[0];
            if (localSmsEnvelope.errorClass == 0)
                break;
            localSmsEnvelope.causeCode = localBitwiseInputStream1.readByteArray(8)[0];
            break;
            localDataInputStream.read(arrayOfByte1, 0, j);
            localSmsEnvelope.bearerData = arrayOfByte1;
            break;
            localByteArrayInputStream.close();
            localDataInputStream.close();
        }
    }

    private static SubmitPdu privateGetSubmitPdu(String paramString, boolean paramBoolean, UserData paramUserData)
    {
        CdmaSmsAddress localCdmaSmsAddress = CdmaSmsAddress.parse(PhoneNumberUtils.cdmaCheckAndProcessPlusCode(paramString));
        if (localCdmaSmsAddress == null);
        BearerData localBearerData;
        byte[] arrayOfByte;
        for (SubmitPdu localSubmitPdu = null; ; localSubmitPdu = null)
        {
            return localSubmitPdu;
            localBearerData = new BearerData();
            localBearerData.messageType = 2;
            localBearerData.messageId = getNextMessageId();
            localBearerData.deliveryAckReq = paramBoolean;
            localBearerData.userAckReq = false;
            localBearerData.readAckReq = false;
            localBearerData.reportReq = false;
            localBearerData.userData = paramUserData;
            arrayOfByte = BearerData.encode(localBearerData);
            if (Log.isLoggable("CDMA:SMS", 2))
            {
                Log.d("CDMA", "MO (encoded) BearerData = " + localBearerData);
                Log.d("CDMA", "MO raw BearerData = '" + HexDump.toHexString(arrayOfByte) + "'");
            }
            if (arrayOfByte != null)
                break;
        }
        if (localBearerData.hasUserDataHeader);
        for (int i = 4101; ; i = 4098)
        {
            while (true)
            {
                SmsEnvelope localSmsEnvelope = new SmsEnvelope();
                localSmsEnvelope.messageType = 0;
                localSmsEnvelope.teleService = i;
                localSmsEnvelope.destAddress = localCdmaSmsAddress;
                localSmsEnvelope.bearerReply = 1;
                localSmsEnvelope.bearerData = arrayOfByte;
                try
                {
                    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(100);
                    DataOutputStream localDataOutputStream = new DataOutputStream(localByteArrayOutputStream);
                    localDataOutputStream.writeInt(localSmsEnvelope.teleService);
                    localDataOutputStream.writeInt(0);
                    localDataOutputStream.writeInt(0);
                    localDataOutputStream.write(localCdmaSmsAddress.digitMode);
                    localDataOutputStream.write(localCdmaSmsAddress.numberMode);
                    localDataOutputStream.write(localCdmaSmsAddress.ton);
                    localDataOutputStream.write(localCdmaSmsAddress.numberPlan);
                    localDataOutputStream.write(localCdmaSmsAddress.numberOfDigits);
                    localDataOutputStream.write(localCdmaSmsAddress.origBytes, 0, localCdmaSmsAddress.origBytes.length);
                    localDataOutputStream.write(0);
                    localDataOutputStream.write(0);
                    localDataOutputStream.write(0);
                    localDataOutputStream.write(arrayOfByte.length);
                    localDataOutputStream.write(arrayOfByte, 0, arrayOfByte.length);
                    localDataOutputStream.close();
                    localSubmitPdu = new SubmitPdu();
                    localSubmitPdu.encodedMessage = localByteArrayOutputStream.toByteArray();
                    localSubmitPdu.encodedScAddress = null;
                }
                catch (IOException localIOException)
                {
                    Log.e("CDMA", "creating SubmitPdu failed: " + localIOException);
                    localSubmitPdu = null;
                }
            }
            break;
        }
    }

    byte[] getIncomingSmsFingerprint()
    {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        localByteArrayOutputStream.write(this.mEnvelope.teleService);
        localByteArrayOutputStream.write(this.mEnvelope.origAddress.origBytes, 0, this.mEnvelope.origAddress.origBytes.length);
        localByteArrayOutputStream.write(this.mEnvelope.bearerData, 0, this.mEnvelope.bearerData.length);
        localByteArrayOutputStream.write(this.mEnvelope.origSubaddress.origBytes, 0, this.mEnvelope.origSubaddress.origBytes.length);
        return localByteArrayOutputStream.toByteArray();
    }

    public SmsMessage.MessageClass getMessageClass()
    {
        if (this.mBearerData.displayMode == 0);
        for (SmsMessage.MessageClass localMessageClass = SmsMessage.MessageClass.CLASS_0; ; localMessageClass = SmsMessage.MessageClass.UNKNOWN)
            return localMessageClass;
    }

    int getMessageType()
    {
        if (this.mEnvelope.serviceCategory != 0);
        for (int i = 1; ; i = 0)
            return i;
    }

    int getNumOfVoicemails()
    {
        return this.mBearerData.numberOfMessages;
    }

    public int getProtocolIdentifier()
    {
        Log.w("CDMA", "getProtocolIdentifier: is not supported in CDMA mode.");
        return 0;
    }

    ArrayList<CdmaSmsCbProgramData> getSmsCbProgramData()
    {
        return this.mBearerData.serviceCategoryProgramData;
    }

    public int getStatus()
    {
        return this.status << 16;
    }

    int getTeleService()
    {
        return this.mEnvelope.teleService;
    }

    public boolean isCphsMwiMessage()
    {
        Log.w("CDMA", "isCphsMwiMessage: is not supported in CDMA mode.");
        return false;
    }

    public boolean isMWIClearMessage()
    {
        if ((this.mBearerData != null) && (this.mBearerData.numberOfMessages == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isMWISetMessage()
    {
        if ((this.mBearerData != null) && (this.mBearerData.numberOfMessages > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isMwiDontStore()
    {
        if ((this.mBearerData != null) && (this.mBearerData.numberOfMessages > 0) && (this.mBearerData.userData == null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isReplace()
    {
        Log.w("CDMA", "isReplace: is not supported in CDMA mode.");
        return false;
    }

    public boolean isReplyPathPresent()
    {
        Log.w("CDMA", "isReplyPathPresent: is not supported in CDMA mode.");
        return false;
    }

    public boolean isStatusReportMessage()
    {
        if (this.mBearerData.messageType == 4);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    SmsCbMessage parseBroadcastSms()
    {
        SmsCbMessage localSmsCbMessage = null;
        BearerData localBearerData = BearerData.decode(this.mEnvelope.bearerData, this.mEnvelope.serviceCategory);
        if (localBearerData == null)
            Log.w("CDMA", "BearerData.decode() returned null");
        while (true)
        {
            return localSmsCbMessage;
            if (Log.isLoggable("CDMA:SMS", 2))
                Log.d("CDMA", "MT raw BearerData = " + HexDump.toHexString(this.mEnvelope.bearerData));
            SmsCbLocation localSmsCbLocation = new SmsCbLocation(SystemProperties.get("gsm.operator.numeric"));
            localSmsCbMessage = new SmsCbMessage(2, 1, localBearerData.messageId, localSmsCbLocation, this.mEnvelope.serviceCategory, localBearerData.getLanguage(), localBearerData.userData.payloadStr, localBearerData.priority, null, localBearerData.cmasWarningInfo);
        }
    }

    protected void parseSms()
    {
        if (this.mEnvelope.teleService == 262144)
        {
            this.mBearerData = new BearerData();
            if (this.mEnvelope.bearerData != null)
                this.mBearerData.numberOfMessages = (0xFF & this.mEnvelope.bearerData[0]);
        }
        while (true)
        {
            return;
            this.mBearerData = BearerData.decode(this.mEnvelope.bearerData);
            if (Log.isLoggable("CDMA:SMS", 2))
            {
                Log.d("CDMA", "MT raw BearerData = '" + HexDump.toHexString(this.mEnvelope.bearerData) + "'");
                Log.d("CDMA", "MT (decoded) BearerData = " + this.mBearerData);
            }
            this.messageRef = this.mBearerData.messageId;
            if (this.mBearerData.userData != null)
            {
                this.userData = this.mBearerData.userData.payload;
                this.userDataHeader = this.mBearerData.userData.userDataHeader;
                this.messageBody = this.mBearerData.userData.payloadStr;
            }
            if (this.originatingAddress != null)
                this.originatingAddress.address = new String(this.originatingAddress.origBytes);
            if (this.mBearerData.msgCenterTimeStamp != null)
                this.scTimeMillis = this.mBearerData.msgCenterTimeStamp.toMillis(true);
            String str;
            if (this.mBearerData.messageType == 4)
                if (!this.mBearerData.messageStatusSet)
                {
                    StringBuilder localStringBuilder = new StringBuilder().append("DELIVERY_ACK message without msgStatus (");
                    if (this.userData == null)
                    {
                        str = "also missing";
                        label310: Log.d("CDMA", str + " userData).");
                        this.status = 0;
                    }
                }
            while (true)
                if (this.messageBody != null)
                {
                    parseMessageBody();
                    break;
                    str = "does have";
                    break label310;
                    this.status = (this.mBearerData.errorClass << 8);
                    this.status |= this.mBearerData.messageStatus;
                    continue;
                    if (this.mBearerData.messageType != 1)
                        throw new RuntimeException("Unsupported message type: " + this.mBearerData.messageType);
                }
            if (this.userData == null);
        }
    }

    public static class SubmitPdu extends SmsMessageBase.SubmitPduBase
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.SmsMessage
 * JD-Core Version:        0.6.2
 */