package com.android.internal.telephony.cdma;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccPhoneBookInterfaceManager;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.PhoneBase;
import java.util.concurrent.atomic.AtomicBoolean;

public class RuimPhoneBookInterfaceManager extends IccPhoneBookInterfaceManager
{
    static final String LOG_TAG = "CDMA";

    public RuimPhoneBookInterfaceManager(CDMAPhone paramCDMAPhone)
    {
        super(paramCDMAPhone);
        this.adnCache = paramCDMAPhone.mIccRecords.getAdnCache();
    }

    public void dispose()
    {
        super.dispose();
    }

    protected void finalize()
    {
        try
        {
            super.finalize();
            Log.d("CDMA", "RuimPhoneBookInterfaceManager finalized");
            return;
        }
        catch (Throwable localThrowable)
        {
            while (true)
                Log.e("CDMA", "Error while finalizing:", localThrowable);
        }
    }

    public int[] getAdnRecordsSize(int paramInt)
    {
        logd("getAdnRecordsSize: efid=" + paramInt);
        synchronized (this.mLock)
        {
            checkThread();
            this.recordSize = new int[3];
            AtomicBoolean localAtomicBoolean = new AtomicBoolean(false);
            Message localMessage = this.mBaseHandler.obtainMessage(1, localAtomicBoolean);
            this.phone.getIccFileHandler().getEFLinearRecordSize(paramInt, localMessage);
            waitForResult(localAtomicBoolean);
            return this.recordSize;
        }
    }

    protected void logd(String paramString)
    {
        Log.d("CDMA", "[RuimPbInterfaceManager] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("CDMA", "[RuimPbInterfaceManager] " + paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.RuimPhoneBookInterfaceManager
 * JD-Core Version:        0.6.2
 */