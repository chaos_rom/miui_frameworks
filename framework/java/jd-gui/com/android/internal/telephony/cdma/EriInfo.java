package com.android.internal.telephony.cdma;

public final class EriInfo
{
    public static final int ROAMING_ICON_MODE_FLASH = 1;
    public static final int ROAMING_ICON_MODE_NORMAL = 0;
    public static final int ROAMING_INDICATOR_FLASH = 2;
    public static final int ROAMING_INDICATOR_OFF = 1;
    public static final int ROAMING_INDICATOR_ON;
    public int mAlertId;
    public int mCallPromptId;
    public String mEriText;
    public int mIconIndex;
    public int mIconMode;
    public int mRoamingIndicator;

    public EriInfo(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5)
    {
        this.mRoamingIndicator = paramInt1;
        this.mIconIndex = paramInt2;
        this.mIconMode = paramInt3;
        this.mEriText = paramString;
        this.mCallPromptId = paramInt4;
        this.mAlertId = paramInt5;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.EriInfo
 * JD-Core Version:        0.6.2
 */