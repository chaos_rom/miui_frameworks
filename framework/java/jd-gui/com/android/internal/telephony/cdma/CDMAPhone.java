package com.android.internal.telephony.cdma;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.provider.Telephony.Carriers;
import android.telephony.CellLocation;
import android.telephony.PhoneNumberUtils;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CallTracker;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandException.Error;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.DataConnectionTracker.Activity;
import com.android.internal.telephony.DataConnectionTracker.State;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccException;
import com.android.internal.telephony.IccPhoneBookInterfaceManager;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.IccSmsInterfaceManager;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.MmiCode;
import com.android.internal.telephony.OperatorInfo;
import com.android.internal.telephony.Phone.DataActivityState;
import com.android.internal.telephony.Phone.DataState;
import com.android.internal.telephony.Phone.State;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.PhoneNotifier;
import com.android.internal.telephony.PhoneProxy;
import com.android.internal.telephony.PhoneSubInfo;
import com.android.internal.telephony.SMSDispatcher;
import com.android.internal.telephony.ServiceStateTracker;
import com.android.internal.telephony.UUSInfo;
import com.android.internal.telephony.uicc.UiccController;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CDMAPhone extends PhoneBase
{
    static final int CANCEL_ECM_TIMER = 1;
    private static final boolean DBG = true;
    private static final int DEFAULT_ECM_EXIT_TIMER_VALUE = 300000;
    private static final int INVALID_SYSTEM_SELECTION_CODE = -1;
    private static final String IS683A_FEATURE_CODE = "*228";
    private static final int IS683A_FEATURE_CODE_NUM_DIGITS = 4;
    private static final int IS683A_SYS_SEL_CODE_NUM_DIGITS = 2;
    private static final int IS683A_SYS_SEL_CODE_OFFSET = 4;
    private static final int IS683_CONST_1900MHZ_A_BLOCK = 2;
    private static final int IS683_CONST_1900MHZ_B_BLOCK = 3;
    private static final int IS683_CONST_1900MHZ_C_BLOCK = 4;
    private static final int IS683_CONST_1900MHZ_D_BLOCK = 5;
    private static final int IS683_CONST_1900MHZ_E_BLOCK = 6;
    private static final int IS683_CONST_1900MHZ_F_BLOCK = 7;
    private static final int IS683_CONST_800MHZ_A_BAND = 0;
    private static final int IS683_CONST_800MHZ_B_BAND = 1;
    static final String LOG_TAG = "CDMA";
    static String PROPERTY_CDMA_HOME_OPERATOR_NUMERIC = "ro.cdma.home.operator.numeric";
    static final int RESTART_ECM_TIMER = 0;
    private static final boolean VDBG = false;
    static final String VM_COUNT_CDMA = "vm_count_key_cdma";
    private static final String VM_NUMBER_CDMA = "vm_number_key_cdma";
    private static Pattern pOtaSpNumSchema = Pattern.compile("[,\\s]+");
    CdmaCallTracker mCT;
    private String mCarrierOtaSpNumSchema;
    CdmaSubscriptionSourceManager mCdmaSSM;
    int mCdmaSubscriptionSource = -1;
    private Registrant mEcmExitRespRegistrant;
    private final RegistrantList mEcmTimerResetRegistrants = new RegistrantList();
    private final RegistrantList mEriFileLoadedRegistrants = new RegistrantList();
    EriManager mEriManager;
    private String mEsn;
    private Runnable mExitEcmRunnable = new Runnable()
    {
        public void run()
        {
            CDMAPhone.this.exitEmergencyCallbackMode();
        }
    };
    protected String mImei;
    protected String mImeiSv;
    private boolean mIsPhoneInEcmState;
    private String mMeid;
    ArrayList<CdmaMmiCode> mPendingMmis = new ArrayList();
    Registrant mPostDialHandler;
    RuimPhoneBookInterfaceManager mRuimPhoneBookInterfaceManager;
    RuimSmsInterfaceManager mRuimSmsInterfaceManager;
    CdmaServiceStateTracker mSST;
    PhoneSubInfo mSubInfo;
    private String mVmNumber = null;
    PowerManager.WakeLock mWakeLock;

    public CDMAPhone(Context paramContext, CommandsInterface paramCommandsInterface, PhoneNotifier paramPhoneNotifier)
    {
        super(paramPhoneNotifier, paramContext, paramCommandsInterface, false);
        initSstIcc();
        init(paramContext, paramPhoneNotifier);
    }

    public CDMAPhone(Context paramContext, CommandsInterface paramCommandsInterface, PhoneNotifier paramPhoneNotifier, boolean paramBoolean)
    {
        super(paramPhoneNotifier, paramContext, paramCommandsInterface, paramBoolean);
        initSstIcc();
        init(paramContext, paramPhoneNotifier);
    }

    private static boolean checkOtaSpNumBasedOnSysSelCode(int paramInt, String[] paramArrayOfString)
    {
        boolean bool = false;
        try
        {
            int i = Integer.parseInt(paramArrayOfString[1]);
            for (int j = 0; ; j++)
                if (j < i)
                {
                    if ((!TextUtils.isEmpty(paramArrayOfString[(j + 2)])) && (!TextUtils.isEmpty(paramArrayOfString[(j + 3)])))
                    {
                        int k = Integer.parseInt(paramArrayOfString[(j + 2)]);
                        int m = Integer.parseInt(paramArrayOfString[(j + 3)]);
                        if ((paramInt >= k) && (paramInt <= m))
                            bool = true;
                    }
                }
                else
                    return bool;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Log.e("CDMA", "checkOtaSpNumBasedOnSysSelCode, error", localNumberFormatException);
        }
    }

    private static int extractSelCodeFromOtaSpNum(String paramString)
    {
        int i = paramString.length();
        int j = -1;
        if ((paramString.regionMatches(0, "*228", 0, 4)) && (i >= 6))
            j = Integer.parseInt(paramString.substring(4, 6));
        Log.d("CDMA", "extractSelCodeFromOtaSpNum " + j);
        return j;
    }

    private void handleCdmaSubscriptionSource(int paramInt)
    {
        if (paramInt != this.mCdmaSubscriptionSource)
        {
            this.mCdmaSubscriptionSource = paramInt;
            if (paramInt == 1)
                sendMessage(obtainMessage(23));
        }
    }

    private void handleEnterEmergencyCallbackMode(Message paramMessage)
    {
        Log.d("CDMA", "handleEnterEmergencyCallbackMode,mIsPhoneInEcmState= " + this.mIsPhoneInEcmState);
        if (!this.mIsPhoneInEcmState)
        {
            this.mIsPhoneInEcmState = true;
            sendEmergencyCallbackModeChange();
            setSystemProperty("ril.cdma.inecmmode", "true");
            long l = SystemProperties.getLong("ro.cdma.ecmexittimer", 300000L);
            postDelayed(this.mExitEcmRunnable, l);
            this.mWakeLock.acquire();
        }
    }

    private void handleExitEmergencyCallbackMode(Message paramMessage)
    {
        AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
        Log.d("CDMA", "handleExitEmergencyCallbackMode,ar.exception , mIsPhoneInEcmState " + localAsyncResult.exception + this.mIsPhoneInEcmState);
        removeCallbacks(this.mExitEcmRunnable);
        if (this.mEcmExitRespRegistrant != null)
            this.mEcmExitRespRegistrant.notifyRegistrant(localAsyncResult);
        if (localAsyncResult.exception == null)
        {
            if (this.mIsPhoneInEcmState)
            {
                this.mIsPhoneInEcmState = false;
                setSystemProperty("ril.cdma.inecmmode", "false");
            }
            sendEmergencyCallbackModeChange();
            this.mDataConnectionTracker.setInternalDataEnabled(true);
        }
    }

    private boolean isCarrierOtaSpNum(String paramString)
    {
        boolean bool1 = false;
        int i = extractSelCodeFromOtaSpNum(paramString);
        boolean bool2;
        if (i == -1)
        {
            bool2 = false;
            return bool2;
        }
        String[] arrayOfString;
        if (!TextUtils.isEmpty(this.mCarrierOtaSpNumSchema))
        {
            Matcher localMatcher = pOtaSpNumSchema.matcher(this.mCarrierOtaSpNumSchema);
            Log.d("CDMA", "isCarrierOtaSpNum,schema" + this.mCarrierOtaSpNumSchema);
            if (localMatcher.find())
            {
                arrayOfString = pOtaSpNumSchema.split(this.mCarrierOtaSpNumSchema);
                if ((!TextUtils.isEmpty(arrayOfString[0])) && (arrayOfString[0].equals("SELC")))
                    if (i != -1)
                        bool1 = checkOtaSpNumBasedOnSysSelCode(i, arrayOfString);
            }
        }
        while (true)
        {
            bool2 = bool1;
            break;
            Log.d("CDMA", "isCarrierOtaSpNum,sysSelCodeInt is invalid");
            continue;
            if ((!TextUtils.isEmpty(arrayOfString[0])) && (arrayOfString[0].equals("FC")))
            {
                int j = Integer.parseInt(arrayOfString[1]);
                if (paramString.regionMatches(0, arrayOfString[2], 0, j))
                    bool1 = true;
                else
                    Log.d("CDMA", "isCarrierOtaSpNum,not otasp number");
            }
            else
            {
                Log.d("CDMA", "isCarrierOtaSpNum,ota schema not supported" + arrayOfString[0]);
                continue;
                Log.d("CDMA", "isCarrierOtaSpNum,ota schema pattern not right" + this.mCarrierOtaSpNumSchema);
                continue;
                Log.d("CDMA", "isCarrierOtaSpNum,ota schema pattern empty");
            }
        }
    }

    private static boolean isIs683OtaSpDialStr(String paramString)
    {
        boolean bool = false;
        if (paramString.length() == 4)
            if (paramString.equals("*228"))
                bool = true;
        while (true)
        {
            return bool;
            switch (extractSelCodeFromOtaSpNum(paramString))
            {
            default:
                break;
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                bool = true;
            }
        }
    }

    private void processIccRecordEvents(int paramInt)
    {
        switch (paramInt)
        {
        default:
            Log.e("CDMA", "Unknown icc records event code " + paramInt);
        case 0:
        }
        while (true)
        {
            return;
            notifyMessageWaitingIndicator();
        }
    }

    private void registerForRuimRecordEvents()
    {
        this.mIccRecords.registerForRecordsEvents(this, 30, null);
        this.mIccRecords.registerForRecordsLoaded(this, 22, null);
    }

    private void setIsoCountryProperty(String paramString)
    {
        if (TextUtils.isEmpty(paramString))
            setSystemProperty("gsm.sim.operator.iso-country", "");
        while (true)
        {
            return;
            Object localObject = "";
            try
            {
                String str = MccTable.countryCodeForMcc(Integer.parseInt(paramString.substring(0, 3)));
                localObject = str;
                setSystemProperty("gsm.sim.operator.iso-country", (String)localObject);
            }
            catch (NumberFormatException localNumberFormatException)
            {
                while (true)
                    Log.w("CDMA", "countryCodeForMcc error" + localNumberFormatException);
            }
            catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException)
            {
                while (true)
                    Log.w("CDMA", "countryCodeForMcc error" + localStringIndexOutOfBoundsException);
            }
        }
    }

    private void storeVoiceMailNumber(String paramString)
    {
        SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        localEditor.putString("vm_number_key_cdma", paramString);
        localEditor.apply();
    }

    private void unregisterForRuimRecordEvents()
    {
        this.mIccRecords.unregisterForRecordsEvents(this);
        this.mIccRecords.unregisterForRecordsLoaded(this);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void acceptCall()
        throws CallStateException
    {
        removeMessages(15);
        this.mCT.acceptCall();
    }

    public void activateCellBroadcastSms(int paramInt, Message paramMessage)
    {
        Log.e("CDMA", "[CDMAPhone] activateCellBroadcastSms() is obsolete; use SmsManager");
        paramMessage.sendToTarget();
    }

    public boolean canConference()
    {
        Log.e("CDMA", "canConference: not possible in CDMA");
        return false;
    }

    public boolean canTransfer()
    {
        Log.e("CDMA", "canTransfer: not possible in CDMA");
        return false;
    }

    public void clearDisconnected()
    {
        this.mCT.clearDisconnected();
    }

    public void conference()
        throws CallStateException
    {
        Log.e("CDMA", "conference: not possible in CDMA");
    }

    public Connection dial(String paramString)
        throws CallStateException
    {
        String str = PhoneNumberUtils.stripSeparators(paramString);
        return this.mCT.dial(str);
    }

    public Connection dial(String paramString, UUSInfo paramUUSInfo)
        throws CallStateException
    {
        throw new CallStateException("Sending UUS information NOT supported in CDMA!");
    }

    public void disableLocationUpdates()
    {
        this.mSST.disableLocationUpdates();
    }

    public void dispose()
    {
        synchronized (PhoneProxy.lockForRadioTechnologyChange)
        {
            super.dispose();
            log("dispose");
            unregisterForRuimRecordEvents();
            this.mCM.unregisterForAvailable(this);
            this.mCM.unregisterForOffOrNotAvailable(this);
            this.mCM.unregisterForOn(this);
            this.mSST.unregisterForNetworkAttached(this);
            this.mCM.unSetOnSuppServiceNotification(this);
            removeCallbacks(this.mExitEcmRunnable);
            this.mPendingMmis.clear();
            this.mCT.dispose();
            this.mDataConnectionTracker.dispose();
            this.mSST.dispose();
            this.mCdmaSSM.dispose(this);
            this.mSMS.dispose();
            this.mRuimPhoneBookInterfaceManager.dispose();
            this.mRuimSmsInterfaceManager.dispose();
            this.mSubInfo.dispose();
            this.mEriManager.dispose();
            return;
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("CDMAPhone extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" mVmNumber=" + this.mVmNumber);
        paramPrintWriter.println(" mCT=" + this.mCT);
        paramPrintWriter.println(" mSST=" + this.mSST);
        paramPrintWriter.println(" mCdmaSSM=" + this.mCdmaSSM);
        paramPrintWriter.println(" mPendingMmis=" + this.mPendingMmis);
        paramPrintWriter.println(" mRuimPhoneBookInterfaceManager=" + this.mRuimPhoneBookInterfaceManager);
        paramPrintWriter.println(" mRuimSmsInterfaceManager=" + this.mRuimSmsInterfaceManager);
        paramPrintWriter.println(" mCdmaSubscriptionSource=" + this.mCdmaSubscriptionSource);
        paramPrintWriter.println(" mSubInfo=" + this.mSubInfo);
        paramPrintWriter.println(" mEriManager=" + this.mEriManager);
        paramPrintWriter.println(" mWakeLock=" + this.mWakeLock);
        paramPrintWriter.println(" mIsPhoneInEcmState=" + this.mIsPhoneInEcmState);
        paramPrintWriter.println(" mCarrierOtaSpNumSchema=" + this.mCarrierOtaSpNumSchema);
        paramPrintWriter.println(" getCdmaEriIconIndex()=" + getCdmaEriIconIndex());
        paramPrintWriter.println(" getCdmaEriIconMode()=" + getCdmaEriIconMode());
        paramPrintWriter.println(" getCdmaEriText()=" + getCdmaEriText());
        paramPrintWriter.println(" isMinInfoReady()=" + isMinInfoReady());
        paramPrintWriter.println(" isCspPlmnEnabled()=" + isCspPlmnEnabled());
    }

    public void enableEnhancedVoicePrivacy(boolean paramBoolean, Message paramMessage)
    {
        this.mCM.setPreferredVoicePrivacy(paramBoolean, paramMessage);
    }

    public void enableLocationUpdates()
    {
        this.mSST.enableLocationUpdates();
    }

    public void exitEmergencyCallbackMode()
    {
        if (this.mWakeLock.isHeld())
            this.mWakeLock.release();
        this.mCM.exitEmergencyCallbackMode(obtainMessage(26));
    }

    public void explicitCallTransfer()
    {
        Log.e("CDMA", "explicitCallTransfer: not possible in CDMA");
    }

    protected void finalize()
    {
        Log.d("CDMA", "CDMAPhone finalized");
        if (this.mWakeLock.isHeld())
        {
            Log.e("CDMA", "UNEXPECTED; mWakeLock is held when finalizing.");
            this.mWakeLock.release();
        }
    }

    public void getAvailableNetworks(Message paramMessage)
    {
        Log.e("CDMA", "getAvailableNetworks: not possible in CDMA");
    }

    public CdmaCall getBackgroundCall()
    {
        return this.mCT.backgroundCall;
    }

    public boolean getCallForwardingIndicator()
    {
        Log.e("CDMA", "getCallForwardingIndicator: not possible in CDMA");
        return false;
    }

    public void getCallForwardingOption(int paramInt, Message paramMessage)
    {
        Log.e("CDMA", "getCallForwardingOption: not possible in CDMA");
    }

    public CallTracker getCallTracker()
    {
        return this.mCT;
    }

    public void getCallWaiting(Message paramMessage)
    {
        this.mCM.queryCallWaiting(1, paramMessage);
    }

    public int getCdmaEriIconIndex()
    {
        return getServiceState().getCdmaEriIconIndex();
    }

    public int getCdmaEriIconMode()
    {
        return getServiceState().getCdmaEriIconMode();
    }

    public String getCdmaEriText()
    {
        int i = getServiceState().getCdmaRoamingIndicator();
        int j = getServiceState().getCdmaDefaultRoamingIndicator();
        return this.mEriManager.getCdmaEriText(i, j);
    }

    public String getCdmaMin()
    {
        return this.mSST.getCdmaMin();
    }

    public String getCdmaPrlVersion()
    {
        return this.mSST.getPrlVersion();
    }

    public void getCellBroadcastSmsConfig(Message paramMessage)
    {
        Log.e("CDMA", "[CDMAPhone] getCellBroadcastSmsConfig() is obsolete; use SmsManager");
        paramMessage.sendToTarget();
    }

    public CellLocation getCellLocation()
    {
        return this.mSST.cellLoc;
    }

    public Phone.DataActivityState getDataActivityState()
    {
        Phone.DataActivityState localDataActivityState = Phone.DataActivityState.NONE;
        if (this.mSST.getCurrentDataConnectionState() == 0)
            switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$Activity[this.mDataConnectionTracker.getActivity().ordinal()])
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            }
        while (true)
        {
            return localDataActivityState;
            localDataActivityState = Phone.DataActivityState.DATAIN;
            continue;
            localDataActivityState = Phone.DataActivityState.DATAOUT;
            continue;
            localDataActivityState = Phone.DataActivityState.DATAINANDOUT;
            continue;
            localDataActivityState = Phone.DataActivityState.DORMANT;
        }
    }

    public void getDataCallList(Message paramMessage)
    {
        this.mCM.getDataCallList(paramMessage);
    }

    public Phone.DataState getDataConnectionState(String paramString)
    {
        Phone.DataState localDataState = Phone.DataState.DISCONNECTED;
        if (this.mSST == null)
            localDataState = Phone.DataState.DISCONNECTED;
        while (true)
        {
            log("getDataConnectionState apnType=" + paramString + " ret=" + localDataState);
            return localDataState;
            if (this.mSST.getCurrentDataConnectionState() != 0)
                localDataState = Phone.DataState.DISCONNECTED;
            else if ((!this.mDataConnectionTracker.isApnTypeEnabled(paramString)) || (!this.mDataConnectionTracker.isApnTypeActive(paramString)))
                localDataState = Phone.DataState.DISCONNECTED;
            else
                switch (2.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$State[this.mDataConnectionTracker.getState(paramString).ordinal()])
                {
                default:
                    break;
                case 1:
                case 2:
                    localDataState = Phone.DataState.DISCONNECTED;
                    break;
                case 3:
                case 4:
                    if ((this.mCT.state != Phone.State.IDLE) && (!this.mSST.isConcurrentVoiceAndDataAllowed()))
                        localDataState = Phone.DataState.SUSPENDED;
                    else
                        localDataState = Phone.DataState.CONNECTED;
                    break;
                case 5:
                case 6:
                case 7:
                    localDataState = Phone.DataState.CONNECTING;
                }
        }
    }

    public boolean getDataRoamingEnabled()
    {
        return this.mDataConnectionTracker.getDataOnRoamingEnabled();
    }

    public String getDeviceId()
    {
        String str = getMeid();
        if ((str == null) || (str.matches("^0*$")))
        {
            Log.d("CDMA", "getDeviceId(): MEID is not initialized use ESN");
            str = getEsn();
        }
        return str;
    }

    public String getDeviceSvn()
    {
        Log.d("CDMA", "getDeviceSvn(): return 0");
        return "0";
    }

    public void getEnhancedVoicePrivacy(Message paramMessage)
    {
        this.mCM.getPreferredVoicePrivacy(paramMessage);
    }

    public String getEsn()
    {
        return this.mEsn;
    }

    public CdmaCall getForegroundCall()
    {
        return this.mCT.foregroundCall;
    }

    public IccPhoneBookInterfaceManager getIccPhoneBookInterfaceManager()
    {
        return this.mRuimPhoneBookInterfaceManager;
    }

    public IccSmsInterfaceManager getIccSmsInterfaceManager()
    {
        return this.mRuimSmsInterfaceManager;
    }

    public String getImei()
    {
        Log.e("CDMA", "IMEI is not available in CDMA");
        return null;
    }

    public String getLine1AlphaTag()
    {
        Log.e("CDMA", "getLine1AlphaTag: not possible in CDMA");
        return null;
    }

    public String getLine1Number()
    {
        return this.mSST.getMdnNumber();
    }

    public String getMeid()
    {
        return this.mMeid;
    }

    public boolean getMessageWaitingIndicator()
    {
        if (getVoiceMessageCount() > 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean getMute()
    {
        return this.mCT.getMute();
    }

    public void getNeighboringCids(Message paramMessage)
    {
        if (paramMessage != null)
        {
            CommandException localCommandException = new CommandException(CommandException.Error.REQUEST_NOT_SUPPORTED);
            AsyncResult.forMessage(paramMessage).exception = localCommandException;
            paramMessage.sendToTarget();
        }
    }

    public void getOutgoingCallerIdDisplay(Message paramMessage)
    {
        Log.e("CDMA", "getOutgoingCallerIdDisplay: not possible in CDMA");
    }

    public List<? extends MmiCode> getPendingMmiCodes()
    {
        return this.mPendingMmis;
    }

    public String getPhoneName()
    {
        return "CDMA";
    }

    public PhoneSubInfo getPhoneSubInfo()
    {
        return this.mSubInfo;
    }

    public int getPhoneType()
    {
        return 2;
    }

    public CdmaCall getRingingCall()
    {
        return this.mCT.ringingCall;
    }

    public ServiceState getServiceState()
    {
        return this.mSST.ss;
    }

    public ServiceStateTracker getServiceStateTracker()
    {
        return this.mSST;
    }

    public SignalStrength getSignalStrength()
    {
        return this.mSST.mSignalStrength;
    }

    public Phone.State getState()
    {
        return this.mCT.state;
    }

    public String getSubscriberId()
    {
        return this.mSST.getImsi();
    }

    public String getVoiceMailAlphaTag()
    {
        String str = "";
        if ((str == null) || (str.length() == 0))
            str = this.mContext.getText(17039364).toString();
        return str;
    }

    public String getVoiceMailNumber()
    {
        SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (getContext().getResources().getBoolean(17891375));
        for (String str = localSharedPreferences.getString("vm_number_key_cdma", getLine1Number()); ; str = localSharedPreferences.getString("vm_number_key_cdma", "*86"))
            return str;
    }

    public int getVoiceMessageCount()
    {
        int i = this.mIccRecords.getVoiceMessageCount();
        if (i == 0)
            i = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("vm_count_key_cdma", 0);
        return i;
    }

    public boolean handleInCallMmiCommands(String paramString)
    {
        Log.e("CDMA", "method handleInCallMmiCommands is NOT supported in CDMA!");
        return false;
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        case 3:
        case 4:
        case 7:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 24:
        case 28:
        case 29:
        default:
            super.handleMessage(paramMessage);
        case 1:
        case 6:
        case 21:
        case 25:
        case 30:
        case 26:
        case 22:
        case 8:
        case 5:
        case 27:
        case 2:
        case 19:
        case 23:
        case 20:
        }
        while (true)
        {
            return;
            this.mCM.getBasebandVersion(obtainMessage(6));
            this.mCM.getDeviceIdentity(obtainMessage(21));
            continue;
            AsyncResult localAsyncResult3 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult3.exception == null)
            {
                Log.d("CDMA", "Baseband version: " + localAsyncResult3.result);
                setSystemProperty("gsm.version.baseband", (String)localAsyncResult3.result);
                continue;
                AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
                if (localAsyncResult2.exception == null)
                {
                    String[] arrayOfString = (String[])localAsyncResult2.result;
                    this.mImei = arrayOfString[0];
                    this.mImeiSv = arrayOfString[1];
                    this.mEsn = arrayOfString[2];
                    this.mMeid = arrayOfString[3];
                    continue;
                    handleEnterEmergencyCallbackMode(paramMessage);
                    continue;
                    processIccRecordEvents(((Integer)((AsyncResult)paramMessage.obj).result).intValue());
                    continue;
                    handleExitEmergencyCallbackMode(paramMessage);
                    continue;
                    Log.d("CDMA", "Event EVENT_RUIM_RECORDS_LOADED Received");
                    updateCurrentCarrierInProvider();
                    continue;
                    Log.d("CDMA", "Event EVENT_RADIO_OFF_OR_NOT_AVAILABLE Received");
                    continue;
                    Log.d("CDMA", "Event EVENT_RADIO_ON Received");
                    handleCdmaSubscriptionSource(this.mCdmaSSM.getCdmaSubscriptionSource());
                    continue;
                    Log.d("CDMA", "EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED");
                    handleCdmaSubscriptionSource(this.mCdmaSSM.getCdmaSubscriptionSource());
                    continue;
                    Log.d("CDMA", "Event EVENT_SSN Received");
                    continue;
                    Log.d("CDMA", "Event EVENT_REGISTERED_TO_NETWORK Received");
                    continue;
                    Log.d("CDMA", "Event EVENT_NV_READY Received");
                    prepareEri();
                    continue;
                    AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
                    if (IccException.class.isInstance(localAsyncResult1.exception))
                    {
                        storeVoiceMailNumber(this.mVmNumber);
                        localAsyncResult1.exception = null;
                    }
                    Message localMessage = (Message)localAsyncResult1.userObj;
                    if (localMessage != null)
                    {
                        AsyncResult.forMessage(localMessage, localAsyncResult1.result, localAsyncResult1.exception);
                        localMessage.sendToTarget();
                    }
                }
            }
        }
    }

    public boolean handlePinMmi(String paramString)
    {
        boolean bool = false;
        CdmaMmiCode localCdmaMmiCode = CdmaMmiCode.newFromDialString(paramString, this);
        if (localCdmaMmiCode == null)
            Log.e("CDMA", "Mmi is NULL!");
        while (true)
        {
            return bool;
            if (localCdmaMmiCode.isPukCommand())
            {
                this.mPendingMmis.add(localCdmaMmiCode);
                this.mMmiRegistrants.notifyRegistrants(new AsyncResult(null, localCdmaMmiCode, null));
                localCdmaMmiCode.processCode();
                bool = true;
            }
            else
            {
                Log.e("CDMA", "Unrecognized mmi!");
            }
        }
    }

    void handleTimerInEmergencyCallbackMode(int paramInt)
    {
        switch (paramInt)
        {
        default:
            Log.e("CDMA", "handleTimerInEmergencyCallbackMode, unsupported action " + paramInt);
        case 1:
        case 0:
        }
        while (true)
        {
            return;
            removeCallbacks(this.mExitEcmRunnable);
            this.mEcmTimerResetRegistrants.notifyResult(Boolean.TRUE);
            continue;
            long l = SystemProperties.getLong("ro.cdma.ecmexittimer", 300000L);
            postDelayed(this.mExitEcmRunnable, l);
            this.mEcmTimerResetRegistrants.notifyResult(Boolean.FALSE);
        }
    }

    protected void init(Context paramContext, PhoneNotifier paramPhoneNotifier)
    {
        this.mCM.setPhoneType(2);
        this.mCT = new CdmaCallTracker(this);
        this.mCdmaSSM = CdmaSubscriptionSourceManager.getInstance(paramContext, this.mCM, this, 27, null);
        this.mSMS = new CdmaSMSDispatcher(this, this.mSmsStorageMonitor, this.mSmsUsageMonitor);
        this.mDataConnectionTracker = new CdmaDataConnectionTracker(this);
        this.mRuimPhoneBookInterfaceManager = new RuimPhoneBookInterfaceManager(this);
        this.mRuimSmsInterfaceManager = new RuimSmsInterfaceManager(this, this.mSMS);
        this.mSubInfo = new PhoneSubInfo(this);
        this.mEriManager = new EriManager(this, paramContext, 0);
        this.mCM.registerForAvailable(this, 1, null);
        registerForRuimRecordEvents();
        this.mCM.registerForOffOrNotAvailable(this, 8, null);
        this.mCM.registerForOn(this, 5, null);
        this.mCM.setOnSuppServiceNotification(this, 2, null);
        this.mSST.registerForNetworkAttached(this, 19, null);
        this.mCM.setEmergencyCallbackMode(this, 25, null);
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "CDMA");
        SystemProperties.set("gsm.current.phone-type", Integer.toString(2));
        this.mIsPhoneInEcmState = SystemProperties.get("ril.cdma.inecmmode", "false").equals("true");
        if (this.mIsPhoneInEcmState)
            this.mCM.exitEmergencyCallbackMode(obtainMessage(26));
        this.mCarrierOtaSpNumSchema = SystemProperties.get("ro.cdma.otaspnumschema", "");
        setSystemProperty("gsm.sim.operator.alpha", SystemProperties.get("ro.cdma.home.operator.alpha"));
        String str = SystemProperties.get(PROPERTY_CDMA_HOME_OPERATOR_NUMERIC);
        log("CDMAPhone: init set 'gsm.sim.operator.numeric' to operator='" + str + "'");
        setSystemProperty("gsm.sim.operator.numeric", str);
        setIsoCountryProperty(str);
        updateCurrentCarrierInProvider(str);
        paramPhoneNotifier.notifyMessageWaitingChanged(this);
    }

    protected void initSstIcc()
    {
        this.mIccCard.set(UiccController.getInstance(this).getIccCard());
        this.mIccRecords = ((IccCard)this.mIccCard.get()).getIccRecords();
        this.mSST = new CdmaServiceStateTracker(this);
    }

    public boolean isEriFileLoaded()
    {
        return this.mEriManager.isEriFileLoaded();
    }

    boolean isInCall()
    {
        Call.State localState1 = getForegroundCall().getState();
        Call.State localState2 = getBackgroundCall().getState();
        Call.State localState3 = getRingingCall().getState();
        if ((localState1.isAlive()) || (localState2.isAlive()) || (localState3.isAlive()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isInEcm()
    {
        return this.mIsPhoneInEcmState;
    }

    public boolean isInEmergencyCall()
    {
        return this.mCT.isInEmergencyCall();
    }

    public boolean isMinInfoReady()
    {
        return this.mSST.isMinInfoReady();
    }

    public boolean isOtaSpNumber(String paramString)
    {
        boolean bool = false;
        String str = PhoneNumberUtils.extractNetworkPortionAlt(paramString);
        if (str != null)
        {
            bool = isIs683OtaSpDialStr(str);
            if (!bool)
                bool = isCarrierOtaSpNum(str);
        }
        Log.d("CDMA", "isOtaSpNumber " + bool);
        return bool;
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[CDMAPhone] " + paramString);
    }

    public boolean needsOtaServiceProvisioning()
    {
        if (this.mSST.getOtasp() != 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void notifyDisconnect(Connection paramConnection)
    {
        this.mDisconnectRegistrants.notifyResult(paramConnection);
    }

    void notifyLocationChanged()
    {
        this.mNotifier.notifyCellLocation(this);
    }

    void notifyNewRingingConnection(Connection paramConnection)
    {
        super.notifyNewRingingConnectionP(paramConnection);
    }

    void notifyPhoneStateChanged()
    {
        this.mNotifier.notifyPhoneState(this);
    }

    void notifyPreciseCallStateChanged()
    {
        super.notifyPreciseCallStateChangedP();
    }

    void notifyServiceStateChanged(ServiceState paramServiceState)
    {
        super.notifyServiceStateChangedP(paramServiceState);
    }

    void notifySignalStrength()
    {
        this.mNotifier.notifySignalStrength(this);
    }

    void notifyUnknownConnection()
    {
        this.mUnknownConnectionRegistrants.notifyResult(this);
    }

    void onMMIDone(CdmaMmiCode paramCdmaMmiCode)
    {
        if (this.mPendingMmis.remove(paramCdmaMmiCode))
            this.mMmiCompleteRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaMmiCode, null));
    }

    public void prepareEri()
    {
        this.mEriManager.loadEriFile();
        if (this.mEriManager.isEriFileLoaded())
        {
            log("ERI read, notify registrants");
            this.mEriFileLoadedRegistrants.notifyRegistrants();
        }
    }

    public void registerForCallWaiting(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCT.registerForCallWaiting(paramHandler, paramInt, paramObject);
    }

    public void registerForCdmaOtaStatusChange(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mCM.registerForCdmaOtaProvision(paramHandler, paramInt, paramObject);
    }

    public void registerForEcmTimerReset(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mEcmTimerResetRegistrants.addUnique(paramHandler, paramInt, paramObject);
    }

    public void registerForEriFileLoaded(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.mEriFileLoadedRegistrants.add(localRegistrant);
    }

    public void registerForSubscriptionInfoReady(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mSST.registerForSubscriptionInfoReady(paramHandler, paramInt, paramObject);
    }

    public void registerForSuppServiceNotification(Handler paramHandler, int paramInt, Object paramObject)
    {
        Log.e("CDMA", "method registerForSuppServiceNotification is NOT supported in CDMA!");
    }

    public void rejectCall()
        throws CallStateException
    {
        this.mCT.rejectCall();
    }

    public void removeReferences()
    {
        log("removeReferences");
        this.mRuimPhoneBookInterfaceManager = null;
        this.mRuimSmsInterfaceManager = null;
        this.mSubInfo = null;
        this.mCT = null;
        this.mSST = null;
        this.mEriManager = null;
        this.mExitEcmRunnable = null;
        super.removeReferences();
    }

    public void selectNetworkManually(OperatorInfo paramOperatorInfo, Message paramMessage)
    {
        Log.e("CDMA", "selectNetworkManually: not possible in CDMA");
    }

    public void sendBurstDtmf(String paramString, int paramInt1, int paramInt2, Message paramMessage)
    {
        int i = 1;
        for (int j = 0; ; j++)
            if (j < paramString.length())
            {
                if (!PhoneNumberUtils.is12Key(paramString.charAt(j)))
                {
                    Log.e("CDMA", "sendDtmf called with invalid character '" + paramString.charAt(j) + "'");
                    i = 0;
                }
            }
            else
            {
                if ((this.mCT.state == Phone.State.OFFHOOK) && (i != 0))
                    this.mCM.sendBurstDtmf(paramString, paramInt1, paramInt2, paramMessage);
                return;
            }
    }

    public void sendDtmf(char paramChar)
    {
        if (!PhoneNumberUtils.is12Key(paramChar))
            Log.e("CDMA", "sendDtmf called with invalid character '" + paramChar + "'");
        while (true)
        {
            return;
            if (this.mCT.state == Phone.State.OFFHOOK)
                this.mCM.sendDtmf(paramChar, null);
        }
    }

    void sendEmergencyCallbackModeChange()
    {
        Intent localIntent = new Intent("android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED");
        localIntent.putExtra("phoneinECMState", this.mIsPhoneInEcmState);
        ActivityManagerNative.broadcastStickyIntent(localIntent, null);
        Log.d("CDMA", "sendEmergencyCallbackModeChange");
    }

    public void sendUssdResponse(String paramString)
    {
        Log.e("CDMA", "sendUssdResponse: not possible in CDMA");
    }

    public void setCallForwardingOption(int paramInt1, int paramInt2, String paramString, int paramInt3, Message paramMessage)
    {
        Log.e("CDMA", "setCallForwardingOption: not possible in CDMA");
    }

    public void setCallWaiting(boolean paramBoolean, Message paramMessage)
    {
        Log.e("CDMA", "method setCallWaiting is NOT supported in CDMA!");
    }

    public void setCellBroadcastSmsConfig(int[] paramArrayOfInt, Message paramMessage)
    {
        Log.e("CDMA", "[CDMAPhone] setCellBroadcastSmsConfig() is obsolete; use SmsManager");
        paramMessage.sendToTarget();
    }

    public void setDataRoamingEnabled(boolean paramBoolean)
    {
        this.mDataConnectionTracker.setDataOnRoamingEnabled(paramBoolean);
    }

    public void setLine1Number(String paramString1, String paramString2, Message paramMessage)
    {
        Log.e("CDMA", "setLine1Number: not possible in CDMA");
    }

    public void setMute(boolean paramBoolean)
    {
        this.mCT.setMute(paramBoolean);
    }

    public void setNetworkSelectionModeAutomatic(Message paramMessage)
    {
        Log.e("CDMA", "method setNetworkSelectionModeAutomatic is NOT supported in CDMA!");
    }

    public void setOnEcbModeExitResponse(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mEcmExitRespRegistrant = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOnPostDialCharacter(Handler paramHandler, int paramInt, Object paramObject)
    {
        this.mPostDialHandler = new Registrant(paramHandler, paramInt, paramObject);
    }

    public void setOutgoingCallerIdDisplay(int paramInt, Message paramMessage)
    {
        Log.e("CDMA", "setOutgoingCallerIdDisplay: not possible in CDMA");
    }

    public void setRadioPower(boolean paramBoolean)
    {
        this.mSST.setRadioPower(paramBoolean);
    }

    public final void setSystemProperty(String paramString1, String paramString2)
    {
        super.setSystemProperty(paramString1, paramString2);
    }

    public void setVoiceMailNumber(String paramString1, String paramString2, Message paramMessage)
    {
        this.mVmNumber = paramString2;
        Message localMessage = obtainMessage(20, 0, 0, paramMessage);
        this.mIccRecords.setVoiceMailNumber(paramString1, this.mVmNumber, localMessage);
    }

    public void startDtmf(char paramChar)
    {
        if (!PhoneNumberUtils.is12Key(paramChar))
            Log.e("CDMA", "startDtmf called with invalid character '" + paramChar + "'");
        while (true)
        {
            return;
            this.mCM.startDtmf(paramChar, null);
        }
    }

    public void stopDtmf()
    {
        this.mCM.stopDtmf(null);
    }

    public void switchHoldingAndActive()
        throws CallStateException
    {
        this.mCT.switchWaitingOrHoldingAndActive();
    }

    public void unregisterForCallWaiting(Handler paramHandler)
    {
        this.mCT.unregisterForCallWaiting(paramHandler);
    }

    public void unregisterForCdmaOtaStatusChange(Handler paramHandler)
    {
        this.mCM.unregisterForCdmaOtaProvision(paramHandler);
    }

    public void unregisterForEcmTimerReset(Handler paramHandler)
    {
        this.mEcmTimerResetRegistrants.remove(paramHandler);
    }

    public void unregisterForEriFileLoaded(Handler paramHandler)
    {
        this.mEriFileLoadedRegistrants.remove(paramHandler);
    }

    public void unregisterForSubscriptionInfoReady(Handler paramHandler)
    {
        this.mSST.unregisterForSubscriptionInfoReady(paramHandler);
    }

    public void unregisterForSuppServiceNotification(Handler paramHandler)
    {
        Log.e("CDMA", "method unregisterForSuppServiceNotification is NOT supported in CDMA!");
    }

    public void unsetOnEcbModeExitResponse(Handler paramHandler)
    {
        this.mEcmExitRespRegistrant.clear();
    }

    boolean updateCurrentCarrierInProvider()
    {
        return true;
    }

    boolean updateCurrentCarrierInProvider(String paramString)
    {
        if (!TextUtils.isEmpty(paramString));
        while (true)
        {
            try
            {
                Uri localUri = Uri.withAppendedPath(Telephony.Carriers.CONTENT_URI, "current");
                ContentValues localContentValues = new ContentValues();
                localContentValues.put("numeric", paramString);
                log("updateCurrentCarrierInProvider from system: numeric=" + paramString);
                getContext().getContentResolver().insert(localUri, localContentValues);
                MccTable.updateMccMncConfiguration(this.mContext, paramString);
                bool = true;
                return bool;
            }
            catch (SQLException localSQLException)
            {
                Log.e("CDMA", "Can't store current operator", localSQLException);
            }
            boolean bool = false;
        }
    }

    public void updateServiceLocation()
    {
        this.mSST.enableSingleLocationUpdate();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CDMAPhone
 * JD-Core Version:        0.6.2
 */