package com.android.internal.telephony.cdma;

import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.ApnSetting;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.DataConnection;
import com.android.internal.telephony.DataConnection.ConnectionParams;
import com.android.internal.telephony.DataConnection.FailCause;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.RetryManager;
import com.android.internal.util.IState;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class CdmaDataConnection extends DataConnection
{
    private static final String LOG_TAG = "CDMA";

    private CdmaDataConnection(CDMAPhone paramCDMAPhone, String paramString, int paramInt, RetryManager paramRetryManager, DataConnectionTracker paramDataConnectionTracker)
    {
        super(paramCDMAPhone, paramString, paramInt, paramRetryManager, paramDataConnectionTracker);
    }

    static CdmaDataConnection makeDataConnection(CDMAPhone paramCDMAPhone, int paramInt, RetryManager paramRetryManager, DataConnectionTracker paramDataConnectionTracker)
    {
        synchronized (mCountLock)
        {
            mCount = 1 + mCount;
            CdmaDataConnection localCdmaDataConnection = new CdmaDataConnection(paramCDMAPhone, "CdmaDC-" + mCount, paramInt, paramRetryManager, paramDataConnectionTracker);
            localCdmaDataConnection.start();
            localCdmaDataConnection.log("Made " + localCdmaDataConnection.getName());
            return localCdmaDataConnection;
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("CdmaDataConnection extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }

    protected boolean isDnsOk(String[] paramArrayOfString)
    {
        boolean bool = false;
        if (("0.0.0.0".equals(paramArrayOfString[0])) && ("0.0.0.0".equals(paramArrayOfString[1])) && (!this.phone.isDnsCheckDisabled()));
        while (true)
        {
            return bool;
            bool = true;
        }
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[" + getName() + "] " + paramString);
    }

    protected void onConnect(DataConnection.ConnectionParams paramConnectionParams)
    {
        log("CdmaDataConnection Connecting...");
        this.mApn = paramConnectionParams.apn;
        this.createTime = -1L;
        this.lastFailTime = -1L;
        this.lastFailCause = DataConnection.FailCause.NONE;
        if ((paramConnectionParams.apn != null) && (paramConnectionParams.apn.types.length > 0) && (paramConnectionParams.apn.types[0] != null) && (paramConnectionParams.apn.types[0].equals("dun")))
            log("CdmaDataConnection using DUN");
        for (int i = 1; ; i = 0)
        {
            Message localMessage = obtainMessage(262145, paramConnectionParams);
            localMessage.obj = paramConnectionParams;
            this.phone.mCM.setupDataCall(Integer.toString(getRilRadioTechnology(0)), Integer.toString(i), null, null, null, Integer.toString(3), "IP", localMessage);
            return;
        }
    }

    public String toString()
    {
        return "State=" + getCurrentState().getName() + " create=" + this.createTime + " lastFail=" + this.lastFailTime + " lastFasilCause=" + this.lastFailCause;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaDataConnection
 * JD-Core Version:        0.6.2
 */