package com.android.internal.telephony.cdma.sms;

import android.util.SparseBooleanArray;
import com.android.internal.telephony.SmsAddress;
import com.android.internal.util.HexDump;

public class CdmaSmsAddress extends SmsAddress
{
    public static final int DIGIT_MODE_4BIT_DTMF = 0;
    public static final int DIGIT_MODE_8BIT_CHAR = 1;
    public static final int NUMBERING_PLAN_ISDN_TELEPHONY = 1;
    public static final int NUMBERING_PLAN_UNKNOWN = 0;
    public static final int NUMBER_MODE_DATA_NETWORK = 1;
    public static final int NUMBER_MODE_NOT_DATA_NETWORK = 0;
    public static final int SMS_ADDRESS_MAX = 36;
    public static final int SMS_SUBADDRESS_MAX = 36;
    public static final int TON_ABBREVIATED = 6;
    public static final int TON_ALPHANUMERIC = 5;
    public static final int TON_INTERNATIONAL_OR_IP = 1;
    public static final int TON_NATIONAL_OR_EMAIL = 2;
    public static final int TON_NETWORK = 3;
    public static final int TON_RESERVED = 7;
    public static final int TON_SUBSCRIBER = 4;
    public static final int TON_UNKNOWN;
    private static final SparseBooleanArray numericCharDialableMap;
    private static final char[] numericCharsDialable;
    private static final char[] numericCharsSugar;
    public int digitMode;
    public int numberMode;
    public int numberOfDigits;
    public int numberPlan;

    static
    {
        char[] arrayOfChar1 = new char[12];
        arrayOfChar1[0] = 48;
        arrayOfChar1[1] = 49;
        arrayOfChar1[2] = 50;
        arrayOfChar1[3] = 51;
        arrayOfChar1[4] = 52;
        arrayOfChar1[5] = 53;
        arrayOfChar1[6] = 54;
        arrayOfChar1[7] = 55;
        arrayOfChar1[8] = 56;
        arrayOfChar1[9] = 57;
        arrayOfChar1[10] = 42;
        arrayOfChar1[11] = 35;
        numericCharsDialable = arrayOfChar1;
        char[] arrayOfChar2 = new char[8];
        arrayOfChar2[0] = 40;
        arrayOfChar2[1] = 41;
        arrayOfChar2[2] = 32;
        arrayOfChar2[3] = 45;
        arrayOfChar2[4] = 43;
        arrayOfChar2[5] = 46;
        arrayOfChar2[6] = 47;
        arrayOfChar2[7] = 92;
        numericCharsSugar = arrayOfChar2;
        numericCharDialableMap = new SparseBooleanArray(numericCharsDialable.length + numericCharsSugar.length);
        for (int i = 0; i < numericCharsDialable.length; i++)
            numericCharDialableMap.put(numericCharsDialable[i], true);
        for (int j = 0; j < numericCharsSugar.length; j++)
            numericCharDialableMap.put(numericCharsSugar[j], false);
    }

    private static String filterNumericSugar(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = paramString.length();
        int j = 0;
        char c;
        int k;
        if (j < i)
        {
            c = paramString.charAt(j);
            k = numericCharDialableMap.indexOfKey(c);
            if (k >= 0);
        }
        for (String str = null; ; str = localStringBuilder.toString())
        {
            return str;
            if (!numericCharDialableMap.valueAt(k));
            while (true)
            {
                j++;
                break;
                localStringBuilder.append(c);
            }
        }
    }

    private static String filterWhitespace(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        int i = paramString.length();
        int j = 0;
        if (j < i)
        {
            char c = paramString.charAt(j);
            if ((c == ' ') || (c == '\r') || (c == '\n') || (c == '\t'));
            while (true)
            {
                j++;
                break;
                localStringBuilder.append(c);
            }
        }
        return localStringBuilder.toString();
    }

    public static CdmaSmsAddress parse(String paramString)
    {
        CdmaSmsAddress localCdmaSmsAddress = new CdmaSmsAddress();
        localCdmaSmsAddress.address = paramString;
        localCdmaSmsAddress.ton = 0;
        byte[] arrayOfByte = null;
        String str = filterNumericSugar(paramString);
        if (str != null)
            arrayOfByte = parseToDtmf(str);
        if (arrayOfByte != null)
        {
            localCdmaSmsAddress.digitMode = 0;
            localCdmaSmsAddress.numberMode = 0;
            if (paramString.indexOf('+') != -1)
                localCdmaSmsAddress.ton = 1;
        }
        while (true)
        {
            localCdmaSmsAddress.origBytes = arrayOfByte;
            localCdmaSmsAddress.numberOfDigits = arrayOfByte.length;
            while (true)
            {
                return localCdmaSmsAddress;
                arrayOfByte = UserData.stringToAscii(filterWhitespace(paramString));
                if (arrayOfByte != null)
                    break;
                localCdmaSmsAddress = null;
            }
            localCdmaSmsAddress.digitMode = 1;
            localCdmaSmsAddress.numberMode = 1;
            if (paramString.indexOf('@') != -1)
                localCdmaSmsAddress.ton = 2;
        }
    }

    private static byte[] parseToDtmf(String paramString)
    {
        int i = paramString.length();
        byte[] arrayOfByte = new byte[i];
        int j = 0;
        if (j < i)
        {
            int k = paramString.charAt(j);
            int m;
            if ((k >= 49) && (k <= 57))
                m = k + -48;
            while (true)
            {
                arrayOfByte[j] = ((byte)m);
                j++;
                break;
                if (k == 48)
                {
                    m = 10;
                }
                else if (k == 42)
                {
                    m = 11;
                }
                else
                {
                    if (k != 35)
                        break label98;
                    m = 12;
                }
            }
            label98: arrayOfByte = null;
        }
        return arrayOfByte;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("CdmaSmsAddress ");
        localStringBuilder.append("{ digitMode=" + this.digitMode);
        localStringBuilder.append(", numberMode=" + this.numberMode);
        localStringBuilder.append(", numberPlan=" + this.numberPlan);
        localStringBuilder.append(", numberOfDigits=" + this.numberOfDigits);
        localStringBuilder.append(", ton=" + this.ton);
        localStringBuilder.append(", address=\"" + this.address + "\"");
        localStringBuilder.append(", origBytes=" + HexDump.toHexString(this.origBytes));
        localStringBuilder.append(" }");
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.sms.CdmaSmsAddress
 * JD-Core Version:        0.6.2
 */