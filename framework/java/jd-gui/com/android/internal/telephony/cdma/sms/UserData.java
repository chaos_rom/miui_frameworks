package com.android.internal.telephony.cdma.sms;

import android.util.SparseIntArray;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.util.HexDump;

public class UserData
{
    public static final int ASCII_CR_INDEX = 13;
    public static final char[] ASCII_MAP;
    public static final int ASCII_MAP_BASE_INDEX = 32;
    public static final int ASCII_MAP_MAX_INDEX = 0;
    public static final int ASCII_NL_INDEX = 10;
    public static final int ENCODING_7BIT_ASCII = 2;
    public static final int ENCODING_GSM_7BIT_ALPHABET = 9;
    public static final int ENCODING_GSM_DCS = 10;
    public static final int ENCODING_IA5 = 3;
    public static final int ENCODING_IS91_EXTENDED_PROTOCOL = 1;
    public static final int ENCODING_KOREAN = 6;
    public static final int ENCODING_LATIN = 8;
    public static final int ENCODING_LATIN_HEBREW = 7;
    public static final int ENCODING_OCTET = 0;
    public static final int ENCODING_SHIFT_JIS = 5;
    public static final int ENCODING_UNICODE_16 = 4;
    public static final int IS91_MSG_TYPE_CLI = 132;
    public static final int IS91_MSG_TYPE_SHORT_MESSAGE = 133;
    public static final int IS91_MSG_TYPE_SHORT_MESSAGE_FULL = 131;
    public static final int IS91_MSG_TYPE_VOICEMAIL_STATUS = 130;
    public static final int PRINTABLE_ASCII_MIN_INDEX = 32;
    static final byte UNENCODABLE_7_BIT_CHAR = 32;
    public static final SparseIntArray charToAscii;
    public int msgEncoding;
    public boolean msgEncodingSet = false;
    public int msgType;
    public int numFields;
    public int paddingBits;
    public byte[] payload;
    public String payloadStr;
    public SmsHeader userDataHeader;

    static
    {
        char[] arrayOfChar = new char[95];
        arrayOfChar[0] = 32;
        arrayOfChar[1] = 33;
        arrayOfChar[2] = 34;
        arrayOfChar[3] = 35;
        arrayOfChar[4] = 36;
        arrayOfChar[5] = 37;
        arrayOfChar[6] = 38;
        arrayOfChar[7] = 39;
        arrayOfChar[8] = 40;
        arrayOfChar[9] = 41;
        arrayOfChar[10] = 42;
        arrayOfChar[11] = 43;
        arrayOfChar[12] = 44;
        arrayOfChar[13] = 45;
        arrayOfChar[14] = 46;
        arrayOfChar[15] = 47;
        arrayOfChar[16] = 48;
        arrayOfChar[17] = 49;
        arrayOfChar[18] = 50;
        arrayOfChar[19] = 51;
        arrayOfChar[20] = 52;
        arrayOfChar[21] = 53;
        arrayOfChar[22] = 54;
        arrayOfChar[23] = 55;
        arrayOfChar[24] = 56;
        arrayOfChar[25] = 57;
        arrayOfChar[26] = 58;
        arrayOfChar[27] = 59;
        arrayOfChar[28] = 60;
        arrayOfChar[29] = 61;
        arrayOfChar[30] = 62;
        arrayOfChar[31] = 63;
        arrayOfChar[32] = 64;
        arrayOfChar[33] = 65;
        arrayOfChar[34] = 66;
        arrayOfChar[35] = 67;
        arrayOfChar[36] = 68;
        arrayOfChar[37] = 69;
        arrayOfChar[38] = 70;
        arrayOfChar[39] = 71;
        arrayOfChar[40] = 72;
        arrayOfChar[41] = 73;
        arrayOfChar[42] = 74;
        arrayOfChar[43] = 75;
        arrayOfChar[44] = 76;
        arrayOfChar[45] = 77;
        arrayOfChar[46] = 78;
        arrayOfChar[47] = 79;
        arrayOfChar[48] = 80;
        arrayOfChar[49] = 81;
        arrayOfChar[50] = 82;
        arrayOfChar[51] = 83;
        arrayOfChar[52] = 84;
        arrayOfChar[53] = 85;
        arrayOfChar[54] = 86;
        arrayOfChar[55] = 87;
        arrayOfChar[56] = 88;
        arrayOfChar[57] = 89;
        arrayOfChar[58] = 90;
        arrayOfChar[59] = 91;
        arrayOfChar[60] = 92;
        arrayOfChar[61] = 93;
        arrayOfChar[62] = 94;
        arrayOfChar[63] = 95;
        arrayOfChar[64] = 96;
        arrayOfChar[65] = 97;
        arrayOfChar[66] = 98;
        arrayOfChar[67] = 99;
        arrayOfChar[68] = 100;
        arrayOfChar[69] = 101;
        arrayOfChar[70] = 102;
        arrayOfChar[71] = 103;
        arrayOfChar[72] = 104;
        arrayOfChar[73] = 105;
        arrayOfChar[74] = 106;
        arrayOfChar[75] = 107;
        arrayOfChar[76] = 108;
        arrayOfChar[77] = 109;
        arrayOfChar[78] = 110;
        arrayOfChar[79] = 111;
        arrayOfChar[80] = 112;
        arrayOfChar[81] = 113;
        arrayOfChar[82] = 114;
        arrayOfChar[83] = 115;
        arrayOfChar[84] = 116;
        arrayOfChar[85] = 117;
        arrayOfChar[86] = 118;
        arrayOfChar[87] = 119;
        arrayOfChar[88] = 120;
        arrayOfChar[89] = 121;
        arrayOfChar[90] = 122;
        arrayOfChar[91] = 123;
        arrayOfChar[92] = 124;
        arrayOfChar[93] = 125;
        arrayOfChar[94] = 126;
        ASCII_MAP = arrayOfChar;
        charToAscii = new SparseIntArray();
        for (int i = 0; i < ASCII_MAP.length; i++)
            charToAscii.put(ASCII_MAP[i], i + 32);
        charToAscii.put(10, 10);
        charToAscii.put(13, 13);
    }

    public static byte[] stringToAscii(String paramString)
    {
        int i = paramString.length();
        byte[] arrayOfByte = new byte[i];
        for (int j = 0; ; j++)
        {
            int k;
            if (j < i)
            {
                k = charToAscii.get(paramString.charAt(j), -1);
                if (k == -1)
                    arrayOfByte = null;
            }
            else
            {
                return arrayOfByte;
            }
            arrayOfByte[j] = ((byte)k);
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder1 = new StringBuilder();
        localStringBuilder1.append("UserData ");
        StringBuilder localStringBuilder2 = new StringBuilder().append("{ msgEncoding=");
        if (this.msgEncodingSet);
        for (Object localObject = Integer.valueOf(this.msgEncoding); ; localObject = "unset")
        {
            localStringBuilder1.append(localObject);
            localStringBuilder1.append(", msgType=" + this.msgType);
            localStringBuilder1.append(", paddingBits=" + this.paddingBits);
            localStringBuilder1.append(", numFields=" + this.numFields);
            localStringBuilder1.append(", userDataHeader=" + this.userDataHeader);
            localStringBuilder1.append(", payload='" + HexDump.toHexString(this.payload) + "'");
            localStringBuilder1.append(", payloadStr='" + this.payloadStr + "'");
            localStringBuilder1.append(" }");
            return localStringBuilder1.toString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.sms.UserData
 * JD-Core Version:        0.6.2
 */