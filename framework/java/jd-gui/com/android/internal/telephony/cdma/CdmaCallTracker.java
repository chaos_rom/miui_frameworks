package com.android.internal.telephony.cdma;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.telephony.PhoneNumberUtils;
import android.telephony.ServiceState;
import android.util.Log;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CallTracker;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.Connection.DisconnectCause;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.DriverCall;
import com.android.internal.telephony.DriverCall.State;
import com.android.internal.telephony.Phone.State;
import com.android.internal.telephony.Phone.SuppService;
import com.android.internal.telephony.PhoneBase;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public final class CdmaCallTracker extends CallTracker
{
    private static final boolean DBG_POLL = false;
    static final String LOG_TAG = "CDMA";
    static final int MAX_CONNECTIONS = 1;
    static final int MAX_CONNECTIONS_PER_CALL = 1;
    private static final boolean REPEAT_POLLING;
    CdmaCall backgroundCall = new CdmaCall(this);
    RegistrantList callWaitingRegistrants = new RegistrantList();
    CdmaConnection[] connections = new CdmaConnection[1];
    boolean desiredMute = false;
    ArrayList<CdmaConnection> droppedDuringPoll = new ArrayList(1);
    CdmaCall foregroundCall = new CdmaCall(this);
    boolean hangupPendingMO;
    private boolean mIsEcmTimerCanceled = false;
    boolean mIsInEmergencyCall = false;
    int pendingCallClirMode;
    boolean pendingCallInEcm = false;
    CdmaConnection pendingMO;
    CDMAPhone phone;
    CdmaCall ringingCall = new CdmaCall(this);
    Phone.State state = Phone.State.IDLE;
    RegistrantList voiceCallEndedRegistrants = new RegistrantList();
    RegistrantList voiceCallStartedRegistrants = new RegistrantList();

    CdmaCallTracker(CDMAPhone paramCDMAPhone)
    {
        this.phone = paramCDMAPhone;
        this.cm = paramCDMAPhone.mCM;
        this.cm.registerForCallStateChanged(this, 2, null);
        this.cm.registerForOn(this, 9, null);
        this.cm.registerForNotAvailable(this, 10, null);
        this.cm.registerForCallWaitingInfo(this, 15, null);
        this.foregroundCall.setGeneric(false);
    }

    private void checkAndEnableDataCallAfterEmergencyCallDropped()
    {
        if (this.mIsInEmergencyCall)
        {
            this.mIsInEmergencyCall = false;
            String str = SystemProperties.get("ril.cdma.inecmmode", "false");
            log("checkAndEnableDataCallAfterEmergencyCallDropped,inEcm=" + str);
            if (str.compareTo("false") == 0)
                this.phone.mDataConnectionTracker.setInternalDataEnabled(true);
        }
    }

    private Connection checkMtFindNewRinging(DriverCall paramDriverCall, int paramInt)
    {
        CdmaConnection localCdmaConnection = null;
        this.connections[paramInt] = new CdmaConnection(this.phone.getContext(), paramDriverCall, this, paramInt);
        if (this.connections[paramInt].getCall() == this.ringingCall)
        {
            localCdmaConnection = this.connections[paramInt];
            log("Notify new ring " + paramDriverCall);
        }
        while (true)
        {
            return localCdmaConnection;
            Log.e("CDMA", "Phantom call appeared " + paramDriverCall);
            if ((paramDriverCall.state != DriverCall.State.ALERTING) && (paramDriverCall.state != DriverCall.State.DIALING))
                this.connections[paramInt].connectTime = System.currentTimeMillis();
        }
    }

    private Connection dialThreeWay(String paramString)
    {
        if (!this.foregroundCall.isIdle())
        {
            disableDataCallInEmergencyCall(paramString);
            this.pendingMO = new CdmaConnection(this.phone.getContext(), checkForTestEmergencyNumber(paramString), this, this.foregroundCall);
            this.cm.sendCDMAFeatureCode(this.pendingMO.address, obtainMessage(16));
        }
        for (CdmaConnection localCdmaConnection = this.pendingMO; ; localCdmaConnection = null)
            return localCdmaConnection;
    }

    private void disableDataCallInEmergencyCall(String paramString)
    {
        if (PhoneNumberUtils.isLocalEmergencyNumber(paramString, this.phone.getContext()))
        {
            log("disableDataCallInEmergencyCall");
            this.mIsInEmergencyCall = true;
            this.phone.mDataConnectionTracker.setInternalDataEnabled(false);
        }
    }

    private void fakeHoldForegroundBeforeDial()
    {
        List localList = (List)this.foregroundCall.connections.clone();
        int i = 0;
        int j = localList.size();
        while (i < j)
        {
            ((CdmaConnection)localList.get(i)).fakeHoldBeforeDial();
            i++;
        }
    }

    private void flashAndSetGenericTrue()
        throws CallStateException
    {
        this.cm.sendCDMAFeatureCode("", obtainMessage(8));
        this.foregroundCall.setGeneric(true);
        this.phone.notifyPreciseCallStateChanged();
    }

    private Phone.SuppService getFailedService(int paramInt)
    {
        Phone.SuppService localSuppService;
        switch (paramInt)
        {
        case 9:
        case 10:
        default:
            localSuppService = Phone.SuppService.UNKNOWN;
        case 8:
        case 11:
        case 12:
        case 13:
        }
        while (true)
        {
            return localSuppService;
            localSuppService = Phone.SuppService.SWITCH;
            continue;
            localSuppService = Phone.SuppService.CONFERENCE;
            continue;
            localSuppService = Phone.SuppService.SEPARATE;
            continue;
            localSuppService = Phone.SuppService.TRANSFER;
        }
    }

    private void handleCallWaitingInfo(CdmaCallWaitingNotification paramCdmaCallWaitingNotification)
    {
        if (this.foregroundCall.connections.size() > 1)
            this.foregroundCall.setGeneric(true);
        this.ringingCall.setGeneric(false);
        new CdmaConnection(this.phone.getContext(), paramCdmaCallWaitingNotification, this, this.ringingCall);
        updatePhoneState();
        notifyCallWaitingInfo(paramCdmaCallWaitingNotification);
    }

    private void handleEcmTimer(int paramInt)
    {
        this.phone.handleTimerInEmergencyCallbackMode(paramInt);
        switch (paramInt)
        {
        default:
            Log.e("CDMA", "handleEcmTimer, unsupported action " + paramInt);
        case 1:
        case 0:
        }
        while (true)
        {
            return;
            this.mIsEcmTimerCanceled = true;
            continue;
            this.mIsEcmTimerCanceled = false;
        }
    }

    private void handleRadioNotAvailable()
    {
        pollCallsWhenSafe();
    }

    private void internalClearDisconnected()
    {
        this.ringingCall.clearDisconnected();
        this.foregroundCall.clearDisconnected();
        this.backgroundCall.clearDisconnected();
    }

    private void notifyCallWaitingInfo(CdmaCallWaitingNotification paramCdmaCallWaitingNotification)
    {
        if (this.callWaitingRegistrants != null)
            this.callWaitingRegistrants.notifyRegistrants(new AsyncResult(null, paramCdmaCallWaitingNotification, null));
    }

    private Message obtainCompleteMessage()
    {
        return obtainCompleteMessage(4);
    }

    private Message obtainCompleteMessage(int paramInt)
    {
        this.pendingOperations = (1 + this.pendingOperations);
        this.lastRelevantPoll = null;
        this.needsPoll = true;
        return obtainMessage(paramInt);
    }

    private void operationComplete()
    {
        this.pendingOperations = (-1 + this.pendingOperations);
        if ((this.pendingOperations == 0) && (this.needsPoll))
        {
            this.lastRelevantPoll = obtainMessage(1);
            this.cm.getCurrentCalls(this.lastRelevantPoll);
        }
        while (true)
        {
            return;
            if (this.pendingOperations < 0)
            {
                Log.e("CDMA", "CdmaCallTracker.pendingOperations < 0");
                this.pendingOperations = 0;
            }
        }
    }

    private void updatePhoneState()
    {
        Phone.State localState = this.state;
        if (this.ringingCall.isRinging())
        {
            this.state = Phone.State.RINGING;
            if ((this.state != Phone.State.IDLE) || (localState == this.state))
                break label157;
            this.voiceCallEndedRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
        }
        while (true)
        {
            log("update phone state, old=" + localState + " new=" + this.state);
            if (this.state != localState)
                this.phone.notifyPhoneStateChanged();
            return;
            if ((this.pendingMO != null) || (!this.foregroundCall.isIdle()) || (!this.backgroundCall.isIdle()))
            {
                this.state = Phone.State.OFFHOOK;
                break;
            }
            this.state = Phone.State.IDLE;
            break;
            label157: if ((localState == Phone.State.IDLE) && (localState != this.state))
                this.voiceCallStartedRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
        }
    }

    void acceptCall()
        throws CallStateException
    {
        if (this.ringingCall.getState() == Call.State.INCOMING)
        {
            Log.i("phone", "acceptCall: incoming...");
            setMute(false);
            this.cm.acceptCall(obtainCompleteMessage());
        }
        while (true)
        {
            return;
            if (this.ringingCall.getState() != Call.State.WAITING)
                break;
            CdmaConnection localCdmaConnection = (CdmaConnection)this.ringingCall.getLatestConnection();
            localCdmaConnection.updateParent(this.ringingCall, this.foregroundCall);
            localCdmaConnection.onConnectedInOrOut();
            updatePhoneState();
            switchWaitingOrHoldingAndActive();
        }
        throw new CallStateException("phone not ringing");
    }

    boolean canConference()
    {
        if ((this.foregroundCall.getState() == Call.State.ACTIVE) && (this.backgroundCall.getState() == Call.State.HOLDING) && (!this.backgroundCall.isFull()) && (!this.foregroundCall.isFull()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean canDial()
    {
        boolean bool1 = true;
        int i = this.phone.getServiceState().getState();
        String str = SystemProperties.get("ro.telephony.disable-call", "false");
        boolean bool2;
        Object[] arrayOfObject;
        boolean bool3;
        label124: boolean bool4;
        label143: boolean bool5;
        label165: boolean bool6;
        label187: boolean bool7;
        label212: boolean bool8;
        if ((i != 3) && (this.pendingMO == null) && (!this.ringingCall.isRinging()) && (!str.equals("true")) && ((!this.foregroundCall.getState().isAlive()) || (this.foregroundCall.getState() == Call.State.ACTIVE) || (!this.backgroundCall.getState().isAlive())))
        {
            bool2 = bool1;
            if (!bool2)
            {
                arrayOfObject = new Object[8];
                arrayOfObject[0] = Integer.valueOf(i);
                if (i == 3)
                    break label290;
                bool3 = bool1;
                arrayOfObject[bool1] = Boolean.valueOf(bool3);
                if (this.pendingMO != null)
                    break label296;
                bool4 = bool1;
                arrayOfObject[2] = Boolean.valueOf(bool4);
                if (this.ringingCall.isRinging())
                    break label302;
                bool5 = bool1;
                arrayOfObject[3] = Boolean.valueOf(bool5);
                if (str.equals("true"))
                    break label308;
                bool6 = bool1;
                arrayOfObject[4] = Boolean.valueOf(bool6);
                if (this.foregroundCall.getState().isAlive())
                    break label314;
                bool7 = bool1;
                arrayOfObject[5] = Boolean.valueOf(bool7);
                if (this.foregroundCall.getState() != Call.State.ACTIVE)
                    break label320;
                bool8 = bool1;
                label237: arrayOfObject[6] = Boolean.valueOf(bool8);
                if (this.backgroundCall.getState().isAlive())
                    break label326;
            }
        }
        while (true)
        {
            arrayOfObject[7] = Boolean.valueOf(bool1);
            log(String.format("canDial is false\n((serviceState=%d) != ServiceState.STATE_POWER_OFF)::=%s\n&& pendingMO == null::=%s\n&& !ringingCall.isRinging()::=%s\n&& !disableCall.equals(\"true\")::=%s\n&& (!foregroundCall.getState().isAlive()::=%s\n     || foregroundCall.getState() == CdmaCall.State.ACTIVE::=%s\n     ||!backgroundCall.getState().isAlive())::=%s)", arrayOfObject));
            return bool2;
            bool2 = false;
            break;
            label290: bool3 = false;
            break label124;
            label296: bool4 = false;
            break label143;
            label302: bool5 = false;
            break label165;
            label308: bool6 = false;
            break label187;
            label314: bool7 = false;
            break label212;
            label320: bool8 = false;
            break label237;
            label326: bool1 = false;
        }
    }

    boolean canTransfer()
    {
        Log.e("CDMA", "canTransfer: not possible in CDMA");
        return false;
    }

    void clearDisconnected()
    {
        internalClearDisconnected();
        updatePhoneState();
        this.phone.notifyPreciseCallStateChanged();
    }

    void conference()
        throws CallStateException
    {
        flashAndSetGenericTrue();
    }

    Connection dial(String paramString)
        throws CallStateException
    {
        return dial(paramString, 0);
    }

    Connection dial(String paramString, int paramInt)
        throws CallStateException
    {
        clearDisconnected();
        if (!canDial())
            throw new CallStateException("cannot dial in current state");
        boolean bool1 = SystemProperties.get("ril.cdma.inecmmode", "false").equals("true");
        boolean bool2 = PhoneNumberUtils.isLocalEmergencyNumber(paramString, this.phone.getContext());
        if ((bool1) && (bool2))
            handleEcmTimer(1);
        this.foregroundCall.setGeneric(false);
        Object localObject;
        if (this.foregroundCall.getState() == Call.State.ACTIVE)
        {
            localObject = dialThreeWay(paramString);
            return localObject;
        }
        this.pendingMO = new CdmaConnection(this.phone.getContext(), checkForTestEmergencyNumber(paramString), this, this.foregroundCall);
        this.hangupPendingMO = false;
        if ((this.pendingMO.address == null) || (this.pendingMO.address.length() == 0) || (this.pendingMO.address.indexOf('N') >= 0))
        {
            this.pendingMO.cause = Connection.DisconnectCause.INVALID_NUMBER;
            pollCallsWhenSafe();
        }
        while (true)
        {
            updatePhoneState();
            this.phone.notifyPreciseCallStateChanged();
            localObject = this.pendingMO;
            break;
            setMute(false);
            disableDataCallInEmergencyCall(paramString);
            if ((!bool1) || ((bool1) && (bool2)))
            {
                this.cm.dial(this.pendingMO.address, paramInt, obtainCompleteMessage());
            }
            else
            {
                this.phone.exitEmergencyCallbackMode();
                this.phone.setOnEcbModeExitResponse(this, 14, null);
                this.pendingCallClirMode = paramInt;
                this.pendingCallInEcm = true;
            }
        }
    }

    public void dispose()
    {
        this.cm.unregisterForCallStateChanged(this);
        this.cm.unregisterForOn(this);
        this.cm.unregisterForNotAvailable(this);
        this.cm.unregisterForCallWaitingInfo(this);
        CdmaConnection[] arrayOfCdmaConnection = this.connections;
        int i = arrayOfCdmaConnection.length;
        int j = 0;
        while (true)
            if (j < i)
            {
                CdmaConnection localCdmaConnection = arrayOfCdmaConnection[j];
                if (localCdmaConnection != null);
                try
                {
                    hangup(localCdmaConnection);
                    j++;
                }
                catch (CallStateException localCallStateException2)
                {
                    while (true)
                        Log.e("CDMA", "unexpected error on hangup during dispose");
                }
            }
        try
        {
            if (this.pendingMO != null)
                hangup(this.pendingMO);
            clearDisconnected();
            return;
        }
        catch (CallStateException localCallStateException1)
        {
            while (true)
                Log.e("CDMA", "unexpected error on hangup during dispose");
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("GsmCallTracker extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println("droppedDuringPoll: length=" + this.connections.length);
        for (int i = 0; i < this.connections.length; i++)
        {
            Object[] arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = Integer.valueOf(i);
            arrayOfObject2[1] = this.connections[i];
            paramPrintWriter.printf(" connections[%d]=%s\n", arrayOfObject2);
        }
        paramPrintWriter.println(" voiceCallEndedRegistrants=" + this.voiceCallEndedRegistrants);
        paramPrintWriter.println(" voiceCallStartedRegistrants=" + this.voiceCallStartedRegistrants);
        paramPrintWriter.println(" callWaitingRegistrants=" + this.callWaitingRegistrants);
        paramPrintWriter.println("droppedDuringPoll: size=" + this.droppedDuringPoll.size());
        for (int j = 0; j < this.droppedDuringPoll.size(); j++)
        {
            Object[] arrayOfObject1 = new Object[2];
            arrayOfObject1[0] = Integer.valueOf(j);
            arrayOfObject1[1] = this.droppedDuringPoll.get(j);
            paramPrintWriter.printf(" droppedDuringPoll[%d]=%s\n", arrayOfObject1);
        }
        paramPrintWriter.println(" ringingCall=" + this.ringingCall);
        paramPrintWriter.println(" foregroundCall=" + this.foregroundCall);
        paramPrintWriter.println(" backgroundCall=" + this.backgroundCall);
        paramPrintWriter.println(" pendingMO=" + this.pendingMO);
        paramPrintWriter.println(" hangupPendingMO=" + this.hangupPendingMO);
        paramPrintWriter.println(" pendingCallInEcm=" + this.pendingCallInEcm);
        paramPrintWriter.println(" mIsInEmergencyCall=" + this.mIsInEmergencyCall);
        paramPrintWriter.println(" phone=" + this.phone);
        paramPrintWriter.println(" desiredMute=" + this.desiredMute);
        paramPrintWriter.println(" pendingCallClirMode=" + this.pendingCallClirMode);
        paramPrintWriter.println(" state=" + this.state);
        paramPrintWriter.println(" mIsEcmTimerCanceled=" + this.mIsEcmTimerCanceled);
    }

    void explicitCallTransfer()
        throws CallStateException
    {
        this.cm.explicitCallTransfer(obtainCompleteMessage(13));
    }

    protected void finalize()
    {
        Log.d("CDMA", "CdmaCallTracker finalized");
    }

    CdmaConnection getConnectionByIndex(CdmaCall paramCdmaCall, int paramInt)
        throws CallStateException
    {
        int i = paramCdmaCall.connections.size();
        int j = 0;
        CdmaConnection localCdmaConnection;
        if (j < i)
        {
            localCdmaConnection = (CdmaConnection)paramCdmaCall.connections.get(j);
            if (localCdmaConnection.getCDMAIndex() != paramInt);
        }
        while (true)
        {
            return localCdmaConnection;
            j++;
            break;
            localCdmaConnection = null;
        }
    }

    boolean getMute()
    {
        return this.desiredMute;
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        case 6:
        case 7:
        case 11:
        case 12:
        case 13:
        default:
            throw new RuntimeException("unexpected event not handled");
        case 1:
            Log.d("CDMA", "Event EVENT_POLL_CALLS_RESULT Received");
            ((AsyncResult)paramMessage.obj);
            if (paramMessage == this.lastRelevantPoll)
            {
                this.needsPoll = false;
                this.lastRelevantPoll = null;
                handlePollCalls((AsyncResult)paramMessage.obj);
            }
            break;
        case 8:
        case 4:
        case 5:
        case 2:
        case 3:
        case 9:
        case 10:
        case 14:
        case 15:
        case 16:
        }
        while (true)
        {
            return;
            operationComplete();
            continue;
            AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
            operationComplete();
            int i;
            if (localAsyncResult2.exception != null)
            {
                i = 16;
                Log.i("CDMA", "Exception during getLastCallFailCause, assuming normal disconnect");
            }
            while (true)
            {
                int j = 0;
                int k = this.droppedDuringPoll.size();
                while (j < k)
                {
                    ((CdmaConnection)this.droppedDuringPoll.get(j)).onRemoteDisconnect(i);
                    j++;
                }
                i = ((int[])(int[])localAsyncResult2.result)[0];
            }
            updatePhoneState();
            this.phone.notifyPreciseCallStateChanged();
            this.droppedDuringPoll.clear();
            continue;
            pollCallsWhenSafe();
            continue;
            handleRadioAvailable();
            continue;
            handleRadioNotAvailable();
            continue;
            if (this.pendingCallInEcm)
            {
                this.cm.dial(this.pendingMO.address, this.pendingCallClirMode, obtainCompleteMessage());
                this.pendingCallInEcm = false;
            }
            this.phone.unsetOnEcbModeExitResponse(this);
            continue;
            AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult1.exception == null)
            {
                handleCallWaitingInfo((CdmaCallWaitingNotification)localAsyncResult1.result);
                Log.d("CDMA", "Event EVENT_CALL_WAITING_INFO_CDMA Received");
                continue;
                if (((AsyncResult)paramMessage.obj).exception == null)
                {
                    this.pendingMO.onConnectedInOrOut();
                    this.pendingMO = null;
                }
            }
        }
    }

    protected void handlePollCalls(AsyncResult paramAsyncResult)
    {
        Object localObject;
        Connection localConnection;
        int i;
        int j;
        int k;
        CdmaConnection localCdmaConnection2;
        DriverCall localDriverCall;
        if (paramAsyncResult.exception == null)
        {
            localObject = (List)paramAsyncResult.result;
            localConnection = null;
            i = 0;
            j = 0;
            k = 0;
            int m = 0;
            int n = ((List)localObject).size();
            if (k >= this.connections.length)
                break label696;
            localCdmaConnection2 = this.connections[k];
            localDriverCall = null;
            if (m < n)
            {
                localDriverCall = (DriverCall)((List)localObject).get(m);
                if (localDriverCall.index != k + 1)
                    break label253;
                m++;
            }
            if ((localCdmaConnection2 != null) || (localDriverCall == null))
                break label340;
            if ((this.pendingMO == null) || (!this.pendingMO.compareTo(localDriverCall)))
                break label273;
            this.connections[k] = this.pendingMO;
            this.pendingMO.index = k;
            this.pendingMO.update(localDriverCall);
            this.pendingMO = null;
            if (!this.hangupPendingMO)
                break label331;
            this.hangupPendingMO = false;
            if (this.mIsEcmTimerCanceled)
                handleEcmTimer(0);
        }
        while (true)
        {
            try
            {
                log("poll: hangupPendingMO, hangup conn " + k);
                hangup(this.connections[k]);
                return;
                if (isCommandExceptionRadioNotAvailable(paramAsyncResult.exception))
                {
                    localObject = new ArrayList();
                    break;
                }
                pollCallsAfterDelay();
                continue;
                label253: localDriverCall = null;
            }
            catch (CallStateException localCallStateException)
            {
                Log.e("CDMA", "unexpected error on hangup");
                continue;
            }
            label273: log("pendingMo=" + this.pendingMO + ", dc=" + localDriverCall);
            localConnection = checkMtFindNewRinging(localDriverCall, k);
            if (localConnection == null)
                j = 1;
            checkAndEnableDataCallAfterEmergencyCallDropped();
            label331: i = 1;
            while (true)
            {
                k++;
                break;
                label340: if ((localCdmaConnection2 != null) && (localDriverCall == null))
                {
                    int i2 = this.foregroundCall.connections.size();
                    for (int i3 = 0; i3 < i2; i3++)
                    {
                        log("adding fgCall cn " + i3 + " to droppedDuringPoll");
                        CdmaConnection localCdmaConnection4 = (CdmaConnection)this.foregroundCall.connections.get(i3);
                        this.droppedDuringPoll.add(localCdmaConnection4);
                    }
                    int i4 = this.ringingCall.connections.size();
                    for (int i5 = 0; i5 < i4; i5++)
                    {
                        log("adding rgCall cn " + i5 + " to droppedDuringPoll");
                        CdmaConnection localCdmaConnection3 = (CdmaConnection)this.ringingCall.connections.get(i5);
                        this.droppedDuringPoll.add(localCdmaConnection3);
                    }
                    this.foregroundCall.setGeneric(false);
                    this.ringingCall.setGeneric(false);
                    if (this.mIsEcmTimerCanceled)
                        handleEcmTimer(0);
                    checkAndEnableDataCallAfterEmergencyCallDropped();
                    this.connections[k] = null;
                }
                else if ((localCdmaConnection2 != null) && (localDriverCall != null))
                {
                    if (localCdmaConnection2.isIncoming == localDriverCall.isMT)
                        break label665;
                    if (localDriverCall.isMT == true)
                    {
                        this.droppedDuringPoll.add(localCdmaConnection2);
                        localConnection = checkMtFindNewRinging(localDriverCall, k);
                        if (localConnection == null)
                            j = 1;
                        checkAndEnableDataCallAfterEmergencyCallDropped();
                    }
                    else
                    {
                        Log.e("CDMA", "Error in RIL, Phantom call appeared " + localDriverCall);
                    }
                }
            }
            label665: boolean bool = localCdmaConnection2.update(localDriverCall);
            if ((i != 0) || (bool));
            for (i = 1; ; i = 0)
                break;
            label696: if (this.pendingMO != null)
            {
                Log.d("CDMA", "Pending MO dropped before poll fg state:" + this.foregroundCall.getState());
                this.droppedDuringPoll.add(this.pendingMO);
                this.pendingMO = null;
                this.hangupPendingMO = false;
                if (this.pendingCallInEcm)
                    this.pendingCallInEcm = false;
            }
            if (localConnection != null)
                this.phone.notifyNewRingingConnection(localConnection);
            int i1 = -1 + this.droppedDuringPoll.size();
            if (i1 >= 0)
            {
                CdmaConnection localCdmaConnection1 = (CdmaConnection)this.droppedDuringPoll.get(i1);
                Connection.DisconnectCause localDisconnectCause;
                if ((localCdmaConnection1.isIncoming()) && (localCdmaConnection1.getConnectTime() == 0L))
                    if (localCdmaConnection1.cause == Connection.DisconnectCause.LOCAL)
                    {
                        localDisconnectCause = Connection.DisconnectCause.INCOMING_REJECTED;
                        label846: log("missed/rejected call, conn.cause=" + localCdmaConnection1.cause);
                        log("setting cause to " + localDisconnectCause);
                        this.droppedDuringPoll.remove(i1);
                        localCdmaConnection1.onDisconnect(localDisconnectCause);
                    }
                while (true)
                {
                    i1--;
                    break;
                    localDisconnectCause = Connection.DisconnectCause.INCOMING_MISSED;
                    break label846;
                    if (localCdmaConnection1.cause == Connection.DisconnectCause.LOCAL)
                    {
                        this.droppedDuringPoll.remove(i1);
                        localCdmaConnection1.onDisconnect(Connection.DisconnectCause.LOCAL);
                    }
                    else if (localCdmaConnection1.cause == Connection.DisconnectCause.INVALID_NUMBER)
                    {
                        this.droppedDuringPoll.remove(i1);
                        localCdmaConnection1.onDisconnect(Connection.DisconnectCause.INVALID_NUMBER);
                    }
                }
            }
            if (this.droppedDuringPoll.size() > 0)
                this.cm.getLastCallFailCause(obtainNoPollCompleteMessage(5));
            if (0 != 0)
                pollCallsAfterDelay();
            if ((localConnection != null) || (i != 0))
                internalClearDisconnected();
            updatePhoneState();
            if (j != 0)
                this.phone.notifyUnknownConnection();
            if ((i != 0) || (localConnection != null))
                this.phone.notifyPreciseCallStateChanged();
        }
    }

    void hangup(CdmaCall paramCdmaCall)
        throws CallStateException
    {
        if (paramCdmaCall.getConnections().size() == 0)
            throw new CallStateException("no connections in call");
        if (paramCdmaCall == this.ringingCall)
        {
            log("(ringing) hangup waiting or background");
            this.cm.hangupWaitingOrBackground(obtainCompleteMessage());
        }
        while (true)
        {
            paramCdmaCall.onHangupLocal();
            this.phone.notifyPreciseCallStateChanged();
            return;
            if (paramCdmaCall == this.foregroundCall)
            {
                if (paramCdmaCall.isDialingOrAlerting())
                {
                    log("(foregnd) hangup dialing or alerting...");
                    hangup((CdmaConnection)paramCdmaCall.getConnections().get(0));
                }
                else
                {
                    hangupForegroundResumeBackground();
                }
            }
            else
            {
                if (paramCdmaCall != this.backgroundCall)
                    break;
                if (this.ringingCall.isRinging())
                {
                    log("hangup all conns in background call");
                    hangupAllConnections(paramCdmaCall);
                }
                else
                {
                    hangupWaitingOrBackground();
                }
            }
        }
        throw new RuntimeException("CdmaCall " + paramCdmaCall + "does not belong to CdmaCallTracker " + this);
    }

    void hangup(CdmaConnection paramCdmaConnection)
        throws CallStateException
    {
        if (paramCdmaConnection.owner != this)
            throw new CallStateException("CdmaConnection " + paramCdmaConnection + "does not belong to CdmaCallTracker " + this);
        if (paramCdmaConnection == this.pendingMO)
        {
            log("hangup: set hangupPendingMO to true");
            this.hangupPendingMO = true;
        }
        while (true)
        {
            paramCdmaConnection.onHangupLocal();
            while (true)
            {
                return;
                if ((paramCdmaConnection.getCall() != this.ringingCall) || (this.ringingCall.getState() != Call.State.WAITING))
                    break;
                paramCdmaConnection.onLocalDisconnect();
                updatePhoneState();
                this.phone.notifyPreciseCallStateChanged();
            }
            try
            {
                this.cm.hangupConnection(paramCdmaConnection.getCDMAIndex(), obtainCompleteMessage());
            }
            catch (CallStateException localCallStateException)
            {
                Log.w("CDMA", "CdmaCallTracker WARN: hangup() on absent connection " + paramCdmaConnection);
            }
        }
    }

    void hangupAllConnections(CdmaCall paramCdmaCall)
        throws CallStateException
    {
        try
        {
            int i = paramCdmaCall.connections.size();
            for (int j = 0; j < i; j++)
            {
                CdmaConnection localCdmaConnection = (CdmaConnection)paramCdmaCall.connections.get(j);
                this.cm.hangupConnection(localCdmaConnection.getCDMAIndex(), obtainCompleteMessage());
            }
        }
        catch (CallStateException localCallStateException)
        {
            Log.e("CDMA", "hangupConnectionByIndex caught " + localCallStateException);
        }
    }

    void hangupConnectionByIndex(CdmaCall paramCdmaCall, int paramInt)
        throws CallStateException
    {
        int i = paramCdmaCall.connections.size();
        for (int j = 0; j < i; j++)
            if (((CdmaConnection)paramCdmaCall.connections.get(j)).getCDMAIndex() == paramInt)
            {
                this.cm.hangupConnection(paramInt, obtainCompleteMessage());
                return;
            }
        throw new CallStateException("no gsm index found");
    }

    void hangupForegroundResumeBackground()
    {
        log("hangupForegroundResumeBackground");
        this.cm.hangupForegroundResumeBackground(obtainCompleteMessage());
    }

    void hangupWaitingOrBackground()
    {
        log("hangupWaitingOrBackground");
        this.cm.hangupWaitingOrBackground(obtainCompleteMessage());
    }

    boolean isInEmergencyCall()
    {
        return this.mIsInEmergencyCall;
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[CdmaCallTracker] " + paramString);
    }

    public void registerForCallWaiting(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.callWaitingRegistrants.add(localRegistrant);
    }

    public void registerForVoiceCallEnded(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.voiceCallEndedRegistrants.add(localRegistrant);
    }

    public void registerForVoiceCallStarted(Handler paramHandler, int paramInt, Object paramObject)
    {
        Registrant localRegistrant = new Registrant(paramHandler, paramInt, paramObject);
        this.voiceCallStartedRegistrants.add(localRegistrant);
        if (this.state != Phone.State.IDLE)
            localRegistrant.notifyRegistrant(new AsyncResult(null, null, null));
    }

    void rejectCall()
        throws CallStateException
    {
        if (this.ringingCall.getState().isRinging())
        {
            this.cm.rejectCall(obtainCompleteMessage());
            return;
        }
        throw new CallStateException("phone not ringing");
    }

    void separate(CdmaConnection paramCdmaConnection)
        throws CallStateException
    {
        if (paramCdmaConnection.owner != this)
            throw new CallStateException("CdmaConnection " + paramCdmaConnection + "does not belong to CdmaCallTracker " + this);
        try
        {
            this.cm.separateConnection(paramCdmaConnection.getCDMAIndex(), obtainCompleteMessage(12));
            return;
        }
        catch (CallStateException localCallStateException)
        {
            while (true)
                Log.w("CDMA", "CdmaCallTracker WARN: separate() on absent connection " + paramCdmaConnection);
        }
    }

    void setMute(boolean paramBoolean)
    {
        this.desiredMute = paramBoolean;
        this.cm.setMute(this.desiredMute, null);
    }

    void switchWaitingOrHoldingAndActive()
        throws CallStateException
    {
        if (this.ringingCall.getState() == Call.State.INCOMING)
            throw new CallStateException("cannot be in the incoming state");
        if (this.foregroundCall.getConnections().size() > 1)
            flashAndSetGenericTrue();
        while (true)
        {
            return;
            this.cm.sendCDMAFeatureCode("", obtainMessage(8));
        }
    }

    public void unregisterForCallWaiting(Handler paramHandler)
    {
        this.callWaitingRegistrants.remove(paramHandler);
    }

    public void unregisterForVoiceCallEnded(Handler paramHandler)
    {
        this.voiceCallEndedRegistrants.remove(paramHandler);
    }

    public void unregisterForVoiceCallStarted(Handler paramHandler)
    {
        this.voiceCallStartedRegistrants.remove(paramHandler);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaCallTracker
 * JD-Core Version:        0.6.2
 */