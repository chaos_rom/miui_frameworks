package com.android.internal.telephony.cdma;

public class TtyIntent
{
    private static final String TAG = "TtyIntent";
    public static final String TTY_ENABLED = "ttyEnabled";
    public static final String TTY_ENABLED_CHANGE_ACTION = "com.android.internal.telephony.cdma.intent.action.TTY_ENABLED_CHANGE";
    public static final String TTY_PREFERRED_MODE_CHANGE_ACTION = "com.android.internal.telephony.cdma.intent.action.TTY_PREFERRED_MODE_CHANGE";
    public static final String TTY_PREFFERED_MODE = "ttyPreferredMode";
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.TtyIntent
 * JD-Core Version:        0.6.2
 */