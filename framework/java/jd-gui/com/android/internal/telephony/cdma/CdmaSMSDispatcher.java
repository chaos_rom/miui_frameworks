package com.android.internal.telephony.cdma;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Message;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsCbMessage;
import android.telephony.SmsMessage.MessageClass;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.SMSDispatcher;
import com.android.internal.telephony.SMSDispatcher.SmsTracker;
import com.android.internal.telephony.SmsAddress;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.SmsMessageBase.SubmitPduBase;
import com.android.internal.telephony.SmsMessageBase.TextEncodingDetails;
import com.android.internal.telephony.SmsStorageMonitor;
import com.android.internal.telephony.SmsUsageMonitor;
import com.android.internal.telephony.WspTypeDecoder;
import com.android.internal.telephony.cdma.sms.BearerData;
import com.android.internal.telephony.cdma.sms.CdmaSmsAddress;
import com.android.internal.telephony.cdma.sms.UserData;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

final class CdmaSMSDispatcher extends SMSDispatcher
{
    private static final String TAG = "CDMA";
    private final boolean mCheckForDuplicatePortsInOmadmWapPush = Resources.getSystem().getBoolean(17891379);
    private byte[] mLastAcknowledgedSmsFingerprint;
    private byte[] mLastDispatchedSmsFingerprint;
    private final BroadcastReceiver mScpResultsReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            int i = getResultCode();
            int j;
            if ((i == -1) || (i == 1))
            {
                j = 1;
                if (j != 0)
                    break label56;
                Log.e("CDMA", "SCP results error: result code = " + i);
            }
            label56: DataOutputStream localDataOutputStream;
            while (true)
            {
                return;
                j = 0;
                break;
                Bundle localBundle = getResultExtras(false);
                if (localBundle == null)
                {
                    Log.e("CDMA", "SCP results error: missing extras");
                }
                else
                {
                    String str = localBundle.getString("sender");
                    if (str == null)
                    {
                        Log.e("CDMA", "SCP results error: missing sender extra.");
                    }
                    else
                    {
                        ArrayList localArrayList = localBundle.getParcelableArrayList("results");
                        if (localArrayList == null)
                        {
                            Log.e("CDMA", "SCP results error: missing results extra.");
                        }
                        else
                        {
                            BearerData localBearerData = new BearerData();
                            localBearerData.messageType = 2;
                            localBearerData.messageId = SmsMessage.getNextMessageId();
                            localBearerData.serviceCategoryProgramResults = localArrayList;
                            byte[] arrayOfByte = BearerData.encode(localBearerData);
                            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(100);
                            localDataOutputStream = new DataOutputStream(localByteArrayOutputStream);
                            try
                            {
                                localDataOutputStream.writeInt(4102);
                                localDataOutputStream.writeInt(0);
                                localDataOutputStream.writeInt(0);
                                CdmaSmsAddress localCdmaSmsAddress = CdmaSmsAddress.parse(PhoneNumberUtils.cdmaCheckAndProcessPlusCode(str));
                                localDataOutputStream.write(localCdmaSmsAddress.digitMode);
                                localDataOutputStream.write(localCdmaSmsAddress.numberMode);
                                localDataOutputStream.write(localCdmaSmsAddress.ton);
                                localDataOutputStream.write(localCdmaSmsAddress.numberPlan);
                                localDataOutputStream.write(localCdmaSmsAddress.numberOfDigits);
                                localDataOutputStream.write(localCdmaSmsAddress.origBytes, 0, localCdmaSmsAddress.origBytes.length);
                                localDataOutputStream.write(0);
                                localDataOutputStream.write(0);
                                localDataOutputStream.write(0);
                                localDataOutputStream.write(arrayOfByte.length);
                                localDataOutputStream.write(arrayOfByte, 0, arrayOfByte.length);
                                CdmaSMSDispatcher.access$000(CdmaSMSDispatcher.this).sendCdmaSms(localByteArrayOutputStream.toByteArray(), null);
                                try
                                {
                                    localDataOutputStream.close();
                                }
                                catch (IOException localIOException3)
                                {
                                }
                            }
                            catch (IOException localIOException2)
                            {
                                localIOException2 = localIOException2;
                                Log.e("CDMA", "exception creating SCP results PDU", localIOException2);
                                localDataOutputStream.close();
                            }
                            finally
                            {
                            }
                        }
                    }
                }
            }
            try
            {
                localDataOutputStream.close();
                label380: throw localObject;
            }
            catch (IOException localIOException1)
            {
                break label380;
            }
        }
    };

    CdmaSMSDispatcher(CDMAPhone paramCDMAPhone, SmsStorageMonitor paramSmsStorageMonitor, SmsUsageMonitor paramSmsUsageMonitor)
    {
        super(paramCDMAPhone, paramSmsStorageMonitor, paramSmsUsageMonitor);
        this.mCm.setOnNewCdmaSms(this, 1, null);
    }

    private static boolean checkDuplicatePortOmadmWappush(byte[] paramArrayOfByte, int paramInt)
    {
        boolean bool = false;
        int i = paramInt + 4;
        byte[] arrayOfByte = new byte[paramArrayOfByte.length - i];
        System.arraycopy(paramArrayOfByte, i, arrayOfByte, 0, arrayOfByte.length);
        WspTypeDecoder localWspTypeDecoder = new WspTypeDecoder(arrayOfByte);
        if (!localWspTypeDecoder.decodeUintvarInteger(2));
        while (true)
        {
            return bool;
            if (localWspTypeDecoder.decodeContentType(2 + localWspTypeDecoder.getDecodedDataLength()))
            {
                String str = localWspTypeDecoder.getValueString();
                if ((str != null) && (str.equals("application/vnd.syncml.notification")))
                    bool = true;
            }
        }
    }

    private void handleCdmaStatusReport(SmsMessage paramSmsMessage)
    {
        int i = 0;
        int j = this.deliveryPendingList.size();
        while (true)
        {
            PendingIntent localPendingIntent;
            Intent localIntent;
            if (i < j)
            {
                SMSDispatcher.SmsTracker localSmsTracker = (SMSDispatcher.SmsTracker)this.deliveryPendingList.get(i);
                if (localSmsTracker.mMessageRef != paramSmsMessage.messageRef)
                    break label101;
                this.deliveryPendingList.remove(i);
                localPendingIntent = localSmsTracker.mDeliveryIntent;
                localIntent = new Intent();
                localIntent.putExtra("pdu", paramSmsMessage.getPdu());
                localIntent.putExtra("format", "3gpp2");
            }
            try
            {
                localPendingIntent.send(this.mContext, -1, localIntent);
                label100: return;
                label101: i++;
            }
            catch (PendingIntent.CanceledException localCanceledException)
            {
                break label100;
            }
        }
    }

    private void handleServiceCategoryProgramData(SmsMessage paramSmsMessage)
    {
        ArrayList localArrayList = paramSmsMessage.getSmsCbProgramData();
        if (localArrayList == null)
            Log.e("CDMA", "handleServiceCategoryProgramData: program data list is null!");
        while (true)
        {
            return;
            Intent localIntent = new Intent("android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED");
            localIntent.putExtra("sender", paramSmsMessage.getOriginatingAddress());
            localIntent.putParcelableArrayListExtra("program_data", localArrayList);
            dispatch(localIntent, "android.permission.RECEIVE_SMS", this.mScpResultsReceiver);
        }
    }

    private static int resultToCause(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        case 0:
        case 2:
        default:
            i = 96;
        case -1:
        case 1:
        case 3:
        case 4:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 35;
            continue;
            i = 4;
        }
    }

    protected void acknowledgeLastIncomingSms(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        if (SystemProperties.get("ril.cdma.inecmmode", "false").equals("true"));
        while (true)
        {
            return;
            int i = resultToCause(paramInt);
            this.mCm.acknowledgeLastIncomingCdmaSms(paramBoolean, i, paramMessage);
            if (i == 0)
                this.mLastAcknowledgedSmsFingerprint = this.mLastDispatchedSmsFingerprint;
            this.mLastDispatchedSmsFingerprint = null;
        }
    }

    protected SmsMessageBase.TextEncodingDetails calculateLength(CharSequence paramCharSequence, boolean paramBoolean)
    {
        return SmsMessage.calculateLength(paramCharSequence, paramBoolean);
    }

    public int dispatchMessage(SmsMessageBase paramSmsMessageBase)
    {
        int i = 1;
        if (paramSmsMessageBase == null)
        {
            Log.e("CDMA", "dispatchMessage: message is null");
            i = 2;
        }
        while (true)
        {
            return i;
            if (SystemProperties.get("ril.cdma.inecmmode", "false").equals("true"))
            {
                i = -1;
            }
            else if (this.mSmsReceiveDisabled)
            {
                Log.d("CDMA", "Received short message on device which doesn't support receiving SMS. Ignored.");
            }
            else
            {
                SmsMessage localSmsMessage = (SmsMessage)paramSmsMessageBase;
                if (i == localSmsMessage.getMessageType())
                {
                    Log.d("CDMA", "Broadcast type message");
                    SmsCbMessage localSmsCbMessage = localSmsMessage.parseBroadcastSms();
                    if (localSmsCbMessage != null)
                        dispatchBroadcastMessage(localSmsCbMessage);
                }
                else
                {
                    this.mLastDispatchedSmsFingerprint = localSmsMessage.getIncomingSmsFingerprint();
                    if ((this.mLastAcknowledgedSmsFingerprint == null) || (!Arrays.equals(this.mLastDispatchedSmsFingerprint, this.mLastAcknowledgedSmsFingerprint)))
                    {
                        localSmsMessage.parseSms();
                        int j = localSmsMessage.getTeleService();
                        int k = 0;
                        if ((4099 == j) || (262144 == j))
                        {
                            int m = localSmsMessage.getNumOfVoicemails();
                            Log.d("CDMA", "Voicemail count=" + m);
                            SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
                            localEditor.putInt("vm_count_key_cdma", m);
                            localEditor.apply();
                            this.mPhone.setVoiceMessageWaiting(i, m);
                            k = 1;
                        }
                        while (true)
                        {
                            if (k != 0)
                                break label332;
                            if ((this.mStorageMonitor.isStorageAvailable()) || (localSmsMessage.getMessageClass() == SmsMessage.MessageClass.CLASS_0))
                                break label334;
                            i = 3;
                            break;
                            if (((4098 == j) || (4101 == j)) && (localSmsMessage.isStatusReportMessage()))
                            {
                                handleCdmaStatusReport(localSmsMessage);
                                k = 1;
                            }
                            else if (4102 == j)
                            {
                                handleServiceCategoryProgramData(localSmsMessage);
                                k = 1;
                            }
                            else if (localSmsMessage.getUserData() == null)
                            {
                                k = 1;
                            }
                        }
                        label332: continue;
                        label334: if (4100 == j)
                            i = processCdmaWapPdu(localSmsMessage.getUserData(), localSmsMessage.messageRef, localSmsMessage.getOriginatingAddress());
                        else if ((4098 != j) && (4101 != j) && (i != localSmsMessage.getMessageType()))
                            i = 4;
                        else
                            i = dispatchNormalMessage(paramSmsMessageBase);
                    }
                }
            }
        }
    }

    public void dispose()
    {
        this.mCm.unSetOnNewCdmaSms(this);
    }

    protected String getFormat()
    {
        return "3gpp2";
    }

    protected int processCdmaWapPdu(byte[] paramArrayOfByte, int paramInt, String paramString)
    {
        int i = 0 + 1;
        int j = 0xFF & paramArrayOfByte[0];
        if (j != 0)
            Log.w("CDMA", "Received a WAP SMS which is not WDP. Discard.");
        int m;
        int n;
        int i1;
        for (int i5 = 1; ; i5 = 1)
        {
            return i5;
            int k = i + 1;
            m = 0xFF & paramArrayOfByte[i];
            n = k + 1;
            i1 = 0xFF & paramArrayOfByte[k];
            if (i1 < m)
                break;
            Log.e("CDMA", "WDP bad segment #" + i1 + " expecting 0-" + (m - 1));
        }
        int i2 = 0;
        int i3 = 0;
        if (i1 == 0)
        {
            int i6 = n + 1;
            int i7 = (0xFF & paramArrayOfByte[n]) << 8;
            int i8 = i6 + 1;
            i2 = i7 | 0xFF & paramArrayOfByte[i6];
            int i9 = i8 + 1;
            int i10 = (0xFF & paramArrayOfByte[i8]) << 8;
            n = i9 + 1;
            i3 = i10 | 0xFF & paramArrayOfByte[i9];
            if ((!this.mCheckForDuplicatePortsInOmadmWapPush) || (!checkDuplicatePortOmadmWappush(paramArrayOfByte, n)));
        }
        for (int i4 = n + 4; ; i4 = n)
        {
            Log.i("CDMA", "Received WAP PDU. Type = " + j + ", originator = " + paramString + ", src-port = " + i2 + ", dst-port = " + i3 + ", ID = " + paramInt + ", segment# = " + i1 + '/' + m);
            byte[] arrayOfByte = new byte[paramArrayOfByte.length - i4];
            System.arraycopy(paramArrayOfByte, i4, arrayOfByte, 0, paramArrayOfByte.length - i4);
            i5 = processMessagePart(arrayOfByte, paramString, paramInt, i1, m, 0L, i3, true);
            break;
        }
    }

    protected void sendData(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfByte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        if (paramPendingIntent2 != null);
        for (boolean bool = true; ; bool = false)
        {
            sendSubmitPdu(SmsMessage.getSubmitPdu(paramString2, paramString1, paramInt, paramArrayOfByte, bool), paramPendingIntent1, paramPendingIntent2, paramString1);
            return;
        }
    }

    protected void sendNewSubmitPdu(String paramString1, String paramString2, String paramString3, SmsHeader paramSmsHeader, int paramInt, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean)
    {
        int i = 1;
        UserData localUserData = new UserData();
        localUserData.payloadStr = paramString3;
        localUserData.userDataHeader = paramSmsHeader;
        if (paramInt == i)
        {
            localUserData.msgEncoding = 9;
            localUserData.msgEncodingSet = i;
            if ((paramPendingIntent2 == null) || (!paramBoolean))
                break label83;
        }
        while (true)
        {
            sendSubmitPdu(SmsMessage.getSubmitPdu(paramString1, localUserData, i), paramPendingIntent1, paramPendingIntent2, paramString1);
            return;
            localUserData.msgEncoding = 4;
            break;
            label83: i = 0;
        }
    }

    protected void sendSms(SMSDispatcher.SmsTracker paramSmsTracker)
    {
        byte[] arrayOfByte = (byte[])paramSmsTracker.mData.get("pdu");
        Message localMessage = obtainMessage(2, paramSmsTracker);
        this.mCm.sendCdmaSms(arrayOfByte, localMessage);
    }

    protected void sendSubmitPdu(SmsMessage.SubmitPdu paramSubmitPdu, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString)
    {
        if ((!SystemProperties.getBoolean("ril.cdma.inecmmode", false)) || (paramPendingIntent1 != null));
        try
        {
            paramPendingIntent1.send(4);
            while (true)
            {
                label18: return;
                sendRawPdu(paramSubmitPdu.encodedScAddress, paramSubmitPdu.encodedMessage, paramPendingIntent1, paramPendingIntent2, paramString);
            }
        }
        catch (PendingIntent.CanceledException localCanceledException)
        {
            break label18;
        }
    }

    protected void sendText(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        if (paramPendingIntent2 != null);
        for (boolean bool = true; ; bool = false)
        {
            sendSubmitPdu(SmsMessage.getSubmitPdu(paramString2, paramString1, paramString3, bool, null), paramPendingIntent1, paramPendingIntent2, paramString1);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaSMSDispatcher
 * JD-Core Version:        0.6.2
 */