package com.android.internal.telephony.cdma;

import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccFileHandler;

public final class CdmaLteUiccFileHandler extends IccFileHandler
{
    static final String LOG_TAG = "CDMA";

    public CdmaLteUiccFileHandler(IccCard paramIccCard, String paramString, CommandsInterface paramCommandsInterface)
    {
        super(paramIccCard, paramString, paramCommandsInterface);
    }

    protected String getEFPath(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = getCommonIccEFPath(paramInt);
        case 28450:
        case 28456:
        case 28474:
        case 28481:
        case 28484:
        case 28506:
        case 28589:
        case 28418:
        case 28419:
        case 28420:
        }
        while (true)
        {
            return str;
            str = "3F007F25";
            continue;
            str = "3F007F20";
            continue;
            str = "3F007FFF";
        }
    }

    public void loadEFTransparent(int paramInt, Message paramMessage)
    {
        if (paramInt == 28506)
            this.mCi.iccIOForApp(176, paramInt, getEFPath(paramInt), 0, 0, 4, null, null, this.mAid, obtainMessage(5, paramInt, 0, paramMessage));
        while (true)
        {
            return;
            super.loadEFTransparent(paramInt, paramMessage);
        }
    }

    protected void logd(String paramString)
    {
        Log.d("CDMA", "[CdmaLteUiccFileHandler] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("CDMA", "[CdmaLteUiccFileHandler] " + paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaLteUiccFileHandler
 * JD-Core Version:        0.6.2
 */