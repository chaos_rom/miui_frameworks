package com.android.internal.telephony.cdma;

import android.os.Parcel;

public final class CdmaInformationRecords
{
    public static final int RIL_CDMA_CALLED_PARTY_NUMBER_INFO_REC = 1;
    public static final int RIL_CDMA_CALLING_PARTY_NUMBER_INFO_REC = 2;
    public static final int RIL_CDMA_CONNECTED_NUMBER_INFO_REC = 3;
    public static final int RIL_CDMA_DISPLAY_INFO_REC = 0;
    public static final int RIL_CDMA_EXTENDED_DISPLAY_INFO_REC = 7;
    public static final int RIL_CDMA_LINE_CONTROL_INFO_REC = 6;
    public static final int RIL_CDMA_REDIRECTING_NUMBER_INFO_REC = 5;
    public static final int RIL_CDMA_SIGNAL_INFO_REC = 4;
    public static final int RIL_CDMA_T53_AUDIO_CONTROL_INFO_REC = 10;
    public static final int RIL_CDMA_T53_CLIR_INFO_REC = 8;
    public static final int RIL_CDMA_T53_RELEASE_INFO_REC = 9;
    public Object record;

    public CdmaInformationRecords(Parcel paramParcel)
    {
        int i = paramParcel.readInt();
        switch (i)
        {
        case 9:
        default:
            throw new RuntimeException("RIL_UNSOL_CDMA_INFO_REC: unsupported record. Got " + idToString(i) + " ");
        case 0:
        case 7:
            this.record = new CdmaDisplayInfoRec(i, paramParcel.readString());
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 8:
        case 10:
        }
        while (true)
        {
            return;
            this.record = new CdmaNumberInfoRec(i, paramParcel.readString(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt());
            continue;
            this.record = new CdmaSignalInfoRec(paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt());
            continue;
            this.record = new CdmaRedirectingNumberInfoRec(paramParcel.readString(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt());
            continue;
            this.record = new CdmaLineControlInfoRec(paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt());
            continue;
            this.record = new CdmaT53ClirInfoRec(paramParcel.readInt());
            continue;
            this.record = new CdmaT53AudioControlInfoRec(paramParcel.readInt(), paramParcel.readInt());
        }
    }

    public static String idToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "<unknown record>";
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        }
        while (true)
        {
            return str;
            str = "RIL_CDMA_DISPLAY_INFO_REC";
            continue;
            str = "RIL_CDMA_CALLED_PARTY_NUMBER_INFO_REC";
            continue;
            str = "RIL_CDMA_CALLING_PARTY_NUMBER_INFO_REC";
            continue;
            str = "RIL_CDMA_CONNECTED_NUMBER_INFO_REC";
            continue;
            str = "RIL_CDMA_SIGNAL_INFO_REC";
            continue;
            str = "RIL_CDMA_REDIRECTING_NUMBER_INFO_REC";
            continue;
            str = "RIL_CDMA_LINE_CONTROL_INFO_REC";
            continue;
            str = "RIL_CDMA_EXTENDED_DISPLAY_INFO_REC";
            continue;
            str = "RIL_CDMA_T53_CLIR_INFO_REC";
            continue;
            str = "RIL_CDMA_T53_RELEASE_INFO_REC";
            continue;
            str = "RIL_CDMA_T53_AUDIO_CONTROL_INFO_REC";
        }
    }

    public static class CdmaT53AudioControlInfoRec
    {
        public byte downlink;
        public byte uplink;

        public CdmaT53AudioControlInfoRec(int paramInt1, int paramInt2)
        {
            this.uplink = ((byte)paramInt1);
            this.downlink = ((byte)paramInt2);
        }

        public String toString()
        {
            return "CdmaT53AudioControlInfoRec: { uplink: " + this.uplink + " downlink: " + this.downlink + " }";
        }
    }

    public static class CdmaT53ClirInfoRec
    {
        public byte cause;

        public CdmaT53ClirInfoRec(int paramInt)
        {
            this.cause = ((byte)paramInt);
        }

        public String toString()
        {
            return "CdmaT53ClirInfoRec: { cause: " + this.cause + " }";
        }
    }

    public static class CdmaLineControlInfoRec
    {
        public byte lineCtrlPolarityIncluded;
        public byte lineCtrlPowerDenial;
        public byte lineCtrlReverse;
        public byte lineCtrlToggle;

        public CdmaLineControlInfoRec(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.lineCtrlPolarityIncluded = ((byte)paramInt1);
            this.lineCtrlToggle = ((byte)paramInt2);
            this.lineCtrlReverse = ((byte)paramInt3);
            this.lineCtrlPowerDenial = ((byte)paramInt4);
        }

        public String toString()
        {
            return "CdmaLineControlInfoRec: { lineCtrlPolarityIncluded: " + this.lineCtrlPolarityIncluded + " lineCtrlToggle: " + this.lineCtrlToggle + " lineCtrlReverse: " + this.lineCtrlReverse + " lineCtrlPowerDenial: " + this.lineCtrlPowerDenial + " }";
        }
    }

    public static class CdmaRedirectingNumberInfoRec
    {
        public static final int REASON_CALLED_DTE_OUT_OF_ORDER = 9;
        public static final int REASON_CALL_FORWARDING_BUSY = 1;
        public static final int REASON_CALL_FORWARDING_BY_THE_CALLED_DTE = 10;
        public static final int REASON_CALL_FORWARDING_NO_REPLY = 2;
        public static final int REASON_CALL_FORWARDING_UNCONDITIONAL = 15;
        public static final int REASON_UNKNOWN;
        public CdmaInformationRecords.CdmaNumberInfoRec numberInfoRec;
        public int redirectingReason;

        public CdmaRedirectingNumberInfoRec(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            this.numberInfoRec = new CdmaInformationRecords.CdmaNumberInfoRec(5, paramString, paramInt1, paramInt2, paramInt3, paramInt4);
            this.redirectingReason = paramInt5;
        }

        public String toString()
        {
            return "CdmaNumberInfoRec: { numberInfoRec: " + this.numberInfoRec + ", redirectingReason: " + this.redirectingReason + " }";
        }
    }

    public static class CdmaNumberInfoRec
    {
        public int id;
        public String number;
        public byte numberPlan;
        public byte numberType;
        public byte pi;
        public byte si;

        public CdmaNumberInfoRec(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            this.number = paramString;
            this.numberType = ((byte)paramInt2);
            this.numberPlan = ((byte)paramInt3);
            this.pi = ((byte)paramInt4);
            this.si = ((byte)paramInt5);
        }

        public String toString()
        {
            return "CdmaNumberInfoRec: { id: " + CdmaInformationRecords.idToString(this.id) + ", number: " + this.number + ", numberType: " + this.numberType + ", numberPlan: " + this.numberPlan + ", pi: " + this.pi + ", si: " + this.si + " }";
        }
    }

    public static class CdmaDisplayInfoRec
    {
        public String alpha;
        public int id;

        public CdmaDisplayInfoRec(int paramInt, String paramString)
        {
            this.id = paramInt;
            this.alpha = paramString;
        }

        public String toString()
        {
            return "CdmaDisplayInfoRec: { id: " + CdmaInformationRecords.idToString(this.id) + ", alpha: " + this.alpha + " }";
        }
    }

    public static class CdmaSignalInfoRec
    {
        public int alertPitch;
        public boolean isPresent;
        public int signal;
        public int signalType;

        public CdmaSignalInfoRec()
        {
        }

        public CdmaSignalInfoRec(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            if (paramInt1 != 0);
            for (boolean bool = true; ; bool = false)
            {
                this.isPresent = bool;
                this.signalType = paramInt2;
                this.alertPitch = paramInt3;
                this.signal = paramInt4;
                return;
            }
        }

        public String toString()
        {
            return "CdmaSignalInfo: { isPresent: " + this.isPresent + ", signalType: " + this.signalType + ", alertPitch: " + this.alertPitch + ", signal: " + this.signal + " }";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaInformationRecords
 * JD-Core Version:        0.6.2
 */