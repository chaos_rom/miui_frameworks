package com.android.internal.telephony.cdma;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccSmsInterfaceManager;
import com.android.internal.telephony.IccUtils;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.SMSDispatcher;
import com.android.internal.telephony.SmsRawData;
import java.util.Arrays;
import java.util.List;

public class RuimSmsInterfaceManager extends IccSmsInterfaceManager
{
    static final boolean DBG = true;
    private static final int EVENT_LOAD_DONE = 1;
    private static final int EVENT_UPDATE_DONE = 2;
    static final String LOG_TAG = "CDMA";
    Handler mHandler = new Handler()
    {
        // ERROR //
        public void handleMessage(Message paramAnonymousMessage)
        {
            // Byte code:
            //     0: aload_1
            //     1: getfield 23	android/os/Message:what	I
            //     4: tableswitch	default:+24 -> 28, 1:+101->105, 2:+25->29
            //     29: aload_1
            //     30: getfield 27	android/os/Message:obj	Ljava/lang/Object;
            //     33: checkcast 29	android/os/AsyncResult
            //     36: astore 6
            //     38: aload_0
            //     39: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     42: invokestatic 33	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;)Ljava/lang/Object;
            //     45: astore 7
            //     47: aload 7
            //     49: monitorenter
            //     50: aload_0
            //     51: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     54: astore 9
            //     56: aload 6
            //     58: getfield 37	android/os/AsyncResult:exception	Ljava/lang/Throwable;
            //     61: ifnonnull +38 -> 99
            //     64: iconst_1
            //     65: istore 10
            //     67: aload 9
            //     69: iload 10
            //     71: invokestatic 41	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$102	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;Z)Z
            //     74: pop
            //     75: aload_0
            //     76: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     79: invokestatic 33	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;)Ljava/lang/Object;
            //     82: invokevirtual 46	java/lang/Object:notifyAll	()V
            //     85: aload 7
            //     87: monitorexit
            //     88: goto -60 -> 28
            //     91: astore 8
            //     93: aload 7
            //     95: monitorexit
            //     96: aload 8
            //     98: athrow
            //     99: iconst_0
            //     100: istore 10
            //     102: goto -35 -> 67
            //     105: aload_1
            //     106: getfield 27	android/os/Message:obj	Ljava/lang/Object;
            //     109: checkcast 29	android/os/AsyncResult
            //     112: astore_2
            //     113: aload_0
            //     114: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     117: invokestatic 33	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;)Ljava/lang/Object;
            //     120: astore_3
            //     121: aload_3
            //     122: monitorenter
            //     123: aload_2
            //     124: getfield 37	android/os/AsyncResult:exception	Ljava/lang/Throwable;
            //     127: ifnonnull +47 -> 174
            //     130: aload_0
            //     131: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     134: aload_0
            //     135: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     138: aload_2
            //     139: getfield 49	android/os/AsyncResult:result	Ljava/lang/Object;
            //     142: checkcast 51	java/util/ArrayList
            //     145: invokestatic 55	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$300	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;
            //     148: invokestatic 59	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$202	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;Ljava/util/List;)Ljava/util/List;
            //     151: pop
            //     152: aload_0
            //     153: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     156: invokestatic 33	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$000	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;)Ljava/lang/Object;
            //     159: invokevirtual 46	java/lang/Object:notifyAll	()V
            //     162: aload_3
            //     163: monitorexit
            //     164: goto -136 -> 28
            //     167: astore 4
            //     169: aload_3
            //     170: monitorexit
            //     171: aload 4
            //     173: athrow
            //     174: aload_0
            //     175: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     178: ldc 61
            //     180: invokevirtual 65	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:log	(Ljava/lang/String;)V
            //     183: aload_0
            //     184: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     187: invokestatic 69	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$200	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;)Ljava/util/List;
            //     190: ifnull -38 -> 152
            //     193: aload_0
            //     194: getfield 12	com/android/internal/telephony/cdma/RuimSmsInterfaceManager$1:this$0	Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;
            //     197: invokestatic 69	com/android/internal/telephony/cdma/RuimSmsInterfaceManager:access$200	(Lcom/android/internal/telephony/cdma/RuimSmsInterfaceManager;)Ljava/util/List;
            //     200: invokeinterface 74 1 0
            //     205: goto -53 -> 152
            //
            // Exception table:
            //     from	to	target	type
            //     50	96	91	finally
            //     123	171	167	finally
            //     174	205	167	finally
        }
    };
    private final Object mLock = new Object();
    private List<SmsRawData> mSms;
    private boolean mSuccess;

    public RuimSmsInterfaceManager(CDMAPhone paramCDMAPhone, SMSDispatcher paramSMSDispatcher)
    {
        super(paramCDMAPhone);
        this.mDispatcher = paramSMSDispatcher;
    }

    public boolean copyMessageToIccEf(int paramInt, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    {
        log("copyMessageToIccEf: status=" + paramInt + " ==> " + "pdu=(" + Arrays.toString(paramArrayOfByte1) + ")");
        enforceReceiveAndSend("Copying message to RUIM");
        synchronized (this.mLock)
        {
            this.mSuccess = false;
            Message localMessage = this.mHandler.obtainMessage(2);
            this.mPhone.mCM.writeSmsToRuim(paramInt, IccUtils.bytesToHexString(paramArrayOfByte1), localMessage);
            try
            {
                this.mLock.wait();
                return this.mSuccess;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    log("interrupted while trying to update by index");
            }
        }
    }

    public boolean disableCellBroadcast(int paramInt)
    {
        Log.e("CDMA", "Error! Not implemented for CDMA.");
        return false;
    }

    public boolean disableCellBroadcastRange(int paramInt1, int paramInt2)
    {
        Log.e("CDMA", "Error! Not implemented for CDMA.");
        return false;
    }

    public void dispose()
    {
    }

    public boolean enableCellBroadcast(int paramInt)
    {
        Log.e("CDMA", "Error! Not implemented for CDMA.");
        return false;
    }

    public boolean enableCellBroadcastRange(int paramInt1, int paramInt2)
    {
        Log.e("CDMA", "Error! Not implemented for CDMA.");
        return false;
    }

    protected void finalize()
    {
        try
        {
            super.finalize();
            Log.d("CDMA", "RuimSmsInterfaceManager finalized");
            return;
        }
        catch (Throwable localThrowable)
        {
            while (true)
                Log.e("CDMA", "Error while finalizing:", localThrowable);
        }
    }

    public List<SmsRawData> getAllMessagesFromIccEf()
    {
        log("getAllMessagesFromEF");
        this.mPhone.getContext().enforceCallingPermission("android.permission.RECEIVE_SMS", "Reading messages from RUIM");
        synchronized (this.mLock)
        {
            Message localMessage = this.mHandler.obtainMessage(1);
            this.mPhone.getIccFileHandler().loadEFLinearFixedAll(28476, localMessage);
            try
            {
                this.mLock.wait();
                return this.mSms;
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    log("interrupted while trying to load from the RUIM");
            }
        }
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[RuimSmsInterfaceManager] " + paramString);
    }

    public boolean updateMessageOnIccEf(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
    {
        log("updateMessageOnIccEf: index=" + paramInt1 + " status=" + paramInt2 + " ==> " + "(" + paramArrayOfByte + ")");
        enforceReceiveAndSend("Updating message on RUIM");
        Message localMessage;
        synchronized (this.mLock)
        {
            this.mSuccess = false;
            localMessage = this.mHandler.obtainMessage(2);
            if (paramInt2 == 0)
                this.mPhone.mCM.deleteSmsOnRuim(paramInt1, localMessage);
        }
        try
        {
            while (true)
            {
                this.mLock.wait();
                return this.mSuccess;
                byte[] arrayOfByte = makeSmsRecordData(paramInt2, paramArrayOfByte);
                this.mPhone.getIccFileHandler().updateEFLinearFixed(28476, paramInt1, arrayOfByte, null, localMessage);
            }
            localObject2 = finally;
            throw localObject2;
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
                log("interrupted while trying to update by index");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.RuimSmsInterfaceManager
 * JD-Core Version:        0.6.2
 */