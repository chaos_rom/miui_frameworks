package com.android.internal.telephony.cdma;

import android.util.Log;
import com.android.internal.telephony.Connection;

public class CdmaCallWaitingNotification
{
    static final String LOG_TAG = "CDMA";
    public int alertPitch = 0;
    public int isPresent = 0;
    public String name = null;
    public int namePresentation = 0;
    public String number = null;
    public int numberPlan = 0;
    public int numberPresentation = 0;
    public int numberType = 0;
    public int signal = 0;
    public int signalType = 0;

    public static int presentationFromCLIP(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            Log.d("CDMA", "Unexpected presentation " + paramInt);
            i = Connection.PRESENTATION_UNKNOWN;
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            return i;
            i = Connection.PRESENTATION_ALLOWED;
            continue;
            i = Connection.PRESENTATION_RESTRICTED;
            continue;
            i = Connection.PRESENTATION_UNKNOWN;
        }
    }

    public String toString()
    {
        return super.toString() + "Call Waiting Notification    " + " number: " + this.number + " numberPresentation: " + this.numberPresentation + " name: " + this.name + " namePresentation: " + this.namePresentation + " numberType: " + this.numberType + " numberPlan: " + this.numberPlan + " isPresent: " + this.isPresent + " signalType: " + this.signalType + " alertPitch: " + this.alertPitch + " signal: " + this.signal;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaCallWaitingNotification
 * JD-Core Version:        0.6.2
 */