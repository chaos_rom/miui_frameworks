package com.android.internal.telephony.cdma;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Telephony.Carriers;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.OperatorInfo;
import com.android.internal.telephony.Phone.DataState;
import com.android.internal.telephony.Phone.State;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.PhoneNotifier;
import com.android.internal.telephony.PhoneProxy;
import com.android.internal.telephony.SMSDispatcher;
import com.android.internal.telephony.gsm.GsmSMSDispatcher;
import com.android.internal.telephony.gsm.SmsMessage;
import com.android.internal.telephony.ims.IsimRecords;
import com.android.internal.telephony.uicc.UiccController;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicReference;

public class CDMALTEPhone extends CDMAPhone
{
    private static final boolean DBG = true;
    static final String LOG_TAG = "CDMA";
    SMSDispatcher m3gppSMS = new GsmSMSDispatcher(this, this.mSmsStorageMonitor, this.mSmsUsageMonitor);

    public CDMALTEPhone(Context paramContext, CommandsInterface paramCommandsInterface, PhoneNotifier paramPhoneNotifier)
    {
        super(paramContext, paramCommandsInterface, paramPhoneNotifier, false);
        this.mIccRecords.registerForNewSms(this, 29, null);
    }

    private void handleSetSelectNetwork(AsyncResult paramAsyncResult)
    {
        if (!(paramAsyncResult.userObj instanceof NetworkSelectMessage))
            Log.e("CDMA", "unexpected result from user object.");
        while (true)
        {
            return;
            NetworkSelectMessage localNetworkSelectMessage = (NetworkSelectMessage)paramAsyncResult.userObj;
            if (localNetworkSelectMessage.message != null)
            {
                log("sending original message to recipient");
                AsyncResult.forMessage(localNetworkSelectMessage.message, paramAsyncResult.result, paramAsyncResult.exception);
                localNetworkSelectMessage.message.sendToTarget();
            }
            SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
            localEditor.putString("network_selection_key", localNetworkSelectMessage.operatorNumeric);
            localEditor.putString("network_selection_name_key", localNetworkSelectMessage.operatorAlphaLong);
            if (!localEditor.commit())
                Log.e("CDMA", "failed to commit network selection preference");
        }
    }

    public void dispose()
    {
        synchronized (PhoneProxy.lockForRadioTechnologyChange)
        {
            super.dispose();
            this.m3gppSMS.dispose();
            this.mIccRecords.unregisterForNewSms(this);
            return;
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("CDMALTEPhone extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" m3gppSMS=" + this.m3gppSMS);
    }

    public void getAvailableNetworks(Message paramMessage)
    {
        this.mCM.getAvailableNetworks(paramMessage);
    }

    public Phone.DataState getDataConnectionState(String paramString)
    {
        Phone.DataState localDataState = Phone.DataState.DISCONNECTED;
        if (this.mSST == null)
            localDataState = Phone.DataState.DISCONNECTED;
        while (true)
        {
            log("getDataConnectionState apnType=" + paramString + " ret=" + localDataState);
            return localDataState;
            if (!this.mDataConnectionTracker.isApnTypeEnabled(paramString))
                localDataState = Phone.DataState.DISCONNECTED;
            else
                switch (1.$SwitchMap$com$android$internal$telephony$DataConnectionTracker$State[this.mDataConnectionTracker.getState(paramString).ordinal()])
                {
                default:
                    break;
                case 1:
                case 2:
                    localDataState = Phone.DataState.DISCONNECTED;
                    break;
                case 3:
                case 4:
                    if ((this.mCT.state != Phone.State.IDLE) && (!this.mSST.isConcurrentVoiceAndDataAllowed()))
                        localDataState = Phone.DataState.SUSPENDED;
                    else
                        localDataState = Phone.DataState.CONNECTED;
                    break;
                case 5:
                case 6:
                case 7:
                    localDataState = Phone.DataState.CONNECTING;
                }
        }
    }

    public String getDeviceSvn()
    {
        return this.mImeiSv;
    }

    public String getImei()
    {
        return this.mImei;
    }

    public IsimRecords getIsimRecords()
    {
        return this.mIccRecords.getIsimRecords();
    }

    public String getMsisdn()
    {
        return this.mIccRecords.getMsisdnNumber();
    }

    public String getSubscriberId()
    {
        return this.mIccRecords.getIMSI();
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
            super.handleMessage(paramMessage);
        case 16:
        case 29:
        }
        while (true)
        {
            return;
            handleSetSelectNetwork((AsyncResult)paramMessage.obj);
            continue;
            AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
            this.m3gppSMS.dispatchMessage((SmsMessage)localAsyncResult.result);
        }
    }

    protected void initSstIcc()
    {
        this.mIccCard.set(UiccController.getInstance(this).getIccCard());
        this.mIccRecords = ((IccCard)this.mIccCard.get()).getIccRecords();
        this.mSST = new CdmaLteServiceStateTracker(this);
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[CDMALTEPhone] " + paramString);
    }

    public void removeReferences()
    {
        super.removeReferences();
        this.m3gppSMS = null;
    }

    public void requestIsimAuthentication(String paramString, Message paramMessage)
    {
        this.mCM.requestIsimAuthentication(paramString, paramMessage);
    }

    public void selectNetworkManually(OperatorInfo paramOperatorInfo, Message paramMessage)
    {
        NetworkSelectMessage localNetworkSelectMessage = new NetworkSelectMessage(null);
        localNetworkSelectMessage.message = paramMessage;
        localNetworkSelectMessage.operatorNumeric = paramOperatorInfo.getOperatorNumeric();
        localNetworkSelectMessage.operatorAlphaLong = paramOperatorInfo.getOperatorAlphaLong();
        Message localMessage = obtainMessage(16, localNetworkSelectMessage);
        this.mCM.setNetworkSelectionModeManual(paramOperatorInfo.getOperatorNumeric(), localMessage);
    }

    public boolean updateCurrentCarrierInProvider()
    {
        boolean bool;
        if (this.mIccRecords != null)
            try
            {
                Uri localUri = Uri.withAppendedPath(Telephony.Carriers.CONTENT_URI, "current");
                ContentValues localContentValues = new ContentValues();
                String str = this.mIccRecords.getOperatorNumeric();
                localContentValues.put("numeric", str);
                log("updateCurrentCarrierInProvider from UICC: numeric=" + str);
                this.mContext.getContentResolver().insert(localUri, localContentValues);
                bool = true;
                return bool;
            }
            catch (SQLException localSQLException)
            {
                Log.e("CDMA", "[CDMALTEPhone] Can't store current operator ret false", localSQLException);
            }
        while (true)
        {
            bool = false;
            break;
            log("updateCurrentCarrierInProvider mIccRecords == null ret false");
        }
    }

    private static class NetworkSelectMessage
    {
        public Message message;
        public String operatorAlphaLong;
        public String operatorNumeric;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CDMALTEPhone
 * JD-Core Version:        0.6.2
 */