package com.android.internal.telephony.cdma.sms;

import android.content.res.Resources;
import android.telephony.SmsCbCmasInfo;
import android.telephony.cdma.CdmaSmsCbProgramData;
import android.telephony.cdma.CdmaSmsCbProgramResults;
import android.text.format.Time;
import android.util.Log;
import android.util.SparseIntArray;
import com.android.internal.telephony.EncodeException;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.IccUtils;
import com.android.internal.telephony.SmsAddress;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsMessageBase.TextEncodingDetails;
import com.android.internal.telephony.gsm.SmsMessage;
import com.android.internal.util.BitwiseInputStream;
import com.android.internal.util.BitwiseInputStream.AccessException;
import com.android.internal.util.BitwiseOutputStream;
import com.android.internal.util.BitwiseOutputStream.AccessException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TimeZone;

public final class BearerData
{
    public static final int ALERT_DEFAULT = 0;
    public static final int ALERT_HIGH_PRIO = 3;
    public static final int ALERT_LOW_PRIO = 1;
    public static final int ALERT_MEDIUM_PRIO = 2;
    public static final int DISPLAY_MODE_DEFAULT = 1;
    public static final int DISPLAY_MODE_IMMEDIATE = 0;
    public static final int DISPLAY_MODE_USER = 2;
    public static final int ERROR_NONE = 0;
    public static final int ERROR_PERMANENT = 3;
    public static final int ERROR_TEMPORARY = 2;
    public static final int ERROR_UNDEFINED = 255;
    public static final int LANGUAGE_CHINESE = 6;
    public static final int LANGUAGE_ENGLISH = 1;
    public static final int LANGUAGE_FRENCH = 2;
    public static final int LANGUAGE_HEBREW = 7;
    public static final int LANGUAGE_JAPANESE = 4;
    public static final int LANGUAGE_KOREAN = 5;
    public static final int LANGUAGE_SPANISH = 3;
    public static final int LANGUAGE_UNKNOWN = 0;
    private static final String LOG_TAG = "SMS";
    public static final int MESSAGE_TYPE_CANCELLATION = 3;
    public static final int MESSAGE_TYPE_DELIVER = 1;
    public static final int MESSAGE_TYPE_DELIVERY_ACK = 4;
    public static final int MESSAGE_TYPE_DELIVER_REPORT = 7;
    public static final int MESSAGE_TYPE_READ_ACK = 6;
    public static final int MESSAGE_TYPE_SUBMIT = 2;
    public static final int MESSAGE_TYPE_SUBMIT_REPORT = 8;
    public static final int MESSAGE_TYPE_USER_ACK = 5;
    public static final int PRIORITY_EMERGENCY = 3;
    public static final int PRIORITY_INTERACTIVE = 1;
    public static final int PRIORITY_NORMAL = 0;
    public static final int PRIORITY_URGENT = 2;
    public static final int PRIVACY_CONFIDENTIAL = 2;
    public static final int PRIVACY_NOT_RESTRICTED = 0;
    public static final int PRIVACY_RESTRICTED = 1;
    public static final int PRIVACY_SECRET = 3;
    public static final int RELATIVE_TIME_DAYS_LIMIT = 196;
    public static final int RELATIVE_TIME_HOURS_LIMIT = 167;
    public static final int RELATIVE_TIME_INDEFINITE = 245;
    public static final int RELATIVE_TIME_MINS_LIMIT = 143;
    public static final int RELATIVE_TIME_MOBILE_INACTIVE = 247;
    public static final int RELATIVE_TIME_NOW = 246;
    public static final int RELATIVE_TIME_RESERVED = 248;
    public static final int RELATIVE_TIME_WEEKS_LIMIT = 244;
    public static final int STATUS_ACCEPTED = 0;
    public static final int STATUS_BLOCKED_DESTINATION = 7;
    public static final int STATUS_CANCELLED = 3;
    public static final int STATUS_CANCEL_FAILED = 6;
    public static final int STATUS_DELIVERED = 2;
    public static final int STATUS_DEPOSITED_TO_INTERNET = 1;
    public static final int STATUS_DUPLICATE_MESSAGE = 9;
    public static final int STATUS_INVALID_DESTINATION = 10;
    public static final int STATUS_MESSAGE_EXPIRED = 13;
    public static final int STATUS_NETWORK_CONGESTION = 4;
    public static final int STATUS_NETWORK_ERROR = 5;
    public static final int STATUS_TEXT_TOO_LONG = 8;
    public static final int STATUS_UNDEFINED = 255;
    public static final int STATUS_UNKNOWN_ERROR = 31;
    private static final byte SUBPARAM_ALERT_ON_MESSAGE_DELIVERY = 12;
    private static final byte SUBPARAM_CALLBACK_NUMBER = 14;
    private static final byte SUBPARAM_DEFERRED_DELIVERY_TIME_ABSOLUTE = 6;
    private static final byte SUBPARAM_DEFERRED_DELIVERY_TIME_RELATIVE = 7;
    private static final byte SUBPARAM_LANGUAGE_INDICATOR = 13;
    private static final byte SUBPARAM_MESSAGE_CENTER_TIME_STAMP = 3;
    private static final byte SUBPARAM_MESSAGE_DEPOSIT_INDEX = 17;
    private static final byte SUBPARAM_MESSAGE_DISPLAY_MODE = 15;
    private static final byte SUBPARAM_MESSAGE_IDENTIFIER = 0;
    private static final byte SUBPARAM_MESSAGE_STATUS = 20;
    private static final byte SUBPARAM_NUMBER_OF_MESSAGES = 11;
    private static final byte SUBPARAM_PRIORITY_INDICATOR = 8;
    private static final byte SUBPARAM_PRIVACY_INDICATOR = 9;
    private static final byte SUBPARAM_REPLY_OPTION = 10;
    private static final byte SUBPARAM_SERVICE_CATEGORY_PROGRAM_DATA = 18;
    private static final byte SUBPARAM_SERVICE_CATEGORY_PROGRAM_RESULTS = 19;
    private static final byte SUBPARAM_USER_DATA = 1;
    private static final byte SUBPARAM_USER_RESPONSE_CODE = 2;
    private static final byte SUBPARAM_VALIDITY_PERIOD_ABSOLUTE = 4;
    private static final byte SUBPARAM_VALIDITY_PERIOD_RELATIVE = 5;
    public int alert = 0;
    public boolean alertIndicatorSet = false;
    public CdmaSmsAddress callbackNumber;
    public SmsCbCmasInfo cmasWarningInfo;
    public TimeStamp deferredDeliveryTimeAbsolute;
    public int deferredDeliveryTimeRelative;
    public boolean deferredDeliveryTimeRelativeSet;
    public boolean deliveryAckReq;
    public int depositIndex;
    public int displayMode = 1;
    public boolean displayModeSet = false;
    public int errorClass = 255;
    public boolean hasUserDataHeader;
    public int language = 0;
    public boolean languageIndicatorSet = false;
    public int messageId;
    public int messageStatus = 255;
    public boolean messageStatusSet = false;
    public int messageType;
    public TimeStamp msgCenterTimeStamp;
    public int numberOfMessages;
    public int priority = 0;
    public boolean priorityIndicatorSet = false;
    public int privacy = 0;
    public boolean privacyIndicatorSet = false;
    public boolean readAckReq;
    public boolean reportReq;
    public ArrayList<CdmaSmsCbProgramData> serviceCategoryProgramData;
    public ArrayList<CdmaSmsCbProgramResults> serviceCategoryProgramResults;
    public boolean userAckReq;
    public UserData userData;
    public int userResponseCode;
    public boolean userResponseCodeSet = false;
    public TimeStamp validityPeriodAbsolute;
    public int validityPeriodRelative;
    public boolean validityPeriodRelativeSet;

    public static SmsMessageBase.TextEncodingDetails calcTextEncodingDetails(CharSequence paramCharSequence, boolean paramBoolean)
    {
        int i = countAsciiSeptets(paramCharSequence, paramBoolean);
        SmsMessageBase.TextEncodingDetails localTextEncodingDetails;
        if ((i != -1) && (i <= 160))
        {
            localTextEncodingDetails = new SmsMessageBase.TextEncodingDetails();
            localTextEncodingDetails.msgCount = 1;
            localTextEncodingDetails.codeUnitCount = i;
            localTextEncodingDetails.codeUnitsRemaining = (160 - i);
            localTextEncodingDetails.codeUnitSize = 1;
        }
        do
        {
            return localTextEncodingDetails;
            localTextEncodingDetails = SmsMessage.calculateLength(paramCharSequence, paramBoolean);
        }
        while ((localTextEncodingDetails.msgCount != 1) || (localTextEncodingDetails.codeUnitSize != 1));
        localTextEncodingDetails.codeUnitCount = paramCharSequence.length();
        int j = 2 * localTextEncodingDetails.codeUnitCount;
        if (j > 140)
            localTextEncodingDetails.msgCount = ((j + 133) / 134);
        for (localTextEncodingDetails.codeUnitsRemaining = ((134 * localTextEncodingDetails.msgCount - j) / 2); ; localTextEncodingDetails.codeUnitsRemaining = ((140 - j) / 2))
        {
            localTextEncodingDetails.codeUnitSize = 3;
            break;
            localTextEncodingDetails.msgCount = 1;
        }
    }

    private static int countAsciiSeptets(CharSequence paramCharSequence, boolean paramBoolean)
    {
        int i = paramCharSequence.length();
        if (paramBoolean);
        label50: 
        while (true)
        {
            return i;
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label50;
                if (UserData.charToAscii.get(paramCharSequence.charAt(j), -1) == -1)
                {
                    i = -1;
                    break;
                }
            }
        }
    }

    public static BearerData decode(byte[] paramArrayOfByte)
    {
        return decode(paramArrayOfByte, 0);
    }

    public static BearerData decode(byte[] paramArrayOfByte, int paramInt)
    {
        try
        {
            localBitwiseInputStream = new BitwiseInputStream(paramArrayOfByte);
            localBearerData = new BearerData();
            i = 0;
            if (localBitwiseInputStream.available() > 0)
            {
                j = localBitwiseInputStream.read(8);
                k = 1 << j;
                if ((i & k) != 0)
                    throw new CodingException("illegal duplicate subparameter (" + j + ")");
            }
        }
        catch (BitwiseInputStream.AccessException localAccessException)
        {
            int j;
            Log.e("SMS", "BearerData decode failed: " + localAccessException);
            localBearerData = null;
            return localBearerData;
            switch (j)
            {
            case 16:
            case 19:
            default:
                throw new CodingException("unsupported bearer data subparameter (" + j + ")");
            case 0:
            case 1:
            case 2:
            case 10:
            case 11:
            case 14:
            case 20:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 9:
            case 13:
            case 15:
            case 8:
            case 12:
            case 17:
            case 18:
            }
        }
        catch (CodingException localCodingException)
        {
            while (true)
            {
                BitwiseInputStream localBitwiseInputStream;
                BearerData localBearerData;
                int i;
                int k;
                Log.e("SMS", "BearerData decode failed: " + localCodingException);
                continue;
                boolean bool = decodeMessageId(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeUserData(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeUserResponseCode(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeReplyOption(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeMsgCount(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeCallbackNumber(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeMsgStatus(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeMsgCenterTimeStamp(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeValidityAbs(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeValidityRel(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeDeferredDeliveryAbs(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeDeferredDeliveryRel(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodePrivacyIndicator(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeLanguageIndicator(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeDisplayMode(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodePriorityIndicator(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeMsgDeliveryAlert(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeDepositIndex(localBearerData, localBitwiseInputStream);
                break label587;
                bool = decodeServiceCategoryProgramData(localBearerData, localBitwiseInputStream);
                break label587;
                if ((i & 0x1) == 0)
                    throw new CodingException("missing MESSAGE_IDENTIFIER subparam");
                if (localBearerData.userData != null)
                    if (isCmasAlertCategory(paramInt))
                    {
                        decodeCmasUserData(localBearerData, paramInt);
                    }
                    else if (localBearerData.userData.msgEncoding == 1)
                    {
                        if ((0x2 ^ (i ^ 0x1)) != 0)
                            Log.e("SMS", "IS-91 must occur without extra subparams (" + i + ")");
                        decodeIs91(localBearerData);
                    }
                    else
                    {
                        decodeUserDataPayload(localBearerData.userData, localBearerData.hasUserDataHeader);
                        continue;
                        label587: if (bool)
                            i |= k;
                    }
            }
        }
    }

    private static String decode7bitAscii(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws BearerData.CodingException
    {
        int i = paramInt1 * 8;
        StringBuffer localStringBuffer;
        BitwiseInputStream localBitwiseInputStream;
        try
        {
            localStringBuffer = new StringBuffer(paramInt2);
            localBitwiseInputStream = new BitwiseInputStream(paramArrayOfByte);
            int j = i * 8 + paramInt2 * 7;
            if (localBitwiseInputStream.available() < j)
                throw new CodingException("insufficient data (wanted " + j + " bits, but only have " + localBitwiseInputStream.available() + ")");
        }
        catch (BitwiseInputStream.AccessException localAccessException)
        {
            throw new CodingException("7bit ASCII decode failed: " + localAccessException);
        }
        localBitwiseInputStream.skip(i);
        for (int k = 0; ; k++)
            if (k < paramInt2)
            {
                int m = localBitwiseInputStream.read(7);
                if ((m >= 32) && (m <= UserData.ASCII_MAP_MAX_INDEX))
                    localStringBuffer.append(UserData.ASCII_MAP[(m - 32)]);
                else if (m == 10)
                    localStringBuffer.append('\n');
                else if (m == 13)
                    localStringBuffer.append('\r');
                else
                    localStringBuffer.append(' ');
            }
            else
            {
                String str = localStringBuffer.toString();
                return str;
            }
    }

    private static String decode7bitGsm(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws BearerData.CodingException
    {
        int i = paramInt1 * 8;
        int j = (i + 6) / 7;
        String str = GsmAlphabet.gsm7BitPackedToString(paramArrayOfByte, paramInt1, paramInt2 - j, j * 7 - i, 0, 0);
        if (str == null)
            throw new CodingException("7bit GSM decoding failed");
        return str;
    }

    private static boolean decodeCallbackNumber(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        int i = 8 * paramBitwiseInputStream.read(8);
        CdmaSmsAddress localCdmaSmsAddress = new CdmaSmsAddress();
        localCdmaSmsAddress.digitMode = paramBitwiseInputStream.read(1);
        int j = 4;
        int k = 1;
        if (localCdmaSmsAddress.digitMode == 1)
        {
            localCdmaSmsAddress.ton = paramBitwiseInputStream.read(3);
            localCdmaSmsAddress.numberPlan = paramBitwiseInputStream.read(4);
            j = 8;
            k = (byte)8;
        }
        localCdmaSmsAddress.numberOfDigits = paramBitwiseInputStream.read(8);
        int m = i - (k + 8);
        int n = j * localCdmaSmsAddress.numberOfDigits;
        int i1 = m - n;
        if (m < n)
            throw new CodingException("CALLBACK_NUMBER subparam encoding size error (remainingBits + " + m + ", dataBits + " + n + ", paddingBits + " + i1 + ")");
        localCdmaSmsAddress.origBytes = paramBitwiseInputStream.readByteArray(n);
        paramBitwiseInputStream.skip(i1);
        decodeSmsAddress(localCdmaSmsAddress);
        paramBearerData.callbackNumber = localCdmaSmsAddress;
        return true;
    }

    private static String decodeCharset(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, String paramString)
        throws BearerData.CodingException
    {
        if ((paramInt2 < 0) || (paramInt1 + paramInt2 * paramInt3 > paramArrayOfByte.length))
        {
            int i = paramInt1 % paramInt3;
            int j = (paramArrayOfByte.length - paramInt1 - i) / paramInt3;
            if (j < 0)
                throw new CodingException(paramString + " decode failed: offset out of range");
            Log.e("SMS", paramString + " decode error: offset = " + paramInt1 + " numFields = " + paramInt2 + " data.length = " + paramArrayOfByte.length + " maxNumFields = " + j);
            paramInt2 = j;
        }
        try
        {
            String str = new String(paramArrayOfByte, paramInt1, paramInt2 * paramInt3, paramString);
            return str;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
            throw new CodingException(paramString + " decode failed: " + localUnsupportedEncodingException);
        }
    }

    private static void decodeCmasUserData(BearerData paramBearerData, int paramInt)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        BitwiseInputStream localBitwiseInputStream = new BitwiseInputStream(paramBearerData.userData.payload);
        if (localBitwiseInputStream.available() < 8)
            throw new CodingException("emergency CB with no CMAE_protocol_version");
        int i = localBitwiseInputStream.read(8);
        if (i != 0)
            throw new CodingException("unsupported CMAE_protocol_version " + i);
        int j = serviceCategoryToCmasMessageClass(paramInt);
        int k = -1;
        int m = -1;
        int n = -1;
        int i1 = -1;
        int i2 = -1;
        while (localBitwiseInputStream.available() >= 16)
        {
            int i3 = localBitwiseInputStream.read(8);
            int i4 = localBitwiseInputStream.read(8);
            switch (i3)
            {
            default:
                Log.w("SMS", "skipping unsupported CMAS record type " + i3);
                localBitwiseInputStream.skip(i4 * 8);
                break;
            case 0:
                UserData localUserData = new UserData();
                localUserData.msgEncoding = localBitwiseInputStream.read(5);
                localUserData.msgEncodingSet = true;
                localUserData.msgType = 0;
                int i5;
                switch (localUserData.msgEncoding)
                {
                case 1:
                case 5:
                case 6:
                case 7:
                default:
                    i5 = 0;
                case 0:
                case 8:
                case 2:
                case 3:
                case 9:
                case 4:
                }
                while (true)
                {
                    localUserData.numFields = i5;
                    localUserData.payload = localBitwiseInputStream.readByteArray(-5 + i4 * 8);
                    decodeUserDataPayload(localUserData, false);
                    paramBearerData.userData = localUserData;
                    break;
                    i5 = i4 - 1;
                    continue;
                    i5 = (-5 + i4 * 8) / 7;
                    continue;
                    i5 = (i4 - 1) / 2;
                }
            case 1:
                k = localBitwiseInputStream.read(8);
                m = localBitwiseInputStream.read(8);
                n = localBitwiseInputStream.read(4);
                i1 = localBitwiseInputStream.read(4);
                i2 = localBitwiseInputStream.read(4);
                localBitwiseInputStream.skip(-28 + i4 * 8);
            }
        }
        paramBearerData.cmasWarningInfo = new SmsCbCmasInfo(j, k, m, n, i1, i2);
    }

    private static boolean decodeDeferredDeliveryAbs(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 48)
        {
            i -= 48;
            bool = true;
            paramBearerData.deferredDeliveryTimeAbsolute = TimeStamp.fromByteArray(paramBitwiseInputStream.readByteArray(48));
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("DEFERRED_DELIVERY_TIME_ABSOLUTE decode ");
            if (!bool)
                break label107;
        }
        label107: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            return bool;
        }
    }

    private static boolean decodeDeferredDeliveryRel(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.validityPeriodRelative = paramBitwiseInputStream.read(8);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("DEFERRED_DELIVERY_TIME_RELATIVE decode ");
            if (!bool)
                break label109;
        }
        label109: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.validityPeriodRelativeSet = bool;
            return bool;
        }
    }

    private static boolean decodeDepositIndex(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 16)
        {
            i -= 16;
            bool = true;
            paramBearerData.depositIndex = (paramBitwiseInputStream.read(8) << 8 | paramBitwiseInputStream.read(8));
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("MESSAGE_DEPOSIT_INDEX decode ");
            if (!bool)
                break label114;
        }
        label114: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            return bool;
        }
    }

    private static boolean decodeDisplayMode(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.displayMode = paramBitwiseInputStream.read(2);
            paramBitwiseInputStream.skip(6);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("DISPLAY_MODE decode ");
            if (!bool)
                break label114;
        }
        label114: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.displayModeSet = bool;
            return bool;
        }
    }

    private static String decodeDtmfSmsAddress(byte[] paramArrayOfByte, int paramInt)
        throws BearerData.CodingException
    {
        StringBuffer localStringBuffer = new StringBuffer(paramInt);
        int i = 0;
        if (i < paramInt)
        {
            int j = 0xF & paramArrayOfByte[(i / 2)] >>> 4 - 4 * (i % 2);
            if ((j >= 1) && (j <= 9))
                localStringBuffer.append(Integer.toString(j, 10));
            while (true)
            {
                i++;
                break;
                if (j == 10)
                {
                    localStringBuffer.append('0');
                }
                else if (j == 11)
                {
                    localStringBuffer.append('*');
                }
                else
                {
                    if (j != 12)
                        break label116;
                    localStringBuffer.append('#');
                }
            }
            label116: throw new CodingException("invalid SMS address DTMF code (" + j + ")");
        }
        return localStringBuffer.toString();
    }

    private static void decodeIs91(BearerData paramBearerData)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        switch (paramBearerData.userData.msgType)
        {
        default:
            throw new CodingException("unsupported IS-91 message type (" + paramBearerData.userData.msgType + ")");
        case 130:
            decodeIs91VoicemailStatus(paramBearerData);
        case 132:
        case 131:
        case 133:
        }
        while (true)
        {
            return;
            decodeIs91Cli(paramBearerData);
            continue;
            decodeIs91ShortMessage(paramBearerData);
        }
    }

    private static void decodeIs91Cli(BearerData paramBearerData)
        throws BearerData.CodingException
    {
        int i = new BitwiseInputStream(paramBearerData.userData.payload).available() / 4;
        int j = paramBearerData.userData.numFields;
        if ((i > 14) || (i < 3) || (i < j))
            throw new CodingException("IS-91 voicemail status decoding failed");
        CdmaSmsAddress localCdmaSmsAddress = new CdmaSmsAddress();
        localCdmaSmsAddress.digitMode = 0;
        localCdmaSmsAddress.origBytes = paramBearerData.userData.payload;
        localCdmaSmsAddress.numberOfDigits = ((byte)j);
        decodeSmsAddress(localCdmaSmsAddress);
        paramBearerData.callbackNumber = localCdmaSmsAddress;
    }

    private static void decodeIs91ShortMessage(BearerData paramBearerData)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        BitwiseInputStream localBitwiseInputStream = new BitwiseInputStream(paramBearerData.userData.payload);
        int i = localBitwiseInputStream.available() / 6;
        int j = paramBearerData.userData.numFields;
        if ((j > 14) || (i < j))
            throw new CodingException("IS-91 short message decoding failed");
        StringBuffer localStringBuffer = new StringBuffer(i);
        for (int k = 0; k < j; k++)
            localStringBuffer.append(UserData.ASCII_MAP[localBitwiseInputStream.read(6)]);
        paramBearerData.userData.payloadStr = localStringBuffer.toString();
    }

    private static void decodeIs91VoicemailStatus(BearerData paramBearerData)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        BitwiseInputStream localBitwiseInputStream = new BitwiseInputStream(paramBearerData.userData.payload);
        int i = localBitwiseInputStream.available() / 6;
        int j = paramBearerData.userData.numFields;
        if ((i > 14) || (i < 3) || (i < j))
            throw new CodingException("IS-91 voicemail status decoding failed");
        char c;
        try
        {
            localStringBuffer = new StringBuffer(i);
            while (localBitwiseInputStream.available() >= 6)
                localStringBuffer.append(UserData.ASCII_MAP[localBitwiseInputStream.read(6)]);
        }
        catch (NumberFormatException localNumberFormatException)
        {
            StringBuffer localStringBuffer;
            throw new CodingException("IS-91 voicemail status decoding failed: " + localNumberFormatException);
            String str = localStringBuffer.toString();
            paramBearerData.numberOfMessages = Integer.parseInt(str.substring(0, 2));
            c = str.charAt(2);
            if (c == ' ');
            for (paramBearerData.priority = 0; ; paramBearerData.priority = 2)
            {
                paramBearerData.priorityIndicatorSet = true;
                paramBearerData.userData.payloadStr = str.substring(3, j - 3);
                return;
                if (c != '!')
                    break;
            }
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
            throw new CodingException("IS-91 voicemail status decoding failed: " + localIndexOutOfBoundsException);
        }
        throw new CodingException("IS-91 voicemail status decoding failed: illegal priority setting (" + c + ")");
    }

    private static boolean decodeLanguageIndicator(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.language = paramBitwiseInputStream.read(8);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("LANGUAGE_INDICATOR decode ");
            if (!bool)
                break label109;
        }
        label109: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.languageIndicatorSet = bool;
            return bool;
        }
    }

    private static String decodeLatin(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws BearerData.CodingException
    {
        return decodeCharset(paramArrayOfByte, paramInt1, paramInt2, 1, "ISO-8859-1");
    }

    private static boolean decodeMessageId(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        int i = 1;
        boolean bool = false;
        int j = 8 * paramBitwiseInputStream.read(8);
        StringBuilder localStringBuilder;
        if (j >= 24)
        {
            j -= 24;
            bool = true;
            paramBearerData.messageType = paramBitwiseInputStream.read(4);
            paramBearerData.messageId = (paramBitwiseInputStream.read(8) << 8);
            paramBearerData.messageId |= paramBitwiseInputStream.read(8);
            if (paramBitwiseInputStream.read(i) == i)
            {
                paramBearerData.hasUserDataHeader = i;
                paramBitwiseInputStream.skip(3);
            }
        }
        else if ((!bool) || (j > 0))
        {
            localStringBuilder = new StringBuilder().append("MESSAGE_IDENTIFIER decode ");
            if (!bool)
                break label162;
        }
        label162: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + j + ")");
            paramBitwiseInputStream.skip(j);
            return bool;
            i = 0;
            break;
        }
    }

    private static boolean decodeMsgCenterTimeStamp(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 48)
        {
            i -= 48;
            bool = true;
            paramBearerData.msgCenterTimeStamp = TimeStamp.fromByteArray(paramBitwiseInputStream.readByteArray(48));
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("MESSAGE_CENTER_TIME_STAMP decode ");
            if (!bool)
                break label107;
        }
        label107: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            return bool;
        }
    }

    private static boolean decodeMsgCount(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.numberOfMessages = IccUtils.cdmaBcdByteToInt((byte)paramBitwiseInputStream.read(8));
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("NUMBER_OF_MESSAGES decode ");
            if (!bool)
                break label108;
        }
        label108: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            return bool;
        }
    }

    private static boolean decodeMsgDeliveryAlert(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.alert = paramBitwiseInputStream.read(2);
            paramBitwiseInputStream.skip(6);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("ALERT_ON_MESSAGE_DELIVERY decode ");
            if (!bool)
                break label114;
        }
        label114: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.alertIndicatorSet = bool;
            return bool;
        }
    }

    private static boolean decodeMsgStatus(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.errorClass = paramBitwiseInputStream.read(2);
            paramBearerData.messageStatus = paramBitwiseInputStream.read(6);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("MESSAGE_STATUS decode ");
            if (!bool)
                break label118;
        }
        label118: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.messageStatusSet = bool;
            return bool;
        }
    }

    private static boolean decodePriorityIndicator(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.priority = paramBitwiseInputStream.read(2);
            paramBitwiseInputStream.skip(6);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("PRIORITY_INDICATOR decode ");
            if (!bool)
                break label114;
        }
        label114: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.priorityIndicatorSet = bool;
            return bool;
        }
    }

    private static boolean decodePrivacyIndicator(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.privacy = paramBitwiseInputStream.read(2);
            paramBitwiseInputStream.skip(6);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("PRIVACY_INDICATOR decode ");
            if (!bool)
                break label114;
        }
        label114: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.privacyIndicatorSet = bool;
            return bool;
        }
    }

    private static boolean decodeReplyOption(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        int i = 1;
        boolean bool = false;
        int j = 8 * paramBitwiseInputStream.read(8);
        int k;
        int m;
        label57: int n;
        label75: label90: StringBuilder localStringBuilder;
        if (j >= 8)
        {
            j -= 8;
            bool = true;
            if (paramBitwiseInputStream.read(i) == i)
            {
                k = i;
                paramBearerData.userAckReq = k;
                if (paramBitwiseInputStream.read(i) != i)
                    break label180;
                m = i;
                paramBearerData.deliveryAckReq = m;
                if (paramBitwiseInputStream.read(i) != i)
                    break label186;
                n = i;
                paramBearerData.readAckReq = n;
                if (paramBitwiseInputStream.read(i) != i)
                    break label192;
                paramBearerData.reportReq = i;
                paramBitwiseInputStream.skip(4);
            }
        }
        else if ((!bool) || (j > 0))
        {
            localStringBuilder = new StringBuilder().append("REPLY_OPTION decode ");
            if (!bool)
                break label197;
        }
        label180: label186: label192: label197: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + j + ")");
            paramBitwiseInputStream.skip(j);
            return bool;
            k = 0;
            break;
            m = 0;
            break label57;
            n = 0;
            break label75;
            i = 0;
            break label90;
        }
    }

    private static boolean decodeServiceCategoryProgramData(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        if (paramBitwiseInputStream.available() < 13)
            throw new CodingException("SERVICE_CATEGORY_PROGRAM_DATA decode failed: only " + paramBitwiseInputStream.available() + " bits available");
        int i = 8 * paramBitwiseInputStream.read(8);
        int j = paramBitwiseInputStream.read(5);
        int k = i - 5;
        if (paramBitwiseInputStream.available() < k)
            throw new CodingException("SERVICE_CATEGORY_PROGRAM_DATA decode failed: only " + paramBitwiseInputStream.available() + " bits available (" + k + " bits expected)");
        ArrayList localArrayList = new ArrayList();
        for (boolean bool = false; k >= 48; bool = true)
        {
            int m = paramBitwiseInputStream.read(4);
            int n = paramBitwiseInputStream.read(8) << 8 | paramBitwiseInputStream.read(8);
            int i1 = paramBitwiseInputStream.read(8);
            int i2 = paramBitwiseInputStream.read(8);
            int i3 = paramBitwiseInputStream.read(4);
            int i4 = paramBitwiseInputStream.read(8);
            int i5 = k - 48;
            int i6 = getBitsForNumFields(j, i4);
            if (i5 < i6)
                throw new CodingException("category name is " + i6 + " bits in length," + " but there are only " + i5 + " bits available");
            UserData localUserData = new UserData();
            localUserData.msgEncoding = j;
            localUserData.msgEncodingSet = true;
            localUserData.numFields = i4;
            localUserData.payload = paramBitwiseInputStream.readByteArray(i6);
            k = i5 - i6;
            decodeUserDataPayload(localUserData, false);
            localArrayList.add(new CdmaSmsCbProgramData(m, n, i1, i2, i3, localUserData.payloadStr));
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (k > 0))
        {
            localStringBuilder = new StringBuilder().append("SERVICE_CATEGORY_PROGRAM_DATA decode ");
            if (!bool)
                break label441;
        }
        label441: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + k + ')');
            paramBitwiseInputStream.skip(k);
            paramBearerData.serviceCategoryProgramData = localArrayList;
            return bool;
        }
    }

    private static void decodeSmsAddress(CdmaSmsAddress paramCdmaSmsAddress)
        throws BearerData.CodingException
    {
        if (paramCdmaSmsAddress.digitMode == 1);
        while (true)
        {
            try
            {
                paramCdmaSmsAddress.address = new String(paramCdmaSmsAddress.origBytes, 0, paramCdmaSmsAddress.origBytes.length, "US-ASCII");
                return;
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                throw new CodingException("invalid SMS address ASCII code");
            }
            paramCdmaSmsAddress.address = decodeDtmfSmsAddress(paramCdmaSmsAddress.origBytes, paramCdmaSmsAddress.numberOfDigits);
        }
    }

    private static boolean decodeUserData(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException
    {
        int i = 8 * paramBitwiseInputStream.read(8);
        paramBearerData.userData = new UserData();
        paramBearerData.userData.msgEncoding = paramBitwiseInputStream.read(5);
        paramBearerData.userData.msgEncodingSet = true;
        paramBearerData.userData.msgType = 0;
        int j = 5;
        if ((paramBearerData.userData.msgEncoding == 1) || (paramBearerData.userData.msgEncoding == 10))
        {
            paramBearerData.userData.msgType = paramBitwiseInputStream.read(8);
            j += 8;
        }
        paramBearerData.userData.numFields = paramBitwiseInputStream.read(8);
        int k = i - (j + 8);
        paramBearerData.userData.payload = paramBitwiseInputStream.readByteArray(k);
        return true;
    }

    private static void decodeUserDataPayload(UserData paramUserData, boolean paramBoolean)
        throws BearerData.CodingException
    {
        int i = 0;
        if (paramBoolean)
        {
            int k = 0xFF & paramUserData.payload[0];
            i = 0 + (k + 1);
            byte[] arrayOfByte2 = new byte[k];
            System.arraycopy(paramUserData.payload, 1, arrayOfByte2, 0, k);
            paramUserData.userDataHeader = SmsHeader.fromByteArray(arrayOfByte2);
        }
        int j;
        switch (paramUserData.msgEncoding)
        {
        case 1:
        case 5:
        case 6:
        case 7:
        default:
            throw new CodingException("unsupported user data encoding (" + paramUserData.msgEncoding + ")");
        case 0:
            boolean bool = Resources.getSystem().getBoolean(17891376);
            byte[] arrayOfByte1 = new byte[paramUserData.numFields];
            if (paramUserData.numFields < paramUserData.payload.length)
            {
                j = paramUserData.numFields;
                System.arraycopy(paramUserData.payload, 0, arrayOfByte1, 0, j);
                paramUserData.payload = arrayOfByte1;
                if (bool)
                    break label235;
                paramUserData.payloadStr = decodeLatin(paramUserData.payload, i, paramUserData.numFields);
            }
            break;
        case 2:
        case 3:
        case 4:
        case 9:
        case 8:
        }
        while (true)
        {
            return;
            j = paramUserData.payload.length;
            break;
            label235: paramUserData.payloadStr = decodeUtf8(paramUserData.payload, i, paramUserData.numFields);
            continue;
            paramUserData.payloadStr = decode7bitAscii(paramUserData.payload, i, paramUserData.numFields);
            continue;
            paramUserData.payloadStr = decodeUtf16(paramUserData.payload, i, paramUserData.numFields);
            continue;
            paramUserData.payloadStr = decode7bitGsm(paramUserData.payload, i, paramUserData.numFields);
            continue;
            paramUserData.payloadStr = decodeLatin(paramUserData.payload, i, paramUserData.numFields);
        }
    }

    private static boolean decodeUserResponseCode(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.userResponseCode = paramBitwiseInputStream.read(8);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("USER_RESPONSE_CODE decode ");
            if (!bool)
                break label109;
        }
        label109: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.userResponseCodeSet = bool;
            return bool;
        }
    }

    private static String decodeUtf16(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws BearerData.CodingException
    {
        return decodeCharset(paramArrayOfByte, paramInt1, paramInt2 - (paramInt1 + paramInt1 % 2) / 2, 2, "utf-16be");
    }

    private static String decodeUtf8(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws BearerData.CodingException
    {
        return decodeCharset(paramArrayOfByte, paramInt1, paramInt2, 1, "UTF-8");
    }

    private static boolean decodeValidityAbs(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 48)
        {
            i -= 48;
            bool = true;
            paramBearerData.validityPeriodAbsolute = TimeStamp.fromByteArray(paramBitwiseInputStream.readByteArray(48));
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("VALIDITY_PERIOD_ABSOLUTE decode ");
            if (!bool)
                break label107;
        }
        label107: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            return bool;
        }
    }

    private static boolean decodeValidityRel(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream)
        throws BitwiseInputStream.AccessException, BearerData.CodingException
    {
        boolean bool = false;
        int i = 8 * paramBitwiseInputStream.read(8);
        if (i >= 8)
        {
            i -= 8;
            bool = true;
            paramBearerData.deferredDeliveryTimeRelative = paramBitwiseInputStream.read(8);
        }
        StringBuilder localStringBuilder;
        if ((!bool) || (i > 0))
        {
            localStringBuilder = new StringBuilder().append("VALIDITY_PERIOD_RELATIVE decode ");
            if (!bool)
                break label109;
        }
        label109: for (String str = "succeeded"; ; str = "failed")
        {
            Log.d("SMS", str + " (extra bits = " + i + ")");
            paramBitwiseInputStream.skip(i);
            paramBearerData.deferredDeliveryTimeRelativeSet = bool;
            return bool;
        }
    }

    public static byte[] encode(BearerData paramBearerData)
    {
        boolean bool = true;
        if ((paramBearerData.userData != null) && (paramBearerData.userData.userDataHeader != null));
        while (true)
        {
            paramBearerData.hasUserDataHeader = bool;
            try
            {
                BitwiseOutputStream localBitwiseOutputStream = new BitwiseOutputStream(200);
                localBitwiseOutputStream.write(8, 0);
                encodeMessageId(paramBearerData, localBitwiseOutputStream);
                if (paramBearerData.userData != null)
                {
                    localBitwiseOutputStream.write(8, 1);
                    encodeUserData(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.callbackNumber != null)
                {
                    localBitwiseOutputStream.write(8, 14);
                    encodeCallbackNumber(paramBearerData, localBitwiseOutputStream);
                }
                if ((paramBearerData.userAckReq) || (paramBearerData.deliveryAckReq) || (paramBearerData.readAckReq) || (paramBearerData.reportReq))
                {
                    localBitwiseOutputStream.write(8, 10);
                    encodeReplyOption(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.numberOfMessages != 0)
                {
                    localBitwiseOutputStream.write(8, 11);
                    encodeMsgCount(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.validityPeriodRelativeSet)
                {
                    localBitwiseOutputStream.write(8, 5);
                    encodeValidityPeriodRel(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.privacyIndicatorSet)
                {
                    localBitwiseOutputStream.write(8, 9);
                    encodePrivacyIndicator(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.languageIndicatorSet)
                {
                    localBitwiseOutputStream.write(8, 13);
                    encodeLanguageIndicator(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.displayModeSet)
                {
                    localBitwiseOutputStream.write(8, 15);
                    encodeDisplayMode(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.priorityIndicatorSet)
                {
                    localBitwiseOutputStream.write(8, 8);
                    encodePriorityIndicator(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.alertIndicatorSet)
                {
                    localBitwiseOutputStream.write(8, 12);
                    encodeMsgDeliveryAlert(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.messageStatusSet)
                {
                    localBitwiseOutputStream.write(8, 20);
                    encodeMsgStatus(paramBearerData, localBitwiseOutputStream);
                }
                if (paramBearerData.serviceCategoryProgramResults != null)
                {
                    localBitwiseOutputStream.write(8, 19);
                    encodeScpResults(paramBearerData, localBitwiseOutputStream);
                }
                byte[] arrayOfByte2 = localBitwiseOutputStream.toByteArray();
                arrayOfByte1 = arrayOfByte2;
                return arrayOfByte1;
                bool = false;
            }
            catch (BitwiseOutputStream.AccessException localAccessException)
            {
                while (true)
                {
                    Log.e("SMS", "BearerData encode failed: " + localAccessException);
                    byte[] arrayOfByte1 = null;
                }
            }
            catch (CodingException localCodingException)
            {
                while (true)
                    Log.e("SMS", "BearerData encode failed: " + localCodingException);
            }
        }
    }

    private static void encode16bitEms(UserData paramUserData, byte[] paramArrayOfByte)
        throws BearerData.CodingException
    {
        byte[] arrayOfByte = encodeUtf16(paramUserData.payloadStr);
        int i = 1 + paramArrayOfByte.length;
        int j = (i + 1) / 2;
        int k = arrayOfByte.length / 2;
        paramUserData.msgEncoding = 4;
        paramUserData.msgEncodingSet = true;
        paramUserData.numFields = (j + k);
        paramUserData.payload = new byte[2 * paramUserData.numFields];
        paramUserData.payload[0] = ((byte)paramArrayOfByte.length);
        System.arraycopy(paramArrayOfByte, 0, paramUserData.payload, 1, paramArrayOfByte.length);
        System.arraycopy(arrayOfByte, 0, paramUserData.payload, i, arrayOfByte.length);
    }

    private static byte[] encode7bitAscii(String paramString, boolean paramBoolean)
        throws BearerData.CodingException
    {
        while (true)
        {
            BitwiseOutputStream localBitwiseOutputStream;
            int j;
            int k;
            try
            {
                localBitwiseOutputStream = new BitwiseOutputStream(paramString.length());
                int i = paramString.length();
                j = 0;
                if (j >= i)
                    break label145;
                k = UserData.charToAscii.get(paramString.charAt(j), -1);
                if (k == -1)
                {
                    if (paramBoolean)
                    {
                        localBitwiseOutputStream.write(7, 32);
                        break label154;
                    }
                    throw new CodingException("cannot ASCII encode (" + paramString.charAt(j) + ")");
                }
            }
            catch (BitwiseOutputStream.AccessException localAccessException)
            {
                throw new CodingException("7bit ASCII encode failed: " + localAccessException);
            }
            localBitwiseOutputStream.write(7, k);
            break label154;
            label145: byte[] arrayOfByte = localBitwiseOutputStream.toByteArray();
            return arrayOfByte;
            label154: j++;
        }
    }

    private static void encode7bitEms(UserData paramUserData, byte[] paramArrayOfByte, boolean paramBoolean)
        throws BearerData.CodingException
    {
        int i = (6 + 8 * (1 + paramArrayOfByte.length)) / 7;
        Gsm7bitCodingResult localGsm7bitCodingResult = encode7bitGsm(paramUserData.payloadStr, i, paramBoolean);
        paramUserData.msgEncoding = 9;
        paramUserData.msgEncodingSet = true;
        paramUserData.numFields = localGsm7bitCodingResult.septets;
        paramUserData.payload = localGsm7bitCodingResult.data;
        paramUserData.payload[0] = ((byte)paramArrayOfByte.length);
        System.arraycopy(paramArrayOfByte, 0, paramUserData.payload, 1, paramArrayOfByte.length);
    }

    private static Gsm7bitCodingResult encode7bitGsm(String paramString, int paramInt, boolean paramBoolean)
        throws BearerData.CodingException
    {
        boolean bool = true;
        if (!paramBoolean);
        try
        {
            while (true)
            {
                byte[] arrayOfByte = GsmAlphabet.stringToGsm7BitPacked(paramString, paramInt, bool, 0, 0);
                Gsm7bitCodingResult localGsm7bitCodingResult = new Gsm7bitCodingResult(null);
                localGsm7bitCodingResult.data = new byte[-1 + arrayOfByte.length];
                System.arraycopy(arrayOfByte, 1, localGsm7bitCodingResult.data, 0, -1 + arrayOfByte.length);
                localGsm7bitCodingResult.septets = (0xFF & arrayOfByte[0]);
                return localGsm7bitCodingResult;
                bool = false;
            }
        }
        catch (EncodeException localEncodeException)
        {
            throw new CodingException("7bit GSM encode failed: " + localEncodeException);
        }
    }

    private static void encodeCallbackNumber(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException, BearerData.CodingException
    {
        CdmaSmsAddress localCdmaSmsAddress = paramBearerData.callbackNumber;
        encodeCdmaSmsAddress(localCdmaSmsAddress);
        int i = 9;
        int j;
        int k;
        int m;
        if (localCdmaSmsAddress.digitMode == 1)
        {
            i += 7;
            j = 8 * localCdmaSmsAddress.numberOfDigits;
            k = i + j;
            m = k / 8;
            if (k % 8 <= 0)
                break label160;
        }
        label160: for (int n = 1; ; n = 0)
        {
            int i1 = m + n;
            int i2 = i1 * 8 - k;
            paramBitwiseOutputStream.write(8, i1);
            paramBitwiseOutputStream.write(1, localCdmaSmsAddress.digitMode);
            if (localCdmaSmsAddress.digitMode == 1)
            {
                paramBitwiseOutputStream.write(3, localCdmaSmsAddress.ton);
                paramBitwiseOutputStream.write(4, localCdmaSmsAddress.numberPlan);
            }
            paramBitwiseOutputStream.write(8, localCdmaSmsAddress.numberOfDigits);
            paramBitwiseOutputStream.writeByteArray(j, localCdmaSmsAddress.origBytes);
            if (i2 > 0)
                paramBitwiseOutputStream.write(i2, 0);
            return;
            j = 4 * localCdmaSmsAddress.numberOfDigits;
            break;
        }
    }

    private static void encodeCdmaSmsAddress(CdmaSmsAddress paramCdmaSmsAddress)
        throws BearerData.CodingException
    {
        if (paramCdmaSmsAddress.digitMode == 1);
        while (true)
        {
            try
            {
                paramCdmaSmsAddress.origBytes = paramCdmaSmsAddress.address.getBytes("US-ASCII");
                return;
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                throw new CodingException("invalid SMS address, cannot convert to ASCII");
            }
            paramCdmaSmsAddress.origBytes = encodeDtmfSmsAddress(paramCdmaSmsAddress.address);
        }
    }

    private static void encodeDisplayMode(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        paramBitwiseOutputStream.write(2, paramBearerData.displayMode);
        paramBitwiseOutputStream.skip(6);
    }

    private static byte[] encodeDtmfSmsAddress(String paramString)
    {
        int i = paramString.length();
        int j = i * 4;
        int k = j / 8;
        int m;
        int n;
        label35: int i1;
        int i2;
        if (j % 8 > 0)
        {
            m = 1;
            arrayOfByte = new byte[k + m];
            n = 0;
            if (n >= i)
                break label156;
            i1 = paramString.charAt(n);
            if ((i1 < 49) || (i1 > 57))
                break label111;
            i2 = i1 + -48;
        }
        while (true)
        {
            int i3 = n / 2;
            arrayOfByte[i3] = ((byte)(arrayOfByte[i3] | i2 << 4 - 4 * (n % 2)));
            n++;
            break label35;
            m = 0;
            break;
            label111: if (i1 == 48)
            {
                i2 = 10;
            }
            else if (i1 == 42)
            {
                i2 = 11;
            }
            else
            {
                if (i1 != 35)
                    break label153;
                i2 = 12;
            }
        }
        label153: byte[] arrayOfByte = null;
        label156: return arrayOfByte;
    }

    private static void encodeEmsUserDataPayload(UserData paramUserData)
        throws BearerData.CodingException
    {
        byte[] arrayOfByte = SmsHeader.toByteArray(paramUserData.userDataHeader);
        if (paramUserData.msgEncodingSet)
            if (paramUserData.msgEncoding == 9)
                encode7bitEms(paramUserData, arrayOfByte, true);
        while (true)
        {
            return;
            if (paramUserData.msgEncoding == 4)
            {
                encode16bitEms(paramUserData, arrayOfByte);
            }
            else
            {
                throw new CodingException("unsupported EMS user data encoding (" + paramUserData.msgEncoding + ")");
                try
                {
                    encode7bitEms(paramUserData, arrayOfByte, false);
                }
                catch (CodingException localCodingException)
                {
                    encode16bitEms(paramUserData, arrayOfByte);
                }
            }
        }
    }

    private static void encodeLanguageIndicator(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        paramBitwiseOutputStream.write(8, paramBearerData.language);
    }

    private static void encodeMessageId(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 3);
        paramBitwiseOutputStream.write(4, paramBearerData.messageType);
        paramBitwiseOutputStream.write(8, paramBearerData.messageId >> 8);
        paramBitwiseOutputStream.write(8, paramBearerData.messageId);
        if (paramBearerData.hasUserDataHeader);
        for (int i = 1; ; i = 0)
        {
            paramBitwiseOutputStream.write(1, i);
            paramBitwiseOutputStream.skip(3);
            return;
        }
    }

    private static void encodeMsgCount(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        paramBitwiseOutputStream.write(8, paramBearerData.numberOfMessages);
    }

    private static void encodeMsgDeliveryAlert(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        paramBitwiseOutputStream.write(2, paramBearerData.alert);
        paramBitwiseOutputStream.skip(6);
    }

    private static void encodeMsgStatus(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        paramBitwiseOutputStream.write(2, paramBearerData.errorClass);
        paramBitwiseOutputStream.write(6, paramBearerData.messageStatus);
    }

    private static void encodePriorityIndicator(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        paramBitwiseOutputStream.write(2, paramBearerData.priority);
        paramBitwiseOutputStream.skip(6);
    }

    private static void encodePrivacyIndicator(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        paramBitwiseOutputStream.write(2, paramBearerData.privacy);
        paramBitwiseOutputStream.skip(6);
    }

    private static void encodeReplyOption(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        int i;
        int j;
        label31: int k;
        if (paramBearerData.userAckReq)
        {
            i = 1;
            paramBitwiseOutputStream.write(1, i);
            if (!paramBearerData.deliveryAckReq)
                break label83;
            j = 1;
            paramBitwiseOutputStream.write(1, j);
            if (!paramBearerData.readAckReq)
                break label88;
            k = 1;
            label47: paramBitwiseOutputStream.write(1, k);
            if (!paramBearerData.reportReq)
                break label94;
        }
        label83: label88: label94: for (int m = 1; ; m = 0)
        {
            paramBitwiseOutputStream.write(1, m);
            paramBitwiseOutputStream.write(4, 0);
            return;
            i = 0;
            break;
            j = 0;
            break label31;
            k = 0;
            break label47;
        }
    }

    private static void encodeScpResults(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        ArrayList localArrayList = paramBearerData.serviceCategoryProgramResults;
        paramBitwiseOutputStream.write(8, 4 * localArrayList.size());
        Iterator localIterator = localArrayList.iterator();
        while (localIterator.hasNext())
        {
            CdmaSmsCbProgramResults localCdmaSmsCbProgramResults = (CdmaSmsCbProgramResults)localIterator.next();
            int i = localCdmaSmsCbProgramResults.getCategory();
            paramBitwiseOutputStream.write(8, i >> 8);
            paramBitwiseOutputStream.write(8, i);
            paramBitwiseOutputStream.write(8, localCdmaSmsCbProgramResults.getLanguage());
            paramBitwiseOutputStream.write(4, localCdmaSmsCbProgramResults.getCategoryResult());
            paramBitwiseOutputStream.skip(4);
        }
    }

    private static void encodeUserData(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException, BearerData.CodingException
    {
        encodeUserDataPayload(paramBearerData.userData);
        if (paramBearerData.userData.userDataHeader != null);
        for (boolean bool = true; ; bool = false)
        {
            paramBearerData.hasUserDataHeader = bool;
            if (paramBearerData.userData.payload.length <= 140)
                break;
            throw new CodingException("encoded user data too large (" + paramBearerData.userData.payload.length + " > " + 140 + " bytes)");
        }
        int i = 8 * paramBearerData.userData.payload.length - paramBearerData.userData.paddingBits;
        int j = i + 13;
        if ((paramBearerData.userData.msgEncoding == 1) || (paramBearerData.userData.msgEncoding == 10))
            j += 8;
        int k = j / 8;
        if (j % 8 > 0);
        for (int m = 1; ; m = 0)
        {
            int n = k + m;
            int i1 = n * 8 - j;
            paramBitwiseOutputStream.write(8, n);
            paramBitwiseOutputStream.write(5, paramBearerData.userData.msgEncoding);
            if ((paramBearerData.userData.msgEncoding == 1) || (paramBearerData.userData.msgEncoding == 10))
                paramBitwiseOutputStream.write(8, paramBearerData.userData.msgType);
            paramBitwiseOutputStream.write(8, paramBearerData.userData.numFields);
            paramBitwiseOutputStream.writeByteArray(i, paramBearerData.userData.payload);
            if (i1 > 0)
                paramBitwiseOutputStream.write(i1, 0);
            return;
        }
    }

    private static void encodeUserDataPayload(UserData paramUserData)
        throws BearerData.CodingException
    {
        if ((paramUserData.payloadStr == null) && (paramUserData.msgEncoding != 0))
        {
            Log.e("SMS", "user data with null payloadStr");
            paramUserData.payloadStr = "";
        }
        if (paramUserData.userDataHeader != null)
            encodeEmsUserDataPayload(paramUserData);
        while (true)
        {
            return;
            if (paramUserData.msgEncodingSet)
            {
                if (paramUserData.msgEncoding == 0)
                {
                    if (paramUserData.payload == null)
                    {
                        Log.e("SMS", "user data with octet encoding but null payload");
                        paramUserData.payload = new byte[0];
                        paramUserData.numFields = 0;
                        continue;
                    }
                    paramUserData.numFields = paramUserData.payload.length;
                    continue;
                }
                if (paramUserData.payloadStr == null)
                {
                    Log.e("SMS", "non-octet user data with null payloadStr");
                    paramUserData.payloadStr = "";
                }
                if (paramUserData.msgEncoding == 9)
                {
                    Gsm7bitCodingResult localGsm7bitCodingResult = encode7bitGsm(paramUserData.payloadStr, 0, true);
                    paramUserData.payload = localGsm7bitCodingResult.data;
                    paramUserData.numFields = localGsm7bitCodingResult.septets;
                    continue;
                }
                if (paramUserData.msgEncoding == 2)
                {
                    paramUserData.payload = encode7bitAscii(paramUserData.payloadStr, true);
                    paramUserData.numFields = paramUserData.payloadStr.length();
                    continue;
                }
                if (paramUserData.msgEncoding == 4)
                {
                    paramUserData.payload = encodeUtf16(paramUserData.payloadStr);
                    paramUserData.numFields = paramUserData.payloadStr.length();
                    continue;
                }
                throw new CodingException("unsupported user data encoding (" + paramUserData.msgEncoding + ")");
            }
            try
            {
                paramUserData.payload = encode7bitAscii(paramUserData.payloadStr, false);
                paramUserData.msgEncoding = 2;
                paramUserData.numFields = paramUserData.payloadStr.length();
                paramUserData.msgEncodingSet = true;
            }
            catch (CodingException localCodingException)
            {
                while (true)
                {
                    paramUserData.payload = encodeUtf16(paramUserData.payloadStr);
                    paramUserData.msgEncoding = 4;
                }
            }
        }
    }

    private static byte[] encodeUtf16(String paramString)
        throws BearerData.CodingException
    {
        try
        {
            byte[] arrayOfByte = paramString.getBytes("utf-16be");
            return arrayOfByte;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
            throw new CodingException("UTF-16 encode failed: " + localUnsupportedEncodingException);
        }
    }

    private static void encodeValidityPeriodRel(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream)
        throws BitwiseOutputStream.AccessException
    {
        paramBitwiseOutputStream.write(8, 1);
        paramBitwiseOutputStream.write(8, paramBearerData.validityPeriodRelative);
    }

    private static int getBitsForNumFields(int paramInt1, int paramInt2)
        throws BearerData.CodingException
    {
        int i;
        switch (paramInt1)
        {
        case 1:
        default:
            throw new CodingException("unsupported message encoding (" + paramInt1 + ')');
        case 0:
        case 5:
        case 6:
        case 7:
        case 8:
            i = paramInt2 * 8;
        case 2:
        case 3:
        case 9:
        case 4:
        }
        while (true)
        {
            return i;
            i = paramInt2 * 7;
            continue;
            i = paramInt2 * 16;
        }
    }

    private static String getLanguageCodeForValue(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = null;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            return str;
            str = "en";
            continue;
            str = "fr";
            continue;
            str = "es";
            continue;
            str = "ja";
            continue;
            str = "ko";
            continue;
            str = "zh";
            continue;
            str = "he";
        }
    }

    private static boolean isCmasAlertCategory(int paramInt)
    {
        if ((paramInt >= 4096) && (paramInt <= 4351));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static int serviceCategoryToCmasMessageClass(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            i = -1;
        case 4096:
        case 4097:
        case 4098:
        case 4099:
        case 4100:
        }
        while (true)
        {
            return i;
            i = 0;
            continue;
            i = 1;
            continue;
            i = 2;
            continue;
            i = 3;
            continue;
            i = 4;
        }
    }

    public String getLanguage()
    {
        return getLanguageCodeForValue(this.language);
    }

    public String toString()
    {
        StringBuilder localStringBuilder1 = new StringBuilder();
        localStringBuilder1.append("BearerData ");
        localStringBuilder1.append("{ messageType=" + this.messageType);
        localStringBuilder1.append(", messageId=" + this.messageId);
        StringBuilder localStringBuilder2 = new StringBuilder().append(", priority=");
        Object localObject1;
        Object localObject2;
        label149: Object localObject3;
        label195: Object localObject4;
        label241: Object localObject5;
        label287: Object localObject6;
        label333: Object localObject7;
        label379: Object localObject8;
        label422: Object localObject9;
        label465: Object localObject10;
        label511: Object localObject11;
        label554: StringBuilder localStringBuilder13;
        if (this.priorityIndicatorSet)
        {
            localObject1 = Integer.valueOf(this.priority);
            localStringBuilder1.append(localObject1);
            StringBuilder localStringBuilder3 = new StringBuilder().append(", privacy=");
            if (!this.privacyIndicatorSet)
                break label888;
            localObject2 = Integer.valueOf(this.privacy);
            localStringBuilder1.append(localObject2);
            StringBuilder localStringBuilder4 = new StringBuilder().append(", alert=");
            if (!this.alertIndicatorSet)
                break label896;
            localObject3 = Integer.valueOf(this.alert);
            localStringBuilder1.append(localObject3);
            StringBuilder localStringBuilder5 = new StringBuilder().append(", displayMode=");
            if (!this.displayModeSet)
                break label904;
            localObject4 = Integer.valueOf(this.displayMode);
            localStringBuilder1.append(localObject4);
            StringBuilder localStringBuilder6 = new StringBuilder().append(", language=");
            if (!this.languageIndicatorSet)
                break label912;
            localObject5 = Integer.valueOf(this.language);
            localStringBuilder1.append(localObject5);
            StringBuilder localStringBuilder7 = new StringBuilder().append(", errorClass=");
            if (!this.messageStatusSet)
                break label920;
            localObject6 = Integer.valueOf(this.errorClass);
            localStringBuilder1.append(localObject6);
            StringBuilder localStringBuilder8 = new StringBuilder().append(", msgStatus=");
            if (!this.messageStatusSet)
                break label928;
            localObject7 = Integer.valueOf(this.messageStatus);
            localStringBuilder1.append(localObject7);
            StringBuilder localStringBuilder9 = new StringBuilder().append(", msgCenterTimeStamp=");
            if (this.msgCenterTimeStamp == null)
                break label936;
            localObject8 = this.msgCenterTimeStamp;
            localStringBuilder1.append(localObject8);
            StringBuilder localStringBuilder10 = new StringBuilder().append(", validityPeriodAbsolute=");
            if (this.validityPeriodAbsolute == null)
                break label944;
            localObject9 = this.validityPeriodAbsolute;
            localStringBuilder1.append(localObject9);
            StringBuilder localStringBuilder11 = new StringBuilder().append(", validityPeriodRelative=");
            if (!this.validityPeriodRelativeSet)
                break label952;
            localObject10 = Integer.valueOf(this.validityPeriodRelative);
            localStringBuilder1.append(localObject10);
            StringBuilder localStringBuilder12 = new StringBuilder().append(", deferredDeliveryTimeAbsolute=");
            if (this.deferredDeliveryTimeAbsolute == null)
                break label960;
            localObject11 = this.deferredDeliveryTimeAbsolute;
            localStringBuilder1.append(localObject11);
            localStringBuilder13 = new StringBuilder().append(", deferredDeliveryTimeRelative=");
            if (!this.deferredDeliveryTimeRelativeSet)
                break label968;
        }
        label896: label904: label912: label920: label928: label936: label944: label952: label960: label968: for (Object localObject12 = Integer.valueOf(this.deferredDeliveryTimeRelative); ; localObject12 = "unset")
        {
            localStringBuilder1.append(localObject12);
            localStringBuilder1.append(", userAckReq=" + this.userAckReq);
            localStringBuilder1.append(", deliveryAckReq=" + this.deliveryAckReq);
            localStringBuilder1.append(", readAckReq=" + this.readAckReq);
            localStringBuilder1.append(", reportReq=" + this.reportReq);
            localStringBuilder1.append(", numberOfMessages=" + this.numberOfMessages);
            localStringBuilder1.append(", callbackNumber=" + this.callbackNumber);
            localStringBuilder1.append(", depositIndex=" + this.depositIndex);
            localStringBuilder1.append(", hasUserDataHeader=" + this.hasUserDataHeader);
            localStringBuilder1.append(", userData=" + this.userData);
            localStringBuilder1.append(" }");
            return localStringBuilder1.toString();
            localObject1 = "unset";
            break;
            label888: localObject2 = "unset";
            break label149;
            localObject3 = "unset";
            break label195;
            localObject4 = "unset";
            break label241;
            localObject5 = "unset";
            break label287;
            localObject6 = "unset";
            break label333;
            localObject7 = "unset";
            break label379;
            localObject8 = "unset";
            break label422;
            localObject9 = "unset";
            break label465;
            localObject10 = "unset";
            break label511;
            localObject11 = "unset";
            break label554;
        }
    }

    private static class Gsm7bitCodingResult
    {
        byte[] data;
        int septets;
    }

    private static class CodingException extends Exception
    {
        public CodingException(String paramString)
        {
            super();
        }
    }

    public static class TimeStamp extends Time
    {
        public TimeStamp()
        {
            super();
        }

        public static TimeStamp fromByteArray(byte[] paramArrayOfByte)
        {
            TimeStamp localTimeStamp = new TimeStamp();
            int i = IccUtils.cdmaBcdByteToInt(paramArrayOfByte[0]);
            if ((i > 99) || (i < 0))
                localTimeStamp = null;
            while (true)
            {
                return localTimeStamp;
                if (i >= 96);
                int k;
                for (int j = i + 1900; ; j = i + 2000)
                {
                    localTimeStamp.year = j;
                    k = IccUtils.cdmaBcdByteToInt(paramArrayOfByte[1]);
                    if ((k >= 1) && (k <= 12))
                        break label81;
                    localTimeStamp = null;
                    break;
                }
                label81: localTimeStamp.month = (k - 1);
                int m = IccUtils.cdmaBcdByteToInt(paramArrayOfByte[2]);
                if ((m < 1) || (m > 31))
                {
                    localTimeStamp = null;
                }
                else
                {
                    localTimeStamp.monthDay = m;
                    int n = IccUtils.cdmaBcdByteToInt(paramArrayOfByte[3]);
                    if ((n < 0) || (n > 23))
                    {
                        localTimeStamp = null;
                    }
                    else
                    {
                        localTimeStamp.hour = n;
                        int i1 = IccUtils.cdmaBcdByteToInt(paramArrayOfByte[4]);
                        if ((i1 < 0) || (i1 > 59))
                        {
                            localTimeStamp = null;
                        }
                        else
                        {
                            localTimeStamp.minute = i1;
                            int i2 = IccUtils.cdmaBcdByteToInt(paramArrayOfByte[5]);
                            if ((i2 < 0) || (i2 > 59))
                                localTimeStamp = null;
                            else
                                localTimeStamp.second = i2;
                        }
                    }
                }
            }
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("TimeStamp ");
            localStringBuilder.append("{ year=" + this.year);
            localStringBuilder.append(", month=" + this.month);
            localStringBuilder.append(", day=" + this.monthDay);
            localStringBuilder.append(", hour=" + this.hour);
            localStringBuilder.append(", minute=" + this.minute);
            localStringBuilder.append(", second=" + this.second);
            localStringBuilder.append(" }");
            return localStringBuilder.toString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.sms.BearerData
 * JD-Core Version:        0.6.2
 */