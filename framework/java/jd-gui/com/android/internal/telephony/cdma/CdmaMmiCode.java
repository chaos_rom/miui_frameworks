package com.android.internal.telephony.cdma;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandException.Error;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.MmiCode;
import com.android.internal.telephony.MmiCode.State;
import com.android.internal.telephony.PhoneBase;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CdmaMmiCode extends Handler
    implements MmiCode
{
    static final String ACTION_REGISTER = "**";
    static final int EVENT_SET_COMPLETE = 1;
    static final String LOG_TAG = "CDMA_MMI";
    static final int MATCH_GROUP_ACTION = 2;
    static final int MATCH_GROUP_DIALING_NUMBER = 12;
    static final int MATCH_GROUP_POUND_STRING = 1;
    static final int MATCH_GROUP_PWD_CONFIRM = 11;
    static final int MATCH_GROUP_SERVICE_CODE = 3;
    static final int MATCH_GROUP_SIA = 5;
    static final int MATCH_GROUP_SIB = 7;
    static final int MATCH_GROUP_SIC = 9;
    static final String SC_PUK = "05";
    static Pattern sPatternSuppService = Pattern.compile("((\\*|#|\\*#|\\*\\*|##)(\\d{2,3})(\\*([^*#]*)(\\*([^*#]*)(\\*([^*#]*)(\\*([^*#]*))?)?)?)?#)(.*)");
    String action;
    Context context;
    String dialingNumber;
    CharSequence message;
    CDMAPhone phone;
    String poundString;
    String pwd;
    String sc;
    String sia;
    String sib;
    String sic;
    MmiCode.State state = MmiCode.State.PENDING;

    CdmaMmiCode(CDMAPhone paramCDMAPhone)
    {
        super(paramCDMAPhone.getHandler().getLooper());
        this.phone = paramCDMAPhone;
        this.context = paramCDMAPhone.getContext();
    }

    private CharSequence getScString()
    {
        if ((this.sc != null) && (isPukCommand()));
        for (Object localObject = this.context.getText(17039582); ; localObject = "")
            return localObject;
    }

    private void handlePasswordError(int paramInt)
    {
        this.state = MmiCode.State.FAILED;
        StringBuilder localStringBuilder = new StringBuilder(getScString());
        localStringBuilder.append("\n");
        localStringBuilder.append(this.context.getText(paramInt));
        this.message = localStringBuilder;
        this.phone.onMMIDone(this);
    }

    private static String makeEmptyNull(String paramString)
    {
        if ((paramString != null) && (paramString.length() == 0))
            paramString = null;
        return paramString;
    }

    public static CdmaMmiCode newFromDialString(String paramString, CDMAPhone paramCDMAPhone)
    {
        CdmaMmiCode localCdmaMmiCode = null;
        Matcher localMatcher = sPatternSuppService.matcher(paramString);
        if (localMatcher.matches())
        {
            localCdmaMmiCode = new CdmaMmiCode(paramCDMAPhone);
            localCdmaMmiCode.poundString = makeEmptyNull(localMatcher.group(1));
            localCdmaMmiCode.action = makeEmptyNull(localMatcher.group(2));
            localCdmaMmiCode.sc = makeEmptyNull(localMatcher.group(3));
            localCdmaMmiCode.sia = makeEmptyNull(localMatcher.group(5));
            localCdmaMmiCode.sib = makeEmptyNull(localMatcher.group(7));
            localCdmaMmiCode.sic = makeEmptyNull(localMatcher.group(9));
            localCdmaMmiCode.pwd = makeEmptyNull(localMatcher.group(11));
            localCdmaMmiCode.dialingNumber = makeEmptyNull(localMatcher.group(12));
        }
        return localCdmaMmiCode;
    }

    private void onSetComplete(AsyncResult paramAsyncResult)
    {
        StringBuilder localStringBuilder = new StringBuilder(getScString());
        localStringBuilder.append("\n");
        if (paramAsyncResult.exception != null)
        {
            this.state = MmiCode.State.FAILED;
            if ((paramAsyncResult.exception instanceof CommandException))
                if (((CommandException)paramAsyncResult.exception).getCommandError() == CommandException.Error.PASSWORD_INCORRECT)
                    if (isPukCommand())
                        localStringBuilder.append(this.context.getText(17039568));
        }
        while (true)
        {
            this.message = localStringBuilder;
            this.phone.onMMIDone(this);
            return;
            localStringBuilder.append(this.context.getText(17039565));
            continue;
            localStringBuilder.append(this.context.getText(17039558));
            continue;
            localStringBuilder.append(this.context.getText(17039558));
            continue;
            if (isRegister())
            {
                this.state = MmiCode.State.COMPLETE;
                localStringBuilder.append(this.context.getText(17039563));
            }
            else
            {
                this.state = MmiCode.State.FAILED;
                localStringBuilder.append(this.context.getText(17039558));
            }
        }
    }

    public void cancel()
    {
        if ((this.state == MmiCode.State.COMPLETE) || (this.state == MmiCode.State.FAILED));
        while (true)
        {
            return;
            this.state = MmiCode.State.CANCELLED;
            this.phone.onMMIDone(this);
        }
    }

    public CharSequence getMessage()
    {
        return this.message;
    }

    public MmiCode.State getState()
    {
        return this.state;
    }

    public void handleMessage(Message paramMessage)
    {
        if (paramMessage.what == 1)
            onSetComplete((AsyncResult)paramMessage.obj);
        while (true)
        {
            return;
            Log.e("CDMA_MMI", "Unexpected reply");
        }
    }

    public boolean isCancelable()
    {
        return false;
    }

    boolean isPukCommand()
    {
        if ((this.sc != null) && (this.sc.equals("05")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isRegister()
    {
        if ((this.action != null) && (this.action.equals("**")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isUssdRequest()
    {
        Log.w("CDMA_MMI", "isUssdRequest is not implemented in CdmaMmiCode");
        return false;
    }

    void processCode()
    {
        while (true)
        {
            String str1;
            String str2;
            int i;
            try
            {
                if (isPukCommand())
                {
                    str1 = this.sia;
                    str2 = this.sib;
                    i = str2.length();
                    if (isRegister())
                    {
                        if (str2.equals(this.sic))
                            break label137;
                        handlePasswordError(17039569);
                        break label136;
                        handlePasswordError(17039570);
                    }
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                this.state = MmiCode.State.FAILED;
                this.message = this.context.getText(17039558);
                this.phone.onMMIDone(this);
            }
            label136: label137: 
            do
            {
                this.phone.mCM.supplyIccPuk(str1, str2, obtainMessage(1, this));
                break label136;
                throw new RuntimeException("Invalid or Unsupported MMI Code");
                throw new RuntimeException("Invalid or Unsupported MMI Code");
                return;
                if (i < 4)
                    break;
            }
            while (i <= 8);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaMmiCode
 * JD-Core Version:        0.6.2
 */