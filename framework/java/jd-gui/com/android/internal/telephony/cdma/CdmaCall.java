package com.android.internal.telephony.cdma;

import com.android.internal.telephony.Call;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.DriverCall;
import com.android.internal.telephony.DriverCall.State;
import com.android.internal.telephony.Phone;
import java.util.ArrayList;
import java.util.List;

public final class CdmaCall extends Call
{
    ArrayList<Connection> connections = new ArrayList();
    CdmaCallTracker owner;
    Call.State state = Call.State.IDLE;

    CdmaCall(CdmaCallTracker paramCdmaCallTracker)
    {
        this.owner = paramCdmaCallTracker;
    }

    static Call.State stateFromDCState(DriverCall.State paramState)
    {
        Call.State localState;
        switch (1.$SwitchMap$com$android$internal$telephony$DriverCall$State[paramState.ordinal()])
        {
        default:
            throw new RuntimeException("illegal call state:" + paramState);
        case 1:
            localState = Call.State.ACTIVE;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return localState;
            localState = Call.State.HOLDING;
            continue;
            localState = Call.State.DIALING;
            continue;
            localState = Call.State.ALERTING;
            continue;
            localState = Call.State.INCOMING;
            continue;
            localState = Call.State.WAITING;
        }
    }

    void attach(Connection paramConnection, DriverCall paramDriverCall)
    {
        this.connections.add(paramConnection);
        this.state = stateFromDCState(paramDriverCall.state);
    }

    void attachFake(Connection paramConnection, Call.State paramState)
    {
        this.connections.add(paramConnection);
        this.state = paramState;
    }

    void clearDisconnected()
    {
        for (int i = -1 + this.connections.size(); i >= 0; i--)
            if (((CdmaConnection)this.connections.get(i)).getState() == Call.State.DISCONNECTED)
                this.connections.remove(i);
        if (this.connections.size() == 0)
            this.state = Call.State.IDLE;
    }

    void connectionDisconnected(CdmaConnection paramCdmaConnection)
    {
        int i;
        int j;
        int k;
        if (this.state != Call.State.DISCONNECTED)
        {
            i = 1;
            j = 0;
            k = this.connections.size();
        }
        while (true)
        {
            if (j < k)
            {
                if (((Connection)this.connections.get(j)).getState() != Call.State.DISCONNECTED)
                    i = 0;
            }
            else
            {
                if (i != 0)
                    this.state = Call.State.DISCONNECTED;
                return;
            }
            j++;
        }
    }

    void detach(CdmaConnection paramCdmaConnection)
    {
        this.connections.remove(paramCdmaConnection);
        if (this.connections.size() == 0)
            this.state = Call.State.IDLE;
    }

    public void dispose()
    {
    }

    public List<Connection> getConnections()
    {
        return this.connections;
    }

    public Phone getPhone()
    {
        return this.owner.phone;
    }

    public Call.State getState()
    {
        return this.state;
    }

    public void hangup()
        throws CallStateException
    {
        this.owner.hangup(this);
    }

    boolean isFull()
    {
        int i = 1;
        if (this.connections.size() == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean isMultiparty()
    {
        int i = 1;
        if (this.connections.size() > i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    void onHangupLocal()
    {
        int i = 0;
        int j = this.connections.size();
        while (i < j)
        {
            ((CdmaConnection)this.connections.get(i)).onHangupLocal();
            i++;
        }
        this.state = Call.State.DISCONNECTING;
    }

    public String toString()
    {
        return this.state.toString();
    }

    boolean update(CdmaConnection paramCdmaConnection, DriverCall paramDriverCall)
    {
        boolean bool = false;
        Call.State localState = stateFromDCState(paramDriverCall.state);
        if (localState != this.state)
        {
            this.state = localState;
            bool = true;
        }
        return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaCall
 * JD-Core Version:        0.6.2
 */