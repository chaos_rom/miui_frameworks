package com.android.internal.telephony.cdma;

import android.os.Message;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccFileHandler;

public final class RuimFileHandler extends IccFileHandler
{
    static final String LOG_TAG = "CDMA";

    public RuimFileHandler(IccCard paramIccCard, String paramString, CommandsInterface paramCommandsInterface)
    {
        super(paramIccCard, paramString, paramCommandsInterface);
    }

    protected void finalize()
    {
        Log.d("CDMA", "RuimFileHandler finalized");
    }

    protected String getEFPath(int paramInt)
    {
        switch (paramInt)
        {
        default:
        case 28466:
        case 28476:
        case 28481:
        }
        for (String str = getCommonIccEFPath(paramInt); ; str = "3F007F25")
            return str;
    }

    public void handleMessage(Message paramMessage)
    {
        super.handleMessage(paramMessage);
    }

    public void loadEFImgTransparent(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Message paramMessage)
    {
        Message localMessage = obtainMessage(10, paramInt1, 0, paramMessage);
        this.mCi.iccIOForApp(192, paramInt1, "img", 0, 0, 10, null, null, this.mAid, localMessage);
    }

    protected void logd(String paramString)
    {
        Log.d("CDMA", "[RuimFileHandler] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("CDMA", "[RuimFileHandler] " + paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.RuimFileHandler
 * JD-Core Version:        0.6.2
 */