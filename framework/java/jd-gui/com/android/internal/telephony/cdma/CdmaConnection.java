package com.android.internal.telephony.cdma;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Registrant;
import android.os.SystemClock;
import android.telephony.PhoneNumberUtils;
import android.telephony.ServiceState;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.Call.State;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CallTracker;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.Connection.DisconnectCause;
import com.android.internal.telephony.Connection.PostDialState;
import com.android.internal.telephony.DriverCall;
import com.android.internal.telephony.DriverCall.State;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.telephony.UUSInfo;

public class CdmaConnection extends Connection
{
    static final int EVENT_DTMF_DONE = 1;
    static final int EVENT_NEXT_POST_DIAL = 3;
    static final int EVENT_PAUSE_DONE = 2;
    static final int EVENT_WAKE_LOCK_TIMEOUT = 4;
    static final String LOG_TAG = "CDMA";
    static final int PAUSE_DELAY_MILLIS = 2000;
    static final int WAKE_LOCK_TIMEOUT_MILLIS = 60000;
    String address;
    Connection.DisconnectCause cause = Connection.DisconnectCause.NOT_DISCONNECTED;
    String cnapName;
    int cnapNamePresentation = Connection.PRESENTATION_ALLOWED;
    long connectTime;
    long connectTimeReal;
    long createTime;
    String dialString;
    long disconnectTime;
    boolean disconnected;
    long duration;
    Handler h;
    long holdingStartTime;
    int index;
    boolean isIncoming;
    private PowerManager.WakeLock mPartialWakeLock;
    int nextPostDialChar;
    int numberPresentation = Connection.PRESENTATION_ALLOWED;
    CdmaCallTracker owner;
    CdmaCall parent;
    Connection.PostDialState postDialState = Connection.PostDialState.NOT_STARTED;
    String postDialString;

    CdmaConnection(Context paramContext, DriverCall paramDriverCall, CdmaCallTracker paramCdmaCallTracker, int paramInt)
    {
        createWakeLock(paramContext);
        acquireWakeLock();
        this.owner = paramCdmaCallTracker;
        this.h = new MyHandler(this.owner.getLooper());
        this.address = paramDriverCall.number;
        this.isIncoming = paramDriverCall.isMT;
        this.createTime = System.currentTimeMillis();
        this.cnapName = paramDriverCall.name;
        this.cnapNamePresentation = paramDriverCall.namePresentation;
        this.numberPresentation = paramDriverCall.numberPresentation;
        this.index = paramInt;
        this.parent = parentFromDCState(paramDriverCall.state);
        this.parent.attach(this, paramDriverCall);
    }

    CdmaConnection(Context paramContext, CdmaCallWaitingNotification paramCdmaCallWaitingNotification, CdmaCallTracker paramCdmaCallTracker, CdmaCall paramCdmaCall)
    {
        createWakeLock(paramContext);
        acquireWakeLock();
        this.owner = paramCdmaCallTracker;
        this.h = new MyHandler(this.owner.getLooper());
        this.address = paramCdmaCallWaitingNotification.number;
        this.numberPresentation = paramCdmaCallWaitingNotification.numberPresentation;
        this.cnapName = paramCdmaCallWaitingNotification.name;
        this.cnapNamePresentation = paramCdmaCallWaitingNotification.namePresentation;
        this.index = -1;
        this.isIncoming = true;
        this.createTime = System.currentTimeMillis();
        this.connectTime = 0L;
        this.parent = paramCdmaCall;
        paramCdmaCall.attachFake(this, Call.State.WAITING);
    }

    CdmaConnection(Context paramContext, String paramString, CdmaCallTracker paramCdmaCallTracker, CdmaCall paramCdmaCall)
    {
        createWakeLock(paramContext);
        acquireWakeLock();
        this.owner = paramCdmaCallTracker;
        this.h = new MyHandler(this.owner.getLooper());
        this.dialString = paramString;
        Log.d("CDMA", "[CDMAConn] CdmaConnection: dialString=" + paramString);
        String str = formatDialString(paramString);
        Log.d("CDMA", "[CDMAConn] CdmaConnection:formated dialString=" + str);
        this.address = PhoneNumberUtils.extractNetworkPortionAlt(str);
        this.postDialString = PhoneNumberUtils.extractPostDialPortion(str);
        this.index = -1;
        this.isIncoming = false;
        this.cnapName = null;
        this.cnapNamePresentation = Connection.PRESENTATION_ALLOWED;
        this.numberPresentation = Connection.PRESENTATION_ALLOWED;
        this.createTime = System.currentTimeMillis();
        if (paramCdmaCall != null)
        {
            this.parent = paramCdmaCall;
            if (paramCdmaCall.state != Call.State.ACTIVE)
                break label214;
            paramCdmaCall.attachFake(this, Call.State.ACTIVE);
        }
        while (true)
        {
            return;
            label214: paramCdmaCall.attachFake(this, Call.State.DIALING);
        }
    }

    private void acquireWakeLock()
    {
        log("acquireWakeLock");
        this.mPartialWakeLock.acquire();
    }

    private void createWakeLock(Context paramContext)
    {
        this.mPartialWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "CDMA");
    }

    private void doDisconnect()
    {
        this.index = -1;
        this.disconnectTime = System.currentTimeMillis();
        this.duration = (SystemClock.elapsedRealtime() - this.connectTimeReal);
        this.disconnected = true;
    }

    static boolean equalsHandlesNulls(Object paramObject1, Object paramObject2)
    {
        boolean bool;
        if (paramObject1 == null)
            if (paramObject2 == null)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = paramObject1.equals(paramObject2);
        }
    }

    private static int findNextPCharOrNonPOrNonWCharIndex(String paramString, int paramInt)
    {
        boolean bool = isWait(paramString.charAt(paramInt));
        int i = paramInt + 1;
        int j = paramString.length();
        while (true)
        {
            if (i < j)
            {
                char c = paramString.charAt(i);
                if (isWait(c))
                    bool = true;
                if ((isWait(c)) || (isPause(c)));
            }
            else
            {
                if ((i < j) && (i > paramInt + 1) && (!bool) && (isPause(paramString.charAt(paramInt))))
                    i = paramInt + 1;
                return i;
            }
            i++;
        }
    }

    private static char findPOrWCharToAppend(String paramString, int paramInt1, int paramInt2)
    {
        if (isPause(paramString.charAt(paramInt1)));
        for (char c = ','; ; c = ';')
        {
            if (paramInt2 > paramInt1 + 1)
                c = ';';
            return c;
        }
    }

    public static String formatDialString(String paramString)
    {
        if (paramString == null);
        StringBuilder localStringBuilder;
        for (String str = null; ; str = PhoneNumberUtils.cdmaCheckAndProcessPlusCode(localStringBuilder.toString()))
        {
            return str;
            int i = paramString.length();
            localStringBuilder = new StringBuilder();
            int j = 0;
            if (j < i)
            {
                char c = paramString.charAt(j);
                int k;
                if ((isPause(c)) || (isWait(c)))
                    if (j < i - 1)
                    {
                        k = findNextPCharOrNonPOrNonWCharIndex(paramString, j);
                        if (k >= i)
                            break label104;
                        localStringBuilder.append(findPOrWCharToAppend(paramString, j, k));
                        if (k > j + 1)
                            j = k - 1;
                    }
                while (true)
                {
                    j++;
                    break;
                    label104: if (k == i)
                    {
                        j = i - 1;
                        continue;
                        localStringBuilder.append(c);
                    }
                }
            }
        }
    }

    private boolean isConnectingInOrOut()
    {
        if ((this.parent == null) || (this.parent == this.owner.ringingCall) || (this.parent.state == Call.State.DIALING) || (this.parent.state == Call.State.ALERTING));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isPause(char paramChar)
    {
        if (paramChar == ',');
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isWait(char paramChar)
    {
        if (paramChar == ';');
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void log(String paramString)
    {
        Log.d("CDMA", "[CDMAConn] " + paramString);
    }

    private void onStartedHolding()
    {
        this.holdingStartTime = SystemClock.elapsedRealtime();
    }

    private CdmaCall parentFromDCState(DriverCall.State paramState)
    {
        CdmaCall localCdmaCall;
        switch (1.$SwitchMap$com$android$internal$telephony$DriverCall$State[paramState.ordinal()])
        {
        default:
            throw new RuntimeException("illegal call state: " + paramState);
        case 1:
        case 2:
        case 3:
            localCdmaCall = this.owner.foregroundCall;
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return localCdmaCall;
            localCdmaCall = this.owner.backgroundCall;
            continue;
            localCdmaCall = this.owner.ringingCall;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private boolean processPostDialChar(char paramChar)
    {
        int i = 1;
        if (PhoneNumberUtils.is12Key(paramChar))
            this.owner.cm.sendDtmf(paramChar, this.h.obtainMessage(i));
        while (true)
        {
            return i;
            if (paramChar == ',')
            {
                Injector.setPostDialState(this, Connection.PostDialState.PAUSE);
                this.h.sendMessageDelayed(this.h.obtainMessage(2), 2000L);
            }
            else if (paramChar == ';')
            {
                setPostDialState(Connection.PostDialState.WAIT);
            }
            else if (paramChar == 'N')
            {
                setPostDialState(Connection.PostDialState.WILD);
            }
            else
            {
                int j = 0;
            }
        }
    }

    private void releaseWakeLock()
    {
        synchronized (this.mPartialWakeLock)
        {
            if (this.mPartialWakeLock.isHeld())
            {
                log("releaseWakeLock");
                this.mPartialWakeLock.release();
            }
            return;
        }
    }

    private void setPostDialState(Connection.PostDialState paramPostDialState)
    {
        if ((paramPostDialState == Connection.PostDialState.STARTED) || (paramPostDialState == Connection.PostDialState.PAUSE));
        while (true)
        {
            synchronized (this.mPartialWakeLock)
            {
                if (this.mPartialWakeLock.isHeld())
                {
                    this.h.removeMessages(4);
                    Message localMessage = this.h.obtainMessage(4);
                    this.h.sendMessageDelayed(localMessage, 60000L);
                    this.postDialState = paramPostDialState;
                    return;
                }
                acquireWakeLock();
            }
            this.h.removeMessages(4);
            releaseWakeLock();
        }
    }

    public void cancelPostDial()
    {
        setPostDialState(Connection.PostDialState.CANCELLED);
    }

    boolean compareTo(DriverCall paramDriverCall)
    {
        boolean bool = true;
        if ((!this.isIncoming) && (!paramDriverCall.isMT));
        while (true)
        {
            return bool;
            String str = PhoneNumberUtils.stringFromStringAndTOA(paramDriverCall.number, paramDriverCall.TOA);
            if ((this.isIncoming != paramDriverCall.isMT) || (!equalsHandlesNulls(this.address, str)))
                bool = false;
        }
    }

    Connection.DisconnectCause disconnectCauseFromCode(int paramInt)
    {
        CDMAPhone localCDMAPhone;
        int i;
        Connection.DisconnectCause localDisconnectCause;
        switch (paramInt)
        {
        default:
            localCDMAPhone = this.owner.phone;
            i = localCDMAPhone.getServiceState().getState();
            if (i == 3)
                localDisconnectCause = Connection.DisconnectCause.POWER_OFF;
            break;
        case 17:
        case 34:
        case 68:
        case 240:
        case 241:
        case 1000:
        case 1001:
        case 1002:
        case 1003:
        case 1004:
        case 1005:
        case 1006:
        case 1007:
        case 1008:
        case 1009:
        }
        while (true)
        {
            return localDisconnectCause;
            localDisconnectCause = Connection.DisconnectCause.BUSY;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CONGESTION;
            continue;
            localDisconnectCause = Connection.DisconnectCause.LIMIT_EXCEEDED;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CALL_BARRED;
            continue;
            localDisconnectCause = Connection.DisconnectCause.FDN_BLOCKED;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_LOCKED_UNTIL_POWER_CYCLE;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_DROP;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_INTERCEPT;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_REORDER;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_SO_REJECT;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_RETRY_ORDER;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_ACCESS_FAILURE;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_PREEMPTED;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_NOT_EMERGENCY;
            continue;
            localDisconnectCause = Connection.DisconnectCause.CDMA_ACCESS_BLOCKED;
            continue;
            if ((i == 1) || (i == 2))
                localDisconnectCause = Connection.DisconnectCause.OUT_OF_SERVICE;
            else if ((localCDMAPhone.mCdmaSubscriptionSource == 0) && (localCDMAPhone.getIccCard().getState() != IccCard.State.READY))
                localDisconnectCause = Connection.DisconnectCause.ICC_ERROR;
            else if (paramInt == 16)
                localDisconnectCause = Connection.DisconnectCause.NORMAL;
            else
                localDisconnectCause = Connection.DisconnectCause.ERROR_UNSPECIFIED;
        }
    }

    public void dispose()
    {
    }

    void fakeHoldBeforeDial()
    {
        if (this.parent != null)
            this.parent.detach(this);
        this.parent = this.owner.backgroundCall;
        this.parent.attachFake(this, Call.State.HOLDING);
        onStartedHolding();
    }

    protected void finalize()
    {
        if (this.mPartialWakeLock.isHeld())
            Log.e("CDMA", "[CdmaConn] UNEXPECTED; mPartialWakeLock is held when finalizing.");
        releaseWakeLock();
    }

    public String getAddress()
    {
        return this.address;
    }

    int getCDMAIndex()
        throws CallStateException
    {
        if (this.index >= 0)
            return 1 + this.index;
        throw new CallStateException("CDMA connection index not assigned");
    }

    public CdmaCall getCall()
    {
        return this.parent;
    }

    public String getCnapName()
    {
        return this.cnapName;
    }

    public int getCnapNamePresentation()
    {
        return this.cnapNamePresentation;
    }

    public long getConnectTime()
    {
        return this.connectTime;
    }

    public long getCreateTime()
    {
        return this.createTime;
    }

    public Connection.DisconnectCause getDisconnectCause()
    {
        return this.cause;
    }

    public long getDisconnectTime()
    {
        return this.disconnectTime;
    }

    public long getDurationMillis()
    {
        long l = 0L;
        if (this.connectTimeReal == l);
        while (true)
        {
            return l;
            if (this.duration == l)
                l = SystemClock.elapsedRealtime() - this.connectTimeReal;
            else
                l = this.duration;
        }
    }

    public long getHoldDurationMillis()
    {
        if (getState() != Call.State.HOLDING);
        for (long l = 0L; ; l = SystemClock.elapsedRealtime() - this.holdingStartTime)
            return l;
    }

    public int getNumberPresentation()
    {
        return this.numberPresentation;
    }

    public String getOrigDialString()
    {
        return this.dialString;
    }

    public Connection.PostDialState getPostDialState()
    {
        return this.postDialState;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public String getRemainingPostDialString()
    {
        String str;
        if ((this.postDialState == Connection.PostDialState.CANCELLED) || (this.postDialState == Connection.PostDialState.COMPLETE) || (this.postDialString == null) || (this.postDialString.length() <= this.nextPostDialChar))
            str = "";
        while (true)
        {
            return str;
            str = this.postDialString.substring(this.nextPostDialChar);
            if (Injector.nullifyString(str) != null)
            {
                int i = str.indexOf(';');
                int j = str.indexOf(',');
                if ((i > 0) && ((i < j) || (j <= 0)))
                    str = str.substring(0, i);
                else if (j > 0)
                    str = str.substring(0, j);
            }
        }
    }

    public Call.State getState()
    {
        if (this.disconnected);
        for (Call.State localState = Call.State.DISCONNECTED; ; localState = super.getState())
            return localState;
    }

    public UUSInfo getUUSInfo()
    {
        return null;
    }

    public void hangup()
        throws CallStateException
    {
        if (!this.disconnected)
        {
            this.owner.hangup(this);
            return;
        }
        throw new CallStateException("disconnected");
    }

    public boolean isIncoming()
    {
        return this.isIncoming;
    }

    void onConnectedInOrOut()
    {
        this.connectTime = System.currentTimeMillis();
        this.connectTimeReal = SystemClock.elapsedRealtime();
        this.duration = 0L;
        log("onConnectedInOrOut: connectTime=" + this.connectTime);
        if (!this.isIncoming)
            processNextPostDialChar();
        while (true)
        {
            return;
            releaseWakeLock();
        }
    }

    void onDisconnect(Connection.DisconnectCause paramDisconnectCause)
    {
        this.cause = paramDisconnectCause;
        if (!this.disconnected)
        {
            doDisconnect();
            this.owner.phone.notifyDisconnect(this);
            if (this.parent != null)
                this.parent.connectionDisconnected(this);
        }
        releaseWakeLock();
    }

    void onHangupLocal()
    {
        this.cause = Connection.DisconnectCause.LOCAL;
    }

    void onLocalDisconnect()
    {
        if (!this.disconnected)
        {
            doDisconnect();
            if (this.parent != null)
                this.parent.detach(this);
        }
        releaseWakeLock();
    }

    void onRemoteDisconnect(int paramInt)
    {
        onDisconnect(disconnectCauseFromCode(paramInt));
    }

    public void proceedAfterWaitChar()
    {
        if (this.postDialState != Connection.PostDialState.WAIT)
            Log.w("CDMA", "CdmaConnection.proceedAfterWaitChar(): Expected getPostDialState() to be WAIT but was " + this.postDialState);
        while (true)
        {
            return;
            setPostDialState(Connection.PostDialState.STARTED);
            processNextPostDialChar();
        }
    }

    public void proceedAfterWildChar(String paramString)
    {
        if (this.postDialState != Connection.PostDialState.WILD)
            Log.w("CDMA", "CdmaConnection.proceedAfterWaitChar(): Expected getPostDialState() to be WILD but was " + this.postDialState);
        while (true)
        {
            return;
            setPostDialState(Connection.PostDialState.STARTED);
            StringBuilder localStringBuilder = new StringBuilder(paramString);
            localStringBuilder.append(this.postDialString.substring(this.nextPostDialChar));
            this.postDialString = localStringBuilder.toString();
            this.nextPostDialChar = 0;
            log("proceedAfterWildChar: new postDialString is " + this.postDialString);
            processNextPostDialChar();
        }
    }

    void processNextPostDialChar()
    {
        if (this.postDialState == Connection.PostDialState.CANCELLED)
            releaseWakeLock();
        while (true)
        {
            return;
            char c;
            if ((this.postDialString == null) || (this.postDialString.length() <= this.nextPostDialChar))
            {
                setPostDialState(Connection.PostDialState.COMPLETE);
                releaseWakeLock();
                c = '\000';
            }
            do
            {
                Registrant localRegistrant = this.owner.phone.mPostDialHandler;
                if (localRegistrant == null)
                    break;
                Message localMessage = localRegistrant.messageForRegistrant();
                if (localMessage == null)
                    break;
                Connection.PostDialState localPostDialState = this.postDialState;
                AsyncResult localAsyncResult = AsyncResult.forMessage(localMessage);
                localAsyncResult.result = this;
                localAsyncResult.userObj = localPostDialState;
                localMessage.arg1 = c;
                localMessage.sendToTarget();
                break;
                setPostDialState(Connection.PostDialState.STARTED);
                String str = this.postDialString;
                int i = this.nextPostDialChar;
                this.nextPostDialChar = (i + 1);
                c = str.charAt(i);
            }
            while (processPostDialChar(c));
            this.h.obtainMessage(3).sendToTarget();
            Log.e("CDMA", "processNextPostDialChar: c=" + c + " isn't valid!");
        }
    }

    public void separate()
        throws CallStateException
    {
        if (!this.disconnected)
        {
            this.owner.separate(this);
            return;
        }
        throw new CallStateException("disconnected");
    }

    boolean update(DriverCall paramDriverCall)
    {
        int i = 0;
        boolean bool1 = isConnectingInOrOut();
        boolean bool2;
        CdmaCall localCdmaCall;
        if (getState() == Call.State.HOLDING)
        {
            bool2 = true;
            localCdmaCall = parentFromDCState(paramDriverCall.state);
            log("parent= " + this.parent + ", newParent= " + localCdmaCall);
            if (!equalsHandlesNulls(this.address, paramDriverCall.number))
            {
                log("update: phone # changed!");
                this.address = paramDriverCall.number;
                i = 1;
            }
            if (!TextUtils.isEmpty(paramDriverCall.name))
                break label318;
            if (!TextUtils.isEmpty(this.cnapName))
            {
                i = 1;
                this.cnapName = "";
            }
        }
        while (true)
        {
            log("--dssds----" + this.cnapName);
            this.cnapNamePresentation = paramDriverCall.namePresentation;
            this.numberPresentation = paramDriverCall.numberPresentation;
            if (localCdmaCall == this.parent)
                break label345;
            if (this.parent != null)
                this.parent.detach(this);
            localCdmaCall.attach(this, paramDriverCall);
            this.parent = localCdmaCall;
            bool4 = true;
            log("Update, wasConnectingInOrOut=" + bool1 + ", wasHolding=" + bool2 + ", isConnectingInOrOut=" + isConnectingInOrOut() + ", changed=" + bool4);
            if ((bool1) && (!isConnectingInOrOut()))
                onConnectedInOrOut();
            if ((bool4) && (!bool2) && (getState() == Call.State.HOLDING))
                onStartedHolding();
            return bool4;
            bool2 = false;
            break;
            label318: if (!paramDriverCall.name.equals(this.cnapName))
            {
                i = 1;
                this.cnapName = paramDriverCall.name;
            }
        }
        label345: boolean bool3 = this.parent.update(this, paramDriverCall);
        if ((i != 0) || (bool3));
        for (boolean bool4 = true; ; bool4 = false)
            break;
    }

    public void updateParent(CdmaCall paramCdmaCall1, CdmaCall paramCdmaCall2)
    {
        if (paramCdmaCall2 != paramCdmaCall1)
        {
            if (paramCdmaCall1 != null)
                paramCdmaCall1.detach(this);
            paramCdmaCall2.attachFake(this, Call.State.ACTIVE);
            this.parent = paramCdmaCall2;
        }
    }

    class MyHandler extends Handler
    {
        MyHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return;
                CdmaConnection.this.processNextPostDialChar();
                continue;
                CdmaConnection.this.releaseWakeLock();
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String nullifyString(String paramString)
        {
            return null;
        }

        static void setPostDialState(CdmaConnection paramCdmaConnection, Connection.PostDialState paramPostDialState)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaConnection
 * JD-Core Version:        0.6.2
 */