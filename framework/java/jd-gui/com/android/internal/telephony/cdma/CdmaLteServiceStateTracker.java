package com.android.internal.telephony.cdma;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Message;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.cdma.CdmaCellLocation;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Log;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.ServiceStateTracker;
import com.android.internal.telephony.gsm.GsmDataConnectionTracker;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class CdmaLteServiceStateTracker extends CdmaServiceStateTracker
{
    CDMALTEPhone mCdmaLtePhone;
    private ServiceState mLteSS;

    public CdmaLteServiceStateTracker(CDMALTEPhone paramCDMALTEPhone)
    {
        super(paramCDMALTEPhone);
        this.mCdmaLtePhone = paramCDMALTEPhone;
        this.mLteSS = new ServiceState();
        log("CdmaLteServiceStateTracker Constructors");
    }

    private boolean isInHomeSidNid(int paramInt1, int paramInt2)
    {
        boolean bool = true;
        if (isSidsAllZeros());
        while (true)
        {
            return bool;
            if ((this.mHomeSystemId.length == this.mHomeNetworkId.length) && (paramInt1 != 0))
            {
                for (int i = 0; ; i++)
                {
                    if (i >= this.mHomeSystemId.length)
                        break label101;
                    if ((this.mHomeSystemId[i] == paramInt1) && ((this.mHomeNetworkId[i] == 0) || (this.mHomeNetworkId[i] == 65535) || (paramInt2 == 0) || (paramInt2 == 65535) || (this.mHomeNetworkId[i] == paramInt2)))
                        break;
                }
                label101: bool = false;
            }
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("CdmaLteServiceStateTracker extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" mCdmaLtePhone=" + this.mCdmaLtePhone);
        paramPrintWriter.println(" mLteSS=" + this.mLteSS);
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
            super.handleMessage(paramMessage);
        case 5:
        case 27:
        }
        while (true)
        {
            return;
            log("handleMessage EVENT_POLL_STATE_GPRS");
            AsyncResult localAsyncResult = (AsyncResult)paramMessage.obj;
            handlePollStateResult(paramMessage.what, localAsyncResult);
            continue;
            CdmaLteUiccRecords localCdmaLteUiccRecords = (CdmaLteUiccRecords)this.phone.mIccRecords;
            if ((localCdmaLteUiccRecords != null) && (localCdmaLteUiccRecords.isProvisioned()))
            {
                this.mMdn = localCdmaLteUiccRecords.getMdn();
                this.mMin = localCdmaLteUiccRecords.getMin();
                parseSidNid(localCdmaLteUiccRecords.getSid(), localCdmaLteUiccRecords.getNid());
                this.mPrlVersion = localCdmaLteUiccRecords.getPrlVersion();
                this.mIsMinInfoReady = true;
                updateOtaspState();
            }
            pollState();
        }
    }

    protected void handlePollStateResultMessage(int paramInt, AsyncResult paramAsyncResult)
    {
        String[] arrayOfString;
        int i;
        int j;
        if (paramInt == 5)
        {
            log("handlePollStateResultMessage: EVENT_POLL_STATE_GPRS");
            arrayOfString = (String[])paramAsyncResult.result;
            i = 0;
            j = -1;
            if (arrayOfString.length <= 0);
        }
        while (true)
        {
            try
            {
                j = Integer.parseInt(arrayOfString[0]);
                if ((arrayOfString.length >= 4) && (arrayOfString[3] != null))
                {
                    int k = Integer.parseInt(arrayOfString[3]);
                    i = k;
                }
                this.mLteSS.setRadioTechnology(i);
                this.mLteSS.setState(regCodeToServiceState(j));
                return;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                loge("handlePollStateResultMessage: error parsing GprsRegistrationState: " + localNumberFormatException);
                continue;
            }
            super.handlePollStateResultMessage(paramInt, paramAsyncResult);
        }
    }

    public boolean isConcurrentVoiceAndDataAllowed()
    {
        if (this.mRilRadioTechnology == 14);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[CdmaLteSST] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("CDMA", "[CdmaLteSST] " + paramString);
    }

    protected void onSignalStrengthResult(AsyncResult paramAsyncResult)
    {
        if (paramAsyncResult.exception != null)
            setSignalStrengthDefaultValues();
        try
        {
            while (true)
            {
                this.phone.notifySignalStrength();
                return;
                int[] arrayOfInt = (int[])paramAsyncResult.result;
                int i = -1;
                int j = -1;
                int k = -1;
                int m = 2147483647;
                int n = -1;
                int i1;
                label67: int i2;
                label79: int i3;
                label91: int i4;
                if (arrayOfInt[2] > 0)
                {
                    i1 = -arrayOfInt[2];
                    if (arrayOfInt[3] <= 0)
                        break label209;
                    i2 = -arrayOfInt[3];
                    if (arrayOfInt[4] <= 0)
                        break label217;
                    i3 = -arrayOfInt[4];
                    if (arrayOfInt[5] <= 0)
                        break label224;
                    i4 = -arrayOfInt[5];
                    label103: if ((arrayOfInt[6] <= 0) || (arrayOfInt[6] > 8))
                        break label231;
                }
                label209: label217: label224: label231: for (int i5 = arrayOfInt[6]; ; i5 = -1)
                {
                    if (this.mRilRadioTechnology == 14)
                    {
                        i = arrayOfInt[7];
                        j = arrayOfInt[8];
                        k = arrayOfInt[9];
                        m = arrayOfInt[10];
                        n = arrayOfInt[11];
                    }
                    if (this.mRilRadioTechnology == 14)
                        break label238;
                    this.mSignalStrength = new SignalStrength(99, -1, i1, i2, i3, i4, i5, false);
                    break;
                    i1 = -120;
                    break label67;
                    i2 = -160;
                    break label79;
                    i3 = -120;
                    break label91;
                    i4 = -1;
                    break label103;
                }
                label238: this.mSignalStrength = new SignalStrength(99, -1, i1, i2, i3, i4, i5, i, j, k, m, n, true);
            }
        }
        catch (NullPointerException localNullPointerException)
        {
            while (true)
                loge("onSignalStrengthResult() Phone already destroyed: " + localNullPointerException + "SignalStrength not notified");
        }
    }

    protected void pollState()
    {
        this.pollingContext = new int[1];
        this.pollingContext[0] = 0;
        switch (1.$SwitchMap$com$android$internal$telephony$CommandsInterface$RadioState[this.cm.getRadioState().ordinal()])
        {
        default:
            int[] arrayOfInt1 = this.pollingContext;
            arrayOfInt1[0] = (1 + arrayOfInt1[0]);
            this.cm.getOperator(obtainMessage(25, this.pollingContext));
            int[] arrayOfInt2 = this.pollingContext;
            arrayOfInt2[0] = (1 + arrayOfInt2[0]);
            this.cm.getVoiceRegistrationState(obtainMessage(24, this.pollingContext));
            int i = Settings.Secure.getInt(this.phone.getContext().getContentResolver(), "preferred_network_mode", 0);
            log("pollState: network mode here is = " + i);
            if ((i == 7) || (i == 11))
            {
                int[] arrayOfInt3 = this.pollingContext;
                arrayOfInt3[0] = (1 + arrayOfInt3[0]);
                this.cm.getDataRegistrationState(obtainMessage(5, this.pollingContext));
            }
            break;
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            this.newSS.setStateOutOfService();
            this.newCellLoc.setStateInvalid();
            setSignalStrengthDefaultValues();
            this.mGotCountryCode = false;
            pollStateDone();
            continue;
            this.newSS.setStateOff();
            this.newCellLoc.setStateInvalid();
            setSignalStrengthDefaultValues();
            this.mGotCountryCode = false;
            pollStateDone();
        }
    }

    protected void pollStateDone()
    {
        boolean bool1;
        label192: boolean bool2;
        label214: boolean bool3;
        label230: boolean bool4;
        label247: boolean bool5;
        label261: boolean bool6;
        label275: boolean bool7;
        label292: boolean bool8;
        label315: boolean bool9;
        label338: boolean bool10;
        label355: boolean bool11;
        label401: boolean bool12;
        label440: boolean bool13;
        label460: String str5;
        label962: String str1;
        String str2;
        label1139: CDMAPhone localCDMAPhone;
        if (this.mLteSS.getState() == 0)
        {
            this.mNewRilRadioTechnology = this.mLteSS.getRilRadioTechnology();
            this.mNewDataConnectionState = this.mLteSS.getState();
            this.newSS.setRadioTechnology(this.mNewRilRadioTechnology);
            log("pollStateDone LTE/eHRPD STATE_IN_SERVICE mNewRilRadioTechnology = " + this.mNewRilRadioTechnology);
            if ((this.newSS.getState() == 1) && (Settings.Secure.getInt(this.phone.getContext().getContentResolver(), "preferred_network_mode", 0) == 11))
            {
                log("pollState: LTE Only mode");
                this.newSS.setState(this.mLteSS.getState());
            }
            log("pollStateDone: oldSS=[" + this.ss + "] newSS=[" + this.newSS + "]");
            if ((this.ss.getState() == 0) || (this.newSS.getState() != 0))
                break label1334;
            bool1 = true;
            if ((this.ss.getState() != 0) || (this.newSS.getState() == 0))
                break label1339;
            bool2 = true;
            if ((this.mDataConnectionState == 0) || (this.mNewDataConnectionState != 0))
                break label1344;
            bool3 = true;
            if ((this.mDataConnectionState != 0) || (this.mNewDataConnectionState == 0))
                break label1349;
            bool4 = true;
            if (this.mDataConnectionState == this.mNewDataConnectionState)
                break label1355;
            bool5 = true;
            if (this.mRilRadioTechnology == this.mNewRilRadioTechnology)
                break label1361;
            bool6 = true;
            if (this.newSS.equals(this.ss))
                break label1367;
            bool7 = true;
            if ((this.ss.getRoaming()) || (!this.newSS.getRoaming()))
                break label1373;
            bool8 = true;
            if ((!this.ss.getRoaming()) || (this.newSS.getRoaming()))
                break label1379;
            bool9 = true;
            if (this.newCellLoc.equals(this.cellLoc))
                break label1385;
            bool10 = true;
            if ((this.mNewDataConnectionState != 0) || (((this.mRilRadioTechnology != 14) || (this.mNewRilRadioTechnology != 13)) && ((this.mRilRadioTechnology != 13) || (this.mNewRilRadioTechnology != 14))))
                break label1391;
            bool11 = true;
            if (((this.mNewRilRadioTechnology != 14) && (this.mNewRilRadioTechnology != 13)) || (this.mRilRadioTechnology == 14) || (this.mRilRadioTechnology == 13))
                break label1397;
            bool12 = true;
            if ((this.mNewRilRadioTechnology < 4) || (this.mNewRilRadioTechnology > 8))
                break label1403;
            bool13 = true;
            log("pollStateDone: hasRegistered=" + bool1 + " hasDeegistered=" + bool2 + " hasCdmaDataConnectionAttached=" + bool3 + " hasCdmaDataConnectionDetached=" + bool4 + " hasCdmaDataConnectionChanged=" + bool5 + " hasRadioTechnologyChanged = " + bool6 + " hasChanged=" + bool7 + " hasRoamingOn=" + bool8 + " hasRoamingOff=" + bool9 + " hasLocationChanged=" + bool10 + " has4gHandoff = " + bool11 + " hasMultiApnSupport=" + bool12 + " hasLostMultiApnSupport=" + bool13);
            if ((this.ss.getState() != this.newSS.getState()) || (this.mDataConnectionState != this.mNewDataConnectionState))
            {
                Object[] arrayOfObject = new Object[4];
                arrayOfObject[0] = Integer.valueOf(this.ss.getState());
                arrayOfObject[1] = Integer.valueOf(this.mDataConnectionState);
                arrayOfObject[2] = Integer.valueOf(this.newSS.getState());
                arrayOfObject[3] = Integer.valueOf(this.mNewDataConnectionState);
                EventLog.writeEvent(50116, arrayOfObject);
            }
            ServiceState localServiceState = this.ss;
            this.ss = this.newSS;
            this.newSS = localServiceState;
            this.newSS.setStateOutOfService();
            this.mLteSS.setStateOutOfService();
            if ((bool12) && ((this.phone.mDataConnectionTracker instanceof CdmaDataConnectionTracker)))
            {
                log("GsmDataConnectionTracker Created");
                this.phone.mDataConnectionTracker.dispose();
                this.phone.mDataConnectionTracker = new GsmDataConnectionTracker(this.mCdmaLtePhone);
            }
            if ((bool13) && ((this.phone.mDataConnectionTracker instanceof GsmDataConnectionTracker)))
            {
                log("GsmDataConnectionTracker disposed");
                this.phone.mDataConnectionTracker.dispose();
                this.phone.mDataConnectionTracker = new CdmaDataConnectionTracker(this.phone);
            }
            CdmaCellLocation localCdmaCellLocation = this.cellLoc;
            this.cellLoc = this.newCellLoc;
            this.newCellLoc = localCdmaCellLocation;
            this.mDataConnectionState = this.mNewDataConnectionState;
            this.mRilRadioTechnology = this.mNewRilRadioTechnology;
            this.mNewRilRadioTechnology = 0;
            this.newSS.setStateOutOfService();
            if (bool6)
                this.phone.setSystemProperty("gsm.network.type", ServiceState.rilRadioTechnologyToString(this.mRilRadioTechnology));
            if (bool1)
                this.mNetworkAttachedRegistrants.notifyRegistrants();
            if (bool7)
            {
                if (this.phone.isEriFileLoaded())
                {
                    if (this.ss.getState() != 0)
                        break label1409;
                    str5 = this.phone.getCdmaEriText();
                    this.ss.setOperatorAlphaLong(str5);
                }
                if (this.phone.getIccCard().getState() == IccCard.State.READY)
                {
                    boolean bool14 = ((CdmaLteUiccRecords)this.phone.mIccRecords).getCsimSpnDisplayCondition();
                    int i = this.ss.getCdmaEriIconIndex();
                    if ((bool14) && (i == 1) && (isInHomeSidNid(this.ss.getSystemId(), this.ss.getNetworkId())))
                        this.ss.setOperatorAlphaLong(this.phone.mIccRecords.getServiceProviderName());
                }
                this.phone.setSystemProperty("gsm.operator.alpha", this.ss.getOperatorAlphaLong());
                str1 = SystemProperties.get("gsm.operator.numeric", "");
                str2 = this.ss.getOperatorNumeric();
                this.phone.setSystemProperty("gsm.operator.numeric", str2);
                if (str2 != null)
                    break label1472;
                log("operatorNumeric is null");
                this.phone.setSystemProperty("gsm.operator.iso-country", "");
                this.mGotCountryCode = false;
                localCDMAPhone = this.phone;
                if (!this.ss.getRoaming())
                    break label1609;
            }
        }
        label1334: label1339: label1344: label1349: label1609: for (String str3 = "true"; ; str3 = "false")
            while (true)
            {
                localCDMAPhone.setSystemProperty("gsm.operator.isroaming", str3);
                updateSpnDisplay();
                this.phone.notifyServiceStateChanged(this.ss);
                if ((bool3) || (bool11))
                    this.mAttachedRegistrants.notifyRegistrants();
                if (bool4)
                    this.mDetachedRegistrants.notifyRegistrants();
                if ((bool5) || (bool6))
                    this.phone.notifyDataConnection(null);
                if (bool8)
                    this.mRoamingOnRegistrants.notifyRegistrants();
                if (bool9)
                    this.mRoamingOffRegistrants.notifyRegistrants();
                if (bool10)
                    this.phone.notifyLocationChanged();
                return;
                this.mNewRilRadioTechnology = this.newSS.getRilRadioTechnology();
                this.mNewDataConnectionState = radioTechnologyToDataServiceState(this.mNewRilRadioTechnology);
                log("pollStateDone CDMA STATE_IN_SERVICE mNewRilRadioTechnology = " + this.mNewRilRadioTechnology + " mNewDataConnectionState = " + this.mNewDataConnectionState);
                break;
                bool1 = false;
                break label192;
                bool2 = false;
                break label214;
                bool3 = false;
                break label230;
                bool4 = false;
                break label247;
                label1355: bool5 = false;
                break label261;
                label1361: bool6 = false;
                break label275;
                label1367: bool7 = false;
                break label292;
                label1373: bool8 = false;
                break label315;
                label1379: bool9 = false;
                break label338;
                label1385: bool10 = false;
                break label355;
                label1391: bool11 = false;
                break label401;
                label1397: bool12 = false;
                break label440;
                label1403: bool13 = false;
                break label460;
                if (this.ss.getState() == 3)
                {
                    str5 = this.phone.mIccRecords.getServiceProviderName();
                    if (!TextUtils.isEmpty(str5))
                        break label962;
                    str5 = SystemProperties.get("ro.cdma.home.operator.alpha");
                    break label962;
                }
                str5 = this.phone.getContext().getText(17039625).toString();
                break label962;
                Object localObject = "";
                str2.substring(0, 3);
                try
                {
                    String str4 = MccTable.countryCodeForMcc(Integer.parseInt(str2.substring(0, 3)));
                    localObject = str4;
                    this.phone.setSystemProperty("gsm.operator.iso-country", (String)localObject);
                    this.mGotCountryCode = true;
                    if (!shouldFixTimeZoneNow(this.phone, str2, str1, this.mNeedFixZone))
                        break label1139;
                    fixTimeZone((String)localObject);
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    while (true)
                        loge("countryCodeForMcc error" + localNumberFormatException);
                }
                catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException)
                {
                    while (true)
                        loge("countryCodeForMcc error" + localStringIndexOutOfBoundsException);
                }
            }
    }

    protected void setCdmaTechnology(int paramInt)
    {
        this.newSS.setRadioTechnology(paramInt);
    }

    protected void setSignalStrengthDefaultValues()
    {
        this.mSignalStrength = new SignalStrength(99, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2147483647, -1, false);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaLteServiceStateTracker
 * JD-Core Version:        0.6.2
 */