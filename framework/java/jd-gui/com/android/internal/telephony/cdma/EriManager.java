package com.android.internal.telephony.cdma;

import android.content.Context;
import android.util.Log;
import com.android.internal.telephony.PhoneBase;
import java.util.HashMap;

public final class EriManager
{
    private static final boolean DBG = true;
    public static final int ERI_FROM_FILE_SYSTEM = 1;
    public static final int ERI_FROM_MODEM = 2;
    public static final int ERI_FROM_XML = 0;
    private static final String LOG_TAG = "CDMA";
    private static final boolean VDBG;
    private boolean isEriFileLoaded;
    private Context mContext;
    private EriFile mEriFile;
    private int mEriFileSource = 0;
    private PhoneBase mPhone;

    public EriManager(PhoneBase paramPhoneBase, Context paramContext, int paramInt)
    {
        this.mPhone = paramPhoneBase;
        this.mContext = paramContext;
        this.mEriFileSource = paramInt;
        this.mEriFile = new EriFile();
    }

    private EriDisplayInformation getEriDisplayInformation(int paramInt1, int paramInt2)
    {
        Object localObject;
        if (this.isEriFileLoaded)
        {
            EriInfo localEriInfo3 = getEriInfo(paramInt1);
            if (localEriInfo3 != null)
            {
                localObject = new EriDisplayInformation(localEriInfo3.mIconIndex, localEriInfo3.mIconMode, localEriInfo3.mEriText);
                return localObject;
            }
        }
        EriDisplayInformation localEriDisplayInformation;
        switch (paramInt1)
        {
        default:
            if (!this.isEriFileLoaded)
            {
                Log.d("CDMA", "ERI File not loaded");
                if (paramInt2 > 2)
                    localEriDisplayInformation = new EriDisplayInformation(2, 1, this.mContext.getText(17039614).toString());
            }
            break;
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        }
        while (true)
        {
            localObject = localEriDisplayInformation;
            break;
            localEriDisplayInformation = new EriDisplayInformation(0, 0, this.mContext.getText(17039612).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(1, 0, this.mContext.getText(17039613).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(2, 1, this.mContext.getText(17039614).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039615).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039616).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039617).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039618).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039619).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039620).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039621).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039622).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039623).toString());
            continue;
            localEriDisplayInformation = new EriDisplayInformation(paramInt1, 0, this.mContext.getText(17039624).toString());
            continue;
            switch (paramInt2)
            {
            default:
                localEriDisplayInformation = new EriDisplayInformation(-1, -1, "ERI text");
                break;
            case 0:
                localEriDisplayInformation = new EriDisplayInformation(0, 0, this.mContext.getText(17039612).toString());
                break;
            case 1:
                localEriDisplayInformation = new EriDisplayInformation(1, 0, this.mContext.getText(17039613).toString());
                break;
            case 2:
                localEriDisplayInformation = new EriDisplayInformation(2, 1, this.mContext.getText(17039614).toString());
                continue;
                EriInfo localEriInfo1 = getEriInfo(paramInt1);
                EriInfo localEriInfo2 = getEriInfo(paramInt2);
                if (localEriInfo1 == null)
                {
                    if (localEriInfo2 == null)
                    {
                        Log.e("CDMA", "ERI defRoamInd " + paramInt2 + " not found in ERI file ...on");
                        localEriDisplayInformation = new EriDisplayInformation(0, 0, this.mContext.getText(17039612).toString());
                    }
                    else
                    {
                        localEriDisplayInformation = new EriDisplayInformation(localEriInfo2.mIconIndex, localEriInfo2.mIconMode, localEriInfo2.mEriText);
                    }
                }
                else
                    localEriDisplayInformation = new EriDisplayInformation(localEriInfo1.mIconIndex, localEriInfo1.mIconMode, localEriInfo1.mEriText);
                break;
            }
        }
    }

    private EriInfo getEriInfo(int paramInt)
    {
        if (this.mEriFile.mRoamIndTable.containsKey(Integer.valueOf(paramInt)));
        for (EriInfo localEriInfo = (EriInfo)this.mEriFile.mRoamIndTable.get(Integer.valueOf(paramInt)); ; localEriInfo = null)
            return localEriInfo;
    }

    private void loadEriFileFromFileSystem()
    {
    }

    private void loadEriFileFromModem()
    {
    }

    // ERROR //
    private void loadEriFileFromXml()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: aload_0
        //     3: getfield 43	com/android/internal/telephony/cdma/EriManager:mContext	Landroid/content/Context;
        //     6: invokevirtual 156	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     9: astore_2
        //     10: ldc 23
        //     12: ldc 158
        //     14: invokestatic 78	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     17: pop
        //     18: new 160	java/io/FileInputStream
        //     21: dup
        //     22: aload_2
        //     23: ldc 161
        //     25: invokevirtual 167	android/content/res/Resources:getString	(I)Ljava/lang/String;
        //     28: invokespecial 170	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     31: astore 29
        //     33: invokestatic 176	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     36: astore 5
        //     38: aload 5
        //     40: aload 29
        //     42: aconst_null
        //     43: invokeinterface 182 3 0
        //     48: ldc 23
        //     50: ldc 184
        //     52: invokestatic 78	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     55: pop
        //     56: aload 29
        //     58: astore_1
        //     59: aload 5
        //     61: ifnonnull +19 -> 80
        //     64: ldc 23
        //     66: ldc 186
        //     68: invokestatic 78	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     71: pop
        //     72: aload_2
        //     73: ldc 187
        //     75: invokevirtual 191	android/content/res/Resources:getXml	(I)Landroid/content/res/XmlResourceParser;
        //     78: astore 5
        //     80: aload 5
        //     82: ldc 192
        //     84: invokestatic 198	com/android/internal/util/XmlUtils:beginDocument	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
        //     87: aload_0
        //     88: getfield 48	com/android/internal/telephony/cdma/EriManager:mEriFile	Lcom/android/internal/telephony/cdma/EriManager$EriFile;
        //     91: aload 5
        //     93: aconst_null
        //     94: ldc 200
        //     96: invokeinterface 204 3 0
        //     101: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     104: putfield 211	com/android/internal/telephony/cdma/EriManager$EriFile:mVersionNumber	I
        //     107: aload_0
        //     108: getfield 48	com/android/internal/telephony/cdma/EriManager:mEriFile	Lcom/android/internal/telephony/cdma/EriManager$EriFile;
        //     111: aload 5
        //     113: aconst_null
        //     114: ldc 213
        //     116: invokeinterface 204 3 0
        //     121: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     124: putfield 216	com/android/internal/telephony/cdma/EriManager$EriFile:mNumberOfEriEntries	I
        //     127: aload_0
        //     128: getfield 48	com/android/internal/telephony/cdma/EriManager:mEriFile	Lcom/android/internal/telephony/cdma/EriManager$EriFile;
        //     131: aload 5
        //     133: aconst_null
        //     134: ldc 218
        //     136: invokeinterface 204 3 0
        //     141: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     144: putfield 221	com/android/internal/telephony/cdma/EriManager$EriFile:mEriFileType	I
        //     147: iconst_0
        //     148: istore 11
        //     150: aload 5
        //     152: invokestatic 225	com/android/internal/util/XmlUtils:nextElement	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     155: aload 5
        //     157: invokeinterface 228 1 0
        //     162: astore 12
        //     164: aload 12
        //     166: ifnonnull +132 -> 298
        //     169: iload 11
        //     171: aload_0
        //     172: getfield 48	com/android/internal/telephony/cdma/EriManager:mEriFile	Lcom/android/internal/telephony/cdma/EriManager$EriFile;
        //     175: getfield 216	com/android/internal/telephony/cdma/EriManager$EriFile:mNumberOfEriEntries	I
        //     178: if_icmpeq +49 -> 227
        //     181: ldc 23
        //     183: new 105	java/lang/StringBuilder
        //     186: dup
        //     187: invokespecial 106	java/lang/StringBuilder:<init>	()V
        //     190: ldc 230
        //     192: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     195: aload_0
        //     196: getfield 48	com/android/internal/telephony/cdma/EriManager:mEriFile	Lcom/android/internal/telephony/cdma/EriManager$EriFile;
        //     199: getfield 216	com/android/internal/telephony/cdma/EriManager$EriFile:mNumberOfEriEntries	I
        //     202: invokevirtual 115	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     205: ldc 232
        //     207: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     210: iload 11
        //     212: invokevirtual 115	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     215: ldc 234
        //     217: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     220: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     223: invokestatic 121	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     226: pop
        //     227: ldc 23
        //     229: ldc 236
        //     231: invokestatic 78	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     234: pop
        //     235: aload_0
        //     236: iconst_1
        //     237: putfield 52	com/android/internal/telephony/cdma/EriManager:isEriFileLoaded	Z
        //     240: aload 5
        //     242: instanceof 238
        //     245: ifeq +13 -> 258
        //     248: aload 5
        //     250: checkcast 238	android/content/res/XmlResourceParser
        //     253: invokeinterface 241 1 0
        //     258: aload_1
        //     259: ifnull +7 -> 266
        //     262: aload_1
        //     263: invokevirtual 242	java/io/FileInputStream:close	()V
        //     266: return
        //     267: astore 26
        //     269: ldc 23
        //     271: ldc 244
        //     273: invokestatic 78	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     276: pop
        //     277: aconst_null
        //     278: astore 5
        //     280: goto -221 -> 59
        //     283: astore_3
        //     284: ldc 23
        //     286: ldc 246
        //     288: invokestatic 78	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     291: pop
        //     292: aconst_null
        //     293: astore 5
        //     295: goto -236 -> 59
        //     298: aload 12
        //     300: ldc 248
        //     302: invokevirtual 253	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     305: ifeq +171 -> 476
        //     308: aload 5
        //     310: aconst_null
        //     311: ldc 255
        //     313: invokeinterface 204 3 0
        //     318: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     321: istore 20
        //     323: aload 5
        //     325: aconst_null
        //     326: ldc_w 257
        //     329: invokeinterface 204 3 0
        //     334: astore 21
        //     336: iload 20
        //     338: iflt +71 -> 409
        //     341: iload 20
        //     343: iconst_2
        //     344: if_icmpgt +65 -> 409
        //     347: aload_0
        //     348: getfield 48	com/android/internal/telephony/cdma/EriManager:mEriFile	Lcom/android/internal/telephony/cdma/EriManager$EriFile;
        //     351: getfield 261	com/android/internal/telephony/cdma/EriManager$EriFile:mCallPromptId	[Ljava/lang/String;
        //     354: iload 20
        //     356: aload 21
        //     358: aastore
        //     359: goto -209 -> 150
        //     362: astore 8
        //     364: ldc 23
        //     366: ldc_w 263
        //     369: aload 8
        //     371: invokestatic 266	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     374: pop
        //     375: aload 5
        //     377: instanceof 238
        //     380: ifeq +13 -> 393
        //     383: aload 5
        //     385: checkcast 238	android/content/res/XmlResourceParser
        //     388: invokeinterface 241 1 0
        //     393: aload_1
        //     394: ifnull -128 -> 266
        //     397: aload_1
        //     398: invokevirtual 242	java/io/FileInputStream:close	()V
        //     401: goto -135 -> 266
        //     404: astore 10
        //     406: goto -140 -> 266
        //     409: ldc 23
        //     411: new 105	java/lang/StringBuilder
        //     414: dup
        //     415: invokespecial 106	java/lang/StringBuilder:<init>	()V
        //     418: ldc_w 268
        //     421: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     424: iload 20
        //     426: invokevirtual 115	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     429: ldc_w 270
        //     432: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     435: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     438: invokestatic 121	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     441: pop
        //     442: goto -292 -> 150
        //     445: astore 6
        //     447: aload 5
        //     449: instanceof 238
        //     452: ifeq +13 -> 465
        //     455: aload 5
        //     457: checkcast 238	android/content/res/XmlResourceParser
        //     460: invokeinterface 241 1 0
        //     465: aload_1
        //     466: ifnull +7 -> 473
        //     469: aload_1
        //     470: invokevirtual 242	java/io/FileInputStream:close	()V
        //     473: aload 6
        //     475: athrow
        //     476: aload 12
        //     478: ldc_w 272
        //     481: invokevirtual 253	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     484: ifeq -334 -> 150
        //     487: aload 5
        //     489: aconst_null
        //     490: ldc_w 274
        //     493: invokeinterface 204 3 0
        //     498: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     501: istore 13
        //     503: aload 5
        //     505: aconst_null
        //     506: ldc_w 276
        //     509: invokeinterface 204 3 0
        //     514: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     517: istore 14
        //     519: aload 5
        //     521: aconst_null
        //     522: ldc_w 278
        //     525: invokeinterface 204 3 0
        //     530: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     533: istore 15
        //     535: aload 5
        //     537: aconst_null
        //     538: ldc_w 280
        //     541: invokeinterface 204 3 0
        //     546: astore 16
        //     548: aload 5
        //     550: aconst_null
        //     551: ldc 248
        //     553: invokeinterface 204 3 0
        //     558: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     561: istore 17
        //     563: aload 5
        //     565: aconst_null
        //     566: ldc_w 282
        //     569: invokeinterface 204 3 0
        //     574: invokestatic 208	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     577: istore 18
        //     579: iinc 11 1
        //     582: aload_0
        //     583: getfield 48	com/android/internal/telephony/cdma/EriManager:mEriFile	Lcom/android/internal/telephony/cdma/EriManager$EriFile;
        //     586: getfield 125	com/android/internal/telephony/cdma/EriManager$EriFile:mRoamIndTable	Ljava/util/HashMap;
        //     589: iload 13
        //     591: invokestatic 131	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     594: new 58	com/android/internal/telephony/cdma/EriInfo
        //     597: dup
        //     598: iload 13
        //     600: iload 14
        //     602: iload 15
        //     604: aload 16
        //     606: iload 17
        //     608: iload 18
        //     610: invokespecial 285	com/android/internal/telephony/cdma/EriInfo:<init>	(IIILjava/lang/String;II)V
        //     613: invokevirtual 289	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     616: pop
        //     617: goto -467 -> 150
        //     620: astore 7
        //     622: goto -149 -> 473
        //     625: astore 31
        //     627: aload 29
        //     629: astore_1
        //     630: goto -346 -> 284
        //     633: astore 30
        //     635: aload 29
        //     637: astore_1
        //     638: goto -369 -> 269
        //
        // Exception table:
        //     from	to	target	type
        //     10	33	267	java/io/FileNotFoundException
        //     10	33	283	org/xmlpull/v1/XmlPullParserException
        //     80	240	362	java/lang/Exception
        //     298	359	362	java/lang/Exception
        //     409	442	362	java/lang/Exception
        //     476	617	362	java/lang/Exception
        //     262	266	404	java/io/IOException
        //     397	401	404	java/io/IOException
        //     80	240	445	finally
        //     298	359	445	finally
        //     364	375	445	finally
        //     409	442	445	finally
        //     476	617	445	finally
        //     469	473	620	java/io/IOException
        //     33	56	625	org/xmlpull/v1/XmlPullParserException
        //     33	56	633	java/io/FileNotFoundException
    }

    public void dispose()
    {
        this.mEriFile = new EriFile();
        this.isEriFileLoaded = false;
    }

    public int getCdmaEriIconIndex(int paramInt1, int paramInt2)
    {
        return getEriDisplayInformation(paramInt1, paramInt2).mEriIconIndex;
    }

    public int getCdmaEriIconMode(int paramInt1, int paramInt2)
    {
        return getEriDisplayInformation(paramInt1, paramInt2).mEriIconMode;
    }

    public String getCdmaEriText(int paramInt1, int paramInt2)
    {
        return getEriDisplayInformation(paramInt1, paramInt2).mEriIconText;
    }

    public int getEriFileType()
    {
        return this.mEriFile.mEriFileType;
    }

    public int getEriFileVersion()
    {
        return this.mEriFile.mVersionNumber;
    }

    public int getEriNumberOfEntries()
    {
        return this.mEriFile.mNumberOfEriEntries;
    }

    public boolean isEriFileLoaded()
    {
        return this.isEriFileLoaded;
    }

    public void loadEriFile()
    {
        switch (this.mEriFileSource)
        {
        default:
            loadEriFileFromXml();
        case 2:
        case 1:
        }
        while (true)
        {
            return;
            loadEriFileFromModem();
            continue;
            loadEriFileFromFileSystem();
        }
    }

    class EriDisplayInformation
    {
        public int mEriIconIndex;
        public int mEriIconMode;
        public String mEriIconText;

        public EriDisplayInformation(int paramInt1, int paramString, String arg4)
        {
            this.mEriIconIndex = paramInt1;
            this.mEriIconMode = paramString;
            Object localObject;
            this.mEriIconText = localObject;
        }

        public String toString()
        {
            return "EriDisplayInformation: { IconIndex: " + this.mEriIconIndex + " EriIconMode: " + this.mEriIconMode + " EriIconText: " + this.mEriIconText + " }";
        }
    }

    class EriFile
    {
        public String[] mCallPromptId;
        public int mEriFileType = -1;
        public int mNumberOfEriEntries = 0;
        public HashMap<Integer, EriInfo> mRoamIndTable;
        public int mVersionNumber = -1;

        public EriFile()
        {
            String[] arrayOfString = new String[3];
            arrayOfString[0] = "";
            arrayOfString[1] = "";
            arrayOfString[2] = "";
            this.mCallPromptId = arrayOfString;
            this.mRoamIndTable = new HashMap();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.EriManager
 * JD-Core Version:        0.6.2
 */