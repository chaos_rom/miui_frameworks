package com.android.internal.telephony.cdma;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Message;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.util.Log;
import com.android.internal.telephony.AdnRecordCache;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccException;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.IccRefreshResponse;
import com.android.internal.telephony.IccUtils;
import com.android.internal.telephony.MccTable;

public final class RuimRecords extends IccRecords
{
    private static final boolean DBG = true;
    private static final int EVENT_GET_ALL_SMS_DONE = 18;
    private static final int EVENT_GET_CDMA_SUBSCRIPTION_DONE = 10;
    private static final int EVENT_GET_DEVICE_IDENTITY_DONE = 4;
    private static final int EVENT_GET_ICCID_DONE = 5;
    private static final int EVENT_GET_IMSI_DONE = 3;
    private static final int EVENT_GET_SMS_DONE = 22;
    private static final int EVENT_GET_SST_DONE = 17;
    private static final int EVENT_MARK_SMS_READ_DONE = 19;
    private static final int EVENT_RADIO_OFF_OR_NOT_AVAILABLE = 2;
    private static final int EVENT_RUIM_REFRESH = 31;
    private static final int EVENT_SMS_ON_RUIM = 21;
    private static final int EVENT_UPDATE_DONE = 14;
    static final String LOG_TAG = "CDMA";
    private String mImsi;
    private String mMin2Min1;
    private String mMyMobileNumber;
    private String mPrlVersion;
    private boolean m_ota_commited = false;

    public RuimRecords(IccCard paramIccCard, Context paramContext, CommandsInterface paramCommandsInterface)
    {
        super(paramIccCard, paramContext, paramCommandsInterface);
        this.adnCache = new AdnRecordCache(this.mFh);
        this.recordsRequested = false;
        this.recordsToLoad = 0;
        this.mCi.registerForOffOrNotAvailable(this, 2, null);
        this.mCi.registerForIccRefresh(this, 31, null);
        onRadioOffOrNotAvailable();
    }

    private void fetchRuimRecords()
    {
        this.recordsRequested = true;
        Log.v("CDMA", "RuimRecords:fetchRuimRecords " + this.recordsToLoad);
        this.mCi.getIMSI(obtainMessage(3));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(12258, obtainMessage(5));
        this.recordsToLoad = (1 + this.recordsToLoad);
        log("RuimRecords:fetchRuimRecords " + this.recordsToLoad + " requested: " + this.recordsRequested);
    }

    private void handleRuimRefresh(IccRefreshResponse paramIccRefreshResponse)
    {
        if (paramIccRefreshResponse == null)
            log("handleRuimRefresh received without input");
        while (true)
        {
            return;
            if ((paramIccRefreshResponse.aid == null) || (paramIccRefreshResponse.aid.equals(this.mParentCard.getAid())))
                switch (paramIccRefreshResponse.refreshResult)
                {
                default:
                    log("handleRuimRefresh with unknown operation");
                    break;
                case 0:
                    log("handleRuimRefresh with SIM_REFRESH_FILE_UPDATED");
                    this.adnCache.reset();
                    fetchRuimRecords();
                    break;
                case 1:
                    log("handleRuimRefresh with SIM_REFRESH_INIT");
                    fetchRuimRecords();
                    break;
                case 2:
                    log("handleRuimRefresh with SIM_REFRESH_RESET");
                    this.mCi.setRadioPower(false, null);
                }
        }
    }

    public void dispose()
    {
        log("Disposing RuimRecords " + this);
        this.mCi.unregisterForOffOrNotAvailable(this);
        this.mCi.unregisterForIccRefresh(this);
        super.dispose();
    }

    protected void finalize()
    {
        log("RuimRecords finalized");
    }

    public String getCdmaMin()
    {
        return this.mMin2Min1;
    }

    public int getDisplayRule(String paramString)
    {
        return 0;
    }

    public String getMdnNumber()
    {
        return this.mMyMobileNumber;
    }

    public String getPrlVersion()
    {
        return this.mPrlVersion;
    }

    public String getRUIMOperatorNumeric()
    {
        String str;
        if (this.mImsi == null)
            str = null;
        while (true)
        {
            return str;
            if ((this.mncLength != -1) && (this.mncLength != 0))
            {
                str = this.mImsi.substring(0, 3 + this.mncLength);
            }
            else
            {
                int i = Integer.parseInt(this.mImsi.substring(0, 3));
                str = this.mImsi.substring(0, 3 + MccTable.smallestDigitsMccForMnc(i));
            }
        }
    }

    public void handleMessage(Message paramMessage)
    {
        int i = 0;
        if (this.mDestroyed)
        {
            loge("Received message " + paramMessage + "[" + paramMessage.what + "] while being destroyed. Ignoring.");
            return;
        }
        while (true)
        {
            try
            {
                int j = paramMessage.what;
                switch (j)
                {
                default:
                    if (i == 0)
                        break;
                    onRecordLoaded();
                    break;
                case 2:
                    onRadioOffOrNotAvailable();
                    continue;
                case 4:
                case 3:
                case 10:
                case 5:
                case 14:
                case 18:
                case 19:
                case 21:
                case 22:
                case 17:
                case 31:
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                Log.w("CDMA", "Exception parsing RUIM record", localRuntimeException);
                if (i == 0)
                    break;
                continue;
                log("Event EVENT_GET_DEVICE_IDENTITY_DONE Received");
                continue;
            }
            finally
            {
                if (i != 0)
                    onRecordLoaded();
            }
            i = 1;
            AsyncResult localAsyncResult5 = (AsyncResult)paramMessage.obj;
            if (localAsyncResult5.exception != null)
            {
                loge("Exception querying IMSI, Exception:" + localAsyncResult5.exception);
            }
            else
            {
                this.mImsi = ((String)localAsyncResult5.result);
                if ((this.mImsi != null) && ((this.mImsi.length() < 6) || (this.mImsi.length() > 15)))
                {
                    loge("invalid IMSI " + this.mImsi);
                    this.mImsi = null;
                }
                log("IMSI: " + this.mImsi.substring(0, 6) + "xxxxxxxxx");
                String str = getRUIMOperatorNumeric();
                if ((str != null) && (str.length() <= 6))
                {
                    MccTable.updateMccMncConfiguration(this.mContext, str);
                    continue;
                    AsyncResult localAsyncResult4 = (AsyncResult)paramMessage.obj;
                    String[] arrayOfString = (String[])localAsyncResult4.result;
                    if (localAsyncResult4.exception == null)
                    {
                        this.mMyMobileNumber = arrayOfString[0];
                        this.mMin2Min1 = arrayOfString[3];
                        this.mPrlVersion = arrayOfString[4];
                        log("MDN: " + this.mMyMobileNumber + " MIN: " + this.mMin2Min1);
                        continue;
                        i = 1;
                        AsyncResult localAsyncResult3 = (AsyncResult)paramMessage.obj;
                        byte[] arrayOfByte = (byte[])localAsyncResult3.result;
                        if (localAsyncResult3.exception == null)
                        {
                            this.iccid = IccUtils.bcdToString(arrayOfByte, 0, arrayOfByte.length);
                            log("iccid: " + this.iccid);
                            continue;
                            AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
                            if (localAsyncResult2.exception != null)
                            {
                                Log.i("CDMA", "RuimRecords update failed", localAsyncResult2.exception);
                                continue;
                                Log.w("CDMA", "Event not supported: " + paramMessage.what);
                                continue;
                                log("Event EVENT_GET_SST_DONE Received");
                                continue;
                                i = 0;
                                AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
                                if (localAsyncResult1.exception == null)
                                    handleRuimRefresh((IccRefreshResponse)localAsyncResult1.result);
                            }
                        }
                    }
                }
            }
        }
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[RuimRecords] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("CDMA", "[RuimRecords] " + paramString);
    }

    protected void onAllRecordsLoaded()
    {
        String str = getRUIMOperatorNumeric();
        log("RuimRecords: onAllRecordsLoaded set 'gsm.sim.operator.numeric' to operator='" + str + "'");
        SystemProperties.set("gsm.sim.operator.numeric", str);
        if (this.mImsi != null)
            SystemProperties.set("gsm.sim.operator.iso-country", MccTable.countryCodeForMcc(Integer.parseInt(this.mImsi.substring(0, 3))));
        this.recordsLoadedRegistrants.notifyRegistrants(new AsyncResult(null, null, null));
        this.mParentCard.broadcastIccStateChangedIntent("LOADED", null);
    }

    protected void onRadioOffOrNotAvailable()
    {
        this.countVoiceMessages = 0;
        this.mncLength = -1;
        this.iccid = null;
        this.adnCache.reset();
        this.recordsRequested = false;
    }

    public void onReady()
    {
        this.mParentCard.broadcastIccStateChangedIntent("READY", null);
        fetchRuimRecords();
        this.mCi.getCDMASubscription(obtainMessage(10));
    }

    protected void onRecordLoaded()
    {
        this.recordsToLoad = (-1 + this.recordsToLoad);
        log("RuimRecords:onRecordLoaded " + this.recordsToLoad + " requested: " + this.recordsRequested);
        if ((this.recordsToLoad == 0) && (this.recordsRequested == true))
            onAllRecordsLoaded();
        while (true)
        {
            return;
            if (this.recordsToLoad < 0)
            {
                loge("RuimRecords: recordsToLoad <0, programmer error suspected");
                this.recordsToLoad = 0;
            }
        }
    }

    public void onRefresh(boolean paramBoolean, int[] paramArrayOfInt)
    {
        if (paramBoolean)
            fetchRuimRecords();
    }

    public void setVoiceMailNumber(String paramString1, String paramString2, Message paramMessage)
    {
        AsyncResult.forMessage(paramMessage).exception = new IccException("setVoiceMailNumber not implemented");
        paramMessage.sendToTarget();
        loge("method setVoiceMailNumber is not implemented");
    }

    public void setVoiceMessageWaiting(int paramInt1, int paramInt2)
    {
        if (paramInt1 != 1)
            return;
        if (paramInt2 < 0)
            paramInt2 = -1;
        while (true)
        {
            this.countVoiceMessages = paramInt2;
            this.mRecordsEventsRegistrants.notifyResult(Integer.valueOf(0));
            break;
            if (paramInt2 > 255)
                paramInt2 = 255;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.RuimRecords
 * JD-Core Version:        0.6.2
 */