package com.android.internal.telephony.cdma;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncResult;
import android.os.SystemProperties;
import android.util.Log;
import com.android.internal.telephony.AdnRecordLoader;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCardApplication.AppType;
import com.android.internal.telephony.IccFileHandler;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.IccRecords.IccRecordLoaded;
import com.android.internal.telephony.IccUtils;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.gsm.SIMRecords;
import com.android.internal.telephony.ims.IsimRecords;
import com.android.internal.telephony.ims.IsimUiccRecords;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public final class CdmaLteUiccRecords extends SIMRecords
{
    boolean mCsimSpnDisplayCondition = false;
    private byte[] mEFli = null;
    private byte[] mEFpl = null;
    private String mHomeNetworkId;
    private String mHomeSystemId;
    private final IsimUiccRecords mIsimUiccRecords = new IsimUiccRecords();
    private String mMdn;
    private String mMin;
    private String mPrlVersion;

    public CdmaLteUiccRecords(IccCard paramIccCard, Context paramContext, CommandsInterface paramCommandsInterface)
    {
        super(paramIccCard, paramContext, paramCommandsInterface);
    }

    private int adjstMinDigits(int paramInt)
    {
        int i = paramInt + 111;
        if (i % 10 == 0)
            i -= 10;
        if (i / 10 % 10 == 0)
            i -= 100;
        if (i / 100 % 10 == 0)
            i -= 1000;
        return i;
    }

    private String findBestLanguage(byte[] paramArrayOfByte)
    {
        String[] arrayOfString = this.mContext.getAssets().getLocales();
        String str;
        if ((paramArrayOfByte == null) || (arrayOfString == null))
        {
            str = null;
            return str;
        }
        int i = 0;
        while (true)
            while (true)
            {
                if (i + 1 < paramArrayOfByte.length);
                try
                {
                    str = new String(paramArrayOfByte, i, 2, "ISO-8859-1");
                    for (int j = 0; ; j++)
                    {
                        if (j >= arrayOfString.length)
                            break label103;
                        if ((arrayOfString[j] != null) && (arrayOfString[j].length() >= 2))
                        {
                            boolean bool = arrayOfString[j].substring(0, 2).equals(str);
                            if (bool)
                                break;
                        }
                    }
                    label103: if (0 != 0)
                        str = null;
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException)
                {
                    log("Failed to parse SIM language records");
                    i += 2;
                }
            }
    }

    private void onGetCSimEprlDone(AsyncResult paramAsyncResult)
    {
        byte[] arrayOfByte = (byte[])paramAsyncResult.result;
        log("CSIM_EPRL=" + IccUtils.bytesToHexString(arrayOfByte));
        if (arrayOfByte.length > 3)
            this.mPrlVersion = Integer.toString((0xFF & arrayOfByte[2]) << 8 | 0xFF & arrayOfByte[3]);
        log("CSIM PRL version=" + this.mPrlVersion);
    }

    private void setLocaleFromCsim()
    {
        String str1 = findBestLanguage(this.mEFli);
        if (str1 == null)
            str1 = findBestLanguage(this.mEFpl);
        if (str1 != null)
        {
            String str2 = getIMSI();
            String str3 = null;
            if (str2 != null)
                str3 = MccTable.countryCodeForMcc(Integer.parseInt(str2.substring(0, 3)));
            log("Setting locale to " + str1 + "_" + str3);
            MccTable.setSystemLocale(this.mContext, str1, str3);
        }
        while (true)
        {
            return;
            log("No suitable CSIM selected locale");
        }
    }

    protected void fetchSimRecords()
    {
        this.recordsRequested = true;
        this.mCi.getIMSIForApp(this.mParentCard.getAid(), obtainMessage(3));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(12258, obtainMessage(4));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28589, obtainMessage(9));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(12037, obtainMessage(100, new EfPlLoaded(null)));
        this.recordsToLoad = (1 + this.recordsToLoad);
        new AdnRecordLoader(this.mFh).loadFromEF(28480, 28490, 1, obtainMessage(10));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28472, obtainMessage(17));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28474, obtainMessage(100, new EfCsimLiLoaded(null)));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28481, obtainMessage(100, new EfCsimSpnLoaded(null)));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFLinearFixed(28484, 1, obtainMessage(100, new EfCsimMdnLoaded(null)));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28450, obtainMessage(100, new EfCsimImsimLoaded(null)));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFLinearFixedAll(28456, obtainMessage(100, new EfCsimCdmaHomeLoaded(null)));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.mFh.loadEFTransparent(28506, obtainMessage(100, new EfCsimEprlLoaded(null)));
        this.recordsToLoad = (1 + this.recordsToLoad);
        this.recordsToLoad += this.mIsimUiccRecords.fetchIsimRecords(this.mFh, this);
    }

    public boolean getCsimSpnDisplayCondition()
    {
        return this.mCsimSpnDisplayCondition;
    }

    public IsimRecords getIsimRecords()
    {
        return this.mIsimUiccRecords;
    }

    public String getMdn()
    {
        return this.mMdn;
    }

    public String getMin()
    {
        return this.mMin;
    }

    public String getNid()
    {
        return this.mHomeNetworkId;
    }

    public String getPrlVersion()
    {
        return this.mPrlVersion;
    }

    public String getSid()
    {
        return this.mHomeSystemId;
    }

    public boolean isProvisioned()
    {
        boolean bool = true;
        if (SystemProperties.getBoolean("persist.radio.test-csim", false));
        while (true)
        {
            return bool;
            if (this.mParentCard == null)
                bool = false;
            else if ((this.mParentCard.isApplicationOnIcc(IccCardApplication.AppType.APPTYPE_CSIM)) && ((this.mMdn == null) || (this.mMin == null)))
                bool = false;
        }
    }

    protected void log(String paramString)
    {
        Log.d("GSM", "[CSIM] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("GSM", "[CSIM] " + paramString);
    }

    protected void onAllRecordsLoaded()
    {
        setLocaleFromCsim();
        super.onAllRecordsLoaded();
    }

    protected void onRecordLoaded()
    {
        this.recordsToLoad = (-1 + this.recordsToLoad);
        if ((this.recordsToLoad == 0) && (this.recordsRequested == true))
            onAllRecordsLoaded();
        while (true)
        {
            return;
            if (this.recordsToLoad < 0)
            {
                Log.e("GSM", "SIMRecords: recordsToLoad <0, programmer error suspected");
                this.recordsToLoad = 0;
            }
        }
    }

    private class EfCsimEprlLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfCsimEprlLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_CSIM_EPRL";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            CdmaLteUiccRecords.this.onGetCSimEprlDone(paramAsyncResult);
        }
    }

    private class EfCsimCdmaHomeLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfCsimCdmaHomeLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_CSIM_CDMAHOME";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            ArrayList localArrayList = (ArrayList)paramAsyncResult.result;
            CdmaLteUiccRecords.this.log("CSIM_CDMAHOME data size=" + localArrayList.size());
            if (localArrayList.isEmpty());
            while (true)
            {
                return;
                StringBuilder localStringBuilder1 = new StringBuilder();
                StringBuilder localStringBuilder2 = new StringBuilder();
                Iterator localIterator = localArrayList.iterator();
                while (localIterator.hasNext())
                {
                    byte[] arrayOfByte = (byte[])localIterator.next();
                    if (arrayOfByte.length == 5)
                    {
                        int i = (0xFF & arrayOfByte[1]) << 8 | 0xFF & arrayOfByte[0];
                        int j = (0xFF & arrayOfByte[3]) << 8 | 0xFF & arrayOfByte[2];
                        localStringBuilder1.append(i).append(',');
                        localStringBuilder2.append(j).append(',');
                    }
                }
                localStringBuilder1.setLength(-1 + localStringBuilder1.length());
                localStringBuilder2.setLength(-1 + localStringBuilder2.length());
                CdmaLteUiccRecords.access$1102(CdmaLteUiccRecords.this, localStringBuilder1.toString());
                CdmaLteUiccRecords.access$1202(CdmaLteUiccRecords.this, localStringBuilder2.toString());
            }
        }
    }

    private class EfCsimImsimLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfCsimImsimLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_CSIM_IMSIM";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            byte[] arrayOfByte = (byte[])paramAsyncResult.result;
            CdmaLteUiccRecords.this.log("CSIM_IMSIM=" + IccUtils.bytesToHexString(arrayOfByte));
            int i;
            if ((0x80 & arrayOfByte[7]) == 128)
            {
                i = 1;
                if (i == 0)
                    break label372;
                int j = ((0x3 & arrayOfByte[2]) << 8) + (0xFF & arrayOfByte[1]);
                int k = ((0xFF & arrayOfByte[5]) << 8 | 0xFF & arrayOfByte[4]) >> 6;
                int m = 0xF & arrayOfByte[4] >> 2;
                if (m > 9)
                    m = 0;
                int n = (0x3 & arrayOfByte[4]) << 8 | 0xFF & arrayOfByte[3];
                int i1 = CdmaLteUiccRecords.this.adjstMinDigits(j);
                int i2 = CdmaLteUiccRecords.this.adjstMinDigits(k);
                int i3 = CdmaLteUiccRecords.this.adjstMinDigits(n);
                StringBuilder localStringBuilder = new StringBuilder();
                Locale localLocale1 = Locale.US;
                Object[] arrayOfObject1 = new Object[1];
                arrayOfObject1[0] = Integer.valueOf(i1);
                localStringBuilder.append(String.format(localLocale1, "%03d", arrayOfObject1));
                Locale localLocale2 = Locale.US;
                Object[] arrayOfObject2 = new Object[1];
                arrayOfObject2[0] = Integer.valueOf(i2);
                localStringBuilder.append(String.format(localLocale2, "%03d", arrayOfObject2));
                Locale localLocale3 = Locale.US;
                Object[] arrayOfObject3 = new Object[1];
                arrayOfObject3[0] = Integer.valueOf(m);
                localStringBuilder.append(String.format(localLocale3, "%d", arrayOfObject3));
                Locale localLocale4 = Locale.US;
                Object[] arrayOfObject4 = new Object[1];
                arrayOfObject4[0] = Integer.valueOf(i3);
                localStringBuilder.append(String.format(localLocale4, "%03d", arrayOfObject4));
                CdmaLteUiccRecords.access$1002(CdmaLteUiccRecords.this, localStringBuilder.toString());
                CdmaLteUiccRecords.this.log("min present=" + CdmaLteUiccRecords.this.mMin);
            }
            while (true)
            {
                return;
                i = 0;
                break;
                label372: CdmaLteUiccRecords.this.log("min not present");
            }
        }
    }

    private class EfCsimMdnLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfCsimMdnLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_CSIM_MDN";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            byte[] arrayOfByte = (byte[])paramAsyncResult.result;
            CdmaLteUiccRecords.this.log("CSIM_MDN=" + IccUtils.bytesToHexString(arrayOfByte));
            int i = 0xF & arrayOfByte[0];
            CdmaLteUiccRecords.access$802(CdmaLteUiccRecords.this, IccUtils.cdmaBcdToString(arrayOfByte, 1, i));
            CdmaLteUiccRecords.this.log("CSIM MDN=" + CdmaLteUiccRecords.this.mMdn);
        }
    }

    private class EfCsimSpnLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfCsimSpnLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_CSIM_SPN";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            byte[] arrayOfByte1 = (byte[])paramAsyncResult.result;
            CdmaLteUiccRecords.this.log("CSIM_SPN=" + IccUtils.bytesToHexString(arrayOfByte1));
            CdmaLteUiccRecords localCdmaLteUiccRecords = CdmaLteUiccRecords.this;
            boolean bool;
            int i;
            byte[] arrayOfByte2;
            int j;
            label88: int k;
            if ((0x1 & arrayOfByte1[0]) != 0)
            {
                bool = true;
                localCdmaLteUiccRecords.mCsimSpnDisplayCondition = bool;
                i = arrayOfByte1[1];
                arrayOfByte1[2];
                arrayOfByte2 = new byte[32];
                if (arrayOfByte1.length >= 32)
                    break label146;
                j = arrayOfByte1.length;
                System.arraycopy(arrayOfByte1, 3, arrayOfByte2, 0, j);
                k = 0;
                label101: if ((k < arrayOfByte2.length) && ((0xFF & arrayOfByte2[k]) != 255))
                    break label153;
                if (k != 0)
                    break label159;
                CdmaLteUiccRecords.access$202(CdmaLteUiccRecords.this, "");
            }
            while (true)
            {
                return;
                bool = false;
                break;
                label146: j = 32;
                break label88;
                label153: k++;
                break label101;
                label159: switch (i)
                {
                case 1:
                case 5:
                case 6:
                case 7:
                default:
                case 0:
                case 8:
                case 2:
                case 3:
                case 9:
                case 4:
                }
                try
                {
                    CdmaLteUiccRecords.this.log("SPN encoding not supported");
                    while (true)
                    {
                        CdmaLteUiccRecords.this.log("spn=" + CdmaLteUiccRecords.access$600(CdmaLteUiccRecords.this));
                        CdmaLteUiccRecords.this.log("spnCondition=" + CdmaLteUiccRecords.this.mCsimSpnDisplayCondition);
                        SystemProperties.set("gsm.sim.operator.alpha", CdmaLteUiccRecords.access$700(CdmaLteUiccRecords.this));
                        break;
                        CdmaLteUiccRecords.access$302(CdmaLteUiccRecords.this, new String(arrayOfByte2, 0, k, "ISO-8859-1"));
                    }
                }
                catch (Exception localException)
                {
                    while (true)
                    {
                        CdmaLteUiccRecords.this.log("spn decode error: " + localException);
                        continue;
                        CdmaLteUiccRecords.access$402(CdmaLteUiccRecords.this, GsmAlphabet.gsm7BitPackedToString(arrayOfByte2, 0, k * 8 / 7));
                        continue;
                        CdmaLteUiccRecords.access$502(CdmaLteUiccRecords.this, new String(arrayOfByte2, 0, k, "utf-16"));
                    }
                }
            }
        }
    }

    private class EfCsimLiLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfCsimLiLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_CSIM_LI";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            CdmaLteUiccRecords.access$102(CdmaLteUiccRecords.this, (byte[])paramAsyncResult.result);
            int i = 0;
            if (i < CdmaLteUiccRecords.this.mEFli.length)
            {
                switch (CdmaLteUiccRecords.this.mEFli[(i + 1)])
                {
                default:
                    CdmaLteUiccRecords.this.mEFli[i] = 32;
                    CdmaLteUiccRecords.this.mEFli[(i + 1)] = 32;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                }
                while (true)
                {
                    i += 2;
                    break;
                    CdmaLteUiccRecords.this.mEFli[i] = 101;
                    CdmaLteUiccRecords.this.mEFli[(i + 1)] = 110;
                    continue;
                    CdmaLteUiccRecords.this.mEFli[i] = 102;
                    CdmaLteUiccRecords.this.mEFli[(i + 1)] = 114;
                    continue;
                    CdmaLteUiccRecords.this.mEFli[i] = 101;
                    CdmaLteUiccRecords.this.mEFli[(i + 1)] = 115;
                    continue;
                    CdmaLteUiccRecords.this.mEFli[i] = 106;
                    CdmaLteUiccRecords.this.mEFli[(i + 1)] = 97;
                    continue;
                    CdmaLteUiccRecords.this.mEFli[i] = 107;
                    CdmaLteUiccRecords.this.mEFli[(i + 1)] = 111;
                    continue;
                    CdmaLteUiccRecords.this.mEFli[i] = 122;
                    CdmaLteUiccRecords.this.mEFli[(i + 1)] = 104;
                    continue;
                    CdmaLteUiccRecords.this.mEFli[i] = 104;
                    CdmaLteUiccRecords.this.mEFli[(i + 1)] = 101;
                }
            }
            CdmaLteUiccRecords.this.log("EF_LI=" + IccUtils.bytesToHexString(CdmaLteUiccRecords.this.mEFli));
        }
    }

    private class EfPlLoaded
        implements IccRecords.IccRecordLoaded
    {
        private EfPlLoaded()
        {
        }

        public String getEfName()
        {
            return "EF_PL";
        }

        public void onRecordLoaded(AsyncResult paramAsyncResult)
        {
            CdmaLteUiccRecords.access$002(CdmaLteUiccRecords.this, (byte[])paramAsyncResult.result);
            CdmaLteUiccRecords.this.log("EF_PL=" + IccUtils.bytesToHexString(CdmaLteUiccRecords.this.mEFpl));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaLteUiccRecords
 * JD-Core Version:        0.6.2
 */