package com.android.internal.telephony.cdma;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.LinkProperties;
import android.net.TrafficStats;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Log;
import com.android.internal.telephony.ApnSetting;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.DataCallState;
import com.android.internal.telephony.DataConnection;
import com.android.internal.telephony.DataConnection.CallSetupException;
import com.android.internal.telephony.DataConnection.FailCause;
import com.android.internal.telephony.DataConnectionAc;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.DataConnectionTracker.Activity;
import com.android.internal.telephony.DataConnectionTracker.State;
import com.android.internal.telephony.IccRecords;
import com.android.internal.telephony.Phone.State;
import com.android.internal.telephony.PhoneBase;
import com.android.internal.telephony.RetryManager;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import miui.net.FirewallManager;

public final class CdmaDataConnectionTracker extends DataConnectionTracker
{
    private static final int DATA_CONNECTION_ACTIVE_PH_LINK_DOWN = 1;
    private static final int DATA_CONNECTION_ACTIVE_PH_LINK_INACTIVE = 0;
    private static final int DATA_CONNECTION_ACTIVE_PH_LINK_UP = 2;
    private static final int DATA_CONNECTION_POOL_SIZE = 1;
    private static final String INTENT_DATA_STALL_ALARM = "com.android.internal.telephony.cdma-data-stall";
    private static final String INTENT_RECONNECT_ALARM = "com.android.internal.telephony.cdma-reconnect";
    private static final int TIME_DELAYED_TO_RESTART_RADIO = SystemProperties.getInt("ro.cdma.timetoradiorestart", 60000);
    private static final int mDefaultApnId;
    private static final String[] mDefaultApnTypes = arrayOfString2;
    private static final String[] mSupportedApnTypes;
    protected final String LOG_TAG = "CDMA";
    private CDMAPhone mCdmaPhone;
    private CdmaSubscriptionSourceManager mCdmaSSM;
    private String[] mDunApnTypes;
    private CdmaDataConnection mPendingDataConnection;
    private boolean mPendingRestartRadio = false;
    private Runnable mPollNetStat;

    static
    {
        String[] arrayOfString1 = new String[4];
        arrayOfString1[0] = "default";
        arrayOfString1[1] = "mms";
        arrayOfString1[2] = "dun";
        arrayOfString1[3] = "hipri";
        mSupportedApnTypes = arrayOfString1;
        String[] arrayOfString2 = new String[3];
        arrayOfString2[0] = "default";
        arrayOfString2[1] = "mms";
        arrayOfString2[2] = "hipri";
    }

    CdmaDataConnectionTracker(CDMAPhone paramCDMAPhone)
    {
        super(paramCDMAPhone);
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "dun";
        this.mDunApnTypes = arrayOfString1;
        this.mPollNetStat = new Runnable()
        {
            public void run()
            {
                long l1 = CdmaDataConnectionTracker.access$000(CdmaDataConnectionTracker.this);
                long l2 = CdmaDataConnectionTracker.access$100(CdmaDataConnectionTracker.this);
                CdmaDataConnectionTracker.access$202(CdmaDataConnectionTracker.this, TrafficStats.getMobileTxPackets());
                CdmaDataConnectionTracker.access$302(CdmaDataConnectionTracker.this, TrafficStats.getMobileRxPackets());
                long l3;
                long l4;
                DataConnectionTracker.Activity localActivity;
                if ((CdmaDataConnectionTracker.access$400(CdmaDataConnectionTracker.this)) && ((l1 > 0L) || (l2 > 0L)))
                {
                    l3 = CdmaDataConnectionTracker.access$500(CdmaDataConnectionTracker.this) - l1;
                    l4 = CdmaDataConnectionTracker.access$600(CdmaDataConnectionTracker.this) - l2;
                    if ((l3 <= 0L) || (l4 <= 0L))
                        break label251;
                    CdmaDataConnectionTracker.access$702(CdmaDataConnectionTracker.this, 0L);
                    localActivity = DataConnectionTracker.Activity.DATAINANDOUT;
                    if ((CdmaDataConnectionTracker.access$1700(CdmaDataConnectionTracker.this) != localActivity) && (CdmaDataConnectionTracker.access$1800(CdmaDataConnectionTracker.this)))
                    {
                        CdmaDataConnectionTracker.access$1902(CdmaDataConnectionTracker.this, localActivity);
                        CdmaDataConnectionTracker.access$2000(CdmaDataConnectionTracker.this).notifyDataActivity();
                    }
                }
                if (CdmaDataConnectionTracker.access$2100(CdmaDataConnectionTracker.this) >= 10L)
                {
                    if (CdmaDataConnectionTracker.access$2200(CdmaDataConnectionTracker.this) == 0)
                        EventLog.writeEvent(50101, CdmaDataConnectionTracker.access$2300(CdmaDataConnectionTracker.this));
                    if (CdmaDataConnectionTracker.access$2400(CdmaDataConnectionTracker.this) < 24)
                    {
                        CdmaDataConnectionTracker.access$2508(CdmaDataConnectionTracker.this);
                        CdmaDataConnectionTracker.access$2602(CdmaDataConnectionTracker.this, 5000);
                    }
                }
                while (true)
                {
                    if (CdmaDataConnectionTracker.access$3100(CdmaDataConnectionTracker.this))
                        CdmaDataConnectionTracker.access$3300(CdmaDataConnectionTracker.this).postDelayed(this, CdmaDataConnectionTracker.access$3200(CdmaDataConnectionTracker.this));
                    return;
                    label251: if ((l3 > 0L) && (l4 == 0L))
                    {
                        if (CdmaDataConnectionTracker.access$800(CdmaDataConnectionTracker.this).getState() == Phone.State.IDLE)
                            CdmaDataConnectionTracker.access$914(CdmaDataConnectionTracker.this, l3);
                        while (true)
                        {
                            localActivity = DataConnectionTracker.Activity.DATAOUT;
                            break;
                            CdmaDataConnectionTracker.access$1002(CdmaDataConnectionTracker.this, 0L);
                        }
                    }
                    if ((l3 == 0L) && (l4 > 0L))
                    {
                        CdmaDataConnectionTracker.access$1102(CdmaDataConnectionTracker.this, 0L);
                        localActivity = DataConnectionTracker.Activity.DATAIN;
                        break;
                    }
                    if ((l3 == 0L) && (l4 == 0L))
                    {
                        if (CdmaDataConnectionTracker.access$1200(CdmaDataConnectionTracker.this) == DataConnectionTracker.Activity.DORMANT);
                        for (localActivity = CdmaDataConnectionTracker.access$1300(CdmaDataConnectionTracker.this); ; localActivity = DataConnectionTracker.Activity.NONE)
                            break;
                    }
                    CdmaDataConnectionTracker.access$1402(CdmaDataConnectionTracker.this, 0L);
                    if (CdmaDataConnectionTracker.access$1500(CdmaDataConnectionTracker.this) == DataConnectionTracker.Activity.DORMANT);
                    for (localActivity = CdmaDataConnectionTracker.access$1600(CdmaDataConnectionTracker.this); ; localActivity = DataConnectionTracker.Activity.NONE)
                        break;
                    CdmaDataConnectionTracker.this.log("Sent " + String.valueOf(CdmaDataConnectionTracker.access$2700(CdmaDataConnectionTracker.this)) + " pkts since last received");
                    CdmaDataConnectionTracker.access$2802(CdmaDataConnectionTracker.this, false);
                    CdmaDataConnectionTracker.this.stopNetStatPoll();
                    CdmaDataConnectionTracker.this.restartRadio();
                    EventLog.writeEvent(50102, 24);
                    continue;
                    CdmaDataConnectionTracker.access$2902(CdmaDataConnectionTracker.this, 0);
                    CdmaDataConnectionTracker.access$3002(CdmaDataConnectionTracker.this, 1000);
                }
            }
        };
        this.mCdmaPhone = paramCDMAPhone;
        paramCDMAPhone.mCM.registerForAvailable(this, 270337, null);
        paramCDMAPhone.mCM.registerForOffOrNotAvailable(this, 270342, null);
        paramCDMAPhone.mIccRecords.registerForRecordsLoaded(this, 270338, null);
        paramCDMAPhone.mCM.registerForDataNetworkStateChanged(this, 270340, null);
        paramCDMAPhone.mCT.registerForVoiceCallEnded(this, 270344, null);
        paramCDMAPhone.mCT.registerForVoiceCallStarted(this, 270343, null);
        paramCDMAPhone.mSST.registerForDataConnectionAttached(this, 270339, null);
        paramCDMAPhone.mSST.registerForDataConnectionDetached(this, 270356, null);
        paramCDMAPhone.mSST.registerForRoamingOn(this, 270347, null);
        paramCDMAPhone.mSST.registerForRoamingOff(this, 270348, null);
        paramCDMAPhone.mCM.registerForCdmaOtaProvision(this, 270361, null);
        this.mCdmaSSM = CdmaSubscriptionSourceManager.getInstance(paramCDMAPhone.getContext(), paramCDMAPhone.mCM, this, 270357, null);
        this.mDataConnectionTracker = this;
        createAllDataConnectionList();
        broadcastMessenger();
        String[] arrayOfString2 = this.mCdmaPhone.getContext().getResources().getStringArray(17235998);
        if ((arrayOfString2 != null) && (arrayOfString2.length > 0))
        {
            ArrayList localArrayList = new ArrayList();
            for (int i = 0; i < arrayOfString2.length; i++)
                if (!"dun".equalsIgnoreCase(arrayOfString2[i]))
                    localArrayList.add(arrayOfString2[i]);
            localArrayList.add(0, "dun");
            this.mDunApnTypes = ((String[])localArrayList.toArray(arrayOfString2));
        }
    }

    private void cleanUpConnection(boolean paramBoolean1, String paramString, boolean paramBoolean2)
    {
        log("cleanUpConnection: reason: " + paramString);
        if (this.mReconnectIntent != null)
        {
            ((AlarmManager)this.mPhone.getContext().getSystemService("alarm")).cancel(this.mReconnectIntent);
            this.mReconnectIntent = null;
        }
        setState(DataConnectionTracker.State.DISCONNECTING);
        notifyOffApnsOfAvailability(paramString);
        int i = 0;
        Iterator localIterator = this.mDataConnections.values().iterator();
        while (localIterator.hasNext())
        {
            DataConnection localDataConnection = (DataConnection)localIterator.next();
            if (localDataConnection != null)
            {
                DataConnectionAc localDataConnectionAc = (DataConnectionAc)this.mDataConnectionAsyncChannels.get(Integer.valueOf(localDataConnection.getDataConnectionId()));
                if (paramBoolean1)
                {
                    if (paramBoolean2)
                    {
                        log("cleanUpConnection: teardown, conn.tearDownAll");
                        localDataConnection.tearDownAll(paramString, obtainMessage(270351, localDataConnection.getDataConnectionId(), 0, paramString));
                    }
                    while (true)
                    {
                        i = 1;
                        break;
                        log("cleanUpConnection: teardown, conn.tearDown");
                        localDataConnection.tearDown(paramString, obtainMessage(270351, localDataConnection.getDataConnectionId(), 0, paramString));
                    }
                }
                log("cleanUpConnection: !tearDown, call conn.resetSynchronously");
                if (localDataConnectionAc != null)
                    localDataConnectionAc.resetSync();
                i = 0;
            }
        }
        stopNetStatPoll();
        if (i == 0)
        {
            log("cleanupConnection: !notificationDeferred");
            gotoIdleAndNotifyDataConnection(paramString);
        }
    }

    private void createAllDataConnectionList()
    {
        String str = SystemProperties.get("ro.cdma.data_retry_config");
        int i = 0;
        if (i < 1)
        {
            RetryManager localRetryManager = new RetryManager();
            if ((!localRetryManager.configure(str)) && (!localRetryManager.configure("default_randomization=2000,5000,10000,20000,40000,80000:5000,160000:5000,320000:5000,640000:5000,1280000:5000,1800000:5000")))
            {
                log("Could not configure using DEFAULT_DATA_RETRY_CONFIG=default_randomization=2000,5000,10000,20000,40000,80000:5000,160000:5000,320000:5000,640000:5000,1280000:5000,1800000:5000");
                localRetryManager.configure(20, 2000, 1000);
            }
            int j = this.mUniqueIdGenerator.getAndIncrement();
            CdmaDataConnection localCdmaDataConnection = CdmaDataConnection.makeDataConnection(this.mCdmaPhone, j, localRetryManager, this);
            this.mDataConnections.put(Integer.valueOf(j), localCdmaDataConnection);
            DataConnectionAc localDataConnectionAc = new DataConnectionAc(localCdmaDataConnection, "CDMA");
            int k = localDataConnectionAc.fullyConnectSync(this.mPhone.getContext(), this, localCdmaDataConnection.getHandler());
            if (k == 0)
            {
                log("Fully connected");
                this.mDataConnectionAsyncChannels.put(Integer.valueOf(localDataConnectionAc.dataConnection.getDataConnectionId()), localDataConnectionAc);
            }
            while (true)
            {
                i++;
                break;
                log("Could not connect to dcac.dataConnection=" + localDataConnectionAc.dataConnection + " status=" + k);
            }
        }
    }

    private void destroyAllDataConnectionList()
    {
        if (this.mDataConnections != null)
            this.mDataConnections.clear();
    }

    private CdmaDataConnection findFreeDataConnection()
    {
        Iterator localIterator = this.mDataConnectionAsyncChannels.values().iterator();
        DataConnectionAc localDataConnectionAc;
        while (localIterator.hasNext())
        {
            localDataConnectionAc = (DataConnectionAc)localIterator.next();
            if (localDataConnectionAc.isInactiveSync())
                log("found free GsmDataConnection");
        }
        for (CdmaDataConnection localCdmaDataConnection = (CdmaDataConnection)localDataConnectionAc.dataConnection; ; localCdmaDataConnection = null)
        {
            return localCdmaDataConnection;
            log("NO free CdmaDataConnection");
        }
    }

    private void notifyDefaultData(String paramString)
    {
        setState(DataConnectionTracker.State.CONNECTED);
        notifyDataConnection(paramString);
        startNetStatPoll();
        ((DataConnection)this.mDataConnections.get(Integer.valueOf(0))).resetRetryCount();
    }

    private void notifyNoData(DataConnection.FailCause paramFailCause)
    {
        setState(DataConnectionTracker.State.FAILED);
        notifyOffApnsOfAvailability(null);
    }

    private void onCdmaDataDetached()
    {
        if (this.mState == DataConnectionTracker.State.CONNECTED)
        {
            startNetStatPoll();
            notifyDataConnection("cdmaDataDetached");
            return;
        }
        CdmaCellLocation localCdmaCellLocation;
        Object[] arrayOfObject;
        if (this.mState == DataConnectionTracker.State.FAILED)
        {
            cleanUpConnection(false, "cdmaDataDetached", false);
            ((DataConnection)this.mDataConnections.get(Integer.valueOf(0))).resetRetryCount();
            localCdmaCellLocation = (CdmaCellLocation)this.mPhone.getCellLocation();
            arrayOfObject = new Object[2];
            if (localCdmaCellLocation == null)
                break label126;
        }
        label126: for (int i = localCdmaCellLocation.getBaseStationId(); ; i = -1)
        {
            arrayOfObject[0] = Integer.valueOf(i);
            arrayOfObject[1] = Integer.valueOf(TelephonyManager.getDefault().getNetworkType());
            EventLog.writeEvent(50110, arrayOfObject);
            trySetupData("cdmaDataDetached");
            break;
        }
    }

    private void onCdmaOtaProvision(AsyncResult paramAsyncResult)
    {
        if (paramAsyncResult.exception != null)
        {
            int[] arrayOfInt = (int[])paramAsyncResult.result;
            if ((arrayOfInt != null) && (arrayOfInt.length > 1))
                switch (arrayOfInt[0])
                {
                case 9:
                default:
                case 8:
                case 10:
                }
        }
        while (true)
        {
            return;
            ((DataConnection)this.mDataConnections.get(Integer.valueOf(0))).resetRetryCount();
        }
    }

    private void onRestartRadio()
    {
        if (this.mPendingRestartRadio)
        {
            log("************TURN OFF RADIO**************");
            this.mPhone.mCM.setRadioPower(false, null);
            this.mPendingRestartRadio = false;
        }
    }

    private void reconnectAfterFail(DataConnection.FailCause paramFailCause, String paramString, int paramInt)
    {
        if (this.mState == DataConnectionTracker.State.FAILED)
        {
            int i = paramInt;
            if (i < 0)
            {
                i = ((DataConnection)this.mDataConnections.get(Integer.valueOf(0))).getRetryTimer();
                ((DataConnection)this.mDataConnections.get(Integer.valueOf(0))).increaseRetryCount();
            }
            startAlarmForReconnect(i, paramString);
            if (shouldPostNotification(paramFailCause))
                break label77;
            log("NOT Posting Data Connection Unavailable notification -- likely transient error");
        }
        while (true)
        {
            return;
            label77: notifyNoData(paramFailCause);
        }
    }

    private void resetPollStats()
    {
        this.mTxPkts = -1L;
        this.mRxPkts = -1L;
        this.mSentSinceLastRecv = 0L;
        this.mNetStatPollPeriod = 1000;
        this.mNoRecvPollCount = 0;
    }

    private boolean retryAfterDisconnected(String paramString)
    {
        boolean bool = true;
        if ("radioTurnedOff".equals(paramString))
            bool = false;
        return bool;
    }

    private boolean setupData(String paramString)
    {
        CdmaDataConnection localCdmaDataConnection = findFreeDataConnection();
        boolean bool;
        if (localCdmaDataConnection == null)
        {
            log("setupData: No free CdmaDataConnection found!");
            bool = false;
            return bool;
        }
        this.mPendingDataConnection = localCdmaDataConnection;
        String[] arrayOfString;
        if (this.mRequestedApnType.equals("dun"))
            arrayOfString = this.mDunApnTypes;
        for (int i = 3; ; i = 0)
        {
            this.mActiveApn = new ApnSetting(i, "", "", "", "", "", "", "", "", "", "", 0, arrayOfString, "IP", "IP", true, 0);
            log("call conn.bringUp mActiveApn=" + this.mActiveApn);
            Message localMessage = obtainMessage();
            localMessage.what = 270336;
            localMessage.obj = paramString;
            localCdmaDataConnection.bringUp(localMessage, this.mActiveApn);
            setState(DataConnectionTracker.State.INITING);
            notifyDataConnection(paramString);
            bool = true;
            break;
            arrayOfString = mDefaultApnTypes;
        }
    }

    private boolean shouldPostNotification(DataConnection.FailCause paramFailCause)
    {
        if (paramFailCause != DataConnection.FailCause.UNKNOWN);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void startAlarmForReconnect(int paramInt, String paramString)
    {
        log("Data Connection activate failed. Scheduling next attempt for " + paramInt / 1000 + "s");
        AlarmManager localAlarmManager = (AlarmManager)this.mPhone.getContext().getSystemService("alarm");
        Intent localIntent = new Intent("com.android.internal.telephony.cdma-reconnect");
        localIntent.putExtra("reconnect_alarm_extra_reason", paramString);
        this.mReconnectIntent = PendingIntent.getBroadcast(this.mPhone.getContext(), 0, localIntent, 0);
        localAlarmManager.set(2, SystemClock.elapsedRealtime() + paramInt, this.mReconnectIntent);
    }

    private void startDelayedRetry(DataConnection.FailCause paramFailCause, String paramString, int paramInt)
    {
        notifyNoData(paramFailCause);
        reconnectAfterFail(paramFailCause, paramString, paramInt);
    }

    private boolean trySetupData(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder().append("***trySetupData due to ");
        String str;
        boolean bool;
        if (paramString == null)
        {
            str = "(unspecified)";
            log(str);
            if (this.mPhone.getSimulatedRadioControl() == null)
                break label79;
            setState(DataConnectionTracker.State.CONNECTED);
            notifyDataConnection(paramString);
            notifyOffApnsOfAvailability(paramString);
            log("(fix?) We're on the simulator; assuming data is connected");
            bool = true;
        }
        while (true)
        {
            return bool;
            str = paramString;
            break;
            label79: this.mCdmaPhone.mSST.getCurrentDataConnectionState();
            this.mPhone.getServiceState().getRoaming();
            this.mCdmaPhone.mSST.getDesiredPowerState();
            if (((this.mState == DataConnectionTracker.State.IDLE) || (this.mState == DataConnectionTracker.State.SCANNING)) && (isDataAllowed()) && (getAnyDataEnabled()) && (!isEmergency()))
            {
                bool = setupData(paramString);
                notifyOffApnsOfAvailability(paramString);
            }
            else
            {
                notifyOffApnsOfAvailability(paramString);
                bool = false;
            }
        }
    }

    private void writeEventLogCdmaDataDrop()
    {
        CdmaCellLocation localCdmaCellLocation = (CdmaCellLocation)this.mPhone.getCellLocation();
        Object[] arrayOfObject = new Object[2];
        if (localCdmaCellLocation != null);
        for (int i = localCdmaCellLocation.getBaseStationId(); ; i = -1)
        {
            arrayOfObject[0] = Integer.valueOf(i);
            arrayOfObject[1] = Integer.valueOf(TelephonyManager.getDefault().getNetworkType());
            EventLog.writeEvent(50111, arrayOfObject);
            return;
        }
    }

    public void dispose()
    {
        cleanUpConnection(false, null, false);
        super.dispose();
        this.mPhone.mCM.unregisterForAvailable(this);
        this.mPhone.mCM.unregisterForOffOrNotAvailable(this);
        this.mCdmaPhone.mIccRecords.unregisterForRecordsLoaded(this);
        this.mPhone.mCM.unregisterForDataNetworkStateChanged(this);
        this.mCdmaPhone.mCT.unregisterForVoiceCallEnded(this);
        this.mCdmaPhone.mCT.unregisterForVoiceCallStarted(this);
        this.mCdmaPhone.mSST.unregisterForDataConnectionAttached(this);
        this.mCdmaPhone.mSST.unregisterForDataConnectionDetached(this);
        this.mCdmaPhone.mSST.unregisterForRoamingOn(this);
        this.mCdmaPhone.mSST.unregisterForRoamingOff(this);
        this.mCdmaSSM.dispose(this);
        this.mPhone.mCM.unregisterForCdmaOtaProvision(this);
        destroyAllDataConnectionList();
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.println("CdmaDataConnectionTracker extends:");
        super.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println(" mCdmaPhone=" + this.mCdmaPhone);
        paramPrintWriter.println(" mCdmaSSM=" + this.mCdmaSSM);
        paramPrintWriter.println(" mPendingDataConnection=" + this.mPendingDataConnection);
        paramPrintWriter.println(" mPendingRestartRadio=" + this.mPendingRestartRadio);
        paramPrintWriter.println(" mSupportedApnTypes=" + mSupportedApnTypes);
        paramPrintWriter.println(" mDefaultApnTypes=" + mDefaultApnTypes);
        paramPrintWriter.println(" mDunApnTypes=" + this.mDunApnTypes);
        paramPrintWriter.println(" mDefaultApnId=0");
    }

    protected void finalize()
    {
        log("CdmaDataConnectionTracker finalized");
    }

    protected String getActionIntentDataStallAlarm()
    {
        return "com.android.internal.telephony.cdma-data-stall";
    }

    protected String getActionIntentReconnectAlarm()
    {
        return "com.android.internal.telephony.cdma-reconnect";
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    ApnSetting getActiveApn()
    {
        return this.mActiveApn;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    HashMap<Integer, DataConnectionAc> getDataConnectionAsyncChannels()
    {
        return this.mDataConnectionAsyncChannels;
    }

    /** @deprecated */
    public DataConnectionTracker.State getState(String paramString)
    {
        try
        {
            DataConnectionTracker.State localState = this.mState;
            return localState;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void gotoIdleAndNotifyDataConnection(String paramString)
    {
        log("gotoIdleAndNotifyDataConnection: reason=" + paramString);
        setState(DataConnectionTracker.State.IDLE);
        notifyDataConnection(paramString);
        this.mActiveApn = null;
    }

    public void handleMessage(Message paramMessage)
    {
        log("CdmaDCT handleMessage msg=" + paramMessage);
        if ((!this.mPhone.mIsTheCurrentActivePhone) || (this.mIsDisposed))
            log("Ignore CDMA msgs since CDMA phone is inactive");
        while (true)
        {
            return;
            switch (paramMessage.what)
            {
            default:
                super.handleMessage(paramMessage);
                break;
            case 270338:
                onRecordsLoaded();
                break;
            case 270357:
                if (this.mCdmaSSM.getCdmaSubscriptionSource() == 1)
                    onNVReady();
                break;
            case 270356:
                onCdmaDataDetached();
                break;
            case 270340:
                onDataStateChanged((AsyncResult)paramMessage.obj);
                break;
            case 270361:
                onCdmaOtaProvision((AsyncResult)paramMessage.obj);
                break;
            case 270362:
                log("EVENT_RESTART_RADIO");
                onRestartRadio();
            }
        }
    }

    protected boolean isApnTypeAvailable(String paramString)
    {
        String[] arrayOfString = mSupportedApnTypes;
        int i = arrayOfString.length;
        int j = 0;
        if (j < i)
            if (!TextUtils.equals(paramString, arrayOfString[j]));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    protected boolean isDataAllowed()
    {
        while (true)
        {
            synchronized (this.mDataEnabledLock)
            {
                boolean bool1 = this.mInternalDataEnabled;
                int i = this.mCdmaPhone.mSST.getCurrentDataConnectionState();
                if ((this.mPhone.getServiceState().getRoaming()) && (!getDataOnRoamingEnabled()))
                {
                    j = 1;
                    boolean bool2 = this.mCdmaPhone.mSST.getDesiredPowerState();
                    if (this.mCdmaSSM.getCdmaSubscriptionSource() != 1)
                        break label511;
                    k = 1;
                    if (((i != 0) && (!this.mAutoAttachOnCreation)) || ((k == 0) && (!this.mCdmaPhone.mIccRecords.getRecordsLoaded())) || ((!this.mCdmaPhone.mSST.isConcurrentVoiceAndDataAllowed()) && (this.mPhone.getState() != Phone.State.IDLE)) || (j != 0) || (!bool1) || (!bool2) || (this.mPendingRestartRadio) || ((this.mPhone.getLteOnCdmaMode() != 1) && (this.mCdmaPhone.needsOtaServiceProvisioning())))
                        break label517;
                    bool3 = true;
                    if (!bool3)
                    {
                        String str = "";
                        if ((i != 0) && (!this.mAutoAttachOnCreation))
                            str = str + " - psState= " + i;
                        if ((k == 0) && (!this.mCdmaPhone.mIccRecords.getRecordsLoaded()))
                            str = str + " - RUIM not loaded";
                        if ((!this.mCdmaPhone.mSST.isConcurrentVoiceAndDataAllowed()) && (this.mPhone.getState() != Phone.State.IDLE))
                            str = str + " - concurrentVoiceAndData not allowed and state= " + this.mPhone.getState();
                        if (j != 0)
                            str = str + " - Roaming";
                        if (!bool1)
                            str = str + " - mInternalDataEnabled= false";
                        if (!bool2)
                            str = str + " - desiredPowerState= false";
                        if (this.mPendingRestartRadio)
                            str = str + " - mPendingRestartRadio= true";
                        if (this.mCdmaPhone.needsOtaServiceProvisioning())
                            str = str + " - needs Provisioning";
                        log("Data not allowed due to" + str);
                    }
                    return bool3;
                }
            }
            int j = 0;
            continue;
            label511: int k = 0;
            continue;
            label517: boolean bool3 = false;
        }
    }

    protected boolean isDataPossible(String paramString)
    {
        if ((isDataAllowed()) && ((!getAnyDataEnabled()) || ((this.mState != DataConnectionTracker.State.FAILED) && (this.mState != DataConnectionTracker.State.IDLE))));
        for (boolean bool = true; ; bool = false)
        {
            if ((!bool) && (isDataAllowed()))
                log("Data not possible.    No coverage: dataState = " + this.mState);
            return bool;
        }
    }

    public boolean isDisconnected()
    {
        if ((this.mState == DataConnectionTracker.State.IDLE) || (this.mState == DataConnectionTracker.State.FAILED));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void log(String paramString)
    {
        Log.d("CDMA", "[CdmaDCT] " + paramString);
    }

    protected void loge(String paramString)
    {
        Log.e("CDMA", "[CdmaDCT] " + paramString);
    }

    protected void onCleanUpAllConnections(String paramString)
    {
        cleanUpConnection(true, paramString, false);
    }

    protected void onCleanUpConnection(boolean paramBoolean, int paramInt, String paramString)
    {
        if (paramInt == 3);
        for (boolean bool = true; ; bool = false)
        {
            cleanUpConnection(paramBoolean, paramString, bool);
            return;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onDataSetupComplete(AsyncResult paramAsyncResult)
    {
        String str = null;
        if ((paramAsyncResult.userObj instanceof String))
            str = (String)paramAsyncResult.userObj;
        if (isDataSetupCompleteOk(paramAsyncResult))
        {
            Injector.onDataSetupComplete(this);
            notifyDefaultData(str);
        }
        while (true)
        {
            return;
            DataConnection.FailCause localFailCause = (DataConnection.FailCause)paramAsyncResult.result;
            log("Data Connection setup failed " + localFailCause);
            if (localFailCause.isPermanentFail())
            {
                notifyNoData(localFailCause);
            }
            else
            {
                int i = -1;
                if ((paramAsyncResult.exception instanceof DataConnection.CallSetupException))
                    i = ((DataConnection.CallSetupException)paramAsyncResult.exception).getRetryOverride();
                if (i == 2147483647)
                    log("No retry is suggested.");
                else
                    startDelayedRetry(localFailCause, str, i);
            }
        }
    }

    protected void onDataStateChanged(AsyncResult paramAsyncResult)
    {
        ArrayList localArrayList = (ArrayList)paramAsyncResult.result;
        if (paramAsyncResult.exception != null);
        while (true)
        {
            return;
            if (this.mState == DataConnectionTracker.State.CONNECTED)
            {
                int i = 0;
                int j = 0;
                for (int k = 0; ; k++)
                    if (k < localArrayList.size())
                    {
                        j = ((DataCallState)localArrayList.get(k)).active;
                        if (j != 0)
                            i = 1;
                    }
                    else
                    {
                        if (i != 0)
                            break label98;
                        log("onDataStateChanged: No active connectionstate is CONNECTED, disconnecting/cleanup");
                        writeEventLogCdmaDataDrop();
                        cleanUpConnection(true, null, false);
                        break;
                    }
                switch (j)
                {
                default:
                    log("onDataStateChanged: IGNORE unexpected DataCallState.active=" + j);
                    break;
                case 2:
                    log("onDataStateChanged: active=LINK_ACTIVE && CONNECTED, ignore");
                    this.mActivity = DataConnectionTracker.Activity.NONE;
                    this.mPhone.notifyDataActivity();
                    startNetStatPoll();
                    break;
                case 1:
                    label98: log("onDataStateChanged active=LINK_DOWN && CONNECTED, dormant");
                    this.mActivity = DataConnectionTracker.Activity.DORMANT;
                    this.mPhone.notifyDataActivity();
                    stopNetStatPoll();
                    break;
                }
            }
            else
            {
                log("onDataStateChanged: not connected, state=" + this.mState + " ignoring");
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected void onDisconnectDone(int paramInt, AsyncResult paramAsyncResult)
    {
        log("EVENT_DISCONNECT_DONE connId=" + paramInt);
        String str = null;
        if ((paramAsyncResult.userObj instanceof String))
            str = (String)paramAsyncResult.userObj;
        setState(DataConnectionTracker.State.IDLE);
        Injector.onDisconnectDone(this);
        if (this.mPendingRestartRadio)
            removeMessages(270362);
        if (this.mCdmaPhone.mSST.processPendingRadioPowerOffAfterDataOff())
            this.mPendingRestartRadio = false;
        while (true)
        {
            notifyDataConnection(str);
            this.mActiveApn = null;
            if (retryAfterDisconnected(str))
                startAlarmForReconnect(APN_DELAY_MILLIS, str);
            return;
            onRestartRadio();
        }
    }

    protected void onEnableNewApn()
    {
        cleanUpConnection(true, "apnSwitched", false);
    }

    protected void onNVReady()
    {
        if (this.mState == DataConnectionTracker.State.FAILED)
            cleanUpAllConnections(null);
        sendMessage(obtainMessage(270339));
    }

    protected void onRadioAvailable()
    {
        if (this.mPhone.getSimulatedRadioControl() != null)
        {
            setState(DataConnectionTracker.State.CONNECTED);
            notifyDataConnection(null);
            log("We're on the simulator; assuming data is connected");
        }
        notifyOffApnsOfAvailability(null);
        if (this.mState != DataConnectionTracker.State.IDLE)
            cleanUpAllConnections(null);
    }

    protected void onRadioOffOrNotAvailable()
    {
        ((DataConnection)this.mDataConnections.get(Integer.valueOf(0))).resetRetryCount();
        if (this.mPhone.getSimulatedRadioControl() != null)
            log("We're on the simulator; assuming radio off is meaningless");
        while (true)
        {
            return;
            log("Radio is off and clean up all connection");
            cleanUpAllConnections(null);
        }
    }

    protected void onRecordsLoaded()
    {
        if (this.mState == DataConnectionTracker.State.FAILED)
            cleanUpAllConnections(null);
        sendMessage(obtainMessage(270339, "simLoaded"));
    }

    protected void onRoamingOff()
    {
        if (!this.mUserDataEnabled);
        while (true)
        {
            return;
            if (!getDataOnRoamingEnabled())
            {
                notifyOffApnsOfAvailability("roamingOff");
                trySetupData("roamingOff");
            }
            else
            {
                notifyDataConnection("roamingOff");
            }
        }
    }

    protected void onRoamingOn()
    {
        if (!this.mUserDataEnabled);
        while (true)
        {
            return;
            if (getDataOnRoamingEnabled())
            {
                trySetupData("roamingOn");
                notifyDataConnection("roamingOn");
            }
            else
            {
                log("Tear down data connection on roaming.");
                cleanUpAllConnections(null);
                notifyOffApnsOfAvailability("roamingOn");
            }
        }
    }

    protected boolean onTrySetupData(String paramString)
    {
        return trySetupData(paramString);
    }

    protected void onVoiceCallEnded()
    {
        if (this.mState == DataConnectionTracker.State.CONNECTED)
            if (!this.mCdmaPhone.mSST.isConcurrentVoiceAndDataAllowed())
            {
                startNetStatPoll();
                notifyDataConnection("2GVoiceCallEnded");
                notifyOffApnsOfAvailability("2GVoiceCallEnded");
            }
        while (true)
        {
            return;
            resetPollStats();
            break;
            ((DataConnection)this.mDataConnections.get(Integer.valueOf(0))).resetRetryCount();
            trySetupData("2GVoiceCallEnded");
        }
    }

    protected void onVoiceCallStarted()
    {
        if ((this.mState == DataConnectionTracker.State.CONNECTED) && (!this.mCdmaPhone.mSST.isConcurrentVoiceAndDataAllowed()))
        {
            stopNetStatPoll();
            notifyDataConnection("2GVoiceCallStarted");
            notifyOffApnsOfAvailability("2GVoiceCallStarted");
        }
    }

    protected void restartDataStallAlarm()
    {
    }

    protected void restartRadio()
    {
        log("Cleanup connection and wait " + TIME_DELAYED_TO_RESTART_RADIO / 1000 + "s to restart radio");
        cleanUpAllConnections(null);
        sendEmptyMessageDelayed(270362, TIME_DELAYED_TO_RESTART_RADIO);
        this.mPendingRestartRadio = true;
    }

    protected void setState(DataConnectionTracker.State paramState)
    {
        log("setState: " + paramState);
        if (this.mState != paramState)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = this.mState.toString();
            arrayOfObject[1] = paramState.toString();
            EventLog.writeEvent(50115, arrayOfObject);
            this.mState = paramState;
        }
    }

    protected void startNetStatPoll()
    {
        if ((this.mState == DataConnectionTracker.State.CONNECTED) && (!this.mNetStatPollEnabled))
        {
            log("[DataConnection] Start poll NetStat");
            resetPollStats();
            this.mNetStatPollEnabled = true;
            this.mPollNetStat.run();
        }
    }

    protected void stopNetStatPoll()
    {
        this.mNetStatPollEnabled = false;
        removeCallbacks(this.mPollNetStat);
        log("[DataConnection] Stop poll NetStat");
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void onDataSetupComplete(CdmaDataConnectionTracker paramCdmaDataConnectionTracker)
        {
            Object localObject = null;
            ApnSetting localApnSetting = paramCdmaDataConnectionTracker.getActiveApn();
            Iterator localIterator = paramCdmaDataConnectionTracker.getDataConnectionAsyncChannels().values().iterator();
            while (localIterator.hasNext())
            {
                DataConnectionAc localDataConnectionAc = (DataConnectionAc)localIterator.next();
                if (localDataConnectionAc.getApnSettingSync().equals(localApnSetting))
                    localObject = localDataConnectionAc;
            }
            if ((localObject != null) && (localApnSetting != null))
                FirewallManager.getInstance().onDataConnected(0, FirewallManager.encodeApnSetting(localApnSetting), localObject.getLinkPropertiesSync().getInterfaceName());
        }

        static void onDisconnectDone(CdmaDataConnectionTracker paramCdmaDataConnectionTracker)
        {
            FirewallManager.getInstance().onDataDisconnected(0, FirewallManager.encodeApnSetting(paramCdmaDataConnectionTracker.getActiveApn()));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.cdma.CdmaDataConnectionTracker
 * JD-Core Version:        0.6.2
 */