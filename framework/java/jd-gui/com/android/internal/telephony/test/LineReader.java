package com.android.internal.telephony.test;

import java.io.InputStream;

class LineReader
{
    static final int BUFFER_SIZE = 4096;
    byte[] buffer = new byte[4096];
    InputStream inStream;

    LineReader(InputStream paramInputStream)
    {
        this.inStream = paramInputStream;
    }

    String getNextLine()
    {
        return getNextLine(false);
    }

    // ERROR //
    String getNextLine(boolean paramBoolean)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: iconst_0
        //     3: istore_3
        //     4: aload_0
        //     5: getfield 20	com/android/internal/telephony/test/LineReader:inStream	Ljava/io/InputStream;
        //     8: invokevirtual 37	java/io/InputStream:read	()I
        //     11: istore 10
        //     13: iload 10
        //     15: ifge +7 -> 22
        //     18: iload_3
        //     19: pop
        //     20: aload_2
        //     21: areturn
        //     22: iload_1
        //     23: ifeq +37 -> 60
        //     26: iload 10
        //     28: bipush 26
        //     30: if_icmpne +30 -> 60
        //     33: iload_3
        //     34: istore 5
        //     36: new 39	java/lang/String
        //     39: dup
        //     40: aload_0
        //     41: getfield 18	com/android/internal/telephony/test/LineReader:buffer	[B
        //     44: iconst_0
        //     45: iload 5
        //     47: ldc 41
        //     49: invokespecial 44	java/lang/String:<init>	([BIILjava/lang/String;)V
        //     52: astore 6
        //     54: aload 6
        //     56: astore_2
        //     57: goto -37 -> 20
        //     60: iload 10
        //     62: bipush 13
        //     64: if_icmpeq +10 -> 74
        //     67: iload 10
        //     69: bipush 10
        //     71: if_icmpne +10 -> 81
        //     74: iload_3
        //     75: ifne -42 -> 33
        //     78: goto -74 -> 4
        //     81: aload_0
        //     82: getfield 18	com/android/internal/telephony/test/LineReader:buffer	[B
        //     85: astore 11
        //     87: iload_3
        //     88: iconst_1
        //     89: iadd
        //     90: istore 5
        //     92: iload 10
        //     94: i2b
        //     95: istore 12
        //     97: aload 11
        //     99: iload_3
        //     100: iload 12
        //     102: bastore
        //     103: iload 5
        //     105: istore_3
        //     106: goto -102 -> 4
        //     109: astore 8
        //     111: iload_3
        //     112: pop
        //     113: goto -93 -> 20
        //     116: astore 4
        //     118: iload_3
        //     119: istore 5
        //     121: getstatic 50	java/lang/System:err	Ljava/io/PrintStream;
        //     124: ldc 52
        //     126: invokevirtual 58	java/io/PrintStream:println	(Ljava/lang/String;)V
        //     129: goto -93 -> 36
        //     132: astore 7
        //     134: getstatic 50	java/lang/System:err	Ljava/io/PrintStream;
        //     137: ldc 60
        //     139: invokevirtual 58	java/io/PrintStream:println	(Ljava/lang/String;)V
        //     142: goto -122 -> 20
        //     145: astore 14
        //     147: goto -26 -> 121
        //     150: astore 13
        //     152: goto -39 -> 113
        //
        // Exception table:
        //     from	to	target	type
        //     4	13	109	java/io/IOException
        //     81	87	109	java/io/IOException
        //     4	13	116	java/lang/IndexOutOfBoundsException
        //     81	87	116	java/lang/IndexOutOfBoundsException
        //     36	54	132	java/io/UnsupportedEncodingException
        //     97	103	145	java/lang/IndexOutOfBoundsException
        //     97	103	150	java/io/IOException
    }

    String getNextLineCtrlZ()
    {
        return getNextLine(true);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.test.LineReader
 * JD-Core Version:        0.6.2
 */