package com.android.internal.telephony.test;

import android.os.AsyncResult;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Registrant;
import android.os.RegistrantList;
import android.util.Log;
import com.android.internal.telephony.BaseCommands;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandException.Error;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.CommandsInterface.RadioState;
import com.android.internal.telephony.UUSInfo;
import com.android.internal.telephony.gsm.SmsBroadcastConfigInfo;
import com.android.internal.telephony.gsm.SuppServiceNotification;
import java.util.ArrayList;

public final class SimulatedCommands extends BaseCommands
    implements CommandsInterface, SimulatedRadioControl
{
    private static final String DEFAULT_SIM_PIN2_CODE = "5678";
    private static final String DEFAULT_SIM_PIN_CODE = "1234";
    private static final SimFdnState INITIAL_FDN_STATE = SimFdnState.NONE;
    private static final SimLockState INITIAL_LOCK_STATE = SimLockState.NONE;
    private static final String LOG_TAG = "SIM";
    private static final String SIM_PUK2_CODE = "87654321";
    private static final String SIM_PUK_CODE = "12345678";
    HandlerThread mHandlerThread = new HandlerThread("SimulatedCommands");
    int mNetworkType;
    String mPin2Code;
    int mPin2UnlockAttempts;
    String mPinCode;
    int mPinUnlockAttempts;
    int mPuk2UnlockAttempts;
    int mPukUnlockAttempts;
    boolean mSimFdnEnabled;
    SimFdnState mSimFdnEnabledState;
    boolean mSimLockEnabled;
    SimLockState mSimLockedState;
    boolean mSsnNotifyOn = false;
    int nextCallFailCause = 16;
    int pausedResponseCount;
    ArrayList<Message> pausedResponses = new ArrayList();
    SimulatedGsmCallState simulatedCallState;

    public SimulatedCommands()
    {
        super(null);
        this.mHandlerThread.start();
        this.simulatedCallState = new SimulatedGsmCallState(this.mHandlerThread.getLooper());
        setRadioState(CommandsInterface.RadioState.RADIO_OFF);
        this.mSimLockedState = INITIAL_LOCK_STATE;
        boolean bool2;
        if (this.mSimLockedState != SimLockState.NONE)
        {
            bool2 = bool1;
            this.mSimLockEnabled = bool2;
            this.mPinCode = "1234";
            this.mSimFdnEnabledState = INITIAL_FDN_STATE;
            if (this.mSimFdnEnabledState == SimFdnState.NONE)
                break label138;
        }
        while (true)
        {
            this.mSimFdnEnabled = bool1;
            this.mPin2Code = "5678";
            return;
            bool2 = false;
            break;
            label138: bool1 = false;
        }
    }

    private boolean isSimLocked()
    {
        if (this.mSimLockedState != SimLockState.NONE);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void resultFail(Message paramMessage, Throwable paramThrowable)
    {
        if (paramMessage != null)
        {
            AsyncResult.forMessage(paramMessage).exception = paramThrowable;
            if (this.pausedResponseCount <= 0)
                break label29;
            this.pausedResponses.add(paramMessage);
        }
        while (true)
        {
            return;
            label29: paramMessage.sendToTarget();
        }
    }

    private void resultSuccess(Message paramMessage, Object paramObject)
    {
        if (paramMessage != null)
        {
            AsyncResult.forMessage(paramMessage).result = paramObject;
            if (this.pausedResponseCount <= 0)
                break label29;
            this.pausedResponses.add(paramMessage);
        }
        while (true)
        {
            return;
            label29: paramMessage.sendToTarget();
        }
    }

    private void unimplemented(Message paramMessage)
    {
        if (paramMessage != null)
        {
            AsyncResult.forMessage(paramMessage).exception = new RuntimeException("Unimplemented");
            if (this.pausedResponseCount <= 0)
                break label37;
            this.pausedResponses.add(paramMessage);
        }
        while (true)
        {
            return;
            label37: paramMessage.sendToTarget();
        }
    }

    public void acceptCall(Message paramMessage)
    {
        if (!this.simulatedCallState.onAnswer())
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
        }
    }

    public void acknowledgeIncomingGsmSmsWithPdu(boolean paramBoolean, String paramString, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void acknowledgeLastIncomingCdmaSms(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void acknowledgeLastIncomingGsmSms(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void cancelPendingUssd(Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void changeBarringPassword(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void changeIccPin(String paramString1, String paramString2, Message paramMessage)
    {
        if ((paramString1 != null) && (paramString1.equals(this.mPinCode)))
        {
            this.mPinCode = paramString2;
            if (paramMessage != null)
            {
                AsyncResult.forMessage(paramMessage, null, null);
                paramMessage.sendToTarget();
            }
        }
        while (true)
        {
            return;
            if (paramMessage != null)
            {
                Log.i("SIM", "[SimCmd] changeIccPin: pin failed!");
                AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
                paramMessage.sendToTarget();
            }
        }
    }

    public void changeIccPin2(String paramString1, String paramString2, Message paramMessage)
    {
        if ((paramString1 != null) && (paramString1.equals(this.mPin2Code)))
        {
            this.mPin2Code = paramString2;
            if (paramMessage != null)
            {
                AsyncResult.forMessage(paramMessage, null, null);
                paramMessage.sendToTarget();
            }
        }
        while (true)
        {
            return;
            if (paramMessage != null)
            {
                Log.i("SIM", "[SimCmd] changeIccPin2: pin2 failed!");
                AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
                paramMessage.sendToTarget();
            }
        }
    }

    public void changeIccPin2ForApp(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void changeIccPinForApp(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void conference(Message paramMessage)
    {
        if (!this.simulatedCallState.onChld('3', '\000'))
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
        }
    }

    public void deactivateDataCall(int paramInt1, int paramInt2, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void deleteSmsOnRuim(int paramInt, Message paramMessage)
    {
        Log.d("SIM", "Delete RUIM message at index " + paramInt);
        unimplemented(paramMessage);
    }

    public void deleteSmsOnSim(int paramInt, Message paramMessage)
    {
        Log.d("SIM", "Delete message at index " + paramInt);
        unimplemented(paramMessage);
    }

    public void dial(String paramString, int paramInt, Message paramMessage)
    {
        this.simulatedCallState.onDial(paramString);
        resultSuccess(paramMessage, null);
    }

    public void dial(String paramString, int paramInt, UUSInfo paramUUSInfo, Message paramMessage)
    {
        this.simulatedCallState.onDial(paramString);
        resultSuccess(paramMessage, null);
    }

    public void exitEmergencyCallbackMode(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void explicitCallTransfer(Message paramMessage)
    {
        if (!this.simulatedCallState.onChld('4', '\000'))
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
        }
    }

    public void forceDataDormancy(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getAvailableNetworks(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getBasebandVersion(Message paramMessage)
    {
        resultSuccess(paramMessage, "SimulatedCommands");
    }

    public void getCDMASubscription(Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void getCLIR(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getCdmaBroadcastConfig(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getCdmaSubscriptionSource(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getCurrentCalls(Message paramMessage)
    {
        if ((this.mState == CommandsInterface.RadioState.RADIO_ON) && (!isSimLocked()))
            resultSuccess(paramMessage, this.simulatedCallState.getDriverCalls());
        while (true)
        {
            return;
            resultFail(paramMessage, new CommandException(CommandException.Error.RADIO_NOT_AVAILABLE));
        }
    }

    public void getDataCallList(Message paramMessage)
    {
        resultSuccess(paramMessage, new ArrayList(0));
    }

    public void getDataRegistrationState(Message paramMessage)
    {
        String[] arrayOfString = new String[4];
        arrayOfString[0] = "5";
        arrayOfString[1] = null;
        arrayOfString[2] = null;
        arrayOfString[3] = "2";
        resultSuccess(paramMessage, arrayOfString);
    }

    public void getDeviceIdentity(Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void getGsmBroadcastConfig(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getIMEI(Message paramMessage)
    {
        resultSuccess(paramMessage, "012345678901234");
    }

    public void getIMEISV(Message paramMessage)
    {
        resultSuccess(paramMessage, "99");
    }

    public void getIMSI(Message paramMessage)
    {
        getIMSIForApp(null, paramMessage);
    }

    public void getIMSIForApp(String paramString, Message paramMessage)
    {
        resultSuccess(paramMessage, "012345678901234");
    }

    public void getIccCardStatus(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getLastCallFailCause(Message paramMessage)
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = this.nextCallFailCause;
        resultSuccess(paramMessage, arrayOfInt);
    }

    public void getLastDataCallFailCause(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getLastPdpFailCause(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getMute(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getNeighboringCids(Message paramMessage)
    {
        int[] arrayOfInt = new int[7];
        arrayOfInt[0] = 6;
        for (int i = 1; i < 7; i++)
            arrayOfInt[i] = i;
        resultSuccess(paramMessage, arrayOfInt);
    }

    public void getNetworkSelectionMode(Message paramMessage)
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 0;
        resultSuccess(paramMessage, arrayOfInt);
    }

    public void getOperator(Message paramMessage)
    {
        String[] arrayOfString = new String[3];
        arrayOfString[0] = "El Telco Loco";
        arrayOfString[1] = "Telco Loco";
        arrayOfString[2] = "001001";
        resultSuccess(paramMessage, arrayOfString);
    }

    public void getPDPContextList(Message paramMessage)
    {
        getDataCallList(paramMessage);
    }

    public void getPreferredNetworkType(Message paramMessage)
    {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = this.mNetworkType;
        resultSuccess(paramMessage, arrayOfInt);
    }

    public void getPreferredVoicePrivacy(Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void getSignalStrength(Message paramMessage)
    {
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 23;
        arrayOfInt[1] = 0;
        resultSuccess(paramMessage, arrayOfInt);
    }

    public void getSmscAddress(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getVoiceRadioTechnology(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void getVoiceRegistrationState(Message paramMessage)
    {
        String[] arrayOfString = new String[14];
        arrayOfString[0] = "5";
        arrayOfString[1] = null;
        arrayOfString[2] = null;
        arrayOfString[3] = null;
        arrayOfString[4] = null;
        arrayOfString[5] = null;
        arrayOfString[6] = null;
        arrayOfString[7] = null;
        arrayOfString[8] = null;
        arrayOfString[9] = null;
        arrayOfString[10] = null;
        arrayOfString[11] = null;
        arrayOfString[12] = null;
        arrayOfString[13] = null;
        resultSuccess(paramMessage, arrayOfString);
    }

    public void handleCallSetupRequestFromSim(boolean paramBoolean, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void hangupConnection(int paramInt, Message paramMessage)
    {
        if (!this.simulatedCallState.onChld('1', (char)(paramInt + 48)))
        {
            Log.i("GSM", "[SimCmd] hangupConnection: resultFail");
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        }
        while (true)
        {
            return;
            Log.i("GSM", "[SimCmd] hangupConnection: resultSuccess");
            resultSuccess(paramMessage, null);
        }
    }

    public void hangupForegroundResumeBackground(Message paramMessage)
    {
        if (!this.simulatedCallState.onChld('1', '\000'))
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
        }
    }

    public void hangupWaitingOrBackground(Message paramMessage)
    {
        if (!this.simulatedCallState.onChld('0', '\000'))
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
        }
    }

    public void iccIO(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, String paramString2, String paramString3, Message paramMessage)
    {
        iccIOForApp(paramInt1, paramInt2, paramString1, paramInt3, paramInt4, paramInt5, paramString2, paramString3, null, paramMessage);
    }

    public void iccIOForApp(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, String paramString2, String paramString3, String paramString4, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void invokeOemRilRequestRaw(byte[] paramArrayOfByte, Message paramMessage)
    {
        if (paramMessage != null)
        {
            AsyncResult.forMessage(paramMessage).result = paramArrayOfByte;
            paramMessage.sendToTarget();
        }
    }

    public void invokeOemRilRequestStrings(String[] paramArrayOfString, Message paramMessage)
    {
        if (paramMessage != null)
        {
            AsyncResult.forMessage(paramMessage).result = paramArrayOfString;
            paramMessage.sendToTarget();
        }
    }

    public void pauseResponses()
    {
        this.pausedResponseCount = (1 + this.pausedResponseCount);
    }

    public void progressConnectingCallState()
    {
        this.simulatedCallState.progressConnectingCallState();
        this.mCallStateRegistrants.notifyRegistrants();
    }

    public void progressConnectingToActive()
    {
        this.simulatedCallState.progressConnectingToActive();
        this.mCallStateRegistrants.notifyRegistrants();
    }

    public void queryAvailableBandMode(Message paramMessage)
    {
        int[] arrayOfInt = new int[4];
        arrayOfInt[0] = 4;
        arrayOfInt[1] = 2;
        arrayOfInt[2] = 3;
        arrayOfInt[3] = 4;
        resultSuccess(paramMessage, arrayOfInt);
    }

    public void queryCLIP(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void queryCallForwardStatus(int paramInt1, int paramInt2, String paramString, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void queryCallWaiting(int paramInt, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void queryCdmaRoamingPreference(Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void queryFacilityLock(String paramString1, String paramString2, int paramInt, Message paramMessage)
    {
        queryFacilityLockForApp(paramString1, paramString2, paramInt, null, paramMessage);
    }

    public void queryFacilityLockForApp(String paramString1, String paramString2, int paramInt, String paramString3, Message paramMessage)
    {
        int i = 1;
        String str2;
        if ((paramString1 != null) && (paramString1.equals("SC")))
            if (paramMessage != null)
            {
                int[] arrayOfInt2 = new int[i];
                if (!this.mSimLockEnabled)
                    break label99;
                arrayOfInt2[0] = i;
                StringBuilder localStringBuilder2 = new StringBuilder().append("[SimCmd] queryFacilityLock: SIM is ");
                if (arrayOfInt2[0] != 0)
                    break label105;
                str2 = "unlocked";
                label68: Log.i("SIM", str2);
                AsyncResult.forMessage(paramMessage, arrayOfInt2, null);
                paramMessage.sendToTarget();
            }
        while (true)
        {
            return;
            label99: i = 0;
            break;
            label105: str2 = "locked";
            break label68;
            if ((paramString1 != null) && (paramString1.equals("FD")))
            {
                if (paramMessage != null)
                {
                    int[] arrayOfInt1 = new int[i];
                    label145: StringBuilder localStringBuilder1;
                    if (this.mSimFdnEnabled)
                    {
                        arrayOfInt1[0] = i;
                        localStringBuilder1 = new StringBuilder().append("[SimCmd] queryFacilityLock: FDN is ");
                        if (arrayOfInt1[0] != 0)
                            break label217;
                    }
                    label217: for (String str1 = "disabled"; ; str1 = "enabled")
                    {
                        Log.i("SIM", str1);
                        AsyncResult.forMessage(paramMessage, arrayOfInt1, null);
                        paramMessage.sendToTarget();
                        break;
                        i = 0;
                        break label145;
                    }
                }
            }
            else
                unimplemented(paramMessage);
        }
    }

    public void queryTTYMode(Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void rejectCall(Message paramMessage)
    {
        if (!this.simulatedCallState.onChld('0', '\000'))
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
        }
    }

    public void reportSmsMemoryStatus(boolean paramBoolean, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void reportStkServiceIsRunning(Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void requestIsimAuthentication(String paramString, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void resetRadio(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void resumeResponses()
    {
        this.pausedResponseCount = (-1 + this.pausedResponseCount);
        if (this.pausedResponseCount == 0)
        {
            int i = 0;
            int j = this.pausedResponses.size();
            while (i < j)
            {
                ((Message)this.pausedResponses.get(i)).sendToTarget();
                i++;
            }
            this.pausedResponses.clear();
        }
        while (true)
        {
            return;
            Log.e("GSM", "SimulatedCommands.resumeResponses < 0");
        }
    }

    public void sendBurstDtmf(String paramString, int paramInt1, int paramInt2, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void sendCDMAFeatureCode(String paramString, Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void sendCdmaSms(byte[] paramArrayOfByte, Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
    }

    public void sendDtmf(char paramChar, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void sendEnvelope(String paramString, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void sendEnvelopeWithStatus(String paramString, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void sendSMS(String paramString1, String paramString2, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void sendTerminalResponse(String paramString, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void sendUSSD(String paramString, Message paramMessage)
    {
        if (paramString.equals("#646#"))
        {
            resultSuccess(paramMessage, null);
            triggerIncomingUssd("0", "You have NNN minutes remaining.");
        }
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
            triggerIncomingUssd("0", "All Done");
        }
    }

    public void separateConnection(int paramInt, Message paramMessage)
    {
        char c = (char)(paramInt + 48);
        if (!this.simulatedCallState.onChld('2', c))
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
        }
    }

    public void setAutoProgressConnectingCall(boolean paramBoolean)
    {
        this.simulatedCallState.setAutoProgressConnectingCall(paramBoolean);
    }

    public void setBandMode(int paramInt, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void setCLIR(int paramInt, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setCallForward(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setCallWaiting(boolean paramBoolean, int paramInt, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setCdmaBroadcastActivation(boolean paramBoolean, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setCdmaBroadcastConfig(int[] paramArrayOfInt, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setCdmaRoamingPreference(int paramInt, Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void setCdmaSubscriptionSource(int paramInt, Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void setFacilityLock(String paramString1, boolean paramBoolean, String paramString2, int paramInt, Message paramMessage)
    {
        setFacilityLockForApp(paramString1, paramBoolean, paramString2, paramInt, null, paramMessage);
    }

    public void setFacilityLockForApp(String paramString1, boolean paramBoolean, String paramString2, int paramInt, String paramString3, Message paramMessage)
    {
        if ((paramString1 != null) && (paramString1.equals("SC")))
            if ((paramString2 != null) && (paramString2.equals(this.mPinCode)))
            {
                Log.i("SIM", "[SimCmd] setFacilityLock: pin is valid");
                this.mSimLockEnabled = paramBoolean;
                if (paramMessage != null)
                {
                    AsyncResult.forMessage(paramMessage, null, null);
                    paramMessage.sendToTarget();
                }
            }
        while (true)
        {
            return;
            if (paramMessage != null)
            {
                Log.i("SIM", "[SimCmd] setFacilityLock: pin failed!");
                AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.GENERIC_FAILURE));
                paramMessage.sendToTarget();
                continue;
                if ((paramString1 != null) && (paramString1.equals("FD")))
                {
                    if ((paramString2 != null) && (paramString2.equals(this.mPin2Code)))
                    {
                        Log.i("SIM", "[SimCmd] setFacilityLock: pin2 is valid");
                        this.mSimFdnEnabled = paramBoolean;
                        if (paramMessage != null)
                        {
                            AsyncResult.forMessage(paramMessage, null, null);
                            paramMessage.sendToTarget();
                        }
                    }
                    else if (paramMessage != null)
                    {
                        Log.i("SIM", "[SimCmd] setFacilityLock: pin2 failed!");
                        AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.GENERIC_FAILURE));
                        paramMessage.sendToTarget();
                    }
                }
                else
                    unimplemented(paramMessage);
            }
        }
    }

    public void setGsmBroadcastActivation(boolean paramBoolean, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setGsmBroadcastConfig(SmsBroadcastConfigInfo[] paramArrayOfSmsBroadcastConfigInfo, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setLocationUpdates(boolean paramBoolean, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setMute(boolean paramBoolean, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setNetworkSelectionModeAutomatic(Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setNetworkSelectionModeManual(String paramString, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setNextCallFailCause(int paramInt)
    {
        this.nextCallFailCause = paramInt;
    }

    public void setNextDialFailImmediately(boolean paramBoolean)
    {
        this.simulatedCallState.setNextDialFailImmediately(paramBoolean);
    }

    public void setPhoneType(int paramInt)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
    }

    public void setPreferredNetworkType(int paramInt, Message paramMessage)
    {
        this.mNetworkType = paramInt;
        resultSuccess(paramMessage, null);
    }

    public void setPreferredVoicePrivacy(boolean paramBoolean, Message paramMessage)
    {
        Log.w("SIM", "CDMA not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void setRadioPower(boolean paramBoolean, Message paramMessage)
    {
        if (paramBoolean)
            setRadioState(CommandsInterface.RadioState.RADIO_ON);
        while (true)
        {
            return;
            setRadioState(CommandsInterface.RadioState.RADIO_OFF);
        }
    }

    public void setSmscAddress(String paramString, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void setSuppServiceNotifications(boolean paramBoolean, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
        if ((paramBoolean) && (this.mSsnNotifyOn))
            Log.w("SIM", "Supp Service Notifications already enabled!");
        this.mSsnNotifyOn = paramBoolean;
    }

    public void setTTYMode(int paramInt, Message paramMessage)
    {
        Log.w("SIM", "Not implemented in SimulatedCommands");
        unimplemented(paramMessage);
    }

    public void setupDataCall(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void shutdown()
    {
        setRadioState(CommandsInterface.RadioState.RADIO_UNAVAILABLE);
        Looper localLooper = this.mHandlerThread.getLooper();
        if (localLooper != null)
            localLooper.quit();
    }

    public void startDtmf(char paramChar, Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void stopDtmf(Message paramMessage)
    {
        resultSuccess(paramMessage, null);
    }

    public void supplyIccPin(String paramString, Message paramMessage)
    {
        if (this.mSimLockedState != SimLockState.REQUIRE_PIN)
        {
            Log.i("SIM", "[SimCmd] supplyIccPin: wrong state, state=" + this.mSimLockedState);
            AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
            paramMessage.sendToTarget();
        }
        while (true)
        {
            return;
            if ((paramString != null) && (paramString.equals(this.mPinCode)))
            {
                Log.i("SIM", "[SimCmd] supplyIccPin: success!");
                this.mPinUnlockAttempts = 0;
                this.mSimLockedState = SimLockState.NONE;
                this.mIccStatusChangedRegistrants.notifyRegistrants();
                if (paramMessage != null)
                {
                    AsyncResult.forMessage(paramMessage, null, null);
                    paramMessage.sendToTarget();
                }
            }
            else if (paramMessage != null)
            {
                this.mPinUnlockAttempts = (1 + this.mPinUnlockAttempts);
                Log.i("SIM", "[SimCmd] supplyIccPin: failed! attempt=" + this.mPinUnlockAttempts);
                if (this.mPinUnlockAttempts >= 3)
                {
                    Log.i("SIM", "[SimCmd] supplyIccPin: set state to REQUIRE_PUK");
                    this.mSimLockedState = SimLockState.REQUIRE_PUK;
                }
                AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
                paramMessage.sendToTarget();
            }
        }
    }

    public void supplyIccPin2(String paramString, Message paramMessage)
    {
        if (this.mSimFdnEnabledState != SimFdnState.REQUIRE_PIN2)
        {
            Log.i("SIM", "[SimCmd] supplyIccPin2: wrong state, state=" + this.mSimFdnEnabledState);
            AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
            paramMessage.sendToTarget();
        }
        while (true)
        {
            return;
            if ((paramString != null) && (paramString.equals(this.mPin2Code)))
            {
                Log.i("SIM", "[SimCmd] supplyIccPin2: success!");
                this.mPin2UnlockAttempts = 0;
                this.mSimFdnEnabledState = SimFdnState.NONE;
                if (paramMessage != null)
                {
                    AsyncResult.forMessage(paramMessage, null, null);
                    paramMessage.sendToTarget();
                }
            }
            else if (paramMessage != null)
            {
                this.mPin2UnlockAttempts = (1 + this.mPin2UnlockAttempts);
                Log.i("SIM", "[SimCmd] supplyIccPin2: failed! attempt=" + this.mPin2UnlockAttempts);
                if (this.mPin2UnlockAttempts >= 3)
                {
                    Log.i("SIM", "[SimCmd] supplyIccPin2: set state to REQUIRE_PUK2");
                    this.mSimFdnEnabledState = SimFdnState.REQUIRE_PUK2;
                }
                AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
                paramMessage.sendToTarget();
            }
        }
    }

    public void supplyIccPin2ForApp(String paramString1, String paramString2, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void supplyIccPinForApp(String paramString1, String paramString2, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void supplyIccPuk(String paramString1, String paramString2, Message paramMessage)
    {
        if (this.mSimLockedState != SimLockState.REQUIRE_PUK)
        {
            Log.i("SIM", "[SimCmd] supplyIccPuk: wrong state, state=" + this.mSimLockedState);
            AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
            paramMessage.sendToTarget();
        }
        while (true)
        {
            return;
            if ((paramString1 != null) && (paramString1.equals("12345678")))
            {
                Log.i("SIM", "[SimCmd] supplyIccPuk: success!");
                this.mSimLockedState = SimLockState.NONE;
                this.mPukUnlockAttempts = 0;
                this.mIccStatusChangedRegistrants.notifyRegistrants();
                if (paramMessage != null)
                {
                    AsyncResult.forMessage(paramMessage, null, null);
                    paramMessage.sendToTarget();
                }
            }
            else if (paramMessage != null)
            {
                this.mPukUnlockAttempts = (1 + this.mPukUnlockAttempts);
                Log.i("SIM", "[SimCmd] supplyIccPuk: failed! attempt=" + this.mPukUnlockAttempts);
                if (this.mPukUnlockAttempts >= 10)
                {
                    Log.i("SIM", "[SimCmd] supplyIccPuk: set state to SIM_PERM_LOCKED");
                    this.mSimLockedState = SimLockState.SIM_PERM_LOCKED;
                }
                AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
                paramMessage.sendToTarget();
            }
        }
    }

    public void supplyIccPuk2(String paramString1, String paramString2, Message paramMessage)
    {
        if (this.mSimFdnEnabledState != SimFdnState.REQUIRE_PUK2)
        {
            Log.i("SIM", "[SimCmd] supplyIccPuk2: wrong state, state=" + this.mSimLockedState);
            AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
            paramMessage.sendToTarget();
        }
        while (true)
        {
            return;
            if ((paramString1 != null) && (paramString1.equals("87654321")))
            {
                Log.i("SIM", "[SimCmd] supplyIccPuk2: success!");
                this.mSimFdnEnabledState = SimFdnState.NONE;
                this.mPuk2UnlockAttempts = 0;
                if (paramMessage != null)
                {
                    AsyncResult.forMessage(paramMessage, null, null);
                    paramMessage.sendToTarget();
                }
            }
            else if (paramMessage != null)
            {
                this.mPuk2UnlockAttempts = (1 + this.mPuk2UnlockAttempts);
                Log.i("SIM", "[SimCmd] supplyIccPuk2: failed! attempt=" + this.mPuk2UnlockAttempts);
                if (this.mPuk2UnlockAttempts >= 10)
                {
                    Log.i("SIM", "[SimCmd] supplyIccPuk2: set state to SIM_PERM_LOCKED");
                    this.mSimFdnEnabledState = SimFdnState.SIM_PERM_LOCKED;
                }
                AsyncResult.forMessage(paramMessage, null, new CommandException(CommandException.Error.PASSWORD_INCORRECT));
                paramMessage.sendToTarget();
            }
        }
    }

    public void supplyIccPuk2ForApp(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void supplyIccPukForApp(String paramString1, String paramString2, String paramString3, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void supplyNetworkDepersonalization(String paramString, Message paramMessage)
    {
        unimplemented(paramMessage);
    }

    public void switchWaitingOrHoldingAndActive(Message paramMessage)
    {
        if (!this.simulatedCallState.onChld('2', '\000'))
            resultFail(paramMessage, new RuntimeException("Hangup Error"));
        while (true)
        {
            return;
            resultSuccess(paramMessage, null);
        }
    }

    public void triggerHangupAll()
    {
        this.simulatedCallState.triggerHangupAll();
        this.mCallStateRegistrants.notifyRegistrants();
    }

    public void triggerHangupBackground()
    {
        this.simulatedCallState.triggerHangupBackground();
        this.mCallStateRegistrants.notifyRegistrants();
    }

    public void triggerHangupForeground()
    {
        this.simulatedCallState.triggerHangupForeground();
        this.mCallStateRegistrants.notifyRegistrants();
    }

    public void triggerIncomingSMS(String paramString)
    {
    }

    public void triggerIncomingUssd(String paramString1, String paramString2)
    {
        if (this.mUSSDRegistrant != null)
        {
            String[] arrayOfString = new String[2];
            arrayOfString[0] = paramString1;
            arrayOfString[1] = paramString2;
            this.mUSSDRegistrant.notifyResult(arrayOfString);
        }
    }

    public void triggerRing(String paramString)
    {
        this.simulatedCallState.triggerRing(paramString);
        this.mCallStateRegistrants.notifyRegistrants();
    }

    public void triggerSsn(int paramInt1, int paramInt2)
    {
        SuppServiceNotification localSuppServiceNotification = new SuppServiceNotification();
        localSuppServiceNotification.notificationType = paramInt1;
        localSuppServiceNotification.code = paramInt2;
        this.mSsnRegistrant.notifyRegistrant(new AsyncResult(null, localSuppServiceNotification, null));
    }

    public void writeSmsToRuim(int paramInt, String paramString, Message paramMessage)
    {
        Log.d("SIM", "Write SMS to RUIM with status " + paramInt);
        unimplemented(paramMessage);
    }

    public void writeSmsToSim(int paramInt, String paramString1, String paramString2, Message paramMessage)
    {
        Log.d("SIM", "Write SMS to SIM with status " + paramInt);
        unimplemented(paramMessage);
    }

    private static enum SimFdnState
    {
        static
        {
            SimFdnState[] arrayOfSimFdnState = new SimFdnState[4];
            arrayOfSimFdnState[0] = NONE;
            arrayOfSimFdnState[1] = REQUIRE_PIN2;
            arrayOfSimFdnState[2] = REQUIRE_PUK2;
            arrayOfSimFdnState[3] = SIM_PERM_LOCKED;
        }
    }

    private static enum SimLockState
    {
        static
        {
            SimLockState[] arrayOfSimLockState = new SimLockState[4];
            arrayOfSimLockState[0] = NONE;
            arrayOfSimLockState[1] = REQUIRE_PIN;
            arrayOfSimLockState[2] = REQUIRE_PUK;
            arrayOfSimLockState[3] = SIM_PERM_LOCKED;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.test.SimulatedCommands
 * JD-Core Version:        0.6.2
 */