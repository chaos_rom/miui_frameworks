package com.android.internal.telephony.test;

import android.os.HandlerThread;
import android.os.Looper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.List;

public class ModelInterpreter
    implements Runnable, SimulatedRadioControl
{
    static final int CONNECTING_PAUSE_MSEC = 500;
    static final String LOG_TAG = "ModelInterpreter";
    static final int MAX_CALLS = 6;
    static final int PROGRESS_CALL_STATE = 1;
    static final String[][] sDefaultResponses = arrayOfString;;
    private String finalResponse;
    InputStream in;
    LineReader lineReader;
    HandlerThread mHandlerThread;
    OutputStream out;
    int pausedResponseCount;
    Object pausedResponseMonitor = new Object();
    SimulatedGsmCallState simulatedCallState;
    ServerSocket ss;

    static
    {
        String[][] arrayOfString; = new String[31][];
        String[] arrayOfString1 = new String[2];
        arrayOfString1[0] = "E0Q0V1";
        arrayOfString1[1] = null;
        arrayOfString;[0] = arrayOfString1;
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "+CMEE=2";
        arrayOfString2[1] = null;
        arrayOfString;[1] = arrayOfString2;
        String[] arrayOfString3 = new String[2];
        arrayOfString3[0] = "+CREG=2";
        arrayOfString3[1] = null;
        arrayOfString;[2] = arrayOfString3;
        String[] arrayOfString4 = new String[2];
        arrayOfString4[0] = "+CGREG=2";
        arrayOfString4[1] = null;
        arrayOfString;[3] = arrayOfString4;
        String[] arrayOfString5 = new String[2];
        arrayOfString5[0] = "+CCWA=1";
        arrayOfString5[1] = null;
        arrayOfString;[4] = arrayOfString5;
        String[] arrayOfString6 = new String[2];
        arrayOfString6[0] = "+COPS=0";
        arrayOfString6[1] = null;
        arrayOfString;[5] = arrayOfString6;
        String[] arrayOfString7 = new String[2];
        arrayOfString7[0] = "+CFUN=1";
        arrayOfString7[1] = null;
        arrayOfString;[6] = arrayOfString7;
        String[] arrayOfString8 = new String[2];
        arrayOfString8[0] = "+CGMI";
        arrayOfString8[1] = "+CGMI: Android Model AT Interpreter\r";
        arrayOfString;[7] = arrayOfString8;
        String[] arrayOfString9 = new String[2];
        arrayOfString9[0] = "+CGMM";
        arrayOfString9[1] = "+CGMM: Android Model AT Interpreter\r";
        arrayOfString;[8] = arrayOfString9;
        String[] arrayOfString10 = new String[2];
        arrayOfString10[0] = "+CGMR";
        arrayOfString10[1] = "+CGMR: 1.0\r";
        arrayOfString;[9] = arrayOfString10;
        String[] arrayOfString11 = new String[2];
        arrayOfString11[0] = "+CGSN";
        arrayOfString11[1] = "000000000000000\r";
        arrayOfString;[10] = arrayOfString11;
        String[] arrayOfString12 = new String[2];
        arrayOfString12[0] = "+CIMI";
        arrayOfString12[1] = "320720000000000\r";
        arrayOfString;[11] = arrayOfString12;
        String[] arrayOfString13 = new String[2];
        arrayOfString13[0] = "+CSCS=?";
        arrayOfString13[1] = "+CSCS: (\"HEX\",\"UCS2\")\r";
        arrayOfString;[12] = arrayOfString13;
        String[] arrayOfString14 = new String[2];
        arrayOfString14[0] = "+CFUN?";
        arrayOfString14[1] = "+CFUN: 1\r";
        arrayOfString;[13] = arrayOfString14;
        String[] arrayOfString15 = new String[2];
        arrayOfString15[0] = "+COPS=3,0;+COPS?;+COPS=3,1;+COPS?;+COPS=3,2;+COPS?";
        arrayOfString15[1] = "+COPS: 0,0,\"Android\"\r+COPS: 0,1,\"Android\"\r+COPS: 0,2,\"310995\"\r";
        arrayOfString;[14] = arrayOfString15;
        String[] arrayOfString16 = new String[2];
        arrayOfString16[0] = "+CREG?";
        arrayOfString16[1] = "+CREG: 2,5, \"0113\", \"6614\"\r";
        arrayOfString;[15] = arrayOfString16;
        String[] arrayOfString17 = new String[2];
        arrayOfString17[0] = "+CGREG?";
        arrayOfString17[1] = "+CGREG: 2,0\r";
        arrayOfString;[16] = arrayOfString17;
        String[] arrayOfString18 = new String[2];
        arrayOfString18[0] = "+CSQ";
        arrayOfString18[1] = "+CSQ: 16,99\r";
        arrayOfString;[17] = arrayOfString18;
        String[] arrayOfString19 = new String[2];
        arrayOfString19[0] = "+CNMI?";
        arrayOfString19[1] = "+CNMI: 1,2,2,1,1\r";
        arrayOfString;[18] = arrayOfString19;
        String[] arrayOfString20 = new String[2];
        arrayOfString20[0] = "+CLIR?";
        arrayOfString20[1] = "+CLIR: 1,3\r";
        arrayOfString;[19] = arrayOfString20;
        String[] arrayOfString21 = new String[2];
        arrayOfString21[0] = "%CPVWI=2";
        arrayOfString21[1] = "%CPVWI: 0\r";
        arrayOfString;[20] = arrayOfString21;
        String[] arrayOfString22 = new String[2];
        arrayOfString22[0] = "+CUSD=1,\"#646#\"";
        arrayOfString22[1] = "+CUSD=0,\"You have used 23 minutes\"\r";
        arrayOfString;[21] = arrayOfString22;
        String[] arrayOfString23 = new String[2];
        arrayOfString23[0] = "+CRSM=176,12258,0,0,10";
        arrayOfString23[1] = "+CRSM: 144,0,981062200050259429F6\r";
        arrayOfString;[22] = arrayOfString23;
        String[] arrayOfString24 = new String[2];
        arrayOfString24[0] = "+CRSM=192,12258,0,0,15";
        arrayOfString24[1] = "+CRSM: 144,0,0000000A2FE204000FF55501020000\r";
        arrayOfString;[23] = arrayOfString24;
        String[] arrayOfString25 = new String[2];
        arrayOfString25[0] = "+CRSM=192,28474,0,0,15";
        arrayOfString25[1] = "+CRSM: 144,0,0000005a6f3a040011f5220102011e\r";
        arrayOfString;[24] = arrayOfString25;
        String[] arrayOfString26 = new String[2];
        arrayOfString26[0] = "+CRSM=178,28474,1,4,30";
        arrayOfString26[1] = "+CRSM: 144,0,437573746f6d65722043617265ffffff07818100398799f7ffffffffffff\r";
        arrayOfString;[25] = arrayOfString26;
        String[] arrayOfString27 = new String[2];
        arrayOfString27[0] = "+CRSM=178,28474,2,4,30";
        arrayOfString27[1] = "+CRSM: 144,0,566f696365204d61696cffffffffffff07918150367742f3ffffffffffff\r";
        arrayOfString;[26] = arrayOfString27;
        String[] arrayOfString28 = new String[2];
        arrayOfString28[0] = "+CRSM=178,28474,3,4,30";
        arrayOfString28[1] = "+CRSM: 144,0,4164676a6dffffffffffffffffffffff0b918188551512c221436587ff01\r";
        arrayOfString;[27] = arrayOfString28;
        String[] arrayOfString29 = new String[2];
        arrayOfString29[0] = "+CRSM=178,28474,4,4,30";
        arrayOfString29[1] = "+CRSM: 144,0,810101c1ffffffffffffffffffffffff068114455245f8ffffffffffffff\r";
        arrayOfString;[28] = arrayOfString29;
        String[] arrayOfString30 = new String[2];
        arrayOfString30[0] = "+CRSM=192,28490,0,0,15";
        arrayOfString30[1] = "+CRSM: 144,0,000000416f4a040011f5550102010d\r";
        arrayOfString;[29] = arrayOfString30;
        String[] arrayOfString31 = new String[2];
        arrayOfString31[0] = "+CRSM=178,28490,1,4,13";
        arrayOfString31[1] = "+CRSM: 144,0,0206092143658709ffffffffff\r";
        arrayOfString;[30] = arrayOfString31;
    }

    public ModelInterpreter(InputStream paramInputStream, OutputStream paramOutputStream)
    {
        this.in = paramInputStream;
        this.out = paramOutputStream;
        init();
    }

    public ModelInterpreter(InetSocketAddress paramInetSocketAddress)
        throws IOException
    {
        this.ss = new ServerSocket();
        this.ss.setReuseAddress(true);
        this.ss.bind(paramInetSocketAddress);
        init();
    }

    private void conference()
        throws InterpreterEx
    {
        if (!this.simulatedCallState.conference())
            throw new InterpreterEx("ERROR");
    }

    private void init()
    {
        new Thread(this, "ModelInterpreter").start();
        this.mHandlerThread = new HandlerThread("ModelInterpreter");
        this.mHandlerThread.start();
        this.simulatedCallState = new SimulatedGsmCallState(this.mHandlerThread.getLooper());
    }

    private void onAnswer()
        throws InterpreterEx
    {
        if (!this.simulatedCallState.onAnswer())
            throw new InterpreterEx("ERROR");
    }

    private void onCHLD(String paramString)
        throws InterpreterEx
    {
        char c1 = '\000';
        char c2 = paramString.charAt(6);
        if (paramString.length() >= 8)
            c1 = paramString.charAt(7);
        if (!this.simulatedCallState.onChld(c2, c1))
            throw new InterpreterEx("ERROR");
    }

    private void onCLCC()
        throws InterpreterEx
    {
        List localList = this.simulatedCallState.getClccLines();
        int i = 0;
        int j = localList.size();
        while (i < j)
        {
            println((String)localList.get(i));
            i++;
        }
    }

    private void onDial(String paramString)
        throws InterpreterEx
    {
        if (!this.simulatedCallState.onDial(paramString.substring(1)))
            throw new InterpreterEx("ERROR");
    }

    private void onHangup()
        throws InterpreterEx
    {
        if (!this.simulatedCallState.onAnswer())
            throw new InterpreterEx("ERROR");
        this.finalResponse = "NO CARRIER";
    }

    private void onSMSSend(String paramString)
        throws InterpreterEx
    {
        print("> ");
        this.lineReader.getNextLineCtrlZ();
        println("+CMGS: 1");
    }

    private void releaseActiveAcceptHeldOrWaiting()
        throws InterpreterEx
    {
        if (!this.simulatedCallState.releaseActiveAcceptHeldOrWaiting())
            throw new InterpreterEx("ERROR");
    }

    private void releaseHeldOrUDUB()
        throws InterpreterEx
    {
        if (!this.simulatedCallState.releaseHeldOrUDUB())
            throw new InterpreterEx("ERROR");
    }

    private void separateCall(int paramInt)
        throws InterpreterEx
    {
        if (!this.simulatedCallState.separateCall(paramInt))
            throw new InterpreterEx("ERROR");
    }

    private void switchActiveAndHeldOrWaiting()
        throws InterpreterEx
    {
        if (!this.simulatedCallState.switchActiveAndHeldOrWaiting())
            throw new InterpreterEx("ERROR");
    }

    public void pauseResponses()
    {
        synchronized (this.pausedResponseMonitor)
        {
            this.pausedResponseCount = (1 + this.pausedResponseCount);
            return;
        }
    }

    void print(String paramString)
    {
        try
        {
            byte[] arrayOfByte = paramString.getBytes("US-ASCII");
            this.out.write(arrayOfByte);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                localIOException.printStackTrace();
        }
        finally
        {
        }
    }

    void println(String paramString)
    {
        try
        {
            byte[] arrayOfByte = paramString.getBytes("US-ASCII");
            this.out.write(arrayOfByte);
            this.out.write(13);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                localIOException.printStackTrace();
        }
        finally
        {
        }
    }

    void processLine(String paramString)
        throws InterpreterEx
    {
        String[] arrayOfString = splitCommands(paramString);
        int i = 0;
        if (i < arrayOfString.length)
        {
            String str1 = arrayOfString[i];
            if (str1.equals("A"))
                onAnswer();
            while (true)
            {
                i++;
                break;
                if (str1.equals("H"))
                {
                    onHangup();
                }
                else if (str1.startsWith("+CHLD="))
                {
                    onCHLD(str1);
                }
                else if (str1.equals("+CLCC"))
                {
                    onCLCC();
                }
                else if (str1.startsWith("D"))
                {
                    onDial(str1);
                }
                else
                {
                    if (!str1.startsWith("+CMGS="))
                        break label136;
                    onSMSSend(str1);
                }
            }
            label136: int j = 0;
            for (int k = 0; ; k++)
                if (k < sDefaultResponses.length)
                {
                    if (str1.equals(sDefaultResponses[k][0]))
                    {
                        String str2 = sDefaultResponses[k][1];
                        if (str2 != null)
                            println(str2);
                        j = 1;
                    }
                }
                else
                {
                    if (j != 0)
                        break;
                    throw new InterpreterEx("ERROR");
                }
        }
    }

    public void progressConnectingCallState()
    {
        this.simulatedCallState.progressConnectingCallState();
    }

    public void progressConnectingToActive()
    {
        this.simulatedCallState.progressConnectingToActive();
    }

    public void resumeResponses()
    {
        synchronized (this.pausedResponseMonitor)
        {
            this.pausedResponseCount = (-1 + this.pausedResponseCount);
            if (this.pausedResponseCount == 0)
                this.pausedResponseMonitor.notifyAll();
            return;
        }
    }

    // ERROR //
    public void run()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 176	com/android/internal/telephony/test/ModelInterpreter:ss	Ljava/net/ServerSocket;
        //     4: ifnull +39 -> 43
        //     7: aload_0
        //     8: getfield 176	com/android/internal/telephony/test/ModelInterpreter:ss	Ljava/net/ServerSocket;
        //     11: invokevirtual 371	java/net/ServerSocket:accept	()Ljava/net/Socket;
        //     14: astore 12
        //     16: aload_0
        //     17: aload 12
        //     19: invokevirtual 377	java/net/Socket:getInputStream	()Ljava/io/InputStream;
        //     22: putfield 163	com/android/internal/telephony/test/ModelInterpreter:in	Ljava/io/InputStream;
        //     25: aload_0
        //     26: aload 12
        //     28: invokevirtual 381	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
        //     31: putfield 165	com/android/internal/telephony/test/ModelInterpreter:out	Ljava/io/OutputStream;
        //     34: ldc 15
        //     36: ldc_w 383
        //     39: invokestatic 389	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     42: pop
        //     43: aload_0
        //     44: new 276	com/android/internal/telephony/test/LineReader
        //     47: dup
        //     48: aload_0
        //     49: getfield 163	com/android/internal/telephony/test/ModelInterpreter:in	Ljava/io/InputStream;
        //     52: invokespecial 392	com/android/internal/telephony/test/LineReader:<init>	(Ljava/io/InputStream;)V
        //     55: putfield 274	com/android/internal/telephony/test/ModelInterpreter:lineReader	Lcom/android/internal/telephony/test/LineReader;
        //     58: aload_0
        //     59: ldc_w 394
        //     62: invokevirtual 253	com/android/internal/telephony/test/ModelInterpreter:println	(Ljava/lang/String;)V
        //     65: aload_0
        //     66: getfield 274	com/android/internal/telephony/test/ModelInterpreter:lineReader	Lcom/android/internal/telephony/test/LineReader;
        //     69: invokevirtual 397	com/android/internal/telephony/test/LineReader:getNextLine	()Ljava/lang/String;
        //     72: astore_1
        //     73: aload_1
        //     74: ifnonnull +52 -> 126
        //     77: ldc 15
        //     79: ldc_w 399
        //     82: invokestatic 389	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     85: pop
        //     86: aload_0
        //     87: getfield 176	com/android/internal/telephony/test/ModelInterpreter:ss	Ljava/net/ServerSocket;
        //     90: ifnonnull -90 -> 0
        //     93: return
        //     94: astore 10
        //     96: ldc 15
        //     98: ldc_w 401
        //     101: aload 10
        //     103: invokestatic 405	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     106: pop
        //     107: goto -14 -> 93
        //     110: astore 13
        //     112: ldc 15
        //     114: ldc_w 407
        //     117: aload 13
        //     119: invokestatic 405	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     122: pop
        //     123: goto -123 -> 0
        //     126: aload_0
        //     127: getfield 161	com/android/internal/telephony/test/ModelInterpreter:pausedResponseMonitor	Ljava/lang/Object;
        //     130: astore_2
        //     131: aload_2
        //     132: monitorenter
        //     133: aload_0
        //     134: getfield 299	com/android/internal/telephony/test/ModelInterpreter:pausedResponseCount	I
        //     137: istore 4
        //     139: iload 4
        //     141: ifle +18 -> 159
        //     144: aload_0
        //     145: getfield 161	com/android/internal/telephony/test/ModelInterpreter:pausedResponseMonitor	Ljava/lang/Object;
        //     148: invokevirtual 410	java/lang/Object:wait	()V
        //     151: goto -18 -> 133
        //     154: astore 8
        //     156: goto -23 -> 133
        //     159: aload_2
        //     160: monitorexit
        //     161: aload_0
        //     162: monitorenter
        //     163: aload_0
        //     164: ldc_w 412
        //     167: putfield 266	com/android/internal/telephony/test/ModelInterpreter:finalResponse	Ljava/lang/String;
        //     170: aload_0
        //     171: aload_1
        //     172: invokevirtual 414	com/android/internal/telephony/test/ModelInterpreter:processLine	(Ljava/lang/String;)V
        //     175: aload_0
        //     176: aload_0
        //     177: getfield 266	com/android/internal/telephony/test/ModelInterpreter:finalResponse	Ljava/lang/String;
        //     180: invokevirtual 253	com/android/internal/telephony/test/ModelInterpreter:println	(Ljava/lang/String;)V
        //     183: aload_0
        //     184: monitorexit
        //     185: goto -120 -> 65
        //     188: astore 6
        //     190: aload_0
        //     191: monitorexit
        //     192: aload 6
        //     194: athrow
        //     195: astore_3
        //     196: aload_2
        //     197: monitorexit
        //     198: aload_3
        //     199: athrow
        //     200: astore 7
        //     202: aload_0
        //     203: aload 7
        //     205: getfield 417	com/android/internal/telephony/test/InterpreterEx:result	Ljava/lang/String;
        //     208: invokevirtual 253	com/android/internal/telephony/test/ModelInterpreter:println	(Ljava/lang/String;)V
        //     211: goto -28 -> 183
        //     214: astore 5
        //     216: aload 5
        //     218: invokevirtual 418	java/lang/RuntimeException:printStackTrace	()V
        //     221: aload_0
        //     222: ldc 196
        //     224: invokevirtual 253	com/android/internal/telephony/test/ModelInterpreter:println	(Ljava/lang/String;)V
        //     227: goto -44 -> 183
        //
        // Exception table:
        //     from	to	target	type
        //     7	16	94	java/io/IOException
        //     16	34	110	java/io/IOException
        //     144	151	154	java/lang/InterruptedException
        //     163	183	188	finally
        //     183	192	188	finally
        //     202	227	188	finally
        //     133	139	195	finally
        //     144	151	195	finally
        //     159	161	195	finally
        //     196	198	195	finally
        //     163	183	200	com/android/internal/telephony/test/InterpreterEx
        //     163	183	214	java/lang/RuntimeException
    }

    public void sendUnsolicited(String paramString)
    {
        try
        {
            println(paramString);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setAutoProgressConnectingCall(boolean paramBoolean)
    {
        this.simulatedCallState.setAutoProgressConnectingCall(paramBoolean);
    }

    public void setNextCallFailCause(int paramInt)
    {
    }

    public void setNextDialFailImmediately(boolean paramBoolean)
    {
        this.simulatedCallState.setNextDialFailImmediately(paramBoolean);
    }

    public void shutdown()
    {
        Looper localLooper = this.mHandlerThread.getLooper();
        if (localLooper != null)
            localLooper.quit();
        try
        {
            this.in.close();
            try
            {
                label23: this.out.close();
                label30: return;
            }
            catch (IOException localIOException2)
            {
                break label30;
            }
        }
        catch (IOException localIOException1)
        {
            break label23;
        }
    }

    String[] splitCommands(String paramString)
        throws InterpreterEx
    {
        if (!paramString.startsWith("AT"))
            throw new InterpreterEx("ERROR");
        String[] arrayOfString;
        if (paramString.length() == 2)
            arrayOfString = new String[0];
        while (true)
        {
            return arrayOfString;
            arrayOfString = new String[1];
            arrayOfString[0] = paramString.substring(2);
        }
    }

    public void triggerHangupAll()
    {
        if (this.simulatedCallState.triggerHangupAll())
            println("NO CARRIER");
    }

    public void triggerHangupBackground()
    {
        if (this.simulatedCallState.triggerHangupBackground())
            println("NO CARRIER");
    }

    public void triggerHangupForeground()
    {
        if (this.simulatedCallState.triggerHangupForeground())
            println("NO CARRIER");
    }

    public void triggerIncomingSMS(String paramString)
    {
    }

    public void triggerIncomingUssd(String paramString1, String paramString2)
    {
    }

    public void triggerRing(String paramString)
    {
        try
        {
            if (this.simulatedCallState.triggerRing(paramString))
                println("RING");
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void triggerSsn(int paramInt1, int paramInt2)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.test.ModelInterpreter
 * JD-Core Version:        0.6.2
 */