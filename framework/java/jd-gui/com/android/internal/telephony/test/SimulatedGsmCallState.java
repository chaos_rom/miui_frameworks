package com.android.internal.telephony.test;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import com.android.internal.telephony.DriverCall;
import java.util.ArrayList;
import java.util.List;

class SimulatedGsmCallState extends Handler
{
    static final int CONNECTING_PAUSE_MSEC = 500;
    static final int EVENT_PROGRESS_CALL_STATE = 1;
    static final int MAX_CALLS = 7;
    private boolean autoProgressConnecting = true;
    CallInfo[] calls = new CallInfo[7];
    private boolean nextDialFailImmediately;

    public SimulatedGsmCallState(Looper paramLooper)
    {
        super(paramLooper);
    }

    private int countActiveLines()
        throws InvalidStateEx
    {
        boolean bool1 = false;
        int i = 0;
        int j = 0;
        boolean bool2 = false;
        boolean bool3 = false;
        int k = 0;
        int m = 0;
        if (m < this.calls.length)
        {
            CallInfo localCallInfo = this.calls[m];
            label68: int i1;
            if (localCallInfo != null)
            {
                if ((bool1) || (!localCallInfo.isMpty))
                    break label146;
                if (localCallInfo.state != CallInfo.State.HOLDING)
                    break label140;
                k = 1;
                bool1 |= localCallInfo.isMpty;
                if (localCallInfo.state != CallInfo.State.HOLDING)
                    break label230;
                i1 = 1;
                label90: i |= i1;
                if (localCallInfo.state != CallInfo.State.ACTIVE)
                    break label236;
            }
            label140: label146: label230: label236: for (int i2 = 1; ; i2 = 0)
            {
                j |= i2;
                bool2 |= localCallInfo.isConnecting();
                bool3 |= localCallInfo.isRinging();
                m++;
                break;
                k = 0;
                break label68;
                if ((localCallInfo.isMpty) && (k != 0) && (localCallInfo.state == CallInfo.State.ACTIVE))
                {
                    Log.e("ModelInterpreter", "Invalid state");
                    throw new InvalidStateEx();
                }
                if ((localCallInfo.isMpty) || (!bool1) || (k == 0) || (localCallInfo.state != CallInfo.State.HOLDING))
                    break label68;
                Log.e("ModelInterpreter", "Invalid state");
                throw new InvalidStateEx();
                i1 = 0;
                break label90;
            }
        }
        int n = 0;
        if (i != 0)
            n = 0 + 1;
        if (j != 0)
            n++;
        if (bool2)
            n++;
        if (bool3)
            n++;
        return n;
    }

    public boolean conference()
    {
        boolean bool = true;
        int i = 0;
        int j = 0;
        if (j < this.calls.length)
        {
            CallInfo localCallInfo2 = this.calls[j];
            if (localCallInfo2 != null)
            {
                i++;
                if (localCallInfo2.isConnecting())
                    bool = false;
            }
        }
        while (true)
        {
            return bool;
            j++;
            break;
            for (int k = 0; k < this.calls.length; k++)
            {
                CallInfo localCallInfo1 = this.calls[k];
                if (localCallInfo1 != null)
                {
                    localCallInfo1.state = CallInfo.State.ACTIVE;
                    if (i > 0)
                        localCallInfo1.isMpty = bool;
                }
            }
        }
    }

    public boolean explicitCallTransfer()
    {
        int i = 0;
        int j = 0;
        if (j < this.calls.length)
        {
            CallInfo localCallInfo = this.calls[j];
            if (localCallInfo != null)
            {
                i++;
                if (!localCallInfo.isConnecting());
            }
        }
        for (boolean bool = false; ; bool = triggerHangupAll())
        {
            return bool;
            j++;
            break;
        }
    }

    public List<String> getClccLines()
    {
        ArrayList localArrayList = new ArrayList(this.calls.length);
        for (int i = 0; i < this.calls.length; i++)
        {
            CallInfo localCallInfo = this.calls[i];
            if (localCallInfo != null)
                localArrayList.add(localCallInfo.toCLCCLine(i + 1));
        }
        return localArrayList;
    }

    public List<DriverCall> getDriverCalls()
    {
        ArrayList localArrayList = new ArrayList(this.calls.length);
        for (int i = 0; i < this.calls.length; i++)
        {
            CallInfo localCallInfo = this.calls[i];
            if (localCallInfo != null)
                localArrayList.add(localCallInfo.toDriverCall(i + 1));
        }
        Log.d("GSM", "SC< getDriverCalls " + localArrayList);
        return localArrayList;
    }

    public void handleMessage(Message paramMessage)
    {
        try
        {
            switch (paramMessage.what)
            {
            default:
                return;
            case 1:
            }
            progressConnectingCallState();
        }
        finally
        {
        }
    }

    public boolean onAnswer()
    {
        for (int i = 0; ; i++)
        {
            boolean bool;
            try
            {
                if (i < this.calls.length)
                {
                    CallInfo localCallInfo = this.calls[i];
                    if ((localCallInfo == null) || ((localCallInfo.state != CallInfo.State.INCOMING) && (localCallInfo.state != CallInfo.State.WAITING)))
                        continue;
                    bool = switchActiveAndHeldOrWaiting();
                }
                else
                {
                    bool = false;
                }
            }
            finally
            {
            }
        }
    }

    public boolean onChld(char paramChar1, char paramChar2)
    {
        int i = 0;
        boolean bool;
        if (paramChar2 != 0)
        {
            i = paramChar2 + '\0)7';
            if ((i < 0) || (i >= this.calls.length))
                bool = false;
        }
        while (true)
        {
            return bool;
            switch (paramChar1)
            {
            default:
                bool = false;
                break;
            case '0':
                bool = releaseHeldOrUDUB();
                break;
            case '1':
                if (paramChar2 <= 0)
                {
                    bool = releaseActiveAcceptHeldOrWaiting();
                }
                else if (this.calls[i] == null)
                {
                    bool = false;
                }
                else
                {
                    this.calls[i] = null;
                    bool = true;
                }
                break;
            case '2':
                if (paramChar2 <= 0)
                    bool = switchActiveAndHeldOrWaiting();
                else
                    bool = separateCall(i);
                break;
            case '3':
                bool = conference();
                break;
            case '4':
                bool = explicitCallTransfer();
                break;
            case '5':
                bool = false;
            }
        }
    }

    public boolean onDial(String paramString)
    {
        boolean bool = false;
        int i = -1;
        Log.d("GSM", "SC> dial '" + paramString + "'");
        if (this.nextDialFailImmediately)
        {
            this.nextDialFailImmediately = false;
            Log.d("GSM", "SC< dial fail (per request)");
        }
        while (true)
        {
            return bool;
            String str = PhoneNumberUtils.extractNetworkPortion(paramString);
            if (str.length() == 0)
            {
                Log.d("GSM", "SC< dial fail (invalid ph num)");
            }
            else if ((str.startsWith("*99")) && (str.endsWith("#")))
            {
                Log.d("GSM", "SC< dial ignored (gprs)");
                bool = true;
            }
            else
            {
                try
                {
                    if (countActiveLines() <= 1)
                        break label147;
                    Log.d("GSM", "SC< dial fail (invalid call state)");
                }
                catch (InvalidStateEx localInvalidStateEx)
                {
                    Log.d("GSM", "SC< dial fail (invalid call state)");
                }
                continue;
                label147: for (int j = 0; ; j++)
                {
                    if (j >= this.calls.length)
                        break label256;
                    if ((i < 0) && (this.calls[j] == null))
                        i = j;
                    if ((this.calls[j] != null) && (!this.calls[j].isActiveOrHeld()))
                    {
                        Log.d("GSM", "SC< dial fail (invalid call state)");
                        break;
                    }
                    if ((this.calls[j] != null) && (this.calls[j].state == CallInfo.State.ACTIVE))
                        this.calls[j].state = CallInfo.State.HOLDING;
                }
                label256: if (i < 0)
                {
                    Log.d("GSM", "SC< dial fail (invalid call state)");
                }
                else
                {
                    this.calls[i] = CallInfo.createOutgoingCall(str);
                    if (this.autoProgressConnecting)
                        sendMessageDelayed(obtainMessage(1, this.calls[i]), 500L);
                    Log.d("GSM", "SC< dial (slot = " + i + ")");
                    bool = true;
                }
            }
        }
    }

    public boolean onHangup()
    {
        boolean bool = false;
        for (int i = 0; i < this.calls.length; i++)
        {
            CallInfo localCallInfo = this.calls[i];
            if ((localCallInfo != null) && (localCallInfo.state != CallInfo.State.WAITING))
            {
                this.calls[i] = null;
                bool = true;
            }
        }
        return bool;
    }

    public void progressConnectingCallState()
    {
        int i = 0;
        while (true)
            try
            {
                CallInfo localCallInfo;
                if (i < this.calls.length)
                {
                    localCallInfo = this.calls[i];
                    if ((localCallInfo == null) || (localCallInfo.state != CallInfo.State.DIALING))
                        continue;
                    localCallInfo.state = CallInfo.State.ALERTING;
                    if (this.autoProgressConnecting)
                        sendMessageDelayed(obtainMessage(1, localCallInfo), 500L);
                }
                return;
                if (localCallInfo != null)
                {
                    if (localCallInfo.state != CallInfo.State.ALERTING)
                        continue;
                    localCallInfo.state = CallInfo.State.ACTIVE;
                }
            }
            finally
            {
            }
    }

    public void progressConnectingToActive()
    {
        for (int i = 0; ; i++)
            try
            {
                if (i < this.calls.length)
                {
                    CallInfo localCallInfo = this.calls[i];
                    if ((localCallInfo != null) && ((localCallInfo.state == CallInfo.State.DIALING) || (localCallInfo.state == CallInfo.State.ALERTING)))
                        localCallInfo.state = CallInfo.State.ACTIVE;
                }
                else
                {
                    return;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
    }

    public boolean releaseActiveAcceptHeldOrWaiting()
    {
        int i = 0;
        int j = 0;
        for (int k = 0; k < this.calls.length; k++)
        {
            CallInfo localCallInfo4 = this.calls[k];
            if ((localCallInfo4 != null) && (localCallInfo4.state == CallInfo.State.ACTIVE))
            {
                this.calls[k] = null;
                j = 1;
            }
        }
        if (j == 0)
            for (int i1 = 0; i1 < this.calls.length; i1++)
            {
                CallInfo localCallInfo3 = this.calls[i1];
                if ((localCallInfo3 != null) && ((localCallInfo3.state == CallInfo.State.DIALING) || (localCallInfo3.state == CallInfo.State.ALERTING)))
                    this.calls[i1] = null;
            }
        for (int m = 0; m < this.calls.length; m++)
        {
            CallInfo localCallInfo2 = this.calls[m];
            if ((localCallInfo2 != null) && (localCallInfo2.state == CallInfo.State.HOLDING))
            {
                localCallInfo2.state = CallInfo.State.ACTIVE;
                i = 1;
            }
        }
        if (i != 0);
        label231: 
        while (true)
        {
            return true;
            for (int n = 0; ; n++)
            {
                if (n >= this.calls.length)
                    break label231;
                CallInfo localCallInfo1 = this.calls[n];
                if ((localCallInfo1 != null) && (localCallInfo1.isRinging()))
                {
                    localCallInfo1.state = CallInfo.State.ACTIVE;
                    break;
                }
            }
        }
    }

    public boolean releaseHeldOrUDUB()
    {
        int i = 0;
        for (int j = 0; ; j++)
            if (j < this.calls.length)
            {
                CallInfo localCallInfo2 = this.calls[j];
                if ((localCallInfo2 != null) && (localCallInfo2.isRinging()))
                {
                    i = 1;
                    this.calls[j] = null;
                }
            }
            else
            {
                if (i != 0)
                    break;
                for (int k = 0; k < this.calls.length; k++)
                {
                    CallInfo localCallInfo1 = this.calls[k];
                    if ((localCallInfo1 != null) && (localCallInfo1.state == CallInfo.State.HOLDING))
                        this.calls[k] = null;
                }
            }
        return true;
    }

    public boolean separateCall(int paramInt)
    {
        int i = 1;
        try
        {
            CallInfo localCallInfo1 = this.calls[paramInt];
            if ((localCallInfo1 != null) && (!localCallInfo1.isConnecting()) && (countActiveLines() == i))
            {
                localCallInfo1.state = CallInfo.State.ACTIVE;
                localCallInfo1.isMpty = false;
                for (int j = 0; j < this.calls.length; j++)
                {
                    int k = 0;
                    int m = 0;
                    if (j != paramInt)
                    {
                        CallInfo localCallInfo2 = this.calls[j];
                        if ((localCallInfo2 != null) && (localCallInfo2.state == CallInfo.State.ACTIVE))
                        {
                            localCallInfo2.state = CallInfo.State.HOLDING;
                            k = 0 + 1;
                            m = j;
                        }
                    }
                    if (k == i)
                        this.calls[m].isMpty = false;
                }
            }
        }
        catch (InvalidStateEx localInvalidStateEx)
        {
            i = 0;
            break label146;
            i = 0;
        }
        label146: return i;
    }

    public void setAutoProgressConnectingCall(boolean paramBoolean)
    {
        this.autoProgressConnecting = paramBoolean;
    }

    public void setNextDialFailImmediately(boolean paramBoolean)
    {
        this.nextDialFailImmediately = paramBoolean;
    }

    public boolean switchActiveAndHeldOrWaiting()
    {
        int i = 0;
        int j = 0;
        int k;
        label41: CallInfo localCallInfo1;
        if (j < this.calls.length)
        {
            CallInfo localCallInfo2 = this.calls[j];
            if ((localCallInfo2 != null) && (localCallInfo2.state == CallInfo.State.HOLDING))
                i = 1;
        }
        else
        {
            k = 0;
            if (k >= this.calls.length)
                break label139;
            localCallInfo1 = this.calls[k];
            if (localCallInfo1 != null)
            {
                if (localCallInfo1.state != CallInfo.State.ACTIVE)
                    break label94;
                localCallInfo1.state = CallInfo.State.HOLDING;
            }
        }
        while (true)
        {
            k++;
            break label41;
            j++;
            break;
            label94: if (localCallInfo1.state == CallInfo.State.HOLDING)
                localCallInfo1.state = CallInfo.State.ACTIVE;
            else if ((i == 0) && (localCallInfo1.isRinging()))
                localCallInfo1.state = CallInfo.State.ACTIVE;
        }
        label139: return true;
    }

    public boolean triggerHangupAll()
    {
        boolean bool = false;
        int i = 0;
        try
        {
            while (i < this.calls.length)
            {
                this.calls[i];
                if (this.calls[i] != null)
                    bool = true;
                this.calls[i] = null;
                i++;
            }
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean triggerHangupBackground()
    {
        boolean bool = false;
        for (int i = 0; ; i++)
            try
            {
                if (i < this.calls.length)
                {
                    CallInfo localCallInfo = this.calls[i];
                    if ((localCallInfo != null) && (localCallInfo.state == CallInfo.State.HOLDING))
                    {
                        this.calls[i] = null;
                        bool = true;
                    }
                }
                else
                {
                    return bool;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
    }

    public boolean triggerHangupForeground()
    {
        boolean bool = false;
        int i = 0;
        while (true)
        {
            try
            {
                if (i >= this.calls.length)
                    break label147;
                CallInfo localCallInfo2 = this.calls[i];
                if ((localCallInfo2 != null) && ((localCallInfo2.state == CallInfo.State.INCOMING) || (localCallInfo2.state == CallInfo.State.WAITING)))
                {
                    this.calls[i] = null;
                    bool = true;
                    break label141;
                    if (j < this.calls.length)
                    {
                        CallInfo localCallInfo1 = this.calls[j];
                        if ((localCallInfo1 == null) || ((localCallInfo1.state != CallInfo.State.DIALING) && (localCallInfo1.state != CallInfo.State.ACTIVE) && (localCallInfo1.state != CallInfo.State.ALERTING)))
                            break label153;
                        this.calls[j] = null;
                        bool = true;
                        break label153;
                    }
                    return bool;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            label141: i++;
            continue;
            label147: int j = 0;
            continue;
            label153: j++;
        }
    }

    public boolean triggerRing(String paramString)
    {
        boolean bool = false;
        int i = -1;
        int j = 0;
        int k = 0;
        while (true)
        {
            CallInfo localCallInfo;
            try
            {
                if (k < this.calls.length)
                {
                    localCallInfo = this.calls[k];
                    if ((localCallInfo == null) && (i < 0))
                    {
                        i = k;
                    }
                    else
                    {
                        if ((localCallInfo == null) || ((localCallInfo.state != CallInfo.State.INCOMING) && (localCallInfo.state != CallInfo.State.WAITING)))
                            break label156;
                        Log.w("ModelInterpreter", "triggerRing failed; phone already ringing");
                        continue;
                    }
                }
                else if (i < 0)
                {
                    Log.w("ModelInterpreter", "triggerRing failed; all full");
                }
            }
            finally
            {
                throw localObject;
                this.calls[i] = CallInfo.createIncomingCall(PhoneNumberUtils.extractNetworkPortion(paramString));
                if (j != 0)
                    this.calls[i].state = CallInfo.State.WAITING;
                bool = true;
                continue;
                k++;
            }
            label156: if (localCallInfo != null)
                j = 1;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.test.SimulatedGsmCallState
 * JD-Core Version:        0.6.2
 */