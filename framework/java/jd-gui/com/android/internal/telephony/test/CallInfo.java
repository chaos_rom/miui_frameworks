package com.android.internal.telephony.test;

import com.android.internal.telephony.ATParseEx;
import com.android.internal.telephony.DriverCall;

class CallInfo
{
    int TOA;
    boolean isMT;
    boolean isMpty;
    String number;
    State state;

    CallInfo(boolean paramBoolean1, State paramState, boolean paramBoolean2, String paramString)
    {
        this.isMT = paramBoolean1;
        this.state = paramState;
        this.isMpty = paramBoolean2;
        this.number = paramString;
        if ((paramString.length() > 0) && (paramString.charAt(0) == '+'));
        for (this.TOA = 145; ; this.TOA = 129)
            return;
    }

    static CallInfo createIncomingCall(String paramString)
    {
        return new CallInfo(true, State.INCOMING, false, paramString);
    }

    static CallInfo createOutgoingCall(String paramString)
    {
        return new CallInfo(false, State.DIALING, false, paramString);
    }

    boolean isActiveOrHeld()
    {
        if ((this.state == State.ACTIVE) || (this.state == State.HOLDING));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isConnecting()
    {
        if ((this.state == State.DIALING) || (this.state == State.ALERTING));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isRinging()
    {
        if ((this.state == State.INCOMING) || (this.state == State.WAITING));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    String toCLCCLine(int paramInt)
    {
        StringBuilder localStringBuilder1 = new StringBuilder().append("+CLCC: ").append(paramInt).append(",");
        String str1;
        StringBuilder localStringBuilder2;
        if (this.isMT)
        {
            str1 = "1";
            localStringBuilder2 = localStringBuilder1.append(str1).append(",").append(this.state.value()).append(",0,");
            if (!this.isMpty)
                break label111;
        }
        label111: for (String str2 = "1"; ; str2 = "0")
        {
            return str2 + ",\"" + this.number + "\"," + this.TOA;
            str1 = "0";
            break;
        }
    }

    DriverCall toDriverCall(int paramInt)
    {
        DriverCall localDriverCall = new DriverCall();
        localDriverCall.index = paramInt;
        localDriverCall.isMT = this.isMT;
        try
        {
            localDriverCall.state = DriverCall.stateFromCLCC(this.state.value());
            localDriverCall.isMpty = this.isMpty;
            localDriverCall.number = this.number;
            localDriverCall.TOA = this.TOA;
            localDriverCall.isVoice = true;
            localDriverCall.als = 0;
            return localDriverCall;
        }
        catch (ATParseEx localATParseEx)
        {
            throw new RuntimeException("should never happen", localATParseEx);
        }
    }

    static enum State
    {
        private final int value;

        static
        {
            DIALING = new State("DIALING", 2, 2);
            ALERTING = new State("ALERTING", 3, 3);
            INCOMING = new State("INCOMING", 4, 4);
            WAITING = new State("WAITING", 5, 5);
            State[] arrayOfState = new State[6];
            arrayOfState[0] = ACTIVE;
            arrayOfState[1] = HOLDING;
            arrayOfState[2] = DIALING;
            arrayOfState[3] = ALERTING;
            arrayOfState[4] = INCOMING;
            arrayOfState[5] = WAITING;
        }

        private State(int paramInt)
        {
            this.value = paramInt;
        }

        public int value()
        {
            return this.value;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.test.CallInfo
 * JD-Core Version:        0.6.2
 */