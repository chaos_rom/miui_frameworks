package com.android.internal.telephony;

import android.app.PendingIntent;
import android.content.Context;
import android.util.Log;
import com.android.internal.util.HexDump;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class IccSmsInterfaceManager extends ISms.Stub
{
    protected Context mContext;
    protected SMSDispatcher mDispatcher;
    protected PhoneBase mPhone;

    protected IccSmsInterfaceManager(PhoneBase paramPhoneBase)
    {
        this.mPhone = paramPhoneBase;
        this.mContext = paramPhoneBase.getContext();
    }

    protected ArrayList<SmsRawData> buildValidRawData(ArrayList<byte[]> paramArrayList)
    {
        int i = paramArrayList.size();
        ArrayList localArrayList = new ArrayList(i);
        int j = 0;
        if (j < i)
        {
            if (((byte[])paramArrayList.get(j))[0] == 0)
                localArrayList.add(null);
            while (true)
            {
                j++;
                break;
                localArrayList.add(new SmsRawData((byte[])paramArrayList.get(j)));
            }
        }
        return localArrayList;
    }

    protected void enforceReceiveAndSend(String paramString)
    {
        this.mContext.enforceCallingPermission("android.permission.RECEIVE_SMS", paramString);
        this.mContext.enforceCallingPermission("android.permission.SEND_SMS", paramString);
    }

    protected abstract void log(String paramString);

    protected byte[] makeSmsRecordData(int paramInt, byte[] paramArrayOfByte)
    {
        byte[] arrayOfByte = new byte['°'];
        arrayOfByte[0] = ((byte)(paramInt & 0x7));
        System.arraycopy(paramArrayOfByte, 0, arrayOfByte, 1, paramArrayOfByte.length);
        for (int i = 1 + paramArrayOfByte.length; i < 176; i++)
            arrayOfByte[i] = -1;
        return arrayOfByte;
    }

    public void sendData(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfByte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        this.mPhone.getContext().enforceCallingPermission("android.permission.SEND_SMS", "Sending SMS message");
        if (Log.isLoggable("SMS", 2))
            log("sendData: destAddr=" + paramString1 + " scAddr=" + paramString2 + " destPort=" + paramInt + " data='" + HexDump.toHexString(paramArrayOfByte) + "' sentIntent=" + paramPendingIntent1 + " deliveryIntent=" + paramPendingIntent2);
        this.mDispatcher.sendData(paramString1, paramString2, paramInt, paramArrayOfByte, paramPendingIntent1, paramPendingIntent2);
    }

    public void sendMultipartText(String paramString1, String paramString2, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2)
    {
        this.mPhone.getContext().enforceCallingPermission("android.permission.SEND_SMS", "Sending SMS message");
        if (Log.isLoggable("SMS", 2))
        {
            int i = 0;
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                StringBuilder localStringBuilder = new StringBuilder().append("sendMultipartText: destAddr=").append(paramString1).append(", srAddr=").append(paramString2).append(", part[");
                int j = i + 1;
                log(i + "]=" + str);
                i = j;
            }
        }
        this.mDispatcher.sendMultipartText(paramString1, paramString2, (ArrayList)paramList, (ArrayList)paramList1, (ArrayList)paramList2);
    }

    public void sendText(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
    {
        this.mPhone.getContext().enforceCallingPermission("android.permission.SEND_SMS", "Sending SMS message");
        if (Log.isLoggable("SMS", 2))
            log("sendText: destAddr=" + paramString1 + " scAddr=" + paramString2 + " text='" + paramString3 + "' sentIntent=" + paramPendingIntent1 + " deliveryIntent=" + paramPendingIntent2);
        this.mDispatcher.sendText(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccSmsInterfaceManager
 * JD-Core Version:        0.6.2
 */