package com.android.internal.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public final class SmsStorageMonitor extends Handler
{
    private static final int EVENT_ICC_FULL = 1;
    private static final int EVENT_RADIO_ON = 3;
    private static final int EVENT_REPORT_MEMORY_STATUS_DONE = 2;
    private static final String TAG = "SmsStorageMonitor";
    private static final int WAKE_LOCK_TIMEOUT = 5000;
    final CommandsInterface mCm;
    private final Context mContext;
    private boolean mReportMemoryStatusPending;
    private final BroadcastReceiver mResultReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (paramAnonymousIntent.getAction().equals("android.intent.action.DEVICE_STORAGE_FULL"))
            {
                SmsStorageMonitor.this.mStorageAvailable = false;
                SmsStorageMonitor.this.mCm.reportSmsMemoryStatus(false, SmsStorageMonitor.this.obtainMessage(2));
            }
            while (true)
            {
                return;
                if (paramAnonymousIntent.getAction().equals("android.intent.action.DEVICE_STORAGE_NOT_FULL"))
                {
                    SmsStorageMonitor.this.mStorageAvailable = true;
                    SmsStorageMonitor.this.mCm.reportSmsMemoryStatus(true, SmsStorageMonitor.this.obtainMessage(2));
                }
            }
        }
    };
    boolean mStorageAvailable = true;
    private PowerManager.WakeLock mWakeLock;

    public SmsStorageMonitor(PhoneBase paramPhoneBase)
    {
        this.mContext = paramPhoneBase.getContext();
        this.mCm = paramPhoneBase.mCM;
        createWakelock();
        this.mCm.setOnIccSmsFull(this, 1, null);
        this.mCm.registerForOn(this, 3, null);
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.DEVICE_STORAGE_FULL");
        localIntentFilter.addAction("android.intent.action.DEVICE_STORAGE_NOT_FULL");
        this.mContext.registerReceiver(this.mResultReceiver, localIntentFilter);
    }

    private void createWakelock()
    {
        this.mWakeLock = ((PowerManager)this.mContext.getSystemService("power")).newWakeLock(1, "SmsStorageMonitor");
        this.mWakeLock.setReferenceCounted(true);
    }

    private void handleIccFull()
    {
        Intent localIntent = new Intent("android.provider.Telephony.SIM_FULL");
        this.mWakeLock.acquire(5000L);
        this.mContext.sendBroadcast(localIntent, "android.permission.RECEIVE_SMS");
    }

    public void dispose()
    {
        this.mCm.unSetOnIccSmsFull(this);
        this.mCm.unregisterForOn(this);
        this.mContext.unregisterReceiver(this.mResultReceiver);
    }

    public void handleMessage(Message paramMessage)
    {
        switch (paramMessage.what)
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            handleIccFull();
            continue;
            if (((AsyncResult)paramMessage.obj).exception != null)
            {
                this.mReportMemoryStatusPending = true;
                Log.v("SmsStorageMonitor", "Memory status report to modem pending : mStorageAvailable = " + this.mStorageAvailable);
            }
            else
            {
                this.mReportMemoryStatusPending = false;
                continue;
                if (this.mReportMemoryStatusPending)
                {
                    Log.v("SmsStorageMonitor", "Sending pending memory status report : mStorageAvailable = " + this.mStorageAvailable);
                    this.mCm.reportSmsMemoryStatus(this.mStorageAvailable, obtainMessage(2));
                }
            }
        }
    }

    public boolean isStorageAvailable()
    {
        return this.mStorageAvailable;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.SmsStorageMonitor
 * JD-Core Version:        0.6.2
 */