package com.android.internal.telephony;

public class IccCardApplication
{
    public String aid;
    public String app_label;
    public AppState app_state;
    public AppType app_type;
    public PersoSubState perso_substate;
    public IccCardStatus.PinState pin1;
    public int pin1_replaced;
    public IccCardStatus.PinState pin2;

    AppState AppStateFromRILInt(int paramInt)
    {
        AppState localAppState;
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Unrecognized RIL_AppState: " + paramInt);
        case 0:
            localAppState = AppState.APPSTATE_UNKNOWN;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return localAppState;
            localAppState = AppState.APPSTATE_DETECTED;
            continue;
            localAppState = AppState.APPSTATE_PIN;
            continue;
            localAppState = AppState.APPSTATE_PUK;
            continue;
            localAppState = AppState.APPSTATE_SUBSCRIPTION_PERSO;
            continue;
            localAppState = AppState.APPSTATE_READY;
        }
    }

    AppType AppTypeFromRILInt(int paramInt)
    {
        AppType localAppType;
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Unrecognized RIL_AppType: " + paramInt);
        case 0:
            localAppType = AppType.APPTYPE_UNKNOWN;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return localAppType;
            localAppType = AppType.APPTYPE_SIM;
            continue;
            localAppType = AppType.APPTYPE_USIM;
            continue;
            localAppType = AppType.APPTYPE_RUIM;
            continue;
            localAppType = AppType.APPTYPE_CSIM;
            continue;
            localAppType = AppType.APPTYPE_ISIM;
        }
    }

    PersoSubState PersoSubstateFromRILInt(int paramInt)
    {
        PersoSubState localPersoSubState;
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Unrecognized RIL_PersoSubstate: " + paramInt);
        case 0:
            localPersoSubState = PersoSubState.PERSOSUBSTATE_UNKNOWN;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        }
        while (true)
        {
            return localPersoSubState;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_IN_PROGRESS;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_READY;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_NETWORK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_NETWORK_SUBSET;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_CORPORATE;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_SERVICE_PROVIDER;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_SIM;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_NETWORK_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_NETWORK_SUBSET_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_CORPORATE_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_SERVICE_PROVIDER_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_SIM_SIM_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_NETWORK1;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_NETWORK2;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_HRPD;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_CORPORATE;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_SERVICE_PROVIDER;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_RUIM;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_NETWORK1_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_NETWORK2_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_HRPD_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_CORPORATE_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_SERVICE_PROVIDER_PUK;
            continue;
            localPersoSubState = PersoSubState.PERSOSUBSTATE_RUIM_RUIM_PUK;
        }
    }

    IccCardStatus.PinState PinStateFromRILInt(int paramInt)
    {
        IccCardStatus.PinState localPinState;
        switch (paramInt)
        {
        default:
            throw new RuntimeException("Unrecognized RIL_PinState: " + paramInt);
        case 0:
            localPinState = IccCardStatus.PinState.PINSTATE_UNKNOWN;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return localPinState;
            localPinState = IccCardStatus.PinState.PINSTATE_ENABLED_NOT_VERIFIED;
            continue;
            localPinState = IccCardStatus.PinState.PINSTATE_ENABLED_VERIFIED;
            continue;
            localPinState = IccCardStatus.PinState.PINSTATE_DISABLED;
            continue;
            localPinState = IccCardStatus.PinState.PINSTATE_ENABLED_BLOCKED;
            continue;
            localPinState = IccCardStatus.PinState.PINSTATE_ENABLED_PERM_BLOCKED;
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("{").append(this.app_type).append(",").append(this.app_state);
        if (this.app_state == AppState.APPSTATE_SUBSCRIPTION_PERSO)
            localStringBuilder.append(",").append(this.perso_substate);
        if ((this.app_type == AppType.APPTYPE_CSIM) || (this.app_type == AppType.APPTYPE_USIM) || (this.app_type == AppType.APPTYPE_ISIM))
        {
            localStringBuilder.append(",pin1=").append(this.pin1);
            localStringBuilder.append(",pin2=").append(this.pin2);
        }
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }

    public static enum PersoSubState
    {
        static
        {
            PERSOSUBSTATE_IN_PROGRESS = new PersoSubState("PERSOSUBSTATE_IN_PROGRESS", 1);
            PERSOSUBSTATE_READY = new PersoSubState("PERSOSUBSTATE_READY", 2);
            PERSOSUBSTATE_SIM_NETWORK = new PersoSubState("PERSOSUBSTATE_SIM_NETWORK", 3);
            PERSOSUBSTATE_SIM_NETWORK_SUBSET = new PersoSubState("PERSOSUBSTATE_SIM_NETWORK_SUBSET", 4);
            PERSOSUBSTATE_SIM_CORPORATE = new PersoSubState("PERSOSUBSTATE_SIM_CORPORATE", 5);
            PERSOSUBSTATE_SIM_SERVICE_PROVIDER = new PersoSubState("PERSOSUBSTATE_SIM_SERVICE_PROVIDER", 6);
            PERSOSUBSTATE_SIM_SIM = new PersoSubState("PERSOSUBSTATE_SIM_SIM", 7);
            PERSOSUBSTATE_SIM_NETWORK_PUK = new PersoSubState("PERSOSUBSTATE_SIM_NETWORK_PUK", 8);
            PERSOSUBSTATE_SIM_NETWORK_SUBSET_PUK = new PersoSubState("PERSOSUBSTATE_SIM_NETWORK_SUBSET_PUK", 9);
            PERSOSUBSTATE_SIM_CORPORATE_PUK = new PersoSubState("PERSOSUBSTATE_SIM_CORPORATE_PUK", 10);
            PERSOSUBSTATE_SIM_SERVICE_PROVIDER_PUK = new PersoSubState("PERSOSUBSTATE_SIM_SERVICE_PROVIDER_PUK", 11);
            PERSOSUBSTATE_SIM_SIM_PUK = new PersoSubState("PERSOSUBSTATE_SIM_SIM_PUK", 12);
            PERSOSUBSTATE_RUIM_NETWORK1 = new PersoSubState("PERSOSUBSTATE_RUIM_NETWORK1", 13);
            PERSOSUBSTATE_RUIM_NETWORK2 = new PersoSubState("PERSOSUBSTATE_RUIM_NETWORK2", 14);
            PERSOSUBSTATE_RUIM_HRPD = new PersoSubState("PERSOSUBSTATE_RUIM_HRPD", 15);
            PERSOSUBSTATE_RUIM_CORPORATE = new PersoSubState("PERSOSUBSTATE_RUIM_CORPORATE", 16);
            PERSOSUBSTATE_RUIM_SERVICE_PROVIDER = new PersoSubState("PERSOSUBSTATE_RUIM_SERVICE_PROVIDER", 17);
            PERSOSUBSTATE_RUIM_RUIM = new PersoSubState("PERSOSUBSTATE_RUIM_RUIM", 18);
            PERSOSUBSTATE_RUIM_NETWORK1_PUK = new PersoSubState("PERSOSUBSTATE_RUIM_NETWORK1_PUK", 19);
            PERSOSUBSTATE_RUIM_NETWORK2_PUK = new PersoSubState("PERSOSUBSTATE_RUIM_NETWORK2_PUK", 20);
            PERSOSUBSTATE_RUIM_HRPD_PUK = new PersoSubState("PERSOSUBSTATE_RUIM_HRPD_PUK", 21);
            PERSOSUBSTATE_RUIM_CORPORATE_PUK = new PersoSubState("PERSOSUBSTATE_RUIM_CORPORATE_PUK", 22);
            PERSOSUBSTATE_RUIM_SERVICE_PROVIDER_PUK = new PersoSubState("PERSOSUBSTATE_RUIM_SERVICE_PROVIDER_PUK", 23);
            PERSOSUBSTATE_RUIM_RUIM_PUK = new PersoSubState("PERSOSUBSTATE_RUIM_RUIM_PUK", 24);
            PersoSubState[] arrayOfPersoSubState = new PersoSubState[25];
            arrayOfPersoSubState[0] = PERSOSUBSTATE_UNKNOWN;
            arrayOfPersoSubState[1] = PERSOSUBSTATE_IN_PROGRESS;
            arrayOfPersoSubState[2] = PERSOSUBSTATE_READY;
            arrayOfPersoSubState[3] = PERSOSUBSTATE_SIM_NETWORK;
            arrayOfPersoSubState[4] = PERSOSUBSTATE_SIM_NETWORK_SUBSET;
            arrayOfPersoSubState[5] = PERSOSUBSTATE_SIM_CORPORATE;
            arrayOfPersoSubState[6] = PERSOSUBSTATE_SIM_SERVICE_PROVIDER;
            arrayOfPersoSubState[7] = PERSOSUBSTATE_SIM_SIM;
            arrayOfPersoSubState[8] = PERSOSUBSTATE_SIM_NETWORK_PUK;
            arrayOfPersoSubState[9] = PERSOSUBSTATE_SIM_NETWORK_SUBSET_PUK;
            arrayOfPersoSubState[10] = PERSOSUBSTATE_SIM_CORPORATE_PUK;
            arrayOfPersoSubState[11] = PERSOSUBSTATE_SIM_SERVICE_PROVIDER_PUK;
            arrayOfPersoSubState[12] = PERSOSUBSTATE_SIM_SIM_PUK;
            arrayOfPersoSubState[13] = PERSOSUBSTATE_RUIM_NETWORK1;
            arrayOfPersoSubState[14] = PERSOSUBSTATE_RUIM_NETWORK2;
            arrayOfPersoSubState[15] = PERSOSUBSTATE_RUIM_HRPD;
            arrayOfPersoSubState[16] = PERSOSUBSTATE_RUIM_CORPORATE;
            arrayOfPersoSubState[17] = PERSOSUBSTATE_RUIM_SERVICE_PROVIDER;
            arrayOfPersoSubState[18] = PERSOSUBSTATE_RUIM_RUIM;
            arrayOfPersoSubState[19] = PERSOSUBSTATE_RUIM_NETWORK1_PUK;
            arrayOfPersoSubState[20] = PERSOSUBSTATE_RUIM_NETWORK2_PUK;
            arrayOfPersoSubState[21] = PERSOSUBSTATE_RUIM_HRPD_PUK;
            arrayOfPersoSubState[22] = PERSOSUBSTATE_RUIM_CORPORATE_PUK;
            arrayOfPersoSubState[23] = PERSOSUBSTATE_RUIM_SERVICE_PROVIDER_PUK;
            arrayOfPersoSubState[24] = PERSOSUBSTATE_RUIM_RUIM_PUK;
        }

        boolean isPersoSubStateUnknown()
        {
            if (this == PERSOSUBSTATE_UNKNOWN);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    public static enum AppState
    {
        static
        {
            APPSTATE_DETECTED = new AppState("APPSTATE_DETECTED", 1);
            APPSTATE_PIN = new AppState("APPSTATE_PIN", 2);
            APPSTATE_PUK = new AppState("APPSTATE_PUK", 3);
            APPSTATE_SUBSCRIPTION_PERSO = new AppState("APPSTATE_SUBSCRIPTION_PERSO", 4);
            APPSTATE_READY = new AppState("APPSTATE_READY", 5);
            AppState[] arrayOfAppState = new AppState[6];
            arrayOfAppState[0] = APPSTATE_UNKNOWN;
            arrayOfAppState[1] = APPSTATE_DETECTED;
            arrayOfAppState[2] = APPSTATE_PIN;
            arrayOfAppState[3] = APPSTATE_PUK;
            arrayOfAppState[4] = APPSTATE_SUBSCRIPTION_PERSO;
            arrayOfAppState[5] = APPSTATE_READY;
        }

        boolean isAppNotReady()
        {
            if ((this == APPSTATE_UNKNOWN) || (this == APPSTATE_DETECTED));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean isAppReady()
        {
            if (this == APPSTATE_READY);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean isPinRequired()
        {
            if (this == APPSTATE_PIN);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean isPukRequired()
        {
            if (this == APPSTATE_PUK);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        boolean isSubscriptionPersoEnabled()
        {
            if (this == APPSTATE_SUBSCRIPTION_PERSO);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    public static enum AppType
    {
        static
        {
            APPTYPE_SIM = new AppType("APPTYPE_SIM", 1);
            APPTYPE_USIM = new AppType("APPTYPE_USIM", 2);
            APPTYPE_RUIM = new AppType("APPTYPE_RUIM", 3);
            APPTYPE_CSIM = new AppType("APPTYPE_CSIM", 4);
            APPTYPE_ISIM = new AppType("APPTYPE_ISIM", 5);
            AppType[] arrayOfAppType = new AppType[6];
            arrayOfAppType[0] = APPTYPE_UNKNOWN;
            arrayOfAppType[1] = APPTYPE_SIM;
            arrayOfAppType[2] = APPTYPE_USIM;
            arrayOfAppType[3] = APPTYPE_RUIM;
            arrayOfAppType[4] = APPTYPE_CSIM;
            arrayOfAppType[5] = APPTYPE_ISIM;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.IccCardApplication
 * JD-Core Version:        0.6.2
 */