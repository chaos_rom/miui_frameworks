package com.android.internal.telephony;

import android.os.AsyncResult;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayList;

public class AdnCacheManager
{
    public static final String SERVICE_NAME = "miui.adnCacheManager";
    private static final String TAG = "AdnCacheManager";
    private AdnRecordCache mAdnCache;
    private int mAdnCapacity;
    private int mFreeAdn;

    public AdnCacheManager(AdnRecordCache paramAdnRecordCache)
    {
        this.mAdnCache = paramAdnRecordCache;
    }

    public int getAdnCapacity()
    {
        return this.mAdnCapacity;
    }

    public int getFreeAdn()
    {
        return this.mFreeAdn;
    }

    public void handleAllAdnLikeLoaded(int paramInt, ArrayList<AdnRecord> paramArrayList)
    {
        if (paramInt == 20272)
        {
            this.mAdnCache.adnLikeFiles.put(28474, paramArrayList);
            this.mAdnCapacity = paramArrayList.size();
            Log.d("AdnCacheManager", "ADN capacity is " + this.mAdnCapacity);
        }
    }

    public void handleLoadAllAdnLike(int paramInt, AsyncResult paramAsyncResult)
    {
        if (paramInt == 20272)
            paramInt = 28474;
        this.mAdnCache.adnLikeFiles.put(paramInt, (ArrayList)paramAsyncResult.result);
        if ((paramInt != 28475) && (paramInt != 28489))
        {
            ArrayList localArrayList = (ArrayList)paramAsyncResult.result;
            this.mAdnCapacity = localArrayList.size();
            Log.d("AdnCacheManager", "ADN capacity is " + this.mAdnCapacity);
            for (int i = 0; i < localArrayList.size(); i++)
                if (((AdnRecord)localArrayList.get(i)).isEmpty())
                    this.mFreeAdn = (1 + this.mFreeAdn);
        }
    }

    public void handleNonExistentAdnRecord(int paramInt)
    {
        if (paramInt == 28474)
            this.mFreeAdn = 0;
    }

    public void handleUpdateAdnRecord(int paramInt, AdnRecord paramAdnRecord1, AdnRecord paramAdnRecord2)
    {
        if ((paramAdnRecord1.isEmpty()) && (paramInt == 28474))
            this.mFreeAdn = (-1 + this.mFreeAdn);
        while (true)
        {
            return;
            if ((paramAdnRecord2.isEmpty()) && (paramInt == 28474))
                this.mFreeAdn = (1 + this.mFreeAdn);
        }
    }

    public void reset()
    {
        this.mFreeAdn = 0;
        this.mAdnCapacity = 0;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.AdnCacheManager
 * JD-Core Version:        0.6.2
 */