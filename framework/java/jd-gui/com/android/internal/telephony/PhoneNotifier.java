package com.android.internal.telephony;

import android.telephony.CellInfo;

public abstract interface PhoneNotifier
{
    public abstract void notifyCallForwardingChanged(Phone paramPhone);

    public abstract void notifyCellInfo(Phone paramPhone, CellInfo paramCellInfo);

    public abstract void notifyCellLocation(Phone paramPhone);

    public abstract void notifyDataActivity(Phone paramPhone);

    public abstract void notifyDataConnection(Phone paramPhone, String paramString1, String paramString2, Phone.DataState paramDataState);

    public abstract void notifyDataConnectionFailed(Phone paramPhone, String paramString1, String paramString2);

    public abstract void notifyMessageWaitingChanged(Phone paramPhone);

    public abstract void notifyOtaspChanged(Phone paramPhone, int paramInt);

    public abstract void notifyPhoneState(Phone paramPhone);

    public abstract void notifyServiceState(Phone paramPhone);

    public abstract void notifySignalStrength(Phone paramPhone);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.PhoneNotifier
 * JD-Core Version:        0.6.2
 */