package com.android.internal.telephony;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;

public class AdnRecordLoader extends Handler
{
    static final int EVENT_ADN_LOAD_ALL_DONE = 3;
    static final int EVENT_ADN_LOAD_DONE = 1;
    static final int EVENT_EF_LINEAR_RECORD_SIZE_DONE = 4;
    static final int EVENT_EXT_RECORD_LOAD_DONE = 2;
    static final int EVENT_UPDATE_RECORD_DONE = 5;
    static final String LOG_TAG = "RIL_AdnRecordLoader";
    ArrayList<AdnRecord> adns;
    int ef;
    int extensionEF;
    private IccFileHandler mFh;
    int pendingExtLoads;
    String pin2;
    int recordNumber;
    Object result;
    Message userResponse;

    public AdnRecordLoader(IccFileHandler paramIccFileHandler)
    {
        super(Looper.getMainLooper());
        this.mFh = paramIccFileHandler;
    }

    public void handleMessage(Message paramMessage)
    {
        ArrayList localArrayList;
        int j;
        int k;
        try
        {
            int i = paramMessage.what;
            switch (i)
            {
            default:
                if ((this.userResponse != null) && (this.pendingExtLoads == 0))
                {
                    AsyncResult.forMessage(this.userResponse).result = this.result;
                    this.userResponse.sendToTarget();
                    this.userResponse = null;
                }
                return;
            case 4:
                localAsyncResult5 = (AsyncResult)paramMessage.obj;
                localAdnRecord4 = (AdnRecord)localAsyncResult5.userObj;
                if (localAsyncResult5.exception != null)
                    throw new RuntimeException("get EF record size failed", localAsyncResult5.exception);
                break;
            case 5:
            case 1:
            case 2:
            case 3:
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                AsyncResult localAsyncResult5;
                AdnRecord localAdnRecord4;
                if (this.userResponse != null)
                {
                    AsyncResult.forMessage(this.userResponse).exception = localRuntimeException;
                    this.userResponse.sendToTarget();
                    this.userResponse = null;
                    continue;
                    int[] arrayOfInt = (int[])localAsyncResult5.result;
                    if ((arrayOfInt.length != 3) || (this.recordNumber > arrayOfInt[2]))
                        throw new RuntimeException("get wrong EF record size format", localAsyncResult5.exception);
                    byte[] arrayOfByte3 = localAdnRecord4.buildAdnString(arrayOfInt[0]);
                    if (arrayOfByte3 == null)
                        throw new RuntimeException("wrong ADN format", localAsyncResult5.exception);
                    this.mFh.updateEFLinearFixed(this.ef, this.recordNumber, arrayOfByte3, this.pin2, obtainMessage(5));
                    this.pendingExtLoads = 1;
                    continue;
                    AsyncResult localAsyncResult4 = (AsyncResult)paramMessage.obj;
                    if (localAsyncResult4.exception != null)
                        throw new RuntimeException("update EF adn record failed", localAsyncResult4.exception);
                    this.pendingExtLoads = 0;
                    this.result = null;
                    continue;
                    AsyncResult localAsyncResult3 = (AsyncResult)paramMessage.obj;
                    byte[] arrayOfByte2 = (byte[])localAsyncResult3.result;
                    if (localAsyncResult3.exception != null)
                        throw new RuntimeException("load failed", localAsyncResult3.exception);
                    AdnRecord localAdnRecord3 = new AdnRecord(this.ef, this.recordNumber, arrayOfByte2);
                    this.result = localAdnRecord3;
                    if (localAdnRecord3.hasExtendedRecord())
                    {
                        this.pendingExtLoads = 1;
                        this.mFh.loadEFLinearFixed(this.extensionEF, localAdnRecord3.extRecord, obtainMessage(2, localAdnRecord3));
                        continue;
                        AsyncResult localAsyncResult2 = (AsyncResult)paramMessage.obj;
                        byte[] arrayOfByte1 = (byte[])localAsyncResult2.result;
                        AdnRecord localAdnRecord2 = (AdnRecord)localAsyncResult2.userObj;
                        if (localAsyncResult2.exception != null)
                            throw new RuntimeException("load failed", localAsyncResult2.exception);
                        Log.d("RIL_AdnRecordLoader", "ADN extension EF: 0x" + Integer.toHexString(this.extensionEF) + ":" + localAdnRecord2.extRecord + "\n" + IccUtils.bytesToHexString(arrayOfByte1));
                        localAdnRecord2.appendExtRecord(arrayOfByte1);
                        this.pendingExtLoads = (-1 + this.pendingExtLoads);
                    }
                }
            }
            AsyncResult localAsyncResult1 = (AsyncResult)paramMessage.obj;
            localArrayList = (ArrayList)localAsyncResult1.result;
            if (localAsyncResult1.exception != null)
                throw new RuntimeException("load failed", localAsyncResult1.exception);
            this.adns = new ArrayList(localArrayList.size());
            this.result = this.adns;
            this.pendingExtLoads = 0;
            j = 0;
            k = localArrayList.size();
        }
        while (j < k)
        {
            AdnRecord localAdnRecord1 = new AdnRecord(this.ef, j + 1, (byte[])localArrayList.get(j));
            this.adns.add(localAdnRecord1);
            if (localAdnRecord1.hasExtendedRecord())
            {
                this.pendingExtLoads = (1 + this.pendingExtLoads);
                this.mFh.loadEFLinearFixed(this.extensionEF, localAdnRecord1.extRecord, obtainMessage(2, localAdnRecord1));
            }
            j++;
        }
    }

    public void loadAllFromEF(int paramInt1, int paramInt2, Message paramMessage)
    {
        this.ef = paramInt1;
        this.extensionEF = paramInt2;
        this.userResponse = paramMessage;
        this.mFh.loadEFLinearFixedAll(paramInt1, obtainMessage(3));
    }

    public void loadFromEF(int paramInt1, int paramInt2, int paramInt3, Message paramMessage)
    {
        this.ef = paramInt1;
        this.extensionEF = paramInt2;
        this.recordNumber = paramInt3;
        this.userResponse = paramMessage;
        this.mFh.loadEFLinearFixed(paramInt1, paramInt3, obtainMessage(1));
    }

    public void updateEF(AdnRecord paramAdnRecord, int paramInt1, int paramInt2, int paramInt3, String paramString, Message paramMessage)
    {
        this.ef = paramInt1;
        this.extensionEF = paramInt2;
        this.recordNumber = paramInt3;
        this.userResponse = paramMessage;
        this.pin2 = paramString;
        this.mFh.getEFLinearRecordSize(paramInt1, obtainMessage(4, paramAdnRecord));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         com.android.internal.telephony.AdnRecordLoader
 * JD-Core Version:        0.6.2
 */